﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
using System.Web.Mvc;

namespace ReviewURLHarvester.API
{
    public static class WebApiConfig
    {
        public static void Register(HttpConfiguration config)
        {
            config.Routes.MapHttpRoute(
                name: "DefaultApi",
                routeTemplate: "api/{controller}/{id}",
                defaults: new { id = RouteParameter.Optional }                
            );


            config.Routes.MapHttpRoute(
                name: "SDReviewApi",
                routeTemplate: "SDReviewApi/{controller}/{dealerlocationname}/{city}/{state}/{zipcode}",
                defaults: new { dealerlocationname = UrlParameter.Optional, city = UrlParameter.Optional, state = UrlParameter.Optional, zipcode = UrlParameter.Optional, Action = "Get" }
            );

            config.Routes.MapHttpRoute(
                name: "SDReviewURLApi",
                routeTemplate: "SDReviewURLApi/{controller}/{reviewsource}/{dealerlocationname}/{city}/{state}/{zipcode}/",
                defaults: new { reviewsource = UrlParameter.Optional, 
                                dealerlocationname = UrlParameter.Optional, 
                                city = UrlParameter.Optional, 
                                state = UrlParameter.Optional,                                 
                                zipcode = UrlParameter.Optional,
                                Action = "GetURL"
                }
            );

            config.Routes.MapHttpRoute(
                name: "SDCarsDotComeApi",
                routeTemplate: "SDCarsDotComeApi/{controller}/{makes}/{radius}/{zipcode}",
                defaults: new { makes = UrlParameter.Optional, radius = UrlParameter.Optional, zipcode = UrlParameter.Optional, Action = "Get" }
            );

            config.Routes.MapHttpRoute(
                name: "SDCarsDotComeSeachApi",
                routeTemplate: "SDCarsDotComeSeachApi/{controller}/{makes}/{radius}/{zipcode}",
                defaults: new { makes = UrlParameter.Optional, radius = UrlParameter.Optional, zipcode = UrlParameter.Optional, Action = "GetCarsDotComDealer" }
            );

            config.Routes.MapHttpRoute(
                name: "SDFacebookSeachApi",
                routeTemplate: "SDFacebookSeachApi/{controller}/{dealerlocationname}",
                defaults: new { dealerlocationname = UrlParameter.Optional, Action = "Get" }
            );

            config.Routes.MapHttpRoute(
                name: "SDFacebookSeachVgroupApi",
                routeTemplate: "SDFacebookSeachVgroupApi/{controller}/{virtualgroupid}",
                defaults: new { virtualgroupid = UrlParameter.Optional, Action = "GetVgroupSearch" }
            );
                       
            config.Routes.MapHttpRoute(
                name: "SDValidatorApi",
                routeTemplate: "ValidatorApi/{controller}/{dealerlocationid}/{reviewsourceid}",
                defaults: new { dealerlocationid = UrlParameter.Optional, reviewsourceid = UrlParameter.Optional, Action = "Get" }
            );

            
            var appXmlType = config.Formatters.XmlFormatter.SupportedMediaTypes.FirstOrDefault(t => t.MediaType == "application/xml");
            config.Formatters.XmlFormatter.SupportedMediaTypes.Remove(appXmlType);
        }
    }
}
