﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using ReviewURLHarvester.BL;

namespace ReviewURLHarvester.API.Controllers
{
    public class FacebookController : ApiController
    {
        // GET api/values
        public FacebookSearch Get(string dealerlocationname)
        {  
            FacebookSearch objFacebookSearch = new FacebookSearch();
            try
            {
                objFacebookSearch =  Facebook.GetFacebookSearch(dealerlocationname);
            }
            catch (Exception ex)
            {
                return objFacebookSearch;
            }

            return objFacebookSearch;            
        }

        // GET api/values
        public List<FacebookSearch> GetVgroupSearch(int VirtualGroupID)
        {
            List<FacebookSearch> listFacebookSearch = new List<FacebookSearch>();
            try
            {
                listFacebookSearch = Facebook.GetFacebookSearchByVirtualGroupID(VirtualGroupID);
            }
            catch (Exception ex)
            {
                return listFacebookSearch;
            }

            return listFacebookSearch;

        }
    }
}
