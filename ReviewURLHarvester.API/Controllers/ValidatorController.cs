﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using ReviewURLHarvester.BL;

namespace ReviewURLHarvester.API.Controllers
{
    public class ValidatorController : ApiController
    {
        // GET api/values   
        public List<ValidationResult> Get(int dealerlocationid,int reviewsourceid)
        {
            List<ValidationResult> listValidationResult = new List<ValidationResult>();

            try
            {
                listValidationResult = Validator.ValidateURLs(dealerlocationid, reviewsourceid);
            }
            catch (Exception ex)
            {

            }
            return listValidationResult.OrderBy(x => x.ReviewsSourceID).ToList();
        }
    }
}
