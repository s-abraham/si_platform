﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using ReviewUrlSearch;
using ReviewUrlSearch.Models;

namespace ReviewURLHarvester.API.Controllers
{
    public class ReviewController : ApiController
    {
        // GET api/values
        public DealerLocationInfo Get(string dealerlocationname, string city, string state, string zipcode)
        {
            DealerLocationInfo objDealerLocationInfo = new DealerLocationInfo();
            var reviewHarvester = new ReviewHarvester();
            try
            {
                //List<ReviewURL> listReviewURLs  = Harvester.IdentifyLocationUrls(dealerlocationname, city, state,zipcode);
                List<ReviewURL> listReviewURLs = reviewHarvester.IdentifyLocationUrls(dealerlocationname, city, state, zipcode);

                objDealerLocationInfo.DealerLocationName = dealerlocationname;
                objDealerLocationInfo.ReviewURLs = listReviewURLs;
            }
            catch(Exception ex)
            {

            }
            return objDealerLocationInfo;
        }

        // GetURL api/values
        public DealerLocationInfo GetURL(int reviewSource, string dealerlocationname, string city, string state, string zipcode)
        {
            string URL = string.Empty;

            DealerLocationInfo objDealerLocationInfo = new DealerLocationInfo();
            var reviewHarvester = new ReviewHarvester();
            try
            {
                //List<ReviewURL> listReviewURLs  = Harvester.IdentifyLocationUrls(dealerlocationname, city, state, zipcode);
                URL = reviewHarvester.IdentifyLocationUrl((SI.DTO.ReviewSourceEnum)reviewSource, dealerlocationname, city, state, zipcode);

                List<ReviewURL> listReviewURL = new List<ReviewURL>();

                ReviewURL objReviewURL = new ReviewURL();
                objReviewURL.ReviewSourceID = reviewSource;
                objReviewURL.ReviewSourceName = ((SI.DTO.ReviewSourceEnum)reviewSource).ToString();
                objReviewURL.URL = URL;
                listReviewURL.Add(objReviewURL);
                objDealerLocationInfo.ReviewURLs = listReviewURL;
            }
            catch (Exception ex)
            {

            }
            return objDealerLocationInfo;
        }

        /*
        public List<DealerLocationInfo> Get(string makes, int radius, string zipcode)
        {
            List<DealerLocationInfo> listDealerLocationInfo = new List<DealerLocationInfo>();

            try
            {
                listDealerLocationInfo = Harvester.SearchCarsDotCom(makes, radius, zipcode);
            }
            catch (Exception ex)
            {
                return listDealerLocationInfo;
            }
            return listDealerLocationInfo;
        }

        public List<CarsDotComDealerLocationInfo> GetCarsDotComDealer(string makes, int radius, string zipcode)
        {
            List<CarsDotComDealerLocationInfo> listCarsDotComDealerLocationInfo = new List<CarsDotComDealerLocationInfo>();

            try
            {
                listCarsDotComDealerLocationInfo = Harvester.ExtractCarDealerImformationCarsDotCom(makes, radius, zipcode);
            }
            catch (Exception ex)
            {
                return listCarsDotComDealerLocationInfo;
            }
            return listCarsDotComDealerLocationInfo;
        }
        */
    }
}
