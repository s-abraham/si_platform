﻿using System;
using System.Collections.Generic;
using System.IO.Compression;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace SI.WEB
{
    public class CompressResponseAttribute : ActionFilterAttribute
    {
        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            HttpRequestBase request = filterContext.HttpContext.Request;

            string acceptEncoding = request.Headers["Accept-Encoding"];
            HttpResponseBase response = filterContext.HttpContext.Response;

            if (Settings.GetSetting<bool>("assets.html.compress"))
            {
                if (!String.IsNullOrEmpty(acceptEncoding))
                {
                    acceptEncoding = acceptEncoding.ToUpperInvariant();

                    if (acceptEncoding.Contains("GZIP"))
                    {
                        response.AppendHeader("Content-encoding", "gzip");
                        response.Filter = new GZipStream(
                            response.Filter,
                            CompressionMode.Compress
                        );
                    }
                    else if (acceptEncoding.Contains("DEFLATE"))
                    {
                        response.AppendHeader("Content-encoding", "deflate");
                        response.Filter = new DeflateStream(
                            response.Filter,
                            CompressionMode.Compress
                        );
                    }
                }
                if (response.ContentType == "text/html")
                {
                    if (Settings.GetSetting<bool>("assets.html.minify", true))
                        response.Filter = new MinifyHTMLFilter(response.Filter);
                }
            }



        }
    }
}
