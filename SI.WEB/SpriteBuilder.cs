﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SI.WEB
{    
    public static class SpriteBuilder
    {
        private static int _iconsPerLine = 10;


        public static SpriteInfo GenerateSprite(
            string iconDirectory,
            string spriteImageBaseURL
        )
        {
            StringBuilder css = new StringBuilder();
            int maxIcoHeight = 0;
            int maxIcoWidth = 0;

            StringBuilder hashFodder = new StringBuilder();

            Dictionary<string, Bitmap> icons = new Dictionary<string, Bitmap>();
            foreach (string pathName in Directory.GetFiles(iconDirectory))
            {
                string filename = Path.GetFileNameWithoutExtension(pathName);
                Bitmap bmp = new Bitmap(pathName);
                if (bmp.Height > maxIcoHeight)
                    maxIcoHeight = bmp.Height;
                if (bmp.Width > maxIcoWidth)
                    maxIcoWidth = bmp.Width;
                icons.Add(filename, bmp);

                byte[] imageData = Imaging.BytesFromImage(bmp);
                hashFodder.AppendFormat("{0}|{1}|", filename, Encoding.UTF8.GetString(imageData));
            }

            hashFodder.AppendFormat("||{0}", _iconsPerLine);
            string spriteHash = HashGenerator.GetURLFriendlyHash(hashFodder.ToString(), 15);

            int cols = _iconsPerLine;
            int rows = Convert.ToInt32(Math.Ceiling(Convert.ToSingle(icons.Count()) / Convert.ToSingle(cols)));
            int width = cols * maxIcoWidth;
            if (icons.Count() < cols)
            {
                width = icons.Count() * maxIcoWidth;
            }

            int height = rows * maxIcoHeight;

            int curCol = 1;
            int curRow = 1;
            using (Bitmap bmain = new Bitmap(width, height, System.Drawing.Imaging.PixelFormat.Format32bppArgb))
            {
                using (Graphics g = Graphics.FromImage(bmain))
                {
                    foreach (string iconName in icons.Keys)
                    {
                        int x = ((curCol - 1) * maxIcoWidth);
                        int y = ((curRow - 1) * maxIcoHeight);
                        Bitmap bIco = icons[iconName];

                        css.AppendFormat("\r\n.{0} {{ background:url({1}/{2}) -{3}px -{4}px no-repeat !important; width:{5}px; height:{6}px; }}",
                            iconName, spriteImageBaseURL, spriteHash, x, y, bIco.Width, bIco.Height
                        );


                        g.DrawImage(bIco, x, y, bIco.Width, bIco.Height);

                        bIco.Dispose();

                        curCol++;
                        if (curCol > cols)
                        {
                            curCol = 1;
                            curRow++;
                        }
                    }
                }

                SpriteInfo info = new SpriteInfo()
                {
                    CSS = css.ToString(),
                    ImageHash = spriteHash,
                    PNGData = Imaging.BytesFromImage(bmain, Imaging.ImageMode.PNG, 100)
                };

                return info;
            }

        }
    }

    public class SpriteInfo
    {
        public byte[] PNGData { get; set; }
        public string CSS { get; set; }
        public string ImageHash { get; set; }
    }

}
