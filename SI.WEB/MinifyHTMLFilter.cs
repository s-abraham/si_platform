﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SI.WEB
{
    public class MinifyHTMLFilter : MemoryStream
    {
        private StringBuilder outputString = new StringBuilder();
        private Stream outputStream = null;

        public MinifyHTMLFilter(Stream outputStream)
        {
            this.outputStream = outputStream;
        }

        public override void Write(byte[] buffer, int offset, int count)
        {
            outputString.Append(Encoding.UTF8.GetString(buffer));
        }

        public override void Close()
        {
            string result = MinifyHTML.Minify(outputString.ToString());            

            byte[] rawResult = Encoding.UTF8.GetBytes(result);
            outputStream.Write(rawResult, 0, rawResult.Length);

            base.Close();
            outputStream.Close();
        }

    }
}
