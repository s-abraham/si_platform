﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Yahoo.Yui.Compressor;

namespace SI.WEB
{

    /// <summary>
    /// Manages script and style resources for a website.
    /// </summary>
    public class AssetManager
    {
        private Dictionary<string, ThemeInfo> _themeCache = new Dictionary<string, ThemeInfo>();

        public AssetManager()
        {
            Rebuild();
        }

        /// <summary>
        /// Forces the Assetmanager to reload all script and style from files.
        /// </summary>
        public void Rebuild()
        {
            buildThemes();
        }

        /// <summary>
        /// Returns style CSS as text for a given theme
        /// </summary>
        /// <param name="themeName">The name of the theme</param>
        /// <returns>The CSS as a byte array</returns>
        public byte[] GetThemeCSS(string themeName)
        {
            themeName = themeName.Trim().ToLower();
            ThemeInfo info = null;
            if (_themeCache.TryGetValue(themeName, out info))
            {
                return info.CSS;
            }
            else
            {
                return new byte[0];
            }
        }

        /// <summary>
        /// Gets the icon sprite image for a given theme
        /// </summary>
        /// <param name="themeName">The name of the theme</param>
        /// <returns>The PNG sprite image as a byte array</returns>
        public byte[] GetThemeSpriteImage(string themeName)
        {
            themeName = themeName.Trim().ToLower();
            ThemeInfo info = null;
            if (_themeCache.TryGetValue(themeName, out info))
            {
                return info.SpriteData;
            }
            else
            {
                return new byte[0];
            }
        }

        /// <summary>
        /// Returns the calculated hash value for the data generated for a given theme's icon sprite
        /// </summary>
        /// <param name="themeName">The name of the theme</param>
        /// <returns>The URL safe calculated hash for the theme's icon sprite.</returns>
        public string GetSpriteHash(string themeName)
        {
            themeName = themeName.Trim().ToLower();
            ThemeInfo info = null;
            if (_themeCache.TryGetValue(themeName, out info))
            {
                return info.SpriteHash;
            }
            else
            {
                return "";
            }
        }

        /// <summary>
        /// Returns the calculated hash value for the data generated for a given theme
        /// </summary>
        /// <param name="themeName">The name of the theme</param>
        /// <returns>The URL safe calculated hash for the theme.</returns>
        public string GetThemeHash(string themeName)
        {
            themeName = themeName.Trim().ToLower();
            ThemeInfo info = null;
            if (_themeCache.TryGetValue(themeName, out info))
            {
                return info.CSSHash;
            }
            else
            {
                return "";
            }
        }

        private string _jsHash = "";
        private byte[] _jsData = null;

        /// <summary>
        /// The URL safe calculated hash value for the bundled Javascript
        /// </summary>
        public string JSHash
        {
            get
            {
                return _jsHash;
            }
        }

        /// <summary>
        /// The bundled javascript as a byte array
        /// </summary>
        public byte[] JSData
        {
            get
            {
                return _jsData;
            }
        }


        private void buildThemes()
        {
            string assetDir = Settings.GetSetting("site.assetdir");
            string styleDir = string.Format("{0}\\style", assetDir);

            //get the base non-theme layout css
            StringBuilder sbCSS = new StringBuilder();
            foreach (string fileName in Directory.GetFiles(styleDir, "*.css"))
            {
                sbCSS.AppendLine(File.ReadAllText(fileName));
            }

			string devTheme = Settings.GetSetting("assets.devtheme");

	        if (string.IsNullOrEmpty(devTheme))
	        {
		        //build themes
		        foreach (string themeDir in Directory.GetDirectories(styleDir))
		        {
			        buildTheme(themeDir, sbCSS.ToString());
		        }
	        }
	        else
	        {
		        string defaultTheme = Settings.GetSetting("site.defaulttheme");
				buildTheme(string.Format("{0}\\{1}", styleDir, defaultTheme), sbCSS.ToString());
				if (defaultTheme != devTheme)
					buildTheme(string.Format("{0}\\{1}", styleDir, devTheme), sbCSS.ToString());
	        }

	        //build bundled javascript
            buildJavascript();
        }

        private void buildJavascript()
        {
            string assetDir = Settings.GetSetting("site.assetdir");
            string scriptDir = string.Format("{0}\\script", assetDir);

            StringBuilder sb = new StringBuilder();
            sb.AppendFormat(" var __baseURL = '{0}'; ", Settings.GetSetting("site.baseurl"));

            foreach (string pathName in
                (
                from f in Directory.GetFiles(scriptDir, "*.js") orderby f select f
                )
            )
            {
                string fileName = Path.GetFileNameWithoutExtension(pathName);
                if (fileName.StartsWith("si."))
                {
                    sb.AppendLine(File.ReadAllText(pathName));
                }
            }

            string script = sb.ToString();

            if (Settings.GetSetting<bool>("assets.javascript.minify"))
            {
                JavaScriptCompressor compressor = new JavaScriptCompressor();
                compressor.IgnoreEval = true;
                compressor.PreserveAllSemicolons = true;
                compressor.CompressionType = CompressionType.Standard;
                compressor.ObfuscateJavascript = false;

                script = compressor.Compress(script);
            }

            byte[] scriptData = System.Text.Encoding.UTF8.GetBytes(script);
            _jsHash = HashGenerator.GetURLFriendlyHash(scriptData, 20);
            _jsData = scriptData;
        }

        private void buildTheme(string themeDir, string baseCSS)
        {
            string themeName = Path.GetFileNameWithoutExtension(themeDir).Trim().ToLower();

            lock (_themeCache)
            {
                bool hadSpriteInfo = false;
                string spriteHash = "";
                byte[] spriteData = null;
                string spriteCSS = "";

                ThemeInfo t;
                if (_themeCache.TryGetValue(themeName, out t))
                {
                    hadSpriteInfo = true;
                    spriteHash = t.SpriteHash;
                    spriteData = t.SpriteData;
                    spriteCSS = t.SpriteCSS;
                }


                string imageDir = string.Format("{0}\\images", themeDir);
                string iconDir = string.Format("{0}\\icons", themeDir);
                string spriteURL = string.Format("{0}/style/{1}/sprite", Settings.GetSetting("site.baseurl"), themeName);

                ThemeInfo tInfo = new ThemeInfo() { Name = themeName };

                StringBuilder sbCSS = new StringBuilder();
                sbCSS.AppendLine(baseCSS);

                foreach (string fileName in Directory.GetFiles(themeDir, "*.css"))
                {
                    sbCSS.AppendLine(File.ReadAllText(fileName));
                }

                if (!hadSpriteInfo || Settings.GetSetting<bool>("assets.icons.cache") == false)
                {
                    //generate the theme's sprite
                    SpriteInfo info = SpriteBuilder.GenerateSprite(iconDir, spriteURL);
                    spriteCSS = info.CSS;
                    spriteData = info.PNGData;
                    spriteHash = info.ImageHash;
                }

                sbCSS.AppendLine(spriteCSS);

                string css = sbCSS.ToString();

                //replace the "images" urls with the right path
                //images/bg_fallback.png
                //{baseURL}/assets/style/{themeName}/images/bg_fallback.png
                                
                css = css.Replace(
                    "images/",
                    string.Format("{0}/assets/style/{1}/images/", Settings.GetSetting("site.baseurl"), themeName)
                );

                css = css.Replace(
                    "textures/",
                    string.Format("{0}/assets/style/{1}/images/", Settings.GetSetting("site.baseurl"), themeName)
                );

                if (Settings.GetSetting<bool>("assets.css.minify"))
                {
                    CssCompressor compressor = new CssCompressor();
                    compressor.RemoveComments = true;
                    css = compressor.Compress(css);
                }



                tInfo.SpriteHash = spriteHash;
                tInfo.SpriteData = spriteData;
                tInfo.SpriteCSS = spriteCSS;
                tInfo.CSS = System.Text.Encoding.UTF8.GetBytes(css);
                tInfo.CSSHash = HashGenerator.GetURLFriendlyHash(css, 20);

                if (_themeCache.ContainsKey(tInfo.Name))
                    _themeCache.Remove(tInfo.Name);
                _themeCache.Add(tInfo.Name, tInfo);

            }
        }

    }

    public class ThemeInfo
    {
        public string Name { get; set; }
        public string CSSHash { get; set; }
        public byte[] CSS { get; set; }

        public byte[] SpriteData { get; set; }
        public string SpriteHash { get; set; }
        public string SpriteCSS { get; set; }
    }

}
