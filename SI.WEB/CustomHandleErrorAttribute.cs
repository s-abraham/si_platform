﻿using SI.BLL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace SI.WEB
{
    public class CustomHandleErrorAttribute : HandleErrorAttribute
    {
        private ILoggingProvider _log = ProviderFactory.Logging;

        public override void OnException(ExceptionContext context)
        {
            var ctx = context.HttpContext;
            var ex = context.Exception;

            if (ex != null)
            {
                _log.LogException(ex, ctx.Request.Url.OriginalString);                
            }

            
        }
    }
}
