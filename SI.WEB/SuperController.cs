﻿using SI.BLL;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace SI.WEB
{
    [CustomHandleError]
    public abstract class SuperController : Controller
    {
        public SuperController()
        {
            //testing localization
            //Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo("pl-US");
        }

        /// <summary>
        /// The logging provider for use with descendent controller classes
        /// </summary>
        protected ILoggingProvider _log = ProviderFactory.Logging;

        /// <summary>
        /// Renders a partial view to a string
        /// </summary>
        /// <param name="viewName">The name of the view to render</param>
        /// <param name="model">The data Model to provide to the partial view</param>
        /// <returns></returns>
        protected string RenderPartialToString(string viewName, object model)
        {
            if (string.IsNullOrEmpty(viewName))
                viewName = ControllerContext.RouteData.GetRequiredString("action");

            ViewData.Model = model;

            using (StringWriter sw = new StringWriter())
            {
                ViewEngineResult viewResult = ViewEngines.Engines.FindPartialView(ControllerContext, viewName);                
                ViewContext viewContext = new ViewContext(ControllerContext, viewResult.View, ViewData, TempData, sw);
                viewResult.View.Render(viewContext, sw);

                return sw.GetStringBuilder().ToString();
            }
        }

  

    }
}
