﻿using System.Linq.Expressions;
using System.Web;
using System.Web.Mvc.Html;
using System.Web.Routing;
using SI.BLL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace SI.WEB
{
    public static class HtmlExtensions
    {
		private const string spanWrapper = "<span id='{1}_span'>{0}</span>";

        public static string Local(this HtmlHelper html, string key)
        {
            return Locale.Localize(key);
        }

		public static MvcHtmlString Label(this HtmlHelper helper, string target, string text)
		{
			return MvcHtmlString.Create(string.Format("<label for='{0}'>{1}</label>", target, text));
		}

		public static MvcHtmlString SpanFor<TModel, TProperty>(this HtmlHelper<TModel> htmlHelper, Expression<Func<TModel, TProperty>> expression, object htmlAttributes = null)
		{
			string fullHtmlFieldName = htmlHelper.ViewContext.ViewData.TemplateInfo.GetFullHtmlFieldName(ExpressionHelper.GetExpressionText(expression)).Replace(".", "_");

			return MvcHtmlString.Create(string.Format(spanWrapper, htmlHelper.DisplayFor(expression, htmlAttributes), fullHtmlFieldName));
		}

		public static MvcHtmlString CheckBoxWithLabel(this HtmlHelper helper, string name, string text, string className = "", bool isChecked = false)
		{
			return MvcHtmlString.Create(string.Format("<input type='checkbox' id='{0}' name='{0}' class='{4}' value='true' {3}/> <label for='{0}'>{2}</label>", name, isChecked.ToString().ToLower(), text, isChecked ? "checked='checked'" : "", className));
		}

		public static MvcHtmlString RadioWithLabel(this HtmlHelper helper, string name, string groupName, string text, bool isChecked = false)
		{
			return MvcHtmlString.Create(string.Format("<input type='radio' id='{0}' name='{1}' value='{2}' {3}/> <label for='{0}'>{2}</label>", name, groupName, text, isChecked ? "checked='checked'" : ""));
		}

		public static MvcHtmlString CheckBoxForWithLabel<TModel>(this HtmlHelper<TModel> htmlHelper, Expression<Func<TModel, bool>> expression, string labelText, object htmlAttributes = null)
		{
			string fullHtmlFieldName = htmlHelper.ViewContext.ViewData.TemplateInfo.GetFullHtmlFieldName(ExpressionHelper.GetExpressionText(expression));

			return MvcHtmlString.Create(string.Format("{0} <label for='{1}'>{2}</label>", htmlHelper.CheckBoxFor(expression, htmlAttributes), fullHtmlFieldName.Replace(".", "_"), labelText));
		}

		public static MvcHtmlString RadioButtonForWithLabel<TModel>(this HtmlHelper<TModel> htmlHelper, Expression<Func<TModel, string>> expression, string labelText)
		{
			string fullHtmlFieldName = htmlHelper.ViewContext.ViewData.TemplateInfo.GetFullHtmlFieldName(ExpressionHelper.GetExpressionText(expression));

			string id = fullHtmlFieldName + labelText;

			var mergedHtmlAttributes = new RouteValueDictionary(new { @id = id });

			return MvcHtmlString.Create(string.Format("{0} <label for='{1}'>{2}</label>", htmlHelper.RadioButtonFor(expression, labelText, mergedHtmlAttributes), id.Replace(".", "_"), labelText));
		}

		public static MvcHtmlString RadioButtonForSelectList<TModel, TProperty>(this HtmlHelper<TModel> htmlHelper, Expression<Func<TModel, TProperty>> expression, IEnumerable<SelectListItem> listOfValues, object htmlAttributes = null)
		{
			var metaData = ModelMetadata.FromLambdaExpression(expression, htmlHelper.ViewData);

			System.Web.Mvc. TagBuilder divTag = new TagBuilder("div");
			divTag.MergeAttributes(new RouteValueDictionary(htmlAttributes), true);

			StringBuilder sb = new StringBuilder();

			if (listOfValues != null)
			{
				// Create a radio button for each item in the list
				foreach (SelectListItem item in listOfValues)
				{
					// Generate an id to be given to the radio button field
					string id = string.Format("{0}_{1}", metaData.PropertyName, item.Value);

					// Create and populate a radio button using the existing html helpers
					MvcHtmlString label = htmlHelper.Label(id, HttpUtility.HtmlEncode(item.Text));
					string radio = htmlHelper.RadioButtonFor(expression, item.Value, new { id = id }).ToHtmlString();

					// Create the html string that will be returned to the client
					// e.g. <input data-val="true" data-val-required="You must select an option" id="TestRadio_1" name="TestRadio" type="radio" value="1" /><label for="TestRadio_1">Line1</label>
					sb.AppendFormat("<div class=\"RadioButtonListItem\">{0}{1}</div>", radio, label);
				}
			}

			divTag.InnerHtml = sb.ToString();

			return MvcHtmlString.Create(divTag.ToString());
		}

		public static MvcHtmlString CheckBoxListFor<TModel, TProperty>(this HtmlHelper<TModel> htmlHelper, Expression<Func<TModel, TProperty[]>> expression, MultiSelectList multiSelectList, object htmlAttributes = null)
		{
			//Derive property name for checkbox name
			MemberExpression body = expression.Body as MemberExpression;
			string propertyName = body.Member.Name;

			//Get currently select values from the ViewData model
			TProperty[] list = expression.Compile().Invoke(htmlHelper.ViewData.Model);

			//Convert selected value list to a List<string> for easy manipulation
			List<string> selectedValues = new List<string>();

			if (list != null)
			{
				selectedValues = new List<TProperty>(list).ConvertAll<string>(i => i.ToString());
			}

			//Create div
			TagBuilder divTag = new TagBuilder("div");
			divTag.MergeAttributes(new RouteValueDictionary(htmlAttributes), true);

			StringBuilder sb = new StringBuilder();

			//Add checkboxes
			foreach (SelectListItem item in multiSelectList)
			{
				string itemValue = item.Value;
				if (string.IsNullOrEmpty(itemValue))
					itemValue = item.Text;

				sb.AppendFormat("<div class='CheckboxListItem'><input type=\"checkbox\" name=\"{0}\" id=\"{0}_{1}\" value=\"{1}\" {2} /><label for=\"{0}_{1}\">{3}</label></div>",
									propertyName,
									item.Value,
									selectedValues.Contains(itemValue) ? "checked=\"checked\"" : "",
									item.Text);
			}

			divTag.InnerHtml = sb.ToString();

			return MvcHtmlString.Create(divTag.ToString());
		}
    }
}
