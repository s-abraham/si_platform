﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Json;
using System.Text;
using System.Threading.Tasks;
using AWS;
using Connectors;
using Connectors.Parameters;
using Connectors.Social;
using Connectors.Social.Parameters;
using Facebook;
using Facebook.Entities;
using Google;
using Google.Model;
using JobLib;
using SI;
using SI.BLL;
using SI.DTO;
using Twitter;
using Twitter.Entities;

namespace Processor.TokenValidator
{
    public class Processor : ProcessorBase<TokenValidatorDownloaderData>
    {
        public Processor(string[] args) : base(args) { }

        protected override void start()
        {
            JobDTO job = getNextJob();

            while (job != null)
            {
                try
                {
                    setJobStatus(job.ID.Value, JobStatusEnum.Started);
                    TokenValidatorDownloaderData data = getJobData(job.ID.Value);

                    SocialCredentialDTO socialCredentialsDTO = null;
                    if (data.CredentialID.HasValue)
                        socialCredentialsDTO = ProviderFactory.Social.GetSocialCredentialInfo(data.CredentialID.Value); //data.CredentialID.Value
                    
                    bool isValid;
                    string ReasonForNotVaid = string.Empty;
                    switch ((SocialNetworkEnum)data.SocialNetwork)
                    {
                        case SocialNetworkEnum.Facebook:
                            {
                                SIFacebookPageParameters pageParameters = new SIFacebookPageParameters();
                                pageParameters.Token = socialCredentialsDTO.Token;
                                pageParameters.AppToken = socialCredentialsDTO.AppToken;
                                pageParameters.UniqueID = socialCredentialsDTO.UniqueID;

                                SIFacebookPageConnectionParameters ConnectionParameters = new SIFacebookPageConnectionParameters(pageParameters);
                                FacebookPageConnection pageconnection = new FacebookPageConnection(ConnectionParameters);

                                if (string.IsNullOrEmpty(ConnectionParameters.pageParameters.AppToken))
                                {
                                    isValid = pageconnection.ValidateTokenForOldCARFAXApp();

                                    if (isValid) //SPN-725
                                    {
                                        //check if the Page is still Active (call Graph API with APP Token from SocialApps Table)                                                                       
                                        FacebookPage objFacebookPage = pageconnection.GetFacebookPageInformation();

                                        if (!string.IsNullOrEmpty(objFacebookPage.facebookResponse.message))
                                        {
                                            isValid = false;
                                            ReasonForNotVaid = objFacebookPage.facebookResponse.message;
                                        }
                                        else if (string.IsNullOrEmpty(objFacebookPage.page.id))
                                        {
                                            isValid = false;
                                            ReasonForNotVaid = "Facebook Page not exists.(CARFAX OLD APP)";
                                        }
                                    }
                                    else
                                    {
                                        ReasonForNotVaid = "Facebook API Response (is_valid = false). (CARFAX OLD APP)";
                                    }
                                }
                                else
                                {
                                    FacebookTokenInfo objFacebookTokenInfo = pageconnection.ValidateToken2();

                                    if (objFacebookTokenInfo != null)
                                    {
                                        if (string.IsNullOrEmpty(objFacebookTokenInfo.facebookResponse.message))
                                        {
                                            isValid = objFacebookTokenInfo.debugToken.data.is_valid;

                                            if(!isValid)
                                                ReasonForNotVaid = "Facebook API Response (is_valid = false).";
                                        }
                                        else
                                        {
                                            isValid = false;
                                            ReasonForNotVaid = objFacebookTokenInfo.facebookResponse.message;
                                        }

                                        if (isValid) //SPN-725
                                        {
                                            //check if the Page is still Active (call Graph API with APP Token from SocialApps Table)                                                                       
                                            FacebookPage objFacebookPage = pageconnection.GetFacebookPageInformation();

                                            if (!string.IsNullOrEmpty(objFacebookPage.facebookResponse.message))
                                            {
                                                isValid = false;
                                                ReasonForNotVaid = objFacebookPage.facebookResponse.message;
                                            }
                                            else if (string.IsNullOrEmpty(objFacebookPage.page.id))
                                            {
                                                isValid = false;
                                                ReasonForNotVaid = "Facebook Page not exists.";
                                            }
                                            else if (string.IsNullOrEmpty(objFacebookTokenInfo.debugToken.data.profile_id))
                                            {
                                                isValid = false;
                                                ReasonForNotVaid = "Facebook Token is connected to user personal page.";
                                            }
                                            else // check if Token has right Scopes // SPN-720
                                            {
                                                bool has_publish_stream = false;
                                                bool has_read_insights = false;
                                                bool has_read_stream = false;
                                                bool has_manage_pages = false;
                                                bool has_create_event = false;
                                                bool has_read_mailbox = false;
                                                bool has_read_page_mailboxes = false;

                                                foreach (string scope in objFacebookTokenInfo.debugToken.data.scopes)
                                                {
                                                    //publish_stream,offline_access,read_insights,read_stream,manage_pages,create_event,read_mailbox,read_page_mailboxes
                                                    if (scope.ToLower() == "publish_stream".ToLower())
                                                    {
                                                        has_publish_stream = true;
                                                    }

                                                    if (scope.ToLower() == "read_insights".ToLower())
                                                    {
                                                        has_read_insights = true;
                                                    }

                                                    if (scope.ToLower() == "read_stream".ToLower())
                                                    {
                                                        has_read_stream = true;
                                                    }

                                                    if (scope.ToLower() == "manage_pages".ToLower())
                                                    {
                                                        has_manage_pages = true;
                                                    }

                                                    if (scope.ToLower() == "create_event".ToLower())
                                                    {
                                                        has_create_event = true;
                                                    }

                                                    if (scope.ToLower() == "read_mailbox".ToLower())
                                                    {
                                                        has_read_mailbox = true;
                                                    }

                                                    if (scope.ToLower() == "read_page_mailboxes".ToLower())
                                                    {
                                                        has_read_page_mailboxes = true;
                                                    }
                                                }

                                                if (!has_publish_stream || !has_read_insights || !has_read_stream || !has_manage_pages || !has_create_event || !has_read_mailbox || !has_read_page_mailboxes)
                                                {
                                                    isValid = false;
                                                    ReasonForNotVaid = "Token Does not have all Scope.";
                                                }
                                            }
                                        }
                                    }
                                    else
                                    {
                                        isValid = false;
                                        ReasonForNotVaid = "Not able to Debug Toke.";
                                    }
                                }
                                //isValid = pageconnection.ValidateToken();

                                ProviderFactory.Social.UpdateSocialConnection(socialCredentialsDTO.ID, isValid);

                                if (!isValid)
                                {
                                    SentTokenValidationEmail(socialCredentialsDTO);

                                    Auditor
                                        .New(AuditLevelEnum.Information, AuditTypeEnum.AccountActivity, UserInfoDTO.System, "")
                                        .SetAuditDataObject(
                                            AuditAccountActivity
                                                .New(socialCredentialsDTO.AccountID, AccountActivityTypeEnum.FacebookTokenInValid)
                                                .SetDescription("CredentialID [{0}] Marked as Invalid. Reason: [{1}]", socialCredentialsDTO.ID, ReasonForNotVaid)
                                        )
                                    .Save(ProviderFactory.Logging);
                                }

                                if (socialCredentialsDTO != null && isValid)
                                {
                                    if (string.IsNullOrWhiteSpace(socialCredentialsDTO.ScreenName))
                                    {
                                        pageParameters.UniqueID = socialCredentialsDTO.UniqueID;

                                        FacebookPage objFacebookPage = pageconnection.GetFacebookPageInformation();
                                        FaceBookUserPicture objFaceBookUserPicture = pageconnection.FaceBookUserPicture();

                                        if (string.IsNullOrEmpty(objFacebookPage.facebookResponse.message))
                                        {
                                            string screenName = objFacebookPage.page.name;
                                            string URL = objFacebookPage.page.link;
                                            
                                            string PictureURL = string.Empty;

                                            if (objFaceBookUserPicture != null)
                                            {
                                                if (objFaceBookUserPicture.picture.data != null)
                                                {
                                                    PictureURL = objFaceBookUserPicture.picture.data.url;
                                                }                                                
                                            }

                                            ProviderFactory.Social.UpdateCredential(socialCredentialsDTO.ID, screenName, URL, PictureURL);
                                        }
                                    }
                                }
                            }
                            break;
                        case SocialNetworkEnum.Google:
                            {
                                SIGooglePlusPageParameters pageParameters = new SIGooglePlusPageParameters();
                                pageParameters.accessToken = socialCredentialsDTO.Token;                                
                                pageParameters.client_id = socialCredentialsDTO.AppID;
                                pageParameters.client_secret = socialCredentialsDTO.AppSecret;
                                                                                             
                                SIGooglePlusPageConnectionParameters ConnectionParameters = new SIGooglePlusPageConnectionParameters(pageParameters);
                                GooglePlusPageConnection pageconnection = new GooglePlusPageConnection(ConnectionParameters);
                               
                                //isValid = pageconnection.ValidateToken();

                                GooglePlusAccessTokenRequestResponse objGooglePlusAccessTokenRequestResponse = pageconnection.ValidateToken2();

                                if (objGooglePlusAccessTokenRequestResponse != null)
                                {
                                    if (String.IsNullOrEmpty(objGooglePlusAccessTokenRequestResponse.access_token))
                                    {
                                        //string Error = "CredentialID: " + socialCredentialsDTO.ID + ",error: " + objGooglePlusAccessTokenRequestResponse.errorResponse.error + ",error_description: " + objGooglePlusAccessTokenRequestResponse.errorResponse.error_description + ",message: " + objGooglePlusAccessTokenRequestResponse.errorResponse.message;
                                        ReasonForNotVaid = objGooglePlusAccessTokenRequestResponse.errorResponse.error;
                                        //ProviderFactory.Logging.Audit(AuditLevelEnum.Information, AuditTypeEnum.TokenValidatorFailure, Error, null, null, null, null, null, null);
                                    }
                                    
                                }

                                if (objGooglePlusAccessTokenRequestResponse == null)
                                {
                                    isValid = false;
                                    ReasonForNotVaid = "Not Able to get GooglePlus API Response.";
                                }
                                else if (!String.IsNullOrEmpty(objGooglePlusAccessTokenRequestResponse.access_token))
                                {
                                    isValid =  true;
                                }
                                else
                                {
                                    isValid = false;
                                    ReasonForNotVaid = objGooglePlusAccessTokenRequestResponse.errorResponse.error;
                                }

                                if (!isValid)
                                {
                                    SentTokenValidationEmail(socialCredentialsDTO);

                                    Auditor
                                        .New(AuditLevelEnum.Information, AuditTypeEnum.AccountActivity, UserInfoDTO.System, "")
                                        .SetAuditDataObject(
                                            AuditAccountActivity
                                                .New(socialCredentialsDTO.AccountID, AccountActivityTypeEnum.GooglePlusTokenInValid)
                                                .SetDescription("CredentialID [{0}] Marked as Invalid. Reason: [{1}]", socialCredentialsDTO.ID, ReasonForNotVaid)
                                        )
                                    .Save(ProviderFactory.Logging);
                                }

                                ProviderFactory.Social.UpdateSocialConnection(socialCredentialsDTO.ID, isValid);

                                if (socialCredentialsDTO != null && isValid)
                                {
                                    if (string.IsNullOrWhiteSpace(socialCredentialsDTO.ScreenName))
                                    {
                                        pageParameters.UniqueID = socialCredentialsDTO.UniqueID;
                                        pageParameters.apiKey = "AIzaSyD6EAaOZy0_JJ8sDw_DqfeAU4J6-q69-28";

                                        GooglePlusGetSpecificPageResponse obj  = pageconnection.GetGooglePlusPagebyPageID();

                                        if (string.IsNullOrWhiteSpace(obj.error.message))                                
                                        {
                                            string screenName = obj.displayName;
                                            string URL = obj.url;
                                            
                                            string PictureURL = string.Empty;

                                            if (obj.image != null)
                                            {
                                                PictureURL = obj.image.url;
                                            }

                                            ProviderFactory.Social.UpdateCredential(socialCredentialsDTO.ID, screenName, URL, PictureURL);
                                        }
                                    }
                                }
                            }
                            break;
                        case SocialNetworkEnum.Twitter:
                            {
                                SITwitterPageParameters pageParameters = new SITwitterPageParameters();
                                pageParameters.oauth_token = socialCredentialsDTO.Token;
                                pageParameters.oauth_token_secret = socialCredentialsDTO.TokenSecret;
                                pageParameters.oauth_consumer_key = socialCredentialsDTO.AppID;
                                pageParameters.oauth_consumer_secret = socialCredentialsDTO.AppSecret;

                                SITwitterPageConnectionParameters ConnectionParameters = new SITwitterPageConnectionParameters(pageParameters);
                                TwitterPageConnection pageconnection = new TwitterPageConnection(ConnectionParameters);

                                //isValid = pageconnection.ValidateToken();
                                TwitterResponseToken result = pageconnection.ValidateToken2();

                                if (result != null)
                                {
                                    isValid = result.isValid;
                                    ReasonForNotVaid = result.ErrorMessage;
                                }
                                else
                                {
                                    isValid = false;  
                                    ReasonForNotVaid = "Not Able to get Twitter API Response.";
                                }

                                if (!isValid)
                                {
                                    SentTokenValidationEmail(socialCredentialsDTO);

                                    Auditor
                                        .New(AuditLevelEnum.Information, AuditTypeEnum.AccountActivity, UserInfoDTO.System, "")
                                        .SetAuditDataObject(
                                            AuditAccountActivity
                                                .New(socialCredentialsDTO.AccountID, AccountActivityTypeEnum.TwitterTokenInValid)
                                                .SetDescription("CredentialID [{0}] Marked as Invalid. Reason: [{1}]", socialCredentialsDTO.ID, ReasonForNotVaid)
                                        )
                                    .Save(ProviderFactory.Logging);
                                }

                                ProviderFactory.Social.UpdateSocialConnection(socialCredentialsDTO.ID, isValid);

                                if (socialCredentialsDTO != null && isValid)
                                {
                                    if (string.IsNullOrWhiteSpace(socialCredentialsDTO.ScreenName))
                                    {
                                        pageParameters.uniqueID = socialCredentialsDTO.UniqueID;

                                        // Page Detail and Update DataBase with Screen Name , URL
                                        TwitterPageInsightsResponse objTwitterPageInsightsResponse = pageconnection.GetTwitterPageInsights();

                                        if (string.IsNullOrEmpty(objTwitterPageInsightsResponse.errors.error))
                                        {
                                            string screenName = objTwitterPageInsightsResponse.screen_name;
                                            string URL = "https://twitter.com/" + screenName;
                                            
                                            ProviderFactory.Social.UpdateCredential(socialCredentialsDTO.ID, screenName, URL);
                                        }

                                    }
                                }
                            }
                            break;
                        case SocialNetworkEnum.YouTube:
                            break;
                        case SocialNetworkEnum.Blogger:
                            break;
                        case SocialNetworkEnum.WordPress:
                            break;
                        default:
                            break;
                    }

                    setJobStatus(job.ID.Value, JobStatusEnum.Completed);

                }
                catch (Exception ex)
                {
                    setJobStatus(job.ID.Value, JobStatusEnum.Failed);
                    ProviderFactory.Logging.LogException(ex, job.ID.Value);
                    ProviderFactory.Logging.Audit(AuditLevelEnum.Exception, AuditTypeEnum.ProcessorException, ex.Message, null, null, null, null, null, null);
                }

                job = getNextJob();
            }
        }

        private void SentTokenValidationEmail(SocialCredentialDTO socialCredentialsDTO)
        {
            if (socialCredentialsDTO != null)
            {
                if (socialCredentialsDTO.CreatedByUserID != 1)
                {
                    //Send SentTokenValidationEmail To User
                    ProviderFactory.Platform.SentTokenValidationEmail(socialCredentialsDTO);
                }
            }
        }

    }
}
