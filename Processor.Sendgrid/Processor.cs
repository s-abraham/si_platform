﻿using Connectors;
using JobLib;
using SendGrid;
using SI;
using SI.BLL;
using SI.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Processor.Sendgrid
{
    public class Processor : ProcessorBase<SendGridProcessorData>
    {
        // Need to pass the args through for the default constructor
        public Processor(string[] args) : base(args) { }

        protected override void start()
        {
            JobDTO job = getNextJob();

            while (job != null)
            {
                try
                {
                    setJobStatus(job.ID.Value, JobStatusEnum.Started);
                    SendGridProcessorData data = getJobData(job.ID.Value);

                    switch (data.Action)
                    {
                        case SendGridProcessorAction.CollectFailures:
                            {

                                List<SendGridEmailFailureDTO> processed = new List<SendGridEmailFailureDTO>();

                                // collect and record email bounces
                                DateTime beginDate = DateTime.Now.AddHours(-25);
                                //DateTime beginDate = DateTime.Now.AddDays(-1000);
                                DateTime endDate = DateTime.Now;

                                List<BouncedEmail> HardBouncedEmails = SendGridAPI.GetBouncedEmails(beginDate, endDate, SendGridBounceTypeEnum.Hard);
                                foreach (var hardBouncedEmail in HardBouncedEmails)
                                {
                                    //if (!(from p in processed where p.SendGridStatus == SendGridStatusEnum.HardBounced && p.EmailAddress == hardBouncedEmail.email select p).Any())
                                    processed.Add(ProviderFactory.Security.SaveSendgridEmailFailures(hardBouncedEmail.email, SendGridStatusEnum.HardBounced, hardBouncedEmail.created.Value));
                                }

                                List<BouncedEmail> SoftBouncedEmails = SendGridAPI.GetBouncedEmails(beginDate, endDate, SendGridBounceTypeEnum.Soft);
                                foreach (var softBouncedEmail in SoftBouncedEmails)
                                {
                                    //if (!(from p in processed where p.SendGridStatus == SendGridStatusEnum.Softbounced && p.EmailAddress == softBouncedEmail.email select p).Any())
                                    processed.Add(ProviderFactory.Security.SaveSendgridEmailFailures(softBouncedEmail.email, SendGridStatusEnum.Softbounced, softBouncedEmail.created.Value));
                                }

                                List<BlockedEmail> BlockedEmails = SendGridAPI.GetBlockedEmails(beginDate, endDate);
                                foreach (var blockedEmail in BlockedEmails)
                                {
                                    //if (!(from p in processed where p.SendGridStatus == SendGridStatusEnum.Blocked && p.EmailAddress == blockedEmail.email select p).Any())
                                    processed.Add(ProviderFactory.Security.SaveSendgridEmailFailures(blockedEmail.email, SendGridStatusEnum.Blocked, blockedEmail.created.Value));
                                }

                                List<InvalidEmail> InvalidEmails = SendGridAPI.GetInvalidEmails(beginDate, endDate);
                                foreach (var invalidEmail in InvalidEmails)
                                {
                                    //if (!(from p in processed where p.SendGridStatus == SendGridStatusEnum.MarkedInvalid && p.EmailAddress == invalidEmail.email select p).Any())
                                    processed.Add(ProviderFactory.Security.SaveSendgridEmailFailures(invalidEmail.email, SendGridStatusEnum.MarkedInvalid, invalidEmail.created.Value));
                                }

                                // queue the job to address queued status items

                                if (processed.Any())
                                {
                                    SendGridProcessorData newData = new SendGridProcessorData() { Action = SendGridProcessorAction.DeleteSendGridObjects };
                                    JobDTO newJob = new JobDTO()
                                    {
                                        DateScheduled = SIDateTime.Now,
                                        JobDataObject = newData,
                                        JobType = newData.JobType,
                                        JobStatus = JobStatusEnum.Queued,
                                        OriginJobID = job.ID.Value,
                                        Priority = 1000
                                    };
                                    SaveEntityDTO<JobDTO> dto = ProviderFactory.Jobs.Save(new SaveEntityDTO<JobDTO>(newJob, UserInfoDTO.System));
                                }

                                //send the diagnostics report
                                ProviderFactory.Security.GenerateSendGridCollectionReport(processed);

                                break;
                            }
                        case SendGridProcessorAction.DeleteSendGridObjects:
                            {
                                // delete bounces where applicable

                                List<SendGridEmailFailureDTO> queued = ProviderFactory.Security.GetSendGridEmailFailures(SendGridProcessingStatusEnum.Queued);
                                List<SendGridEmailFailureDTO> processed = new List<SendGridEmailFailureDTO>();

                                foreach (SendGridEmailFailureDTO item in queued)
                                {
                                    try
                                    {
                                        switch (item.SendGridStatus)
                                        {
                                            case SendGridStatusEnum.Softbounced:
                                            case SendGridStatusEnum.HardBounced:
                                                SendGridBounceTypeEnum bounceType = (item.SendGridStatus == SendGridStatusEnum.Softbounced) ? SendGridBounceTypeEnum.Soft : SendGridBounceTypeEnum.Hard;
                                                if (SendGridAPI.DeleteBouncedEmail(item.EmailAddress, bounceType))
                                                {
                                                    // success
                                                    processed.Add(ProviderFactory.Security.SetSendgridEmailProcessingStatus(item.EmailAddress, item.SendGridStatus, SendGridProcessingStatusEnum.Released));

                                                    Auditor
                                                        .New(AuditLevelEnum.Information, AuditTypeEnum.UserActivity, UserInfoDTO.System, "")
                                                        .SetAuditDataObject(
                                                            AuditUserActivity
                                                                .New(UserInfoDTO.System, AuditUserActivityTypeEnum.DeletedSendGridBounceOnEmailAddress)
                                                                .SetDescription("SendGrid Bounce deleted for Email Address: {0}", item.EmailAddress)
                                                                .SetUserIDActedUpon(item.MatchedUserID)
                                                        )
                                                    .Save(ProviderFactory.Logging);
                                                }
                                                else
                                                {
                                                    // failed
                                                    processed.Add(ProviderFactory.Security.SetSendgridEmailProcessingStatus(item.EmailAddress, item.SendGridStatus, SendGridProcessingStatusEnum.DeleteError));
                                                }
                                                break;

                                            case SendGridStatusEnum.Blocked:
                                                if (SendGridAPI.DeleteBlockedEmail(item.EmailAddress))
                                                {
                                                    // success
                                                    processed.Add(ProviderFactory.Security.SetSendgridEmailProcessingStatus(item.EmailAddress, item.SendGridStatus, SendGridProcessingStatusEnum.Released));

                                                    Auditor
                                                        .New(AuditLevelEnum.Information, AuditTypeEnum.UserActivity, UserInfoDTO.System, "")
                                                        .SetAuditDataObject(
                                                            AuditUserActivity
                                                                .New(UserInfoDTO.System, AuditUserActivityTypeEnum.DeletedSendGridBlockOnEmailAddress)
                                                                .SetDescription("SendGrid Block deleted for Email Address: {0}", item.EmailAddress)
                                                                .SetUserIDActedUpon(item.MatchedUserID)
                                                        )
                                                    .Save(ProviderFactory.Logging);
                                                }
                                                else
                                                {
                                                    // failed
                                                    processed.Add(ProviderFactory.Security.SetSendgridEmailProcessingStatus(item.EmailAddress, item.SendGridStatus, SendGridProcessingStatusEnum.DeleteError));
                                                }
                                                break;

                                            case SendGridStatusEnum.MarkedInvalid:
                                                if (SendGridAPI.DeleteInvalidEmail(item.EmailAddress))
                                                {
                                                    // success
                                                    processed.Add(ProviderFactory.Security.SetSendgridEmailProcessingStatus(item.EmailAddress, item.SendGridStatus, SendGridProcessingStatusEnum.Released));

                                                    Auditor
                                                        .New(AuditLevelEnum.Information, AuditTypeEnum.UserActivity, UserInfoDTO.System, "")
                                                        .SetAuditDataObject(
                                                            AuditUserActivity
                                                                .New(UserInfoDTO.System, AuditUserActivityTypeEnum.DeletedSendGridInvalidMarkOnEmailAddress)
                                                                .SetDescription("SendGrid Invalid mark deleted for Email Address: {0}", item.EmailAddress)
                                                                .SetUserIDActedUpon(item.MatchedUserID)
                                                        )
                                                    .Save(ProviderFactory.Logging);
                                                }
                                                else
                                                {
                                                    // failed
                                                    processed.Add(ProviderFactory.Security.SetSendgridEmailProcessingStatus(item.EmailAddress, item.SendGridStatus, SendGridProcessingStatusEnum.DeleteError));
                                                }
                                                break;

                                            default:
                                                break;
                                        }

                                    }
                                    catch (Exception ex)
                                    {
                                        ProviderFactory.Logging.LogException(ex, job.ID.Value);
                                    }
                                }

                                ProviderFactory.Security.GenerateSendGridActionReport(processed);
                                break;
                            }

                        default:
                            break;
                    }






                    setJobStatus(job.ID.Value, JobStatusEnum.Completed);
                }

                catch (Exception ex)
                {
                    setJobStatus(job.ID.Value, JobStatusEnum.Failed);
                    ProviderFactory.Logging.LogException(ex);
                }

                job = getNextJob();
            }
        }
    }
}
