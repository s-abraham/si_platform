﻿using SI.BLL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Processor.Sendgrid
{
    class Program
    {
        private static void Main(string[] args)
        {
            try
            {
                Processor p = new Processor(args);
                p.Start();
            }
            catch (Exception ex)
            {
                try
                {
                    ProviderFactory.Logging.LogException(ex);
                }
                catch (Exception)
                {
                }
            }
        }
    }
}
