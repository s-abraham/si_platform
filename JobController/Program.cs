﻿using JobLib;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace JobController
{
    class Program
    {
        static void Main(string[] args)
        {
            Controller c = new Controller();
            c.Start();

            do
            {
                Thread.Sleep(500);
            } while (true);
        }
    }
}
