﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using JobLib;
using SI.BLL;
using SI.DTO;

namespace Processor.Notification
{
    public class Processor : ProcessorBase<NotificationProcessorData>
    {
        // Need to pass the args through for the default constructor
        public Processor(string[] args) : base(args) { }

        //static void Main(string[] args)
        protected override void start()
        {
            JobDTO job = getNextJob();

            while (job != null)
            {
                try
                {
                    setJobStatus(job.ID.Value, JobStatusEnum.Started);

                    // data is currently not used, but we may use it in the future to control how often
                    // we process different types of notification
                    //NotificationDownloaderData data = getJobData(job.ID.Value);
                    ProviderFactory.Notifications.ProcessNotifications();

                    setJobStatus(job.ID.Value, JobStatusEnum.Completed);
                }
                catch (Exception ex)
                {
                    setJobStatus(job.ID.Value, JobStatusEnum.Failed);
                    ProviderFactory.Logging.LogException(ex);
                    Auditor
                        .New(AuditLevelEnum.Exception, AuditTypeEnum.ProcessorException, new UserInfoDTO(1, null), ex.ToString())
                        .Save(ProviderFactory.Logging)
                    ;
                }
                job = getNextJob();
            }
        }
    }
}
