﻿using Connectors;
using Connectors.Parameters;
using Connectors.URL_Validators.Parameters;
using JobLib;
using SI;
using SI.BLL;
using SI.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Processor.ReviewURLValidator
{
    public class Processor : ProcessorBase<ReviewURLValidatorData>
    {
        // Need to pass the args through for the default constructor
        public Processor(string[] args) : base(args) { }

        protected override void start()
        {
            JobDTO job = getNextJob();

            while (job != null)
            {
                try
                {

                    setJobStatus(job.ID.Value, JobStatusEnum.Started);
                    ReviewURLValidatorData data = getJobData(job.ID.Value);

                    //create the connection

                    SIReviewURLValidatorConnection connection = null;

                    switch (data.ReviewSource)
                    {
                        case ReviewSourceEnum.Edmunds:
                            break;
                        case ReviewSourceEnum.Google:
                            break;
                        case ReviewSourceEnum.Yelp:

                            //YelpReviewURLValidatorXPathParameters xPathParameters = new YelpReviewURLValidatorXPathParameters(
                            //    headerPath: ProviderFactory.Reviews.GetProcessorSetting("Yelp.HeaderPath"),
                            //    addressHeaderPath: ProviderFactory.Reviews.GetProcessorSetting("Yelp.AddressHeaderPath"),
                            //    addressPath: ProviderFactory.Reviews.GetProcessorSetting("Yelp.AddressPath"),
                            //    cityPath: ProviderFactory.Reviews.GetProcessorSetting("Yelp.CityPath"),
                            //    locationNamePath: ProviderFactory.Reviews.GetProcessorSetting("Yelp.LocationNamePath"),
                            //    parentnodeRatingXPath: "", //ProviderFactory.Reviews.GetProcessorSetting("Yelp.ParentNodeRatingXPath"), //!!
                            //    parentNodeReviewCountXPath: "", //ProviderFactory.Reviews.GetProcessorSetting("Yelp.ParentNodeReviewCountXPath"), //!!
                            //    statePath: ProviderFactory.Reviews.GetProcessorSetting("Yelp.StatePath"),
                            //    zipPath: ProviderFactory.Reviews.GetProcessorSetting("Yelp.ZipPath"),
                            //    phonePath: ProviderFactory.Reviews.GetProcessorSetting("Yelp.PhonePath"),
                            //    websiteURLPath: ProviderFactory.Reviews.GetProcessorSetting("Yelp.WebsiteURLPath"),
                            //    reviewCountCollectedXPath: ProviderFactory.Reviews.GetProcessorSetting("Yelp.ReviewCountCollectedXPath"),
                            //    queryBasePath: ProviderFactory.Reviews.GetProcessorSetting("Yelp.QueryBasePath"),
                            //    parentURLPath: ProviderFactory.Reviews.GetProcessorSetting("Yelp.ParentURLPath"),
                            //    urlPath: ProviderFactory.Reviews.GetProcessorSetting("Yelp.URLPath")
                            //);

                            //YelpReviewURLValidatorParameters parameters = new YelpReviewURLValidatorParameters(
                            //    timeoutSeconds: 10, useProxy: true, maxRedirects: 10,
                            //    companyName: data.CompanyName, city: data.City, stateFull: data.StateFull, stateAbbrev: data.StateAbbrev, zip: data.Zip, streetAddress: data.StreetAddress,
                            //    xPaths: xPathParameters
                            //);

                            //connection = new YelpReviewURLValidatorConnection(parameters);


                            break;
                        case ReviewSourceEnum.CarsDotCom:
                            break;
                        case ReviewSourceEnum.Yahoo:
                            break;
                        case ReviewSourceEnum.YellowPages:
                            break;
                        case ReviewSourceEnum.DealerRater:
                            break;
                        case ReviewSourceEnum.JudysBook:
                            break;
                        case ReviewSourceEnum.CitySearch:
                            break;
                        case ReviewSourceEnum.CarDealer:
                            break;
                        case ReviewSourceEnum.InsiderPages:
                            break;
                        default:
                            break;
                    }

                    //get the existing url if it exists
                    ReviewUrlDTO dto = ProviderFactory.Reviews.GetReviewUrl(UserInfoDTO.System, data.AccountID, data.ReviewSource);

                    bool foundNewURL = false;

                    //if the url does not exist or is invalid, try to find it
                    if (dto == null || dto.isValid == false)
                    {
                        string url = connection.HarvestReviewURL();

                        if (string.IsNullOrWhiteSpace(url))
                        {
                            ProviderFactory.Logging.Audit(AuditLevelEnum.Warning, AuditTypeEnum.ReviewURLHarvestFailed,
                                string.Format("{0} harvest failed for {1}", data.ReviewSource.ToString(), data.CompanyName),
                                new AuditReviewURLHarvestInfo() { AccountID = data.AccountID, ReviewSource = data.ReviewSource },
                                null, data.AccountID, null, null, null
                            );
                        }
                        else
                        {
                            if (dto == null)
                                dto = new ReviewUrlDTO() { AccountID = data.AccountID, ReviewSource = data.ReviewSource, HtmlURL = url };
                            else
                                dto.HtmlURL = url;

                            foundNewURL = true;
                        }
                    }

                    // we have a url to validate if there's a dto
                    if (dto != null)
                    {
                        dto.LastValidation = SIDateTime.Now;
                        ReviewURLValidatorData accountInfo = new ReviewURLValidatorData(
                        companyName: data.CompanyName, 
                        address: data.StreetAddress, 
                        city: data.City, 
                        stateFull: data.StateFull, 
                        stateAbbrev: data.StateAbbrev, 
                        zip: data.Zip, 
                        phone: data.Phone);

                        ReviewURLValidationResult result = connection.ValidateReviewURL(dto.HtmlURL, accountInfo);
                        dto.isValid = result.IsValidURL;

                        if (result.ValidationMisMatch.Count == 0)
                        {

                            if (foundNewURL)
                            {
                                ProviderFactory.Logging.Audit(AuditLevelEnum.Information, AuditTypeEnum.ReviewURLHarvestSucceeded,
                                    string.Format("{0} harvest succeeded for {1}", data.ReviewSource.ToString(), data.CompanyName),
                                    new AuditReviewURLHarvestInfo() { AccountID = data.AccountID, ReviewSource = data.ReviewSource },
                                    null, data.AccountID, null, null, null
                                );
                            }

                            ProviderFactory.Logging.Audit(AuditLevelEnum.Information, AuditTypeEnum.ReviewURLValidationSucceeded,
                                string.Format("{0} validation succeeded for {1}", data.ReviewSource.ToString(), data.CompanyName),
                                new AuditReviewURLValidationInfo() { AccountID = data.AccountID, ReviewSource = data.ReviewSource, URL = dto.HtmlURL },
                                null, data.AccountID, null, null, null
                            );
                        }
                        else
                        {
                            if (foundNewURL)
                            {
                                ProviderFactory.Logging.Audit(AuditLevelEnum.Warning, AuditTypeEnum.ReviewURLHarvestFailed,
                                    string.Format("{0} harvest failed for {1}", data.ReviewSource.ToString(), data.CompanyName),
                                    new AuditReviewURLHarvestInfo() { AccountID = data.AccountID, ReviewSource = data.ReviewSource },
                                    null, data.AccountID, null, null, null
                                );
                            }

                            //ProviderFactory.Logging.Audit(AuditLevelEnum.Warning, AuditTypeEnum.ReviewURLValidationFailed,
                            //    string.Format("{0} validation failed for {1}", data.ReviewSource.ToString(), data.CompanyName),
                            //    new AuditReviewURLValidationInfo() { AccountID = data.AccountID, ReviewSource = data.ReviewSource, URL = dto.HtmlURL, Reasons = result.ValidationMisMatch},
                            //    null, data.AccountID, null, null, null
                            //);
                        }

                        ProviderFactory.Reviews.SaveReviewUrl(UserInfoDTO.System, dto);
                    }

                }
                catch (Exception ex)
                {
                    setJobStatus(job.ID.Value, JobStatusEnum.Failed);
                    ProviderFactory.Logging.LogException(ex);
                }

                setJobStatus(job.ID.Value, JobStatusEnum.Completed);
                job = getNextJob();
            }
        }
    }
}
