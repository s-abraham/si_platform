﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using YouTube;
using YouTube.Entities;

namespace SDKTest
{
    class YouTubeAPITest
    {
        static void Main(string[] args)
        {
            var testNumber = 4;
            User YoutubeUser = new User();

            string userid = "hendrickhyundainorth";
            switch (testNumber)
            {
                case 1:
                    {
                        #region User Profile
                        UserProfileResult userProfileResult = YoutubeUser.GetUserProfile(userid);
                        #endregion
                    }
                    break;
                case 2:
                    {
                        #region ChannelInfo
                        UserProfileResult userProfileResult = YoutubeUser.GetUserProfile(userid);

                        if (userProfileResult != null)
                        {
                            if (!string.IsNullOrEmpty(userProfileResult.ChannelId))
                            {
                                string channelId = userProfileResult.ChannelId;

                                ChannelInfoResult channelInfoResult = YoutubeUser.GetChannelInfo(channelId);                                
                            }
                        }
                        #endregion
                    }
                    break;
                case 3:
                    {
                        #region ChannelVideos
                        UserProfileResult userProfileResult = YoutubeUser.GetUserProfile(userid);

                        if (userProfileResult != null)
                        {
                            if (!string.IsNullOrEmpty(userProfileResult.ChannelId))
                            {
                                string channelId = userProfileResult.ChannelId;

                                ChannelInfoResult channelInfoResult = YoutubeUser.GetChannelInfo(channelId);

                                if (channelInfoResult != null)
                                {
                                    if (!string.IsNullOrEmpty(channelInfoResult.PlaylistUploads))
                                    {
                                        string playlistid = channelInfoResult.PlaylistUploads;

                                        VideosList videosList = YoutubeUser.GetChannelVideos(playlistid);
                                    }
                                }                                
                            }
                        }
                        #endregion
                    }
                    break;
                case 4:
                    {
                        #region GetVideoInfoforPublish
                        YouTubeVideoInfo youTubeVideoInfo = YoutubeUser.GetVideoInfoforPublish("http://www.youtube.com/watch?v=Qjlc36Dv0rw");
                        #endregion
                    }
                    break;
            }
        }
    }
}
