﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Bitly;
using ReviewUrlSearch;
using ReviewUrlSearch.Models;
using ReviewUrlSearch.Parameters;
using SI;
using SI.DTO;
using YouTube;

namespace SDKTest
{
    class Harvester
    {
        static void Main(string[] args)
        {          
            var reviewHarvester = new ReviewHarvester();
            var searchParameters = new SearchParameters();
            var searchAccountsList = new List<SearchParameters>();

            //searchParameters.ReviewSource = SI.DTO.ReviewSourceEnum.DealerRater;
            //searchAccountsList.Add(new SearchParameters(201479, ReviewSourceEnum.All, "Carter Subaru 18833", "17225 Aurora Ave N", "Shoreline", "WA", "98133"));
            searchAccountsList.Add(new SearchParameters(201480, ReviewSourceEnum.All, "Carter Subaru Ballard 641238", "5201 Leary Ave Nw", "Seattle", "WA", "98199"));
            searchAccountsList.Add(new SearchParameters(201482, ReviewSourceEnum.All, "Carter Volkswagen 18819", "5202 Leary Ave Nw", "Seattle", "WA", "98199"));
            searchAccountsList.Add(new SearchParameters(208876, ReviewSourceEnum.All, "Kearny Mesa Subaru 765958", "4797 Convoy St", "San Diego", "CA", "92198"));
            searchAccountsList.Add(new SearchParameters(212626, ReviewSourceEnum.All, "Nate Wade Subaru 18071", "1207 S Main St", "Salt Lake City", "UT", "84171"));
            searchAccountsList.Add(new SearchParameters(214917, ReviewSourceEnum.All, "Renick Subaru 646241", "1100 S Euclid St", "Fullerton", "CA", "92838"));
            searchAccountsList.Add(new SearchParameters(218581, ReviewSourceEnum.All, "Tucson Subaru 766", "6020 E Speedway Blvd", "Tucson", "AZ", "85757"));

            



            foreach (var item in searchAccountsList)
            {
                List<ReviewURL> listReviewURL = reviewHarvester.IdentifyLocationUrls(item.CompanyName, item.City, item.State, item.Zip, true, item.AccountId);
                
                var insertStatement = "INSERT INTO [dbo].[Accounts_ReviewSources] ([AccountID] ,[ReviewSourceID] ,[URL] ,[URLAPI] ,[ExternalID] ,[ExternalID2] ,[LastSummeryAttempted] ,[LastSummaryRun] ,[LastDetailAttempted] ,[LastDetailRun] ,[LastValidation] ,[IsValid],[DateCreated]) VALUES (";
                var iCounter = 1;

                foreach (var reviewURL in listReviewURL)
                {
                    if (!string.IsNullOrEmpty(reviewURL.URL))
                    {
                        //Console.WriteLine(iCounter + ". " + reviewURL.ReviewSourceName + " : " + reviewURL.URL);
                        Debug.WriteLine("INSERT INTO [dbo].[Accounts_ReviewSources] ([AccountID] ,[ReviewSourceID] ,[URL] ,[URLAPI] ,[ExternalID] ,[ExternalID2] ,[LastSummeryAttempted] ,[LastSummaryRun] ,[LastDetailAttempted] ,[LastDetailRun] ,[LastValidation] ,[IsValid],[DateCreated]) VALUES ({0}, {1}, '{2}', null, null, null, null, null, null, null, null, 0, '2013-11-21 13:34:01.001')", item.AccountId, reviewURL.ReviewSourceID, reviewURL.URL);
                        iCounter++;    
                    }
                }
                Debug.WriteLine("--{0} URLs Found", iCounter);
            }
            


        }
    }
}
