﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using AWS;
using Connectors.Social;
using Connectors.Social.Parameters;
using Google;
using Google.Model;
using Newtonsoft.Json;
using SI.BLL;
using SI.DTO;

namespace SDKTest
{
    class GoogleAPITest 
    {
        static void Main(string[] args)
        {
            GooglePlusGetActivityListByPageResponse posts = GoogleAPI.GetGooglePlusActivitiesListForAPage("107954436489136926687", "AIzaSyD6EAaOZy0_JJ8sDw_DqfeAU4J6-q69-28");

            int iCounter = 1;
            
            foreach (var post in posts.items)
            {               
                string posttype = "status";
                string LinkURL = string.Empty;
                string PictureURL = string.Empty;
                string PostMessage = string.Empty;

                foreach (var item in (dynamic)post.@object)
                {
                    string key = item.Key;

                    if (key.ToLower() == "content")
                    {                        
                        PostMessage = HttpUtility.HtmlDecode(item.Value);
                    }

                    if (key.ToLower() == "attachments")
                    {                        
                        foreach (var attachment in item.Value[0])
                        {
                            key = attachment.Key;
                            if (key.ToLower() == "objecttype")
                            {
                                if (attachment.Value.ToLower() == "article")
                                {
                                    posttype = "link";
                                }
                                else if (attachment.Value.ToLower() == "photo")
                                {
                                    posttype = "photo";
                                }
                                else if (attachment.Value.ToLower() == "video")
                                {
                                    posttype = "video";
                                }
                            }

                            if (posttype == "link" && key.ToLower() == "url")
                            {
                                LinkURL = attachment.Value;
                                PictureURL = string.Empty;
                            }

                            if (posttype == "photo" && key.ToLower() == "image")
                            {
                                foreach (var image in attachment.Value)
                                {
                                    key = image.Key;

                                    if (key.ToLower() == "url")
                                    {
                                        PictureURL = image.Value;
                                        LinkURL = string.Empty;
                                    }
                                }
                            }
                        }
                    }

                }

                Debug.WriteLine(iCounter);
                Debug.WriteLine(" Post Type : " + posttype);
                Debug.WriteLine(" ResultID : " + post.url);
                Debug.WriteLine(" GooglePlusPostID : " + post.id);
                Debug.WriteLine(" Name : " + string.Empty);
                Debug.WriteLine(" Message : " + PostMessage);
                Debug.WriteLine(" LinkURL : " + LinkURL);
                Debug.WriteLine(" PictureURL : " + string.Empty);

                iCounter++;

                //Console.WriteLine(o.GetType().GetProperty("Name").GetValue(o, null));

                //var a = objects.Items[5];
                //(new System.Collections.Generic.Mscorlib_DictionaryDebugView<string, object>(((System.Collections.Generic.Dictionary<string, object>)()))).Items[5];

                GooglePlusPostStatistics googlePlusPostStatistics = new GooglePlusPostStatistics();

                post.id = "z13zc1szfwraurtxv04ci1xwjozrfjeymx40k";

                googlePlusPostStatistics.ActivityInfo =  GoogleAPI.GetGooglePlusActivityByActivityID(post.id, "AIzaSyD6EAaOZy0_JJ8sDw_DqfeAU4J6-q69-28");

                googlePlusPostStatistics.Comments = GoogleAPI.GetGooglePlusCommentsListForAnActivity(post.id, "AIzaSyCGQzZRurRxCD_6YnSWRn4SCwgHfRSFatE");

                googlePlusPostStatistics.Plusoners = GoogleAPI.GetGooglePlusPlusonersListForAnActivity(post.id, "AIzaSyCGQzZRurRxCD_6YnSWRn4SCwgHfRSFatE");

                googlePlusPostStatistics.Resharers = GoogleAPI.GetGooglePlusResharersListForAnActivity(post.id, "AIzaSyCGQzZRurRxCD_6YnSWRn4SCwgHfRSFatE");

                ProviderFactory.Social.SaveGooglePlusPostStatistics(googlePlusPostStatistics, 5069);
            }
            

            //GoogleAPI.GetGooglePlusCommentsListForAnActivity("z131exmpividungdw23iurzw4zyqwh55y04", "AIzaSyCGQzZRurRxCD_6YnSWRn4SCwgHfRSFatE");

            //GooglePlusGetActivityListByPageResponse rawResult = GoogleAPI.GetGooglePlusActivitiesListForAPage("100876660965460969939", "AIzaSyCGQzZRurRxCD_6YnSWRn4SCwgHfRSFatE");

            //foreach (var result in rawResult.items)
            //{
            //    //GooglePlusAddorUpdateorGetSpecificActivityResponse obj = GoogleAPI.GetGooglePlusActivityByActivityID("z12gzzdxwuz1s1rtm22oeddgpxq3wvso204", "AIzaSyCGQzZRurRxCD_6YnSWRn4SCwgHfRSFatE");
            //    GooglePlusGetCommentsListResponse obj = GoogleAPI.GetGooglePlusCommentsListForAnActivity("z12gzzdxwuz1s1rtm22oeddgpxq3wvso204", "AIzaSyCGQzZRurRxCD_6YnSWRn4SCwgHfRSFatE");
            //}

            //GoogleAPI.ValidateToken("1/ir3jjyLGhepxVxInq4GxXpKtlbDnHmm7kTbGMXcdbF4", "257710766138-qgidtjd5kg42bfovjrequ81c86f0e4tn.apps.googleusercontent.com", "W_6KlDWmXBm_j8pI1JPFbmn3");

            //GoogleAPI.ValidateToken("257710766138-qgidtjd5kg42bfovjrequ81c86f0e4tn.apps.googleusercontent.com", "W_6KlDWmXBm_j8pI1JPFbmn3", "1/ir3jjyLGhepxVxInq4GxXpKtlbDnHmm7kTbGMXcdbF4");
            

            /*
            #region Publish

            SocialCredentialDTO socialCredentialsDTO = null;
            GooglePlusDownloaderData data = new GooglePlusDownloaderData();

            data.CredentialID = 13;
            data.ReferencePostTargetID = null;
            data.PostID = 15;
            data.PostTargetID = 12;
            data.PostImageID = null;
            data.GooglePlusEnum = SI.DTO.GooglePlusProcessorActionEnum.GooglePlusPublish;

            //PostTargetPublishInfoDTO info = ProviderFactory.Social.GetPostTargetPublishInfo(data.PostTargetID, data.PostImageID);
            //socialCredentialsDTO = ProviderFactory.Social.GetSocialCredentialInfo(info.CredentialID);

            PostTargetPublishInfoDTO info = new PostTargetPublishInfoDTO();

            #region GET ACCESS TOKEN

            //Call Connection Class to get data from SDK   
            SIGooglePlusPageParameters pageParameters = new SIGooglePlusPageParameters();

            pageParameters.UniqueID = "116077733098916251493";
            pageParameters.client_id = "257710766138.apps.googleusercontent.com";
            pageParameters.client_secret = "xDKxcfzIGaKTxVX1uFKHJh6d";
            pageParameters.refresh_token = "1/GvdGSO6TGwsBUHjJo5PY2FMSiqJTmwCzUE8pbvZ_mHs";
            pageParameters.grant_type = "refresh_token";


            // Get Access Token                                
            string access_token = string.Empty;

            SIGooglePlusPageConnectionParameters ConnectionParameters = new SIGooglePlusPageConnectionParameters(pageParameters);
            GooglePlusPageConnection pageconnection = new GooglePlusPageConnection(ConnectionParameters);

            GooglePlusAccessTokenRequestResponse objGooglePlusAccessTokenRequestResponse = pageconnection.GetGooglePlusAccessToken();

            access_token = objGooglePlusAccessTokenRequestResponse.access_token;

            #endregion

            //TODO: Call BLL to Get Post from Post Target Table or Read from Jobs Table
            SIGooglePlusPublishParameters publishParameters = new SIGooglePlusPublishParameters();

            publishParameters.UniqueID = "116077733098916251493";
            publishParameters.accessToken = access_token;
            publishParameters.accessType = "public";
            publishParameters.objectType = string.Empty;
            publishParameters.activityType = string.Empty;
            publishParameters.Content = "Check out our service specials online! Hawaii's best Nissan services to keep your car in tip top shape.";
            publishParameters.attachmentID = string.Empty;
            publishParameters.circleID = string.Empty;
            publishParameters.userID = string.Empty;
            publishParameters.postType = PostTypeEnum.Link;
            publishParameters.pathToImage = string.Empty;
            publishParameters.postURL = "http://www.newcitynissan.com/page/service-coupons/master/car-service/Honolulu-HI-Nissan-Dealer";

            SIGooglePlusPublishConnectionParameters publishConnectionParameters = new SIGooglePlusPublishConnectionParameters(publishParameters);
            GooglePlusPublishConnection publishconnection = new GooglePlusPublishConnection(publishConnectionParameters);

            info.PostType = PostTypeEnum.Link;
            
            switch (info.PostType)
            {
                case PostTypeEnum.Status:
                    {
                        #region Status

                        //ProviderFactory.Social.SetPostTargetJobID(data.PostTargetID, job.ID.Value);
                        GooglePlusPostResponse objGooglePlusPostResponse = publishconnection.CreateGooglePlusActivity();

                        
                        #endregion
                    }
                    break;
                case PostTypeEnum.Link:
                    {
                        #region Link

                        //ProviderFactory.Social.SetPostTargetJobID(data.PostTargetID, job.ID.Value);
                        GooglePlusPostResponse objGooglePlusPostResponse = publishconnection.CreateGooglePlusActivity();

                       
                        #endregion
                    }
                    break;
                case PostTypeEnum.Photo:
                    {
                        #region Photo

                        //ProviderFactory.Social.SetPostImageJobID(data.PostImageID.Value, data.PostTargetID, job.ID.Value);
                        GooglePlusPostResponse objGooglePlusPostResponse = publishconnection.PostGooglePlusPhoto();

                        #endregion
                    }
                    break;
            }
            #endregion

            */
        }
    }
}
