﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Facebook;
using Facebook.Entities;
using SI.DTO;
using SI.BLL;
using Connectors.Parameters;
using Connectors;
using Newtonsoft.Json;

namespace SDKTest
{
    class FacebookAPITest
    {
        static void Main(string[] args)
        {
            var testNumber = 1;

            switch (testNumber)
            {
                case 1:
                    {
                        #region PostStatistics
                        //FacebookPostStatistics objFacebookPostStatistics1 =  FacebookAPI.GetFacebookPostStatistics(  "10151942012311669", 
                        //                                                                                            "AAAAANqD36fQBAP2jyPCXBCzGrI5BvZBt8hYltgyZBltHL2t6D8iSVqNFMmFCXJmqd6zV9tTDfcya3aS5ZCCkexW1L17gFGhZAqbZAyDmep6dyZCv8M1A5V",
                        //                                                                                             PostTypeEnum.Photo,
                        //                                                                                            false);
                        #endregion

                        #region POST STATISTICS (Basic/Insights)

                        long AccountID = 220931;
                        string ResultID = "65978488882_10152356368888883";
                        int PageNumber = 1;
                        int PageSize = 500;
                        string FeedID = "65978488882_10152356368888883";

                        //TimeZoneInfo timeZone = ProviderFactory.TimeZones.GetTimezoneForUserID(userInfo.EffectiveUser.ID.GetValueOrDefault());
                        string PostDateTime_TimeZone = "CST";

                        SocialCredentialDTO socialCredentialsDTO = ProviderFactory.Social.GetSocialCredentials(SocialNetworkEnum.Facebook, AccountID, true).SingleOrDefault();

                        if (socialCredentialsDTO != null)
                        {
                            SIFacebookPostStatisticsParameters postParameters = new SIFacebookPostStatisticsParameters();

                            postParameters.Token = socialCredentialsDTO.Token;
                            postParameters.UniqueID = socialCredentialsDTO.UniqueID;
                            postParameters.Posttype = PostTypeEnum.Link;

                            SIFacebookPostStatisticsConnectionParameters postConnectionParameters = new SIFacebookPostStatisticsConnectionParameters(postParameters);
                            FacebookPostStatisticConnection postconnection = new FacebookPostStatisticConnection(postConnectionParameters);

                            #region Post Basic
                            if (!string.IsNullOrEmpty(ResultID))
                            {
                                postParameters.ResultID = ResultID;

                                FacebookPostStatistics objFacebookPostStatistics = postconnection.GetFacebookPostStatistics();

                                //Save to dynoDb if objFacebookPostStatistics.facebookResponse.message = string.empty
                                if (string.IsNullOrWhiteSpace(objFacebookPostStatistics.facebookResponse.message))
                                {
                                    int commentscount = objFacebookPostStatistics.postStatistics.commentscount;
                                    int likescount = objFacebookPostStatistics.postStatistics.likescount;
                                    int sharescount = objFacebookPostStatistics.postStatistics.sharescount;

                                    int NumberofRecord = 1;
                                    if (PageNumber == 0)
                                    {
                                        NumberofRecord = PageSize;
                                    }
                                    else
                                    {
                                        NumberofRecord = (PageNumber * PageSize) + 3;
                                    }

                                    if (objFacebookPostStatistics.postStatistics.comments.data != null)
                                    {
                                        var comments = objFacebookPostStatistics.postStatistics.comments.data.Take(NumberofRecord);
                                        foreach (var item in comments)
                                        {
                                            
                                        }
                                    }
                                    //List Comments (Get top 3 Commnets)
                                    //objFacebookPostStatistics.postStatistics.comments
                                }
                            }

                            #endregion

                            #region Post Insights

                            int NumberofpeoplewhosawyourPagepost = 0;

                            if (!string.IsNullOrEmpty(FeedID))
                            {
                                postParameters.FeedID = FeedID;

                                FacebookInsights objFacebookInsights = postconnection.GetFacebookPostInsights();

                                //Save to dynoDb if objFacebookPostStatistics.facebookResponse.message = string.empty
                                if (string.IsNullOrWhiteSpace(objFacebookInsights.facebookResponse.message))
                                {
                                    var Insights = objFacebookInsights.insights.data.Where(x => x.name == "post_impressions_unique" && x.period == "lifetime").SingleOrDefault();

                                    foreach (Value value in Insights.values)
                                    {
                                        bool hasValue = false;
                                        int intValue = 0;
                                        if (value._value.StartsWith("{"))
                                        {
                                            Dictionary<string, object> resultDictionary = JsonConvert.DeserializeObject<Dictionary<string, object>>(value._value);
                                            if (resultDictionary != null && resultDictionary.Count > 0)
                                            {
                                                foreach (KeyValuePair<string, object> keyV in resultDictionary)
                                                {
                                                    int tempValue;
                                                    if (keyV.Key != "total")
                                                    {
                                                        if (int.TryParse(keyV.Value.ToString(), out tempValue))
                                                        {
                                                            intValue += tempValue;
                                                            hasValue = true;
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                        else if (!value._value.StartsWith("["))
                                        {
                                            int tempValue = 0;
                                            if (int.TryParse(value._value, out tempValue))
                                            {
                                                intValue = tempValue;
                                                hasValue = true;
                                            }
                                        }
                                        if (hasValue)
                                        {
                                            NumberofpeoplewhosawyourPagepost = intValue;

                                        }
                                        else
                                        {
                                            NumberofpeoplewhosawyourPagepost = 0;
                                        }

                                    }
                                }
                            }
                            #endregion
                        }
                        #endregion
                    }
                    break;
               case 2:
                    {
                        #region PostStatisticsByFeedId
                        FacebookPostStatisticsFeedID objFacebookPostStatisticsFeedID = FacebookAPI.GetFacebookPostStatisticsByFeedId("255547016773_10151854771161774", 
                                                                                                                                        "AAAAANqD36fQBAAFAqE1dFdLQG7NSBhtfvQkU5ZBwqabJDbfTGTYdKjw0WwT4h6fODoWoEtbbajypmqO21Akeund5PWzxh0Qvxwi6z6UHJ4ZA53bYxe",                                                                                                                                        
                                                                                                                                        true);
                        #endregion
                    }
                    break;
               case 3:
                    {
                        #region CreateWallPost

                        FacebookResponse ObjFacebookResponse = FacebookAPI.CreateFacebookWallPost("571476339562480",
                                                                                                   "CAAGfD25ViIcBAM0lj3PDoDDPWNrgZA6GKMrhJqiyfUZBSmJqQbdDdUByyKvpT4Jp7R91veJmS7v6J3METZAe7dZBzYrPn0Ca4cPpSfNScfyiiENjasNKYuZCTi2vbcW1vH7cDD7CZAjZB6GZARC0WMknwOagehnfjLIM5b2HuKqczCvVZCC1amUyt",
                                                                                                   "My Message for Link post, with Custom Image", //Message
                                                                                                   DateTime.UtcNow.ToString(), //caption
                                                                                                   null, //Description
                                                                                                   "My Message for Link post, with Custom Image", //Name / Title
                                                                                                   "http://dealers.socialdealer.com/Public/PublishImages/78398850-d909-4a5c-9345-ae9e7554cd78.jpg", //picture
                                                                                                   "http://www.castlechevycars.com/", //Link
                                                                                                   string.Empty, //Source
                                                                                                   "{'countries':''}");//Targeting

                        //FacebookResponse ObjFacebookResponse1 = FacebookAPI.CreateFacebookWallPost("120358044649547",
                        //                                                                           "CAAGfD25ViIcBAG8zWSMZCFHJAmViAKdCZC8sW1ZCsdb8GhDZBg3KFfJCd1v22ZB4IqftaOCvqpRxLKbaTGxn3JrPwrfypCpH5OPZCxU0PAnBMS4wXWZAhT4o6EUsH3ii6QGIAlZBQWxGbd0bBH159NKCfBn4dY7srilZBJR486gWlWDKJRZCY9aKx2",
                        //                                                                           "Test", //Message
                        //                                                                           DateTime.UtcNow.ToString(), //caption
                        //                                                                           null, //Description
                        //                                                                           "Test", //Name / Title
                        //                                                                           null, //picture
                        //                                                                           null, //Link
                        //                                                                           string.Empty, //Source
                        //                                                                           "{'countries':''}");//Targeting

                        #endregion
                    }
                    break;
               case 4:
                    {
                        #region UploadPhoto
                        FacebookResponse ObjFacebookResponse = FacebookAPI.UploadFacebookPhoto("CAAGfD25ViIcBAMWLoyvsqX6Yl3AdVgJaTDyPBQuAa8OiZA876lrSUqVVfmTF90CBTlJQNZBu16fd3YajQvOZBAyo5TUFWcP3ZC34oKKlq0xZABriZAoDGXE8V2GabXuGd0A6m7aTsQJxM7bZApxRspzZAnUTwTYHKk3ikXpaLZBZCIbIz1bTZBe5iqZCW6rowLl6u5IZD",
                                                                                                "I love to eat Chipotle!",
                                                                                                "http://socialintegration.com/UploadedPostImages/a02465dd-ab30-4ff5-9c13-c36655a346d8.jpg",
                                                                                                ""); // Album ID
                        #endregion
                    }
                    break;
               case 5:
                    {
                        #region GetAlbums
                        FacebookAlbum ObjFacebookAlbum = FacebookAPI.GetFacebookAlbums("571476339562480",
                                                                                        "CAAELjVztUOgBAKGR4IKn0RDl4S3NLAArGEMZA7XZAJ9dRBMOmQoKG3lg0LW0RltR8B1zIuBxoyUWvEkIgpRaEOTn0X8ZAqDtwyMZCyrJiXgqmQaJUZBTdP4pDqyZAVXYSSFOSC6RJQnmG8LJfoTfzZAUmIcoSMjOXZBXxubmzvk7czMvSzy5upHZB");
                                                                                                
                        #endregion
                    }
                    break;
               case 6:
                    {
                        #region CreateAlbums
                        FacebookResponse ObjFacebookResponse = FacebookAPI.CreateFacebookAlbum("571476339562480",
                                                                                                "CAAELjVztUOgBAKGR4IKn0RDl4S3NLAArGEMZA7XZAJ9dRBMOmQoKG3lg0LW0RltR8B1zIuBxoyUWvEkIgpRaEOTn0X8ZAqDtwyMZCyrJiXgqmQaJUZBTdP4pDqyZAVXYSSFOSC6RJQnmG8LJfoTfzZAUmIcoSMjOXZBXxubmzvk7czMvSzy5upHZB",
                                                                                                "SDK Album",
                                                                                                "This album is creatd from SocialDealer SDK");

                        #endregion
                    }
                    break;
               case 7:
                    {
                        #region GetEvents
                        FacebookEvent objFacebookEvent = FacebookAPI.GetFacebookEvent("571476339562480",
                                                                                       "CAAELjVztUOgBAKGR4IKn0RDl4S3NLAArGEMZA7XZAJ9dRBMOmQoKG3lg0LW0RltR8B1zIuBxoyUWvEkIgpRaEOTn0X8ZAqDtwyMZCyrJiXgqmQaJUZBTdP4pDqyZAVXYSSFOSC6RJQnmG8LJfoTfzZAUmIcoSMjOXZBXxubmzvk7czMvSzy5upHZB");

                        #endregion
                    }
                    break;
               case 8:
                    {
                        #region CreateEvent

                        FacebookResponse ObjFacebookResponse = FacebookAPI.CreateFacebookEvent("571476339562480",
                                                                                                "CAAELjVztUOgBAKGR4IKn0RDl4S3NLAArGEMZA7XZAJ9dRBMOmQoKG3lg0LW0RltR8B1zIuBxoyUWvEkIgpRaEOTn0X8ZAqDtwyMZCyrJiXgqmQaJUZBTdP4pDqyZAVXYSSFOSC6RJQnmG8LJfoTfzZAUmIcoSMjOXZBXxubmzvk7czMvSzy5upHZB",
                                                                                                "10th Event from SDK",
                                                                                                Convert.ToString(DateTime.Now.AddHours(2)),
                                                                                                Convert.ToString(DateTime.Now.AddHours(4)),
                                                                                                "Oak Brook City",
                                                                                                "1 Friday SDK Event");

                        #endregion
                    }
                    break;
               case 9:
                    {
                        #region EventStatistics

                        FacebookEventStatistics objFacebookEventStatistics = FacebookAPI.GetFacebookEventStatistics("216148455218202",
                                                                                                                    "CAAELjVztUOgBAKGR4IKn0RDl4S3NLAArGEMZA7XZAJ9dRBMOmQoKG3lg0LW0RltR8B1zIuBxoyUWvEkIgpRaEOTn0X8ZAqDtwyMZCyrJiXgqmQaJUZBTdP4pDqyZAVXYSSFOSC6RJQnmG8LJfoTfzZAUmIcoSMjOXZBXxubmzvk7czMvSzy5upHZB");

                        #endregion
                    }
                    break;
               case 10:
                    {
                        #region Insights

                        FacebookInsights objFacebookInsights = FacebookAPI.GetFacebookPageInsights("255547016773",
                                                                                               "AAAB6FRG2kxcBAOxf22mU2wgbXsOaO4g89QCKHeXbR8umMjBwrZA8BMU3ZBcEyXx7Bv8o7tl5a3orLwfbWjUiTmkWsNAziQbwZCCkpwuhTHz58cTMvbY",
                                                                                               Convert.ToString(DateTime.Now.AddDays(-2)),
                                                                                               Convert.ToString(DateTime.Now),
                                                                                               "days_28,lifetime");

                        #endregion
                    }
                    break;
               case 11:
                    {
                        #region PageInformation

                        FacebookPage objFacebookPage = FacebookAPI.GetFacebookPageInformation("255547016773",
                                                                                               "AAAB6FRG2kxcBAOxf22mU2wgbXsOaO4g89QCKHeXbR8umMjBwrZA8BMU3ZBcEyXx7Bv8o7tl5a3orLwfbWjUiTmkWsNAziQbwZCCkpwuhTHz58cTMvbY");

                        #endregion

                        FacebookFeed objFacebookFeed = FacebookAPI.GetFacebookFeed("255547016773", "CAAGfD25ViIcBAGvALQINSVnHOp5TkuEhEGxeTLsueyTOpSBZCJb6xS4ECmpYQDRqT8NcIt2iTw0oHDgMH1x4vwHRBkFnmezcf2bPH1yt66b0zCoi2FRD3CWwLNgwTvEGhorr7Vi2ZBas7uAx9n0ajZAWMZAfVxcZBMO83dmNljOZBcf83qlgri", "", "10");
                        int startindex = objFacebookFeed.feed.paging.next.IndexOf("until=");
                        if (startindex > 0)
                        {
                            string Until = objFacebookFeed.feed.paging.next.Substring(startindex + 6);
                        }
                    }
                    break;
               case 12:
                    {
                        #region PermissionsList

                        List<string> listPermissions = FacebookAPI.GetFacebookPermissions("AAAB6FRG2kxcBAOxf22mU2wgbXsOaO4g89QCKHeXbR8umMjBwrZA8BMU3ZBcEyXx7Bv8o7tl5a3orLwfbWjUiTmkWsNAziQbwZCCkpwuhTHz58cTMvbY");

                        #endregion
                    }
                    break;
               case 13:
                    {
                        #region ObjectIDbyResultID

                        FacebookObjectIDByResultID objFacebookObjectIDByResultID = FacebookAPI.GetFacebookObjectIDbyFeedID("255547016773_10151854771161774", 
                                                                                                                                "AAAB6FRG2kxcBAOxf22mU2wgbXsOaO4g89QCKHeXbR8umMjBwrZA8BMU3ZBcEyXx7Bv8o7tl5a3orLwfbWjUiTmkWsNAziQbwZCCkpwuhTHz58cTMvbY");

                        #endregion
                    }
                    break;
               case 14:
                    {
                        #region GetAdStatistics

                        var result14 = FacebookAPI.GetAdsAccountInfo("act_124232571038675", "CAAB945ZCZALqgBAN7Ja0lSIDa8Uoe2XaDa5lllZBIKurGs637Sr4lxQ9c4dcLfZCdLCV2u7tZCokPHPysrcx9CJ9p8QKx6izsZBzrik8qN5BgVeEcjmtwQRfZCZCFiG6ShEURcts3nI0YeOFbgnR9NTxWUhf6F9ZC1QQVq0SX95JG2NsiqJHX4gzAX9NWj7ZATLh491rmtPQYchgZDZD");

                        #endregion
                    }
                    break;
               case 15:
                    {
                        #region ValidateToken

                        bool isValid = FacebookAPI.ValidateToken("AAAAANqD36fQBAAFAqE1dFdLQG7NSBhtfvQkU5ZBwqabJDbfTGTYdKjw0WwT4h6fODoWoEtbbajypmqO21Akeund5PWzxh0Qvxwi6z6UHJ4ZA53bYxe", "BAAAANqD36fQBALboY6kBwZCMh9iAH8zJ7Lts0vG3dxn8gBNCi2ICDRsznZCMVfcgXVMxkq07h3muFSsEZC6utBLTYsPfFbwZCyVhTgEJqHe0J4rWRnxU0qyM38ha2qLOTxEzWA649cHlsUZCLx21k9acpwG0pdjaTXenHW4ZBZCWogOzdtnEZBjelXpCnxCMI2uh8BvWqxalmXiihS4clcvL8vUESmLs1cfzfpHVEZCTsTwZDZD");

                        #endregion
                    }
                    break;
               case 16:
                    {
                        #region Reviews

                        //FacebookReviewResponse facebookReviewResponse = FacebookAPI.GetReviews("255547016773", "CAAGfD25ViIcBAC1sjcdMextc9jh1uQ85ZAc2bQyvqaxaKWgORhZCxH8GI62unPC9MYeZA9cqZBmyHJJUMGzVaxfo3W8bGrbg5JNHTaIPDmR21G899vMT6dNuW9JekZB1bkIBdVZBAvKRwcthsdxcZC9ZAtMXHOHlI6qGKo5y6j1M87EDXVkbRrFp", true);
                        FacebookReviewResponse facebookReviewResponse = FacebookAPI.GetReviews("255547016773", "CAAGfD25ViIcBAC1sjcdMextc9jh1uQ85ZAc2bQyvqaxaKWgORhZCxH8GI62unPC9MYeZA9cqZBmyHJJUMGzVaxfo3W8bGrbg5JNHTaIPDmR21G899vMT6dNuW9JekZB1bkIBdVZBAvKRwcthsdxcZC9ZAtMXHOHlI6qGKo5y6j1M87EDXVkbRrFp", true);
                        if (facebookReviewResponse != null)
                        {
                            if (facebookReviewResponse.ReviewList.data.Count > 0)
                            {
                                double Avg = Math.Round(Convert.ToDouble(facebookReviewResponse.ReviewList.data.Where(x => x.has_rating == true).Average(r => r.rating)), 1);
                                Console.WriteLine("castlechevrolet");
                                Console.WriteLine("Reviews:" + facebookReviewResponse.ReviewList.data.Count);
                                Console.WriteLine("Rating:" + Avg);
                            }
                        }

                        Console.WriteLine("");
                        facebookReviewResponse = FacebookAPI.GetReviews("175419496588", "CAAGfD25ViIcBABVMJTaAtMXcNjwxtzU6aEDQzC3kGTAUZAdu6jfvK2ZAqHy6ZCKcNQusMlUQa2vXLU6CD7h6WPE7FbfZC5wv03qXlF6DQOQ87UdUKaGPHHRWRx13JHo2noCY3xwK31EZCzlZC1akGCugZBhHYYyXPbe6NciVFGLXhNfd3VakGZCj5Jms4IjNxnEZD", true);
                        if (facebookReviewResponse != null)
                        {
                            if (facebookReviewResponse.ReviewList.data.Count > 0)
                            {
                                double Avg = Math.Round(Convert.ToDouble(facebookReviewResponse.ReviewList.data.Where(x => x.has_rating == true).Average(r => r.rating)),1);
                                Console.WriteLine("UniversalToyota");
                                Console.WriteLine("Reviews:" + facebookReviewResponse.ReviewList.data.Count);
                                Console.WriteLine("Rating:" + Avg);
                            }
                        }

                        Console.WriteLine("");
                        facebookReviewResponse = FacebookAPI.GetReviews("140583377130", "CAAGfD25ViIcBAL1N1VR58CZCs2sxF1Or6GylczDSDizy2eZCz6i2yCpUftSSsytsQP6dUD1vLdPRkYpYclikLZCAIkIT9nIUBTcy5aePiQnuL0KmW1I1n7cuY55DZArCImV8gX4MXep2U3USE6LZCeFSRPmpC7MIniObr6OTRXmPKoGPZCufYn", true);
                        if (facebookReviewResponse != null)
                        {
                            if (facebookReviewResponse.ReviewList.data.Count > 0)
                            {
                                double Avg = Math.Round(Convert.ToDouble(facebookReviewResponse.ReviewList.data.Where(x => x.has_rating == true).Average(r => r.rating)), 1);
                                Console.WriteLine("oakridgeford");
                                Console.WriteLine("Reviews:" + facebookReviewResponse.ReviewList.data.Count);
                                Console.WriteLine("Rating:" + Avg);
                            }
                        }


                        Console.WriteLine();
                        facebookReviewResponse = FacebookAPI.GetReviews("400714916714517", "CAAGfD25ViIcBACovzaZAgI4fxEeZCBY10WtVmZBzNymIdaeZAMxTxlyWuFaZATOZCe4eowXCZBZCQiIZCiW2PK0ZCooYxX0kGhxOHQQc8ZA2VplNE6Egd2qmuSOucdHHOWJ8l1PbRZBFXiwVba1gxK8dVuMegJNlri3Xh1XyjNUIVIadp6d9DByksBAIlvORygpZCND0ZD", true);
                        if (facebookReviewResponse != null)
                        {
                            if (facebookReviewResponse.ReviewList.data.Count > 0)
                            {
                                double Avg = Math.Round(Convert.ToDouble(facebookReviewResponse.ReviewList.data.Where(x => x.has_rating == true).Average(r => r.rating)), 1);
                                Console.WriteLine("QuickServiceNewportNews");
                                Console.WriteLine("Reviews:" + facebookReviewResponse.ReviewList.data.Count);
                                Console.WriteLine("Rating:" + Avg);
                            }
                        }
                        

                        int a = 10;
                        Console.ReadLine();
                        #endregion
                    }
                    break;
                    
            }
        }
    }
}
