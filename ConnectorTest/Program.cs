﻿using System;
using System.Collections.Generic;
using Connectors;
using Connectors.Content.Parameters;
using Connectors.Entities;
using Connectors.Lookups;
using Connectors.Lookups.Parameters;
using Connectors.Parameters;
using Connectors.Social;
using Connectors.Social.Parameters;
using Connectors.URLHarvester;
using Connectors.URLHarvester.Parameters;
using Connectors.URL_Validators;
using Connectors.URL_Validators.Parameters;
using SI.BLL;
using SI.DTO;
using System.ComponentModel;
using ReviewUrlSearch;
using ReviewUrlSearch.Models;
using ReviewUrlSearch.Parameters;
using System.Diagnostics;
using System.Data;
using SDTest;


namespace ConnectorTest
{
    class Program
    {
        static void Main(string[] args)
        {
            var testNumber = 0;
            int subtest = 0;
            var strURL = "";
            ReviewDownloadResult testresult = new ReviewDownloadResult();
            ReviewURLValidationResult testvalidator = new ReviewURLValidationResult();
            var url = "";

            var testUrlList = new List<string>();

            int[] numbers = {6};
            
            //5, 6, 7, 8, 9, 10, 11
            foreach (int element in numbers)
            {
                testNumber = element;


                #region Review Downloaders

                #region Google#1

                if (testNumber == 1)
                {
                    //string myXMLfile = @"C:\Dev2\SI\SI_PLATFORM\ConnectorTest\URLs.xml";
                    //DataSet ds = new DataSet();
                    
                    //ds.ReadXml(myXMLfile);
                   

                    //int iCounter = 1;
                    SIReviewConnection connection = ProviderFactory.Reviews.GetReviewConnection(SI.DTO.ReviewSourceEnum.Google);

                    //SDReviewEntities dbContext = new SDReviewEntities();

                    //foreach (DataRow row in ds.Tables[0].Rows)
                    //{
                    //    ReviewUrlDTO dto = new ReviewUrlDTO();

                    //    dto.HtmlURL = row[2].ToString();
                    //    dto.AccountID = Convert.ToInt64(row[0].ToString());
                    //    dto.ExternalID = row[3].ToString();
                        
                    //    int DealerLocationID  = Convert.ToInt32(row[1].ToString());

                    //    var googleResult = connection.Scrape(dto, true);
                    //    Debug.WriteLine(iCounter + ". {0} Rating, {1} ReviewCount , {2} Details, {3} URL", googleResult.SalesAverageRating, googleResult.SalesReviewCount, googleResult.ReviewDetails.Count,dto.HtmlURL);
                    //    Console.WriteLine(iCounter + ". {0} Rating, {1} ReviewCount , {2} Details, {3} URL", googleResult.SalesAverageRating, googleResult.SalesReviewCount, googleResult.ReviewDetails.Count, dto.HtmlURL);

                    //    dbContext.spReviewSummarySDSIUpdate("Google", Convert.ToInt32(DealerLocationID),Convert.ToInt32(dto.AccountID), dto.HtmlURL, Convert.ToInt32(googleResult.SalesReviewCount),Convert.ToDouble(googleResult.SalesAverageRating), googleResult.ReviewDetails.Count);

                    //    iCounter++;
                    //}

                   
                    //testUrlList.Add("https://plus.google.com/102224938406592734351/about");
                    //testUrlList.Add("https://plus.google.com/100380266944175007365/about");
                    //testUrlList.Add("https://plus.google.com/100006547192040575107/about");
                    //testUrlList.Add("https://plus.google.com/100661285222889758067/about");
                    //testUrlList.Add("https://plus.google.com/101884564461186191693/about");
                    //testUrlList.Add("https://plus.google.com/107578514302166386006/about?rfmt=s&hl=en&gl=us");
                    //testUrlList.Add("https://plus.google.com/117740471640576391970/about");

                    testUrlList.Add("https://plus.google.com/114284807299404883225/about");
                    foreach (var item in testUrlList)
                    {
                        ReviewUrlDTO dto = new ReviewUrlDTO();
                        dto.HtmlURL = item;
                        dto.ExternalID = "7991780716084416063";

                        var googleResult = connection.Scrape(dto, true);
                        Debug.WriteLine("{0} Rating, {1} ReviewCount , {2} Details, {3} URL", googleResult.SalesAverageRating, googleResult.SalesReviewCount, googleResult.ReviewDetails.Count, item);
                    }


                    //dto.ExternalID = "13076967133698945353";

                    Debug.WriteLine("");
                    //strURL = dto.HtmlURL;


                }

                #endregion

                #region DealerRater#2
                
                if (testNumber == 2)
                {
                    SIReviewConnection connection = ProviderFactory.Reviews.GetReviewConnection(SI.DTO.ReviewSourceEnum.DealerRater);
                    string myXMLfile = @"C:\Dev2\SI\SI_PLATFORM\ConnectorTest\URLs.xml";
                    DataSet ds = new DataSet();

                    ds.ReadXml(myXMLfile);


                    int iCounter = 1;
                    SDReviewEntities dbContext = new SDReviewEntities();

                    foreach (DataRow row in ds.Tables[0].Rows)
                    {
                        ReviewUrlDTO dto = new ReviewUrlDTO();

                        dto.HtmlURL = row[2].ToString();
                        dto.AccountID = Convert.ToInt64(row[0].ToString());
                        dto.ExternalID = row[3].ToString();

                        int DealerLocationID = Convert.ToInt32(row[1].ToString());

                        var googleResult = connection.Scrape(dto, true);
                        Debug.WriteLine(iCounter + ". {0} Rating, {1} ReviewCount , {2} Details, {3} URL", googleResult.SalesAverageRating, googleResult.SalesReviewCount, googleResult.ReviewDetails.Count, dto.HtmlURL);
                        Console.WriteLine(iCounter + ". {0} Rating, {1} ReviewCount , {2} Details, {3} URL", googleResult.SalesAverageRating, googleResult.SalesReviewCount, googleResult.ReviewDetails.Count, dto.HtmlURL);

                        dbContext.spReviewSummarySDSIUpdate("Dealer Rater", Convert.ToInt32(DealerLocationID), Convert.ToInt32(dto.AccountID), dto.HtmlURL, Convert.ToInt32(googleResult.SalesReviewCount), Convert.ToDouble(googleResult.SalesAverageRating), googleResult.ReviewDetails.Count);

                        iCounter++;
                    }


                    //ReviewUrlDTO dto = new ReviewUrlDTO();
                    //dto.HtmlURL = "http://www.dealerrater.com/dealer/Massey-Cadillac-Saab-of-Orlando-North-review-15351/";
                    //                  //dto.ExternalID = "15351";

                    //                http://www.dealerrater.com/dealer/BMW-of-Manhattan-review-155/
                    //              http://www.dealerrater.com/dealer/MINI-of-Manhattan-review-16129/
                    //              http://www.dealerrater.com/dealer/Castle-Buick-GMC-of-North-Riverside-review-2887/
                    //              http://www.dealerrater.com/dealer/Castle-Chevrolet-review-19746/
                    //              http://www.dealerrater.com/dealer/De-La-Fuente-Cadillac-review-4123/
                    //              http://www.dealerrater.com/dealer/Frederick-Nissan-review-18014/
                    //              http://www.dealerrater.com/dealer/Frederick-Nissan-review-18014/
                    //              http://www.dealerrater.com/dealer/Mike-Haggerty-Volkswagen-review-17958/
                    //              http://www.dealerrater.com/dealer/Happy-Hyundai-review-18329/

                    //              http://www.dealerrater.com/dealer/Advantage-Chevrolet-of-Bolingbrook-review-2182/ - Certified With Reviews & Rating - has comments
                    //              http://www.dealerrater.com/dealer/Bill-Kay-Buick-GMC-review-1840/ - Uncertified Dealer
                    //              http://www.dealerrater.com/dealer/Ray-Buick-review-36171/ - No Reviews & No Rating
                    // url = "http://www.dealerrater.ca/dealer/Applewood-Langley-Kia-review-31597/"; //Canadian Site


                    //testUrlList.Add("http://www.dealerrater.com/classifieds/d/Arizona/GMC/Acadia/");
                    //testUrlList.Add("http://www.dealerrater.com/dealer/Renick-Subaru-review-39596/");
                    //testUrlList.Add("http://www.dealerrater.com/dealer/Nate-Wade-Subaru-review-9880/");
                    //testUrlList.Add("http://www.dealerrater.com/dealer/Kearny-Mesa-Subaru-review-43299/");
                    //testUrlList.Add("http://www.dealerrater.com/dealer/Carter-Volkswagen-Saab-review-2871/");
                    //testUrlList.Add("http://www.dealerrater.com/dealer/Carter-Subaru-Ballard-review-31433/");
                    testUrlList.Add("http://www.dealerrater.com/dealer/Carter-Subaru-Shoreline-review-16573/");

                    //testUrlList.Add("http://www.dealerrater.com/dealer/MINI-of-Manhattan-review-16129/");
                    //testUrlList.Add("http://www.dealerrater.com/dealer/Castle-Buick-GMC-of-North-Riverside-review-2887/");
                    //testUrlList.Add("http://www.dealerrater.com/dealer/Castle-Chevrolet-review-19746/");
                    //testUrlList.Add("http://www.dealerrater.com/dealer/De-La-Fuente-Cadillac-review-4123/");
                    //testUrlList.Add("http://www.dealerrater.com/dealer/Frederick-Nissan-review-18014/");
                    //testUrlList.Add("http://www.dealerrater.com/dealer/Frederick-Nissan-review-18014/");
                    //testUrlList.Add("http://www.dealerrater.com/dealer/Mike-Haggerty-Volkswagen-review-17958/");
                    //testUrlList.Add("http://www.dealerrater.com/dealer/Happy-Hyundai-review-18329/");


                    foreach (var item in testUrlList)
                    {
                        var dto = new ReviewUrlDTO();
                        dto.HtmlURL = item;

                        var dealerRaterResult = connection.Scrape(dto, true);
                        Debug.WriteLine("{0} Rating, {1} ReviewCount , {2} Details, {3} Errors, {4} URL", dealerRaterResult.SalesAverageRating, dealerRaterResult.SalesReviewCount, dealerRaterResult.ReviewDetails.Count, dealerRaterResult.FailureReasons.Count, item);
                    }

                    Debug.WriteLine("");
                }

                #endregion

                #region Yahoo#3

                if (testNumber == 3)
                {
                    SIReviewConnection connection = ProviderFactory.Reviews.GetReviewConnection(SI.DTO.ReviewSourceEnum.Yahoo);

                    //string myXMLfile = @"C:\Dev2\SI\SI_PLATFORM\ConnectorTest\URLs.xml";
                    //DataSet ds = new DataSet();

                    //ds.ReadXml(myXMLfile);


                    //int iCounter = 1;
                    //SDReviewEntities dbContext = new SDReviewEntities();

                    //foreach (DataRow row in ds.Tables[0].Rows)
                    //{
                    //    ReviewUrlDTO dto = new ReviewUrlDTO();

                    //    dto.HtmlURL = row[2].ToString();
                    //    dto.AccountID = Convert.ToInt64(row[0].ToString());
                    //    dto.ExternalID = row[3].ToString();

                    //    int DealerLocationID = Convert.ToInt32(row[1].ToString());

                    //    var googleResult = connection.Scrape(dto, true);
                    //    Debug.WriteLine(iCounter + ". {0} Rating, {1} ReviewCount , {2} Details, {3} URL", googleResult.SalesAverageRating, googleResult.SalesReviewCount, googleResult.ReviewDetails.Count, dto.HtmlURL);
                    //    Console.WriteLine(iCounter + ". {0} Rating, {1} ReviewCount , {2} Details, {3} URL", googleResult.SalesAverageRating, googleResult.SalesReviewCount, googleResult.ReviewDetails.Count, dto.HtmlURL);

                    //    dbContext.spReviewSummarySDSIUpdate("Yahoo", Convert.ToInt32(DealerLocationID), Convert.ToInt32(dto.AccountID), dto.HtmlURL, Convert.ToInt32(googleResult.SalesReviewCount), Convert.ToDouble(googleResult.SalesAverageRating), googleResult.ReviewDetails.Count);

                    //    iCounter++;
                    //}

                    //SIReviewConnection connection = ProviderFactory.Reviews.GetReviewConnection(SI.DTO.ReviewSourceEnum.Yahoo);
                    //ReviewUrlDTO dto = new ReviewUrlDTO();
                    ////http://local.yahoo.com/info-41697286-advantage-chevrolet-of-bolingbrook-incorporated-bolingbrook
                    ////http://local.yahoo.com/info-48281824-elmhurst-toyota-elmhurst
                    ////http://local.yahoo.com/info-88906863-rightway-auto-sales-villa-park no reviews
                    //dto.HtmlURL = "http://local.yahoo.com/info-46213031-orange-coast-buick-gmc-cadillac-costa-mesa";
                    //strURL = dto.HtmlURL;
                    //ReviewDownloadResult result = connection.Scrape(dto, true);
                    //testresult = result;


                    //testUrlList.Add("http://local.yahoo.com/details?id=60981124&stx=Groove+Toyota&csz=Englewood+CO");
                    //testUrlList.Add("http://local.yahoo.com/info-17329148-castle-buick-gmc-riverside?csz=Chicago%2C+IL");
                    testUrlList.Add("http://local.yahoo.com/info-17253593-castle-chevrolet-villa-park?tab=reviews");
                    //testUrlList.Add("http://local.yahoo.com/info-22137490-carter-volkswagen-seattle");
                    //testUrlList.Add("http://local.yahoo.com/info-20879276-kearney-mesa-subaru-san-diego");

                    foreach (var item in testUrlList)
                    {
                        var dto = new ReviewUrlDTO();
                        dto.HtmlURL = item;
                        dto.ExternalID = "17253593";

                        var yahooResult = connection.Scrape(dto, true);
                        Debug.WriteLine("{0} Rating, {1} ReviewCount , {2} Details, {3} Errors, {4} URL", yahooResult.SalesAverageRating,
                                                                                                            yahooResult.SalesReviewCount,
                                                                                                            yahooResult.ReviewDetails.Count,
                                                                                                            yahooResult.FailureReasons.Count, item);
                    }

                    Debug.WriteLine("");
                }

                #endregion

                #region Yelp#4

                if (testNumber == 4)
                {
                    SIReviewConnection connection = ProviderFactory.Reviews.GetReviewConnection(SI.DTO.ReviewSourceEnum.Yelp);

                    ReviewUrlDTO dto1 = new ReviewUrlDTO();
                    dto1.HtmlURL = "http://www.yelp.com/biz/atlantic-autosports-and-accessories-virginia-beach";
                    //dto1.HtmlURL = "http://www.yelp.com/biz/castle-chevrolet-villa-park-2";
                    var googleResult1 = connection.Scrape(dto1, true);

                    string myXMLfile = @"C:\Dev2\SI\SI_PLATFORM\ConnectorTest\URLs.xml";
                    DataSet ds = new DataSet();

                    ds.ReadXml(myXMLfile);


                    int iCounter = 1;
                    SDReviewEntities dbContext = new SDReviewEntities();

                    foreach (DataRow row in ds.Tables[0].Rows)
                    {
                        ReviewUrlDTO dto = new ReviewUrlDTO();

                        //dto.HtmlURL = row[2].ToString();
                        //dto.AccountID = Convert.ToInt64(row[0].ToString());
                        //dto.ExternalID = row[3].ToString();

                        dto.HtmlURL = "http://www.yelp.com/biz/pakwan-san-francisco?nb=1";
                        int DealerLocationID = Convert.ToInt32(row[1].ToString());

                        var googleResult = connection.Scrape(dto, true);
                        Debug.WriteLine(iCounter + ". {0} Rating, {1} ReviewCount , {2} Details, {3} URL", googleResult.SalesAverageRating, googleResult.SalesReviewCount, googleResult.ReviewDetails.Count, dto.HtmlURL);
                        Console.WriteLine(iCounter + ". {0} Rating, {1} ReviewCount , {2} Details, {3} URL", googleResult.SalesAverageRating, googleResult.SalesReviewCount, googleResult.ReviewDetails.Count, dto.HtmlURL);

                        dbContext.spReviewSummarySDSIUpdate("Yelp", Convert.ToInt32(DealerLocationID), Convert.ToInt32(dto.AccountID), dto.HtmlURL, Convert.ToInt32(googleResult.SalesReviewCount), Convert.ToDouble(googleResult.SalesAverageRating), googleResult.ReviewDetails.Count);

                        iCounter++;
                    }


                    //SIReviewConnection connection = ProviderFactory.Reviews.GetReviewConnection(SI.DTO.ReviewSourceEnum.Yelp);
                    //ReviewUrlDTO dto = new ReviewUrlDTO();
                    ////http://www.yelp.com/biz/advantage-chevrolet-bolingbrook
                    ////http://www.yelp.com/biz/auto-showcase-of-carol-stream-carol-stream
                    ////http://www.yelp.com/biz/bmw-of-manhattan-new-york-2
                    ////http://www.yelp.com/biz/bmw-motorcycles-of-manhattan-new-york
                    ////http://www.yelp.com/biz/mini-of-manhattan-new-york-2
                    ////http://www.yelp.com/biz/elmhurst-toyota-elmhurst#query:Chevrolet (Lots of reviews)
                    ////http://www.yelp.com/biz/fineride-motors-villa-park#query:Chevrolet No reviews no rating
                    ////http://www.yelp.com/biz/castle-chevrolet-villa-park Castle with Reviews
                    //url = "http://www.yelp.ca/biz/auto-west-infiniti-richmond"; //Canadian Site
                    //dto.HtmlURL = url;
                    //strURL = dto.HtmlURL;
                    //ReviewDownloadResult result = connection.Scrape(dto, true);
                    //testresult = result;

                    

                    testUrlList.Add("http://www.yelp.com/biz/carter-subaru-shoreline-seattle");
                    testUrlList.Add("http://www.yelp.com/biz/carter-subaru-ballard-seattle");
                    testUrlList.Add("http://www.yelp.com/biz/carter-volkswagen-saab-seattle");
                    testUrlList.Add("http://www.yelp.com/biz/kearny-mesa-subaru-san-diego-3");
                    testUrlList.Add("http://www.yelp.com/biz/nate-wade-subaru-salt-lake-city-4");
                    testUrlList.Add("http://www.yelp.com/biz/renick-subaru-fullerton");
                    testUrlList.Add("http://www.yelp.com/biz/tucson-subaru-tucson");

                    foreach (var item in testUrlList)
                    {
                        var dto = new ReviewUrlDTO();
                        dto.HtmlURL = item;

                        var yelpResult = connection.Scrape(dto, true);
                        Debug.WriteLine("{0} Rating, {1} ReviewCount , {2} Details, {3} Errors, {4} URL", yelpResult.SalesAverageRating, yelpResult.SalesReviewCount, yelpResult.ReviewDetails.Count, yelpResult.FailureReasons.Count, item);
                    }

                    Debug.WriteLine("");
                }

                #endregion

                #region CarDealerReviews#5

                if (testNumber == 5)
                {
                    SIReviewConnection connection = ProviderFactory.Reviews.GetReviewConnection(SI.DTO.ReviewSourceEnum.CarDealer);
                    ReviewUrlDTO dto = new ReviewUrlDTO();
                    //http://www.cardealerreviews.org/?p=16913 No Reviews NoRating
                    //http://www.cardealerreviews.org/?p=84541 1 review
                    //http://www.cardealerreviews.org/?p=11210 No Reviews NoRating
                    //http://www.cardealerreviews.org/?p=11209 No Reviews NoRating
                    //http://www.cardealerreviews.org/?p=10135 No Reviews NoRating
                    //http://www.cardealerreviews.org/?p=8324 No Reviews NoRating
                    //http://www.merchantcircle.com/business/Cantrell.Portrait.Design.707-745-1951 ???
                    //http://www.cardealerreviews.org/?p=12226 5 Reviews
                    //http://www.cardealerreviews.org/?p=27671 Reviews & Rating
                    //http://www.cardealerreviews.org/?p=17578 No Reviews NoRating
                    //http://www.cardealerreviews.org/?p=7421 No Reviews & Rating
                    dto.HtmlURL = "http://www.cardealerreviews.org/?p=5610";
                    strURL = dto.HtmlURL;
                    ReviewDownloadResult result = connection.Scrape(dto, true);

                    testresult = result;
                }

                #endregion

                #region Yellow Pages #6

                if (testNumber == 6)
                {
                    SIReviewConnection connection = ProviderFactory.Reviews.GetReviewConnection(SI.DTO.ReviewSourceEnum.YellowPages);
                    ReviewUrlDTO dto1 = new ReviewUrlDTO();

                    dto1.HtmlURL = "http://www.yellowpages.com/san-antonio-tx/mip/red-mccombs-toyota-461726648?lid=171392453";
                    dto1.AccountID = 220245;
                    dto1.ExternalID = "";
                    var googleResult1 = connection.Scrape(dto1, true);


                    string myXMLfile = @"C:\Dev2\SI\SI_PLATFORM\ConnectorTest\URLs.xml";
                    DataSet ds = new DataSet();

                    ds.ReadXml(myXMLfile);


                    int iCounter = 1;
                    SDReviewEntities dbContext = new SDReviewEntities();

                    foreach (DataRow row in ds.Tables[0].Rows)
                    {
                        ReviewUrlDTO dto = new ReviewUrlDTO();

                        dto.HtmlURL = "http://www.yellowpages.com/san-antonio-tx/mip/red-mccombs-toyota-461726648?lid=171392453";
                        //dto.AccountID = Convert.ToInt64(row[0].ToString());
                        //dto.ExternalID = row[3].ToString();

                        int DealerLocationID = Convert.ToInt32(row[1].ToString());

                        var googleResult = connection.Scrape(dto, true);
                        Debug.WriteLine(iCounter + ". {0} Rating, {1} ReviewCount , {2} Details, {3} URL", googleResult.SalesAverageRating, googleResult.SalesReviewCount, googleResult.ReviewDetails.Count, dto.HtmlURL);
                        Console.WriteLine(iCounter + ". {0} Rating, {1} ReviewCount , {2} Details, {3} URL", googleResult.SalesAverageRating, googleResult.SalesReviewCount, googleResult.ReviewDetails.Count, dto.HtmlURL);

                        dbContext.spReviewSummarySDSIUpdate("Yellow Pages", Convert.ToInt32(DealerLocationID), Convert.ToInt32(dto.AccountID), dto.HtmlURL, Convert.ToInt32(googleResult.SalesReviewCount), Convert.ToDouble(googleResult.SalesAverageRating), googleResult.ReviewDetails.Count);

                        iCounter++;
                    }



                    //SIReviewConnection connection = ProviderFactory.Reviews.GetReviewConnection(SI.DTO.ReviewSourceEnum.YellowPages);
                    //ReviewUrlDTO dto = new ReviewUrlDTO();
                    ////http://www.yellowpages.com/new-york-ny/mip/lombardis-pizza-193613?lid=193613 -pass
                    ////http://www.yellowpages.com/oak-brook-il/mip/sears-auto-center-11302446?lid=178684725 --bad
                    ////http://www.yellowpages.com/new-york-ny/mip/the-sports-center-at-chelsea-piers-21722011?lid=173703152 --pass
                    ////http://www.yellowpages.com/omaha-ne/mip/gregg-young-chevrolet-460353502?lid=460353502 --bad
                    ////http://www.yellowpages.com/owings-mills-md/mip/len-stoler-inc-455318001?lid=455318001 --bad
                    ////http://www.yellowpages.com/westminster-md/mip/len-stoler-chevrolet-461440271 --bad
                    ////http://www.yellowpages.com/san-antonio-tx/mip/alamo-toyota-12835083?lid=182825480 --bad
                    ////http://www.yellowpages.com/charleston-sc/mip/hendrick-honda-of-charleston-474816769?lid=474816769 -pass
                    ////http://www.yellowpages.com/charleston-sc/mip/hendrick-hyundai-455410236?lid=455410236 --pass
                    ////http://www.yellowpages.com/nationwide/mip/lexus-of-charleston-462212578?lid=462212578 - No Reviews - No Rating --pass
                    ////http://www.yellowpages.com/westminster-md/mip/len-stoler-chevrolet-cadillac-474222781?lid=474222781 - No Reviews - No Rating --pass
                    ////http://www.yellowpages.com/villa-park-il/mip/castle-chevrolet-1163184?lid=185221277 - Rating & Reviews --pass
                    ////http://www.yellowpages.com/hodgkins-il/mip/advantage-chevrolet-21864451?lid=21864451 Has response --pass

                    ////dto.HtmlURL = "http://www.yellowpages.com/new-york-ny/mip/the-sports-center-at-chelsea-piers-21722011?lid=173703152"; 14 - Gim reviews
                    ////dto.HtmlURL = "http://www.yellowpages.com/elmhurst-il/mip/elmhurst-toyota-scion-1491261?lid=1491261"; //12 reviews
                    ////dto.HtmlURL = "http://www.yellowpages.com/hodgkins-il/mip/advantage-chevrolet-21864451?lid=21864451"; //has review comments
                    ////dto.HtmlURL = "http://www.yellowpages.com/westmont-il/mip/mcgrath-acura-of-westmont-6730894/reviews?lid=6730894"; //54 reviews
                    ////dto.HtmlURL = "http://www.yellowpages.com/costa-mesa-ca/mip/orange-coast-buick-gmc-cadillac-230604/reviews?lid=230604"; //has NO reviews
                    //url = "http://www.yellowpages.ca/bus/British-Columbia/Vancouver/Destination-Hyundai/6627693.html?what=Destination+hyundai&where=Canada&cli=1,4&le=15eec34154|15eec33fae|15eec38581|15eec34124|15eec34026|15eec3400e|8e2a99ed54|15354"; //Canadian Site (Destination Hyundai)
                    ////url = "http://www.yellowpages.ca/bus/British-Columbia/Port-Moody/Westwood-Honda/3278531.html?what=Westwood+Honda&where=Canada&cli=1,2&le=16a789c481|15faaf020e|15faaf01ae|15faaf4781|15faaf0226|15faaf0324|0|15faaf0354";
                    ////url = "http://www.yellowpages.ca/bus/British-Columbia/Coquitlam/Eagle-Ridge-Chevrolet/2241941.html?what=eagle+ridge+chevrolet&where=Canada&cli=1,2&le=15eec33fc6|15eec34154|15eec33fae|15eec34124|15eec34026|15eec3400e|15eec38581|15faaf01c6|163e55bbce|15faaf020e";
                    ////url = "http://www.yellowpages.ca/bus/British-Columbia/Victoria/Harris-Victoria-Chrysler-Dodge-Jeep-Ram-Ltd/6596613.html?what=Harris+victoria&where=Canada&cli=1,7&le=15eec3418e";
                    ////url = "http://www.yellowpages.ca/bus/British-Columbia/Port-Coquitlam/Metro-Motors-Ltd/2408092.html?what=metro+motors&where=Canada&cli=1,8&le=13ce1ed3a1e|15eec34026|15eec34124|15eec38581|15eec34154|15eec3400e|15eec33fae";
                    ////url = "http://www.yellowpages.ca/bus/British-Columbia/Surrey/White-Rock-Hyundai/3876104.html?what=whiterock+hyundai&where=Canada&cli=1,4&le=15eec34124|15eec34026|15eec34154|15eec3400e";
                    ////url = "http://www.yellowpages.ca/bus/British-Columbia/West-Vancouver/Urban-Garage/6292821.html?what=the+urban+garage&where=Canada&cli=1,6&le=15faaf020e|15faaf01ae|15faaf4781|15faaf0226|15faaf0354|15faaf0324"; 
                    ////url = "http://www.yellowpages.ca/bus/British-Columbia/Pitt-Meadows/West-Coast-Kia/8025078.html?what=west+coast+kia&where=Canada&cli=1,4&le=15eec34148|15eec33e1e|16a7898048|13ce1ed3d48|15faaf0348|1665f8d548|16a7897d1e|13ce1ed3a1e|15faaf001e|1665f8d21e";
                    ////url = "";
                    ////url = "";
                    ////url = "";
                    ////url = "";
                    ////url = "";
                    //dto.HtmlURL = url;
                    //strURL = dto.HtmlURL;
                    //ReviewDownloadResult result = connection.Scrape(dto, true);
                    //testresult = result;

                    //SIReviewConnection connection = ProviderFactory.Reviews.GetReviewConnection(SI.DTO.ReviewSourceEnum.YellowPages);

                    /* USA
                    //testUrlList.Add("http://www.yellowpages.com/shoreline-wa/mip/carter-subaru-9324621?lid=9324621");
                    //testUrlList.Add("http://www.yellowpages.com/seattle-wa/mip/carter-subaru-ballard-22985562?lid=22985562");
                    //testUrlList.Add("http://www.yellowpages.com/seattle-wa/mip/carter-volkswagen-473086420?lid=473086420");
                    //testUrlList.Add("http://www.yellowpages.com/salt-lake-city-ut/mip/nate-wade-subaru-58887?lid=217091201");
                    //testUrlList.Add("http://www.yellowpages.com/fullerton-ca/mip/renick-subaru-461878289?lid=461878289");
                    //testUrlList.Add("http://www.yellowpages.com/tucson-az/mip/emich-subaru-parts-7398983?lid=7398983");
                    */

                    //testUrlList.Add("http://www.yellowpages.ca/bus/British-Columbia/Vancouver/Kingsway-Honda/4152171.html?what=kingsway+honda&where=Canada&cli=1,8&le=15eec34154|8e2a99ed54|15eec33fae|15eec38581|15eec34124|15eec34026|15eec3400e|15354");

                    //testUrlList.Add("http://www.yellowpages.ca/bus/British-Columbia/Vancouver/Blue-Star-Motors/2128728.html?what=Blue+star+motors&where=Canada&cli=1,5&le=15faaf01ae|15faaf4781|15faaf0324|15faaf0354|15faaf0226|15faaf020e");

                    //testUrlList.Add("http://www.yellowpages.ca/bus/British-Columbia/Vancouver/Destination-Hyundai/6627693.html?what=Destination+hyundai&where=Canada&cli=1,4&le=15eec34154|15eec33fae|15eec38581|15eec34124|15eec34026|15eec3400e|8e2a99ed54|15354");

                    //testUrlList.Add("http://www.yellowpages.ca/bus/British-Columbia/Vancouver/Destination-Hyundai/6627693.html?what=Destination+hyundai&where=Canada&cli=1,4&le=15eec34154|15eec33fae|15eec38581|15eec34124|15eec34026|15eec3400e|8e2a99ed54|15354"); //Has Reviews
                    //testUrlList.Add("http://www.yellowpages.ca/bus/British-Columbia/Prince-George/5th-and-Carney-Subaru/2063711.html?what=5th+%26+Carney+Subaru&where=Canada&cli=1,1&le=15eec340c4|0");
                    //testUrlList.Add("http://www.yellowpages.ca/bus/British-Columbia/Richmond/Auto-Fleet-Services-Inc/2104446.html?what=autofleet&where=Canada&cli=1,4&le=19781|15354|15226|15324|1520e|151ae");
                    //testUrlList.Add("http://www.yellowpages.ca/bus/British-Columbia/Richmond/Accent-Leasing-Sales-Ltd/8074534.html?what=accent+leasing+and+sales&where=Canada&cli=1,8&le=b1a6596124|163e55bbce|b1a6596154|163e55bb6e|163e560141|163e55bbe6");
                    //testUrlList.Add("http://www.yellowpages.ca/bus/British-Columbia/Port-Alberni/Alberni-Chrysler-Dodge-Jeep/6507945.html?what=alberni+chrysler&where=Canada&cli=1,2&le=15eec3407c|1527c|2ba1ea647c");
                    //testUrlList.Add("http://www.yellowpages.ca/bus/British-Columbia/Port-Alberni/Alberni-Toyota/7924510.html?what=alberni+toyota&where=Canada&cli=1,2&le=15eec3407c|15faaf027c");
                    //testUrlList.Add("http://www.yellowpages.ca/bus/British-Columbia/Langley/Applewood-Langley-Kia/6115490.html?what=applewood+langley+kia&where=Canada&cli=1,8&le=15eec33fae|15eec34154|15eec33e1e|15226|151ae|1520e|15354|19781|15324|150e0");
                    //testUrlList.Add("http://www.yellowpages.ca/bus/British-Columbia/Surrey/Applewood-Kia/2093027.html?what=applewood+surrey+kia&where=Canada&cli=1,3&le=0");
                    //testUrlList.Add("http://www.yellowpages.ca/bus/Canada/Auto-West-BMW/5208555.html?what=autowest+bmw&where=Canada&cli=1,3&le=15eec34154|15eec33fae|15eec34026|16a7898054|16a7898024|16a7897eae|16a789c481|16a7897f0e");
                    //testUrlList.Add("http://www.yellowpages.ca/bus/British-Columbia/Vernon/Bannister-Honda/1530486.html?what=bannister+honda&where=Canada&cli=1,1&le=15eec3416c|15faaf036c|1665f8d56c|16a789806c");
                    
                    //testUrlList.Add("http://www.yellowpages.ca/bus/British-Columbia/Duncan/Bow-Mel-Chrysler-Ltd/3539880.html?what=Bow-Mel+Chrysler&where=Canada&cli=1,1&le=15faaf00c8");
                    //testUrlList.Add("http://www.yellowpages.ca/bus/British-Columbia/Vancouver/Brian-Jessel-BMW/2138507.html?what=Brian+Jessel+BMW&where=Canada&cli=1,7&le=15eec34124|15eec34026|15eec33fae|15eec38581|16a7898054|16a7897eae|16a7897f0e");
                    //testUrlList.Add("http://www.yellowpages.ca/bus/British-Columbia/Courtenay/Courtenay-Toyota-Ltd/100179194.html?what=Courtenay+Toyota&where=Canada&cli=1,3&le=15eec33e68|15eec3404c|15eec33e12|15eec340ac|15faaf0068|13ce1ed3c4c|13ce1ed3a12|13ce1ed3a68|16a7897d68|13ce1ed3cac");
                    //testUrlList.Add("http://www.yellowpages.ca/bus/British-Columbia/Vancouver/Ferrari-Maserati-Of-Vancouver/6218704.html?what=ferrari+maserati+of+vancouver&where=Canada&cli=1,2&le=15eec34154|15eec34026|15eec34124|15eec33fae|15eec3400e|15eec38581");
                    //testUrlList.Add("http://www.yellowpages.ca/bus/British-Columbia/North-Vancouver/Cam-Clark-Ford-Lincoln-Ltd/1797591.html?what=Cam+Clark+Ford+Lincoln&where=Canada&cli=1,2&le=15eec33fae|15eec34154|15eec38581|15eec34026|15eec34124|15eec3400e|15a73cb426|16a7897f26|15354|15226");
                    //testUrlList.Add("http://www.yellowpages.ca/bus/British-Columbia/Burnaby/Carter-Chevrolet-Cadillac-Buick-GMC-Burnaby-Ltd/7751763.html?what=Carter+burnaby&where=Canada&cli=1,1&le=b4300a2d26|b4300a2e24|b4300a2d0e|1520e|a920e5954|b4300a2e54");
                    //testUrlList.Add("http://www.yellowpages.ca/bus/British-Columbia/North-Vancouver/Carter-GM-Northshore/3253918.html?what=Carter+GM&where=Canada&cli=1,2&le=15eec34154|15eec34026|15eec38581|15eec34124|15eec34130|15eec3400e|151ae|16a7898054|163e55bbe6|15faaf0226");
                    //testUrlList.Add("http://www.yellowpages.ca/bus/British-Columbia/Burnaby/Carter-Dodge-Chrysler/2163527.html?what=Carter+dodge&where=Canada&cli=1,2&le=15eec3400e|15eec38581|15eec34154|15eec33fae|15eec34026|15eec34124|13ce1ed3c0e|13ce1ed8181|0|13ce1ed3d54");
                    //testUrlList.Add("http://www.yellowpages.ca/bus/British-Columbia/Vancouver/Carter-Honda/315858.html?what=Carter+honda&where=Canada&cli=1,8&le=15eec34026|15eec34124|15eec33fae|15eec38581|15eec34154");
                    //testUrlList.Add("http://www.yellowpages.ca/bus/British-Columbia/Castlegar/Castlegar-Toyota/8001702.html?what=castlegar+toyota&where=Canada&cli=1,4&le=15eec3413c|15eec33f10|15eec33ff6|15faaf033c|1533c");
                    //testUrlList.Add("http://www.yellowpages.ca/bus/British-Columbia/Victoria/Colwood-Car-Mart-Ltd/6574478.html?what=Colwood+Car+Mart&where=Canada&cli=1,3&le=15faaf3726|15faaf038e");
                    //testUrlList.Add("http://www.yellowpages.ca/bus/British-Columbia/North-Vancouver/Destination-Chrysler-Dodge-Jeep/2225153.html?what=Destination+Chrysler&where=Canada&cli=1,5&le=0");
                    //testUrlList.Add("http://www.yellowpages.ca/bus/British-Columbia/Vancouver/Destination-Hyundai/6627693.html?what=Destination+hyundai&where=Canada&cli=1,4&le=15eec34154|15eec33fae|15eec38581|15eec34124|15eec34026|15eec3400e|8e2a99ed54|15354");
                    //testUrlList.Add("http://www.yellowpages.ca/bus/British-Columbia/Vancouver/Destination-Mazda/1803232.html?what=Destination+mazda&where=Canada&cli=1,2&le=15eec34154|8e2a99ed54|0");
                    //testUrlList.Add("http://www.yellowpages.ca/bus/British-Columbia/Burnaby/Destination-Toyota-Burnaby/4152015.html?what=Destination+Toyota&where=Canada&cli=1,7&le=15eec38581|15eec34154|1520e");
                    //testUrlList.Add("http://www.yellowpages.ca/bus/British-Columbia/Port-Moody/Westwood-Honda/3278531.html?what=Westwood+Honda&where=Canada&cli=1,2&le=16a789c481|15faaf020e|15faaf01ae|15faaf4781|15faaf0226|15faaf0324|0|15faaf0354");
                    //testUrlList.Add("http://www.yellowpages.ca/bus/British-Columbia/Kelowna/Don-Folk-Chevrolet/2231677.html?what=don+folk+chevy&where=Canada&cli=1,4&le=15eec33f66|15faaf0166|0|163e55bb26|15166");
                    //testUrlList.Add("http://www.yellowpages.ca/bus/British-Columbia/Vancouver/Dueck-Downtown/100351914.html?what=dueck+downtown&where=Canada&cli=1,6&le=0|1345079cbae|15354|134507a1181");
                    //testUrlList.Add("http://www.yellowpages.ca/bus/British-Columbia/Vancouver/Dueck/530991.html?what=dueck&where=Canada&cli=1,6&le=1345079cd54");
                    //testUrlList.Add("http://www.yellowpages.ca/bus/British-Columbia/Richmond/Dueck-Richmond-Buick-Cadillace-GMC-Ltd/7209220.html?what=dueck+richmond&where=Canada&cli=1,6&le=15eec34124|15faaf0324|15a73cb524|15a73cb426|15a73cb40e|15a73cf981|15a73cb3ae|15a73cb554|0&dirs=");
                    //testUrlList.Add("http://www.yellowpages.ca/bus/British-Columbia/Coquitlam/Eagle-Ridge-Chevrolet/2241941.html?what=eagle+ridge+chevrolet&where=Canada&cli=1,2&le=15eec33fc6|15eec34154|15eec33fae|15eec34124|15eec34026|15eec3400e|15eec38581|15faaf01c6|163e55bbce|15faaf020e");
                    //testUrlList.Add("http://www.yellowpages.ca/bus/British-Columbia/Castlegar/Glacier-Honda/2290813.html?what=Glacier+honda&where=Canada&cli=1,3&le=15eec3413c|8e2a99ed3c|8e2a99ebf6|15eec33ff6|15faaf033c|1345079cd3c|1345079cbf6|15faaf01f6|16a7897ef6");
                    //testUrlList.Add("http://www.yellowpages.ca/bus/British-Columbia/Surrey/White-Rock-Haley-Dodge-Chrysler-Jeep/2306460.html?what=Haley+Dodge&where=Canada&cli=1,5&le=15eec341a0|15eec38581|15eec34124|15eec34026|15eec34154|15eec3400e|15eec33fae|0");
                    //testUrlList.Add("http://www.yellowpages.ca/bus/British-Columbia/Nanaimo/Harris-Kia/6512389.html?what=Harris+Kia&where=Canada&cli=1,1&le=15eec33fde");
                    //testUrlList.Add("http://www.yellowpages.ca/bus/British-Columbia/Nanaimo/Harris-Mazda/6510409.html?what=Harris+mazda&where=Canada&cli=1,1&le=15faaf01de");
                    //testUrlList.Add("http://www.yellowpages.ca/bus/Canada/Harris-Mitsubishi/7702253.html?what=Harris+mitsubishi&where=Canada&cli=1,7&le=15eec33fde");
                    //testUrlList.Add("http://www.yellowpages.ca/bus/British-Columbia/Campbell-River/Harris-Nissan-North-Island-Ltd/6590082.html?what=Harris+nissan&where=Canada&cli=1,1&le=15eec33e12|15012");
                    //testUrlList.Add("http://www.yellowpages.ca/bus/British-Columbia/Parksville/Oceanside-Chevrolet-Buick-GMC-Ltd/6468978.html?what=Harris+gm&where=Canada&cli=1,7&le=0");
                    //testUrlList.Add("http://www.yellowpages.ca/bus/British-Columbia/Victoria/Harris-Victoria-Chrysler-Dodge-Jeep-Ram-Ltd/6596613.html?what=Harris+victoria&where=Canada&cli=1,7&le=15eec3418e");
                    //testUrlList.Add("http://www.yellowpages.ca/bus/British-Columbia/Victoria/Howie-s-Car-Corral/1799366.html?what=howies+car+corral&where=Canada&cli=1,4&le=15faaf038e|81506cc58e|15faaf0327|15faaf3726|16a789808e");
                    //testUrlList.Add("http://www.yellowpages.ca/bus/British-Columbia/Dawson-Creek/Inland-Auto-Centre-Ltd/819468.html?what=inland+auto+centre&where=Canada&cli=1,8&le=8e2a99eab0|16a7897db0|13ce1ed3ab0|15eec33eb0|15faaf00b0|1665f8d2b0");
                    //testUrlList.Add("http://www.yellowpages.ca/bus/British-Columbia/Burnaby/Metrotown-Mazda/1056131.html?what=metrotown+mazda&where=Canada&cli=1,3&le=15eec3400e|15eec34026|15eec33fae|15eec34124|15eec34154|15eec38581");
                    //testUrlList.Add("http://www.yellowpages.ca/bus/British-Columbia/Castlegar/Kalawsky-Chevrolet-Buick-GMC/7441879.html?what=kalawsky+chevroley&where=Canada&cli=1,2&le=15faaf033c|1533c");
                    //testUrlList.Add("http://www.yellowpages.ca/bus/British-Columbia/Kamloops/Kamloops-Dodge-Chrysler-Jeep/2809502.html?what=Kamloops+dodge&where=Canada&cli=1,6&le=15faaf01b2");
                    //testUrlList.Add("http://www.yellowpages.ca/bus/British-Columbia/Kamloops/Kamloops-Hyundai/6293968.html?what=Kamloops+hyundai&where=Canada&cli=1,4&le=15eec33f4e|16a7897e4e");
                    //testUrlList.Add("http://www.yellowpages.ca/bus/British-Columbia/Kelowna/Kelowna-Mercedes-Benz/4229274.html?what=kelowna+mercedes&where=Canada&cli=1,7&le=0");
                    //testUrlList.Add("http://www.yellowpages.ca/bus/British-Columbia/New-Westminster/Key-West-Ford-Sales-Ltd/888997.html?what=Key+West+Ford&where=Canada&cli=1,3&le=15eec34154|15eec3400e|15eec34026|13ce1ed3d24|15faaf01ae|19781|15faaf0354|163e55bd14|15faaf020e|16a7897f0e");
                    
                    //testUrlList.Add("http://www.yellowpages.ca/bus/British-Columbia/Vancouver/Lamborghini-Vancouver/3562271.html?what=lamborghini+of+vancouver&where=Canada&cli=1,7&le=0");
                    //testUrlList.Add("http://www.yellowpages.ca/bus/British-Columbia/Surrey/Langley-Hyundai/6563893.html?what=langley+hyundai&where=Canada&cli=1,1&le=15eec34124|15eec33fae|15eec34026|15eec34154|15eec3400e|15eec38581");
                    //testUrlList.Add("http://www.yellowpages.ca/bus/British-Columbia/Kelowna/Lexus-of-Kelowna/7950261.html?what=kelowna+lexus&where=Canada&cli=1,4&le=15eec33f66");
                    //testUrlList.Add("http://www.yellowpages.ca/bus/British-Columbia/Abbotsford/Magnuson-Ford-Sales-Ltd/6445743.html?what=magnuson+ford&where=Canada&cli=1,4&le=16a7897d1e");
                    //testUrlList.Add("http://www.yellowpages.ca/bus/British-Columbia/Maple-Ridge/Marv-Jones-Honda/1818133.html?what=marv+jones+honda&where=Canada&cli=1,8&le=15eec3400e|15eec38581|15eec33fc6|15eec34026|15eec34124|15eec33fae|15eec34154");
                    //testUrlList.Add("http://www.yellowpages.ca/bus/British-Columbia/Chilliwack/Mertin-Chevrolet-Cadillac-Buick-GMC-Ltd/6762551.html?what=mertin+chevrolet&where=Canada&cli=1,2&le=15eec34148|16a7898048");
                    //testUrlList.Add("http://www.yellowpages.ca/bus/British-Columbia/Chilliwack/Mertin-Hyundai/4499328.html?what=mertin+hyundai&where=Canada&cli=1,4&le=15eec34148");
                    //testUrlList.Add("http://www.yellowpages.ca/bus/British-Columbia/Chilliwack/Mertin-Nissan/6619738.html?what=mertin+nissan&where=Canada&cli=1,6&le=15eec34148");
                    //testUrlList.Add("http://www.yellowpages.ca/bus/British-Columbia/Port-Coquitlam/Metro-Motors-Ltd/2408092.html?what=metro+motors&where=Canada&cli=1,8&le=13ce1ed3a1e|15eec34026|15eec34124|15eec38581|15eec34154|15eec3400e|15eec33fae");
                    //testUrlList.Add("http://www.yellowpages.ca/bus/British-Columbia/Richmond/Mini-Richmond/7975337.html?what=mini+richmond&where=Canada&cli=1,3&le=15324");
                    //testUrlList.Add("http://www.yellowpages.ca/bus/British-Columbia/Port-Coquitlam/Morrey-Infiniti-Nissan-Of-Coquitlam-Ltd/2418735.html?what=morrey+infiniti&where=Canada&cli=1,8&le=15eec34026|15eec34154");
                    //testUrlList.Add("http://www.yellowpages.ca/bus/British-Columbia/North-Vancouver/Mazda-Morrey-Of-The-North-Shore/4469780.html?what=morrey+mazda&where=Canada&cli=1,4&le=15eec33fae|15eec38581|15eec34124|15eec3400e|15354|15226");
                    //testUrlList.Add("http://www.yellowpages.ca/bus/British-Columbia/Burnaby/Morrey-Nissan/7759955.html?what=morrey+nissan&where=Canada&cli=1,4&le=15eec34026|15eec34124|15eec38581|15eec34154|15eec3400e|15eec33fae");
                    //testUrlList.Add("http://www.yellowpages.ca/bus/British-Columbia/Abbotsford/M-S-A-Ford-Sales-Ltd/2386125.html?what=MSA+ford&where=Canada&cli=1,5&le=15eec33e1e|15eec33ee0|1501e|1665f8d21e|15faaf00e0");
                    //testUrlList.Add("http://www.yellowpages.ca/bus/British-Columbia/Penticton/Murray-Buick-GMC-Penticton/7092328.html?what=murrey+buick&where=Canada&cli=1,7&le=15eec34064|15eec3412f|0|15a73cb464|13ce1ed3c64|16a7897f64|16a789802f|15a73cb52f");
                    //testUrlList.Add("http://www.yellowpages.ca/bus/British-Columbia/Surrey/White-Rock-Hyundai/3876104.html?what=whiterock+hyundai&where=Canada&cli=1,4&le=15eec34124|15eec34026|15eec34154|15eec3400e");
                    //testUrlList.Add("http://www.yellowpages.ca/bus/British-Columbia/Houston/Sullivan-Motor-Products-Ltd/1524180.html?what=sullivan+motor+products&where=Canada&cli=1,6&le=15eec33e06|13ce1ed3a06");
                    //testUrlList.Add("http://www.yellowpages.ca/bus/British-Columbia/Nanaimo/Chrysler-Nanaimo/8138923.html?what=nanaimo+chrysler&where=Canada&cli=1,3&le=15eec33fde|151de");
                    //testUrlList.Add("http://www.yellowpages.ca/bus/British-Columbia/Nelson/Nelson-Ford-Sales-2003-Inc/1119988.html?what=nelson+ford&where=Canada&cli=1,7&le=15eec33ff6|16a7897ef6|151f6|8e2a99ebf6|15faaf01f6|1665f8d3f6");
                    //testUrlList.Add("http://www.yellowpages.ca/bus/British-Columbia/North-Vancouver/North-Shore-Mitsubishi/1140958.html?what=north+shore+mitsubishi&where=Canada&cli=1,8&le=15faaf020e|15faaf0226|15faaf01ae|15faaf0354|15faaf4781|15faaf0324");
                    //testUrlList.Add("http://www.yellowpages.ca/bus/British-Columbia/Surrey/Ocean-Park-Ford-Sales/2440712.html?what=Ocean+park+for&where=Canada&cli=1,7&le=15eec33fae|1665f8d3ae|15faaf01ae|15eec341a0|15eec38581|15eec34124|15eec34026|1665f8d554|15faaf0191|1665f8d391");
                    //testUrlList.Add("http://www.yellowpages.ca/bus/British-Columbia/Surrey/Pacific-Auto-Source-Ltd/2449489.html?what=pacific+auto+source&where=Canada&cli=1,5&le=15faaf4781|15faaf0324|15faaf0226|15faaf0354|15faaf020e|15faaf01ae|15faaf039d");
                    //testUrlList.Add("http://www.yellowpages.ca/bus/British-Columbia/North-Vancouver/Pacific-Honda/1800802.html?what=pacific+honda&where=Canada&cli=1,3&le=16a7897eae|16a7898054|16a789c481|1345079cc26|16a7898024|16a7897f0e");
                    //testUrlList.Add("http://www.yellowpages.ca/bus/British-Columbia/Victoria/Pacific-Mazda/1185044.html?what=pacific+mazda&where=Canada&cli=1,6&le=15eec3418e|15faaf038e|1538e");
                    //testUrlList.Add("http://www.yellowpages.ca/bus/British-Columbia/Penticton/Penticton-Honda/2459722.html?what=penticton+honda&where=Canada&cli=1,4&le=15264");
                    //testUrlList.Add("http://www.yellowpages.ca/bus/British-Columbia/Mission/Pioneer-Chrysler-Jeep/2466612.html?what=pioneer+chrysler&where=Canada&cli=1,2&le=15faaf4781|13ce1ed3a1e");
                    //testUrlList.Add("http://www.yellowpages.ca/bus/British-Columbia/Prince-Rupert/Rainbow-Chrysler-Dodge-Jeep-Ltd/2488078.html?what=rainbow+chrysler&where=Canada&cli=1,8&le=15eec340dc|13ce1ed3cdc|16a7897fdc|15faaf02dc|1665f8d4dc");
                    //testUrlList.Add("http://www.yellowpages.ca/bus/British-Columbia/Richmond/Richmond-Acura/2500544.html?what=richmond+acura&where=Canada&cli=1,4&le=0");
                    //testUrlList.Add("http://www.yellowpages.ca/bus/British-Columbia/Richmond/Richmond-Honda/6700395.html?what=richmond+honda&where=Canada&cli=1,7&le=15a73cb554|15a73cb524|19781|15354|15324|15226|1520e|151ae");
                    //testUrlList.Add("http://www.yellowpages.ca/bus/British-Columbia/Kamloops/River-City-Nissan/7758848.html?what=river+city+nissan&where=Canada&cli=1,2&le=15faaf014e");
                    //testUrlList.Add("http://www.yellowpages.ca/bus/British-Columbia/Salmon-Arm/Salmon-Arm-GM/5981374.html?what=salmon+arm+gm&where=Canada&cli=1,3&le=15eec34129|15faaf0329");
                    //testUrlList.Add("http://www.yellowpages.ca/bus/British-Columbia/Richmond/Signature-Mazda/1448966.html?what=signature+mazda&where=Canada&cli=1,2&le=163e55bd14|163e55bce4|163e55bbe6|163e55bbce|163e55bb6e|163e560141");
                    //testUrlList.Add("http://www.yellowpages.ca/bus/British-Columbia/Kamloops/Smith-Chevrolet-Cadillac-Ltd/2540867.html?what=smith+gm&where=Canada&cli=1,5&le=15eec33f4e|15faaf014e|15a73cb34e|163e55bb0e");
                    //testUrlList.Add("http://www.yellowpages.ca/bus/British-Columbia/Nanaimo/Steve-Marshall-Ford-Lincoln-Ltd/2555063.html?what=steve+marshall+ford&where=Canada&cli=1,2&le=15eec33fde|15eec3404c|8e2a99ebde|15faaf01de|163e55bb9e|0");
                    //testUrlList.Add("http://www.yellowpages.ca/bus/British-Columbia/Surrey/Surrey-Honda/2319056.html?what=Surrey+Honda&where=Canada&cli=1,3&le=15eec3419d|15eec33fae|0|16a7897f0e|16a789c481|16a7898024|16a7897f26|16a7898054|151ae|15faaf01ae");
                    //testUrlList.Add("http://www.yellowpages.ca/bus/British-Columbia/West-Vancouver/Urban-Garage/6292821.html?what=the+urban+garage&where=Canada&cli=1,6&le=15faaf020e|15faaf01ae|15faaf4781|15faaf0226|15faaf0354|15faaf0324");
                    //testUrlList.Add("http://www.yellowpages.ca/bus/British-Columbia/Port-Coquitlam/Tricity-Mitsubishi/8195721.html?what=tricity+mitsubishi&where=Canada&cli=1,4&le=15eec34026|15eec34124|15eec34154|15eec3400e|15eec33fae|15eec38581");
                    //testUrlList.Add("http://www.yellowpages.ca/bus/British-Columbia/Coquitlam/Trinity-Auto-Center/8144689.html?what=trinity+auto+centre&where=Canada&cli=1,7&le=16a7898054|16a7897eae|16a7898024|16a7897f26|16a7897f0e|16a789c481");
                    //testUrlList.Add("http://www.yellowpages.ca/bus/British-Columbia/Abbotsford/VIP-Mazda/2606030.html?what=VIP+mazda&where=Canada&cli=1,4&le=15eec33e1e|150e0");
                    //testUrlList.Add("http://www.yellowpages.ca/bus/British-Columbia/Vancouver/Weissach-Performance/3565196.html?what=weissach+perfromance&where=Canada&cli=1,2&le=0");
                    //testUrlList.Add("http://www.yellowpages.ca/bus/British-Columbia/Maple-Ridge/West-Coast-Ford-Lincoln/7561688.html?what=west+coast+ford+lincoln&where=Canada&cli=1,5&le=15eec33e1e|16a7897d1e|15faaf001e|1665f8d21e|13ce1ed3a1e&dirs=");
                    //testUrlList.Add("http://www.yellowpages.ca/bus/British-Columbia/Pitt-Meadows/West-Coast-Kia/8025078.html?what=west+coast+kia&where=Canada&cli=1,4&le=15eec34148|15eec33e1e|16a7898048|13ce1ed3d48|15faaf0348|1665f8d548|16a7897d1e|13ce1ed3a1e|15faaf001e|1665f8d21e");
                    //testUrlList.Add("http://www.yellowpages.ca/bus/British-Columbia/Pitt-Meadows/West-Coast-Mazda/6696382.html?what=west+coast+mazda&where=Canada&cli=1,6&le=15eec33fc6");
                    //testUrlList.Add("http://www.yellowpages.ca/bus/British-Columbia/Pitt-Meadows/West-Coast-Nissan/6696384.html?what=west+coast+nissan&where=Canada&cli=1,4&le=15eec33ee0");
                    //testUrlList.Add("http://www.yellowpages.ca/bus/British-Columbia/Pitt-Meadows/West-Coast-Toyota/2614493.html?what=west+coast+toyota&where=Canada&cli=1,8&le=1665f8d554|1665f8d40e|1665f8d3c6|1665f8d3ae|1665f91981|1665f8d426|1665f8d524|1665f8d2e0|15354|1520e");
                    //testUrlList.Add("http://www.yellowpages.ca/bus/British-Columbia/Abbotsford/Windmill-Auto-Sales-Detailing/3543866.html?what=windmill+auto+sales&where=Canada&cli=1,6&le=16013d779e");


                    //foreach (var item in testUrlList)
                    //{
                    //    var dto = new ReviewUrlDTO();
                    //    dto.HtmlURL = item;

                    //    var yellowPagesResult = connection.Scrape(dto, true);
                    //    Debug.WriteLine("{0} Rating, {1} ReviewCount , {2} Details, {3} Errors, {4} URL", yellowPagesResult.SalesAverageRating, 
                    //                                                                                        yellowPagesResult.SalesReviewCount, 
                    //                                                                                        yellowPagesResult.ReviewDetails.Count,
                    //                                                                                        yellowPagesResult.FailureReasons.Count, item);
                    //}

                    //Debug.WriteLine("");
                }

                #endregion

                #region Edmunds #7

                if (testNumber == 7)
                {

                    SIReviewConnection connection7 = ProviderFactory.Reviews.GetReviewConnection(SI.DTO.ReviewSourceEnum.Edmunds);
                    string myXMLfile = @"C:\Dev2\SI\SI_PLATFORM\ConnectorTest\URLs.xml";
                    DataSet ds = new DataSet();

                    ds.ReadXml(myXMLfile);


                    int iCounter = 1;
                    SDReviewEntities dbContext = new SDReviewEntities();

                    foreach (DataRow row in ds.Tables[0].Rows)
                    {
                        ReviewUrlDTO dto = new ReviewUrlDTO();

                        dto.HtmlURL = row[2].ToString();
                        dto.AccountID = Convert.ToInt64(row[0].ToString());
                        dto.ExternalID = row[3].ToString();

                        int DealerLocationID = Convert.ToInt32(row[1].ToString());

                        var googleResult = connection7.Scrape(dto, true);
                        Debug.WriteLine(iCounter + ". {0} Rating, {1} ReviewCount , {2} Details, {3} URL", googleResult.SalesAverageRating, googleResult.SalesReviewCount, googleResult.ReviewDetails.Count, dto.HtmlURL);
                        Console.WriteLine(iCounter + ". {0} Rating, {1} ReviewCount , {2} Details, {3} URL", googleResult.SalesAverageRating, googleResult.SalesReviewCount, googleResult.ReviewDetails.Count, dto.HtmlURL);

                        dbContext.spReviewSummarySDSIUpdate("Edmunds", Convert.ToInt32(DealerLocationID), Convert.ToInt32(dto.AccountID), dto.HtmlURL, Convert.ToInt32(googleResult.SalesReviewCount), Convert.ToDouble(googleResult.SalesAverageRating), googleResult.ReviewDetails.Count);

                        iCounter++;
                    }

                    ////http://www.edmunds.com/dealerships/Illinois/Lombard/LombardToyotaScion/sales.1.html - Ratings & Reviews
                    ////http://www.edmunds.com/dealerships/Illinois/VillaPark/HaggertyBuickGMC/sales.1.html - No Ratings or Reviews (Older Reviews Exist)
                    ////http://www.edmunds.com/dealerships/Illinois/GlenEllyn/EnterpriseCarSalesGlenEllyn/sales.1.html - No Ratings or Review (No Older Reviews Exist)


                    //testUrlList.Add("http://www.edmunds.com/dealerships/Washington/Shoreline/CarterSubaru/sales.1.html");
                    //testUrlList.Add("http://www.edmunds.com/dealerships/Washington/Seattle/CarterSubaruBallard/sales.1.html");
                    //testUrlList.Add("http://www.edmunds.com/dealerships/Washington/Seattle/CarterVolkswagen/sales.1.html");
                    //testUrlList.Add("http://www.edmunds.com/dealerships/California/SanDiego/KearnyMesaSubaru/sales.1.html");
                    //testUrlList.Add("http://www.edmunds.com/dealerships/Utah/SaltLakeCity/NateWadeSubaru/sales.1.html");
                    //testUrlList.Add("http://www.edmunds.com/dealerships/California/Fullerton/RenickSubaru/sales.1.html");
                    //testUrlList.Add("http://www.edmunds.com/dealerships/Arizona/Tucson/TucsonSubaru/sales.1.html");

                    //SIReviewConnection connection = ProviderFactory.Reviews.GetReviewConnection(SI.DTO.ReviewSourceEnum.Edmunds);

                    //foreach (var item in testUrlList)
                    //{
                    //    var dto = new ReviewUrlDTO();
                    //    dto.HtmlURL = item;

                    //    var edmundsResult = connection.Scrape(dto, true);
                    //    Debug.WriteLine("{0} Rating, {1} ReviewCount , {2} Details, {3} Errors, {4} URL", edmundsResult.SalesAverageRating, edmundsResult.SalesReviewCount, edmundsResult.ReviewDetails.Count, edmundsResult.FailureReasons.Count, item);
                    //}

                    //Debug.WriteLine("");
                }

                #endregion

                #region carsdotCom #8

                if (testNumber == 8)
                {

                    SIReviewConnection connection8 = ProviderFactory.Reviews.GetReviewConnection(SI.DTO.ReviewSourceEnum.CarsDotCom);

                    ReviewUrlDTO dto1 = new ReviewUrlDTO();

                    dto1.HtmlURL = "http://www.cars.com/dealers/2355/fair-oaks-chrysler-jeep-dodge-ram/reviews/";
                    var googleResult1 = connection8.Scrape(dto1, true);
                    
                    string myXMLfile = @"C:\Users\Sanju\Downloads\XMLs\URLs800.xml";
                    DataSet ds = new DataSet();

                    ds.ReadXml(myXMLfile);
                

                    int iCounter = 1;
                    SDReviewEntities dbContext = new SDReviewEntities();

                    foreach (DataRow row in ds.Tables[0].Rows)
                    {
                        ReviewUrlDTO dto = new ReviewUrlDTO();

                        dto.HtmlURL = row[2].ToString();
                        dto.AccountID = Convert.ToInt64(row[0].ToString());
                        dto.ExternalID = row[3].ToString();

                        int DealerLocationID = Convert.ToInt32(row[1].ToString());

                        var googleResult = connection8.Scrape(dto, true);
                        Debug.WriteLine(iCounter + ". {0} Rating, {1} ReviewCount , {2} Details, {3} URL", googleResult.SalesAverageRating, googleResult.SalesReviewCount, googleResult.ReviewDetails.Count, dto.HtmlURL);
                        Console.WriteLine(iCounter + ". {0} Rating, {1} ReviewCount , {2} Details, {3} URL", googleResult.SalesAverageRating, googleResult.SalesReviewCount, googleResult.ReviewDetails.Count, dto.HtmlURL);

                        dbContext.spReviewSummarySDSIUpdate("CarsDotCom", Convert.ToInt32(DealerLocationID), Convert.ToInt32(dto.AccountID), dto.HtmlURL, Convert.ToInt32(googleResult.SalesReviewCount), Convert.ToDouble(googleResult.SalesAverageRating), googleResult.ReviewDetails.Count);

                        iCounter++;
                    }



                    ////http://www.cars.com/dealers/153978/villa-marin-buick-gmc/reviews/ 4
                    ////http://www.cars.com/dealers/154140/paul-conte-cadillac/reviews/ 7
                    ////http://www.cars.com/dealers/98445/englewood-auto-group/reviews/  48 have comments
                    ////http://www.cars.com/dealers/181827/classic-chevrolet-mentor/reviews/ good
                    ////http://www.cars.com/dealers/14281/reviews.action?dlId=14281
                    ////http://www.cars.com/dealers/197615/reviews.action?dlId=197615
                    ////http://www.cars.com/dealers/14230/ron-marhofer-chevrolet/reviews/ good
                    ////http://www.cars.com/dealers/181817/pat-obrien-chevrolet-east/reviews/ 1
                    ////http://www.cars.com/dealers/154883/pat-obrien-chevrolet-south/reviews/ 1
                    ////http://www.cars.com/dealers/14130/ganley-chevrolet/reviews/ - Has Reviews & Rating
                    ////http://www.cars.com/dealers/211629/z-frank-chevrolet-kia/reviews/ - Has no Reviews or Rating
                    //dto.HtmlURL = "http://www.cars.com/dealers/98445/englewood-auto-group/reviews/";


                    //SIReviewConnection connection = ProviderFactory.Reviews.GetReviewConnection(SI.DTO.ReviewSourceEnum.CarsDotCom);

                    //testUrlList.Add("http://www.cars.com/dealers/26584/carter-subaru-shoreline/reviews/");
                    //testUrlList.Add("http://www.cars.com/dealers/2269373/carter-subaru-ballard/");
                    //testUrlList.Add("http://www.cars.com/dealers/2427812/carter-volkswagen/");
                    //testUrlList.Add("http://www.cars.com/dealers/176456/kearny-mesa-subaru/");
                    //testUrlList.Add("http://www.cars.com/dealers/25118/renick-cadillac-subaru/?count=2500");
                    //testUrlList.Add("http://www.cars.com/dealers/148367/tucson-subaru/?count=2500");

                    //foreach (var item in testUrlList)
                    //{
                    //    var dto = new ReviewUrlDTO();
                    //    dto.HtmlURL = item;

                    //    var carsDotComPagesResult = connection.Scrape(dto, true);
                    //    Debug.WriteLine("{0} Rating, {1} ReviewCount , {2} Details, {3} Errors, {4} URL", carsDotComPagesResult.SalesAverageRating,
                    //                                                                                        carsDotComPagesResult.SalesReviewCount,
                    //                                                                                        carsDotComPagesResult.ReviewDetails.Count,
                    //                                                                                        carsDotComPagesResult.FailureReasons.Count, 
                    //                                                                                        dto.HtmlURL);
                    //}

                    //Debug.WriteLine("");

                }

                #endregion

                #region CitySearch #9

                if (testNumber == 9)
                {
                    SIReviewConnection connection9 = ProviderFactory.Reviews.GetReviewConnection(SI.DTO.ReviewSourceEnum.CitySearch);
                    string myXMLfile = @"C:\Users\Sanju\Downloads\XMLs\URLs900.xml";
                    DataSet ds = new DataSet();

                    ds.ReadXml(myXMLfile);


                    int iCounter = 1;
                    SDReviewEntities dbContext = new SDReviewEntities();

                    foreach (DataRow row in ds.Tables[0].Rows)
                    {
                        ReviewUrlDTO dto = new ReviewUrlDTO();

                        dto.HtmlURL = row[2].ToString();
                        dto.AccountID = Convert.ToInt64(row[0].ToString());
                        dto.ExternalID = row[3].ToString();

                        int DealerLocationID = Convert.ToInt32(row[1].ToString());

                        var googleResult = connection9.Scrape(dto, true);
                        Debug.WriteLine(iCounter + ". {0} Rating, {1} ReviewCount , {2} Details, {3} URL", googleResult.SalesAverageRating, googleResult.SalesReviewCount, googleResult.ReviewDetails.Count, dto.HtmlURL);
                        Console.WriteLine(iCounter + ". {0} Rating, {1} ReviewCount , {2} Details, {3} URL", googleResult.SalesAverageRating, googleResult.SalesReviewCount, googleResult.ReviewDetails.Count, dto.HtmlURL);

                        dbContext.spReviewSummarySDSIUpdate("CitySearch", Convert.ToInt32(DealerLocationID), Convert.ToInt32(dto.AccountID), dto.HtmlURL, Convert.ToInt32(googleResult.SalesReviewCount), Convert.ToDouble(googleResult.SalesAverageRating), googleResult.ReviewDetails.Count);

                        iCounter++;
                    }

                    //SIReviewConnection connection = ProviderFactory.Reviews.GetReviewConnection(SI.DTO.ReviewSourceEnum.CitySearch);
                    //ReviewUrlDTO dto = new ReviewUrlDTO();
                    ////http://dallas.citysearch.com/profile/34507178/dallas_tx/bonnie_clyde_s_cb_stereo.html 
                    ////http://kansascity.citysearch.com/profile/34031659/olathe_ks/sleep_inn.html
                    ////http://orlando.citysearch.com/profile/2371038/winter_park_fl/gary_lambert_salon_spa.html
                    ////http://cleveland.citysearch.com/profile/7957027/mentor_oh/classic_chevrolet.html
                    ////http://akron.citysearch.com/profile/8058319/akron_oh/doug_chevrolet.html
                    ////http://cleveland.citysearch.com/profile/7982350/cleveland_oh/ganley_chevrolet_inc.html
                    ////http://akron.citysearch.com/profile/716102860/aurora_oh/ganley_chevrolet_of_aurora_llc.html
                    ////http://akron.citysearch.com/profile/8039578/stow_oh/marhofer_ron.html
                    ////http://cleveland.citysearch.com/profile/7966535/willoughby_hills_oh/pat_o_brien_chevrolet.html No Review No Rating
                    ////http://chicago.citysearch.com/profile/39983382/villa_park_il/castle_chevrolet.html Castle Reviews & Rating
                    ////http://cleveland.citysearch.com/profile/8044520/medina_oh/pat_o_brien_chevrolet_medina.html
                    //dto.HtmlURL = "http://charlotte.citysearch.com/profile/739829540/charlotte_nc/hendrick_acura.html";
                    //strURL = dto.HtmlURL;
                    //ReviewDownloadResult result = connection.Scrape(dto, true);

                    //testresult = result;
                }

                #endregion

                #region JudysBook #10

                if (testNumber == 10)
                {
                    SIReviewConnection connection7 = ProviderFactory.Reviews.GetReviewConnection(SI.DTO.ReviewSourceEnum.JudysBook);
                    ReviewUrlDTO dto1 = new ReviewUrlDTO();

                    dto1.HtmlURL = "http://www.judysbook.com/cities/kalamazoo/jaguar-dealers/27431621/metro_toyota.htm";
                    var googleResul1t = connection7.Scrape(dto1, true);


                    string myXMLfile = @"C:\Users\Sanju\Downloads\XMLs\URLs1000.xml";
                    DataSet ds = new DataSet();

                    ds.ReadXml(myXMLfile);


                    int iCounter = 1;
                    SDReviewEntities dbContext = new SDReviewEntities();

                    foreach (DataRow row in ds.Tables[0].Rows)
                    {
                        ReviewUrlDTO dto = new ReviewUrlDTO();

                        dto.HtmlURL = row[2].ToString();
                        dto.AccountID = Convert.ToInt64(row[0].ToString());
                        dto.ExternalID = row[3].ToString();

                        int DealerLocationID = Convert.ToInt32(row[1].ToString());

                        var googleResult = connection7.Scrape(dto, true);
                        Debug.WriteLine(iCounter + ". {0} Rating, {1} ReviewCount , {2} Details, {3} URL", googleResult.SalesAverageRating, googleResult.SalesReviewCount, googleResult.ReviewDetails.Count, dto.HtmlURL);
                        Console.WriteLine(iCounter + ". {0} Rating, {1} ReviewCount , {2} Details, {3} URL", googleResult.SalesAverageRating, googleResult.SalesReviewCount, googleResult.ReviewDetails.Count, dto.HtmlURL);

                        dbContext.spReviewSummarySDSIUpdate("JudysBook", Convert.ToInt32(DealerLocationID), Convert.ToInt32(dto.AccountID), dto.HtmlURL, Convert.ToInt32(googleResult.SalesReviewCount), Convert.ToDouble(googleResult.SalesAverageRating), googleResult.ReviewDetails.Count);

                        iCounter++;
                    }

                    //SIReviewConnection connection = ProviderFactory.Reviews.GetReviewConnection(SI.DTO.ReviewSourceEnum.JudysBook);
                    //ReviewUrlDTO dto = new ReviewUrlDTO();
                    ////http://www.judysbook.com/Pink-Door--The-Restaurants-seattle-r4768.htm  //>100 Reviews & Rating
                    ////http://www.judysbook.com/Tulio-Ristorante-Restaurants-seattle-r23550603.htm  //>40 Reviews & Rating
                    ////http://www.judysbook.com/Ganley-Chevrolet-Inc-Auto-Parts-cleveland-oh-r28222242.htm
                    ////http://www.judysbook.com/GANLEY-CHEVROLET-OF-AURORA-LLC-Uncategorized-aurora-oh-r37612467.htm//(No Review No Rating)
                    ////http://www.judysbook.com/Marhofer-Ron-Chevrolet-Dealers-stow-oh-r32701319.htm//(No Review No Rating)
                    ////http://www.judysbook.com/Serpentini-Chevrolet-Chevrolet-Dealers-tallmadge-r37858787.htm//(No Review No Rating)
                    ////http://www.judysbook.com/Spitzer-Chevrolet-Amherst-Chevrolet-Dealers-amherst-oh-r29975427.htm //(No Review No Rating)
                    ////http://www.judysbook.com/Spitzer-Chevrolet-North-Canton-Chevrolet-Dealers-canton-oh-r29956890.htm //(No Review No Rating)
                    ////http://www.judysbook.com/SPITZER-CHEVROLET-Chevrolet-Dealers-northfield-oh-r13297407.htm          // 1-reviw
                    ////http://www.judysbook.com/VANDEVERE-Uncategorized-akron-oh-r32567416.htm           //(No Review No Rating)
                    ////http://www.judysbook.com/Castle-Chevrolet-Auto-Parts-villapark-il-r31025482.htm 
                    //dto.HtmlURL = "http://www.judysbook.com/VANDEVERE-Uncategorized-akron-oh-r32567416.htm";
                    //strURL = dto.HtmlURL;
                    //ReviewDownloadResult result = connection.Scrape(dto, true);

                    //testresult = result;
                }

                #endregion

                #region InsiderPages#11

                if (testNumber == 11)
                {
                    SIReviewConnection connection = ProviderFactory.Reviews.GetReviewConnection(SI.DTO.ReviewSourceEnum.InsiderPages);
                    ReviewUrlDTO dto = new ReviewUrlDTO();
                    //http://www.insiderpages.com/b/3713877919/castle-chevrolet-of-villa-park-villa-park?sort_reviews=recency
                    //http://www.insiderpages.com/b/3714067692/midtown-scion-chicago no reviews
                    dto.HtmlURL = "http://www.insiderpages.com/b/3713877919/castle-chevrolet-of-villa-park-villa-park?sort_reviews=recency";
                    strURL = dto.HtmlURL;
                    ReviewDownloadResult result = connection.Scrape(dto, true);

                    testresult = result;
                }

                #endregion

                #region DealerRater Canada#12


                if (testNumber == 12)
                {
                    //SIReviewConnection connection = ProviderFactory.Reviews.GetReviewConnection(SI.DTO.ReviewSourceEnum.DealerRaterCanada);
                    //ReviewUrlDTO dto = new ReviewUrlDTO();
                    //dto.HtmlURL = "http://www.dealerrater.ca/dealer/Chasse-Toyota-review-16505/";
                    //dto.ExternalID = "16505";
                    //strURL = dto.HtmlURL;
                    //ReviewDownloadResult result = connection.Scrape(dto, true);

                    //testresult = result;

                    testUrlList.Add("http://www.dealerrater.ca/dealer/Bannister-GM-Vernon-review-31757/");

                    testUrlList.Add("http://www.dealerrater.ca/dealer/Auto-West-BMW-review-18778/");

                    testUrlList.Add("http://www.dealerrater.ca/dealer/Auto-West-Infiniti-review-31665/");
                    testUrlList.Add("http://www.dealerrater.ca/dealer/5th-and-Carney-Subaru-review-32567/");
                    testUrlList.Add("http://www.dealerrater.ca/dealer/Alberni-Chrysler-review-31556/");
                    testUrlList.Add("http://www.dealerrater.ca/dealer/Alberni-Toyota-review-32484/");
                    testUrlList.Add("http://www.dealerrater.ca/dealer/Applewood-Langley-Kia-review-31597/");
                    testUrlList.Add("http://www.dealerrater.ca/dealer/Applewood-Kia-review-31598/");
                    
                    
                    testUrlList.Add("http://www.dealerrater.ca/dealer/Bow-Mel-Chrylser-Ltd-review-31912/");
                    testUrlList.Add("http://www.dealerrater.ca/dealer/Brian-Jessel-BMW-review-17691/");
                    testUrlList.Add("http://www.dealerrater.ca/dealer/Courtenay-Toyota-review-32243/");
                    testUrlList.Add("http://www.dealerrater.ca/dealer/Ferrari-Maserati-of-Vancouver-review-32562/");
                    testUrlList.Add("http://www.dealerrater.ca/dealer/Cam-Clark-Ford-Lincoln-review-31995/");
                    testUrlList.Add("http://www.dealerrater.ca/dealer/Carter-GM-review-32078/");
                    testUrlList.Add("http://www.dealerrater.ca/dealer/Carter-Chevrolet-review-32076/");
                    testUrlList.Add("http://www.dealerrater.ca/dealer/Carter-Dodge-Chrysler-Ltd-review-32077/");
                    testUrlList.Add("http://www.dealerrater.ca/dealer/Carter-Honda-review-32080/");
                    testUrlList.Add("http://www.dealerrater.ca/dealer/Castlegar-Toyota-review-32084/");
                    testUrlList.Add("http://www.dealerrater.ca/dealer/Colwood-Car-Mart-review-42397/");
                    testUrlList.Add("http://www.dealerrater.ca/dealer/Destination-Chrysler-review-32353/");
                    testUrlList.Add("http://www.dealerrater.ca/dealer/Destination-Hyundai-review-40616/");
                    testUrlList.Add("http://www.dealerrater.ca/dealer/Destination-Mazda-Vancouver-review-41775/");
                    testUrlList.Add("http://www.dealerrater.ca/dealer/Destination-Toyota-Burnaby-review-32355/");
                    testUrlList.Add("http://www.dealerrater.ca/dealer/Westwood-Honda-review-34894/");
                    testUrlList.Add("http://www.dealerrater.ca/dealer/Don-Folk-Chevrolet-Kelowna-review-32392/");
                    testUrlList.Add("http://www.dealerrater.ca/dealer/Dueck-GM-Downtown-review-32446/");
                    testUrlList.Add("http://www.dealerrater.ca/dealer/Dueck-GM-on-Marine-review-32447/");
                    testUrlList.Add("http://www.dealerrater.ca/dealer/Dueck-Richmond-review-32448/");
                    testUrlList.Add("http://www.dealerrater.ca/dealer/Eagle-Ridge-GM-service-32470/");
                    testUrlList.Add("http://www.dealerrater.ca/dealer/Glacier-Honda-review-32714/");
                    testUrlList.Add("http://www.dealerrater.ca/dealer/White-Rock-Chrysler-review-32798/");
                    testUrlList.Add("http://www.dealerrater.ca/dealer/Harris-Kia-review-39005/");
                    testUrlList.Add("http://www.dealerrater.ca/dealer/Harris-Mazda-review-32827/");
                    testUrlList.Add("http://www.dealerrater.ca/dealer/Harris-Mitsubishi-review-38536/");
                    testUrlList.Add("http://www.dealerrater.ca/dealer/Harris-Nissan-review-31765/");
                    testUrlList.Add("http://www.dealerrater.ca/dealer/Harris-Oceanside-Chevrolet-Buick-GMC-review-38548/");
                    testUrlList.Add("http://www.dealerrater.ca/dealer/Harris-Victoria-Chrysler-Dodge-Jeep-Ram-review-32515/");
                    testUrlList.Add("http://www.dealerrater.ca/dealer/Inland-Auto-Centre-review-32979/");
                    testUrlList.Add("http://www.dealerrater.ca/dealer/Metrotown-Mazda-review-33592/");
                    testUrlList.Add("http://www.dealerrater.ca/dealer/Kalawsky-Chevrolet-Buick-GMC-review-33080/");
                    testUrlList.Add("http://www.dealerrater.ca/dealer/Kamloops-Dodge-Chrysler-Jeep-Ltd-review-33500/");
                    testUrlList.Add("http://www.dealerrater.ca/dealer/Kamloops-Hyundai-review-33083/");
                    testUrlList.Add("http://www.dealerrater.ca/dealer/Kelowna-Mercedes-Benz-review-33104/");
                    testUrlList.Add("http://www.dealerrater.ca/dealer/Key-West-Ford-review-33124/");
                    testUrlList.Add("http://www.dealerrater.ca/dealer/Kingsway-Honda-review-33192/");
                    testUrlList.Add("http://www.dealerrater.ca/dealer/Langley-Hyundai-review-33262/");
                    testUrlList.Add("http://www.dealerrater.ca/dealer/Magnuson-Ford-Sales-Ltd-review-33409/");
                    testUrlList.Add("http://www.dealerrater.ca/dealer/Marv-Jones-Honda-review-33456/");
                    testUrlList.Add("http://www.dealerrater.ca/dealer/Mertin-Chevrolet-Cadillac-Buick-GMC-Ltd-review-33574/");
                    testUrlList.Add("http://www.dealerrater.ca/dealer/Mertin-Hyundai-review-33575/");
                    testUrlList.Add("http://www.dealerrater.ca/dealer/Mertin-Nissan-review-33576/");
                    testUrlList.Add("http://www.dealerrater.ca/dealer/Metro-Ford-review-33588/");
                    testUrlList.Add("http://www.dealerrater.ca/dealer/Mini-Richmond-review-392/");
                    testUrlList.Add("http://www.dealerrater.ca/dealer/Morrey-Infiniti-Nissan-of-Coquitlam-Ltd-review-33684/");
                    testUrlList.Add("http://www.dealerrater.ca/dealer/Morrey-Mazda-of-the-North-Shore-review-33681/");
                    testUrlList.Add("http://www.dealerrater.ca/dealer/Morrey-Nissan-review-33683/");
                    testUrlList.Add("http://www.dealerrater.ca/dealer/MSA-Ford-Sales-review-33385/");
                    testUrlList.Add("http://www.dealerrater.ca/dealer/White-Rock-Hyundai-review-42410/");
                    testUrlList.Add("http://www.dealerrater.ca/dealer/Sullivan-Motor-Products-Ltd-review-34510/");
                    testUrlList.Add("http://www.dealerrater.ca/dealer/Nanaimo-Chrysler-Ltd-review-33717/");
                    testUrlList.Add("http://www.dealerrater.ca/dealer/Nelson-Ford-review-33724/");
                    testUrlList.Add("http://www.dealerrater.ca/dealer/North-Shore-Mitsubishi-review-33768/");
                    testUrlList.Add("http://www.dealerrater.ca/dealer/Ocean-Park-Ford-Sales-Ltd-review-33834/");
                    testUrlList.Add("http://www.dealerrater.ca/dealer/Pacific-Honda-review-33886/");
                    testUrlList.Add("http://www.dealerrater.ca/dealer/Pacific-Mazda-review-33887/");
                    testUrlList.Add("http://www.dealerrater.ca/dealer/Penticton-Honda-review-33952/");
                    testUrlList.Add("http://www.dealerrater.ca/dealer/Pioneer-Chrysler-review-34010/");

                    SIReviewConnection connection = ProviderFactory.Reviews.GetReviewConnection(SI.DTO.ReviewSourceEnum.DealerRaterCanada);

                    foreach (var item in testUrlList)
                    {
                        var dto = new ReviewUrlDTO();
                        dto.HtmlURL = item;

                        var dealerRaterCanadaResult = connection.Scrape(dto, true);
                        Debug.WriteLine("{0} Rating, {1} ReviewCount , {2} Details, {3} Errors, {4} URL", dealerRaterCanadaResult.SalesAverageRating,
                                                                                                            dealerRaterCanadaResult.SalesReviewCount,
                                                                                                            dealerRaterCanadaResult.ReviewDetails.Count,
                                                                                                            dealerRaterCanadaResult.FailureReasons.Count,
                                                                                                            dto.HtmlURL);
                    }

                    Debug.WriteLine("");
                }

                #endregion

                #region Yelp Canada#13


                if (testNumber == 13)
                {
                    //SIReviewConnection connection = ProviderFactory.Reviews.GetReviewConnection(SI.DTO.ReviewSourceEnum.YelpCanada);
                    //ReviewUrlDTO dto = new ReviewUrlDTO();
                    //dto.HtmlURL = "http://www.yelp.com/biz/chasse-toyota-montreal";
                    //strURL = dto.HtmlURL;
                    //ReviewDownloadResult result = connection.Scrape(dto, true);

                    //testresult = result;

                    testUrlList.Add("http://www.yelp.com/biz/chasse-toyota-montreal");
                    testUrlList.Add("http://www.yelp.ca/biz/auto-west-infiniti-richmond");
                    testUrlList.Add("http://www.yelp.ca/biz/auto-fleet-services-richmond");
                    testUrlList.Add("http://www.yelp.ca/biz/applewood-kia-langley-langley");
                    testUrlList.Add("http://www.yelp.ca/biz/applewood-kia-surrey-surrey");
                    testUrlList.Add("http://www.yelp.ca/biz/auto-west-bmw-richmond");
                    testUrlList.Add("http://www.yelp.ca/biz/bowmel-chrysler-duncan#query:bow-mel%20chrylser");
                    testUrlList.Add("http://www.yelp.ca/biz/brian-jessel-bmw-vancouver");
                    testUrlList.Add("http://www.yelp.ca/biz/ferrari-maserati-of-vancouver-vancouver-2");
                    testUrlList.Add("http://www.yelp.ca/biz/cam-clark-ford-lincoln-north-vancouver");
                    testUrlList.Add("http://www.yelp.ca/biz/carter-gm-north-shore-north-vancouver");
                    testUrlList.Add("http://www.yelp.ca/biz/carter-dodge-chrysler-burnaby");
                    testUrlList.Add("http://www.yelp.ca/biz/carter-honda-new-and-used-car-sales-vancouver-2");
                    testUrlList.Add("http://www.yelp.ca/biz/destination-chrysler-jeep-dodge-ram-north-vancouver");
                    testUrlList.Add("http://www.yelp.ca/biz/destination-hyundai-vancouver-vancouver");
                    testUrlList.Add("http://www.yelp.ca/biz/destination-mazda-vancouver-2");
                    testUrlList.Add("http://www.yelp.ca/biz/destination-toyota-burnaby-burnaby");
                    testUrlList.Add("http://www.yelp.ca/biz/westwood-honda-port-moody");
                    testUrlList.Add("http://www.yelp.ca/biz/dueck-downtown-vancouver");
                    testUrlList.Add("http://www.yelp.ca/biz/dueck-on-marine-vancouver");
                    testUrlList.Add("http://www.yelp.ca/biz/dueck-richmond-richmond");
                    testUrlList.Add("http://www.yelp.ca/biz/eagle-ridge-chevrolet-buick-gmc-ltd-coquitlam");
                    testUrlList.Add("http://www.yelp.ca/biz/haleys-white-rock-dodge-chrysler-jeep-surrey");
                    testUrlList.Add("http://www.yelp.ca/biz/metrotown-mazda-burnaby");
                    testUrlList.Add("http://www.yelp.ca/biz/key-west-ford-sales-new-westminster");
                    testUrlList.Add("http://www.yelp.ca/biz/kingsway-honda-vancouver-vancouver");
                    testUrlList.Add("http://www.yelp.ca/biz/lamborghini-vancouver-vancouver");
                    testUrlList.Add("http://www.yelp.ca/biz/magnuson-ford-abbotsford");
                    testUrlList.Add("http://www.yelp.ca/biz/marv-jones-honda-maple-ridge");
                    testUrlList.Add("http://www.yelp.ca/biz/metro-motors-ltd-port-coquitlam");
                    testUrlList.Add("http://www.yelp.ca/biz/mini-richmond-richmond-2");
                    testUrlList.Add("http://www.yelp.ca/biz/morrey-infiniti-nissan-of-coquitlam-port-coquitlam-2");
                    testUrlList.Add("http://www.yelp.ca/biz/morrey-mazda-of-the-northshore-north-vancouver");
                    testUrlList.Add("http://www.yelp.ca/biz/morrey-nissan-of-burnaby-burnaby-2");
                    testUrlList.Add("http://www.yelp.ca/biz/msa-ford-sales-abbotsford");
                    testUrlList.Add("http://www.yelp.ca/biz/pacific-auto-source-ltd-surrey");
                    testUrlList.Add("http://www.yelp.ca/biz/pacific-honda-north-vancouver");
                    testUrlList.Add("http://www.yelp.ca/biz/pioneer-chrysler-jeep-mission");
                    testUrlList.Add("http://www.yelp.ca/biz/richmond-acura-richmond");
                    testUrlList.Add("http://www.yelp.ca/biz/honda-richmond-richmond");
                    testUrlList.Add("http://www.yelp.ca/biz/signature-mazda-richmond");
                    testUrlList.Add("http://www.yelp.ca/biz/surrey-honda-surrey");
                    testUrlList.Add("http://www.yelp.ca/biz/original-applewood-motors-langley");
                    testUrlList.Add("http://www.yelp.ca/biz/the-urban-garage-west-vancouver");
                    testUrlList.Add("http://www.yelp.ca/biz/trinity-auto-centre-north-vancouver");
                    testUrlList.Add("http://www.yelp.ca/biz/vip-mazda-abbotsford");
                    testUrlList.Add("http://www.yelp.ca/biz/weissach-performance-vancouver");
                    testUrlList.Add("http://www.yelp.ca/biz/west-coast-ford-lincoln-maple-ridge");
                    testUrlList.Add("http://www.yelp.ca/biz/west-coast-kia-pitt-meadows");
                    testUrlList.Add("http://www.yelp.ca/biz/west-coast-mazda-pitt-meadows");
                    testUrlList.Add("http://www.yelp.ca/biz/west-coast-nissan-pitt-meadows");
                    testUrlList.Add("http://www.yelp.ca/biz/west-coast-toyota-pitt-meadows");
                    testUrlList.Add("http://www.yelp.ca/biz/windmill-auto-sales-and-detailing-abbotsford");


                    SIReviewConnection connection = ProviderFactory.Reviews.GetReviewConnection(SI.DTO.ReviewSourceEnum.YelpCanada);

                    foreach (var item in testUrlList)
                    {
                        var dto = new ReviewUrlDTO();
                        dto.HtmlURL = item;

                        var yelpCanadaResult = connection.Scrape(dto, true);
                        Debug.WriteLine("{0} Rating, {1} ReviewCount , {2} Details, {3} Errors, {4} URL", yelpCanadaResult.SalesAverageRating,
                                                                                                            yelpCanadaResult.SalesReviewCount,
                                                                                                            yelpCanadaResult.ReviewDetails.Count,
                                                                                                            yelpCanadaResult.FailureReasons.Count,
                                                                                                            dto.HtmlURL);
                    }

                    Debug.WriteLine("");
                }

                #endregion

                #region CitySearch Canada #14

                if (testNumber == 14)
                {
                    SIReviewConnection connection = ProviderFactory.Reviews.GetReviewConnection(SI.DTO.ReviewSourceEnum.CitySearchCanada);
                    ReviewUrlDTO dto = new ReviewUrlDTO();
                    //http://www.yellowpages.com/oak-brook-il/mip/sears-auto-center-11302446?lid=178684725
                    //http://www.yellowpages.com/new-york-ny/mip/the-sports-center-at-chelsea-piers-21722011?lid=173703152
                    //http://www.yellowpages.com/omaha-ne/mip/gregg-young-chevrolet-460353502?lid=460353502
                    //http://www.yellowpages.com/owings-mills-md/mip/len-stoler-inc-455318001?lid=455318001
                    //http://www.yellowpages.com/westminster-md/mip/len-stoler-chevrolet-461440271
                    //http://www.yellowpages.com/san-antonio-tx/mip/alamo-toyota-12835083?lid=182825480 
                    //http://www.yellowpages.com/charleston-sc/mip/hendrick-honda-of-charleston-474816769?lid=474816769
                    //http://www.yellowpages.com/charleston-sc/mip/hendrick-hyundai-455410236?lid=455410236
                    //http://www.yellowpages.com/nationwide/mip/lexus-of-charleston-462212578?lid=462212578 - No Reviews - No Rating

                    //http://www.yellowpages.com/villa-park-il/mip/castle-chevrolet-1163184?lid=185221277 - Rating & Reviews
                    //http://www.yellowpages.com/hodgkins-il/mip/advantage-chevrolet-21864451?lid=21864451 Has response

                    //dto.HtmlURL = "http://www.yellowpages.ca/bus/Ontario/Barrie/Jackson-s-Toyota-Scion/2338445.html";
                    dto.HtmlURL = "http://www.yellowpages.com/hodgkins-il/mip/advantage-chevrolet-21864451?lid=21864451";
                    strURL = dto.HtmlURL;
                    ReviewDownloadResult result = connection.Scrape(dto, true);

                    testresult = result;
                }

                #endregion

                #endregion

                #region Old Code
                //#region Edmunds #1

                //if (testNumber == 1)
                //{

                //    SIEdmundsReviewXPathParameters edmundsReviewXPathParameters = new SIEdmundsReviewXPathParameters();
                //    edmundsReviewXPathParameters.ReviewCountCollectedSalesParentXPath = @".//*[@id='dealerSaleRating']";
                //    edmundsReviewXPathParameters.RatingCollectedSalesXPath = @".//span[contains(@class,'rating-big')]";
                //    edmundsReviewXPathParameters.ReviewCountCollectedSalesXPath = @".//span[@class='count nodisplay hidden']";
                //    edmundsReviewXPathParameters.ReviewCountCollectedServiceParentXPath = @".//*[@id='dealerServiceRating']";
                //    edmundsReviewXPathParameters.RatingCollectedServiceXPath = @".//span[contains(@class,'rating-big')]";
                //    edmundsReviewXPathParameters.ReviewCountCollectedServiceXPath = @".//span[contains(@class, 'service-review-count')]";
                //    edmundsReviewXPathParameters.ParentnodeReviewDetailsXPath = @"//div[contains(@id, 'sales_review_item_')]";
                //    edmundsReviewXPathParameters.RatingValueXPath = @".//span[@class='rating-big']";
                //    edmundsReviewXPathParameters.ReviewerNameXPath = @"span[3]/span[1]"; //!!WRONG IN APP.CONFIG
                //    edmundsReviewXPathParameters.ReviewTextFullURLXPath = @".//a[@id='default_sales_review_read_full']";
                //    edmundsReviewXPathParameters.ReviewFullTextXPath = @".//p[@class='sales-review-paragraph loose-spacing']";
                //    edmundsReviewXPathParameters.ReviewDateXPath = @".//span[@class='dtreviewed']";
                //    edmundsReviewXPathParameters.SalesExtraInfoXPath = @".//li[contains(@class,'ddp-li-sls-rcmnd')]";
                //    edmundsReviewXPathParameters.ServiceExtraInfoXPath = @".//li[contains(@class,'ddp-li-srv-rcmnd')]"; //!!WRONG IN APP.CONFIG

                //    edmundsReviewXPathParameters.ReviewCommentsXPath = @".//div[@class='comment-content']";
                //    edmundsReviewXPathParameters.ReviewCommentDateXPath = @".//span[@class='comment-date']";

                //    edmundsReviewXPathParameters.ReviewTitleExtraInfoXPath = @"span[2]";
                //    edmundsReviewXPathParameters.ReviewRecommendationExtraInfoXPath = @"span[4]/span";
                //    edmundsReviewXPathParameters.ReviewPurchasedVehicleExtraInfoXPath = @"span[5]/span";
                //    edmundsReviewXPathParameters.ReviewsPerPage = 5;

                //    //**NEW Service XPaths
                //    edmundsReviewXPathParameters.ParentnodeSeviceReviewDetailsXPath = @".//div[@class='hreview service-review-item']";
                //    edmundsReviewXPathParameters.ServiceReviewTextFullURLXPath = @".//a[@id='default_service-review-read-full']";
                //    edmundsReviewXPathParameters.ServiceReviewFullTextXPath = @".//p[@class='service-review-paragraph loose-spacing']";

                //    SIEdmundsReviewConnectionParameters edmundsReviewConnectionParameters = new SIEdmundsReviewConnectionParameters(@"http://api.edmunds.com/v1/api/drrrepository/getdrrbydealerid?dealerid=21903&fmt=json&api_key=w4mdm2h6rejuvkt5fsge3ags", edmundsReviewXPathParameters, @"http://www.edmunds.com/dealerships/Idaho/Blackfoot/20thCenturyChryslerJeepDodgeRam/sales.1.html");

                //    EdmundsReviewsConnection edmundsConnection = new EdmundsReviewsConnection(edmundsReviewConnectionParameters);
                //    edmundsConnection.GetReviewsAPI();
                //    edmundsConnection.GetReviewsHtml();
                //    //edmundsConnection.HtmlRequestUrl = @"http://www.edmunds.com/dealerships/Pennsylvania/WilkesBarre/MotorworldLexus";//sales.1.html";
                //    //edmundsConnection.HtmlRequestUrl = @"http://www.edmunds.com/dealerships/Idaho/Blackfoot/20thCenturyChryslerJeepDodgeRam/sales.1.html";
                //    //edmundsConnection.HtmlRequestUrl = @"http://www.edmunds.com/dealerships/Connecticut/Enfield/ArtioliDodge/";

                //}

                //#endregion

                //#region Cars.Dot.Com #2

                //if (testNumber == 2)
                //{
                //    SICarsDotComReviewXPathParameters siCarsDotComReviewXPathParameters = new SICarsDotComReviewXPathParameters();
                //    siCarsDotComReviewXPathParameters.ParentnodeReviewSummaryXPath = @"//div[@id='dealer-average-ratings']";
                //    siCarsDotComReviewXPathParameters.RatingCollectedXPath = @".//div[@class='rating dealer']//span[@itemprop='ratingValue']";
                //    siCarsDotComReviewXPathParameters.ReviewCountCollectedXPath = @".//meta[@itemprop='reviewCount']";
                //    siCarsDotComReviewXPathParameters.ParentnodeReviewDetailsXPath = @"//div[@itemprop='review']";
                //    siCarsDotComReviewXPathParameters.RatingValueXPath = @".//span[@itemprop='ratingValue']";
                //    siCarsDotComReviewXPathParameters.ReviewTextFullXPath = @".//p[@itemprop='description']";
                //    siCarsDotComReviewXPathParameters.ReviewerNameXPath = @".//span[@class='reviewer']";
                //    siCarsDotComReviewXPathParameters.ReviewDateXPath = @".//span[@itemprop='datePublished']";

                //    siCarsDotComReviewXPathParameters.ParentnodeExtraValueXPath = @".//div[@class='col12']/div";
                //    siCarsDotComReviewXPathParameters.XtraValueWrapperXPath = @".//div[@class='wrapper']";
                //    siCarsDotComReviewXPathParameters.XtraValueNodeXPath = @".//div[@class='value']";
                //    siCarsDotComReviewXPathParameters.XtraKeyNodeXPath = @".//div[@class='detail']";
                //    siCarsDotComReviewXPathParameters.XtraNoWrapperValueNodeXPath = @".//span//span";
                //    siCarsDotComReviewXPathParameters.XtraNoWrapperKeyNodeXPath = @".//div[@class='detail']";
                //    siCarsDotComReviewXPathParameters.ParentnodeExtraValueParaXPath = @".//div[@class='col12']/p";

                //    SICarsDotComReviewConnectionParameters siCarsDotComReviewConnectionParameters = new SICarsDotComReviewConnectionParameters(
                //        siCarsDotComReviewXPathParameters,
                //        @"http://www.cars.com/dealers/181827/classic-chevrolet-mentor/reviews/?count=1500"
                //        );

                //    CarsDotComReviewsConnection carsDotComConnection = new CarsDotComReviewsConnection(siCarsDotComReviewConnectionParameters);
                //    carsDotComConnection.GetReviewsHtml();

                //}

                //#endregion



                //#region DealerRater #4

                //if (testNumber == 4)
                //{
                //    SIDealerRaterReviewXPathParameters siDealerRaterReviewXPathParameters = new SIDealerRaterReviewXPathParameters();
                //    siDealerRaterReviewXPathParameters.ReviewCountCollectedXPath = @".//span[@class='count']";
                //    siDealerRaterReviewXPathParameters.ParentNodeReviewDetailsXPath = @"//div[@id='reviews']/article";
                //    siDealerRaterReviewXPathParameters.RatingValueXPath = @".//li[@class='total']/span";
                //    siDealerRaterReviewXPathParameters.ReviewerNameXPath = @".//span[@class='reviewer']";
                //    siDealerRaterReviewXPathParameters.ReviewDateXPath = @".//span[@class='dtreviewed']";
                //    siDealerRaterReviewXPathParameters.ReviewTextFullXPath = @".//p[@class='description']";
                //    siDealerRaterReviewXPathParameters.RatingCollectedXPath = @"//div[@class='hreview-aggregate']/span[1]";

                //    siDealerRaterReviewXPathParameters.ReviewsPerPage = 10;
                //    siDealerRaterReviewXPathParameters.ReasonForVisitXPath = @"div[1]/div[1]/span[3]";
                //    siDealerRaterReviewXPathParameters.ServiceRatingXPath = @".//*[@id='dealerRatings']/ul/li[6]/span";

                //    SIDealerRaterReviewConnectionParameters siDealerRaterReviewConnectionParameters = new SIDealerRaterReviewConnectionParameters(
                //        @"https://api.dealerrater.com/reviews/26899?accessToken=A4545A1A-D2A1-4222-8174-9BA4EBF6C933",
                //        siDealerRaterReviewXPathParameters,
                //        @"http://www.dealerrater.com/dealer/Driver-s-World-review-26899/"
                //        );

                //    DealerRaterReviewsConnection dealerRaterConnection = new DealerRaterReviewsConnection(siDealerRaterReviewConnectionParameters);
                //    dealerRaterConnection.GetReviewsAPI();
                //    dealerRaterConnection.GetReviewsHtml();

                //    //DealerRaterConnection dealerRaterConnection = new DealerRaterConnection(); //bad dealer 26899
                //    //dealerRaterConnection.ApiRequestUrl = @"https://api.dealerrater.com/reviews/26899?accessToken=A4545A1A-D2A1-4222-8174-9BA4EBF6C933";
                //    //dealerRaterConnection.HtmlRequestUrl = @"http://www.dealerrater.com/dealer/Driver-s-World-review-26899/";

                //    //dealerRaterConnection.GetSummaryFromAPI();
                //}

                //#endregion

                //#region City Search #5

                //if (testNumber == 5)
                //{
                //    SICitySearchReviewXPathParameters siCitySearchReviewXPathParameters = new SICitySearchReviewXPathParameters();
                //    siCitySearchReviewXPathParameters.ParentnodeReviewSummaryXPath = @".//*[@id='placeTabsContent']"; //NOT THE SAME AS APP.CONFIG
                //    siCitySearchReviewXPathParameters.ReviewCountCollectedXPath = @".//span[@class='itemCount']"; //NOT THE SAME AS APP.CONFIG
                //    siCitySearchReviewXPathParameters.ParentNodeReviewDetailsXPath = @"//div[starts-with(@id, 'review.')]";
                //    siCitySearchReviewXPathParameters.ReviewerNameXPath = @".//span[starts-with(@id, 'review.name.')]";
                //    siCitySearchReviewXPathParameters.RatingValueXPath = @".//span[@class='userRecommended']";
                //    siCitySearchReviewXPathParameters.ReviewDateXPath = @".//span[starts-with(@id, 'review.date.')]";
                //    siCitySearchReviewXPathParameters.ReviewTextFullXPath = @".//p[starts-with(@id, 'review.description.')]";
                //    siCitySearchReviewXPathParameters.ReviewsPerPage = 10;

                //    SICitySearchReviewConnectionParameters siCitySearchReviewConnectionParameters = new SICitySearchReviewConnectionParameters(
                //        siCitySearchReviewXPathParameters,
                //        @"http://philadelphia.citysearch.com/profile/8921881/haverford_pa/wilkie_lexus.html"
                //        );

                //    CitySearchReviewsConnection citySearchConnection = new CitySearchReviewsConnection(siCitySearchReviewConnectionParameters);
                //    citySearchConnection.GetReviewsHtml();
                //}

                //#endregion

                //#region Yahoo Local #6

                //if (testNumber == 6)
                //{
                //    SIYahooLocalReviewXPathParameters siYahooLocalReviewXPathParameters = new SIYahooLocalReviewXPathParameters();
                //    siYahooLocalReviewXPathParameters.ReviewsPerPage = 100;

                //    SIYahooLocalReviewConnectionParameters siYahooLocalReviewConnectionParameters = new SIYahooLocalReviewConnectionParameters(
                //        @"http://local.yahoo.com/rnr/getReviewNRating.php?bizId=17289075&mode=getReviews&rcnt=100&rstart=0&rsort=2",
                //        siYahooLocalReviewXPathParameters,
                //        @""
                //        );

                //    YahooLocalReviewsConnection yahooLocalConnection = new YahooLocalReviewsConnection(siYahooLocalReviewConnectionParameters);
                //    yahooLocalConnection.GetReviewsAPI();
                //}


                //#endregion

                //#region Yellow Pages #7

                //if (testNumber == 7)
                //{
                //    SIYellowPagesReviewXPathParameters siYellowPagesReviewXPathParameters = new SIYellowPagesReviewXPathParameters();
                //    siYellowPagesReviewXPathParameters.ParentnodeReviewSummaryXPath = @"//div[@class='basic-info-rating basic-info-average-rating']";
                //    siYellowPagesReviewXPathParameters.RatingCollectedXPath = @".//div/span";
                //    siYellowPagesReviewXPathParameters.ReviewCountCollectedXPath = @".//p/a";
                //    siYellowPagesReviewXPathParameters.ParentNodeReviewDetailsXPath = @"//li[starts-with(@id, 'review-')]";
                //    siYellowPagesReviewXPathParameters.RatingValueXPath = @".//span[starts-with(@class, 'rating-')]";
                //    siYellowPagesReviewXPathParameters.ReviewTextFullXPath = @".//p[@class='review-text truncated']";
                //    siYellowPagesReviewXPathParameters.ReviewerNameXPath = @"div/p[2]/strong/a"; //NOT IN APP.CONFIG
                //    siYellowPagesReviewXPathParameters.ReviewDateXPath = @"div/p[2]/text()[2]";//NOT IN APP.CONFIG
                //    siYellowPagesReviewXPathParameters.ExtraInfoKeysXPath = "Title";  //pipe delimited in app.config, use object? hash keyvaluepair
                //    siYellowPagesReviewXPathParameters.ExtraInfoValuesXPath = @".//h4";

                //    siYellowPagesReviewXPathParameters.ReviewsPerPage = 20;

                //    SIYellowPagesReviewConnectionParameters siYellowPagesReviewConnectionParameters = new SIYellowPagesReviewConnectionParameters(
                //        siYellowPagesReviewXPathParameters
                //    );

                //    YellowPagesReviewsConnection yellowPagesConnection = new YellowPagesReviewsConnection(siYellowPagesReviewConnectionParameters);
                //    //yellowPagesConnection.GetReviewsHtml();


                //    ////yellowPagesConnection.HtmlRequestUrl = @"http://www.yellowpages.com/san-antonio-tx/mip/alamo-toyota-467175679/reviews?lid=246737211";
                //    //yellowPagesConnection.HtmlRequestUrl = @"http://www.yellowpages.com/van-nuys-ca/mip/keyes-lexus-9905/reviews?lid=9905";

                //}

                //#endregion

                //#region Car Dealer Reviews #8

                //if (testNumber == 8)
                //{
                //    SICarDealerReviewsReviewXPathParameters siCarDealerReviewsReviewXPathParameters = new SICarDealerReviewsReviewXPathParameters();
                //    siCarDealerReviewsReviewXPathParameters.ParentnodeReviewSummaryXPath = @".//span[@class='hreview-aggregate']"; //NOT THE SAME AS APP.CONFIG
                //    siCarDealerReviewsReviewXPathParameters.RatingCollectedXPath = @"span[2]/span[1]"; //NOT THE SAME AS APP.CONFIG
                //    siCarDealerReviewsReviewXPathParameters.ReviewCountCollectedXPath = @"span[2]/span[3]"; //NOT THE SAME AS APP.CONFIG

                //    SICarDealerReviewsReviewConnectionParameters siCarDealerReviewsReviewConnectionParameters = new SICarDealerReviewsReviewConnectionParameters(
                //        siCarDealerReviewsReviewXPathParameters,
                //        @"http://www.cardealerreviews.org/?p=32846"
                //        );

                //    CarDealerReviewsConnection carDealerReviewsConnection = new CarDealerReviewsConnection(siCarDealerReviewsReviewConnectionParameters);
                //    carDealerReviewsConnection.GetReviewsHtml();
                //}

                //#endregion

                //#region Judys Book #9

                //if (testNumber == 9)
                //{
                //    SIJudysBookReviewXPathParameters siJudysBookReviewXPathParameters = new SIJudysBookReviewXPathParameters();
                //    siJudysBookReviewXPathParameters.ParentnodeReviewSummaryXPath = @".//div[contains(@id, '_reviewsDiv')]/div[2]/div[1]/div"; //NOT THE SAME AS APP.CONFIG
                //    siJudysBookReviewXPathParameters.RatingCollectedXPath = @".//input[contains(@id, '_rating_currRating')]"; //NOT THE SAME AS APP.CONFIG
                //    siJudysBookReviewXPathParameters.ReviewCountCollectedXPath = @"div/div[2]/strong";
                //    siJudysBookReviewXPathParameters.RatingValueXPath = @".//input[contains(@id, '_jbRating_currRating')]"; //NOT THE SAME AS APP.CONFIG
                //    siJudysBookReviewXPathParameters.ReviewerNameXPath = @".//a[contains(@id, 'userNameLink')]";
                //    siJudysBookReviewXPathParameters.ReviewDateXPath = @".//span[contains(@id, 'lblReviewDate')]";
                //    siJudysBookReviewXPathParameters.ReviewTextFullXPath = @".//span[contains(@id, 'lblReviewContent')]";
                //    siJudysBookReviewXPathParameters.FullReviewURLXPath = @".//div[contains(@id, 'adiv')]/span/a";
                //    siJudysBookReviewXPathParameters.ReviewTextFullParentXPath = @"//div[contains(@id, 'ReviewDetail1_active')]";
                //    siJudysBookReviewXPathParameters.ParentNodeReviewDetailsXPath = @"//table[contains(@id, 'jbReviewGridview')]/tr"; //NOT THE SAME AS APP.CONFIG
                //    siJudysBookReviewXPathParameters.ReviewsPerPage = 10;

                //    SIJudysBookReviewConnectionParameters siJudysBookReviewConnectionParameters = new SIJudysBookReviewConnectionParameters(
                //        siJudysBookReviewXPathParameters,
                //        @"http://www.judysbook.com/cities/haverford/Lexus-Dealers/30476893/Wilkie_Lexus.htm"
                //        );

                //    JudysBookReviewsConnection judysBookConnection = new JudysBookReviewsConnection(siJudysBookReviewConnectionParameters);
                //    judysBookConnection.GetReviewsHtml();

                //}

                //#endregion

                //#region Google API #10

                //if (testNumber == 10)
                //{
                //    SIGoogleReviewXPathParameters siGoogleReviewXPathParameters = new SIGoogleReviewXPathParameters();

                //    siGoogleReviewXPathParameters.ParentnodeReviewSummaryXPath = @"//*[@id='contentPane']/div/div[2]/div/div/div[3]";
                //    siGoogleReviewXPathParameters.RatingValueXPath = @".//div[1]/div[1]/div[2]/div[3]/div/div[1]/div/div[1]/div/div/span[1]";
                //    siGoogleReviewXPathParameters.ReviewCountCollectedXPath = @".//div[1]/div[1]/div[2]/div[3]/div/div[1]/div/div[1]/div/div/span[2]";
                //    siGoogleReviewXPathParameters.ListNodeReviewDetailsXPath = @".//div[@class='Xna kS']";
                //    siGoogleReviewXPathParameters.ReviewDetailRatingValueXPath = @"div[2]/div[1]/div[1]/span[contains(@class,'c-Rga-DDa c-Rga-DDa-nyb')]";
                //    siGoogleReviewXPathParameters.ReviewDetailRatingHalfValueXPath = @"div[2]/div[1]/div[1]/span[contains(@class,'c-Rga-DDa c-Rga-DDa-oyb')]";
                //    siGoogleReviewXPathParameters.ReviewerNameXPath = @"div[2]/span/a";
                //    siGoogleReviewXPathParameters.ReviewDateXPath = @"div[2]/div[1]/div[2]/span";
                //    siGoogleReviewXPathParameters.ReviewTextFullXPath = @"div[2]/div[2]/span";
                //    siGoogleReviewXPathParameters.ReviewCommentParentXPath = @"div[2]/div[5]/div";
                //    siGoogleReviewXPathParameters.ReviewCommentDateXPath = @"div[2]/div[5]/div/div[1]/span[2]";
                //    siGoogleReviewXPathParameters.ReviewCommentTextXPath = @"div[2]/div[5]/div/div[2]";
                //    siGoogleReviewXPathParameters.ReviewAPIPlaceIDXPath = @".//div[@class='a-f-e c-b c-b-haDnnc cf3rA L0a X9']";

                //    SIGoogleReviewConnectionParameters siGoogleReviewConnectionParameters = new SIGoogleReviewConnectionParameters(
                //        @"https://plus.google.com/_/pages/local/loadreviews?_reqid=1043554&rt=j",
                //        siGoogleReviewXPathParameters,
                //        @"https://plus.google.com/114284807299404883225/about?gl=US&hl=en-US"
                //        );
                //    GoogleReviewsConnection googleConnection = new GoogleReviewsConnection(siGoogleReviewConnectionParameters);

                //    googleConnection.GetReviewsHtml();

                //    //SIGoogleReviewConnectionParameters siGoogleReviewConnectionParameters = new SIGoogleReviewConnectionParameters(
                //    //	@"https://plus.google.com/_/pages/local/loadreviews?_reqid=267916&rt=j",
                //    //	siGoogleReviewXPathParameters,
                //    //	@"https://plus.google.com/112351237584855391100/about?gl=US&hl=en-US&review=1"
                //    //	);


                //    googleConnection.PostData.Clear();

                //    googleConnection.PostData.Add("f.req", "[\"7991780716084416063\",null,[null,null,[[28,0,1000,{}],[30,null,null,{}]]],[null,null,true,true,6,true]]");
                //    googleConnection.PostData.Add("at", "AObGSAjXxm1VBhhQWAfeQkIeTA9IoGethA:1372251820988");
                //    //googleConnection.PostData.Add("f.req", "[\"278050991257464660\",null,[null,null,[[28,0,1000,{}],[30,null,null,{}]]],[null,null,true,true,6,true]]");
                //    //googleConnection.PostData.Add("at", "AObGSAjXxm1VBhhQWAfeQkIeTA9IoGethA:1372251820988");

                //    googleConnection.GetReviewsAPI();

                //    //7991780716084416063  castle lookupid  114284807299404883225
                //    //278050991257464660 - url 112351237584855391100
                //}

                //#endregion
                
                #endregion

                #region Facebook Page #15

                if (testNumber == 15)
                {
                    SIFacebookPageParameters siFacebookPageParameters = new SIFacebookPageParameters();

                    siFacebookPageParameters.Token = "AAAAANqD36fQBAAFAqE1dFdLQG7NSBhtfvQkU5ZBwqabJDbfTGTYdKjw0WwT4h6fODoWoEtbbajypmqO21Akeund5PWzxh0Qvxwi6z6UHJ4ZA53bYxe";
                    siFacebookPageParameters.UniqueID = "255547016773";
                    //siFacebookPageParameters.FacebookAPITimeOut = "30000";

                    SIFacebookPageConnectionParameters siFacebookPageConnectionParameters = new SIFacebookPageConnectionParameters(siFacebookPageParameters);
                    /*
                    #region Page Basic Information
                    if (subtest == 1)
                    {
                        siFacebookPageParameters.FacebookGraphAPIURLPage = "https://graph.facebook.com/{0}?fields={1}&access_token={2}";

                        FacebookPageConnection facebookPageConnection = new FacebookPageConnection(siFacebookPageConnectionParameters);
                        FacebookPage objFacebookPage = facebookPageConnection.GetPageInformation();
                    }
                    #endregion

                    #region Page Insights
                    if (subtest == 2)
                    {
                        siFacebookPageParameters.FacebookGraphAPIURLInsights = "https://graph.facebook.com/{0}/insights?format=json&since={1}&until={2}&access_token={3}";
                        FacebookPageConnection facebookPageConnection = new FacebookPageConnection(siFacebookPageConnectionParameters);

                        string startDate = string.Empty;
                        string endDate = string.Empty;

                        //Get the Latest Insights Dates from Facebook As Facebook is behind for Insights
                        facebookPageConnection.SetDateForFacebookInsights(out startDate, out endDate);

                        FacebookInsights objFacebookInsights = facebookPageConnection.GetPageInsights(startDate, endDate);
                    }
                    #endregion

                    #region Page Feed Information
                    if (subtest == 3)
                    {
                        siFacebookPageParameters.FacebookGraphAPIURLFeed = "https://graph.facebook.com/{0}/feed?fields={1}&since={2}&until={3}&format={4}&limit={5}&access_token={6}";
                        FacebookPageConnection facebookPageConnection = new FacebookPageConnection(siFacebookPageConnectionParameters);

                        FacebookFeed objFacebookFeed = facebookPageConnection.GetFacebookFeedID(DateTime.Now.AddDays(-7).ToShortDateString(), DateTime.Now.ToShortDateString(), "500");
                    }
                    #endregion

                    #region RSS Feed Information
                    if (subtest == 4)
                    {
                        FacebookPageConnection facebookPageConnection = new FacebookPageConnection(siFacebookPageConnectionParameters);

                        FacebookPageRSSFeed objFacebookPageRSSFeed = facebookPageConnection.GetRSSFeedInformation("https://www.facebook.com/feeds/notifications.php?id=255547016773&viewer=100002916188585&key=AWif3BKtbYxO61cY&format=json");

                        List<FaceBookPostIdType> listFaceBookPostIdType = facebookPageConnection.ProcessRSSFeed(objFacebookPageRSSFeed);
                    }
                    #endregion

                    #region Page Permissions
                    if (subtest == 5)
                    {
                        siFacebookPageParameters.FacebookPermissionsURL = "https://developers.facebook.com/tools/debug/access_token?q={0}";

                        siFacebookPageParameters.XPathParentnodePermissionsDetails = "//table[contains(@class,'uiInfoTable')]";
                        siFacebookPageParameters.XPathlistnodePermissionsDetails = ".//tr";
                        siFacebookPageParameters.XPathParentError = "//div[contains(@class,'pam uiBoxRed')]";
                        siFacebookPageParameters.XPathError = "error parsing url";
                        siFacebookPageParameters.XPathTableheader = ".//th";
                        siFacebookPageParameters.XPathLink = ".//td/a";
                        siFacebookPageParameters.XPathErrorTd = ".//td";

                        FacebookPageConnection facebookPageConnection = new FacebookPageConnection(siFacebookPageConnectionParameters);

                        List<string> listPermissions = facebookPageConnection.GetPagePermissions();
                    }
                    #endregion

                    #region Facebook Page Search
                    if (subtest == 6)
                    {
                        siFacebookPageParameters.FacebookGraphAPIURLSearch = "https://graph.facebook.com/search?q={0}&type=page";
                        siFacebookPageParameters.FacebookGraphAPIURLPageSearch = "https://graph.facebook.com/{0}?fields={1}";

                        FacebookPageConnection facebookPageConnection = new FacebookPageConnection(siFacebookPageConnectionParameters);
                        string PageName = "Castle Chevrolet";

                        FacebookSearch objFacebookSearch = facebookPageConnection.GetPageSearch(PageName);
                    }
                    #endregion

                    #region Get Albums
                    if (subtest == 7)
                    {
                        siFacebookPageParameters.FacebookGraphAPIURLAlbumGet = "https://graph.facebook.com/{0}/albums?fields={1}&limit={2}&access_token={3}";

                        FacebookPageConnection facebookPageConnection = new FacebookPageConnection(siFacebookPageConnectionParameters);

                        FacebookAlbum objFacebookAlbums = facebookPageConnection.GetAlbums();
                    }
                    #endregion

                    #region User Picture
                    if (subtest == 8)
                    {
                        siFacebookPageParameters.FacebookGraphAPIURLUserPicture = "https://graph.facebook.com/{0}?fields=picture.type%28small%29";
                        string UniqueID = "255547016773";

                        FacebookPageConnection facebookPageConnection = new FacebookPageConnection(siFacebookPageConnectionParameters);

                        FaceBookUserPicture objFaceBookUserPicture = facebookPageConnection.UserPicture(UniqueID);
                    }
                    #endregion
                    */
                }
                #endregion

                #region Old Code

                /*
                #region Facebook Post Statistics #16
                if (testNumber == 16)
                {

                    SIFacebookPostStatisticsParameters siFacebookPostStatisticsParameters = new SIFacebookPostStatisticsParameters();
                    siFacebookPostStatisticsParameters.Token = "AAAAANqD36fQBAAFAqE1dFdLQG7NSBhtfvQkU5ZBwqabJDbfTGTYdKjw0WwT4h6fODoWoEtbbajypmqO21Akeund5PWzxh0Qvxwi6z6UHJ4ZA53bYxe";

                    SIFacebookPostStatisticsConnectionParameters siFacebookPostStatisticsConnectionParameters = new SIFacebookPostStatisticsConnectionParameters(siFacebookPostStatisticsParameters);

                    #region Post Statistics By PostID/ResultID
                    if (subtest == 1)
                    {
                        siFacebookPostStatisticsParameters.FacebookGraphAPIURLPostStatistics = "https://graph.facebook.com/{0}?fields={1}&access_token={2}";
                        siFacebookPostStatisticsParameters.ResultID = "10151722577356774";
                        siFacebookPostStatisticsParameters.Posttype = PostType.FacebookPhoto;

                        FacebookPostStatisticConnection facebookPostStatisticConnection = new FacebookPostStatisticConnection(siFacebookPostStatisticsConnectionParameters);

                        FacebookPostStatistics objFacebookPostStatistics = facebookPostStatisticConnection.GetPostStatistics();

                    }
                    #endregion

                    #region Post Statistics By FeedID
                    if (subtest == 2)
                    {
                        siFacebookPostStatisticsParameters.FacebookGraphAPIURLPostStatisticsByFeedID = "https://graph.facebook.com/{0}?access_token={1}";
                        siFacebookPostStatisticsParameters.FeedID = "255547016773_10151722577416774";
                        siFacebookPostStatisticsParameters.Posttype = PostType.FacebookPhoto;

                        FacebookPostStatisticConnection facebookPostStatisticConnection = new FacebookPostStatisticConnection(siFacebookPostStatisticsConnectionParameters);

                        FacebookPostStatisticsFeedID objFacebookPostStatisticsFeedID = facebookPostStatisticConnection.GetPostStatisticsByFeedId();

                    }
                    #endregion

                    #region Post(Event) Statistics By PostID/ResultID
                    if (subtest == 3)
                    {
                        siFacebookPostStatisticsParameters.FacebookGraphAPIURLEventStatistics = "https://graph.facebook.com/{0}/invited?fields={1}&limit={2}&summary={3}&access_token={4}";
                        siFacebookPostStatisticsParameters.ResultID = "428107527258911";
                        siFacebookPostStatisticsParameters.Posttype = PostType.FacebookEvent;

                        FacebookPostStatisticConnection facebookPostStatisticConnection = new FacebookPostStatisticConnection(siFacebookPostStatisticsConnectionParameters);

                        FacebookEventStatistics objFacebookEventStatistics = facebookPostStatisticConnection.GetEventStatistics();

                    }
                    #endregion

                    #region Post(Question) Statistics By PostID/ResultID
                    if (subtest == 4)
                    {
                        siFacebookPostStatisticsParameters.FacebookGraphAPIURLQuestionStatistics = "https://graph.facebook.com/{0}?fields={1}&access_token={2}";
                        siFacebookPostStatisticsParameters.ResultID = "10151630832626214";
                        siFacebookPostStatisticsParameters.Posttype = PostType.FacebookQuestion;

                        FacebookPostStatisticConnection facebookPostStatisticConnection = new FacebookPostStatisticConnection(siFacebookPostStatisticsConnectionParameters);

                        FacebookQuestionStatistics objFacebookQuestionStatistics = facebookPostStatisticConnection.GetQuestionStatistics();

                    }
                    #endregion
                }
                #endregion

                
                #region Facebook Wall Post (Create) #17
                if (testNumber == 17)
                {
                    SIFacebookPublishParameters siFacebookPublishParameters = new SIFacebookPublishParameters();

                    siFacebookPublishParameters.Token = "CAAELjVztUOgBAKGR4IKn0RDl4S3NLAArGEMZA7XZAJ9dRBMOmQoKG3lg0LW0RltR8B1zIuBxoyUWvEkIgpRaEOTn0X8ZAqDtwyMZCyrJiXgqmQaJUZBTdP4pDqyZAVXYSSFOSC6RJQnmG8LJfoTfzZAUmIcoSMjOXZBXxubmzvk7czMvSzy5upHZB";
                    siFacebookPublishParameters.UniqueID = "571476339562480";
                    siFacebookPublishParameters.FacebookGraphAPIURLWallPostCreate = "https://graph.facebook.com/{0}/feed?access_token={1}";

                    siFacebookPublishParameters.wallpost_message = "Link Test Post from SD Connection.";
                    siFacebookPublishParameters.wallpost_caption = DateTime.UtcNow.ToString();
                    siFacebookPublishParameters.wallpost_description = null;
                    siFacebookPublishParameters.wallpost_name = "Link Title Post";
                    siFacebookPublishParameters.wallpost_picture = string.Empty;
                    siFacebookPublishParameters.wallpost_source = string.Empty;
                    siFacebookPublishParameters.wallpost_targeting = "{'countries':''}";
                    siFacebookPublishParameters.wallpost_link = "www.socialdealer.com";


                    SIFacebookPublishConnectionParameters siFacebookPublishConnectionParameters = new SIFacebookPublishConnectionParameters(siFacebookPublishParameters);


                    FacebookPublishConnection facebookPublishConnection = new FacebookPublishConnection(siFacebookPublishConnectionParameters);

                    if (!string.IsNullOrEmpty(siFacebookPublishParameters.wallpost_message))
                        facebookPublishConnection.PostData.Add("message", siFacebookPublishParameters.wallpost_message);

                    if (!string.IsNullOrEmpty(siFacebookPublishParameters.wallpost_caption))
                        facebookPublishConnection.PostData.Add("caption", siFacebookPublishParameters.wallpost_caption);

                    if (!string.IsNullOrEmpty(siFacebookPublishParameters.wallpost_description))
                        facebookPublishConnection.PostData.Add("description", siFacebookPublishParameters.wallpost_description);

                    if (!string.IsNullOrEmpty(siFacebookPublishParameters.wallpost_name))
                        facebookPublishConnection.PostData.Add("name", siFacebookPublishParameters.wallpost_name);

                    if (!string.IsNullOrEmpty(siFacebookPublishParameters.wallpost_picture))
                        facebookPublishConnection.PostData.Add("picture", siFacebookPublishParameters.wallpost_picture);
                    else
                        facebookPublishConnection.PostData.Add("picture", string.Empty);

                    if (!string.IsNullOrEmpty(siFacebookPublishParameters.wallpost_source))
                        facebookPublishConnection.PostData.Add("source", siFacebookPublishParameters.wallpost_source);

                    if (!string.IsNullOrEmpty(siFacebookPublishParameters.wallpost_targeting))
                        facebookPublishConnection.PostData.Add("targeting", siFacebookPublishParameters.wallpost_targeting);

                    if (!string.IsNullOrEmpty(siFacebookPublishParameters.wallpost_link))
                        facebookPublishConnection.PostData.Add("link", siFacebookPublishParameters.wallpost_link);

                    FacebookResponse ObjFacebookResponse = facebookPublishConnection.CreateWallPost();
                }

                #endregion

                #region Facebook Create Album #18
                if (testNumber == 18)
                {
                    SIFacebookPublishParameters siFacebookPublishParameters = new SIFacebookPublishParameters();

                    siFacebookPublishParameters.Token = "CAAELjVztUOgBAKGR4IKn0RDl4S3NLAArGEMZA7XZAJ9dRBMOmQoKG3lg0LW0RltR8B1zIuBxoyUWvEkIgpRaEOTn0X8ZAqDtwyMZCyrJiXgqmQaJUZBTdP4pDqyZAVXYSSFOSC6RJQnmG8LJfoTfzZAUmIcoSMjOXZBXxubmzvk7czMvSzy5upHZB";
                    siFacebookPublishParameters.UniqueID = "571476339562480";
                    siFacebookPublishParameters.FacebookGraphAPIURLAlbumCreate = "https://graph.facebook.com/{0}/albums?access_token={1}";

                    siFacebookPublishParameters.album_Name = "SD Connection";
                    siFacebookPublishParameters.album_Message = "New Connection Ablem for SD";


                    SIFacebookPublishConnectionParameters siFacebookPublishConnectionParameters = new SIFacebookPublishConnectionParameters(siFacebookPublishParameters);


                    FacebookPublishConnection facebookPublishConnection = new FacebookPublishConnection(siFacebookPublishConnectionParameters);

                    if (!string.IsNullOrEmpty(siFacebookPublishParameters.album_Name))
                        facebookPublishConnection.PostData.Add("name", siFacebookPublishParameters.album_Name);

                    if (!string.IsNullOrEmpty(siFacebookPublishParameters.album_Message))
                        facebookPublishConnection.PostData.Add("message", siFacebookPublishParameters.album_Message);

                    FacebookResponse ObjFacebookResponse = facebookPublishConnection.CreateAlbum();
                }

                #endregion

                #region Facebook Create Question #19
                if (testNumber == 19)
                {
                    SIFacebookPublishParameters siFacebookPublishParameters = new SIFacebookPublishParameters();

                    siFacebookPublishParameters.Token = "CAAELjVztUOgBAKGR4IKn0RDl4S3NLAArGEMZA7XZAJ9dRBMOmQoKG3lg0LW0RltR8B1zIuBxoyUWvEkIgpRaEOTn0X8ZAqDtwyMZCyrJiXgqmQaJUZBTdP4pDqyZAVXYSSFOSC6RJQnmG8LJfoTfzZAUmIcoSMjOXZBXxubmzvk7czMvSzy5upHZB";
                    siFacebookPublishParameters.UniqueID = "571476339562480";
                    siFacebookPublishParameters.FacebookGraphAPIURLQuestionCreate = "https://graph.facebook.com/{0}/questions?access_token={1}";

                    siFacebookPublishParameters.Question_Text = "Would you use self-driving car?";
                    siFacebookPublishParameters.Question_Options = "Yes|No";
                    siFacebookPublishParameters.Question_AllowNewOptions = "0"; // 0 Or 1

                    string options_Temp = string.Empty;
                    int Question_OptionsCount = 0;
                    options_Temp = FacebookPublishConnection.ConverttoFacebookStyleOption(siFacebookPublishParameters.Question_Options, out Question_OptionsCount);
                    siFacebookPublishParameters.Question_OptionsCount = Question_OptionsCount;
                    siFacebookPublishParameters.Question_Options = options_Temp;

                    SIFacebookPublishConnectionParameters siFacebookPublishConnectionParameters = new SIFacebookPublishConnectionParameters(siFacebookPublishParameters);

                    FacebookPublishConnection facebookPublishConnection = new FacebookPublishConnection(siFacebookPublishConnectionParameters);

                    if (!string.IsNullOrEmpty(siFacebookPublishParameters.Question_Text))
                        facebookPublishConnection.PostData.Add("question", siFacebookPublishParameters.Question_Text);



                    if (!string.IsNullOrEmpty(siFacebookPublishParameters.Question_Options))
                        facebookPublishConnection.PostData.Add("options", siFacebookPublishParameters.Question_Options);

                    if (!string.IsNullOrEmpty(siFacebookPublishParameters.Question_Options))
                        facebookPublishConnection.PostData.Add("allow_new_options", siFacebookPublishParameters.Question_AllowNewOptions);

                    FacebookResponse ObjFacebookResponse = facebookPublishConnection.CreateQuestion();
                }

                #endregion
                */
                
                #endregion

                #region URL HARVESTER EDMUNDS #20
                if (testNumber == 20)
                {
                    SIURLHarvesterXPathParameters siURLHarvesterXPathParameters = new SIURLHarvesterXPathParameters();
                    siURLHarvesterXPathParameters.Edmunds_QueryBasePath = "http://www.edmunds.com/dealer-reviews/?zip={Zip}&make=&query={LocationName}";

                    siURLHarvesterXPathParameters.EdmundsDealeListParentXPath = "//div[@id='dealer-listing-parent']";
                    siURLHarvesterXPathParameters.EdmundsDealeListChildXPath = ".//div[contains(@id,'pdp_dealer_')][1]//li[contains(@class,'dlr-name')]//a";

                    SIURLHarvesterConnectionParameters siURLHarvesterConnectionParameters = new SIURLHarvesterConnectionParameters(
                        siURLHarvesterXPathParameters, "Castle Chevrolet", "Villa Park", "IL", "Illinois", "60181");

                    URLHarvesterConnection urlHarvesterConnection = new URLHarvesterConnection(siURLHarvesterConnectionParameters);
                    string EdmundsURL = urlHarvesterConnection.GetEdmundsURL();

                }
                #endregion

                #region URL HARVESTER GOOGLE #21
                if (testNumber == 21)
                {
                    SIURLHarvesterXPathParameters siURLHarvesterXPathParameters = new SIURLHarvesterXPathParameters();

                    siURLHarvesterXPathParameters.Google_QueryBasePath1 = "https://www.google.com/search?hl=en&q={LocationName}%20{City}%20{Zip}%20google%20review&btnG=Google+Search#hl=en&biw=1920&bih=928&sclient=psy-ab&q={LocationName}+{City}+{State}+google+review&oq={LocationName}&gs_l=serp.3.0.0i22i30.11507.12120.0.13857.4.2.0.2.2.0.111.212.0j2.2.0...0.0...1c.1.15.psy-ab.85MpcXxY2lM&pbx=1&bav=on.2,or.r_cp.r_qf.&bvm=bv.47244034,d.aWc&fp=bc706989ed678464";
                    siURLHarvesterXPathParameters.Google_QueryBasePath2 = "https://www.google.com/search?hl=en&q={LocationName}%20{City}%20{Zip}&btnG=Google+Search#hl=en&biw=1920&bih=928&sclient=psy-ab&q={LocationName}+{City}+{State}&oq={LocationName}&gs_l=serp.3.0.0i22i30.11507.12120.0.13857.4.2.0.2.2.0.111.212.0j2.2.0...0.0...1c.1.15.psy-ab.85MpcXxY2lM&pbx=1&bav=on.2,or.r_cp.r_qf.&bvm=bv.47244034,d.aWc&fp=bc706989ed678464";
                    siURLHarvesterXPathParameters.Edmunds_QueryBasePath = "http://www.edmunds.com/dealer-reviews/?zip={Zip}&make=&query={LocationName}";

                    siURLHarvesterXPathParameters.GoogleMoreReviewsListPath = ".//*[@id='rhs_block']/div/div[1]/div/div[5]/div[2]/div/div[4]/span/a";
                    //siURLHarvesterXPathParameters.GoogleMoreReviewsPath = ".//*[@id='rhs_block']/div/div[1]/div/div[5]/div[2]/div/div[4]";
                    siURLHarvesterXPathParameters.GoogleSectionPath = "//div[@id='rhs_block']";
                    siURLHarvesterXPathParameters.GoogleURLPath = ".//*[@id='rhs_block']/div/div[1]/div/div[5]/div[2]/div/div[2]/span[2]/a";

                    siURLHarvesterXPathParameters.EdmundsDealeListParentXPath = "//div[@id='dealer-listing-parent']";
                    siURLHarvesterXPathParameters.EdmundsDealeListChildXPath = ".//div[contains(@id,'pdp_dealer_')][1]//li[contains(@class,'dlr-name')]//a";

                    SIURLHarvesterConnectionParameters siURLHarvesterConnectionParameters = new SIURLHarvesterConnectionParameters(
                        siURLHarvesterXPathParameters, "Castle Chevrolet", "Villa Park", "IL", "Illinois", "60181"
                        );

                    URLHarvesterConnection urlHarvesterConnection = new URLHarvesterConnection(siURLHarvesterConnectionParameters);

                    urlHarvesterConnection.GetURLs();
                }
                #endregion

                #region EdmundsLookups #22

                if (testNumber == 22)
                {
                    SIEdmundsLookupParameters siedmundsLookupParameters = new SIEdmundsLookupParameters()
                    {
                        Address = "",
                        ApiRequestUrl = "http://api.edmunds.com/v1/api/dealer?zipcode=60181&api_key=w4mdm2h6rejuvkt5fsge3ags&fmt=json",
                        DealerId = "",
                        DealerLogicalName = "",
                        DealerName = "",
                        DealerType = "",
                        Make = "",
                        ZipCode = "60181"
                    };



                    var edmundsConnection = new EdmundsLookupConnection(siedmundsLookupParameters);
                    edmundsConnection.GetDealers();
                }


                #endregion
                
                #region Upload Photo #23
                //if (testNumber == 23)
                //{
                //    SIFacebookPublishParameters siFacebookPublishParameters = new SIFacebookPublishParameters();

                //    siFacebookPublishParameters.Token = "CAAELjVztUOgBAKGR4IKn0RDl4S3NLAArGEMZA7XZAJ9dRBMOmQoKG3lg0LW0RltR8B1zIuBxoyUWvEkIgpRaEOTn0X8ZAqDtwyMZCyrJiXgqmQaJUZBTdP4pDqyZAVXYSSFOSC6RJQnmG8LJfoTfzZAUmIcoSMjOXZBXxubmzvk7czMvSzy5upHZB";
                //    siFacebookPublishParameters.UniqueID = "571476339562480";
                //    siFacebookPublishParameters.FacebookGraphAPIURLUploadPhoto = "https://graph.facebook.com/";

                //    siFacebookPublishParameters.Photo_Message = "Test Photo";
                //    siFacebookPublishParameters.Photo_Filename = "http://dealers.socialdealer.com/Public/PublishImages/909c461e-ca1a-4ee3-aacf-f75533796923.jpg";
                //    siFacebookPublishParameters.Photo_Album_id = string.Empty;

                //    SIFacebookPublishConnectionParameters siFacebookPublishConnectionParameters = new SIFacebookPublishConnectionParameters(siFacebookPublishParameters);

                //    FacebookPublishConnection facebookPublishConnection = new FacebookPublishConnection(siFacebookPublishConnectionParameters);

                //    FacebookResponse ObjFacebookResponse = facebookPublishConnection.UploadPhoto();
                //}
                #endregion
                
                #region TWITTER PAGE INSIGHTS #25
                if (testNumber == 25)
                {
                    SITwitterPageParameters siTwitterPageParameters = new SITwitterPageParameters();

                    siTwitterPageParameters.uniqueID = "95928073";
                    siTwitterPageParameters.oauth_token = "95928073-tyobaXX4cRgc1dNHB3ebHI4mnBsZNN4AKC9XPvE3U";
                    siTwitterPageParameters.oauth_token_secret = "Ke8nF7p4T5vkLktWFf6XWzXr21tliEbjxe8DXRBWwzw";
                    siTwitterPageParameters.oauth_consumer_key = "3yJ8qfr2ZcWaXwIryI7xg";
                    siTwitterPageParameters.oauth_consumer_secret = "2ut16gtlx6DRccjkmd50S7iSwUghdLs8wZb7n8tkfQ";

                    //siFacebookPageParameters.FacebookAPITimeOut = "30000";

                    SITwitterPageConnectionParameters siTwitterPageConnectionParameters = new SITwitterPageConnectionParameters(siTwitterPageParameters);

                    TwitterPageConnection twitterPageConnection = new TwitterPageConnection(siTwitterPageConnectionParameters);
                    //TwitterPageInsightsResponse objTwitterPageInsightsResponse = twitterPageConnection.GetPageInsights();


                }
                #endregion

                #region TWITTER STATUS UPDATE #26
                /*
                if (testNumber == 26)
                {
                    SITwitterPublishParameters siTwitterPublishParameters = new SITwitterPublishParameters();

                    siTwitterPublishParameters.uniqueID = "1395614894";
                    siTwitterPublishParameters.oauth_token = "1395614894-zDjUonGHYLF88FPsqDBCkbkrLB6MEqJzmfxfKeK";
                    siTwitterPublishParameters.oauth_token_secret = "BlNntzX9zkxSktAg482GhAEd5sSIDJ85knBn0JACzPw";
                    siTwitterPublishParameters.oauth_consumer_key = "3yJ8qfr2ZcWaXwIryI7xg";
                    siTwitterPublishParameters.oauth_consumer_secret = "2ut16gtlx6DRccjkmd50S7iSwUghdLs8wZb7n8tkfQ";

                    siTwitterPublishParameters.status = "What would you #bbq if this was yours? #Riverside #California";
                    siTwitterPublishParameters.link = "http://tinyurl.com/ke6qdt4";

                    SITwitterPublishConnectionParameters siTwitterPublishConnectionParameters = new SITwitterPublishConnectionParameters(siTwitterPublishParameters);

                    TwitterPublishConnection twitterPublishConnection = new TwitterPublishConnection(siTwitterPublishConnectionParameters);
                    TwitterResponse objTwitterResponse = twitterPublishConnection.StatusUpdate();


                }
                */
                #endregion

                #region EdmundsLookups #27

                if (testNumber == 27)
                {
                    EdmundsVehicleContentConnectionParameters siContentConnectionParameters = new EdmundsVehicleContentConnectionParameters(timeOutSeconds: 120, useProxy: false, maxRedirects: 1);

                    var edmundsVehicleContentConnection = new EdmundsVehicleContentConnection(siContentConnectionParameters);
                    edmundsVehicleContentConnection.GetAPI();
                }


                #endregion

                #region YellowPages #28

                if (testNumber == 28)
                {
                    ReviewURLValidatorData accountInfo = new ReviewURLValidatorData(
                        companyName: "Enterprise Car Sales Glen Ellyn",
                        address: "395 W Roosevelt Rd",
                        city: "Glen Ellyn",
                        stateFull: "ILLINOIS",
                        stateAbbrev: "IL",
                        zip: "60137",
                        phone: "(888) 718-4150");

                    var xPaths = new YellowPagesReviewURLValidatorXPathParameters();

                    var connection = ProviderFactory.Validator.GetValidatorConnection(SI.DTO.ReviewSourceEnum.YellowPages);

                    //var test1 = connection.ValidateReviewURL("http://www.yellowpages.com/villa-park-il/mip/castle-chevrolet-1163184?lid=185221277");
                    var test1 = connection.ValidateReviewURL("http://www.yellowpages.com/tampa-fl/mip/sport-spine-physical-therapy-473136433", accountInfo);
                    //var test2 = connection.ValidateReviewURL("http://www.yellowpages.com/nationwide/mip/lexus-of-charleston-462212578?lid=462212578");

                    testvalidator = test1;
                }

                //var xPaths =
                //    new YellowPagesReviewURLValidatorXPathParameters(
                //        queryBasePath: "http://www.yellowpages.com/08054/{LocationName}?q={LocationName} ",
                //        reviewCountPath: ".//*[@id='average-rating-count'] ",
                //        ratingValuePath: ".//span[starts-with(@class, 'rating-')] ",
                //        locationNamePath: ".//a[@class='url '] ", phonePath: ".//p[@class='phone']/strong ",
                //        addressPath: ".//span[@class='street-address'] ", cityPath: ".//span[@class='locality'] ",
                //        statePath: ".//span[@class='region'] ", zipPath: ".//span[@class='postal-code'] ",
                //        websiteURLPath: ".//a[@class='primary-website'] ", manufacturerPath: " ", latitudePath: " ",
                //        longitudePath: " ", reviewDetailIteratorPath: ".//li[contains(@id,'review-')] ",
                //        noReviewText: "Be the first to review ", noReviewTextPath: ".//a[@id='first-to-review-mip'] ",
                //        noRatingPath: "<span class=\"rating-0\"> stars</span> ");

                //var xPaths = new YellowPagesReviewURLValidatorXPathParameters)
                //{
                //    QueryBasePath = "http://www.yellowpages.com/08054/{LocationName}?q={LocationName} ", 
                //    URLPath = "http://www.yellowpages.com/villa-park-il/mip/castle-chevrolet-1163184?lid=185221277", 
                //    ReviewCountPath = ".//*[@id='average-rating-count'] ", 
                //    RatingValuePath = ".//span[starts-with(@class, 'rating-')] ", 
                //    LocationNamePath = ".//a[@class='url'] ", 
                //    PhonePath = ".//p[@class='phone']/strong ", 
                //    AddressPath = ".//span[@class='street-address'] ", 
                //    CityPath = ".//span[@class='locality'] ", 
                //    StatePath = ".//span[@class='region'] ", 
                //    ZipPath = ".//span[@class='postal-code'] ", 
                //    WebsiteURLPath = ".//a[@class='primary-website'] ", 
                //    ManufacturerPath = " ", 
                //    LatitudePath = " ", 
                //    LongitudePath = " ", 
                //    ReviewDetailIteratorPath = ".//li[contains(@id,'review-')] ", 
                //    NoReviewText = "Be the first to review ", 
                //    NoReviewTextPath = ".//a[@id='first-to-review-mip'] ", 
                //    NoRatingPath = "<span class=\"rating-0\"> stars</span> "
                //};

                //SIReviewURLValidatorConnection connection = null;

                //var parameters = new YellowPagesReviewURLValidatorParameters(120, false, 1);

                //connection = new YellowPagesReviewURLValidatorConnection(parameters);
                //var test1 = connection.ValidateReviewURL("http://www.yellowpages.com/villa-park-il/mip/castle-chevrolet-1163184?lid=185221277");
                //var test2 = connection.ValidateReviewURL("http://www.yellowpages.com/nationwide/mip/lexus-of-charleston-462212578?lid=462212578");



                //}


                #endregion

                #region Yahoo Validation #29

                if (testNumber == 29)
                {
                    ReviewURLValidatorData accountInfo = new ReviewURLValidatorData(
                        companyName: "Castle Chevrolet",
                        address: "400 E Roosevelt Rd",
                        city: "Villa Park",
                        stateFull: "ILLINOIS",
                        stateAbbrev: "IL",
                        zip: "60181",
                        phone: "(630) 279-5200");


                    var xPaths = new YahooReviewURLValidatorXPathParameters();
                    var connection = ProviderFactory.Validator.GetValidatorConnection(SI.DTO.ReviewSourceEnum.Yahoo);

                    var test1 = connection.ValidateReviewURL("http://local.yahoo.com/info-17253593-castle-chevrolet-villa-park", accountInfo);
                    //var test2 = connection.ValidateReviewURL("http://local.yahoo.com/info-88906863-rightway-auto-sales-villa-park");
                    //var test3 = connection.ValidateReviewURL("http://local.yahoo.com/info-41697286-advantage-chevrolet-of-bolingbrook-incorporated-bolingbrook");
                    testvalidator = test1;

                }
                #endregion

                #region Edmunds Validation #30

                if (testNumber == 30)
                {
                    ReviewURLValidatorData accountInfo = new ReviewURLValidatorData(
                        companyName: "Lombard Toyota Scion",
                        address: "395 W Roosevelt Rd",
                        city: "Glen Ellyn",
                        stateFull: "ILLINOIS",
                        stateAbbrev: "IL",
                        zip: "60137",
                        phone: "(888) 718-4150");


                    var xPaths = new EdmundsReviewURLValidatorXPathParameters();
                    var connection = ProviderFactory.Validator.GetValidatorConnection(SI.DTO.ReviewSourceEnum.Edmunds);

                    //var test1 = connection.ValidateReviewURL("http://www.edmunds.com/dealerships/Illinois/Lombard/LombardToyotaScion/sales.1.html ", accountInfo);
                    var test2 = connection.ValidateReviewURL("http://www.edmunds.com/dealerships/Illinois/GlenEllyn/EnterpriseCarSalesGlenEllyn/sales.1.html", accountInfo); //No Reviews
                    testvalidator = test2; //3 mismatch


                }
                #endregion

                #region Facebook Reviews #31

                //if (testNumber == 31)
                //{
                //    SIFacebookReviewXPathParemeters siFacebookReviewXPathParemeters = new SIFacebookReviewXPathParemeters();


                //    SIFacebookReviewConnectionParameters siFacebookReviewConnectionParameters = new SIFacebookReviewConnectionParameters(
                //        "",
                //        siFacebookReviewXPathParemeters,
                //        @"https://www.facebook.com/ajax/pages/recommendations/see_all?page_id=255547016773&amp;filter_type=1&&__user=100002916188585&__a=1"
                //        );

                //    FacebookReviewsConnection facebookReviewsConnection = new FacebookReviewsConnection(siFacebookReviewConnectionParameters);
                //    facebookReviewsConnection.GetReviewsHtml();



                //}
                #endregion

                #region Google Validation #32

                if (testNumber == 32)
                {
                    ReviewURLValidatorData accountInfo = new ReviewURLValidatorData(
                        companyName: "Advantage Chevrolet of Bolingbrook",
                        address: "115 SW Frontage Rd",
                        city: "Bolingbrook",
                        stateFull: "ILLINOIS",
                        stateAbbrev: "IL",
                        zip: "60440",
                        phone: "(630) 243 4460");

                    var xPaths = new GoogleReviewURLValidatorXPathParameters();
                    var connection = ProviderFactory.Validator.GetValidatorConnection(SI.DTO.ReviewSourceEnum.Google);

                    //var test1 = connection.ValidateReviewURL("https://plus.google.com/u/0/b/114284807299404883225/114284807299404883225/about");
                    //var test2 = connection.ValidateReviewURL("https://plus.google.com/107886752260991943641/about?gl=US&hl=en-US", accountInfo);
                    var testCanada = connection.ValidateReviewURL("https://plus.google.com/u/0/115720849003724493249/about", accountInfo);
                    testvalidator = testCanada;

                }
                #endregion

                #region JudysBook Validation #33

                if (testNumber == 33)
                {
                    ReviewURLValidatorData accountInfo = new ReviewURLValidatorData(
                        companyName: "Castle Chevrolet",
                        address: "400 E Roosevelt Rd",
                        city: "Villa Park",
                        stateFull: "Illinois",
                        stateAbbrev: "",
                        zip: "60181 ",
                        phone: "(630) 279-5200");

                    var xPaths = new JudysBookReviewURLValidatorXPathParameters();
                    var connection = ProviderFactory.Validator.GetValidatorConnection(SI.DTO.ReviewSourceEnum.JudysBook);

                    //var test1 = connection.ValidateReviewURL("http://www.judysbook.com/Pink-Door--The-Restaurants-seattle-r4768.htm");
                    var test2 = connection.ValidateReviewURL("http://www.judysbook.com/Castle-Chevrolet-Auto-Parts-villapark-il-r31025482.htm", accountInfo);
                    testvalidator = test2;
                }
                #endregion

                #region CitySearch Validation #34

                if (testNumber == 34)
                {
                    ReviewURLValidatorData accountInfo = new ReviewURLValidatorData(
                        companyName: "Bonnie & Clyde's Cb & Stereo",
                        address: "11311 Harry Hines Blvd Ste 104",
                        city: " Dallas",
                        stateFull: "",
                        stateAbbrev: "TX",
                        zip: "",
                        phone: "(972) 241-1188");

                    var xPaths = new CitySearchReviewURLValidatorXPathParameters();
                    var connection = ProviderFactory.Validator.GetValidatorConnection(SI.DTO.ReviewSourceEnum.CitySearch);

                    var test1 = connection.ValidateReviewURL("http://dallas.citysearch.com/profile/34507178/dallas_tx/bonnie_clyde_s_cb_stereo.html", accountInfo);
                    //var test2 = connection.ValidateReviewURL("http://cleveland.citysearch.com/profile/7966535/willoughby_hills_oh/pat_o_brien_chevrolet.html");
                    testvalidator = test1;
                }
                #endregion

                #region DealerRater Validation #35

                if (testNumber == 35)
                {
                    ReviewURLValidatorData accountInfo = new ReviewURLValidatorData(
                        companyName: "Ray Buick",
                        address: "5011 w. 63rd ST",
                        city: "Chicago ",
                        stateFull: "ILLINOIS",
                        stateAbbrev: "IL",
                        zip: "60638",
                        phone: "(773) 767-8530");

                    var xPaths = new DealerRaterReviewURLValidatorXPathParameters();
                    var connection = ProviderFactory.Validator.GetValidatorConnection(SI.DTO.ReviewSourceEnum.DealerRater);

                    //var test1 = connection.ValidateReviewURL("http://www.dealerrater.com/dealer/Advantage-Chevrolet-of-Bolingbrook-review-2182/");
                    //var test2 = connection.ValidateReviewURL("http://www.dealerrater.com/dealer/Bill-Kay-Buick-GMC-review-1840/");
                    var test3 = connection.ValidateReviewURL("http://www.dealerrater.com/dealer/Ray-Buick-review-36171/", accountInfo);
                    testvalidator = test3;
                }
                #endregion

                #region Yelp Validation #36

                if (testNumber == 36)
                {
                    ReviewURLValidatorData accountInfo = new ReviewURLValidatorData(
                        companyName: "Fineride Motors ",
                        address: "638 N Iowa Ave",
                        city: "Villa Park",
                        stateFull: "ILLINOIS",
                        stateAbbrev: "IL",
                        zip: "60181",
                        phone: "(630) 833-1866");

                    var xPaths = new YelpReviewURLValidatorXPathParameters();
                    var connection = ProviderFactory.Validator.GetValidatorConnection(SI.DTO.ReviewSourceEnum.Yelp);

                    //var test1 = connection.ValidateReviewURL("http://www.yelp.com/biz/elmhurst-toyota-elmhurst#query:Chevrolet");
                    var test2 = connection.ValidateReviewURL("http://www.yelp.com/biz/grand-subaru-bensenville", accountInfo);
                    //var test3 = connection.ValidateReviewURL("http://www.yelp.com/biz/castle-chevrolet-villa-park");
                    testvalidator = test2;
                }
                #endregion

                #region CarDealerReviews Validation #37

                if (testNumber == 37)
                {
                    ReviewURLValidatorData accountInfo = new ReviewURLValidatorData(
                        companyName: "Enterprise Car Sales Glen Ellyn",
                        address: "395 W Roosevelt Rd",
                        city: "Glen Ellyn",
                        stateFull: "ILLINOIS",
                        stateAbbrev: "IL",
                        zip: "60137",
                        phone: "(888) 718-4150");

                    var xPaths = new CarDealerReviewURLValidatorXPathParameters();
                    var connection = ProviderFactory.Validator.GetValidatorConnection(SI.DTO.ReviewSourceEnum.CarDealer);

                    //var test3 = connection.ValidateReviewURL("http://www.cardealerreviews.org/?p=27671");
                    var test1 = connection.ValidateReviewURL("http://www.cardealerreviews.org/?p=7421", accountInfo);
                    //var test2 = connection.ValidateReviewURL("http://www.cardealerreviews.org/?p=17578");
                    testvalidator = test1;
                }
                #endregion

                #region CarsDotCom Validation #38

                if (testNumber == 38)
                {
                    ReviewURLValidatorData accountInfo = new ReviewURLValidatorData(
                        companyName: "Pat O'Brien Chevrolet South",
                        address: "3880 Pearl Rd.",
                        city: "Medina",
                        stateFull: "",
                        stateAbbrev: "OH",
                        zip: "44256",
                        phone: "330-725-4588 ");

                    var xPaths = new CarsdotcomReviewURLValidatorXPathParameters();
                    var connection = ProviderFactory.Validator.GetValidatorConnection(SI.DTO.ReviewSourceEnum.CarsDotCom);


                    //http://www.cars.com/dealers/14130/ganley-chevrolet/reviews/ - Has Reviews & Rating
                    //http://www.cars.com/dealers/211629/z-frank-chevrolet-kia/reviews/ - Has no Reviews or Rating
                    var test1 = connection.ValidateReviewURL(" http://www.cars.com/dealers/154883/pat-obrien-chevrolet-south/reviews/", accountInfo);
                    testvalidator = test1;
                }
                #endregion

                #region InsiderPages Validation #39

                if (testNumber == 39)
                {
                    ReviewURLValidatorData accountInfo = new ReviewURLValidatorData(
                        companyName: "Pat O'Brien Chevrolet South",
                        address: "3880 Pearl Rd.",
                        city: "Medina",
                        stateFull: "",
                        stateAbbrev: "OH",
                        zip: "44256",
                        phone: "330-725-4588 ");

                    var xPaths = new InsiderPagesReviewURLValidatorXPathParameters();
                    var connection = ProviderFactory.Validator.GetValidatorConnection(SI.DTO.ReviewSourceEnum.InsiderPages);

                    var test1 = connection.ValidateReviewURL(" http://www.insiderpages.com/b/3713877919/castle-chevrolet-of-villa-park-villa-park?sort_reviews=recency", accountInfo);
                    testvalidator = test1;
                }
                #endregion

                #region URL HARVESTER Edmunds #40
                //if (testNumber == 40)
                //{
                //    SIURLHarvesterXPathParameters siURLHarvesterXPathParameters = new SIURLHarvesterXPathParameters();
                //    //siURLHarvesterXPathParameters.Yelp_QueryBasePath = "http://www.edmunds.com/dealer-reviews/?zip={Zip}&make=&query={LocationName}";

                //    //siURLHarvesterXPathParameters.YelpDealeListParentXPath = "//div[@id='dealer-listing-parent']";
                //    //siURLHarvesterXPathParameters.YelpDealeListChildXPath = ".//div[contains(@id,'pdp_dealer_')][1]//li[contains(@class,'dlr-name')]//a";

                //    siURLHarvesterXPathParameters = ProviderFactory.Harvester.GetHarvesterSetting(SI.DTO.ReviewSourceEnum.Edmunds);

                //    SIURLHarvesterConnectionParameters siURLHarvesterConnectionParameters = new SIURLHarvesterConnectionParameters(
                //    siURLHarvesterXPathParameters, "Castle Chevrolet", "Villa Park", "IL", "Illinois", "60181");

                //    URLHarvesterConnection urlHarvesterConnection = new URLHarvesterConnection(siURLHarvesterConnectionParameters);
                //    string URL = urlHarvesterConnection.GetURLs();

                //}
                #endregion

                #region URL HARVESTER Yelp #41
                if (testNumber == 41)
                {
                    SIURLHarvesterXPathParameters siURLHarvesterXPathParameters = new SIURLHarvesterXPathParameters();

                    //var connection = ProviderFactory.Harvester.GetHarvesterSetting(SI.DTO.ReviewSourceEnum.Yelp);

                    SIURLHarvesterConnectionParameters siURLHarvesterConnectionParameters = new SIURLHarvesterConnectionParameters(
                    siURLHarvesterXPathParameters, "Castle Chevrolet", "Villa Park", "IL", "Illinois", "60181");

                    URLHarvesterConnection urlHarvesterConnection = new URLHarvesterConnection(siURLHarvesterConnectionParameters);
                    //string resultURL = urlHarvesterConnection.GetReviewURL();

                }
                #endregion

                #region URL HARVESTER VALIDATION #42
                if (testNumber == 42)
                {
                    var reviewHarvester = new ReviewHarvester();

                    //ReviewURLValidatorData accountInfo = new ReviewURLValidatorData(
                    //accountInfo.CompanyName = "Castle Chevrolet",
                    //accountInfo.StreetAddress = "400 E Roosevelt Rd",
                    //accountInfo.City = "Villa Park",
                    //accountInfo.StateAbbrev = "IL",
                    //accountInfo.StateFull = "Illinois",
                    //accountInfo.Zip = "60181",
                    //accountInfo.Phone = "6302795200");

                    //ReviewURLValidatorData accountInfo = new ReviewURLValidatorData(
                        //companyName: "Hendrick BMW",
                        //address: "	6950 E Independence Blvd",
                        //city: "Charlotte",
                        //stateFull: "North Carolina",
                        //stateAbbrev: "NC",
                        //zip: "28278",
                        //phone: "");
                    //ReviewURLValidatorData accountInfo = new ReviewURLValidatorData(
                    //    companyName: "Hendrick Buick",
                    //    address: "1626 Montgomery Hwy",
                    //    city: "Hoover",
                    //    stateFull: "Alabama",
                    //    stateAbbrev: "AL",
                    //    zip: "35244",
                    //    phone: "");
                    //ReviewURLValidatorData accountInfo = new ReviewURLValidatorData(
                    //    companyName: "Castle Chevrolet",
                    //    address: "400 E Roosevelt Rd",
                    //    city: "Villa Park",
                    //    stateFull: "Illinois",
                    //    stateAbbrev: "IL",
                    //    zip: "60181",
                    //    phone: "6302795200");
                    ReviewURLValidatorData accountInfo = new ReviewURLValidatorData(
                        companyName: "Hendrick Acura Overland Park",
                        address: "",
                        city: "Overland Park",
                        stateFull: "Kansas",
                        stateAbbrev: "KS",
                        zip: "66204",
                        phone: "");
                    List<ReviewUrlSearch.Models.ReviewURL> listReviewURL = reviewHarvester.IdentifyLocationUrls(accountInfo.CompanyName, accountInfo.City, accountInfo.StateAbbrev, accountInfo.Zip);
                    List<ReviewUrlSearch.Models.ValidatedURL> listValidatedURL = new List<ReviewUrlSearch.Models.ValidatedURL>();

                    int iCounter = 1;
                    foreach (var reviewURL in listReviewURL)
                    {
                        if (!string.IsNullOrEmpty(reviewURL.URL))
                        {
                            ValidatedURL validatedURL = new ValidatedURL();
                            SI.DTO.ReviewSourceEnum providerName = (SI.DTO.ReviewSourceEnum)Enum.Parse(typeof(SI.DTO.ReviewSourceEnum), reviewURL.ReviewSourceName);
                            var connection = ProviderFactory.Validator.GetValidatorConnection(providerName);
                            testvalidator = connection.ValidateReviewURL(reviewURL.URL, accountInfo);


                            validatedURL.ReviewSourceID = Convert.ToInt32(providerName);
                            validatedURL.ReviewSourceName = providerName.ToString();
                            if (testvalidator.MismatchCount > 0)
                                validatedURL.isValid = false;
                            else
                                validatedURL.isValid = true;

                            validatedURL.URL = reviewURL.URL;
                            validatedURL.MismatchCount = testvalidator.MismatchCount;

                            listValidatedURL.Add(validatedURL);

                            //var query =
                            //from ss in testvalidator.ValidationMisMatch
                            //where ss.property == testvalidator.MisMatchesList
                            //select ss.Value;

                            Debugger.Log(0, null, Environment.NewLine);
                            Debug.WriteLine(iCounter + " " + providerName.ToString());
                            Debug.WriteLine("Review URL: " + reviewURL.URL);
                            foreach (var item in testvalidator.ValidationMisMatch)
                            {
                                Debug.WriteLine("   Field: " + item.FieldName);
                                Debug.WriteLine("       Page: " + item.PageValue);
                                Debug.WriteLine("       Account: " + item.ValidatorValue);
                            }

                            iCounter++;

                        }
                    }
                }
                #endregion

                if (testNumber < 15) //Test review connectors
                {

                    System.Console.WriteLine();
                    System.Console.WriteLine(element);
                    System.Console.WriteLine(strURL);
                    foreach (PropertyDescriptor descriptor in TypeDescriptor.GetProperties(testresult))
                    {
                        string name = descriptor.Name;
                        object value = descriptor.GetValue(testresult);
                        Console.WriteLine("{0}={1}", name, value);
                    }

                    System.Console.WriteLine();
                    System.Console.WriteLine("Details");
                    foreach (PropertyDescriptor descriptor in TypeDescriptor.GetProperties(testresult.ReviewDetails))
                    {
                        string name = descriptor.Name;
                        object value = descriptor.GetValue(testresult.ReviewDetails);
                        Console.WriteLine("{0}={1}", name, value);
                    }

                    System.Console.WriteLine();
                    System.Console.WriteLine("FailureReasons");
                    foreach (PropertyDescriptor descriptor in TypeDescriptor.GetProperties(testresult.FailureReasons))
                    {
                        string name = descriptor.Name;
                        object value = descriptor.GetValue(testresult.FailureReasons);
                        Console.WriteLine("{0}={1}", name, value);
                    }
                    Console.ReadLine();
                }

                else if (testNumber < 40)  //Validators
                {
                    System.Console.WriteLine();
                    System.Console.WriteLine(element);
                    //System.Console.WriteLine(strURL);
                    foreach (PropertyDescriptor descriptor in TypeDescriptor.GetProperties(testvalidator))
                    {
                        string name = descriptor.Name;
                        object value = descriptor.GetValue(testvalidator);
                        Console.WriteLine("{0}={1}", name, value);
                    }
                    System.Console.WriteLine();
                    System.Console.WriteLine("Details");
                    foreach (PropertyDescriptor descriptor in TypeDescriptor.GetProperties(testvalidator.AccountDetails))
                    {
                        string name = descriptor.Name;
                        object value = descriptor.GetValue(testvalidator.AccountDetails);
                        Console.WriteLine("{0}={1}", name, value);
                    }
                    System.Console.WriteLine();
                    System.Console.WriteLine("ValidationMisMatch");
                    foreach (PropertyDescriptor descriptor in TypeDescriptor.GetProperties(testvalidator.ValidationMisMatch))
                    {
                        string name = descriptor.Name;
                        object value = descriptor.GetValue(testvalidator.ValidationMisMatch);
                        Console.WriteLine("{0}={1}", name, value);
                    }
                    Console.ReadLine();
                    //Console.ReadKey(true);
                }
                else
                {

                }

            }
        }
    }

    enum TestType
    {
        ConnectionOnly = 1,
        Processor = 2
    }
}
