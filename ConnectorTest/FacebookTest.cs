﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using Connectors.Entities;
using Connectors.Parameters;
using Connectors.Social;
using Facebook.Entities;

namespace ConnectorTest
{
    class FacebookTest
    {
        private static void Main(string[] args)
        {
            var uniqueID = "255547016773";
            var token = "CAAGfD25ViIcBAGvALQINSVnHOp5TkuEhEGxeTLsueyTOpSBZCJb6xS4ECmpYQDRqT8NcIt2iTw0oHDgMH1x4vwHRBkFnmezcf2bPH1yt66b0zCoi2FRD3CWwLNgwTvEGhorr7Vi2ZBas7uAx9n0ajZAWMZAfVxcZBMO83dmNljOZBcf83qlgri";
            var fromDate = "02-25-2014";
            var toDate = "02-27-2014";
            var limit = "";
            var postID = "266189080102001_541788679261667";
            var commentID = "685554498154663_2096830"; //Via Response

            //CreateComment(postID, token, "comment test dave");
            //CreateCommentonComment(commentID, token, "My comment - Level 2");
            //DeleteComment(commentID, token);
            //CreateLikeonPost(postID, token);
            //DeleteLikeonPost(postID, token);
            //CreateLikeonComment(commentID, token);
            //DeleteLikeonComment(commentID, token);

            #region Testing Facebook Connections
            //var streamParameters = new SIFacebookStreamParameters();
            //streamParameters.UniqueID = "266189080102001";
            //streamParameters.Token = "CAAGfD25ViIcBAAn5MZAvOA9m2kNpz9c7Qr3ylNFT4UfecJ3cu4b7KEpVHHgVIClR0YfIj7ILbxkd2eJNResSohf2gXcPGh9SS7R00iJ68UBIR0EVF62gQz5BpZByaEZBgUkEr1zZBO7Bd6WSsIqLkFd53qbwlAJMkdCvvBGKQvU61APrERmz";
            //streamParameters.PostID = "266189080102001_541788679261667";
            //streamParameters.CommentID = "685554498154663_2096830";
            //streamParameters.Comment = "comment test dave 2";
            
            //var connectionParameters = new SIFacebookStreamConnectionParameters(streamParameters, 30, false, 1);
            //var fbConnection = new FacebookStreamConnection(connectionParameters);
            //fbConnection.CreateCommentonPost();
            #endregion

            //#region Testing Facebook Reviews

            //GetReviews(uniqueID, token);

            //#endregion

            #region Testing Facebook Messages

            //GetMessages(uniqueID, token);
            UpdateCoverPhoto();

            #endregion

        }

        #region Testing FacebookSDK directly

        private static void UpdateCoverPhoto()
        {
            //https://www.facebook.com/developmentmotors
            var uniqueID = "266189080102001"; //Development Motors
            var token = "CAAGfD25ViIcBAAn5MZAvOA9m2kNpz9c7Qr3ylNFT4UfecJ3cu4b7KEpVHHgVIClR0YfIj7ILbxkd2eJNResSohf2gXcPGh9SS7R00iJ68UBIR0EVF62gQz5BpZByaEZBgUkEr1zZBO7Bd6WSsIqLkFd53qbwlAJMkdCvvBGKQvU61APrERmz";
            var photoID = "616681745052731";
            var yOffset = "10";
            var suppressStoryPost = "true";
            

            Stopwatch sw = new Stopwatch();
            sw.Start();

            var result = Facebook.FacebookAPI.FacebookChangeCoverPhoto(uniqueID, photoID, token, yOffset, suppressStoryPost);

            //foreach (var reviewItem in result.ReviewList.data)
            //{
            //    if (reviewItem.has_review.ToLower() == "true")
            //    {
            //        Debug.WriteLine("Rating: {0}, Review: {1}", reviewItem.rating, reviewItem.review_text.Substring(1,10));
            //    }
            //    else
            //    {
            //        Debug.WriteLine("Rating: " + reviewItem.rating);
            //    }

            //}

            Debug.WriteLine("Create Comment: {0}ms to Process", sw.ElapsedMilliseconds);
        }

        private static void GetReviews(string uniqueID, string token)
        {
            Stopwatch sw = new Stopwatch();
            sw.Start();

            var result = Facebook.FacebookAPI.GetReviews(uniqueID, token);

            //foreach (var reviewItem in result.ReviewList.data)
            //{
            //    if (reviewItem.has_review.ToLower() == "true")
            //    {
            //        Debug.WriteLine("Rating: {0}, Review: {1}", reviewItem.rating, reviewItem.review_text.Substring(1,10));
            //    }
            //    else
            //    {
            //        Debug.WriteLine("Rating: " + reviewItem.rating);
            //    }
                
            //}

            Debug.WriteLine("Create Comment: {0}ms to Process", sw.ElapsedMilliseconds);
        }

        private static void GetMessages(string uniqueID, string token)
        {
            Stopwatch sw = new Stopwatch();
            sw.Start();

            var result = Facebook.FacebookAPI.GetMessages(uniqueID, token);

            //foreach (var reviewItem in result.ReviewList.data)
            //{
            //    if (reviewItem.has_review.ToLower() == "true")
            //    {
            //        Debug.WriteLine("Rating: {0}, Review: {1}", reviewItem.rating, reviewItem.review_text.Substring(1,10));
            //    }
            //    else
            //    {
            //        Debug.WriteLine("Rating: " + reviewItem.rating);
            //    }

            //}

            Debug.WriteLine("Get Messages: {0}ms to Process", sw.ElapsedMilliseconds);
        }

        private static void CreateComment(string postID, string token, string comment)
        {
            Stopwatch sw = new Stopwatch();
            sw.Start();

            Facebook.FacebookAPI.CreateCommentOnPost(postID, token, comment);

            Debug.WriteLine("Create Comment: {0}ms to Process", sw.ElapsedMilliseconds);
        }

        private static void DeleteComment(string commentID, string token)
        {
            Stopwatch sw = new Stopwatch();
            sw.Start();

            Facebook.FacebookAPI.DeleteCommentOnPost(commentID, token);

            Debug.WriteLine("Delete Comment: {0}ms to Process", sw.ElapsedMilliseconds);
        }

        private static void CreateLikeonPost(string postID, string token)
        {
            Stopwatch sw = new Stopwatch();
            sw.Start();

            Facebook.FacebookAPI.LikePost(postID, token);

            Debug.WriteLine("Create Like on Post: {0}ms to Process", sw.ElapsedMilliseconds);
        }

        private static void DeleteLikeonPost(string postID, string token)
        {
            Stopwatch sw = new Stopwatch();
            sw.Start();

            Facebook.FacebookAPI.UnLikePost(postID, token);

            Debug.WriteLine("Delete Like on Post: {0}ms to Process", sw.ElapsedMilliseconds);
        }

        private static void CreateLikeonComment(string commentID, string token)
        {
            Stopwatch sw = new Stopwatch();
            sw.Start();

            Facebook.FacebookAPI.LikeComment(commentID, token);

            Debug.WriteLine("Create Comment: {0}ms to Process", sw.ElapsedMilliseconds);
        }

        private static void DeleteLikeonComment(string commentID, string token)
        {
            Stopwatch sw = new Stopwatch();
            sw.Start();

            Facebook.FacebookAPI.UnLikeComment(commentID, token);

            Debug.WriteLine("Delete Like on Comment: {0}ms to Process", sw.ElapsedMilliseconds);
        }

        private static void CreateCommentonComment(string commentID, string token, string comment)
        {
            Stopwatch sw = new Stopwatch();
            sw.Start();

            Facebook.FacebookAPI.CreateCommentOnComment(commentID, token, comment);

            Debug.WriteLine("Create Comment: {0}ms to Process", sw.ElapsedMilliseconds);
        }
        #endregion

        


        
        
    }
}
