﻿using JobLib;
using SI;
using SI.BLL;
using SI.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Processor.ReviewDownloadQueuer
{
    public class Processor : ProcessorBase<QueueReviewDownloadsData>
    {
        // Need to pass the args through for the default constructor
        public Processor(string[] args) : base(args) { }

        protected override void start()
        {
            Auditor
               .New(AuditLevelEnum.Information, AuditTypeEnum.General, new UserInfoDTO(1, null), "ReviewDownloadQueuer starting")
               .Save(ProviderFactory.Logging)
            ;
            JobDTO job = getNextJob();

            if (job == null)
            {
                Auditor
                   .New(AuditLevelEnum.Information, AuditTypeEnum.JobFailure, new UserInfoDTO(1, null), "ReviewDownloadQueuer found NULL for getNextJob() first call!")
                   .Save(ProviderFactory.Logging)
                ;
            }

            while (job != null)
            {
                try
                {
                    setJobStatus(job.ID.Value, JobStatusEnum.Started);
                    QueueReviewDownloadsData data = getJobData(job.ID.Value);

                    Auditor
                        .New(AuditLevelEnum.Information, AuditTypeEnum.General, new UserInfoDTO(1, null),
                                "ReviewDownloadQueuer Queueing jobs for {0} sources.  Details: {1}. Spread: {2} min. Priority: {3}",
                                data.ReviewSources.Count(), data.DownloadDetails, data.JobSpreadMinutes, data.DownloaderJobPriority
                            )
                        .Save(ProviderFactory.Logging)
                    ;

                    ProviderFactory.Reviews.QueueReviewDownloaderJobs(
                        new UserInfoDTO(1, null), data.DownloadDetails,
                        data.JobSpreadMinutes,
                        data.ReviewSources, 0, JobStatusEnum.Queued, data.DownloaderJobPriority
                    );

                    Auditor
                       .New(AuditLevelEnum.Information, AuditTypeEnum.General, new UserInfoDTO(1, null), "ReviewDownloadQueuer finished call to BLL: QueueReviewDownloaderJobs")
                       .Save(ProviderFactory.Logging)
                    ;

                    setJobStatus(job.ID.Value, JobStatusEnum.Completed);

                }
                catch (Exception ex)
                {
                    setJobStatus(job.ID.Value, JobStatusEnum.Failed);
                    ProviderFactory.Logging.LogException(ex);
                    Auditor
                        .New(AuditLevelEnum.Exception, AuditTypeEnum.ProcessorException, new UserInfoDTO(1, null), ex.Message)
                        .Save(ProviderFactory.Logging)
                    ;

                }

                job = getNextJob();
            }
        }
    }
}
