﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AWS;
using Connectors.Social;
using Connectors.Social.Parameters;
using Google.Model;
using JobLib;
using Newtonsoft.Json;
using SI.BLL;
using SI.DTO;
using SI;
using System.Web;
using Google;
using System.Diagnostics;

namespace Processor.Social.GooglePlus
{
    public class Processor : ProcessorBase<GooglePlusDownloaderData>
    {
        private const string prefix = "AWS.";

        // Need to pass the args through for the default constructor
        public Processor(string[] args) : base(args) { }

        //static void Main(string[] args)
        protected override void start()
        {
            string accessKey = ProviderFactory.Reviews.GetProcessorSetting(prefix + "AWSAccessKey");
            string secretKey = ProviderFactory.Reviews.GetProcessorSetting(prefix + "AWSSecretKey");
            string serviceURL = ProviderFactory.Reviews.GetProcessorSetting(prefix + "DynamoDBServiceURL");
            string table = ProviderFactory.Reviews.GetProcessorSetting(prefix + "DynamoDBSocialRawTable");

            DynamoDB dynamoDB = new DynamoDB(accessKey, secretKey, serviceURL, table);

            JobDTO job = getNextJob();

            while (job != null)
            {
                try
                {
                    setJobStatus(job.ID.Value, JobStatusEnum.Started);
                    GooglePlusDownloaderData data = getJobData(job.ID.Value);

                    SocialCredentialDTO socialCredentialsDTO = null;
                    if (data.CredentialID.HasValue)
                        socialCredentialsDTO = ProviderFactory.Social.GetSocialCredentialInfo(data.CredentialID.Value);

                    switch (data.GooglePlusEnum)
                    {
                        case GooglePlusProcessorActionEnum.GooglePlusPageInformationDownloder:
                            {
                                #region PAGE INSIGHTS

                                //Call Connection Class to get data from SDK   
                                SIGooglePlusPageParameters pageParameters = new SIGooglePlusPageParameters();

                                pageParameters.UniqueID = socialCredentialsDTO.UniqueID;                                
                                pageParameters.apiKey = "AIzaSyD6EAaOZy0_JJ8sDw_DqfeAU4J6-q69-28";

                                SIGooglePlusPageConnectionParameters ConnectionParameters = new SIGooglePlusPageConnectionParameters(pageParameters);
                                GooglePlusPageConnection pageconnection = new GooglePlusPageConnection(ConnectionParameters);

                                GooglePlusPageStatistics objGooglePlusPageStatistics = pageconnection.GetPageInformation();

                                if (string.IsNullOrWhiteSpace(objGooglePlusPageStatistics.Error))
                                {
                                    string serializeValue = JsonConvert.SerializeObject(objGooglePlusPageStatistics);

                                    var dynoDbResult = dynamoDB.PutValue(serializeValue);

                                    if (dynoDbResult.IsSuccess)
                                    {
                                        GooglePlusIntegratorData integratorData = new GooglePlusIntegratorData()
                                        {
                                            GooglePlusEnum = GooglePlusProcessorActionEnum.GooglePlusPageInformationIntegrator,
                                            Key = dynoDbResult.Key,
                                            CredentialID = data.CredentialID.Value                                            
                                        };

                                        JobDTO jobIntegrator = new JobDTO()
                                        {
                                            JobDataObject = integratorData,
                                            JobType = integratorData.JobType,
                                            DateScheduled = new SIDateTime(DateTime.UtcNow, TimeZoneInfo.Utc),
                                            JobStatus = JobStatusEnum.Queued,
                                            Priority = 300,
                                            OriginJobID = job.ID
                                        };

                                        SaveEntityDTO<JobDTO> saveInfo = ProviderFactory.Jobs.Save(new SaveEntityDTO<JobDTO>(jobIntegrator, UserInfoDTO.System));
                                        Console.WriteLine(saveInfo.Entity.ID);

                                        setJobStatus(job.ID.Value, JobStatusEnum.Completed);

                                    }
                                    else
                                    {
                                        setJobStatus(job.ID.Value, JobStatusEnum.Failed);
                                        ProviderFactory.Logging.Audit(AuditLevelEnum.Warning, AuditTypeEnum.GooglePlusDownloaderFailure,
                                                                      string.Format("GooglePlus Page Information could not be saved to DynamoDB, CredentialID: {0}. JobID {1}",
                                                                                     data.CredentialID, job.ID), null, null, null, data.CredentialID);
                                    }
                                }
                                else
                                {
                                    //Log Error
                                }

                                #endregion
                            }
                            break;
                        case GooglePlusProcessorActionEnum.GooglePlusPostStatisticsDownloder:
                            {
                                // we should mark the post's DateLastRefreshed
                                ProviderFactory.Social.MarkGooglePlusPostRefreshed(data.PostID);

                                #region POST STATISTICS
                                //TODO: Get UniqueID, Token PostID (ResultID) and FeedID from New Table.
                                GooglePlusPostInfoDTO info = ProviderFactory.Social.GetGooglePlusPostInfo(data.PostID);

                                SIGooglePlusPostStatisticsParameters postParameters = new SIGooglePlusPostStatisticsParameters();
                                postParameters.activityID = info.GooglePlusPostID;
                                postParameters.apiKey = "AIzaSyD6EAaOZy0_JJ8sDw_DqfeAU4J6-q69-28";


                                SIGooglePlusPostStatisticsConnectionParameters postConnectionParameters = new SIGooglePlusPostStatisticsConnectionParameters(postParameters);
                                GooglePlusPostStatisticConnection postconnection = new GooglePlusPostStatisticConnection(postConnectionParameters);

                                //GooglePlusAddorUpdateorGetSpecificActivityResponse objGooglePlusAddorUpdateorGetSpecificActivityResponse = postconnection.GetGooglePlusActivityByActivityID();

                                //GooglePlusGetCommentsListResponse objGooglePlusGetCommentsListResponse = postconnection.GetGooglePlusCommentsListForAnActivity();

                                GooglePlusPostStatistics googlePlusPostStatistics = new GooglePlusPostStatistics();

                                googlePlusPostStatistics.ActivityInfo = postconnection.GetGooglePlusActivityByActivityID();

                                googlePlusPostStatistics.Comments = postconnection.GetGooglePlusCommentsListForAnActivity();

                                googlePlusPostStatistics.Plusoners = postconnection.GetGooglePlusPlusonersListForAnActivity();

                                googlePlusPostStatistics.Resharers = postconnection.GetGooglePlusResharersListForAnActivity();

                                string serializeValue = JsonConvert.SerializeObject(googlePlusPostStatistics);

                                var dynoDbResult = dynamoDB.PutValue(serializeValue);

                                #region Save to DynamoDB & Queue Next job

                                if (dynoDbResult.IsSuccess)
                                {
                                    GooglePlusIntegratorData integratorData = new GooglePlusIntegratorData()
                                    {
                                        GooglePlusEnum = GooglePlusProcessorActionEnum.GooglePlusPostStatisticsIntegrator,
                                        Key = dynoDbResult.Key,
                                        CredentialID = info.CredentialID,                                        
                                        GooglePlusPostID = data.PostID
                                    };

                                    JobDTO jobIntegrator = new JobDTO()
                                    {
                                        JobDataObject = integratorData,
                                        JobType = integratorData.JobType,
                                        DateScheduled = new SIDateTime(DateTime.UtcNow, TimeZoneInfo.Utc),
                                        JobStatus = JobStatusEnum.Queued,
                                        Priority = 300,
                                        OriginJobID = job.ID
                                    };

                                    SaveEntityDTO<JobDTO> saveInfo = ProviderFactory.Jobs.Save(new SaveEntityDTO<JobDTO>(jobIntegrator, UserInfoDTO.System));
                                    Console.WriteLine(saveInfo.Entity.ID);

                                    setJobStatus(job.ID.Value, JobStatusEnum.Completed);
                                }
                                else
                                {
                                    setJobStatus(job.ID.Value, JobStatusEnum.Failed);
                                    ProviderFactory.Logging.Audit(AuditLevelEnum.Warning, AuditTypeEnum.FacebookDownloaderFailure,
                                                                  string.Format("GooglePlus Post could not be saved to DynamoDB, ResultID: {0}, CredentialID: {1}. JobID {2}",
                                                                                info.ResultID, info.CredentialID, job.ID), null, null, null, info.CredentialID);
                                }

                                #endregion

                                #endregion
                            }
                            break;
                        case GooglePlusProcessorActionEnum.GooglePlusPublish:
                            {
                                #region Publish

                                Auditor
                                    .New(AuditLevelEnum.Information, AuditTypeEnum.PublishActivity, UserInfoDTO.System, "")
                                    .SetAuditDataObject(
                                        AuditPublishActivity
                                        .New(data.PostID, AuditPublishActivityTypeEnum.PublishProcessorEntry)
                                        .SetPostID(data.PostID)
                                        .SetPostTargetID(data.PostTargetID)
                                        .SetPostImageID(data.PostImageID)
                                        .SetJobID(job.ID)
                                    )
                                    .Save(ProviderFactory.Logging)
                                ;

                                string PublishImageUploadBaseURL = Settings.GetSetting("site.PublishImageUploadURL");

                                PostTargetPublishInfoDTO info = ProviderFactory.Social.GetPostTargetPublishInfo(data.PostTargetID, data.PostImageID);
                                socialCredentialsDTO = ProviderFactory.Social.GetSocialCredentialInfo(info.CredentialID);

                                #region GET ACCESS TOKEN

                                //Call Connection Class to get data from SDK   
                                SIGooglePlusPageParameters pageParameters = new SIGooglePlusPageParameters();

                                pageParameters.UniqueID = info.UniqueID;
                                pageParameters.client_id = socialCredentialsDTO.AppID;
                                pageParameters.client_secret = socialCredentialsDTO.AppSecret;
                                pageParameters.refresh_token = info.Token;
                                pageParameters.grant_type = "refresh_token";


                                // Get Access Token                                
                                string access_token = string.Empty;

                                SIGooglePlusPageConnectionParameters ConnectionParameters = new SIGooglePlusPageConnectionParameters(pageParameters);
                                GooglePlusPageConnection pageconnection = new GooglePlusPageConnection(ConnectionParameters);

                                GooglePlusAccessTokenRequestResponse objGooglePlusAccessTokenRequestResponse = pageconnection.GetGooglePlusAccessToken();

                                access_token = objGooglePlusAccessTokenRequestResponse.access_token;

                                if (string.IsNullOrWhiteSpace(access_token))
                                {
                                    Auditor
                                        .New(AuditLevelEnum.Error, AuditTypeEnum.PublishFailure, new UserInfoDTO(info.UserID, null), "Google+ publish failed.")
                                        .SetAccountID(socialCredentialsDTO.AccountID)
                                        .SetCredentialID(info.CredentialID)
                                        .SetAuditDataObject(new AuditPublishFailure()
                                        {
                                            JobID = job.ID.Value,
                                            PostImageID = data.PostImageID.GetValueOrDefault(0),
                                            PostTargetID = data.PostTargetID,
                                            SocialNetworkResponse = "Not able to Get Access Toke, Google Response: " + objGooglePlusAccessTokenRequestResponse.errorResponse.error.ToString(),
                                            CredentialID = socialCredentialsDTO != null ? socialCredentialsDTO.ID : 0,
                                            AccountID = socialCredentialsDTO != null ? socialCredentialsDTO.AccountID : 0,
                                            SocialAppID = socialCredentialsDTO != null ? socialCredentialsDTO.SocialAppID : 0,
                                            SocialNetworkID = socialCredentialsDTO != null ? socialCredentialsDTO.SocialNetworkID : 0
                                        })
                                        .Save(ProviderFactory.Logging);

                                    ProviderFactory.Social.SetPostTargetFailed(data.PostTargetID);

                                    setJobStatus(job.ID.Value, JobStatusEnum.Failed);
                                    break;
                                }
                                #endregion

                                //TODO: Call BLL to Get Post from Post Target Table or Read from Jobs Table
                                SIGooglePlusPublishParameters publishParameters = new SIGooglePlusPublishParameters();
                                if (data.ReferencePostTargetID.HasValue && info.PostType == PostTypeEnum.Photo)
                                {
                                    PostTargetPublishInfoDTO referenceinfo = ProviderFactory.Social.GetPostTargetPublishInfo(data.ReferencePostTargetID.Value, null);

                                    SocialCredentialDTO referencesocialCredentialsDTO = ProviderFactory.Social.GetSocialCredentialInfo(referenceinfo.CredentialID);

                                    //Get PostImageResults by PostTargetID
                                    //List<PostImageResultDTO> listPostImageResultDTO = ProviderFactory.Social.GetPostImageResulInfo(data.ReferencePostTargetID.Value);
                                    //PostImageResultDTO postImageResultDTO = ProviderFactory.Social.GetPostImageResulInfo(data.ReferencePostTargetID.Value).FirstOrDefault();
                                    PostImageDTO postImageDTO = ProviderFactory.Social.GetPostImageInfo(referenceinfo.PostID);
                                    if (postImageDTO != null)
                                    {               
                                        publishParameters.pathToImage = PublishImageUploadBaseURL + postImageDTO.URL;
                                    }

                                    //StringBuilder stb = new StringBuilder();
                                    //foreach (var postImageResultDTO in listPostImageResultDTO)
                                    //{
                                    //    if (postImageResultDTO.ResultID != null)
                                    //    {
                                    //        string ResultID = postImageResultDTO.ResultID.ToString();
                                    //        string FacebookPostURL = string.Empty;

                                    //        if (ResultID.Contains("_"))
                                    //        {
                                    //            string[] ids = ResultID.Split('_');

                                    //            FacebookPostURL = "http://www.facebook.com/" + ids[0] + "/posts/" + ids[1];
                                    //        }
                                    //        else
                                    //        {
                                    //            FacebookPostURL = "http://www.facebook.com/" + ResultID;
                                    //        }

                                    //        stb.AppendLine(FacebookPostURL);
                                    //    }
                                    //}
                                    
                                    publishParameters.UniqueID = info.UniqueID;
                                    publishParameters.accessToken = access_token;
                                    publishParameters.accessType = "public";
                                    publishParameters.objectType = string.Empty;
                                    publishParameters.activityType = string.Empty;
                                    publishParameters.Content = referenceinfo.Message;
                                    publishParameters.attachmentID = string.Empty;
                                    publishParameters.circleID = string.Empty;
                                    publishParameters.userID = string.Empty;
                                    //publishParameters.postType = PostTypeEnum.Status;

                                    //if (stb.ToString().Length > 0)
                                    //{
                                    //    publishParameters.Content = publishParameters.Content  + Environment.NewLine + stb.ToString();
                                    //}                                  
                                   
                                }
                                else
                                {                                    
                                    publishParameters.UniqueID = info.UniqueID;
                                    publishParameters.accessToken = access_token;
                                    publishParameters.accessType = "public";
                                    publishParameters.objectType = string.Empty;
                                    publishParameters.activityType = string.Empty;
                                    publishParameters.Content = info.Message;
                                    publishParameters.attachmentID = string.Empty;
                                    publishParameters.circleID = string.Empty;
                                    publishParameters.userID = string.Empty;
                                    //publishParameters.postType = info.PostType;

                                    if (info.PostType == PostTypeEnum.Photo)
                                    {
                                        publishParameters.pathToImage = PublishImageUploadBaseURL + info.ImageURL;
                                    }
                                    else if (info.PostType == PostTypeEnum.Link)
                                    {
                                        publishParameters.pathToImage = string.Empty;
                                        publishParameters.postURL = info.Link;
                                    }
                                    else if (info.PostType == PostTypeEnum.Status)
                                    {
                                        publishParameters.pathToImage = string.Empty;
                                        publishParameters.postURL = string.Empty;
                                    }

                                    
                                }

                                SIGooglePlusPublishConnectionParameters publishConnectionParameters = new SIGooglePlusPublishConnectionParameters(publishParameters);
                                GooglePlusPublishConnection publishconnection = new GooglePlusPublishConnection(publishConnectionParameters);

                                switch (info.PostType)
                                {
                                    case PostTypeEnum.Status:
                                        {
                                            #region Status

                                            ProviderFactory.Social.SetPostTargetJobID(data.PostTargetID, job.ID.Value);
                                            GooglePlusPostResponse objGooglePlusPostResponse = publishconnection.CreateGooglePlusActivity();

                                            if (!string.IsNullOrWhiteSpace(objGooglePlusPostResponse.postURL))
                                            {
                                                ProviderFactory.Social.SetPostTargetResults(data.PostTargetID, objGooglePlusPostResponse.postURL, objGooglePlusPostResponse.finalActivityresponse.id);
                                                setJobStatus(job.ID.Value, JobStatusEnum.Completed);
                                            }
                                            else
                                            {
                                                Auditor
                                                    .New(AuditLevelEnum.Error, AuditTypeEnum.PublishFailure, new UserInfoDTO(info.UserID, null), "Google+ publish failed.")
                                                    .SetAccountID(socialCredentialsDTO.AccountID)
                                                    .SetCredentialID(info.CredentialID)
                                                    .SetAuditDataObject(new AuditPublishFailure()
                                                    {
                                                        JobID = job.ID.Value,
                                                        PostImageID = data.PostImageID.GetValueOrDefault(0),
                                                        PostTargetID = data.PostTargetID,
                                                        SocialNetworkResponse = objGooglePlusPostResponse.error.ToString(),
                                                        CredentialID = socialCredentialsDTO != null ? socialCredentialsDTO.ID : 0,
                                                        AccountID = socialCredentialsDTO != null ? socialCredentialsDTO.AccountID : 0,
                                                        SocialAppID = socialCredentialsDTO != null ? socialCredentialsDTO.SocialAppID : 0,
                                                        SocialNetworkID = socialCredentialsDTO != null ? socialCredentialsDTO.SocialNetworkID : 0
                                                    })
                                                    .Save(ProviderFactory.Logging)
                                                ;

                                                //if (data.RemainingRetries == 0)
                                                {
                                                    // set failed in post target
                                                    ProviderFactory.Social.SetPostTargetFailed(data.PostTargetID);
                                                }

                                                setJobStatus(job.ID.Value, JobStatusEnum.Failed);
                                            }

                                            #endregion
                                        }
                                        break;
                                    case PostTypeEnum.Link:
                                        {
                                            #region Link

                                            ProviderFactory.Social.SetPostTargetJobID(data.PostTargetID, job.ID.Value);
                                            GooglePlusPostResponse objGooglePlusPostResponse = publishconnection.CreateGooglePlusActivity();

                                            if (!string.IsNullOrWhiteSpace(objGooglePlusPostResponse.postURL))
                                            {
                                                ProviderFactory.Social.SetPostTargetResults(data.PostTargetID, objGooglePlusPostResponse.postURL, objGooglePlusPostResponse.finalActivityresponse.id);
                                                setJobStatus(job.ID.Value, JobStatusEnum.Completed);
                                            }
                                            else
                                            {
                                                Auditor
                                                     .New(AuditLevelEnum.Error, AuditTypeEnum.PublishFailure, new UserInfoDTO(info.UserID, null), "Google+ publish failed.")
                                                     .SetAccountID(socialCredentialsDTO.AccountID)
                                                     .SetCredentialID(info.CredentialID)
                                                     .SetAuditDataObject(new AuditPublishFailure()
                                                     {
                                                         JobID = job.ID.Value,
                                                         PostImageID = data.PostImageID.GetValueOrDefault(0),
                                                         PostTargetID = data.PostTargetID,
                                                         SocialNetworkResponse = objGooglePlusPostResponse.error.ToString(),
                                                         CredentialID = socialCredentialsDTO != null ? socialCredentialsDTO.ID : 0,
                                                         AccountID = socialCredentialsDTO != null ? socialCredentialsDTO.AccountID : 0,
                                                         SocialAppID = socialCredentialsDTO != null ? socialCredentialsDTO.SocialAppID : 0,
                                                         SocialNetworkID = socialCredentialsDTO != null ? socialCredentialsDTO.SocialNetworkID : 0
                                                     })
                                                     .Save(ProviderFactory.Logging)
                                                ;

                                                //if (data.RemainingRetries == 0)
                                                {
                                                    // set failed in post target
                                                    ProviderFactory.Social.SetPostTargetFailed(data.PostTargetID);
                                                }

                                                setJobStatus(job.ID.Value, JobStatusEnum.Failed);
                                            }
                                            #endregion
                                        }
                                        break;
                                    case PostTypeEnum.Photo:
                                        {
                                            #region Photo

                                            ProviderFactory.Social.SetPostTargetJobID(data.PostTargetID, job.ID.Value);

                                            //ProviderFactory.Social.SetPostImageJobID(data.PostImageID.Value, data.PostTargetID, job.ID.Value);

                                            GooglePlusPostResponse objGooglePlusPostResponse = publishconnection.PostGooglePlusPhoto();

                                            if (!string.IsNullOrWhiteSpace(objGooglePlusPostResponse.postURL))
                                            {
                                                ProviderFactory.Social.SetPostTargetResults(data.PostTargetID, objGooglePlusPostResponse.postURL, objGooglePlusPostResponse.finalActivityresponse.id);
                                                //ProviderFactory.Social.SetPostImageResults(data.PostImageID.Value, data.PostTargetID, objGooglePlusPostResponse.postURL, objGooglePlusPostResponse.finalActivityresponse.id);
                                                setJobStatus(job.ID.Value, JobStatusEnum.Completed);
                                            }
                                            else
                                            {
                                                Auditor
                                                    .New(AuditLevelEnum.Error, AuditTypeEnum.PublishFailure, new UserInfoDTO(info.UserID, null), "Google+ publish failed.")
                                                    .SetAccountID(socialCredentialsDTO.AccountID)
                                                    .SetCredentialID(info.CredentialID)
                                                    .SetAuditDataObject(new AuditPublishFailure()
                                                    {
                                                        JobID = job.ID.Value,
                                                        PostImageID = data.PostImageID.GetValueOrDefault(0),
                                                        PostTargetID = data.PostTargetID,
                                                        SocialNetworkResponse = objGooglePlusPostResponse.error.ToString(),
                                                        CredentialID = socialCredentialsDTO != null ? socialCredentialsDTO.ID : 0,
                                                        AccountID = socialCredentialsDTO != null ? socialCredentialsDTO.AccountID : 0,
                                                        SocialAppID = socialCredentialsDTO != null ? socialCredentialsDTO.SocialAppID : 0,
                                                        SocialNetworkID = socialCredentialsDTO != null ? socialCredentialsDTO.SocialNetworkID : 0
                                                    })
                                                    .Save(ProviderFactory.Logging);

                                                //if (data.RemainingRetries == 0)
                                                {
                                                    // set failed in post target
                                                    ProviderFactory.Social.SetPostTargetFailed(data.PostTargetID);
                                                }

                                                setJobStatus(job.ID.Value, JobStatusEnum.Failed);
                                            }

                                            #endregion
                                        }
                                        break;
                                }
                                #endregion
                            }
                            break;
                        case GooglePlusProcessorActionEnum.GooglePlusPostDownLoader:
                            {
                                #region Post DownLoader
                                if (socialCredentialsDTO != null)
                                {
                                    SIGooglePlusPageParameters pageParameters = new SIGooglePlusPageParameters();
                                    pageParameters.UniqueID = socialCredentialsDTO.UniqueID;
                                    pageParameters.apiKey = "AIzaSyD6EAaOZy0_JJ8sDw_DqfeAU4J6-q69-28";

                                    SIGooglePlusPageConnectionParameters ConnectionParameters = new SIGooglePlusPageConnectionParameters(pageParameters);
                                    GooglePlusPageConnection pageconnection = new GooglePlusPageConnection(ConnectionParameters);

                                    GooglePlusGetActivityListByPageResponse posts = pageconnection.GetGooglePlusActivitiesListForAPage();

                                    if (string.IsNullOrEmpty(posts.error.message))
                                    {
                                        List<GooglePlusPostDTO> listgooglePlusPostDTO = new List<GooglePlusPostDTO>();
                                        int icounter = 1;

                                        foreach (ActivityItem post in posts.items)
                                        {
                                            //if (icounter == 150)
                                            //{
                                            //    int a = 1;
                                            //}
                                            GooglePlusPostDTO googlePlusPostDTO = new GooglePlusPostDTO();

                                            //ToDo Add dynmoDB 

                                            string posttype = "status";
                                            string LinkURL = string.Empty;
                                            string PictureURL = string.Empty;
                                            string PostMessage = string.Empty;

                                            foreach (var item in (dynamic)post.@object)
                                            {
                                                string key = item.Key;

                                                if (key.ToLower() == "content")
                                                {
                                                    
                                                    PostMessage = HttpUtility.HtmlDecode(item.Value);                                                    

                                                    // GooglePlus Adding one extra char "?" to then end of string 
                                                    //var a = Encoding.ASCII.GetBytes(PostMessage);

                                                    if (!string.IsNullOrWhiteSpace(PostMessage))
                                                    {
                                                        PostMessage = PostMessage.Substring(0, PostMessage.Length - 1);
                                                    }
                                                    //a = Encoding.ASCII.GetBytes(PostMessage);
                                                    
                                                }

                                                if (key.ToLower() == "attachments")
                                                {
                                                    foreach (var attachment in item.Value[0])
                                                    {
                                                        key = attachment.Key;
                                                        if (key.ToLower() == "objecttype")
                                                        {
                                                            if (attachment.Value.ToLower() == "article")
                                                            {
                                                                posttype = "link";
                                                            }
                                                            else if (attachment.Value.ToLower() == "photo")
                                                            {
                                                                posttype = "photo";
                                                            }
                                                            else if (attachment.Value.ToLower() == "video")
                                                            {
                                                                posttype = "video";
                                                            }
                                                        }

                                                        if (posttype == "link" && key.ToLower() == "url")
                                                        {
                                                            LinkURL = attachment.Value;
                                                            PictureURL = string.Empty;
                                                        }

                                                        if (posttype == "photo" && key.ToLower() == "image")
                                                        {
                                                            foreach (var image in attachment.Value)
                                                            {
                                                                key = image.Key;

                                                                if (key.ToLower() == "url")
                                                                {
                                                                    PictureURL = image.Value;
                                                                    LinkURL = string.Empty;
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                           
                                            PostTypeEnum PostType = GoogleAPI.GetPostType(posttype);

                                            googlePlusPostDTO.CredentialID = data.CredentialID.Value;
                                            googlePlusPostDTO.ResultID = HttpUtility.UrlDecode(post.url);
                                            googlePlusPostDTO.GooglePlusPostID = post.id;
                                            googlePlusPostDTO.Name = string.Empty;
                                            googlePlusPostDTO.Message = PostMessage;
                                            googlePlusPostDTO.PostTypeID = (long)PostType;
                                            googlePlusPostDTO.LinkURL = LinkURL;
                                            googlePlusPostDTO.PictureURL = PictureURL;
                                            googlePlusPostDTO.GooglePlusCreatedTime = Convert.ToDateTime(post.published).ToUniversalTime();
                                            googlePlusPostDTO.GooglePlusUpdatedTime = Convert.ToDateTime(post.updated).ToUniversalTime();

                                            listgooglePlusPostDTO.Add(googlePlusPostDTO);
                                            Debug.WriteLine(icounter);
                                            icounter++;
                                            //bool Success = ProviderFactory.Social.SaveGooglePlusPost(data.CredentialID.Value, HttpUtility.UrlDecode(post.url), post.id, 
                                            //    string.Empty, PostMessage, PostType, LinkURL, PictureURL, Convert.ToDateTime(post.published).ToUniversalTime(), 
                                            //    Convert.ToDateTime(post.updated).ToUniversalTime());
                                            
                                        }

                                        if (listgooglePlusPostDTO.Any())
                                        {

                                            bool Success = ProviderFactory.Social.SaveGooglePlusPost(data.CredentialID.Value, listgooglePlusPostDTO);

                                            if (!Success)
                                            {
                                                ProviderFactory.Logging.Audit(AuditLevelEnum.Warning, AuditTypeEnum.GooglePlusDownloaderFailure,
                                                                              string.Format("Could not save GooglePlusPost To GooglePlus Post table JobID {1}", job.ID), null, null, null, data.CredentialID);
                                            }
                                            else
                                            {
                                                setJobStatus(job.ID.Value, JobStatusEnum.Completed);
                                            }
                                        }
                                        else
                                        {
                                            setJobStatus(job.ID.Value, JobStatusEnum.Completed);
                                        }
                                    }
                                    else
                                    {
                                        //Log Error
                                    }
                                }
                                #endregion
                            }
                            break;
                    }
                }
                catch (Exception ex)
                {
                    setJobStatus(job.ID.Value, JobStatusEnum.Failed);
                    ProviderFactory.Logging.LogException(ex, job.ID.Value);

                    Auditor
                        .New(AuditLevelEnum.Exception, AuditTypeEnum.ProcessorException, UserInfoDTO.System, ex.ToString())
                        .Save(ProviderFactory.Logging)
                    ;

                }
                job = getNextJob();
            }

        }
    }
}
