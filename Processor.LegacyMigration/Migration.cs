﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Data.Extensions;

namespace Processor.LegacyMigration
{
    public class Migration
    {
        public List<PostsDTO> GetPostsFromSocialDealer(int DealerLocationID, DateTime StartDate, DateTime EndDate)
        {
            List<PostsDTO> posts = new List<PostsDTO>();

            using (SocialDealerEntities db = new SocialDealerEntities())
            {
                if (db.Connection.State != System.Data.ConnectionState.Open)
                    db.Connection.Open();

                using (DbCommand cmd3 = db.CreateStoreCommand(
                            "spGetPostForMigration",
                            System.Data.CommandType.StoredProcedure,
                                new SqlParameter("@DealerLocationID", DealerLocationID),
                                new SqlParameter("@StartDate", StartDate),
                                new SqlParameter("@EndDate", EndDate)
                            ))
                {
                    using (DbDataReader reader = cmd3.ExecuteReader())
                    {
                        posts = db.Translate<PostsDTO>(reader).ToList();
                    }

                }
            }

            return posts;
        }

        public List<Comment> GetFacebookCommentByPostQueueID(long PostQueueID)
        {
            List<Comment> comments = new List<Comment>();
            List<CommentLike> commentLikes = new List<CommentLike>();

            using (SocialDealerEntities db = new SocialDealerEntities())
            {
                if (db.Connection.State != System.Data.ConnectionState.Open)
                    db.Connection.Open();

                using (DbCommand cmd3 = db.CreateStoreCommand(
                            "spGetFacebookCommentByPostQueueIDForMigration",
                            System.Data.CommandType.StoredProcedure,
                                new SqlParameter("@PostQueueID", PostQueueID)
                            ))
                {
                    using (DbDataReader reader = cmd3.ExecuteReader())
                    {
                        comments = db.Translate<Comment>(reader).ToList();

                        reader.NextResult();

                        commentLikes = db.Translate<CommentLike>(reader).ToList();
                    }

                }
            }

            return comments;
        }

        public List<CommentLike> GetFacebookCommentLikeByPostQueueID(long PostQueueID)
        {            
            List<CommentLike> commentLikes = new List<CommentLike>();

            using (SocialDealerEntities db = new SocialDealerEntities())
            {
                if (db.Connection.State != System.Data.ConnectionState.Open)
                    db.Connection.Open();

                using (DbCommand cmd3 = db.CreateStoreCommand(
                            "spGetFacebookCommentLikeByPostQueueIDForMigration",
                            System.Data.CommandType.StoredProcedure,
                                new SqlParameter("@PostQueueID", PostQueueID)
                            ))
                {
                    using (DbDataReader reader = cmd3.ExecuteReader())
                    {                       
                        commentLikes = db.Translate<CommentLike>(reader).ToList();
                    }

                }
            }

            return commentLikes;
        }

        //public List<CommentLike> GetFacebookCommentLikeByPostQueueID(long PostQueueID)
        //{
        //    List<CommentLike> commentLikes = new List<CommentLike>();

        //    using (SocialDealerEntities db = new SocialDealerEntities())
        //    {
        //        if (db.Connection.State != System.Data.ConnectionState.Open)
        //            db.Connection.Open();

        //        using (DbCommand cmd3 = db.CreateStoreCommand(
        //                    "spGetFacebookPostStatsByPostQueueIDForMigration",
        //                    System.Data.CommandType.StoredProcedure,
        //                        new SqlParameter("@PostQueueID", PostQueueID)
        //                    ))
        //        {
        //            using (DbDataReader reader = cmd3.ExecuteReader())
        //            {
        //                reader.NextResult();

        //                commentLikes = db.Translate<CommentLike>(reader).ToList();
        //            }

        //        }
        //    }

        //    return commentLikes;
        //}

        public List<PostLike> GetFacebookPostLikeByPostQueueID(long PostQueueID)
        {
            List<PostLike> postLikes = new List<PostLike>();

            using (SocialDealerEntities db = new SocialDealerEntities())
            {
                if (db.Connection.State != System.Data.ConnectionState.Open)
                    db.Connection.Open();

                using (DbCommand cmd3 = db.CreateStoreCommand(
                            "spGetFacebookPostLikesByPostQueueIDForMigration",
                            System.Data.CommandType.StoredProcedure,
                                new SqlParameter("@PostQueueID", PostQueueID)
                            ))
                {
                    using (DbDataReader reader = cmd3.ExecuteReader())
                    {
                        postLikes = db.Translate<PostLike>(reader).ToList();
                    }

                }
            }

            return postLikes;
        }

        public List<PostShared> GetFacebookPostSharedByPostQueueID(long PostQueueID)
        {
            List<PostShared> postShareds = new List<PostShared>();

            using (SocialDealerEntities db = new SocialDealerEntities())
            {
                if (db.Connection.State != System.Data.ConnectionState.Open)
                    db.Connection.Open();

                using (DbCommand cmd3 = db.CreateStoreCommand(
                            "spGetFacebookPostSharedByPostQueueIDForMigration",
                            System.Data.CommandType.StoredProcedure,
                                new SqlParameter("@PostQueueID", PostQueueID)
                            ))
                {
                    using (DbDataReader reader = cmd3.ExecuteReader())
                    {                      
                        postShareds = db.Translate<PostShared>(reader).ToList();
                    }

                }
            }

            return postShareds;
        }

        public List<PostInsight> GetFacebookPostInsightByPostQueueID(long PostQueueID)
        {
            List<PostInsight> postInsights = new List<PostInsight>();

            using (SocialDealerEntities db = new SocialDealerEntities())
            {
                if (db.Connection.State != System.Data.ConnectionState.Open)
                    db.Connection.Open();

                using (DbCommand cmd3 = db.CreateStoreCommand(
                            "spGetFacebookPostInsightByPostQueueIDForMigration",
                            System.Data.CommandType.StoredProcedure,
                                new SqlParameter("@PostQueueID", PostQueueID)
                            ))
                {
                    using (DbDataReader reader = cmd3.ExecuteReader())
                    {
                        postInsights = db.Translate<PostInsight>(reader).ToList();
                    }

                }
            }

            return postInsights;
        }
    }

    public class PostsDTO
    {
        public long PostID { get; set; }
        public string UserName { get; set; }
        public string Title { get; set; }
        public string Link { get; set; }
        public string Image { get; set; }
        public DateTime DateCreated { get; set; }
		public DateTime ScheduleDate { get; set; }
        public string Comment { get; set; }
        public bool? IsSyndicated { get; set; }
        public bool? IsUploadedImage { get; set; }
		public int PostTypeId { get; set; }
        public string HashValue { get; set; }
        public long PostQueueID { get; set; }
		public int DealerLocationSocialNetworkID  { get; set; }
		public string UniqueID  { get; set; }
		public int DealerLocationID  { get; set; }
		public DateTime? ProcessedDate  { get; set; }
		public string ResultID { get; set; }
        public string FeedID { get; set; }        
		public string ErrMsg  { get; set; }
		public bool? ApprovalStatus  { get; set; }
        public bool? NeedsApproval { get; set; }
    }

    public class Comment
    {
        public int FBCommentId { get; set; }
        public int FBPostDataId { get; set; }
        public string FacebookId { get; set; }
        public string Message { get; set; }
        public DateTime CreatedTime { get; set; }
        public int UserLikes { get; set; }
        public int LikeCount { get; set; }
        public string FromFacebookId { get; set; }
        public string FromFacebookName { get; set; }
    }

    public class CommentLike
    {
        public int FBCommentId { get; set; }
        public string FacebookId { get; set; }        
        public string FromFacebookName { get; set; }
    }

    public class PostLike
    {
        public int FBPostDataId { get; set; }
        public string FacebookId { get; set; }
        public string FacebookName { get; set; }
        public DateTime CreatedTime { get; set; }
        public bool Unlike { get; set; }
    }

    public class PostShared
    {
        public int FBPostDataId { get; set; }
        public string FacebookId { get; set; }
        public string Name { get; set; }
        public string Message { get; set; }
        public string FromFacebookId { get; set; }
        public string FromFacebookName { get; set; }
        public DateTime CreatedTime { get; set; }

    }

    public class PostInsight
    {
        public long PostQueueId { get; set; }
        public string ResultId { get; set; }
        public int FBPostInsightNameId { get; set; }
        public string Name { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public DateTime InsightNameInsertedDate { get; set; }
        public string Value { get; set; }
        public int? Sum { get; set; }
        public string Period { get; set; }
        public string Error { get; set; }
        public DateTime InsertedDate { get; set; }
        public DateTime QueryDate { get; set; }
    }
}
