﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SI.BLL;
using SI.DTO;
using JobLib;

namespace Processor.LegacyMigration
{
    public class Processor : ProcessorBase<LegacyMigrationData>
    {
        public Processor(string[] args) : base(args) { }

        protected override void start()
        {
            JobDTO job = getNextJob();
            while (job != null)
            {
                try
                {
                    setJobStatus(job.ID.Value, JobStatusEnum.Started);
                    LegacyMigrationData data = getJobData(job.ID.Value);

                    switch (data.Type)
                    {
                        case LegacyMigrationData.LegacyMigrationType.SocialFacebookPost:
                            {
                                //Get the Post from SocialDealer by DealerLocationID, StartDate and EndDate
                                Migration objMigration = new Migration();
                                List<PostsDTO> posts = objMigration.GetPostsFromSocialDealer(data.DealerLocationID, data.MinQueryDate.GetValueOrDefault(), data.MaxQueryDate.GetValueOrDefault());

                                foreach (var post in posts)
                                {
                                    //Insert Post to New System

                                    //Get Comments, CommentLike, PostLike, PostShared from SocialDealer

                                    //Insert Comments, CommentLike, PostLike, PostShared to New System

                                    //Get FBPostInsight from SocialDealer

                                    //Insert FBPostInsight from SocialDealer
                                }
                            }
                            break;

                        default:
                            break;
                    }
                    

                    setJobStatus(job.ID.Value, JobStatusEnum.Completed);

                }
                catch (Exception ex)
                {
                    setJobStatus(job.ID.Value, JobStatusEnum.Failed);
                    ProviderFactory.Logging.LogException(ex, job.ID.Value);

                    Auditor
                        .New(AuditLevelEnum.Exception, AuditTypeEnum.ProcessorException, UserInfoDTO.System, "Legacy Migration processor exception: {0}", ex.ToString())
                        .Save(ProviderFactory.Logging)
                    ;
                }
                job = getNextJob();
            }
        }
                
    }
}
