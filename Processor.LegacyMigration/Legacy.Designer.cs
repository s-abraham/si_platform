﻿//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using System;
using System.ComponentModel;
using System.Data.EntityClient;
using System.Data.Objects;
using System.Data.Objects.DataClasses;
using System.Linq;
using System.Runtime.Serialization;
using System.Xml.Serialization;

[assembly: EdmSchemaAttribute()]
namespace Processor.LegacyMigration
{
    #region Contexts
    
    /// <summary>
    /// No Metadata Documentation available.
    /// </summary>
    public partial class SocialDealerEntities : ObjectContext
    {
        #region Constructors
    
        /// <summary>
        /// Initializes a new SocialDealerEntities object using the connection string found in the 'SocialDealerEntities' section of the application configuration file.
        /// </summary>
        public SocialDealerEntities() : base("name=SocialDealerEntities", "SocialDealerEntities")
        {
            this.ContextOptions.LazyLoadingEnabled = true;
            OnContextCreated();
        }
    
        /// <summary>
        /// Initialize a new SocialDealerEntities object.
        /// </summary>
        public SocialDealerEntities(string connectionString) : base(connectionString, "SocialDealerEntities")
        {
            this.ContextOptions.LazyLoadingEnabled = true;
            OnContextCreated();
        }
    
        /// <summary>
        /// Initialize a new SocialDealerEntities object.
        /// </summary>
        public SocialDealerEntities(EntityConnection connection) : base(connection, "SocialDealerEntities")
        {
            this.ContextOptions.LazyLoadingEnabled = true;
            OnContextCreated();
        }
    
        #endregion
    
        #region Partial Methods
    
        partial void OnContextCreated();
    
        #endregion
    
        #region ObjectSet Properties
    
        /// <summary>
        /// No Metadata Documentation available.
        /// </summary>
        public ObjectSet<DealerLocation> DealerLocations
        {
            get
            {
                if ((_DealerLocations == null))
                {
                    _DealerLocations = base.CreateObjectSet<DealerLocation>("DealerLocations");
                }
                return _DealerLocations;
            }
        }
        private ObjectSet<DealerLocation> _DealerLocations;

        #endregion

        #region AddTo Methods
    
        /// <summary>
        /// Deprecated Method for adding a new object to the DealerLocations EntitySet. Consider using the .Add method of the associated ObjectSet&lt;T&gt; property instead.
        /// </summary>
        public void AddToDealerLocations(DealerLocation dealerLocation)
        {
            base.AddObject("DealerLocations", dealerLocation);
        }

        #endregion

    }

    #endregion

    #region Entities
    
    /// <summary>
    /// No Metadata Documentation available.
    /// </summary>
    [EdmEntityTypeAttribute(NamespaceName="SocialDealerModel", Name="DealerLocation")]
    [Serializable()]
    [DataContractAttribute(IsReference=true)]
    public partial class DealerLocation : EntityObject
    {
        #region Factory Method
    
        /// <summary>
        /// Create a new DealerLocation object.
        /// </summary>
        /// <param name="id">Initial value of the ID property.</param>
        /// <param name="dealerID">Initial value of the DealerID property.</param>
        /// <param name="addressID">Initial value of the AddressID property.</param>
        /// <param name="name">Initial value of the Name property.</param>
        /// <param name="logo">Initial value of the Logo property.</param>
        /// <param name="homePage">Initial value of the HomePage property.</param>
        public static DealerLocation CreateDealerLocation(global::System.Int32 id, global::System.Int32 dealerID, global::System.Int32 addressID, global::System.String name, global::System.String logo, global::System.String homePage)
        {
            DealerLocation dealerLocation = new DealerLocation();
            dealerLocation.ID = id;
            dealerLocation.DealerID = dealerID;
            dealerLocation.AddressID = addressID;
            dealerLocation.Name = name;
            dealerLocation.Logo = logo;
            dealerLocation.HomePage = homePage;
            return dealerLocation;
        }

        #endregion

        #region Simple Properties
    
        /// <summary>
        /// No Metadata Documentation available.
        /// </summary>
        [EdmScalarPropertyAttribute(EntityKeyProperty=true, IsNullable=false)]
        [DataMemberAttribute()]
        public global::System.Int32 ID
        {
            get
            {
                return _ID;
            }
            set
            {
                if (_ID != value)
                {
                    OnIDChanging(value);
                    ReportPropertyChanging("ID");
                    _ID = StructuralObject.SetValidValue(value, "ID");
                    ReportPropertyChanged("ID");
                    OnIDChanged();
                }
            }
        }
        private global::System.Int32 _ID;
        partial void OnIDChanging(global::System.Int32 value);
        partial void OnIDChanged();
    
        /// <summary>
        /// No Metadata Documentation available.
        /// </summary>
        [EdmScalarPropertyAttribute(EntityKeyProperty=false, IsNullable=false)]
        [DataMemberAttribute()]
        public global::System.Int32 DealerID
        {
            get
            {
                return _DealerID;
            }
            set
            {
                OnDealerIDChanging(value);
                ReportPropertyChanging("DealerID");
                _DealerID = StructuralObject.SetValidValue(value, "DealerID");
                ReportPropertyChanged("DealerID");
                OnDealerIDChanged();
            }
        }
        private global::System.Int32 _DealerID;
        partial void OnDealerIDChanging(global::System.Int32 value);
        partial void OnDealerIDChanged();
    
        /// <summary>
        /// No Metadata Documentation available.
        /// </summary>
        [EdmScalarPropertyAttribute(EntityKeyProperty=false, IsNullable=false)]
        [DataMemberAttribute()]
        public global::System.Int32 AddressID
        {
            get
            {
                return _AddressID;
            }
            set
            {
                OnAddressIDChanging(value);
                ReportPropertyChanging("AddressID");
                _AddressID = StructuralObject.SetValidValue(value, "AddressID");
                ReportPropertyChanged("AddressID");
                OnAddressIDChanged();
            }
        }
        private global::System.Int32 _AddressID;
        partial void OnAddressIDChanging(global::System.Int32 value);
        partial void OnAddressIDChanged();
    
        /// <summary>
        /// No Metadata Documentation available.
        /// </summary>
        [EdmScalarPropertyAttribute(EntityKeyProperty=false, IsNullable=false)]
        [DataMemberAttribute()]
        public global::System.String Name
        {
            get
            {
                return _Name;
            }
            set
            {
                OnNameChanging(value);
                ReportPropertyChanging("Name");
                _Name = StructuralObject.SetValidValue(value, false, "Name");
                ReportPropertyChanged("Name");
                OnNameChanged();
            }
        }
        private global::System.String _Name;
        partial void OnNameChanging(global::System.String value);
        partial void OnNameChanged();
    
        /// <summary>
        /// No Metadata Documentation available.
        /// </summary>
        [EdmScalarPropertyAttribute(EntityKeyProperty=false, IsNullable=false)]
        [DataMemberAttribute()]
        public global::System.String Logo
        {
            get
            {
                return _Logo;
            }
            set
            {
                OnLogoChanging(value);
                ReportPropertyChanging("Logo");
                _Logo = StructuralObject.SetValidValue(value, false, "Logo");
                ReportPropertyChanged("Logo");
                OnLogoChanged();
            }
        }
        private global::System.String _Logo;
        partial void OnLogoChanging(global::System.String value);
        partial void OnLogoChanged();
    
        /// <summary>
        /// No Metadata Documentation available.
        /// </summary>
        [EdmScalarPropertyAttribute(EntityKeyProperty=false, IsNullable=false)]
        [DataMemberAttribute()]
        public global::System.String HomePage
        {
            get
            {
                return _HomePage;
            }
            set
            {
                OnHomePageChanging(value);
                ReportPropertyChanging("HomePage");
                _HomePage = StructuralObject.SetValidValue(value, false, "HomePage");
                ReportPropertyChanged("HomePage");
                OnHomePageChanged();
            }
        }
        private global::System.String _HomePage;
        partial void OnHomePageChanging(global::System.String value);
        partial void OnHomePageChanged();
    
        /// <summary>
        /// No Metadata Documentation available.
        /// </summary>
        [EdmScalarPropertyAttribute(EntityKeyProperty=false, IsNullable=true)]
        [DataMemberAttribute()]
        public global::System.String CRMEmail
        {
            get
            {
                return _CRMEmail;
            }
            set
            {
                OnCRMEmailChanging(value);
                ReportPropertyChanging("CRMEmail");
                _CRMEmail = StructuralObject.SetValidValue(value, true, "CRMEmail");
                ReportPropertyChanged("CRMEmail");
                OnCRMEmailChanged();
            }
        }
        private global::System.String _CRMEmail;
        partial void OnCRMEmailChanging(global::System.String value);
        partial void OnCRMEmailChanged();
    
        /// <summary>
        /// No Metadata Documentation available.
        /// </summary>
        [EdmScalarPropertyAttribute(EntityKeyProperty=false, IsNullable=true)]
        [DataMemberAttribute()]
        public Nullable<global::System.Int32> CRMTypeID
        {
            get
            {
                return _CRMTypeID;
            }
            set
            {
                OnCRMTypeIDChanging(value);
                ReportPropertyChanging("CRMTypeID");
                _CRMTypeID = StructuralObject.SetValidValue(value, "CRMTypeID");
                ReportPropertyChanged("CRMTypeID");
                OnCRMTypeIDChanged();
            }
        }
        private Nullable<global::System.Int32> _CRMTypeID;
        partial void OnCRMTypeIDChanging(Nullable<global::System.Int32> value);
        partial void OnCRMTypeIDChanged();
    
        /// <summary>
        /// No Metadata Documentation available.
        /// </summary>
        [EdmScalarPropertyAttribute(EntityKeyProperty=false, IsNullable=true)]
        [DataMemberAttribute()]
        public Nullable<global::System.Int32> EmailFormatID
        {
            get
            {
                return _EmailFormatID;
            }
            set
            {
                OnEmailFormatIDChanging(value);
                ReportPropertyChanging("EmailFormatID");
                _EmailFormatID = StructuralObject.SetValidValue(value, "EmailFormatID");
                ReportPropertyChanged("EmailFormatID");
                OnEmailFormatIDChanged();
            }
        }
        private Nullable<global::System.Int32> _EmailFormatID;
        partial void OnEmailFormatIDChanging(Nullable<global::System.Int32> value);
        partial void OnEmailFormatIDChanged();
    
        /// <summary>
        /// No Metadata Documentation available.
        /// </summary>
        [EdmScalarPropertyAttribute(EntityKeyProperty=false, IsNullable=true)]
        [DataMemberAttribute()]
        public Nullable<global::System.Boolean> Active
        {
            get
            {
                return _Active;
            }
            set
            {
                OnActiveChanging(value);
                ReportPropertyChanging("Active");
                _Active = StructuralObject.SetValidValue(value, "Active");
                ReportPropertyChanged("Active");
                OnActiveChanged();
            }
        }
        private Nullable<global::System.Boolean> _Active;
        partial void OnActiveChanging(Nullable<global::System.Boolean> value);
        partial void OnActiveChanged();
    
        /// <summary>
        /// No Metadata Documentation available.
        /// </summary>
        [EdmScalarPropertyAttribute(EntityKeyProperty=false, IsNullable=true)]
        [DataMemberAttribute()]
        public Nullable<global::System.DateTime> CreatedDate
        {
            get
            {
                return _CreatedDate;
            }
            set
            {
                OnCreatedDateChanging(value);
                ReportPropertyChanging("CreatedDate");
                _CreatedDate = StructuralObject.SetValidValue(value, "CreatedDate");
                ReportPropertyChanged("CreatedDate");
                OnCreatedDateChanged();
            }
        }
        private Nullable<global::System.DateTime> _CreatedDate;
        partial void OnCreatedDateChanging(Nullable<global::System.DateTime> value);
        partial void OnCreatedDateChanged();
    
        /// <summary>
        /// No Metadata Documentation available.
        /// </summary>
        [EdmScalarPropertyAttribute(EntityKeyProperty=false, IsNullable=true)]
        [DataMemberAttribute()]
        public Nullable<global::System.Boolean> IsDummy
        {
            get
            {
                return _IsDummy;
            }
            set
            {
                OnIsDummyChanging(value);
                ReportPropertyChanging("IsDummy");
                _IsDummy = StructuralObject.SetValidValue(value, "IsDummy");
                ReportPropertyChanged("IsDummy");
                OnIsDummyChanged();
            }
        }
        private Nullable<global::System.Boolean> _IsDummy;
        partial void OnIsDummyChanging(Nullable<global::System.Boolean> value);
        partial void OnIsDummyChanged();
    
        /// <summary>
        /// No Metadata Documentation available.
        /// </summary>
        [EdmScalarPropertyAttribute(EntityKeyProperty=false, IsNullable=true)]
        [DataMemberAttribute()]
        public Nullable<global::System.Boolean> IsAllowEmail
        {
            get
            {
                return _IsAllowEmail;
            }
            set
            {
                OnIsAllowEmailChanging(value);
                ReportPropertyChanging("IsAllowEmail");
                _IsAllowEmail = StructuralObject.SetValidValue(value, "IsAllowEmail");
                ReportPropertyChanged("IsAllowEmail");
                OnIsAllowEmailChanged();
            }
        }
        private Nullable<global::System.Boolean> _IsAllowEmail;
        partial void OnIsAllowEmailChanging(Nullable<global::System.Boolean> value);
        partial void OnIsAllowEmailChanged();
    
        /// <summary>
        /// No Metadata Documentation available.
        /// </summary>
        [EdmScalarPropertyAttribute(EntityKeyProperty=false, IsNullable=true)]
        [DataMemberAttribute()]
        public Nullable<global::System.Boolean> IsPayingCustomer
        {
            get
            {
                return _IsPayingCustomer;
            }
            set
            {
                OnIsPayingCustomerChanging(value);
                ReportPropertyChanging("IsPayingCustomer");
                _IsPayingCustomer = StructuralObject.SetValidValue(value, "IsPayingCustomer");
                ReportPropertyChanged("IsPayingCustomer");
                OnIsPayingCustomerChanged();
            }
        }
        private Nullable<global::System.Boolean> _IsPayingCustomer;
        partial void OnIsPayingCustomerChanging(Nullable<global::System.Boolean> value);
        partial void OnIsPayingCustomerChanged();
    
        /// <summary>
        /// No Metadata Documentation available.
        /// </summary>
        [EdmScalarPropertyAttribute(EntityKeyProperty=false, IsNullable=true)]
        [DataMemberAttribute()]
        public Nullable<global::System.Boolean> IsTracking
        {
            get
            {
                return _IsTracking;
            }
            set
            {
                OnIsTrackingChanging(value);
                ReportPropertyChanging("IsTracking");
                _IsTracking = StructuralObject.SetValidValue(value, "IsTracking");
                ReportPropertyChanged("IsTracking");
                OnIsTrackingChanged();
            }
        }
        private Nullable<global::System.Boolean> _IsTracking;
        partial void OnIsTrackingChanging(Nullable<global::System.Boolean> value);
        partial void OnIsTrackingChanged();

        #endregion

    }

    #endregion

}
