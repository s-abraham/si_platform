﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AWS;
using Connectors.Social;
using Connectors.Social.Parameters;
using JobLib;
using Newtonsoft.Json;
using SI;
using SI.BLL;
using SI.DTO;
using Twitter.Entities;

namespace Processor.Social.Twitter
{
    public class Processor : ProcessorBase<TwitterDownloaderData>
    {
        //private static DynamoDB dynamoDB = new DynamoDB("AKIAIU5WXGFXWDQRWQZA", "740Bwoxy0JlpXkOzyzSeaAtjM8r9tEQHjDMTIh8m", "http://dynamodb.us-west-2.amazonaws.com", "SocialTable");
        private const string prefix = "AWS.";
        private const int TwitterMaxLength = 140;

        // Need to pass the args through for the default constructor
        public Processor(string[] args) : base(args) { }

        //static void Main(string[] args)
        protected override void start()
        {
            string accessKey = ProviderFactory.Reviews.GetProcessorSetting(prefix + "AWSAccessKey");
            string secretKey = ProviderFactory.Reviews.GetProcessorSetting(prefix + "AWSSecretKey");
            string serviceURL = ProviderFactory.Reviews.GetProcessorSetting(prefix + "DynamoDBServiceURL");
            string table = ProviderFactory.Reviews.GetProcessorSetting(prefix + "DynamoDBSocialRawTable");

            DynamoDB dynamoDB = new DynamoDB(accessKey, secretKey, serviceURL, table);

            JobDTO job = getNextJob();

            while (job != null)
            {
                try
                {
                    setJobStatus(job.ID.Value, JobStatusEnum.Started);
                    TwitterDownloaderData data = getJobData(job.ID.Value);

                    SocialCredentialDTO socialCredentialsDTO = null;
                    if (data.CredentialID.HasValue)
                        socialCredentialsDTO = ProviderFactory.Social.GetSocialCredentialInfo(data.CredentialID.Value);

                    switch (data.TwitterEnum)
                    {
                        case TwitterProcessorActionEnum.TwitterPageInformationDownloder:
                            {
                                #region PAGE INSIGHTS

                                //Call Connection Class to get data from SDK   
                                SITwitterPageParameters pageParameters = new SITwitterPageParameters();

                                pageParameters.uniqueID = socialCredentialsDTO.UniqueID;
                                pageParameters.oauth_token = socialCredentialsDTO.Token;
                                pageParameters.oauth_token_secret = socialCredentialsDTO.TokenSecret;
                                //TODO: socialCredentialsDTO should have AppID from AppSecret [SocialApps]
                                pageParameters.oauth_consumer_key = socialCredentialsDTO.AppID;
                                pageParameters.oauth_consumer_secret = socialCredentialsDTO.AppSecret;

                                SITwitterPageConnectionParameters ConnectionParameters = new SITwitterPageConnectionParameters(pageParameters);
                                TwitterPageConnection pageconnection = new TwitterPageConnection(ConnectionParameters);

                                TwitterPageInsightsResponse objTwitterPageInsightsResponse = pageconnection.GetTwitterPageInsights();

                                if (string.IsNullOrEmpty(objTwitterPageInsightsResponse.errors.error))
                                {
                                    //save Twitter Page Insights to DynamoDB from SocialDealer AWS SDK
                                    string serializeValue = JsonConvert.SerializeObject(objTwitterPageInsightsResponse);
                                    
                                    var dynoDbResult = dynamoDB.PutValue(serializeValue);

                                    if (dynoDbResult.IsSuccess)
                                    {
                                        TwitterIntegratorData integratorData = new TwitterIntegratorData()
                                        {
                                            TwitterActionEnum = TwitterProcessorActionEnum.TwitterPageInformationIntegrator,
                                            Key = dynoDbResult.Key,
                                            CredentialID = data.CredentialID.Value                                            
                                        };

                                        JobDTO jobIntegrator = new JobDTO()
                                        {
                                            JobDataObject = integratorData,
                                            JobType = integratorData.JobType,
                                            DateScheduled = new SIDateTime(DateTime.UtcNow, TimeZoneInfo.Utc),
                                            JobStatus = JobStatusEnum.Queued,
                                            //JobStatus = JobStatusEnum.Parked,
                                            Priority = 150
                                        };

                                        SaveEntityDTO<JobDTO> saveInfo = ProviderFactory.Jobs.Save(new SaveEntityDTO<JobDTO>(jobIntegrator, UserInfoDTO.System));
                                        Console.WriteLine(saveInfo.Entity.ID);
                                        Debug.WriteLine(saveInfo.Entity.ID);

                                        setJobStatus(job.ID.Value, JobStatusEnum.Completed);
                                    }
                                    else
                                    {
                                        setJobStatus(job.ID.Value, JobStatusEnum.Failed);
                                        ProviderFactory.Logging.Audit(AuditLevelEnum.Warning, AuditTypeEnum.FacebookDownloaderFailure,
                                                                      string.Format("Twitter Page Information could not be saved to DynamoDB, CredentialID: {0}. JobID {1}",
                                                                                     data.CredentialID, job.ID), null, null, null, data.CredentialID);
                                    }
                                }
                                else
                                {
                                    //Log Error
                                }

                                #endregion
                            }
                            break;
                        case TwitterProcessorActionEnum.TwitterPostStatisticsDownloder:
                            {
                                #region POST STATISTICS (Basic)
                                
                                PostTargetPublishInfoDTO info = ProviderFactory.Social.GetPostTargetPublishInfo(data.PostTargetID, data.PostImageID);
                                socialCredentialsDTO = ProviderFactory.Social.GetSocialCredentialInfo(info.CredentialID);

                                //Call Connection Class to get data from SDK   
                                SITwitterPostStatisticsParameters postParameters = new SITwitterPostStatisticsParameters();

                                postParameters.statusID =Convert.ToInt64(info.ResultID);
                                postParameters.oauth_token = socialCredentialsDTO.Token;
                                postParameters.oauth_token_secret = socialCredentialsDTO.TokenSecret;                                
                                postParameters.oauth_consumer_key = socialCredentialsDTO.AppID;
                                postParameters.oauth_consumer_secret = socialCredentialsDTO.AppSecret;

                                SITwitterPostStatisticsConnectionParameters ConnectionParameters = new SITwitterPostStatisticsConnectionParameters(postParameters);
                                TwitterPostStatisticConnection postconnection = new TwitterPostStatisticConnection(ConnectionParameters);

                                TwitterPostStatistics PostStatistics = postconnection.GetTwitterStatusStatistics();

                                if (string.IsNullOrEmpty(PostStatistics.Error))
                                {
                                    string serializeValue = JsonConvert.SerializeObject(PostStatistics);

                                    var dynoDbResult = dynamoDB.PutValue(serializeValue);

                                    if (dynoDbResult.IsSuccess)
                                    {
                                        TwitterIntegratorData integratorData = new TwitterIntegratorData()
                                        {
                                            TwitterActionEnum = TwitterProcessorActionEnum.TwitterPostStatisticsIntegrator,
                                            Key = dynoDbResult.Key,
                                            PostTargetID = data.PostTargetID
                                        };

                                        JobDTO jobIntegrator = new JobDTO()
                                        {
                                            JobDataObject = integratorData,
                                            JobType = integratorData.JobType,
                                            DateScheduled = new SIDateTime(DateTime.UtcNow, TimeZoneInfo.Utc),
                                            JobStatus = JobStatusEnum.Queued,
                                            //JobStatus = JobStatusEnum.Parked,
                                            Priority = 150
                                        };

                                        SaveEntityDTO<JobDTO> saveInfo = ProviderFactory.Jobs.Save(new SaveEntityDTO<JobDTO>(jobIntegrator, UserInfoDTO.System));
                                        Console.WriteLine(saveInfo.Entity.ID);

                                        setJobStatus(job.ID.Value, JobStatusEnum.Completed);
                                    }
                                    else
                                    {
                                        setJobStatus(job.ID.Value, JobStatusEnum.Failed);
                                        ProviderFactory.Logging.Audit(AuditLevelEnum.Warning, AuditTypeEnum.FacebookDownloaderFailure,
                                                                      string.Format("Twitter Page Information could not be saved to DynamoDB, CredentialID: {0}. JobID {1}",
                                                                                     data.CredentialID, job.ID), null, null, null, data.CredentialID);
                                    }
                                }
                                else
                                {
                                    //Log Error
                                }
                                //save Facebook Page Insights to DynamoDB from SocialDealer AWS SDK
                                //string serializeValue = JsonConvert.SerializeObject(objTwitterPageInsightsResponse);

                                //var result = dynamoDB.PutValue(serializeValue);
                                //var obj = dynamoDB.GetValue(result.Key);

                                //string serializeValueNew = JsonConvert.DeserializeObject<string>(obj.Value);

                                #endregion
                            }
                            break;
                        case TwitterProcessorActionEnum.TwitterPublish:
                            {
                                #region PUBLISH

                                Auditor
                                    .New(AuditLevelEnum.Information, AuditTypeEnum.PublishActivity, UserInfoDTO.System, "")
                                    .SetAuditDataObject(
                                        AuditPublishActivity
                                        .New(data.PostID, AuditPublishActivityTypeEnum.PublishProcessorEntry)
                                        .SetPostID(data.PostID)
                                        .SetPostTargetID(data.PostTargetID)
                                        .SetPostImageID(data.PostImageID)
                                        .SetJobID(job.ID)
                                    )
                                    .Save(ProviderFactory.Logging)
                                ;

                                int linklength = 0;

                                //TODO: Add PostType to Job Data (in PostTargetPublishInfoDTO)
                                string PublishImageUploadBaseURL = Settings.GetSetting("site.PublishImageUploadURL");

                                PostTargetPublishInfoDTO info = ProviderFactory.Social.GetPostTargetPublishInfo(data.PostTargetID, data.PostImageID);
                                socialCredentialsDTO = ProviderFactory.Social.GetSocialCredentialInfo(info.CredentialID);

                                SITwitterPublishParameters publishParameters = new SITwitterPublishParameters();

                                publishParameters.uniqueID = info.UniqueID;
                                publishParameters.oauth_token = info.Token;
                                publishParameters.oauth_token_secret = socialCredentialsDTO.TokenSecret;
                                publishParameters.oauth_consumer_key = info.AppID;
                                publishParameters.oauth_consumer_secret = info.AppSecret;

                                if (data.ReferencePostTargetID.HasValue)
                                {
                                    #region Set Post Data from ReferencePostTargetID

                                    PostTargetPublishInfoDTO referenceinfo = ProviderFactory.Social.GetPostTargetPublishInfo(data.ReferencePostTargetID.Value, null);
                                    SocialCredentialDTO referencesocialCredentialsDTO = ProviderFactory.Social.GetSocialCredentialInfo(referenceinfo.CredentialID);

                                    if (info.PostType == PostTypeEnum.Photo)
                                    {
                                        #region Photo

                                        //Get PostImageResults by PostTargetID
                                        PostImageResultDTO postImageResultDTO = ProviderFactory.Social.GetPostImageResulInfo(data.ReferencePostTargetID.Value).FirstOrDefault();

                                        string ResultID = string.Empty;

                                        string PostURL = string.Empty;

                                        if (postImageResultDTO != null || referenceinfo != null)
                                        {
                                            PostImageDTO postImageDTO = ProviderFactory.Social.GetPostImageInfo(referenceinfo.PostID);
                                                                                        
                                            if (postImageResultDTO != null)
                                            {
                                                if (postImageResultDTO.ResultID != null)
                                                {
                                                    ResultID = postImageResultDTO.ResultID.ToString(CultureInfo.InvariantCulture);
                                                    PostURL = PublishImageUploadBaseURL + postImageDTO.URL;
                                                }
                                            }
                                            else if (referenceinfo != null)
                                            {
                                                if (referenceinfo.ResultID != null)
                                                {
                                                    ResultID = referenceinfo.ResultID.ToString(CultureInfo.InvariantCulture);
                                                    PostURL = PublishImageUploadBaseURL + postImageDTO.URL;
                                                }
                                            }                                            

                                            //publishParameters.postType = PostTypeEnum.Status;

                                            if (!string.IsNullOrEmpty(PostURL))
                                            {
                                                //linklength = link.Length;
                                                linklength = 23;
                                            }

                                            int TwitterStatusLength = (info.Message + " ").Length + linklength;

                                            if (TwitterStatusLength >= TwitterMaxLength)
                                            {
                                                linklength = 23;
                                                int maxLength = TwitterMaxLength - (linklength + 1);

                                                switch (referencesocialCredentialsDTO.SocialNetworkID)
                                                {
                                                    case 1:
                                                        {
                                                            #region Facebook is Primary
                                                            if (ResultID.Contains("_"))
                                                            {
                                                                string[] ids = referenceinfo.ResultID.Split('_');

                                                                publishParameters.link = "http://www.facebook.com/" + ids[0] + "/posts/" + ids[1];
                                                                if (info.Message.Length > maxLength)
                                                                {                                                                    
                                                                    //publishParameters.status = info.Message.Substring(0, maxLength);
                                                                    publishParameters.status = info.Message.Substring(0, maxLength - 3).PadRight(maxLength, '.');
                                                                }
                                                                else
                                                                {
                                                                    publishParameters.status = info.Message;
                                                                }
                                                            }
                                                            else
                                                            {
                                                                publishParameters.link = "http://www.facebook.com/" + ResultID;
                                                                if (info.Message.Length > maxLength)
                                                                {
                                                                    //publishParameters.status = info.Message.Substring(0, maxLength);
                                                                    publishParameters.status = info.Message.Substring(0, maxLength - 3).PadRight(maxLength, '.');
                                                                }
                                                                else
                                                                {
                                                                    publishParameters.status = info.Message;
                                                                }
                                                            }

                                                            #endregion
                                                        }
                                                        break;
                                                    case 2:
                                                        {
                                                            #region Google+ is Primary

                                                            publishParameters.link = ResultID;
                                                            if (info.Message.Length > maxLength)
                                                            {
                                                                //publishParameters.status = info.Message.Substring(0, maxLength);
                                                                publishParameters.status = info.Message.Substring(0, maxLength - 3).PadRight(maxLength, '.');
                                                            }
                                                            else
                                                            {
                                                                publishParameters.status = info.Message;
                                                            }

                                                            #endregion
                                                        }
                                                        break;

                                                }

                                                                                        
                                                info.PostType = PostTypeEnum.Link;
                                            }
                                            else
                                            {
                                                publishParameters.status = info.Message;
                                                publishParameters.imagePath = PostURL;
                                            }
                                        }

                                        #endregion
                                    }
                                    else
                                    {                                        
                                        if (!string.IsNullOrEmpty(info.Link))
                                        {                                            
                                            linklength = 23;
                                        }

                                        int TwitterStatusLength = (info.Message + " ").Length + linklength;

                                        if (TwitterStatusLength >= TwitterMaxLength)
                                        {
                                            linklength = 23;
                                            int maxLength = TwitterMaxLength - (linklength + 1);

                                            switch (referencesocialCredentialsDTO.SocialNetworkID)
                                            {
                                                case 1:
                                                    {
                                                        #region Facebook is Primary
                                                        if (referenceinfo.ResultID.Contains("_"))
                                                        {
                                                            string[] ids = referenceinfo.ResultID.Split('_');

                                                            publishParameters.link = "http://www.facebook.com/" + ids[0] + "/posts/" + ids[1];
                                                            if (info.Message.Length > maxLength)
                                                            {
                                                                //publishParameters.status = info.Message.Substring(0, maxLength);
                                                                publishParameters.status = info.Message.Substring(0, maxLength - 3).PadRight(maxLength, '.');
                                                            }
                                                            else
                                                            {
                                                                publishParameters.status = info.Message;
                                                            }
                                                        }
                                                        else
                                                        {
                                                            publishParameters.link = "http://www.facebook.com/" + referenceinfo.ResultID;
                                                            if (info.Message.Length > maxLength)
                                                            {
                                                                //publishParameters.status = info.Message.Substring(0, maxLength);
                                                                publishParameters.status = info.Message.Substring(0, maxLength - 3).PadRight(maxLength, '.');
                                                            }
                                                            else
                                                            {
                                                                publishParameters.status = info.Message;
                                                            }
                                                        }
                                                        
                                                        #endregion
                                                    }
                                                    break;
                                                case 2:
                                                    {
                                                        #region Google+ is Primary

                                                        publishParameters.link = referenceinfo.ResultID;
                                                        if (info.Message.Length > maxLength)
                                                        {
                                                            //publishParameters.status = info.Message.Substring(0, maxLength);
                                                            publishParameters.status = info.Message.Substring(0, maxLength - 3).PadRight(maxLength, '.');
                                                        }
                                                        else
                                                        {
                                                            publishParameters.status = info.Message;
                                                        }

                                                        #endregion
                                                    }
                                                    break;

                                            }

                                            //publishParameters.postType = PostTypeEnum.Link;
                                        }
                                        else
                                        {
                                            publishParameters.link = info.Link;
                                            publishParameters.status = info.Message;

                                            //publishParameters.postType = info.PostType;
                                        }
                                    }
                                    #endregion
                                }
                                else
                                {
                                    publishParameters.link = info.Link;
                                    publishParameters.status = info.Message;

                                    //publishParameters.postType = info.PostType;
                                                                        
                                    if (info.PostType == PostTypeEnum.Photo)
                                    {
                                        publishParameters.imagePath = PublishImageUploadBaseURL + info.ImageURL;
                                        linklength = 24;
                                    }
                                    else if (info.PostType == PostTypeEnum.Link)
                                    {
                                        publishParameters.imagePath = string.Empty;
                                        linklength = 24;
                                    }
                                    else
                                    {
                                        publishParameters.imagePath = string.Empty;
                                    }

                                    int TwitterStatusLength = (info.Message + linklength).Length;

                                    if (TwitterStatusLength >= TwitterMaxLength)
                                    {                                        
                                        int maxLength = TwitterMaxLength - (linklength);

                                        if (info.Message.Length > maxLength)
                                        {                                           
                                            publishParameters.status = info.Message.Substring(0, maxLength - 3).PadRight(maxLength,'.');
                                        }
                                        else
                                        {
                                            publishParameters.status = info.Message;
                                        }
                                    }

                                }

                               
                              
                                SITwitterPublishConnectionParameters publishConnectionParameters = new SITwitterPublishConnectionParameters(publishParameters);
                                TwitterPublishConnection publishconnection = new TwitterPublishConnection(publishConnectionParameters);

                                switch (info.PostType)
                                {
                                    case PostTypeEnum.Status:
                                        {
                                            #region Status

                                            ProviderFactory.Social.SetPostTargetJobID(data.PostTargetID, job.ID.Value);
                                            TwitterResponse objTwitterResponse = publishconnection.StatusUpdate();

                                            if (!string.IsNullOrWhiteSpace(objTwitterResponse.ID))
                                            {
                                                ProviderFactory.Social.SetPostTargetResults(data.PostTargetID, objTwitterResponse.ID, null);
                                                setJobStatus(job.ID.Value, JobStatusEnum.Completed);
                                            }
                                            else
                                            {
                                                Auditor
                                                    .New(AuditLevelEnum.Error, AuditTypeEnum.PublishFailure, new UserInfoDTO(info.UserID, null), "Twitter publish failed.")
                                                    .SetAccountID(socialCredentialsDTO.AccountID)
                                                    .SetCredentialID(info.CredentialID)
                                                    .SetAuditDataObject(new AuditPublishFailure()
                                                    {
                                                        JobID = job.ID.Value,
                                                        PostImageID = data.PostImageID.GetValueOrDefault(0),
                                                        PostTargetID = data.PostTargetID,
                                                        SocialNetworkResponse = objTwitterResponse.status,
                                                        CredentialID = socialCredentialsDTO != null ? socialCredentialsDTO.ID : 0,
                                                        AccountID = socialCredentialsDTO != null ? socialCredentialsDTO.AccountID : 0,
                                                        SocialAppID = socialCredentialsDTO != null ? socialCredentialsDTO.SocialAppID : 0,
                                                        SocialNetworkID = socialCredentialsDTO != null ? socialCredentialsDTO.SocialNetworkID : 0
                                                    })
                                                    .Save(ProviderFactory.Logging);

                                                //if (data.RemainingRetries == 0)
                                                {
                                                    // set failed in post target
                                                    ProviderFactory.Social.SetPostTargetFailed(data.PostTargetID);
                                                }

                                                setJobStatus(job.ID.Value, JobStatusEnum.Failed);
                                            }
                                            #endregion
                                        }
                                        break;
                                    case PostTypeEnum.Link:
                                        {
                                            #region Link

                                            ProviderFactory.Social.SetPostTargetJobID(data.PostTargetID, job.ID.Value);
                                            TwitterResponse objTwitterResponse = publishconnection.StatusUpdate();

                                            if (!string.IsNullOrWhiteSpace(objTwitterResponse.ID))
                                            {
                                                ProviderFactory.Social.SetPostTargetResults(data.PostTargetID, objTwitterResponse.ID, null);
                                                setJobStatus(job.ID.Value, JobStatusEnum.Completed);
                                            }
                                            else
                                            {
                                                Auditor
                                                     .New(AuditLevelEnum.Error, AuditTypeEnum.PublishFailure, new UserInfoDTO(info.UserID, null), "Twitter publish failed.")
                                                     .SetAccountID(socialCredentialsDTO.AccountID)
                                                     .SetCredentialID(info.CredentialID)
                                                     .SetAuditDataObject(new AuditPublishFailure()
                                                     {
                                                         JobID = job.ID.Value,
                                                         PostImageID = data.PostImageID.GetValueOrDefault(0),
                                                         PostTargetID = data.PostTargetID,
                                                         CredentialID = socialCredentialsDTO != null ? socialCredentialsDTO.ID : 0,
                                                         AccountID = socialCredentialsDTO != null ? socialCredentialsDTO.AccountID : 0,
                                                         SocialAppID = socialCredentialsDTO != null ? socialCredentialsDTO.SocialAppID : 0,
                                                         SocialNetworkID = socialCredentialsDTO != null ? socialCredentialsDTO.SocialNetworkID : 0,
                                                         SocialNetworkResponse = string.Format("{0} {1}", objTwitterResponse.status, objTwitterResponse.error)
                                                     })
                                                     .Save(ProviderFactory.Logging)
                                                ;

                                                //if (data.RemainingRetries == 0)
                                                {
                                                    // set failed in post target
                                                    ProviderFactory.Social.SetPostTargetFailed(data.PostTargetID);
                                                }

                                                setJobStatus(job.ID.Value, JobStatusEnum.Failed);
                                            }
                                            #endregion
                                        }
                                        break;
                                    case PostTypeEnum.Photo:
                                        {
                                            #region Photo

                                            ProviderFactory.Social.SetPostTargetJobID(data.PostTargetID, job.ID.Value);

                                          //  string resultforLog = publishconnection.StatusUpdateWithMediaLog();

                                          //  Auditor
                                          //    .New(AuditLevelEnum.Information, AuditTypeEnum.PublishActivity, UserInfoDTO.System, "")
                                          //    .SetAuditDataObject(
                                          //        AuditPublishActivity
                                          //        .New(data.PostID, AuditPublishActivityTypeEnum.PublishProcessorEntry)
                                          //        .SetPostID(data.PostID)
                                          //        .SetPostTargetID(data.PostTargetID)
                                          //        .SetPostImageID(data.PostImageID)
                                          //        .SetJobID(job.ID)
                                          //        .SetDescription("TW: " + resultforLog)
                                          //    )
                                          //    .Save(ProviderFactory.Logging)
                                          //;


                                            //ProviderFactory.Social.SetPostImageJobID(data.PostImageID.Value, data.PostTargetID, job.ID.Value);
                                            TwitterResponse objTwitterResponse = publishconnection.StatusUpdateWithMedia();

                                            if (!string.IsNullOrWhiteSpace(objTwitterResponse.ID))
                                            {
                                                ProviderFactory.Social.SetPostTargetResults(data.PostTargetID, objTwitterResponse.ID, null);
                                                //ProviderFactory.Social.SetPostImageResults(data.PostImageID.Value, data.PostTargetID, objTwitterResponse.ID, null);
                                                setJobStatus(job.ID.Value, JobStatusEnum.Completed);
                                            }
                                            else
                                            {
                                                Auditor
                                                    .New(AuditLevelEnum.Error, AuditTypeEnum.PublishFailure, new UserInfoDTO(info.UserID, null), "Twitter publish failed.")
                                                    .SetAccountID(socialCredentialsDTO.AccountID)
                                                    .SetCredentialID(info.CredentialID)
                                                    .SetAuditDataObject(new AuditPublishFailure()
                                                    {
                                                        JobID = job.ID.Value,
                                                        PostImageID = data.PostImageID.GetValueOrDefault(0),
                                                        PostTargetID = data.PostTargetID,
                                                        SocialNetworkResponse = string.Format("{0} {1}", objTwitterResponse.status, objTwitterResponse.error),
                                                        CredentialID = socialCredentialsDTO != null ? socialCredentialsDTO.ID : 0,
                                                        AccountID = socialCredentialsDTO != null ? socialCredentialsDTO.AccountID : 0,
                                                        SocialAppID = socialCredentialsDTO != null ? socialCredentialsDTO.SocialAppID : 0,
                                                        SocialNetworkID = socialCredentialsDTO != null ? socialCredentialsDTO.SocialNetworkID : 0
                                                    })
                                                    .Save(ProviderFactory.Logging);

                                                //if (data.RemainingRetries == 0)
                                                {
                                                    // set failed in post target
                                                    ProviderFactory.Social.SetPostTargetFailed(data.PostTargetID);
                                                }

                                                setJobStatus(job.ID.Value, JobStatusEnum.Failed);
                                            }
                                            #endregion
                                        }
                                        break;
                                }

                                #endregion
                            }
                            break;
                    }
                }
                catch (Exception ex)
                {
                    setJobStatus(job.ID.Value, JobStatusEnum.Failed);
                    ProviderFactory.Logging.LogException(ex, job.ID.Value);
                    ProviderFactory.Logging.Audit(AuditLevelEnum.Exception, AuditTypeEnum.ProcessorException, ex.Message, null, null, null, null, null, null);
                }
                job = getNextJob();
            }

        }
    }
}
