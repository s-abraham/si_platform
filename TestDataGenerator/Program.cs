﻿//using SI;
//using SI.BLL;
//using SI.DTO;
//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Text;
//using System.Threading;
//using System.Threading.Tasks;

namespace TestDataGenerator
{
    class Program
    {
        static void Main(string[] args)
        {
        }
    }
}
//namespace TestDataGenerator
//{
//    class Program
//    {
//        private static Random _r = new Random(DateTime.Now.Millisecond + DateTime.Now.Minute);

//        static void Main(string[] args)
//        {
//            Locale.RegisterCustomCultures();
//            createTestData();
//        }

//        private static string[] _firstNames = { 
//            "James", "John", "Robert", "Michael", "William", "David", "Richard", "Charles", "Joseph", "Thomas", "Christopher",
//            "Daniel", "Paul", "Mark", "Donald", "George", "Kenneth", "Steven", "Edward", "Brian", "Ronald", "Anthony", "Kevin",
//            "Jason", "Matthew", "Gary", "Timothy", "Jose", "Laurence",

//            "Mary", "Linda", "Barbara", "Elizabeth", "Jennifer", "Maria", "Suan", "Margaret", "Dorothy", "Lisa", "Nancy", "Karen",
//            "Betty", "Helen", "Sandra", "Donna", "Carol", "Ruth", "Sharon", "Michelle", "Laura", "Sarah", "Kimberly", "Deborah",
//            "Jessica", "Shirley", "Cynthia", "Angela", "Melissa", "Brenda"
//        };

//        private static string[] _lastNames = { 
//            "Smith", "Johnson", "Williams", "Jones", "Brown", "Davis", "Miller", "Wilson", "Moore", "Taylor", "Anderson",
//            "Thomas", "Jackson", "White", "Harris", "Martin", "Thompson", "Garcia", "Martinez", "Robinson", "Clark", "Rodriguez",
//            "Lewis", "Lee", "Walker", "Hall", "Allen", "Young", "Hernandez", "King", "Wright", "Lopez"
//        };

//        private static string[] _streetPrefixes = { 
//            "South", "North", "East", "West", "Old", "New"
//        };

//        private static string[] _streetSuffixes = { 
//            "Road", "Lane", "Parkway", "Avenue", "Boulevard", "Street"
//        };

//        private static string[] _streetNames = { 
//            "Second", "Third", "First", "Fourth", "Park", "Fifth", "Main", "Sixth", "Oak", "Seventh", "Pine", "Maple", "Cedar",
//            "Eighth", "Elm", "Washington", "Ninth", "Lake", "Pondview", "Wicker", "Dartmuth", "Viceroy", "Hill", "Burns"
//            , "Temple", "Edgemore", "Commerce", "Standish", "Mackey", "Iroquois", "Hanson", "Tanner", "Thurston", "Lincoln"
//            , "Iron", "Billings", "Tower", "Finnley", "Mack", "Otto", "Klein", "Duncan", "Gratoit", "Groesbeck", "14 Mile"
//        };

//        private static string[] _companyNames = { 
//            "SAS", "Boston Consulting", "Wegman Foods", "Google", "NetApp", "Zappos", "Camden", "Nugget Marketing", "Dreamworks", 
//            "Scottrade", "Alston", "Baird", "Mercedes", "Chevrolet", "USAA", "Cisco", "Container Store", "DPR Construction", 
//            "Goldman Sachs", "Whole Foods Market", "Quicken Loans", "NuStar Energy", "Qualcomm", "QuikTrip", "Genetech", 
//            "Southern Ohio Medical Center", "Scripps", "American Fidelity", "Balfour Beatty", "Devon Energy"
//        };

//        private static string[] _companySuffixes = { 
//            "Incorporated", "Company", "LLC", "Consolidated", "Enterprises", "Holdings", "Worldwide", "Corporation"
//        };


//        private static int _usersCreated = 0;
//        private static int _accountsCreated = 0;
//        private static int _maxNest = 0;

//        private static void createTestData()
//        {

//            long ptCreateAccounts = ProviderFactory.Security.GetPermissionType(PermissionTypeEnum.accounts_create).ID;
//            long ptAdminAccounts = ProviderFactory.Security.GetPermissionType(PermissionTypeEnum.accounts_admin).ID;
//            long ptUserAdmin = ProviderFactory.Security.GetPermissionType(PermissionTypeEnum.users_admin).ID;
//            long ptVirtualGroupsAdmin = ProviderFactory.Security.GetPermissionType(PermissionTypeEnum.virtualGroups_admin).ID;



//            AccountDTO aMaster = new AccountDTO()
//            {
//                Name = "Master",
//                ParentAccountID = null,
//                Type = AccountTypeDTO.Grouping
//            };
//            SaveAccountResultDTO res = ProviderFactory.Security.SaveAccount(aMaster);
//            aMaster = res.Account;



//            UserDTO dto = new UserDTO()
//            {
//                FirstName = "Master",
//                LastName = "User",
//                EmailAddress = "reynolds@autostartups.com",
//                Username = "muser",
//                Password = "password"
//            };
//            CreateUserResultDTO res2 = ProviderFactory.Security.SaveUser(dto);
//            dto = res2.User;

//            //ProviderFactory.Security.AddPermission(dto.ID.Value, aMaster.ID.Value, ptAdminAccounts, true);
//            //ProviderFactory.Security.AddPermission(dto.ID.Value, aMaster.ID.Value, ptUserAdmin, true);
//            //ProviderFactory.Security.AddPermission(dto.ID.Value, aMaster.ID.Value, ptVirtualGroupsAdmin, true);

//            for (int i = 0; i < 25; i++)
//            {
//                createRandomAccount(aMaster.ID, 1);
//            }


//        }

//        private static AccountDTO createRandomAccount(long? parentAccountID, int nestLevel)
//        {
//            long ptCreateAccounts = ProviderFactory.Security.GetPermissionType(PermissionTypeEnum.accounts_create).ID;
//            long ptAdminAccounts = ProviderFactory.Security.GetPermissionType(PermissionTypeEnum.accounts_admin).ID;
//            long ptUserAdmin = ProviderFactory.Security.GetPermissionType(PermissionTypeEnum.users_admin).ID;
//            long ptVirtualGroupsAdmin = ProviderFactory.Security.GetPermissionType(PermissionTypeEnum.virtualGroups_admin).ID;

//            if (nestLevel > _maxNest)
//            {
//                _maxNest = nestLevel;
//            }

//            //the account name
//            string companyName = _companyNames[_r.Next(0, _companyNames.Count() - 1)];
//            if (_r.Next(0, 5) == 1)
//            {
//                companyName = string.Format("{0} {1}", companyName, _companySuffixes[_r.Next(0, _companySuffixes.Count())]);
//            }
//            companyName = string.Format("{0} {1}", companyName, _r.Next(10000, 99999));

//            //the addresses
//            long? billingAddressID = null;
//            long? physicalAddressID = null;
//            if (_r.Next(1, 3) == 1)
//            {
//                AddressDTO add = getRandomAddress();
//                billingAddressID = add.ID;
//            }
//            if (_r.Next(1, 3) == 1)
//            {
//                AddressDTO add = getRandomAddress();
//                physicalAddressID = add.ID;
//            }

//            AccountDTO account = new AccountDTO()
//            {
//                Name = companyName,
//                ParentAccountID = parentAccountID
//            };

//            switch (_r.Next(1, 4))
//            {
//                case 1:
//                    account.Type = AccountTypeDTO.Person;
//                    break;
//                case 2:
//                    account.Type = AccountTypeDTO.Grouping;
//                    break;
//                default:
//                    account.Type = AccountTypeDTO.Company;
//                    break;
//            }

//            SaveAccountResultDTO res = ProviderFactory.Security.SaveAccount(mUser.ID.Value, account);

//            if (res.Problems.Count() == 0)
//            {
//                account = res.Account;

//                _accountsCreated++;
//                stat();

//                for (int i = 0; i < _r.Next(5, 20); i++)
//                {
//                    UserDTO user = createRandomUser();
//                    if (user != null)
//                    {
//                        //switch (_r.Next(1, 4))
//                        //{
//                        //    case 1:
//                        //        ProviderFactory.Security.AddPermission(user.ID.Value, account.ID.Value, ptAdminAccounts, (_r.Next(0, 10) >= 5));
//                        //        break;
//                        //    case 2:
//                        //        ProviderFactory.Security.AddPermission(user.ID.Value, account.ID.Value, ptVirtualGroupsAdmin, (_r.Next(0, 10) >= 5));
//                        //        break;
//                        //    case 3:
//                        //        ProviderFactory.Security.AddPermission(user.ID.Value, account.ID.Value, ptUserAdmin, (_r.Next(0, 10) >= 5));
//                        //        break;
//                        //    default:
//                        //        break;
//                        //}
//                    }
//                }


//                if (_r.Next(1, 4) == 1)
//                {
//                    int childAccounts = _r.Next(0, 7);
//                    for (int i = 0; i < childAccounts; i++)
//                    {
//                        createRandomAccount(res.Account.ID, nestLevel + 1);
//                        //ProviderFactory.Security.AddPermission(res2.User.ID.Value, res.Account.ID.Value, ptAdminAccounts, true);
//                    }
//                }

//            }
//            else
//            {
//                res = res;
//            }



//            return null;

//        }

//        private static UserDTO createRandomUser()
//        {
//            _usersCreated++;
//            string fname = _firstNames[_r.Next(0, _firstNames.Count())];
//            string lname = _lastNames[_r.Next(0, _lastNames.Count())];
//            UserDTO dto = new UserDTO()
//                {
//                    FirstName = fname,
//                    LastName = lname,
//                    EmailAddress = string.Format("{0}.{1}@{2}.com", fname, _r.Next(10000, 99999), lname),
//                    Username = string.Format("{0}{1}{2}", fname.Substring(0, 1), lname, _r.Next(10000, 99999)),
//                    Password = "password"
//                };
//            CreateUserResultDTO res2 = ProviderFactory.Security.SaveUser(dto);

//            UserDTO user = res2.User;

//            if (user != null)
//            {
//                ProviderFactory.Security.SaveUserSetting(user.ID.Value, Security.GetRandomCode(10), Security.GetRandomCode(10));
//                ProviderFactory.Security.SaveUserSetting(user.ID.Value, Security.GetRandomCode(10), Security.GetRandomCode(10));
//            }

//            return user;
//        }



//        private static AddressDTO getRandomAddress()
//        {
//            CountryDTO country = ProviderFactory.Lists.GetCountryByPrefix("US");

//            ZipCodeDTO zip = ProviderFactory.Lists.GetRandomZipCode();
//            StateDTO state = ProviderFactory.Lists.GetStateByID(zip.StateID);
//            List<CityDTO> cities = ProviderFactory.Lists.GetCitiesForZipCode(zip.ZipCode);

//            while (cities.Count() == 0)
//            {
//                zip = ProviderFactory.Lists.GetRandomZipCode();
//                state = ProviderFactory.Lists.GetStateByID(zip.StateID);
//                cities = ProviderFactory.Lists.GetCitiesForZipCode(zip.ZipCode);
//            }

//            CityDTO city = cities[_r.Next(0, cities.Count() - 1)];



//            string streetName = _r.Next(10, 9999).ToString();

//            if (_r.Next(0, 4) == 1)
//                streetName = string.Format("{0} {1}", streetName, _streetPrefixes[_r.Next(0, _streetPrefixes.Count())]);

//            streetName = string.Format("{0} {1}", streetName, _streetNames[_r.Next(0, _streetNames.Count())]);

//            if (_r.Next(0, 2) == 1)
//                streetName = string.Format("{0} {1}", streetName, _streetSuffixes[_r.Next(0, _streetSuffixes.Count())]);

//            string street2 = null;
//            if (_r.Next(0, 5) == 1)
//                street2 = string.Format("Suite {0}", _r.Next(100, 500));

//            AddressDTO address = new AddressDTO()
//            {
//                CityID = city.ID,
//                StateID = state.ID,
//                ZipCode = zip.ZipCode,
//                CountryID = country.ID,
//                Street1 = streetName,
//                Street2 = street2
//            };

//            SaveAddressResultDTO result = ProviderFactory.Security.SaveAddress(address);
//            address = result.Address;

//            return address;

//        }

//        private static void stat()
//        {
//            Console.WriteLine("accounts:{0} - users:{1} - nest:{2}", _accountsCreated, _usersCreated, _maxNest);
//        }
//    }
//}
