﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace YouTube.Entities
{
    public class YouTubeResult
    {
        public bool IsSuccess { get; set; }

        public string ErrorMessage { get; set; }

        public string RawJSON { get; set; }
    }
}
