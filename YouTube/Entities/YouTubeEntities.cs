﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace YouTube.Entities
{
    public class YouTubeEntities
    {

    }

    public class YouTubeVideoInfo
    {
        public YouTubeVideoInfo()
        {
        }

        public string Title { get; set; }
        public string Image { get; set; }
        public string Description { get; set; }
    }

}
