﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace YouTube.Entities
{
    public class UserProfileResult : YouTubeResult
    {
        public UserProfileResult()
        {
            Author = new AuthorEntity();
            Statistics = new UserProfileStatisticsEntity();
            base.IsSuccess = true;
        }

        public string PublishedDate { get; set; }
        public string UpdatedDate { get; set; }
        public string Title { get; set; }
        public string Summary { get; set; }
        public string ChannelId { get; set; }
        public AuthorEntity Author { get; set; }
        public string GooglePlusUserId { get; set; }
        public string Location { get; set; }
        public UserProfileStatisticsEntity Statistics { get; set; }
        public string MediaThumbnail { get; set; }
        public string DisplayName { get; set; }
        public string UserName { get; set; }
    }

    public class AuthorEntity
    {
        public string Name { get; set; }
        public string URI { get; set; }
        public string UserId { get; set; }
    }

    public class UserProfileStatisticsEntity
    {
        public string LastWebAccess { get; set; }
        public int SubscriberCount { get; set; }
        public int VideoWatchCount { get; set; }
        public int ViewCount { get; set; }
        public int TotalUploadViews { get; set; }
    }

    public class ChannelInfoResult : YouTubeResult
    {
        public ChannelInfoResult()
        {
            ChannelStatistics = new ChannelStatisticsEntity();
            VideoUploads = new VideosList();
        }

        public string ChannelId { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public string PublishedAt { get; set; }
        public string Thumbnail { get; set; }

        public string PlaylistLikes { get; set; }
        public string PlaylistFavorites { get; set; }
        public string PlaylistUploads { get; set; }

        public ChannelStatisticsEntity ChannelStatistics { get; set; }

        public VideosList VideoUploads { get; set; }
    }

    public class ChannelStatisticsEntity
    {
        public bool HiddenSubscriberCount { get; set; }
        public int ViewCount { get; set; }
        public int CommentCount { get; set; }
        public int VideoCount { get; set; }
        public int SubscriberCount { get; set; }
    }

    public class VideosList : YouTubeResult
    {
        public VideosList()
        {
            VideoList = new List<VideoDetailsEntity>();
        }

        public int TotalVideoViewsCount { get; set; }
        public int TotalVideoCommentsCount { get; set; }
        public int TotalVideoFavoriteCount { get; set; }
        public int TotalVideoLikeCount { get; set; }
        public int TotalVideoDisLikesCount { get; set; }
        public List<VideoDetailsEntity> VideoList { get; set; }
    }

    public class VideoDetailsEntity
    {
        public VideoDetailsEntity()
        {
            VideoStatistics = new VideoStatisticsEntity();
            VideoStatus = new VideoStatusEntity();
        }

        public string ChannelId { get; set; }
        public string ChannelTitle { get; set; }
        public string PlaylistId { get; set; }
        public string VideoId { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public string Thumbnail { get; set; }
        public string PrivacyStatus { get; set; }
        public string PublishedAt { get; set; }
        public string VideoURL { get; set; }
        
        public VideoStatusEntity VideoStatus { get; set; }
        public VideoStatisticsEntity VideoStatistics { get; set; }
    }

    public class VideoStatisticsEntity
    {
        public VideoStatisticsEntity()
        {
            ViewCount = 0;
            LikeCount = 0;
            DislikeCount = 0;
            FavoriteCount = 0;
            CommentCount = 0;
        }

        public int ViewCount { get; set; }
        public int LikeCount { get; set; }
        public int DislikeCount { get; set; }
        public int FavoriteCount { get; set; }
        public int CommentCount { get; set; }
    }

    public class VideoStatusEntity
    {
        public string UploadStatus { get; set; }
        public string PrivacyStatus { get; set; }
        public string License { get; set; }
        public string Embeddable { get; set; }
        public string PublicStatsViewable { get; set; }
    }
}
