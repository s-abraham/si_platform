﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;
using Google.GData.Client;
using Google.GData.Client.ResumableUpload;
using Google.GData.Extensions.Location;
using Google.GData.Extensions.MediaRss;
using Google.GData.YouTube;
using Google.YouTube;
using SI;
using YouTube.Entities;



namespace YouTube
{
    public class User : YouTubeAPI
    {
        private string _access_token;
        private string _refresh_token;
        private string _api_key;
        private string _user_id;
        private string _user_name;
        private string _google_id;
        private string _channel_id;
        private string _client_id;

        private string apiKey;
        private string UserProfileBaseURL = "http://gdata.youtube.com/feeds/api/users/{username}?alt=json&v=2";
        private string ChannelInfoBaseURL = "https://www.googleapis.com/youtube/v3/channels?part=id%2Csnippet%2CcontentDetails%2Cstatistics%2CtopicDetails&id={channelId}&key={apikey}";
        private string ChannelVideosBaseURL = "https://www.googleapis.com/youtube/v3/playlistItems?part=snippet%2CcontentDetails%2Cstatus&playlistId={playlistId}&key={apikey}&maxResults=50";
        private string BaseVideoURL = "http://www.youtube.com/watch?v={videoid}";
        private string VideoDetailsBaseURL = "https://www.googleapis.com/youtube/v3/videos?part=contentDetails%2Cstatistics%2cid%2cid%2csnippet%2cstatistics%2cstatus%2ctopicDetails&id={videoid}&key={apikey}";

        public User()
        {
            //_access_token = "ya29.AHES6ZRrB4zeOm_x8p2HAr8BYCgOlC6Ww7gUaGRxM0-Mus5G";
            //_refresh_token = "1/crtNSgi2SbNMwYbPHlgHW_4J5wNnAdZSfB3hk53iNzo";
            apiKey = "AIzaSyAy4vWUi5BOuGtamvRiAn5oREVuyqnZa8w";
            //_user_id = "O2tPnu4iOK8r4qiuSVW2Bg"; //Sanju
            //_user_name = "stunifex";
            _google_id = "109145607811942855855";
            //_channel_id = "UCO2tPnu4iOK8r4qiuSVW2Bg";
            _client_id = "257710766138-t8gfk5j3qvfbcdm22jeg5u22urb5k5dm.apps.googleusercontent.com";
        }

        public User(string accessToken, string apiKey, string userId, string channelId)
        {
            _access_token = accessToken;
            _api_key = apiKey;
            _user_id = userId;
            _channel_id = channelId;
            _client_id = "257710766138-t8gfk5j3qvfbcdm22jeg5u22urb5k5dm.apps.googleusercontent.com";
        }

        public UserProfileResult GetUserProfile(string userid)
        {
            //Sample URL: http://gdata.youtube.com/feeds/api/users/6Z4SkRhcSbMOntpnDx2HWw?alt=json&v=2 (Arpan)
            //Sample URL: http://gdata.youtube.com/feeds/api/users/UCO2tPnu4iOK8r4qiuSVW2Bg?alt=json&v=2 (Sanju)
            var result = new UserProfileResult();

            try
            {
                if (!string.IsNullOrEmpty(userid))
                {
                    if (string.IsNullOrEmpty(UserProfileBaseURL))
                    {
                        UserProfileBaseURL = "http://gdata.youtube.com/feeds/api/users/{Username}?alt=json&v=2";
                    }

                    string URL = UserProfileBaseURL.Replace("{username}", userid);

                    try
                    {
                        dynamic dynamicResult = GetRequestDynamic(URL);

                        if (dynamicResult != null)
                        {
                            if (dynamicResult.entry != null)
                            {
                                result.PublishedDate = dynamicResult.entry.published.val ?? "";
                                result.UpdatedDate = dynamicResult.entry.updated.val ?? "";
                                result.Title = dynamicResult.entry.title.val ?? ""; ;

                                result.Summary = dynamicResult.entry.val ?? "";
                                result.ChannelId = dynamicResult.entry.yt_channelId.val ?? "";

                                if (!string.IsNullOrEmpty(_channel_id))
                                {
                                    _channel_id = result.ChannelId;
                                }

                                if (dynamicResult.entry.author[0] != null)
                                {
                                    result.Author.Name = dynamicResult.entry.author[0].name.val ?? "";
                                    result.Author.URI = dynamicResult.entry.author[0].uri.val ?? "";
                                    result.Author.UserId = dynamicResult.entry.author[0].yt_userId.val ?? "";
                                }

                                result.GooglePlusUserId = dynamicResult.entry.yt_googlePlusUserId.val ?? "";
                                result.Location = dynamicResult.entry.yt_location.val ?? "";

                                result.MediaThumbnail = dynamicResult.entry.media_thumbnail.url ?? "";
                                result.DisplayName = dynamicResult.entry.yt_username.display ?? "";
                                result.UserName = dynamicResult.entry.yt_username.val ?? "";

                                if (dynamicResult.entry.yt_statistics != null)
                                {
                                    result.Statistics.LastWebAccess = dynamicResult.entry.yt_statistics.lastWebAccess ?? "";
                                    result.Statistics.SubscriberCount = dynamicResult.entry.yt_statistics.subscriberCount ?? 0;
                                    result.Statistics.VideoWatchCount = dynamicResult.entry.yt_statistics.videoWatchCount ?? 0;
                                    result.Statistics.ViewCount = dynamicResult.entry.yt_statistics.viewCount ?? 0;
                                    result.Statistics.TotalUploadViews = dynamicResult.entry.yt_statistics.totalUploadViews ?? 0;
                                }

                            }
                        }
                        else
                        {
                            result.IsSuccess = false;
                            result.ErrorMessage = "No response from YouTube";
                        }
                    }
                    catch (Exception ex)
                    {
                        result.IsSuccess = false;
                        result.ErrorMessage = ex.Message;
                    }
                }
                else
                {
                    result.IsSuccess = false;
                    result.ErrorMessage = "Missing required parameter UserId";
                }
            }
            catch (Exception ex)
            {
                result.IsSuccess = false;
                result.ErrorMessage = ex.Message;
            }
            return result;
        }
             
        public void GetUploadedVideos(string userName)
        {
            
            //Get Channel for User
            //https://www.googleapis.com/youtube/v3/channels?part=id%2Csnippet%2CcontentDetails%2Cstatistics%2CtopicDetails&id=UCO2tPnu4iOK8r4qiuSVW2Bg&key=AIzaSyAy4vWUi5BOuGtamvRiAn5oREVuyqnZa8w

            //Get Upload Playlist from contentDetails
            //https://www.googleapis.com/youtube/v3/playlistItems?part=snippet%2CcontentDetails%2Cstatus&playlistId=UUO2tPnu4iOK8r4qiuSVW2Bg&key=AIzaSyAy4vWUi5BOuGtamvRiAn5oREVuyqnZa8w

            //Get Likes Playlist
            //https://www.googleapis.com/youtube/v3/playlistItems?part=snippet%2CcontentDetails%2Cstatus&playlistId=LLO2tPnu4iOK8r4qiuSVW2Bg&key=AIzaSyAy4vWUi5BOuGtamvRiAn5oREVuyqnZa8w

            //Get Favorites Playlist
            //https://www.googleapis.com/youtube/v3/playlistItems?part=snippet%2CcontentDetails%2Cstatus&playlistId=FLO2tPnu4iOK8r4qiuSVW2Bg&key=AIzaSyAy4vWUi5BOuGtamvRiAn5oREVuyqnZa8w
            
        }
        
        public ChannelInfoResult GetChannelInfo(string channelId, bool getVideos = false)
        {
            var result = new ChannelInfoResult();

            //Sample: https://www.googleapis.com/youtube/v3/channels?part=id%2Csnippet%2CcontentDetails%2Cstatistics%2CtopicDetails&id=UCO2tPnu4iOK8r4qiuSVW2Bg&key=AIzaSyAy4vWUi5BOuGtamvRiAn5oREVuyqnZa8w
            //var baseURL = "https://www.googleapis.com/youtube/v3/channels?part=id%2Csnippet%2CcontentDetails%2Cstatistics%2CtopicDetails&id={channelId}&key={apikey}";

            try
            {
                if (!string.IsNullOrEmpty(channelId))
                {
                    string URL = ChannelInfoBaseURL.Replace("{channelId}", channelId).Replace("{apikey}", apiKey);

                    dynamic dynamicResult = GetRequestDynamic(URL);

                    if (dynamicResult != null && (dynamicResult.items != null && dynamicResult.items.Count > 0))
                    {
                        result.ChannelId = dynamicResult.items[0].id ?? "";
                        result.Title = dynamicResult.items[0].snippet.title ?? "";
                        result.Description = dynamicResult.items[0].snippet.description ?? "";
                        result.PublishedAt = dynamicResult.items[0].snippet.publishedAt ?? "";
                        result.Thumbnail = dynamicResult.items[0].snippet.thumbnails.def.url ?? "";

                        result.PlaylistLikes = dynamicResult.items[0].contentDetails.relatedPlaylists.likes ?? "";
                        result.PlaylistFavorites = dynamicResult.items[0].contentDetails.relatedPlaylists.favorites ?? "";
                        result.PlaylistUploads = dynamicResult.items[0].contentDetails.relatedPlaylists.uploads ?? "";

                        result.ChannelStatistics.ViewCount = dynamicResult.items[0].statistics.viewCount ?? 0;
                        result.ChannelStatistics.CommentCount = dynamicResult.items[0].statistics.commentCount ?? 0;
                        result.ChannelStatistics.SubscriberCount = dynamicResult.items[0].statistics.subscriberCount ?? 0;
                        result.ChannelStatistics.HiddenSubscriberCount = dynamicResult.items[0].statistics.hiddenSubscriberCount ?? true;
                        result.ChannelStatistics.VideoCount = dynamicResult.items[0].statistics.videoCount ?? 0;

                        if (getVideos && !string.IsNullOrEmpty(result.PlaylistUploads))
                        {
                            var videosList = GetChannelVideos(result.PlaylistUploads);

                            if (videosList.VideoList != null && videosList.VideoList.Count > 0)
                            {
                                result.VideoUploads = videosList;
                            }
                        }
                    }
                    else
                    {
                        result.IsSuccess = false;
                        result.ErrorMessage = "No response from YouTube";
                    }
                }
                else
                {
                    result.IsSuccess = false;
                    result.ErrorMessage = "Missing required parameter: Channel Id and/or API Key";
                }
            }
            catch (Exception ex)
            {
                result.IsSuccess = false;
                result.ErrorMessage = ex.Message;
            }
            return result;
        }

        public VideosList GetChannelVideos(string playlistid)
        {
            //Sample: https://www.googleapis.com/youtube/v3/playlistItems?part=snippet%2CcontentDetails%2Cstatus&playlistId=UUO2tPnu4iOK8r4qiuSVW2Bg&key=AIzaSyAy4vWUi5BOuGtamvRiAn5oREVuyqnZa8w

            var result = new VideosList();

            try
            {
                if (!string.IsNullOrEmpty(playlistid))
                {
                    if (string.IsNullOrEmpty(ChannelVideosBaseURL))
                    {
                        ChannelVideosBaseURL = "https://www.googleapis.com/youtube/v3/playlistItems?part=snippet%2CcontentDetails%2Cstatus&playlistId={playlistId}&key={apikey}&maxResults=50";
                    }

                    string URL = ChannelVideosBaseURL.Replace("{playlistId}", playlistid).Replace("{apikey}", apiKey);

                    dynamic dynamicResult = GetRequestDynamic(URL);

                    if (dynamicResult != null && dynamicResult.items.Count > 0)
                    {
                        string nextPageToken = string.Empty;

                        do
                        {
                            if (!string.IsNullOrEmpty(nextPageToken))
                            {
                                string ChannelVideosBaseURLNextPage = ChannelVideosBaseURL + "&pageToken={pagetoken}";
                                URL = ChannelVideosBaseURLNextPage.Replace("{playlistId}", playlistid).Replace("{apikey}", apiKey).Replace("{pagetoken}", nextPageToken);

                                dynamicResult = GetRequestDynamic(URL);
                            }
                            foreach (var video in dynamicResult.items)
                            {
                                var videoResult = new VideoDetailsEntity();

                                videoResult.ChannelId = video.snippet.channelId ?? "";
                                videoResult.PublishedAt = video.snippet.publishedAt ?? "";
                                videoResult.Title = video.snippet.title ?? "";
                                videoResult.Description = video.snippet.description ?? "";
                                videoResult.Thumbnail = video.snippet.thumbnails.def.url ?? "";
                                videoResult.ChannelTitle = video.snippet.channelTitle ?? "";
                                videoResult.PlaylistId = video.snippet.playlistId ?? "";
                                videoResult.VideoId = video.contentDetails.videoId ?? "";
                                videoResult.PrivacyStatus = video.status.privacyStatus ?? "";
                                videoResult.VideoURL = BaseVideoURL.Replace("{videoid}", videoResult.VideoId);

                                if (!string.IsNullOrEmpty(videoResult.VideoId))
                                {
                                    var videoDetails = GetVideoDetails(videoResult.VideoId);

                                    if (videoDetails != null)
                                    {
                                        videoResult.VideoStatistics = videoDetails.VideoStatistics;
                                        videoResult.VideoStatus = videoDetails.VideoStatus;

                                        result.TotalVideoCommentsCount += videoDetails.VideoStatistics.CommentCount;
                                        result.TotalVideoDisLikesCount += videoDetails.VideoStatistics.DislikeCount;
                                        result.TotalVideoFavoriteCount += videoDetails.VideoStatistics.FavoriteCount;
                                        result.TotalVideoLikeCount += videoDetails.VideoStatistics.LikeCount;
                                        result.TotalVideoViewsCount += videoDetails.VideoStatistics.ViewCount;
                                    }
                                }

                                result.VideoList.Add(videoResult);
                            }
                            
                            nextPageToken = dynamicResult.nextPageToken;

                        } while (!string.IsNullOrEmpty(nextPageToken));

                        
                    }
                    else
                    {
                        result.IsSuccess = false;
                        result.ErrorMessage = "No response from YouTube";
                    }
                }
                else
                {
                    result.IsSuccess = false;
                    result.ErrorMessage = "Missing required parameter: Uploaded Playlist Id and/or API Key";
                }
            }
            catch (Exception ex)
            {
                result.IsSuccess = false;
                result.ErrorMessage = ex.Message;
            }
            return result;
        }

        private VideoDetailsEntity GetVideoDetails(string videoid)
        {
            var result = new VideoDetailsEntity();
            //https://www.googleapis.com/youtube/v3/videos?part=contentDetails%2Cstatistics%2cid%2cid%2csnippet&id=AKLrbjLUAjk%2cstatistics%2cstatus%2ctopicDetails&key=AIzaSyAy4vWUi5BOuGtamvRiAn5oREVuyqnZa8w

            try
            {
                if (!string.IsNullOrEmpty(videoid))
                {
                    if (string.IsNullOrEmpty(ChannelVideosBaseURL))
                    {
                        VideoDetailsBaseURL = "https://www.googleapis.com/youtube/v3/videos?part=contentDetails%2Cstatistics%2cid%2cid%2csnippet%2cstatistics%2cstatus%2ctopicDetails&id={videoid}&key={apikey}";
                    }

                    string URL = VideoDetailsBaseURL.Replace("{videoid}", videoid).Replace("{apikey}", apiKey);

                    dynamic dynamicResult = GetRequestDynamic(URL);

                    if (dynamicResult != null && dynamicResult.items.Count == 1)
                    {
                        result.VideoStatistics.ViewCount = dynamicResult.items[0].statistics.viewCount ?? 0;
                        result.VideoStatistics.LikeCount = dynamicResult.items[0].statistics.likeCount ?? 0;
                        result.VideoStatistics.DislikeCount = dynamicResult.items[0].statistics.dislikeCount ?? 0;
                        result.VideoStatistics.FavoriteCount = dynamicResult.items[0].statistics.favoriteCount ?? 0;
                        result.VideoStatistics.CommentCount = dynamicResult.items[0].statistics.commentCount ?? 0;

                        result.VideoStatus.UploadStatus = dynamicResult.items[0].status.uploadStatus ?? "";
                        result.VideoStatus.PrivacyStatus = dynamicResult.items[0].status.privacyStatus ?? "";
                        result.VideoStatus.License = dynamicResult.items[0].status.license ?? "";
                        result.VideoStatus.Embeddable = dynamicResult.items[0].status.embeddable ?? "";
                        result.VideoStatus.PublicStatsViewable = dynamicResult.items[0].status.publicStatsViewable ?? "";
                    }                    
                }
            }
            catch (Exception ex)
            {                
            }

            return result;
        }

        public YouTubeVideoInfo GetVideoInfoforPublish(string videoURL)
        {
            YouTubeVideoInfo youTubeVideoInfo = new YouTubeVideoInfo();

            try
            {
                if (!string.IsNullOrEmpty(videoURL))
                {
                    if (string.IsNullOrEmpty(ChannelVideosBaseURL))
                    {
                        VideoDetailsBaseURL = "https://www.googleapis.com/youtube/v3/videos?part=contentDetails%2Cstatistics%2cid%2cid%2csnippet%2cstatistics%2cstatus%2ctopicDetails&id={videoid}&key={apikey}";
                    }

                    string videoid = string.Empty;
                    if (videoURL.ToLower().Contains("youtube.com"))
                    {
                        if (videoURL.ToLower().Contains("youtube.com"))
                        {
                            int startindex = videoURL.IndexOf("v=") + 2;
                            int endindex = videoURL.Length - startindex;

                            videoid = videoURL.Substring(startindex, endindex);
                        }
                    }

                    if(!string.IsNullOrEmpty(videoid))
                    {
                        string URL = VideoDetailsBaseURL.Replace("{videoid}", videoid).Replace("{apikey}", apiKey);

                        dynamic dynamicResult = GetRequestDynamic(URL);
                        if (dynamicResult != null && dynamicResult.items.Count == 1)
                        {
                            youTubeVideoInfo.Title = dynamicResult.items[0].snippet.title;
                            youTubeVideoInfo.Description = dynamicResult.items[0].snippet.description;
                            youTubeVideoInfo.Image = dynamicResult.items[0].snippet.thumbnails.medium.url;
                        }

                    }
                }
            }
            catch(Exception ex)
            {
                return youTubeVideoInfo;
            }

            return youTubeVideoInfo;
        }

        public void UploadVideo(string videoLocalPath)
        {
            var endPoint = "https://www.googleapis.com/upload/youtube/v3/videos?uploadType=resumable&part=snippet,status,contentDetails"; 
            var maxSize = 68719476736; // 64 gig

            var fileBytes = System.IO.File.ReadAllBytes(videoLocalPath);

            if (maxSize > fileBytes.Length)
            {
                var json = Encoding.ASCII.GetBytes("{  \"snippet\": {    \"title\": \"My video title\",    \"description\": \"This is a description of my video\",    \"tags\": [\"cool\", \"video\", \"more keywords\"],    \"categoryId\": 22  },  \"status\": {    \"privacyStatus\": \"public\",    \"embeddable\": True,    \"license\": \"youtube\"  }}");

                var request = WebRequest.Create(endPoint);
                request.Headers[HttpRequestHeader.Authorization] = string.Format("Bearer {0}", _access_token);
                request.ContentLength = json.Length;
                request.ContentType = "application/json; charset=UTF-8";
                request.Headers["X-Upload-Content-Length"] = fileBytes.Length.ToString();
                request.Headers["X-Upload-Content-Type"] = "video/mp4";
                request.Method = "POST";

                using (var stream = request.GetRequestStream())
                {
                    stream.Write(json, 0, (int)json.Length);
                }

                var response = request.GetResponse();
                Debug.WriteLine(response);
            }
        }
        
        public void UploadVideo2(string videoLocalPath)
        {
            var f = new FileInfo(videoLocalPath);
            var len = f.Length;

            Debug.WriteLine(len);

            var settings = new YouTubeRequestSettings("Publish", _client_id, _api_key);
            var request = new YouTubeRequest(settings);

            var oauth2Parameters = new OAuth2Parameters();
            oauth2Parameters.ClientId = _client_id;
            oauth2Parameters.AccessToken = _access_token;
            

            var youTubeAuthenticator = new OAuth2Authenticator("Project", oauth2Parameters);


            var newVideo = new Video();
            newVideo.Title = "Testing";
            newVideo.Tags.Add(new MediaCategory("Autos", YouTubeNameTable.CategorySchema));
            newVideo.Keywords = "Testing";
            newVideo.Description = "Testing";
            newVideo.YouTubeEntry.Private = false;
            newVideo.YouTubeEntry.AccessControls.Add(new YtAccessControl("comment", "denied"));
            newVideo.YouTubeEntry.AccessControls.Add(new YtAccessControl("commentVote", "denied"));
            newVideo.YouTubeEntry.AccessControls.Add(new YtAccessControl("videoRespond",  "denied"));
            newVideo.YouTubeEntry.AccessControls.Add(new YtAccessControl("rate", "denied"));
            newVideo.YouTubeEntry.AccessControls.Add(new YtAccessControl("embed", "denied"));



            var uploader = new ResumableUploader(5346742);
            uploader.AsyncOperationCompleted += mResumableUploader_AsyncOperationCompleted;
            uploader.AsyncOperationProgress += mResumableUploader_AsyncOperationProgress;

            newVideo.YouTubeEntry.MediaSource = new MediaFileSource(videoLocalPath, "video/mp4");
            // uploader.Insert(youTubeAuthenticator, new Uri("http://www.socialdealer.com"), fs, "video/mp4", "");
            uploader.InsertAsync(youTubeAuthenticator, newVideo.YouTubeEntry, "inserter");


            //Video newVideo = new Video();

            //newVideo.Title = "My Test Movie";
            //newVideo.Tags.Add(new MediaCategory("Autos", YouTubeNameTable.CategorySchema));
            //newVideo.Keywords = "cars, funny";
            //newVideo.Description = "My description";
            //newVideo.YouTubeEntry.Private = false;
            //newVideo.Tags.Add(new MediaCategory("mydevtag, anotherdevtag",
            //  YouTubeNameTable.DeveloperTagSchema));

            //newVideo.YouTubeEntry.Location = new GeoRssWhere(37, -122);
            //// alternatively, you could just specify a descriptive string
            //// newVideo.YouTubeEntry.setYouTubeExtension("location", "Mountain View, CA");

            //newVideo.YouTubeEntry.MediaSource = new MediaFileSource(videoLocalPath,
            //  "video/quicktime");
            //Video createdVideo = request.Upload(newVideo);

            //var endPoint = "https://www.googleapis.com/videos?uploadType=resumable&part=snippet&key=" + _client_id; 
            //var maxSize = 68719476736; // 64 gig

            //var location = CompanyProvider.GetUploadLocation(this.baseUploadDirectory, companyId, FileType.Asset);
            //var filePath = System.IO.Path.Combine(location, asset.FileName);
            //using (var data = new FileStream(filePath, FileMode.Open))
            //{
            //    if (maxSize > data.Length && (asset.MimeType.ToLower().StartsWith("video/") || asset.MimeType.ToLower().Equals("application/octet-stream")))
            //    {
            //        var json = "{ \"snippet\": { \"title\": \"" + asset.FileName + "\", \"description\": \"This is a description of my video\" } }";
            //        var buffer = Encoding.ASCII.GetBytes(json);

            //        var request = WebRequest.Create(endPoint);
            //        request.Headers[HttpRequestHeader.Authorization] = string.Format("Bearer {0}", api.Tokens.AccessToken);
            //        request.Headers["X-Upload-Content-Length"] = data.Length.ToString();
            //        request.Headers["X-Upload-Content-Type"] = asset.MimeType;
            //        request.ContentType = "application/json; charset=UTF-8";
            //        request.ContentLength = buffer.Length;
            //        request.Method = "POST";

            //        using (var stream = request.GetRequestStream())
            //        {
            //            stream.Write(buffer, 0, (int)buffer.Length);
            //        }

            //        var response = request.GetResponse();
            //    }
            //}
        }

        private void mResumableUploader_AsyncOperationProgress(object sender, AsyncOperationProgressEventArgs e)
        {
            Debug.WriteLine("Async Progress");
        }

        public void mResumableUploader_AsyncOperationCompleted(object sender, AsyncOperationCompletedEventArgs asyncOperationCompletedEventArgs)
        {
            Debug.WriteLine("Completed");
        }                
    }
}
