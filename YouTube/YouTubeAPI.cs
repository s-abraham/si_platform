﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SI;

namespace YouTube
{
    public abstract class YouTubeAPI
    {
        protected static SInet _WebNet = null;

        protected static string GetYouTubeAuthCode()
        {
            var result = string.Empty;



            return result;
        }

        protected static dynamic GetRequestDynamic(string requestUrl)
        {
            dynamic shortenResult;
            var rawResult = string.Empty;


            if (_WebNet == null)
            {
                _WebNet = new SInet(false, 5, null, 60 * 1000);
            }

            rawResult = _WebNet.DownloadString(requestUrl);
            rawResult = rawResult.Replace("media$", "media_")
                                .Replace("$t", "val")
                                .Replace("yt$", "yt_")
                                .Replace("\"default\": {", "\"def\": {");

            if (rawResult != null)
            {
                shortenResult = Newtonsoft.Json.JsonConvert.DeserializeObject<dynamic>(rawResult);
            }
            else
            {
                rawResult = "";
                shortenResult = Newtonsoft.Json.JsonConvert.DeserializeObject<dynamic>(rawResult);
            }

            return shortenResult;
        }
    }
}
