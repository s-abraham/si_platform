﻿using JobLib;
using SI.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Processor.ReviewRunCleanup
{
    public class Processor : ProcessorBase<ReviewRunCleanupData>
    {
        public Processor(string[] args) : base(args) { }

        protected override void start()
        {
            JobDTO job = getNextJob();
            while (job != null)
            {
                setJobStatus(job.ID.Value, JobStatusEnum.Started);
                ReviewRunCleanupData data = getJobData(job.ID.Value);

                
            }
        }
    }
}
