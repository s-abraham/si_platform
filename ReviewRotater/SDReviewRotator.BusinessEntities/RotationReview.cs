﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SDReviewRotator.BusinessEntities
{
    public class RotationReview
    {
        public int ReviewDetailId { get; set; }

        public int LocationId { get; set; }

        public string LocationName { get; set; }

        public decimal Rating { get; set; }

        public string Review { get; set; }

        public DateTime ReviewDate { get; set; }

        public string ExtraInfo { get; set; }

        public int ReviewsSourceId { get; set; }
        
        public string SourceName { get; set; }

        public bool IsRead { get; set; }

        public bool IsShared { get; set; }

        public string ImageSource { get; set; }

        public string LogInUrl { get; set; }
    }
}
