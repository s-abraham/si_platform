﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SDReviewRotator.BusinessEntities
{
    public class ErrorLog
    {
        public ErrorLog()
        {
            DealerLocationId = 0;
            Message = string.Empty;
            StackSource = string.Empty;
            ClassName = string.Empty;
            MethodName = string.Empty;
            ExtraInfo = string.Empty;
            Section = string.Empty;
            ExceptionType = string.Empty;
        }
        public int DealerLocationId { get; set; }
        public string Message { get; set; }
        public string StackSource { get; set; }
        public string ClassName { get; set; }
        public string MethodName { get; set; }
        public string ExtraInfo { get; set; }
        public string Section { get; set; }
        public string ExceptionType { get; set; }

        public override string ToString()
        {
            var result = new StringBuilder();
            result.Append(" DateTime: " + DateTime.Now);
            result.Append(" Exc [" + ClassName);
            result.Append("." + MethodName + "]");
            result.Append(" \nSec:" + Section);
            result.Append(" \nDealerLocationId:" + DealerLocationId);
            result.Append(" \nType:" + ExceptionType);
            result.Append(" \nMessage:" + Message);

            return result.ToString();
        }
    }
}
