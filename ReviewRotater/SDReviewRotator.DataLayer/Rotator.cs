﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using SDReviewRotator.BusinessEntities;
using System.Data;
using Microsoft.Data.Extensions;
using System.Data.Common;
using System.Data.SqlClient;

namespace SDReviewRotator.DataLayer
{
    public class Rotator
    {
        private string EventLogMessage = string.Empty;
        private EventLog _logger;
        private ErrorLog errorLog = new ErrorLog();
        private SIEntities socialDealerEntities = new SIEntities();

        //private SocialDealerDB _socialDealerDB = new SocialDealerDB();

        public Rotator()
        {

        }

        public class ReviewRotationItem
        {
            public long ReviewDetailID { get; set; }
            public long LocationID { get; set; }
            public string LocationName { get; set; }
            public decimal Rating { get; set; }
            public string Review { get; set; }
            public DateTime ReviewDate { get; set; }
            public string ExtraInfo { get; set; }
            public long ReviewSourceID { get; set; }
            public bool IsRead { get; set; }
            public bool IsShared { get; set; }
            public string ImageSource { get; set; }
            public string LoginURL { get; set; }
            public string URL { get; set; }
            public string LocationURL { get; set; }
            public string ReviewerName { get; set; }
        }

        public List<ReviewRotationItem> GetReviewsList(string groupGuid, int dealerLocationId, int virtualGroupId = -1)
        {
            //var result = new List<spGetReviewRotations_Result>();

            try
            {
                Guid guid = Guid.Parse(groupGuid);
                EventLogMessage = string.Format("[ProcessReview.ProcessReviewsDetails()] Retrieving Worklist.");
                // WriteEventLogEntry(EventLogMessage, EventLogEntryType.Information);
                //var rawDataSetReviewDetail = socialDealerEntities.spGetReviewRotations(guid, dealerLocationId, virtualGroupId).ToList();
                //var rawDataSetReviewCSS = socialDealerEntities.spGetReviewRotationsCSS(guid, dealerLocationId).ToList();
                //result = socialDealerEntities.spGetReviewRotations(guid); //, dealerLocationId, virtualGroupId).ToList();

                using (SIEntities db = new SIEntities())
                {
                    db.Connection.Open();
                    using (DbCommand cmd = db.CreateStoreCommand("spGetReviewRotations", CommandType.StoredProcedure,
                            new SqlParameter("@GroupGUID", guid)
                    ))
                    {
                        using (DbDataReader reader = cmd.ExecuteReader())
                        {
                            List<ReviewRotationItem> items = db.Translate<ReviewRotationItem>(reader).ToList();
                            return items;
                        }
                    }

                }

            }
            catch (System.Data.EntityCommandExecutionException ex)
            {
                #region ErrorHandler
                errorLog.Message = ex.Message;
                errorLog.StackSource = ex.StackTrace;
                errorLog.ClassName = "Rotator";
                errorLog.MethodName = "GetReviews";
                errorLog.Section = "Main";
                errorLog.ExceptionType = "EntityCommandExecutionException";
                WriteEventLogEntry(errorLog.ToString(), EventLogEntryType.Error);
                Console.WriteLine(errorLog.ToString());
                #endregion
            }
            catch (Exception ex)
            {
                #region ErrorHandler
                errorLog.Message = ex.Message;
                errorLog.StackSource = ex.StackTrace;
                errorLog.ClassName = "Rotator";
                errorLog.MethodName = "GetReviews";
                errorLog.Section = "Main";
                errorLog.ExceptionType = "General";
                WriteEventLogEntry(errorLog.ToString(), EventLogEntryType.Error);
                Console.WriteLine(errorLog.ToString());
                #endregion
            }

            return null;
        }

        public class ReviewRotationCSS
        {
            public string CSSStyle { get; set; }
            public string HtmlTemplate { get; set; }
            public string ItemHtml { get; set; }
        }
        public ReviewRotationCSS GetReviewTemplate(string groupGuid)
        {
            using (SIEntities db = new SIEntities())
            {
                db.Connection.Open();
                using (DbCommand cmd = db.CreateStoreCommand(
                    string.Format("select CSSStyleSheetName CSSStyle, HtmlTemplate, ItemHtml from ReviewRotatorAccount with(nolock) where GroupGUID='{0}'", groupGuid), 
                    CommandType.Text))
                {
                    using (DbDataReader reader = cmd.ExecuteReader())
                    {
                        ReviewRotationCSS css = db.Translate<ReviewRotationCSS>(reader).FirstOrDefault();
                        return css;
                    }
                }

            }
        }
        //public spGetReviewRotationsCSS_Result GetReviewTemplate(string groupGuid, int dealerLocationId)
        //{
        //    var result = new spGetReviewRotationsCSS_Result();

        //    try
        //    {
        //        var guid = Guid.Parse(groupGuid);
        //        EventLogMessage = string.Format("[ProcessReview.ProcessReviewsDetails()] Retrieving Template & CSS.");
        //        // WriteEventLogEntry(EventLogMessage, EventLogEntryType.Information);
        //        result = socialDealerEntities.spGetReviewRotationsCSS(guid, dealerLocationId).FirstOrDefault();
        //    }
        //    catch (System.Data.EntityCommandExecutionException ex)
        //    {
        //        #region ErrorHandler
        //        errorLog.Message = ex.Message;
        //        errorLog.StackSource = ex.StackTrace;
        //        errorLog.ClassName = "Rotator";
        //        errorLog.MethodName = "GetReviewTemplate";
        //        errorLog.Section = "Main";
        //        errorLog.ExceptionType = "EntityCommandExecutionException";
        //        WriteEventLogEntry(errorLog.ToString(), EventLogEntryType.Error);
        //        Console.WriteLine(errorLog.ToString());
        //        #endregion
        //    }
        //    catch (Exception ex)
        //    {
        //        #region ErrorHandler
        //        errorLog.Message = ex.Message;
        //        errorLog.StackSource = ex.StackTrace;
        //        errorLog.ClassName = "Rotator";
        //        errorLog.MethodName = "GetReviewTemplate";
        //        errorLog.Section = "Main";
        //        errorLog.ExceptionType = "General";
        //        WriteEventLogEntry(errorLog.ToString(), EventLogEntryType.Error);
        //        Console.WriteLine(errorLog.ToString());
        //        #endregion
        //    }

        //    return result;
        //}

        private void WriteEventLogEntry(string message, EventLogEntryType type)
        {
            _logger.WriteEntry(message, type);
        }
    }
}
