﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using System.Text;
using SDReviewRotator.BusinessEntities;
using SDReviewRotator.DataLayer;

namespace SDReviewRotator.Web
{
    public partial class _default : System.Web.UI.Page
    {
        private Guid groupGuid = new Guid();
        private int dealerLocationId = 0;
        private Rotator rotator = new Rotator();

        protected void Page_Load(object sender, EventArgs e)
        {
            var encryption = new AESEncryption();
            //All Dealers for MileOne
            //var encryptedString = encryption.EncryptToString("CE6F870F-01AE-454B-82A2-4560703A04F9|-1");
            //Sample encrypted string = '038026148250224189143054170156211043046083014111168035041095250007196208127225232204089241173110244104142080143162137135254143089022111176014096';

            //All Dealers for MileOne - Heritage Honda Parkville
            //var encryptedString = encryption.EncryptToString("CE6F870F-01AE-454B-82A2-4560703A04F9|337");
            //Sample encrypted string = '038026148250224189143054170156211043046083014111168035041095250007196208127225232204089241173110031215235227244143193100041135188129188174045237';

            //All Dealers for MileOne - Audi Silver Spring
            //var encryptedString = encryption.EncryptToString("CE6F870F-01AE-454B-82A2-4560703A04F9|368");
            //Sample encrypted string = '038026148250224189143054170156211043046083014111168035041095250007196208127225232204089241173110192101229159143010029012089034083074047079046064';
            try
            {
                if (Request.QueryString["revval"] != null)
                {
                    var queryString = encryption.DecryptString(Request.QueryString["revval"].ToString());

                    if (queryString.Length > 0)
                    {
                        var queryParams = queryString.Split('|');

                        if (queryParams.Any())
                        {
                            groupGuid = Guid.Parse(queryParams[0]);
                            dealerLocationId = Convert.ToInt32(queryParams[1]);
                            //reviewsObject.Text = GetReviewsHTML(groupGuid, dealerLocationId);
                            setCss("~/templates/defaulttemplate.css", this.Page);
                        }
                        else
                        {

                        }
                    }
                }
            }
            catch (Exception ex)
            {
                
                throw;
            }
            
        }
        
        //private string GetReviewsHTML(Guid groupGuid, int dealerLocationId)
        //{
        //    var result = string.Empty;

        //    try
        //    {
                
        //        //Loop through reviewsList
        //        //For each review fill in tokens
                
        //        var reviewsList = rotator.GetReviewsList(groupGuid.ToString(), dealerLocationId);
        //        int itemflag = 0;
        //        var currentState = string.Empty;

        //        foreach (spGetReviewRotations_Result review in reviewsList)
        //        {
        //            var templateInfo = rotator.GetReviewTemplate(groupGuid.ToString(), dealerLocationId);

        //            var temp = string.Empty;
        //            var temp1 = string.Empty;
        //            var temp2 = string.Empty;
        //            var temp3 = string.Empty;
        //            var temp4 = string.Empty;

        //            itemflag++;

        //            temp = templateInfo.ItemHtml.Replace("$ReviewSource$", "<a href=\"" + review.URL.ToString() + "\"><img src=\"http://dealers.socialdealer.com/" + review.ImageSource.ToString() + "\"/></a>");
        //            temp = temp.Replace("$ReviewDate$", review.ReviewDate.ToString());
        //            temp = temp.Replace("$Reviewer$", review.ReviewerName.ToString());
        //            temp = temp.Replace("$Rating$", review.Rating.ToString());
        //            temp = temp.Replace("$ReviewSource$", review.ReviewsSourceID.ToString());
        //            temp = temp.Replace("$ReviewBody$", review.Review.ToString());
                    
        //            templateInfo.ItemHtml = temp;

        //            var htmlTemplate = File.ReadAllText(Server.MapPath(templateInfo.HtmlTemplate));


        //            if (itemflag == 1)
        //            {
        //                if (!currentState.ToString().Contains("%LEFTITEM%"))
        //                {
        //                    currentState = currentState.Replace("</table>", "  <tr><td>%LEFTITEM%</td></tr></table>");
        //                }

        //                var replaceLeft = htmlTemplate.Replace("%LEFTITEM%", templateInfo.ItemHtml);
        //                currentState = replaceLeft;
        //            }

        //            else if (!(itemflag % 2 == 0) && itemflag != 1)
        //            {

        //                if (!currentState.ToString().Contains("%LEFTITEM%"))
        //                {
        //                    currentState = currentState.Replace("</table>", "  <tr><td>%LEFTITEM%</td></tr></table>");
        //                }

        //                var replaceLeft = currentState.Replace("%LEFTITEM%", templateInfo.ItemHtml);
        //                currentState = replaceLeft;
        //            }

        //            else if (itemflag % 2 == 0)
        //            {
        //                if (!currentState.ToString().Contains("%RIGHTITEM%"))
        //                {
        //                    currentState = currentState.Replace("</tr></table>", "  <td>%RIGHTITEM%</td></tr></table>");
        //                }

        //                var replaceRight = currentState.Replace("%RIGHTITEM%", templateInfo.ItemHtml);
        //                currentState = replaceRight;
        //            }                   
                  
        //            result = currentState;
        //          }
                
        //        //htmlTemplate = htmlTemplate.Replace("%CSSNAME%", templateInfo.CSSSTyle);
               
        //    }
        //    catch (Exception ex)
        //    {
                
        //        throw;
        //    }
            
            
        //    return result;
        //}
        
        public static void setCss(string pathtoCSS, Page htmlPage)
        {
            Literal cssFile = new Literal() { Text = @"<link href=""" + htmlPage.ResolveUrl(pathtoCSS) + @""" type=""text/css"" rel=""stylesheet"" />" };
            htmlPage.Header.Controls.Add(cssFile);
        }
        
    }
}