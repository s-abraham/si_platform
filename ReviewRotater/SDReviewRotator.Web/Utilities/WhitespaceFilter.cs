using System;
using System.IO;
using System.Text;
using System.Text.RegularExpressions;

public class WhitespaceFilter : Stream
{

    // using Mads Kristensen httpModule
    // http://madskristensen.net/post/A-whitespace-removal-HTTP-module-for-ASPNET-20.aspx

    private Stream os;
    private static Regex regWhitespace = new Regex(@"^\s+", RegexOptions.Multiline | RegexOptions.Compiled);

    public WhitespaceFilter(Stream os)
    {
        this.os = os;
    }

    //methods that need to be overridden from stream
    public override bool CanRead
    {
        get { return true; }
    }

    public override bool CanSeek
    {
        get { return true; }
    }

    public override bool CanWrite
    {
        get { return true; }
    }

    public override void Flush()
    {
        os.Flush();
    }

    public override long Length
    {
        get { return 0; }
    }

    private long _position;
    public override long Position
    {
        get { return _position; }
        set { _position = value; }
    }

    public override int Read(byte[] buffer, int offset, int count)
    {
        return os.Read(buffer, offset, count);
    }

    public override long Seek(long offset, SeekOrigin origin)
    {
        return os.Seek(offset, origin);
    }

    public override void SetLength(long value)
    {
        os.SetLength(value);
    }

    public override void Close()
    {
        os.Close();
    }

    public override void Write(byte[] buffer, int offset, int count)
    {
        string html = System.Text.Encoding.Default.GetString(buffer);

        //remove whitespace
        html = regWhitespace.Replace(html, string.Empty);
        while (html.Contains("  "))
            html = html.Replace("  ", " ");

        //remove redundant line breaks
        html = html.Replace(">\r\n<", "><");
        html = html.Replace("\r\n<p>\r\n", "<p>");
        html = html.Replace(" <p>\r\n", " <p>");
        html = html.Replace("\r\n</p>", " </p>");

        html = html.Replace("<div>\r\n", "<div>");
        html = html.Replace("\r\n<div", "<div");
        html = html.Replace("\r\n</div>", "</div>");
        html = html.Replace("</div>\r\n", "</div>");

        byte[] outdata = System.Text.Encoding.Default.GetBytes(html);

        os.Write(outdata, 0, outdata.GetLength(0));
    }

}
