﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SDReviewRotator.Web.Models
{
    public class Review
    {
        /// <summary>
        /// The url of the review source image
        /// </summary>
        public string SourceImage { get; set; }

        /// <summary>
        /// The id of the rating source
        /// </summary>
        public int RatingSourceID { get; set; }

        /// <summary>
        /// The URL to the review itself
        /// </summary>
        public string URL { get; set; }

        /// <summary>
        /// The date of the review
        /// </summary>
        public string Date { get; set; }

        /// <summary>
        /// The name of the reviewer
        /// </summary>
        public string Reviewer { get; set; }

        /// <summary>
        /// The numeric rating
        /// </summary>
        public double Rating { get; set; }

        /// <summary>
        /// The body of the review
        /// </summary>
        public string Body { get; set; }

        /// <summary>
        /// If not null or empty, this will be rendered as-is to the client
        /// </summary>
        public string HTML { get; set; }

        /// <summary>
        /// The name of the dealer location
        /// </summary>
        public string LocationName { get; set; }

        /// <summary>
        /// The URL of the dealer location's website
        /// </summary>
        public string LocationURL { get; set; }
    }
}