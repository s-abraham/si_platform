﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SDReviewRotator.Web.Models
{
    public class RatingImage
    {
        public int SourceID { get; set; }
        public double Rating { get; set; }
        public int MaxImageHeight { get; set; }

        public string ImageURL
        {
            get
            {
                string path = "http://dealers.socialdealer.com/";
                switch (SourceID)
                {
                    case 1:
                        {
                            path = path + "Images/reviews/YahooGoogle/";
                            break;
                        }
                    case 2:
                        {
                            path = path + "Images/reviews/DealerRater/";
                            break;
                        }
                    case 3:
                        {
                            path = path + "Images/reviews/YahooGoogle/";
                            break;
                        }
                    case 4:
                        {
                            path = path + "Images/reviews/Yelp/";
                            break;
                        }
                    case 5:
                        {
                            path = path + "Images/reviews/YelowPages/";
                            break;
                        }
                    case 6:
                        {
                            path = path + "Images/reviews/YelowPages/";
                            break;
                        }
                    case 7:
                        {
                            path = path + "Images/reviews/Edmunds/";
                            break;
                        }
                    case 9:
                        {
                            path = path + "Images/reviews/CitySearch/";
                            break;
                        }
                    case 8:
                        {
                            path = path + "Images/reviews/CarsDotCom/";
                            break;
                        }
                    case 10:
                        {
                            path = path + "Images/reviews/JudysBook/";
                            break;
                        }
                    default:
                        {
                            path = path + "Images/reviews/YahooGoogle/";
                            break;
                        }
                }

                if (Rating >= 5.0) return path + "star5.png";
                if (Rating >= 4.5) return path + "star45.png";
                if (Rating >= 4.0) return path + "star4.png";
                if (Rating >= 3.5) return path + "star35.png";
                if (Rating >= 3.0) return path + "star3.png";
                if (Rating >= 2.5) return path + "star25.png";
                if (Rating >= 2.0) return path + "star2.png";
                if (Rating >= 1.5) return path + "star15.png";
                if (Rating >= 1.0) return path + "star1.png";
                if (Rating >= 0.5) return path + "star05.png";
                return path + "star0.png";

            }
        }
    }
}