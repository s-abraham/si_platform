﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.IO;

namespace SDReviewRotator.Web.Models
{
    public class ReviewRotator
    {
        #region fields

        /// <summary>
        /// The list of reviews to render
        /// </summary>
        public List<Review> Reviews { get; set; }

        /// <summary>
        /// The name of the dealer location
        /// </summary>
        public string LocationName { get; set; }


        #endregion

        #region static url resolving helpers

        private static string baseViewName(ViewContext context)
        {
            string viewURL = ((RazorView)context.View).ViewPath;
            string viewFileName = viewURL.Substring(viewURL.LastIndexOf('/'));
            string viewFileNameWithoutExtension = Path.GetFileNameWithoutExtension(viewFileName);
            return viewFileNameWithoutExtension;            
        }
        public static string StyleURL(ViewContext context)
        {
            return string.Format("../../style/{0}", baseViewName(context));
        }
        public static string TemplateViewName(ViewContext context)
        {
            return string.Format("~/Views/ReviewRotator/Templates/{0}_Template.cshtml", baseViewName(context));
        }

        #endregion

    }
}