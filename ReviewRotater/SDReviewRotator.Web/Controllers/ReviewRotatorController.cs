﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SDReviewRotator.Web.Models;
using System.IO;
using SDReviewRotator.BusinessEntities;
using SDReviewRotator.DataLayer;

namespace SDReviewRotator.Web.Controllers
{
    public class ReviewRotatorController : EnhancedController
    {
        /// <summary>
        /// Returns a review frame for use in customer sites...
        /// </summary>
        /// <param name="type">Part of the REST URL to indicate the mode in which to render</param>
        /// <param name="revVal">The encrypted ret val.</param>
        [CompressResponse]
        [UrlRoute(Order = 2, Name = "Review_Frame", Path = "reviews/frame/{type}/{revVal}")]
        [UrlRoute(Order = 2, Name = "Review_Frame_Neg", Path = "negreviews/frame/{type}/{revVal}")]
        public ActionResult Index(string type, string revVal, string mode)
        {
            ReviewRotator model = new ReviewRotator();
            bool isCustom = type.Trim().Equals("custom", StringComparison.InvariantCultureIgnoreCase);
            AESEncryption encryption = AESEncryption.Encrypter;

            try
            {
                var queryString = encryption.DecryptString(revVal);

                if (queryString.Length > 0)
                {
                    var queryParams = queryString.Split('|');
                    if (queryParams.Length > 1)
                    {
                        Guid groupGuid = Guid.Parse(queryParams[0]);
                        int dealerLocationId = Convert.ToInt32(queryParams[1]);
                        int virtualGroupId = Convert.ToInt32(queryParams[2]);

                        Rotator rotator = new Rotator();

                        List<SDReviewRotator.DataLayer.Rotator.ReviewRotationItem> reviewsList = rotator.GetReviewsList(groupGuid.ToString(), dealerLocationId, virtualGroupId);

                        //convert the reviews list from the data layer to mvc model objects
                        if (isCustom)
                        {
                            Rotator.ReviewRotationCSS templateInfo = rotator.GetReviewTemplate(groupGuid.ToString());

                            model.Reviews = (
                                from review in reviewsList
                                orderby review.ReviewDate descending
                                select new Review
                                {
                                    Body = review.Review,
                                    Date = review.ReviewDate.ToString("MM/dd/yyyy"),
                                    Rating = Convert.ToDouble(review.Rating),
                                    Reviewer = review.ReviewerName,
                                    SourceImage = String.Format("{0}", review.ImageSource),
                                    URL = review.URL,
                                    HTML = GetCustomTemplateHTML(templateInfo, review),
                                    RatingSourceID = (int)review.ReviewSourceID,
                                    LocationName = review.LocationName,
                                    LocationURL = review.LocationURL
                                }
                            ).ToList();
                        }
                        else
                        {
                            model.Reviews = (
                                from review in reviewsList
                                orderby review.ReviewDate descending
                                select new Review
                                {
                                    Body = review.Review,
                                    Date = review.ReviewDate.ToString("MM/dd/yyyy"),
                                    Rating = Convert.ToDouble(review.Rating),
                                    Reviewer = review.ReviewerName,
                                    SourceImage = String.Format("{0}", review.ImageSource),
                                    URL = review.URL,
                                    HTML = null,
                                    RatingSourceID = (int)review.ReviewSourceID,
                                    LocationName = review.LocationName,
                                    LocationURL = review.LocationURL
                                }
                            ).ToList();
                        }
                    }
                    else
                    {
                        model.Reviews = new List<Review>();
                    }

                    foreach (Review review in model.Reviews)
                    {
                        if (!string.IsNullOrEmpty(review.LocationURL))
                        {
                            if (!review.LocationURL.StartsWith("http://", StringComparison.InvariantCultureIgnoreCase))
                            {
                                review.LocationURL = string.Format("http://{0}", review.LocationURL);
                            }
                        }
                    }

                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex);
                return null;
            }

            return View(String.Format("Templates/{0}", type), model);

        }

        /// <summary>
        /// Returns minified css rendered from the partial view with the name: Templates/[styleName]_Style
        /// </summary>
        /// <param name="styleName">The name of the style to fetch</param>        
        [CompressResponse]
        [UrlRoute(Order = 1, Name = "Review_Style", Path = "reviews/style/{styleName}")]
        [UrlRoute(Order = 1, Name = "Review_Style_Neg", Path = "negreviews/style/{styleName}")]
        public String Style(string styleName)
        {
            try
            {
                Response.ContentType = "text/css";

                string viewName = string.Format("Templates/{0}_Style", styleName);
                string styleText = RenderPartialViewToString(viewName, new ReviewStyle());
                styleText = styleText
                    .Replace("<style>", "")
                    .Replace("</style>", "")
                ;

                using (StringReader reader = new StringReader(styleText))
                {
                    CssMinifier minifier = new CssMinifier();
                    return minifier.Minify(reader);
                }

            }
            catch (Exception)
            {
                return "";
            }
        }

        private string GetCustomTemplateHTML(Rotator.ReviewRotationCSS templateInfo, Rotator.ReviewRotationItem review)
        {
            string temp = templateInfo.ItemHtml.Replace("$ReviewSource$", "<a href=\"" + review.URL.ToString() + "\"><img src=\"" + review.ImageSource.ToString() + "\"/></a>");
            temp = temp.Replace("$ReviewDate$", review.ReviewDate.ToString());
            temp = temp.Replace("$Reviewer$", review.ReviewerName.ToString());
            temp = temp.Replace("$Rating$", review.Rating.ToString());
            temp = temp.Replace("$ReviewSource$", review.ReviewSourceID.ToString());
            temp = temp.Replace("$ReviewBody$", review.Review.ToString());

            return temp;
        }
    }
}
