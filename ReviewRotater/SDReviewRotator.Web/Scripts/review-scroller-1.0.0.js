﻿var sitems = [];
var scrolling = true;
var curtop = 0;

$(document).ready(function () {
    var scroller = $('#myinnerscroll');

    if (scroller) {

        $('#myscrollframe')
            .css('overflow', 'hidden')
            .mouseenter(function () {
                scrolling = false;
            })
            .mouseleave(function () {
                scrolling = true;
            });
        ;

        //get the items within and calculate their sizes
        $.each(scroller.children('.rritem'), function () {
            var itm = $(this);
            sitems.push({
                item: itm,
                height: itm.outerHeight()
            });
        });

        setInterval('scrollMe();', 100);
    }
});

function scrollMe() {
    if (scrolling) {
        var iscroll = $('#myinnerscroll');
        curtop -= 1;

        var firstItem = sitems[0];

        if (Math.abs(curtop) >= firstItem.height) {
            curtop = 0;
            var lastItem = sitems[sitems.length - 1];

            firstItem.item.insertAfter(lastItem.item);
            sitems.splice(0, 1);
            sitems[sitems.length] = firstItem;
        }

        iscroll.css('top', curtop + 'px');
    }
}