﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using SI.DTO;

namespace TestApp
{
    class Template
    {
        static void Main(string[] args)
        {
            #region New Social Alert
            {
                string body = ReadAllText(@"C:\Dev2\SI\SI_PLATFORM\SI.DTO\Template\NewSocialAlertHermanAdvertising\Body.html");
                string CommentRepeatingBlock = ReadAllText(@"C:\Dev2\SI\SI_PLATFORM\SI.DTO\Template\NewSocialAlertHermanAdvertising\CommentRepeatingBlock.html");
                string PostRepeatingBlock = ReadAllText(@"C:\Dev2\SI\SI_PLATFORM\SI.DTO\Template\NewSocialAlertHermanAdvertising\PostRepeatingBlock.html");


                NewSocialAlertTemplateDTO TemplateDTO = new NewSocialAlertTemplateDTO();
                TemplateDTO.body = body;
                TemplateDTO.CommentRepeatingBlock = CommentRepeatingBlock;
                TemplateDTO.PostRepeatingBlock = PostRepeatingBlock;

                string serializeValue = JsonConvert.SerializeObject(TemplateDTO);

                NewSocialAlertTemplateDTO TemplateDTONew = JsonConvert.DeserializeObject<NewSocialAlertTemplateDTO>(serializeValue);
            }
            #endregion

            #region New Review Alert
            {
                string body = ReadAllText(@"C:\Dev2\SI\SI_PLATFORM\SI.DTO\Template\NewReviewSD\body.html");
                string TemplateRepeatingBlock = ReadAllText(@"C:\Dev2\SI\SI_PLATFORM\SI.DTO\Template\NewReviewSD\TemplateRepeatingBlock.html");

                NewReviewAlertTemplateDTO TemplateDTO = new NewReviewAlertTemplateDTO();
                TemplateDTO.body = body;
                TemplateDTO.TemplateRepeatingBlock = TemplateRepeatingBlock;

                string serializeValue = JsonConvert.SerializeObject(TemplateDTO);

                NewReviewAlertTemplateDTO TemplateDTONew = JsonConvert.DeserializeObject<NewReviewAlertTemplateDTO>(serializeValue);
            }
            #endregion

            #region SyndicationSignup
            {
                string body = ReadAllText(@"C:\Dev2\SI\SI_PLATFORM\SI.DTO\Template\SyndicationSignup\CATA\Body.html");

                SyndicationSignupDTO TemplateDTO = new SyndicationSignupDTO();
                TemplateDTO.body = body;

                string serializeValue = JsonConvert.SerializeObject(TemplateDTO);

                SyndicationSignupDTO TemplateDTONew = JsonConvert.DeserializeObject<SyndicationSignupDTO>(serializeValue);
            }
            #endregion

            #region Platform HTML Report
            {
                //string body = ReadAllText(@"C:\Dev2\SI\SI_PLATFORM\SI.DTO\Template\PlatformSocialHTMLReport\PrintHTML.html");

                //PlatformSocialHTMLReport TemplateDTO = new PlatformSocialHTMLReport();
                //TemplateDTO.Body = body;

                //string serializeValue = JsonConvert.SerializeObject(TemplateDTO);

                //PlatformSocialHTMLReport TemplateDTONew = JsonConvert.DeserializeObject<PlatformSocialHTMLReport>(serializeValue);
            }
            #endregion

            #region Platform ReputationAndSocialReport
            {
                string body = ReadAllText(@"C:\Dev2\SI\SI_PLATFORM\SI.DTO\Template\PlatformReputationAndSocialReport\PrintHTML.html");
                string TemplateRepeatingBlock = ReadAllText(@"C:\Dev2\SI\SI_PLATFORM\SI.DTO\Template\PlatformReputationAndSocialReport\TemplateRepeatingBlock.html");
                string TemplateRepeatingBlockSummary = ReadAllText(@"C:\Dev2\SI\SI_PLATFORM\SI.DTO\Template\PlatformReputationAndSocialReport\TemplateRepeatingBlockSummary.html");

                ReputationAndSocialReportDTO TemplateDTO = new ReputationAndSocialReportDTO();
                TemplateDTO.Body = body;
                TemplateDTO.TemplateRepeatingBlock = TemplateRepeatingBlock;
                TemplateDTO.TemplateRepeatingBlockSummary = TemplateRepeatingBlockSummary;

                string serializeValue = JsonConvert.SerializeObject(TemplateDTO);

                ReputationAndSocialReportDTO TemplateDTONew = JsonConvert.DeserializeObject<ReputationAndSocialReportDTO>(serializeValue);
            }
            #endregion

            #region ReputationAndSocialReport

            //string body = ReadAllText(@"C:\Dev2\SI\SI_PLATFORM\SI.DTO\Template\ReputationAndSocialReport\Body.html");
            //string TemplateRepeatingBlock = ReadAllText(@"C:\Dev2\SI\SI_PLATFORM\SI.DTO\Template\ReputationAndSocialReport\TemplateRepeatingBlock.html");
            //string TemplateRepeatingBlockSummary = ReadAllText(@"C:\Dev2\SI\SI_PLATFORM\SI.DTO\Template\ReputationAndSocialReport\TemplateRepeatingBlockSummary.html");

            //ReputationAndSocialReportDTO TemplateDTO = new ReputationAndSocialReportDTO();
            //TemplateDTO.Body = body;
            //TemplateDTO.TemplateRepeatingBlock = TemplateRepeatingBlock;
            //TemplateDTO.TemplateRepeatingBlockSummary = TemplateRepeatingBlockSummary;

            //string serializeValue = JsonConvert.SerializeObject(TemplateDTO);

            //ReputationAndSocialReportDTO TemplateDTONew = JsonConvert.DeserializeObject<ReputationAndSocialReportDTO>(serializeValue);

            #endregion

            #region PendingApprovalNonSyndicationPost
            {
                //string body = ReadAllText(@"C:\Dev2\SI\SI_PLATFORM\SI.DTO\Template\PendingApprovalNonSyndicationPost\Body.html");
                //string TemplateRepeatingBlock = ReadAllText(@"C:\Dev2\SI\SI_PLATFORM\SI.DTO\Template\PendingApprovalNonSyndicationPost\TemplateRepeatingBlock.html");
                //string Image = ReadAllText(@"C:\Dev2\SI\SI_PLATFORM\SI.DTO\Template\PendingApprovalNonSyndicationPost\Image.html");
                //string Link = ReadAllText(@"C:\Dev2\SI\SI_PLATFORM\SI.DTO\Template\PendingApprovalNonSyndicationPost\Link.html");

                //NonSyndicationPendingPostApprovalDTO TemplateDTO = new NonSyndicationPendingPostApprovalDTO();
                //TemplateDTO.Body = body;
                //TemplateDTO.TemplateRepeatingBlock = TemplateRepeatingBlock;
                //TemplateDTO.Image = Image;
                //TemplateDTO.Link = Link;               

                //string serializeValue = JsonConvert.SerializeObject(TemplateDTO);

                //NonSyndicationPendingPostApprovalDTO TemplateDTONew = JsonConvert.DeserializeObject<NonSyndicationPendingPostApprovalDTO>(serializeValue);
            }

            #endregion

            #region PendingApproval Email
            {
                string body = ReadAllText(@"C:\Dev2\SI\SI_Platform\SI.DTO\Template\PendingApproval\CATA.html");


                PendingApprovalTemplateDTO TemplateDTO = new PendingApprovalTemplateDTO();
                TemplateDTO.body = body;

                string serializeValue = JsonConvert.SerializeObject(TemplateDTO);

                PendingApprovalTemplateDTO TemplateDTONew = JsonConvert.DeserializeObject<PendingApprovalTemplateDTO>(serializeValue);
            }
            #endregion

            #region SocialTokenValidationReport Email
            {
                //string body = ReadAllText(@"C:\Dev2\SI\SI_PLATFORM\SI.DTO\Template\SocialTokenValidationReport\Body.html");
                //string TemplateRepeatingBlock = ReadAllText(@"C:\Dev2\SI\SI_PLATFORM\SI.DTO\Template\SocialTokenValidationReport\TemplateRepeatingBlock.html");

                //SocialTokenValidationReportTemplateDTO TemplateDTO = new SocialTokenValidationReportTemplateDTO();
                //TemplateDTO.body = body;
                //TemplateDTO.TemplateRepeatingBlock = TemplateRepeatingBlock;

                //string serializeValue = JsonConvert.SerializeObject(TemplateDTO);

                //SocialTokenValidationReportTemplateDTO TemplateDTONew = JsonConvert.DeserializeObject<SocialTokenValidationReportTemplateDTO>(serializeValue);
            }
            #endregion

            #region Enterprise Reputation SummaryReport Email
            {
                //string body = ReadAllText(@"C:\Development\AutoStartups\SocialIntegration\SI.DTO\Template\EnterpriseReputationSummaryReportMarano\Daily.html");
                //string TemplateRepeatingBlock = ReadAllText(@"C:\Development\AutoStartups\SocialIntegration\SI.DTO\Template\EnterpriseReputationSummaryReportMarano\DailyTemplateRepeatingBlock.html");

                //EnterpriseReputationSummaryReportDTO TemplateDTO = new EnterpriseReputationSummaryReportDTO();
                //TemplateDTO.body = body;
                //TemplateDTO.TemplateRepeatingBlock = TemplateRepeatingBlock;

                //string serializeValue = JsonConvert.SerializeObject(TemplateDTO);

                //EnterpriseReputationSummaryReportDTO TemplateDTONew = JsonConvert.DeserializeObject<EnterpriseReputationSummaryReportDTO>(serializeValue);
            }
            #endregion

            #region Enrollment Welcome Email
            {
                string body = ReadAllText(@"C:\Dev2\SI\SI_PLATFORM\SI.DTO\Template\ClientFacingWelcomeEmail\CATA.html");

                WelcomeEmailTemplateDTO TemplateDTO = new WelcomeEmailTemplateDTO();
                TemplateDTO.Body = body;

                string serializeValue = JsonConvert.SerializeObject(TemplateDTO);

                WelcomeEmailTemplateDTO TemplateDTONew = JsonConvert.DeserializeObject<WelcomeEmailTemplateDTO>(serializeValue);

            }
            #endregion

            #region Review Email
            {
                //string body = ReadAllText(@"C:\Development\AutoStartups\SocialIntegration\SI.DTO\Template\ReviewShareEmail\ShareReview.html");

                //ReviewShareEmailTemplateDTO TemplateDTO = new ReviewShareEmailTemplateDTO();
                //TemplateDTO.Body = body;

                //string serializeValue = JsonConvert.SerializeObject(TemplateDTO);

                //ReviewShareEmailTemplateDTO TemplateDTONew = JsonConvert.DeserializeObject<ReviewShareEmailTemplateDTO>(serializeValue);
            }

            #endregion

            #region ClientFaceingTokenValidation
            {
                string body = ReadAllText(@"C:\Dev2\SI\SI_PLATFORM\SI.DTO\Template\ClientFacingTokenValidationReport\Body.html");
                string body48Hours = ReadAllText(@"C:\Dev2\SI\SI_PLATFORM\SI.DTO\Template\ClientFacingTokenValidationReport\Body48Hours.html");


                ClientFaceingTokenValidationDTO TemplateDTO = new ClientFaceingTokenValidationDTO();
                TemplateDTO.Body = body;
                TemplateDTO.Subject = "Let’s Get Reconnected!";
                TemplateDTO.Body48Hours = body48Hours;
                TemplateDTO.Subject48Hours = "Don’t Forget to Reconnect!";

                string serializeValue = JsonConvert.SerializeObject(TemplateDTO);

                ClientFaceingTokenValidationDTO TemplateDTONew = JsonConvert.DeserializeObject<ClientFaceingTokenValidationDTO>(serializeValue);
            }
            #endregion

            #region AutoApprovedNewContentNotification
             {
                 string body = ReadAllText(@"C:\Dev2\SI\SI_PLATFORM\SI.DTO\Template\AutoApprovedNewContent\CARFAX\Body.html");
                 string Accounts = ReadAllText(@"C:\Dev2\SI\SI_PLATFORM\SI.DTO\Template\AutoApprovedNewContent\MT20.Default.Accounts.html");
                 string LinkSection = ReadAllText(@"C:\Dev2\SI\SI_PLATFORM\SI.DTO\Template\AutoApprovedNewContent\MT20.Default.LinkSection.html");


                AutoApprovedNewContentNotificationDTO TemplateDTO = new AutoApprovedNewContentNotificationDTO();
                TemplateDTO.Body = body;
                TemplateDTO.LinkSection = LinkSection;
                TemplateDTO.Accounts = Accounts;

                string serializeValue = JsonConvert.SerializeObject(TemplateDTO);

                AutoApprovedNewContentNotificationDTO TemplateDTONew = JsonConvert.DeserializeObject<AutoApprovedNewContentNotificationDTO>(serializeValue);
            }
            

            #endregion


        }

        public static string ReadAllText(string path)
        {
            if (path == null)
            {
                throw new ArgumentNullException("path");
            }

            return InternalReadAllText(path, Encoding.UTF8);
        }

        private static string InternalReadAllText(string path, Encoding encoding)
        {
            string result;
            using (StreamReader streamReader = new StreamReader(path, encoding))
            {
                result = streamReader.ReadToEnd();
            }
            return result;
        }
    }
}
