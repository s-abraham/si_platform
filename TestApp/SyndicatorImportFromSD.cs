﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SI.BLL;
using SI.DTO;

namespace TestApp
{
    class SyndicatorImportFromSD
    {
        static void Main(string[] args)
        {
            //ProviderFactory.SyndicatorImport.ImportCARFAX(new Guid("EF2139A2-2FE7-4C3A-AE03-F9AFA9CD3A64")); // CARFAX
            
            //ProviderFactory.SyndicatorImport.ImportCARFAX(new Guid("33721D21-335D-43A0-B92B-29DD73287A0F")); // Ally

            //ProviderFactory.SyndicatorImport.importRides();


            //ProviderFactory.SyndicatorImport.importAllyFromExcel();

            ProviderFactory.SyndicatorImport.importAllyUsersFromExcel_SPN_824();
        }
    }
}
