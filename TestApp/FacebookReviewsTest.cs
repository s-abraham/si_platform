﻿using SI;
using SI.BLL;
using SI.DTO;
using SI.WEB;
using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Diagnostics;
using System.Net;

namespace TestApp
{
    class FacebookReviewsTest
    {
        static void Main(string[] args)
        {
            WebClient client = new WebClient();

            //Hard Bounces

            Console.WriteLine("");
            var baseQueryURL = "https://graph.facebook.com/255547016773/ratings?fields=open_graph_story,created_time,reviewer,has_rating,has_review&version=2.0";
            var accessTokenURL = "&access_token=CAAGfD25ViIcBAGvALQINSVnHOp5TkuEhEGxeTLsueyTOpSBZCJb6xS4ECmpYQDRqT8NcIt2iTw0oHDgMH1x4vwHRBkFnmezcf2bPH1yt66b0zCoi2FRD3CWwLNgwTvEGhorr7Vi2ZBas7uAx9n0ajZAWMZAfVxcZBMO83dmNljOZBcf83qlgri";
            var reviewQuery = baseQueryURL + accessTokenURL;
            var recordCount = 0;
            var resultJSON = client.DownloadString(reviewQuery);

            if (!string.IsNullOrEmpty(resultJSON))
            {
                dynamic resultsJSON = Newtonsoft.Json.JsonConvert.DeserializeObject<dynamic>(resultJSON);
                foreach (var item in resultsJSON)
                {
                    Console.WriteLine(item);
                    var id = Convert.ToString(item.open_graph_story.id);
                    var createdDate = Convert.ToString(item.open_graph_story.publish_time);
                    recordCount++;
                    Console.WriteLine("#{0} - ID {1}, Created: {2}", recordCount, id, createdDate);

                }
            }
            Console.WriteLine("{0} records found", recordCount);
            Console.WriteLine("*****************************************************************************");
            recordCount = 0;
	
        }
    }
}
