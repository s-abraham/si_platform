﻿using SI;
using SI.BLL;
using SI.DTO;
using SI.WEB;
using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Diagnostics;
using System.Reflection;

namespace TestApp
{
    class Program
    {
        private static ILoggingProvider _log = ProviderFactory.Logging;
        static void Main(string[] args)
        {
            ITimezoneProvider tz = ProviderFactory.TimeZones;

            //timezone will always be for system user
            TimeZoneInfo userTimeZone = tz.GetTimezoneForUserID(0);

            ProviderFactory.Notifications.SendAutoApprovalNotification(64811); //62198

            PostApprovalResultsDTO apprs = ProviderFactory.Social.GetPostApprovals(
                UserInfoDTO.System,
                new PostApprovalHistoryRequestDTO()
                {
                    AccountIDs = new List<long>() { 220745, 199551, 200346, 202222, 203158, 204488, 205119, 205120, 205913, 205914, 205915, 206803, 206975, 207835, 208414, 208494, 208713, 209039, 209040, 209055, 209407, 209472, 209473, 209879, 210216, 210217, 210218, 210220, 210221, 210222, 210223, 210225, 210226, 210227, 210229, 210230, 210231, 210232, 210233, 210235, 210236, 210238, 210239, 210240, 210241, 210242, 210243, 210244, 210245, 210246, 210248, 210249, 210250, 210251, 210252, 210253, 210254, 210255, 210256, 210257, 210258, 210259, 210260, 210261, 210262, 210263, 210264, 210265, 210269, 210270, 210271, 210272, 210273, 210274, 210275, 210276, 210278, 210280, 210281, 210282, 210283, 210284, 210285, 210286, 210287, 210288, 210289, 210290, 210291, 210292, 210293, 210294, 210295, 210296, 210297, 210298, 210299, 210300, 210301, 210302, 210303, 210304, 210306, 210307, 210308, 210309, 210407, 210621, 210673, 210823, 211421, 212399, 212497, 212695, 212954, 214060, 214736, 214737, 214738, 215911, 216158, 216226, 216712, 217193, 220736, 220806, 220910 },
                    StartDate = new DateTime(2014, 1, 1),
                    EndDate = new DateTime(2014, 10, 1),
                    SocialNetworkID = 0,
                    SortBy = 7,
                    SortAscending = false,
                    StartIndex = 1,
                    EndIndex = 25
                }
           );
            apprs = apprs;

            //GetResourceTextFile
            //string body = Util.GetResourceTextFile("MT20.Default.Body.html", Assembly.GetAssembly(typeof(AutoApprovedNewContentNotificationDTO)));
            //string accounts = Util.GetResourceTextFile("MT20.Default.Accounts.html", Assembly.GetAssembly(typeof(AutoApprovedNewContentNotificationDTO)));
            //string linksection = Util.GetResourceTextFile("MT20.Default.LinkSection.html", Assembly.GetAssembly(typeof(AutoApprovedNewContentNotificationDTO)));
            //AutoApprovedNewContentNotificationDTO template = new AutoApprovedNewContentNotificationDTO() { Body = body, Accounts = accounts, LinkSection = linksection };
            //string templatejson = Newtonsoft.Json.JsonConvert.SerializeObject(template);
            //templatejson = templatejson;

            //ScheduleSpecDTO spec = new ScheduleSpecDTO() { EarliestHour = 22, MinimumTimeBetweenSchedules = new TimeSpan(23, 0, 0) };
            //string sdata = spec.ToString();
            //sdata = sdata;

            //schedule: ||22|-1|82800
            //data: {"JobType":1600,"Action":1,"RemainingRetries":0}

            //ProviderFactory.Security.GetUserActivityByUserID(42022, 50);

            //UserInfoDTO userInfo = new UserInfoDTO(42022, null);
            //ProviderFactory.Social.GetPostApprovals(userInfo, "220245|220246", 0, 0, DateTime.Now.AddDays(-7), DateTime.Now, "all");

            // ProviderFactory.Social.GetPostApprovals("220245|220246", 0, 0, DateTime.Now.AddDays(-7), DateTime.Now, "all");




            //ProviderFactory.Report.SendTokenValidationByUser();
            //string a = ProviderFactory.Report.GetReportLogByID(790);
            //a = a;
            //ProviderFactory.Report.ProcessTokenValidationReport();

            //ProviderFactory.Social.InitPostApprovals(15416, null);
            //ProviderFactory.Report.GetReportTemplateAndEmailReport(42022);


            //ProviderFactory.Report.ProcessDailyEnterpriseReputationSummaryReport();


            //BasicUserInfoDTO info = ProviderFactory.Security.GetBasicUserInfoByID(42042);
            //info = info;

            //UserInfoDTO userInfo = new UserInfoDTO(42042,null);
            //List<RecentNotificationItemDTO> items = ProviderFactory.Notifications.GetMostRecentEmailNotificationsForUser(userInfo, 20);
            //string body = ProviderFactory.Notifications.GetBodyTextForNotificationTargetID(items[0].NotificationTargetID);

            //items = items;
            //List<UserNotificationTypeInfoDTO> items;
            //items = ProviderFactory.Notifications.GetAvailableNotificationsForUser(userInfo);
            //items = ProviderFactory.Notifications.GetAvailableNotificationsForUser(userInfo);
            //items = items;

            //UserInfoDTO userInfo = new UserInfoDTO(42022,null);
            //userInfo.EffectiveUser.CurrentContextAccountID = 197922;

            //List<ReviewSourceEnum> reviewSources = new List<ReviewSourceEnum>();
            //reviewSources.Add((ReviewSourceEnum)100);
            //reviewSources.Add((ReviewSourceEnum)200);
            //reviewSources.Add((ReviewSourceEnum)300);
            //reviewSources.Add((ReviewSourceEnum)400);
            //reviewSources.Add((ReviewSourceEnum)500);
            //reviewSources.Add((ReviewSourceEnum)600);
            //reviewSources.Add((ReviewSourceEnum)700);
            //reviewSources.Add((ReviewSourceEnum)800);
            //reviewSources.Add((ReviewSourceEnum)900);
            //reviewSources.Add((ReviewSourceEnum)1000);
            //reviewSources.Add((ReviewSourceEnum)1100);
            //reviewSources.Add((ReviewSourceEnum)1200);
            //reviewSources.Add((ReviewSourceEnum)1300);
            //reviewSources.Add((ReviewSourceEnum)1400);
            //reviewSources.Add((ReviewSourceEnum)1500);
            //reviewSources.Add((ReviewSourceEnum)1600);
            //reviewSources.Add((ReviewSourceEnum)1700);
            //reviewSources.Add((ReviewSourceEnum)1800);
            //reviewSources.Add((ReviewSourceEnum)1900);
            //ProviderFactory.Reviews.QueueReviewDownloaderJobs(userInfo, true, 3, reviewSources, 212696, JobStatusEnum.Queued, 100);

            //List<DashboardSocialLocationDTO>  result1 = ProviderFactory.Social.GetDashBoardSocialLocationData(userInfo, true, false, false);

            //var a = result1;
            //ProviderFactory.Report.ProcessDailyEnterpriseReputationSummaryReport();
            //FacebookDownloaderData facebookDownloaderData = new FacebookDownloaderData();

            //facebookDownloaderData.PostID = 61072;
            //facebookDownloaderData.FacebookEnum = FacebookProcessorActionEnum.FacebookPostStatisticsDownloader;

            //JobDTO job1 = new JobDTO()
            //{
            //    JobDataObject = facebookDownloaderData,
            //    JobType = facebookDownloaderData.JobType,
            //    DateScheduled = new SIDateTime(DateTime.UtcNow, TimeZoneInfo.Utc),                
            //    JobStatus = JobStatusEnum.Parked,
            //    Priority = 200
            //};

            //SaveEntityDTO<JobDTO> save = ProviderFactory.Jobs.Save(new SaveEntityDTO<JobDTO>(job1, UserInfoDTO.System));
            //var result = save;

            //ProviderFactory.Report.ProcessDailyEnterpriseReputationSummaryReport();

            //TokenValidatorDownloaderData TokenDownloaderData = new TokenValidatorDownloaderData()
            //{
            //    CredentialID = 810,
            //    SocialNetwork = SocialNetworkEnum.Facebook
            //};

            //JobDTO job = new JobDTO()
            //{
            //    DateScheduled = new SIDateTime(DateTime.UtcNow, TimeZoneInfo.Utc),
            //    JobStatus = JobStatusEnum.Queued,
            //    JobDataObject = TokenDownloaderData,
            //    JobType = JobTypeEnum.TokenValidator,
            //    Priority = 100,
            //    ContinueChainIfFailed = false
            //};

            //SaveEntityDTO<JobDTO> save = ProviderFactory.Jobs.Save(new SaveEntityDTO<JobDTO>(job, UserInfoDTO.System));
            //var result = save;


            //ProviderFactory.Social.DeletePost(UserInfoDTO.System, 13713);


            //ProviderFactory.Social.QueuePost(13603, 220737);

            //PostSearchRequestDTO req = new PostSearchRequestDTO(UserInfoDTO.System);
            //req.PostedByUserID = 42263;

            //PostSearchResultsDTO res = ProviderFactory.Social.PostSearch(UserInfoDTO.System, req);
            //res = res;

            //ScheduleSpecDTO dto = new ScheduleSpecDTO();
            //dto.MinimumTimeBetweenSchedules = new TimeSpan(23, 0, 0);
            //dto.EarliestHour = 13;
            //dto.LatestHour = 14;
            ////dto.DaysOfWeek = new List<DayOfWeek>() { DayOfWeek.Monday, DayOfWeek.Tuesday };
            ////dto.DaysOfMonth = new List<int>() { 1, 15 };


            //string scheduleString = dto.ToString();


            //dto = ScheduleSpecDTO.FromString(scheduleString);

            //DateTime? lastRun = null;
            //DateTime nextRun = dto.GetNextRunDate(lastRun);

            //nextRun = nextRun;

            //string template = ProviderFactory.Report.GetReputationSnapshotReportHTML(220246, DateTime.Now);

            //string template = ProviderFactory.Report.GetSocialSnapshotReportHTML(220246, DateTime.Now);

            //string template = ProviderFactory.Report.GetReputationAndSocialReportHTML(42022);

            //Start Lexus Enrollment Link Generation
            //var bulkdAccountUserList = new List<string>();
            //bulkdAccountUserList.Add("210228|62564");
            //bulkdAccountUserList.Add("100002|1001");
            //bulkdAccountUserList.Add("100003|1001");
            //bulkdAccountUserList.Add("100004|1001");
            //bulkdAccountUserList.Add("100005|1005");
            //var resultLinks = GenerateEnrollmentBulkTokens(bulkdAccountUserList, 1, 15, 9);
            //Debug.WriteLine(resultLinks);
            //End Lexus Enrollment Link Generation

            //string template = ProviderFactory.Report.GetSocialSnapshotReportHTML(220246, DateTime.Now);
            //ProviderFactory.Report.ProcessReputationAndSocialReport();

            //ProviderFactory.Jobs.SetJobStatus(525347, JobStatusEnum.Parked);
            //ProviderFactory.Jobs.SetJobStatus(525347, JobStatusEnum.PickedUp);
            //ProviderFactory.Jobs.SetJobStatus(525347, JobStatusEnum.Assigned);
            //ProviderFactory.Jobs.SetJobStatus(525347, JobStatusEnum.Started);
            //ProviderFactory.Jobs.SetJobStatus(525347, JobStatusEnum.Completed);
            //ProviderFactory.Jobs.SetJobStatus(525347, JobStatusEnum.Failed);            
            //ProviderFactory.Jobs.SetJobStatus(525347, JobStatusEnum.Parked);

            //importRides();
            //SyndicationUnsubscribeDTO tokenRequest = new SyndicationUnsubscribeDTO()
            //{                
            //    UserID = 42022, //UserID of user that the link is for                
            //    NotificationMessageTypeID = 7,
            //    NotificationTemplateID = 4,
            //    RemainingUses = null,
            //    TimeTillExpiration = null
            //};

            //AccessTokenBaseDTO tok = ProviderFactory.Security.CreateAccessToken(tokenRequest);            
            //SyndicationUnsubscribeDTO accessTokenBaseDto = (SyndicationUnsubscribeDTO)ProviderFactory.Security.GetAccessToken(tok.Token, true);
            //Debug.WriteLine(tok.Token);

            //UserInfoDTO userInfo = UserInfoDTO.System;
            //GetEntityDTO<RevisePostDTO> ent = ProviderFactory.Social.GetPostForRevision(userInfo, 11685, 221295);
            //ent = ent;

            //string localIP = Util.GetLocalIPAddress();
            //localIP = localIP;

            //SyndicationSelfRegistrationDTO tokenRequest = new SyndicationSelfRegistrationDTO()
            //{
            //    UserID = 42022, //UserID of user that the link is for
            //    PackageIDs = new long[] { 29, 30, 51 }, //Syndicatee PackageID -- 51 CATA
            //    DefaultPackageIDs = new long[] { 51 }, //  CATA
            //    PreferedParentAccountID = 197922,
            //    VerticalMarketID = 1,
            //    ResellerID = 25,
            //    SyndicatorID = 8,
            //    RemainingUses = null,
            //    TimeTillExpiration = null
            //};

            //AccessTokenBaseDTO tok = ProviderFactory.Security.CreateAccessToken(tokenRequest);
            //Debug.WriteLine(tok.Token);

            //SyndicationInviteAccessTokenDTO tokenRequest = new SyndicationInviteAccessTokenDTO()
            //{
            //    AccountID = 222670, //Account ot sign up
            //    UserID = 44170, //UserID of user that the link is for
            //    PackageID = 22, //Syndicatee PackageID
            //    ResellerID = 22,
            //    SyndicatorID = 4,
            //    RemainingUses = null,
            //    TimeTillExpiration = null
            //};

            //AccessTokenBaseDTO tok = ProviderFactory.Security.CreateAccessToken(tokenRequest);
            //Debug.WriteLine(tok.Token);
            //Debug.WriteLine(tok.Token);


            //ProviderFactory.Security.SetUserActiveState(UserInfoDTO.System, 42309, false);

            //ProviderFactory.Report.ProcessTokenValidationReport();

            //ProviderFactory.Report.ProcessDailyEnterpriseReputationSummaryReport();

            //var userSanju = new UserInfoDTO(41986, null);

            //ProviderFactory.Security.SetUserActiveState(UserInfoDTO.System, 42309, false);
            //ProviderFactory.Security.SetUserActiveState(UserInfoDTO.System, 42332, false);
            //ProviderFactory.Security.SetUserActiveState(UserInfoDTO.System, 42329, false);
            //Debug.WriteLine("test");


            //PostApprovalAccessTokenDTO token = new PostApprovalAccessTokenDTO()
            //{
            //    PostID = 11641,
            //    PostTargetID = 16391,
            //    UserID = 42022
            //};
            //var tok = ProviderFactory.Security.CreateAccessToken(token);
            //Debug.WriteLine(tok.Token);


            //ProviderFactory.Notifications.sendPendingApprovalNotifications();

            //ProviderFactory.Notifications.GenerateInitialTargetHashes();

            //RSSSearchRequestDTO req = new RSSSearchRequestDTO()
            //{
            //    SortBy=1,
            //    SortAscending=false,
            //    StartIndex=1,
            //    EndIndex=100,

            //    SearchText="hyundai -toyota -\"check and check\""
            //};
            //RSSSearchResultDTO res = ProviderFactory.RSS.SearchRSSItems(UserInfoDTO.System, req);
            //res = res;

            //ProviderFactory.Social.QueuePost(10714, 220536);

            //UserSearchRequestDTO req = new UserSearchRequestDTO()
            //{
            //    NotMemberOfAccountID = 220598,
            //    SearchString = "lange",
            //    UserInfo = new UserInfoDTO(41986, null),
            //    StartIndex = 1,
            //    EndIndex = 100,
            //    SortBy = 1,
            //    SortAscending = false
            //};

            //UserSearchResultDTO result = ProviderFactory.Security.SearchUsers(req);
            //result = result;

            //SearchStringParser parser = new SearchStringParser(" i wanted to -\"see if\" this thing -works");
            //parser = parser;

            //LegacyMigrationData data = new LegacyMigrationData()
            //{
            //    AccountID = 220246,
            //    DealerLocationID = 2,
            //    MinQueryDate = new DateTime (2014,01,01),
            //    MaxQueryDate = new DateTime (2014,01,31),
            //    Type = LegacyMigrationData.LegacyMigrationType.SocialFacebookPost                
            //};

            //JobDTO job = new JobDTO()
            //{
            //    DateScheduled = new SIDateTime(DateTime.UtcNow, TimeZoneInfo.Utc),
            //    JobStatus = JobStatusEnum.Parked,
            //    JobDataObject = data,
            //    JobType = JobTypeEnum.LegacyMigration,
            //    Priority = 100,
            //    ContinueChainIfFailed = false
            //};

            //SaveEntityDTO<JobDTO> save = ProviderFactory.Jobs.Save(new SaveEntityDTO<JobDTO>(job, UserInfoDTO.System));
            //var result = save;

            //List<Process> processes = Process.GetProcessesByName("vlc").ToList();
            //string name1 = GetProcessInstanceName(processes[0].Id);
            //string name2 = GetProcessInstanceName(processes[1].Id);

            //name1 = name1;


            //SyndicationInviteAccessTokenDTO tokenRequest = new SyndicationInviteAccessTokenDTO()
            //{
            //    AccountID = 221323,
            //    UserID = 42022, //UserID of user that the link is for
            //    PackageID = 16, //Syndicatee PackageID
            //    ResellerID = 13, 
            //    SyndicatorID = 2,
            //    RemainingUses = null,
            //    TimeTillExpiration = null
            //};

            //AccessTokenBaseDTO tok = ProviderFactory.Security.CreateAccessToken(tokenRequest);
            //Debug.WriteLine(tok.Token);

            //tokenRequest = new SyndicationInviteAccessTokenDTO()
            //{
            //    AccountID = 220809,
            //    UserID = 42021, //UserID of user that the link is for
            //    PackageID = 16, //Syndicatee PackageID
            //    ResellerID = 13,
            //    SyndicatorID = 2,
            //    RemainingUses = null,
            //    TimeTillExpiration = null
            //};

            //tok = ProviderFactory.Security.CreateAccessToken(tokenRequest);
            //Debug.WriteLine(tok.Token);

            //tokenRequest = new SyndicationInviteAccessTokenDTO()
            //{
            //    AccountID = 220808,
            //    UserID = 42028, //UserID of user that the link is for
            //    PackageID = 16, //Syndicatee PackageID
            //    ResellerID = 13,
            //    SyndicatorID = 2,
            //    RemainingUses = null,
            //    TimeTillExpiration = null
            //};

            //tok = ProviderFactory.Security.CreateAccessToken(tokenRequest);
            //Debug.WriteLine(tok.Token);

            //tokenRequest = new SyndicationInviteAccessTokenDTO()
            //{
            //    AccountID = 220804,
            //    UserID = 42003, //UserID of user that the link is for
            //    PackageID = 16, //Syndicatee PackageID
            //    ResellerID = 13,
            //    SyndicatorID = 2,
            //    RemainingUses = null,
            //    TimeTillExpiration = null
            //};

            //tok = ProviderFactory.Security.CreateAccessToken(tokenRequest);
            //Debug.WriteLine(tok.Token);

            //tokenRequest = new SyndicationInviteAccessTokenDTO()
            //{
            //    AccountID = 220245,
            //    UserID = 42055, //UserID of user that the link is for
            //    PackageID = 16, //Syndicatee PackageID
            //    ResellerID = 13,
            //    SyndicatorID = 2,
            //    RemainingUses = null,
            //    TimeTillExpiration = null
            //};

            //tok = ProviderFactory.Security.CreateAccessToken(tokenRequest);
            //Debug.WriteLine(tok.Token);

            //SyndicationInviteAccessTokenDTO tokenRequest = new SyndicationInviteAccessTokenDTO()
            //{
            //    AccountID = 222710,
            //    UserID = 62572, //UserID of user that the link is for
            //    PackageID = 30, //Syndicatee PackageID
            //    ResellerID = 25,
            //    SyndicatorID = 8,
            //    RemainingUses = null,
            //    TimeTillExpiration = null
            //};

            //AccessTokenBaseDTO tok = ProviderFactory.Security.CreateAccessToken(tokenRequest);
            //Debug.WriteLine(tok.Token);


            //SyndicationSelfRegistrationDTO tokenRequest = new SyndicationSelfRegistrationDTO()
            //{
            //	UserID = 42022, //UserID of user that the link is for

            //	ResellerID = 13,
            //	SyndicatorID = 1,
            //	RemainingUses = null,
            //	TimeTillExpiration = null
            //};

            //AccessTokenBaseDTO tok = ProviderFactory.Security.CreateAccessToken(tokenRequest);
            //Debug.WriteLine(tok.Token);sss

            //AccessTokenBaseDTO tok = ProviderFactory.Security.GetAccessToken(new Guid("B1CAB6BA-95ED-499B-A8CD-5328CEA2700F"), false);
            //Debug.WriteLine(tok.Token);


            //SocialProductConfigAccessTokenDTO tokenRequest = new SocialProductConfigAccessTokenDTO()
            //{
            //	UserID = 42045, //UserID of user that the link is for
            //	AccountID = 197922,

            //	RemainingUses = 100,
            //	TimeTillExpiration = new TimeSpan(1,0,0,0)
            //};

            //AccessTokenBaseDTO tok = ProviderFactory.Security.CreateAccessToken(tokenRequest);
            //Debug.WriteLine(tok.Token);			

        }

        private static List<string> GenerateEnrollmentBulkTokens(List<string> AccountUsers, int syndicatorID, int packageID, int resellerID)
        {
            var result = new List<string>();

            var baseURL = "http://platform.socialintegration.com/LandingPages/Syndication/Signup/{0}";
            //http://platform.socialintegration.com/LandingPages/Syndication/Signup/E245031E-B521-46FF-BA8E-F5614FAA17BE
            var tokenRequest = new SyndicationInviteAccessTokenDTO();

            var accountID = 0;
            var userID = 0;

            try
            {
                foreach (var accountUser in AccountUsers)
                {
                    var accountUserSplit = accountUser.Split('|');

                    if (!string.IsNullOrEmpty(accountUserSplit[0]) && !string.IsNullOrEmpty(accountUserSplit[1]))
                    {
                        tokenRequest = new SyndicationInviteAccessTokenDTO()
                        {
                            AccountID = Convert.ToInt64(accountUserSplit[0]),
                            UserID = Convert.ToInt64(accountUserSplit[1]),
                            PackageID = packageID,
                            ResellerID = resellerID,
                            SyndicatorID = syndicatorID,
                            RemainingUses = null,
                            TimeTillExpiration = null
                        };

                        var tok = ProviderFactory.Security.CreateAccessToken(tokenRequest);
                        result.Add(string.Format(baseURL, tok.Token));
                    }
                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine("Error encountered: {0}", ex.Message);
            }

            return result;
        }

        private static string GetProcessInstanceName(int pid)
        {
            PerformanceCounterCategory cat = new PerformanceCounterCategory("Process");
            string[] instances = cat.GetInstanceNames();

            foreach (string instance in instances)
            {

                using (PerformanceCounter cnt = new PerformanceCounter("Process",
                     "ID Process", instance, true))
                {
                    int val = (int)cnt.RawValue;
                    if (val == pid)
                    {
                        return instance;
                    }
                }
            }

            return "";
        }

        private static void queueNewReviewNotificationTest()
        {

        }

        private static void moveAccounts(long oldParentAccountID, long newParentAccountID)
        {
            UserInfoDTO uiSystem = new UserInfoDTO(1, null);
            AccountSearchResultDTO dto = ProviderFactory.Security.SearchAccounts(new SI.DTO.AccountSearchRequestDTO()
            {
                UserInfo = uiSystem,
                SearchString = "",
                OnlyIfHasChildAccounts = false,
                ParentAccountID = oldParentAccountID,
                SortBy = 1,
                StartIndex = 1,
                EndIndex = 100000
            });

            int itemsMoved = 0;
            int counter = 0;
            foreach (AccountListItemDTO item in dto.Items)
            {
                ProviderFactory.Security.ChangeParentAccount(uiSystem, item.ID, newParentAccountID, true);
                itemsMoved++;
                counter++;
                if (counter >= 100)
                {
                    counter = 0;
                    Console.WriteLine("moved {0} accounts", itemsMoved);
                }
            }
            Console.WriteLine("moved {0} accounts DONE", itemsMoved);
        }

        #region rides.ca account import

        private static void importRides()
        {
            long ridesParentAccountID = 220862;

            Dictionary<int, SDLocation> locations = readRidesLocations(@"C:\SocialDealer Docs\Rides Freemium accounts.csv");

            UserInfoDTO ui = new UserInfoDTO(1, null); //system
            //now import the locations
            foreach (SDLocation item in locations.Values)
            {
                try
                {
                    Console.WriteLine("importing location {0}", item.LocationName);
                    //SDLocation group = groups[(int)item.GroupID];
                    long siParentAccountID = ridesParentAccountID;

                    GetEntityDTO<AccountDTO> dto2 = ProviderFactory.Security.GetAccountByNameForImport(ui, item.LocationName);
                    if (dto2 != null)
                    {
                        Debug.WriteLine("Account Exists Name: " + dto2.Entity.Name + " , ID : " + dto2.Entity.ID);
                    }
                    else
                    {
                        Debug.WriteLine("New Account: " + item.LocationName);
                    }


                    //long id = importRidesAccount(siParentAccountID, item);
                    //item.SocialIntegrationID = id;
                    //Debug.WriteLine(item.SocialIntegrationID + "," + item.LocationName);
                    //Console.WriteLine(item.SocialIntegrationID + "," + item.LocationName);
                }
                catch (Exception ex)
                {
                    Debug.WriteLine("0," + item.LocationName);
                    Debug.WriteLine("0," + ex.InnerException);
                    Console.WriteLine("0," + item.LocationName);
                }
            }
            Console.ReadLine();

            //locations = locations;

        }

        private static Dictionary<int, SDLocation> readRidesLocations(string filename)
        {
            Dictionary<int, SDLocation> locations = new Dictionary<int, SDLocation>();

            using (StreamReader sReader = new StreamReader(filename))
            {
                using (LumenWorks.Framework.IO.Csv.CsvReader csv = new LumenWorks.Framework.IO.Csv.CsvReader(sReader, true))
                {
                    string[] head = csv.GetFieldHeaders();
                    Dictionary<string, int> headers = new Dictionary<string, int>();
                    for (int i = 0; i < head.Length; i++)
                    {
                        headers.Add(head[i].Trim().ToLower(), i);
                    }

                    int locationID = 98;
                    while (csv.ReadNextRecord())
                    {
                        locationID++;

                        SDLocation loc = null;
                        if (!locations.TryGetValue(locationID, out loc))
                        {
                            loc = new SDLocation(false);

                            loc.SocialDealerID = locationID;
                            loc.GroupID = 0;

                            loc.Address = csv[headers["address"]];
                            loc.City = csv[headers["city"]];
                            loc.StateAbbreviation = csv[headers["state"]];
                            loc.ZipCode = csv[headers["zip"]];
                            loc.LocationName = csv[headers["dealership"]];

                            locations.Add(locationID, loc);
                        }
                        /*
                        if (!string.IsNullOrWhiteSpace(csv[headers["google"]]))
                        {
                            SDReviewURL url = new SDReviewURL()
                            {
                                ReviewSource = SDReviewSource.Google,
                                URL = csv[headers["google"]],
                                RequestURL = null,
                                LookupID = null
                            };
                            loc.ReviewURLs.Add(url);
                        }
                        if (!string.IsNullOrWhiteSpace(csv[headers["yellowpages.ca"]]))
                        {
                            SDReviewURL url = new SDReviewURL()
                            {
                                ReviewSource = SDReviewSource.YellowPages,
                                URL = csv[headers["google"]],
                                RequestURL = null,
                                LookupID = null
                            };
                            loc.ReviewURLs.Add(url);
                        }
                        if (!string.IsNullOrWhiteSpace(csv[headers["yelp"]]))
                        {
                            SDReviewURL url = new SDReviewURL()
                            {
                                ReviewSource = SDReviewSource.Yelp,
                                URL = csv[headers["yelp"]],
                                RequestURL = null,
                                LookupID = null
                            };
                            loc.ReviewURLs.Add(url);
                        }
                        if (!string.IsNullOrWhiteSpace(csv[headers["dealerrater.ca"]]))
                        {
                            SDReviewURL url = new SDReviewURL()
                            {
                                ReviewSource = SDReviewSource.DealerRater,
                                URL = csv[headers["dealerrater.ca"]],
                                RequestURL = null,
                                LookupID = null
                            };
                            loc.ReviewURLs.Add(url);
                        }
                        */

                    }

                }
            }

            return locations;

        }

        private static long importRidesAccount(long parentAccountID, SDLocation loc)
        {
            long? accountID = null;

            UserInfoDTO ui = new UserInfoDTO(1, null); //system

            string originID = string.Format("Rides.{0}", loc.SocialDealerID);

            AccountSearchRequestDTO sreq = new AccountSearchRequestDTO()
            {
                UserInfo = ui,
                SearchString = "",
                OnlyIfHasChildAccounts = false,
                OriginSystemIdentifier = originID,
                SortBy = 1,
                StartIndex = 1,
                EndIndex = 100000
            };

            AccountDTO account = null;

            string zip = loc.ZipCode.Replace(" ", "").Trim();

            account = new AccountDTO()
            {
                Name = loc.LocationName,
                DisplayName = loc.LocationName,
                OriginSystemIdentifier = originID,
                CountryID = 41,
                CityName = loc.City,
                PostalCode = zip,
                ParentAccountID = parentAccountID,
                StateAbbreviation = loc.StateAbbreviation,
                StreetAddress1 = loc.Address,
                TimezoneID = 11, //central
                Type = loc.IsGroup ? AccountTypeEnum.Grouping : AccountTypeEnum.Company
            };

            SaveEntityDTO<AccountDTO> result = ProviderFactory.Security.Save(new SaveEntityDTO<AccountDTO>(account, ui));
            if (result.Problems.Count() > 0)
            {
                // try to change the name to include (location)
                //account.Name = string.Format("{0} (location)", account.Name);
                //result = ProviderFactory.Security.Save(new SaveEntityDTO<AccountDTO>(account, ui));
                //if (result.Problems.Count() > 0)
                //{
                //    return 0;
                //}
                //accountID = result.Entity.ID;
                foreach (var Problem in result.Problems)
                {
                    Debug.WriteLine("Problem : " + Problem);
                }

            }
            else
            {
                //account = result.Entity;
                accountID = result.Entity.ID;
                Debug.WriteLine("Created New : " + loc.LocationName);
            }

            if (accountID.HasValue && accountID.Value > 0 && !loc.IsGroup && loc.ReviewURLs != null)
            {
                foreach (SDReviewURL url in loc.ReviewURLs)
                {
                    ReviewUrlDTO rUrl = ProviderFactory.Reviews.GetReviewUrl(UserInfoDTO.System, accountID.Value, (ReviewSourceEnum)(((int)(url.ReviewSource)) * 100));
                    if (rUrl == null)
                    {
                        rUrl = new ReviewUrlDTO()
                        {
                            AccountID = accountID.Value,
                            ReviewSource = (ReviewSourceEnum)(((int)(url.ReviewSource)) * 100),
                            ExternalID = url.LookupID,
                            isValid = true,
                            HtmlURL = (string.IsNullOrWhiteSpace(url.RequestURL) ? url.URL : url.RequestURL)
                        };
                        if (rUrl.ReviewSource == ReviewSourceEnum.Edmunds)
                        {
                            rUrl.HtmlURL = url.URL;
                            rUrl.ApiURL = url.RequestURL;
                        }

                        ProviderFactory.Reviews.SaveReviewUrl(UserInfoDTO.System, rUrl);
                    }
                }
            }

            return accountID.Value;
        }

        #endregion

        #region legacy socialdealer import routines

        private static void importSDURLs()
        {
            string importFilename = "c:\\_LOAD\\locations.csv";

            Dictionary<int, SDLocation> locations = new Dictionary<int, SDLocation>();

            using (StreamReader sReader = new StreamReader(importFilename))
            {
                using (LumenWorks.Framework.IO.Csv.CsvReader csv = new LumenWorks.Framework.IO.Csv.CsvReader(sReader, true))
                {
                    string[] head = csv.GetFieldHeaders();
                    Dictionary<string, int> headers = new Dictionary<string, int>();
                    for (int i = 0; i < head.Length; i++)
                    {
                        headers.Add(head[i].Trim().ToLower(), i);
                    }

                    while (csv.ReadNextRecord())
                    {
                        int locationID = Convert.ToInt32(csv[headers["locationid"]]);

                        SDLocation loc = null;
                        if (!locations.TryGetValue(locationID, out loc))
                        {
                            loc = new SDLocation(false);
                            loc.Address = csv[headers["address"]];
                            loc.City = csv[headers["city"]];
                            loc.StateAbbreviation = csv[headers["state"]];
                            loc.ZipCode = csv[headers["zipcode"]];
                            loc.LocationName = csv[headers["locationname"]];

                            locations.Add(locationID, loc);
                        }

                        SDReviewURL url = new SDReviewURL()
                        {
                            ReviewSource = (SDReviewSource)Convert.ToInt32(csv[headers["reviewssourceid"]]),
                            URL = csv[headers["url"]],
                            RequestURL = csv[headers["requesturl"]],
                            LookupID = csv[headers["lookupid"]]
                        };

                        if (url.URL.Trim().ToLower() == "null")
                            url.URL = null;
                        if (url.RequestURL.Trim().ToLower() == "null")
                            url.RequestURL = null;

                        loc.ReviewURLs.Add(url);

                        if (url.ReviewSource == SDReviewSource.Edmunds && !string.IsNullOrWhiteSpace(url.LookupID))
                        {
                            long edmundsID;
                            if (long.TryParse(url.LookupID, out edmundsID))
                            {
                                loc.EdmundsDealershipID = edmundsID;
                            }
                        }

                    }

                }
            }

            locations = locations;

            int itemNo = 0;
            foreach (int locationID in locations.Keys)
            {
                itemNo++;
                SDLocation loc = locations[locationID];

                Console.WriteLine("({0}/{1}) {2}", itemNo, locations.Count(), loc.LocationName);

                long? accountID = null;

                // if we have an edmunds id, find the account with it...
                if (loc.EdmundsDealershipID > 0)
                {
                    accountID = ProviderFactory.Security.GetAccountIDByEdmundsDealershipID(loc.EdmundsDealershipID);
                }

                // todo: other methods of matching here...

                // did we find an account?
                if (accountID.HasValue && accountID.Value > 0)
                {
                    foreach (SDReviewURL url in loc.ReviewURLs)
                    {
                        ReviewSourceEnum source = ReviewSourceEnum.All;
                        switch (url.ReviewSource)
                        {
                            case SDReviewSource.Google:
                                source = ReviewSourceEnum.Google;
                                break;
                            case SDReviewSource.DealerRater:
                                source = ReviewSourceEnum.DealerRater;
                                break;
                            case SDReviewSource.Yahoo:
                                source = ReviewSourceEnum.Yahoo;
                                break;
                            case SDReviewSource.Yelp:
                                source = ReviewSourceEnum.Yelp;
                                break;
                            case SDReviewSource.CarDealer:
                                source = ReviewSourceEnum.CarDealer;
                                break;
                            case SDReviewSource.YellowPages:
                                source = ReviewSourceEnum.YellowPages;
                                break;
                            case SDReviewSource.Edmunds:
                                source = ReviewSourceEnum.Edmunds;
                                break;
                            case SDReviewSource.CarsDotCom:
                                source = ReviewSourceEnum.CarsDotCom;
                                break;
                            case SDReviewSource.CitySearch:
                                source = ReviewSourceEnum.CitySearch;
                                break;
                            case SDReviewSource.JudysBook:
                                source = ReviewSourceEnum.JudysBook;
                                break;
                            default:
                                break;
                        }

                        if (source != ReviewSourceEnum.All)
                        {
                            ReviewUrlDTO rUrl = ProviderFactory.Reviews.GetReviewUrl(UserInfoDTO.System, accountID.Value, source);
                            if (rUrl == null)
                            {
                                rUrl = new ReviewUrlDTO()
                                {
                                    AccountID = accountID.Value,
                                    ReviewSource = source,
                                    ExternalID = url.LookupID,
                                    isValid = true,
                                    HtmlURL = (string.IsNullOrWhiteSpace(url.RequestURL) ? url.URL : url.RequestURL)
                                };
                                if (source == ReviewSourceEnum.Edmunds)
                                {
                                    rUrl.HtmlURL = url.URL;
                                    rUrl.ApiURL = url.RequestURL;
                                }

                                ProviderFactory.Reviews.SaveReviewUrl(UserInfoDTO.System, rUrl);
                            }
                        }
                    }
                }
            }
        }

        private static void importLegacySD()
        {
            long socialDealerLegacyAccountID = 220172;

            Dictionary<int, SDLocation> locations = readSDLocations("c:\\_load\\locations.csv");
            Dictionary<int, SDLocation> groups = readSDGroups("c:\\_load\\groups.csv");


            //first inport the groups

            foreach (SDLocation item in groups.Values)
            {
                Console.WriteLine("importing group {0}", item.LocationName);
                if (!string.IsNullOrWhiteSpace(item.StateAbbreviation))
                {
                    long id = importSDAccount(socialDealerLegacyAccountID, item);
                    item.SocialIntegrationID = id;
                }
            }

            //now import the locations
            foreach (SDLocation item in locations.Values)
            {
                Console.WriteLine("importing location {0}", item.LocationName);
                SDLocation group = groups[(int)item.GroupID];
                long siParentAccountID = group.SocialIntegrationID;
                if (siParentAccountID <= 1)
                {
                    siParentAccountID = siParentAccountID;
                }
                else
                {
                    long id = importSDAccount(siParentAccountID, item);
                    item.SocialIntegrationID = id;
                }

            }

            locations = locations;

        }

        private static long importSDAccount(long parentAccountID, SDLocation loc)
        {
            long? accountID = null;

            UserInfoDTO ui = new UserInfoDTO(1, null); //system

            string originID = "";
            if (loc.IsGroup)
                originID = string.Format("SD.Group.{0}", loc.SocialDealerID);
            else
                originID = string.Format("SD.Loc.{0}", loc.SocialDealerID);

            AccountSearchRequestDTO sreq = new AccountSearchRequestDTO()
            {
                UserInfo = ui,
                SearchString = "",
                OnlyIfHasChildAccounts = false,
                OriginSystemIdentifier = originID,
                SortBy = 1,
                StartIndex = 1,
                EndIndex = 100000
            };

            AccountDTO account = null;
            AccountSearchResultDTO sres = ProviderFactory.Security.SearchAccounts(sreq);
            if (sres.Items.Count() > 0)
            {
                //account = ProviderFactory.Security.GetAccountByID(ui, sres.Items[0].ID).Entity;
                accountID = sres.Items[0].ID;
            }
            else
            {
                string zip = loc.ZipCode.Replace(" ", "").Trim();

                account = new AccountDTO()
                {
                    Name = loc.LocationName,
                    OriginSystemIdentifier = originID,
                    CountryID = 1,
                    CityName = loc.City,
                    PostalCode = zip,
                    ParentAccountID = parentAccountID,
                    StateAbbreviation = loc.StateAbbreviation,
                    StreetAddress1 = loc.Address,
                    TimezoneID = 11, //central
                    Type = loc.IsGroup ? AccountTypeEnum.Grouping : AccountTypeEnum.Company
                };

                SaveEntityDTO<AccountDTO> result = ProviderFactory.Security.Save(new SaveEntityDTO<AccountDTO>(account, ui));
                if (result.Problems.Count() > 0)
                {
                    // try to change the name to include (location)
                    account.Name = string.Format("{0} (location)", account.Name);
                    result = ProviderFactory.Security.Save(new SaveEntityDTO<AccountDTO>(account, ui));
                    if (result.Problems.Count() > 0)
                    {
                        return 0;
                    }
                    accountID = result.Entity.ID;
                }
                else
                {
                    //account = result.Entity;
                    accountID = result.Entity.ID;
                }
            }

            if (accountID.HasValue && accountID.Value > 0 && !loc.IsGroup && loc.ReviewURLs != null)
            {
                foreach (SDReviewURL url in loc.ReviewURLs)
                {
                    ReviewUrlDTO rUrl = ProviderFactory.Reviews.GetReviewUrl(UserInfoDTO.System, accountID.Value, (ReviewSourceEnum)(((int)(url.ReviewSource)) * 100));
                    if (rUrl == null)
                    {
                        rUrl = new ReviewUrlDTO()
                        {
                            AccountID = accountID.Value,
                            ReviewSource = (ReviewSourceEnum)(((int)(url.ReviewSource)) * 100),
                            ExternalID = url.LookupID,
                            isValid = true,
                            HtmlURL = (string.IsNullOrWhiteSpace(url.RequestURL) ? url.URL : url.RequestURL)
                        };
                        if (rUrl.ReviewSource == ReviewSourceEnum.Edmunds)
                        {
                            rUrl.HtmlURL = url.URL;
                            rUrl.ApiURL = url.RequestURL;
                        }

                        ProviderFactory.Reviews.SaveReviewUrl(UserInfoDTO.System, rUrl);
                    }
                }
            }

            return accountID.Value;
        }

        private static Dictionary<int, SDLocation> readSDGroups(string filename)
        {
            Dictionary<int, SDLocation> locations = new Dictionary<int, SDLocation>();

            using (StreamReader sReader = new StreamReader(filename))
            {
                using (LumenWorks.Framework.IO.Csv.CsvReader csv = new LumenWorks.Framework.IO.Csv.CsvReader(sReader, true))
                {
                    string[] head = csv.GetFieldHeaders();
                    Dictionary<string, int> headers = new Dictionary<string, int>();
                    for (int i = 0; i < head.Length; i++)
                    {
                        headers.Add(head[i].Trim().ToLower(), i);
                    }

                    while (csv.ReadNextRecord())
                    {
                        int groupid = Convert.ToInt32(csv[headers["groupid"]]);

                        SDLocation loc = null;
                        if (!locations.TryGetValue(groupid, out loc))
                        {
                            loc = new SDLocation(true);

                            loc.SocialDealerID = groupid;

                            loc.Address = csv[headers["address"]];
                            loc.City = csv[headers["city"]];
                            loc.StateAbbreviation = csv[headers["state"]];
                            loc.ZipCode = csv[headers["zipcode"]];
                            loc.LocationName = csv[headers["groupname"]];

                            locations.Add(groupid, loc);
                        }


                    }

                }
            }

            return locations;

        }
        private static Dictionary<int, SDLocation> readSDLocations(string filename)
        {
            Dictionary<int, SDLocation> locations = new Dictionary<int, SDLocation>();

            using (StreamReader sReader = new StreamReader(filename))
            {
                using (LumenWorks.Framework.IO.Csv.CsvReader csv = new LumenWorks.Framework.IO.Csv.CsvReader(sReader, true))
                {
                    string[] head = csv.GetFieldHeaders();
                    Dictionary<string, int> headers = new Dictionary<string, int>();
                    for (int i = 0; i < head.Length; i++)
                    {
                        headers.Add(head[i].Trim().ToLower(), i);
                    }

                    while (csv.ReadNextRecord())
                    {
                        int locationID = Convert.ToInt32(csv[headers["locationid"]]);

                        SDLocation loc = null;
                        if (!locations.TryGetValue(locationID, out loc))
                        {
                            loc = new SDLocation(false);

                            loc.SocialDealerID = Convert.ToInt64(csv[headers["locationid"]]);
                            loc.GroupID = Convert.ToInt64(csv[headers["groupid"]]);

                            loc.Address = csv[headers["address"]];
                            loc.City = csv[headers["city"]];
                            loc.StateAbbreviation = csv[headers["state"]];
                            loc.ZipCode = csv[headers["zipcode"]];
                            loc.LocationName = csv[headers["locationname"]];

                            locations.Add(locationID, loc);
                        }

                        SDReviewURL url = new SDReviewURL()
                        {
                            ReviewSource = (SDReviewSource)Convert.ToInt32(csv[headers["reviewssourceid"]]),
                            URL = csv[headers["url"]],
                            RequestURL = csv[headers["requesturl"]],
                            LookupID = csv[headers["lookupid"]]
                        };

                        if (url.URL.Trim().ToLower() == "null")
                            url.URL = null;
                        if (url.RequestURL.Trim().ToLower() == "null")
                            url.RequestURL = null;

                        loc.ReviewURLs.Add(url);

                        if (url.ReviewSource == SDReviewSource.Edmunds && !string.IsNullOrWhiteSpace(url.LookupID))
                        {
                            long edmundsID;
                            if (long.TryParse(url.LookupID, out edmundsID))
                            {
                                loc.EdmundsDealershipID = edmundsID;
                            }
                        }

                    }

                }
            }

            return locations;

        }
        private class SDLocation
        {
            public SDLocation(bool isGroup)
            {
                IsGroup = isGroup;
                ReviewURLs = new List<SDReviewURL>();
            }
            public long SocialDealerID { get; set; }
            public long SocialIntegrationID { get; set; }

            public bool IsGroup { get; set; }
            public long GroupID { get; set; }

            public string LocationName { get; set; }
            public string Address { get; set; }
            public string City { get; set; }
            public string StateAbbreviation { get; set; }
            public string ZipCode { get; set; }

            public long EdmundsDealershipID { get; set; }

            public List<SDReviewURL> ReviewURLs { get; set; }
        }
        private class SDReviewURL
        {
            public SDReviewSource ReviewSource { get; set; }
            public string URL { get; set; }
            public string RequestURL { get; set; }
            public string LookupID { get; set; }
        }

        private enum SDReviewSource : int
        {
            Google = 1,
            DealerRater = 2,
            Yahoo = 3,
            Yelp = 4,
            CarDealer = 5,
            YellowPages = 6,
            Edmunds = 7,
            CarsDotCom = 8,
            CitySearch = 9,
            JudysBook = 10
        }

        #endregion
    }
}
