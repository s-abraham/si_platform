﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SI.DTO;
using SI.Services.Social;
using SI.Services.Social.Models;
using System.Diagnostics;
using SI.BLL;
using SI.Services.Admin;
using SI.Services.Framework.Models;

namespace TestApp
{
    class ServiceTest
    {
        static void Main(string[] args)
        {
            DeletePostTargets();

            //ProviderFactory.Platform.SendSyndicationSignupEmail(new Guid("dfd2c6e8-cff0-49bc-a370-88f1c4107419"), SocialNetworkEnum.Facebook);
            //ProviderFactory.Platform.SendSyndicationSignupEmail(new Guid("dfd2c6e8-cff0-49bc-a370-88f1c4107419"), SocialNetworkEnum.Twitter);


            //List<GooglePlusComment> googlePlusComments = new List<GooglePlusComment>();
            //UserInfoDTO userInfo = new UserInfoDTO(42022,null);
            //userInfo.EffectiveUser.CurrentContextAccountID = 220246;
            
            
            //int totalCount = 0;
            //PagedGridRequest pagedGridRequest = new PagedGridRequest();
            //List<PostActivity> obj= SocialService.DealerPostActivityGrid(userInfo, pagedGridRequest, ref totalCount);

            //AdminService.SocialConfigListView(userInfo, null, 220342);

            //List<SocialDashboardDataTwitter> results = ProviderFactory.Social.GetSocialDashboardData_Twitter(userInfo, DateTime.Now.AddDays(-50), DateTime.Now);

            //ProviderFactory.Social.SetSyndicationUnsubscribe(new Guid("13578506-8695-403a-849e-0e3df23ac48b"), userInfo);

            ////List<SocialDashboardDataFacebook> results = ProviderFactory.Social.GetSocialDashboardData_facebook(userInfo, DateTime.Now.AddDays(-50), DateTime.Now);
            ////List<SocialFacebookPostData> results = ProviderFactory.Social.GetSocialFacebookPostData(userInfo, DateTime.Now.AddDays(-50), DateTime.Now);
            //List<SocialTwitterPostData> results = ProviderFactory.Social.GetSocialTwitterPostData(userInfo, DateTime.Now.AddDays(-120), DateTime.Now);
            //Console.ReadLine();

            //Guid a1 = new Guid("26C1CF06-436A-4D86-B885-35C850F540EC");
                      
            //ProviderFactory.Social.SaveSocialConnectionFromSyndication(userInfo, 54, "CAAGfD25ViIcBABUMIYYjHGsYkxXSnLdLgHVsz1z5FYveceQV4AX5d5f06qzOGyDSwWaIwtidpEw6HNo4C76oLoS5oQFOAUIPNZC148mrGVr8uY7GW2qdFYtjSBOwmAlJh8OB9L1UpwedUCIsAYab6P7mZCODym7YOBoEgLFdniM61ZC6mGIPEscewmqzNsZD"
            //                                                                , "222950021115519", "Phil Penton", "http://www.facebook.com/home.php?#!/profile.php?id=222950021115519", "", a1);


            //List<GooglePlusStream> results = SocialService.GetGooglePlusStreams(userInfo, 220795, 20);
			//#region Facebook
			//var sw = Stopwatch.StartNew();
			//SocialService.DeletePostfromSocialNetwork(userInfo, 12649);
			//double totaldupstime = sw.Elapsed.Seconds;
			//Console.WriteLine("Facebook : " + totaldupstime);
			//#endregion

			//#region Twitter
			//var sw1 = Stopwatch.StartNew();
			//SocialService.DeletePostfromSocialNetwork(userInfo, 12651);
			//double totaldupstime1 = sw1.Elapsed.Seconds;
			//Console.WriteLine("Twitter : " + totaldupstime1);
			//#endregion

			//#region Google+
			//var sw2 = Stopwatch.StartNew();
			//SocialService.DeletePostfromSocialNetwork(userInfo, 12650);
			//double totaldupstime2 = sw2.Elapsed.Seconds;
			//Console.WriteLine("Google+ : " + totaldupstime2);
			//#endregion
            
            Console.ReadLine();
        }

        private static void DeletePostTargets()
        {
            UserInfoDTO userInfo = new UserInfoDTO(42022, null);
            userInfo.EffectiveUser.CurrentContextAccountID = 220246;
            string message = string.Empty;
            
            List<long> PostTargets = GetPostTargets();

            int iCounter = 1;
            foreach (var postTarget in PostTargets)
            {
                SocialService.DeletePostfromSocialNetwork(userInfo, 54544, ref message);

                Debug.WriteLine(iCounter + ": " + postTarget + " - " + message);
                Console.WriteLine(iCounter + ": " + postTarget + " - " + message);
                iCounter++;
            }
            
        }
        private static List<long> GetPostTargets()
        {
            List<long> PostTargets = new List<long>();

            //PostTargets.Add(54508);
            PostTargets.Add(54509);
            PostTargets.Add(54510);
            PostTargets.Add(54511);
            PostTargets.Add(54512);
            PostTargets.Add(54513);
            PostTargets.Add(54514);
            PostTargets.Add(54515);
            PostTargets.Add(54516);
            PostTargets.Add(54517);
            PostTargets.Add(54518);
            PostTargets.Add(54519);
            PostTargets.Add(54520);
            PostTargets.Add(54521);
            PostTargets.Add(54522);
            PostTargets.Add(54523);
            PostTargets.Add(54524);
            PostTargets.Add(54525);
            PostTargets.Add(54526);
            PostTargets.Add(54527);
            PostTargets.Add(54528);
            PostTargets.Add(54529);
            PostTargets.Add(54530);
            PostTargets.Add(54531);
            PostTargets.Add(54532);
            PostTargets.Add(54533);
            PostTargets.Add(54534);
            PostTargets.Add(54535);
            PostTargets.Add(54536);
            PostTargets.Add(54537);
            PostTargets.Add(54538);
            PostTargets.Add(54539);
            PostTargets.Add(54540);
            PostTargets.Add(54541);
            PostTargets.Add(54542);
            PostTargets.Add(54543);
            PostTargets.Add(54544);
            PostTargets.Add(54545);
            PostTargets.Add(54546);
            PostTargets.Add(54547);
            PostTargets.Add(54548);
            PostTargets.Add(54549);
            PostTargets.Add(54550);
            PostTargets.Add(54551);
            PostTargets.Add(54552);
            PostTargets.Add(54553);
            PostTargets.Add(54554);
            PostTargets.Add(54555);
            PostTargets.Add(54556);
            PostTargets.Add(54557);
            PostTargets.Add(54558);
            PostTargets.Add(54559);
            PostTargets.Add(54560);
            PostTargets.Add(54561);
            PostTargets.Add(54562);
            PostTargets.Add(54563);
            PostTargets.Add(54564);
            PostTargets.Add(54565);
            PostTargets.Add(54566);
            PostTargets.Add(54567);
            PostTargets.Add(54568);
            PostTargets.Add(54569);
            PostTargets.Add(54570);
            PostTargets.Add(54571);
            PostTargets.Add(54572);
            PostTargets.Add(54573);
            PostTargets.Add(54574);
            PostTargets.Add(54575);
            PostTargets.Add(54576);
            PostTargets.Add(54577);
            PostTargets.Add(54578);
            PostTargets.Add(54579);
            PostTargets.Add(54580);
            PostTargets.Add(54581);
            PostTargets.Add(54582);
            PostTargets.Add(54583);
            PostTargets.Add(54584);
            PostTargets.Add(54585);
            PostTargets.Add(54586);
            PostTargets.Add(54587);
            PostTargets.Add(54588);
            PostTargets.Add(54589);
            PostTargets.Add(54590);
            PostTargets.Add(54591);
            PostTargets.Add(54592);
            PostTargets.Add(54593);
            PostTargets.Add(54594);
            PostTargets.Add(54595);
            PostTargets.Add(54596);
            PostTargets.Add(54597);
            PostTargets.Add(54598);
            PostTargets.Add(54599);
            PostTargets.Add(54600);
            PostTargets.Add(54601);
            PostTargets.Add(54602);
            PostTargets.Add(54603);
            PostTargets.Add(54604);
            PostTargets.Add(54605);
            PostTargets.Add(54606);
            PostTargets.Add(54607);
            PostTargets.Add(54608);
            PostTargets.Add(54609);
            PostTargets.Add(54610);
            PostTargets.Add(54611);
            PostTargets.Add(54612);
            PostTargets.Add(54613);
            PostTargets.Add(54614);
            PostTargets.Add(54615);
            PostTargets.Add(54616);
            PostTargets.Add(54617);
            PostTargets.Add(54618);
            PostTargets.Add(54619);
            PostTargets.Add(54620);
            PostTargets.Add(54621);
            PostTargets.Add(54622);
            PostTargets.Add(54623);
            PostTargets.Add(54624);
            PostTargets.Add(54625);
            PostTargets.Add(54626);
            PostTargets.Add(54627);
            PostTargets.Add(54628);
            PostTargets.Add(54629);
            PostTargets.Add(54630);
            PostTargets.Add(54631);
            PostTargets.Add(54632);
            PostTargets.Add(54633);
            PostTargets.Add(54634);
            PostTargets.Add(54635);
            PostTargets.Add(54636);
            PostTargets.Add(54637);
            PostTargets.Add(54638);
            PostTargets.Add(54639);
            PostTargets.Add(54640);
            PostTargets.Add(54641);
            PostTargets.Add(54642);
            PostTargets.Add(54643);
            PostTargets.Add(54644);
            PostTargets.Add(54645);
            PostTargets.Add(54646);
            PostTargets.Add(54647);
            PostTargets.Add(54648);
            PostTargets.Add(54649);
            PostTargets.Add(54650);
            PostTargets.Add(54651);
            PostTargets.Add(54652);
            PostTargets.Add(54653);
            PostTargets.Add(54654);
            PostTargets.Add(54655);
            PostTargets.Add(54656);
            PostTargets.Add(54657);
            PostTargets.Add(54658);
            PostTargets.Add(54659);
            PostTargets.Add(54660);
            PostTargets.Add(54661);
            PostTargets.Add(54662);
            PostTargets.Add(54663);
            PostTargets.Add(54664);
            PostTargets.Add(54665);
            PostTargets.Add(54666);
            PostTargets.Add(54667);
            PostTargets.Add(54668);
            PostTargets.Add(54669);
            PostTargets.Add(54670);
            PostTargets.Add(54671);
            PostTargets.Add(54672);
            PostTargets.Add(54673);
            PostTargets.Add(54674);
            PostTargets.Add(54675);
            PostTargets.Add(54676);
            PostTargets.Add(54677);
            PostTargets.Add(54678);
            PostTargets.Add(54679);
            PostTargets.Add(54680);
            PostTargets.Add(54681);
            PostTargets.Add(54682);
            PostTargets.Add(54683);
            PostTargets.Add(54684);
            PostTargets.Add(54685);
            PostTargets.Add(54686);
            PostTargets.Add(54687);
            PostTargets.Add(54688);
            PostTargets.Add(54689);
            PostTargets.Add(54690);
            PostTargets.Add(54691);
            PostTargets.Add(54692);
            PostTargets.Add(54693);
            PostTargets.Add(54694);
            PostTargets.Add(54695);
            PostTargets.Add(54696);
            PostTargets.Add(54697);
            PostTargets.Add(54698);
            PostTargets.Add(54699);
            PostTargets.Add(54700);
            PostTargets.Add(54701);
            PostTargets.Add(54702);
            PostTargets.Add(54703);
            PostTargets.Add(54704);
            PostTargets.Add(54705);
            PostTargets.Add(54706);
            PostTargets.Add(54707);
            PostTargets.Add(54708);
            PostTargets.Add(54709);
            PostTargets.Add(54710);
            PostTargets.Add(54711);
            PostTargets.Add(54712);
            PostTargets.Add(54713);
            PostTargets.Add(54714);
            PostTargets.Add(54715);
            PostTargets.Add(54716);
            PostTargets.Add(54717);
            PostTargets.Add(54718);
            PostTargets.Add(54719);
            PostTargets.Add(54720);
            PostTargets.Add(54721);
            PostTargets.Add(54722);
            PostTargets.Add(54723);
            PostTargets.Add(54724);
            PostTargets.Add(54725);
            PostTargets.Add(54726);
            PostTargets.Add(54727);
            PostTargets.Add(54728);
            PostTargets.Add(54729);
            PostTargets.Add(54730);
            PostTargets.Add(54731);
            PostTargets.Add(54732);
            PostTargets.Add(54733);
            PostTargets.Add(54734);
            PostTargets.Add(54735);
            PostTargets.Add(54736);
            PostTargets.Add(54737);
            PostTargets.Add(54738);
            PostTargets.Add(54739);
            PostTargets.Add(54740);
            PostTargets.Add(54741);
            PostTargets.Add(54742);
            PostTargets.Add(54743);
            PostTargets.Add(54744);
            PostTargets.Add(54745);
            PostTargets.Add(54746);
            PostTargets.Add(54747);
            PostTargets.Add(54748);
            PostTargets.Add(54749);
            PostTargets.Add(54750);
            PostTargets.Add(54751);
            PostTargets.Add(54752);
            PostTargets.Add(54753);
            PostTargets.Add(54754);
            PostTargets.Add(54755);
            PostTargets.Add(54756);
            PostTargets.Add(54757);
            PostTargets.Add(54758);
            PostTargets.Add(54759);
            PostTargets.Add(54760);
            PostTargets.Add(54761);
            PostTargets.Add(54762);
            PostTargets.Add(54763);
            PostTargets.Add(54764);
            PostTargets.Add(54765);
            PostTargets.Add(54766);
            PostTargets.Add(54767);
            PostTargets.Add(54768);
            PostTargets.Add(54769);
            PostTargets.Add(54770);
            PostTargets.Add(54771);
            PostTargets.Add(54772);
            PostTargets.Add(54773);
            PostTargets.Add(54774);
            PostTargets.Add(54775);
            PostTargets.Add(54776);
            PostTargets.Add(54777);
            PostTargets.Add(54778);
            PostTargets.Add(54779);
            PostTargets.Add(54780);
            PostTargets.Add(54781);
            PostTargets.Add(54782);
            PostTargets.Add(54783);
            PostTargets.Add(54784);
            PostTargets.Add(54785);
            PostTargets.Add(54786);
            PostTargets.Add(54787);
            PostTargets.Add(54788);
            PostTargets.Add(54789);
            PostTargets.Add(54790);
            PostTargets.Add(54791);
            PostTargets.Add(54792);
            PostTargets.Add(54793);
            PostTargets.Add(54794);
            PostTargets.Add(54795);
            PostTargets.Add(54796);
            PostTargets.Add(54797);
            PostTargets.Add(54798);
            PostTargets.Add(54799);
            PostTargets.Add(54800);
            PostTargets.Add(54801);
            PostTargets.Add(54802);
            PostTargets.Add(54803);
            PostTargets.Add(54804);
            PostTargets.Add(54805);
            PostTargets.Add(54806);
            PostTargets.Add(54807);
            PostTargets.Add(54808);
            PostTargets.Add(54809);
            PostTargets.Add(54810);
            PostTargets.Add(54811);
            PostTargets.Add(54812);
            PostTargets.Add(54813);
            PostTargets.Add(54814);
            PostTargets.Add(54815);
            PostTargets.Add(54816);
            PostTargets.Add(54817);
            PostTargets.Add(54818);
            PostTargets.Add(54819);
            PostTargets.Add(54820);
            PostTargets.Add(54821);
            PostTargets.Add(54822);
            PostTargets.Add(54823);
            PostTargets.Add(54824);
            PostTargets.Add(54825);
            PostTargets.Add(54826);
            PostTargets.Add(54827);
            PostTargets.Add(54828);
            PostTargets.Add(54829);
            PostTargets.Add(54830);
            PostTargets.Add(54831);
            PostTargets.Add(54832);
            PostTargets.Add(54833);
            PostTargets.Add(54834);
            PostTargets.Add(54835);
            PostTargets.Add(54836);
            PostTargets.Add(54837);
            PostTargets.Add(54838);
            PostTargets.Add(54839);
            PostTargets.Add(54840);
            PostTargets.Add(54841);
            PostTargets.Add(54842);
            PostTargets.Add(54843);
            PostTargets.Add(54844);
            PostTargets.Add(54845);
            PostTargets.Add(54846);
            PostTargets.Add(54847);
            PostTargets.Add(54848);
            PostTargets.Add(54849);
            PostTargets.Add(54850);
            PostTargets.Add(54851);
            PostTargets.Add(54852);
            PostTargets.Add(54853);
            PostTargets.Add(54854);
            PostTargets.Add(54855);
            PostTargets.Add(54856);
            PostTargets.Add(54857);
            PostTargets.Add(54858);
            PostTargets.Add(54859);
            PostTargets.Add(54860);
            PostTargets.Add(54861);
            PostTargets.Add(54862);
            PostTargets.Add(54863);
            PostTargets.Add(54864);
            PostTargets.Add(54865);
            PostTargets.Add(54866);
            PostTargets.Add(54867);
            PostTargets.Add(54868);
            PostTargets.Add(54869);
            PostTargets.Add(54870);
            PostTargets.Add(54871);
            PostTargets.Add(54872);
            PostTargets.Add(54873);
            PostTargets.Add(54874);
            PostTargets.Add(54875);

            return PostTargets;
        }
    }
}
