﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using SI.DTO;
using SI.BLL;
using System.Diagnostics;
using System.Data;
using SI;
using Connectors.Parameters;
using Connectors;
using Facebook.Entities;
using Facebook;

namespace TestApp
{
    class Facebook
    {
        static void Main(string[] args)
        {
            BackFillPost();

            //PageAccessTokenByUserAccessToken();
            /*
            List<FBInsightDateDTO> FBInsightDates = ProviderFactory.Social.GetDatesforFaceBookInsightstoBackFill();
            if (FBInsightDates.Any())
            {
                UserInfoDTO userInfo = new UserInfoDTO(42022,null);
                userInfo.EffectiveUser.CurrentContextAccountID = 220246;

                // spread jobs over 1 hours
                double secondsBetweenJobs = 3600.00 / Convert.ToDouble(FBInsightDates.Count());

                //temporary
                //secondsBetweenJobs = 5.0;

                DateTime curDate = DateTime.UtcNow;
                List<JobDTO> jobsToQueue = new List<JobDTO>();

                foreach (var FBInsightDate in FBInsightDates)
                {
                    //List<SocialCredentialDTO> SocialCredentials = ProviderFactory.Social.GetAccountSocialCredentials(userInfo, FBInsightDate.AccountID);
                    //SocialCredentialDTO socialCredential = SocialCredentials.Where(s => s.IsActive == true && s.IsValid == true && s.SocialNetworkID == 1).FirstOrDefault();

                    if (FBInsightDate != null)
                    {
                        if (FBInsightDate.CredentialID > 0)
                        {
                            FacebookDownloaderData fdd = new FacebookDownloaderData()
                            {
                                FacebookEnum = FacebookProcessorActionEnum.FacebookPageInsightsDownloaderHistoric,
                                CredentialID = FBInsightDate.CredentialID,
                                StartDate = FBInsightDate.Date,
                                EndDate = FBInsightDate.Date,
                                RemainingRetries = 0
                            };

                            JobDTO job1 = new JobDTO()
                            {
                                JobDataObject = fdd,
                                JobType = fdd.JobType,
                                DateScheduled = new SIDateTime(curDate, 1),
                                JobStatus = JobStatusEnum.Queued,
                                Priority = 200
                            };

                            //SaveEntityDTO<JobDTO> save = ProviderFactory.Jobs.Save(new SaveEntityDTO<JobDTO>(job1, UserInfoDTO.System));
                            //var result = save;

                            jobsToQueue.Add(job1);

                            curDate = curDate.AddSeconds(secondsBetweenJobs);
                        }
                    }                    
                }

                if (jobsToQueue.Any())
                {
                    ProviderFactory.Jobs.Save(new SaveEntityDTO<List<JobDTO>>(jobsToQueue, UserInfoDTO.System));
                }
            }
            //ProcessFromXML();
            */
            
            //set Start And End Date for Insight
            ///DateTime Insight_StartDate = new DateTime(2014, 01, 17);
            //DateTime Insight_EndDate = new DateTime(2014, 01, 17);

            //List<long> AccoutIDs = SetAccountIDs();

            //foreach (long AccoutID in AccoutIDs)
            //{
            //    Debug.WriteLine("AccountID: " + AccoutID);
            //    Console.WriteLine("AccountID: " + AccoutID);
            //    for (DateTime date = Insight_StartDate; date <= Insight_EndDate; date = date.AddDays(1))
            //    {
            //        ProviderFactory.Social.ProcessFacebookHistoricData(AccoutID, date.ToShortDateString(), date.ToShortDateString());
            //        Debug.WriteLine("Date: " + date.ToShortDateString());
            //        Console.WriteLine("Date: " + date.ToShortDateString());
            //    }
            //    Debug.WriteLine("-----------------------------------");
            //    Console.WriteLine("-----------------------------------");
            //}
           
            //Get Credentials for Each Accounts
             
        }

        private static void BackFillPost()
        {
            List<long> CredentialIDs = new List<long>();
            CredentialIDs.Add(201);
            CredentialIDs.Add(4980);
            CredentialIDs.Add(4981);
            CredentialIDs.Add(4983);
            CredentialIDs.Add(4987);
            CredentialIDs.Add(4992);
            CredentialIDs.Add(5000);
            CredentialIDs.Add(5001);
            CredentialIDs.Add(5002);
            CredentialIDs.Add(5003);
            CredentialIDs.Add(5007);
            CredentialIDs.Add(5011);
            CredentialIDs.Add(5012);

            foreach (var CredentialID in CredentialIDs)
            {
                Console.WriteLine(CredentialID);
                SocialCredentialDTO socialCredentialsDTO = ProviderFactory.Social.GetSocialCredentialInfo(CredentialID);

                SIFacebookPageParameters pageParameters = new SIFacebookPageParameters();
                pageParameters.UniqueID = socialCredentialsDTO.UniqueID;
                pageParameters.Token = socialCredentialsDTO.Token;

                SIFacebookPageConnectionParameters ConnectionParameters = new SIFacebookPageConnectionParameters(pageParameters);
                FacebookPageConnection pageconnection = new FacebookPageConnection(ConnectionParameters);
                FacebookFeed facebookFeed = new FacebookFeed();

                pageParameters.StartDate = (new DateTime(2014, 02, 01)).ToShortDateString();
                pageParameters.EndDate = (new DateTime(2014, 03, 31)).ToShortDateString();
                
                FacebookFeed FBFeed = pageconnection.GetFacebookFeed();

                if (string.IsNullOrEmpty(FBFeed.facebookResponse.message))
                {
                    facebookFeed.facebookResponse = FBFeed.facebookResponse;

                    foreach (var feed in FBFeed.feed.data)
                    {
                        facebookFeed.feed.data.Add(feed);
                    }
                }

                if (string.IsNullOrEmpty(facebookFeed.facebookResponse.message))
                {
                    int iCounter = 1;
                    foreach (DataFeed feed in facebookFeed.feed.data)
                    {
                        //ToDo Add dynmoDB 

                        //Insert/Update Post Detail to Facebook Post Table
                        string feedid = string.Empty;
                        string resultid = string.Empty;

                        if (string.IsNullOrWhiteSpace(feed.object_id))
                        {
                            resultid = feed.id;
                            feedid = string.Empty;
                        }
                        else
                        {
                            resultid = feed.object_id;
                            feedid = feed.id;
                        }

                        if (string.IsNullOrEmpty(feedid))
                        {
                            feedid = resultid;
                        }

                        string linkURL = string.Empty;
                        string pictureURL = string.Empty;
                        PostTypeEnum PostType = FacebookAPI.GetPostType(feed.type.ToLower());

                        if (PostType == PostTypeEnum.Link)
                        {
                            linkURL = feed.link;
                        }

                        if (PostType == PostTypeEnum.Photo)
                        {
                            pictureURL = feed.picture;
                        }

                        if (PostType == PostTypeEnum.Video)
                        {
                            linkURL = feed.link;
                        }

                        if (PostType == PostTypeEnum.Offer)
                        {
                            linkURL = feed.link;
                        }

                        if (string.IsNullOrWhiteSpace(feed.message))
                        {
                            feed.message = feed.caption;
                        }

                        if (string.IsNullOrWhiteSpace(feed.message))
                        {
                            feed.message = feed.name;
                        }

                        Debug.WriteLine(iCounter + " ResultID :" + resultid + " ,Date: " + feed.created_time);

                        bool Success = ProviderFactory.Social.SaveFaceBookPost(CredentialID, feedid, resultid, feed.name, feed.message, PostType, linkURL, pictureURL, feed.created_time.ToUniversalTime(), feed.updated_time.ToUniversalTime(), feed.status_type);
                        
                    }


                }
            }
        }
        private static void PageAccessTokenByUserAccessToken()
        {
            List<long> CredentialIDs = new List<long>();

            CredentialIDs.Add(201);
            CredentialIDs.Add(4980);
            CredentialIDs.Add(4981);
            CredentialIDs.Add(4983);
            CredentialIDs.Add(4987);
            CredentialIDs.Add(4992);
            CredentialIDs.Add(5000);
            CredentialIDs.Add(5001);
            CredentialIDs.Add(5002);
            CredentialIDs.Add(5003);
            CredentialIDs.Add(5007);
            CredentialIDs.Add(5011);
            CredentialIDs.Add(5012);

            UserInfoDTO userInfo = new UserInfoDTO(42022, null);
            userInfo.EffectiveUser.CurrentContextAccountID = 220246;

            foreach (var CredentialID in CredentialIDs)
            {
                SocialCredentialDTO socialCredentialsDTO  = ProviderFactory.Social.GetSocialCredentialInfo(CredentialID);

                if (socialCredentialsDTO.IsActive == true && socialCredentialsDTO.IsValid == false)
                {
                    string accessToken =  ProviderFactory.Social.GetPageAccessTokenByUserAccessToken(socialCredentialsDTO.UniqueID, socialCredentialsDTO.Token);

                    if (!string.IsNullOrEmpty(accessToken))
                    {
                        ProviderFactory.Social.UpdateSocialConnectionToken(socialCredentialsDTO.ID, accessToken);
                    }
                }


            }
        }
        private static void ProcessFromXML()
        {
            string myXMLfile = @"C:\Dev2\SI\SI_PLATFORM\TestApp\FBDataToProcess.xml";
            DataSet ds = new DataSet();

            ds.ReadXml(myXMLfile);
            int icounter = 1;
            foreach (DataRow row in ds.Tables[0].Rows)
            {                
                long AccountID = Convert.ToInt64(row[0].ToString());
                DateTime date = Convert.ToDateTime(row[1].ToString());

                Debug.WriteLine(icounter + " AccountID: " + AccountID + " , Date: " + date.ToShortDateString());
                Console.WriteLine(icounter + " AccountID: " + AccountID + " , Date: " + date.ToShortDateString());                

                ProviderFactory.Social.ProcessFacebookHistoricData(AccountID, date.ToShortDateString(), date.ToShortDateString());
                icounter++;
            }

        }

        private static List<long> SetAccountIDs()
        {
            List<long> AccoutIDs = new List<long>();

            //All(2) Castle Motor Group 
            AccoutIDs.Add(220246);
            //AccoutIDs.Add(220245);

            //All(56) MileOne Account 
            //AccoutIDs.Add(220272);
            //AccoutIDs.Add(220273);
            //AccoutIDs.Add(220274);
            //AccoutIDs.Add(220275);
            //AccoutIDs.Add(220276);
            //AccoutIDs.Add(220277);
            //AccoutIDs.Add(220278);
            //AccoutIDs.Add(220279);
            //AccoutIDs.Add(220280);
            //AccoutIDs.Add(220281);
            //AccoutIDs.Add(220282);
            //AccoutIDs.Add(220283);
            //AccoutIDs.Add(220284);
            //AccoutIDs.Add(220285);
            //AccoutIDs.Add(220286);
            //AccoutIDs.Add(220287);
            //AccoutIDs.Add(220288);
            //AccoutIDs.Add(220289);
            //AccoutIDs.Add(220290);
            //AccoutIDs.Add(220291);
            //AccoutIDs.Add(220292);
            //AccoutIDs.Add(220293);
            //AccoutIDs.Add(220294);
            //AccoutIDs.Add(220295);
            //AccoutIDs.Add(220296);
            //AccoutIDs.Add(220297);
            //AccoutIDs.Add(220298);
            //AccoutIDs.Add(220299);
            //AccoutIDs.Add(220300);
            //AccoutIDs.Add(220301);
            //AccoutIDs.Add(220302);
            //AccoutIDs.Add(220303);
            //AccoutIDs.Add(220304);
            //AccoutIDs.Add(220305);
            //AccoutIDs.Add(220306);
            //AccoutIDs.Add(220307);
            //AccoutIDs.Add(220308);
            //AccoutIDs.Add(220309);
            //AccoutIDs.Add(220310);
            //AccoutIDs.Add(220311);
            //AccoutIDs.Add(220312);
            //AccoutIDs.Add(220313);
            //AccoutIDs.Add(220314);
            //AccoutIDs.Add(220315);
            //AccoutIDs.Add(220318);
            //AccoutIDs.Add(220320);
            //AccoutIDs.Add(220321);
            //AccoutIDs.Add(220390);
            //AccoutIDs.Add(220402);
            //AccoutIDs.Add(220403);
            //AccoutIDs.Add(220404);
            //AccoutIDs.Add(220405);
            //AccoutIDs.Add(220406);
            //AccoutIDs.Add(220541);
            //AccoutIDs.Add(220816);
            //AccoutIDs.Add(220865);

            return AccoutIDs;
        }
    }
}
