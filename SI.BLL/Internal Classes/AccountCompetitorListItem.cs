﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SI.BLL
{
    internal class AccountCompetitorListItem
    {
        public long AccountID { get; set; }
        public long CompetitorAccountID { get; set; }
    }
}
