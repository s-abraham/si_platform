﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SI.BLL
{
    internal class SearchStringParser
    {
        public List<string> IncludePhrases { get; set; }
        public List<string> ExcludePhrases { get; set; }

        public SearchStringParser(string searchString)
        {
            IncludePhrases = new List<string>();
            ExcludePhrases = new List<string>();
            if (!string.IsNullOrWhiteSpace(searchString))
            {
                parse(searchString);
            }
        }

        private void parse(string searchString)
        {

            List<string> parts =
                searchString
                .Trim()
                .Replace("\r", " ")
                .Replace("\n", " ")
                .Replace("\"", " \" ")
                .Replace("-", " - ")
                .Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries).ToList()
            ;

            string curPhrase = "";
            bool inQuotes = false;
            bool negate = false;
            foreach (string part in parts)
            {
                string item = part.Trim();
                if (item == "\"")
                {
                    inQuotes = !inQuotes;
                    if (!inQuotes)
                    {
                        if (!string.IsNullOrWhiteSpace(curPhrase))
                        {
                            if (negate)
                            {
                                if (!ExcludePhrases.Contains(curPhrase)) ExcludePhrases.Add(curPhrase);
                            }
                            else
                            {
                                if (!IncludePhrases.Contains(curPhrase)) IncludePhrases.Add(curPhrase);
                            }
                            negate = false;
                            curPhrase = "";
                        }
                    }
                }
                else if (item == "-" && !inQuotes)
                {
                    negate = true;
                }
                else
                {
                    if (inQuotes)
                    {
                        curPhrase = string.Format("{0} {1}", curPhrase, item).Trim();
                    }
                    else
                    {
                        if (!string.IsNullOrWhiteSpace(item))
                        {
                            if (negate)
                            {
                                if (!ExcludePhrases.Contains(item)) ExcludePhrases.Add(item);
                            }
                            else
                            {
                                if (!IncludePhrases.Contains(item)) IncludePhrases.Add(item);
                            }
                            negate = false;
                        }
                    }
                }
            }
        }
    }
}
