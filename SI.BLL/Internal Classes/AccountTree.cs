﻿using SI.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SI.BLL
{
    internal class AccountTree
    {
        public AccountTree()
        {
            Children = new List<AccountTree>();
        }

        public long ID { get; set; }
        public string Name { get; set; }

        /// <summary>
        /// 0 = Root, 1 = Reseller (usually), 2 = Groups or Dealers
        /// </summary>
        public int Depth { get; set; }

        public AccountTree Parent { get; set; }
        public List<AccountTree> Children { get; set; }
        public long? ResellerID { get; set; }

        public AccountTypeEnum AccountType { get; set; }
    }
}
