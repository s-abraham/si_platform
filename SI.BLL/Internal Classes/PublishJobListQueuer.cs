﻿using SI.DAL;
using SI.DTO;
using System;
using System.Collections.Generic;
using System.Data.Common;
using Microsoft.Data.Extensions;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SI.BLL.Interface;

namespace SI.BLL
{
    internal class PublishJobListQueuer
    {
        public PublishJobListQueuer(long accountID, long postID)
        {
            AccountID = accountID;
            PostID = postID;

            Auditor
                .New(AuditLevelEnum.Information, AuditTypeEnum.PublishActivity, UserInfoDTO.System, "")
                .SetAuditDataObject(
                    AuditPublishActivity
                    .New(postID, AuditPublishActivityTypeEnum.PassedToPublishJobListQueuer)
                    .SetPostID(postID)
                    .SetAccountID(accountID)
                    .SetDescription("PublishJobListQueuer instantiated for PostID {0} and AccountID {1}", postID, accountID)
                )
                .Save(ProviderFactory.Logging)
            ;

            PublishTargets = new List<PublishTargetInfo>();
            publishInfos = new List<PostTargetPublishInfoDTO>();
            //PostImageIDs = new List<long>();
            PostImages = new List<PostImageInfo>();
        }

        public long AccountID { get; set; }
        public long PostID { get; set; }
        public List<PublishTargetInfo> PublishTargets { get; set; }
        private List<PostTargetPublishInfoDTO> publishInfos { get; set; }

        //public List<long> PostImageIDs { get; set; }
        public List<PostImageInfo> PostImages { get; set; }

        public static PublishJobListQueuer New(long accountID, long postID)
        {
            return new PublishJobListQueuer(accountID, postID);
        }

        public PublishJobListQueuer LoadTargets(MainEntities db, ISocialProvider social)
        {
            PublishTargets.Clear();
            PostImages.Clear();

            if (db.Connection.State != System.Data.ConnectionState.Open) db.Connection.Open();

            using (DbCommand cmd = db.CreateStoreCommand(
               "spGetTargetsForAccountAndPostID2",
                System.Data.CommandType.StoredProcedure,

                new SqlParameter("@accountID", AccountID),
                new SqlParameter("@postID", PostID)
           ))
            {

                using (DbDataReader reader = cmd.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        PublishTargetInfo pt = new PublishTargetInfo()
                        {
                            CredentialID = Util.GetReaderValue<long>(reader, "CredentialID", 0),
                            SocialAppID = Util.GetReaderValue<long>(reader, "SocialAppID", 0),
                            PostTargetID = Util.GetReaderValue<long>(reader, "PostTargetID", 0),
                            SocialNetwork = (SocialNetworkEnum)Util.GetReaderValue<long>(reader, "SocialNetworkID", 0),
                            PostType = (PostTypeEnum)Util.GetReaderValue<long>(reader, "PostTypeID", 0),
                            NoImagesSelected = Util.GetReaderValue<bool?>(reader, "NoImagesSelected", false)
                        };
                        Auditor
                            .New(AuditLevelEnum.Information, AuditTypeEnum.PublishActivity, UserInfoDTO.System, "")
                            .SetAuditDataObject(
                                AuditPublishActivity
                                .New(PostID, AuditPublishActivityTypeEnum.PublishJobListQueuerLoadedTarget)
                                .SetPostID(PostID)
                                .SetPostTargetID(pt.PostTargetID)
                                .SetAccountID(AccountID)
                                .SetDescription("PublishJobListQueuer loaded target for SocialAppID {0}, SocialNetwork {1} and PostType {2}", pt.SocialAppID, pt.SocialNetwork.ToString(), pt.PostType.ToString())
                            )
                            .Save(ProviderFactory.Logging)
                        ;
                        PublishTargets.Add(pt);
                    }

                    if (reader.NextResult())
                    {
                        while (reader.Read())
                        {
                            long postImageID = Util.GetReaderValue<long>(reader, "PostImageID", 0);
                            long? postTargetID = Util.GetReaderValue<long?>(reader, "PostTargetID", null);
                            Auditor
                                .New(AuditLevelEnum.Information, AuditTypeEnum.PublishActivity, UserInfoDTO.System, "")
                                .SetAuditDataObject(
                                    AuditPublishActivity
                                    .New(PostID, AuditPublishActivityTypeEnum.PublishJobListQueuerLoadedImage)
                                    .SetPostID(PostID)
                                    .SetPostImageID(postImageID)
                                    .SetPostTargetID(postTargetID)
                                    .SetAccountID(AccountID)
                                )
                                .Save(ProviderFactory.Logging)
                            ;
                            PostImages.Add(new PostImageInfo()
                            {
                                PostImageID = postImageID,
                                PostTargetID = postTargetID
                            });
                        }
                    }
                }
            }


            foreach (PublishTargetInfo item in PublishTargets)
            {
                publishInfos.Add(social.GetPostTargetPublishInfo(item.PostTargetID, null));
            }
            return this;
        }

        private PublishTargetInfo getTarget(SocialNetworkEnum network)
        {
            return (from t in PublishTargets where t.SocialNetwork == network select t).FirstOrDefault();
        }

        public PublishJobListQueuer QueueJobs(IJobProvider jobProvider)
        {
            Auditor
                .New(AuditLevelEnum.Information, AuditTypeEnum.PublishActivity, UserInfoDTO.System, "")
                .SetAuditDataObject(
                    AuditPublishActivity
                    .New(PostID, AuditPublishActivityTypeEnum.PublishJobListQueuerStartingJobQueueing)
                    .SetPostID(PostID)
                    .SetAccountID(AccountID)
                    .SetDescription("Starting with {0} PostTargets", PublishTargets.Count())
                )
                .Save(ProviderFactory.Logging)
            ;

            // take the publish targets and turn them into jobs

            List<JobDTO> jobsToQueue = new List<JobDTO>();
            long? referencePostTargetID = null;

            #region facebook

            // check for a facebook job
            {
                PublishTargetInfo tiFB = getTarget(SocialNetworkEnum.Facebook);
                if (tiFB == null)
                {
                    Auditor
                        .New(AuditLevelEnum.Information, AuditTypeEnum.PublishActivity, UserInfoDTO.System, "")
                        .SetAuditDataObject(
                            AuditPublishActivity
                            .New(PostID, AuditPublishActivityTypeEnum.PublishJobListQueuerSocialNetworkNotRepresented)
                            .SetPostID(PostID)
                            .SetAccountID(AccountID)
                            .SetDescription("Did not locate a target for social network Facebook.")
                        )
                        .Save(ProviderFactory.Logging)
                    ;
                }
                else
                {
                    Auditor
                        .New(AuditLevelEnum.Information, AuditTypeEnum.PublishActivity, UserInfoDTO.System, "")
                        .SetAuditDataObject(
                            AuditPublishActivity
                            .New(PostID, AuditPublishActivityTypeEnum.PublishJobListQueuerSocialNetworkRepresented)
                            .SetPostID(PostID)
                            .SetPostTargetID(tiFB.PostTargetID)
                            .SetAccountID(AccountID)
                            .SetDescription("Facebook target found.")
                        )
                        .Save(ProviderFactory.Logging)
                    ;

                    List<PostImageInfo> myPostImages = (from pi in PostImages where pi.PostTargetID.HasValue && pi.PostTargetID.Value == tiFB.PostTargetID select pi).ToList();
                    if (!myPostImages.Any() && tiFB.NoImagesSelected.GetValueOrDefault(false) == false)
                    {
                        myPostImages = (from pi in PostImages where !pi.PostTargetID.HasValue select pi).ToList();
                    }

                    if (myPostImages.Any())
                    {
                        // one job for each image
                        foreach (PostImageInfo pInfo in myPostImages)
                        {
                            
                            {
                                FacebookPublisherData data = new FacebookPublisherData
                                {
                                    FacebookEnum = FacebookProcessorActionEnum.FacebookPublish,
                                    CredentialID = tiFB.CredentialID,
                                    PostID = PostID,
                                    PostImageID = pInfo.PostImageID,
                                    PostTargetID = tiFB.PostTargetID,
                                    ReferencePostTargetID = referencePostTargetID,
                                    RemainingRetries = 1
                                };

                                JobDTO jobFB = new JobDTO()
                                {
                                    DateScheduled = new SIDateTime(DateTime.UtcNow, UserInfoDTO.System.EffectiveUser.ID.Value),
                                    Priority = 1000,
                                    ContinueChainIfFailed = false,
                                    JobStatus = JobStatusEnum.Queued,
                                    JobDataObject = data,
                                    JobType = data.JobType
                                };

                                Auditor
                                    .New(AuditLevelEnum.Information, AuditTypeEnum.PublishActivity, UserInfoDTO.System, "")
                                    .SetAuditDataObject(
                                        AuditPublishActivity
                                        .New(data.PostID, AuditPublishActivityTypeEnum.PublishJobListQueuerAddingPublishImageJob)
                                        .SetPostID(data.PostID)
                                        .SetPostTargetID(data.PostTargetID)
                                        .SetPostImageID(data.PostImageID)
                                        .SetAccountID(AccountID)
                                    )
                                    .Save(ProviderFactory.Logging)
                                ;

                                jobsToQueue.Add(jobFB);
                                if (!referencePostTargetID.HasValue) referencePostTargetID = tiFB.PostTargetID;
                            }
                        }
                    }
                    else
                    {
                        
                        {
                            FacebookPublisherData data = new FacebookPublisherData
                            {
                                FacebookEnum = FacebookProcessorActionEnum.FacebookPublish,
                                CredentialID = tiFB.CredentialID,
                                PostID = PostID,
                                PostImageID = null,
                                PostTargetID = tiFB.PostTargetID,
                                ReferencePostTargetID = referencePostTargetID,
                                RemainingRetries = 1
                            };

                            // no images, single job
                            JobDTO jobFB = new JobDTO()
                            {
                                DateScheduled = new SIDateTime(DateTime.UtcNow, UserInfoDTO.System.EffectiveUser.ID.Value),
                                Priority = 1000,
                                ContinueChainIfFailed = false,
                                JobStatus = JobStatusEnum.Queued,
                                JobDataObject = data,
                                JobType = data.JobType
                            };

                            Auditor
                                .New(AuditLevelEnum.Information, AuditTypeEnum.PublishActivity, UserInfoDTO.System, "")
                                .SetAuditDataObject(
                                    AuditPublishActivity
                                    .New(data.PostID, AuditPublishActivityTypeEnum.PublishJobListQueuerAddingPublishJob)
                                    .SetPostID(data.PostID)
                                    .SetPostTargetID(data.PostTargetID)
                                    .SetPostImageID(data.PostImageID)
                                    .SetAccountID(AccountID)
                                )
                                .Save(ProviderFactory.Logging)
                            ;

                            jobsToQueue.Add(jobFB);
                            if (!referencePostTargetID.HasValue)
                            {
                                referencePostTargetID = tiFB.PostTargetID;
                            }
                        }
                    }
                }
            }

            #endregion

            #region google

            {
                // check for a google job
                PublishTargetInfo tiGP = getTarget(SocialNetworkEnum.Google);
                if (tiGP == null)
                {
                    Auditor
                        .New(AuditLevelEnum.Information, AuditTypeEnum.PublishActivity, UserInfoDTO.System, "")
                        .SetAuditDataObject(
                            AuditPublishActivity
                            .New(PostID, AuditPublishActivityTypeEnum.PublishJobListQueuerSocialNetworkNotRepresented)
                            .SetPostID(PostID)
                            .SetAccountID(AccountID)
                            .SetDescription("Did not locate a target for social network Google.")
                        )
                        .Save(ProviderFactory.Logging)
                    ;
                }
                else
                {
                    Auditor
                        .New(AuditLevelEnum.Information, AuditTypeEnum.PublishActivity, UserInfoDTO.System, "")
                        .SetAuditDataObject(
                            AuditPublishActivity
                            .New(PostID, AuditPublishActivityTypeEnum.PublishJobListQueuerSocialNetworkRepresented)
                            .SetPostID(PostID)
                            .SetPostTargetID(tiGP.PostTargetID)
                            .SetAccountID(AccountID)
                            .SetDescription("Google target found.")
                        )
                        .Save(ProviderFactory.Logging)
                    ;

                    List<PostImageInfo> myPostImages = (from pi in PostImages where pi.PostTargetID.HasValue && pi.PostTargetID.Value == tiGP.PostTargetID select pi).ToList();
                    if (!myPostImages.Any() && tiGP.NoImagesSelected.GetValueOrDefault(false) == false)
                    {
                        myPostImages = (from pi in PostImages where !pi.PostTargetID.HasValue select pi).ToList();
                    }

                    
                    {
                        GooglePlusPublisherData data = new GooglePlusPublisherData
                        {
                            GooglePlusEnum = GooglePlusProcessorActionEnum.GooglePlusPublish,
                            CredentialID = tiGP.CredentialID,
                            PostID = PostID,
                            PostImageID = myPostImages.Any() ? (long?)myPostImages[0].PostImageID : null,
                            PostTargetID = tiGP.PostTargetID,
                            ReferencePostTargetID = referencePostTargetID,
                            RemainingRetries = 1
                        };
                        // single job only, if images exist, use the first postimageid
                        JobDTO jobGP = new JobDTO()
                        {
                            DateScheduled = new SIDateTime(DateTime.UtcNow, UserInfoDTO.System.EffectiveUser.ID.Value),
                            Priority = 1000,
                            ContinueChainIfFailed = false,
                            JobStatus = JobStatusEnum.Queued,
                            JobDataObject = data,
                            JobType = data.JobType
                        };

                        Auditor
                            .New(AuditLevelEnum.Information, AuditTypeEnum.PublishActivity, UserInfoDTO.System, "")
                            .SetAuditDataObject(
                                AuditPublishActivity
                                .New(data.PostID, AuditPublishActivityTypeEnum.PublishJobListQueuerAddingPublishJob)
                                .SetPostID(data.PostID)
                                .SetPostTargetID(data.PostTargetID)
                                .SetPostImageID(data.PostImageID)
                                .SetAccountID(AccountID)
                            )
                            .Save(ProviderFactory.Logging)
                        ;

                        jobsToQueue.Add(jobGP);
                        if (!referencePostTargetID.HasValue)
                        {
                            referencePostTargetID = tiGP.PostTargetID;
                        }
                    }
                }
            }

            #endregion

            #region twitter

            {
                // check for a twitter job
                PublishTargetInfo tiTW = getTarget(SocialNetworkEnum.Twitter);
                if (tiTW == null)
                {
                    Auditor
                        .New(AuditLevelEnum.Information, AuditTypeEnum.PublishActivity, UserInfoDTO.System, "")
                        .SetAuditDataObject(
                            AuditPublishActivity
                            .New(PostID, AuditPublishActivityTypeEnum.PublishJobListQueuerSocialNetworkNotRepresented)
                            .SetPostID(PostID)
                            .SetAccountID(AccountID)
                            .SetDescription("Did not locate a target for social network Twitter.")
                        )
                        .Save(ProviderFactory.Logging)
                    ;
                }
                else
                {
                    Auditor
                        .New(AuditLevelEnum.Information, AuditTypeEnum.PublishActivity, UserInfoDTO.System, "")
                        .SetAuditDataObject(
                            AuditPublishActivity
                            .New(PostID, AuditPublishActivityTypeEnum.PublishJobListQueuerSocialNetworkRepresented)
                            .SetPostID(PostID)
                            .SetPostTargetID(tiTW.PostTargetID)
                            .SetAccountID(AccountID)
                            .SetDescription("Twitter target found.")
                        )
                        .Save(ProviderFactory.Logging)
                    ;

                    List<PostImageInfo> myPostImages = (from pi in PostImages where pi.PostTargetID.HasValue && pi.PostTargetID.Value == tiTW.PostTargetID select pi).ToList();
                    if (!myPostImages.Any() && tiTW.NoImagesSelected.GetValueOrDefault(false) == false)
                    {
                        myPostImages = (from pi in PostImages where !pi.PostTargetID.HasValue select pi).ToList();
                    }

                    
                    {

                        TwitterPublisherData data = new TwitterPublisherData
                            {
                                TwitterEnum = TwitterProcessorActionEnum.TwitterPublish,
                                CredentialID = tiTW.CredentialID,
                                PostID = PostID,
                                PostImageID = myPostImages.Any() ? (long?)myPostImages[0].PostImageID : null,
                                PostTargetID = tiTW.PostTargetID,
                                ReferencePostTargetID = referencePostTargetID,
                                RemainingRetries = 1
                            }
                        ;

                        if (isPostTooBigForTwitter(tiTW, myPostImages.Any()) && jobsToQueue.Any())
                        {
                            // if it's too large for a twitter job, then slave it to the first job
                            ChainedJobSpec spec = new ChainedJobSpec()
                            {
                                ContinueChainIfFailed = false,
                                JobType = JobTypeEnum.TwitterPublish,
                                JobData = Newtonsoft.Json.JsonConvert.SerializeObject(data),
                                MaxAttempts = 2,
                                Priority = 1000
                            };
                            jobsToQueue[0].ChainedJobs.Enqueue(spec);

                            Auditor
                                .New(AuditLevelEnum.Information, AuditTypeEnum.PublishActivity, UserInfoDTO.System, "")
                                .SetAuditDataObject(
                                    AuditPublishActivity
                                    .New(data.PostID, AuditPublishActivityTypeEnum.PublishJobListQueuerAddingChainedJob)
                                    .SetPostID(data.PostID)
                                    .SetPostTargetID(data.PostTargetID)
                                    .SetPostImageID(data.PostImageID)
                                    .SetAccountID(AccountID)
                                    .SetDescription("Post too large for Twitter.  Chaining Twitter job to first job added.")
                                )
                                .Save(ProviderFactory.Logging)
                            ;
                        }
                        else
                        {
                            // it is not too large, or it's the only job, create its own job

                            data.ReferencePostTargetID = null;

                            JobDTO jobTW = new JobDTO()
                            {
                                DateScheduled = new SIDateTime(DateTime.UtcNow, UserInfoDTO.System.EffectiveUser.ID.Value),
                                Priority = 1000,
                                ContinueChainIfFailed = false,
                                JobStatus = JobStatusEnum.Queued,
                                JobDataObject = data,
                                JobType = data.JobType
                            };

                            Auditor
                                .New(AuditLevelEnum.Information, AuditTypeEnum.PublishActivity, UserInfoDTO.System, "")
                                .SetAuditDataObject(
                                    AuditPublishActivity
                                    .New(data.PostID, AuditPublishActivityTypeEnum.PublishJobListQueuerAddingPublishJob)
                                    .SetPostID(data.PostID)
                                    .SetPostTargetID(data.PostTargetID)
                                    .SetPostImageID(data.PostImageID)
                                    .SetAccountID(AccountID)
                                )
                                .Save(ProviderFactory.Logging)
                            ;

                            jobsToQueue.Add(jobTW);
                            if (!referencePostTargetID.HasValue) referencePostTargetID = tiTW.PostTargetID;

                        }
                    }

                }
            }

            #endregion

            SaveEntityDTO<List<JobDTO>> seJobs = null;
            
            {
                // queue the jobs
                seJobs = jobProvider.Save(new SaveEntityDTO<List<JobDTO>>(jobsToQueue, UserInfoDTO.System));
            }


            
            {
                if (seJobs.Problems.Count() > 0)
                {
                    ProviderFactory.Logging.Audit(AuditLevelEnum.Error, AuditTypeEnum.QueuePublishJobFailure, string.Join(", ", seJobs.Problems), null, UserInfoDTO.System, AccountID, null, null, null);
                    Auditor
                        .New(AuditLevelEnum.Error, AuditTypeEnum.PublishActivity, UserInfoDTO.System, "")
                        .SetAuditDataObject(
                            AuditPublishActivity
                            .New(PostID, AuditPublishActivityTypeEnum.PublishJobListQueuerSaveJobsFailure)
                            .SetPostID(PostID)
                            .SetAccountID(AccountID)
                            .SetDescription("Failed saving one or more jobs: {0}", string.Join(", ", seJobs.Problems))
                        )
                        .Save(ProviderFactory.Logging)
                    ;
                }
                else
                {
                    Auditor
                        .New(AuditLevelEnum.Information, AuditTypeEnum.PublishActivity, UserInfoDTO.System, "")
                        .SetAuditDataObject(
                            AuditPublishActivity
                            .New(PostID, AuditPublishActivityTypeEnum.PublishJobListQueuerSavedJobs)
                            .SetPostID(PostID)
                            .SetAccountID(AccountID)
                            .SetDescription("Saved job IDs: {0}", string.Join(", ",
                                (
                                    from x in seJobs.Entity
                                    select x.ID.Value
                                ).ToList()
                            ))
                        )
                        .Save(ProviderFactory.Logging)
                    ;
                }
            }
            return this;
        }

        private bool isPostTooBigForTwitter(PublishTargetInfo info, bool hasImages)
        {
            const int twitterMaxLength = 140;
            string message = (from pi in publishInfos where pi.PostTargetID == info.PostTargetID select pi.Message).FirstOrDefault();
            if (string.IsNullOrWhiteSpace(message))
                message = "";

            int linkLength = 0;
            if (info.PostType == PostTypeEnum.Photo)
            {
                linkLength = 24;
            }
            else if (info.PostType == PostTypeEnum.Link)
            {
                linkLength = 24;
            }
            else
            {
                linkLength = 0;
            }

            int twitterStatusLength = (message + linkLength).Length;

            if (twitterStatusLength > twitterMaxLength) return true;

            return false;
        }

        private JobDTO createPublishJob(SocialNetworkEnum network, PublishTargetInfo tinfo, long? postImageID)
        {
            switch (network)
            {
                case SocialNetworkEnum.Facebook:
                    {
                        FacebookPublisherData data = new FacebookPublisherData()
                        {
                            CredentialID = tinfo.CredentialID,
                            PostID = PostID,
                            PostTargetID = tinfo.PostTargetID,
                            FacebookEnum = FacebookProcessorActionEnum.FacebookPublish,
                            RemainingRetries = 1,
                            ReferencePostTargetID = null,
                            PostImageID = postImageID
                        };

                        JobDTO job = new JobDTO()
                        {
                            JobDataObject = data,
                            JobType = data.JobType,
                            DateScheduled = new SIDateTime(DateTime.UtcNow, UserInfoDTO.System.EffectiveUser.ID.Value),
                            JobStatus = JobStatusEnum.Queued,
                            Priority = 1000
                        };

                        return job;
                    }

                case SocialNetworkEnum.Google:
                    {
                        GooglePlusPublisherData data = new GooglePlusPublisherData()
                        {
                            CredentialID = tinfo.CredentialID,
                            PostID = PostID,
                            PostTargetID = tinfo.PostTargetID,
                            GooglePlusEnum = GooglePlusProcessorActionEnum.GooglePlusPublish,
                            RemainingRetries = 1,
                            ReferencePostTargetID = null,
                            PostImageID = postImageID
                        };
                        JobDTO job = new JobDTO()
                        {
                            JobDataObject = data,
                            JobType = data.JobType,
                            DateScheduled = new SIDateTime(DateTime.UtcNow, UserInfoDTO.System.EffectiveUser.ID.Value),
                            JobStatus = JobStatusEnum.Queued,
                            Priority = 1000
                        };

                        return job;
                    }

                case SocialNetworkEnum.Twitter:
                    {
                        TwitterPublisherData data = new TwitterPublisherData()
                        {
                            CredentialID = tinfo.CredentialID,
                            PostID = PostID,
                            PostTargetID = tinfo.PostTargetID,
                            TwitterEnum = TwitterProcessorActionEnum.TwitterPublish,
                            RemainingRetries = 1,
                            ReferencePostTargetID = null,
                            PostImageID = postImageID
                        };
                        JobDTO job = new JobDTO()
                        {
                            JobDataObject = data,
                            JobType = data.JobType,
                            DateScheduled = new SIDateTime(DateTime.UtcNow, UserInfoDTO.System.EffectiveUser.ID.Value),
                            JobStatus = JobStatusEnum.Queued,
                            Priority = 1000
                        };

                        return job;

                    }

            }
            return null;
        }

        public PublishJobListQueuer SetTargetsPickedUp(MainEntities db)
        {
            if (db.Connection.State != System.Data.ConnectionState.Open) db.Connection.Open();

            if (PublishTargets.Any())
            {
                StringBuilder sb = new StringBuilder();
                string sql = string.Format("update posttargets set PickedUpDate=getdate() where ID IN({0})", string.Join(",", (from pt in PublishTargets select pt.PostTargetID).Distinct().ToList()));
                db.ExecuteStoreCommand(sql);
            }

            return this;
        }


    }

    internal class PublishTargetInfo
    {
        public long PostTargetID { get; set; }
        public long SocialAppID { get; set; }
        public long CredentialID { get; set; }
        public bool? NoImagesSelected { get; set; }
        public PostTypeEnum PostType { get; set; }
        public SocialNetworkEnum SocialNetwork { get; set; }
    }

    internal class PostImageInfo
    {
        public long PostImageID { get; set; }
        public long? PostTargetID { get; set; }
    }
}
