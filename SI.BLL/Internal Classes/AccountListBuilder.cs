﻿using SI.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SI.BLL
{
    /// <summary>
    /// This class is used to form a list of accountIDs based upon varying requirements
    /// </summary>
    internal class AccountListBuilder
    {
        private SecurityProvider _security = null;

        public AccountListBuilder(SecurityProvider security)
        {
            _security = security;
            AccountIDList = new List<long>();
        }

        /// <summary>
        /// Creates a new instance of the AccountListBuilder
        /// </summary>
        public static AccountListBuilder New(SecurityProvider security)
        {
            return new AccountListBuilder(security);
        }

        /// <summary>
        /// The full account ID list that has been built
        /// </summary>
        public List<long> AccountIDList { get; set; }

        /// <summary>
        /// Adds a list of account IDs if they are not already added
        /// </summary>
        public AccountListBuilder AddAccountIDs(List<long> accountIDs)
        {
            AccountIDList.AddRange((from a in accountIDs where !AccountIDList.Contains(a) select a).Distinct().ToList());
            return this;
        }

        /// <summary>
        /// Adds a single account ID to the list if it has not already been added
        /// </summary>
        public AccountListBuilder AddAccountID(long accountID)
        {
            return AddAccountIDs(new List<long>() { accountID });
        }

        /// <summary>
        /// Expands the current account ID list to include all descendant accounts
        /// </summary>
        public AccountListBuilder ExpandToDescendants()
        {
            List<long> toAdd = new List<long>();
            foreach (long accountid in AccountIDList)
            {
                if (!toAdd.Contains(accountid))
                {
                    List<long> desc = _security.GetAccountDescendants(accountid);
                    toAdd.AddRange((from d in desc where !toAdd.Contains(d) select d).ToList());
                }
            }
            if (toAdd.Any())
            {
                AddAccountIDs(toAdd);
            }

            return this;
        }

        /// <summary>
        /// Adds account IDs where the given user has the given permission for that account
        /// </summary>
        public AccountListBuilder AddAccountsWithPermission(UserInfoDTO userInfo, PermissionTypeEnum type)
        {
            if (userInfo.EffectiveUser.SuperAdmin)
            {
                AddAccountIDs(_security.getAccountTree().Keys.ToList());
            }
            else
            {
                AddAccountIDs(_security.AccountsWithPermission(userInfo.EffectiveUser.ID.Value, type));
            }

            return this;
        }

        /// <summary>
        /// Removes account IDs where the given user does not have the given permission
        /// </summary>
        public AccountListBuilder FilterToAccountsWithPermission(UserInfoDTO userInfo, PermissionTypeEnum type)
        {
            if (!userInfo.EffectiveUser.SuperAdmin)
            {
                List<long> aids = _security.AccountsWithPermission(userInfo.EffectiveUser.ID.Value, type);
                AccountIDList = (from alist in AccountIDList where aids.Contains(alist) select alist).ToList();
            }
            return this;
        }

        /// <summary>
        /// Adds accounts that have a given feature
        /// </summary>
        public AccountListBuilder AddAccountsWithFeature(FeatureTypeEnum featureType)
        {
            List<long> aids = _security.GetAccountsWithFeature(featureType);
            AddAccountIDs(aids);
            return this;
        }
        /// <summary>
        /// Removes accounts that do not have a given feature
        /// </summary>
        public AccountListBuilder FilterToAccountsWithFeature(FeatureTypeEnum featureType)
        {
            List<long> aids = _security.GetAccountsWithFeature(featureType);
            AccountIDList = (from alist in AccountIDList where aids.Contains(alist) select alist).ToList();
            return this;
        }

        /// <summary>
        /// Adds accounts that are contained within a given virtual group
        /// </summary>
        public AccountListBuilder AddAccountsFromVirtualGroup(long virtualGroupID)
        {
            AddAccountIDs(_security.GetVirtualGroupAccountIDs(virtualGroupID));
            return this;
        }

        /// <summary>
        /// Removes accounts that are not contained within a given virtual group
        /// </summary>
        public AccountListBuilder FilterToAccountsFromVirtualGroup(long virtualGroupID)
        {
            List<long> aids = _security.GetVirtualGroupAccountIDs(virtualGroupID);
            AccountIDList = (from alist in AccountIDList where aids.Contains(alist) select alist).ToList();
            return this;
        }

        /// <summary>
        /// Gets the current list of account IDs, but limits to a maximum number of accounts
        /// </summary>
        /// <param name="maxIDs">The maximum number of account IDs to return</param>
        public List<long> GetIDs(int maxIDs)
        {
            return AccountIDList.Take(maxIDs).ToList();
        }

        /// <summary>
        /// Gets a list of syndicator ids based upon each account in the list that is a syndicator.
        /// </summary>
        public List<long> GetSyndicatorIDs()
        {
            return _security.getSyndicatorIDsFromAccountList(AccountIDList);
        }

        public string GetListHash()
        {
            List<long> idList = (from a in AccountIDList orderby a select a).ToList();
            string content = string.Join("|", idList.ToArray());
            return Security.GetHash(content, 25);
        }

    }
}
