﻿using SI.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SI.BLL
{
    internal class SubmitApprovalInfo
    {
        public long PostApprovalID { get; set; }
        public int ApprovalOrder { get; set; }

        public long PostTargetID { get; set; }
        public long PostID { get; set; }
        public long CredentialID { get; set; }
        public long AccountID { get; set; }
        public long SocialAppID { get; set; }
        public long SocialNetworkID { get; set; }

        public long? SyndicatorID { get; set; }
        public string SyndicatorName { get; set; }

        public long? SyndicateeFeatureTypeID { get; set; }
        public long? SyndicatorFeatureTypeID { get; set; }

        public long? PostAutoApprovalID { get; set; }
        public int? RejectionCount { get; set; }
        public DateTime? PostAutoApprovalDate { get; set; }

        public DateTime? DateApproved { get; set; }
        public DateTime? DateRejected { get; set; }
        public bool AutoApproved { get; set; }

        public long? UserID { get; set; }
        public long? PostApprovalGroupID { get; set; }
    }
}
