﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SI.BLL.Implementation;
using SI.BLL.Interface;

namespace SI.BLL
{
    public static class ProviderFactory
    {
        public static ILoggingProvider Logging
        {
            get
            {
                return new LoggingProvider();
            }
        }

        public static IListProvider Lists
        {
            get
            {
                return new ListProvider();
            }
        }

        public static ISecurityProvider Security
        {
            get
            {
                return new SecurityProvider();
            }
        }

        public static IThemeProvider Themes
        {
            get
            {
                return new ThemeProvider();
            }
        }

        public static IJobProvider Jobs
        {
            get
            {
                return new JobProvider();
            }
        }

        public static IReviewProvider Reviews
        {
            get
            {
                return new ReviewProvider();
            }
        }

        public static IValidatorProvider Validator
        {
            get
            {
                return new ValidatorProvider();
            }
        }

        public static IHarvesterProvider Harvester
        {
            get
            {
                return new HarvesterProvider();
            }
        }

        public static ISocialProvider Social
        {
            get
            {
                return new SocialProvider();
            }
        }

        public static IUtilProvider Util
        {
            get
            {
                return new UtilProvider();
            }
        }

		public static ITimezoneProvider TimeZones
        {
            get
            {
                return new UtilProvider();
            }
        }

        public static INotificationProvider Notifications
        {
            get
            {
                return new NotificationProvider();
            }
        }

        public static IPlatformNotification Platform
        {
            get
            {
                return new PlatformNotification();
            }
        }


        public static IProductsProvider Products
        {
            get
            {
                return new ProductsProvider();
            }
        }

        public static IRSSProvider RSS
        {
            get
            {
                return new RSSProvider();
            }
        }

        public static IReportsProvider Report
        {
            get
            {
                return new ReportsProvider();
            }
        }

        public static ISyndicatorImport SyndicatorImport
        {
            get
            {
                return new SyndicatorImport();
            }
        }
    }
}
