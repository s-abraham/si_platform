﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SI.BLL
{
    internal class ThemeProvider : IThemeProvider
    {
        public string GetThemeName(string url)
        {
            return Settings.GetSetting("site.defaulttheme");
        }
    }
}
