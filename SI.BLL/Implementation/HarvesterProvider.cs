﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SI.BLL.Interface;
using SI.DAL;
using SI.DTO;
using Connectors.URLHarvester;
using Connectors.URLHarvester.Parameters;

namespace SI.BLL.Implementation
{
    internal class HarvesterProvider : IHarvesterProvider
    {
        private static Dictionary<string, string> _processorSettingsCache = null;

        public HarvestorxPaths GetHarvesterSettings(ReviewSourceEnum reviewSource)
        {
            var result = new HarvestorxPaths();
            var namePrefix = "";

            #region Set Name Prefix
            switch (reviewSource)
            {
                case ReviewSourceEnum.Google:
                    namePrefix = "Google";
                    break;
                case ReviewSourceEnum.DealerRater:
                    namePrefix = "DealerRater";
                    break;
                case ReviewSourceEnum.Yahoo:
                    namePrefix = "Yahoo";
                    break;
                case ReviewSourceEnum.Yelp:
                    namePrefix = "Yelp";
                    break;
                case ReviewSourceEnum.CarDealer:
                    namePrefix = "CarDealer";
                    break;
                case ReviewSourceEnum.YellowPages:
                    namePrefix = "YellowPages";
                    break;
                case ReviewSourceEnum.Edmunds:
                    namePrefix = "Edmunds";
                    break;
                case ReviewSourceEnum.CarsDotCom:
                    namePrefix = "CarsDotCom";
                    break;
                case ReviewSourceEnum.CitySearch:
                    namePrefix = "CitySearch";
                    break;
                case ReviewSourceEnum.JudysBook:
                    namePrefix = "JudysBook";
                    break;
                case ReviewSourceEnum.InsiderPages:
                    namePrefix = "InsiderPages";
                    break;
                case ReviewSourceEnum.GoogleCA:
                    break;
                case ReviewSourceEnum.DealerRaterCA:
                    break;
                case ReviewSourceEnum.YelpCA:
                    break;
                case ReviewSourceEnum.YellowPagesCA:
                    break;
                case ReviewSourceEnum.JudysBookCA:
                    break;
                case ReviewSourceEnum.SureCritic:
                    break;
                case ReviewSourceEnum.BetterBusinessBureau:
                    break;
                case ReviewSourceEnum.BetterBusinessBureauCA:
                    break;
            }
            #endregion

            FillBaseReviewDownloaderXPaths(namePrefix, ref result);

            return result;
        }

        public void FillBaseReviewDownloaderXPaths(string namePrefix, ref HarvestorxPaths xPaths)
        {
            xPaths.QueryBasePath = GetProcessorSetting(string.Format("{0}.QueryBasePath", namePrefix));
            xPaths.QueryIterator = GetProcessorSetting(string.Format("{0}.QueryIterator", namePrefix));
            xPaths.QueryURL = GetProcessorSetting(string.Format("{0}.QueryURL", namePrefix));
            xPaths.GoogleSearch = GetProcessorSetting(string.Format("{0}.GoogleSearch", namePrefix));
            xPaths.QueryURLAttribute = GetProcessorSetting(string.Format("{0}.QueryURLAttribute", namePrefix));
            xPaths.QueryParentURL = GetProcessorSetting(string.Format("{0}.QueryParentURL", namePrefix));
        }

        private string GetProcessorSetting(string key)
        {
            if (_processorSettingsCache == null)
            {
                _processorSettingsCache = new Dictionary<string, string>();
                lock (_processorSettingsCache)
                {
                    using (SystemDBEntities db = ContextFactory.System)
                    {
                        List<ProcessorSetting> list = (from p in db.ProcessorSettings select p).ToList();
                        foreach (ProcessorSetting set in list)
                        {
                            _processorSettingsCache.Add(set.Name.Trim().ToLower(), set.Value);
                        }
                    }
                }
            }

            lock (_processorSettingsCache)
            {
                string value = null;
                _processorSettingsCache.TryGetValue(key.Trim().ToLower(), out value);
                return value;
            }
        }
    }

    public class HarvestorxPaths
    {
        public HarvestorxPaths(string queryBasePath, string queryIterator, string queryURL, string googleSearch, string queryURLAttribute, string queryParentURL)
        {
            QueryBasePath = queryBasePath;
            QueryIterator = queryIterator;
            QueryURL = queryURL;
            GoogleSearch = googleSearch;
            QueryURLAttribute = queryURLAttribute;
            QueryParentURL = queryParentURL;
        }
        public HarvestorxPaths()
        {
            QueryBasePath = "";
            QueryIterator = "";
            QueryURL = "";
            GoogleSearch = "";
            QueryURLAttribute = "";
            QueryParentURL = "";
        }

        public string QueryBasePath { get; set; }
        public string QueryIterator { get; set; }
        public string QueryURL { get; set; }
        public string GoogleSearch { get; set; }
        public string QueryURLAttribute { get; set; }
        public string QueryParentURL { get; set; }
    }
}