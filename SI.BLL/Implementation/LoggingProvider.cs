﻿using SI.DAL;
using SI.DTO;
using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Data.Objects;
using System.Diagnostics;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Data.Extensions;

namespace SI.BLL
{
    internal class LoggingProvider : ILoggingProvider
    {

        public void LogException(Exception ex, long jobID)
        {
            logException(ex, null, jobID);
        }

        public void LogException(Exception ex)
        {
            logException(ex, null, null);
        }

        public void LogException(Exception ex, string URL)
        {
            logException(ex, URL, null);
        }

        private void logException(Exception ex, string url, long? jobID)
        {
            StackTrace trace = new StackTrace(ex, true);
            StackFrame[] frames = trace.GetFrames();

            string assemblyName = Assembly.GetExecutingAssembly().GetName().FullName;
            if (Assembly.GetEntryAssembly() != null)
                assemblyName = Assembly.GetEntryAssembly().GetName().FullName;

            int lineNumber = 0;
            string methodName = "";
            string className = "";

            if (frames.Any())
            {
                lineNumber = frames[0].GetFileLineNumber();
                MethodBase mInfo = frames[0].GetMethod();
                if (mInfo != null)
                {
                    methodName = mInfo.Name;
                    if (mInfo.DeclaringType != null)
                    {
                        className = mInfo.DeclaringType.Name;
                    }
                }
            }

            ExceptionLog log = new ExceptionLog()
            {
                AssemblyName = assemblyName,
                Class = className,
                Method = methodName,
                LineNumber = lineNumber,
                Message = ex.Message,
                FullException = ex.ToString(),
                URL = url,
                JobID = jobID,
                DateCreated = DateTime.UtcNow
            };

            using (SystemDBEntities db = ContextFactory.System)
            {
                try
                {
                    db.ExceptionLogs.AddObject(log);
                    db.SaveChanges();
                }
                catch (Exception)
                {
                    //for now we'll ignore failed log attempts...
                    //todo: make this more robust
                }
            }
        }

        public void Audit(
            DTO.AuditLevelEnum level, DTO.AuditTypeEnum type,
            string message, object dataObject = null,
            UserInfoDTO userInfo = null, long? accountID = null,
            long? credentialID = null, long? account_ReviewSourceID = null,
            long? reviewID = null
        )
        {
            if (!string.IsNullOrWhiteSpace(message) && message.Length > 250)
            {
                message = message.Substring(0, 250);
            }

            string objectJson = null;
            if (dataObject != null)
            {
                objectJson = Newtonsoft.Json.JsonConvert.SerializeObject(dataObject);
            }
            if (userInfo == null)
                userInfo = new UserInfoDTO(0, null);

            Audit a = new Audit()
            {
                AuditLevelID = (long)level,
                AuditTypeID = (long)type,
                Message = message,
                Info = objectJson,
                DateCreated = DateTime.UtcNow,
                UserID = userInfo.LoggedInUser.ID.Value,
                ImpersonatedUserID = (userInfo.ImpersonatingUser == null) ? null : userInfo.ImpersonatingUser.ID,
                AccountID = accountID,
                CredentialID = credentialID,
                Account_ReviewSourceID = account_ReviewSourceID,
                ReviewID = reviewID
            };
            using (SystemDBEntities db = ContextFactory.System)
            {
                db.Audits.AddObject(a);
                db.SaveChanges();
            }
        }

        public List<ExceptionLogDTO> GetExceptions(int skip, int take, ref int totalCount)
        {
            List<ExceptionLogDTO> exceptionLogDtos = new List<ExceptionLogDTO>();

            using (SystemDBEntities db = ContextFactory.System)
            {
                totalCount = db.ExceptionLogs.Count();

                List<ExceptionLog> exLog = db.ExceptionLogs.OrderByDescending(e => e.DateCreated).Skip(skip).Take(take).ToList();

                if (exLog != null)
                {
                    foreach (ExceptionLog ex in exLog)
                    {
                        ExceptionLogDTO log = new ExceptionLogDTO();

                        log.ID = ex.ID;
                        log.AssemblyName = ex.AssemblyName;
                        log.Class = ex.Class;
                        log.DateCreated = ex.DateCreated;
                        log.FullException = ex.FullException;
                        log.LineNumber = ex.LineNumber.GetValueOrDefault();
                        log.Message = ex.Message;
                        log.Method = ex.Method;
                        log.URL = ex.URL;

                        exceptionLogDtos.Add(log);
                    }
                }
            }

            return exceptionLogDtos;
        }


        public List<AuditDTO> GetBatchOfAuditsToProcess()
        {
            List<AuditDTO> audits = new List<AuditDTO>();
            TimeZoneInfo timezone = TimeZoneInfo.Utc;

            using (SystemDBEntities db = ContextFactory.System)
            {
                if (db.Connection.State != System.Data.ConnectionState.Open)
                    db.Connection.Open();

                using (DbCommand cmd = db.CreateStoreCommand("spGetAuditsToProcess",  System.Data.CommandType.StoredProcedure, null))
                {
                    using (DbDataReader reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            audits.Add(new AuditDTO()
                            {
                                ID = Util.GetReaderValue<long>(reader, "ID", 0),
                                Level = (AuditLevelEnum)Util.GetReaderValue<long>(reader, "AuditLevelID", 0),
                                Type = (AuditTypeEnum)Util.GetReaderValue<long>(reader, "AuditTypeID", 0),
                                Message = Util.GetReaderValue<string>(reader, "Message", ""),
                                Info = Util.GetReaderValue<string>(reader, "Info", ""),
                                ProcessBatch = Util.GetReaderValue<Guid?>(reader, "ProcessBatch", null),
                                DateCreated = new SIDateTime(Util.GetReaderValue<DateTime>(reader, "DateCreated", DateTime.UtcNow), timezone),
                                DateProcessed = null,
                                UserID = Util.GetReaderValue<long>(reader, "UserID", 0),
                                ImpersonatedUserID = Util.GetReaderValue<long?>(reader, "ImpersonatedUserID", null),
                                AccountID = Util.GetReaderValue<long?>(reader, "AccountID", null),
                                CredentialID = Util.GetReaderValue<long?>(reader, "CredentialID", null),
                                Account_ReviewSourceID = Util.GetReaderValue<long?>(reader, "Account_ReviewSourceID", null),
                                ReviewID = Util.GetReaderValue<long?>(reader, "ReviewID", null)
                            });
                        }
                    }
                }
            }

            return audits;
        }


        public void SetAuditsAsProcessed(List<long> auditIDs)
        {
            if (auditIDs.Any())
            {
                string sql = string.Format("update audits set DateProcessed=getdate() where ID IN ({0})", string.Join(",", auditIDs.ToArray()));
                using (SystemDBEntities db = ContextFactory.System)
                {
                    db.ExecuteStoreCommand(sql, null);
                }
            }
        }


        public AuditHandlerBase GetAuditHandler(AuditTypeEnum type)
        {
            switch (type)
            {
                case AuditTypeEnum.General:
                    break;

                case AuditTypeEnum.SQLException:
                    break;
                case AuditTypeEnum.ReviewDownloaderSuccess:
                    break;
                case AuditTypeEnum.ReviewDownloaderFailure:
                    return new ReviewDownloaderFailureAuditHandler();

                case AuditTypeEnum.NewReviewDetected:
                    return new NewReviewDetectedAuditHandler();

                case AuditTypeEnum.PublishActivity:
                    return new PublishActivityAuditHandler();

                case AuditTypeEnum.PublishFailure:
                    return new PublishFailureAuditHandler();

                case AuditTypeEnum.PublishSuccess:
                    return new PublishSuccessAuditHandler();

                case AuditTypeEnum.AccountActivity:
                    return new AccountActivityAuditHandler();

                case AuditTypeEnum.UserActivity:
                    return new UserActivityAuditHandler();

                case AuditTypeEnum.QueuePublishJobFailure:
                    break;
                case AuditTypeEnum.FacebookDownloaderFailure:
                    break;
                case AuditTypeEnum.FacebookIntegratorFailure:
                    break;
                case AuditTypeEnum.SocialConfiguration:
                    break;
                case AuditTypeEnum.ControllerAction:
                    break;
                case AuditTypeEnum.ControllerException:
                    break;
                case AuditTypeEnum.JobFailure:
                    break;
                case AuditTypeEnum.ProcessorException:
                    break;
                case AuditTypeEnum.ReviewURLHarvestSucceeded:
                    break;
                case AuditTypeEnum.ReviewURLHarvestFailed:
                    break;
                case AuditTypeEnum.ReviewURLValidationSucceeded:
                    break;
                case AuditTypeEnum.ReviewURLValidationFailed:
                    break;
                case AuditTypeEnum.AccountConfiguration:
                    break;
                case AuditTypeEnum.NotificationService:
                    break;
                default:
                    break;
            }

            return null;
        }



    }
}
