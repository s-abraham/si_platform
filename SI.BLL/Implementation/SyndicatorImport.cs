﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SI.DAL;
using SI.DTO;
using System.IO;
using System.Data.Common;
using System.Data.SqlClient;
using Microsoft.Data.Extensions;
using System.Data.SqlClient;


namespace SI.BLL.Implementation
{
    internal class SyndicatorImport : ISyndicatorImport
    {
        public void ImportCARFAX(Guid UserID)
        {
            long CARFAXParentAccountID = 197922;
            UserInfoDTO userInfo = new UserInfoDTO(1, null); //system

            using (SocialDealerEntities db = ContextFactory.SocialDealer)
            {
                int iCountter = 0;
                foreach (var result in db.spGetDealerLocationListforMessageSyndicationEmail2(UserID)) // Enterprise Reputation Daily Summary Report (2)
                {
                    iCountter++;
                    SDLocation sdLocation = new SDLocation(false);
                    string Description = string.Empty;
                    Debug.WriteLine(iCountter + " : SD DealerLocation - " + result.Dealership);
                    long _UserID = 0;
                    long _SubscriptionID = 0;
                    long _CredentialID = 0;

                    try
                    {
                        #region Create Account
                        long siParentAccountID = CARFAXParentAccountID;

                        sdLocation.SocialDealerID = result.DealerLocationId;
                        sdLocation.GroupID = 0;
                        sdLocation.LocationName = result.Dealership;
                        sdLocation.Address = result.Address1;
                        sdLocation.City = result.CityName;
                        sdLocation.StateAbbreviation = result.StateAbbreviation;
                        sdLocation.ZipCode = result.ZipCode;

                        long id = importAccount(siParentAccountID, result.Email, result.FirstName, result.LastName, sdLocation);
                        sdLocation.SocialIntegrationID = id;

                        #endregion

                        #region Create User
                        CreateUserResultDTO saveuserdto = new CreateUserResultDTO();
                        if (sdLocation.SocialIntegrationID > 0)
                        {

                            //Check for the User
                            UserDTO dto = ProviderFactory.Security.GetUserByEmailAddress(result.Email, userInfo);

                            if (dto == null)
                            {
                                dto = ProviderFactory.Security.GetUserByUsername(result.Email, userInfo);
                            }

                            if (dto == null)
                            {
                                saveuserdto = AddUser(userInfo, result.Email, result.FirstName, result.LastName, sdLocation.SocialIntegrationID, sdLocation.SocialIntegrationID, (long)RoleTypeEnum.basic_admin, result.Email);

                                #region Audit Log
                                Auditor
                                  .New(AuditLevelEnum.Information, AuditTypeEnum.UserActivity, userInfo, "")
                                  .SetAuditDataObject(
                                      AuditUserActivity
                                      .New(userInfo, AuditUserActivityTypeEnum.CreatedNewUser)
                                          .SetAccountID(sdLocation.SocialIntegrationID)
                                          .SetUserIDActedUpon(saveuserdto.User.ID)
                                          .SetDescription("Created New User for Ally.")
                                  )
                                  .Save(ProviderFactory.Logging);
                                #endregion

                                if (saveuserdto.Outcome == CreateUserOutcome.Success)
                                {
                                    _UserID = saveuserdto.User.ID.Value;
                                    Description = "Created New User for Ally.";
                                    ProviderFactory.Security.AddRole(userInfo, saveuserdto.User.ID.Value, sdLocation.SocialIntegrationID, (long)RoleTypeEnum.basic_admin, false, true, true);
                                    #region Audit Log
                                    Auditor
                                      .New(AuditLevelEnum.Information, AuditTypeEnum.UserActivity, userInfo, "")
                                      .SetAuditDataObject(
                                          AuditUserActivity
                                          .New(userInfo, AuditUserActivityTypeEnum.CreatedNewUser)
                                              .SetAccountID(sdLocation.SocialIntegrationID)
                                              .SetUserIDActedUpon(saveuserdto.User.ID)
                                              .SetDescription("Created New Role for Ally User.")
                                      )
                                      .Save(ProviderFactory.Logging);
                                    #endregion

                                }
                                else
                                {
                                    _UserID = 0;
                                    Description = "Not able to Create New Role for Ally User.";
                                    #region Audit Log
                                    Auditor
                                      .New(AuditLevelEnum.Information, AuditTypeEnum.UserActivity, userInfo, "")
                                      .SetAuditDataObject(
                                          AuditUserActivity
                                          .New(userInfo, AuditUserActivityTypeEnum.CreatedNewUser)
                                              .SetAccountID(sdLocation.SocialIntegrationID)
                                              .SetDescription("Not able to Create New Role for Ally User. " + result.Email)
                                      )
                                      .Save(ProviderFactory.Logging);
                                    #endregion
                                }
                            }
                            else
                            {
                                _UserID = dto.ID.Value;
                                Description = "User exists for Ally.";
                                #region Audit Log
                                Auditor
                                  .New(AuditLevelEnum.Information, AuditTypeEnum.UserActivity, userInfo, "")
                                  .SetAuditDataObject(
                                      AuditUserActivity
                                      .New(userInfo, AuditUserActivityTypeEnum.CreatedNewUser)
                                          .SetAccountID(sdLocation.SocialIntegrationID)
                                          .SetUserIDActedUpon(dto.ID.Value)
                                          .SetDescription("User exists for Ally.")
                                  )
                                  .Save(ProviderFactory.Logging);
                                #endregion

                                ProviderFactory.Security.AddRole(userInfo, dto.ID.Value, sdLocation.SocialIntegrationID, (long)RoleTypeEnum.basic_admin, false, true, true);

                                #region Audit Log
                                Auditor
                                  .New(AuditLevelEnum.Information, AuditTypeEnum.UserActivity, userInfo, "")
                                  .SetAuditDataObject(
                                      AuditUserActivity
                                      .New(userInfo, AuditUserActivityTypeEnum.CreatedNewUser)
                                          .SetAccountID(sdLocation.SocialIntegrationID)
                                          .SetUserIDActedUpon(dto.ID.Value)
                                          .SetDescription("Created New Role for Ally User.")
                                  )
                                  .Save(ProviderFactory.Logging);
                                #endregion

                            }

                            using (MainEntities dbLog = ContextFactory.Main)
                            {
                                ImportLog log = new ImportLog();

                                log.AccountID = sdLocation.SocialIntegrationID;
                                log.DealerLocationID = result.DealerLocationId;
                                log.UserID = _UserID;
                                log.Description = Description;

                                dbLog.ImportLogs.AddObject(log);
                                dbLog.SaveChanges();
                            }

                        }
                        #endregion

                        #region Add Account Subscription

                        long Reseller_PackageID = 34; //Ally
                        //long Reseller_PackageID = 16; // CARFAX

                        if (sdLocation.SocialIntegrationID > 0)
                        {
                            using (MainEntities db2 = ContextFactory.Main)
                            {
                                Subscription sub = new Subscription();

                                sub = (from s in db2.Subscriptions
                                       where s.AccountID == sdLocation.SocialIntegrationID
                                            && s.Reseller_PackageID == Reseller_PackageID
                                       select s).FirstOrDefault();

                                if (sub == null)
                                {
                                    sub = new Subscription() { AccountID = sdLocation.SocialIntegrationID, Reseller_PackageID = Reseller_PackageID, StartDate = DateTime.UtcNow, EndDate = DateTime.UtcNow.AddYears(1), Active = true, DateCreated = DateTime.UtcNow };
                                    db2.Subscriptions.AddObject(sub);
                                    db2.SaveChanges();

                                    _SubscriptionID = sub.ID;
                                    Description = "Created New Subscription for Ally.";

                                    #region Audit Log

                                    Auditor
                                      .New(AuditLevelEnum.Information, AuditTypeEnum.UserActivity, userInfo, "")
                                      .SetAuditDataObject(
                                          AuditUserActivity
                                          .New(userInfo, AuditUserActivityTypeEnum.SubscriptionAdded)
                                              .SetAccountID(sdLocation.SocialIntegrationID)
                                              .SetUserIDActedUpon(_UserID)
                                              .SetSubscriptionID(sub.ID)
                                              .SetDescription("Created New Subscription for Ally.")
                                      )
                                      .Save(ProviderFactory.Logging);
                                    #endregion
                                }
                                else
                                {
                                    _SubscriptionID = sub.ID;
                                    Description = "Subscription exists for Ally.";

                                    #region Audit Log

                                    Auditor
                                      .New(AuditLevelEnum.Information, AuditTypeEnum.UserActivity, userInfo, "")
                                      .SetAuditDataObject(
                                          AuditUserActivity
                                          .New(userInfo, AuditUserActivityTypeEnum.SubscriptionAdded)
                                              .SetAccountID(sdLocation.SocialIntegrationID)
                                              .SetUserIDActedUpon(_UserID)
                                              .SetSubscriptionID(sub.ID)
                                              .SetDescription("Subscription exists for Ally.")
                                      )
                                      .Save(ProviderFactory.Logging);
                                    #endregion
                                }
                            }

                            using (MainEntities dbLog = ContextFactory.Main)
                            {
                                ImportLog log = new ImportLog();

                                log.AccountID = sdLocation.SocialIntegrationID;
                                log.DealerLocationID = result.DealerLocationId;
                                log.UserID = _UserID;
                                log.SubscriptionID = _SubscriptionID;
                                log.Description = Description;

                                dbLog.ImportLogs.AddObject(log);
                                dbLog.SaveChanges();
                            }
                        }
                        #endregion

                        #region Create Credential
                        long ResellerID = 22;  // Ally
                        //long ResellerID = 13; // CARFAX

                        if (sdLocation.SocialIntegrationID > 0)
                        {
                            List<DealerLocationSocialNetwork> credentials = (from dlsn in db.DealerLocationSocialNetworks
                                                                             where dlsn.LocationID == result.DealerLocationId
                                                                                   && dlsn.MainPage == true
                                                                                   && (dlsn.SocialNetworkID == 2 || dlsn.SocialNetworkID == 3)
                                                                                   && dlsn.AppKey != null
                                                                             select dlsn).ToList();
                            bool facebook = false;
                            bool twitter = false;

                            foreach (var credential in credentials)
                            {
                                long socialappid = 0;
                                Credential SIcredential = null;

                                using (MainEntities db1 = ContextFactory.Main)
                                {
                                    socialappid = (from c in db1.SocialApps where c.AppID == credential.AppKey && c.ResellerID == ResellerID select c.ID).FirstOrDefault();

                                    SIcredential = (from c in db1.Credentials
                                                    where c.UniqueID == credential.UniqueID
                                                            && c.AccountID == sdLocation.SocialIntegrationID
                                                            && c.IsActive == true
                                                    select c).FirstOrDefault();
                                }
                                if (socialappid > 0)
                                {
                                    if (SIcredential == null)
                                    {
                                        if (credential.SocialNetworkID == 2 && !facebook) //facebook
                                        {
                                            SaveFacebookConnection(userInfo, sdLocation.SocialIntegrationID, credential.UniqueID, credential.Token, credential.PageFriendlyName, credential.PictureURL, socialappid);

                                            #region Audit Log
                                            Auditor
                                                 .New(AuditLevelEnum.Information, AuditTypeEnum.UserActivity, userInfo, "")
                                                 .SetAuditDataObject(
                                                     AuditUserActivity
                                                     .New(userInfo, AuditUserActivityTypeEnum.AddedSocialConnection)
                                                         .SetCredentialID(credential.ID)
                                                         .SetDescription("Created New Credential for Ally. Facebook")
                                                 )
                                                 .Save(ProviderFactory.Logging)
                                             ;
                                            #endregion

                                            SocialCredentialDTO socialCredentialDTO = ProviderFactory.Social.GetSocialCredentials(SocialNetworkEnum.Facebook, sdLocation.SocialIntegrationID, true).FirstOrDefault();
                                            _CredentialID = socialCredentialDTO.ID;
                                            Description = "Created New Credential for Ally. Facebook";

                                            facebook = true;
                                        }
                                        if (credential.SocialNetworkID == 3 & !twitter) // Twitter
                                        {
                                            SaveTwitterConnection(userInfo, sdLocation.SocialIntegrationID, credential.UniqueID, credential.Token, credential.TokenSecret, credential.TwitterScreenName, credential.PictureURL, socialappid);

                                            #region Audit Log
                                            Auditor
                                                .New(AuditLevelEnum.Information, AuditTypeEnum.UserActivity, userInfo, "")
                                                .SetAuditDataObject(
                                                    AuditUserActivity
                                                    .New(userInfo, AuditUserActivityTypeEnum.AddedSocialConnection)
                                                        .SetCredentialID(credential.ID)
                                                        .SetDescription("Created New Credential for Ally. Twitter")
                                                )
                                                .Save(ProviderFactory.Logging)
                                            ;
                                            #endregion

                                            SocialCredentialDTO socialCredentialDTO = ProviderFactory.Social.GetSocialCredentials(SocialNetworkEnum.Twitter, sdLocation.SocialIntegrationID, true).FirstOrDefault();
                                            _CredentialID = socialCredentialDTO.ID;
                                            Description = "Created New Credential for Ally. Twitter";

                                            twitter = true;
                                        }
                                    }
                                    else
                                    {
                                        #region Audit Log

                                        Auditor
                                            .New(AuditLevelEnum.Information, AuditTypeEnum.UserActivity, userInfo, "")
                                            .SetAuditDataObject(
                                                AuditUserActivity
                                                .New(userInfo, AuditUserActivityTypeEnum.AddedSocialConnection)
                                                    .SetCredentialID(SIcredential.ID)
                                                    .SetDescription("Credential exists for Ally.")
                                            )
                                            .Save(ProviderFactory.Logging)
                                        ;
                                        #endregion

                                        _CredentialID = SIcredential.ID;
                                        Description = "Credential exists for Ally.";
                                    }
                                }
                                else
                                {
                                    int a = 10;
                                }

                                using (MainEntities dbLog = ContextFactory.Main)
                                {
                                    ImportLog log = new ImportLog();

                                    log.AccountID = sdLocation.SocialIntegrationID;
                                    log.DealerLocationID = result.DealerLocationId;
                                    log.UserID = _UserID;
                                    log.SubscriptionID = _SubscriptionID;
                                    log.CredentialID = _CredentialID;
                                    log.Description = Description;

                                    dbLog.ImportLogs.AddObject(log);
                                    dbLog.SaveChanges();
                                }
                            }
                        }
                        #endregion
                        /*
                        #region NotificationPendingSubscriptionChecks with AccountID
                        if (sdLocation.SocialIntegrationID > 0)
                        {
                            using (MainEntities db3 = ContextFactory.Main)
                            {
                                NotificationPendingSubscriptionCheck ntsc = new NotificationPendingSubscriptionCheck() { AccountID = sdLocation.SocialIntegrationID, UserID = null };
                                db3.NotificationPendingSubscriptionChecks.AddObject(ntsc);
                                db3.SaveChanges();

                                Debug.WriteLine("AccountID Added to NotificationPendingSubscriptionChecks:");
                            }
                        }
                        #endregion

                        #region NotificationPendingSubscriptionChecks with UserID
                        //if (sdLocation.SocialIntegrationID > 0)
                        //{
                        //    using (MainEntities db4 = ContextFactory.Main)
                        //    {
                        //        NotificationPendingSubscriptionCheck ntsc = new NotificationPendingSubscriptionCheck() { AccountID = null, UserID = saveuserdto.User.ID };
                        //        db4.NotificationPendingSubscriptionChecks.AddObject(ntsc);
                        //        db4.SaveChanges();

                        //        Debug.WriteLine("UserID Added to NotificationPendingSubscriptionChecks:");
                        //    }
                        //}
                        #endregion

                        */

                    }
                    catch (Exception ex)
                    {
                        Description = "Exception. " + result.Dealership + " " + result.Email;

                        using (MainEntities dbLog = ContextFactory.Main)
                        {
                            ImportLog log = new ImportLog();

                            log.AccountID = sdLocation.SocialIntegrationID;
                            log.DealerLocationID = result.DealerLocationId;
                            log.UserID = _UserID;
                            log.SubscriptionID = _SubscriptionID;
                            log.CredentialID = _CredentialID;
                            log.Description = Description;

                            dbLog.ImportLogs.AddObject(log);
                            dbLog.SaveChanges();
                        }

                    }
                }
            }
        }

        public void importRides()
        {
            long ridesParentAccountID = 220862;
            Dictionary<int, SDLocation> locations = readRidesLocations(@"C:\SocialDealer Docs\Rides_dealers_Add_2 - Sheet1.csv");

            foreach (SDLocation item in locations.Values)
            {
                try
                {

                    Console.WriteLine("importing location {0}", item.LocationName);

                    SDLocation sdLocation = new SDLocation(false);

                    sdLocation.SocialDealerID = item.SocialDealerID;
                    sdLocation.GroupID = 0;
                    sdLocation.LocationName = item.LocationName;
                    sdLocation.Address = item.Address;
                    sdLocation.City = item.City;
                    sdLocation.StateAbbreviation = item.StateAbbreviation;
                    sdLocation.ZipCode = item.ZipCode;

                    long id = importAccount(ridesParentAccountID, string.Empty, string.Empty, string.Empty, sdLocation);
                    sdLocation.SocialIntegrationID = id;

                    //Debug.WriteLine(item.SocialIntegrationID + "," + item.LocationName);
                    //Console.WriteLine(item.SocialIntegrationID + "," + item.LocationName);
                }
                catch (Exception ex)
                {
                    Debug.WriteLine("0," + item.LocationName);
                    Debug.WriteLine("0," + ex.InnerException);
                    Console.WriteLine("0," + item.LocationName);
                }
            }
        }
        /*
        public void importAllyFromExcel()
        {
            using (MainEntities db = ContextFactory.Main)
            {
                if (db.Connection.State != System.Data.ConnectionState.Open) db.Connection.Open();

                using (DbCommand cmd = db.CreateStoreCommand("spGetAllyDealerToImport", System.Data.CommandType.StoredProcedure))
                {
                    using (DbDataReader reader = cmd.ExecuteReader())
                    {
                        List<AllyLocation> AllyLocations = db.Translate<AllyLocation>(reader).ToList();
                        UserInfoDTO UserInfo = new UserInfoDTO(1, null); //system

                        int iCounter = 1;
                        foreach (var allyLocation in AllyLocations)
                        {
                            try
                            {
                                long? accountID = null;
                                string accountName = allyLocation.PDN_Name.Replace(", LLC", "")
                                                                        .Replace(", INC.", "")
                                                                        .Replace(" LLC", "")
                                                                        .Replace(", LLC", "")
                                                                        .Replace(" L.L.C.", "")
                                                                        .Replace(", L.P.", "")
                                                                        .Replace(" LTD", "")
                                                                        .Replace(" COMPANY", "")
                                                                        .Replace(", INC.", "")
                                                                        .Replace(", LP", "")
                                                                        .Replace(" CORPORATION", "")
                                                                        .Replace(" CO.", "")
                                                                        .Replace(",", "")
                                                                        .Replace(", INCORPORATED", "")
                                                                        .Replace(" INC.", "")
                                                                        .Replace(" INC", "")
                                                                        .Replace(" CORP.", "")
                                                                        .Replace(" L.P.", "")
                                                                        .Replace(" L.L.P.", "")
                                                                        .Replace(" CORP", "")
                                                                        .Replace(" GROUP", "")
                                                                        .Replace(" CO", "")
                                                                        .Replace(" LP", "")
                                                                        .Replace(" LLP", "");
                                accountName = accountName.Trim();
                                GetEntityDTO<AccountDTO> dto2 = ProviderFactory.Security.GetAccountByNameForImport(UserInfo, accountName);
                                string Description, Status;

                                
                                string ExcelZipCode = allyLocation.CUST_ZIP.Trim().ToLower();
                                string ExcelStreetAddress = allyLocation.CUST_ADDR1.Trim().ToLower();
                                string ExcelCity = allyLocation.CITY.Trim().ToLower();
                                string Excelstate = allyLocation.CUST_STATE_CD.Trim().ToLower();

                                if (dto2 != null)                                
                                {
                                    accountID = dto2.Entity.ID;

                                    string DBZipCode = dto2.Entity.PostalCode.Trim().ToLower();
                                    string DBStreetAddress = dto2.Entity.StreetAddress1.Trim().ToLower();
                                    string DBCity = dto2.Entity.CityName.Trim().ToLower();
                                    string DBstate = dto2.Entity.StateAbbreviation.Trim().ToLower();
                                    

                                    if (DBZipCode == ExcelZipCode)
                                    {                                        
                                        Status = "Perfect Match";
                                        Debug.WriteLine(iCounter + " " + Status + " , AccountID : " + accountID);                                        
                                    }
                                    else if (DBStreetAddress == ExcelStreetAddress && DBCity == ExcelCity && DBstate == Excelstate && DBZipCode == ExcelZipCode)
                                    {
                                        var franchise = GetFranchiseByLocationName(accountName);
                                        if (!string.IsNullOrEmpty(franchise))
                                        {
                                            Status = "Perfect Match";
                                            Debug.WriteLine(iCounter + " " + Status + " , AccountID : " + accountID);
                                        }
                                        else
                                        {
                                            Status = "Partial Match";
                                            Debug.WriteLine(iCounter + " " + Status + " , AccountID : " + accountID);
                                        }
                                    }
                                    else
                                    {
                                        Status = "Partial Match";
                                        Debug.WriteLine(iCounter + " " + Status + " , AccountID : " + accountID);
                                    }                                    
                                }
                                else
                                {                                      
                                    var result = (from a in db.Addresses
                                                  join c in db.Cities on a.CityID equals c.ID
                                                  join s in db.States on c.StateID equals s.ID
                                                  join z in db.ZipCodes on c.ZipCodeID equals z.ID
                                                  where a.Street1.Equals(ExcelStreetAddress, StringComparison.InvariantCultureIgnoreCase) &&
                                                        c.Name.Equals(ExcelCity, StringComparison.InvariantCultureIgnoreCase) &&
                                                        s.Abbreviation.Equals(Excelstate, StringComparison.InvariantCultureIgnoreCase) &&
                                                        z.ZipCode1.Equals(ExcelZipCode, StringComparison.InvariantCultureIgnoreCase)
                                                  select a.ID).ToList();

                                    if (result != null)
                                    {
                                        if (result.Count > 0)
                                        {
                                            var franchise = GetFranchiseByLocationName(accountName);
                                            if (!string.IsNullOrEmpty(franchise))
                                            {
                                                Status = "Partial Match";
                                                Debug.WriteLine(iCounter + " " + Status + " , AccountID : " + accountID);
                                            }
                                            else
                                            {
                                                Status = "No Match";
                                                Debug.WriteLine(iCounter + " " + Status + " , AccountID : " + accountID);
                                            }
                                        }
                                        else
                                        {
                                            accountID = null;
                                            Status = "No Match";
                                            Debug.WriteLine(iCounter + " " + Status);
                                        }
                                    }
                                    else
                                    {
                                        accountID = null;                                    
                                        Status = "No Match";
                                        Debug.WriteLine(iCounter + " " + Status);
                                    }

                                }
                                //else
                                //{
                                //    accountID = null;                                    
                                //    Status = "No Match";
                                //    Debug.WriteLine(iCounter + " " + Status);
                                //}

                                using (MainEntities dbLog = ContextFactory.Main)
                                {
                                    AllyImportLog log = new AllyImportLog();

                                    log.AccountID = accountID;
                                    log.PDNID = allyLocation.PDN;
                                    log.Status = Status;

                                    dbLog.AllyImportLogs.AddObject(log);
                                    dbLog.SaveChanges();
                                }

                                iCounter++;
                            }
                            catch (Exception ex)
                            {
                                Debug.WriteLine(iCounter + " Exception.");
                            }
                        }
                    }
                }
            }
        }
        */

        public void importAllyFromExcel()
        {
            using (MainEntities db = ContextFactory.Main)
            {
                if (db.Connection.State != System.Data.ConnectionState.Open) db.Connection.Open();

                using (DbCommand cmd = db.CreateStoreCommand("spGetAllyDealerToImport", System.Data.CommandType.StoredProcedure))
                {
                    using (DbDataReader reader = cmd.ExecuteReader())
                    {
                        List<AllyLocation> AllyLocations = db.Translate<AllyLocation>(reader).ToList();
                        UserInfoDTO UserInfo = new UserInfoDTO(1, null); //system

                        int iCounter = 0;
                        int userCounter = 0;
                        int run = 3;
                        foreach (var allyLocation in AllyLocations)
                        {
                            iCounter++;
                            Console.WriteLine("Processing " + iCounter);

                            try
                            {
                                // Check the Street Address in Addresses Table 
                                string ExcelZipCode = allyLocation.CUST_ZIP.Trim().ToLower();
                                string ExcelStreetAddress = allyLocation.CUST_ADDR1.Trim().ToLower();
                                string ExcelCity = allyLocation.CITY.Trim().ToLower();
                                string Excelstate = allyLocation.CUST_STATE_CD.Trim().ToLower();


                                var streetAddMatch = (from a in db.Addresses
                                                      where a.Street1.Equals(ExcelStreetAddress, StringComparison.InvariantCultureIgnoreCase)
                                                      select a).ToList();

                                string status = string.Empty;
                                if (streetAddMatch != null)
                                {
                                    if (streetAddMatch.Count > 1)
                                    {
                                        status = "More then one Addresses Found for Street Add: " + ExcelStreetAddress + ", AddressIDs: " + string.Join("|", streetAddMatch.Select(s => s.ID).ToArray());
                                        #region Log
                                        using (MainEntities dbLog = ContextFactory.Main)
                                        {
                                            AllyImportLog log = new AllyImportLog();

                                            log.PDNID = allyLocation.PDN;
                                            log.Status = status;
                                            log.Run = run;

                                            dbLog.AllyImportLogs.AddObject(log);
                                            dbLog.SaveChanges();
                                        }
                                        #endregion
                                    }
                                    else if (streetAddMatch.Count == 1)
                                    {
                                        long BillingAddressID = streetAddMatch.Select(s => s.ID).FirstOrDefault();

                                        //Get the AccountID by AddressID
                                        var account = (from a in db.Accounts
                                                       where a.BillingAddressID == BillingAddressID
                                                       select a).FirstOrDefault();

                                        if (account != null)
                                        {
                                            long AccountID = account.ID;

                                            // Check the Account Under Edmunds Import then move to SocialDealer
                                            if (account.ParentAccountID == 220171)
                                            {
                                                using (MainEntities db1 = ContextFactory.Main)
                                                {
                                                    var _account = db1.Accounts.Where(c => c.ID == AccountID).FirstOrDefault();

                                                    if (_account != null)
                                                    {
                                                        _account.ParentAccountID = 197922;

                                                        db1.SaveChanges();
                                                    }
                                                }
                                                status = "Account Moved to SocialDealer from Edmunds Import. AccountID: " + AccountID;

                                                #region Log
                                                using (MainEntities dbLog = ContextFactory.Main)
                                                {
                                                    AllyImportLog log = new AllyImportLog();

                                                    log.PDNID = allyLocation.PDN;
                                                    log.AccountID = AccountID;
                                                    log.Status = status;
                                                    log.Run = run;

                                                    dbLog.AllyImportLogs.AddObject(log);
                                                    dbLog.SaveChanges();
                                                }
                                                #endregion
                                            }

                                            //Ally Syndication Presale (Reseller_PackageID ID:42)
                                            #region Add Account Subscription

                                            long Reseller_PackageID = 42; //Ally Syndication Presale
                                            long _SubscriptionID = 0;

                                            if (AccountID > 0)
                                            {
                                                using (MainEntities db2 = ContextFactory.Main)
                                                {
                                                    Subscription sub = new Subscription();

                                                    sub = (from s in db2.Subscriptions
                                                           where s.AccountID == AccountID
                                                               && s.Reseller_PackageID == Reseller_PackageID
                                                           select s).FirstOrDefault();

                                                    if (sub == null)
                                                    {
                                                        sub = new Subscription() { AccountID = AccountID, Reseller_PackageID = Reseller_PackageID, StartDate = DateTime.UtcNow, EndDate = DateTime.UtcNow.AddYears(1), Active = true, DateCreated = DateTime.UtcNow };
                                                        db2.Subscriptions.AddObject(sub);
                                                        db2.SaveChanges();

                                                        _SubscriptionID = sub.ID;
                                                        status = "Created New Subscription for AccountID: " + AccountID + " , SubscriptionID: " + _SubscriptionID;
                                                    }
                                                    else
                                                    {
                                                        _SubscriptionID = sub.ID;
                                                        status = "Subscription exists for AccountID: " + AccountID + " , SubscriptionID: " + _SubscriptionID;
                                                    }

                                                    #region Log
                                                    using (MainEntities dbLog = ContextFactory.Main)
                                                    {
                                                        AllyImportLog log = new AllyImportLog();

                                                        log.PDNID = allyLocation.PDN;
                                                        log.AccountID = AccountID;
                                                        log.SubscriptionID = _SubscriptionID;
                                                        log.Status = status;
                                                        log.Run = run;

                                                        dbLog.AllyImportLogs.AddObject(log);
                                                        dbLog.SaveChanges();
                                                    }
                                                    #endregion
                                                }
                                            }
                                            #endregion

                                            //Get the All the USer from from Execel sheet "Tab1" (AllyCombinedData)
                                            var excelUsers = (from a in db.AllyCombinedDatas
                                                              where a.PDN == allyLocation.PDN
                                                              select a).ToList();

                                            if (excelUsers != null)
                                            {
                                                status = "Found " + excelUsers.Count + " User In Excel for PDN: " + allyLocation.PDN;
                                                #region Log
                                                using (MainEntities dbLog = ContextFactory.Main)
                                                {
                                                    AllyImportLog log = new AllyImportLog();

                                                    log.PDNID = allyLocation.PDN;
                                                    log.AccountID = AccountID;
                                                    log.SubscriptionID = _SubscriptionID;
                                                    log.Status = status;
                                                    log.Run = run;

                                                    dbLog.AllyImportLogs.AddObject(log);
                                                    dbLog.SaveChanges();
                                                }
                                                #endregion

                                                foreach (var excelUser in excelUsers)
                                                {
                                                    userCounter++;
                                                    try
                                                    {

                                                        long _UserID = 0;

                                                        #region Create User
                                                        CreateUserResultDTO saveuserdto = new CreateUserResultDTO();
                                                        if (AccountID > 0)
                                                        {

                                                            //Check for the User
                                                            UserDTO dto = ProviderFactory.Security.GetUserByEmailAddress(excelUser.Email.Trim(), UserInfo);

                                                            if (dto == null)
                                                            {
                                                                dto = ProviderFactory.Security.GetUserByUsername(excelUser.Email.Trim(), UserInfo);
                                                            }

                                                            if (dto == null)
                                                            {
                                                                saveuserdto = AddUser(UserInfo, excelUser.Email.Trim(), excelUser.FirstName.Trim(), excelUser.LastName.Trim(), AccountID, AccountID, (long)RoleTypeEnum.basic_admin, excelUser.Email.Trim());


                                                                if (saveuserdto.Outcome == CreateUserOutcome.Success)
                                                                {
                                                                    _UserID = saveuserdto.User.ID.Value;
                                                                    status = "Created New User, UserID: " + _UserID;
                                                                    #region Log
                                                                    using (MainEntities dbLog = ContextFactory.Main)
                                                                    {
                                                                        AllyImportLog log = new AllyImportLog();

                                                                        log.PDNID = allyLocation.PDN;
                                                                        log.AccountID = AccountID;
                                                                        log.SubscriptionID = _SubscriptionID;
                                                                        log.UserID = _UserID;
                                                                        log.Status = status;
                                                                        log.Run = run;

                                                                        dbLog.AllyImportLogs.AddObject(log);
                                                                        dbLog.SaveChanges();
                                                                    }
                                                                    #endregion

                                                                    ProviderFactory.Security.AddRole(UserInfo, _UserID, AccountID, (long)RoleTypeEnum.basic_admin, false, true, false);
                                                                    status = "Created New Role for UserID: " + _UserID;

                                                                    #region Log
                                                                    using (MainEntities dbLog = ContextFactory.Main)
                                                                    {
                                                                        AllyImportLog log = new AllyImportLog();

                                                                        log.PDNID = allyLocation.PDN;
                                                                        log.AccountID = AccountID;
                                                                        log.SubscriptionID = _SubscriptionID;
                                                                        log.UserID = _UserID;
                                                                        log.Status = status;
                                                                        log.Run = run;

                                                                        dbLog.AllyImportLogs.AddObject(log);
                                                                        dbLog.SaveChanges();
                                                                    }
                                                                    #endregion
                                                                }
                                                                else
                                                                {
                                                                    _UserID = 0;
                                                                    status = "Not able to Create User and Role.";
                                                                    #region Log
                                                                    using (MainEntities dbLog = ContextFactory.Main)
                                                                    {
                                                                        AllyImportLog log = new AllyImportLog();

                                                                        log.PDNID = allyLocation.PDN;
                                                                        log.AccountID = AccountID;
                                                                        log.SubscriptionID = _SubscriptionID;
                                                                        log.Status = status;
                                                                        log.Run = run;

                                                                        dbLog.AllyImportLogs.AddObject(log);
                                                                        dbLog.SaveChanges();
                                                                    }
                                                                    #endregion
                                                                }
                                                            }
                                                            else
                                                            {
                                                                _UserID = dto.ID.Value;
                                                                status = "User exists for UserID: " + _UserID;
                                                                #region Log
                                                                using (MainEntities dbLog = ContextFactory.Main)
                                                                {
                                                                    AllyImportLog log = new AllyImportLog();

                                                                    log.PDNID = allyLocation.PDN;
                                                                    log.AccountID = AccountID;
                                                                    log.SubscriptionID = _SubscriptionID;
                                                                    log.UserID = _UserID;
                                                                    log.Status = status;
                                                                    log.Run = run;

                                                                    dbLog.AllyImportLogs.AddObject(log);
                                                                    dbLog.SaveChanges();
                                                                }
                                                                #endregion

                                                                ProviderFactory.Security.AddRole(UserInfo, dto.ID.Value, AccountID, (long)RoleTypeEnum.basic_admin, false, true, false);
                                                                status = "Created New Role for Ally exists UserID: " + _UserID;

                                                                #region Log
                                                                using (MainEntities dbLog = ContextFactory.Main)
                                                                {
                                                                    AllyImportLog log = new AllyImportLog();

                                                                    log.PDNID = allyLocation.PDN;
                                                                    log.AccountID = AccountID;
                                                                    log.SubscriptionID = _SubscriptionID;
                                                                    log.UserID = _UserID;
                                                                    log.Status = status;
                                                                    log.Run = run;

                                                                    dbLog.AllyImportLogs.AddObject(log);
                                                                    dbLog.SaveChanges();
                                                                }
                                                                #endregion


                                                            }


                                                        }
                                                        #endregion

                                                        if (userCounter >= 1000)
                                                        {
                                                            ProviderFactory.Security.ApplyRolesAndReloadTree();
                                                            userCounter = 0;
                                                        }
                                                    }
                                                    catch (Exception ex)
                                                    {
                                                        #region Log
                                                        using (MainEntities dbLog = ContextFactory.Main)
                                                        {
                                                            AllyImportLog log = new AllyImportLog();

                                                            log.PDNID = allyLocation.PDN;
                                                            log.Status = "Exception In Creating User";
                                                            log.Run = run;

                                                            dbLog.AllyImportLogs.AddObject(log);
                                                            dbLog.SaveChanges();
                                                        }
                                                        #endregion
                                                    }

                                                }
                                            }
                                            else
                                            {
                                                status = "No User Found In Excel for PDN: " + allyLocation.PDN;

                                                #region Log
                                                using (MainEntities dbLog = ContextFactory.Main)
                                                {
                                                    AllyImportLog log = new AllyImportLog();

                                                    log.PDNID = allyLocation.PDN;
                                                    log.AccountID = AccountID;
                                                    log.SubscriptionID = _SubscriptionID;
                                                    log.Status = status;
                                                    log.Run = run;

                                                    dbLog.AllyImportLogs.AddObject(log);
                                                    dbLog.SaveChanges();
                                                }
                                                #endregion
                                            }

                                        }
                                        else
                                        {
                                            status = "Not Able to find Account PDN: " + allyLocation.PDN + ", BillingAddressID: " + BillingAddressID;

                                            #region Log
                                            using (MainEntities dbLog = ContextFactory.Main)
                                            {
                                                AllyImportLog log = new AllyImportLog();

                                                log.PDNID = allyLocation.PDN;
                                                log.Status = status;
                                                log.Run = run;

                                                dbLog.AllyImportLogs.AddObject(log);
                                                dbLog.SaveChanges();
                                            }
                                            #endregion

                                        }

                                    }
                                }
                            }
                            catch (Exception ex)
                            {
                                #region Log
                                using (MainEntities dbLog = ContextFactory.Main)
                                {
                                    AllyImportLog log = new AllyImportLog();

                                    log.PDNID = allyLocation.PDN;
                                    log.Status = "Exception";
                                    log.Run = run;

                                    dbLog.AllyImportLogs.AddObject(log);
                                    dbLog.SaveChanges();
                                }
                                #endregion
                            }
                        }
                        ProviderFactory.Security.ApplyRolesAndReloadTree();                        
                    }

                }

            }
        }

        public void importAllyUsersFromExcel_SPN_824()
        {
            long AccountID = 222864;

            using (MainEntities db = ContextFactory.Main)
            {
                if (db.Connection.State != System.Data.ConnectionState.Open) db.Connection.Open();

                #region Create User
                UserInfoDTO UserInfo = new UserInfoDTO(1, null); //system

                CreateUserResultDTO saveuserdto = new CreateUserResultDTO();
                if (AccountID > 0)
                {

                    List<AllyUSer> allyUSers = GetAllyUSerFromExcel();
                    foreach (var allyUSer in allyUSers)
                    {
                        string Email = allyUSer.Email;
                        string FirstName = allyUSer.FirstName;
                        string LastName = allyUSer.LastName;
                        long _UserID = 0;
                        string status = string.Empty;

                        //Check for the User
                        UserDTO dto = ProviderFactory.Security.GetUserByEmailAddress(Email.Trim(), UserInfo);

                        if (dto == null)
                        {
                            dto = ProviderFactory.Security.GetUserByUsername(Email.Trim(), UserInfo);
                        }

                        if (dto == null)
                        {
                            saveuserdto = AddUser(UserInfo, Email.Trim(), FirstName.Trim(), LastName.Trim(), AccountID, AccountID, (long)RoleTypeEnum.basic_admin, Email.Trim());


                            if (saveuserdto.Outcome == CreateUserOutcome.Success)
                            {
                                _UserID = saveuserdto.User.ID.Value;
                                status = "Created New User, UserID: " + _UserID;

                                ProviderFactory.Security.AddRole(UserInfo, _UserID, AccountID, (long)RoleTypeEnum.basic_admin, false, true, false);

                                status = status + " & Created New Role for UserID: " + _UserID;

                            }
                            else
                            {
                                _UserID = 0;
                                status = "Not able to Create User and Role. Email:" + Email;
                            }
                        }
                        else
                        {
                            _UserID = dto.ID.Value;
                            status = "User exists for UserID: " + _UserID;

                            ProviderFactory.Security.AddRole(UserInfo, dto.ID.Value, AccountID, (long)RoleTypeEnum.basic_admin, false, true, false);
                            status = status + " & Created New Role for Ally exists UserID: " + _UserID;
                        }
                        Console.WriteLine("--------------------------------------------------------------------");
                        Console.WriteLine(status);

                        Debug.WriteLine("--------------------------------------------------------------------");
                        Debug.WriteLine(status);
                    }
                   

                    
                }
                #endregion
            }
        }


        private CreateUserResultDTO AddUser(UserInfoDTO userInfo, string EmailAddress, string FirstName, string LastName, long RootAccountID, long MemberOfAccountID, long RoleTypeID, string Username)
        {
            CreateUserResultDTO saveuserdto = new CreateUserResultDTO();
            UserDTO user = new UserDTO();

            user.EmailAddress = EmailAddress;
            user.FirstName = FirstName;
            user.LastName = LastName;
            user.RootAccountID = RootAccountID;
            user.MemberOfAccountID = MemberOfAccountID;
            user.RoleTypeID = RoleTypeID;
            user.SuperAdmin = false;
            user.Username = Username;
            user.CascadeRole = false;
            user.Password = "Password$";

            saveuserdto = ProviderFactory.Security.SaveUser(user, userInfo, true, true);

            //if (saveuserdto.Outcome == CreateUserOutcome.Success)
            //{
            //    ProviderFactory.Security.AddRole(userInfo, saveuserdto.User.ID.Value, RootAccountID, user.RoleTypeID.Value, false);
            //}
            //else if (saveuserdto.Outcome != CreateUserOutcome.Success)
            //{
            //    switch (saveuserdto.Outcome)
            //    {
            //        case CreateUserOutcome.EmailAddressAlreadyExists:
            //            ProviderFactory.Security.AddRole(userInfo, saveuserdto.User.ID.Value, RootAccountID, user.RoleTypeID.Value, false);
            //            break;
            //    }
            //}

            return saveuserdto;

        }

        private bool SaveTwitterConnection(UserInfoDTO userInfo, long accountId, string UniqueID, string token, string tokenSecret, string screenName, string pictureURL, long socialappid)
        {
            if (!string.IsNullOrEmpty(token))
            {
                //long credentialId = 0;
                //result = ProviderFactory.Social.SaveSocialConnection(userInfo, credentialId, accountId, socialappid, token, tokenSecret, UniqueID, URL, pictureURL, screenName);

                string URL = string.Format("http://Twitter.com/{0}", screenName);

                SocialConnectionInfoDTO dto = new SocialConnectionInfoDTO()
                {
                    AccountID = accountId,
                    SocialNetwork = SocialNetworkEnum.Twitter,
                    PictureURL = pictureURL,
                    ScreenName = screenName,
                    Token = token,
                    TokenSecret = tokenSecret,
                    URI = URL,
                    UniqueID = UniqueID,
					SocialAppID = socialappid
                };

                SaveEntityDTO<SocialConnectionInfoDTO> result = ProviderFactory.Social.Save(new SaveEntityDTO<SocialConnectionInfoDTO>(dto, userInfo));
                return (!result.Problems.Any());

            }

            return false;
        }

        private bool SaveFacebookConnection(UserInfoDTO userInfo, long accountId, string UniqueID, string token, string screenName, string pictureURL, long socialappid)
        {

            //bool result = false;

            string URL = string.Format("http://www.facebook.com/home.php?#!/profile.php?id={0}", UniqueID);

            //long credentialId = 0;
            //result = ProviderFactory.Social.SaveSocialConnection(userInfo, credentialId, accountId, socialappid, token, string.Empty, UniqueID, URL, pictureURL, screenName);

            SocialConnectionInfoDTO dto = new SocialConnectionInfoDTO()
            {
                AccountID = accountId,
                SocialNetwork = SocialNetworkEnum.Facebook,
                PictureURL = pictureURL,
                ScreenName = screenName,
                Token = token,
                TokenSecret = string.Empty,
                URI = URL,
                UniqueID = UniqueID,
				SocialAppID = socialappid
            };

            SaveEntityDTO<SocialConnectionInfoDTO> result = ProviderFactory.Social.Save(new SaveEntityDTO<SocialConnectionInfoDTO>(dto, userInfo));
            return (!result.Problems.Any());

            //return result;
        }

        private long importAccount(long parentAccountID, string Email, string FirstName, string LastName, SDLocation loc)
        {
            long? accountID = null;

            UserInfoDTO UserInfo = new UserInfoDTO(1, null); //system

            string originID = string.Format("Rides.{0}", loc.SocialDealerID);

            AccountDTO account = null;
            string zip = loc.ZipCode.Replace(" ", "").Trim();

            account = new AccountDTO()
            {
                Name = loc.LocationName,
                DisplayName = loc.LocationName,
                OriginSystemIdentifier = originID,
                CountryID = 41,
                CityName = loc.City,
                PostalCode = zip,
                ParentAccountID = parentAccountID,
                StateAbbreviation = loc.StateAbbreviation,
                StreetAddress1 = loc.Address,
                TimezoneID = 11, //central
                Type = AccountTypeEnum.Company,
                AccountTypeID = (long)AccountTypeEnum.Company
            };

            GetEntityDTO<AccountDTO> dto2 = ProviderFactory.Security.GetAccountByNameForImport(UserInfo, account.Name);

            using (MainEntities db5 = ContextFactory.Main)
            {
                CARFAX_Import carfax = new CARFAX_Import();

                carfax.SDLocationID = (int)loc.SocialDealerID;
                carfax.SDName = loc.LocationName;
                carfax.SDEmail = Email;
                carfax.SDFirstName = FirstName;
                carfax.SDLastName = LastName;

                if (dto2 != null)
                {
                    carfax.SIAccountID = dto2.Entity.ID;
                    carfax.SIAccountDisplayName = dto2.Entity.DisplayName;
                    carfax.SIAccountName = dto2.Entity.Name;
                    carfax.SIParentAccountID = dto2.Entity.ParentAccountID;
                    carfax.SIParentAccountName = dto2.Entity.ParentAccountName;
                    carfax.SIResellerAccountID = dto2.Entity.ResellerForThisAccount.AccountID;
                }

                db5.CARFAX_Import.AddObject(carfax);
                db5.SaveChanges();
            }
            string Description = string.Empty;

            if (dto2 != null)
            {
                accountID = dto2.Entity.ID;

                #region Audit Log
                Auditor
                  .New(AuditLevelEnum.Information, AuditTypeEnum.UserActivity, UserInfo, "")
                  .SetAuditDataObject(
                      AuditUserActivity
                      .New(UserInfo, AuditUserActivityTypeEnum.CreatedNewAccount)
                      .SetAccountID(accountID)
                      .SetObjectID(accountID)
                      .SetDescription("Account All Ready Exists for Rides. " + accountID)
                  )
                  .Save(ProviderFactory.Logging)
                ;
                #endregion

                Description = "Account Exists for Rides.";
            }
            else
            {
                SaveEntityDTO<AccountDTO> result = ProviderFactory.Security.Save(new SaveEntityDTO<AccountDTO>(account, UserInfo));
                if (result.Problems.Count() > 0)
                {

                    #region Audit Log
                    Auditor
                      .New(AuditLevelEnum.Information, AuditTypeEnum.UserActivity, UserInfo, "")
                      .SetAuditDataObject(
                          AuditUserActivity
                          .New(UserInfo, AuditUserActivityTypeEnum.CreatedNewAccount)
                              .SetDescription("Not Able to Create Account for Rides. " + account.Name)
                      )
                      .Save(ProviderFactory.Logging)
                  ;
                    #endregion

                    Description = "Not Able to Create Account for Rides.";
                    return 0;
                }
                else
                {
                    //account = result.Entity;
                    accountID = result.Entity.ID;

                    #region Audit Log
                    Auditor
                      .New(AuditLevelEnum.Information, AuditTypeEnum.UserActivity, UserInfo, "")
                      .SetAuditDataObject(
                          AuditUserActivity
                          .New(UserInfo, AuditUserActivityTypeEnum.CreatedNewAccount)
                              .SetAccountID(accountID)
                              .SetObjectID(accountID)
                              .SetDescription("Created New Account for Rides. " + account.Name)
                      )
                      .Save(ProviderFactory.Logging)
                  ;
                    #endregion

                    Description = "Created New Account for Rides.";
                }
            }


            using (MainEntities dbLog = ContextFactory.Main)
            {
                ImportLog log = new ImportLog();

                log.AccountID = accountID;
                log.DealerLocationID = (int)loc.SocialDealerID;
                log.Description = Description;

                dbLog.ImportLogs.AddObject(log);
                dbLog.SaveChanges();
            }

            return accountID.Value;
            //return 0;
        }

        private static Dictionary<int, SDLocation> readRidesLocations(string filename)
        {
            Dictionary<int, SDLocation> locations = new Dictionary<int, SDLocation>();

            using (StreamReader sReader = new StreamReader(filename))
            {
                using (LumenWorks.Framework.IO.Csv.CsvReader csv = new LumenWorks.Framework.IO.Csv.CsvReader(sReader, true))
                {
                    string[] head = csv.GetFieldHeaders();
                    Dictionary<string, int> headers = new Dictionary<string, int>();
                    for (int i = 0; i < head.Length; i++)
                    {
                        headers.Add(head[i].Trim().ToLower(), i);
                    }

                    int locationID = 98;
                    while (csv.ReadNextRecord())
                    {
                        locationID++;

                        SDLocation loc = null;
                        if (!locations.TryGetValue(locationID, out loc))
                        {
                            loc = new SDLocation(false);

                            loc.SocialDealerID = locationID;
                            loc.GroupID = 0;

                            loc.Address = csv[headers["address"]];
                            loc.City = csv[headers["city"]];
                            loc.StateAbbreviation = csv[headers["state/province"]];
                            loc.ZipCode = csv[headers["postal code"]];
                            loc.LocationName = csv[headers["dealer name"]];

                            locations.Add(locationID, loc);
                        }
                        /*
                        if (!string.IsNullOrWhiteSpace(csv[headers["google"]]))
                        {
                            SDReviewURL url = new SDReviewURL()
                            {
                                ReviewSource = SDReviewSource.Google,
                                URL = csv[headers["google"]],
                                RequestURL = null,
                                LookupID = null
                            };
                            loc.ReviewURLs.Add(url);
                        }
                        if (!string.IsNullOrWhiteSpace(csv[headers["yellowpages.ca"]]))
                        {
                            SDReviewURL url = new SDReviewURL()
                            {
                                ReviewSource = SDReviewSource.YellowPages,
                                URL = csv[headers["google"]],
                                RequestURL = null,
                                LookupID = null
                            };
                            loc.ReviewURLs.Add(url);
                        }
                        if (!string.IsNullOrWhiteSpace(csv[headers["yelp"]]))
                        {
                            SDReviewURL url = new SDReviewURL()
                            {
                                ReviewSource = SDReviewSource.Yelp,
                                URL = csv[headers["yelp"]],
                                RequestURL = null,
                                LookupID = null
                            };
                            loc.ReviewURLs.Add(url);
                        }
                        if (!string.IsNullOrWhiteSpace(csv[headers["dealerrater.ca"]]))
                        {
                            SDReviewURL url = new SDReviewURL()
                            {
                                ReviewSource = SDReviewSource.DealerRater,
                                URL = csv[headers["dealerrater.ca"]],
                                RequestURL = null,
                                LookupID = null
                            };
                            loc.ReviewURLs.Add(url);
                        }
                        */

                    }

                }
            }

            return locations;

        }

        private string GetFranchiseByLocationName(string Name)
        {
            var franchiseList = FranchiseList();
            var franchise = string.Empty;

            foreach (var item in franchiseList)
            {
                if (Name.Trim().ToLower().Contains(item.ToLower()))
                {
                    franchise = item;
                    break;
                }
            }

            return franchise;
        }

        private List<string> FranchiseList()
        {
            var franchiseList = new List<string>();
            franchiseList.Add("Chevrolet");
            franchiseList.Add("GMC");
            franchiseList.Add("Ford");
            franchiseList.Add("Toyota");
            franchiseList.Add("Honda");
            franchiseList.Add("Buick");
            franchiseList.Add("Jeep");
            franchiseList.Add("Dodge");
            franchiseList.Add("Chrysler");
            franchiseList.Add("Cadillac");
            franchiseList.Add("Nissan");
            franchiseList.Add("Hyundai");
            franchiseList.Add("Mazda");
            franchiseList.Add("Kia");
            franchiseList.Add("Infiniti");
            franchiseList.Add("Volvo");
            franchiseList.Add("Scion");
            franchiseList.Add("BMW");
            franchiseList.Add("Jaguar");
            franchiseList.Add("Subaru");
            franchiseList.Add("Lincoln");
            franchiseList.Add("Acura");
            franchiseList.Add("Volkswagen");
            franchiseList.Add("Mercedes-Benz");
            franchiseList.Add("Lexus");
            franchiseList.Add("Audi");
            franchiseList.Add("Porsche");
            franchiseList.Add("Ram");
            franchiseList.Add("Ferrari");
            franchiseList.Add("Fiat");
            franchiseList.Add("Suzuki");
            franchiseList.Add("Aston Martin");
            franchiseList.Add("Bentley");
            franchiseList.Add("Bugatti");
            franchiseList.Add("Lotus");
            franchiseList.Add("Maserati");
            franchiseList.Add("Maybach");
            franchiseList.Add("MINI");
            franchiseList.Add("Spyker");
            franchiseList.Add("Rolls-Royce");
            franchiseList.Add("Lamborghini");
            franchiseList.Add("Land Rover");
            franchiseList.Add("Pontiac");
            franchiseList.Add("Mitsubishi");
            franchiseList.Add("Isuzu");
            franchiseList.Add("Fisker");
            franchiseList.Add("McLaren");
            franchiseList.Add("smart");
            franchiseList.Add("Tesla");

            return franchiseList;
        }

        private List<AllyUSer> GetAllyUSerFromExcel()
        {
            List<AllyUSer> allyUSers = new List<AllyUSer>();

            //allyUSers.Add(new AllyUSer { FirstName = "Judith" , LastName="Tabor" , Email="judith.tabor@ally.com"});
            //allyUSers.Add(new AllyUSer { FirstName = "Colin" , LastName="Padden" , Email="cpadden@embarkdigital.com"});
            //allyUSers.Add(new AllyUSer { FirstName = "Laura" , LastName="Cumbow" , Email="Laura.Cumbow@ally.com"});
            //allyUSers.Add(new AllyUSer { FirstName = "Andrew" , LastName="Meleski" , Email="Andrew.Meleski@ally.com"});
            //allyUSers.Add(new AllyUSer { FirstName = "Kelly" , LastName="Grinblatt" , Email="kelly.grinblatt@ally.com"});
            //allyUSers.Add(new AllyUSer { FirstName = "Lloyd" , LastName="Page" , Email="lloyd.page@ally.com"});
            allyUSers.Add(new AllyUSer { FirstName = "Tim" , LastName="Grubbs" , Email="timothy.grubbs@ally.com"});
            allyUSers.Add(new AllyUSer { FirstName = "Shannon" , LastName="Adams" , Email="shannon.adams@ally.com"});
            allyUSers.Add(new AllyUSer { FirstName = "Shannon" , LastName="Hatcher" , Email="shannon.hatcher@ally.com"});
            allyUSers.Add(new AllyUSer { FirstName = "Francis" , LastName="Coupe" , Email="francis.coupe@ally.com"});
            allyUSers.Add(new AllyUSer { FirstName = "Juanita" , LastName="kiesler" , Email="juanita.kiesler@ally.com"});
            allyUSers.Add(new AllyUSer { FirstName = "rick" , LastName="moore" , Email="rickey.moore@ally.com"});
            allyUSers.Add(new AllyUSer { FirstName = "Vincent" , LastName="Apicella" , Email="vincent.apicella@ally.com"});
            allyUSers.Add(new AllyUSer { FirstName = "Jack" , LastName="Carozza" , Email="jack.carozza@ally.com"});
            allyUSers.Add(new AllyUSer { FirstName = "Julie" , LastName="Blackwood" , Email="julie.blackwood@ally.com"});
            allyUSers.Add(new AllyUSer { FirstName = "Kevin" , LastName="Smith" , Email="kevin.l.smith@ally.com"});
            allyUSers.Add(new AllyUSer { FirstName = "Kevin" , LastName="Marino" , Email="kma5353486@aol.com"});
            allyUSers.Add(new AllyUSer { FirstName = "Dan" , LastName="Flynn" , Email="daniel.flynn@ally.com"});
            allyUSers.Add(new AllyUSer { FirstName = "Daniel" , LastName="Zuniga" , Email="daniel.zuniga@ally.com"});
            allyUSers.Add(new AllyUSer { FirstName = "Mark" , LastName="Smith" , Email="mark.w.smith@ally.com"});
            allyUSers.Add(new AllyUSer { FirstName = "La Donna" , LastName="Slaughter" , Email="ladonna.slaughter@ally.com"});
            allyUSers.Add(new AllyUSer { FirstName = "Andy" , LastName="Gess" , Email="andy.gess@ally.com"});
            allyUSers.Add(new AllyUSer { FirstName = "Rich" , LastName="Coill" , Email="richard.coill@ally.com"});
            allyUSers.Add(new AllyUSer { FirstName = "andrew" , LastName="barton" , Email="andrew.barton@ally.com"});
            allyUSers.Add(new AllyUSer { FirstName = "William" , LastName="Cody" , Email="william.cody@ally.com"});
            allyUSers.Add(new AllyUSer { FirstName = "JIm" , LastName="Pinsoneault" , Email="james.pinsoneault@ally.com"});
            allyUSers.Add(new AllyUSer { FirstName = "Jim" , LastName="Auttonberry" , Email="james.auttonberry@ally.com"});
            allyUSers.Add(new AllyUSer { FirstName = "Tyler" , LastName="Gahn" , Email="tyler.gahn@ally.com"});
            allyUSers.Add(new AllyUSer { FirstName = "Moe" , LastName="Shamon" , Email="monther.shamon@ally.com"});
            allyUSers.Add(new AllyUSer { FirstName = "Jeff" , LastName="Palumbo" , Email="jeffrey.palumbo@ally.com"});
            allyUSers.Add(new AllyUSer { FirstName = "Kenneth" , LastName="MacFarquhar" , Email="kenneth.macfarquhar@ally.com"});
            allyUSers.Add(new AllyUSer { FirstName = "kristelle" , LastName="bolton" , Email="kristelle.bolton@ally.com"});
            allyUSers.Add(new AllyUSer { FirstName = "Mike" , LastName="Roberts" , Email="michael.roberts@ally.com"});
            allyUSers.Add(new AllyUSer { FirstName = "amy" , LastName="alexander" , Email="amy.alexander@ally.com"});
            allyUSers.Add(new AllyUSer { FirstName = "James" , LastName="Dimon" , Email="jimmy.dimon@ally.com"});
            allyUSers.Add(new AllyUSer { FirstName = "Doug" , LastName="Kier" , Email="douglas.kier@ally.com"});
            allyUSers.Add(new AllyUSer { FirstName = "Debbie" , LastName="Marcks" , Email="debbie.marcks@ally.com"});
            allyUSers.Add(new AllyUSer { FirstName = "Rick" , LastName="Allen" , Email="richard.allen@ally.com"});
            allyUSers.Add(new AllyUSer { FirstName = "Mark" , LastName="Sauntry" , Email="mark.sauntry@ally.com"});
            allyUSers.Add(new AllyUSer { FirstName = "David" , LastName="Luke" , Email="david.luke@ally.com"});
            allyUSers.Add(new AllyUSer { FirstName = "Mark" , LastName="Hale" , Email="mark.hale@ally.com"});
            allyUSers.Add(new AllyUSer { FirstName = "John" , LastName="Hilland" , Email="john.hilland@ally.com"});
            allyUSers.Add(new AllyUSer { FirstName = "Mike" , LastName="Coleman" , Email="michael.coleman@ally.com"});
            allyUSers.Add(new AllyUSer { FirstName = "Paul" , LastName="McMillan" , Email="paul.mcmillanjr@ally.com"});
            allyUSers.Add(new AllyUSer { FirstName = "Kathryn" , LastName="McDonald" , Email="kathryn.mcdonald@ally.com"});
            allyUSers.Add(new AllyUSer { FirstName = "John" , LastName="Titch" , Email="john.titch@ally.com"});
            allyUSers.Add(new AllyUSer { FirstName = "Marty" , LastName="Ransom" , Email="bennie.ransom@ally.com"});
            allyUSers.Add(new AllyUSer { FirstName = "Robert" , LastName="Dougherty" , Email="robert.dougherty@ally.com"});
            allyUSers.Add(new AllyUSer { FirstName = "Patricia" , LastName="Ilkow" , Email="patricia.ilkow@ally.com"});
            allyUSers.Add(new AllyUSer { FirstName = "David" , LastName="Herschelman" , Email="david.herschelman@ally.com"});
            allyUSers.Add(new AllyUSer { FirstName = "Paige" , LastName="Whited" , Email="paige.whited@ally.com"});
            allyUSers.Add(new AllyUSer { FirstName = "Ted" , LastName="Wurm" , Email="ted.wurm@ally.com"});
            allyUSers.Add(new AllyUSer { FirstName = "Doug" , LastName="Stull" , Email="douglas.stull@ally.com"});
            allyUSers.Add(new AllyUSer { FirstName = "Mike" , LastName="Rapp" , Email="mike.rapp@ally.com"});
            allyUSers.Add(new AllyUSer { FirstName = "Suzanne" , LastName="Modrick" , Email="suzanne.modrick@ally.com"});
            allyUSers.Add(new AllyUSer { FirstName = "Ed" , LastName="Fannon" , Email="edward.j.fannon@ally.com"});
            allyUSers.Add(new AllyUSer { FirstName = "Teresa" , LastName="Crawford" , Email="teresa.crawford@ally.com"});
            allyUSers.Add(new AllyUSer { FirstName = "Amanda" , LastName="Mays" , Email="amanda.mays@ally.com"});
            allyUSers.Add(new AllyUSer { FirstName = "Denise" , LastName="Douglas" , Email="denise.douglas@ally.com"});
            allyUSers.Add(new AllyUSer { FirstName = "Larry" , LastName="Williams" , Email="larry.williams@ally.com"});
            allyUSers.Add(new AllyUSer { FirstName = "Don" , LastName="Kullgren" , Email="donald.kullgren@ally.com"});
            allyUSers.Add(new AllyUSer { FirstName = "John" , LastName="Matrone" , Email="john.matrone@ally.com"});
            allyUSers.Add(new AllyUSer { FirstName = "Kevin" , LastName="Burk" , Email="kevin.r.burk@ally.com"});
            allyUSers.Add(new AllyUSer { FirstName = "kelly" , LastName="schmitt" , Email="kelly.schmitt@ally.com"});
            allyUSers.Add(new AllyUSer { FirstName = "Trish" , LastName="Sather" , Email="trish.sather@ally.com"});
            allyUSers.Add(new AllyUSer { FirstName = "matt" , LastName="blake" , Email="matthew.blake@ally.com"});
            allyUSers.Add(new AllyUSer { FirstName = "Joe" , LastName="Wiesner" , Email="joseph.wiesner@ally.com"});
            allyUSers.Add(new AllyUSer { FirstName = "Ken" , LastName="Shields" , Email="kenneth.shields@ally.com"});
            allyUSers.Add(new AllyUSer { FirstName = "christal" , LastName="delay" , Email="christal.delay@ally.com"});
            allyUSers.Add(new AllyUSer { FirstName = "John" , LastName="Guenther" , Email="john.guenther@ally.com"});
            allyUSers.Add(new AllyUSer { FirstName = "elizabeth" , LastName="frolick" , Email="elizabeth.frolick@ally.com"});
            allyUSers.Add(new AllyUSer { FirstName = "Mark" , LastName="Comune" , Email="mark.comune@ally.com"});
            allyUSers.Add(new AllyUSer { FirstName = "Jerry" , LastName="Henderson" , Email="gerald.henderson@ally.com"});
            allyUSers.Add(new AllyUSer { FirstName = "Janlyn" , LastName="Messina" , Email="janlyn.messina@ally.com"});
            allyUSers.Add(new AllyUSer { FirstName = "Michael" , LastName="Jefferson" , Email="michael.jefferson@ally.com"});


            return allyUSers;
        }
    }

    class SDLocation
    {
        public SDLocation(bool isGroup)
        {
            IsGroup = isGroup;
        }
        public long SocialDealerID { get; set; }
        public long SocialIntegrationID { get; set; }

        public bool IsGroup { get; set; }
        public long GroupID { get; set; }

        public string LocationName { get; set; }
        public string Address { get; set; }
        public string City { get; set; }
        public string StateAbbreviation { get; set; }
        public string ZipCode { get; set; }


    }

    class AllyLocation
    {

        public long PDN { get; set; }
        public string PDN_Name { get; set; }
        public string CUST_ADDR1 { get; set; }
        public string CITY { get; set; }
        public string CUST_STATE_CD { get; set; }
        public string CUST_ZIP { get; set; }


    }

    class AllyUSer
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }

    }
}
