﻿using System;
using System.Collections.Generic;
using System.Data.Objects;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SI.BLL.Interface;
using SI.DAL;
using Microsoft.Data.Extensions;
using SI.DTO;
using System.Data.SqlClient;
using System.Data.Common;
using Newtonsoft.Json;
using Notification;
using System.Reflection;

namespace SI.BLL
{
    internal class NotificationProvider : INotificationProvider
    {

        #region routines to queue notifications

        public void QueueNotification(NotificationMessageTypeEnum type, long entityID, long accountID)
        {
            QueueNotificationDataDTO data = new QueueNotificationDataDTO()
            {
                Type = type,
                AccountID = accountID,
                EntityID = entityID
            };
            QueueNotifications(new List<QueueNotificationDataDTO>() { data });
        }
        public void QueueNotifications(List<QueueNotificationDataDTO> items)
        {
            using (MainEntities db = ContextFactory.Main)
            {
                foreach (QueueNotificationDataDTO item in items)
                {
                    switch (item.Type)
                    {
                        #region standard subscribed to notifications

                        case NotificationMessageTypeEnum.NewReviewAlert:
                        case NotificationMessageTypeEnum.NewNegativeReviewAlert:
                        case NotificationMessageTypeEnum.NegReviewNoResponse6h:
                        case NotificationMessageTypeEnum.NegReviewNoResponse12h:
                        case NotificationMessageTypeEnum.NegReviewNoResponse24h:
                        case NotificationMessageTypeEnum.SyndicationPendingPostApproval:

                            DAL.Notification n = new DAL.Notification()
                                {
                                    NotificationMessageTypeID = (long)item.Type,
                                    AccountID = item.AccountID,
                                    EntityID = item.EntityID,
                                    DateCreated = DateTime.UtcNow
                                };
                            db.Notifications.AddObject(n);

                            break;

                        #endregion

                        #region special cases where we want to control the recipient list not based on any subscription

                        #endregion

                        default:
                            break;
                    }

                }
                db.SaveChanges();
            }
        }

        #endregion

        public void ProcessNotifications()
        {
            using (MainEntities db = ContextFactory.Main)
            {
                bool doProcessSubscriptions = true;
                int subscriptionCheckBatchSize = 250;

                bool doSendSocialNotifications = true;
                bool doSendDistributionPendingApprovals = true;
                bool doSendSyndicationPendingApprovals = true;
                bool doSendNewReviewAlerts = true;

                if (doProcessSubscriptions)
                {

                    List<long> subscriptionIDs = new List<long>();
                    foreach (var result in db.spGetNotificationSubscriptionList())
                    {
                        ScheduleSpecDTO spec = ScheduleSpecDTO.FromString(result.ScheduleSpec);
                        DateTime nextRunDate = spec.GetNextRunDate(result.LastNotifiedDate);
                        if (nextRunDate <= DateTime.UtcNow)
                        {
                            subscriptionIDs.Add(result.NotificationSubscriptionID);
                        }
                    }

                    if (subscriptionIDs.Any())
                    {
                        while (subscriptionIDs.Any())
                        {
                            List<long> subIDsToProcess = new List<long>();
                            subIDsToProcess.AddRange(subscriptionIDs.Take(subscriptionCheckBatchSize));
                            subscriptionIDs = (from s in subscriptionIDs where !subIDsToProcess.Contains(s) select s).ToList();
                            db.spAddNotificationRecipients(string.Join("|", subIDsToProcess.ToArray()));
                        }
                    }

                }

                try
                {
                    if (doSendNewReviewAlerts) sendNewReviewNotifications();
                }
                catch (Exception ex)
                {
                    ProviderFactory.Logging.LogException(ex);
                }

                try
                {
                    if (doSendSyndicationPendingApprovals) sendSyndicationPendingApprovalNotifications();
                }
                catch (Exception ex)
                {
                    ProviderFactory.Logging.LogException(ex);
                }

                try
                {

                    if (doSendDistributionPendingApprovals) sendDistributionPendingApprovalNotifications();
                }
                catch (Exception ex)
                {
                    ProviderFactory.Logging.LogException(ex);
                }

                try
                {
                    if (doSendSocialNotifications) sendSocialNotifications();
                }
                catch (Exception ex)
                {
                    ProviderFactory.Logging.LogException(ex);
                }


            }
        }



        private void deleteNotificationSubscriptions(List<long> notificationSubscriptionIDs)
        {
            using (MainEntities db = ContextFactory.Main)
            {
                if (db.Connection.State != System.Data.ConnectionState.Open)
                    db.Connection.Open();

                using (DbCommand cmd = db.CreateStoreCommand(
                    "spDeleteNotificationSubscriptions", System.Data.CommandType.StoredProcedure,
                    new SqlParameter("@NotificationSubscriptionIDs", string.Join("|", notificationSubscriptionIDs.ToArray()))
                ))
                {
                    cmd.ExecuteNonQuery();
                }
            }
        }

        //private class GetNotificationsSubscriptionItem
        //{
        //    public long NotificationSubscriptionID { get; set; }
        //    public long AccountID { get; set; }
        //    public long NotificationTemplateID { get; set; }
        //    public long RecipientUserID { get; set; }
        //    public long NotificationMessageTypeID { get; set; }
        //    public long NotificationMethodID { get; set; }
        //    public long ResellerID { get; set; }
        //    public string ScheduleSpec { get; set; }

        //}

        //private List<GetNotificationsSubscriptionItem> getNotificationSubscriptions(long? accountID, NotificationMessageTypeEnum? messageType, long? userID)
        //{
        //    using (MainEntities db = ContextFactory.Main)
        //    {
        //        if (db.Connection.State != System.Data.ConnectionState.Open)
        //            db.Connection.Open();

        //        using (DbCommand cmd = db.CreateStoreCommand(
        //            "spGetNotificationsSubscriptions", System.Data.CommandType.StoredProcedure,
        //            new SqlParameter("@AccountID", accountID),
        //            new SqlParameter("@NotificationMessageTypeID", (long?)messageType),
        //            new SqlParameter("@UserID", userID)
        //        ))
        //        {
        //            using (DbDataReader reader = cmd.ExecuteReader())
        //            {
        //                List<GetNotificationsSubscriptionItem> items = db.Translate<GetNotificationsSubscriptionItem>(reader).ToList();
        //                return items;
        //            }
        //        }
        //    }
        //}

        public void SetNotificationMessageTypeSubscriptions(
            long userID, long accountID,
            NotificationMessageTypeEnum messageType, List<NotificationMethodEnum> methods,
            long resellerID, ScheduleSpecDTO schedule
        )
        {
            using (MainEntities db = ContextFactory.Main)
            {
                db.spSetNotificationMessageTypeSubscriptions(
                    userID,
                    accountID,
                    (long)messageType,
                    string.Join("|", (from m in methods select (long)m).Distinct().ToArray()),
                    schedule.ToString(),
                    resellerID
                );
            }

        }

        #region Notification Counts and Lists

        public List<NotificationSummaryDTO> GetGlobeNotificationList(UserInfoDTO userInfo)
        {
            List<NotificationSummaryDTO> list = new List<NotificationSummaryDTO>();

            List<long> accountIDs =
                AccountListBuilder
                    .New(new SecurityProvider())
                    .AddAccountsWithPermission(userInfo, PermissionTypeEnum.accounts_view)
                    .GetIDs(SecurityProvider.PLATFORM_MAX_ACCOUNT_ID_LIST)
            ;

            using (MainEntities db = ContextFactory.Main)
            {
                if (db.Connection.State != System.Data.ConnectionState.Open)
                    db.Connection.Open();

                using (DbCommand cmd2 = db.CreateStoreCommand("spGetPlatformNotificationCounts", System.Data.CommandType.StoredProcedure,
                        new SqlParameter("@UserID", userInfo.EffectiveUser.ID.Value),
                        new SqlParameter("@NotificationMessageTypeIDs", SecurityProvider.PLATFORM_NOTIFICATION_MESSAGE_TYPE_IDS),
                        new SqlParameter("@AccountIDs", userInfo.EffectiveUser.CurrentContextAccountID.ToString(CultureInfo.InvariantCulture))
                    ))
                {
                    using (DbDataReader reader2 = cmd2.ExecuteReader())
                    {
                        List<PlatformNotificationCount> ncounts = db.Translate<PlatformNotificationCount>(reader2).ToList();

                        List<PlatformNotificationCount> newReviewCounts = (from n in ncounts where n.NotificationMessageTypeID == (long)NotificationMessageTypeEnum.NewReviewAlert select n).ToList();
                        if (newReviewCounts.Any())
                        {
                            int total = (from n in newReviewCounts select n.RolledNotificationCount).First();
                            if (total > 0)
                            {
                                list.Add(new NotificationSummaryDTO()
                                {
                                    ID = (int)NotificationMessageTypeEnum.NewReviewAlert,
                                    AlertLevel = NotificationAlertLevelIconEnum.alert,
                                    Text = string.Format("You have {0} new review(s)", total),
                                    Count = total
                                });
                            }
                        }
                        List<PlatformNotificationCount> pendingApprovalCounts = (
                            from n in ncounts
                            where n.NotificationMessageTypeID == (long)NotificationMessageTypeEnum.SyndicationPendingPostApproval
                            || n.NotificationMessageTypeID == (long)NotificationMessageTypeEnum.NonSyndicationPendingPostApproval
                            select n
                        ).ToList();
                        if (pendingApprovalCounts.Any())
                        {
                            int total = (from n in pendingApprovalCounts select n.RolledNotificationCount).First();
                            if (total > 0)
                            {
                                list.Add(new NotificationSummaryDTO()
                                {
                                    ID = (int)NotificationMessageTypeEnum.SyndicationPendingPostApproval,
                                    AlertLevel = NotificationAlertLevelIconEnum.alert,
                                    Text = string.Format("You have {0} pending approvals(s)", total),
                                    Count = total
                                });
                            }
                        }

                    }
                }
            }

            return list;
        }

        public List<NotificationsAlertDTO> GetNotificationList(UserInfoDTO userInfo)
        {
            List<NotificationsAlertDTO> list = new List<NotificationsAlertDTO>();

            //the account list
            List<long> accountIDs = new List<long>() { userInfo.EffectiveUser.CurrentContextAccountID };

            //add descendants
            accountIDs.AddRange(ProviderFactory.Security.GetAccountDescendants(userInfo.EffectiveUser.CurrentContextAccountID));

            using (MainEntities db = ContextFactory.Main)
            {
                if (db.Connection.State != System.Data.ConnectionState.Open)
                    db.Connection.Open();

                using (DbCommand cmd2 = db.CreateStoreCommand("spGetNotificationCountsByAccount", System.Data.CommandType.StoredProcedure,
                        new SqlParameter("@UserID", userInfo.EffectiveUser.ID.Value),
                        new SqlParameter("@NotificationMessageTypeIDs", SecurityProvider.PLATFORM_NOTIFICATION_MESSAGE_TYPE_IDS),
                        new SqlParameter("@AccountIDs", string.Join("|", accountIDs.ToArray()))
                    ))
                {
                    using (DbDataReader reader2 = cmd2.ExecuteReader())
                    {
                        List<PlatformNotificationCountWithAccountName> ncounts = db.Translate<PlatformNotificationCountWithAccountName>(reader2).ToList();

                        List<PlatformNotificationCountWithAccountName> newReviewCounts = (from n in ncounts where n.NotificationMessageTypeID == (long)NotificationMessageTypeEnum.NewReviewAlert select n).ToList();
                        foreach (PlatformNotificationCountWithAccountName item in newReviewCounts)
                        {
                            if (item.NotificationCount > 0)
                            {
                                list.Add(new NotificationsAlertDTO()
                                {
                                    AlertType = (NotificationMessageTypeEnum)item.NotificationMessageTypeID,
                                    ID = item.AccountID,
                                    LocationName = item.AccountName,
                                    Description = string.Format("{0} new Unread Reviews detected.", item.NotificationCount),
                                });
                            }
                        }

                        List<PlatformNotificationCountWithAccountName> pendingApprovalCounts = (from n in ncounts where n.NotificationMessageTypeID == (long)NotificationMessageTypeEnum.SyndicationPendingPostApproval select n).ToList();
                        foreach (PlatformNotificationCountWithAccountName item in pendingApprovalCounts)
                        {
                            if (item.NotificationCount > 0)
                            {
                                list.Add(new NotificationsAlertDTO()
                                {
                                    AlertType = (NotificationMessageTypeEnum)item.NotificationMessageTypeID,
                                    ID = item.AccountID,
                                    LocationName = item.AccountName,
                                    Description = string.Format("{0} Pending Approval(s) - Click here to Approve or Deny", item.NotificationCount),
                                });
                            }
                        }
                    }
                }
            }


            return list;
        }

        private class PlatformNotificationCount
        {
            public long AccountID { get; set; }
            public long NotificationMessageTypeID { get; set; }
            public long UserID { get; set; }
            public int NotificationCount { get; set; }
            public int RolledNotificationCount { get; set; }
        }
        private class PlatformNotificationCountWithAccountName
        {
            public long AccountID { get; set; }
            public long NotificationMessageTypeID { get; set; }
            public long UserID { get; set; }
            public int NotificationCount { get; set; }
            public int RolledNotificationCount { get; set; }
            public string AccountName { get; set; }
        }

        #endregion

        #region Pending Notification Subscripton Checks

        public void AddSubscriptionCheck(long? userID, long? accountID)
        {
            using (MainEntities db = ContextFactory.Main)
            {
                if (db.Connection.State != System.Data.ConnectionState.Open) db.Connection.Open();

                using (DbCommand cmd = db.CreateStoreCommand("spAddNotificationPendingSubscriptionCheck", System.Data.CommandType.StoredProcedure,
                    new SqlParameter("@UserID", userID),
                    new SqlParameter("@AccountID", accountID)
                ))
                {
                    cmd.ExecuteNonQuery();
                }
            }
        }

        public List<PendingNotificationSubscriptionCheckDTO> GetPendingNotificationSubscriptionChecks()
        {
            using (MainEntities db = ContextFactory.Main)
            {
                if (db.Connection.State != System.Data.ConnectionState.Open) db.Connection.Open();

                using (DbCommand cmd = db.CreateStoreCommand("spGetNotificationPendingSubscriptionChecks", System.Data.CommandType.StoredProcedure))
                {
                    using (DbDataReader reader = cmd.ExecuteReader())
                    {
                        return db.Translate<PendingNotificationSubscriptionCheckDTO>(reader).ToList();
                    }
                }
            }
        }


        public void ApplyDefaultNotificationSubscriptionsForUser(long userID)
        {
            using (MainEntities db = ContextFactory.Main)
            {
                db.spFixNotiSubsForUser(userID);
            }
        }

        public void ApplyDefaultNotificationSubscriptionsForAccount(long accountID)
        {
            // use account view permission to determine all of the users to process for a given account
            List<AccountPermissionDTO> aps = ProviderFactory.Security.UsersWithAccountPermission(accountID, PermissionTypeEnum.accounts_view, true);
            List<long> userIDs = (from a in aps select a.UserID).Distinct().ToList();

            foreach (long userID in userIDs)
            {
                ApplyDefaultNotificationSubscriptionsForUser(userID);
            }

        }


        #endregion

        #region Custom Email Send Routines

        internal void sendWelcomeEmail(long userID, string userName, long accountID, string emailAddress, long resellerID, AccessTokenBaseDTO passwordResetToken)
        {
            try
            {
                string accessURLBase = string.Format("{0}/access", Settings.GetSetting("site.baseurl", "socialintegration.com"));
                string changePasswordLink = string.Format("{0}/{1}", accessURLBase, passwordResetToken.Token.ToString());

                // find the template
                using (MainEntities db = ContextFactory.Main)
                {
                    NotificationTemplate template = (
                        from t in db.NotificationTemplates
                        where t.NotificationMessageTypeID == (long)NotificationMessageTypeEnum.EnrollmentWelcomeEmail
                        && t.NotificationMethodID == (long)NotificationMethodEnum.HtmlEmail
                        && t.ResellerID.HasValue && t.ResellerID.Value == resellerID
                        select t
                    ).FirstOrDefault();

                    if (template == null)
                    {
                        template = (
                            from t in db.NotificationTemplates
                            where t.NotificationMessageTypeID == (long)NotificationMessageTypeEnum.EnrollmentWelcomeEmail
                            && t.NotificationMethodID == (long)NotificationMethodEnum.HtmlEmail
                            && !t.ResellerID.HasValue
                            select t
                        ).FirstOrDefault();
                    }

                    if (template == null)
                    {
                        // do something?
                        return;
                    }

                    SyndicationSignupDTO rdto = Newtonsoft.Json.JsonConvert.DeserializeObject<SyndicationSignupDTO>(template.Body);

                    //string body = template.Body;
                    string body = rdto.body;
                    body = body.Replace("[$UserName$]", userName);
                    body = body.Replace("[$ChangePasswordLink$]", changePasswordLink);

                    string subject = template.Subject;
                    subject = subject.Replace("[$UserName$]", userName);

                    // insert a notification placeholder
                    DAL.Notification noti = new DAL.Notification()
                    {
                        NotificationMessageTypeID = (long)NotificationMessageTypeEnum.EnrollmentWelcomeEmail,
                        AccountID = accountID,
                        EntityID = userID,
                        DateCreated = DateTime.UtcNow
                    };
                    db.Notifications.AddObject(noti);
                    db.SaveChanges();

                    // insert a target
                    DAL.NotificationTarget target = new NotificationTarget()
                    {
                        NotificationMethodID = (long)NotificationMethodEnum.HtmlEmail,
                        NotificationStatusID = (long)NotificationStatusesEnum.Pending,
                        Subject = subject,
                        Body = body,
                        DateRead = DateTime.UtcNow,
                        DateSent = null,
                        Address = emailAddress
                    };
                    db.NotificationTargets.AddObject(target);
                    db.SaveChanges();

                    // insert a notification recipient placeholder
                    DAL.NotificationRecipient reci = new NotificationRecipient()
                    {
                        NotificationID = noti.ID,
                        NotificationTemplateID = template.ID,
                        NotificationTargetID = target.ID,
                        PickedUpDate = DateTime.UtcNow,
                        UserID = userID
                    };
                    try
                    {
                        db.NotificationRecipients.AddObject(reci);
                        db.SaveChanges();
                    }
                    catch (Exception ex)
                    {
                        ProviderFactory.Logging.LogException(ex);
                    }

                    //Send Email
                    string SendFromEmailAddress = Settings.GetSetting("site.SendFromEmailAddress", "notifications@socialdealer.com");

                    EmailManager emailManager = new EmailManager();
                    bool sendResult = emailManager.sendNotification(
                        "Welcome Email", target.Subject, target.Body,
                        SendFromEmailAddress, target.Address,
                        string.Empty, string.Empty)
                    ;

                    if (sendResult == true)
                    {
                        updateNotificationTarget(target.ID, NotificationStatusesEnum.Success);
                    }
                    else
                    {
                        updateNotificationTarget(target.ID, NotificationStatusesEnum.Failure);
                    }



                }

            }
            catch (Exception ex)
            {
                ProviderFactory.Logging.LogException(ex);
            }
        }

        internal void sendSyndicationSignupSupportEmail(SignupSubmitInfoDTO signupInfo)
        {
            try
            {
                bool isAlly = false;
                bool isLexus = false;
                bool isAutoConverse = false;
                bool isCATA = false;

                using (MainEntities db = ContextFactory.Main)
                {
                    //List<long> rpids = (from sp in signupInfo.SelectedResellerPackages select sp.ResellerPackageID).ToList();


                    if (signupInfo.SyndicatorID.HasValue && signupInfo.SyndicatorID == 4)
                    {
                        isAlly = true;
                    }
                    if (signupInfo.SyndicatorID.HasValue && signupInfo.SyndicatorID == 1)
                    {
                        isLexus = true;
                    }
                    if (signupInfo.SyndicatorID.HasValue && signupInfo.SyndicatorID == 3)
                    {
                        isAutoConverse = true;
                    }
                    if (signupInfo.SyndicatorID.HasValue && signupInfo.SyndicatorID == 8)
                    {
                        isCATA = true;
                    }
                    //List<long> featuretypes = (
                    //    from pft in db.Products_FeatureTypes
                    //    join pp in db.Packages_Products on pft.ProductID equals pp.ProductID
                    //    join rp in db.Resellers_Packages on pp.PackageID equals rp.PackageID
                    //    where rpids.Contains(rp.ID)
                    //    select pft.FeatureTypeID
                    //).Distinct().ToList();

                    //if (featuretypes.Contains((long)FeatureTypeEnum.AllySyndicatee))
                    //{
                    //    isAlly = true;
                    //}
                    //if (featuretypes.Contains((long)FeatureTypeEnum.LexusSyndicatee))
                    //{
                    //    isLexus = true;
                    //}

                    var resellerInfo = (
                        from r in db.Resellers
                        join a in db.Accounts on r.AccountID equals a.ID
                        where r.ID == signupInfo.ResellerID
                        select new
                        {
                            DisplayName = a.DisplayName,
                            Name = a.Name,
                            AccountID = a.ID
                        }
                    ).FirstOrDefault();

                    string stateName = (from s in db.States where s.ID == signupInfo.StateID select s.Name).FirstOrDefault();
                    if (stateName == null) stateName = "";

                    //string accountName = resellerInfo.Name;
                    //if (!string.IsNullOrWhiteSpace(resellerInfo.DisplayName))
                    //    accountName = resellerInfo.DisplayName;

                    string accountName = signupInfo.AccountName;

                    string body = Util.GetResourceTextFile("SyndSignupSupport.html", Assembly.GetAssembly(typeof(ProviderFactory)));
                    string bodyStripped = Util.GetResourceTextFile("SyndSignupSupport.html", Assembly.GetAssembly(typeof(ProviderFactory)));
                    string repeat = Util.GetResourceTextFile("SyndSignupSupport.RepeatingBlock.html", Assembly.GetAssembly(typeof(ProviderFactory)));
                    string subject = string.Format("Syndication Signup: {0} for {1}", signupInfo.AccountName, accountName);

                    StringBuilder rBlock = new StringBuilder();
                    StringBuilder rBlockStripped = new StringBuilder();

                    #region basic info

                    rBlock.Append(repeat
                        .Replace("[Item]", "Account Name")
                        .Replace("[Value]", accountName)
                    );
                    rBlock.Append(repeat
                        .Replace("[Item]", "Billing Phone")
                        .Replace("[Value]", signupInfo.BillingPhone)
                    );
                    rBlock.Append(repeat
                        .Replace("[Item]", "City Name")
                        .Replace("[Value]", signupInfo.CityName)
                    );
                    rBlock.Append(repeat
                        .Replace("[Item]", "Company Email")
                        .Replace("[Value]", signupInfo.CompanyEmailAddress)
                    );
                    rBlock.Append(repeat
                        .Replace("[Item]", "Company Website")
                        .Replace("[Value]", signupInfo.CompanyWebsiteURL)
                    );
                    rBlock.Append(repeat
                        .Replace("[Item]", "Postal Code")
                        .Replace("[Value]", signupInfo.PostalCode)
                    );
                    rBlock.Append(repeat
                        .Replace("[Item]", "User Email")
                        .Replace("[Value]", signupInfo.UserEmailAddress)
                    );
                    rBlock.Append(repeat
                        .Replace("[Item]", "First Name")
                        .Replace("[Value]", signupInfo.UserFirstName)
                    );
                    rBlock.Append(repeat
                        .Replace("[Item]", "Last Name")
                        .Replace("[Value]", signupInfo.UserLastName)
                    );
                    rBlock.Append(repeat
                        .Replace("[Item]", "Website URL")
                        .Replace("[Value]", signupInfo.WebsiteURL)
                    );
                    rBlock.Append(repeat
                        .Replace("[Item]", "Screen Name")
                        .Replace("[Value]", signupInfo.ScreenName)
                    );
                    rBlock.Append(repeat
                        .Replace("[Item]", "Street Address")
                        .Replace("[Value]", signupInfo.StreetAddress)
                    );
                    rBlock.Append(repeat
                        .Replace("[Item]", "State")
                        .Replace("[Value]", stateName)
                    );

                    rBlockStripped.Append(repeat
                        .Replace("[Item]", "Account Name")
                        .Replace("[Value]", accountName)
                    );
                    rBlockStripped.Append(repeat
                        .Replace("[Item]", "Billing Phone")
                        .Replace("[Value]", signupInfo.BillingPhone)
                    );
                    rBlockStripped.Append(repeat
                        .Replace("[Item]", "City Name")
                        .Replace("[Value]", signupInfo.CityName)
                    );
                    rBlockStripped.Append(repeat
                        .Replace("[Item]", "Company Email")
                        .Replace("[Value]", signupInfo.CompanyEmailAddress)
                    );
                    rBlockStripped.Append(repeat
                        .Replace("[Item]", "Company Website")
                        .Replace("[Value]", signupInfo.CompanyWebsiteURL)
                    );
                    rBlockStripped.Append(repeat
                        .Replace("[Item]", "Postal Code")
                        .Replace("[Value]", signupInfo.PostalCode)
                    );
                    rBlockStripped.Append(repeat
                        .Replace("[Item]", "User Email")
                        .Replace("[Value]", signupInfo.UserEmailAddress)
                    );
                    rBlockStripped.Append(repeat
                        .Replace("[Item]", "First Name")
                        .Replace("[Value]", signupInfo.UserFirstName)
                    );
                    rBlockStripped.Append(repeat
                        .Replace("[Item]", "Last Name")
                        .Replace("[Value]", signupInfo.UserLastName)
                    );
                    rBlockStripped.Append(repeat
                        .Replace("[Item]", "Website URL")
                        .Replace("[Value]", signupInfo.WebsiteURL)
                    );
                    rBlockStripped.Append(repeat
                        .Replace("[Item]", "Screen Name")
                        .Replace("[Value]", signupInfo.ScreenName)
                    );
                    rBlockStripped.Append(repeat
                        .Replace("[Item]", "Street Address")
                        .Replace("[Value]", signupInfo.StreetAddress)
                    );
                    rBlockStripped.Append(repeat
                        .Replace("[Item]", "State")
                        .Replace("[Value]", stateName)
                    );

                    #endregion

                    #region extended info

                    rBlock.Append(repeat
                        .Replace("[Item]", "CountryID")
                        .Replace("[Value]", signupInfo.CountryID.ToString())
                    );
                    rBlock.Append(repeat
                        .Replace("[Item]", "FranchiseTypeIDs")
                        .Replace("[Value]", string.Join(", ", signupInfo.FranchiseTypeIDs.ToArray()))
                    );
                    rBlock.Append(repeat
                        .Replace("[Item]", "Social Network Picture URL")
                        .Replace("[Value]", signupInfo.PictureURL)
                    );

                    rBlock.Append(repeat
                        .Replace("[Item]", "Preferred Parent Account ID")
                        .Replace("[Value]", signupInfo.PreferredParentAccountID.GetValueOrDefault(0).ToString())
                    );
                    rBlock.Append(repeat
                        .Replace("[Item]", "Reseller ID")
                        .Replace("[Value]", signupInfo.ResellerID.ToString())
                    );
                    rBlock.Append(repeat
                        .Replace("[Item]", "Reseller Name")
                        .Replace("[Value]", resellerInfo.Name)
                    );
                    rBlock.Append(repeat
                        .Replace("[Item]", "Selected Reseller Package IDs")
                        .Replace("[Value]", string.Join(", ", (from s in signupInfo.SelectedResellerPackages select s.ResellerPackageID).ToArray()))
                    );
                    rBlock.Append(repeat
                        .Replace("[Item]", "SocialAppID")
                        .Replace("[Value]", signupInfo.SocialAppID.ToString())
                    );
                    rBlock.Append(repeat
                        .Replace("[Item]", "Social Network")
                        .Replace("[Value]", signupInfo.SocialNetwork.ToString())
                    );
                    rBlock.Append(repeat
                        .Replace("[Item]", "StateID")
                        .Replace("[Value]", signupInfo.StateID.ToString())
                    );
                    rBlock.Append(repeat
                        .Replace("[Item]", "Timezone ID")
                        .Replace("[Value]", signupInfo.TimezoneID.ToString())
                    );
                    rBlock.Append(repeat
                        .Replace("[Item]", "Token")
                        .Replace("[Value]", signupInfo.Token)
                    );
                    rBlock.Append(repeat
                        .Replace("[Item]", "TokenSecret")
                        .Replace("[Value]", signupInfo.TokenSecret)
                    );
                    rBlock.Append(repeat
                        .Replace("[Item]", "UniqueID")
                        .Replace("[Value]", signupInfo.UniqueID)
                    );
                    rBlock.Append(repeat
                        .Replace("[Item]", "URI")
                        .Replace("[Value]", signupInfo.URI)
                    );

                    rBlock.Append(repeat
                        .Replace("[Item]", "Vertical ID")
                        .Replace("[Value]", signupInfo.VerticalID.ToString())
                    );

                    #endregion


                    body = body.Replace("[RepeatingBlock]", rBlock.ToString());
                    bodyStripped = bodyStripped.Replace("[RepeatingBlock]", rBlockStripped.ToString());

                    //Send Email
                    string SendFromEmailAddress = Settings.GetSetting("site.SendFromEmailAddress", "notifications@socialdealer.com");

                    EmailManager emailManager = new EmailManager();
                    bool sendResult = emailManager.sendNotification(
                        "Syndication Signup", subject, body,
                        SendFromEmailAddress, "reynolds@autostartups.com",
                        "s.abraham@socialdealer.com", "support@socialdealer.com")
                    ;
                    sendResult = emailManager.sendNotification(
                        "Syndication Signup", subject, body,
                        SendFromEmailAddress, "billing@socialdealer.com", null, null)
                    ;
                    sendResult = emailManager.sendNotification(
                        "Syndication Signup", subject, body,
                        SendFromEmailAddress, "j.castle@socialdealer.com", null, null)
                    ;

                    if (isAlly)
                    {
                        sendResult = emailManager.sendNotification(
                            "Syndication Signup", subject, bodyStripped,
                            SendFromEmailAddress, "kelly.grinblatt@ally.com", null, null)
                        ;
                        sendResult = emailManager.sendNotification(
                            "Syndication Signup", subject, bodyStripped,
                            SendFromEmailAddress, "saloni.janveja@ally.com", null, null)
                        ;
                    }

                    if (isAutoConverse)
                    {
                        sendResult = emailManager.sendNotification(
                            "Syndication Signup", subject, bodyStripped,
                            SendFromEmailAddress, "assist@autoconversion.net", null, null)
                        ;

                        sendResult = emailManager.sendNotification(
                            "Syndication Signup", subject, bodyStripped,
                            SendFromEmailAddress, "rgerardi@autoconversion.net ", null, null)
                        ;
                    }

                    if (isCATA)
                    {
                        sendResult = emailManager.sendNotification(
                            "Syndication Signup", subject, bodyStripped,
                            SendFromEmailAddress, "rgerardi@autoconversion.net ", null, null)
                        ;
                    }


                }

            }
            catch (Exception ex)
            {
                ProviderFactory.Logging.LogException(ex);
            }
        }

        public bool SendResetPasswordEmail(string fullName, string emailAddress, string accessUrl)
        {
            string template = Util.GetResourceTextFile("ResetPassword.html", Assembly.GetAssembly(typeof(ProviderFactory)));
            template = template.Replace("[user_name]", fullName).Replace("[reset_url]", accessUrl);

            EmailManager emailManager = new EmailManager();
            bool sendResult = emailManager.sendNotification(
                "Account",
                "Need some help with your password?",
                template,
                Settings.GetSetting("site.SendFromEmailAddress", "notifications@socialdealer.com"),
                emailAddress,
                null,
                "support@socialdealer.com"
            );

            return sendResult;
        }

        #endregion

        #region translator classes for readers

        private class NewNotificationToSendItem
        {
            public long NotificationRecipientID { get; set; }
            public long UserID { get; set; }
            public long AccountID { get; set; }
            public long NotificationTemplateID { get; set; }
            public long NotificationMessageTypeID { get; set; }
            public long EntityID { get; set; }
            public long NotificationMethodID { get; set; }
            public string EmailAddress { get; set; }

            public string AccountName { get; set; }
            public string AccountPhone { get; set; }
            public string AccountURL { get; set; }
        }

        private class SubscribedPackage
        {
            public long AccountID { get; set; }
            public long SubscriptionID { get; set; }
            public long ResellerID { get; set; }
            public long PackageID { get; set; }

            public DateTime? DateAdded { get; set; }
            public string ResellerName { get; set; }
            public string PackageName { get; set; }

            //public long? FeatureTypeID { get; set; }
            //public string FeatureName { get; set; }
        }

        #endregion

        #region sender routines for each notification type

        private class PostTargetNotification
        {
            public long PostApprovalID { get; set; }
            public long ID { get; set; }
            public long PostID { get; set; }
            public long CredentialID { get; set; }
            public long? JobID { get; set; }
            public string ResultID { get; set; }
            public string FeedID { get; set; }
            public bool Deleted { get; set; }
            public DateTime? ScheduleDate { get; set; }
            public DateTime? PostExpirationDate { get; set; }
            public DateTime? PickedUpDate { get; set; }
            public DateTime? PublishedDate { get; set; }
            public DateTime? FailedDate { get; set; }
            public DateTime? NoApproverHoldDate { get; set; }
        }

        private class SyndicationTemplate
        {
            public long ID { get; set; }
            public long NotificationMessageTypeID { get; set; }
            public long NotificationMethodID { get; set; }
            public long? ResellerID { get; set; }
            public string Subject { get; set; }
            public string Body { get; set; }
            public long? SyndicatorID { get; set; }
        }


        private class SyndicationNewNotificationToSendItem
        {
            public long NotificationRecipientID { get; set; }
            public long UserID { get; set; }
            public long NotificationTemplateID { get; set; }
            public long NotificationMessageTypeID { get; set; }
            public string EmailAddress { get; set; }

            public long EntityID { get; set; }
            public long AccountID { get; set; }
            public long NotificationMethodID { get; set; }

            public string AccountName { get; set; }
            public string AccountPhone { get; set; }
            public string AccountURL { get; set; }

            public long PostApprovalID { get; set; }
            public long PostID { get; set; }
            public long PostTargetID { get; set; }

            public DateTime? ScheduleDate { get; set; }
            public long SyndicatorID { get; set; }
            public long CredentialID { get; set; }

            public string ResultID { get; set; }
            public string FeedID { get; set; }

            public string SocialNetworkName { get; set; }
            public long SocialNetworkID { get; set; }

            public long PostTypeID { get; set; }

            public string Message { get; set; }
            public string Link { get; set; }
            public string ImageURL { get; set; }
            public string LinkDescription { get; set; }
            public string LinkTitle { get; set; }
        }

        private void sendSyndicationPendingApprovalNotifications()
        {
            using (MainEntities db = ContextFactory.Main)
            {
                if (db.Connection.State != System.Data.ConnectionState.Open)
                    db.Connection.Open();
                using (DbCommand cmd = db.CreateStoreCommand("spGetNotificationsToSend_SYND", System.Data.CommandType.StoredProcedure))
                {
                    using (DbDataReader reader = cmd.ExecuteReader())
                    {
                        #region read in information for items to send

                        List<SyndicationTemplate> templates = db.Translate<SyndicationTemplate>(reader).ToList();

                        reader.NextResult();

                        List<SyndicationNewNotificationToSendItem> itemsToSend = db.Translate<SyndicationNewNotificationToSendItem>(reader).ToList();

                        reader.NextResult();

                        #endregion

                        // there's only ever going to be one platform template to use
                        SyndicationTemplate platformTemplate = (from t in templates where t.NotificationMethodID == (long)NotificationMethodEnum.Platform select t).FirstOrDefault();

                        // for each user
                        foreach (long userID in (from i in itemsToSend select i.UserID).Distinct().ToList())
                        {

                            List<SyndicationNewNotificationToSendItem> itemsForUser = (from i in itemsToSend where i.UserID == userID select i).ToList();
                            TimeZoneInfo timeZone = ProviderFactory.TimeZones.GetTimezoneForUserID(userID);

                            // for each syndicator
                            foreach (long syndicatorID in (from i in itemsForUser select i.SyndicatorID).Distinct().ToList())
                            {

                                SyndicationTemplate emailTemplate = (
                                    from t in templates
                                    where t.NotificationMethodID == (long)NotificationMethodEnum.HtmlEmail
                                    && t.SyndicatorID.HasValue
                                    && t.SyndicatorID.Value == syndicatorID
                                    select t
                                ).FirstOrDefault();

                                if (emailTemplate == null)
                                {
                                    emailTemplate = (
                                        from t in templates
                                        where t.NotificationMethodID == (long)NotificationMethodEnum.HtmlEmail
                                        && t.SyndicatorID.HasValue == false
                                        select t
                                    ).FirstOrDefault();
                                }

                                List<SyndicationNewNotificationToSendItem> itemsForUserAndSyndicator = (from i in itemsForUser where i.SyndicatorID == syndicatorID select i).ToList();

                                // for each post

                                foreach (long postID in (from i in itemsForUserAndSyndicator select i.PostID).Distinct().ToList())
                                {
                                    List<SyndicationNewNotificationToSendItem> itemsForUserSyndicatorPost = (from i in itemsForUserAndSyndicator where i.PostID == postID select i).ToList();

                                    List<SyndicationNewNotificationToSendItem> platformNotis = (
                                        from i in itemsForUserSyndicatorPost
                                        where i.NotificationMethodID == (long)NotificationMethodEnum.Platform
                                        select i
                                    ).ToList();
                                    List<SyndicationNewNotificationToSendItem> emailNotis = (
                                        from i in itemsForUserSyndicatorPost
                                        where i.NotificationMethodID == (long)NotificationMethodEnum.HtmlEmail
                                        select i
                                    ).ToList();

                                    #region platform notifications

                                    // do the platform items
                                    if (platformNotis.Any())
                                    {
                                        List<long> notiRecipientIDs = new List<long>();
                                        foreach (SyndicationNewNotificationToSendItem item in platformNotis)
                                        {
                                            notiRecipientIDs.Add(item.NotificationRecipientID);

                                            //Insert Into NotificationTarget
                                            NotificationTarget notificationTarget = new NotificationTarget();

                                            notificationTarget.NotificationMethodID = (long)NotificationMethodEnum.Platform;
                                            notificationTarget.Address = string.Empty;
                                            notificationTarget.Subject = string.Empty;
                                            notificationTarget.Body = string.Empty;
                                            notificationTarget.NotificationStatusID = Convert.ToInt64(NotificationStatusesEnum.Success);
                                            long NotificationTargetID = addNotificationTarget(notificationTarget);

                                            //Update NotififcationRecipient with NotificationTargetID
                                            updateNotififcationRecipient(new List<long> { item.NotificationRecipientID }, NotificationTargetID);
                                        }
                                    }

                                    #endregion

                                    #region email notifications

                                    // do the email items
                                    if (emailNotis.Any())
                                    {
                                        try
                                        {


                                            List<long> notiRecipientIDs = new List<long>();


                                            if (emailTemplate != null)
                                            {
                                                PendingApprovalTemplateDTO templateDTO = JsonConvert.DeserializeObject<PendingApprovalTemplateDTO>(emailTemplate.Body);
                                                string body = templateDTO.body;


                                                SyndicationNewNotificationToSendItem firstItem = (from e in emailNotis select e).FirstOrDefault();
                                                if (firstItem != null)
                                                {

                                                    string emailAddress = firstItem.EmailAddress;
                                                    //emailAddress = "s.abraham@socialdealer.com";

                                                    if (!string.IsNullOrWhiteSpace(emailAddress))
                                                    {
                                                        DateTime? scheduleDate = firstItem.ScheduleDate;

                                                        #region get or create the unsubscribe token

                                                        AccessToken accessToken = db.AccessTokens.FirstOrDefault(a => a.UserID == userID && (a.RemainingUses == null || a.RemainingUses > 0) && a.AccessTokenTypeID == (long)AccessTokenTypeEnum.SyndicationUnsubscribe);

                                                        string unsubscribeToken = string.Empty;
                                                        if (accessToken == null)
                                                        {
                                                            SyndicationUnsubscribeDTO tokenRequest = new SyndicationUnsubscribeDTO()
                                                            {
                                                                UserID = userID, //UserID of user that the link is for                
                                                                NotificationMessageTypeID = emailTemplate.NotificationMessageTypeID,
                                                                NotificationTemplateID = emailTemplate.ID,
                                                                RemainingUses = 1,
                                                                TimeTillExpiration = null,
                                                                ResellerID = emailTemplate.ResellerID.GetValueOrDefault()
                                                            };
                                                            AccessTokenBaseDTO tok = ProviderFactory.Security.CreateAccessToken(tokenRequest);

                                                            unsubscribeToken = tok.Token.ToString();
                                                        }
                                                        else
                                                        {
                                                            unsubscribeToken = accessToken.Token.ToString();
                                                        }

                                                        if (!string.IsNullOrWhiteSpace(unsubscribeToken))
                                                        {
                                                            body = body.Replace("[$UnsubscribeLink$]", string.Format("http://platform.socialintegration.com/access/{0}", unsubscribeToken));
                                                        }
                                                        else
                                                        {
                                                            body = body.Replace("[$UnsubscribeLink$]", "#");
                                                        }

                                                        #endregion

                                                        #region post date and time

                                                        string PostDateTime = string.Empty;
                                                        if (scheduleDate.HasValue)
                                                        {
                                                            DateTime dt = new SIDateTime(scheduleDate.Value, timeZone).LocalDate.Value;

                                                            string postDateTimeTimeZone = timeZone.Id;

                                                            body = body.Replace("[$PostDate$]", dt.ToString(CultureInfo.InvariantCulture) + " " + postDateTimeTimeZone);
                                                        }
                                                        else
                                                        {
                                                            body = body.Replace("[$PostDate$]", "Immediate Post");
                                                        }

                                                        #endregion

                                                        #region account list

                                                        string accountList = "<ul>";
                                                        foreach (var item in emailNotis)
                                                        {
                                                            notiRecipientIDs.Add(item.NotificationRecipientID);
                                                            accountList = accountList +
                                                                string.Format("<li>{0} ({1})</li>", item.AccountName, item.SocialNetworkName);
                                                        }
                                                        accountList = accountList + "</ul>";

                                                        body = body.Replace("[$AccountsList$]", accountList);

                                                        #endregion

                                                        #region message, link and image

                                                        string PostMessage = firstItem.Message;
                                                        body = body.Replace("[$PostDescription$]", PostMessage);

                                                        switch ((PostTypeEnum)firstItem.PostTypeID)
                                                        {
                                                            case PostTypeEnum.Photo:

                                                                string BaseURL = Settings.GetSetting("site.baseurl");
                                                                if (!BaseURL.EndsWith("/"))
                                                                    BaseURL += "/";

                                                                PostImage postImage = db.PostImages.Where(pi => pi.PostID == postID).FirstOrDefault();

                                                                //Post Photo
                                                                //string postPhoto = publishImageUploadBaseURL + postImage.URL;
                                                                //string img = "<img width=\"400px\" src=\"" + postPhoto + "\">";

                                                                string img = string.Format("<img width='400' src='{0}ImageSizer?image=/UploadedPostImages/{1}&w=400' />", BaseURL, postImage.URL);

                                                                body = body.Replace("[$PostImageorLink$]", img);

                                                                break;

                                                            case PostTypeEnum.Link:

                                                                string postLink = firstItem.Link;
                                                                body = body.Replace("[$PostImageorLink$]", postLink);

                                                                break;

                                                            default:
                                                                body = body.Replace("[$PostImageorLink$]", "");
                                                                break;
                                                        }

                                                        #endregion

                                                        #region create approval token

                                                        PostApprovalAccessTokenDTO token = new PostApprovalAccessTokenDTO()
                                                        {
                                                            RemainingUses = null,
                                                            TimeTillExpiration = new TimeSpan(15, 0, 0, 0),
                                                            UserID = userID,
                                                            PostID = postID,
                                                            PostTargetID = firstItem.PostTargetID
                                                        };
                                                        token = (PostApprovalAccessTokenDTO)ProviderFactory.Security.CreateAccessToken(token);

                                                        string baseURL = Settings.GetSetting("site.baseurl");
                                                        switch (syndicatorID)
                                                        {
                                                            case 1:
                                                            case 2:
                                                            case 3:
                                                            case 4:
                                                            case 8:
                                                                baseURL = "http://platform.socialdealer.com";
                                                                break;
                                                        }

                                                        string platformURL = string.Format("{0}/access/{1}", baseURL, token.Token.ToString("D"));

                                                        body = body.Replace("[$NotificationLink$]", platformURL);

                                                        #endregion

                                                        #region tokenize message

                                                        string subject = emailTemplate.Subject;
                                                        subject = ProviderFactory.Social.TokenizeMessage(subject, firstItem.AccountName, firstItem.AccountPhone, firstItem.AccountURL);
                                                        body = ProviderFactory.Social.TokenizeMessage(body, firstItem.AccountName, firstItem.AccountPhone, firstItem.AccountURL);

                                                        #endregion

                                                        #region create target and send

                                                        //Insert Into NotificationTarget
                                                        NotificationTarget target = new NotificationTarget();


                                                        target.NotificationMethodID = (long)NotificationMethodEnum.HtmlEmail;
                                                        target.Address = emailAddress;
                                                        target.Subject = subject;
                                                        target.Body = body;
                                                        target.NotificationStatusID = (long)NotificationStatusesEnum.Pending;

                                                        long NotificationTargetID = addNotificationTarget(target);

                                                        //Update NotififcationRecipient with NotificationTargetID
                                                        updateNotififcationRecipient(notiRecipientIDs, NotificationTargetID);

                                                        //Send Email
                                                        string sendFromEmailAddress = Settings.GetSetting("site.SendFromEmailAddress", "notifications@socialdealer.com");
                                                        string address = target.Address;
                                                        if (address == "reynolds@kosloskey.com")
                                                            address = "reynolds@autostartups.com";

                                                        EmailManager emailManager = new EmailManager();
                                                        bool sendResult = emailManager.sendNotification(
                                                            "Pending Post Approval", target.Subject, target.Body,
                                                            sendFromEmailAddress, address,
                                                            string.Empty, string.Empty)
                                                        ;

                                                        if (sendResult == true)
                                                        {
                                                            updateNotificationTarget(NotificationTargetID, NotificationStatusesEnum.Success);
                                                        }
                                                        else
                                                        {
                                                            updateNotificationTarget(NotificationTargetID, NotificationStatusesEnum.Failure);
                                                        }

                                                        #endregion
                                                    }
                                                }
                                            }
                                        }
                                        catch (Exception ex)
                                        {
                                            ProviderFactory.Logging.LogException(ex);
                                        }
                                    }

                                    #endregion

                                } // end for each post
                            } // end for each syndicator
                        } // end for each user

                        #region old code
                        //if (posts.Any())
                        //{
                        //    //List<SocialNetwork> socialNetworks = (from s in db.SocialNetworks select s).ToList();

                        //    List<NewNotificationToSendItem> toSend = db.Translate<NewNotificationToSendItem>(reader).ToList();

                        //    foreach (long userID in (from ts in toSend select ts.UserID).Distinct())
                        //    {
                        //        foreach (long messageTemplateID in (from ts2 in toSend where ts2.UserID == userID select ts2.NotificationTemplateID).Distinct())
                        //        {
                        //            List<NewNotificationToSendItem> itemsForThisUserandTemplate = (
                        //                from ts3 in toSend
                        //                where ts3.UserID == userID && ts3.NotificationTemplateID == messageTemplateID
                        //                select ts3
                        //            ).ToList();

                        //            NotificationTemplate template = (from t in templates where t.ID == messageTemplateID select t).FirstOrDefault();

                        //            //long AccountId = (from ts in itemsForThisUserandTemplate select ts.AccountID).FirstOrDefault();
                        //            string Address = (from ts in itemsForThisUserandTemplate select ts.EmailAddress).FirstOrDefault();

                        //            switch ((NotificationMethodEnum)template.NotificationMethodID)
                        //            {
                        //                case NotificationMethodEnum.HtmlEmail:
                        //                    {
                        //                        try
                        //                        {
                        //                            #region HtmlEmail


                        //                            PendingApprovalTemplateDTO TemplateDTO = JsonConvert.DeserializeObject<PendingApprovalTemplateDTO>(template.Body);

                        //                            //AccessToken accessToken = db.AccessTokens.FirstOrDefault(a => a.UserID == userID && a.RemainingUses.GetValueOrDefault() > 0 && a.AccessTokenTypeID == (long) AccessTokenTypeEnum.SyndicationUnsubscribe);
                        //                            AccessToken accessToken = db.AccessTokens.FirstOrDefault(a => a.UserID == userID && (a.RemainingUses == null || a.RemainingUses > 0) && a.AccessTokenTypeID == (long)AccessTokenTypeEnum.SyndicationUnsubscribe);

                        //                            string Token = string.Empty;
                        //                            if (accessToken == null)
                        //                            {
                        //                                SyndicationUnsubscribeDTO tokenRequest = new SyndicationUnsubscribeDTO()
                        //                                {
                        //                                    UserID = userID, //UserID of user that the link is for                
                        //                                    NotificationMessageTypeID = template.NotificationMessageTypeID,
                        //                                    NotificationTemplateID = template.ID,
                        //                                    RemainingUses = 1,
                        //                                    TimeTillExpiration = null,
                        //                                    ResellerID = template.ResellerID.GetValueOrDefault()
                        //                                };

                        //                                AccessTokenBaseDTO tok = ProviderFactory.Security.CreateAccessToken(tokenRequest);

                        //                                Token = tok.Token.ToString();
                        //                            }
                        //                            else
                        //                            {
                        //                                Token = accessToken.Token.ToString();
                        //                            }

                        //                            List<long> approvalIDs = (from i in itemsForThisUserandTemplate select i.EntityID).Distinct().ToList();

                        //                            var approvalTargets = (
                        //                                from a in db.PostApprovals
                        //                                join pt in db.PostTargets on a.PostTargetID equals pt.ID
                        //                                where approvalIDs.Contains(a.ID)
                        //                                select new
                        //                                {
                        //                                    PostTargetID = pt.ID,
                        //                                    PostApprovalID = a.ID,
                        //                                    PostID = pt.PostID
                        //                                }
                        //                            ).ToList();


                        //                            //foreach (var item in itemsForThisUserandTemplate)
                        //                            //{

                        //                            List<long> NotififcationRecipientIDs = new List<long>();
                        //                            string AccountList = string.Empty;
                        //                            foreach (var item1 in itemsForThisUserandTemplate)
                        //                            {
                        //                                NotififcationRecipientIDs.Add(item1.NotificationRecipientID);
                        //                                AccountList = AccountList + "<li>" + item1.AccountName + "</li>";
                        //                            }
                        //                            AccountList = "<ul>" + AccountList + "</ul>";

                        //                            var item = itemsForThisUserandTemplate.FirstOrDefault();

                        //                            //PostTargetPublishInfoDTO info = ProviderFactory.Social.GetPostTargetPublishInfo(PostID, null);


                        //                            long PostID = (
                        //                                from x in approvalTargets
                        //                                where x.PostApprovalID == item.EntityID
                        //                                select x.PostID
                        //                            ).FirstOrDefault();
                        //                            long postTargetID = (
                        //                                from x in approvalTargets
                        //                                where x.PostApprovalID == item.EntityID
                        //                                select x.PostTargetID
                        //                            ).FirstOrDefault();

                        //                            Post post = (
                        //                                from p in db.Posts
                        //                                where p.ID == PostID
                        //                                select p
                        //                            ).FirstOrDefault();
                        //                            PostTarget postTarget = (
                        //                                from p in db.PostTargets
                        //                                where p.ID == postTargetID
                        //                                select p
                        //                            ).FirstOrDefault();


                        //                            TimeZoneInfo timeZone = ProviderFactory.TimeZones.GetTimezoneForUserID(item.UserID);

                        //                            PostRevision PostRevision = db.PostRevisions.Where(pr => pr.PostID == PostID).SingleOrDefault();

                        //                            string EmailBody = TemplateDTO.body;

                        //                            //Company Logo
                        //                            //string CompanyLogo = string.Empty;
                        //                            //EmailBody = EmailBody.Replace("[$CompanyLogo$]", CompanyLogo);

                        //                            //Post Date & Time
                        //                            string PostDateTime = string.Empty;
                        //                            if (post.ScheduleDate != null)
                        //                            {
                        //                                DateTime dt = new SIDateTime(postTarget.ScheduleDate.Value, timeZone).LocalDate.Value;

                        //                                string PostDateTime_TimeZone = timeZone.Id;

                        //                                EmailBody = EmailBody.Replace("[$PostDate$]", dt.ToString(CultureInfo.InvariantCulture) + " " + PostDateTime_TimeZone);
                        //                            }
                        //                            else
                        //                            {
                        //                                EmailBody = EmailBody.Replace("[$PostDate$]", "Immediate Post");
                        //                            }

                        //                            //Post Message
                        //                            string PostMessage = PostRevision.Message;
                        //                            EmailBody = EmailBody.Replace("[$PostDescription$]", PostMessage);

                        //                            if (post.PostType.ID == (long)PostTypeEnum.Link)
                        //                            {
                        //                                //Post Link
                        //                                string PostLink = PostRevision.Link;
                        //                                EmailBody = EmailBody.Replace("[$PostImageorLink$]", PostLink);

                        //                                ////Post Link
                        //                                //string LinkDescription = PostRevision.LinkDescription;
                        //                                //EmailBody = EmailBody.Replace("[$PostImageorLink$]", LinkDescription);
                        //                            }
                        //                            else if (post.PostType.ID == (long)PostTypeEnum.Photo)
                        //                            {
                        //                                string PublishImageUploadBaseURL = Settings.GetSetting("site.PublishImageUploadURL");
                        //                                PostImage postImage = db.PostImages.Where(pi => pi.PostID == PostID).FirstOrDefault();

                        //                                //Post Photo
                        //                                string PostPhoto = PublishImageUploadBaseURL + postImage.URL;

                        //                                string img = "<img width=\"400px\" Height=\"400px\" src=\"" + PostPhoto + "\">";

                        //                                EmailBody = EmailBody.Replace("[$PostImageorLink$]", img);
                        //                            }
                        //                            else
                        //                            {
                        //                                EmailBody = EmailBody.Replace("[$PostImageorLink$]", "");
                        //                            }

                        //                            //URL to Notification Page in Platform

                        //                            PostApprovalAccessTokenDTO token = new PostApprovalAccessTokenDTO()
                        //                            {
                        //                                RemainingUses = null,
                        //                                TimeTillExpiration = new TimeSpan(15, 0, 0, 0),
                        //                                UserID = item.UserID,
                        //                                PostID = (post == null) ? (long?)null : post.ID,
                        //                                PostTargetID = (postTarget == null) ? (long?)null : postTarget.ID
                        //                            };
                        //                            token = (PostApprovalAccessTokenDTO)ProviderFactory.Security.CreateAccessToken(token);

                        //                            string baseURL = Settings.GetSetting("site.baseurl");
                        //                            string platformURL = string.Format("{0}/access/{1}", baseURL, token.Token.ToString("D"));

                        //                            EmailBody = EmailBody.Replace("[$NotificationLink$]", platformURL);

                        //                            EmailBody = EmailBody.Replace("[$AccountsList$]", AccountList);

                        //                            if (!string.IsNullOrEmpty(Token))
                        //                            {
                        //                                EmailBody = EmailBody.Replace("[$UnsubscribeLink$]", string.Format("http://platform.socialintegration.com/access/{0}", Token));
                        //                            }
                        //                            else
                        //                            {
                        //                                EmailBody = EmailBody.Replace("[$UnsubscribeLink$]", "#");
                        //                            }

                        #endregion


                        //                            //Insert Into NotificationTarget
                        //                            NotificationTarget notificationTarget = new NotificationTarget();

                        //                            EmailBody = ProviderFactory.Social.TokenizeMessage(EmailBody, item.AccountName, item.AccountPhone, item.AccountURL);

                        //                            notificationTarget.NotificationMethodID = template.NotificationMethodID;
                        //                            notificationTarget.Address = Address;
                        //                            notificationTarget.Subject = template.Subject;
                        //                            notificationTarget.Body = EmailBody;
                        //                            notificationTarget.NotificationStatusID = Convert.ToInt64(NotificationStatusesEnum.Pending);

                        //                            long NotificationTargetID = addNotificationTarget(notificationTarget);

                        //                            //Update NotififcationRecipient with NotificationTargetID
                        //                            updateNotififcationRecipient(NotififcationRecipientIDs, NotificationTargetID);

                        //                            //Send Email
                        //                            string SendFromEmailAddress = Settings.GetSetting("site.SendFromEmailAddress", "notifications@socialdealer.com");
                        //                            string address = notificationTarget.Address;
                        //                            if (address == "reynolds@kosloskey.com")
                        //                                address = "reynolds@autostartups.com";

                        //                            EmailManager emailManager = new EmailManager();
                        //                            bool sendResult = emailManager.sendNotification(
                        //                                "Pending Post Approval", notificationTarget.Subject, notificationTarget.Body,
                        //                                SendFromEmailAddress, address,
                        //                                string.Empty, string.Empty)
                        //                            ;

                        //                            if (sendResult == true)
                        //                            {
                        //                                updateNotificationTarget(NotificationTargetID, NotificationStatusesEnum.Success);
                        //                            }
                        //                            else
                        //                            {
                        //                                updateNotificationTarget(NotificationTargetID, NotificationStatusesEnum.Failure);
                        //                            }
                        //                            //}
                        //                            #endregion
                        //                        }
                        //                        catch (Exception ex)
                        //                        {
                        //                            ProviderFactory.Logging.LogException(ex);
                        //                        }

                        //                    }
                        //                    break;
                        //                case NotificationMethodEnum.TextEmail:
                        //                    break;
                        //                case NotificationMethodEnum.SMS:
                        //                    break;
                        //                case NotificationMethodEnum.Platform:
                        //                    {
                        //                        List<long> NotififcationRecipientIDs = new List<long>();

                        //                        // LOOP THROUGH ITEMS
                        //                        foreach (NewNotificationToSendItem item in itemsForThisUserandTemplate)
                        //                        {
                        //                            NotififcationRecipientIDs.Add(item.NotificationRecipientID);
                        //                        }
                        //                        //Insert Into NotificationTarget
                        //                        NotificationTarget notificationTarget = new NotificationTarget();

                        //                        notificationTarget.NotificationMethodID = template.NotificationMethodID;
                        //                        notificationTarget.Address = string.Empty;
                        //                        notificationTarget.Subject = string.Empty;
                        //                        notificationTarget.Body = string.Empty;
                        //                        notificationTarget.NotificationStatusID = Convert.ToInt64(NotificationStatusesEnum.Success);

                        //                        long NotificationTargetID = addNotificationTarget(notificationTarget);

                        //                        //Update NotififcationRecipient with NotificationTargetID
                        //                        updateNotififcationRecipient(NotififcationRecipientIDs, NotificationTargetID);
                        //                    }
                        //                    break;
                        //                default:
                        //                    break;
                        //            }
                        //        }
                        //    }


                        //}

                    }
                }


            }
        }

        private void sendDistributionPendingApprovalNotifications()
        {
            SecurityProvider sec = (SecurityProvider)ProviderFactory.Security;

            using (MainEntities db = ContextFactory.Main)
            {
                if (db.Connection.State != System.Data.ConnectionState.Open)
                    db.Connection.Open();
                using (DbCommand cmd = db.CreateStoreCommand(
                        "spGetNotificationsToSend2",
                        System.Data.CommandType.StoredProcedure,
                            new SqlParameter("NotificationMessageTypeID", (long)NotificationMessageTypeEnum.NonSyndicationPendingPostApproval)
                        ))
                {
                    using (DbDataReader reader = cmd.ExecuteReader())
                    {
                        List<NotificationTemplate> templates = db.Translate<NotificationTemplate>(reader).ToList();

                        reader.NextResult();

                        List<PostTargetNotification> posts = db.Translate<PostTargetNotification>(reader).ToList();

                        reader.NextResult();

                        if (posts.Any())
                        {
                            List<NewNotificationToSendItem> toSend = db.Translate<NewNotificationToSendItem>(reader).ToList();

                            #region figure out the templates

                            List<NotificationTemplate> allTemplates = (
                                from t in db.NotificationTemplates
                                where t.NotificationMessageTypeID == (long)NotificationMessageTypeEnum.NonSyndicationPendingPostApproval
                                select t
                            ).ToList();

                            //get the reseller for each receiving account
                            Dictionary<long, long> accountResellers = new Dictionary<long, long>();
                            foreach (long receivingAccountID in (from ts in toSend select ts.AccountID).Distinct())
                            {
                                ResellerDTO rdto = sec.GetResellerByAccountID(UserInfoDTO.System, receivingAccountID, TimeZoneInfo.Utc);
                                if (rdto != null)
                                {
                                    accountResellers.Add(receivingAccountID, rdto.ID.Value);
                                }
                            }

                            //get the email template to use for each reseller
                            Dictionary<long, NotificationTemplate> resellerEmailTemplates = new Dictionary<long, NotificationTemplate>();
                            Dictionary<long, NotificationTemplate> resellerPlatformTemplates = new Dictionary<long, NotificationTemplate>();
                            foreach (long resellerID in (from t in accountResellers.Values select t).Distinct())
                            {
                                NotificationTemplate tEmail = (
                                    from t in allTemplates
                                    where t.NotificationMethodID == (long)NotificationMethodEnum.HtmlEmail
                                    && t.ResellerID.HasValue && t.ResellerID.Value == resellerID
                                    select t
                                ).FirstOrDefault();
                                if (tEmail == null)
                                {
                                    tEmail = (
                                        from t in allTemplates
                                        where t.NotificationMethodID == (long)NotificationMethodEnum.HtmlEmail
                                        && !t.ResellerID.HasValue
                                        select t
                                    ).FirstOrDefault();
                                }
                                if (tEmail != null)
                                {
                                    resellerEmailTemplates.Add(resellerID, tEmail);
                                }

                                NotificationTemplate tPlat = (
                                    from t in allTemplates
                                    where t.NotificationMethodID == (long)NotificationMethodEnum.Platform
                                    && t.ResellerID.HasValue && t.ResellerID.Value == resellerID
                                    select t
                                ).FirstOrDefault();
                                if (tPlat == null)
                                {
                                    tPlat = (
                                        from t in allTemplates
                                        where t.NotificationMethodID == (long)NotificationMethodEnum.Platform
                                        && !t.ResellerID.HasValue
                                        select t
                                    ).FirstOrDefault();
                                }
                                if (tPlat != null)
                                {
                                    resellerPlatformTemplates.Add(resellerID, tPlat);
                                }
                            }

                            #endregion

                            #region email

                            {
                                // for each user
                                foreach (long userID in (from ts in toSend select ts.UserID).Distinct())
                                {
                                    // for each reseller
                                    foreach (long resellerID in resellerEmailTemplates.Keys)
                                    {
                                        // get the user items for this reseller's template
                                        List<NewNotificationToSendItem> itemsForUserAndReseller = (
                                            from ts in toSend
                                            join ar in accountResellers on ts.AccountID equals ar.Key
                                            join rt in resellerEmailTemplates on ar.Value equals rt.Key
                                            where rt.Key == resellerID && ts.UserID == userID && ts.NotificationMethodID == rt.Value.NotificationMethodID
                                            select ts
                                        ).ToList();

                                        // get the email template
                                        NotificationTemplate template = resellerEmailTemplates[resellerID];

                                        // get the email address
                                        string Address = (from ts in itemsForUserAndReseller select ts.EmailAddress).FirstOrDefault();

                                        //// get the platform template
                                        //NotificationTemplate tPlat = resellerPlatformTemplates[resellerID];

                                        try
                                        {
                                            #region Format Message


                                            NonSyndicationPendingPostApprovalDTO TemplateDTO = JsonConvert.DeserializeObject<NonSyndicationPendingPostApprovalDTO>(template.Body);

                                            string Token = getOrCreateUnsubscribeAccessToken(userID, template.NotificationMessageTypeID, template.ID, template.ResellerID.GetValueOrDefault());

                                            List<long> approvalIDs = (from i in itemsForUserAndReseller select i.EntityID).Distinct().ToList();

                                            var approvalTargets = (
                                                from apr in db.PostApprovals
                                                join pt in db.PostTargets on apr.PostTargetID equals pt.ID
                                                join c in db.Credentials on pt.CredentialID equals c.ID
                                                join a in db.Accounts on c.AccountID equals a.ID
                                                join sa in db.SocialApps on c.SocialAppID equals sa.ID
                                                join sn in db.SocialNetworks on sa.SocialNetworkID equals sn.ID
                                                where approvalIDs.Contains(apr.ID)
                                                select new
                                                {
                                                    PostTargetID = pt.ID,
                                                    AccountID = c.AccountID,
                                                    PostApprovalID = apr.ID,
                                                    PostID = pt.PostID,
                                                    SocialNetworkName = sn.Name,
                                                    SocialNetworkID = sa.SocialNetworkID,
                                                    AccountName = a.Name,
                                                    AccountDisplayName = a.DisplayName,
                                                    PageURL = c.URI
                                                }
                                            ).ToList();


                                            List<long> notififcationRecipientIDs = new List<long>();
                                            //string accountList = string.Empty;
                                            foreach (var item1 in itemsForUserAndReseller)
                                            {
                                                notififcationRecipientIDs.Add(item1.NotificationRecipientID);
                                                //accountList = accountList + "<li>" + item1.AccountName + "</li>";
                                            }
                                            //accountList = "<ul>" + accountList + "</ul>";

                                            var item = itemsForUserAndReseller.FirstOrDefault();

                                            // get a list of social networks affected
                                            List<long> socialNetworks = (from a in approvalTargets select a.SocialNetworkID).Distinct().ToList();


                                            long PostID = (
                                                from x in approvalTargets
                                                where x.PostApprovalID == item.EntityID
                                                select x.PostID
                                            ).FirstOrDefault();
                                            long postTargetID = (
                                                from x in approvalTargets
                                                where x.PostApprovalID == item.EntityID
                                                select x.PostTargetID
                                            ).FirstOrDefault();

                                            Post post = (
                                                from p in db.Posts
                                                where p.ID == PostID
                                                select p
                                            ).FirstOrDefault();
                                            PostTarget postTarget = (
                                                from p in db.PostTargets
                                                where p.ID == postTargetID
                                                select p
                                            ).FirstOrDefault();

                                            if (item != null)
                                            {
                                                TimeZoneInfo timeZone = ProviderFactory.TimeZones.GetTimezoneForUserID(item.UserID);

                                                PostRevision postRevision = db.PostRevisions.Where(pr => pr.PostID == PostID).SingleOrDefault();


                                                string emailBody = TemplateDTO.Body;

                                                //build up the locations block
                                                StringBuilder sbLoc = new StringBuilder();
                                                foreach (long accountID in (from t in approvalTargets select t.AccountID).Distinct().ToList())
                                                {
                                                    var accountTargets = (from a in approvalTargets where a.AccountID == accountID select a).ToList();

                                                    var first = (from a in accountTargets select a).FirstOrDefault();
                                                    var fb = (from a in accountTargets where a.SocialNetworkID == (long)SocialNetworkEnum.Facebook select a).FirstOrDefault();
                                                    var twit = (from a in accountTargets where a.SocialNetworkID == (long)SocialNetworkEnum.Twitter select a).FirstOrDefault();
                                                    var goog = (from a in accountTargets where a.SocialNetworkID == (long)SocialNetworkEnum.Google select a).FirstOrDefault();

                                                    string locPart = TemplateDTO.TemplateRepeatingBlock;

                                                    string accountName = first.AccountName.Trim();
                                                    if (!string.IsNullOrWhiteSpace(first.AccountDisplayName))
                                                    {
                                                        accountName = first.AccountDisplayName.Trim();
                                                    }

                                                    locPart = locPart.Replace("[$AccountName$]", accountName);

                                                    if (fb != null)
                                                    {
                                                        locPart = locPart.Replace("[$FacebookShowHide$]", "inline-block");
                                                        locPart = locPart.Replace("[$AccountFacebookURL$]", fb.PageURL);
                                                    }
                                                    else
                                                    {
                                                        locPart = locPart.Replace("[$FacebookShowHide$]", "none");
                                                    }
                                                    if (twit != null)
                                                    {
                                                        locPart = locPart.Replace("[$TwitterShowHide$]", "inline-block");
                                                        locPart = locPart.Replace("[$AccountTwitterURL$]", twit.PageURL);
                                                    }
                                                    else
                                                    {
                                                        locPart = locPart.Replace("[$TwitterShowHide$]", "none");
                                                    }
                                                    if (goog != null)
                                                    {
                                                        locPart = locPart.Replace("[$GooglePlusShowHide$]", "inline-block");
                                                        locPart = locPart.Replace("[$AccountGooglePlusURL$]", goog.PageURL);
                                                    }
                                                    else
                                                    {
                                                        locPart = locPart.Replace("[$GooglePlusShowHide$]", "none");
                                                    }

                                                    sbLoc.Append(locPart);
                                                }

                                                emailBody = emailBody.Replace("[$TemplateRepeatingBlock$]", sbLoc.ToString());

                                                emailBody = emailBody.Replace("[$PostTitle$]", post.Title);

                                                var submitterInfo =
                                                    (from u in db.Users where u.ID == post.UserID select new { FirstName = u.FirstName, LastName = u.LastName }).FirstOrDefault();
                                                string submitterName = "";
                                                if (!string.IsNullOrWhiteSpace(submitterInfo.FirstName))
                                                    submitterName = submitterInfo.FirstName;
                                                if (!string.IsNullOrWhiteSpace(submitterInfo.LastName))
                                                    submitterName = submitterName + " " + submitterInfo.LastName;
                                                submitterName = submitterName.Trim();

                                                emailBody = emailBody.Replace("[$SubmittedBy$]", submitterName);


                                                //Post Date & Time
                                                string PostDateTime = string.Empty;
                                                if (post.ScheduleDate != null)
                                                {
                                                    DateTime dt = new SIDateTime(postTarget.ScheduleDate.Value, timeZone).LocalDate.Value;

                                                    string PostDateTime_TimeZone = timeZone.Id;

                                                    emailBody = emailBody.Replace("[$ScheduleDate$]", dt.ToString(CultureInfo.InvariantCulture) + " " + PostDateTime_TimeZone);
                                                }
                                                else
                                                {
                                                    emailBody = emailBody.Replace("[$ScheduleDate$]", "Immediate Post");
                                                }

                                                //Post Message
                                                string PostMessage = postRevision.Message;
                                                emailBody = emailBody.Replace("[$PostMessage$]", PostMessage);

                                                switch ((PostTypeEnum)post.PostTypeID)
                                                {
                                                    case PostTypeEnum.Status:
                                                        emailBody = emailBody.Replace("[$PostType$]", "Status");
                                                        emailBody = emailBody.Replace("[$ImagesPostBlock$]", "");
                                                        emailBody = emailBody.Replace("[$LinkPostBlock$]", "");
                                                        break;

                                                    case PostTypeEnum.Photo:
                                                        emailBody = emailBody.Replace("[$PostType$]", "Photo");
                                                        emailBody = emailBody.Replace("[$LinkPostBlock$]", "");

                                                        string PublishImageUploadBaseURL = Settings.GetSetting("site.PublishImageUploadURL");
                                                        StringBuilder sbImages = new StringBuilder();

                                                        int imageNo = 0;
                                                        foreach (PostImage pImg in db.PostImages.Where(pi => pi.PostID == PostID))
                                                        {
                                                            imageNo++;
                                                            string imageURL = PublishImageUploadBaseURL + pImg.URL;
                                                            sbImages.AppendFormat("<li><a href=\"{0}\">Image {1}</a></li>", imageURL, imageNo);
                                                        }
                                                        string imageBlock = TemplateDTO.Image;
                                                        imageBlock = imageBlock.Replace("[$Image$]", sbImages.ToString());
                                                        emailBody = emailBody.Replace("[$ImagesPostBlock$]", imageBlock);
                                                        break;

                                                    case PostTypeEnum.Link:
                                                        emailBody = emailBody.Replace("[$PostType$]", "Link");
                                                        emailBody = emailBody.Replace("[$ImagesPostBlock$]", "");

                                                        string linkBlock = TemplateDTO.Link;
                                                        linkBlock = linkBlock.Replace("[$LinkTitle$]", postRevision.LinkTitle);
                                                        linkBlock = linkBlock.Replace("[$LinkURL$]", postRevision.Link);
                                                        linkBlock = linkBlock.Replace("[$Thumbnail$]", postRevision.ImageURL);
                                                        linkBlock = linkBlock.Replace("[$LinkDescription$]", postRevision.LinkDescription);

                                                        emailBody = emailBody.Replace("[$LinkPostBlock$]", linkBlock);


                                                        break;

                                                    case PostTypeEnum.Event:
                                                        emailBody = emailBody.Replace("[$PostType$]", "Event");
                                                        emailBody = emailBody.Replace("[$LinkPostBlock$]", "");
                                                        emailBody = emailBody.Replace("[$ImagesPostBlock$]", "");
                                                        break;
                                                    case PostTypeEnum.Video:
                                                        emailBody = emailBody.Replace("[$PostType$]", "Video");
                                                        emailBody = emailBody.Replace("[$LinkPostBlock$]", "");
                                                        emailBody = emailBody.Replace("[$ImagesPostBlock$]", "");
                                                        break;
                                                    case PostTypeEnum.Offer:
                                                        emailBody = emailBody.Replace("[$PostType$]", "Offer");
                                                        emailBody = emailBody.Replace("[$LinkPostBlock$]", "");
                                                        emailBody = emailBody.Replace("[$ImagesPostBlock$]", "");
                                                        break;
                                                    case PostTypeEnum.Question:
                                                        emailBody = emailBody.Replace("[$PostType$]", "Question");
                                                        emailBody = emailBody.Replace("[$LinkPostBlock$]", "");
                                                        emailBody = emailBody.Replace("[$ImagesPostBlock$]", "");
                                                        break;
                                                    default:
                                                        break;
                                                }

                                                //URL to Notification Page in Platform

                                                PostApprovalAccessTokenDTO token = new PostApprovalAccessTokenDTO()
                                                {
                                                    RemainingUses = null,
                                                    TimeTillExpiration = new TimeSpan(15, 0, 0, 0),
                                                    UserID = item.UserID,
                                                    PostID = (post == null) ? (long?)null : post.ID,
                                                    PostTargetID = (postTarget == null) ? (long?)null : postTarget.ID
                                                };
                                                token = (PostApprovalAccessTokenDTO)ProviderFactory.Security.CreateAccessToken(token);

                                                //string baseURL = Settings.GetSetting("site.baseurl");
                                                //string platformURL = string.Format("{0}/access/{1}", baseURL, token.Token.ToString("D"));

                                                //emailBody = emailBody.Replace("[$NotificationLink$]", platformURL);

                                                //emailBody = emailBody.Replace("[$AccountsList$]", accountList);

                                                //if (!string.IsNullOrEmpty(Token))
                                                //{
                                                //    emailBody = emailBody.Replace("[$UnsubscribeLink$]", string.Format("http://platform.socialintegration.com/access/{0}", Token));
                                                //}
                                                //else
                                                //{
                                                //    emailBody = emailBody.Replace("[$UnsubscribeLink$]", "#");
                                                //}


                                                //Insert Into NotificationTarget
                                                NotificationTarget notificationTarget = new NotificationTarget();

                                                emailBody = ProviderFactory.Social.TokenizeMessage(emailBody, item.AccountName, item.AccountPhone, item.AccountURL);

                                                notificationTarget.NotificationMethodID = template.NotificationMethodID;
                                                notificationTarget.Address = Address;
                                                notificationTarget.Subject = template.Subject;
                                                notificationTarget.Body = emailBody;
                                                notificationTarget.NotificationStatusID = Convert.ToInt64(NotificationStatusesEnum.Pending);

                                                long notificationTargetID = addNotificationTarget(notificationTarget);

                                                //Update NotififcationRecipient with NotificationTargetID
                                                updateNotififcationRecipient(notififcationRecipientIDs, notificationTargetID);

                                                //Send Email
                                                string SendFromEmailAddress = Settings.GetSetting("site.SendFromEmailAddress", "notifications@socialdealer.com");
                                                string address = notificationTarget.Address;
                                                if (address == "reynolds@kosloskey.com")
                                                    address = "reynolds@autostartups.com";

                                                EmailManager emailManager = new EmailManager();
                                                bool sendResult = emailManager.sendNotification(
                                                    "Pending Post Approval", notificationTarget.Subject, notificationTarget.Body,
                                                    SendFromEmailAddress, address,
                                                    string.Empty, string.Empty)
                                                ;

                                                if (sendResult == true)
                                                {
                                                    updateNotificationTarget(notificationTargetID, NotificationStatusesEnum.Success);
                                                }
                                                else
                                                {
                                                    updateNotificationTarget(notificationTargetID, NotificationStatusesEnum.Failure);
                                                }

                                            #endregion
                                            }
                                        }
                                        catch (Exception ex)
                                        {
                                            ProviderFactory.Logging.LogException(ex);
                                        }


                                    }
                                }
                            }

                            #endregion

                            #region platform
                            {
                                // for each user
                                foreach (long userID in (from ts in toSend select ts.UserID).Distinct())
                                {
                                    // for each reseller
                                    foreach (long resellerID in resellerPlatformTemplates.Keys)
                                    {
                                        // get the user items for this reseller's template
                                        List<NewNotificationToSendItem> itemsForUserAndReseller = (
                                            from ts in toSend
                                            join ar in accountResellers on ts.AccountID equals ar.Key
                                            join rt in resellerPlatformTemplates on ar.Value equals rt.Key
                                            where rt.Key == resellerID && ts.UserID == userID && ts.NotificationMethodID == rt.Value.NotificationMethodID
                                            select ts
                                        ).ToList();

                                        // get the template
                                        NotificationTemplate template = resellerPlatformTemplates[resellerID];

                                        //// get the email address
                                        //string Address = (from ts in itemsForUserAndReseller select ts.EmailAddress).FirstOrDefault();

                                        // get the platform template
                                        //NotificationTemplate tPlat = resellerPlatformTemplates[resellerID];

                                        try
                                        {
                                            #region Format Message

                                            List<long> notififcationRecipientIDs = new List<long>();

                                            // LOOP THROUGH ITEMS
                                            foreach (NewNotificationToSendItem item in itemsForUserAndReseller)
                                            {
                                                notififcationRecipientIDs.Add(item.NotificationRecipientID);

                                                NotificationTarget notificationTarget = new NotificationTarget();

                                                notificationTarget.NotificationMethodID = template.NotificationMethodID;
                                                notificationTarget.Address = string.Empty;
                                                notificationTarget.Subject = string.Empty;
                                                notificationTarget.Body = string.Empty;
                                                notificationTarget.NotificationStatusID = Convert.ToInt64(NotificationStatusesEnum.Success);

                                                long NotificationTargetID = addNotificationTarget(notificationTarget);

                                                updateNotififcationRecipient(new List<long> { item.NotificationRecipientID }, NotificationTargetID);
                                            }


                                            #endregion
                                        }
                                        catch (Exception ex)
                                        {
                                            ProviderFactory.Logging.LogException(ex);
                                        }


                                    }
                                }
                            }

                            #endregion


                        }

                    }
                }


            }
        }

        private void sendNewReviewNotifications()
        {
            using (MainEntities db = ContextFactory.Main)
            {
                if (db.Connection.State != System.Data.ConnectionState.Open)
                    db.Connection.Open();

                using (DbCommand cmd = db.CreateStoreCommand(
                        "spGetNotificationsToSend2",
                        System.Data.CommandType.StoredProcedure,
                            new SqlParameter("NotificationMessageTypeID", (long)NotificationMessageTypeEnum.NewReviewAlert)
                        ))
                {
                    using (DbDataReader reader = cmd.ExecuteReader())
                    {
                        List<NotificationTemplate> templates = db.Translate<NotificationTemplate>(reader).ToList();

                        reader.NextResult();

                        List<Review> reviews = db.Translate<Review>(reader).ToList();

                        reader.NextResult();

                        if (reviews.Any())
                        {
                            List<ReviewSource> reviewSources = (from r in db.ReviewSources select r).ToList();
                            List<NewNotificationToSendItem> toSend = db.Translate<NewNotificationToSendItem>(reader).ToList();

                            foreach (long userID in (from ts in toSend select ts.UserID).Distinct())
                            {
                                foreach (long messageTemplateID in (from ts2 in toSend where ts2.UserID == userID select ts2.NotificationTemplateID).Distinct())
                                {
                                    foreach (long accountID in (from ts3 in toSend where ts3.UserID == userID && ts3.NotificationTemplateID == messageTemplateID select ts3.AccountID).Distinct())
                                    {
                                        try
                                        {

                                            List<NewNotificationToSendItem> itemsForThisUserandTemplate = (
                                                from ts4 in toSend
                                                where ts4.UserID == userID &&
                                                      ts4.NotificationTemplateID == messageTemplateID &&
                                                      ts4.AccountID == accountID
                                                select ts4
                                            ).ToList();

                                            NotificationTemplate template = (from t in templates where t.ID == messageTemplateID select t).FirstOrDefault();

                                            //long AccountId = (from ts in itemsForThisUserandTemplate select ts.AccountID).FirstOrDefault();
                                            string Address = (from ts in itemsForThisUserandTemplate select ts.EmailAddress).FirstOrDefault();

                                            switch ((NotificationMethodEnum)template.NotificationMethodID)
                                            {
                                                case NotificationMethodEnum.HtmlEmail:
                                                    {
                                                        #region HtmlEmail

                                                        NewReviewAlertTemplateDTO TemplateDTO = JsonConvert.DeserializeObject<NewReviewAlertTemplateDTO>(template.Body);
                                                        string EmailBody = TemplateDTO.body;


                                                        //GetEntityDTO<AccountDTO, AccountOptionsDTO> result = ProviderFactory.Security.GetAccountByID(UserInfoDTO.System, accountID);
                                                        string accountName = (
                                                            from a in db.Accounts
                                                            where a.ID == accountID
                                                            select a.DisplayName == null ? a.Name : a.DisplayName
                                                        ).SingleOrDefault();

                                                        //Replace Account Name
                                                        EmailBody = EmailBody.Replace("[$AccountName$]", accountName);

                                                        //Replcae NumberOfNewReview, PositiveNewReview, NegativeNewReview
                                                        int NumberOfNewReview = (from r in reviews where r.AccountID == accountID select r).Count();
                                                        int PositiveNewReview = (from r in reviews where r.RatingPolarity != null && r.RatingPolarity == true && r.AccountID == accountID select r).Count();
                                                        int NegativeNewReview = (from r in reviews where r.RatingPolarity != null && r.RatingPolarity == false && r.AccountID == accountID select r).Count();

                                                        EmailBody = EmailBody.Replace("[$NumberOfNewReview$]", NumberOfNewReview.ToString(CultureInfo.InvariantCulture))
                                                                        .Replace("[$PositiveNewReview$]", PositiveNewReview.ToString(CultureInfo.InvariantCulture))
                                                                        .Replace("[$NegativeNewReview$]", NegativeNewReview.ToString(CultureInfo.InvariantCulture));

                                                        //[$NumberOfNewReview$] New Reviews(s) ([$PositiveNewReview$] Positive and [$NegativeNewReview$] Negative)

                                                        string Reviewcontent = string.Empty;

                                                        List<long> NotififcationRecipientIDs = new List<long>();

                                                        // LOOP THROUGH ITEMS
                                                        foreach (NewNotificationToSendItem item in itemsForThisUserandTemplate)
                                                        {
                                                            NotififcationRecipientIDs.Add(item.NotificationRecipientID);

                                                            string templateRepeatingBlock = TemplateDTO.TemplateRepeatingBlock;
                                                            if (templateRepeatingBlock != null && !string.IsNullOrWhiteSpace(templateRepeatingBlock))
                                                            {

                                                                Review review = (from r in reviews where r.ID == item.EntityID select r).FirstOrDefault();
                                                                if (review != null)
                                                                {
                                                                    ReviewSource reviewSource = (from rs in reviewSources where rs.ID == review.ReviewSourceID select rs).FirstOrDefault();

                                                                    var accounts_ReviewSource = (from ar in db.Accounts_ReviewSources
                                                                                                 where ar.AccountID == review.AccountID
                                                                                                       && ar.ReviewSourceID == review.ReviewSourceID
                                                                                                 select ar).SingleOrDefault();

                                                                    if (reviewSource != null && !string.IsNullOrWhiteSpace(reviewSource.ImagePath))
                                                                    {
                                                                        templateRepeatingBlock = templateRepeatingBlock.Replace("[$ReviewSourceImagePath$]", reviewSource.ImagePath);
                                                                    }
                                                                    else
                                                                    {
                                                                        templateRepeatingBlock = templateRepeatingBlock.Replace("[$ReviewSourceImagePath$]", "");
                                                                    }

                                                                    templateRepeatingBlock = templateRepeatingBlock.Replace("[$ReviewRating$]", review.Rating.HasValue ? review.Rating.ToString() : "0");
                                                                    templateRepeatingBlock = templateRepeatingBlock.Replace("[$ReviewMaxRating$]", "5");
                                                                    templateRepeatingBlock = templateRepeatingBlock.Replace("[$ReviewDate$]", review.ReviewDate.HasValue ? review.ReviewDate.Value.ToShortDateString() : "");
                                                                    templateRepeatingBlock = templateRepeatingBlock.Replace("[$ReviewerName$]", string.IsNullOrWhiteSpace(review.ReviewerName) ? "" : review.ReviewerName);
                                                                    templateRepeatingBlock = templateRepeatingBlock.Replace("[$ReviewText$]", string.IsNullOrWhiteSpace(review.ReviewText) ? "" : review.ReviewText);

                                                                    templateRepeatingBlock = templateRepeatingBlock.Replace("[$AccountReviewSourceURL$]", accounts_ReviewSource.URL);

                                                                    if (review.RatingPolarity == true)
                                                                    {
                                                                        templateRepeatingBlock = templateRepeatingBlock.Replace("[$ReviewgPolarity$]", "Positive")
                                                                                                    .Replace("[$ReviewgPolarityColor$]", "green");
                                                                    }
                                                                    else if (review.RatingPolarity == false)
                                                                    {
                                                                        templateRepeatingBlock = templateRepeatingBlock.Replace("[$ReviewgPolarity$]", "Negative")
                                                                                                    .Replace("[$ReviewgPolarityColor$]", "red");
                                                                    }
                                                                    else
                                                                    {
                                                                        templateRepeatingBlock = templateRepeatingBlock.Replace("[$ReviewgPolarity$]", "")
                                                                                                    .Replace("[$ReviewgPolarityColor$]", "gray");
                                                                    }



                                                                    Reviewcontent += templateRepeatingBlock;
                                                                }
                                                            }

                                                            EmailBody = ProviderFactory.Social.TokenizeMessage(EmailBody, item.AccountName, item.AccountPhone, item.AccountURL);
                                                        }

                                                        EmailBody = EmailBody.Replace("[$TemplateRepeatingBlock$]", Reviewcontent);

                                                        string Subject = template.Subject + " for " + accountName;

                                                        //Insert Into NotificationTarget
                                                        NotificationTarget notificationTarget = new NotificationTarget();

                                                        notificationTarget.NotificationMethodID = template.NotificationMethodID;
                                                        notificationTarget.Address = Address;
                                                        notificationTarget.Subject = Subject;
                                                        notificationTarget.Body = EmailBody;
                                                        notificationTarget.NotificationStatusID = Convert.ToInt64(NotificationStatusesEnum.Pending);

                                                        string targetHash = Security.GetHash(string.Format("NewReviewAlertEmail-{0}-{1}-{2}", EmailBody, Subject, Address), 20);
                                                        long? duplicateTargetID = (
                                                            from t in db.NotificationTargets
                                                            where t.TargetHash == targetHash
                                                            select t.ID
                                                        ).FirstOrDefault();

                                                        if (duplicateTargetID.HasValue && duplicateTargetID.Value > 0)
                                                        {
                                                            Auditor
                                                                .New(AuditLevelEnum.Error, AuditTypeEnum.NotificationService, UserInfoDTO.System, "")
                                                                .SetMessage("Duplicate NotificationTarget detected with Hash [{0}], ID [{1}], not sending new email", targetHash, duplicateTargetID)
                                                                .Save(ProviderFactory.Logging)
                                                            ;
                                                        }
                                                        else
                                                        {
                                                            notificationTarget.TargetHash = targetHash;

                                                            long NotificationTargetID = addNotificationTarget(notificationTarget);

                                                            //Update NotififcationRecipient with NotificationTargetID
                                                            updateNotififcationRecipient(NotififcationRecipientIDs, NotificationTargetID);

                                                            //Send Email
                                                            string SendFromEmailAddress = Settings.GetSetting("site.SendFromEmailAddress", "notifications@socialdealer.com");

                                                            EmailManager emailManager = new EmailManager();
                                                            bool sendResult = emailManager.sendNotification(
                                                                "New Review", notificationTarget.Subject, notificationTarget.Body,
                                                                SendFromEmailAddress, notificationTarget.Address,
                                                                string.Empty, string.Empty)
                                                            ;

                                                            if (sendResult == true)
                                                            {
                                                                updateNotificationTarget(NotificationTargetID, NotificationStatusesEnum.Success);
                                                            }
                                                            else
                                                            {
                                                                updateNotificationTarget(NotificationTargetID, NotificationStatusesEnum.Failure);
                                                            }

                                                            // After done, need to create the NotificationTarget record, 
                                                            // and set the NotificationTargetID in each of the NotififcationRecipient records for this
                                                            // email.
                                                        }
                                                        #endregion
                                                    }
                                                    break;
                                                case NotificationMethodEnum.TextEmail:
                                                    break;
                                                case NotificationMethodEnum.SMS:
                                                    break;
                                                case NotificationMethodEnum.Platform:
                                                    {
                                                        #region Platform

                                                        List<long> NotififcationRecipientIDs = new List<long>();

                                                        // LOOP THROUGH ITEMS
                                                        foreach (NewNotificationToSendItem item in itemsForThisUserandTemplate)
                                                        {
                                                            NotififcationRecipientIDs.Add(item.NotificationRecipientID);
                                                        }

                                                        //Insert Into NotificationTarget
                                                        NotificationTarget notificationTarget = new NotificationTarget();

                                                        notificationTarget.NotificationMethodID = template.NotificationMethodID;
                                                        notificationTarget.Address = string.Empty;
                                                        notificationTarget.Subject = string.Empty;
                                                        notificationTarget.Body = string.Empty;
                                                        notificationTarget.NotificationStatusID = Convert.ToInt64(NotificationStatusesEnum.Success);

                                                        long NotificationTargetID = addNotificationTarget(notificationTarget);

                                                        //Update NotififcationRecipient with NotificationTargetID
                                                        updateNotififcationRecipient(NotififcationRecipientIDs, NotificationTargetID);

                                                        #endregion
                                                    }
                                                    break;
                                                default:
                                                    break;
                                            }


                                        }
                                        catch (Exception ex)
                                        {
                                            ProviderFactory.Logging.LogException(ex);
                                        }
                                    }// end accountID loop
                                } // end messageTemplateID loop
                            } // end userID loop
                        }
                    }
                }
            }
        }

        private void sendSyndicationSignupCCNotifications()
        {
            using (MainEntities db = ContextFactory.Main)
            {
                if (db.Connection.State != System.Data.ConnectionState.Open)
                    db.Connection.Open();

                using (DbCommand cmd = db.CreateStoreCommand(
                        "spGetNotificationsToSend2",
                        System.Data.CommandType.StoredProcedure,
                            new SqlParameter("NotificationMessageTypeID", (long)NotificationMessageTypeEnum.SyndicationSignupCC)
                        ))
                {
                    using (DbDataReader reader = cmd.ExecuteReader())
                    {
                        List<NotificationTemplate> templates = db.Translate<NotificationTemplate>(reader).ToList();

                        reader.NextResult();


                        List<NewNotificationToSendItem> toSend = db.Translate<NewNotificationToSendItem>(reader).ToList();

                        foreach (long userID in (from ts in toSend select ts.UserID).Distinct())
                        {
                            foreach (long messageTemplateID in (from ts2 in toSend where ts2.UserID == userID select ts2.NotificationTemplateID).Distinct())
                            {
                                foreach (long accountID in (from ts3 in toSend where ts3.UserID == userID && ts3.NotificationTemplateID == messageTemplateID select ts3.AccountID).Distinct())
                                {
                                    try
                                    {

                                        foreach (NewNotificationToSendItem item in
                                            (
                                                from ts4 in toSend
                                                where ts4.UserID == userID &&
                                                      ts4.NotificationTemplateID == messageTemplateID &&
                                                      ts4.AccountID == accountID
                                                select ts4
                                            ).ToList()
                                        )
                                        {


                                            NotificationTemplate template = (from t in templates where t.ID == messageTemplateID select t).FirstOrDefault();

                                            string address = item.EmailAddress;
                                            var userInfo = (
                                                from u in db.Users
                                                where u.ID == item.EntityID
                                                select new
                                                {
                                                    FirstName = u.FirstName,
                                                    LastName = u.LastName,
                                                    EmailAddress = u.EmailAddress,
                                                    MemberOfAccountID = u.MemberOfAccountID
                                                }
                                            ).FirstOrDefault();

                                            if (userInfo != null)
                                            {

                                                string accountName = (
                                                    from a in db.Accounts
                                                    where a.ID == userInfo.MemberOfAccountID
                                                    select a.DisplayName == null ? a.Name : a.DisplayName
                                                ).SingleOrDefault();

                                                switch ((NotificationMethodEnum)template.NotificationMethodID)
                                                {
                                                    case NotificationMethodEnum.HtmlEmail:
                                                        {
                                                            #region HtmlEmail

                                                            NewReviewAlertTemplateDTO templateDTO = JsonConvert.DeserializeObject<NewReviewAlertTemplateDTO>(template.Body);
                                                            string emailBody = templateDTO.body;



                                                            //Replace Account Name
                                                            emailBody = emailBody.Replace("[$AccountName$]", accountName);
                                                            emailBody = emailBody.Replace("[$Username$]", string.Format("{0} {1}", userInfo.FirstName, userInfo.LastName));


                                                            //[$NumberOfNewReview$] New Reviews(s) ([$PositiveNewReview$] Positive and [$NegativeNewReview$] Negative)

                                                            string Reviewcontent = string.Empty;

                                                            List<long> NotififcationRecipientIDs = new List<long>();


                                                            NotififcationRecipientIDs.Add(item.NotificationRecipientID);


                                                            string subject = template.Subject;

                                                            //Insert Into NotificationTarget
                                                            NotificationTarget notificationTarget = new NotificationTarget();
                                                            notificationTarget.NotificationMethodID = template.NotificationMethodID;
                                                            notificationTarget.Address = address;
                                                            notificationTarget.Subject = subject;
                                                            notificationTarget.Body = emailBody;
                                                            notificationTarget.NotificationStatusID = Convert.ToInt64(NotificationStatusesEnum.Pending);

                                                            string targetHash = Security.GetHash(string.Format("SyndicationSignupCC-{0}-{1}-{2}", emailBody, subject, address), 20);
                                                            long? duplicateTargetID = (
                                                                from t in db.NotificationTargets
                                                                where t.TargetHash == targetHash
                                                                select t.ID
                                                            ).FirstOrDefault();
                                                            notificationTarget.TargetHash = targetHash;

                                                            long NotificationTargetID = addNotificationTarget(notificationTarget);

                                                            //Update NotififcationRecipient with NotificationTargetID
                                                            updateNotififcationRecipient(NotififcationRecipientIDs, NotificationTargetID);

                                                            //Send Email
                                                            string SendFromEmailAddress = Settings.GetSetting("site.SendFromEmailAddress", "notifications@socialdealer.com");

                                                            EmailManager emailManager = new EmailManager();
                                                            bool sendResult = emailManager.sendNotification(
                                                                "Synd Signup CC", notificationTarget.Subject, notificationTarget.Body,
                                                                SendFromEmailAddress, notificationTarget.Address,
                                                                string.Empty, string.Empty)
                                                            ;

                                                            if (sendResult == true)
                                                            {
                                                                updateNotificationTarget(NotificationTargetID, NotificationStatusesEnum.Success);
                                                            }
                                                            else
                                                            {
                                                                updateNotificationTarget(NotificationTargetID, NotificationStatusesEnum.Failure);
                                                            }


                                                            #endregion
                                                        }
                                                        break;
                                                    case NotificationMethodEnum.TextEmail:
                                                        break;
                                                    case NotificationMethodEnum.SMS:
                                                        break;
                                                    case NotificationMethodEnum.Platform:
                                                        {
                                                            #region Platform

                                                            List<long> notififcationRecipientIDs = new List<long>();

                                                            notififcationRecipientIDs.Add(item.NotificationRecipientID);

                                                            //Insert Into NotificationTarget
                                                            NotificationTarget notificationTarget = new NotificationTarget();

                                                            notificationTarget.NotificationMethodID = template.NotificationMethodID;
                                                            notificationTarget.Address = string.Empty;
                                                            notificationTarget.Subject = string.Empty;
                                                            notificationTarget.Body = string.Empty;
                                                            notificationTarget.NotificationStatusID = Convert.ToInt64(NotificationStatusesEnum.Success);

                                                            long NotificationTargetID = addNotificationTarget(notificationTarget);

                                                            //Update NotififcationRecipient with NotificationTargetID
                                                            updateNotififcationRecipient(notififcationRecipientIDs, NotificationTargetID);

                                                            #endregion
                                                        }
                                                        break;
                                                    default:
                                                        break;
                                                }
                                            }

                                        }
                                    }
                                    catch (Exception ex)
                                    {
                                        ProviderFactory.Logging.LogException(ex);
                                    }
                                }// end accountID loop
                            } // end messageTemplateID loop
                        } // end userID loop
                    }
                }
            }
        }

        private void sendSocialNotifications()
        {
            using (MainEntities db = ContextFactory.Main)
            {
                if (db.Connection.State != System.Data.ConnectionState.Open)
                    db.Connection.Open();

                using (DbCommand cmd = db.CreateStoreCommand(
                        "spGetNotificationsToSend2",
                        System.Data.CommandType.StoredProcedure,
                            new SqlParameter("NotificationMessageTypeID", (long)NotificationMessageTypeEnum.SocialAlert)
                        ))
                {
                    using (DbDataReader reader = cmd.ExecuteReader())
                    {
                        List<NotificationTemplate> templates = db.Translate<NotificationTemplate>(reader).ToList();

                        reader.NextResult();

                        List<SocialCommnetsNotificationDTO> comments = db.Translate<SocialCommnetsNotificationDTO>(reader).ToList();

                        reader.NextResult();


                        if (comments.Any())
                        {
                            List<NewNotificationToSendItem> toSend = db.Translate<NewNotificationToSendItem>(reader).ToList();

                            foreach (long userID in (from ts in toSend select ts.UserID).Distinct())
                            {
                                TimeZoneInfo timeZone = ProviderFactory.TimeZones.GetTimezoneForUserID(userID);

                                foreach (long messageTemplateID in (from ts2 in toSend where ts2.UserID == userID select ts2.NotificationTemplateID).Distinct())
                                {
                                    foreach (long accountID in (from ts3 in toSend where ts3.UserID == userID && ts3.NotificationTemplateID == messageTemplateID select ts3.AccountID).Distinct())
                                    {
                                        try
                                        {

                                            List<NewNotificationToSendItem> itemsForThisUserandTemplate = (
                                                from ts4 in toSend
                                                where ts4.UserID == userID &&
                                                      ts4.NotificationTemplateID == messageTemplateID &&
                                                      ts4.AccountID == accountID
                                                select ts4
                                            ).ToList();

                                            NotificationTemplate template = (from t in templates where t.ID == messageTemplateID select t).FirstOrDefault();

                                            //long AccountId = (from ts in itemsForThisUserandTemplate select ts.AccountID).FirstOrDefault();
                                            string Address = (from ts in itemsForThisUserandTemplate select ts.EmailAddress).FirstOrDefault();
                                            int NewCommentCount = itemsForThisUserandTemplate.Count;
                                            int TotalPostwithNewComments = (from c in comments where c.AccountID == accountID select c.PostID).Distinct().Count();

                                            switch ((NotificationMethodEnum)template.NotificationMethodID)
                                            {
                                                case NotificationMethodEnum.HtmlEmail:
                                                    {
                                                        #region HtmlEmail
                                                        //Update to Social Alert DTO
                                                        NewSocialAlertTemplateDTO TemplateDTO = JsonConvert.DeserializeObject<NewSocialAlertTemplateDTO>(template.Body);

                                                        string EmailBody = TemplateDTO.body;

                                                        //Get AccountName and Replace [$AccountName$] in TemplateDTO
                                                        GetEntityDTO<AccountDTO, AccountOptionsDTO> result = ProviderFactory.Security.GetAccountByID(UserInfoDTO.System, accountID);
                                                        EmailBody = EmailBody.Replace("[$AccountName$]", result.Entity.DisplayName);

                                                        //Replace [$NewCommentCount$] in TemplateDTO
                                                        EmailBody = EmailBody.Replace("[$NewCommentCount$]", NewCommentCount.ToString());

                                                        //Replace [$NewCommentCount$] in TemplateDTO
                                                        EmailBody = EmailBody.Replace("[$PostsWithCommentsCount$]", TotalPostwithNewComments.ToString());

                                                        List<long> NotififcationRecipientIDs = new List<long>();

                                                        // LOOP THROUGH ITEMS
                                                        string AllPostRepeatingBlock = string.Empty;
                                                        foreach (long PostID in (from cs in comments where cs.AccountID == accountID select cs.PostID).Distinct())
                                                        {
                                                            try
                                                            {
                                                                string postRepeatingBlock = TemplateDTO.PostRepeatingBlock;
                                                                var Post = (from p in comments where p.PostID == PostID select p).FirstOrDefault();

                                                                string socialNetworkIcon = "http://cdn.socialintegration.com/socialnetwork/fb.png"; //Facebook
                                                                string socialNetworkPostURL = string.Empty;
                                                                string postDescription = Post.PostMessage;

                                                                DateTime dt = new SIDateTime(Post.PostCreateTime, timeZone).LocalDate.Value;
                                                                string postDateTime_TimeZone = timeZone.Id;
                                                                string postCreatedDate = dt.ToString(CultureInfo.InvariantCulture);// +" " + PostDateTime_TimeZone;


                                                                string pictureURLOrLinkURL = string.Empty;
                                                                string pictureIconOrLinkIcon = string.Empty;

                                                                if (Post.PostTypeID == (long)PostTypeEnum.Photo)
                                                                {
                                                                    pictureURLOrLinkURL = Post.PictureURL;
                                                                    pictureIconOrLinkIcon = "http://cdn.socialintegration.com/socialnetwork/post_photo.png";
                                                                }
                                                                if (Post.PostTypeID == (long)PostTypeEnum.Link)
                                                                {
                                                                    pictureURLOrLinkURL = Post.LinkURL;
                                                                    pictureIconOrLinkIcon = "http://cdn.socialintegration.com/socialnetwork/post_link.png";
                                                                }


                                                                string LikesCount = Post.LikesCount.ToString();
                                                                string CommentCount = Post.CommentsCount.ToString();
                                                                string SharesCount = Post.SharesCount.ToString();

                                                                if (Post.ResultID.Contains("_"))
                                                                {
                                                                    string[] ids = Post.ResultID.Split('_');
                                                                    socialNetworkPostURL = "http://www.facebook.com/" + ids[0] + "/posts/" + ids[1];
                                                                }
                                                                else
                                                                {
                                                                    socialNetworkPostURL = "http://www.facebook.com/" + Post.ResultID;
                                                                }

                                                                postRepeatingBlock = postRepeatingBlock.Replace("[$SocialNetworkIcon$]", socialNetworkIcon)
                                                                                                        .Replace("[$SocialNetworkPostURL$]", socialNetworkPostURL)
                                                                                                        .Replace("[$PostDescription$]", postDescription)
                                                                                                        .Replace("[$PostCreatedDate$]", postCreatedDate)
                                                                                                        .Replace("[$PictureURLOrLinkURL$]", pictureURLOrLinkURL)
                                                                                                        .Replace("[$PictureIconOrLinkIcon$]", pictureIconOrLinkIcon)
                                                                                                        .Replace("[$LikesCount$]", LikesCount)
                                                                                                        .Replace("[$CommentCount$]", CommentCount)
                                                                                                        .Replace("[$SharesCount$]", SharesCount);

                                                                var newCommentsforPost = (
                                                                                            from cs1 in comments
                                                                                            where cs1.PostID == PostID
                                                                                            select cs1
                                                                                        ).ToList();


                                                                //string CommentRepeatingBlockTemp = CommentRepeatingBlock;
                                                                string NewCommentsForPost = string.Empty;

                                                                foreach (SocialCommnetsNotificationDTO comment in newCommentsforPost)
                                                                {
                                                                    try
                                                                    {
                                                                        //NewNotificationToSendItem item in itemsForThisUserandTemplate
                                                                        string CommentRepeatingBlock = TemplateDTO.CommentRepeatingBlock;

                                                                        NewNotificationToSendItem item = (from i in itemsForThisUserandTemplate where i.EntityID == comment.FaceBookCommentID select i).FirstOrDefault();

                                                                        if (item != null)
                                                                        {

                                                                            NotififcationRecipientIDs.Add(item.NotificationRecipientID);

                                                                            //var Commet = (from n in NewCommentsforPost where n.FaceBookCommentID == item.EntityID select n).SingleOrDefault();

                                                                            DateTime dt1 = new SIDateTime(comment.CommentCreateTime, timeZone).LocalDate.Value;
                                                                            postDateTime_TimeZone = timeZone.Id;
                                                                            string CommentCreatedDate = dt1.ToString(CultureInfo.InvariantCulture);// +" " + PostDateTime_TimeZone;
                                                                            string CommentAuthor = comment.CommentorName;
                                                                            string CommentDescription = comment.CommnetMessage;

                                                                            CommentRepeatingBlock = CommentRepeatingBlock.Replace("[$CommentCreatedDate$]", CommentCreatedDate)
                                                                                                        .Replace("[$CommentAuthor$]", CommentAuthor)
                                                                                                        .Replace("[$CommentDescription$]", CommentDescription);

                                                                            NewCommentsForPost = NewCommentsForPost + CommentRepeatingBlock;

                                                                            postRepeatingBlock = ProviderFactory.Social.TokenizeMessage(postRepeatingBlock, item.AccountName, item.AccountPhone, item.AccountURL);
                                                                            AllPostRepeatingBlock = ProviderFactory.Social.TokenizeMessage(AllPostRepeatingBlock, item.AccountName, item.AccountPhone, item.AccountURL);
                                                                        }
                                                                    }
                                                                    catch (Exception ex)
                                                                    {
                                                                        ProviderFactory.Logging.LogException(ex);
                                                                    }
                                                                }//Comments

                                                                postRepeatingBlock = postRepeatingBlock.Replace("[$CommentRepeatingBlock$]", NewCommentsForPost);
                                                                AllPostRepeatingBlock = AllPostRepeatingBlock + postRepeatingBlock;

                                                            }
                                                            catch (Exception ex)
                                                            {
                                                                ProviderFactory.Logging.LogException(ex);
                                                            }
                                                        }//Posts

                                                        EmailBody = EmailBody.Replace("[$PostRepeatingBlock$]", AllPostRepeatingBlock);

                                                        string Subject = template.Subject + " for " + (string.IsNullOrWhiteSpace(result.Entity.DisplayName) ? result.Entity.Name : result.Entity.DisplayName);

                                                        //Insert Into NotificationTarget
                                                        NotificationTarget notificationTarget = new NotificationTarget();


                                                        notificationTarget.NotificationMethodID = template.NotificationMethodID;
                                                        notificationTarget.Address = Address;
                                                        notificationTarget.Subject = Subject;
                                                        notificationTarget.Body = EmailBody;
                                                        notificationTarget.NotificationStatusID = Convert.ToInt64(NotificationStatusesEnum.Pending);

                                                        long NotificationTargetID = addNotificationTarget(notificationTarget);

                                                        //Update NotififcationRecipient with NotificationTargetID
                                                        updateNotififcationRecipient(NotififcationRecipientIDs, NotificationTargetID);

                                                        //Send Email
                                                        string SendFromEmailAddress = Settings.GetSetting("site.SendFromEmailAddress", "notifications@socialdealer.com");

                                                        EmailManager emailManager = new EmailManager();
                                                        bool sendResult = emailManager.sendNotification(
                                                            "Social - New Comments", notificationTarget.Subject, notificationTarget.Body,
                                                            SendFromEmailAddress, notificationTarget.Address,
                                                            string.Empty, string.Empty)
                                                        ;

                                                        if (sendResult == true)
                                                        {
                                                            updateNotificationTarget(NotificationTargetID, NotificationStatusesEnum.Success);
                                                        }
                                                        else
                                                        {
                                                            updateNotificationTarget(NotificationTargetID, NotificationStatusesEnum.Failure);
                                                        }

                                                        #endregion
                                                    }
                                                    break;
                                                case NotificationMethodEnum.TextEmail:
                                                    break;
                                                case NotificationMethodEnum.SMS:
                                                    break;
                                                case NotificationMethodEnum.Platform:
                                                    {
                                                        #region Platform

                                                        List<long> NotififcationRecipientIDs = new List<long>();

                                                        // LOOP THROUGH ITEMS
                                                        foreach (NewNotificationToSendItem item in itemsForThisUserandTemplate)
                                                        {
                                                            NotififcationRecipientIDs.Add(item.NotificationRecipientID);
                                                        }

                                                        //Insert Into NotificationTarget
                                                        NotificationTarget notificationTarget = new NotificationTarget();

                                                        notificationTarget.NotificationMethodID = template.NotificationMethodID;
                                                        notificationTarget.Address = string.Empty;
                                                        notificationTarget.Subject = string.Empty;
                                                        notificationTarget.Body = string.Empty;
                                                        notificationTarget.NotificationStatusID = Convert.ToInt64(NotificationStatusesEnum.Success);

                                                        long NotificationTargetID = addNotificationTarget(notificationTarget);

                                                        //Update NotififcationRecipient with NotificationTargetID
                                                        updateNotififcationRecipient(NotififcationRecipientIDs, NotificationTargetID);

                                                        #endregion
                                                    }
                                                    break;
                                                default:
                                                    break;
                                            }//switch

                                        }
                                        catch (Exception ex)
                                        {
                                            ProviderFactory.Logging.LogException(ex);
                                        }
                                    }// end accountID loop
                                } // end messageTemplateID loop
                            } // end userID loop
                        }
                    }
                }

            }
        }

        #endregion

        #region private helpers

        private string getOrCreateUnsubscribeAccessToken(long userID, long notificationMessageTypeID, long notificationTemplateID, long resellerID)
        {
            using (MainEntities db = ContextFactory.Main)
            {
                //AccessToken accessToken = db.AccessTokens.FirstOrDefault(a => a.UserID == userID && a.RemainingUses.GetValueOrDefault() > 0 && a.AccessTokenTypeID == (long) AccessTokenTypeEnum.SyndicationUnsubscribe);
                AccessToken accessToken = db.AccessTokens.FirstOrDefault(a => a.UserID == userID && (a.RemainingUses == null || a.RemainingUses > 0) && a.AccessTokenTypeID == (long)AccessTokenTypeEnum.SyndicationUnsubscribe);

                if (accessToken == null)
                {
                    SyndicationUnsubscribeDTO tokenRequest = new SyndicationUnsubscribeDTO()
                    {
                        UserID = userID, //UserID of user that the link is for                
                        NotificationMessageTypeID = notificationMessageTypeID, // template.NotificationMessageTypeID,
                        NotificationTemplateID = notificationTemplateID, // template.ID,
                        RemainingUses = 1,
                        TimeTillExpiration = null,
                        ResellerID = resellerID //template.ResellerID.GetValueOrDefault()
                    };

                    AccessTokenBaseDTO tok = ProviderFactory.Security.CreateAccessToken(tokenRequest);

                    return tok.Token.ToString();
                }
                else
                {
                    return accessToken.Token.ToString();
                }
            }
        }

        private long addNotificationTarget(NotificationTarget notificationTarget)
        {
            long result = 0;

            using (MainEntities db = ContextFactory.Main)
            {
                db.NotificationTargets.AddObject(notificationTarget);
                db.SaveChanges();
                result = notificationTarget.ID;
            }

            return result;

        }

        private void updateNotificationTarget(long NotificationTargetID, NotificationStatusesEnum NotificationStatusID)
        {
            using (MainEntities db = ContextFactory.Main)
            {
                db.spUpdateNotificationTarget(NotificationTargetID, DateTime.UtcNow, (long)NotificationStatusID);
            }
        }

        private void updateNotififcationRecipient(long NotififcationRecipientID, long NotificationTargetID)
        {
            updateNotififcationRecipient(new List<long> { NotififcationRecipientID }, NotificationTargetID);
        }

        private void updateNotififcationRecipient(List<long> NotififcationRecipientID, long NotificationTargetID)
        {
            using (MainEntities db = ContextFactory.Main)
            {
                db.spUpdateNotififcationRecipient(string.Join("|", NotififcationRecipientID.ToArray()), NotificationTargetID);
            }
        }

        #endregion

        /*
        public MessageTypeDTO GetMessageTemplateByID(int MessageTypeID)
        {
            using (NotificationEntities db = ContextFactory.Notification)
            {
                return MessageType2DTO(
                    (from m in db.MessageTypes where m.MessageTypeId.Equals(MessageTypeID) select m).FirstOrDefault()
                );    
            }
        }

        public int SaveNotificationEntry(SI.DAL.Notification notification, NotificationTransmission notificationTransmission)
        {
            var result = 0;
            var notification_id = 0;

            notification_id = SaveNotification(notification);

            if (notification_id > 0)
            {

                notificationTransmission.NotificationId = notification_id;
                int notificationTransmission_id = SaveNotificationTransmission(notificationTransmission);

                if (notificationTransmission_id > 0)
                {
                    result = notificationTransmission_id;
                }
            }
            return result;
        }

        public void UpdateNotificationTransmission(int NotificationTransmissionId, int StatusId)
        {
            using (NotificationEntities db = ContextFactory.Notification)
            {
                db.spUpdateNotificationTransmission(NotificationTransmissionId, StatusId);
            }
        }

        public List<int> PendingNotificationsCount()
        {            
            using (NotificationEntities db = ContextFactory.Notification)
            {
                return (from nt in db.NotificationTransmissions
                          where nt.StatusId == 1
                          select nt.NotificationId).ToList();
            }

            
        }

        public List<EmailNotificationDTO> PendingNotifications(List<int> NotificationIDs)
        {
            List<EmailNotificationDTO> listemailNotificationDTO = new List<EmailNotificationDTO>();
            using (NotificationEntities db = ContextFactory.Notification)
            {
                ObjectResult<spGetNotificationTosend_Result> results = db.spGetNotificationTosend((NotificationIDs == null) ? null : string.Join("|", NotificationIDs.ToArray()));

                foreach (var result in results)
                {
                    EmailNotificationDTO emailNotificationDTO = new EmailNotificationDTO();

                    emailNotificationDTO.NotificationID = result.NotificationId;
                    emailNotificationDTO.SendFrom = result.SendFrom;
                    emailNotificationDTO.SendTo = ProviderFactory.Util.FromXml<SendToDTO>(result.SendTo);
                    emailNotificationDTO.SendCC = ProviderFactory.Util.FromXml<SendCCDTO>(result.SendCC);
                    emailNotificationDTO.SendBCC = ProviderFactory.Util.FromXml<SendBCCDTO>(result.SendBCC);
                    emailNotificationDTO.MessageTypeID = result.MessageTypeId;
                    emailNotificationDTO.MessageCategoryID = result.MessageCategoryId;
                    emailNotificationDTO.NotificationTransmissionID = result.NotificationTransmissionId;
                    emailNotificationDTO.Subject = result.Subject;
                    emailNotificationDTO.Body = result.Body;
                    emailNotificationDTO.CreatedDate = result.CreatedDate;
                    emailNotificationDTO.ProcessedDate = result.ProcessedDate;
                    emailNotificationDTO.AccountID = result.AccountID;
                    emailNotificationDTO.ScheduledDate = result.ScheduledDate;
                    emailNotificationDTO.StatusID = (NotificationStatus)result.StatusId;
                    
                    listemailNotificationDTO.Add(emailNotificationDTO);
                }
            }

            return listemailNotificationDTO;
        }

        private int SaveNotification(SI.DAL.Notification notification)
        {
            var result = 0;


            using (NotificationEntities db = ContextFactory.Notification)
            {

                var activityResult = db.spNotificationInsert(
                    notification.MessageTypeId,
                    notification.MessageCategoryId,
                    notification.Subject,
                    notification.Body,
                    notification.CreatedDate,
                    notification.AccountID,
                    notification.ProcessedDate,                    
                    notification.VirtualGroupID).SingleOrDefault() ?? 0;

                result = Convert.ToInt32(activityResult);
            }

            return result;
        }

        private int SaveNotificationTransmission(NotificationTransmission notificationTransmission)
        {
            var result = 0;

            using (NotificationEntities db = ContextFactory.Notification)
            {

                var activityResult = db.spNotificationTransmissionInsert(
                    notificationTransmission.NotificationId,
                    notificationTransmission.TransmissionTypeId,
                    notificationTransmission.DateUpdated,
                    notificationTransmission.ScheduledDate,
                    notificationTransmission.StatusId,                    
                    notificationTransmission.SendFrom,
                    notificationTransmission.SendTo,
                    notificationTransmission.SendCC,
                    notificationTransmission.SendBCC
                    ).SingleOrDefault() ?? 0;

                result = Convert.ToInt32(activityResult);

            }
            return result;
        }

        public List<NotificationsAlertDTO> GetNotificationsByAccountID(List<long> accountIDs)
        {
			List<NotificationsAlertDTO> notificationalerts = new List<NotificationsAlertDTO>();

            using (NotificationEntities db = ContextFactory.Notification)
            {
                ObjectResult<spGetNotifications_Result> results = db.spGetNotifications((accountIDs == null) ? null : string.Join("|", accountIDs.ToArray()));
	            if (results != null)
	            {
		            foreach (var item in results)
		            {
			            NotificationsAlertDTO notificationalert = new NotificationsAlertDTO();

			            notificationalert.ID = item.ID;
			            notificationalert.ActionImage = item.ImagePath;
			            notificationalert.LocationName = item.LocationName;
			            notificationalert.Description = item.Description;
			            notificationalert.Type = item.MessageType;
			            notificationalert.AlertDateLocal = item.CreatedDate; //TODO CHANGE TO SIDATETIME

			            notificationalerts.Add(notificationalert);
		            }
	            }
            }

            return notificationalerts;
        }

		public List<NotificationsToDoDTO> GetNotificationsToDoByAccountID(List<long> accountIDs, bool? showCompleted)
        {
			List<NotificationsToDoDTO> notificationstodos = new List<NotificationsToDoDTO>();

            using (NotificationEntities db = ContextFactory.Notification)
            {
				//TODO NO CompletedDate ????
				//TODO ADD showCompleted to proc, not complete
                ObjectResult<spGetNotificationsToDo_Result> results = db.spGetNotificationsToDo((accountIDs == null) ? null : string.Join("|", accountIDs.ToArray()));
	            if (results != null)
	            {
		            foreach (var item in results)
		            {
			            NotificationsToDoDTO notificationstodo = new NotificationsToDoDTO();

			            notificationstodo.ID = item.ID;
			            notificationstodo.ActionImage = item.ImagePath;
			            notificationstodo.LocationName = item.LocationName;
			            notificationstodo.Description = item.Description;
			            notificationstodo.Type = item.MessageType;
			            notificationstodo.ToDoDateLocal = item.CreatedDate; //TODO CHANGE TO SIDATETIME
			            //notificationstodo.CompletedDateLocal = item.CompletedDate //TODO need completed date

			            notificationstodos.Add(notificationstodo);
		            }
	            }
            }

            return notificationstodos;
        }

        public List<NotificationSummaryDTO> GetNotificationsSummaryByAccountID(List<long> accountIDs)
        {
			List<NotificationSummaryDTO> notificationssummarys = new List<NotificationSummaryDTO>();

            using (NotificationEntities db = ContextFactory.Notification)
            {
                ObjectResult<spGetNotificationsSummary_Result> results = db.spGetNotificationsSummary((accountIDs == null) ? null : string.Join("|", accountIDs.ToArray()));
                foreach (var item in results)
                {
					NotificationSummaryDTO notificationssummary = new NotificationSummaryDTO();
                    notificationssummary.ID = item.ID;
                    notificationssummary.ActionImage = item.ImagePath;
                    notificationssummary.AlertLevel = item.AlertLevel;
                    notificationssummary.Text = item.Description;

                    notificationssummarys.Add(notificationssummary);
                }
            }

            return notificationssummarys;
        }

        #region translation helpers

        private MessageTypeDTO MessageType2DTO(MessageType m)
        {
            if (m == null)
                return null;
            return new MessageTypeDTO()
            {
                Description = m.Description,
                DisplayOrder = m.DisplayOrder,
                MessageCategoryId = Convert.ToInt32(m.MessageCategoryId),
                MessageTemplate = m.MessageTemplate,
                MessageTypeId = m.MessageTypeId,
                MessageTypeName = m.MessageTypeName
            };
        }

        #endregion

        */



        public void GenerateInitialTargetHashes()
        {
            using (MainEntities db = ContextFactory.Main)
            {
                List<long> templatesIDs = new List<long>();
                templatesIDs.Add(1);
                templatesIDs.Add(6);
                templatesIDs.Add(19);
                templatesIDs.Add(20);

                List<long> targetIDs = (
                    from t in db.NotificationTargets
                    join nr in db.NotificationRecipients on t.ID equals nr.NotificationTargetID
                    where templatesIDs.Contains(nr.NotificationTemplateID)
                    && t.TargetHash == null
                    select t.ID
                ).Distinct().ToList();

                int counter = 0;
                foreach (long targetID in targetIDs)
                {
                    NotificationTarget target = (from t in db.NotificationTargets where t.ID == targetID select t).FirstOrDefault();
                    string targetHash = Security.GetHash(string.Format("NewReviewAlertEmail-{0}-{1}-{2}", target.Body, target.Subject, target.Address), 20);
                    target.TargetHash = targetHash;
                    counter++;
                    if (counter >= 100)
                    {
                        counter = 0;
                        db.SaveChanges();
                    }
                }
                db.SaveChanges();

            }

        }

        #region Enable/Disable Notifications and UI State

        public List<UserNotificationTypeInfoDTO> GetAvailableNotificationsForUser(long userID)
        {
            // the user id
            //long userID = userInfo.EffectiveUser.ID.Value;

            // the results list
            List<UserNotificationTypeInfoDTO> items = new List<UserNotificationTypeInfoDTO>();

            // the security provider
            ISecurityProvider sec = ProviderFactory.Security;

            // list of accounts that this user has at least account view on
            List<long> baseAccountIDs = sec.AccountsWithPermission(userID, PermissionTypeEnum.accounts_view);

            using (MainEntities db = ContextFactory.Main)
            {
                // the account feature list for all accounts the user has at least account view for
                List<vActiveAccountFeature> afeats = (
                    from af in db.vActiveAccountFeatures
                    select af
                ).ToList();

                afeats = (
                    from af in afeats
                    where baseAccountIDs.Contains(af.AccountID)
                    select af
                ).ToList();

                // get the requirements
                List<NotificationRequirement> reqs = db.NotificationRequirements.ToList();

                // get the base types
                List<NotificationBaseType> baseTypes = db.NotificationBaseTypes.ToList();

                // get the current subscriptions for this user
                var subs = (
                    from s in db.NotificationSubscriptions
                    join tpl in db.NotificationTemplates on s.NotificationTemplateID equals tpl.ID
                    where s.RecipientUserID == userID
                    select new
                    {
                        NotificationMessageTypeID = tpl.NotificationMessageTypeID,
                        NotificationMethodID = tpl.NotificationMethodID,
                        AccountID = s.AccountID
                    }
                ).ToList();

                // get the exclusions
                List<EmailExclusion> excl = (
                    from e in db.EmailExclusions
                    where e.UserID == userID
                    select e
                ).ToList();

                // get the notificationmessagetypes
                List<NotificationMessageType> types = db.NotificationMessageTypes.ToList();

                // loop through the requirements
                foreach (NotificationRequirement req in reqs)
                {
                    if ((
                        from bt in baseTypes
                        where bt.NotificationMessageTypeID == req.NotificationMessageTypeID
                        select bt
                        ).Any()
                    )
                    {

                        PermissionTypeEnum ptype = (PermissionTypeEnum)req.RequiredPermissionTypeID;

                        // list of account with the given user permission
                        List<long> accountIDs = sec.AccountsWithPermission(userID, ptype);

                        // filter on requried feature if in effect                    
                        if (req.RequiredFeatureTypeID.HasValue)
                        {
                            // accounts with the required feature
                            List<long> rfeats = (
                                from af in afeats
                                where af.FeatureTypeID == req.RequiredFeatureTypeID.Value
                                select af.AccountID
                            ).Distinct().ToList();

                            accountIDs = (
                                from ids in accountIDs
                                where rfeats.Contains(ids)
                                select ids
                            ).ToList();
                        }

                        if (accountIDs.Any())
                        {
                            UserNotificationTypeInfoDTO emailItem = new UserNotificationTypeInfoDTO()
                            {
                                UserID = userID,
                                CanSubscribe = true,
                                AccountsSubscribedTo = (
                                    from s in subs
                                    where s.NotificationMessageTypeID == req.NotificationMessageTypeID
                                    && s.NotificationMethodID == (long)NotificationMethodEnum.HtmlEmail
                                    select s.AccountID
                                ).Distinct().Count(),
                                Method = NotificationMethodEnum.HtmlEmail,
                                Type = (NotificationMessageTypeEnum)req.NotificationMessageTypeID,
                                TypeName = (from t in types where t.ID == req.NotificationMessageTypeID select t.Name).FirstOrDefault()
                            };

                            items.Add(emailItem);

                            UserNotificationTypeInfoDTO platItem = new UserNotificationTypeInfoDTO()
                            {
                                UserID = userID,
                                CanSubscribe = true,
                                AccountsSubscribedTo = (
                                    from s in subs
                                    where s.NotificationMessageTypeID == req.NotificationMessageTypeID
                                    && s.NotificationMethodID == (long)NotificationMethodEnum.Platform
                                    select s.AccountID
                                ).Distinct().Count(),
                                Method = NotificationMethodEnum.Platform,
                                Type = (NotificationMessageTypeEnum)req.NotificationMessageTypeID,
                                TypeName = (from t in types where t.ID == req.NotificationMessageTypeID select t.Name).FirstOrDefault()
                            };

                            items.Add(platItem);
                        }
                    }
                }
            }

            return items;
        }

        public void SetUserNotificationStates(UserInfoDTO userInfo, long userIDtoChange, List<SetNotificationStateDTO> states)
        {
            //long userID = userInfo.EffectiveUser.ID.Value;
            bool changesMade = false;

            using (MainEntities db = ContextFactory.Main)
            {
                //get all exclusions for this user
                List<EmailExclusion> exclusions = (
                    from e in db.EmailExclusions
                    where e.UserID == userIDtoChange
                    select e
                ).ToList();

                foreach (SetNotificationStateDTO state in states)
                {
                    EmailExclusion exclusion = (
                        from e in exclusions
                        where (e.NotificationMessageTypeID.HasValue && e.NotificationMessageTypeID.Value == (long)state.Type)
                        && (e.NotificationMethodID.HasValue && e.NotificationMethodID.Value == (long)state.Method)
                        && e.AccountID.HasValue == false
                        && e.NotificationTemplateID.HasValue == false
                        select e
                    ).FirstOrDefault();

                    if (state.Enabled)
                    {
                        if (exclusion != null)
                        {
                            changesMade = true;
                            exclusions.Remove(exclusion);

                            string sql = string.Format("delete EmailExclusions where id={0}", exclusion.ID);
                            execNonQuery(sql);
                        }
                    }
                    else
                    {
                        if (exclusion == null)
                        {
                            changesMade = true;
                            exclusion = new EmailExclusion()
                            {
                                UserID = userIDtoChange,
                                NotificationMessageTypeID = (long)state.Type,
                                NotificationMethodID = (long)state.Method,
                                CreatedDate = DateTime.UtcNow
                            };
                            db.EmailExclusions.AddObject(exclusion);
                            db.SaveChanges();
                            exclusions.Add(exclusion);
                        }
                    }
                }

                if (changesMade)
                {
                    db.spFixNotiSubsForUser(userIDtoChange);
                }
            }
        }

        public List<RecentNotificationItemDTO> GetMostRecentEmailNotificationsForUser(long userID, int maxItems)
        {
            List<RecentNotificationItemDTO> items = new List<RecentNotificationItemDTO>();

            //long userID = userInfo.EffectiveUser.ID.Value;
            TimeZoneInfo tz = ProviderFactory.TimeZones.GetTimezoneForUserID(userID);

            using (MainEntities db = ContextFactory.Main)
            {
                if (db.Connection.State != System.Data.ConnectionState.Open)
                    db.Connection.Open();

                using (DbCommand cmd = db.CreateStoreCommand("spGetMostRecentUserNotifications", System.Data.CommandType.StoredProcedure,
                    new SqlParameter("@userID", userID),
                    new SqlParameter("@maxItems", maxItems)
                ))
                {
                    using (DbDataReader reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            RecentNotificationItemDTO item = new RecentNotificationItemDTO()
                            {
                                NotificationTargetID = Util.GetReaderValue<long>(reader, "ID", 0),
                                DateSent = SIDateTime.CreateSIDateTime(Util.GetReaderValue<DateTime?>(reader, "DateSent", null), tz),
                                Type = (NotificationMessageTypeEnum)Util.GetReaderValue<long>(reader, "NotificationMessageTypeID", 0),
                                TypeName = Util.GetReaderValue<string>(reader, "TypeName", ""),
                                Subject = Util.GetReaderValue<string>(reader, "Subject", "")
                            };
                            items.Add(item);
                        }
                    }
                }
            }

            return items;
        }

        public string GetBodyTextForNotificationTargetID(long notificationTargetID)
        {
            using (MainEntities db = ContextFactory.Main)
            {
                return (
                    from t in db.NotificationTargets
                    where t.ID == notificationTargetID
                    select t.Body
                ).FirstOrDefault();
            }
        }

        #endregion

        #region utils

        private void execNonQuery(string sql)
        {
            using (MainEntities db = ContextFactory.Main)
            {
                if (db.Connection.State != System.Data.ConnectionState.Open)
                    db.Connection.Open();

                using (DbCommand cmd = db.CreateStoreCommand(sql, System.Data.CommandType.Text))
                {
                    cmd.ExecuteNonQuery();
                }
            }
        }

        #endregion


        public void SendAutoApprovalNotification(long postApprovalID)
        {
            using (MainEntities db = ContextFactory.Main)
            {
                PostApproval pa = (from p in db.PostApprovals where p.ID == postApprovalID select p).FirstOrDefault();
                if (pa == null)
                {
                    Auditor
                        .New(AuditLevelEnum.Warning, AuditTypeEnum.General, UserInfoDTO.System, "")
                        .SetMessage("SendAutoApprovalNotification was called for PostApprovalID [{0}] which does not exist", postApprovalID)
                        .Save(ProviderFactory.Logging)
                    ;
                }
                else
                {
                    var info = (
                        from pap in db.PostApprovals
                        join pt in db.PostTargets on pap.PostTargetID equals pt.ID
                        join p in db.Posts on pt.PostID equals p.ID
                        join s in db.Syndicators on p.SyndicatorID.Value equals s.ID
                        join sa in db.Accounts on s.AccountID equals sa.ID
                        join r in db.Resellers on s.AccountID equals r.AccountID
                        join u in db.Users on pap.SetByUserID.Value equals u.ID
                        where pap.ID == postApprovalID
                        && pap.SetByUserID.HasValue
                        && pap.AutoApproved == true
                        && p.SyndicatorID.HasValue
                        select new
                        {
                            PostTargetID = pt.ID,
                            PostID = pt.PostID,

                            SyndicatorID = p.SyndicatorID,
                            SyndicatorResellerID = r.ID,
                            SyndicatorAccountID = sa.ID,
                            SyndicatorDisplayName = sa.DisplayName,
                            SyndicatorName = sa.Name,

                            UserFirstName = u.FirstName,
                            UserLastName = u.LastName,
                            UserID = u.ID,
                            UserEmailAddress = u.EmailAddress,
                            UserMemberOfAccountID = u.MemberOfAccountID
                        }
                    ).FirstOrDefault();

                    if (info == null)
                    {
                        Auditor
                            .New(AuditLevelEnum.Warning, AuditTypeEnum.General, UserInfoDTO.System, "")
                            .SetMessage("SendAutoApprovalNotification was called for PostApprovalID [{0}], but all information could not be obtained in order to send the notification.", postApprovalID)
                            .Save(ProviderFactory.Logging)
                        ;
                    }
                    else
                    {
                        // most recent post level revision
                        PostRevision rev = (
                            from r in db.PostRevisions
                            where r.PostTargetID.HasValue == false
                            && r.PostID == info.PostID
                            orderby r.ID descending
                            select r
                        ).FirstOrDefault();

                        // user's time zone
                        TimeZoneInfo timeZone = ProviderFactory.TimeZones.GetTimezoneForUserID(info.UserID);

                        // get the access token
                        PostApprovalHistoryAccessTokenDTO pahToken = new PostApprovalHistoryAccessTokenDTO()
                        {
                            RemainingUses = null,
                            TimeTillExpiration = new TimeSpan(7, 0, 0, 0),
                            UserID = info.UserID
                        };
                        AccessTokenBaseDTO baseToken = ProviderFactory.Security.CreateAccessToken(pahToken);
                        string tokenValue = baseToken.Token.ToString("D");
                        string baseURL = Settings.GetSetting("site.baseurl");
                        string platformURL = string.Format("{0}/access/{1}", baseURL, tokenValue);

                        // try to get the template for the syndicator
                        NotificationTemplate template = (
                            from tpl in db.NotificationTemplates
                            where tpl.ResellerID.HasValue
                            && tpl.ResellerID.Value == info.SyndicatorResellerID
                            && tpl.NotificationMessageTypeID == (long)NotificationMessageTypeEnum.AutoApprovedNewContent
                            && tpl.NotificationMethodID == (long)NotificationMethodEnum.HtmlEmail
                            select tpl
                        ).FirstOrDefault();

                        // if not found, try the reseller of the auto approving user
                        if (template == null)
                        {
                            long userResellerID = ProviderFactory.Security.GetResellerByAccountID(UserInfoDTO.System, info.UserMemberOfAccountID, TimeZoneInfo.Utc).ID.Value;
                            template = (
                                from tpl in db.NotificationTemplates
                                where tpl.ResellerID.HasValue
                                && tpl.ResellerID.Value == userResellerID
                                && tpl.NotificationMessageTypeID == (long)NotificationMessageTypeEnum.AutoApprovedNewContent
                                && tpl.NotificationMethodID == (long)NotificationMethodEnum.HtmlEmail
                                select tpl
                            ).FirstOrDefault();
                        }

                        // if not found, try the default template
                        if (template == null)
                        {
                            template = (
                                from tpl in db.NotificationTemplates
                                where !tpl.ResellerID.HasValue
                                && tpl.NotificationMessageTypeID == (long)NotificationMessageTypeEnum.AutoApprovedNewContent
                                && tpl.NotificationMethodID == (long)NotificationMethodEnum.HtmlEmail
                                select tpl
                            ).FirstOrDefault();
                        }

                        if (template == null)
                        {
                            Auditor
                                .New(AuditLevelEnum.Warning, AuditTypeEnum.General, UserInfoDTO.System, "")
                                .SetMessage("SendAutoApprovalNotification was called for PostApprovalID [{0}], but no email template could be found.", postApprovalID)
                                .Save(ProviderFactory.Logging)
                            ;
                        }
                        else
                        {
                            // we have a template, onward

                            // deserialize the template
                            AutoApprovedNewContentNotificationDTO dto = Newtonsoft.Json.JsonConvert.DeserializeObject<AutoApprovedNewContentNotificationDTO>(template.Body);

                            // get an image if there is one
                            PostImage pimg = (
                                from pi in db.PostImages
                                where pi.PostID == info.PostID
                                && pi.PostTargetID.HasValue == false
                                select pi
                            ).FirstOrDefault();

                            // get all of the accounts auto approved by this user for this post
                            var accounts = (
                                from pap in db.PostApprovals
                                join pt in db.PostTargets on pap.PostTargetID equals pt.ID
                                join creds in db.Credentials on pt.CredentialID equals creds.ID
                                join apps in db.SocialApps on creds.SocialAppID equals apps.ID
                                join nets in db.SocialNetworks on apps.SocialNetworkID equals nets.ID
                                join a in db.Accounts on creds.AccountID equals a.ID
                                where pt.PostID == info.PostID
                                && pap.SetByUserID.HasValue
                                && pap.AutoApproved
                                && pap.SetByUserID.Value == info.UserID
                                && pt.Deleted == false
                                select new
                                {
                                    PostApprovalID = pap.ID,
                                    AccountName = a.Name,
                                    AccountDisplayName = a.DisplayName,
                                    SocialNetwork = nets.Name,
                                    ScheduledDate = pt.ScheduleDate
                                }
                            ).ToList();

                            // form the html
                            string body = dto.Body;

                            #region post date and time

                            var scheduleDate = accounts.Select(x => x.ScheduledDate).FirstOrDefault();

                            string PostDateTime = string.Empty;
                            if (scheduleDate.HasValue)
                            {
                                DateTime dt = new SIDateTime(scheduleDate.Value, timeZone).LocalDate.Value;

                                string postDateTimeTimeZone = timeZone.Id;

                                body = body.Replace("[$PostDate$]", dt.ToString(CultureInfo.InvariantCulture) + " " + postDateTimeTimeZone);
                            }
                            else
                            {
                                body = body.Replace("[$PostDate$]", "Immediate Post");
                            }

                            #endregion

                            // syndicator name
                            //string syndicatorName = string.IsNullOrWhiteSpace(info.SyndicatorDisplayName) ? info.SyndicatorName : info.SyndicatorDisplayName;
                            //body = body.Replace("[$Syndicator$]", syndicatorName);

                            #region message, link and image
                                                        
                            // post text
                            if (rev != null && !string.IsNullOrWhiteSpace(rev.Message))
                            {
                                body = body.Replace("[$PostDescription$]", rev.Message);
                            }
                            else
                            {
                                body = body.Replace("[$PostDescription$]", "");
                            }


                            if (pimg != null)
                            {
                                //string imageURL = string.Format("{0}/ImageSizer?image=/UploadedPostImages/{1}&w=400", baseURL, pimg.URL);
                                //string imageTag = string.Format("<img width=400 src=\"{0}\" />", imageURL);
                                //body = body.Replace("[$PostImage$]", imageTag);
                                if (!baseURL.EndsWith("/"))
                                    baseURL += "/";
                                string img = string.Format("<img width='400' src='{0}ImageSizer?image=/UploadedPostImages/{1}&w=400' />", baseURL, pimg.URL);
                                body = body.Replace("[$PostImageorLink$]", img);

                            }
                            else if (!string.IsNullOrWhiteSpace(rev.Link))
                            {
                                body = body.Replace("[$PostImageorLink$]", rev.Link);
                            }
                            else
                            {
                                body = body.Replace("[$PostImageorLink$]", "");
                            }                           
                            #endregion

                            // post image
                            //if (pimg != null)
                            //{
                            //    string imageURL = string.Format("{0}/ImageSizer?image=/UploadedPostImages/{1}&w=400", baseURL, pimg.URL);
                            //    string imageTag = string.Format("<img width=400 src=\"{0}\" />", imageURL);
                            //    body = body.Replace("[$PostImage$]", imageTag);
                            //}
                            //else
                            //{
                            //    body = body.Replace("[$PostImage$]", "");
                            //}

                            
                            // content link
                            //if (string.IsNullOrWhiteSpace(rev.Link))
                            //{
                            //    body = body.Replace("[$LinkSection$]", "");
                            //}
                            //else
                            //{
                            //    string link = rev.Link;
                            //    string linktitle = rev.LinkTitle;
                            //    if (string.IsNullOrWhiteSpace(linktitle))
                            //        linktitle = "Click Here";
                            //    string linkdesc = rev.LinkDescription;
                            //    if (string.IsNullOrWhiteSpace(linkdesc))
                            //        linkdesc = "";

                            //    string lbody = dto.LinkSection;
                            //    lbody = lbody.Replace("[$LinkDescription$]", linkdesc);
                            //    lbody = lbody.Replace("[$LinkURL$]", link);
                            //    lbody = lbody.Replace("[$LinkTitle$]", linktitle);

                            //    body = body.Replace("[$LinkSection$]", lbody);
                            //}

                            #region link to platform
                            // link to platform
                            body = body.Replace("[$TokenLink$]", platformURL);
                            #endregion
                            // do the account list

                            //StringBuilder sbAccounts = new StringBuilder();

                            #region account list
                            string accountList = "<ul>";
                            foreach (var account in accounts)
                            {
                                
                                //string ablock = dto.Accounts;
                                string accountName = string.IsNullOrWhiteSpace(account.AccountDisplayName) ? account.AccountName : account.AccountDisplayName;
                                accountList = accountList + string.Format("<li>{0} ({1})</li>", accountName, account.SocialNetwork);                                                                
                              
                            }
                            accountList = accountList + "</ul>";

                            body = body.Replace("[$AccountsList$]", accountList);
                            #endregion

                            //body = body.Replace("[$Accounts$]", sbAccounts.ToString());
                            string subject = template.Subject;

                            // create a notification...
                            // all of these will be registered under autostartups, and nobody should ever subscribe
                            // 197920 = autostartups

                            DAL.Notification noti = new DAL.Notification()
                            {
                                AccountID = 197920,
                                NotificationMessageTypeID = (long)NotificationMessageTypeEnum.AutoApprovedNewContent,
                                DateCreated = DateTime.UtcNow,
                                EntityID = info.PostID
                            };
                            db.Notifications.AddObject(noti);
                            db.SaveChanges();

                            // create a target
                            NotificationTarget target = new NotificationTarget()
                            {
                                Body = body,
                                Subject = subject,
                                DateRead = DateTime.UtcNow,
                                DateSent = null,
                                NotificationStatusID = (long)NotificationStatusesEnum.Pending,
                                Address = info.UserEmailAddress,
                                NotificationMethodID = (long)NotificationMethodEnum.HtmlEmail
                            };
                            db.NotificationTargets.AddObject(target);
                            db.SaveChanges();

                            // create a recipient record
                            NotificationRecipient rec = new NotificationRecipient()
                            {
                                NotificationID = noti.ID,
                                NotificationTargetID = target.ID,
                                NotificationTemplateID = template.ID,
                                PickedUpDate = DateTime.UtcNow,
                                UserID = info.UserID
                            };
                            db.NotificationRecipients.AddObject(rec);
                            db.SaveChanges();

                            // send the notification

                            string sendFromEmailAddress = Settings.GetSetting("site.SendFromEmailAddress", "notifications@socialdealer.com");

                            EmailManager emailManager = new EmailManager();
                            bool sendResult = emailManager.sendNotification(
                                "AutoApprovals", subject, body,
                                sendFromEmailAddress, "reynolds@autostartups.com",
                                "s.abraham@socialdealer.com", null)
                            ;
                            sendResult = emailManager.sendNotification(
                                "AutoApprovals", subject, body,
                                sendFromEmailAddress,
                                "k.inman@socialdealer.com",
                                "e.krystof@socialdealer.com",
                                "p.penton@socialdealer.com"
                            );

                            if (sendResult)
                            {
                                target.DateSent = DateTime.UtcNow;
                                target.NotificationStatusID = (long)NotificationStatusesEnum.Success;
                            }
                            else
                            {
                                target.NotificationStatusID = (long)NotificationStatusesEnum.Failure;
                            }

                            db.SaveChanges();

                        }
                    }
                }
            }
        }
    }


}
