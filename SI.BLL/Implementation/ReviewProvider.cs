﻿using System.Globalization;
using Connectors;
using Connectors.Parameters;
using SI.DAL;
using SI.DTO;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Connectors.Reviews.Parameters;
using Connectors.Reviews;
using System.Data.Objects;
using SI.Core;
using Microsoft.Data.Extensions;
using System.Data.Common;
using Notification;
using System.Reflection;

namespace SI.BLL
{
    internal class ReviewProvider : IReviewProvider
    {

        #region diagnostics

        public void SendReviewDiagnosticsEmail()
        {
            //get the template
            string template = Util.GetResourceTextFile("RepDiag.html", Assembly.GetAssembly(typeof(ProviderFactory)));


            using (MainEntities db = ContextFactory.Main)
            {
                if (db.Connection.State != System.Data.ConnectionState.Open)
                    db.Connection.Open();
                using (DbCommand cmd = db.CreateStoreCommand(
                    "spReputationDiagnostics",
                     System.Data.CommandType.StoredProcedure
                ))
                {
                    using (DbDataReader reader = cmd.ExecuteReader())
                    {
                        List<JobStatusListItem> rdls = db.Translate<JobStatusListItem>(reader).ToList();
                        reader.NextResult();
                        List<JobStatusListItem> rint = db.Translate<JobStatusListItem>(reader).ToList();

                        int dlCountFailed = (from r in rdls where r.JobStatusID == 50 select r.Count).FirstOrDefault();
                        int dlCountSucceeded = (from r in rdls where r.JobStatusID == 40 select r.Count).FirstOrDefault();
                        int intCountFailed = (from r in rint where r.JobStatusID == 50 select r.Count).FirstOrDefault();
                        int intCountSucceeded = (from r in rint where r.JobStatusID == 40 select r.Count).FirstOrDefault();

                        template = template.Replace("[d_failed]", dlCountFailed.ToString(CultureInfo.InvariantCulture));
                        template = template.Replace("[d_succeeded]", dlCountSucceeded.ToString(CultureInfo.InvariantCulture));
                        template = template.Replace("[i_failed]", intCountFailed.ToString(CultureInfo.InvariantCulture));
                        template = template.Replace("[i_succeeded]", intCountSucceeded.ToString(CultureInfo.InvariantCulture));

                        int d_percent = Convert.ToInt32((Convert.ToSingle(dlCountFailed) / (Convert.ToSingle(dlCountFailed) + Convert.ToSingle(dlCountSucceeded))) * 100);
                        template = template.Replace("[d_percent]", d_percent.ToString(CultureInfo.InvariantCulture));
                        int i_percent = Convert.ToInt32((Convert.ToSingle(intCountFailed) / (Convert.ToSingle(intCountFailed) + Convert.ToSingle(intCountSucceeded))) * 100);
                        template = template.Replace("[i_percent]", i_percent.ToString(CultureInfo.InvariantCulture));

                        StringBuilder sb = new StringBuilder();
                        reader.NextResult();

                        while (reader.Read())
                        {
                            Single rs_missing = Convert.ToSingle(Util.GetReaderValue<long>(reader, "CountMissing", 0));
                            Single rs_total = Convert.ToSingle(Util.GetReaderValue<long>(reader, "Count", 0));
                            int res_percent = Convert.ToInt32((rs_missing / rs_total) * 100);

                            sb.Append("<tr>");
                            sb.AppendFormat("<td>{0}</td>", Util.GetReaderValue<long>(reader, "ReviewSourceID", 0));
                            sb.AppendFormat("<td>{0}</td>", Util.GetReaderValue<string>(reader, "ReviewSource", ""));
                            sb.AppendFormat("<td>{0}</td>", rs_missing);
                            sb.AppendFormat("<td>{0}</td>", rs_total);
                            sb.AppendFormat("<td><b>{0}</b></td>", res_percent);
                            sb.Append("</tr>");
                        }
                        template = template.Replace("[12hmissing]", sb.ToString());

                        reader.NextResult();
                        sb.Clear();

                        while (reader.Read())
                        {
                            sb.Append("<tr>");
                            sb.AppendFormat("<td>{0}</td>", Util.GetReaderValue<string>(reader, "ReviewSource", ""));
                            sb.AppendFormat("<td>{0}</td>", Util.GetReaderValue<DateTime>(reader, "ReviewDate", DateTime.MinValue).ToString("MM/dd/yyyy"));
                            sb.Append("</tr>");
                        }
                        template = template.Replace("[ByReviewDate]", sb.ToString());


                    }
                }

            }

            EmailManager emailManager = new EmailManager();
            bool sendResult = emailManager.sendNotification(
                "Diagnostics",
                "Reputation System Diagnostics",
                template,
                Settings.GetSetting("site.SendFromEmailAddress", "notifications@socialdealer.com"),
                "reynolds@autostartups.com",
                null,
                null
            );
            sendResult = emailManager.sendNotification(
                "Diagnostics",
                "Reputation System Diagnostics",
                template,
                Settings.GetSetting("site.SendFromEmailAddress", "notifications@socialdealer.com"),
                "sanju@autostartups.com",
                null,
                null
            );
            sendResult = emailManager.sendNotification(
                "Diagnostics",
                "Reputation System Diagnostics",
                template,
                Settings.GetSetting("site.SendFromEmailAddress", "notifications@socialdealer.com"),
                "a.patel@socialdealer.com",
                null,
                null
            );
            sendResult = emailManager.sendNotification(
                "Diagnostics",
                "Reputation System Diagnostics",
                template,
                Settings.GetSetting("site.SendFromEmailAddress", "notifications@socialdealer.com"),
                "dave@autostartups.com",
                null,
                null
            );
        }

        private class JobStatusListItem
        {
            public long JobStatusID { get; set; }
            public string Status { get; set; }
            public int Count { get; set; }
        }

        public long CreateParkedDownloaderJob(long accountReviewSourceID)
        {
            ReviewDownloaderData data = new ReviewDownloaderData()
            {
                Account_ReviewSourceID = accountReviewSourceID,
                DownloadDetails = true,
                RemainingRetries = 0
            };

            JobDTO job = new JobDTO()
            {
                DateScheduled = SIDateTime.Now,
                JobDataObject = data,
                JobType = data.JobType,
                JobStatus = JobStatusEnum.Parked,
                Priority = 100
            };

            SaveEntityDTO<JobDTO> save = ProviderFactory.Jobs.Save(new SaveEntityDTO<JobDTO>(job, UserInfoDTO.System));
            return save.Entity.ID.Value;

        }

        public long CreateParkedDownloaderJob(long accountID, ReviewSourceEnum reviewSource)
        {
            using (MainEntities db = ContextFactory.Main)
            {
                long arsid = (from ars in db.Accounts_ReviewSources where ars.AccountID == accountID && ars.ReviewSourceID == (long)reviewSource select ars.ID).First();
                return CreateParkedDownloaderJob(arsid);
            }
        }

        public long CreateParkedIntegratorJob(long accountID, ReviewSourceEnum reviewSource)
        {
            ReviewIntegratorData data = new ReviewIntegratorData()
            {
                AccountID = accountID,
                ReviewSourceID = (long)reviewSource,
                RemainingRetries = 0
            };

            JobDTO job = new JobDTO()
            {
                DateScheduled = SIDateTime.Now,
                JobDataObject = data,
                JobType = data.JobType,
                JobStatus = JobStatusEnum.Parked,
                Priority = 100
            };

            SaveEntityDTO<JobDTO> save = ProviderFactory.Jobs.Save(new SaveEntityDTO<JobDTO>(job, UserInfoDTO.System));
            return save.Entity.ID.Value;
        }

        public bool CheckIfReviewsMatch(long reviewID1, long reviewID2)
        {
            using (MainEntities db = ContextFactory.Main)
            {
                Review r1 = (from r in db.Reviews where r.ID == reviewID1 select r).FirstOrDefault();
                Review r2 = (from r in db.Reviews where r.ID == reviewID2 select r).FirstOrDefault();

                ReviewDTO rd1 = reviewDTOFromReview(r1, TimeZoneInfo.Utc);
                ReviewDTO rd2 = reviewDTOFromReview(r2, TimeZoneInfo.Utc);

                List<string> wl1 = ReviewMatching.GetWordList(r1.ReviewText);
                List<string> wl2 = ReviewMatching.GetWordList(r2.ReviewText);

                //review
                return ReviewMatching.ReviewsMatch(rd1, wl1, rd2, wl2, 30);

            }

        }


        #endregion

        public List<DTO.ReviewWorklistItemDTO> GetReviewValidatorWorklist(ReviewSourceEnum reviewSource, int minMinutesSinceLastValidation, bool onlyMissingURL, bool onlyInvalidURL)
        {
            throw new NotImplementedException();
        }

        public List<long> GetCountryIDsForAccounts(List<long> accountIDs)
        {
            List<long> countryIDs = new List<long>();
            using (MainEntities db = ContextFactory.Main)
            {
                if (db.Connection.State != System.Data.ConnectionState.Open)
                    db.Connection.Open();

                using (DbCommand cmd = db.CreateStoreCommand(
                    "spGetCountryIDListForAccounts",
                     System.Data.CommandType.StoredProcedure,

                     new SqlParameter("@accountIDs", string.Join("|", accountIDs.ToArray()))
                ))
                {
                    using (DbDataReader reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            countryIDs.Add(Util.GetReaderValue<long>(reader, "CountryID", 0));
                        }
                    }
                }
            }
            return countryIDs;
        }

        public List<DTO.ReviewSourceDTO> GetReviewSources(long accountID)
        {
            using (MainEntities db = ContextFactory.Main)
            {
                List<long> countryIDs = GetCountryIDsForAccounts(new List<long>() { accountID });

                long? VerticalID = (from a in db.Accounts where a.ID == accountID select a.VerticalID).FirstOrDefault();

                if (VerticalID == null)
                {
                    VerticalID = 2;
                }

                List<long> reviewSourceIDs =
                (
                    from rv in db.ReviewSources_Verticals
                    join rs in db.ReviewSources on rv.ReviewSourceID equals rs.ID
                    join rc in db.ReviewSources_Countries on rs.ID equals rc.ReviewSourceID
                    where rv.VerticalID == VerticalID && countryIDs.Contains(rc.CountryID)
                    select rs.ID
                ).Distinct().ToList();

                return (
                    from rs in db.ReviewSources
                    where reviewSourceIDs.Contains(rs.ID)
                    select new ReviewSourceDTO()
                    {
                        ID = rs.ID,
                        Name = rs.Name,
                        DisplayOrder = rs.DisplayOrder,
                        Status = (ReviewSourceStatusEnum)rs.Status,
                        ImageUrl = rs.ImagePath
                    }
                ).ToList();
            }

        }

        public List<ReviewSourceDTO> GetReviewSources(UserInfoDTO userInfo)
        {
            using (MainEntities db = ContextFactory.Main)
            {
                if (userInfo.EffectiveUser.SuperAdmin)
                {
                    return (
                        from rs in db.ReviewSources
                        select new ReviewSourceDTO()
                        {
                            ID = rs.ID,
                            Name = rs.Name,
                            DisplayOrder = rs.DisplayOrder,
                            Status = (ReviewSourceStatusEnum)rs.Status,
                            ImageUrl = rs.ImagePath
                        }
                    ).ToList();
                }
                else
                {
                    List<long> accountIDs = ProviderFactory.Security.AccountsWithPermission(userInfo.EffectiveUser.ID.Value, PermissionTypeEnum.reviews_view);
                    List<long> countryIDs = GetCountryIDsForAccounts(accountIDs);

                    long? VerticalID = (from a in db.Accounts where a.ID == userInfo.EffectiveUser.CurrentContextAccountID select a.VerticalID).FirstOrDefault();

                    if (VerticalID == null)
                    {
                        VerticalID = 2;
                    }

                    List<long> reviewSourceIDs =
                    (
                        from rv in db.ReviewSources_Verticals
                        join rs in db.ReviewSources on rv.ReviewSourceID equals rs.ID
                        join rc in db.ReviewSources_Countries on rs.ID equals rc.ReviewSourceID
                        where rv.VerticalID == VerticalID && countryIDs.Contains(rc.CountryID)
                        select rs.ID
                    ).Distinct().ToList();

                    return (
                        from rs in db.ReviewSources
                        where reviewSourceIDs.Contains(rs.ID)
                        select new ReviewSourceDTO()
                        {
                            ID = rs.ID,
                            Name = rs.Name,
                            DisplayOrder = rs.DisplayOrder,
                            Status = (ReviewSourceStatusEnum)rs.Status,
                            ImageUrl = rs.ImagePath
                        }
                    ).ToList();
                }
            }
        }

        private static Dictionary<string, string> _processorSettingsCache = null;
        public string GetProcessorSetting(string key)
        {
            if (_processorSettingsCache == null)
            {
                _processorSettingsCache = new Dictionary<string, string>();
                lock (_processorSettingsCache)
                {
                    using (SystemDBEntities db = ContextFactory.System)
                    {
                        List<ProcessorSetting> list = (from p in db.ProcessorSettings select p).ToList();
                        foreach (ProcessorSetting set in list)
                        {
                            _processorSettingsCache.Add(set.Name.Trim().ToLower(), set.Value);
                        }
                    }
                }
            }

            lock (_processorSettingsCache)
            {
                string value = null;
                _processorSettingsCache.TryGetValue(key.Trim().ToLower(), out value);
                return value;
            }

        }

        public ReviewDTO GetReviewByID(long reviewID)
        {
            using (MainEntities db = ContextFactory.Main)
            {
                Review review = (from r in db.Reviews where r.ID == reviewID select r).FirstOrDefault();
                if (review == null) return null;
                return reviewDTOFromReview(review, TimeZoneInfo.Utc);
            }
        }

        public SIReviewConnection GetReviewConnection(ReviewSourceEnum reviewSource, int timeOutSeconds = 30, bool useProxy = true, int maxRedirects = 50)
        {
            try
            {
                SIReviewConnection conn = null;

                switch (reviewSource)
                {
                    case ReviewSourceEnum.Google:
                        {
                            #region Google

                            SIGoogleReviewXPathParameters xGoogle = new SIGoogleReviewXPathParameters();
                            SIGoogleReviewConnectionParameters pGoogle = new SIGoogleReviewConnectionParameters(xGoogle, timeOutSeconds, useProxy, maxRedirects);

                            fillBaseReviewDownloaderXPaths(pGoogle.ProcessorSettingsPrefix(), xGoogle);

                            xGoogle.freq = GetProcessorSetting(pGoogle.ProcessorSettingsPrefix() + ".freq");
                            xGoogle.At = GetProcessorSetting(pGoogle.ProcessorSettingsPrefix() + ".At");
                            xGoogle.ApiURL = GetProcessorSetting(pGoogle.ProcessorSettingsPrefix() + ".ApiURL");

                            conn = new GoogleReviewsConnection(pGoogle);
                            #endregion
                        }
                        break;
                    case ReviewSourceEnum.DealerRater:
                        {
                            #region DealerRater

                            SIDealerRaterReviewXPathParameters xDealerRater = new SIDealerRaterReviewXPathParameters();
                            SIDealerRaterReviewConnectionParameters pDealerRater = new SIDealerRaterReviewConnectionParameters(xDealerRater, timeOutSeconds, useProxy, maxRedirects);
                            fillBaseReviewDownloaderXPaths(pDealerRater.ProcessorSettingsPrefix(), xDealerRater);

                            xDealerRater.APIURLTemplate = GetProcessorSetting("DealerRater.APIURLTemplate");

                            xDealerRater.ReviewerNameXPath = GetProcessorSetting("DealerRater.ReviewerNameXPath");
                            xDealerRater.RatingValueXPath2 = GetProcessorSetting("DealerRater.RatingValueXPath2");
                            xDealerRater.ReviewTextFullXPath = GetProcessorSetting("DealerRater.ReviewTextFullXPath");
                            xDealerRater.ReviewDateXPath = GetProcessorSetting("DealerRater.ReviewDateXPath");

                            xDealerRater.CommentIteratorXPath = GetProcessorSetting("DealerRater.CommentIteratorXPath");
                            xDealerRater.CommenterNameXPath = GetProcessorSetting("DealerRater.CommenterNameXPath");
                            xDealerRater.CommentDateAddedXPath = GetProcessorSetting("DealerRater.CommentDateAddedXPath");
                            xDealerRater.CommentTextFullXPath = GetProcessorSetting("DealerRater.CommentTextFullXPath");

                            xDealerRater.ExtraInfoKeys = GetProcessorSetting("DealerRater.ExtraInfoKeysXPath");
                            xDealerRater.ExtraInfoXPaths = GetProcessorSetting("DealerRater.ExtraInfoValuesXPath");

                            xDealerRater.ExtraInfoKeysDetailXPath = GetProcessorSetting("DealerRater.ExtraInfoKeysDetailXPath");
                            xDealerRater.ExtraInfoValuesDetailXPath = GetProcessorSetting("DealerRater.ExtraInfoValuesDetailXPath");

                            xDealerRater.ReasonForVisitXPath = GetProcessorSetting("DealerRater.ReasonForVisitXPath");

                            xDealerRater.PagingNextXPath = GetProcessorSetting(pDealerRater.ProcessorSettingsPrefix() + ".PagingNextXPath");

                            xDealerRater.ReviewRecommendationExtraInfoXPath = GetProcessorSetting("DealerRater.ReviewRecommendationExtraInfoXPath");

                            conn = new DealerRaterReviewsConnection(pDealerRater);
                            #endregion
                        }
                        break;
                    case ReviewSourceEnum.Yahoo:
                        {
                            #region Yahoo

                            SIYahooLocalReviewXPathParameters xYahooLocal = new SIYahooLocalReviewXPathParameters();
                            SIYahooLocalReviewConnectionParameters pYahooLocal = new SIYahooLocalReviewConnectionParameters(xYahooLocal, timeOutSeconds, useProxy, maxRedirects);

                            fillBaseReviewDownloaderXPaths(pYahooLocal.ProcessorSettingsPrefix(), xYahooLocal);

                            xYahooLocal.APIURLTemplate = GetProcessorSetting(pYahooLocal.ProcessorSettingsPrefix() + ".APIURLTemplate");
                            xYahooLocal.ReviewsPerPage = Convert.ToInt32(GetProcessorSetting(pYahooLocal.ProcessorSettingsPrefix() + ".ReviewsPerPage"));

                            conn = new YahooLocalReviewsConnection(pYahooLocal);
                            #endregion
                        }
                        break;
                    case ReviewSourceEnum.Yelp:
                        {
                            #region Yelp

                            SIYelpReviewXPathParameters x = new SIYelpReviewXPathParameters();
                            SIYelpReviewConnectionParameters p = new SIYelpReviewConnectionParameters(x, timeOutSeconds, useProxy, maxRedirects);

                            fillBaseReviewDownloaderXPaths(p.ProcessorSettingsPrefix(), x);

                            //public string ReviewerNameXPath { get; set; }
                            //public string ReviewDateXPath { get; set; }
                            //public string ReviewTextFullXPath { get; set; }

                            x.ReviewerNameXPath = GetProcessorSetting("Yelp.ReviewerNameXPath");
                            x.ReviewDateXPath = GetProcessorSetting("Yelp.ReviewDateXPath");
                            x.ReviewTextFullXPath = GetProcessorSetting("Yelp.ReviewTextFullXPath");
                            x.FrenchReviewXPath = GetProcessorSetting("Yelp.FrenchReviewXPath");
                            x.FilteredReviewsCountXPath = GetProcessorSetting("Yelp.FilteredReviewsCountXPath");

                            conn = new YelpReviewsConnection(p);
                            #endregion
                        }
                        break;
                    case ReviewSourceEnum.CarDealer:
                        {
                            #region CarDealer

                            SICarDealerReviewsReviewXPathParameters xCarDealerReviews = new SICarDealerReviewsReviewXPathParameters();
                            SICarDealerReviewsReviewConnectionParameters pCarDealerReviews = new SICarDealerReviewsReviewConnectionParameters(xCarDealerReviews, timeOutSeconds, useProxy, maxRedirects);

                            fillBaseReviewDownloaderXPaths(pCarDealerReviews.ProcessorSettingsPrefix(), xCarDealerReviews);

                            conn = new CarDealerReviewsConnection(pCarDealerReviews);
                            #endregion
                        }
                        break;
                    case ReviewSourceEnum.YellowPages:
                        {
                            #region YellowPages

                            SIYellowPagesReviewXPathParameters xYellowPages = new SIYellowPagesReviewXPathParameters();
                            SIYellowPagesReviewConnectionParameters pYellowPages = new SIYellowPagesReviewConnectionParameters(xYellowPages, timeOutSeconds, useProxy, maxRedirects);

                            fillBaseReviewDownloaderXPaths(pYellowPages.ProcessorSettingsPrefix(), xYellowPages);

                            //xYellowPages.ReviewTextFull = @".//p[@class='review-text truncated']";
                            //xYellowPages.ReviewerName = @"div/p[2]/strong/a";
                            //xYellowPages.ReviewDate = "div/p[2]/text()[2]";

                            xYellowPages.ReviewTextFull = GetProcessorSetting(pYellowPages.ProcessorSettingsPrefix() + ".ReviewTextFullXPath");
                            xYellowPages.ReviewerName = GetProcessorSetting(pYellowPages.ProcessorSettingsPrefix() + ".ReviewerNameXPath");
                            xYellowPages.ReviewDate = GetProcessorSetting(pYellowPages.ProcessorSettingsPrefix() + ".ReviewDateXPath");
                            xYellowPages.RatingCollected = GetProcessorSetting(pYellowPages.ProcessorSettingsPrefix() + ".RatingValueXPath");
                            xYellowPages.ReviewCommentXPath = GetProcessorSetting(pYellowPages.ProcessorSettingsPrefix() + ".ReviewCommentXPath");
                            xYellowPages.ReviewCommentNameXPath = GetProcessorSetting(pYellowPages.ProcessorSettingsPrefix() + ".ReviewCommentNameXPath");
                            xYellowPages.ReviewCommentTextXPath = GetProcessorSetting(pYellowPages.ProcessorSettingsPrefix() + ".ReviewCommentTextXPath");
                            xYellowPages.PagingNextXPath = GetProcessorSetting(pYellowPages.ProcessorSettingsPrefix() + ".PagingNextXPath");
                            xYellowPages.ListingId = GetProcessorSetting(pYellowPages.ProcessorSettingsPrefix() + ".ListingId");
                            xYellowPages.ReviewCommentDate = GetProcessorSetting(pYellowPages.ProcessorSettingsPrefix() + ".ReviewCommentDateXPath");

                            xYellowPages.CA_RatingValuePath = GetProcessorSetting(pYellowPages.ProcessorSettingsPrefix() + ".CA_RatingValuePath");
                            xYellowPages.CA_ReviewCountPath = GetProcessorSetting(pYellowPages.ProcessorSettingsPrefix() + ".CA_ReviewCountPath");

                            xYellowPages.CA_NoReviewTextPath = GetProcessorSetting(pYellowPages.ProcessorSettingsPrefix() + ".CA_NoReviewTextPath");
                            xYellowPages.CA_NoRatingPath = GetProcessorSetting(pYellowPages.ProcessorSettingsPrefix() + ".CA_NoRatingPath");
                            xYellowPages.CA_ReviewDetailIteratorPath = GetProcessorSetting(pYellowPages.ProcessorSettingsPrefix() + ".CA_ReviewDetailIteratorPath");
                            xYellowPages.CA_ReviewTextFull = GetProcessorSetting(pYellowPages.ProcessorSettingsPrefix() + ".CA_ReviewTextFullXPath");
                            xYellowPages.CA_ReviewerName = GetProcessorSetting(pYellowPages.ProcessorSettingsPrefix() + ".CA_ReviewerNameXPath");
                            xYellowPages.CA_ReviewDate = GetProcessorSetting(pYellowPages.ProcessorSettingsPrefix() + ".CA_ReviewDateXPath");
                            xYellowPages.CA_RatingCollected = GetProcessorSetting(pYellowPages.ProcessorSettingsPrefix() + ".CA_RatingValueXPath");
                            xYellowPages.CA_ReviewCommentXPath = GetProcessorSetting(pYellowPages.ProcessorSettingsPrefix() + ".CA_ReviewCommentXPath");
                            xYellowPages.CA_ReviewCommentNameXPath = GetProcessorSetting(pYellowPages.ProcessorSettingsPrefix() + ".CA_ReviewCommentNameXPath");
                            xYellowPages.CA_ReviewCommentTextXPath = GetProcessorSetting(pYellowPages.ProcessorSettingsPrefix() + ".CA_ReviewCommentTextXPath");
                            xYellowPages.CA_PagingNextXPath = GetProcessorSetting(pYellowPages.ProcessorSettingsPrefix() + ".CA_PagingNextXPath");
                            xYellowPages.CA_ListingId = GetProcessorSetting(pYellowPages.ProcessorSettingsPrefix() + ".CA_ListingId");
                            xYellowPages.CA_ReviewCommentDate = GetProcessorSetting(pYellowPages.ProcessorSettingsPrefix() + ".ReviewCommentDateXPath");



                            conn = new YellowPagesReviewsConnection(pYellowPages);
                            #endregion
                        }

                        break;
                    case ReviewSourceEnum.Edmunds:
                        {
                            #region Edmunds

                            SIEdmundsReviewXPathParameters xEdmunds = new SIEdmundsReviewXPathParameters();
                            SIEdmundsReviewConnectionParameters pEdmunds = new SIEdmundsReviewConnectionParameters(xEdmunds, timeOutSeconds, useProxy, maxRedirects);

                            fillBaseReviewDownloaderXPaths(pEdmunds.ProcessorSettingsPrefix(), xEdmunds);

                            xEdmunds.APIURLTemplate = GetProcessorSetting(pEdmunds.ProcessorSettingsPrefix() + ".APIURLTemplate");
                            xEdmunds.NoServicesReviewText = GetProcessorSetting(pEdmunds.ProcessorSettingsPrefix() + ".NoServicesReviewText");
                            xEdmunds.NoServicesReviewText2 = GetProcessorSetting(pEdmunds.ProcessorSettingsPrefix() + ".NoServicesReviewText2");
                            xEdmunds.NoSalesReviewText2 = GetProcessorSetting(pEdmunds.ProcessorSettingsPrefix() + ".NoSalesReviewText2");
                            xEdmunds.NoSalesReviewText = GetProcessorSetting(pEdmunds.ProcessorSettingsPrefix() + ".NoSalesReviewText");
                            xEdmunds.NoServicesReviewTextXPath = GetProcessorSetting(pEdmunds.ProcessorSettingsPrefix() + ".NoServicesReviewTextXPath");
                            xEdmunds.NoSalesReviewTextXPath = GetProcessorSetting(pEdmunds.ProcessorSettingsPrefix() + ".NoSalesReviewTextXPath");

                            xEdmunds.NoServiceRatingPath = GetProcessorSetting(pEdmunds.ProcessorSettingsPrefix() + ".NoServiceRatingPath");
                            xEdmunds.NoServiceRatingText = GetProcessorSetting(pEdmunds.ProcessorSettingsPrefix() + ".NoServiceRatingText");
                            xEdmunds.ReviewCountServicePath = GetProcessorSetting(pEdmunds.ProcessorSettingsPrefix() + ".ReviewCountServicePath");
                            xEdmunds.RatingValueServicePath = GetProcessorSetting(pEdmunds.ProcessorSettingsPrefix() + ".RatingValueServicePath");

                            xEdmunds.ExtraValueSalesPath = GetProcessorSetting(pEdmunds.ProcessorSettingsPrefix() + ".ExtraValueSalesPath");
                            xEdmunds.ExtraValueServicesPath = GetProcessorSetting(pEdmunds.ProcessorSettingsPrefix() + ".ExtraValueServicesPath");

                            conn = new EdmundsReviewsConnection(pEdmunds);
                            #endregion
                        }
                        break;
                    case ReviewSourceEnum.CarsDotCom:
                        {
                            #region CarsDotCom

                            SICarsDotComReviewXPathParameters xCarsDotCom = new SICarsDotComReviewXPathParameters();
                            SICarsDotComReviewConnectionParameters pCarsDotCom = new SICarsDotComReviewConnectionParameters(xCarsDotCom, timeOutSeconds, useProxy, maxRedirects);

                            fillBaseReviewDownloaderXPaths(pCarsDotCom.ProcessorSettingsPrefix(), xCarsDotCom);

                            xCarsDotCom.ParentNodeExtraValueSummaryXPath = GetProcessorSetting(pCarsDotCom.ProcessorSettingsPrefix() + ".ParentNodeExtraValueSummaryXPath"); //".//div[@class='col13']/div";
                            xCarsDotCom.XtraValueWrapperXPath = GetProcessorSetting(pCarsDotCom.ProcessorSettingsPrefix() + ".XtraValueWrapperXPath"); //".//div[@class='wrapper']";
                            xCarsDotCom.XtraValueNodeXPath = GetProcessorSetting(pCarsDotCom.ProcessorSettingsPrefix() + ".XtraValueNodeXPath"); //".//div[@class='value']";
                            xCarsDotCom.XtraKeyNodeXPath = GetProcessorSetting(pCarsDotCom.ProcessorSettingsPrefix() + ".XtraKeyNodeXPath"); //".//div[@class='detail']";
                            xCarsDotCom.XtraNoWrapperValueNodeXPath = GetProcessorSetting(pCarsDotCom.ProcessorSettingsPrefix() + ".XtraNoWrapperValueNodeXPath"); //".//span//span";
                            xCarsDotCom.XtraNoWrapperKeyNodeXPath = GetProcessorSetting(pCarsDotCom.ProcessorSettingsPrefix() + ".XtraNoWrapperKeyNodeXPath"); //".//div[@class='detail']";
                            xCarsDotCom.ParentnodeExtraValueXPath = GetProcessorSetting(pCarsDotCom.ProcessorSettingsPrefix() + ".ParentnodeExtraValueXPath");
                            xCarsDotCom.ParentnodeExtraValueParaXPath = GetProcessorSetting(pCarsDotCom.ProcessorSettingsPrefix() + ".ParentnodeExtraValueParaXPath");
                            xCarsDotCom.ExtraValueSummaryRecommendedXPath = GetProcessorSetting(pCarsDotCom.ProcessorSettingsPrefix() + ".ExtraValueSummaryRecommendedXPath"); //".//div[@id='dealer-average-ratings']/div[1]/div[1]/p";
                            xCarsDotCom.ExtraValueSummaryRecommendedText = "Recommended";

                            xCarsDotCom.ReviewerNameXPath = GetProcessorSetting(pCarsDotCom.ProcessorSettingsPrefix() + ".ReviewerNameXPath");
                            xCarsDotCom.ReviewTextFullXPath = GetProcessorSetting(pCarsDotCom.ProcessorSettingsPrefix() + ".ReviewTextFullXPath");
                            xCarsDotCom.ReviewDateXPath = GetProcessorSetting(pCarsDotCom.ProcessorSettingsPrefix() + ".ReviewDateXPath");
                            xCarsDotCom.ReviewDetailIteratorPath = GetProcessorSetting(pCarsDotCom.ProcessorSettingsPrefix() + ".ReviewDetailIteratorPath");

                            xCarsDotCom.ParentNodeReviewCommentXPath = GetProcessorSetting(pCarsDotCom.ProcessorSettingsPrefix() + ".ParentNodeReviewCommentXPath");
                            xCarsDotCom.CommenterNameXPath = GetProcessorSetting(pCarsDotCom.ProcessorSettingsPrefix() + ".CommenterNameXPath");
                            xCarsDotCom.CommentDateXPath = GetProcessorSetting(pCarsDotCom.ProcessorSettingsPrefix() + ".CommentDateXPath");
                            xCarsDotCom.CommentXPath = GetProcessorSetting(pCarsDotCom.ProcessorSettingsPrefix() + ".CommentXPath");

                            conn = new CarsDotComReviewsConnection(pCarsDotCom);
                            #endregion
                        }
                        break;
                    case ReviewSourceEnum.CitySearch:
                        {
                            #region CitySearch

                            SICitySearchReviewXPathParameters xCitySearch = new SICitySearchReviewXPathParameters();
                            SICitySearchReviewConnectionParameters pCitySearch = new SICitySearchReviewConnectionParameters(xCitySearch, timeOutSeconds, useProxy, maxRedirects);

                            fillBaseReviewDownloaderXPaths(pCitySearch.ProcessorSettingsPrefix(), xCitySearch);

                            xCitySearch.ReviewRatingPath = GetProcessorSetting(pCitySearch.ProcessorSettingsPrefix() + ".ReviewRatingPath");
                            xCitySearch.ReviewerNameXPath = GetProcessorSetting(pCitySearch.ProcessorSettingsPrefix() + ".ReviewerNameXPath");
                            xCitySearch.ReviewDateXPath = GetProcessorSetting(pCitySearch.ProcessorSettingsPrefix() + ".ReviewDateXPath");
                            xCitySearch.ReviewTextFullXPath = GetProcessorSetting(pCitySearch.ProcessorSettingsPrefix() + ".ReviewTextFullXPath");

                            conn = new CitySearchReviewsConnection(pCitySearch);

                            #endregion
                        }
                        break;
                    case ReviewSourceEnum.JudysBook:
                        {
                            #region JudysBook

                            SIJudysBookReviewXPathParameters xJudysBook = new SIJudysBookReviewXPathParameters();
                            SIJudysBookReviewConnectionParameters pJudysBook = new SIJudysBookReviewConnectionParameters(xJudysBook, timeOutSeconds, useProxy, maxRedirects);

                            fillBaseReviewDownloaderXPaths(pJudysBook.ProcessorSettingsPrefix(), xJudysBook);

                            xJudysBook.ReviewURLXPath = GetProcessorSetting(pJudysBook.ProcessorSettingsPrefix() + ".ReviewURLXPath");
                            xJudysBook.FullReviewURLXPath = GetProcessorSetting(pJudysBook.ProcessorSettingsPrefix() + ".FullReviewURLXPath");
                            xJudysBook.ReviewRatingValuePath = GetProcessorSetting(pJudysBook.ProcessorSettingsPrefix() + ".ReviewRatingValuePath");
                            xJudysBook.ReviewerNameXPath = GetProcessorSetting(pJudysBook.ProcessorSettingsPrefix() + ".ReviewerNameXPath");
                            xJudysBook.ReviewDateXPath = GetProcessorSetting(pJudysBook.ProcessorSettingsPrefix() + ".ReviewDateXPath");
                            xJudysBook.ReviewTextFullParentXPath = GetProcessorSetting(pJudysBook.ProcessorSettingsPrefix() + ".ReviewTextFullParentXPath");
                            xJudysBook.ReviewTextFullXPath = GetProcessorSetting(pJudysBook.ProcessorSettingsPrefix() + ".ReviewTextFullXPath");

                            conn = new JudysBookReviewsConnection(pJudysBook);
                            #endregion
                        }
                        break;
                    case ReviewSourceEnum.InsiderPages:
                        {
                            #region InsiderPages

                            SIInsiderPagesReviewXPathParameters xInsiderPages = new SIInsiderPagesReviewXPathParameters();
                            SIInsiderPagesReviewConnectionParameters pInsiderPages = new SIInsiderPagesReviewConnectionParameters(xInsiderPages, timeOutSeconds, useProxy, maxRedirects);

                            fillBaseReviewDownloaderXPaths(pInsiderPages.ProcessorSettingsPrefix(), xInsiderPages);

                            xInsiderPages.ReviewRatingPath = GetProcessorSetting(pInsiderPages.ProcessorSettingsPrefix() + ".ReviewRatingPath");
                            xInsiderPages.ReviewerNameXPath = GetProcessorSetting(pInsiderPages.ProcessorSettingsPrefix() + ".ReviewerNameXPath");
                            xInsiderPages.ReviewDateXPath = GetProcessorSetting(pInsiderPages.ProcessorSettingsPrefix() + ".ReviewDateXPath");
                            xInsiderPages.ReviewTextFullXPath = GetProcessorSetting(pInsiderPages.ProcessorSettingsPrefix() + ".ReviewTextFullXPath");
                            xInsiderPages.NextPathURLPath = GetProcessorSetting(pInsiderPages.ProcessorSettingsPrefix() + ".NextPathURLPath");

                            conn = new InsiderPagesReviewsConnection(pInsiderPages);
                            #endregion
                        }
                        break;
                    case ReviewSourceEnum.DealerRaterCanada:
                        {
                            #region DealerRaterCanada

                            SIDealerRaterReviewXPathParameters x = new SIDealerRaterReviewXPathParameters();
                            SIDealerRaterReviewConnectionParameters p = new SIDealerRaterReviewConnectionParameters(x, timeOutSeconds, useProxy, maxRedirects);

                            fillBaseReviewDownloaderXPaths(p.ProcessorSettingsPrefix(), x);

                            x.APIURLTemplate = GetProcessorSetting("DealerRater.APIURLTemplate");

                            x.ReviewerNameXPath = GetProcessorSetting("DealerRater.ReviewerNameXPath");
                            x.RatingValueXPath2 = GetProcessorSetting("DealerRater.RatingValueXPath2");
                            x.ReviewTextFullXPath = GetProcessorSetting("DealerRater.ReviewTextFullXPath");
                            x.ReviewDateXPath = GetProcessorSetting("DealerRater.ReviewDateXPath");

                            x.CommentIteratorXPath = GetProcessorSetting("DealerRater.CommentIteratorXPath");
                            x.CommenterNameXPath = GetProcessorSetting("DealerRater.CommenterNameXPath");
                            x.CommentDateAddedXPath = GetProcessorSetting("DealerRater.CommentDateAddedXPath");
                            x.CommentTextFullXPath = GetProcessorSetting("DealerRater.CommentTextFullXPath");

                            x.ExtraInfoKeys = GetProcessorSetting("DealerRater.ExtraInfoKeysXPath");
                            x.ExtraInfoXPaths = GetProcessorSetting("DealerRater.ExtraInfoValuesXPath");

                            x.ExtraInfoKeysDetailXPath = GetProcessorSetting("DealerRater.ExtraInfoKeysDetailXPath");
                            x.ExtraInfoValuesDetailXPath = GetProcessorSetting("DealerRater.ExtraInfoValuesDetailXPath");

                            conn = new DealerRaterReviewsConnection(p);
                            #endregion
                        }
                        break;
                    case ReviewSourceEnum.YelpCanada:
                        {
                            #region YelpCanada

                            SIYelpReviewXPathParameters xYelpCanada = new SIYelpReviewXPathParameters();
                            SIYelpReviewConnectionParameters pYelpCanada = new SIYelpReviewConnectionParameters(xYelpCanada, timeOutSeconds, useProxy, maxRedirects);

                            fillBaseReviewDownloaderXPaths(pYelpCanada.ProcessorSettingsPrefix(), xYelpCanada);

                            xYelpCanada.ReviewerNameXPath = GetProcessorSetting("Yelp.ReviewerNameXPath");
                            xYelpCanada.ReviewDateXPath = GetProcessorSetting("Yelp.ReviewDateXPath");
                            xYelpCanada.ReviewTextFullXPath = GetProcessorSetting("Yelp.ReviewTextFullXPath");
                            xYelpCanada.FrenchReviewXPath = GetProcessorSetting("Yelp.FrenchReviewXPath");
                            xYelpCanada.FilteredReviewsCountXPath = GetProcessorSetting("Yelp.FilteredReviewsCountXPath");

                            conn = new YelpReviewsConnection(pYelpCanada);
                            #endregion
                        }
                        break;

                    case ReviewSourceEnum.CitySearchCanada:
                        {
                            #region CitySearchCanada

                            SICitySearchReviewXPathParameters xCitySearch = new SICitySearchReviewXPathParameters();
                            SICitySearchReviewConnectionParameters pCitySearch = new SICitySearchReviewConnectionParameters(xCitySearch, timeOutSeconds, useProxy, maxRedirects);

                            fillBaseReviewDownloaderXPaths(pCitySearch.ProcessorSettingsPrefix(), xCitySearch);

                            xCitySearch.ReviewRatingPath = GetProcessorSetting(pCitySearch.ProcessorSettingsPrefix() + ".ReviewRatingPath");
                            xCitySearch.ReviewerNameXPath = GetProcessorSetting(pCitySearch.ProcessorSettingsPrefix() + ".ReviewerNameXPath");
                            xCitySearch.ReviewDateXPath = GetProcessorSetting(pCitySearch.ProcessorSettingsPrefix() + ".ReviewDateXPath");
                            xCitySearch.ReviewTextFullXPath = GetProcessorSetting(pCitySearch.ProcessorSettingsPrefix() + ".ReviewTextFullXPath");

                            conn = new CitySearchReviewsConnection(pCitySearch);
                            #endregion
                        }
                        break;
                    case ReviewSourceEnum.Facebook:
                        {
                            #region Facebook

                            SIFacebookReviewXPathParemeters xFacebook = new SIFacebookReviewXPathParemeters();
                            SIFacebookReviewConnectionParameters pFacebook = new SIFacebookReviewConnectionParameters(xFacebook, timeOutSeconds, useProxy, maxRedirects);

                            conn = new FacebookReviewsConnection(pFacebook);

                            #endregion
                        }
                        break;
                }

                return conn;
            }
            catch (Exception ex)
            {
                ProviderFactory.Logging.LogException(ex);
                return null;
            }
        }

        private void fillBaseReviewDownloaderXPaths(string namePrefix, SIBaseReviewXPathParameters xpathParameters)
        {
            xpathParameters.AddressPath = GetProcessorSetting(string.Format("{0}.AddressPath", namePrefix));
            xpathParameters.CityPath = GetProcessorSetting(string.Format("{0}.CityPath", namePrefix));
            xpathParameters.LatitudePath = GetProcessorSetting(string.Format("{0}.LatitudePath", namePrefix));
            xpathParameters.LocationNamePath = GetProcessorSetting(string.Format("{0}.LocationNamePath", namePrefix));
            xpathParameters.LongitudePath = GetProcessorSetting(string.Format("{0}.LongitudePath", namePrefix));
            xpathParameters.ManufacturerPath = GetProcessorSetting(string.Format("{0}.ManufacturerPath", namePrefix));
            xpathParameters.NoRatingPath = GetProcessorSetting(string.Format("{0}.NoRatingPath", namePrefix));
            xpathParameters.NoRatingText = GetProcessorSetting(string.Format("{0}.NoRatingText", namePrefix));
            xpathParameters.NoReviewPath = GetProcessorSetting(string.Format("{0}.NoReviewTextPath", namePrefix));
            xpathParameters.NoReviewText = GetProcessorSetting(string.Format("{0}.NoReviewText", namePrefix));
            xpathParameters.PhonePath = GetProcessorSetting(string.Format("{0}.PhonePath", namePrefix));
            xpathParameters.RatingValuePath = GetProcessorSetting(string.Format("{0}.RatingValuePath", namePrefix));
            xpathParameters.ReviewCountPath = GetProcessorSetting(string.Format("{0}.ReviewCountPath", namePrefix));
            xpathParameters.ReviewDetailIteratorPath = GetProcessorSetting(string.Format("{0}.ReviewDetailIteratorPath", namePrefix));
            xpathParameters.StatePath = GetProcessorSetting(string.Format("{0}.StatePath", namePrefix));
            xpathParameters.WebsiteURLPath = GetProcessorSetting(string.Format("{0}.WebsiteURLPath", namePrefix));
            xpathParameters.ZipPath = GetProcessorSetting(string.Format("{0}.ZipPath", namePrefix));
            xpathParameters.RatingValueXPath = GetProcessorSetting(string.Format("{0}.RatingValueXPath", namePrefix));
        }


        public ReviewUrlDTO GetReviewUrl(UserInfoDTO userInfo, long accountID, ReviewSourceEnum reviewSource)
        {
            ITimezoneProvider tz = ProviderFactory.TimeZones;

            using (MainEntities db = ContextFactory.Main)
            {
                Accounts_ReviewSources ars = (from a in db.Accounts_ReviewSources where a.AccountID.Equals(accountID) && a.ReviewSourceID.Equals((long)reviewSource) select a).FirstOrDefault();
                return AccountReviewSourceToDTO(ars, tz.GetTimezoneForUserID(userInfo.EffectiveUser.ID.Value));
            }

        }

        public List<AccountReviewUrlDTO> GetReviewUrls(long accountID)
        {
            ITimezoneProvider tz = ProviderFactory.TimeZones;
            List<AccountReviewUrlDTO> urls = new List<AccountReviewUrlDTO>();

            using (MainEntities db = ContextFactory.Main)
            {
                if (db.Connection.State != System.Data.ConnectionState.Open)
                    db.Connection.Open();

                using (DbCommand cmd = db.CreateStoreCommand(
                    "spGetReviewUrlsByAccountID",
                     System.Data.CommandType.StoredProcedure,

                     new SqlParameter("@accountID", accountID)
                ))
                {
                    using (DbDataReader reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            AccountReviewUrlDTO url = new AccountReviewUrlDTO();

                            url.AccountID = Util.GetReaderValue<long>(reader, "AccountID", 0);
                            url.ReviewSource = (ReviewSourceEnum)Util.GetReaderValue<long>(reader, "ReviewSourceID", 0);
                            url.ApiURL = Util.GetReaderValue<string>(reader, "URLAPI", "");
                            url.ExternalID = Util.GetReaderValue<string>(reader, "ExternalID", "");
                            url.ExternalID2 = Util.GetReaderValue<string>(reader, "ExternalID2", "");
                            url.HtmlURL = Util.GetReaderValue<string>(reader, "URL", "");
                            url.ID = Util.GetReaderValue<long>(reader, "ID", 0);
                            url.isValid = Util.GetReaderValue<bool>(reader, "IsValid", false);
                            url.ReviewSourceName = Util.GetReaderValue<string>(reader, "Name", "");
                            url.SummaryDelta = Util.GetReaderValue<int>(reader, "SummaryDelta", 0);
                            url.DetailDelta = Util.GetReaderValue<int>(reader, "DetailDelta", 0);
                            url.DateCreated = Util.GetReaderValue<DateTime>(reader, "DateCreated", DateTime.UtcNow);
                            url.URLCreatedHours = Util.GetReaderValue<int>(reader, "URLCreatedHours", 0);

                            //Show as Valid when the (LastSummaryAttempted - LastSummaryRun) = 0 and IsValid flag is set to true. 
                            if (url.SummaryDelta == 0 && url.isValid)
                            {
                                url.Status = ReviewUrlDTOEnum.Valid;
                            } //Show as InValid when the (IsValid flag is set to false) OR (IsValid = true AND ((LastSummaryAttempted - LastSummaryRun) > 3) AND (DateCreated is less than 3 hours from now)) OR ((LastSummaryRun is null) AND (DateCreated is less than 3 hours from now))
                            else if (!url.isValid || (url.isValid && url.SummaryDelta > 3 && url.URLCreatedHours < 3) || (url.SummaryDelta == null && url.URLCreatedHours < 3))
                            {
                                url.Status = ReviewUrlDTOEnum.InValid;
                            }
                            else
                            {
                                url.Status = ReviewUrlDTOEnum.NotYetRan;
                            }
                            //url.Status Util.GetReaderValue<long>(reader, "AccountID", 0);


                            //countryIDs.Add(Util.GetReaderValue<long>(reader, "CountryID", 0));
                            urls.Add(url);

                        }
                    }
                }

                //return (
                //    from a in db.Accounts_ReviewSources
                //    join r in db.ReviewSources on a.ReviewSourceID equals r.ID
                //    where a.AccountID.Equals(accountID)
                //    select new AccountReviewUrlDTO()
                //    {
                //        AccountID = a.AccountID,
                //        ReviewSource = (ReviewSourceEnum)a.ReviewSourceID,
                //        ApiURL = a.URLAPI,
                //        ExternalID = a.ExternalID,
                //        ExternalID2 = a.ExternalID2,
                //        HtmlURL = a.URL,
                //        ID = a.ID,
                //        isValid = a.IsValid,
                //        ReviewSourceName = r.Name
                //    }
                //).ToList();
            }

            return urls;
        }


        public ReviewUrlDTO SaveReviewUrl(UserInfoDTO userInfo, ReviewUrlDTO dto)
        {
            ITimezoneProvider tz = ProviderFactory.TimeZones;
            TimeZoneInfo userTimeZone = tz.GetTimezoneForUserID(userInfo.EffectiveUser.ID.Value);

            //check for changes to log:
            string oldURL = "";
            ReviewUrlDTO oldDTO = GetReviewUrl(userInfo, dto.AccountID, dto.ReviewSource);
            if (oldDTO != null)
            {
                if (!string.IsNullOrWhiteSpace(oldDTO.HtmlURL))
                {
                    oldURL = oldDTO.HtmlURL;
                }
            }
            string newURL = "";
            if (!string.IsNullOrWhiteSpace(dto.HtmlURL))
            {
                newURL = dto.HtmlURL;
            }

            string message = string.Format("New URL: [{0}]", newURL);
            message = string.Format("{0}, Old URL: [{1}]", message, oldURL);

            if (oldURL.Trim().ToLower() != newURL.Trim().ToLower())
            {
                Auditor
                    .New(AuditLevelEnum.Information, AuditTypeEnum.UserActivity, userInfo, "")
                    .SetAuditDataObject(
                        AuditUserActivity
                        .New(userInfo, AuditUserActivityTypeEnum.ReviewURLChanged)
                            .SetReviewSourceID((long)dto.ReviewSource)
                            .SetAccountID(dto.AccountID)
                            .SetDescription(message)
                    )
                    .Save(ProviderFactory.Logging)
                ;
            }

            using (MainEntities db = ContextFactory.Main)
            {
                if (!string.IsNullOrEmpty(newURL))
                {
                    Accounts_ReviewSources ars = (
                                                     from a in db.Accounts_ReviewSources
                                                     where a.AccountID.Equals(dto.AccountID)
                                                           && a.ReviewSourceID.Equals((long)dto.ReviewSource)
                                                     select a
                                                 ).FirstOrDefault();
                    if (ars == null)
                        ars = new Accounts_ReviewSources() { AccountID = dto.AccountID, ReviewSourceID = (long)dto.ReviewSource, DateCreated = DateTime.UtcNow };

                    ars.URL = dto.HtmlURL;
                    ars.URLAPI = dto.ApiURL;
                    ars.ExternalID = dto.ExternalID;
                    ars.ExternalID2 = dto.ExternalID2;
                    ars.LastSummaryRun = (dto.LastSummaryRun == null) ? null : (DateTime?)dto.LastSummaryRun.GetUTCDate(userTimeZone);
                    ars.LastDetailRun = (dto.LastDetailRun == null) ? null : (DateTime?)dto.LastDetailRun.GetUTCDate(userTimeZone);
                    ars.LastSummeryAttempted = (dto.LastSummaryAttempted == null) ? null : (DateTime?)dto.LastSummaryAttempted.GetUTCDate(userTimeZone);
                    ars.LastDetailAttempted = (dto.LastDetailAttempted == null) ? null : (DateTime?)dto.LastDetailAttempted.GetUTCDate(userTimeZone);
                    ars.LastValidation = (dto.LastValidation == null) ? null : (DateTime?)dto.LastValidation.GetUTCDate(userTimeZone);
                    ars.DateCreated = DateTime.UtcNow;
                    ars.IsValid = dto.isValid;

                    if (ars.EntityState == System.Data.EntityState.Detached)
                        db.Accounts_ReviewSources.AddObject(ars);

                    db.SaveChanges();

                    dto = GetReviewUrl(userInfo, ars.ID);
                }
                else //delete
                {
                    db.spDeleteReviewURL(dto.AccountID, (long)dto.ReviewSource);
                    dto.DateCreated = userInfo.EffectiveUser.CurrentDateTime;
                }
            }

            return dto;
        }

        public ReviewUrlDTO GetReviewUrl(UserInfoDTO userInfo, long accountReviewSourceID)
        {
            ITimezoneProvider tz = ProviderFactory.TimeZones;

            using (MainEntities db = ContextFactory.Main)
            {
                Accounts_ReviewSources ars = (from a in db.Accounts_ReviewSources where a.ID.Equals(accountReviewSourceID) select a).FirstOrDefault();
                return AccountReviewSourceToDTO(ars, tz.GetTimezoneForUserID(userInfo.EffectiveUser.ID.Value));
            }
        }

        public List<ReviewUrlDTO> GetReviewUrls(UserInfoDTO userInfo, List<long> account_reviewSourceIDs)
        {
            ITimezoneProvider tz = ProviderFactory.TimeZones;
            List<ReviewUrlDTO> results = new List<ReviewUrlDTO>();


            using (MainEntities db = ContextFactory.Main)
            {
                List<Accounts_ReviewSources> items = (from a in db.Accounts_ReviewSources where account_reviewSourceIDs.Contains(a.ID) select a).ToList();
                foreach (Accounts_ReviewSources item in items)
                {
                    results.Add(AccountReviewSourceToDTO(item, tz.GetTimezoneForUserID(userInfo.EffectiveUser.ID.Value)));
                }
            }

            return results;
        }

        private ReviewUrlDTO AccountReviewSourceToDTO(Accounts_ReviewSources ars, TimeZoneInfo timeZone)
        {
            if (ars == null) return null;

            return new ReviewUrlDTO()
            {
                ID = ars.ID,
                AccountID = ars.AccountID,
                ExternalID = ars.ExternalID,
                ExternalID2 = ars.ExternalID2,
                HtmlURL = ars.URL,
                ApiURL = ars.URLAPI,
                isValid = ars.IsValid,
                LastDetailRun = (ars.LastDetailRun.HasValue) ? new SIDateTime(ars.LastDetailRun.Value, timeZone) : null,
                LastSummaryRun = (ars.LastSummaryRun.HasValue) ? new SIDateTime(ars.LastSummaryRun.Value, timeZone) : null,
                LastValidation = (ars.LastValidation.HasValue) ? new SIDateTime(ars.LastValidation.Value, timeZone) : null,
                LastDetailAttempted = (ars.LastDetailAttempted.HasValue) ? new SIDateTime(ars.LastDetailAttempted.Value, timeZone) : null,
                LastSummaryAttempted = (ars.LastSummeryAttempted.HasValue) ? new SIDateTime(ars.LastSummeryAttempted.Value, timeZone) : null,
                DateCreated = new SIDateTime(ars.DateCreated, timeZone),
                ReviewSource = (ReviewSourceEnum)ars.ReviewSourceID
            };
        }


        public long SaveNewRawReviewSummary(RawReviewSummaryDTO dto, long jobID)
        {
            try
            {


                using (ImportEntities db = ContextFactory.Import)
                {
                    string extraInfo = Newtonsoft.Json.JsonConvert.SerializeObject(dto.ExtraInfo);
                    RawReviewSummary summ = new RawReviewSummary()
                    {
                        AccountID = dto.AccountID,
                        ReviewSourceID = (long)dto.ReviewSource,
                        SalesAverageRating = Convert.ToDouble(dto.SalesAverageRating),
                        SalesReviewCount = dto.SalesReviewCount,
                        ServiceAverageRating = Convert.ToDouble(dto.ServiceAverageRating),
                        ServiceReviewCount = dto.ServiceReviewCount,
                        ExtraInfo = extraInfo,
                        DateEntered = DateTime.UtcNow
                    };

                    db.RawReviewSummaries.AddObject(summ);
                    db.SaveChanges();
                    return summ.ID;
                }

            }
            catch (Exception ex)
            {
                ProviderFactory.Logging.LogException(ex, jobID);
                throw new Exception(ex.ToString(), ex);
            }
        }

        public void SaveNewRawReviewSummaries(List<RawReviewSummaryDTO> dtos, long jobID)
        {
            try
            {

                using (ImportEntities db = ContextFactory.Import)
                {
                    foreach (RawReviewSummaryDTO item in dtos)
                    {
                        string extraInfo = Newtonsoft.Json.JsonConvert.SerializeObject(item.ExtraInfo);
                        RawReviewSummary summ = new RawReviewSummary()
                        {
                            AccountID = item.AccountID,
                            ReviewSourceID = (long)item.ReviewSource,
                            SalesAverageRating = Convert.ToDouble(item.SalesAverageRating),
                            SalesReviewCount = item.SalesReviewCount,
                            ServiceAverageRating = Convert.ToDouble(item.ServiceAverageRating),
                            ServiceReviewCount = item.ServiceReviewCount,
                            ExtraInfo = extraInfo,
                            DateEntered = DateTime.UtcNow
                        };
                        db.RawReviewSummaries.AddObject(summ);
                    }
                    db.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                ProviderFactory.Logging.LogException(ex, jobID);
                throw new Exception(ex.ToString(), ex);
            }
        }

        public List<long> SaveNewRawReviews(List<RawReviewDTO> list, long jobID)
        {
            try
            {


                //ITimezoneProvider tz = ProviderFactory.TimeZones;

                //timezone will always be for system user
                //TimeZoneInfo userTimeZone = tz.GetTimezoneForUserID(0);
                
                TimeZoneInfo userTimeZone = TimeZoneInfo.Utc;
                StringBuilder sb = new StringBuilder();
                 
                List<DateTime> dates = (from l in list orderby l.ReviewDate.GetUTCDate(userTimeZone) select l.ReviewDate.GetUTCDate(userTimeZone)).Distinct().ToList();

                sb.AppendLine(" set nocount on create table #t (id bigint not null) ");
                sb.AppendLine(" declare @idOut bigint ");

                DateTime minDateTime = new DateTime(1753, 1, 2);

                foreach (RawReviewDTO rr in list)
                {
                    string extraInfo = Newtonsoft.Json.JsonConvert.SerializeObject(rr.ExtraInfo);

                    DateTime reviewDate = rr.ReviewDate.GetUTCDate(userTimeZone);
                    if (reviewDate < minDateTime) reviewDate = minDateTime;

                    sb.AppendFormat(" insert #t (id) exec spCreateRawReview @AccountID={0}", rr.AccountID);
                    sb.AppendFormat(", @ReviewSourceID={0}", (long)rr.ReviewSource);
                    sb.AppendFormat(", @Rating={0}", rr.Rating.HasValue ? rr.Rating.Value.ToString(CultureInfo.InvariantCulture) : "null");
                    sb.AppendFormat(", @ReviewerName={0}", string.IsNullOrWhiteSpace(rr.ReviewerName) ? "null" : "'" + rr.ReviewerName.Replace("'", "''") + "'");
                    sb.AppendFormat(", @ReviewDate={0}", rr.ReviewDate != null ? "'" + reviewDate.ToString("MM/dd/yyyy hh:mm:ss tt") + "'" : "null");
                    sb.AppendFormat(", @ReviewText={0}", string.IsNullOrWhiteSpace(rr.ReviewText) ? "null" : "'" + rr.ReviewText.Replace("'", "''") + "'");
                    sb.AppendFormat(", @ReviewURL={0}", string.IsNullOrWhiteSpace(rr.ReviewURL) ? "null" : "'" + rr.ReviewURL.Replace("'", "''") + "'");
                    sb.AppendFormat(", @ExtraInfo={0}", string.IsNullOrWhiteSpace(extraInfo) ? "null" : "'" + extraInfo.Replace("'", "''") + "'");
                    sb.Append(", @IDOutput=@idOut OUTPUT \r\n");
                    //sb.Append("insert #t(id) values(@idOut) ");

                    foreach (RawReviewCommentDTO comm in rr.Comments)
                    {
                        sb.Append(" insert RawReviewComments(RawReviewID,Comment,CommentDate) ");
                        sb.AppendFormat(" values(@idOut,'{0}',{1}) /r/n", comm.Comment.Replace("'", "''"), comm.CommentDate != null ? "'" + comm.CommentDate.GetUTCDate(userTimeZone).ToString("MM/dd/yyyy hh:mm:ss tt") + "'" : "null");
                    }
                }

                sb.AppendLine(" select id from #t ");
                sb.AppendLine(" drop table #t ");

                string sql = sb.ToString();

                using (ImportEntities db = ContextFactory.Import)
                {
                    //string sql = sb.ToString();
                    List<long> idList = db.ExecuteStoreQuery<long>(sql).ToList();
                    return idList;
                }
            }
            catch (Exception ex)
            {
                ProviderFactory.Logging.LogException(ex, jobID);
                throw new Exception(ex.ToString(), ex);
            }
        }

        public void QueueReviewDownloaderJobs(
            UserInfoDTO userInfo, bool downloadDetails,
            int spreadJobsOutOverXMinutes,
            List<ReviewSourceEnum> reviewSources, long accountID,
            JobStatusEnum initialJobStatus, int priority
        )
        {
            try
            {
                ITimezoneProvider tz = ProviderFactory.TimeZones;
                TimeZoneInfo timeZone = tz.GetTimezoneForUserID(userInfo.EffectiveUser.ID.Value);

                DateTime firstJobScheduled = DateTime.UtcNow;
                DateTime lastJobScheduled = firstJobScheduled.AddMinutes(spreadJobsOutOverXMinutes);

                string reviewSourceIDs = "0";
                foreach (ReviewSourceEnum rs in reviewSources)
                {
                    reviewSourceIDs = reviewSourceIDs + "," + ((long)rs).ToString(CultureInfo.InvariantCulture);
                }
                using (MainEntities db = ContextFactory.Main)
                {
                    SqlParameter[] parameters = (new List<SqlParameter>()
                            {
                                new SqlParameter("@ReviewSourceIDs_param", reviewSourceIDs),
                                new SqlParameter("@AccountID_param",  accountID)
                            }
                    ).ToArray();

                    List<ReviewDownloaderWorklistItem> worklist = db.ExecuteStoreQuery<ReviewDownloaderWorklistItem>(
                        "exec spGetReviewDownloaderWorklist @ReviewSourceIDs=@ReviewSourceIDs_param, @AccountID=@AccountID_param", parameters).ToList();

                    Auditor
                        .New(AuditLevelEnum.Information, AuditTypeEnum.General, new UserInfoDTO(1, null),
                                "BLL:QueueReviewDownloadJobs found {0} worklist items...", worklist.Count())
                        .Save(ProviderFactory.Logging)
                    ;

                    if (worklist.Any())
                    {
                        DateTime scheduleDate = firstJobScheduled;
                        List<long> accountIDs = (from wl in worklist select wl.AccountID).Distinct().ToList();

                        Auditor
                           .New(AuditLevelEnum.Information, AuditTypeEnum.General, new UserInfoDTO(1, null),
                                   "BLL:QueueReviewDownloadJobs found {0} account ids...", accountIDs.Count())
                           .Save(ProviderFactory.Logging)
                        ;

                        double msBetweenJobs = 0;

                        TimeSpan ts = lastJobScheduled - firstJobScheduled;
                        msBetweenJobs = ts.TotalMilliseconds / Convert.ToDouble(accountIDs.Count());


                        Auditor
                           .New(AuditLevelEnum.Information, AuditTypeEnum.General, new UserInfoDTO(1, null),
                                   "BLL:QueueReviewDownloadJobs is scheduling {0} accounts {1} ms apart each", accountIDs.Count(), msBetweenJobs)
                           .Save(ProviderFactory.Logging)
                        ;

                        List<JobDTO> joblist = new List<JobDTO>();

                        DateTime jobDate = firstJobScheduled;

                        foreach (long aID in (from a in accountIDs orderby a descending select a))
                        {
                            jobDate = jobDate.AddMilliseconds(msBetweenJobs);

                            //JobDTO masterJob = null;
                            foreach (ReviewDownloaderWorklistItem item in (
                                from wl in worklist where wl.AccountID.Equals(aID) select wl
                                ).OrderBy(x => Guid.NewGuid())
                            )
                            {
                                ReviewDownloaderData data = new ReviewDownloaderData()
                                {
                                    Account_ReviewSourceID = item.Account_ReviewSourceID,
                                    DownloadDetails = downloadDetails,
                                    RemainingRetries = 1
                                };

                                JobDTO job = new JobDTO()
                                {
                                    DateScheduled = new SIDateTime(jobDate, TimeZoneInfo.Utc),
                                    JobStatus = initialJobStatus,
                                    JobDataObject = data,
                                    JobType = JobTypeEnum.ReviewDownloader,
                                    Priority = priority,
                                    ContinueChainIfFailed = false
                                };

                                joblist.Add(job);

                            }

                            if (joblist.Count() >= 1000)
                            {
                                ProviderFactory.Jobs.Save(new SaveEntityDTO<List<JobDTO>>(joblist, userInfo));
                                joblist.Clear();
                            }
                        }

                        if (joblist.Any())
                        {
                            ProviderFactory.Jobs.Save(new SaveEntityDTO<List<JobDTO>>(joblist, userInfo));
                            joblist.Clear();
                        }



                    }
                }
            }
            catch (Exception ex)
            {
                ProviderFactory.Logging.LogException(ex);
            }
        }

        public BatchReviewIntegratorPullDTO GetReviewsForAccountAndSource(long accountID, long reviewSourceID)
        {
            TimeZoneInfo timeZone = TimeZoneInfo.Utc;

            BatchReviewIntegratorPullDTO pull = new BatchReviewIntegratorPullDTO();

            using (MainEntities db = ContextFactory.Main)
            {
                if (db.Connection.State != System.Data.ConnectionState.Open)
                    db.Connection.Open();

                using (DbCommand cmd = db.CreateStoreCommand(
                    "spGetReviewDataForAccountAndSource2",
                    System.Data.CommandType.StoredProcedure,
                    new SqlParameter("AccountID", accountID),
                    new SqlParameter("ReviewSourceID", reviewSourceID)
                ))
                {
                    using (DbDataReader reader = cmd.ExecuteReader())
                    {

                        while (reader.Read())
                        {
                            pull.Summaries.Add(new ReviewSummaryDTO()
                            {
                                AccountID = Util.GetReaderValue<long>(reader, "AccountID", 0),
                                ReviewSourceID = Util.GetReaderValue<long>(reader, "ReviewSourceID", 0),
                                NewReviewsAverageRating = Util.GetReaderValue<double>(reader, "NewReviewsAverageRating", 0),
                                NewReviewsCount = Util.GetReaderValue<int>(reader, "NewReviewsCount", 0),
                                NewReviewsNegativeCount = Util.GetReaderValue<int>(reader, "NewReviewsNegativeCount", 0),
                                NewReviewsPositiveCount = Util.GetReaderValue<int>(reader, "NewReviewsPositiveCount", 0),
                                NewReviewsRatingSum = Util.GetReaderValue<int>(reader, "NewReviewsRatingSum", 0),
                                TotalNegativeReviews = Util.GetReaderValue<int>(reader, "TotalNegativeReviews", 0),
                                TotalPositiveReviews = Util.GetReaderValue<int>(reader, "TotalPositiveReviews", 0),
                                ReviewSummaryDataSource = (ReviewSummaryDataSourceEnum)Util.GetReaderValue<long>(reader, "ReviewSummaryDataSourceID", 0),
                                SalesAverageRating = Util.GetReaderValue<double>(reader, "SalesAverageRating", 0),
                                ServiceAverageRating = Util.GetReaderValue<double>(reader, "ServiceAverageRating", 0),
                                SalesReviewCount = Util.GetReaderValue<int>(reader, "SalesReviewCount", 0),
                                ServiceReviewCount = Util.GetReaderValue<int>(reader, "ServiceReviewCount", 0),
                                SummaryDate = new SIDateTime(Util.GetReaderValue<DateTime>(reader, "SummaryDate", DateTime.UtcNow), timeZone),

                                HasBefore = Util.GetReaderValue<bool>(reader, "HasBefore", true),
                                HasAfter = Util.GetReaderValue<bool>(reader, "HasAfter", true)
                            });
                        }

                        reader.NextResult();

                        while (reader.Read())
                        {
                            pull.Reviews.Add(reviewDTOFromReader(reader, timeZone));
                        }

                    }



                    return pull;
                }
            }

        }

        private ReviewDTO reviewDTOFromReader(DbDataReader reader, TimeZoneInfo timeZone)
        {
            return new ReviewDTO()
                {
                    ID = Util.GetReaderValue<long>(reader, "ID", 0),
                    AccountID = Util.GetReaderValue<long>(reader, "AccountID", 0),
                    ReviewSource = (ReviewSourceEnum)Util.GetReaderValue<long>(reader, "ReviewSourceID", 0),
                    Rating = Convert.ToDecimal(Util.GetReaderValue<double>(reader, "Rating", 0)),
                    ReviewerName = Util.GetReaderValue<string>(reader, "ReviewerName", ""),
                    ReviewDate = new SIDateTime(Util.GetReaderValue<DateTime>(reader, "ReviewDate", DateTime.UtcNow), timeZone),
                    ReviewText = Util.GetReaderValue<string>(reader, "ReviewText", ""),
                    ReviewURL = Util.GetReaderValue<string>(reader, "ReviewURL", ""),
                    Filtered = Util.GetReaderValue<bool>(reader, "Filtered", true),
                    DateUpdated = new SIDateTime(Util.GetReaderValue<DateTime>(reader, "DateUpdated", DateTime.UtcNow), timeZone),
                    DateEntered = new SIDateTime(Util.GetReaderValue<DateTime>(reader, "DateEntered", DateTime.UtcNow), timeZone)
                };
        }
        private ReviewDTO reviewDTOFromReview(Review review, TimeZoneInfo timeZone)
        {
            return new ReviewDTO()
            {
                ID = review.ID,
                AccountID = review.AccountID,
                ReviewSource = (ReviewSourceEnum)review.ReviewSourceID,
                Rating = Convert.ToDecimal(review.Rating),
                ReviewerName = review.ReviewerName,
                ReviewDate = new SIDateTime(review.ReviewDate.GetValueOrDefault(DateTime.UtcNow), timeZone),
                ReviewText = review.ReviewText,
                ReviewURL = review.ReviewURL,
                Filtered = review.Filtered,
                DateUpdated = new SIDateTime(review.DateUpdated, timeZone),
                DateEntered = new SIDateTime(review.DateEntered, timeZone)
            };
        }

        private class ReviewDownloaderWorklistItem
        {
            public long Account_ReviewSourceID { get; set; }
            public long ReviewSourceID { get; set; }
            public long AccountID { get; set; }
        }

        public RawReviewDataDTO GetRawReviewDataForAccountAndSource(long accountID, long reviewSourceID, long userID)
        {
            RawReviewDataDTO results = new RawReviewDataDTO();

            ITimezoneProvider tz = ProviderFactory.TimeZones;
            TimeZoneInfo timeZone = tz.GetTimezoneForUserID(userID);

            using (ImportEntities db = ContextFactory.Import)
            {
                if (db.Connection.State != System.Data.ConnectionState.Open)
                    db.Connection.Open();

                using (DbCommand cmd = db.CreateStoreCommand(
                    "spGetRawReviewData",
                     System.Data.CommandType.StoredProcedure,

                     new SqlParameter("@accountID", accountID),
                     new SqlParameter("@reviewSourceID", reviewSourceID)
                ))
                {
                    using (DbDataReader reader = cmd.ExecuteReader())
                    {
                        List<RawReview> rrlist = db.Translate<RawReview>(reader).ToList();

                        results.RawReviews = (
                            from r in rrlist
                            select new ReviewDTO()
                            {
                                ID = r.ID,
                                AccountID = r.AccountID,
                                ReviewSource = (ReviewSourceEnum)r.ReviewSourceID,
                                Rating = Convert.ToDecimal(r.Rating.GetValueOrDefault()),
                                ReviewerName = r.ReviewerName,
                                ReviewDate = (r.ReviewDate.HasValue) ? new SIDateTime(r.ReviewDate.Value, timeZone) : null,
                                ReviewText = r.ReviewText,
                                ReviewURL = r.ReviewURL,
                                Filtered = false,
                                DateEntered = new SIDateTime(r.DateEntered, timeZone),
                                DateUpdated = new SIDateTime(r.DateEntered, timeZone)
                            }
                        ).ToList();

                        if (reader.NextResult())
                        {
                            RawReviewSummary summary = db.Translate<RawReviewSummary>(reader).FirstOrDefault();
                            if (summary != null)
                            {
                                results.Summary = new RawReviewSummaryDTO()
                                {
                                    AccountID = summary.AccountID,
                                    ReviewSource = (ReviewSourceEnum)summary.ReviewSourceID,
                                    //ExtraInfo=summary.ExtraInfo,
                                    SalesAverageRating = Convert.ToDecimal(summary.SalesAverageRating.GetValueOrDefault(0)),
                                    SalesReviewCount = summary.SalesReviewCount,
                                    ServiceAverageRating = Convert.ToDecimal(summary.ServiceAverageRating.GetValueOrDefault(0)),
                                    ServiceReviewCount = summary.ServiceReviewCount
                                };
                            }

                        }
                    }
                }

            }

            return results;
        }

        public void ClearRawReviewsForAccountAndSource(long accountID, ReviewSourceEnum reviewSource)
        {
            StringBuilder sb = new StringBuilder();

            sb.AppendFormat(" delete RawReviewSummaries where AccountID={0} and ReviewSourceID={1} \r\n", accountID, (long)reviewSource);
            sb.AppendFormat(" delete rrc from RawReviewComments rrc inner join RawReviews rr on rr.ID=rrc.RawReviewID where rr.AccountID={0} and rr.ReviewSourceID={1} \r\n", accountID, (long)reviewSource);
            sb.AppendFormat(" delete RawReviews where AccountID={0} and ReviewSourceID={1} \r\n", accountID, (long)reviewSource);

            using (ImportEntities db = ContextFactory.Import)
            {
                db.ExecuteStoreCommand(sb.ToString(), null);
            }
        }

        public void ClearRawReviewsForAccountAndSource(List<ReviewSourceAndAccountPairDTO> accountSources)
        {
            StringBuilder sb = new StringBuilder();

            foreach (ReviewSourceAndAccountPairDTO item in accountSources)
            {
                sb.AppendFormat(" delete RawReviewSummaries where AccountID={0} and ReviewSourceID={1} \r\n", item.AccountID, item.ReviewSourceID);
                sb.AppendFormat(" delete rrc from RawReviewComments rrc inner join RawReviews rr on rr.ID=rrc.RawReviewID where rr.AccountID={0} and rr.ReviewSourceID={1} \r\n", item.AccountID, item.ReviewSourceID);
                sb.AppendFormat(" delete RawReviews where AccountID={0} and ReviewSourceID={1} \r\n", item.AccountID, item.ReviewSourceID);
            }

            using (ImportEntities db = ContextFactory.Import)
            {
                db.ExecuteStoreCommand(sb.ToString(), null);
            }
        }

        public void BatchReviewIntegratorUpdate(BatchReviewIntegratorUpdateDTO data)
        {
            StringBuilder sb = new StringBuilder();

            sb.AppendFormat(" set nocount on \r\n");
            sb.AppendFormat(" create table #t1 (id bigint not null, accountid bigint not null) \r\n");
            sb.AppendFormat(" create table #t2 (id bigint not null, accountid bigint not null) \r\n");

            if (data.ReviewIDsSetFiltered.Any())
                sb.AppendFormat(" update reviews set filtered=1 where ID IN ({0}) \r\n", string.Join<long>(",", data.ReviewIDsSetFiltered));
            if (data.ReviewIDsSetUnfiltered.Any())
                sb.AppendFormat(" update reviews set filtered=0 where ID IN ({0}) \r\n", string.Join<long>(",", data.ReviewIDsSetUnfiltered));

            if (data.ReviewsToSave.Any())
            {
                foreach (ReviewDTO item in data.ReviewsToSave)
                {
                    if (!item.ID.HasValue)
                    {
                        sb.AppendFormat(" insert #t1 ");
                    }
                    else
                    {
                        sb.AppendFormat(" insert #t2 ");
                    }
                    sb.AppendFormat(" exec spUpsertReview @ID={0}", item.ID.HasValue ? item.ID.Value.ToString(CultureInfo.InvariantCulture) : "null");
                    sb.AppendFormat(",@AccountID={0}", item.AccountID);
                    sb.AppendFormat(",@ReviewSourceID={0}", (long)item.ReviewSource);
                    sb.AppendFormat(",@Rating={0}", item.Rating);
                    sb.AppendFormat(",@ReviewerName={0}", string.IsNullOrWhiteSpace(item.ReviewerName) ? "null" : "'" + Util.SafeSQL(item.ReviewerName) + "'");
                    sb.AppendFormat(",@ReviewDate='{0}'", item.ReviewDate.ToString());
                    sb.AppendFormat(",@ReviewText={0}", string.IsNullOrWhiteSpace(item.ReviewText) ? "null" : "'" + Util.SafeSQL(item.ReviewText) + "'");
                    sb.AppendFormat(",@ReviewURL={0}", string.IsNullOrWhiteSpace(item.ReviewURL) ? "null" : "'" + Util.SafeSQL(item.ReviewURL) + "'");
                    sb.AppendFormat(",@ExtraInfo={0}", string.IsNullOrWhiteSpace(item.ExtraInfo) ? "null" : "'" + Util.SafeSQL(item.ExtraInfo) + "'");
                    sb.AppendFormat(",@Filtered={0} \r\n", item.Filtered ? "1" : "0");
                }

            }

            //update or insert the review summaries

            if (data.ReviewSummaries != null && data.ReviewSummaries.Any())
            {
                foreach (ReviewSummaryDTO item in data.ReviewSummaries)
                {
                    if (item.AccountID > 0 && item.ReviewSourceID > 0)
                    {
                        sb.AppendFormat(" exec spUpsertReviewSummary @reviewSummaryDataSourceID={0},", (long)item.ReviewSummaryDataSource);
                        sb.AppendFormat(" @accountID={0},", item.AccountID);
                        sb.AppendFormat(" @reviewSourceID={0},", item.ReviewSourceID);
                        sb.AppendFormat(" @salesAverageRating={0},", item.SalesAverageRating);
                        sb.AppendFormat(" @serviceAverageRating={0},", item.ServiceAverageRating);
                        sb.AppendFormat(" @salesReviewCount={0},", item.SalesReviewCount);
                        sb.AppendFormat(" @serviceReviewCount={0},", item.ServiceReviewCount);
                        sb.AppendFormat(" @extraInfo=null,");
                        sb.AppendFormat(" @newReviewsCount={0},", item.NewReviewsCount);
                        sb.AppendFormat(" @newReviewsAverageRating={0},", item.NewReviewsAverageRating);
                        sb.AppendFormat(" @newReviewsRatingSum={0},", item.NewReviewsRatingSum);
                        sb.AppendFormat(" @newReviewsPositiveCount={0},", item.NewReviewsPositiveCount);
                        sb.AppendFormat(" @newReviewsNegativeCount={0},", item.NewReviewsNegativeCount);
                        sb.AppendFormat(" @totalPositiveReviews={0},", item.TotalPositiveReviews);
                        sb.AppendFormat(" @totalNegativeReviews={0},", item.TotalNegativeReviews);
                        sb.AppendFormat(" @summaryDate='{0}' \r\n", item.SummaryDate.ToString());
                    }
                }
            }

            sb.AppendFormat(" select * from #t1 ");
            sb.AppendFormat(" drop table #t1 ");
            sb.AppendFormat(" drop table #t2 ");

            using (MainEntities db = ContextFactory.Main)
            {
                if (sb.Length > 0)
                {
                    string sql = sb.ToString();

                    try
                    {
                        if (db.Connection.State != System.Data.ConnectionState.Open)
                            db.Connection.Open();

                        UserInfoDTO sys_user = new UserInfoDTO(1, null);

                        using (DbCommand cmd = db.CreateStoreCommand(sql, System.Data.CommandType.Text, null))
                        {
                            using (DbDataReader reader = cmd.ExecuteReader())
                            {
                                while (reader.Read())
                                {
                                    long reviewID = Util.GetReaderValue<long>(reader, "id", 0);
                                    long accountID = Util.GetReaderValue<long>(reader, "accountid", 0);
                                    Auditor
                                        .New(AuditLevelEnum.Information, AuditTypeEnum.NewReviewDetected, sys_user, "New review ID [{0}] for account [{1}]", reviewID, accountID)
                                        .SetAuditDataObject(new AuditNewReviewDetected() { ReviewID = reviewID, AccountID = accountID })
                                        .SetReviewID(reviewID)
                                        .Save(ProviderFactory.Logging)
                                    ;
                                }
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        string message = ex.Message;
                        if (message.Length > 250)
                            message = message.Substring(0, 250);
                        Auditor
                            .New(AuditLevelEnum.Exception, AuditTypeEnum.SQLException, UserInfoDTO.System, message)
                            .SetAuditDataObject(new AuditSQLException()
                            {
                                Message = ex.ToString(),
                                SQL = sb.ToString()
                            })
                            .Save(ProviderFactory.Logging)
                        ;

                        throw new Exception(ex.ToString(), ex);
                    }
                }

            }

            //delete raw reviews if specified
            if (data.RawReviewIDsToDelete.Any())
            {
                sb = new StringBuilder();
                sb.AppendFormat(" delete RawReviewComments where RawReviewID IN ({0}) \r\n", string.Join<long>(",", data.RawReviewIDsToDelete));
                sb.AppendFormat(" delete RawReviews where ID IN ({0}) \r\n", string.Join<long>(",", data.RawReviewIDsToDelete));
                using (ImportEntities db = ContextFactory.Import)
                {
                    try
                    {
                        db.ExecuteStoreCommand(sb.ToString(), null);
                    }
                    catch (Exception ex)
                    {
                        ProviderFactory.Logging.Audit(AuditLevelEnum.Exception, AuditTypeEnum.SQLException, ex.Message, new AuditSQLException() { Message = ex.ToString(), SQL = sb.ToString() });
                        throw new Exception(ex.ToString(), ex);
                    }
                }
            }
        }

        public ReviewStreamSearchResultDTO GetReviewStreamData(UserInfoDTO userInfo, ReviewStreamSearchRequestDTO request)
        {
            ReviewStreamSearchResultDTO result = new ReviewStreamSearchResultDTO()
            {
                StartIndex = request.StartIndex,
                EndIndex = request.EndIndex,
                SortBy = (int)request.SortBy,
                SortAscending = request.SortAscending
            };

            ITimezoneProvider tz = ProviderFactory.TimeZones;
            TimeZoneInfo timeZone = tz.GetTimezoneForUserID(userInfo.EffectiveUser.ID.Value);

            List<StreamDTO> streams = new List<StreamDTO>();

            List<long> accountIDs = null;

            if (request.AccountIDs != null && request.AccountIDs.Any())
            {
                accountIDs = AccountListBuilder
                    .New(new SecurityProvider())
                    .AddAccountIDs(request.AccountIDs)
                    .ExpandToDescendants()
                    .FilterToAccountsWithPermission(userInfo, PermissionTypeEnum.reviews_view)
                    .GetIDs(SecurityProvider.PLATFORM_MAX_ACCOUNT_ID_LIST)
                ;
            }
            else
            {
                accountIDs = AccountListBuilder
                    .New(new SecurityProvider())
                    .AddAccountsWithPermission(userInfo, PermissionTypeEnum.reviews_view)
                    .GetIDs(SecurityProvider.PLATFORM_MAX_ACCOUNT_ID_LIST)
                ;
            }


            using (MainEntities db = ContextFactory.Main)
            {
                if (db.Connection.State != System.Data.ConnectionState.Open)
                    db.Connection.Open();

                using (DbCommand cmd = db.CreateStoreCommand(
                    "spGetReviewStreamData2",
                    System.Data.CommandType.StoredProcedure,

                    //lists
                    new SqlParameter("AccountIDs", string.Join("|", accountIDs)),
                    new SqlParameter("StateIDs", string.Join("|", request.StateIDs)),
                    new SqlParameter("FranchiseTypeIDs", string.Join("|", request.FranchiseTypeIDs)),
                    new SqlParameter("ReviewSourceIDs", string.Join("|", request.ReviewSourcesIDs)),

                    //pagination and sort
                    new SqlParameter("StartIndex", request.StartIndex),
                    new SqlParameter("EndIndex", request.EndIndex),
                    new SqlParameter("SortBy", (int)request.SortBy),
                    new SqlParameter("SortAscending", request.SortAscending),

                    //filtered?
                    new SqlParameter("IncludeFiltered", request.IncludeFiltered),
                    new SqlParameter("IncludeUnFiltered", request.IncludeUnFiltered),

                    new SqlParameter("FilterRead", request.FilterRead),
                    new SqlParameter("FilterUnRead", request.FilterUnRead),
                    new SqlParameter("FilterResponded", request.FilterResponded),
                    new SqlParameter("FilterUnResponded", request.FilterUnResponded),
                    new SqlParameter("FilterIgnored", request.FilterIgnored),

                    //dates?
                    new SqlParameter("MinReviewDate", request.MinDate),
                    new SqlParameter("MaxReviewDate", request.MaxDate),

                    //text searches
                    new SqlParameter("ReviewTextContains", request.ReviewTextContains),
                    new SqlParameter("ReviewTextDoesNotContain", request.ReviewTextDoesNotContain),

                    //rating ranges
                    new SqlParameter("RatingIsGreaterThanOrEqualTo", request.RatingIsGreaterThanOrEqualTo),
                    new SqlParameter("RatingIsLessThanOrEqualTo", request.RatingIsLessThanOrEqualTo),

                    //other
                    new SqlParameter("CategoryID", request.CategoryID)
                ))
                {
                    using (DbDataReader reader = cmd.ExecuteReader())
                    {
                        reader.Read();
                        result.TotalRecords = Util.GetReaderValue<int>(reader, "Total", 0);

                        reader.NextResult();

                        while (reader.Read())
                        {
                            result.Items.Add(new StreamDTO()
                            {
                                ReviewID = Util.GetReaderValue<long>(reader, "ReviewID", 0),
                                DealerLocation = new StreamDTO.Location()
                                {
                                    Name = Util.GetReaderValue<string>(reader, "AccountName", ""),
                                    LocationID = Util.GetReaderValue<long>(reader, "AccountID", 0),
                                    City = Util.GetReaderValue<string>(reader, "City", ""),
                                    StateID = Util.GetReaderValue<long>(reader, "StateID", 0),
                                    SiteUrl = Util.GetReaderValue<string>(reader, "SiteURL", ""),
                                    StateAbrev = Util.GetReaderValue<string>(reader, "StateAbbrev", ""),
                                    Zipcode = Util.GetReaderValue<string>(reader, "ZipCode", "")
                                },
                                SiteID = Util.GetReaderValue<long>(reader, "ReviewSourceID", 0),
                                SiteName = Util.GetReaderValue<string>(reader, "ReviewSourceName", ""),
                                SiteImageUrl = Util.GetReaderValue<string>(reader, "ImagePath", ""),
                                Category = "",
                                CollectedDate = new SIDateTime(Util.GetReaderValue<DateTime>(reader, "DateEntered", DateTime.UtcNow), timeZone),
                                ReviewDate = new SIDateTime(Util.GetReaderValue<DateTime>(reader, "ReviewDate", DateTime.UtcNow), timeZone),
                                IsFiltered = Util.GetReaderValue<bool>(reader, "Filtered", false),
                                Rating = (double)Util.GetReaderValue<decimal>(reader, "Rating", 0),
                                ReviewerName = Util.GetReaderValue<string>(reader, "ReviewerName", ""),
                                ReviewText = Util.GetReaderValue<string>(reader, "ReviewText", ""),
                                ReviewUrl = Util.GetReaderValue<string>(reader, "ReviewURL", ""),

                                ReadDate = new SIDateTime(Util.GetReaderValue<DateTime>(reader, "ReadDate", DateTime.MinValue), timeZone),
                                RespondedDate = new SIDateTime(Util.GetReaderValue<DateTime>(reader, "RespondedDate", DateTime.MinValue), timeZone),
                                IgnoredDate = new SIDateTime(Util.GetReaderValue<DateTime>(reader, "IgnoredDate", DateTime.MinValue), timeZone)
                            });
                        }
                    }
                }

            }
            return result;
        }

        public ReviewStreamCountPerSiteResultDTO GetReviewCountPerSite(UserInfoDTO userInfo, ReviewStreamCountPerSiteRequestDTO request)
        {
            ReviewStreamCountPerSiteResultDTO result = new ReviewStreamCountPerSiteResultDTO();

            List<long> accountIDs = null;

            if (request.AccountIDs != null && request.AccountIDs.Any())
            {
                accountIDs = AccountListBuilder
                    .New(new SecurityProvider())
                    .AddAccountIDs(request.AccountIDs)
                    .ExpandToDescendants()
                    .FilterToAccountsWithPermission(userInfo, PermissionTypeEnum.reviews_view)
                    .GetIDs(SecurityProvider.PLATFORM_MAX_ACCOUNT_ID_LIST)
                ;
            }
            else
            {
                accountIDs = AccountListBuilder
                    .New(new SecurityProvider())
                    .AddAccountsWithPermission(userInfo, PermissionTypeEnum.reviews_view)
                    .GetIDs(SecurityProvider.PLATFORM_MAX_ACCOUNT_ID_LIST)
                ;
            }

            using (MainEntities db = ContextFactory.Main)
            {
                result.results = db.CreateStoreCommand(
                        "spGetReviewCountPerSite",
                        System.Data.CommandType.StoredProcedure,

                        //lists
                        new SqlParameter("AccountIDs", string.Join("|", accountIDs)),
                        new SqlParameter("StateIDs", string.Join("|", request.StateIDs)),
                        new SqlParameter("FranchiseTypeIDs", string.Join("|", request.FranchiseTypeIDs)),
                        new SqlParameter("ReviewSourceIDs", string.Join("|", request.ReviewSourcesIDs)),

                        //filtered?
                        new SqlParameter("IncludeFiltered", request.IncludeFiltered),
                        new SqlParameter("IncludeUnFiltered", request.IncludeUnFiltered),

                        new SqlParameter("FilterRead", request.FilterRead),
                        new SqlParameter("FilterUnRead", request.FilterUnRead),
                        new SqlParameter("FilterResponded", request.FilterResponded),
                        new SqlParameter("FilterUnResponded", request.FilterUnResponded),
                        new SqlParameter("FilterIgnored", request.FilterIgnored),

                        //dates?
                        new SqlParameter("MinReviewDate", request.MinDate),
                        new SqlParameter("MaxReviewDate", request.MaxDate),

                        //text searches
                        new SqlParameter("ReviewTextContains", request.ReviewTextContains),
                        new SqlParameter("ReviewTextDoesNotContain", request.ReviewTextDoesNotContain),

                        //rating ranges
                        new SqlParameter("RatingIsGreaterThanOrEqualTo", request.RatingIsGreaterThanOrEqualTo),
                        new SqlParameter("RatingIsLessThanOrEqualTo", request.RatingIsLessThanOrEqualTo),

                        //other
                        new SqlParameter("CategoryID", request.CategoryID)
                    ).Materialize<ReviewCountPerSiteResultsDTO>().ToList();
            }

            return result;
        }

        public bool UpdateReviewByID(UserInfoDTO userInfo, long ReviewID, string menuaction)
        {
            bool result = false;

            if (ReviewID != 0)
            {
                ProviderFactory.Logging.Audit(AuditLevelEnum.Information, AuditTypeEnum.ReviewStreamAction, menuaction, null, userInfo, null, null, null, ReviewID);

                DateTime date = DateTime.UtcNow;

                using (MainEntities db = ContextFactory.Main)
                {
                    Review review = db.Reviews.Where(r => r.ID == ReviewID).SingleOrDefault();

                    if (review != null)
                    {
                        switch (menuaction)
                        {
                            case "markasread":
                                if (review.ReadDate.HasValue)
                                    review.ReadDate = null;
                                else
                                    review.ReadDate = date;
                                break;
                            case "responded":
                                if (review.RespondedDate.HasValue)
                                    review.RespondedDate = null;
                                else
                                {
                                    review.RespondedDate = date;
                                    review.ReadDate = date;
                                    review.IgnoredDate = null;
                                }
                                break;
                            case "ignore":
                                if (review.IgnoredDate.HasValue)
                                    review.IgnoredDate = null;
                                else
                                {
                                    review.IgnoredDate = date;
                                    review.ReadDate = date;
                                    review.RespondedDate = null;
                                }
                                break;
                        }

                        return db.SaveChanges() > 0;
                    }
                }
            }
            return result;
        }


        public DashboardOverviewDTO GetDashboardOverviewData(UserInfoDTO userInfo)
        {
            ITimezoneProvider tz = ProviderFactory.TimeZones;
            TimeZoneInfo timeZone = tz.GetTimezoneForUserID(userInfo.EffectiveUser.ID.Value);

            List<long> accountIDs =
                AccountListBuilder
                    .New(new SecurityProvider())
                    .AddAccountID(userInfo.EffectiveUser.CurrentContextAccountID)
                    .ExpandToDescendants()
                    .FilterToAccountsWithPermission(userInfo, PermissionTypeEnum.accounts_view)
                    .GetIDs(SecurityProvider.PLATFORM_MAX_ACCOUNT_ID_LIST)
            ;

            DashboardOverviewDTO res = new DashboardOverviewDTO();

            using (MainEntities db = ContextFactory.Main)
            {
                if (db.Connection.State != System.Data.ConnectionState.Open)
                    db.Connection.Open();

                DbCommand cmd = null;
                List<SqlParameter> parameters = new List<SqlParameter>();

                using (cmd = db.CreateStoreCommand(
                    "spGetDashboardData3",
                    System.Data.CommandType.StoredProcedure,
                    new SqlParameter("AccountIDs", string.Join("|", accountIDs.ToArray()))
                ))
                {
                    using (DbDataReader reader = cmd.ExecuteReader())
                    {
                        if (reader.Read())
                        {
                            res.ReputationOverview.ReviewsAllTime = new ReviewCountsDTO()
                            {
                                Total = Util.GetReaderValue<int>(reader, "Total", 0),
                                TotalNegative = Util.GetReaderValue<int>(reader, "NegativeReviews", 0),
                                TotalPositive = Util.GetReaderValue<int>(reader, "PositiveReviews", 0),
                                TotalFiltered = 0,
                                TotalNegativeFiltered = 0,
                                TotalPositiveFiltered = 0
                            };
                            if (reader.NextResult() && reader.Read())
                            {
                                res.ReputationOverview.ReviewsLast30 = new ReviewCountsDTO()
                                {
                                    Total = Util.GetReaderValue<int>(reader, "Total", 0),
                                    TotalNegative = Util.GetReaderValue<int>(reader, "NegativeReviews", 0),
                                    TotalPositive = Util.GetReaderValue<int>(reader, "PositiveReviews", 0),
                                    TotalFiltered = 0,
                                    TotalNegativeFiltered = 0,
                                    TotalPositiveFiltered = 0
                                };

                                if (reader.NextResult() && reader.Read())
                                {
                                    res.ReputationOverview.ReviewsLast7 = new ReviewCountsDTO()
                                    {
                                        Total = Util.GetReaderValue<int>(reader, "Total", 0),
                                        TotalNegative = Util.GetReaderValue<int>(reader, "NegativeReviews", 0),
                                        TotalPositive = Util.GetReaderValue<int>(reader, "PositiveReviews", 0),
                                        TotalFiltered = 0,
                                        TotalNegativeFiltered = 0,
                                        TotalPositiveFiltered = 0
                                    };

                                    if (reader.NextResult())
                                    {
                                        res.ReputationOverview.ReviewCountsByAccountID = new Dictionary<long, ReviewCountsDTO>();
                                        while (reader.Read())
                                        {
                                            ReviewCountsDTO rcd = new ReviewCountsDTO()
                                            {
                                                Total = Util.GetReaderValue<int>(reader, "TotalReviews", 0),
                                                TotalNegative = Util.GetReaderValue<int>(reader, "NegativeReviews", 0),
                                                TotalPositive = Util.GetReaderValue<int>(reader, "PositiveReviews", 0),
                                                AverageRating = Util.GetReaderValue<double>(reader, "AvgRating", 0),
                                                TotalFiltered = 0,
                                                TotalNegativeFiltered = 0,
                                                TotalPositiveFiltered = 0
                                            };
                                            DateTime? ldate = Util.GetReaderValue<DateTime?>(reader, "LastReviewDate", null);
                                            if (ldate.HasValue) rcd.LastReviewDate = new SIDateTime(ldate.Value, timeZone);

                                            res.ReputationOverview.ReviewCountsByAccountID.Add(Util.GetReaderValue<long>(reader, "AccountID", 0), rcd);
                                        }

                                        if (reader.NextResult())
                                        {
                                            res.ReputationOverview.ReviewCountsBySource = new Dictionary<ReviewSourceEnum, ReviewCountsDTO>();
                                            while (reader.Read())
                                            {
                                                ReviewCountsDTO rcd = new ReviewCountsDTO()
                                                {
                                                    Total = Util.GetReaderValue<int>(reader, "TotalReviews", 0),
                                                    TotalNegative = Util.GetReaderValue<int>(reader, "NegativeReviews", 0),
                                                    TotalPositive = Util.GetReaderValue<int>(reader, "PositiveReviews", 0),
                                                    AverageRating = Util.GetReaderValue<double>(reader, "AvgRating", 0),
                                                    TotalFiltered = 0,
                                                    TotalNegativeFiltered = 0,
                                                    TotalPositiveFiltered = 0
                                                };
                                                DateTime? ldate = Util.GetReaderValue<DateTime?>(reader, "LastReviewDate", null);
                                                if (ldate.HasValue) rcd.LastReviewDate = new SIDateTime(ldate.Value, timeZone);

                                                res.ReputationOverview.ReviewCountsBySource.Add(
                                                    (ReviewSourceEnum)Util.GetReaderValue<long>(reader, "ReviewSourceID", 0), rcd);
                                            }

                                            if (reader.NextResult())
                                            {
                                                List<SimpleAccountInfoDTO> addresses = db.Translate<SimpleAccountInfoDTO>(reader).ToList();
                                                foreach (SimpleAccountInfoDTO address in addresses)
                                                {
                                                    res.ReputationOverview.AccountList.Add(address.ID, address);
                                                }
                                                res.ReputationOverview.NumberOfDealers = (from a in addresses where a.AccountTypeID == 1 select a).Count();

                                                if (reader.NextResult() && reader.Read()) // Posts
                                                {
                                                    //res.SocialOverview.FBPosts = Util.GetReaderValue<int>(reader, "FBPosts", 0);

                                                    if (reader.NextResult() && reader.Read()) // Comments
                                                    {
                                                        //res.SocialOverview.FBComments = Util.GetReaderValue<int>(reader, "FBComments", 0);

                                                        if (reader.NextResult()) // Likes by Month
                                                        {
                                                            while (reader.Read())
                                                            {
                                                                res.SocialOverview.FBLikes.Add(new SocialMonthlyLikesDTO()
                                                                {
                                                                    Likes = Util.GetReaderValue<int>(reader, "FBLikes", 0),
                                                                    MonthDate = Util.GetReaderValue<DateTime>(reader, "MonthDate", DateTime.UtcNow)
                                                                });
                                                            }
                                                            if (reader.NextResult()) // Social Location Data
                                                            {
                                                                while (reader.Read())
                                                                {
                                                                    long accountIDSocial = Util.GetReaderValue<long>(reader, "AccountID", 0);
                                                                    CombinedSocialInfoDTO sinfo = new CombinedSocialInfoDTO()
                                                                    {
                                                                        PageLikes = Util.GetReaderValue<int>(reader, "PageLikes", 0),
                                                                        PageWereHere = Util.GetReaderValue<int>(reader, "PageWereHere", 0),
                                                                        PageTalkingAbout = Util.GetReaderValue<int>(reader, "PageLikes", 0)
                                                                    };
                                                                    sinfo.InternalPostMetrics = new SocialMetricsDTO()
                                                                    {
                                                                        Posts = Util.GetReaderValue<int>(reader, "InternalPosts", 0),
                                                                        PostLikes = Util.GetReaderValue<int>(reader, "InternalPostLikes", 0),
                                                                        Shares = Util.GetReaderValue<int>(reader, "InternalShares", 0),
                                                                        Comments = Util.GetReaderValue<int>(reader, "InternalComments", 0)
                                                                    };
                                                                    sinfo.ExternalPostMetrics = new SocialMetricsDTO()
                                                                    {
                                                                        Posts = Util.GetReaderValue<int>(reader, "ExternalPosts", 0),
                                                                        PostLikes = Util.GetReaderValue<int>(reader, "ExternalPostLikes", 0),
                                                                        Shares = Util.GetReaderValue<int>(reader, "ExternalShares", 0),
                                                                        Comments = Util.GetReaderValue<int>(reader, "ExternalComments", 0)
                                                                    };
                                                                    sinfo.PostMetrics = new SocialMetricsDTO()
                                                                    {
                                                                        Posts = sinfo.InternalPostMetrics.Posts + sinfo.ExternalPostMetrics.Posts,
                                                                        PostLikes = sinfo.InternalPostMetrics.PostLikes + sinfo.ExternalPostMetrics.PostLikes,
                                                                        Shares = sinfo.InternalPostMetrics.Shares + sinfo.ExternalPostMetrics.Shares,
                                                                        Comments = sinfo.InternalPostMetrics.Comments + sinfo.ExternalPostMetrics.Comments
                                                                    };
                                                                    if (!res.SocialOverview.SocialInfoByAccount.ContainsKey(accountIDSocial))
                                                                        res.SocialOverview.SocialInfoByAccount.Add(accountIDSocial, sinfo);
                                                                }
                                                                if (reader.NextResult()) // Connected Networks
                                                                {
                                                                    while (reader.Read())
                                                                    {
                                                                        long accountIDSocial = Util.GetReaderValue<long>(reader, "AccountID", 0);
                                                                        CombinedSocialInfoDTO sinfo = (from s in res.SocialOverview.SocialInfoByAccount where s.Key == accountIDSocial select s.Value).FirstOrDefault();
                                                                        if (sinfo != null)
                                                                        {
                                                                            sinfo.ConnectedNetworks.Add((SocialNetworkEnum)Util.GetReaderValue<long>(reader, "SocialNetworkID", 0));
                                                                        }
                                                                    }
                                                                    if (reader.NextResult()) // Last Internal Post Dates
                                                                    {
                                                                        while (reader.Read())
                                                                        {
                                                                            long accountIDSocial = Util.GetReaderValue<long>(reader, "AccountID", 0);
                                                                            CombinedSocialInfoDTO sinfo = (from s in res.SocialOverview.SocialInfoByAccount where s.Key == accountIDSocial select s.Value).FirstOrDefault();
                                                                            if (sinfo != null)
                                                                            {
                                                                                DateTime lastPostDate = Util.GetReaderValue<DateTime>(reader, "LastInternalPostDate", DateTime.UtcNow);
                                                                                sinfo.InternalPostMetrics.LastPostDate = new SIDateTime(lastPostDate, timeZone);
                                                                            }
                                                                        }

                                                                        if (reader.NextResult()) // Last External Post Dates
                                                                        {
                                                                            while (reader.Read())
                                                                            {
                                                                                long accountIDSocial = Util.GetReaderValue<long>(reader, "AccountID", 0);
                                                                                CombinedSocialInfoDTO sinfo = (from s in res.SocialOverview.SocialInfoByAccount where s.Key == accountIDSocial select s.Value).FirstOrDefault();
                                                                                if (sinfo != null)
                                                                                {
                                                                                    DateTime lastPostDate = Util.GetReaderValue<DateTime>(reader, "LastExternalPostDate", DateTime.UtcNow);
                                                                                    sinfo.ExternalPostMetrics.LastPostDate = new SIDateTime(lastPostDate, timeZone);
                                                                                }
                                                                            }

                                                                            foreach (CombinedSocialInfoDTO sinfo in res.SocialOverview.SocialInfoByAccount.Values)
                                                                            {
                                                                                if (sinfo.ExternalPostMetrics.LastPostDate != null)
                                                                                {
                                                                                    sinfo.PostMetrics.LastPostDate = sinfo.ExternalPostMetrics.LastPostDate;
                                                                                }
                                                                                if (sinfo.InternalPostMetrics.LastPostDate != null)
                                                                                {
                                                                                    if (sinfo.PostMetrics.LastPostDate == null)
                                                                                    {
                                                                                        sinfo.PostMetrics.LastPostDate = sinfo.InternalPostMetrics.LastPostDate;
                                                                                    }
                                                                                    else if (sinfo.InternalPostMetrics.LastPostDate.CompareTo(sinfo.PostMetrics.LastPostDate) > 0)
                                                                                    {
                                                                                        sinfo.PostMetrics.LastPostDate = sinfo.InternalPostMetrics.LastPostDate;
                                                                                    }
                                                                                }
                                                                            }

                                                                            if (reader.NextResult() && reader.Read()) // GooglePlus 3 month snapshot
                                                                            {
                                                                                res.SocialOverview.GooglePlusPosts = Util.GetReaderValue<int>(reader, "GooglePlusPosts", 0);
                                                                                res.SocialOverview.GooglePlusPostComments = Util.GetReaderValue<int>(reader, "GooglePlusPostComments", 0);
                                                                                res.SocialOverview.GooglePlusPostPlusoners = Util.GetReaderValue<int>(reader, "GooglePlusPostPlusoners", 0);
                                                                                res.SocialOverview.GooglePlusPostResharers = Util.GetReaderValue<int>(reader, "GooglePlusPostResharers", 0);

                                                                                if (reader.NextResult() && reader.Read()) // Facebook 3 month snapshot
                                                                                {
                                                                                    res.SocialOverview.FBPosts = Util.GetReaderValue<int>(reader, "FBPosts", 0);
                                                                                    res.SocialOverview.FBPostComments = Util.GetReaderValue<int>(reader, "FBPostComments", 0);
                                                                                    res.SocialOverview.FBPostLikes = Util.GetReaderValue<int>(reader, "FBPostLikes", 0);
                                                                                    res.SocialOverview.FBPostShares = Util.GetReaderValue<int>(reader, "FBPostShares", 0);

                                                                                    if (reader.NextResult() && reader.Read()) // Twitter 3 month snapshot
                                                                                    {
                                                                                        res.SocialOverview.TwitterPosts = Util.GetReaderValue<int>(reader, "TwitterPosts", 0);
                                                                                        res.SocialOverview.TwitterRetweetCount = Util.GetReaderValue<int>(reader, "TwitterRetweetCount", 0);

                                                                                        if (reader.NextResult() && reader.Read()) // Total Audience for Facebook, google+ and Twitter (Alltime)
                                                                                        {

                                                                                            res.SocialOverview.FacebookPageLikes_AllTime = Util.GetReaderValue<int>(reader, "FacebookPageLikes_AllTime", 0);
                                                                                            res.SocialOverview.GooglePlusPlusOneCount_AllTime = Util.GetReaderValue<int>(reader, "GooglePlusPlusOneCount_AllTime", 0);
                                                                                            res.SocialOverview.TwitterFollowersCount_AllTime = Util.GetReaderValue<int>(reader, "TwitterFollowersCount_AllTime", 0);

                                                                                            if (reader.NextResult() && reader.Read()) // Total Audience for Facebook, google+ and Twitter (30 days)
                                                                                            {

                                                                                                res.SocialOverview.FacebookPageLikes_30days = Util.GetReaderValue<int>(reader, "FacebookPageLikes_30days", 0);
                                                                                                res.SocialOverview.GooglePlusPlusOneCount_30days = Util.GetReaderValue<int>(reader, "GooglePlusPlusOneCount_30days", 0);
                                                                                                res.SocialOverview.TwitterFollowersCount_30days = Util.GetReaderValue<int>(reader, "TwitterFollowersCount_30days", 0);

                                                                                                if (reader.NextResult() && reader.Read()) // Total Audience for Facebook, google+ and Twitter (7 days)
                                                                                                {

                                                                                                    res.SocialOverview.FacebookPageLikes_7days = Util.GetReaderValue<int>(reader, "FacebookPageLikes_7days", 0);
                                                                                                    res.SocialOverview.GooglePlusPlusOneCount_7days = Util.GetReaderValue<int>(reader, "GooglePlusPlusOneCount_7days", 0);
                                                                                                    res.SocialOverview.TwitterFollowersCount_7days = Util.GetReaderValue<int>(reader, "TwitterFollowersCount_7days", 0);

                                                                                                    if (reader.NextResult())
                                                                                                    {
                                                                                                        while (reader.Read())
                                                                                                        {
                                                                                                            res.ReputationOverview.MonthlyMetrics.Add(new ReputationMonthMetrics()
                                                                                                            {
                                                                                                                AvgRating = Util.GetReaderValue<double>(reader, "AvgRating", 0),
                                                                                                                MonthDate = Util.GetReaderValue<DateTime>(reader, "StartDate", DateTime.UtcNow),
                                                                                                                ReviewSource = (ReviewSourceEnum)Util.GetReaderValue<long>(reader, "ReviewSourceID", 0)
                                                                                                            });
                                                                                                        }
                                                                                                        reader.NextResult();
                                                                                                        if (reader.NextResult() && reader.Read()) // SPN-613 Reputaton Dashboard & Reports - Convert Average of Average to Average of Detail
                                                                                                        {
                                                                                                            res.ReputationOverview.AllNonZeroRatings = Util.GetReaderValue<double>(reader, "AllNonZeroRatings", 0);
                                                                                                            res.ReputationOverview.ReviewDetailCount = Util.GetReaderValue<int>(reader, "ReviewDetailCount", 0);
                                                                                                        }// SPN-613 Reputaton Dashboard & Reports - Convert Average of Average to Average of Detail
                                                                                                    } // Monthly reputation metrics by review source (12 months max)
                                                                                                } // Total Audience for Facebook, google+ and Twitter (7 days)
                                                                                            } // Total Audience for Facebook, google+ and Twitter (30 days)                                                                                      
                                                                                        }// Total Audience for Facebook, google+ and Twitter (Alltime)
                                                                                    }// Twitter 3 month snapshot
                                                                                }// Facebook 3 month snapshot
                                                                            }// GooglePlus 3 month snapshot                                                                           
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }


                    }
                }

            }
            return res;
        }

        public CompAnalysisDataDTO GetCompetitiveAnalysisData(UserInfoDTO userInfo)
        {
            CompAnalysisDataDTO dto = new CompAnalysisDataDTO();

            using (MainEntities db = ContextFactory.Main)
            {
                if (db.Connection.State != System.Data.ConnectionState.Open)
                    db.Connection.Open();

                // get the account lists
                #region get the account lists

                long accountID = userInfo.EffectiveUser.CurrentContextAccountID;
                SecurityProvider sec = ((SecurityProvider)ProviderFactory.Security);

                AccountTree tree = sec.getAccountTree()[accountID];


                List<long> mainIDs = new List<long>();
                List<long> groupIDs = new List<long>();
                List<long> compIDs = new List<long>();

                List<AccountCompetitorListItem> competitors = null;

                bool singleDealerGroup = false;
                bool contextIsDealer = false;
                bool contextIsGroup = false;

                switch (tree.AccountType)
                {
                    case AccountTypeEnum.Grouping:

                        singleDealerGroup = (tree.Children.Count() <= 1);
                        contextIsDealer = false;
                        break;

                    case AccountTypeEnum.Company:
                        singleDealerGroup = !(tree.Parent.Depth > 1 && tree.Parent.Children.Count() > 1);
                        contextIsDealer = true;
                        break;
                }

                if (singleDealerGroup)
                {
                    if (contextIsDealer)
                    {
                        mainIDs.Add(tree.ID);
                        groupIDs.AddRange(mainIDs);
                        competitors = sec.GetAccountCompetitorIDs(mainIDs);
                    }
                    else //context is group
                    {
                        mainIDs.Add(tree.ID);
                        if (tree.Children.Any())
                            mainIDs.Add(tree.Children[0].ID);
                        groupIDs.AddRange(mainIDs);
                        competitors = sec.GetAccountCompetitorIDs(mainIDs);
                    }
                }
                else //multi-dealer group
                {
                    if (contextIsDealer)
                    {
                        mainIDs.Add(tree.ID);
                        groupIDs.AddRange(sec.GetAccountDescendants(tree.Parent.ID));
                        competitors = sec.GetAccountCompetitorIDs(mainIDs);
                    }
                    else //context is group
                    {
                        contextIsGroup = true;
                        mainIDs.Add(tree.ID);
                        mainIDs.AddRange(sec.GetAccountDescendants(tree.ID));
                        groupIDs.AddRange(mainIDs);
                        competitors = sec.GetAccountCompetitorIDs(groupIDs);
                    }
                }

                compIDs = (from c in competitors select c.CompetitorAccountID).Distinct().ToList();

                #endregion

                using (DbCommand cmd = db.CreateStoreCommand("spGetCompetitiveData4", System.Data.CommandType.StoredProcedure,
                    new SqlParameter("AccountIDsMain", string.Join("|", mainIDs)),
                    new SqlParameter("AccountIDsGroup", string.Join("|", groupIDs)),
                    new SqlParameter("AccountIDsComp", string.Join("|", compIDs))
                ))
                {
                    cmd.CommandTimeout = 30000;

                    using (DbDataReader reader = cmd.ExecuteReader())
                    {
                        //master aggregates Satisfaction Scores
                        SatisfactionScores satisfactionScores = db.Translate<SatisfactionScores>(reader).FirstOrDefault();

                        reader.NextResult();

                        // AccountCompetitorGroupGrid 2ndGrid
                        List<AccountCompetitorGroupGrid> accountCompetitorGroupGrid = db.Translate<AccountCompetitorGroupGrid>(reader).ToList();

                        reader.NextResult();

                        // AccountsGrid 3rd Grid
                        List<AccountsGrid> accountsGrid = db.Translate<AccountsGrid>(reader).ToList();

                        #region SatisfactionScores

                        dto.MyScore = satisfactionScores.MyScore.GetValueOrDefault();
                        dto.CompetitorScore = satisfactionScores.CompetitorScore.GetValueOrDefault();

                        #endregion

                        #region 2nd Grid

                        #region My Location(s)

                        var Main = (from i in accountCompetitorGroupGrid
                                    where i.Type == "MyLocation"
                                    select i).FirstOrDefault();

                        if (Main != null)
                        {
                            dto.Main = new CompAnalysisSummaryGroupDTO();

                            #region Current

                            dto.Main.Current = new CompAnalysisSummaryDTO();

                            dto.Main.Current.Score = Main.ScoreCurrent.GetValueOrDefault();
                            dto.Main.Current.Count = Main.TotalCurrent.GetValueOrDefault();
                            dto.Main.Current.ReviewCount = Main.TotalCurrent.GetValueOrDefault();
                            dto.Main.Current.CountNegative = Main.NegCurrent.GetValueOrDefault();
                            dto.Main.Current.CountPositive = Main.PosCurrent.GetValueOrDefault();

                            if (dto.Main.Current.ReviewCount > 0)
                            {
                                dto.Main.Current.PercentPositive = Convert.ToDecimal(dto.Main.Current.CountPositive) / (Convert.ToDecimal(dto.Main.Current.ReviewCount) + .00001M);
                                dto.Main.Current.PercentNegative = 1.0M - dto.Main.Current.PercentPositive;
                            }
                            else
                            {
                                dto.Main.Current.PercentPositive = 0;
                                dto.Main.Current.PercentNegative = 0;
                            }
                            #endregion

                            #region Last30Days
                            dto.Main.Last30Days = new CompAnalysisSummaryDTO();

                            dto.Main.Last30Days.Score = Main.Score30Days.GetValueOrDefault();
                            dto.Main.Last30Days.Count = Main.Total30Days.GetValueOrDefault();
                            dto.Main.Last30Days.ReviewCount = Main.Total30Days.GetValueOrDefault();
                            dto.Main.Last30Days.CountNegative = Main.Neg30Days.GetValueOrDefault();
                            dto.Main.Last30Days.CountPositive = Main.Pos30Days.GetValueOrDefault();

                            if (dto.Main.Last30Days.ReviewCount > 0)
                            {
                                dto.Main.Last30Days.PercentPositive = Convert.ToDecimal(dto.Main.Last30Days.CountPositive) / (Convert.ToDecimal(dto.Main.Last30Days.ReviewCount) + .00001M);
                                dto.Main.Last30Days.PercentNegative = 1.0M - dto.Main.Last30Days.PercentPositive;
                            }
                            else
                            {
                                dto.Main.Last30Days.PercentPositive = 0;
                                dto.Main.Last30Days.PercentNegative = 0;
                            }
                            #endregion

                        }
                        #endregion

                        #region My Group Avg
                        var MyGroup = (from i in accountCompetitorGroupGrid
                                       where i.Type == "MyGroup"
                                       select i).FirstOrDefault();

                        if (MyGroup != null)
                        {
                            dto.Group = new CompAnalysisSummaryGroupDTO();

                            #region Current

                            dto.Group.Current = new CompAnalysisSummaryDTO();

                            dto.Group.Current.Score = MyGroup.ScoreCurrent.GetValueOrDefault();
                            dto.Group.Current.Count = MyGroup.TotalCurrent.GetValueOrDefault();
                            dto.Group.Current.ReviewCount = MyGroup.TotalCurrent.GetValueOrDefault();
                            dto.Group.Current.CountNegative = MyGroup.NegCurrent.GetValueOrDefault();
                            dto.Group.Current.CountPositive = MyGroup.PosCurrent.GetValueOrDefault();

                            if (dto.Group.Current.ReviewCount > 0)
                            {
                                dto.Group.Current.PercentPositive = Convert.ToDecimal(dto.Group.Current.CountPositive) / (Convert.ToDecimal(dto.Group.Current.ReviewCount) + .00001M);
                                dto.Group.Current.PercentNegative = 1.0M - dto.Group.Current.PercentPositive;
                            }
                            else
                            {
                                dto.Group.Current.PercentPositive = 0;
                                dto.Group.Current.PercentNegative = 0;
                            }
                            #endregion

                            #region Last30Days
                            dto.Group.Last30Days = new CompAnalysisSummaryDTO();

                            dto.Group.Last30Days.Score = MyGroup.Score30Days.GetValueOrDefault();
                            dto.Group.Last30Days.Count = MyGroup.Total30Days.GetValueOrDefault();
                            dto.Group.Last30Days.ReviewCount = MyGroup.Total30Days.GetValueOrDefault();
                            dto.Group.Last30Days.CountNegative = MyGroup.Neg30Days.GetValueOrDefault();
                            dto.Group.Last30Days.CountPositive = MyGroup.Pos30Days.GetValueOrDefault();

                            if (dto.Group.Last30Days.ReviewCount > 0)
                            {
                                dto.Group.Last30Days.PercentPositive = Convert.ToDecimal(dto.Group.Last30Days.CountPositive) / (Convert.ToDecimal(dto.Group.Last30Days.ReviewCount) + .00001M);
                                dto.Group.Last30Days.PercentNegative = 1.0M - dto.Group.Last30Days.PercentPositive;
                            }
                            else
                            {
                                dto.Group.Last30Days.PercentPositive = 0;
                                dto.Group.Last30Days.PercentNegative = 0;
                            }
                            #endregion
                        }
                        #endregion

                        #region Competitors Avg

                        var Competitor = (from i in accountCompetitorGroupGrid
                                          where i.Type == "Competitor"
                                          select i).FirstOrDefault();

                        if (Competitor != null)
                        {
                            dto.Comp = new CompAnalysisSummaryGroupDTO();

                            #region Current

                            dto.Comp.Current = new CompAnalysisSummaryDTO();

                            dto.Comp.Current.Score = Competitor.ScoreCurrent.GetValueOrDefault();
                            dto.Comp.Current.Count = Competitor.TotalCurrent.GetValueOrDefault();
                            dto.Comp.Current.ReviewCount = Competitor.TotalCurrent.GetValueOrDefault();
                            dto.Comp.Current.CountNegative = Competitor.NegCurrent.GetValueOrDefault();
                            dto.Comp.Current.CountPositive = Competitor.PosCurrent.GetValueOrDefault();

                            if (dto.Comp.Current.ReviewCount > 0)
                            {
                                dto.Comp.Current.PercentPositive = Convert.ToDecimal(dto.Comp.Current.CountPositive) / (Convert.ToDecimal(dto.Comp.Current.ReviewCount) + .00001M);
                                dto.Comp.Current.PercentNegative = 1.0M - dto.Comp.Current.PercentPositive;
                            }
                            else
                            {
                                dto.Comp.Current.PercentPositive = 0;
                                dto.Comp.Current.PercentNegative = 0;
                            }
                            #endregion

                            #region Last30Days
                            dto.Comp.Last30Days = new CompAnalysisSummaryDTO();

                            dto.Comp.Last30Days.Score = Competitor.Score30Days.GetValueOrDefault();
                            dto.Comp.Last30Days.Count = Competitor.Total30Days.GetValueOrDefault();
                            dto.Comp.Last30Days.ReviewCount = Competitor.Total30Days.GetValueOrDefault();
                            dto.Comp.Last30Days.CountNegative = Competitor.Neg30Days.GetValueOrDefault();
                            dto.Comp.Last30Days.CountPositive = Competitor.Pos30Days.GetValueOrDefault();

                            if (dto.Comp.Last30Days.ReviewCount > 0)
                            {
                                dto.Comp.Last30Days.PercentPositive = Convert.ToDecimal(dto.Comp.Last30Days.CountPositive) / (Convert.ToDecimal(dto.Comp.Last30Days.ReviewCount) + .00001M);
                                dto.Comp.Last30Days.PercentNegative = 1.0M - dto.Comp.Last30Days.PercentPositive;
                            }
                            else
                            {
                                dto.Comp.Last30Days.PercentPositive = 0;
                                dto.Comp.Last30Days.PercentNegative = 0;
                            }
                            #endregion
                        }
                        #endregion

                        #endregion

                        #region 3rd Grid

                        if (accountsGrid != null && !contextIsGroup)
                        {
                            foreach (var item in accountsGrid)
                            {
                                CompAnalysisSummaryGroupDTO account = new CompAnalysisSummaryGroupDTO();

                                #region Current
                                account.Current = new CompAnalysisSummaryDTO();

                                account.Current.Score = item.ScoreCurrent.GetValueOrDefault();
                                account.Current.Count = item.TotalCurrent.GetValueOrDefault();
                                account.Current.ReviewCount = item.TotalCurrent.GetValueOrDefault();
                                account.Current.CountNegative = item.NegCurrent.GetValueOrDefault();
                                account.Current.CountPositive = item.PosCurrent.GetValueOrDefault();

                                if (account.Current.ReviewCount > 0)
                                {
                                    account.Current.PercentPositive = Convert.ToDecimal(account.Current.CountPositive) / (Convert.ToDecimal(account.Current.ReviewCount) + .00001M);
                                    account.Current.PercentNegative = 1.0M - account.Current.PercentPositive;
                                }
                                else
                                {
                                    account.Current.PercentPositive = 0;
                                    account.Current.PercentNegative = 0;
                                }
                                #endregion

                                #region Last24h
                                account.Last24h = new CompAnalysisSummaryDTO();

                                account.Last24h.Score = item.Score24Hours.GetValueOrDefault();
                                account.Last24h.Count = item.Total24Hours.GetValueOrDefault();
                                account.Last24h.ReviewCount = item.Total24Hours.GetValueOrDefault();
                                account.Last24h.CountNegative = item.Neg24Hours.GetValueOrDefault();
                                account.Last24h.CountPositive = item.Pos24Hours.GetValueOrDefault();

                                if (account.Last24h.ReviewCount > 0)
                                {
                                    account.Last24h.PercentPositive = Convert.ToDecimal(account.Last24h.CountPositive) / (Convert.ToDecimal(account.Last24h.ReviewCount) + .00001M);
                                    account.Last24h.PercentNegative = 1.0M - account.Last24h.PercentPositive;
                                }
                                else
                                {
                                    account.Last24h.PercentPositive = 0;
                                    account.Last24h.PercentNegative = 0;
                                }
                                #endregion

                                #region Last7Days
                                account.Last7Days = new CompAnalysisSummaryDTO();

                                account.Last7Days.Score = item.Score7Days.GetValueOrDefault();
                                account.Last7Days.Count = item.Total7Days.GetValueOrDefault();
                                account.Last7Days.ReviewCount = item.Total7Days.GetValueOrDefault();
                                account.Last7Days.CountNegative = item.Neg7Days.GetValueOrDefault();
                                account.Last7Days.CountPositive = item.Pos7Days.GetValueOrDefault();

                                if (account.Last7Days.ReviewCount > 0)
                                {
                                    account.Last7Days.PercentPositive = Convert.ToDecimal(account.Last7Days.CountPositive) / (Convert.ToDecimal(account.Last7Days.ReviewCount) + .00001M);
                                    account.Last7Days.PercentNegative = 1.0M - account.Last7Days.PercentPositive;
                                }
                                else
                                {
                                    account.Last7Days.PercentPositive = 0;
                                    account.Last7Days.PercentNegative = 0;
                                }
                                #endregion

                                #region Last30Days
                                account.Last30Days = new CompAnalysisSummaryDTO();

                                account.Last30Days.Score = item.Score30Days.GetValueOrDefault();
                                account.Last30Days.Count = item.Total30Days.GetValueOrDefault();
                                account.Last30Days.ReviewCount = item.Total30Days.GetValueOrDefault();
                                account.Last30Days.CountNegative = item.Neg30Days.GetValueOrDefault();
                                account.Last30Days.CountPositive = item.Pos30Days.GetValueOrDefault();

                                if (account.Last30Days.ReviewCount > 0)
                                {
                                    account.Last30Days.PercentPositive = Convert.ToDecimal(account.Last30Days.CountPositive) / (Convert.ToDecimal(account.Last30Days.ReviewCount) + .00001M);
                                    account.Last30Days.PercentNegative = 1.0M - account.Last30Days.PercentPositive;
                                }
                                else
                                {
                                    account.Last30Days.PercentPositive = 0;
                                    account.Last30Days.PercentNegative = 0;
                                }
                                #endregion

                                CompAnalysisSummaryGroupDTO compToUseCAG1 = new CompAnalysisSummaryGroupDTO()
                                {
                                    Current = account.Current,
                                    Last30Days = account.Last30Days,
                                    Last7Days = account.Last7Days,
                                    Last24h = account.Last24h
                                };

                                dto.CompetitorScores.Add(item.AccountName, compToUseCAG1);
                            }
                        }

                        #endregion
                    }
                }
            }
            return dto;
        }

        //public CompAnalysisDataDTO GetCompetitiveAnalysisData(UserInfoDTO userInfo)
        //{
        //    CompAnalysisDataDTO dto = new CompAnalysisDataDTO();

        //    using (MainEntities db = ContextFactory.Main)
        //    {
        //        if (db.Connection.State != System.Data.ConnectionState.Open)
        //            db.Connection.Open();

        //        // get the account lists

        //        long accountID = userInfo.EffectiveUser.CurrentContextAccountID;
        //        SecurityProvider sec = ((SecurityProvider)ProviderFactory.Security);

        //        AccountTree tree = sec.getAccountTree()[accountID];


        //        List<long> mainIDs = new List<long>();
        //        List<long> groupIDs = new List<long>();
        //        List<long> compIDs = new List<long>();

        //        List<AccountCompetitorListItem> competitors = null;

        //        bool singleDealerGroup = false;
        //        bool contextIsDealer = false;

        //        switch (tree.AccountType)
        //        {
        //            case AccountTypeEnum.Grouping:

        //                singleDealerGroup = (tree.Children.Count() <= 1);
        //                contextIsDealer = false;
        //                break;

        //            case AccountTypeEnum.Company:
        //                singleDealerGroup = !(tree.Parent.Depth > 1 && tree.Parent.Children.Count() > 1);
        //                contextIsDealer = true;
        //                break;
        //        }

        //        if (singleDealerGroup)
        //        {
        //            if (contextIsDealer)
        //            {
        //                mainIDs.Add(tree.ID);
        //                groupIDs.AddRange(mainIDs);
        //                competitors = sec.GetAccountCompetitorIDs(mainIDs);
        //            }
        //            else //context is group
        //            {
        //                mainIDs.Add(tree.ID);
        //                if (tree.Children.Any())
        //                    mainIDs.Add(tree.Children[0].ID);
        //                groupIDs.AddRange(mainIDs);
        //                competitors = sec.GetAccountCompetitorIDs(mainIDs);
        //            }
        //        }
        //        else //multi-dealer group
        //        {
        //            if (contextIsDealer)
        //            {
        //                mainIDs.Add(tree.ID);
        //                groupIDs.AddRange(sec.GetAccountDescendants(tree.Parent.ID));
        //                competitors = sec.GetAccountCompetitorIDs(groupIDs);
        //            }
        //            else //context is group
        //            {
        //                mainIDs.Add(tree.ID);
        //                mainIDs.AddRange(sec.GetAccountDescendants(tree.ID));
        //                groupIDs.AddRange(mainIDs);
        //                competitors = sec.GetAccountCompetitorIDs(groupIDs);
        //            }
        //        }

        //        compIDs = (from c in competitors select c.CompetitorAccountID).Distinct().ToList();

        //        using (DbCommand cmd = db.CreateStoreCommand("spGetCompetitiveData", System.Data.CommandType.StoredProcedure,
        //            new SqlParameter("AccountIDsMain", string.Join("|", mainIDs)),
        //            new SqlParameter("AccountIDsGroup", string.Join("|", groupIDs)),
        //            new SqlParameter("AccountIDsComp", string.Join("|", compIDs))
        //        ))
        //        {
        //            cmd.CommandTimeout = 30000;

        //            using (DbDataReader reader = cmd.ExecuteReader())
        //            {
        //                // master aggregates
        //                List<CompAnalysisAggregate> master = db.Translate<CompAnalysisAggregate>(reader).ToList();

        //                reader.NextResult();

        //                // by account aggregates
        //                List<CompAlalysisByAccount> acct = db.Translate<CompAlalysisByAccount>(reader).ToList();

        //                reader.NextResult();

        //                // address list
        //                List<SimpleAccountInfoDTO> addresses = db.Translate<SimpleAccountInfoDTO>(reader).ToList();

        //                //build up the report data

        //                // set the addresses
        //                dto.Accounts = addresses;

        //                // get the filtered lists
        //                List<CompAlalysisByAccount> mainAccounts = (from a in acct where mainIDs.Contains(a.AccountID) select a).ToList();
        //                List<CompAlalysisByAccount> groupAccounts = (from a in acct where groupIDs.Contains(a.AccountID) select a).ToList();
        //                List<CompAlalysisByAccount> compAccounts = (from a in acct where compIDs.Contains(a.AccountID) select a).ToList();

        //                // my score
        //                List<CompAlalysisByAccount> mscore = (
        //                    from a in mainAccounts
        //                    where a.FromSummary == true && a.Score > 0
        //                    select a
        //                ).ToList();
        //                if (mscore.Any())
        //                {
        //                    dto.MyScore = mscore.Average(x => x.Score);
        //                }

        //                //competitor score
        //                List<CompAlalysisByAccount> cscore = (
        //                    from a in compAccounts
        //                    where a.FromSummary == true && a.Score > 0
        //                    select a
        //                ).ToList();
        //                if (cscore.Any())
        //                {
        //                    dto.CompetitorScore = cscore.Average(x => x.Score);
        //                }

        //                #region main numbers

        //                CompAnalysisSummaryDTO mainCAS = new CompAnalysisSummaryDTO()
        //                {
        //                    Score = dto.MyScore,
        //                    ReviewCount = (from a in mainAccounts where a.FromSummary == true select a.Total).Sum(),
        //                    CountNegative = (from a in mainAccounts where a.FromSummary == false select a.Negative).Sum(),
        //                    CountPositive = (from a in mainAccounts where a.FromSummary == false select a.Positive).Sum(),
        //                    Count = (from a in mainAccounts where a.FromSummary == false select a.Total).Sum()
        //                };
        //                mainCAS.PercentPositive = Convert.ToDecimal(mainCAS.CountPositive) / (Convert.ToDecimal(mainCAS.Count) + .00001M);
        //                mainCAS.PercentNegative = 1.0M - mainCAS.PercentPositive;

        //                CompAnalysisSummaryDTO mainCAS30 = new CompAnalysisSummaryDTO()
        //                {
        //                    Score = (from a in mainAccounts where a.FromSummary == true select a.Score30).Average(),
        //                    ReviewCount = (from a in mainAccounts where a.FromSummary == true select a.Total30).Sum(),
        //                    CountNegative = (from a in mainAccounts where a.FromSummary == false select a.Negative30).Sum(),
        //                    CountPositive = (from a in mainAccounts where a.FromSummary == false select a.Positive30).Sum(),
        //                    Count = (from a in mainAccounts where a.FromSummary == false select a.Total30).Sum()
        //                };
        //                mainCAS30.PercentPositive = Convert.ToDecimal(mainCAS30.CountPositive) / (Convert.ToDecimal(mainCAS30.Count) + .00001M);
        //                mainCAS30.PercentNegative = 1.0M - mainCAS30.PercentPositive;

        //                CompAnalysisSummaryDTO mainCAS7 = new CompAnalysisSummaryDTO()
        //                {
        //                    Score = (from a in mainAccounts where a.FromSummary == true select a.Score7).Average(),
        //                    ReviewCount = (from a in mainAccounts where a.FromSummary == true select a.Total7).Sum(),
        //                    CountNegative = (from a in mainAccounts where a.FromSummary == false select a.Negative7).Sum(),
        //                    CountPositive = (from a in mainAccounts where a.FromSummary == false select a.Positive7).Sum(),
        //                    Count = (from a in mainAccounts where a.FromSummary == false select a.Total7).Sum()
        //                };
        //                mainCAS7.PercentPositive = Convert.ToDecimal(mainCAS7.CountPositive) / (Convert.ToDecimal(mainCAS7.Count) + .00001M);
        //                mainCAS7.PercentNegative = 1.0M - mainCAS7.PercentPositive;

        //                CompAnalysisSummaryDTO mainCAS24h = new CompAnalysisSummaryDTO()
        //                {
        //                    Score = (from a in mainAccounts where a.FromSummary == true select a.Score24h).Average(),
        //                    ReviewCount = (from a in mainAccounts where a.FromSummary == true select a.Total24h).Sum(),
        //                    CountNegative = (from a in mainAccounts where a.FromSummary == false select a.Negative24h).Sum(),
        //                    CountPositive = (from a in mainAccounts where a.FromSummary == false select a.Positive24h).Sum(),
        //                    Count = (from a in mainAccounts where a.FromSummary == false select a.Total24h).Sum()
        //                };
        //                mainCAS24h.PercentPositive = Convert.ToDecimal(mainCAS24h.CountPositive) / (Convert.ToDecimal(mainCAS24h.Count) + .00001M);
        //                mainCAS24h.PercentNegative = 1.0M - mainCAS24h.PercentPositive;

        //                CompAnalysisSummaryGroupDTO mainCAG = new CompAnalysisSummaryGroupDTO()
        //                {
        //                    Current = mainCAS,
        //                    Last30Days = mainCAS30,
        //                    Last7Days = mainCAS7,
        //                    Last24h = mainCAS24h
        //                };

        //                dto.Main = mainCAG;

        //                #endregion

        //                #region group numbers

        //                CompAnalysisSummaryDTO groupCAS = new CompAnalysisSummaryDTO()
        //                {
        //                    Score = (from a in groupAccounts where a.FromSummary == true select a.Score).Average(),
        //                    ReviewCount = (from a in groupAccounts where a.FromSummary == true select a.Total).Sum(),
        //                    CountNegative = (from a in groupAccounts where a.FromSummary == false select a.Negative).Sum(),
        //                    CountPositive = (from a in groupAccounts where a.FromSummary == false select a.Positive).Sum(),
        //                    Count = (from a in groupAccounts where a.FromSummary == false select a.Total).Sum()
        //                };
        //                groupCAS.PercentPositive = Convert.ToDecimal(groupCAS.CountPositive) / (Convert.ToDecimal(groupCAS.Count) + .00001M);
        //                groupCAS.PercentNegative = 1.0M - groupCAS.PercentPositive;

        //                CompAnalysisSummaryDTO groupCAS30 = new CompAnalysisSummaryDTO()
        //                {
        //                    Score = (from a in groupAccounts where a.FromSummary == true select a.Score30).Average(),
        //                    ReviewCount = (from a in groupAccounts where a.FromSummary == true select a.Total30).Sum(),
        //                    CountNegative = (from a in groupAccounts where a.FromSummary == false select a.Negative30).Sum(),
        //                    CountPositive = (from a in groupAccounts where a.FromSummary == false select a.Positive30).Sum(),
        //                    Count = (from a in groupAccounts where a.FromSummary == false select a.Total30).Sum()
        //                };
        //                groupCAS30.PercentPositive = Convert.ToDecimal(groupCAS30.CountPositive) / (Convert.ToDecimal(groupCAS30.Count) + .00001M);
        //                groupCAS30.PercentNegative = 1.0M - groupCAS30.PercentPositive;

        //                CompAnalysisSummaryDTO groupCAS7 = new CompAnalysisSummaryDTO()
        //                {
        //                    Score = (from a in groupAccounts where a.FromSummary == true select a.Score7).Average(),
        //                    ReviewCount = (from a in groupAccounts where a.FromSummary == true select a.Total7).Sum(),
        //                    CountNegative = (from a in groupAccounts where a.FromSummary == false select a.Negative7).Sum(),
        //                    CountPositive = (from a in groupAccounts where a.FromSummary == false select a.Positive7).Sum(),
        //                    Count = (from a in groupAccounts where a.FromSummary == false select a.Total7).Sum()
        //                };
        //                groupCAS7.PercentPositive = Convert.ToDecimal(groupCAS7.CountPositive) / (Convert.ToDecimal(groupCAS7.Count) + .00001M);
        //                groupCAS7.PercentNegative = 1.0M - groupCAS7.PercentPositive;

        //                CompAnalysisSummaryDTO groupCAS24h = new CompAnalysisSummaryDTO()
        //                {
        //                    Score = (from a in groupAccounts where a.FromSummary == true select a.Score24h).Average(),
        //                    ReviewCount = (from a in groupAccounts where a.FromSummary == true select a.Total24h).Sum(),
        //                    CountNegative = (from a in groupAccounts where a.FromSummary == false select a.Negative24h).Sum(),
        //                    CountPositive = (from a in groupAccounts where a.FromSummary == false select a.Positive24h).Sum(),
        //                    Count = (from a in groupAccounts where a.FromSummary == false select a.Total24h).Sum()
        //                };
        //                groupCAS24h.PercentPositive = Convert.ToDecimal(mainCAS24h.CountPositive) / (Convert.ToDecimal(groupCAS24h.Count) + .00001M);
        //                groupCAS24h.PercentNegative = 1.0M - groupCAS24h.PercentPositive;

        //                CompAnalysisSummaryGroupDTO groupCAG = new CompAnalysisSummaryGroupDTO()
        //                {
        //                    Current = groupCAS,
        //                    Last30Days = groupCAS30,
        //                    Last7Days = groupCAS7,
        //                    Last24h = groupCAS24h
        //                };

        //                dto.Group = groupCAG;

        //                #endregion

        //                #region competitor numbers

        //                List<CompAlalysisByAccount> comCASSUM = (
        //                    from a in compAccounts
        //                    where a.FromSummary == true
        //                    select a
        //                ).ToList();
        //                List<CompAlalysisByAccount> comCASDET = (
        //                    from a in compAccounts
        //                    where a.FromSummary == false
        //                    select a
        //                ).ToList();

        //                if (comCASDET.Any() && comCASSUM.Any())
        //                {
        //                    CompAnalysisSummaryDTO compCAS = new CompAnalysisSummaryDTO()
        //                    {
        //                        Score = (from a in compAccounts where a.FromSummary == true select a.Score).Average(),
        //                        ReviewCount = (from a in compAccounts where a.FromSummary == true select a.Total).Sum(),
        //                        CountNegative = (from a in compAccounts where a.FromSummary == false select a.Negative).Sum(),
        //                        CountPositive = (from a in compAccounts where a.FromSummary == false select a.Positive).Sum(),
        //                        Count = (from a in compAccounts where a.FromSummary == false select a.Total).Sum()
        //                    };
        //                    compCAS.PercentPositive = Convert.ToDecimal(compCAS.CountPositive) / (Convert.ToDecimal(compCAS.Count) + .00001M);
        //                    compCAS.PercentNegative = 1.0M - compCAS.PercentPositive;

        //                    CompAnalysisSummaryDTO compCAS30 = new CompAnalysisSummaryDTO()
        //                    {
        //                        Score = (from a in compAccounts where a.FromSummary == true select a.Score30).Average(),
        //                        ReviewCount = (from a in compAccounts where a.FromSummary == true select a.Total30).Sum(),
        //                        CountNegative = (from a in compAccounts where a.FromSummary == false select a.Negative30).Sum(),
        //                        CountPositive = (from a in compAccounts where a.FromSummary == false select a.Positive30).Sum(),
        //                        Count = (from a in compAccounts where a.FromSummary == false select a.Total30).Sum()
        //                    };
        //                    compCAS30.PercentPositive = Convert.ToDecimal(compCAS30.CountPositive) / (Convert.ToDecimal(compCAS30.Count) + .00001M);
        //                    compCAS30.PercentNegative = 1.0M - compCAS30.PercentPositive;

        //                    CompAnalysisSummaryDTO compCAS7 = new CompAnalysisSummaryDTO()
        //                    {
        //                        Score = (from a in compAccounts where a.FromSummary == true select a.Score7).Average(),
        //                        ReviewCount = (from a in compAccounts where a.FromSummary == true select a.Total7).Sum(),
        //                        CountNegative = (from a in compAccounts where a.FromSummary == false select a.Negative7).Sum(),
        //                        CountPositive = (from a in compAccounts where a.FromSummary == false select a.Positive7).Sum(),
        //                        Count = (from a in compAccounts where a.FromSummary == false select a.Total7).Sum()
        //                    };
        //                    compCAS7.PercentPositive = Convert.ToDecimal(compCAS7.CountPositive) / (Convert.ToDecimal(compCAS7.Count) + .00001M);
        //                    compCAS7.PercentNegative = 1.0M - compCAS7.PercentPositive;

        //                    CompAnalysisSummaryDTO compCAS24h = new CompAnalysisSummaryDTO()
        //                    {
        //                        Score = (from a in compAccounts where a.FromSummary == true select a.Score24h).Average(),
        //                        ReviewCount = (from a in compAccounts where a.FromSummary == true select a.Total24h).Sum(),
        //                        CountNegative = (from a in compAccounts where a.FromSummary == false select a.Negative24h).Sum(),
        //                        CountPositive = (from a in compAccounts where a.FromSummary == false select a.Positive24h).Sum(),
        //                        Count = (from a in compAccounts where a.FromSummary == false select a.Total24h).Sum()
        //                    };
        //                    compCAS24h.PercentPositive = Convert.ToDecimal(compCAS24h.CountPositive) / (Convert.ToDecimal(compCAS24h.Count) + .00001M);
        //                    compCAS24h.PercentNegative = 1.0M - compCAS24h.PercentPositive;

        //                    CompAnalysisSummaryGroupDTO compCAG = new CompAnalysisSummaryGroupDTO()
        //                    {
        //                        Current = compCAS,
        //                        Last30Days = compCAS30,
        //                        Last7Days = compCAS7,
        //                        Last24h = compCAS24h
        //                    };

        //                    dto.Comp = compCAG;
        //                }
        //                else
        //                {
        //                    CompAnalysisSummaryDTO compCAS = new CompAnalysisSummaryDTO()
        //                    {
        //                        Score = 0,
        //                        ReviewCount = 0,
        //                        CountNegative = 0,
        //                        CountPositive = 0,
        //                        Count = 0
        //                    };
        //                    compCAS.PercentPositive = Convert.ToDecimal(compCAS.CountPositive) / (Convert.ToDecimal(compCAS.Count) + .00001M);
        //                    compCAS.PercentNegative = 1.0M - compCAS.PercentPositive;

        //                    CompAnalysisSummaryDTO compCAS30 = new CompAnalysisSummaryDTO()
        //                    {
        //                        Score = 0,
        //                        ReviewCount = 0,
        //                        CountNegative = 0,
        //                        CountPositive = 0,
        //                        Count = 0
        //                    };
        //                    compCAS30.PercentPositive = Convert.ToDecimal(compCAS30.CountPositive) / (Convert.ToDecimal(compCAS30.Count) + .00001M);
        //                    compCAS30.PercentNegative = 1.0M - compCAS30.PercentPositive;

        //                    CompAnalysisSummaryDTO compCAS7 = new CompAnalysisSummaryDTO()
        //                    {
        //                        Score = 0,
        //                        ReviewCount = 0,
        //                        CountNegative = 0,
        //                        CountPositive = 0,
        //                        Count = 0
        //                    };
        //                    compCAS7.PercentPositive = Convert.ToDecimal(compCAS7.CountPositive) / (Convert.ToDecimal(compCAS7.Count) + .00001M);
        //                    compCAS7.PercentNegative = 1.0M - compCAS7.PercentPositive;

        //                    CompAnalysisSummaryDTO compCAS24h = new CompAnalysisSummaryDTO()
        //                    {
        //                        Score = 0,
        //                        ReviewCount = 0,
        //                        CountNegative = 0,
        //                        CountPositive = 0,
        //                        Count = 0
        //                    };
        //                    compCAS24h.PercentPositive = Convert.ToDecimal(compCAS24h.CountPositive) / (Convert.ToDecimal(compCAS24h.Count) + .00001M);
        //                    compCAS24h.PercentNegative = 1.0M - compCAS24h.PercentPositive;

        //                    CompAnalysisSummaryGroupDTO compCAG = new CompAnalysisSummaryGroupDTO()
        //                    {
        //                        Current = compCAS,
        //                        Last30Days = compCAS30,
        //                        Last7Days = compCAS7,
        //                        Last24h = compCAS24h
        //                    };

        //                    dto.Comp = compCAG;
        //                }


        //                #endregion

        //                #region competitor by account

        //                if (contextIsDealer)
        //                {
        //                    List<long> compIDsToUse = (from c in competitors where c.AccountID == accountID select c.CompetitorAccountID).Distinct().ToList();

        //                    if (compIDsToUse.Any())
        //                    {
        //                        foreach (long compAccountID in compIDsToUse)
        //                        {
        //                            List<CompAlalysisByAccount> compsToUseAccounts = (from a in acct where a.AccountID == compAccountID select a).ToList();

        //                            #region competitor numbers

        //                            CompAnalysisSummaryDTO compToUseCAS = new CompAnalysisSummaryDTO()
        //                            {
        //                                Score = (from a in compsToUseAccounts where a.FromSummary == true select a.Score).Average(),
        //                                ReviewCount = (from a in compsToUseAccounts where a.FromSummary == true select a.Total).Sum(),
        //                                CountNegative = (from a in compsToUseAccounts where a.FromSummary == false select a.Negative).Sum(),
        //                                CountPositive = (from a in compsToUseAccounts where a.FromSummary == false select a.Positive).Sum(),
        //                                Count = (from a in compsToUseAccounts where a.FromSummary == false select a.Total).Sum()
        //                            };
        //                            compToUseCAS.PercentPositive = Convert.ToDecimal(compToUseCAS.CountPositive) / (Convert.ToDecimal(compToUseCAS.Count) + .00001M);
        //                            compToUseCAS.PercentNegative = 1.0M - compToUseCAS.PercentPositive;

        //                            CompAnalysisSummaryDTO compToUseCAS30 = new CompAnalysisSummaryDTO()
        //                            {
        //                                Score = (from a in compsToUseAccounts where a.FromSummary == true select a.Score30).Average(),
        //                                ReviewCount = (from a in compsToUseAccounts where a.FromSummary == true select a.Total30).Sum(),
        //                                CountNegative = (from a in compsToUseAccounts where a.FromSummary == false select a.Negative30).Sum(),
        //                                CountPositive = (from a in compsToUseAccounts where a.FromSummary == false select a.Positive30).Sum(),
        //                                Count = (from a in compsToUseAccounts where a.FromSummary == false select a.Total30).Sum()
        //                            };
        //                            compToUseCAS30.PercentPositive = Convert.ToDecimal(compToUseCAS30.CountPositive) / (Convert.ToDecimal(compToUseCAS30.Count) + .00001M);
        //                            compToUseCAS30.PercentNegative = 1.0M - compToUseCAS30.PercentPositive;

        //                            CompAnalysisSummaryDTO compToUseCAS7 = new CompAnalysisSummaryDTO()
        //                            {
        //                                Score = (from a in compsToUseAccounts where a.FromSummary == true select a.Score7).Average(),
        //                                ReviewCount = (from a in compsToUseAccounts where a.FromSummary == true select a.Total7).Sum(),
        //                                CountNegative = (from a in compsToUseAccounts where a.FromSummary == false select a.Negative7).Sum(),
        //                                CountPositive = (from a in compsToUseAccounts where a.FromSummary == false select a.Positive7).Sum(),
        //                                Count = (from a in compsToUseAccounts where a.FromSummary == false select a.Total7).Sum()
        //                            };
        //                            compToUseCAS7.PercentPositive = Convert.ToDecimal(compToUseCAS7.CountPositive) / (Convert.ToDecimal(compToUseCAS7.Count) + .00001M);
        //                            compToUseCAS7.PercentNegative = 1.0M - compToUseCAS7.PercentPositive;

        //                            CompAnalysisSummaryDTO compToUseCAS24h = new CompAnalysisSummaryDTO()
        //                            {
        //                                Score = (from a in compsToUseAccounts where a.FromSummary == true select a.Score24h).Average(),
        //                                ReviewCount = (from a in compsToUseAccounts where a.FromSummary == true select a.Total24h).Sum(),
        //                                CountNegative = (from a in compsToUseAccounts where a.FromSummary == false select a.Negative24h).Sum(),
        //                                CountPositive = (from a in compsToUseAccounts where a.FromSummary == false select a.Positive24h).Sum(),
        //                                Count = (from a in compsToUseAccounts where a.FromSummary == false select a.Total24h).Sum()
        //                            };
        //                            compToUseCAS24h.PercentPositive = Convert.ToDecimal(compToUseCAS24h.CountPositive) / (Convert.ToDecimal(compToUseCAS24h.Count) + .00001M);
        //                            compToUseCAS24h.PercentNegative = 1.0M - compToUseCAS24h.PercentPositive;

        //                            CompAnalysisSummaryGroupDTO compToUseCAG = new CompAnalysisSummaryGroupDTO()
        //                            {
        //                                Current = compToUseCAS,
        //                                Last30Days = compToUseCAS30,
        //                                Last7Days = compToUseCAS7,
        //                                Last24h = compToUseCAS24h
        //                            };

        //                            dto.CompetitorScores.Add(compAccountID, compToUseCAG);

        //                            #endregion
        //                        }
        //                    }
        //                }

        //                #endregion
        //            }
        //        }

        //    }

        //    return dto;
        //}


        private class SatisfactionScores
        {
            public decimal? MyScore { get; set; }
            public decimal? CompetitorScore { get; set; }
        }

        private class AccountCompetitorGroupGrid
        {
            public string Type { get; set; }
            public decimal? ScoreCurrent { get; set; }
            public int? TotalCurrent { get; set; }
            public int? PosCurrent { get; set; }
            public int? NegCurrent { get; set; }
            public decimal? Score30Days { get; set; }
            public int? Total30Days { get; set; }
            public int? Pos30Days { get; set; }
            public int? Neg30Days { get; set; }
        }

        private class AccountsGrid
        {
            public long AccountID { get; set; }
            public string AccountName { get; set; }

            public decimal? ScoreCurrent { get; set; }
            public int? TotalCurrent { get; set; }
            public int? PosCurrent { get; set; }
            public int? NegCurrent { get; set; }

            public decimal? Score24Hours { get; set; }
            public int? Total24Hours { get; set; }
            public int? Pos24Hours { get; set; }
            public int? Neg24Hours { get; set; }

            public decimal? Score7Days { get; set; }
            public int? Total7Days { get; set; }
            public int? Pos7Days { get; set; }
            public int? Neg7Days { get; set; }

            public decimal? Score30Days { get; set; }
            public int? Total30Days { get; set; }
            public int? Pos30Days { get; set; }
            public int? Neg30Days { get; set; }
        }

        private class CompAnalysisAggregate
        {
            public bool FromSummary { get; set; }

            public decimal MainScore { get; set; }
            public int MainCount { get; set; }
            public int MainPosCount { get; set; }
            public int MainNegCount { get; set; }
            public decimal MainScore30 { get; set; }
            public int MainCount30 { get; set; }
            public int MainPosCount30 { get; set; }
            public int MainNegCount30 { get; set; }
            public decimal MainScore7 { get; set; }
            public int MainCount7 { get; set; }
            public int MainPosCount7 { get; set; }
            public int MainNegCount7 { get; set; }
            public decimal MainScore24h { get; set; }
            public int MainCount24h { get; set; }
            public int MainPosCount24h { get; set; }
            public int MainNegCount24h { get; set; }

            public decimal GroupScore { get; set; }
            public int GroupCount { get; set; }
            public int GroupPosCount { get; set; }
            public int GroupNegCount { get; set; }
            public decimal GroupScore30 { get; set; }
            public int GroupCount30 { get; set; }
            public int GroupPosCount30 { get; set; }
            public int GroupNegCount30 { get; set; }
            public decimal GroupScore7 { get; set; }
            public int GroupCount7 { get; set; }
            public int GroupPosCount7 { get; set; }
            public int GroupNegCount7 { get; set; }
            public decimal GroupScore24h { get; set; }
            public int GroupCount24h { get; set; }
            public int GroupPosCount24h { get; set; }
            public int GroupNegCount24h { get; set; }

            public decimal CompScore { get; set; }
            public int CompCount { get; set; }
            public int CompPosCount { get; set; }
            public int CompNegCount { get; set; }
            public decimal CompScore30 { get; set; }
            public int CompCount30 { get; set; }
            public int CompPosCount30 { get; set; }
            public int CompNegCount30 { get; set; }
            public decimal CompScore7 { get; set; }
            public int CompCount7 { get; set; }
            public int CompPosCount7 { get; set; }
            public int CompNegCount7 { get; set; }
            public decimal CompScore24h { get; set; }
            public int CompCount24h { get; set; }
            public int CompPosCount24h { get; set; }
            public int CompNegCount24h { get; set; }

        }

        private class CompAlalysisByAccount
        {
            public bool FromSummary { get; set; }
            public long AccountID { get; set; }

            public decimal Score { get; set; }
            public decimal Score30 { get; set; }
            public decimal Score7 { get; set; }
            public decimal Score24h { get; set; }

            public int Total { get; set; }
            public int Total30 { get; set; }
            public int Total7 { get; set; }
            public int Total24h { get; set; }

            public int Positive { get; set; }
            public int Positive30 { get; set; }
            public int Positive7 { get; set; }
            public int Positive24h { get; set; }

            public int Negative { get; set; }
            public int Negative30 { get; set; }
            public int Negative7 { get; set; }
            public int Negative24h { get; set; }
        }

    }
}






