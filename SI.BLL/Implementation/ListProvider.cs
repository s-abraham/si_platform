﻿using SI.DAL;
using SI.DTO;
using System;
using System.Collections.Generic;
using System.Data.Objects;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace SI.BLL
{
    internal class ListProvider : IListProvider
    {

        public SaveEntityDTO<CountryDTO> SaveCountry(DTO.CountryDTO country)
        {
            throw new NotImplementedException();
        }

        public List<CountryDTO> GetCountries()
        {
            using (MainEntities db = ContextFactory.Main)
            {
                return
                (
                    from c
                    in db.Countries
                    select new CountryDTO()
                    {
                        ID = c.ID,
                        LanguageID = c.LanguageID,
                        Name = c.Name,
                        Prefix = c.Prefix
                    }
                ).ToList();
            }
        }

        public CountryDTO GetCountryByID(long countryID)
        {
            using (MainEntities db = ContextFactory.Main)
            {
                return Country2DTO(
                    (from c in db.Countries where c.ID.Equals(countryID) select c).FirstOrDefault()
                );
            }
        }

        public CountryDTO GetCountryByPrefix(string prefix)
        {
            prefix = prefix.Trim().ToLower();
            using (MainEntities db = ContextFactory.Main)
            {
                return Country2DTO(
                    (from c in db.Countries where c.Prefix.Trim().ToLower() == prefix select c).FirstOrDefault()
                );
            }
        }

        public SaveEntityDTO<StateDTO> SaveState(DTO.StateDTO state)
        {
            throw new NotImplementedException();
        }

        public StateDTO GetStateByID(long stateID)
        {
            using (MainEntities db = ContextFactory.Main)
            {
                return State2DTO(
                    (from s in db.States where s.ID.Equals(stateID) select s).FirstOrDefault()
                );
            }
        }

        public StateDTO GetStateByAbbreviation(long countryID, string abbreviation)
        {
            abbreviation = abbreviation.Trim().ToLower();
            using (MainEntities db = ContextFactory.Main)
            {
                return State2DTO(
                    (
                    from s
                    in db.States
                    where s.Abbreviation.Trim().ToLower() == abbreviation
                    && s.CountryID.Equals(countryID)
                    select s
                    ).FirstOrDefault()
                );
            }
        }

        public StateDTO GetStateByName(long countryID, string name)
        {
            name = name.Trim().ToLower();
            using (MainEntities db = ContextFactory.Main)
            {
                return State2DTO(
                    (
                    from s in db.States
                    where s.Name.Trim().ToLower() == name && s.CountryID.Equals(countryID)
                    select s
                    ).FirstOrDefault()
                );
            }
        }

        public List<StateDTO> GetStatesForCountry(long countryID)
        {
            using (MainEntities db = ContextFactory.Main)
            {
                return
                (
                    from s
                    in db.States
                    where s.CountryID.Equals(countryID)
                    orderby s.Name
                    select new StateDTO()
                        {
                            ID = s.ID,
                            CountryID = s.CountryID,
                            LanguageID = s.LanguageID,
                            Abbreviation = s.Abbreviation,
                            Name = s.Name
                        }
                ).ToList();
            }
        }

        public ZipCodeDTO GetZipCode(long zipCode)
        {
            using (MainEntities db = ContextFactory.Main)
            {
                return Zip2DTO(
                    (from z in db.ZipCodes where z.ZipCode1.Equals(zipCode) select z).FirstOrDefault()
                );
            }
        }

        private static List<ZipCodeDTO> _zipCache = null;
        private Random _r = new Random(DateTime.Now.Millisecond + DateTime.Now.Minute);

        public ZipCodeDTO GetRandomZipCode()
        {
            if (_zipCache == null)
            {
                using (MainEntities db = ContextFactory.Main)
                {
                    List<ZipCode> list = (
                        from z in db.ZipCodes select z
                    ).ToList();

                    _zipCache = (
                        from z in list
                        select Zip2DTO(z)
                    ).ToList();

                }
            }

            return _zipCache[_r.Next(0, _zipCache.Count() - 1)];
        }

        public CityDTO GetCityByID(long cityID)
        {
            using (MainEntities db = ContextFactory.Main)
            {
                return City2DTO(
                    (from c in db.Cities where c.ID.Equals(cityID) select c).FirstOrDefault()
                );
            }
        }

        public List<CityDTO> GetCitiesForZipCode(string zipCode)
        {
            using (MainEntities db = ContextFactory.Main)
            {
                return
                (
                    from c2 in
                        (
                        from c in db.Cities
                        join z in db.ZipCodes on c.ZipCodeID equals z.ID
                        where z.ZipCode1.Equals(zipCode)
                        orderby c.Name
                        select c
                        ).ToList()
                    select City2DTO(c2)
                ).ToList();
            }
        }

        public List<CityDTO> GetCitiesForState(long stateID)
        {
            using (MainEntities db = ContextFactory.Main)
            {
                return
                (
                    from c2 in
                        (
                        from c in db.Cities
                        where c.StateID.Equals(stateID)
                        orderby c.Name
                        select c
                        )
                    select City2DTO(c2)
                ).ToList();
            }
        }


        private List<FranchiseTypeDTO> _franchiseTypeCache = null;
        public List<FranchiseTypeDTO> GetFranchiseTypes()
        {
            if (_franchiseTypeCache != null)
            {
                return _franchiseTypeCache;
            }
            using (MainEntities db = ContextFactory.Main)
            {
                _franchiseTypeCache = (
                    from i in db.FranchiseTypes
                    orderby i.Name
                    select new FranchiseTypeDTO()
                    {
                        ID = i.ID,
                        Name = i.Name
                    }
                 ).ToList();
            }

            return _franchiseTypeCache;
        }

		private List<VerticalMarketDTO> _VerticalMarketCache = null;
		public List<VerticalMarketDTO> GetVerticalMarkets()
		{
			if (_VerticalMarketCache != null)
			{
				return _VerticalMarketCache;
			}
			using (MainEntities db = ContextFactory.Main)
			{
				_VerticalMarketCache = (
					from i in db.Verticals
					orderby i.Name
					select new VerticalMarketDTO()
					{
						ID = i.ID,
						Name = i.Name
					}
				 ).ToList();
			}

			return _VerticalMarketCache;
		}

        private List<CRMSystemTypeDTO> _crmSystemTypeCache = null;
        public List<CRMSystemTypeDTO> GetCRMSystemTypes()
        {
            if (_crmSystemTypeCache != null)
            {
                return _crmSystemTypeCache;
            }
            using (MainEntities db = ContextFactory.Main)
            {
                _crmSystemTypeCache = (
                    from i in db.CRMSystemTypes
                    orderby i.Name
                    select new CRMSystemTypeDTO()
                    {
                        ID = i.ID,
                        Name = i.Name
                    }
                 ).ToList();
            }

            return _crmSystemTypeCache;
        }

        public FranchiseTypeDTO AddFranchiseType(string Name)
        {
            using (MainEntities db = ContextFactory.Main)
            {
                FranchiseType type = new FranchiseType() { Name = Name };
                db.FranchiseTypes.AddObject(type);
                db.SaveChanges();
                return new FranchiseTypeDTO() { ID = type.ID, Name = type.Name };
            }
        }

        private List<AccountTypesDTO> _accountTypesCache = null;
        public List<AccountTypesDTO> GetAccountTypes()
        {
            if (_accountTypesCache != null)
            {
                return _accountTypesCache;
            }
            using (MainEntities db = ContextFactory.Main)
            {
                _accountTypesCache = (
                    from i in db.AccountTypes
                    select new AccountTypesDTO()
                    {
                        ID = i.ID,
                        Name = i.Name
                    }
                 ).ToList();
            }

            return _accountTypesCache;
        }


        private List<CategoryDTO> _categoryCache = null;
        public List<CategoryDTO> GetCategory()
        {
            if (_categoryCache != null)
            {
                return _categoryCache;
            }
            using (MainEntities db = ContextFactory.Main)
            {
                _categoryCache = (
                    from i in db.FranchiseCategories
                    orderby i.Name
                    select new CategoryDTO()
                    {
                        ID = i.ID,
                        Name = i.Name
                    }
                 ).ToList();
            }

            return _categoryCache;
        }

        public List<CompetitorDTO> GetCompetitorByAccountID(long accountID)
        {
            List<CompetitorDTO> competitors = new List<CompetitorDTO>();

            using (MainEntities db = ContextFactory.Main)
            {
                ObjectResult<spGetAccountCompetitors_Result> results = db.spGetAccountCompetitors(accountID);

                foreach (var item in results)
                {
                    CompetitorDTO competitor = new CompetitorDTO();

                    competitor.Name = item.DisplayName;
                    competitor.City = item.CityName;
                    competitor.State = item.StateName;
                    competitor.ZipCode = item.ZipCode;
                    competitor.AccountID = item.AccountID;
                    competitor.ID = Convert.ToInt64(item.Accounts_CompetitorsID);

                    competitors.Add(competitor);
                }
            }

            return competitors;
        }

        public List<CompetitorDTO> GetAccountsByDistance(UserInfoDTO userInfoDto, long accountID, int RangeInMiles = 50)
        {
            List<CompetitorDTO> competitors = new List<CompetitorDTO>();
           
            using (MainEntities db = ContextFactory.Main)
            {
                List<long> accountIDs =
                 AccountListBuilder
                     .New(new SecurityProvider())
                     .AddAccountsWithPermission(userInfoDto, PermissionTypeEnum.reviews_view)
                     .ExpandToDescendants()                     
                     .GetIDs(SecurityProvider.PLATFORM_MAX_ACCOUNT_ID_LIST)
             ;

                //string.Join("|", accountIDs.ToArray())
                //RangeInMiles
                ObjectResult<spGetAccountsByDistance3_Result> results = db.spGetAccountsByDistance3(accountID, string.Join("|", accountIDs.ToArray()), RangeInMiles);

                foreach (var item in results)
                {
                    CompetitorDTO competitor = new CompetitorDTO();

                    competitor.Name = item.DisplayName;
                    competitor.City = item.CityName;
                    competitor.State = item.StateName;
                    competitor.ZipCode = item.ZipCode;
                    competitor.ID = item.AccountID;

                    competitors.Add(competitor);
                }
            }

            return competitors;
        }

        #region translation helpers

        private CountryDTO Country2DTO(Country c)
        {
            if (c == null)
                return null;

            return new CountryDTO()
            {
                ID = c.ID,
                LanguageID = c.LanguageID,
                Name = c.Name,
                Prefix = c.Prefix
            };
        }

        private StateDTO State2DTO(State s)
        {
            if (s == null)
                return null;

            return new StateDTO()
            {
                ID = s.ID,
                CountryID = s.CountryID,
                LanguageID = s.LanguageID,
                Abbreviation = s.Abbreviation,
                Name = s.Name
            };
        }

        private ZipCodeDTO Zip2DTO(ZipCode z)
        {
            if (z == null)
                return null;

            return new ZipCodeDTO()
            {
                ID = z.ID,
                StateID = z.StateID,
                ZipCode = z.ZipCode1,
                Latitude = z.Latitude,
                Longitude = z.Longitude
            };
        }

        private CityDTO City2DTO(City c)
        {
            if (c == null)
                return null;
            return new CityDTO()
            {
                ID = c.ID,
                LanguageID = c.LanguageID,
                Name = c.Name,
                StateID = c.StateID,
                ZipCodeID = c.ZipCodeID
            };
        }

        #endregion



        public List<TimeZoneDTO> GetTimeZones()
        {
            using (MainEntities db = ContextFactory.Main)
            {
                List<TimeZoneDTO> zones = new List<TimeZoneDTO>();
                foreach (Timezone tz in (from z in db.Timezones where z.Active == true orderby z.BaseUTCOffsetHours select z))
                {
                    zones.Add(new TimeZoneDTO()
                    {
                        ID = tz.ID,
                        Name = tz.Name
                    });
                }

                return zones;
            }
        }

        private List<CategoriesDTO> _postCategoriesCache = null;
        public List<CategoriesDTO> GetPostCategoriesByVerticalID(long verticalID)
        {
            if (_postCategoriesCache != null)
            {
                return _postCategoriesCache;
            }
			
            using (MainEntities db = ContextFactory.Main)
            {
                _postCategoriesCache = (
                    from i in db.PostCategories
                    where i.VerticalID == verticalID
                    orderby i.Name
                    select new CategoriesDTO()
                    {
                        ID = i.ID,
                        Name = i.Name
                    }
                 ).ToList();
            }

            return _postCategoriesCache;
        }
    }
}
