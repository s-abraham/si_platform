﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using Microsoft.Data.Extensions;
using SI.DAL;
using SI.Core;
using System.Text;
using System.Data.SqlClient;
using System.Data.Objects;
using System.Threading.Tasks;
using Connectors.Entities;
using SI.BLL.Interface;
using SI.DTO;
using System.Data;

namespace SI.BLL.Implementation
{
    public class RSSProvider : IRSSProvider
    {
        public bool SaveRSSData(RSSEntities rssEntities)
        {
            StringBuilder sb = new StringBuilder();

            sb.AppendLine(" set nocount on create table #t (id bigint not null) ");
            sb.AppendLine(" declare @idOut bigint ");

            foreach (var item in rssEntities.RSSItems)
            {
                sb.AppendFormat(" insert #t (id) exec spSaveRSSData @FeedID={0}", rssEntities.RSSFeedID);
                sb.AppendFormat(", @Title={0}", StringUtils.PrepStringForSQL(item.Title));
                sb.AppendFormat(", @Link={0}", StringUtils.PrepStringForSQL(item.Link));
                sb.AppendFormat(", @Permalink={0}", string.IsNullOrWhiteSpace(item.PermaLink) ? "null" : StringUtils.PrepStringForSQL(item.PermaLink));
                sb.AppendFormat(", @Summary={0}", string.IsNullOrWhiteSpace(item.Summary) ? "null" : StringUtils.PrepStringForSQL(item.Summary));
                sb.AppendFormat(", @SummaryAsText={0}", string.IsNullOrWhiteSpace(item.SummaryAsText) ? "null" : StringUtils.PrepStringForSQL(item.SummaryAsText));
                sb.AppendFormat(", @PubDate='{0}'", item.PubDate.ToString("MM-dd-yyyy"));
                sb.AppendFormat(", @MainImage={0}", item.Images != null && item.Images.Any() && !string.IsNullOrWhiteSpace(item.Images[0]) ? StringUtils.PrepStringForSQL(item.Images[0]) : "null");
                sb.AppendFormat(", @Thumbnail={0}", string.IsNullOrWhiteSpace(item.Thumbnail) ? "null" : StringUtils.PrepStringForSQL(item.Thumbnail));
                sb.AppendFormat(", @CategoryList={0}", StringUtils.PrepStringForSQL(String.Join("|", item.Categories)));
                sb.AppendFormat(", @Hash={0}", StringUtils.PrepStringForSQL(item.Hash));
                sb.Append(", @IDOutput=@idOut OUTPUT \r\n");

            }

            sb.AppendLine(" select id from #t ");
            sb.AppendLine(" drop table #t ");

            using (MainEntities db = ContextFactory.Main)
            {
                //string sql = sb.ToString();
                List<long> idList = db.ExecuteStoreQuery<long>(sb.ToString()).ToList();

                if (rssEntities.RSSItems.Count == idList.Count)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }

        }

        private List<RSSDownloaderData> getFeedsToProcess(int daysBack, int maxItems, bool downloadImages)
        {
            var results = new List<RSSDownloaderData>();
            TimeZoneInfo timeZone = TimeZoneInfo.Utc;

            using (MainEntities db = ContextFactory.Main)
            {
                if (db.Connection.State != System.Data.ConnectionState.Open)
                    db.Connection.Open();

                using (DbCommand cmd = db.CreateStoreCommand(
                    "spGetRSSFeedsToProcess",
                     System.Data.CommandType.StoredProcedure
                ))
                {
                    using (DbDataReader reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            results.Add(new RSSDownloaderData()
                            {
                                MaxItems = maxItems,
                                DaysBack = daysBack,
                                RSSFeedID = Util.GetReaderValue<long>(reader, "ID", 0),
                                DownloadImages = downloadImages
                            });
                        }
                    }
                }
            }


            return results;
        }

        public void QueueRSSDownloads(int spreadAcrossMinutes, int daysBack, int maxItems, bool downloadImages)
        {
            List<RSSDownloaderData> list = getFeedsToProcess(daysBack, maxItems, downloadImages);

            DateTime startDate = DateTime.UtcNow;
            DateTime endDate = startDate.AddMinutes(spreadAcrossMinutes);
            double msBetweenJobs = Math.Abs(((endDate - startDate).TotalMilliseconds)) / list.Count();

            List<JobDTO> jobsToQueue = new List<JobDTO>();
            foreach (RSSDownloaderData item in list)
            {
                JobDTO job = new JobDTO()
                {
                    DateScheduled = SIDateTime.CreateSIDateTime(startDate, TimeZoneInfo.Utc),
                    JobDataObject = item,
                    JobType = item.JobType,
                    JobStatus = JobStatusEnum.Queued,
                    Priority = 200
                };
                jobsToQueue.Add(job);

                if (jobsToQueue.Count() > 100)
                {
                    ProviderFactory.Jobs.Save(new SaveEntityDTO<List<JobDTO>>(jobsToQueue, UserInfoDTO.System));
                    jobsToQueue.Clear();
                }

                startDate = startDate.AddMilliseconds(msBetweenJobs);
            }

            if (jobsToQueue.Any())
            {
                ProviderFactory.Jobs.Save(new SaveEntityDTO<List<JobDTO>>(jobsToQueue, UserInfoDTO.System));
                jobsToQueue.Clear();
            }
        }

        public RSSFeedDTO GetRSSFeed(long rssFeedID)
        {
            using (MainEntities db = ContextFactory.Main)
            {
                RSSFeed feed = (from f in db.RSSFeeds where f.ID == rssFeedID select f).FirstOrDefault();
                if (feed == null) return null;
                return new RSSFeedDTO()
                {
                    ID = feed.ID,
                    VerticalID = feed.VerticalID,
                    IsActive = feed.IsActive,
                    IsValid = feed.IsValid,
                    URL = feed.URL,
                    LastAttempted = SIDateTime.CreateSIDateTime(feed.LastAttempted, TimeZoneInfo.Utc),
                    LastSuccessful = SIDateTime.CreateSIDateTime(feed.LastSuccessful, TimeZoneInfo.Utc)
                };
            }
        }

        public SaveEntityDTO<RSSFeedDTO> Save(SaveEntityDTO<RSSFeedDTO> dto)
        {
            RSSFeedDTO fdto = dto.Entity;
            using (MainEntities db = ContextFactory.Main)
            {
                RSSFeed feed = null;
                if (fdto.ID.HasValue)
                {
                    feed = (from f in db.RSSFeeds where f.ID == fdto.ID.Value select feed).FirstOrDefault();
                }
                else
                {
                    feed = new RSSFeed();
                }

                feed.IsActive = fdto.IsActive;
                feed.IsValid = fdto.IsValid;
                feed.URL = fdto.URL;
                feed.LastAttempted = fdto.LastAttempted != null ? (DateTime?)fdto.LastAttempted.GetUTCDate(TimeZoneInfo.Utc) : null;
                feed.LastSuccessful = fdto.LastSuccessful != null ? (DateTime?)fdto.LastSuccessful.GetUTCDate(TimeZoneInfo.Utc) : null;
                feed.VerticalID = fdto.VerticalID;

                if (feed.EntityState == System.Data.EntityState.Detached)
                    db.RSSFeeds.AddObject(feed);

                db.SaveChanges();

                return dto;
            }
        }

        public RSSSearchResultDTO SearchRSSItems(UserInfoDTO userInfo, RSSSearchRequestDTO req)
        {
            RSSSearchResultDTO result = new RSSSearchResultDTO();
            result.SortAscending = req.SortAscending;
            result.SortBy = req.SortBy;
            result.StartIndex = req.StartIndex;
            result.EndIndex = req.EndIndex;

            TimeZoneInfo timeZone = ProviderFactory.TimeZones.GetTimezoneForUserID(userInfo.EffectiveUser.ID.Value);

            SearchStringParser parser = new SearchStringParser(req.SearchText);

			if (!string.IsNullOrEmpty(req.Franchise))
		        parser.IncludePhrases.Add(req.Franchise);

            using (MainEntities db = ContextFactory.Main)
            {
                if (db.Connection.State != ConnectionState.Open)
                    db.Connection.Open();

	            SqlParameter MinPubDate = new SqlParameter("@MinPubDate", SqlDbType.DateTime);
				if (req.MinPubDate != null)
		            MinPubDate.Value = req.MinPubDate.GetUTCDate(timeZone);
				SqlParameter MaxPubDate = new SqlParameter("@MaxPubDate", SqlDbType.DateTime);
				if (req.MaxPubDate != null)
					MaxPubDate.Value = req.MaxPubDate.GetUTCDate(timeZone);
				SqlParameter MinCreateDate = new SqlParameter("@MinCreateDate", SqlDbType.DateTime);
				if (req.MinCreateDate != null)
					MinCreateDate.Value = req.MinCreateDate.GetUTCDate(timeZone);
				SqlParameter MaxCreateDate = new SqlParameter("@MaxCreateDate", SqlDbType.DateTime);
				if (req.MaxCreateDate != null)
					MaxCreateDate.Value = req.MaxCreateDate.GetUTCDate(timeZone);

                using (DbCommand cmd = db.CreateStoreCommand("spSearchRSSItems2", CommandType.StoredProcedure,
                    new SqlParameter("@SortBy", req.SortBy),
                    new SqlParameter("@SortAscending", req.SortAscending),
                    new SqlParameter("@StartIndex", req.StartIndex),
                    new SqlParameter("@EndIndex", req.EndIndex),
                    new SqlParameter("@RSSItemID", req.RSSItemID),
                    new SqlParameter("@Include", string.Join("|", parser.IncludePhrases.ToArray())),
                    new SqlParameter("@Exclude", string.Join("|", parser.ExcludePhrases.ToArray())),
					MinPubDate,
					MaxPubDate,
					MinCreateDate,
					MaxCreateDate
                ))
                {
                    using (DbDataReader reader = cmd.ExecuteReader())
                    {
                        if (reader.Read())
                        {
                            result.TotalRecords = Util.GetReaderValue<int>(reader, "Total", 0);
                        }
                        if (reader.NextResult())
                        {
                            while (reader.Read())
                            {
                                result.Items.Add(new RSSItemDTO()
                                {
                                    DateCreated = new SIDateTime(Util.GetReaderValue<DateTime>(reader, "DateCreated", DateTime.Now), timeZone),
                                    PubDate = new SIDateTime(Util.GetReaderValue<DateTime>(reader, "PubDate", DateTime.Now), timeZone),
                                    RSSFeedID = Util.GetReaderValue<long>(reader, "RSSFeedID", 0),
                                    ID = Util.GetReaderValue<long>(reader, "ID", 0),
                                    Title = Util.GetReaderValue<string>(reader, "Title", ""),
                                    Link = Util.GetReaderValue<string>(reader, "Link", ""),
                                    Summary = Util.GetReaderValue<string>(reader, "Summary", ""),
                                    SummaryAsText = Util.GetReaderValue<string>(reader, "SummaryAsText", ""),
                                    MainImage = Util.GetReaderValue<string>(reader, "MainImage", "")
                                });
                            }
                        }
                    }
                }
            }

            return result;
        }

    }
}
