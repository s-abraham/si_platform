﻿using System.Globalization;
using Connectors;
using Connectors.Parameters;
using SI.DAL;
using SI.DTO;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Connectors.Reviews.Parameters;
using Connectors.Reviews;
using System.Data.Objects;
using SI.Core;
using Microsoft.Data.Extensions;
using System.Data.Common;
using Notification;
using Newtonsoft.Json;
using SI.Extensions;
using System.Reflection;

namespace SI.BLL.Implementation
{
    internal class PlatformNotification : IPlatformNotification
    {
        #region User

        public void SendNewUserEmail(long userID, UserInfoDTO userInfo)
        {
            try
            {
                //get the template   
                string template = @"<html><body><table width=""75%""><tr><td valign=""top""><table cellpadding=""0"" cellspacing=""0""><tr><td style=""font-weight: bold; padding: 0 0 10px 10px"">Account Name:</td><td style=""font-weight: normal; padding: 0 0 10px 10px"">[$AccountName$]</td></tr><tr><td style=""font-weight: bold; padding: 0 0 10px 10px"">Name:</td><td style=""font-weight: normal; padding: 0 0 10px 10px"">[$UserName$]</td></tr><tr><td style=""font-weight: bold; padding: 0 0 10px 10px"">Email Address:</td><td style=""font-weight: normal; padding: 0 0 10px 10px"">[$Email$]</td></tr><tr><td style=""font-weight: bold; padding: 0 0 10px 10px"">Role:</td><td style=""font-weight: normal; padding: 0 0 10px 10px"">[$RoleName$]</td></tr></table></td></tr></table></body></html>";

                UserDTO userDTO = ProviderFactory.Security.GetUserByID(userID, UserInfoDTO.System);
                GetEntityDTO<AccountDTO, AccountOptionsDTO> AccountResult = ProviderFactory.Security.GetAccountByID(userInfo, userDTO.MemberOfAccountID);

                if (userDTO != null)
                {
                    string Subject = "New User Registration For " + AccountResult.Entity.DisplayName;
                    string emailBody = string.Empty;

                    emailBody = template.Replace("[$UserName$]", userDTO.FirstName + " " + userDTO.LastName)
                                        .Replace("[$Email$]", userDTO.EmailAddress)
                                        .Replace("[$RoleName$]", userDTO.RoleTypeName)
                                        .Replace("[$AccountName$]", AccountResult.Entity.DisplayName);

                    long NotificationTargetID = SaveNotificationTarget(userDTO.EmailAddress, Subject, emailBody);

                    bool sendResult = false;
                    if (NotificationTargetID != 0)
                    {
                        sendResult = SendEmail("New User Registration", Subject, emailBody, userDTO.EmailAddress, null, null);
                    }
                    if (sendResult == true)
                    {
                        updateNotificationTarget(NotificationTargetID, NotificationStatusesEnum.Success);
                    }
                    else
                    {
                        updateNotificationTarget(NotificationTargetID, NotificationStatusesEnum.Failure);
                    }
                }
            }
            catch (Exception ex)
            {
                ProviderFactory.Logging.LogException(ex);
            }

        }

        #endregion

        #region Enrollment Welcome Email
        public void SendEnrollmentWelcomeEmail(Guid accessToken)
        {
            try
            {
                SyndicationInviteAccessTokenDTO accessTokenBaseDto = (SyndicationInviteAccessTokenDTO)ProviderFactory.Security.GetAccessToken(accessToken, true);
                if (accessTokenBaseDto != null && accessTokenBaseDto.Type() == AccessTokenTypeEnum.SyndicationInvitation)
                {
                    using (MainEntities db = ContextFactory.Main)
                    {
                        //Get AccountId By SyndicatorID
                        long SyndicatorAccountID = (from s in db.Syndicators where s.ID == accessTokenBaseDto.SyndicatorID select s.AccountID).FirstOrDefault();

                        //Get ResellerID by Syndicator AccountID
                        long ResellerID = (from r in db.Resellers where r.AccountID == SyndicatorAccountID select r.ID).FirstOrDefault();

                        //Get User
                        UserDTO userDTO = ProviderFactory.Security.GetUserByID(accessTokenBaseDto.UserID, UserInfoDTO.System);

                        if (userDTO != null)
                        {
                            //Get the Template by ResellerID and NotificationMessageTypeID (NotificationMessageTypeID: 13)
                            NotificationTemplate template = (from t in db.NotificationTemplates
                                                             where t.NotificationMessageTypeID == 13 && t.ResellerID == ResellerID
                                                             select t).FirstOrDefault();

                            if (template != null)
                            {
                                #region HtmlEmail

                                AccessTokenBaseDTO accessTokenForEmail = new PasswordResetAccessTokenDTO("");
                                accessTokenForEmail.TimeTillExpiration = new TimeSpan(30, 0, 0, 0); //reset good for 30 days
                                accessTokenForEmail.UserID = userDTO.ID.Value;

                                AccessTokenBaseDTO token = ProviderFactory.Security.CreateAccessToken(accessTokenForEmail);
                                if (token != null)
                                {
                                    string callback = string.Format("{0}/access", Settings.GetSetting("site.baseurl", "socialintegration.com"));
                                    string ChangePasswordLink = string.Format("{0}/{1}", callback, token.Token.ToString());

                                    string UserName = userDTO.Username;
                                    WelcomeEmailTemplateDTO TemplateDTO = JsonConvert.DeserializeObject<WelcomeEmailTemplateDTO>(template.Body);

                                    string EmailBody = TemplateDTO.Body;
                                    EmailBody = EmailBody.Replace("[$UserName$]", UserName);
                                    EmailBody = EmailBody.Replace("[$ChangePasswordLink$]", ChangePasswordLink);

                                    string Subject = template.Subject;

                                    long NotificationTargetID = SaveNotificationTarget(userDTO.EmailAddress, Subject, EmailBody);

                                    bool sendResult = false;
                                    if (NotificationTargetID != 0)
                                    {
                                        sendResult = SendEmail("Enrollment Welcome Email", Subject, EmailBody, userDTO.EmailAddress, null, null);
                                    }

                                    if (sendResult == true)
                                    {
                                        updateNotificationTarget(NotificationTargetID, NotificationStatusesEnum.Success);
                                    }
                                    else
                                    {
                                        updateNotificationTarget(NotificationTargetID, NotificationStatusesEnum.Failure);
                                    }
                                }
                                #endregion
                            }

                        }
                    }
                }
            }
            catch (Exception ex)
            {
                ProviderFactory.Logging.LogException(ex);
            }
                
        }

        #endregion

        #region Review Diagnostics

        public void SendReviewDiagnosticsEmail()
        {
            try
            {
                //get the template
                string template = Util.GetResourceTextFile("RepDiag.html", Assembly.GetAssembly(typeof(ProviderFactory)));

                using (MainEntities db = ContextFactory.Main)
                {
                    if (db.Connection.State != System.Data.ConnectionState.Open)
                        db.Connection.Open();
                    using (DbCommand cmd = db.CreateStoreCommand(
                        "spReputationDiagnostics",
                         System.Data.CommandType.StoredProcedure
                    ))
                    {
                        using (DbDataReader reader = cmd.ExecuteReader())
                        {
                            List<JobStatusListItem> rdls = db.Translate<JobStatusListItem>(reader).ToList();
                            reader.NextResult();
                            List<JobStatusListItem> rint = db.Translate<JobStatusListItem>(reader).ToList();

                            int dlCountFailed = (from r in rdls where r.JobStatusID == 50 select r.Count).FirstOrDefault();
                            int dlCountSucceeded = (from r in rdls where r.JobStatusID == 40 select r.Count).FirstOrDefault();
                            int intCountFailed = (from r in rint where r.JobStatusID == 50 select r.Count).FirstOrDefault();
                            int intCountSucceeded = (from r in rint where r.JobStatusID == 40 select r.Count).FirstOrDefault();

                            template = template.Replace("[d_failed]", dlCountFailed.ToString(CultureInfo.InvariantCulture));
                            template = template.Replace("[d_succeeded]", dlCountSucceeded.ToString(CultureInfo.InvariantCulture));
                            template = template.Replace("[i_failed]", intCountFailed.ToString(CultureInfo.InvariantCulture));
                            template = template.Replace("[i_succeeded]", intCountSucceeded.ToString(CultureInfo.InvariantCulture));

                            int d_percent = Convert.ToInt32((Convert.ToSingle(dlCountFailed) / (Convert.ToSingle(dlCountFailed) + Convert.ToSingle(dlCountSucceeded))) * 100);
                            template = template.Replace("[d_percent]", d_percent.ToString(CultureInfo.InvariantCulture));
                            int i_percent = Convert.ToInt32((Convert.ToSingle(intCountFailed) / (Convert.ToSingle(intCountFailed) + Convert.ToSingle(intCountSucceeded))) * 100);
                            template = template.Replace("[i_percent]", i_percent.ToString(CultureInfo.InvariantCulture));

                            StringBuilder sb = new StringBuilder();
                            reader.NextResult();

                            while (reader.Read())
                            {
                                Single rs_missing = Convert.ToSingle(Util.GetReaderValue<long>(reader, "CountMissing", 0));
                                Single rs_total = Convert.ToSingle(Util.GetReaderValue<long>(reader, "Count", 0));
                                int res_percent = Convert.ToInt32((rs_missing / rs_total) * 100);

                                sb.Append("<tr>");
                                sb.AppendFormat("<td>{0}</td>", Util.GetReaderValue<long>(reader, "ReviewSourceID", 0));
                                sb.AppendFormat("<td>{0}</td>", Util.GetReaderValue<string>(reader, "ReviewSource", ""));
                                sb.AppendFormat("<td>{0}</td>", rs_missing);
                                sb.AppendFormat("<td>{0}</td>", rs_total);
                                sb.AppendFormat("<td><b>{0}</b></td>", res_percent);
                                sb.Append("</tr>");
                            }
                            template = template.Replace("[12hmissing]", sb.ToString());

                            reader.NextResult();
                            sb.Clear();

                            while (reader.Read())
                            {
                                sb.Append("<tr>");
                                sb.AppendFormat("<td>{0}</td>", Util.GetReaderValue<string>(reader, "ReviewSource", ""));
                                sb.AppendFormat("<td>{0}</td>", Util.GetReaderValue<DateTime>(reader, "ReviewDate", DateTime.MinValue).ToString("MM/dd/yyyy"));
                                sb.Append("</tr>");
                            }
                            template = template.Replace("[ByReviewDate]", sb.ToString());


                        }
                    }
                }

                long NotificationTargetID  = SaveNotificationTarget("reynolds@autostartups.com", "Reputation System Diagnostics", template);
                bool sendResult = false;    
                if (NotificationTargetID != 0)
                {
                    sendResult = SendEmail("Diagnostics", "Reputation System Diagnostics", template, "reynolds@autostartups.com", null, null);
                }
                if (sendResult == true)
                {
                    updateNotificationTarget(NotificationTargetID, NotificationStatusesEnum.Success);
                }
                else
                {
                    updateNotificationTarget(NotificationTargetID, NotificationStatusesEnum.Failure);
                }


                NotificationTargetID = SaveNotificationTarget("sanju@autostartups.com", "Reputation System Diagnostics", template);
                if (NotificationTargetID != 0)
                {
                    sendResult = SendEmail("Diagnostics", "Reputation System Diagnostics", template, "sanju@autostartups.com", null, null);
                }
                if (sendResult == true)
                {
                    updateNotificationTarget(NotificationTargetID, NotificationStatusesEnum.Success);
                }
                else
                {
                    updateNotificationTarget(NotificationTargetID, NotificationStatusesEnum.Failure);
                }


                NotificationTargetID = SaveNotificationTarget("a.patel@socialdealer.com", "Reputation System Diagnostics", template);
                if (NotificationTargetID != 0)
                {
                    sendResult = SendEmail("Diagnostics", "Reputation System Diagnostics", template, "a.patel@socialdealer.com", null, null);
                }
                if (sendResult == true)
                {
                    updateNotificationTarget(NotificationTargetID, NotificationStatusesEnum.Success);
                }
                else
                {
                    updateNotificationTarget(NotificationTargetID, NotificationStatusesEnum.Failure);
                }

				//NotificationTargetID = SaveNotificationTarget("dave@autostartups.com", "Reputation System Diagnostics", template);
				//if (NotificationTargetID != 0)
				//{
				//	sendResult = SendEmail("Diagnostics", "Reputation System Diagnostics", template, "dave@autostartups.com", null, null);
				//}
				//if (sendResult == true)
				//{
				//	updateNotificationTarget(NotificationTargetID, NotificationStatusesEnum.Success);
				//}
				//else
				//{
				//	updateNotificationTarget(NotificationTargetID, NotificationStatusesEnum.Failure);
				//}
            }
            catch (Exception ex)
            {
                ProviderFactory.Logging.LogException(ex);
            }
        }

        private class JobStatusListItem
        {
            public long JobStatusID { get; set; }
            public string Status { get; set; }
            public int Count { get; set; }
        }

        #endregion

        #region Syndication Signup Email

        public void SendSyndicationSignupEmail(Guid accessToken, SocialNetworkEnum socialNetwork)
        {
            try
            {
                SyndicationInviteAccessTokenDTO accessTokenBaseDto = (SyndicationInviteAccessTokenDTO)ProviderFactory.Security.GetAccessToken(accessToken, true);
                if (accessTokenBaseDto != null && accessTokenBaseDto.Type() == AccessTokenTypeEnum.SyndicationInvitation)
                {

                    using (MainEntities db = ContextFactory.Main)
                    {
                        //Get AccountId By SyndicatorID
                        long SyndicatorAccountID = (from s in db.Syndicators where s.ID == accessTokenBaseDto.SyndicatorID select s.AccountID).FirstOrDefault();

                        //Get ResellerID by AccountID
                        long ResellerID = (from r in db.Resellers where r.AccountID == SyndicatorAccountID select r.ID).FirstOrDefault();

                        //Get User
                        UserDTO userDTO = ProviderFactory.Security.GetUserByID(accessTokenBaseDto.UserID, UserInfoDTO.System);

                        //Get Account
                        GetEntityDTO<AccountDTO, AccountOptionsDTO> accountDto = ProviderFactory.Security.GetAccountByID(UserInfoDTO.System, accessTokenBaseDto.AccountID);

                        //Get Credential
                        SocialCredentialDTO socialCredentialDTO = ProviderFactory.Social.GetSocialCredentials(socialNetwork, accessTokenBaseDto.AccountID, false).FirstOrDefault();

                        if (socialCredentialDTO != null)
                        {
                            //Get the Template by ResellerID and NotificationMessageTypeID
                            NotificationTemplate template = (from t in db.NotificationTemplates
                                                             where t.NotificationMessageTypeID == 11 && t.ResellerID == ResellerID
                                                             select t).FirstOrDefault();

                            if (template != null)
                            {
                                #region HtmlEmail
                                string SocialNetWorkName = socialNetwork.ToString();
                                string UserName = userDTO.FirstName + " " + userDTO.LastName;
                                string AccountName = (accountDto.Entity.DisplayName == null) ? accountDto.Entity.Name : accountDto.Entity.DisplayName;
                                string CityName = accountDto.Entity.CityName;
                                string StateName = accountDto.Entity.StateName;
                                string ZipCode = accountDto.Entity.PostalCode;
                                string Address = accountDto.Entity.StreetAddress1;
                                string ScreenName = socialCredentialDTO.ScreenName;
                                string PageURL = socialCredentialDTO.PageUrl;

                                SyndicationSignupDTO TemplateDTO = JsonConvert.DeserializeObject<SyndicationSignupDTO>(template.Body);

                                string EmailBody = TemplateDTO.body;

                                EmailBody = EmailBody.Replace("[$SocialNetWorkName$]", SocialNetWorkName)
                                                     .Replace("[$UserName$]", UserName)
                                                     .Replace("[$AccountName$]", AccountName)
                                                     .Replace("[$ScreenName$]", ScreenName)
                                                     .Replace("[$PageURL$]", PageURL)
                                                     .Replace("[$Address$]", Address)
                                                     .Replace("[$CityName$]", CityName)
                                                     .Replace("[$StateName$]", StateName)
                                                     .Replace("[$ZipCode$]", ZipCode);

                                string Subject = AccountName + " has registered " + SocialNetWorkName + " Page for " + template.Subject;

                                //Get List of the Users who should receive Sign Up Email for Reseller
                                foreach (var user in db.spGetNotificationSubscriptionByTemplateID(template.ID))
                                {
                                    try
                                    {
                                        //UserDTO userDTOforEmail = ProviderFactory.Security.GetUserByID(user.EmailAddress.ID, UserInfoDTO.System);

                                        long NotificationTargetID = SaveNotificationTarget(user.EmailAddress, Subject, EmailBody);

                                        bool sendResult = false;
                                        if (NotificationTargetID != 0)
                                        {
                                            sendResult = SendEmail("Syndication Signup", Subject, EmailBody, user.EmailAddress, null, null);
                                        }

                                        if (sendResult == true)
                                        {
                                            updateNotificationTarget(NotificationTargetID, NotificationStatusesEnum.Success);
                                        }
                                        else
                                        {
                                            updateNotificationTarget(NotificationTargetID, NotificationStatusesEnum.Failure);
                                        }
                                    }
                                    catch (Exception ex)
                                    {
                                        ProviderFactory.Logging.LogException(ex);
                                    }
                                }
                                #endregion
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                ProviderFactory.Logging.LogException(ex);
            }
        }

        #endregion

        #region SentTokenValidationEmail

        public void SentTokenValidationEmail(SocialCredentialDTO socialCredentialsDTO)
        {
            try
            {
                if (socialCredentialsDTO != null)
                {
                    if (socialCredentialsDTO.CreatedByUserID.GetValueOrDefault() != 1)
                    {
                        using (MainEntities db = ContextFactory.Main)
                        {
                            //Get User
                            UserDTO userDTO = ProviderFactory.Security.GetUserByID(socialCredentialsDTO.CreatedByUserID.GetValueOrDefault(), UserInfoDTO.System);

                            //Get Account
                            GetEntityDTO<AccountDTO, AccountOptionsDTO> accountDto = ProviderFactory.Security.GetAccountByID(UserInfoDTO.System, socialCredentialsDTO.AccountID);

                            //Get the Template by ResellerID and NotificationMessageTypeID
                            NotificationTemplate template = (from t in db.NotificationTemplates
                                                             where t.ID == 72 
                                                             select t).FirstOrDefault();

                            if (template != null)
                            {
                                #region HtmlEmail
                                ClientFaceingTokenValidationDTO TemplateDTO = JsonConvert.DeserializeObject<ClientFaceingTokenValidationDTO>(template.Body);

                                string EmailBody = TemplateDTO.Body;
                                string Subject = TemplateDTO.Subject;
                                string ReConnectURL = string.Empty;

                                SocialProductConfigAccessTokenDTO tokenRequest = new SocialProductConfigAccessTokenDTO()
                                {
                                    UserID = userDTO.ID.Value,
                                    AccountID = socialCredentialsDTO.AccountID,

                                    RemainingUses = 100,
                                    TimeTillExpiration = new TimeSpan(1, 0, 0, 0)
                                };
                                AccessTokenBaseDTO accessToken = ProviderFactory.Security.CreateAccessToken(tokenRequest);
                                                               

                                string callback = string.Format("{0}/access", Settings.GetSetting("site.baseurl", "socialintegration.com"));
                                ReConnectURL = string.Format("{0}/{1}", callback, accessToken.Token.ToString());

                                EmailBody = EmailBody.Replace("[$UserName$]", string.Format("{0} {1}", userDTO.FirstName, userDTO.LastName))
                                                    .Replace("[$SocialNetworkName$]", socialCredentialsDTO.SocialNetworkName)
                                                    .Replace("[$PageName$]", socialCredentialsDTO.ScreenName)
                                                    .Replace("[$PageURL$]", "http://platform.socialintegration.com/")
                                                    .Replace("[$ReConnectURL$]", ReConnectURL);

                                long NotificationTargetID = SaveNotificationTarget(userDTO.EmailAddress, Subject, EmailBody);

                                bool sendResult = false;
                                if (NotificationTargetID != 0)
                                {
                                    sendResult = SendEmail("Token Validation Email", Subject, EmailBody, userDTO.EmailAddress, null, null);
                                }

                                if (sendResult == true)
                                {
                                    updateNotificationTarget(NotificationTargetID, NotificationStatusesEnum.Success);
                                }
                                else
                                {
                                    updateNotificationTarget(NotificationTargetID, NotificationStatusesEnum.Failure);
                                }
                                #endregion
                            }
                            
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                ProviderFactory.Logging.LogException(ex);
            }
        }

        #endregion

        #region Review Email Share

        public bool SendReviewShareEmail(ReviewShareEmailDTO reviewShareEmail, long userID)
		{
			bool success = false;

			try
			{
				using (MainEntities db = ContextFactory.Main)
				{
					//Get User
					UserDTO userDTO = ProviderFactory.Security.GetUserByID(userID, UserInfoDTO.System);
					//UserDTO ToEmailUserDTO = ProviderFactory.Security.GetUserByID(reviewShareEmail.UserToEmailID, UserInfoDTO.System);

					//Get Review
					ReviewDTO reviewDto = ProviderFactory.Reviews.GetReviewByID(reviewShareEmail.ReviewID);

                    if (reviewDto != null && !string.IsNullOrEmpty(reviewShareEmail.ToEmailID))
					{
						//Get the Template by ResellerID and NotificationMessageTypeID
						NotificationTemplate template = db.NotificationTemplates.Where(n => n.NotificationMessageTypeID == 18).FirstOrDefault();

						if (template != null)
						{
							string siteImageUrl = "";
							string siteName = "";
							ReviewSource reviewSource = db.ReviewSources.Where(r => r.ID == (long) reviewDto.ReviewSource).FirstOrDefault();
							if (reviewSource != null)
							{
								siteImageUrl = reviewSource.ImagePath.IsNullOptional("");
								siteName = reviewSource.Name.IsNullOptional("");
							}

							string reviewSiteUrl = "";
							Accounts_ReviewSources accountsReviewSources = db.Accounts_ReviewSources.Where(a => a.AccountID == reviewDto.AccountID && a.ReviewSourceID == (long) reviewDto.ReviewSource && a.IsValid).FirstOrDefault();
							if (accountsReviewSources != null)
								reviewSiteUrl = accountsReviewSources.URL.IsNullOptional("");

							#region HtmlEmail

							ReviewShareEmailTemplateDTO TemplateDTO = JsonConvert.DeserializeObject<ReviewShareEmailTemplateDTO>(template.Body);

							string EmailBody = TemplateDTO.Body;

							EmailBody = EmailBody.Replace("[$PlatformURL$]", string.Format("{0}/Reputation/ReviewStream", Settings.GetSetting("site.baseurl", "socialintegration.com")))
							                     .Replace("[$ReviewSiteURL$]", reviewSiteUrl)
							                     .Replace("[%Domain%]", Settings.GetSetting("site.baseurl", "socialintegration.com"))
							                     .Replace("[$Reviewer$]", reviewDto.ReviewerName.IsNullOptional(""))
							                     .Replace("[$Rating$]", reviewDto.Rating.ToString("n1"))
							                     .Replace("[$PostorNeg$]", "") //what is the logic here??
							                     .Replace("[$ReviewSiteIconURL$]", siteImageUrl)
							                     .Replace("[$ReviewDate$]", reviewDto.ReviewDate.LocalDate.GetValueOrDefault().ToShortDateString())
							                     .Replace("[$ReviewSiteName$]", siteName)
							                     .Replace("[$Comment$]", reviewShareEmail.Comments.IsNullOptional(""));

							if (userDTO != null)
								EmailBody = EmailBody.Replace("[$SharedBy$]", string.Format("{0} {1}, {2}", userDTO.FirstName, userDTO.LastName, userDTO.EmailAddress));

							string Subject = string.IsNullOrEmpty(reviewShareEmail.Subject) ? template.Subject : reviewShareEmail.Subject;

							try
							{
                                long NotificationTargetID = SaveNotificationTarget(reviewShareEmail.ToEmailID, Subject, EmailBody);

								bool sendResult = false;
								if (NotificationTargetID != 0)
								{
                                    sendResult = SendEmail("Review Share", Subject, EmailBody, reviewShareEmail.ToEmailID, null, null);
								}

								if (sendResult == true)
								{
									updateNotificationTarget(NotificationTargetID, NotificationStatusesEnum.Success);
									success = true;
								}
								else
								{
									updateNotificationTarget(NotificationTargetID, NotificationStatusesEnum.Failure);
								}
							}
							catch (Exception ex)
							{
								ProviderFactory.Logging.LogException(ex);
							}

							#endregion
						}
					}
				}
			}

			catch (Exception ex)
			{
				ProviderFactory.Logging.LogException(ex);
			}

			return success;
		}

		#endregion

        #region Log Email Notification to DB

        private long SaveNotificationTarget(string Address, string Subject, string EmailBody)
        {
            //Insert Into NotificationTarget
            NotificationTarget notificationTarget = new NotificationTarget();

            notificationTarget.NotificationMethodID = 10; //SELECT * FROM NotificationMethods  // HTML
            notificationTarget.Address = Address;
            notificationTarget.Subject = Subject;
            notificationTarget.Body = EmailBody;
            notificationTarget.NotificationStatusID = Convert.ToInt64(NotificationStatusesEnum.Pending);

            long NotificationTargetID = addNotificationTarget(notificationTarget);

            return NotificationTargetID;
        }

        private long addNotificationTarget(NotificationTarget notificationTarget)
        {
            long result = 0;

            using (MainEntities db = ContextFactory.Main)
            {
                DAL.NotificationTarget nt = new DAL.NotificationTarget();

                nt.NotificationMethodID = notificationTarget.NotificationMethodID;
                nt.Address = notificationTarget.Address;
                nt.Subject = notificationTarget.Subject;
                nt.Body = notificationTarget.Body;
                nt.NotificationStatusID = notificationTarget.NotificationStatusID;
                nt.DateSent = null;

                db.NotificationTargets.AddObject(nt);

                db.SaveChanges();

                result = nt.ID;
            }

            return result;

        }

        private void updateNotificationTarget(long NotificationTargetID, NotificationStatusesEnum NotificationStatusID)
        {
            using (MainEntities db = ContextFactory.Main)
            {
                db.spUpdateNotificationTarget(NotificationTargetID, DateTime.UtcNow, (long)NotificationStatusID);
            }
        }

        #endregion

        #region Send Email 

        private bool SendEmail(string Category, string Subject, string Body, string SendTo, string SendCC, string SendBCC)
        {
            EmailManager emailManager = new EmailManager();
            bool sendResult = emailManager.sendNotification(
                Category,
                Subject,
                Body,
                Settings.GetSetting("site.SendFromEmailAddress", "notifications@socialdealer.com"),
                SendTo,
                null,
                null
            );

            return sendResult;
        }
        #endregion

        /*
        private string SendFrom = "notifications@socialdealer.com";

        #region User
        public bool NewUserNotification(UserInfoDTO userInfo, long userID)
        {
            bool result = false;

            UserDTO userDTO = ProviderFactory.Security.GetUserByID(userID, userInfo);
            GetEntityDTO<AccountDTO, AccountOptionsDTO> AccountResult = ProviderFactory.Security.GetAccountByID(userInfo, userDTO.MemberOfAccountID);
            
            int MessageTypeID = 18; // [MessageTypeId] from [dbo].[MessageType]
            int MessageCategoryID = 3;

            MessageTypeDTO messageTypeDTO = ProviderFactory.NotificationDB.GetMessageTemplateByID(MessageTypeID);

            if (messageTypeDTO != null && userDTO != null)
            {
                string MessageSubject = "New User Registration For " + AccountResult.Entity.DisplayName;

                string emailBody = GenerateNewUserEmailBody(messageTypeDTO, userDTO, AccountResult.Entity);

                //Save Email To SI.Notification DB
                SI.DAL.Notification notification = new SI.DAL.Notification();
                NotificationTransmission notificationTransmission = new NotificationTransmission();

                SendToDTO sendToDTO = new SendToDTO();
                sendToDTO.sendTo.Add(userDTO.EmailAddress);

                notification.ProcessedDate = DateTime.UtcNow;
                notification.MessageTypeId = MessageTypeID; 
                notification.CreatedDate = DateTime.UtcNow;
                notification.AccountID = Convert.ToInt32(userDTO.MemberOfAccountID);
                notification.MessageCategoryId = MessageCategoryID;
                notification.VirtualGroupID = 0;
                notification.Body = emailBody;
                notification.Subject = MessageSubject;

                notificationTransmission.TransmissionTypeId = 1; //Email from SI.Notification.[lookups].[TransmissionType]
                notificationTransmission.DateUpdated = DateTime.UtcNow;
                notificationTransmission.ScheduledDate = DateTime.UtcNow;
                notificationTransmission.StatusId = Convert.ToInt32(NotificationStatus.Pending); //Pending from SI.Notification.[lookups].[Status]                
                notificationTransmission.SendFrom = SendFrom;
                notificationTransmission.SendTo = ProviderFactory.Util.Trim(ProviderFactory.Util.SerializeObject(sendToDTO));

                int notificationTransmissionID = ProviderFactory.NotificationDB.SaveNotificationEntry(notification, notificationTransmission);

               
                //#region Send Email (Comment in production)
                ////Debug Send Email notification 
                //EmailManager emailManager = new EmailManager();
                //bool sendResult = emailManager.sendNotification("User Registration", notification.Subject, notification.Body, notificationTransmission.SendFrom
                //                                                , new List<string> { notificationTransmission.SendTo }, new List<string>(), new List<string>());

                //if (sendResult == true)
                //{
                //    ProviderFactory.NotificationDB.UpdateNotificationTransmission(notificationTransmissionID, Convert.ToInt32(NotificationStatus.Success));
                //}
                //else
                //{
                //    ProviderFactory.NotificationDB.UpdateNotificationTransmission(notificationTransmissionID, Convert.ToInt32(NotificationStatus.Failure));
                //}
                //#endregion
                
            }
            
            return result;
        }

        private string GenerateNewUserEmailBody(MessageTypeDTO messageTypeDTO, UserDTO userDTO, AccountDTO accountDTO)
        {
            string emailBody = string.Empty;

            string messageTemplate = messageTypeDTO.MessageTemplate;

            messageTemplate = messageTemplate.Replace("[$UserName$]", userDTO.FirstName + " " + userDTO.LastName)
                                             .Replace("[$Email$]", userDTO.EmailAddress)
                                             .Replace("[$RoleName$]", userDTO.RoleTypeName)
                                             .Replace("[$AccountName$]", accountDTO.DisplayName);

            emailBody = messageTemplate;

            return emailBody;
        }
        #endregion

        #region Account
        public bool NewAccountNotification(SaveEntityDTO<AccountDTO> saveEntity)
        {
            bool result = false;

            int MessageTypeID = 16; // [MessageTypeId] from [dbo].[MessageType]
            int MessageCategoryID = 3;

            MessageTypeDTO messageTypeDTO = ProviderFactory.NotificationDB.GetMessageTemplateByID(MessageTypeID);

            if (messageTypeDTO != null && saveEntity.Entity != null)
            {
                string MessageSubject = "New Account Created";
                string emailBody = GenerateNewAccountEmailBody(messageTypeDTO, saveEntity.Entity);

                //Save Email To SI.Notification DB
                SI.DAL.Notification notification = new SI.DAL.Notification();
                NotificationTransmission notificationTransmission = new NotificationTransmission();
                SendToDTO sendToDTO = new SendToDTO();
                sendToDTO.sendTo.Add(saveEntity.Entity.EmailAddress);

                notification.ProcessedDate = DateTime.UtcNow;
                notification.MessageTypeId = MessageTypeID;
                notification.CreatedDate = DateTime.UtcNow;
                notification.AccountID = Convert.ToInt32(saveEntity.Entity.ID);
                notification.MessageCategoryId = MessageCategoryID;
                notification.VirtualGroupID = 0;
                notification.Body = emailBody;
                notification.Subject = MessageSubject;

                notificationTransmission.TransmissionTypeId = 1; //Email from SI.Notification.[lookups].[TransmissionType]
                notificationTransmission.DateUpdated = DateTime.UtcNow;
                notificationTransmission.ScheduledDate = DateTime.UtcNow;
                notificationTransmission.StatusId = Convert.ToInt32(NotificationStatus.Pending); //Pending from SI.Notification.[lookups].[Status]                
                notificationTransmission.SendFrom = SendFrom;
                notificationTransmission.SendTo = ProviderFactory.Util.Trim(ProviderFactory.Util.SerializeObject(sendToDTO)); 

                int notificationTransmissionID = ProviderFactory.NotificationDB.SaveNotificationEntry(notification, notificationTransmission);

                
                //#region Send Email (Comment in production)
                ////Debug Send Email notification 

                //EmailManager emailManager = new EmailManager();
                //bool sendResult = emailManager.sendNotification("User Registration", notification.Subject, notification.Body, notificationTransmission.SendFrom
                //                                                , new List<string> { notificationTransmission.SendTo }, new List<string>(), new List<string>());

                //if (sendResult == true)
                //{
                //    ProviderFactory.NotificationDB.UpdateNotificationTransmission(notificationTransmissionID, Convert.ToInt32(NotificationStatus.Success));
                //}
                //else
                //{
                //    ProviderFactory.NotificationDB.UpdateNotificationTransmission(notificationTransmissionID, Convert.ToInt32(NotificationStatus.Failure));
                //}
                //#endregion
                

            }
            return result;
        }

        private string GenerateNewAccountEmailBody(MessageTypeDTO messageTypeDTO, AccountDTO accountDTO)
        {
            string emailBody = string.Empty;

            string messageTemplate = messageTypeDTO.MessageTemplate;

            string FranchiseTypeNames = string.Empty;
            string AccountTypeName = string.Empty;

			if (accountDTO.FranchiseTypes != null && accountDTO.FranchiseTypes.Length > 0)
            {
                List<FranchiseTypeDTO> listFranchiseTypeDTO = ProviderFactory.Lists.GetFranchiseTypes();
                
                List<string> Names = (from x in listFranchiseTypeDTO
                                      where accountDTO.FranchiseTypes.Contains(x.ID)
                                        select x.Name).ToList();

				FranchiseTypeNames = string.Join(",", Names.ToArray());
	            //FranchiseTypeNames = string.Join(",", accountDTO.FranchiseTypes);
            }

            AccountTypeName = ProviderFactory.Lists.GetAccountTypes().Where(x => x.ID == accountDTO.AccountTypeID).Select(x => x.Name).SingleOrDefault();

            messageTemplate = messageTemplate.Replace("[$AccountName$]", accountDTO.Name)
                                             .Replace("[$DisplayName$]", accountDTO.DisplayName)
                                             .Replace("[$Country$]", accountDTO.CountryName)
                                             .Replace("[$Address$]", accountDTO.StreetAddress1)
                                             .Replace("[$City$]", accountDTO.CityName)
                                             .Replace("[$State$]", accountDTO.StateName)
                                             .Replace("[$ZipCode$]", accountDTO.PostalCode)
                                             .Replace("[$Phone$]", accountDTO.PhoneNumber)
                                             .Replace("[$EmailAddress$]", accountDTO.EmailAddress)
                                             .Replace("[$Franchises$]", FranchiseTypeNames)
                                             .Replace("[$WebsiteURL$]", accountDTO.WebsiteURL)
                                             .Replace("[$AccountType$]", AccountTypeName);

            emailBody = messageTemplate;

            return emailBody;
        }
        #endregion

        #region ProcessPendingNotifications

        public void ProcessPendingNotifications(List<int> NotificationIDs)
        {
            List<EmailNotificationDTO> listemailNotificationDTO = ProviderFactory.NotificationDB.PendingNotifications(NotificationIDs);

            foreach (var emailNotificationDTO in listemailNotificationDTO)
            {
                EmailManager emailManager = new EmailManager();
                bool sendResult = emailManager.sendNotification("User Registration", emailNotificationDTO.Subject, emailNotificationDTO.Body, emailNotificationDTO.SendFrom
                                                                , emailNotificationDTO.SendTo.sendTo
                                                                , (emailNotificationDTO.SendCC == null) ? null : emailNotificationDTO.SendCC.sendCC
                                                                , (emailNotificationDTO.SendBCC == null) ? null : emailNotificationDTO.SendBCC.sendBCC);

                if (sendResult == true)
                {
                    ProviderFactory.NotificationDB.UpdateNotificationTransmission(emailNotificationDTO.NotificationTransmissionID, Convert.ToInt32(NotificationStatus.Success));
                }
                else
                {
                    ProviderFactory.NotificationDB.UpdateNotificationTransmission(emailNotificationDTO.NotificationTransmissionID, Convert.ToInt32(NotificationStatus.Failure));
                }
            }
        }
        #endregion

       */

    }
}
