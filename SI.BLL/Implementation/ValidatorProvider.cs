﻿using Connectors;
using Connectors.Parameters;
using SI.DAL;
using SI.DTO;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Connectors.Reviews.Parameters;
using Connectors.Reviews;
using System.Data.Objects;
using SI.Core;
using Microsoft.Data.Extensions;
using System.Data.Common;
using Connectors.URL_Validators;
using Connectors.URL_Validators.Parameters;

namespace SI.BLL
{
    public class ValidatorProvider : IValidatorProvider
    {
        private static Dictionary<string, string> _processorSettingsCache = null;


        public List<DTO.ReviewSourceDTO> GetReviewSources()
        {
            using (MainEntities db = ContextFactory.Main)
            {
                return (
                    from rs in db.ReviewSources
                    select new ReviewSourceDTO()
                    {
                        ID = rs.ID,
                        Name = rs.Name,
                        DisplayOrder = rs.DisplayOrder,
                        Status = (ReviewSourceStatusEnum)rs.Status
                    }
                ).ToList();
            }

        }

        public string GetProcessorSetting(string key)
        {
            if (_processorSettingsCache == null)
            {
                _processorSettingsCache = new Dictionary<string, string>();
                lock (_processorSettingsCache)
                {
                    using (SystemDBEntities db = ContextFactory.System)
                    {
                        List<ProcessorSetting> list = (from p in db.ProcessorSettings select p).ToList();
                        foreach (ProcessorSetting set in list)
                        {
                            _processorSettingsCache.Add(set.Name.Trim().ToLower(), set.Value);
                        }
                    }
                }
            }

            lock (_processorSettingsCache)
            {
                string value = null;
                _processorSettingsCache.TryGetValue(key.Trim().ToLower(), out value);
                return value;
            }

        }

        public void fillBaseReviewDownloaderXPaths(string namePrefix, XPathParameters xpathParameters)
        {
            xpathParameters.QueryBasePath = GetProcessorSetting(string.Format("{0}.QueryBasePath", namePrefix));
            xpathParameters.ReviewCountPath = GetProcessorSetting(string.Format("{0}.ReviewCountPath", namePrefix));
            xpathParameters.RatingValuePath = GetProcessorSetting(string.Format("{0}.RatingValuePath", namePrefix));
            xpathParameters.LocationNamePath = GetProcessorSetting(string.Format("{0}.LocationNamePath", namePrefix));
            xpathParameters.PhonePath = GetProcessorSetting(string.Format("{0}.PhonePath", namePrefix));
            xpathParameters.AddressPath= GetProcessorSetting(string.Format("{0}.AddressPath", namePrefix));
            xpathParameters.CityPath = GetProcessorSetting(string.Format("{0}.CityPath", namePrefix));
            xpathParameters.StatePath = GetProcessorSetting(string.Format("{0}.StatePath", namePrefix));
            xpathParameters.ZipPath = GetProcessorSetting(string.Format("{0}.ZipPath", namePrefix));
            xpathParameters.WebsiteURLPath = GetProcessorSetting(string.Format("{0}.WebsiteURLPath", namePrefix));
            xpathParameters.ManufacturerPath = GetProcessorSetting(string.Format("{0}.ManufacturerPath", namePrefix));
            xpathParameters.LatitudePath = GetProcessorSetting(string.Format("{0}.LatitudePath", namePrefix));
            xpathParameters.LongitudePath = GetProcessorSetting(string.Format("{0}.LongitudePath", namePrefix));
            xpathParameters.ReviewDetailIteratorPath = GetProcessorSetting(string.Format("{0}.ReviewDetailIteratorPath", namePrefix));
            xpathParameters.NoReviewText = GetProcessorSetting(string.Format("{0}.NoReviewText", namePrefix));
            xpathParameters.NoReviewTextPath = GetProcessorSetting(string.Format("{0}.NoReviewTextPath", namePrefix));
            xpathParameters.NoRatingPath = GetProcessorSetting(string.Format("{0}.NoRatingPath", namePrefix));
        }

        public SIReviewURLValidatorConnection GetValidatorConnection(ReviewSourceEnum reviewSource, int timeOutSeconds = 30, bool useProxy = true, int maxRedirects = 50)
        {
            try
            {
                SIReviewURLValidatorConnection conn = null;

                switch (reviewSource)
                {
                    case ReviewSourceEnum.Google:
                        {
                            GoogleReviewURLValidatorXPathParameters xPaths = new GoogleReviewURLValidatorXPathParameters();

                            GoogleReviewURLValidatorParameters param = new GoogleReviewURLValidatorParameters(120, false, 1, xPaths);

                            fillBaseReviewDownloaderXPaths(param.ProcessorSettingsPrefix(), xPaths);

                            conn = new GoogleReviewURLValidatorConnection(param);
                        }
                        break;
                    case ReviewSourceEnum.DealerRater:
                        {
                            DealerRaterReviewURLValidatorXPathParameters xPaths = new DealerRaterReviewURLValidatorXPathParameters();
                            DealerRaterReviewURLValidatorParameters param = new DealerRaterReviewURLValidatorParameters(120, false, 1, xPaths);
                            fillBaseReviewDownloaderXPaths(param.ProcessorSettingsPrefix(), xPaths);

                            conn = new DealerRaterReviewURLValidatorConnection(param);
                        }
                        break;
                    case ReviewSourceEnum.Yahoo:
                        {
                            YahooReviewURLValidatorXPathParameters xPaths = new YahooReviewURLValidatorXPathParameters();
                            YahooReviewURLValidatorParameters param = new YahooReviewURLValidatorParameters(120, false, 1, xPaths);

                            fillBaseReviewDownloaderXPaths(param.ProcessorSettingsPrefix(), xPaths);

                            conn = new YahooReviewURLValidatorConnection(param);
                            
                            //xYahooLocal.APIURLTemplate = GetProcessorSetting(pYahooLocal.ProcessorSettingsPrefix() + ".APIURLTemplate");
                            //xYahooLocal.ReviewsPerPage = Convert.ToInt32(GetProcessorSetting(pYahooLocal.ProcessorSettingsPrefix() + ".ReviewsPerPage"));

                        }
                        break;
                    case ReviewSourceEnum.Yelp:
                        {
                            YelpReviewURLValidatorXPathParameters xPaths = new YelpReviewURLValidatorXPathParameters();
                            YelpReviewURLValidatorParameters param = new YelpReviewURLValidatorParameters(120, false, 1, xPaths);
                            fillBaseReviewDownloaderXPaths(param.ProcessorSettingsPrefix(), xPaths);

                            conn = new YelpReviewURLValidatorConnection(param);

                            //public string ReviewerNameXPath { get; set; }
                            //public string ReviewDateXPath { get; set; }
                            //public string ReviewTextFullXPath { get; set; }

                            //x.ReviewerNameXPath = GetProcessorSetting("Yelp.ReviewerNameXPath");
                            //x.ReviewDateXPath = GetProcessorSetting("Yelp.ReviewDateXPath");
                            //x.ReviewTextFullXPath = GetProcessorSetting("Yelp.ReviewTextFullXPath");
                            //x.FrenchReviewXPath = GetProcessorSetting("Yelp.FrenchReviewXPath");
                            //x.FilteredReviewsCountXPath = GetProcessorSetting("Yelp.FilteredReviewsCountXPath");

                        }
                        break;
                    case ReviewSourceEnum.CarDealer:
                        {
                            CarDealerReviewURLValidatorXPathParameters xPaths = new CarDealerReviewURLValidatorXPathParameters();
                            CarDealerReviewURLValidatorParameters param = new CarDealerReviewURLValidatorParameters(120, false, 1, xPaths);
                            fillBaseReviewDownloaderXPaths(param.ProcessorSettingsPrefix(), xPaths);

                            conn = new CarDealerReviewURLValidatorConnection(param);

                        }
                        break;
                    case ReviewSourceEnum.YellowPages:
                        {

                            YellowPagesReviewURLValidatorXPathParameters xPaths = new YellowPagesReviewURLValidatorXPathParameters();
                            YellowPagesReviewURLValidatorParameters param = new YellowPagesReviewURLValidatorParameters(120, false, 1, xPaths);
                            fillBaseReviewDownloaderXPaths(param.ProcessorSettingsPrefix(), xPaths);

                            conn = new YellowPagesReviewURLValidatorConnection(param);

                            //xYellowPages.ReviewTextFull = @".//p[@class='review-text truncated']";
                            //xYellowPages.ReviewerName = @"div/p[2]/strong/a";
                            //xYellowPages.ReviewDate = "div/p[2]/text()[2]";

                            //xYellowPages.ReviewTextFull = GetProcessorSetting(pYellowPages.ProcessorSettingsPrefix() + ".ReviewTextFullXPath");
                            //xYellowPages.ReviewerName = GetProcessorSetting(pYellowPages.ProcessorSettingsPrefix() + ".ReviewerNameXPath");
                            //xYellowPages.ReviewDate = GetProcessorSetting(pYellowPages.ProcessorSettingsPrefix() + ".ReviewDateXPath");
                            //xYellowPages.RatingCollected = GetProcessorSetting(pYellowPages.ProcessorSettingsPrefix() + ".RatingValueXPath");
                            //xYellowPages.ReviewCommentXPath = GetProcessorSetting(pYellowPages.ProcessorSettingsPrefix() + ".ReviewCommentXPath");
                            //xYellowPages.ReviewCommentNameXPath = GetProcessorSetting(pYellowPages.ProcessorSettingsPrefix() + ".ReviewCommentNameXPath");
                            //xYellowPages.ReviewCommentTextXPath = GetProcessorSetting(pYellowPages.ProcessorSettingsPrefix() + ".ReviewCommentTextXPath");
                            //xYellowPages.PagingNextXPath = GetProcessorSetting(pYellowPages.ProcessorSettingsPrefix() + ".PagingNextXPath");

                        }

                        break;
                    case ReviewSourceEnum.Edmunds:
                        {
                            EdmundsReviewURLValidatorXPathParameters xPaths = new EdmundsReviewURLValidatorXPathParameters();
                            EdmundsReviewURLValidatorParameters param = new EdmundsReviewURLValidatorParameters(120, false, 1, xPaths);
                            fillBaseReviewDownloaderXPaths(param.ProcessorSettingsPrefix(), xPaths);

                            conn = new EdmundsURLValidatorConnection(param);

                            //xEdmunds.APIURLTemplate = GetProcessorSetting(pEdmunds.ProcessorSettingsPrefix() + ".APIURLTemplate");
                            //xEdmunds.NoServicesReviewText = GetProcessorSetting(pEdmunds.ProcessorSettingsPrefix() + ".NoServicesReviewText");
                            //xEdmunds.NoServicesReviewText2 = GetProcessorSetting(pEdmunds.ProcessorSettingsPrefix() + ".NoServicesReviewText2");
                            //xEdmunds.NoSalesReviewText2 = GetProcessorSetting(pEdmunds.ProcessorSettingsPrefix() + ".NoSalesReviewText2");
                            //xEdmunds.NoSalesReviewText = GetProcessorSetting(pEdmunds.ProcessorSettingsPrefix() + ".NoSalesReviewText");
                            //xEdmunds.NoServicesReviewTextXPath = GetProcessorSetting(pEdmunds.ProcessorSettingsPrefix() + ".NoServicesReviewTextXPath");
                            //xEdmunds.NoSalesReviewTextXPath = GetProcessorSetting(pEdmunds.ProcessorSettingsPrefix() + ".NoSalesReviewTextXPath");

                            //xEdmunds.NoServiceRatingPath = GetProcessorSetting(pEdmunds.ProcessorSettingsPrefix() + ".NoServiceRatingPath");
                            //xEdmunds.NoServiceRatingText = GetProcessorSetting(pEdmunds.ProcessorSettingsPrefix() + ".NoServiceRatingText");
                            //xEdmunds.ReviewCountServicePath = GetProcessorSetting(pEdmunds.ProcessorSettingsPrefix() + ".ReviewCountServicePath");
                            //xEdmunds.RatingValueServicePath = GetProcessorSetting(pEdmunds.ProcessorSettingsPrefix() + ".RatingValueServicePath");

                            //xEdmunds.ExtraValueSalesPath = GetProcessorSetting(pEdmunds.ProcessorSettingsPrefix() + ".ExtraValueSalesPath");
                            //xEdmunds.ExtraValueServicesPath = GetProcessorSetting(pEdmunds.ProcessorSettingsPrefix() + ".ExtraValueServicesPath");


                        }
                        break;
                    case ReviewSourceEnum.CarsDotCom:
                        {
                            CarsdotcomReviewURLValidatorXPathParameters xPaths = new CarsdotcomReviewURLValidatorXPathParameters();
                            CarsdotcomReviewURLValidatorParameters param = new CarsdotcomReviewURLValidatorParameters(120, false, 1, xPaths);
                            fillBaseReviewDownloaderXPaths(param.ProcessorSettingsPrefix(), xPaths);

                            conn = new CarsdotcomReviewURLValidatorConnection(param);

                            //xCarsDotCom.ParentNodeExtraValueSummaryXPath = GetProcessorSetting(pCarsDotCom.ProcessorSettingsPrefix() + ".ParentNodeExtraValueSummaryXPath"); //".//div[@class='col13']/div";
                            //xCarsDotCom.XtraValueWrapperXPath = GetProcessorSetting(pCarsDotCom.ProcessorSettingsPrefix() + ".XtraValueWrapperXPath"); //".//div[@class='wrapper']";
                            //xCarsDotCom.XtraValueNodeXPath = GetProcessorSetting(pCarsDotCom.ProcessorSettingsPrefix() + ".XtraValueNodeXPath"); //".//div[@class='value']";
                            //xCarsDotCom.XtraKeyNodeXPath = GetProcessorSetting(pCarsDotCom.ProcessorSettingsPrefix() + ".XtraKeyNodeXPath"); //".//div[@class='detail']";
                            //xCarsDotCom.XtraNoWrapperValueNodeXPath = GetProcessorSetting(pCarsDotCom.ProcessorSettingsPrefix() + ".XtraNoWrapperValueNodeXPath"); //".//span//span";
                            //xCarsDotCom.XtraNoWrapperKeyNodeXPath = GetProcessorSetting(pCarsDotCom.ProcessorSettingsPrefix() + ".XtraNoWrapperKeyNodeXPath"); //".//div[@class='detail']";
                            //xCarsDotCom.ParentnodeExtraValueXPath = GetProcessorSetting(pCarsDotCom.ProcessorSettingsPrefix() + ".ParentnodeExtraValueXPath");
                            //xCarsDotCom.ParentnodeExtraValueParaXPath = GetProcessorSetting(pCarsDotCom.ProcessorSettingsPrefix() + ".ParentnodeExtraValueParaXPath");
                            //xCarsDotCom.ExtraValueSummaryRecommendedXPath = GetProcessorSetting(pCarsDotCom.ProcessorSettingsPrefix() + ".ExtraValueSummaryRecommendedXPath"); //".//div[@id='dealer-average-ratings']/div[1]/div[1]/p";
                            //xCarsDotCom.ExtraValueSummaryRecommendedText = "Recommended";

                            //xCarsDotCom.ReviewerNameXPath = GetProcessorSetting(pCarsDotCom.ProcessorSettingsPrefix() + ".ReviewerNameXPath");
                            //xCarsDotCom.ReviewTextFullXPath = GetProcessorSetting(pCarsDotCom.ProcessorSettingsPrefix() + ".ReviewTextFullXPath");
                            //xCarsDotCom.ReviewDateXPath = GetProcessorSetting(pCarsDotCom.ProcessorSettingsPrefix() + ".ReviewDateXPath");
                            //xCarsDotCom.ReviewDetailIteratorPath = GetProcessorSetting(pCarsDotCom.ProcessorSettingsPrefix() + ".ReviewDetailIteratorPath");

                            //xCarsDotCom.ParentNodeReviewCommentXPath = GetProcessorSetting(pCarsDotCom.ProcessorSettingsPrefix() + ".ParentNodeReviewCommentXPath");
                            //xCarsDotCom.CommenterNameXPath = GetProcessorSetting(pCarsDotCom.ProcessorSettingsPrefix() + ".CommenterNameXPath");
                            //xCarsDotCom.CommentDateXPath = GetProcessorSetting(pCarsDotCom.ProcessorSettingsPrefix() + ".CommentDateXPath");
                            //xCarsDotCom.CommentXPath = GetProcessorSetting(pCarsDotCom.ProcessorSettingsPrefix() + ".CommentXPath");

                          }
                        break;
                    case ReviewSourceEnum.CitySearch:
                        {
                            CitySearchReviewURLValidatorXPathParameters xPaths = new CitySearchReviewURLValidatorXPathParameters();
                            CitySearchReviewURLValidatorParameters param = new CitySearchReviewURLValidatorParameters(120, false, 1, xPaths);
                            fillBaseReviewDownloaderXPaths(param.ProcessorSettingsPrefix(), xPaths);

                            conn = new CitySearchReviewURLValidatorConnection(param);

                            //xCitySearch.ReviewRatingPath = GetProcessorSetting(pCitySearch.ProcessorSettingsPrefix() + ".ReviewRatingPath");
                            //xCitySearch.ReviewerNameXPath = GetProcessorSetting(pCitySearch.ProcessorSettingsPrefix() + ".ReviewerNameXPath");
                            //xCitySearch.ReviewDateXPath = GetProcessorSetting(pCitySearch.ProcessorSettingsPrefix() + ".ReviewDateXPath");
                            //xCitySearch.ReviewTextFullXPath = GetProcessorSetting(pCitySearch.ProcessorSettingsPrefix() + ".ReviewTextFullXPath");

                        }
                        break;
                    case ReviewSourceEnum.JudysBook:
                        {
                            JudysBookReviewURLValidatorXPathParameters xPaths = new JudysBookReviewURLValidatorXPathParameters();
                            JudysBookReviewURLValidatorParameters param = new JudysBookReviewURLValidatorParameters(120, false, 1, xPaths);
                            fillBaseReviewDownloaderXPaths(param.ProcessorSettingsPrefix(), xPaths);

                            conn = new JudysBookReviewURLValidatorConnection(param);

                            //xJudysBook.ReviewURLXPath = GetProcessorSetting(pJudysBook.ProcessorSettingsPrefix() + ".ReviewURLXPath");
                            //xJudysBook.FullReviewURLXPath = GetProcessorSetting(pJudysBook.ProcessorSettingsPrefix() + ".FullReviewURLXPath");
                            //xJudysBook.ReviewRatingValuePath = GetProcessorSetting(pJudysBook.ProcessorSettingsPrefix() + ".ReviewRatingValuePath");
                            //xJudysBook.ReviewerNameXPath = GetProcessorSetting(pJudysBook.ProcessorSettingsPrefix() + ".ReviewerNameXPath");
                            //xJudysBook.ReviewDateXPath = GetProcessorSetting(pJudysBook.ProcessorSettingsPrefix() + ".ReviewDateXPath");
                            //xJudysBook.ReviewTextFullParentXPath = GetProcessorSetting(pJudysBook.ProcessorSettingsPrefix() + ".ReviewTextFullParentXPath");
                            //xJudysBook.ReviewTextFullXPath = GetProcessorSetting(pJudysBook.ProcessorSettingsPrefix() + ".ReviewTextFullXPath");

                        }
                        break;
                    case ReviewSourceEnum.InsiderPages:
                        {
                            InsiderPagesReviewURLValidatorXPathParameters xPaths = new InsiderPagesReviewURLValidatorXPathParameters();
                            InsiderPagesReviewURLValidatorParameters param = new InsiderPagesReviewURLValidatorParameters(120, false, 1, xPaths);
                            fillBaseReviewDownloaderXPaths(param.ProcessorSettingsPrefix(), xPaths);

                            conn = new InsiderPagesReviewURLValidatorConnection(param);

                            //xInsiderPages.ReviewRatingPath = GetProcessorSetting(pInsiderPages.ProcessorSettingsPrefix() + ".ReviewRatingPath");
                            //xInsiderPages.ReviewerNameXPath = GetProcessorSetting(pInsiderPages.ProcessorSettingsPrefix() + ".ReviewerNameXPath");
                            //xInsiderPages.ReviewDateXPath = GetProcessorSetting(pInsiderPages.ProcessorSettingsPrefix() + ".ReviewDateXPath");
                            //xInsiderPages.ReviewTextFullXPath = GetProcessorSetting(pInsiderPages.ProcessorSettingsPrefix() + ".ReviewTextFullXPath");
                            //xInsiderPages.NextPathURLPath = GetProcessorSetting(pInsiderPages.ProcessorSettingsPrefix() + ".NextPathURLPath");

                       }
                        break;
                    //case ReviewSourceEnum.DealerRaterCanada:
                    //    {
                    //        DealerRaterCanadaReviewURLValidatorXPathParameters xPaths = new DealerRaterCanadaReviewURLValidatorXPathParameters();
                    //        DealerRaterCanadaReviewURLValidatorParameters param = new DealerRaterCanadaReviewURLValidatorParameters(120, false, 1, xPaths);
                    //        fillBaseReviewDownloaderXPaths(param.ProcessorSettingsPrefix(), xPaths);

                    //        conn = new DealerRaterCanadaReviewURLValidatorConnection(param);

                    //        //x.APIURLTemplate = GetProcessorSetting("DealerRater.APIURLTemplate");

                    //        //x.ReviewerNameXPath = GetProcessorSetting("DealerRater.ReviewerNameXPath");
                    //        //x.RatingValueXPath2 = GetProcessorSetting("DealerRater.RatingValueXPath2");
                    //        //x.ReviewTextFullXPath = GetProcessorSetting("DealerRater.ReviewTextFullXPath");
                    //        //x.ReviewDateXPath = GetProcessorSetting("DealerRater.ReviewDateXPath");

                    //        //x.CommentIteratorXPath = GetProcessorSetting("DealerRater.CommentIteratorXPath");
                    //        //x.CommenterNameXPath = GetProcessorSetting("DealerRater.CommenterNameXPath");
                    //        //x.CommentDateAddedXPath = GetProcessorSetting("DealerRater.CommentDateAddedXPath");
                    //        //x.CommentTextFullXPath = GetProcessorSetting("DealerRater.CommentTextFullXPath");

                    //        //x.ExtraInfoKeys = GetProcessorSetting("DealerRater.ExtraInfoKeysXPath");
                    //        //x.ExtraInfoXPaths = GetProcessorSetting("DealerRater.ExtraInfoValuesXPath");

                    //        //x.ExtraInfoKeysDetailXPath = GetProcessorSetting("DealerRater.ExtraInfoKeysDetailXPath");
                    //        //x.ExtraInfoValuesDetailXPath = GetProcessorSetting("DealerRater.ExtraInfoValuesDetailXPath");

                    //    }
                    //    break;
                    //case ReviewSourceEnum.YelpCanada:
                    //    {
                    //        YelpCanadaReviewURLValidatorXPathParameters xPaths = new YelpCanadaReviewURLValidatorXPathParameters();
                    //        YelpCanadaReviewURLValidatorParameters param = new YelpCanadaReviewURLValidatorParameters(120, false, 1, xPaths);
                    //        fillBaseReviewDownloaderXPaths(param.ProcessorSettingsPrefix(), xPaths);

                    //        conn = new YelpCanadaReviewURLValidatorConnection(param);

                    //        //xYelpCanada.ReviewerNameXPath = GetProcessorSetting("Yelp.ReviewerNameXPath");
                    //        //xYelpCanada.ReviewDateXPath = GetProcessorSetting("Yelp.ReviewDateXPath");
                    //        //xYelpCanada.ReviewTextFullXPath = GetProcessorSetting("Yelp.ReviewTextFullXPath");
                    //        //xYelpCanada.ReviewTextFullXPath = GetProcessorSetting("Yelp.FrenchReviewXPath");

                    //    }
                    //    break;

                    //case ReviewSourceEnum.CitySearchCanada:
                    //    {
                    //        CitySearchCanadaReviewURLValidatorXPathParameters xPaths = new CitySearchCanadaReviewURLValidatorXPathParameters();
                    //        CitySearchCanadaReviewURLValidatorParameters param = new CitySearchCanadaReviewURLValidatorParameters(120, false, 1, xPaths);
                    //        fillBaseReviewDownloaderXPaths(param.ProcessorSettingsPrefix(), xPaths);

                    //        conn = new CitySearchCanadaReviewURLValidatorConnection(param);

                    //        //xCitySearch.ReviewRatingPath = GetProcessorSetting(pCitySearch.ProcessorSettingsPrefix() + ".ReviewRatingPath");
                    //        //xCitySearch.ReviewerNameXPath = GetProcessorSetting(pCitySearch.ProcessorSettingsPrefix() + ".ReviewerNameXPath");
                    //        //xCitySearch.ReviewDateXPath = GetProcessorSetting(pCitySearch.ProcessorSettingsPrefix() + ".ReviewDateXPath");
                    //        //xCitySearch.ReviewTextFullXPath = GetProcessorSetting(pCitySearch.ProcessorSettingsPrefix() + ".ReviewTextFullXPath");

                    //    }
                    //    break;
                }

                return conn;
            }
            catch (Exception ex)
            {
                ProviderFactory.Logging.LogException(ex);
                return null;
            }
        }
    }
}
