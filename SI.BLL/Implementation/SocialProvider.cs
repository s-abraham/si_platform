﻿using System.Data;
using System.Data.Objects;
using System.Globalization;
using Newtonsoft.Json;
using SI.DAL;
using SI.DTO;
using Microsoft.Data.Extensions;
using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using System.Xml.Linq;
using System.Dynamic;
using SI.BLL.Extensions;
using SI.BLL.Interface;
using System.Web;
using Connectors.Publish;
using Facebook.Entities;
using Google.Model;
using HtmlAgilityPack;
using Connectors.Parameters;
using Connectors;
using AWS;
using Twitter.Entities;
using Connectors.Social;
using Connectors.Social.Parameters;

namespace SI.BLL.Implementation
{
    internal class SocialProvider : ISocialProvider
    {
        #region social

        //public bool SaveSocialConnection(UserInfoDTO userInfo, long credentialId, long accountId, long socialappid, string token, string tokenSecret, string id, string url, string pictureURL = "", string screenName = "")
        //{
        //    bool Success = false;
        //    using (MainEntities db = ContextFactory.Main)
        //    {
        //        Credential credential;

        //        bool isNew = false;
        //        if (credentialId == 0)
        //        {
        //            isNew = true;
        //            credential = new Credential();
        //            credential.SocialAppID = socialappid;
        //            credential.AccountID = accountId;
        //            credential.DateCreated = DateTime.UtcNow;

        //            db.Credentials.AddObject(credential);
        //        }
        //        else
        //            credential = db.Credentials.Where(c => c.ID == credentialId).FirstOrDefault();

        //        if (credential != null)
        //        {
        //            credential.URI = url.IsNullOptional("");
        //            credential.UniqueID = id;
        //            credential.Token = token;
        //            credential.TokenSecret = tokenSecret;
        //            credential.DateModified = DateTime.UtcNow;
        //            credential.DateLastValidated = credential.DateModified;
        //            credential.IsActive = true;
        //            credential.IsValid = true;
        //            credential.PictureURL = pictureURL;
        //            credential.ScreenName = screenName;

        //            db.SaveChanges();

        //            Success = true;

        //            Auditor
        //                .New(AuditLevelEnum.Information, AuditTypeEnum.UserActivity, userInfo, "")
        //                .SetAuditDataObject(
        //                    AuditUserActivity
        //                    .New(userInfo, isNew ? AuditUserActivityTypeEnum.AddedSocialConnection : AuditUserActivityTypeEnum.UpdatedSocialConnection)
        //                        .SetCredentialID(credential.ID)
        //                        .SetDescription(isNew ? "Social Connection Added" : "Social Connection Updated")
        //                )
        //                .Save(ProviderFactory.Logging)
        //            ;
        //        }
        //    }

        //    return Success;
        //}


        public SaveEntityDTO<SocialConnectionInfoDTO> Save(SaveEntityDTO<SocialConnectionInfoDTO> dto)
        {
            dto.Problems.Clear();
            SocialConnectionInfoDTO obj = dto.Entity;
            UserInfoDTO userInfoDTO = dto.UserInfo;

            long socialNetworkID = (long)obj.SocialNetwork;

            AccountActivityTypeEnum type = AccountActivityTypeEnum.FacebookTokenCreated;

            if (obj.SocialNetwork == SocialNetworkEnum.Facebook)
                type = AccountActivityTypeEnum.FacebookTokenCreated;
            if (obj.SocialNetwork == SocialNetworkEnum.Twitter)
                type = AccountActivityTypeEnum.TwitterTokenCreated;
            if (obj.SocialNetwork == SocialNetworkEnum.Google)
                type = AccountActivityTypeEnum.GooglePlusTokenCreated;

            using (MainEntities db = ContextFactory.Main)
            {
                if (obj.ID.HasValue)
                {
                    //save changes
                    Credential cred = (from c in db.Credentials where c.ID == obj.ID.Value select c).FirstOrDefault();
                    if (cred == null)
                    {
                        dto.Problems.Add("Existing credential was not found, changes could not be saved");
                    }
                    else
                    {
                        cred.PictureURL = obj.PictureURL;
                        cred.ScreenName = obj.ScreenName;
                        cred.Token = obj.Token;
                        cred.TokenSecret = obj.TokenSecret;
                        cred.UniqueID = obj.UniqueID;
                        cred.URI = obj.URI;
                        cred.DateModified = DateTime.UtcNow;
                        cred.SocialAppID = obj.SocialAppID;
                        cred.CreatedByUserID = userInfoDTO.LoggedInUser.ID;

                        db.SaveChanges();

                        Auditor
                            .New(AuditLevelEnum.Information, AuditTypeEnum.AccountActivity, userInfoDTO, "")
                            .SetAuditDataObject(
                                AuditAccountActivity
                                    .New(cred.AccountID, type)
                                    .SetDescription("Updated [{0}] Credential, CredentialID [{1}].", obj.SocialNetwork.ToString(), cred.ID)
                            )
                        .Save(ProviderFactory.Logging);
                    }
                }
                else
                {
                    //save new
                    //long? socialAppID = findSocialAppID(obj.AccountID, obj.SocialNetwork);                    
                    long? socialAppID = dto.Entity.SocialAppID;
                    if (!socialAppID.HasValue)
                    {
                        dto.Problems.Add("Could not find a valid social application for this account.");
                    }
                    else
                    {
                        // find any current credentials
                        IEnumerable<Credential> creds = (
                            from c in db.Credentials
                            join sa in db.SocialApps on c.SocialAppID equals sa.ID
                            where c.IsActive == true
                            && sa.SocialNetworkID == socialNetworkID
                            && c.AccountID == obj.AccountID
                            select c
                        );


                        Credential credPrevious = null;
                        if (creds.Any())
                        {
                            //Find the current Active credential
                            credPrevious = creds.Where(c => c.IsActive == true).OrderByDescending(x => x.ID).FirstOrDefault();
                        }
                        else
                        {
                            // find Last Not Active credential
                            credPrevious = (
                                from c in db.Credentials
                                join sa in db.SocialApps on c.SocialAppID equals sa.ID
                                where c.IsActive == false
                                && sa.SocialNetworkID == socialNetworkID
                                && c.AccountID == obj.AccountID
                                select c
                            ).OrderByDescending(x => x.ID).FirstOrDefault();
                        }


                        // disable any credentials prior to this
                        foreach (Credential cred in creds)
                        {
                            cred.IsActive = false;
                            cred.DateModified = DateTime.UtcNow;
                        }

                        db.SaveChanges();

                        //create a new credentials
                        Credential newCred = new Credential()
                        {
                            AccountID = obj.AccountID,
                            SocialAppID = obj.SocialAppID,
                            DateModified = DateTime.UtcNow,
                            DateCreated = DateTime.UtcNow,
                            DateLastValidated = DateTime.UtcNow,
                            IsActive = true,
                            IsValid = true,
                            PictureURL = obj.PictureURL,
                            ScreenName = obj.ScreenName,
                            Token = obj.Token,
                            TokenSecret = obj.TokenSecret,
                            UniqueID = obj.UniqueID,
                            URI = obj.URI,
                            CreatedByUserID = userInfoDTO.LoggedInUser.ID
                        };

                        db.Credentials.AddObject(newCred);
                        db.SaveChanges();

                        dto.Entity.ID = newCred.ID;

                        Auditor
                           .New(AuditLevelEnum.Information, AuditTypeEnum.AccountActivity, userInfoDTO, "")
                           .SetAuditDataObject(
                               AuditAccountActivity
                                   .New(newCred.AccountID, type)
                                   .SetDescription("Created New [{0}] Credential, CredentialID [{1}].", obj.SocialNetwork.ToString(), newCred.ID)
                           )
                       .Save(ProviderFactory.Logging);


                        int NumberOfDaysBack = 60;
                        if (credPrevious != null && newCred != null)
                        {
                            //Check old credential UniqueID & new credential UniqueID
                            //If both are not same then 
                            //SPN-593 
                            //1. Delete existing future posts
                            //2. Delete existing previous posts
                            //3. Delete existing stats

                            //4. Backfill the last 2 months worth of posts for the new page.
                            if (credPrevious.UniqueID.Trim() != newCred.UniqueID.Trim())
                            {
                                #region 4 Backfill the last 2 months worth of posts for the new page

                                if (obj.SocialNetwork == SocialNetworkEnum.Facebook)
                                {
                                    #region Facebook
                                    FacebookDownloaderData facebookDownloaderData = new FacebookDownloaderData();

                                    facebookDownloaderData.CredentialID = newCred.ID;
                                    facebookDownloaderData.FacebookEnum = FacebookProcessorActionEnum.FacebookPostDownLoader;
                                    facebookDownloaderData.NumberOfDaysBack = NumberOfDaysBack * -1;

                                    JobDTO job1 = new JobDTO()
                                    {
                                        JobDataObject = facebookDownloaderData,
                                        JobType = facebookDownloaderData.JobType,
                                        DateScheduled = new SIDateTime(DateTime.UtcNow, TimeZoneInfo.Utc),
                                        JobStatus = JobStatusEnum.Queued,
                                        Priority = 200
                                    };

                                    SaveEntityDTO<JobDTO> save = ProviderFactory.Jobs.Save(new SaveEntityDTO<JobDTO>(job1, UserInfoDTO.System));
                                    #endregion
                                }

                                if (obj.SocialNetwork == SocialNetworkEnum.Google)
                                {
                                    #region Google+
                                    GooglePlusDownloaderData googlePlusDownloaderData = new GooglePlusDownloaderData();

                                    googlePlusDownloaderData.CredentialID = newCred.ID;
                                    googlePlusDownloaderData.GooglePlusEnum = GooglePlusProcessorActionEnum.GooglePlusPostDownLoader;

                                    JobDTO job1 = new JobDTO()
                                    {
                                        JobDataObject = googlePlusDownloaderData,
                                        JobType = googlePlusDownloaderData.JobType,
                                        DateScheduled = new SIDateTime(DateTime.UtcNow, TimeZoneInfo.Utc),
                                        JobStatus = JobStatusEnum.Queued,
                                        Priority = 200
                                    };

                                    SaveEntityDTO<JobDTO> save = ProviderFactory.Jobs.Save(new SaveEntityDTO<JobDTO>(job1, UserInfoDTO.System));
                                    #endregion
                                }

                                Auditor
                                     .New(AuditLevelEnum.Information, AuditTypeEnum.AccountActivity, userInfoDTO, "")
                                     .SetAuditDataObject(
                                         AuditAccountActivity
                                             .New(newCred.AccountID, AccountActivityTypeEnum.SocialDataBackFillDataQueued)
                                             .SetDescription("Added job to Backfill last 2 months of posts for the new [{0}] page, CredentialID [{1}]. (UniqueID Changed Old {2}, New {3})", obj.SocialNetwork.ToString(), newCred.ID, credPrevious.UniqueID, newCred.UniqueID)
                                     )
                                 .Save(ProviderFactory.Logging);
                                #endregion

                                #region 1,2,3

                                FacebookDownloaderData facebookDownloaderData1 = new FacebookDownloaderData();

                                facebookDownloaderData1.CredentialID = credPrevious.ID;
                                facebookDownloaderData1.FacebookEnum = FacebookProcessorActionEnum.DeletePostsAndPageInfo;

                                JobDTO job2 = new JobDTO()
                                {
                                    JobDataObject = facebookDownloaderData1,
                                    JobType = facebookDownloaderData1.JobType,
                                    DateScheduled = new SIDateTime(DateTime.UtcNow, TimeZoneInfo.Utc),
                                    JobStatus = JobStatusEnum.Queued,
                                    Priority = 250
                                };

                                SaveEntityDTO<JobDTO> save1 = ProviderFactory.Jobs.Save(new SaveEntityDTO<JobDTO>(job2, UserInfoDTO.System));

                                Auditor
                                        .New(AuditLevelEnum.Information, AuditTypeEnum.AccountActivity, userInfoDTO, "")
                                        .SetAuditDataObject(
                                            AuditAccountActivity
                                                .New(newCred.AccountID, AccountActivityTypeEnum.SocialDataDeleteJobQueued)
                                                .SetDescription("Added job To Delete Posts, Post Stats and Page Stats CredentialID {0}", credPrevious.ID)
                                        )
                                    .Save(ProviderFactory.Logging);
                                #endregion

                            }
                            else if (credPrevious.UniqueID.Trim() == newCred.UniqueID.Trim())
                            {
                                //If the unique ID is the same then:
                                //1. Update post targets table for unpublished posts to point to new credential
                                if (credPrevious.ID != newCred.ID)
                                {
                                    #region 1. Update post targets table for unpublished posts to point to new credential

                                    db.spUpdatePostTargets(credPrevious.ID, newCred.ID);

                                    Auditor
                                        .New(AuditLevelEnum.Information, AuditTypeEnum.AccountActivity, userInfoDTO, "")
                                        .SetAuditDataObject(
                                            AuditAccountActivity
                                                .New(newCred.AccountID, AccountActivityTypeEnum.UpdatedPostTargetForNewCredential)
                                                .SetDescription("Updated Post Targets From CredentialID {0} to CredentialID {1}.", credPrevious.ID, newCred.ID)
                                        )
                                    .Save(ProviderFactory.Logging);

                                    #endregion
                                }
                            }
                        }
                        else if (credPrevious == null && newCred != null)
                        {
                            #region 4 Backfill the last 2 months worth of posts for the new page

                            if (obj.SocialNetwork == SocialNetworkEnum.Facebook)
                            {
                                #region Facebook
                                FacebookDownloaderData facebookDownloaderData = new FacebookDownloaderData();

                                facebookDownloaderData.CredentialID = newCred.ID;
                                facebookDownloaderData.FacebookEnum = FacebookProcessorActionEnum.FacebookPostDownLoader;
                                facebookDownloaderData.NumberOfDaysBack = NumberOfDaysBack * -1;

                                JobDTO job1 = new JobDTO()
                                {
                                    JobDataObject = facebookDownloaderData,
                                    JobType = facebookDownloaderData.JobType,
                                    DateScheduled = new SIDateTime(DateTime.UtcNow, TimeZoneInfo.Utc),
                                    JobStatus = JobStatusEnum.Queued,
                                    Priority = 200
                                };

                                SaveEntityDTO<JobDTO> save = ProviderFactory.Jobs.Save(new SaveEntityDTO<JobDTO>(job1, UserInfoDTO.System));
                                #endregion
                            }

                            if (obj.SocialNetwork == SocialNetworkEnum.Google)
                            {
                                #region Google+
                                GooglePlusDownloaderData googlePlusDownloaderData = new GooglePlusDownloaderData();

                                googlePlusDownloaderData.CredentialID = newCred.ID;
                                googlePlusDownloaderData.GooglePlusEnum = GooglePlusProcessorActionEnum.GooglePlusPostDownLoader;

                                JobDTO job1 = new JobDTO()
                                {
                                    JobDataObject = googlePlusDownloaderData,
                                    JobType = googlePlusDownloaderData.JobType,
                                    DateScheduled = new SIDateTime(DateTime.UtcNow, TimeZoneInfo.Utc),
                                    JobStatus = JobStatusEnum.Queued,
                                    Priority = 200
                                };

                                SaveEntityDTO<JobDTO> save = ProviderFactory.Jobs.Save(new SaveEntityDTO<JobDTO>(job1, UserInfoDTO.System));
                                #endregion
                            }

                            Auditor
                                 .New(AuditLevelEnum.Information, AuditTypeEnum.AccountActivity, userInfoDTO, "")
                                 .SetAuditDataObject(
                                     AuditAccountActivity
                                         .New(newCred.AccountID, AccountActivityTypeEnum.SocialDataBackFillDataQueued)
                                         .SetDescription("Added job to Backfill last 2 months of posts for the new [{0}] page, CredentialID [{1}].", obj.SocialNetwork.ToString(), newCred.ID)
                                 )
                             .Save(ProviderFactory.Logging);
                            #endregion
                        }

                    }
                }
            }

            return dto;
        }

        //public bool SaveSocialConnectionFromSyndication(UserInfoDTO userInfo, long? socialappid, string pageToken, string pageId, string screenName, string website, string pictureURL, Guid accessToken)
        //{
        //    bool Success = false;

        //    SyndicationInviteAccessTokenDTO accessTokenBaseDto = (SyndicationInviteAccessTokenDTO)ProviderFactory.Security.GetAccessToken(accessToken, true);

        //    if (accessTokenBaseDto != null && accessTokenBaseDto.Type() == AccessTokenTypeEnum.SyndicationInvitation)
        //    {
        //        using (MainEntities db = ContextFactory.Main)
        //        {
        //            Credential credential = db.Credentials.Where(c => c.AccountID == accessTokenBaseDto.AccountID && c.UniqueID == pageId).FirstOrDefault();

        //            if (credential == null)
        //            {
        //                credential = new Credential();
        //                credential.SocialAppID = socialappid.GetValueOrDefault();
        //                credential.AccountID = accessTokenBaseDto.AccountID;
        //                credential.DateCreated = DateTime.UtcNow;

        //                db.Credentials.AddObject(credential);
        //            }

        //            credential.URI = website.IsNullOptional("");
        //            credential.UniqueID = pageId;
        //            credential.Token = pageToken;
        //            credential.TokenSecret = "";
        //            credential.DateModified = DateTime.UtcNow;
        //            credential.DateLastValidated = credential.DateModified;
        //            credential.IsActive = true;
        //            credential.IsValid = true;
        //            credential.PictureURL = pictureURL;
        //            credential.ScreenName = screenName;

        //            db.SaveChanges();

        //            UpdatePackageResultDTO dto = ProviderFactory.Products.AddPackage(accessTokenBaseDto.AccountID, accessTokenBaseDto.ResellerID, accessTokenBaseDto.PackageID, userInfo);
        //            Success = dto.Outcome == UpdatePackageOutcome.Success;
        //        }
        //    }

        //    return Success;
        //}

        /// <summary>
        /// Finds the correct social app for a given account and social network
        /// </summary>
        private long? findSocialAppID(long accountID, SocialNetworkEnum network)
        {
            SecurityProvider sec = new SecurityProvider();
            Dictionary<long, AccountTree> tree = sec.getAccountTree();
            long resellerID = sec.GetResellerByAccountID(UserInfoDTO.System, accountID, TimeZoneInfo.Utc).ID.Value;
            long resellerAccountID = (from t in tree where t.Value.ResellerID.GetValueOrDefault(-1) == resellerID select t.Key).FirstOrDefault();

            using (MainEntities db = ContextFactory.Main)
            {
                if (db.Connection.State != ConnectionState.Open)
                    db.Connection.Open();

                using (DbCommand cmd = db.CreateStoreCommand("spGetCurrentSocialAppList", CommandType.StoredProcedure))
                {
                    using (DbDataReader reader = cmd.ExecuteReader())
                    {
                        List<SocialAppListItem> items = db.Translate<SocialAppListItem>(reader).ToList();

                        long? socialAppID = (from i in items where i.ResellerID == resellerID && i.SocialNetworkID == (long)network select i.SocialAppID).FirstOrDefault();
                        while (!socialAppID.HasValue)
                        {
                            //find the next reseller
                            AccountTree res = tree[resellerAccountID];
                            while (true)
                            {
                                res = res.Parent;
                                if (res == null || res.ResellerID.HasValue) break;
                            }

                            if (res == null) return null;

                            resellerID = res.ResellerID.Value;
                            resellerAccountID = res.ID;

                            socialAppID = (from i in items where i.ResellerID == resellerID && i.SocialNetworkID == (long)network select i.SocialAppID).FirstOrDefault(null);
                        }

                        return socialAppID;
                    }
                }
            }
        }

        private class SocialAppListItem
        {
            public long SocialAppID { get; set; }
            public long SocialNetworkID { get; set; }
            public long ResellerID { get; set; }
        }

        public bool UpdateSocialConnection(long credentialId, bool isValid)
        {
            bool Success = false;
            using (MainEntities db = ContextFactory.Main)
            {
                Credential credential;

                if (credentialId == 0)
                {
                    return Success;
                }
                else
                    credential = db.Credentials.Where(c => c.ID == credentialId).FirstOrDefault();

                if (credential != null)
                {

                    credential.DateLastValidated = DateTime.UtcNow;
                    credential.IsValid = isValid;

                    db.SaveChanges();

                    Success = true;
                }
            }

            return Success;
        }

        public bool UpdateSocialConnectionToken(long credentialId, string Token)
        {
            bool Success = false;
            using (MainEntities db = ContextFactory.Main)
            {
                Credential credential;

                if (credentialId == 0)
                {
                    return Success;
                }
                else
                    credential = db.Credentials.Where(c => c.ID == credentialId).FirstOrDefault();

                if (credential != null)
                {

                    credential.DateLastValidated = DateTime.UtcNow;
                    credential.IsValid = true;
                    credential.Token = Token;

                    db.SaveChanges();

                    Success = true;
                }
            }

            return Success;
        }

        public List<FranchiseTypeDTO> GetAccountFranchises(long accountID)
        {
            using (MainEntities db = ContextFactory.Main)
            {
                return (
                    from i in db.FranchiseTypes
                    join a in db.Accounts_FranchiseTypes on i.ID equals a.FranchiseTypeID
                    where a.AccountID == accountID
                    orderby i.Name
                    select new FranchiseTypeDTO()
                    {
                        ID = i.ID,
                        Name = i.Name
                    }
                 ).ToList();
            }
        }

        public bool DisconnectionSocialConnection(UserInfoDTO userInfo, long configId, long accountId)
        {
            using (MainEntities db = ContextFactory.Main)
            {
                Credential credential = db.Credentials.Where(c => c.ID == configId && c.AccountID == accountId).FirstOrDefault();
                if (credential != null)
                {
                    credential.IsActive = false;
                    credential.DateModified = DateTime.UtcNow;

                    long oldCredentialID = credential.ID;

                    db.SaveChanges();

                    Auditor
                        .New(AuditLevelEnum.Information, AuditTypeEnum.UserActivity, userInfo, "")
                        .SetAuditDataObject(
                            AuditUserActivity
                            .New(userInfo, AuditUserActivityTypeEnum.RemovedSocialConnection)
                                .SetCredentialID(oldCredentialID)
                                .SetDescription("Social Connection Removed")
                        )
                        .Save(ProviderFactory.Logging)
                    ;

                    return true;
                }
            }

            return false;
        }

        public List<SocialCredentialDTO> GetAccountSocialCredentials(UserInfoDTO userInfo, long accountId)
        {
            using (MainEntities db = ContextFactory.Main)
            {
                return (from c in db.Credentials
                        join sa in db.SocialApps on c.SocialAppID equals sa.ID
                        join sn in db.SocialNetworks on sa.SocialNetworkID equals sn.ID
                        where c.AccountID == accountId
                        select new SocialCredentialDTO()
                        {
                            ID = c.ID,
                            SocialNetworkID = sn.ID,
                            SocialNetworkName = sn.Name,
                            PageUrl = c.URI,
                            UniqueID = c.UniqueID,
                            Token = c.Token,
                            TokenSecret = c.TokenSecret,
                            ScreenName = c.ScreenName,
                            PictureURL = c.PictureURL,
                            DateLastVerified = c.DateLastValidated,
                            IsValid = c.IsValid,
                            IsActive = c.IsActive,
                            SocialAppID = c.SocialAppID
                        }).ToList();
            }

        }

        public List<SocialCredentialDTO> GetSocialCredentials()
        {
            return GetSocialCredentials(null, null, false);
        }

        public List<SocialCredentialDTO> GetSocialCredentials(SocialNetworkEnum? socialNetworkID, long? accountID, bool onlyValidCredentials)
        {
            using (MainEntities db = ContextFactory.Main)
            {
                return (from c in db.Credentials
                        join sa in db.SocialApps on c.SocialAppID equals sa.ID
                        join sn in db.SocialNetworks on sa.SocialNetworkID equals sn.ID
                        where sn.ID == (socialNetworkID.HasValue ? (long)socialNetworkID.Value : sn.ID)
                        && c.AccountID == (accountID.HasValue ? accountID.Value : c.AccountID)
                        && c.IsActive == true
                        && c.IsValid == (onlyValidCredentials ? true : c.IsValid)
                        select new SocialCredentialDTO()
                        {
                            ID = c.ID,
                            SocialNetworkID = sn.ID,
                            SocialNetworkName = sn.Name,
                            PageUrl = c.URI,
                            UniqueID = c.UniqueID,
                            Token = c.Token,
                            TokenSecret = c.TokenSecret,
                            ScreenName = c.ScreenName,
                            PictureURL = c.PictureURL,
                            DateLastVerified = c.DateLastValidated,
                            IsValid = c.IsValid,
                            IsActive = c.IsActive,
                            SocialAppID = c.SocialAppID,
                            AppID = sa.AppID,
                            AppSecret = sa.AppSecret
                        }).ToList();
            }
        }


        public List<SocialNetworkDTO> GetSocialNetworks()
        {
            using (MainEntities db = ContextFactory.Main)
            {
                return db.SocialNetworks.Select(s => new SocialNetworkDTO { ID = s.ID, Name = s.Name }).ToList();
            }
        }

        public List<SyndicatorsDTO> GetSyndicators()
        {
            var results = new List<SyndicatorsDTO>();

            using (MainEntities db = ContextFactory.Main)
            {
                var rawResult = db.CreateStoreCommand("report.GetSyndicationSubscribers", System.Data.CommandType.StoredProcedure).Materialize<LookupInfo>().ToList();
                results.AddRange(rawResult.Select(info => new SyndicatorsDTO() { Name = info.Name, ID = info.ID }));
            }

            return results;
        }

        public LinkPreviewDTO GetLinkPreview(string url)
        {
            LinkPreviewDTO linkPreviewDto = new LinkPreviewDTO();

            Connectors.Entities.LinkPreview linkPreview = HtmlPreviewConnection.GetLinkPreview(url);
            if (linkPreview != null)
            {
                linkPreviewDto.IsValidUrl = linkPreview.IsValidUrl;
                linkPreviewDto.PageDescription = linkPreview.PageDescription;
                linkPreviewDto.PageImages.AddRange(linkPreview.PageImages);
                linkPreviewDto.PageTitle = linkPreview.PageTitle;
                linkPreviewDto.PageUrl = linkPreview.PageUrl;
            }

            return linkPreviewDto;
        }


        public SocialConfigDTO GetSocialConfig(UserInfoDTO userInfo, long resellerID, SocialNetworkEnum network)
        {
            using (MainEntities db = ContextFactory.Main)
            {
                return (from sa in db.SocialApps
                        //join sn in db.SocialNetworks on sa.SocialNetworkID equals sn.ID
                        where sa.ResellerID == resellerID
                            //&& sn.Name == networkName 
                        && sa.SocialNetworkID == (long)network
                        && sa.Active
                        orderby sa.AppVersion descending
                        select new SocialConfigDTO()
                        {
                            AppCallback = sa.CallbackURL,
                            AppID = sa.AppID,
                            AppPermissions = sa.Scope,
                            AppSecret = sa.AppSecret,
                            SocialAppID = sa.ID
                        }
                ).FirstOrDefault();
            }

        }

        public SocialRefreshDTO GetSocialRefresh(UserInfoDTO userInfo, long credentialId, long accountId)
        {
            using (MainEntities db = ContextFactory.Main)
            {
                var socialRefresh = (from c in db.Credentials
                                     join sa in db.SocialApps on c.SocialAppID equals sa.ID
                                     where c.ID == credentialId && c.AccountID == accountId && sa.Active
                                     select new { c, sa }).FirstOrDefault();

                if (socialRefresh != null)
                {
                    SocialRefreshDTO socialRefreshDto = new SocialRefreshDTO();

                    socialRefreshDto.NetworkName = socialRefresh.sa.SocialNetwork.Name;
                    socialRefreshDto.CredentialID = socialRefresh.c.ID;
                    socialRefreshDto.AppID = socialRefresh.sa.AppID;
                    socialRefreshDto.AppSecret = socialRefresh.sa.AppSecret;
                    socialRefreshDto.CallbackURL = socialRefresh.sa.CallbackURL;
                    socialRefreshDto.RefreshToken = socialRefresh.c.Token;
                    socialRefreshDto.TokenSecret = socialRefresh.c.TokenSecret;
                    socialRefreshDto.UniqueID = socialRefresh.c.UniqueID;

                    return socialRefreshDto;
                }
            }

            return null;

        }

        public SocialRefreshDTO GetSocialRefresh(long credentialId, long accountId)
        {
            using (MainEntities db = ContextFactory.Main)
            {
                var socialRefresh = (from c in db.Credentials
                                     join sa in db.SocialApps on c.SocialAppID equals sa.ID
                                     where c.ID == credentialId && c.AccountID == accountId && sa.Active
                                     select new { c, sa }).FirstOrDefault();

                if (socialRefresh != null)
                {
                    SocialRefreshDTO socialRefreshDto = new SocialRefreshDTO();

                    socialRefreshDto.NetworkName = socialRefresh.sa.SocialNetwork.Name;
                    socialRefreshDto.CredentialID = socialRefresh.c.ID;
                    socialRefreshDto.AppID = socialRefresh.sa.AppID;
                    socialRefreshDto.AppSecret = socialRefresh.sa.AppSecret;
                    socialRefreshDto.CallbackURL = socialRefresh.sa.CallbackURL;
                    socialRefreshDto.RefreshToken = socialRefresh.c.Token;
                    socialRefreshDto.TokenSecret = socialRefresh.c.TokenSecret;
                    socialRefreshDto.UniqueID = socialRefresh.c.UniqueID;

                    return socialRefreshDto;
                }
            }

            return null;
        }

        public bool UpdateValidCredential(UserInfoDTO userInfo, long credentialID, long accountID)
        {
            using (MainEntities db = ContextFactory.Main)
            {
                Credential credential = db.Credentials.Where(c => c.ID == credentialID && c.AccountID == accountID).FirstOrDefault();
                if (credential != null)
                {
                    credential.IsActive = true;
                    credential.IsValid = true;
                    credential.DateModified = DateTime.UtcNow;
                    credential.DateLastValidated = credential.DateModified;

                    db.SaveChanges();
                    return true;
                }
            }

            return false;
        }

        #endregion

        #region publishing

        public SaveEntityDTO<PostDTO> Save(SaveEntityDTO<PostDTO> dto)
        {
            if (dto.Entity.ID.HasValue)
            {
                Auditor
                    .New(AuditLevelEnum.Information, AuditTypeEnum.UserActivity, dto.UserInfo, "SaveEntityDTO<PostDTO> Save(SaveEntityDTO<PostDTO>) called")
                    .SetAuditDataObject(
                        AuditUserActivity
                        .New(dto.UserInfo, AuditUserActivityTypeEnum.SavePostCalled)
                        .SetPostID(dto.Entity.ID)
                    )
                    .Save(ProviderFactory.Logging)
                ;
            }

            ITimezoneProvider tz = ProviderFactory.TimeZones;
            TimeZoneInfo timeZone = tz.GetTimezoneForUserID(dto.UserInfo.EffectiveUser.ID.Value);

            #region initial validation

            if (dto.Entity == null)
            {
                dto.Problems.Add("No post entity supplied");
                return dto;
            }
            if (string.IsNullOrWhiteSpace(dto.Entity.Title))
            {
                dto.Problems.Add("Title is required.");
            }
            if (dto.Entity.PostCategoryID <= 0)
            {
                dto.Problems.Add("Post category is required.");
            }
            if ((dto.Entity.Targets == null || !dto.Entity.Targets.Any()) && dto.Entity.Draft == false)
            {
                dto.Problems.Add("If the post is not a draft, targets must be specified.");
            }
            if (dto.Entity.Type == PostTypeEnum.Photo && (dto.Entity.Images == null || !dto.Entity.Images.Any()))
            {
                dto.Problems.Add("\"Image\" post type was specified, but there are no images.");
            }
            if (dto.Entity.Type != PostTypeEnum.Photo && dto.Entity.MasterRevision == null)
            {
                dto.Problems.Add("Non \"Image\" post types require content.");
            }

            if (dto.Entity.MasterRevision != null)
            {
                if (string.IsNullOrWhiteSpace(dto.Entity.MasterRevision.Message)
                    && (dto.Entity.Type == PostTypeEnum.Status || dto.Entity.Type == PostTypeEnum.Event))
                {
                    dto.Problems.Add("\"Event\" and \"Status\" post types require a message.");
                }
                if (string.IsNullOrWhiteSpace(dto.Entity.MasterRevision.Link) && dto.Entity.Type == PostTypeEnum.Link)
                {
                    dto.Problems.Add("\"Link\" post types require a link.");
                }
            }


            //after cursory checks, if there are problems return them
            if (dto.Problems.Any())
                return dto;

            #endregion

            ISecurityProvider security = ProviderFactory.Security;

            using (MainEntities db = ContextFactory.Main)
            {
                #region syndication specific stuff

                bool isSyndication = false;
                long syndicateeFeatureTypeID = 0;
                PostSyndicationOption options = null;

                bool isNew = dto.Entity.ID.GetValueOrDefault(0) <= 0;

                if (dto.Entity.SyndicatorID.HasValue)
                {
                    syndicateeFeatureTypeID = (from s in db.Syndicators where s.ID == dto.Entity.SyndicatorID.Value select s.SyndicateeFeatureTypeID).FirstOrDefault();
                    isSyndication = true;
                    if (!isNew)
                    {
                        options = (from o in db.PostSyndicationOptions where o.PostID == dto.Entity.ID.Value select o).FirstOrDefault();
                    }
                }

                #endregion

                #region post

                List<long> accountIDs = null;
                List<long> credentialIDs = null;

                List<long> accountIDsWithUserPermission = new List<long>();

                credentialIDs = (from t in dto.Entity.Targets select t.CredentialID).Distinct().ToList();
                var credList = (
                    from c in db.Credentials
                    where credentialIDs.Contains(c.ID)
                    select new
                    {
                        CredentialID = c.ID,
                        AccountID = c.AccountID
                    }
                ).ToList();

                accountIDs = (from c in credList select c.AccountID).Distinct().ToList();

                if (!isNew)
                {
                    accountIDs = (
                        from pt in db.PostTargets
                        join c in db.Credentials on pt.CredentialID equals c.ID
                        where pt.PostID == dto.Entity.ID.Value
                        select c.AccountID
                    ).Distinct().ToList();
                }


                //check for the permission to post on all target accounts specified by credentials passed in
                if (dto.Entity.Targets != null && dto.Entity.Targets.Any())
                {


                    foreach (long accountID in accountIDs)
                    {
                        if (isSyndication)
                        {
                            if (!security.HasFeature(accountID, (FeatureTypeEnum)syndicateeFeatureTypeID))
                            {
                                dto.Problems.Add(Locale.Localize("system.nopermission"));
                                return dto;
                            }
                        }
                        else
                        {
                            if (!security.HasPermission(dto.UserInfo.EffectiveUser.ID.Value, accountID, PermissionTypeEnum.social_publish))
                            {
                                //dto.Problems.Add(Locale.Localize("system.nopermission"));
                                //return dto;
                            }
                            else
                            {
                                accountIDsWithUserPermission.Add(accountID);
                            }
                        }
                    }

                    if (!isSyndication)
                    {
                        // new rule is that a user can edit a post even if that user has permission to even only one account
                        if (!accountIDsWithUserPermission.Any())
                        {
                            dto.Problems.Add(Locale.Localize("system.nopermission"));
                            return dto;
                        }

                        // however, if this is a new post, they must have permission for all targets
                        if (isNew && (accountIDsWithUserPermission.Count() != accountIDs.Count()))
                        {
                            dto.Problems.Add(Locale.Localize("system.nopermission"));
                            return dto;
                        }
                    }
                }

                Post post = null;
                if (isNew)
                {
                    post = new Post()
                    {
                        AccountID = dto.UserInfo.EffectiveUser.MemberOfAccountID,
                        UserID = dto.UserInfo.EffectiveUser.ID.Value,
                        PostCategoryID = dto.Entity.PostCategoryID,
                        PostTypeID = (long)dto.Entity.Type,
                        PostSourceID = (long)dto.Entity.Source,
                        Title = dto.Entity.Title.Trim(),
                        DateCreated = DateTime.UtcNow,
                        DateModified = DateTime.UtcNow,
                        SyndicatorID = dto.Entity.SyndicatorID
                    };
                    db.Posts.AddObject(post);
                }
                else
                {
                    post = (from p in db.Posts where p.ID == dto.Entity.ID.Value select p).FirstOrDefault();
                    if (post == null)
                    {
                        dto.Problems.Add("The post to edit was not found.");
                        return dto;
                    }

                    post.DateModified = DateTime.UtcNow;
                    post.PostCategoryID = dto.Entity.PostCategoryID;
                    post.PostTypeID = (long)dto.Entity.Type;
                    post.PostSourceID = (long)dto.Entity.Source;
                    post.Title = dto.Entity.Title.Trim();
                    post.SyndicatorID = dto.Entity.SyndicatorID;
                }

                //set the dates
                if (dto.Entity.ScheduleDate == null)
                {
                    post.ScheduleDate = null;
                }
                else
                {
                    post.ScheduleDate = dto.Entity.ScheduleDate.GetUTCForGivenTimezone(timeZone);
                }
                if (dto.Entity.PostExpirationDate == null)
                {
                    post.PostExpirationDate = null;
                }
                else
                {
                    post.PostExpirationDate = dto.Entity.PostExpirationDate.GetUTCForGivenTimezone(timeZone);
                }
                if (dto.Entity.ApprovalExpirationDate == null)
                {
                    post.ApprovalExpirationDate = null;
                }
                else
                {
                    post.ApprovalExpirationDate = dto.Entity.ApprovalExpirationDate.GetUTCForGivenTimezone(timeZone);
                }

                #endregion

                bool isChanged = false;

                // if true, the user has access to at least one, but not all of the targets, so a post revision for each target
                // they have access to needs to be saved.
                bool targetLevelRevisionMode = (!isSyndication) && (accountIDs.Count() != accountIDsWithUserPermission.Count());

                #region revision

                //get all revisions for this post
                List<PostRevision> allRevisions = (
                    from r in db.PostRevisions
                    where r.PostID == post.ID
                    select r
                ).ToList();

                //get the most current master revision
                PostRevision masterRev = (
                    from r in allRevisions
                    where r.PostTargetID.HasValue == false
                    orderby r.DateCreated descending
                    select r
                ).FirstOrDefault();

                if (!targetLevelRevisionMode)
                {
                    if (masterRev != null)
                    {
                        if (
                            dto.Entity.MasterRevision == null ||
                            (string.IsNullOrWhiteSpace(dto.Entity.MasterRevision.Message) && string.IsNullOrWhiteSpace(dto.Entity.MasterRevision.Link))
                        )
                        {
                            isChanged = true;
                            if (!string.IsNullOrWhiteSpace(masterRev.Message) || !string.IsNullOrWhiteSpace(masterRev.Link))
                            {
                                masterRev = new PostRevision()
                                {
                                    PostID = post.ID,
                                    DateCreated = DateTime.UtcNow,
                                    Message = null,
                                    Link = null,
                                    PostTargetID = null,
                                    LinkTitle = masterRev.LinkTitle,
                                    LinkDescription = masterRev.LinkDescription,
                                    ImageURL = masterRev.ImageURL
                                };
                                db.PostRevisions.AddObject(masterRev);
                            }
                        }
                        else
                        {
                            string oldLink = string.IsNullOrWhiteSpace(masterRev.Link) ? "" : masterRev.Link.Trim();
                            string newLink = string.IsNullOrWhiteSpace(dto.Entity.MasterRevision.Link) ? "" : dto.Entity.MasterRevision.Link.Trim();
                            string oldMsg = string.IsNullOrWhiteSpace(masterRev.Message) ? "" : masterRev.Message.Trim();
                            string newMsg = string.IsNullOrWhiteSpace(dto.Entity.MasterRevision.Message) ? "" : dto.Entity.MasterRevision.Message.Trim();

                            if ((oldLink != newLink) || (oldMsg != newMsg))
                            {
                                isChanged = true;
                                masterRev = new PostRevision()
                                {
                                    PostID = post.ID,
                                    DateCreated = DateTime.UtcNow,
                                    Message = dto.Entity.MasterRevision.Message,
                                    Link = dto.Entity.MasterRevision.Link,
                                    PostTargetID = null,
                                    LinkTitle = masterRev.LinkTitle,
                                    LinkDescription = masterRev.LinkDescription,
                                    ImageURL = masterRev.ImageURL
                                };
                                Auditor
                                    .New(AuditLevelEnum.Information, AuditTypeEnum.UserActivity, dto.UserInfo, "")
                                    .SetAuditDataObject(
                                        AuditUserActivity
                                        .New(dto.UserInfo, AuditUserActivityTypeEnum.SavedPostRevision)
                                        .SetPostID(post.ID)
                                        .SetDescription("SaveEntityDTO<PostDTO> Save(SaveEntityDTO<PostDTO> dto)")
                                    )
                                    .Save(ProviderFactory.Logging)
                                ;
                                db.PostRevisions.AddObject(masterRev);
                            }
                        }
                    }

                    if (masterRev == null && dto.Entity.MasterRevision != null)
                    {
                        isChanged = true;
                        masterRev = new PostRevision()
                        {
                            PostID = post.ID,
                            DateCreated = DateTime.UtcNow,
                            Message = dto.Entity.MasterRevision.Message,
                            Link = dto.Entity.MasterRevision.Link,
                            PostTargetID = null,
                            LinkTitle = dto.Entity.MasterRevision.LinkTitle,
                            LinkDescription = dto.Entity.MasterRevision.LinkDescription,
                            ImageURL = dto.Entity.MasterRevision.ImageURL
                        };
                        db.PostRevisions.AddObject(masterRev);
                    }
                }


                #endregion

                #region targets

                List<PostTarget> postTargets = (from pt in db.PostTargets where pt.PostID == post.ID select pt).ToList();

                if (targetLevelRevisionMode)
                {
                    // with target level revision mode, we should only see targets where the user has access
                    postTargets = (
                        from pt in postTargets
                        join c in credList on pt.CredentialID equals c.CredentialID
                        where accountIDsWithUserPermission.Contains(c.AccountID)
                        select pt
                    ).ToList();
                }

                List<PostTarget> activePostTargetsForRevisions = new List<PostTarget>();
                List<PostTarget> targetsAdded = new List<PostTarget>();

                long accountIDForResolution = 0;
                if (dto.Entity.Targets != null && dto.Entity.Targets.Any())
                {
                    //mark some deleted
                    for (int i = postTargets.Count() - 1; i >= 0; i--)
                    {
                        PostTarget ptarget = postTargets[i];
                        if (!(from pi in dto.Entity.Targets where pi.CredentialID == ptarget.CredentialID select pi).Any())
                        {
                            ptarget.Deleted = true;
                            Auditor
                                .New(AuditLevelEnum.Information, AuditTypeEnum.UserActivity, dto.UserInfo, "")
                                .SetAuditDataObject(
                                    AuditUserActivity
                                    .New(dto.UserInfo, AuditUserActivityTypeEnum.PostTargetDeleted)
                                    .SetPostID(post.ID)
                                    .SetPostTargetID(ptarget.ID)
                                    .SetDescription("SaveEntityDTO<PostDTO> Save(SaveEntityDTO<PostDTO> dto)")
                                )
                                .Save(ProviderFactory.Logging)
                            ;
                        }
                    }

                    //add or mark not deleted
                    foreach (PostTargetDTO item in dto.Entity.Targets)
                    {
                        PostTarget ptarget = (from pt in postTargets where pt.CredentialID == item.CredentialID select pt).FirstOrDefault();
                        if (ptarget != null)
                        {
                            ptarget.Deleted = false;
                            if (isSyndication)
                            {
                                ptarget.NoApproverHoldDate = DateTime.UtcNow;
                            }
                            Auditor
                                .New(AuditLevelEnum.Information, AuditTypeEnum.UserActivity, dto.UserInfo, "")
                                .SetAuditDataObject(
                                    AuditUserActivity
                                    .New(dto.UserInfo, AuditUserActivityTypeEnum.PostTargetUndeleted)
                                    .SetPostID(post.ID)
                                    .SetPostTargetID(ptarget.ID)
                                    .SetDescription("SaveEntityDTO<PostDTO> Save(SaveEntityDTO<PostDTO> dto)")
                                )
                                .Save(ProviderFactory.Logging)
                            ;
                            activePostTargetsForRevisions.Add(ptarget);
                        }
                        else
                        {
                            ptarget = new PostTarget()
                            {
                                PostID = post.ID,
                                CredentialID = item.CredentialID,
                                Deleted = false
                            };
                            db.PostTargets.AddObject(ptarget);
                            postTargets.Add(ptarget);
                            targetsAdded.Add(ptarget);
                            if (isSyndication)
                            {
                                ptarget.NoApproverHoldDate = DateTime.UtcNow;
                            }
                            activePostTargetsForRevisions.Add(ptarget);
                        }
                    }

                    // get the timezones for each account targetted
                    List<long> targetCredentialIDs = (from t in dto.Entity.Targets select t.CredentialID).ToList();
                    var tzlist = (
                        from c in db.Credentials
                        join a in db.Accounts on c.AccountID equals a.ID
                        join t in db.Timezones on a.TimezoneID equals t.ID
                        where targetCredentialIDs.Contains(c.ID)
                        select new
                        {
                            c.ID,
                            t.MicrosoftID,
                            AccountID = a.ID
                        }
                    ).ToList();

                    accountIDForResolution = tzlist.First().AccountID;

                    //set the corrected dates
                    foreach (PostTarget item in postTargets)
                    {

                        string tzID = (from t in tzlist where t.ID == item.CredentialID select t.MicrosoftID).FirstOrDefault();
                        if (!string.IsNullOrWhiteSpace(tzID))
                        {
                            TimeZoneInfo tzInfo = TimeZoneInfo.FindSystemTimeZoneById(tzID);
                            item.ScheduleDate = getTargetCorrectedUTCForScheduleDate(dto.Entity.ScheduleDate, tzInfo);
                            item.PostExpirationDate = getTargetCorrectedUTCForScheduleDate(dto.Entity.PostExpirationDate, tzInfo);
                        }
                    }

                    // add post target revisions if in revision mode
                    if (targetLevelRevisionMode)
                    {
                        db.SaveChanges();
                        foreach (PostTarget ptarget in activePostTargetsForRevisions)
                        {
                            PostRevision revision = new PostRevision()
                            {
                                PostID = post.ID,
                                PostTargetID = ptarget.ID,
                                DateCreated = DateTime.UtcNow,
                                Message = dto.Entity.MasterRevision.Message,
                                Link = dto.Entity.MasterRevision.Link,
                                LinkTitle = dto.Entity.MasterRevision.LinkTitle,
                                LinkDescription = dto.Entity.MasterRevision.LinkDescription,
                                ImageURL = dto.Entity.MasterRevision.ImageURL
                            };
                            db.PostRevisions.AddObject(revision);
                        }
                    }
                }
                else
                {
                    foreach (PostTarget item in postTargets)
                    {
                        item.Deleted = true;
                        Auditor
                            .New(AuditLevelEnum.Information, AuditTypeEnum.UserActivity, dto.UserInfo, "")
                            .SetAuditDataObject(
                                AuditUserActivity
                                .New(dto.UserInfo, AuditUserActivityTypeEnum.PostTargetDeleted)
                                .SetPostID(post.ID)
                                .SetPostTargetID(item.ID)
                                .SetDescription("SaveEntityDTO<PostDTO> Save(SaveEntityDTO<PostDTO> dto)")
                            )
                            .Save(ProviderFactory.Logging)
                        ;
                    }
                }

                db.SaveChanges();

                #endregion

                #region images

                List<PostImage> allPostImages = (from pi in db.PostImages where pi.PostID == post.ID select pi).ToList();

                if (targetLevelRevisionMode)
                {
                    if (dto.Entity.Images == null || (!dto.Entity.Images.Any()))
                    {
                        // no images, set the targets appropriately
                        foreach (PostTarget ptarget in activePostTargetsForRevisions)
                        {
                            ptarget.NoImagesSelected = true;
                        }
                        db.SaveChanges();
                    }
                    else
                    {
                        foreach (PostTarget ptarget in activePostTargetsForRevisions)
                        {
                            List<PostImage> postImages = (from p in allPostImages where p.PostTargetID.HasValue && p.PostTargetID == ptarget.ID select p).ToList();

                            //remove images that aren't in the edited post target
                            for (int i = postImages.Count() - 1; i >= 0; i--)
                            {
                                PostImage pimage = postImages[i];
                                if (!(from pi in dto.Entity.Images where pi.URL.Trim() == pimage.URL.Trim() select pi).Any())
                                {
                                    isChanged = true;
                                    db.PostImages.DeleteObject(pimage);
                                    postImages.RemoveAt(i);
                                }
                            }
                            //add images that are in the edited post target, but not in the database
                            foreach (PostImageDTO item in dto.Entity.Images)
                            {
                                if (!(from pi in postImages where pi.URL.Trim() == item.URL.Trim() select pi).Any())
                                {
                                    isChanged = true;
                                    PostImage pimage = new PostImage()
                                    {
                                        PostID = post.ID,
                                        PostTargetID = ptarget.ID,
                                        URL = item.URL
                                    };
                                    db.PostImages.AddObject(pimage);
                                    postImages.Add(pimage);
                                }
                            }
                        }
                    }
                }
                else
                {
                    // only select master revision images
                    List<PostImage> postImages = (from p in allPostImages where p.PostTargetID.HasValue == false select p).ToList();

                    if (dto.Entity.Images != null && dto.Entity.Images.Any())
                    {
                        //remove images that aren't in the edited post
                        for (int i = postImages.Count() - 1; i >= 0; i--)
                        {
                            PostImage pimage = postImages[i];
                            if (!(from pi in dto.Entity.Images where pi.URL.Trim() == pimage.URL.Trim() select pi).Any())
                            {
                                isChanged = true;
                                db.PostImages.DeleteObject(pimage);
                                postImages.RemoveAt(i);
                            }
                        }
                        //add images that are in the edited post, but not in the database
                        foreach (PostImageDTO item in dto.Entity.Images)
                        {
                            if (!(from pi in postImages where pi.URL.Trim() == item.URL.Trim() select pi).Any())
                            {
                                isChanged = true;
                                PostImage pimage = new PostImage()
                                {
                                    PostID = post.ID,
                                    URL = item.URL
                                };
                                db.PostImages.AddObject(pimage);
                                postImages.Add(pimage);
                            }
                        }
                    }
                    else
                    {
                        //remove any images
                        foreach (PostImage item in postImages)
                        {
                            isChanged = true;
                            db.PostImages.DeleteObject(item);
                        }
                    }
                }

                db.SaveChanges();

                #endregion

                #region Approvals

                if (isSyndication && dto.Entity.SyndicationOptions != null)
                {
                    if (options == null)
                    {
                        options = new PostSyndicationOption() { PostID = post.ID, DateCreated = DateTime.UtcNow };
                    }

                    if (dto.Entity.SyndicationOptions.AllowChangeLinkURL)
                    {
                        dto.Entity.SyndicationOptions.AllowChangeLinkThumb = true;
                        dto.Entity.SyndicationOptions.AllowChangeLinkText = true;
                    }
                    options.DateModified = DateTime.UtcNow;
                    options.AllowChangePublishDate = dto.Entity.SyndicationOptions.AllowChangePublishDate;
                    options.AllowChangeMessage = dto.Entity.SyndicationOptions.AllowChangeMessage;
                    options.AllowChangeTargets = dto.Entity.SyndicationOptions.AllowChangeTargets;
                    options.AllowChangeLinkURL = dto.Entity.SyndicationOptions.AllowChangeLinkURL;
                    options.AllowChangeLinkThumb = dto.Entity.SyndicationOptions.AllowChangeLinkThumb;
                    options.AllowChangeLinkText = dto.Entity.SyndicationOptions.AllowChangeLinkText;
                    options.AllowChangeImage = dto.Entity.SyndicationOptions.AllowChangeImage;
                    options.ForbidFacebookTargetChange = dto.Entity.SyndicationOptions.ForbidFacebookTargetChange;
                    options.ForbidGoogleTargetChange = dto.Entity.SyndicationOptions.ForbidGoogleTargetChange;
                    options.ForbidTwitterTargetChange = dto.Entity.SyndicationOptions.ForbidTwitterTargetChange;
                    options.RequireImage = dto.Entity.SyndicationOptions.RequireImage;
                    options.RequireLinkURL = dto.Entity.SyndicationOptions.RequireLinkURL;
                    options.RequireMessage = dto.Entity.SyndicationOptions.RequireMessage;
                    options.MaxDaysAfterOriginalPublishDate = dto.Entity.SyndicationOptions.MaxDaysAfterOriginalPublishDate;
                    options.MaxDaysBeforeOriginalPublishDate = dto.Entity.SyndicationOptions.MaxDaysBeforeOriginalPublishDate;

                    if (options.EntityState == EntityState.Detached)
                        db.PostSyndicationOptions.AddObject(options);

                    db.SaveChanges();
                }

                if (isNew || isChanged)
                {
                    //InitPostApprovals(post.ID, dto.UserInfo.EffectiveUser.ID.Value);
                    string sql = string.Format(
                        " if not exists (select * from PostApprovalsInitQueue where PostID={0}) " +
                        " insert PostApprovalsInitQueue(PostID,UserIDAlreadyApproved,DateCreated) values ({0}, {1}, GetDate())"
                    , post.ID, dto.UserInfo.EffectiveUser.ID.Value);
                    execNonQuery(sql);
                }

                #endregion

                #region return the post


                GetEntityDTO<PostDTO> getPost = getPostByID(dto.UserInfo, post.ID, true, accountIDForResolution);
                if (getPost.HasProblems)
                {
                    dto.Problems.AddRange(getPost.Problems);
                    return dto;
                }

                dto.Entity = getPost.Entity;

                string sqlQ = string.Format(
                    "if not exists (select * from [UpdateQueue.Posts] where PostID={0}) insert [UpdateQueue.Posts](PostID) values ({0})",
                    dto.Entity.ID.Value
                );
                execNonQuery(sqlQ);

                //UpdateQueue_Posts q = new UpdateQueue_Posts()
                //{
                //    PostID = dto.Entity.ID.Value
                //};
                //db.UpdateQueue_Posts.AddObject(q);
                //db.SaveChanges();

                if (isNew)
                {
                    Auditor
                        .New(AuditLevelEnum.Information, AuditTypeEnum.UserActivity, dto.UserInfo, "")
                        .SetAuditDataObject(
                            AuditUserActivity
                            .New(dto.UserInfo, AuditUserActivityTypeEnum.SavedPost)
                            .SetPostID(dto.Entity.ID.Value)
                        )
                        .Save(ProviderFactory.Logging)
                    ;
                    Auditor
                        .New(AuditLevelEnum.Information, AuditTypeEnum.PublishActivity, dto.UserInfo, "")
                        .SetAuditDataObject(
                            AuditPublishActivity
                            .New(dto.Entity.ID.Value, AuditPublishActivityTypeEnum.SavedNew)
                            .SetPostID(dto.Entity.ID.Value)
                        )
                        .Save(ProviderFactory.Logging)
                    ;
                    if (dto.Entity.SyndicatorID.HasValue)
                    {
                        Auditor
                            .New(AuditLevelEnum.Information, AuditTypeEnum.UserActivity, dto.UserInfo, "")
                            .SetAuditDataObject(
                                AuditUserActivity
                                .New(dto.UserInfo, AuditUserActivityTypeEnum.SavedPostForSyndication)
                                .SetPostID(dto.Entity.ID.Value)
                                .SetSyndicatorID(dto.Entity.SyndicatorID.Value)
                            )
                            .Save(ProviderFactory.Logging)
                        ;
                    }
                }
                else
                {
                    Auditor
                        .New(AuditLevelEnum.Information, AuditTypeEnum.PublishActivity, dto.UserInfo, "")
                        .SetAuditDataObject(
                            AuditPublishActivity
                            .New(dto.Entity.ID.Value, AuditPublishActivityTypeEnum.SavedExisting)
                            .SetPostID(dto.Entity.ID.Value)
                        )
                        .Save(ProviderFactory.Logging)
                    ;
                }
                return dto;

                #endregion
            }

        }

        private DateTime? getTargetCorrectedUTCForScheduleDate(SIDateTime scheduleDate, TimeZoneInfo targetTimeZone)
        {
            if (scheduleDate == null)
                return null;
            else
                return TimeZoneInfo.ConvertTimeToUtc(scheduleDate.LocalDate.Value, targetTimeZone);

        }

        private GetEntityDTO<PostDTO> getPostByID(UserInfoDTO userInfo, long postID, bool skipSecurityChecks, long? accountIDForOriginalPostCalculation)
        {
            ITimezoneProvider tz = ProviderFactory.TimeZones;
            TimeZoneInfo timeZoneLoggedInUser = tz.GetTimezoneForUserID(userInfo.EffectiveUser.ID.Value);


            ISecurityProvider security = ProviderFactory.Security;

            GetEntityDTO<PostDTO> dto = new GetEntityDTO<PostDTO>();

            if (!security.HasPermission(userInfo.EffectiveUser.ID.Value, PermissionTypeEnum.social_publish) && !skipSecurityChecks)
            {
                dto.Problems.Add(Locale.Localize("system.nopermission"));
                return dto;
            }

            using (MainEntities db = ContextFactory.Main)
            {

                Post post = (from p in db.Posts where p.ID == postID select p).FirstOrDefault();

                if (post == null)
                {
                    dto.Problems.Add("That post was not found.");
                    return dto;
                }

                PostDTO postDTO = new PostDTO()
                {
                    ID = post.ID,
                    Title = post.Title,
                    Source = (PostSourceEnum)post.PostSourceID,
                    PostCategoryID = post.PostCategoryID,
                    Type = (PostTypeEnum)post.PostTypeID,
                    DateCreated = new SIDateTime(post.DateCreated, timeZoneLoggedInUser),
                    DateModified = new SIDateTime(post.DateModified, timeZoneLoggedInUser),
                    Draft = post.Draft,
                    SyndicatorID = post.SyndicatorID,
                    ScheduleDate = post.ScheduleDate.HasValue ? new SIDateTime(post.ScheduleDate.Value, timeZoneLoggedInUser) : null,
                    PostExpirationDate = post.PostExpirationDate.HasValue ? new SIDateTime(post.PostExpirationDate.Value, timeZoneLoggedInUser) : null,
                    ApprovalExpirationDate = post.ApprovalExpirationDate.HasValue ? new SIDateTime(post.ApprovalExpirationDate.Value, timeZoneLoggedInUser) : null,
                    PostedByUserID = post.UserID
                };

                bool isSyndication = postDTO.SyndicatorID.HasValue && postDTO.SyndicatorID.Value > 0;
                long? syndicatorAccountID = null;
                if (isSyndication)
                {
                    syndicatorAccountID = (from s in db.Syndicators where s.ID == postDTO.SyndicatorID.Value select s.AccountID).FirstOrDefault();
                }

                // get the targets
                var targets = (
                    from t in db.PostTargets
                    join c in db.Credentials on t.CredentialID equals c.ID
                    where t.PostID == postID
                    && t.Deleted == false
                    select new
                    {
                        t.ID,
                        t.CredentialID,
                        c.AccountID,
                        t.ScheduleDate,
                        t.PickedUpDate,
                        t.PublishedDate,
                        t.ResultID,
                        t.PostExpirationDate,
                        t.NoImagesSelected
                    }
                ).ToList();

                var targetsWithUserPermission = (from t in targets select t).ToList();

                foreach (var item in targets)
                {
                    if (
                        (
                            (!isSyndication && !security.HasPermission(userInfo.EffectiveUser.ID.Value, item.AccountID, PermissionTypeEnum.social_publish))
                            ||
                            (isSyndication && !security.HasPermission(userInfo.EffectiveUser.ID.Value, syndicatorAccountID.Value, PermissionTypeEnum.social_publish))
                        )
                        && !skipSecurityChecks
                    )
                    {
                        targetsWithUserPermission.Remove(item);
                    }
                    else
                    {

                        PostTargetStatusEnum status = PostTargetStatusEnum.Scheduled;
                        if (postDTO.Draft == true)
                        {
                            status = PostTargetStatusEnum.Draft;
                        }
                        else if (item.PublishedDate.HasValue)
                        {
                            status = PostTargetStatusEnum.Published;
                        }
                        else if (item.PickedUpDate.HasValue)
                        {
                            status = PostTargetStatusEnum.Processing;
                        }
                        else if (item.PostExpirationDate.HasValue && item.PostExpirationDate.Value > DateTime.UtcNow && !item.PublishedDate.HasValue)
                        {
                            status = PostTargetStatusEnum.Expired;
                        }
                        else if (item.ScheduleDate.HasValue && item.ScheduleDate.Value > DateTime.UtcNow)
                        {
                            status = PostTargetStatusEnum.Scheduled;
                        }

                        postDTO.Targets.Add(new PostTargetDTO()
                        {
                            ID = item.ID,
                            CredentialID = item.CredentialID,
                            DateScheduled = item.ScheduleDate.HasValue ? new SIDateTime(item.ScheduleDate.Value, timeZoneLoggedInUser) : null,
                            TimeUntilScheduled = item.ScheduleDate.HasValue ? (item.ScheduleDate.Value - DateTime.UtcNow) : default(TimeSpan),
                            Status = status
                        });
                    }
                }

                if (!targetsWithUserPermission.Any())
                {
                    dto.Problems.Add("User does not have publish permissions for any targets in this post.");
                    return dto;
                }

                bool revisionMode = (targetsWithUserPermission.Count() != targets.Count());
                postDTO.TotalTargetCount = targets.Count();
                postDTO.TargetsUserCanSeeCount = targetsWithUserPermission.Count();
                postDTO.RevisionMode = revisionMode;

                // get the images
                List<PostImage> allImages = (from pi in db.PostImages where pi.PostID == postID select pi).ToList();
                List<PostImage> images = null;

                if (revisionMode)
                {
                    images = (
                        from pi in allImages
                        join pt in targetsWithUserPermission on pi.PostTargetID.GetValueOrDefault() equals pt.ID
                        select pi
                    ).ToList();

                    if (images.Any())
                    {
                        // find the first target with image revisions, and use the image list from that target...
                        long ptID = images.First().PostTargetID.Value;
                        images = (from i in images where i.PostTargetID.Value == ptID select i).ToList();
                    }
                    else
                    {
                        if ((from t in targetsWithUserPermission where !t.NoImagesSelected.HasValue || t.NoImagesSelected.Value == false select t).Any())
                        {
                            // if there exists at least one target with NoImagesSelected=false or is null, use the master image list
                            images = (from i in allImages where i.PostTargetID.HasValue == false select i).ToList();
                        }
                        else
                        {
                            // otherwise assume images were deselected and we have an empty image list
                            images = new List<PostImage>();
                        }
                    }
                }
                else
                {
                    images = (from i in allImages where i.PostTargetID.HasValue == false select i).ToList();
                }

                foreach (PostImage item in images)
                {
                    postDTO.Images.Add(new PostImageDTO()
                    {
                        ID = item.ID,
                        URL = item.URL
                    });
                }

                List<PostRevision> allRevisions = (from r in db.PostRevisions where r.PostID == postID select r).ToList();

                PostRevision rev = null;

                if (revisionMode)
                {

                    // see if from the available targets, there exists different content
                    List<string> targetSigs = new List<string>();
                    bool foundDiff = false;
                    foreach (var target in targetsWithUserPermission)
                    {
                        // find the last revison at the target level
                        PostRevision trev = (
                            from r in allRevisions
                            where r.PostTargetID.HasValue && r.PostTargetID.Value == target.ID
                            orderby r.DateCreated descending
                            select r
                        ).FirstOrDefault();

                        // if no target revision, find the last master revision
                        if (trev == null)
                        {
                            trev = (from r in allRevisions where r.PostID == postID && r.PostTargetID == null orderby r.DateCreated descending select r).FirstOrDefault();
                        }

                        // fnd the image list for this target
                        List<string> timages = new List<string>();

                        if (target.NoImagesSelected.HasValue && target.NoImagesSelected.Value)
                        {
                            //no images selected, move on
                        }
                        else
                        {
                            timages = (from i in allImages where i.PostTargetID.HasValue && i.PostTargetID.Value == target.ID orderby i.URL select i.URL).ToList();
                            if (!timages.Any())
                            {
                                timages = (from i in allImages where !i.PostTargetID.HasValue orderby i.URL select i.URL).ToList();
                            }
                        }

                        if (trev != null)
                        {
                            //create a revision signature
                            string tsig = "";
                            if (!string.IsNullOrWhiteSpace(trev.Message))
                            {
                                tsig = string.Format("{0}|{1}", tsig, trev.Message.Trim().ToLower().Replace(" ", "").Replace("\r", "").Replace("\n", ""));
                            }
                            if (!string.IsNullOrWhiteSpace(trev.Link))
                            {
                                tsig = string.Format("{0}|{1}", tsig, trev.Link.Trim().ToLower().Replace(" ", "").Replace("\r", "").Replace("\n", ""));
                            }
                            if (!string.IsNullOrWhiteSpace(trev.ImageURL))
                            {
                                tsig = string.Format("{0}|{1}", tsig, trev.ImageURL.Trim().ToLower().Replace(" ", "").Replace("\r", "").Replace("\n", ""));
                            }
                            if (!string.IsNullOrWhiteSpace(trev.LinkDescription))
                            {
                                tsig = string.Format("{0}|{1}", tsig, trev.LinkDescription.Trim().ToLower().Replace(" ", "").Replace("\r", "").Replace("\n", ""));
                            }
                            if (!string.IsNullOrWhiteSpace(trev.LinkTitle))
                            {
                                tsig = string.Format("{0}|{1}", tsig, trev.LinkTitle.Trim().ToLower().Replace(" ", "").Replace("\r", "").Replace("\n", ""));
                            }
                            if (timages.Any())
                            {
                                foreach (string imageURL in timages)
                                {
                                    if (!string.IsNullOrWhiteSpace(imageURL))
                                    {
                                        tsig = string.Format("{0}|{1}", tsig, imageURL.Trim().ToLower().Replace(" ", "").Replace("\r", "").Replace("\n", ""));
                                    }
                                }
                            }

                            if (!targetSigs.Contains(tsig) && targetSigs.Any())
                            {
                                foundDiff = true;
                                break;
                            }
                            targetSigs.Add(tsig);
                        }
                    }

                    if (!foundDiff) // noDupes means that non of the current revisions for any target have different content
                    {
                        // get the latest target level revision
                        rev = (
                            from r in allRevisions
                            join t in targetsWithUserPermission on r.PostTargetID.GetValueOrDefault() equals t.ID
                            orderby r.DateCreated descending
                            select r
                        ).FirstOrDefault();
                    }
                    else
                    {
                        // get the latest master revision
                        rev = (from r in allRevisions where r.PostID == postID && r.PostTargetID == null orderby r.DateCreated descending select r).FirstOrDefault();
                    }
                }

                if (rev == null)
                {
                    //get the current master revision
                    rev = (from r in allRevisions where r.PostID == postID && r.PostTargetID == null orderby r.DateCreated descending select r).FirstOrDefault();
                }

                if (rev != null)
                {
                    postDTO.MasterRevision = new PostRevisionDTO()
                    {
                        ID = rev.ID,
                        Link = rev.Link,
                        Message = rev.Message,
                        LinkDescription = rev.LinkDescription,
                        LinkTitle = rev.LinkTitle,
                        ImageURL = rev.ImageURL,
                        DateCreated = new SIDateTime(rev.DateCreated, timeZoneLoggedInUser)
                    };
                }

                SIDateTime scheduleDateLoggedInUser = getScheduledDateForLoggedInUser(accountIDForOriginalPostCalculation, post.UserID, post.ScheduleDate, timeZoneLoggedInUser);
                postDTO.SyndicationOptions = getSyndicationOptionsForPost(postID, scheduleDateLoggedInUser);

                dto.Entity = postDTO;
                return dto;

            }
        }
        public GetEntityDTO<PostDTO> GetPostByID(UserInfoDTO userInfo, long postID, bool openingForEdit)
        {
            if (openingForEdit)
            {
                Auditor
                    .New(AuditLevelEnum.Information, AuditTypeEnum.UserActivity, userInfo, "GetPostForRevision called")
                    .SetAuditDataObject(
                        AuditUserActivity
                        .New(userInfo, AuditUserActivityTypeEnum.PostOpenedForRevision)
                        .SetPostID(postID)
                        .SetDescription("GetPostByID(UserInfoDTO userInfo, {0}, true)", postID)
                    )
                    .Save(ProviderFactory.Logging)
                ;
            }
            return getPostByID(userInfo, postID, false, null);
        }

        private SIDateTime getScheduledDateForLoggedInUser(long? accountIDForOriginalPostCalculation, long postedByUserID, DateTime? postScheduleDate, TimeZoneInfo timeZoneLoggedInUser)
        {
            using (MainEntities db = ContextFactory.Main)
            {
                TimeZoneInfo timeZoneTargetAccount = null;
                if (accountIDForOriginalPostCalculation.HasValue)
                {
                    timeZoneTargetAccount = ProviderFactory.TimeZones.GetTimezoneForAccountID(accountIDForOriginalPostCalculation.Value);
                }
                else
                {
                    long memberAccountID = (from u in db.Users where u.ID == postedByUserID select u.MemberOfAccountID).FirstOrDefault();
                    timeZoneTargetAccount = ProviderFactory.TimeZones.GetTimezoneForAccountID(memberAccountID);
                }
                TimeZoneInfo timeZoneOriginalPoster = ProviderFactory.TimeZones.GetTimezoneForUserID(postedByUserID);
                if (timeZoneTargetAccount == null)
                {
                    timeZoneTargetAccount = timeZoneOriginalPoster;
                }

                // get the schedule date corrected to the timezone of the original poster
                SIDateTime scheduleDatePoster = new SIDateTime(postScheduleDate.HasValue ? postScheduleDate.Value : DateTime.UtcNow, timeZoneOriginalPoster);

                // assume the same time of day for the target timezone to get the original schedule date
                SIDateTime scheduleDateTarget = new SIDateTime(scheduleDatePoster.LocalDate.Value, timeZoneTargetAccount);

                // now correct that date for the user logged in
                SIDateTime scheduleDateLoggedInUser = new SIDateTime(scheduleDateTarget.LocalDate.Value, timeZoneLoggedInUser);

                return scheduleDateLoggedInUser;
            }
        }

        private SyndicationPublishOptionsDTO getSyndicationOptionsForPost(long postID, SIDateTime scheduleDateLoggedInUser)
        {
            using (MainEntities db = ContextFactory.Main)
            {
                SyndicationPublishOptionsDTO sopts = new SyndicationPublishOptionsDTO(scheduleDateLoggedInUser);
                PostSyndicationOption option = (from o in db.PostSyndicationOptions where o.PostID == postID select o).FirstOrDefault();
                if (option != null)
                {
                    sopts.AllowChangeMessage = option.AllowChangeMessage;
                    sopts.AllowChangeImage = option.AllowChangeImage;

                    if (option.AllowChangeLinkURL)
                    {
                        option.AllowChangeLinkThumb = true;
                        option.AllowChangeLinkText = true;
                    }

                    sopts.AllowChangeLinkText = option.AllowChangeLinkText;
                    sopts.AllowChangeLinkThumb = option.AllowChangeLinkThumb;
                    sopts.AllowChangeLinkURL = option.AllowChangeLinkURL;
                    sopts.AllowChangePublishDate = option.AllowChangePublishDate;
                    sopts.AllowChangeTargets = option.AllowChangeTargets;
                    sopts.ForbidFacebookTargetChange = option.ForbidFacebookTargetChange;
                    sopts.ForbidGoogleTargetChange = option.ForbidGoogleTargetChange;
                    sopts.ForbidTwitterTargetChange = option.ForbidTwitterTargetChange;
                    sopts.MaxDaysAfterOriginalPublishDate = option.MaxDaysAfterOriginalPublishDate;
                    sopts.MaxDaysBeforeOriginalPublishDate = option.MaxDaysBeforeOriginalPublishDate;
                    sopts.RequireMessage = option.RequireMessage;
                    sopts.RequireImage = option.RequireImage;
                    sopts.RequireLinkURL = option.RequireLinkURL;
                    sopts.PickSingleImage = option.PickSingleImage;
                }
                else
                {
                    sopts.AllowChangeMessage = true;
                    sopts.AllowChangeImage = true;

                    sopts.AllowChangeLinkText = true;
                    sopts.AllowChangeLinkThumb = true;
                    sopts.AllowChangeLinkURL = true;
                    sopts.AllowChangePublishDate = true;
                    sopts.AllowChangeTargets = true;
                    sopts.ForbidFacebookTargetChange = false;
                    sopts.ForbidGoogleTargetChange = false;
                    sopts.ForbidTwitterTargetChange = false;
                    sopts.MaxDaysAfterOriginalPublishDate = null;
                    sopts.MaxDaysBeforeOriginalPublishDate = null;
                    sopts.RequireMessage = false;
                    sopts.RequireImage = false;
                    sopts.RequireLinkURL = false;
                    sopts.PickSingleImage = false;
                }

                return sopts;
            }
        }

        public SaveEntityDTO<RevisePostDTO> SavePostRevisionForAccount(SaveEntityDTO<RevisePostDTO> revision)
        {
            Auditor
                .New(AuditLevelEnum.Information, AuditTypeEnum.UserActivity, revision.UserInfo, "GetPostForRevision called")
                .SetAuditDataObject(
                    AuditUserActivity
                    .New(revision.UserInfo, AuditUserActivityTypeEnum.AttemptingToSavePostForRevision)
                    .SetPostID(revision.Entity.PostID)
                    .SetAccountID(revision.Entity.AccountID)
                    .SetDescription("SavePostRevisionForAccount(userInfo,{0},{1})", revision.Entity.PostID, revision.Entity.AccountID)
                )
                .Save(ProviderFactory.Logging)
            ;

            revision.Problems.Clear();

            TimeZoneInfo timeZone = ProviderFactory.TimeZones.GetTimezoneForUserID(revision.UserInfo.EffectiveUser.ID.Value);
            bool hasAdmin = ProviderFactory.Security.HasPermission(revision.UserInfo.EffectiveUser.ID.Value, revision.Entity.AccountID, PermissionTypeEnum.accounts_admin);
            bool hasPublish = ProviderFactory.Security.HasPermission(revision.UserInfo.EffectiveUser.ID.Value, revision.Entity.AccountID, PermissionTypeEnum.social_publish);
            bool hasApprover = ProviderFactory.Security.HasPermission(revision.UserInfo.EffectiveUser.ID.Value, revision.Entity.AccountID, PermissionTypeEnum.social_approveposts);

            if (!hasAdmin && !hasApprover && !hasPublish)
            {
                revision.Problems.Add(Locale.Localize("system.nopermission"));
            }

            if (revision.Problems.Any())
                return revision;

            GetEntityDTO<RevisePostDTO> oEnt = GetPostForRevision(revision.UserInfo, revision.Entity.PostID, revision.Entity.AccountID);
            if (oEnt.Problems.Count() > 0)
            {
                revision.Problems.AddRange(oEnt.Problems);
                return revision;
            }

            RevisePostDTO original = oEnt.Entity;
            RevisePostDTO changed = revision.Entity;

            DateTime? oldSchedDateUTC = (original.ScheduleDate == null) ? null : (DateTime?)original.ScheduleDate.GetUTCDate(timeZone);
            DateTime? newSchedDateUTC = (changed.ScheduleDate == null) ? null : (DateTime?)changed.ScheduleDate.GetUTCDate(timeZone);

            List<SocialNetworkEnum> oldSelectedNets = (from x in original.SocialNetworks where x.Selected == true select x.SocialNetwork).ToList();
            List<SocialNetworkEnum> newSelectedNets = (from x in changed.SocialNetworks where x.Selected == true select x.SocialNetwork).ToList();
            List<SocialNetworkEnum> networksAdded = (from x in newSelectedNets where !oldSelectedNets.Contains(x) select x).ToList();
            List<SocialNetworkEnum> networksRemoved = (from x in oldSelectedNets where !newSelectedNets.Contains(x) select x).ToList();

            string originalMessage = string.IsNullOrWhiteSpace(original.Message) ? "" : original.Message;
            string newMessage = string.IsNullOrWhiteSpace(changed.Message) ? "" : changed.Message;
            string originalLinkText = string.IsNullOrWhiteSpace(original.LinkDescription) ? "" : original.LinkDescription;
            string newLinkText = string.IsNullOrWhiteSpace(changed.LinkDescription) ? "" : changed.LinkDescription;
            string originalLinkURL = string.IsNullOrWhiteSpace(original.Link) ? "" : original.Link;
            string newLinkURL = string.IsNullOrWhiteSpace(changed.Link) ? "" : changed.Link;
            string originalLinkThumbURL = string.IsNullOrWhiteSpace(original.LinkThumbURL) ? "" : original.LinkThumbURL;
            string newLinkThumbURL = string.IsNullOrWhiteSpace(changed.LinkThumbURL) ? "" : changed.LinkThumbURL;
            string originalLinkTitle = string.IsNullOrWhiteSpace(original.LinkTitle) ? "" : original.LinkTitle;
            string newLinkTitle = string.IsNullOrWhiteSpace(changed.LinkTitle) ? "" : changed.LinkTitle;

            #region check syndication options for problems

            if (original.SyndicationOptions != null)
            {
                SyndicationPublishOptionsDTO opts = original.SyndicationOptions;

                if (!opts.AllowChangePublishDate)
                {
                    if (oldSchedDateUTC.GetValueOrDefault(DateTime.Now.Date) != newSchedDateUTC.GetValueOrDefault(DateTime.Now.Date))
                    {
                        revision.Problems.Add("Changes to the scheduled date are not allowed.");
                    }
                }
                else
                {
                    int daysDiff = Convert.ToInt32(Math.Abs((oldSchedDateUTC.GetValueOrDefault(DateTime.UtcNow) - newSchedDateUTC.GetValueOrDefault(DateTime.UtcNow)).TotalDays));

                    if (opts.MaxDaysAfterOriginalPublishDate.HasValue
                        && newSchedDateUTC.Value > oldSchedDateUTC.Value
                        && daysDiff > opts.MaxDaysAfterOriginalPublishDate.Value
                    )
                    {
                        revision.Problems.Add("Scheduled date is too far in the future.");
                    }
                    if (opts.MaxDaysBeforeOriginalPublishDate.HasValue
                        && newSchedDateUTC.Value < oldSchedDateUTC.Value
                        && daysDiff > opts.MaxDaysBeforeOriginalPublishDate.Value
                    )
                    {
                        revision.Problems.Add("Scheduled date is too far in the past.");
                    }
                }

                if (opts.RequireImage)
                {
                    if (changed.PostImages.Count() == 0)
                    {
                        revision.Problems.Add("At least one image is required.");
                    }
                }
                if (opts.RequireLinkURL)
                {
                    if (string.IsNullOrWhiteSpace(changed.Link))
                    {
                        revision.Problems.Add("Link URL is required.");
                    }
                }
                if (opts.RequireMessage)
                {
                    if (string.IsNullOrWhiteSpace(changed.Message))
                    {
                        revision.Problems.Add("A message is required.");
                    }
                }
                if (opts.PickSingleImage)
                {
                    if (changed.PostImages.Count() != 1)
                    {
                        revision.Problems.Add("One image must be selected.");
                    }
                    else
                    {
                        long iid = changed.PostImages[0].ID.GetValueOrDefault(0);
                        if (!(
                            from o in original.PostImages
                            where o.ID == iid
                            select o
                        ).Any())
                        {
                            revision.Problems.Add("One image must be selected from the list of original post images.");
                        }
                    }
                }

                if (!opts.AllowChangeImage)
                {
                    if ((from c in changed.PostImages where c.ID == null select c).Any())
                    {
                        revision.Problems.Add("Changes to images are not allowed.");
                    }
                    else
                    {
                        List<long> newImageIDs = (from c in changed.PostImages select c.ID.Value).ToList();
                        List<long> oldImageIDs = (from c in original.PostImages select c.ID.Value).ToList();
                        if (
                            (from i in newImageIDs where !oldImageIDs.Contains(i) select i).Any()
                            || (from i in oldImageIDs where !newImageIDs.Contains(i) select i).Any()
                        )
                        {
                            revision.Problems.Add("Changes to images are not allowed.");
                        }
                    }
                }

                if (!opts.AllowChangeTargets)
                {
                    if (networksAdded.Any() || networksRemoved.Any())
                    {
                        revision.Problems.Add("Changes to social network targets is not allowed.");
                    }
                }
                else
                {
                    if (
                        opts.ForbidFacebookTargetChange &&
                        (networksAdded.Contains(SocialNetworkEnum.Facebook) || networksRemoved.Contains(SocialNetworkEnum.Facebook))
                    )
                    {
                        revision.Problems.Add("Changes to Facebook target are not allowed.");
                    }
                    if (
                        opts.ForbidTwitterTargetChange &&
                        (networksAdded.Contains(SocialNetworkEnum.Twitter) || networksRemoved.Contains(SocialNetworkEnum.Twitter))
                    )
                    {
                        revision.Problems.Add("Changes to Twitter target are not allowed.");
                    }
                    if (
                        opts.ForbidGoogleTargetChange &&
                        (networksAdded.Contains(SocialNetworkEnum.Google) || networksRemoved.Contains(SocialNetworkEnum.Google))
                    )
                    {
                        revision.Problems.Add("Changes to Google+ target are not allowed.");
                    }
                }

                if (!opts.AllowChangeLinkThumb)
                {
                    if (originalLinkThumbURL != newLinkThumbURL)
                    {
                        revision.Problems.Add("Changes to link thumbnail are not allowed.");
                    }
                }
                if (!opts.AllowChangeMessage)
                {
                    if (originalMessage != newMessage)
                    {
                        revision.Problems.Add("Changes to message are not allowed.");
                    }
                }

                if (!opts.AllowChangeLinkText)
                {
                    if ((originalLinkTitle != newLinkTitle) || (originalLinkText != newLinkText))
                    {
                        revision.Problems.Add("Changes to link text are not allowed.");
                    }
                }

                if (!opts.AllowChangeLinkURL)
                {
                    if (originalLinkURL != newLinkURL)
                    {
                        revision.Problems.Add("Changes to link URL are not allowed.");
                    }
                }

            }

            if (revision.Problems.Any())
                return revision;

            #endregion

            bool noImagesSelected = false;
            if (!changed.PostImages.Any())
                noImagesSelected = true;

            using (MainEntities db = ContextFactory.Main)
            {

                #region remove targets

                // do we have to remove any targets?
                if (networksRemoved.Any())
                {
                    foreach (SocialNetworkEnum net in networksRemoved)
                    {
                        long? postTargetID = (
                            from pt in db.PostTargets
                            join c in db.Credentials on pt.CredentialID equals c.ID
                            join sa in db.SocialApps on c.SocialAppID equals sa.ID
                            where pt.PostID == revision.Entity.PostID
                            && sa.SocialNetworkID == (long)net
                            select pt.ID
                        ).FirstOrDefault();
                        if (postTargetID.HasValue)
                        {

                            db.spRemovePostTarget(postTargetID);

                            Auditor
                                .New(AuditLevelEnum.Information, AuditTypeEnum.UserActivity, revision.UserInfo, "")
                                .SetAuditDataObject(
                                    AuditUserActivity
                                    .New(revision.UserInfo, AuditUserActivityTypeEnum.PostTargetDeleted)
                                    .SetPostID(revision.Entity.PostID)
                                    .SetPostTargetID(postTargetID)
                                    .SetDescription("SavePostRevisionForAccount(userInfo,{0},{1})", revision.Entity.PostID, revision.Entity.AccountID)
                                )
                                .Save(ProviderFactory.Logging)
                            ;

                        }

                    }
                }

                #endregion

                #region add targets

                // do we have to add any post targets?
                if (networksAdded.Any())
                {
                    foreach (SocialNetworkEnum net in networksAdded)
                    {
                        // get the credential id
                        long? credID = (from o in original.SocialNetworks where o.SocialNetwork == net select o.CredentialID).FirstOrDefault();
                        if (credID.HasValue)
                        {
                            PostTarget target = new PostTarget()
                            {
                                CredentialID = credID.Value,
                                Deleted = false,
                                NoApproverHoldDate = null,
                                JobID = null,
                                FeedID = null,
                                FailedDate = null,
                                PickedUpDate = null,
                                PostExpirationDate = null,
                                PublishedDate = null,
                                PostID = revision.Entity.PostID,
                                ResultID = null,
                                ScheduleDate = newSchedDateUTC,
                                NoImagesSelected = noImagesSelected
                            };
                            db.PostTargets.AddObject(target);

                            db.SaveChanges();

                            Auditor
                                .New(AuditLevelEnum.Information, AuditTypeEnum.UserActivity, revision.UserInfo, "")
                                .SetAuditDataObject(
                                    AuditUserActivity
                                    .New(revision.UserInfo, AuditUserActivityTypeEnum.PostTargetAdded)
                                    .SetPostID(target.PostID)
                                    .SetPostTargetID(target.ID)
                                    .SetDescription("SavePostRevisionForAccount(userInfo,{0},{1})", revision.Entity.PostID, revision.Entity.AccountID)
                                )
                                .Save(ProviderFactory.Logging)
                            ;
                        }
                    }

                }

                #endregion

                #region get targets and alter schedule date

                List<PostTarget> targets = (
                    from pt in db.PostTargets
                    join c in db.Credentials on pt.CredentialID equals c.ID
                    where c.AccountID == revision.Entity.AccountID
                    && pt.PostID == revision.Entity.PostID
                    select pt
                ).ToList();

                if (oldSchedDateUTC != newSchedDateUTC)
                {
                    foreach (PostTarget target in targets)
                    {
                        target.ScheduleDate = newSchedDateUTC;
                        target.NoImagesSelected = noImagesSelected;
                    }
                    db.SaveChanges();
                }

                #endregion

                #region create new revision records for all targets

                foreach (PostTarget target in targets)
                {
                    PostRevision rev = new PostRevision()
                    {
                        DateCreated = DateTime.UtcNow,
                        Link = newLinkURL,
                        LinkTitle = newLinkTitle,
                        LinkDescription = newLinkText,
                        Message = newMessage,
                        PostID = revision.Entity.PostID,
                        PostTargetID = target.ID,
                        ImageURL = changed.LinkThumbURL
                    };


                    //create post image records with this target id
                    execNonQuery(string.Format("delete PostImages where PostTargetID={0}", target.ID));

                    if (revision.Entity.PostImages.Any())
                    {
                        foreach (PostImageDTO img in revision.Entity.PostImages)
                        {
                            PostImage pimg = new PostImage()
                                {
                                    PostID = revision.Entity.PostID,
                                    PostTargetID = target.ID,
                                    URL = img.URL
                                };
                            db.PostImages.AddObject(pimg);
                        }
                    }

                    db.PostRevisions.AddObject(rev);
                    db.SaveChanges();
                }

                #endregion

                UpdateQueue_Posts q = new UpdateQueue_Posts()
                {
                    PostID = revision.Entity.PostID
                };
                db.UpdateQueue_Posts.AddObject(q);
                db.SaveChanges();
            }

            // now to be safe, pull the revision information
            revision.Entity = getPostForRevision(revision.UserInfo, revision.Entity.PostID, revision.Entity.AccountID).Entity;


            return revision;
        }

        public GetEntityDTO<RevisePostDTO> GetPostForRevision(UserInfoDTO userInfo, long postID, long accountID)
        {
            Auditor
                .New(AuditLevelEnum.Information, AuditTypeEnum.UserActivity, userInfo, "GetPostForRevision called")
                .SetAuditDataObject(
                    AuditUserActivity
                    .New(userInfo, AuditUserActivityTypeEnum.PostOpenedForRevision)
                    .SetPostID(postID)
                    .SetAccountID(accountID)
                    .SetDescription("GetPostForRevision(userInfo,{0},{1})", postID, accountID)
                )
                .Save(ProviderFactory.Logging)
            ;

            return getPostForRevision(userInfo, postID, accountID);
        }

        private GetEntityDTO<RevisePostDTO> getPostForRevision(UserInfoDTO userInfo, long postID, long accountID)
        {

            TimeZoneInfo timeZoneLoggedInUser = ProviderFactory.TimeZones.GetTimezoneForUserID(userInfo.EffectiveUser.ID.Value);

            GetEntityDTO<RevisePostDTO> result = new GetEntityDTO<RevisePostDTO>();
            if (!ProviderFactory.Security.HasPermission(userInfo.EffectiveUser.ID.Value, accountID, PermissionTypeEnum.social_publish))
            {
                result.Problems.Add(Locale.Localize("system.nopermission"));
                return result;
            }

            PostDTO post = getPostByID(userInfo, postID, true, accountID).Entity;

            using (MainEntities db = ContextFactory.Main)
            {


                List<PostRevision> revs = (from pr in db.PostRevisions where pr.PostID == postID select pr).ToList();

                List<long> ptIDs = (from pt in post.Targets select pt.ID.Value).ToList();
                //just targets for this account
                List<PostTarget> postTargets = (
                    from pt in db.PostTargets
                    join c in db.Credentials on pt.CredentialID equals c.ID
                    where c.AccountID == accountID
                    && ptIDs.Contains(pt.ID)
                    select pt
                ).ToList();

                List<long> postTargetIDs = (
                    from p in postTargets
                    select p.ID
                ).ToList();


                //newest revision
                PostRevision newRevision = (
                    from r in revs
                    where r.PostTargetID.HasValue
                    && postTargetIDs.Contains(r.PostTargetID.Value)
                    orderby r.DateCreated descending
                    select r
                ).FirstOrDefault();

                if (newRevision == null)
                {
                    newRevision = (
                        from r in revs
                        orderby r.DateCreated descending
                        select r
                    ).FirstOrDefault();
                }

                //newest image list selection
                List<PostImage> postImages = (from pi in db.PostImages where pi.PostTargetID.HasValue && postTargetIDs.Contains(pi.PostTargetID.Value) select pi).ToList();

                if (postImages.Count() > 0)
                {
                    // we only want the first post target's list.  We are currently doing this by account
                    long postTargetIDReference = (from pi in postImages select pi.PostTargetID.Value).First();
                    postImages = (from pi in postImages where pi.PostTargetID.Value == postTargetIDReference select pi).ToList();
                }
                else
                {
                    postImages = (from pi in db.PostImages where pi.PostID == postID && pi.PostTargetID == null select pi).ToList();
                }

                string syndicatorName = null;
                if (post.SyndicatorID.HasValue)
                {
                    syndicatorName = (from s in db.Syndicators where s.ID == post.SyndicatorID.Value select s.Account.DisplayName).FirstOrDefault();
                };

                DateTime? scheduleDate = null;
                if (postTargets.Any())
                {
                    scheduleDate = postTargets[0].ScheduleDate;
                }
                if (!scheduleDate.HasValue)
                {
                    //scheduleDate=post.sc
                }


                RevisePostDTO rpd = new RevisePostDTO()
                {
                    AccountID = accountID,
                    AccountName = ProviderFactory.Security.GetAccountNameByID(accountID),
                    Link = newRevision.Link,
                    LinkDescription = newRevision.LinkDescription,
                    LinkTitle = newRevision.LinkTitle,
                    LinkThumbURL = newRevision.ImageURL,
                    Message = newRevision.Message,
                    PostID = postID,
                    PostTitle = post.Title,
                    SyndicationOptions = post.SyndicationOptions,
                    SyndicatorName = syndicatorName,
                    ScheduleDate = SIDateTime.CreateSIDateTime(scheduleDate, timeZoneLoggedInUser),
                    PostType = post.Type
                };

                foreach (PostImage img in postImages)
                {
                    rpd.PostImages.Add(new PostImageDTO()
                    {
                        ID = img.ID,
                        URL = img.URL
                    });
                }

                // add the social network infos

                List<long> selectedSocialNetworkIDs = new List<long>();
                foreach (PostTarget target in postTargets)
                {
                    SIDateTime schedDate = SIDateTime.CreateSIDateTime(target.ScheduleDate, timeZoneLoggedInUser);

                    RevisePostSocialNetworkInfoDTO info = (
                        from c in db.Credentials
                        where c.ID == target.CredentialID
                        select new RevisePostSocialNetworkInfoDTO()
                        {
                            SocialNetwork = (SocialNetworkEnum)c.SocialApp.SocialNetworkID,
                            ScreenName = c.ScreenName,
                            Selected = true,
                            URL = c.URI,
                            CredentialID = c.ID
                        }
                    ).First();

                    info.ScheduleDate = schedDate;

                    selectedSocialNetworkIDs.Add((long)info.SocialNetwork);

                    rpd.SocialNetworks.Add(info);
                }

                // add the valid but unselected social networks
                rpd.SocialNetworks.AddRange((
                    from c in db.Credentials
                    where c.AccountID == accountID
                    && c.IsActive == true
                    && c.IsValid == true
                    && !selectedSocialNetworkIDs.Contains(c.SocialApp.SocialNetworkID)
                    select new RevisePostSocialNetworkInfoDTO()
                    {
                        CredentialID = c.ID,
                        SocialNetwork = (SocialNetworkEnum)c.SocialApp.SocialNetworkID,
                        ScreenName = c.ScreenName,
                        Selected = false,
                        URL = c.URI
                    }
                ).ToList());

                result.Entity = rpd;
            }

            return result;
        }

        public PublishAccountNetworkListSearchResultDTO GetPublishAccountNetworkListItems(
            UserInfoDTO userInfo, PublishAccountNetworkListSearchRequestDTO req
        )
        {

            ITimezoneProvider tz = ProviderFactory.TimeZones;
            TimeZoneInfo timeZone = tz.GetTimezoneForUserID(userInfo.EffectiveUser.ID.Value);
            ISecurityProvider security = ProviderFactory.Security;

            List<long> aids = null;
            if (req.AccountIDs == null || !req.AccountIDs.Any() && userInfo.EffectiveUser.SuperAdmin)
            {
                aids = null;
            }
            else
            {
                aids = security.AccountsWithPermission(userInfo.EffectiveUser.ID.Value, PermissionTypeEnum.social_publish);
            }

            //remove accounts based on filter
            if (req.AccountIDs != null && req.AccountIDs.Any())
            {
                List<long> accountIDsAndDescendants = new List<long>(req.AccountIDs);

                // add descendants
                foreach (long accountID in req.AccountIDs)
                {
                    List<long> desc = ProviderFactory.Security.GetAccountDescendants(accountID);
                    foreach (long accountID2 in desc)
                    {
                        if (!accountIDsAndDescendants.Contains(accountID2))
                            accountIDsAndDescendants.Add(accountID2);
                    }
                }

                if (aids == null)
                {
                    aids = accountIDsAndDescendants;
                }
                else
                {
                    aids = (from a in aids where accountIDsAndDescendants.Contains(a) select a).ToList();
                }
            }

            if (aids != null && !aids.Any()) return new PublishAccountNetworkListSearchResultDTO()
            {
                StartIndex = 0,
                EndIndex = 0,
                Items = new List<PublishAccountNetworkListItemDTO>(),
                TotalRecords = 0
            };

            string search = "";
            if (!string.IsNullOrWhiteSpace(req.AccountSearchString))
            {
                string[] searchParts = req.AccountSearchString.Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);
                if (searchParts.Length > 0)
                {
                    search = string.Join("|", searchParts);
                }
            }

            PublishAccountNetworkListSearchResultDTO results = new PublishAccountNetworkListSearchResultDTO()
            {
                StartIndex = req.StartIndex,
                EndIndex = req.EndIndex,
                SortAscending = req.SortAscending,
                SortBy = req.SortBy
            };

            using (MainEntities db = ContextFactory.Main)
            {
                if (db.Connection.State != System.Data.ConnectionState.Open)
                    db.Connection.Open();

                using (DbCommand cmd = db.CreateStoreCommand(
                   "spGetAvailablePublishTargets7",
                    System.Data.CommandType.StoredProcedure,

                    new SqlParameter("@SearchStringParts", search),
                    new SqlParameter("@accountIDs", (aids == null) ? null : string.Join("|", aids.ToArray())),
                    new SqlParameter("@FranchiseTypeIDs", (req.FranchiseTypeIDs == null) ? null : string.Join("|", req.FranchiseTypeIDs)),
                    new SqlParameter("@SocialNetworkIDs", (req.SocialNetworkIDs == null) ? null : string.Join("|", req.SocialNetworkIDs)),
                    new SqlParameter("@SyndicatorID", req.SyndicatorID),
                    new SqlParameter("@VirtualGroupID", req.VirtualGroupID),
                    new SqlParameter("@StateID", req.StateID),

                    new SqlParameter("@SortBy", req.SortBy),
                    new SqlParameter("@SortAscending", req.SortAscending),

                    new SqlParameter("@StartIndex", req.StartIndex),
                    new SqlParameter("@EndIndex", req.EndIndex)
               ))
                {
                    using (DbDataReader reader = cmd.ExecuteReader())
                    {
                        if (reader.Read())
                        {
                            //the total records
                            results.TotalRecords = reader.GetInt32(0);

                            if (reader.NextResult())
                            {
                                while (reader.Read())
                                {
                                    DateTime? lastPostDate = Util.GetReaderValue<DateTime?>(reader, "LastPostDate", null);
                                    DateTime? nextPostDate = Util.GetReaderValue<DateTime?>(reader, "NextScheduledPostDate", null);
                                    PublishAccountNetworkListItemDTO item = new PublishAccountNetworkListItemDTO()
                                    {
                                        AccountID = Util.GetReaderValue<long>(reader, "AccountID", 0),
                                        CredentialID = Util.GetReaderValue<long>(reader, "CredentialID", 0),
                                        ResellerID = Util.GetReaderValue<long>(reader, "ResellerID", 0),
                                        IsCredentialActive = Util.GetReaderValue<bool>(reader, "IsCredentialActive", false),
                                        IsSocialAppActive = Util.GetReaderValue<bool>(reader, "IsSocialAppActive", false),
                                        LastPostDate = (lastPostDate == null) ? null : new SIDateTime(lastPostDate.Value, timeZone),
                                        NextScheduledPostDate = (nextPostDate == null) ? null : new SIDateTime(nextPostDate.Value, timeZone),
                                        SocialAppID = Util.GetReaderValue<long>(reader, "CredentialID", 0),
                                        SocialNetworkID = Util.GetReaderValue<long>(reader, "SocialNetworkID", 0),
                                        StateID = Util.GetReaderValue<long>(reader, "StateID", 0),
                                        StateAbbr = Util.GetReaderValue(reader, "StateAbbr", string.Empty),
                                        Address = Util.GetReaderValue(reader, "Address", string.Empty),
                                        City = Util.GetReaderValue(reader, "City", string.Empty),
                                        Zipcode = Util.GetReaderValue(reader, "ZipCode", string.Empty),
                                        Network = Util.GetReaderValue(reader, "Network", string.Empty),
                                        Name = Util.GetReaderValue(reader, "Name", string.Empty),
                                        TimezoneID = Util.GetReaderValue<long>(reader, "TimeZoneID", 0),
                                        TimezoneName = Util.GetReaderValue(reader, "TimeZoneName", ""),

                                        SocialNetworkScreenName = Util.GetReaderValue<string>(reader, "SocialNetworkScreenName", ""),
                                        SocialNetworkUniqueID = Util.GetReaderValue<string>(reader, "SocialNetworkUniqueID", ""),
                                        SocialNetworkURL = Util.GetReaderValue<string>(reader, "SocialNetworkURL", ""),
                                        SocialNetworkPictureURL = Util.GetReaderValue<string>(reader, "SocialNetworkPictureURL", ""),

                                        IsAutoApproved = Util.GetReaderValue<bool>(reader, "IsAutoApproved", false)

                                    };

                                    DateTime utcTest = DateTime.UtcNow;
                                    TimeZoneInfo tzAccount = tz.GetTimezoneByID(item.TimezoneID);

                                    TimeSpan accountOffset = tzAccount.GetUtcOffset(utcTest);
                                    TimeSpan userOffset = timeZone.GetUtcOffset(utcTest);
                                    item.TimezoneOffsetMinutesFromLoggedInUsersZone = Convert.ToInt32(accountOffset.TotalMinutes - userOffset.TotalMinutes);

                                    results.Items.Add(item);

                                }

                                if (reader.NextResult())
                                {
                                    while (reader.Read())
                                    {
                                        long AccountID = Util.GetReaderValue<long>(reader, "AccountID", 0);
                                        long franchiseTypeID = Util.GetReaderValue<long>(reader, "FranchiseTypeID", 0);
                                        string franchiseName = Util.GetReaderValue(reader, "FranchiseType", string.Empty);

                                        foreach (var fran in results.Items.Where(i => i.AccountID == AccountID).Select(i => i.Franchises))
                                        {
                                            fran.Add(new PublishAccountNetworkListItemDTO.FranchisesTypes { ID = franchiseTypeID, Name = franchiseName });
                                        }
                                    }
                                }
                            }
                        }
                    }
                }

            }

            return results;
        }


        public GetEntityDTO<PostSummaryDTO> GetPostSummary(UserInfoDTO userInfo, long postID)
        {
            TimeZoneInfo timeZoneLoggedInUser = ProviderFactory.TimeZones.GetTimezoneForUserID(userInfo.EffectiveUser.ID.Value);
            GetEntityDTO<PostSummaryDTO> result = new GetEntityDTO<PostSummaryDTO>();
            using (MainEntities db = ContextFactory.Main)
            {
                spGetPostSummary_Result data = db.spGetPostSummary(postID).FirstOrDefault();
                result.Entity = new PostSummaryDTO()
                {
                    ApprovalExpirationDate = SIDateTime.CreateSIDateTime(data.ApprovalExpirationDate, timeZoneLoggedInUser),
                    CreatedDate = SIDateTime.CreateSIDateTime(data.DateCreated, timeZoneLoggedInUser),
                    PostExpirationDate = SIDateTime.CreateSIDateTime(data.PostExpirationDate, timeZoneLoggedInUser),
                    ScheduledDate = SIDateTime.CreateSIDateTime(data.ScheduleDate, timeZoneLoggedInUser),
                    PostID = data.PostID,
                    Title = data.Title,
                    PostCategoryID = data.PostCategoryID,
                    PostCategoryName = data.PostCategory,
                    PostCategoryVerticalID = data.PostCategoryVerticalID,
                    PostCategoryVertcialName = data.PostCategoryVertical,
                    LastMasterRevisionMessage = data.LastMasterRevisionMessage,
                    LastMasterRevisionLink = data.LastMasterRevisionLink,
                    LastMasterRevisionLinkImageURL = data.LastMasterRevisionImageURL,
                    LastMasterRevisionLinkDescription = data.LastMasterRevisionLinkDescription,
                    LastMasterRevisionLinkTitle = data.LastMasterRevisionLinkTitle,
                    FirstPostImageID = data.FirstPostImageID,
                    FirstPostImageURL = data.FirstPostImageURL,
                    CreatedByUserID = data.PostByUserID,
                    CreatedByUser = data.PostedByUsername,
                    NumberOfAccountsTargetted = data.Accounts.Value,
                    NumberOfAccountsWithRevisedTargets = data.RevisedAccounts.Value,
                    NumberOfRevisedTargets = data.RevisedTargets.Value,
                    NumberOfTargets = data.Targets.Value,
                    PostType = (PostTypeEnum)data.PostTypeID
                };

                if (data.SyndicatorID.HasValue && data.SyndicatorID.Value > 0)
                {
                    SIDateTime scheduleDateLoggedInUser = getScheduledDateForLoggedInUser(null, data.PostByUserID, data.ScheduleDate, timeZoneLoggedInUser);
                    result.Entity.SyndicationPublishOptions = getSyndicationOptionsForPost(postID, scheduleDateLoggedInUser);
                }
            }

            return result;
        }

        public void SetPostTargetFailed(long postTargetID)
        {
            using (MainEntities db = ContextFactory.Main)
            {
                if (db.Connection.State != System.Data.ConnectionState.Open)
                    db.Connection.Open();

                string sql = string.Format("update posttargets set faileddate=getutcdate() where id={0}", postTargetID);
                using (DbCommand cmd = db.CreateStoreCommand(sql, System.Data.CommandType.Text))
                {
                    cmd.ExecuteNonQuery();
                }

                long postID = (from pt in db.PostTargets where pt.ID == postTargetID select pt.PostID).FirstOrDefault();

                //UpdateQueue_Posts q = new UpdateQueue_Posts()
                //{
                //    PostID = postID
                //};
                //db.UpdateQueue_Posts.AddObject(q);

                string sqlQ = string.Format(
                    "if not exists (select * from [UpdateQueue.Posts] where PostID={0}) insert [UpdateQueue.Posts](PostID) values ({0})",
                    postID
                );
                execNonQuery(sqlQ);

                db.SaveChanges();
            }
        }

        public void SetPostImageResultFailed(long postImageID, long postTargetID)
        {
            using (MainEntities db = ContextFactory.Main)
            {
                if (db.Connection.State != System.Data.ConnectionState.Open)
                    db.Connection.Open();

                string sql = string.Format("update PostImageResults set FailedDate=getutcdate() where PostImageID={0} and PostTargetID={1}", postImageID, postTargetID);
                using (DbCommand cmd = db.CreateStoreCommand(sql, System.Data.CommandType.Text))
                {
                    cmd.ExecuteNonQuery();
                }

                long postID = (from pt in db.PostTargets where pt.ID == postTargetID select pt.PostID).FirstOrDefault();

                //UpdateQueue_Posts q = new UpdateQueue_Posts()
                //{
                //    PostID = postID
                //};
                //db.UpdateQueue_Posts.AddObject(q);

                string sqlQ = string.Format(
                    "if not exists (select * from [UpdateQueue.Posts] where PostID={0}) insert [UpdateQueue.Posts](PostID) values ({0})",
                    postID
                );
                execNonQuery(sqlQ);

                db.SaveChanges();
            }
        }

        #region processor routines

        public PostTargetPublishInfoDTO GetPostTargetPublishInfo(long postTargetID, long? postImageID)
        {
            ITimezoneProvider tz = ProviderFactory.TimeZones;

            using (MainEntities db = ContextFactory.Main)
            {
                if (db.Connection.State != System.Data.ConnectionState.Open)
                    db.Connection.Open();

                using (DbCommand cmd = db.CreateStoreCommand(
                   "spGetPostTargetInfo",
                    System.Data.CommandType.StoredProcedure,

                    new SqlParameter("@PostTargetID", postTargetID),
                    new SqlParameter("@PostImageID", postImageID)
               ))
                {

                    using (DbDataReader reader = cmd.ExecuteReader())
                    {
                        if (reader.Read())
                        {
                            PostTargetPublishInfoDTO info = new PostTargetPublishInfoDTO()
                            {
                                CredentialID = Util.GetReaderValue<long>(reader, "CredentialID", 0),
                                PostTargetID = Util.GetReaderValue<long>(reader, "PostTargetID", 0),
                                PostID = Util.GetReaderValue<long>(reader, "PostID", 0),
                                PostType = (PostTypeEnum)Util.GetReaderValue<long>(reader, "PostTypeID", 0),
                                Title = Util.GetReaderValue<string>(reader, "Title", null),

                                Link = Util.GetReaderValue<string>(reader, "Link", null),
                                Message = Util.GetReaderValue<string>(reader, "Message", null),
                                LinkTitle = Util.GetReaderValue<string>(reader, "LinkTitle", null),
                                LinkDescription = Util.GetReaderValue<string>(reader, "LinkDescription", null),

                                RevisionImageURL = Util.GetReaderValue<string>(reader, "RevisionImageURL", null),
                                ImageURL = Util.GetReaderValue<string>(reader, "ImageURL", null),
                                PostImageID = Util.GetReaderValue<long?>(reader, "PostImageID", null),

                                AppID = Util.GetReaderValue<string>(reader, "AppID", null),
                                AppSecret = Util.GetReaderValue<string>(reader, "AppSecret", null),
                                UniqueID = Util.GetReaderValue<string>(reader, "UniqueID", null),
                                Token = Util.GetReaderValue<string>(reader, "Token", null),
                                TokenSecret = Util.GetReaderValue<string>(reader, "TokenSecret", null),

                                ImageFeedID = Util.GetReaderValue<string>(reader, "ImageFeedID", null),
                                ImageResultID = Util.GetReaderValue<string>(reader, "ImageResultID", null),
                                FeedID = Util.GetReaderValue<string>(reader, "FeedID", null),
                                ResultID = Util.GetReaderValue<string>(reader, "ResultID", null)
                            };

                            string accountName = Util.GetReaderValue<string>(reader, "AccountName", null);
                            string accountPhone = Util.GetReaderValue<string>(reader, "PhoneNumber", null);
                            string accountURL = Util.GetReaderValue<string>(reader, "WebsiteURL", null);

                            info.Message = TokenizeMessage(info.Message, accountName, accountPhone, accountURL);

                            long accountTimeZoneID = Util.GetReaderValue<long>(reader, "TimeZoneID", 0);
                            DateTime? scheduleDate = Util.GetReaderValue<DateTime?>(reader, "ScheduleDate", null);
                            DateTime? publishedDate = Util.GetReaderValue<DateTime?>(reader, "PublishedDate", null);

                            if (scheduleDate.HasValue)
                            {
                                info.ScheduleDate = new SIDateTime(scheduleDate.Value, tz.GetTimezoneByID(accountTimeZoneID));
                            }
                            else
                            {
                                info.ScheduleDate = null;
                            }

                            if (publishedDate.HasValue)
                            {
                                info.PublishedDate = new SIDateTime(publishedDate.Value, tz.GetTimezoneByID(accountTimeZoneID));
                            }
                            else
                            {
                                info.PublishedDate = null;
                            }

                            info.UserID = Util.GetReaderValue<long>(reader, "UserID", 0);

                            return info;
                        }
                        else
                        {
                            return null;
                        }
                    }

                }
            }

        }

        public List<PostImageResultDTO> GetPostImageResulInfo(long postTargetID)
        {
            List<PostImageResultDTO> list = new List<PostImageResultDTO>();
            using (MainEntities db = ContextFactory.Main)
            {
                if (db.Connection.State != System.Data.ConnectionState.Open)
                    db.Connection.Open();

                var results = (from pi in db.PostImageResults
                               where pi.PostTargetID == postTargetID
                               select pi).ToList();

                foreach (var result in results)
                {
                    PostImageResultDTO obj = new PostImageResultDTO();

                    obj.PostImageResultID = result.ID;
                    obj.PostImageID = result.PostImageID;
                    obj.PostTargetID = result.PostTargetID;
                    obj.JobID = result.JobID;
                    obj.ResultID = result.ResultID;
                    obj.FeedID = result.FeedID;
                    obj.ProcessedDate = result.ProcessedDate;
                    obj.FailedDate = result.FailedDate;

                    list.Add(obj);
                }
            }

            return list;

        }

        public void SetPostImageJobID(long postImageID, long postTargetID, long jobID)
        {
            using (MainEntities db = ContextFactory.Main)
            {
                db.spSetPostImageJob(postImageID, postTargetID, jobID);
            }
        }

        public void SetPostTargetJobID(long postTargetID, long jobID)
        {
            using (MainEntities db = ContextFactory.Main)
            {
                db.spSetPostTargetJob(postTargetID, jobID);
            }
        }

        public void SetPostTargetResults(long postTargetID, string resultID, string feedID)
        {
            using (MainEntities db = ContextFactory.Main)
            {
                db.spSetPostTargetResult(postTargetID, resultID, feedID);
            }
        }

        public void SetPostTargetResults(long postTargetID)
        {
            using (MainEntities db = ContextFactory.Main)
            {
                db.spSetPostTargetResultPhotoPost(postTargetID);
            }
        }

        public void SetPostImageResults(long postImageID, long postTargetID, string resultID, string feedID)
        {
            using (MainEntities db = ContextFactory.Main)
            {
                db.spSetPostImageResult(postImageID, postTargetID, resultID, feedID);
            }
        }

        public SocialCredentialDTO GetSocialCredentialInfo(long credentialID)
        {
            using (MainEntities db = ContextFactory.Main)
            {
                if (db.Connection.State != System.Data.ConnectionState.Open)
                    db.Connection.Open();

                using (DbCommand cmd = db.CreateStoreCommand(
                   "spGetCredentialInfo",
                    System.Data.CommandType.StoredProcedure,

                    new SqlParameter("@CredentialID", credentialID)
               ))
                {

                    using (DbDataReader reader = cmd.ExecuteReader())
                    {
                        if (reader.Read())
                        {
                            SocialCredentialDTO info = new SocialCredentialDTO()
                            {
                                ID = Util.GetReaderValue<long>(reader, "CredentialID", 0),
                                AccountID = Util.GetReaderValue<long>(reader, "AccountID", 0),
                                SocialAppID = Util.GetReaderValue<long>(reader, "SocialAppID", 0),
                                UniqueID = Util.GetReaderValue<string>(reader, "UniqueID", null),
                                Token = Util.GetReaderValue<string>(reader, "Token", null),
                                TokenSecret = Util.GetReaderValue<string>(reader, "TokenSecret", null),

                                IsValid = Util.GetReaderValue<bool>(reader, "IsValid", false),
                                IsActive = Util.GetReaderValue<bool>(reader, "IsActive", false),

                                AppVersion = Util.GetReaderValue<int>(reader, "AppVersion", 0),

                                AppID = Util.GetReaderValue<string>(reader, "AppID", null),
                                CallbackURL = Util.GetReaderValue<string>(reader, "CallbackURL", null),
                                AppSecret = Util.GetReaderValue<string>(reader, "AppSecret", null),

                                IsSocialAppActive = Util.GetReaderValue<bool>(reader, "IsSocialAppActive", false),
                                SocialNetworkID = Util.GetReaderValue<long>(reader, "SocialNetworkID", 0),

                                ScreenName = Util.GetReaderValue<string>(reader, "ScreenName", null),
                                PictureURL = Util.GetReaderValue<string>(reader, "PictureURL", null),
                                SocialNetworkName = Util.GetReaderValue<string>(reader, "SocialNetworkName", null),
                                PageUrl = Util.GetReaderValue<string>(reader, "PageURL", null),

                                AppToken = Util.GetReaderValue<string>(reader, "AppToken", null),
                                CreatedByUserID = Util.GetReaderValue<long>(reader, "CreatedByUserID", 0),
                            };

                            return info;
                        }
                        else
                        {
                            return null;
                        }
                    }

                }
            }
        }

        public string TokenizeMessage(string message, string AccountName, string AccountPhone, string AccountURL)
        {
            if (string.IsNullOrWhiteSpace(message))
                return "";
            if (!string.IsNullOrWhiteSpace(AccountName))
                message = message.Replace("[Dealer Name]", AccountName);
            if (!string.IsNullOrWhiteSpace(AccountPhone))
                message = message.Replace("[Dealer Phone #]", AccountPhone);
            if (!string.IsNullOrWhiteSpace(AccountURL))
                message = message.Replace("[Dealer Website Link]", AccountURL);
            return message;
        }

        #endregion

        #region queueing routines



        public void QueuePost(long postID, long accountID)
        {
            using (MainEntities db = ContextFactory.Main)
            {
                PublishJobListQueuer
                    .New(accountID, postID)
                    .LoadTargets(db, this)
                    .QueueJobs(ProviderFactory.Jobs)
                    .SetTargetsPickedUp(db)
                ;
            }

        }

        public List<PostToQueueDTO> GetPostsToQueue()
        {
            using (MainEntities db = ContextFactory.Main)
            {
                if (db.Connection.State != System.Data.ConnectionState.Open)
                    db.Connection.Open();

                using (DbCommand cmd = db.CreateStoreCommand(
                   "spGetPostsToQueue",
                    System.Data.CommandType.StoredProcedure
               ))
                {
                    List<PostToQueueDTO> ptqs = new List<PostToQueueDTO>();

                    using (DbDataReader reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            PostToQueueDTO ptq = new PostToQueueDTO()
                            {
                                AccountID = Util.GetReaderValue<long>(reader, "AccountID", 0),
                                PostID = Util.GetReaderValue<long>(reader, "PostID", 0)
                            };

                            Auditor
                                .New(AuditLevelEnum.Information, AuditTypeEnum.PublishActivity, UserInfoDTO.System, "")
                                .SetAuditDataObject(
                                    AuditPublishActivity
                                    .New(ptq.PostID, AuditPublishActivityTypeEnum.PickedUpForQueuing)
                                    .SetPostID(ptq.PostID)
                                    .SetAccountID(ptq.AccountID)
                                )
                                .Save(ProviderFactory.Logging)
                            ;

                            ptqs.Add(ptq);
                        }
                    }

                    return ptqs;
                }
            }

            return null;
        }


        #endregion

        #region approvals

        public List<PendingApprovalInfoDTO> GetPendingApprovals(UserInfoDTO userInfo)
        {
            List<PendingApprovalInfoDTO> list = new List<PendingApprovalInfoDTO>();
            TimeZoneInfo timeZone = ProviderFactory.TimeZones.GetTimezoneForUserID(userInfo.EffectiveUser.ID.Value);

            List<long> accountIDs = AccountListBuilder
                .New((SecurityProvider)ProviderFactory.Security)
                .AddAccountID(userInfo.EffectiveUser.CurrentContextAccountID)
                .ExpandToDescendants()
                .GetIDs(SecurityProvider.PLATFORM_MAX_ACCOUNT_ID_LIST)
            ;

            using (MainEntities db = ContextFactory.Main)
            {
                if (db.Connection.State != System.Data.ConnectionState.Open)
                    db.Connection.Open();

                using (DbCommand cmd = db.CreateStoreCommand(
                    "spGetPendingApprovalsForUser4",
                    CommandType.StoredProcedure,
                    new SqlParameter("@userid", userInfo.EffectiveUser.ID.Value)
                ))
                {
                    using (DbDataReader reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            long targetAccountID = Util.GetReaderValue<long>(reader, "TargetAccountID", 0);
                            if (accountIDs.Contains(targetAccountID))
                            {
                                list.Add(new PendingApprovalInfoDTO()
                                {
                                    PostID = Util.GetReaderValue<long>(reader, "PostID", 0),
                                    PostApprovalID = Util.GetReaderValue<long>(reader, "PostApprovalID", 0),
                                    TargetAccountID = Util.GetReaderValue<long>(reader, "TargetAccountID", 0),
                                    PostTargetID = Util.GetReaderValue<long>(reader, "PostTargetID", 0),
                                    SyndicatorID = Util.GetReaderValue<long?>(reader, "SyndicatorID", null),
                                    PostedByUserID = Util.GetReaderValue<long>(reader, "PostedByUserID", 0),
                                    ApprovalOrder = Util.GetReaderValue<int>(reader, "ApprovalOrder", 0),
                                    SyndicatorName = Util.GetReaderValue<string>(reader, "SyndicatorName", ""),
                                    SocialNetworkName = Util.GetReaderValue<string>(reader, "SocialNetworkName", ""),
                                    SocialNetworkScreenName = Util.GetReaderValue<string>(reader, "SocialNetworkScreenName", ""),
                                    SocialNetworkPictureURL = Util.GetReaderValue<string>(reader, "SocialNetworkPictureURL", ""),
                                    PostedByUserName = Util.GetReaderValue<string>(reader, "PostedByUserName", ""),
                                    Title = Util.GetReaderValue<string>(reader, "Title", ""),
                                    TargetAccountName = Util.GetReaderValue<string>(reader, "TargetAccountName", ""),
                                    Message = Util.GetReaderValue<string>(reader, "Message", ""),
                                    Link = Util.GetReaderValue<string>(reader, "Link", ""),
                                    LinkTitle = Util.GetReaderValue<string>(reader, "LinkTitle", ""),
                                    LinkDescription = Util.GetReaderValue<string>(reader, "LinkDescription", ""),
                                    LinkImageURL = Util.GetReaderValue<string>(reader, "LinkImageURL", ""),
                                    PostImageURL = Util.GetReaderValue<string>(reader, "PostImageURL", ""),
                                    ScheduleDate = SIDateTime.CreateSIDateTime(Util.GetReaderValue<DateTime?>(reader, "ScheduleDate", null), timeZone),
                                    PublishedDate = SIDateTime.CreateSIDateTime(Util.GetReaderValue<DateTime?>(reader, "PublishedDate", null), timeZone),
                                    PostCreationDate = SIDateTime.CreateSIDateTime(Util.GetReaderValue<DateTime?>(reader, "PostCreationDate", null), timeZone),
                                    SocialNetwork = (SocialNetworkEnum)Util.GetReaderValue<long>(reader, "SocialNetworkID", 0),

                                    SocialNetworkUniqueID = Util.GetReaderValue<string>(reader, "SocialNetworkUniqueID", ""),
                                    SocialNetworkURL = Util.GetReaderValue<string>(reader, "SocialNetworkURL", ""),
                                    CredentialIsActive = Util.GetReaderValue<bool>(reader, "CredentialIsActive", false),
                                    CredentialIsValid = Util.GetReaderValue<bool>(reader, "CredentialIsValid", false),

                                    AllowChangeImage = Util.GetReaderValue<bool?>(reader, "AllowChangeImage", false),
                                    AllowChangePublishDate = Util.GetReaderValue<bool?>(reader, "AllowChangePublishDate", false),
                                    AllowChangeMessage = Util.GetReaderValue<bool?>(reader, "AllowChangeMessage", false),
                                    AllowChangeTargets = Util.GetReaderValue<bool?>(reader, "AllowChangeTargets", false),
                                    AllowChangeLinkURL = Util.GetReaderValue<bool?>(reader, "AllowChangeLinkURL", false),
                                    AllowChangeLinkThumb = Util.GetReaderValue<bool?>(reader, "AllowChangeLinkThumb", false),
                                    AllowChangeLinkText = Util.GetReaderValue<bool?>(reader, "AllowChangeLinkText", false),
                                });
                            }
                        }
                    }
                }
            }

            return list;
        }

        public ApprovalResponseResultDTO SubmitApprovalResponse(UserInfoDTO userInfo, long postApprovalID, ApprovalResponseEnum result)
        {
            ApprovalResponseResultDTO res = new ApprovalResponseResultDTO();

            using (MainEntities db = ContextFactory.Main)
            {
                if (db.Connection.State != ConnectionState.Open)
                    db.Connection.Open();

                //set the date read on any notifications

                StringBuilder sb = new StringBuilder();
                sb.AppendFormat(" update nt ");
                sb.AppendFormat(" set nt.DateRead=getdate() ");
                sb.AppendFormat(" from NotificationTargets nt ");

                sb.AppendFormat(" where exists ( ");
                sb.AppendFormat(" select * ");
                sb.AppendFormat(" from Notifications n ");
                sb.AppendFormat(" inner join NotificationRecipients nr on nr.NotificationID=n.ID ");
                sb.AppendFormat(" where nt.ID=nr.NotificationTargetID ");
                sb.AppendFormat(" and ( n.NotificationMessageTypeID={0} or n.NotificationMessageTypeID={1} )",
                    (long)NotificationMessageTypeEnum.SyndicationPendingPostApproval,
                    (long)NotificationMessageTypeEnum.NonSyndicationPendingPostApproval
                );
                sb.AppendFormat(" and n.EntityID={0} ", postApprovalID);
                sb.AppendFormat(" ) ");

                execNonQuery(sb.ToString());

                //get the list of targets, will include targets other than the one referred to by the postApprovalID
                List<SubmitApprovalInfo> alist;
                using (DbCommand cmd = db.CreateStoreCommand(
                                "spGetSubmitApprovalInfoForUser", CommandType.StoredProcedure,
                                new SqlParameter("@userID", userInfo.EffectiveUser.ID.Value),
                                new SqlParameter("@postApprovalID", postApprovalID)
                ))
                {
                    using (DbDataReader reader = cmd.ExecuteReader())
                    {
                        alist = db.Translate<SubmitApprovalInfo>(reader).ToList();
                    }
                }

                //restrict to only the post approval in question
                alist = (from a in alist where a.PostApprovalID == postApprovalID select a).ToList();


                List<long> pids = (from p in alist select p.PostApprovalID).Distinct().ToList();
                List<PostApproval> palist = (from p in db.PostApprovals where pids.Contains(p.ID) select p).ToList();

                foreach (SubmitApprovalInfo info in alist)
                {
                    PostApproval pa = (from p in palist where p.ID == info.PostApprovalID select p).FirstOrDefault();

                    if (!info.SyndicatorID.HasValue && result == ApprovalResponseEnum.AutoApprove)
                    {
                        //if not syndication then this is merely an approval
                        result = ApprovalResponseEnum.Approve;
                    }



                    switch (result)
                    {
                        case ApprovalResponseEnum.Approve:
                            {
                                pa.DateApproved = DateTime.UtcNow;
                                pa.SetByUserID = userInfo.EffectiveUser.ID.Value;
                                pa.DateRejected = null;
                                pa.AutoApproved = false;
                                Auditor
                                    .New(AuditLevelEnum.Information, AuditTypeEnum.UserActivity, userInfo, "")
                                    .SetAuditDataObject(
                                        AuditUserActivity
                                        .New(userInfo, AuditUserActivityTypeEnum.PostTargetApproved)
                                        .SetPostTargetID(pa.PostTargetID)
                                    )
                                    .Save(ProviderFactory.Logging)
                                ;
                                break;
                            }
                        case ApprovalResponseEnum.AutoApprove:
                            {
                                pa.DateApproved = DateTime.Now;
                                pa.SetByUserID = userInfo.EffectiveUser.ID.Value;
                                pa.DateRejected = null;
                                pa.AutoApproved = true;

                                Auditor
                                    .New(AuditLevelEnum.Information, AuditTypeEnum.UserActivity, userInfo, "")
                                    .SetAuditDataObject(
                                        AuditUserActivity
                                        .New(userInfo, AuditUserActivityTypeEnum.PostTargetApproved)
                                        .SetPostTargetID(pa.PostTargetID)
                                    )
                                    .Save(ProviderFactory.Logging)
                                ;

                                if (!info.PostAutoApprovalID.HasValue && info.SyndicatorID.HasValue)
                                {
                                    Auditor
                                        .New(AuditLevelEnum.Information, AuditTypeEnum.UserActivity, userInfo, "")
                                        .SetAuditDataObject(
                                            AuditUserActivity
                                            .New(userInfo, AuditUserActivityTypeEnum.PostTargetAutoApprovalAdded)
                                            .SetPostTargetID(pa.PostTargetID)
                                        )
                                        .Save(ProviderFactory.Logging)
                                    ;

                                    PostAutoApproval pap = (
                                        from p in db.PostAutoApprovals
                                        where p.SyndicatorID == info.SyndicatorID.Value
                                        && p.AccountID == info.AccountID
                                        && p.SocialNetworkID == info.SocialNetworkID
                                        && p.ApprovingUserID == userInfo.EffectiveUser.ID.Value
                                        select p
                                    ).FirstOrDefault();

                                    if (pap == null)
                                    {
                                        pap = new PostAutoApproval()
                                        {
                                            SocialNetworkID = info.SocialNetworkID,
                                            AccountID = info.AccountID,
                                            ApprovingUserID = userInfo.EffectiveUser.ID.Value,
                                            RejectionCount = 0,
                                            SyndicatorID = info.SyndicatorID.Value,
                                            DateCreated = DateTime.UtcNow
                                        };
                                        db.PostAutoApprovals.AddObject(pap);
                                    }
                                    else
                                    {
                                        pap.RejectionCount = 0;
                                    }
                                }
                                else
                                {
                                    execNonQuery(string.Format("update postautoapprovals set rejectioncount=0 where id={0}", info.PostAutoApprovalID));
                                }

                                break;
                            }
                        case ApprovalResponseEnum.Reject:
                            {
                                pa.DateApproved = null;
                                pa.AutoApproved = false;
                                pa.DateRejected = DateTime.UtcNow;
                                pa.SetByUserID = userInfo.EffectiveUser.ID.Value;

                                Auditor
                                    .New(AuditLevelEnum.Information, AuditTypeEnum.UserActivity, userInfo, "")
                                    .SetAuditDataObject(
                                        AuditUserActivity
                                        .New(userInfo, AuditUserActivityTypeEnum.PostTargetRejected)
                                        .SetPostTargetID(pa.PostTargetID)
                                    )
                                    .Save(ProviderFactory.Logging)
                                ;

                                if (info.SyndicatorID.HasValue)
                                {
                                    if (info.PostAutoApprovalID.HasValue)
                                    {
                                        PostAutoApproval pap = (from p in db.PostAutoApprovals where p.ID == info.PostAutoApprovalID select p).FirstOrDefault();
                                        if (pap != null)
                                        {
                                            pap.RejectionCount++;
                                            if (pap.RejectionCount >= 3)
                                            {
                                                db.PostAutoApprovals.DeleteObject(pap);


                                                Auditor
                                                    .New(AuditLevelEnum.Information, AuditTypeEnum.UserActivity, userInfo, "")
                                                    .SetAuditDataObject(
                                                        AuditUserActivity
                                                        .New(userInfo, AuditUserActivityTypeEnum.PostTargetAutoApprovalRemoved)
                                                        .SetPostTargetID(pa.PostTargetID)
                                                    )
                                                    .Save(ProviderFactory.Logging)
                                                ;
                                            }
                                        }
                                    }
                                }

                                break;
                            }
                    }

                    db.SaveChanges();

                }

            }

            return res;
        }

        public void ProcessPostApprovalInits()
        {
            using (MainEntities db = ContextFactory.Main)
            {
                if (db.Connection.State != ConnectionState.Open) db.Connection.Open();

                using (DbCommand cmd = db.CreateStoreCommand("spGetPostsToInitApprovalsFor2", CommandType.StoredProcedure))
                {
                    using (DbDataReader reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            long postID = Util.GetReaderValue<long>(reader, "PostID", 0);
                            long? userIDAlreadyApproved = Util.GetReaderValue<long?>(reader, "UserIDAlreadyApproved", null);
                            if (!userIDAlreadyApproved.HasValue)
                            {
                                userIDAlreadyApproved = Util.GetReaderValue<long>(reader, "UserID", 0);
                            }

                            if (postID > 0)
                            {
                                InitPostApprovals(postID, userIDAlreadyApproved);
                            }
                        }
                    }
                }
            }
        }

        public void InitPostApprovals(long postID, long? setUserIDAlreadyApproved)
        {
            using (MainEntities db = ContextFactory.Main)
            {
                // key = user id, value = postapprovalid
                Dictionary<long, long> postApprovingUsers = new Dictionary<long, long>();

                if (db.Connection.State != ConnectionState.Open) db.Connection.Open();

                db.spSetNoApproverHoldDatesForPost(postID, DateTime.UtcNow);

                var postInfo = (
                    from p in db.Posts
                    where p.ID == postID
                    select new
                    {
                        SyndicatorID = p.SyndicatorID,
                        AccountID = p.AccountID
                    }
                ).FirstOrDefault();

                using (DbCommand cmd = db.CreateStoreCommand("spGetPostApproverInitInfo4", CommandType.StoredProcedure, new SqlParameter("@PostID", postID)))
                {
                    using (DbDataReader reader = cmd.ExecuteReader())
                    {
                        //collect the relevant data used to set up approvals and notifications

                        List<IPATargetInfo> targets = db.Translate<IPATargetInfo>(reader).ToList();

                        reader.NextResult();
                        List<IPAApprovingUsers> users = db.Translate<IPAApprovingUsers>(reader).ToList();

                        reader.NextResult();
                        List<IPAAccountTemplate> emailTemplates = db.Translate<IPAAccountTemplate>(reader).ToList();
                        reader.NextResult();
                        List<IPAAccountTemplate> platformTemplates = db.Translate<IPAAccountTemplate>(reader).ToList();

                        reader.NextResult();
                        List<IPANotiSubs> notiSubs = db.Translate<IPANotiSubs>(reader).ToList();

                        reader.NextResult();
                        List<IPAAutoApprovals> autoApprovals = db.Translate<IPAAutoApprovals>(reader).ToList();

                        reader.NextResult();
                        List<IPAPostApprovals> approvals = db.Translate<IPAPostApprovals>(reader).ToList();

                        //close the reader
                        reader.Close();


                        if (targets.Any())
                        {
                            IPATargetInfo firstTarget = targets.First();
                            long? postSyndicatorID = firstTarget.SyndicatorID;
                            long? syndicatorResellerID = firstTarget.SyndicatorResellerID;

                            foreach (IPATargetInfo target in targets)
                            {
                                List<IPAApprovingUsers> approvingUsers = (from u in users where u.TargetAccountID == target.TargetAccountID select u).ToList();

                                if (approvingUsers.Count() == 0)
                                {
                                    if (postSyndicatorID.HasValue)
                                    {
                                        Auditor
                                            .New(AuditLevelEnum.Warning, AuditTypeEnum.PublishActivity, UserInfoDTO.System, "")
                                            .SetAuditDataObject(new AuditPublishActivity(postID, AuditPublishActivityTypeEnum.ApproversInitWarning)
                                            {
                                                AccountID = target.TargetAccountID,
                                                PostID = postID,
                                                PostTargetID = target.PostTargetID,
                                                Description = "Syndication mode: no users have permission to approve."
                                            })
                                        .Save(ProviderFactory.Logging);
                                    }
                                    else if (target.PostsRequireApproval)
                                    {
                                        Auditor
                                            .New(AuditLevelEnum.Warning, AuditTypeEnum.PublishActivity, UserInfoDTO.System, "")
                                            .SetAuditDataObject(new AuditPublishActivity(postID, AuditPublishActivityTypeEnum.ApproversInitWarning)
                                            {
                                                AccountID = target.TargetAccountID,
                                                PostID = postID,
                                                PostTargetID = target.PostTargetID,
                                                Description = "Distribution mode: Account requires approvals, but no users have permission to approve."
                                            })
                                        .Save(ProviderFactory.Logging);
                                    }
                                    else
                                    {
                                        // no approvers required, release then return
                                        db.spSetNoApproverHoldDateForPostTarget(target.PostTargetID, null);
                                    }
                                }
                                else // there are approvers
                                {
                                    long postTargetID = target.PostTargetID;

                                    // Clear the approvals for this post target, it will not publish yet because we have set the no approver hold date
                                    clearApproversForPostTarget(postTargetID);

                                    // get a simple distinct list of user ids
                                    List<long> approvingUserIDs = (from a in approvingUsers select a.UserID).Distinct().ToList();

                                    // get or create a approval group based on our approvers list
                                    long approvalGroupID = getOrCreatePostApprovalGroup(approvingUserIDs);

                                    // create the approval record
                                    PostApproval apr = new PostApproval()
                                    {
                                        AutoApproved = false,
                                        DateApproved = null,
                                        DateRejected = null,
                                        PostApprovalGroupID = approvalGroupID,
                                        PostTargetID = postTargetID,
                                        UserID = null,
                                        ApprovalOrder = 1
                                    };

                                    // find existing approval
                                    IPAPostApprovals oldApproval = (
                                        from a in approvals
                                        where a.PostTargetID == postTargetID
                                        select a
                                    ).FirstOrDefault();

                                    if (oldApproval != null)
                                    {
                                        apr.DateApproved = oldApproval.DateApproved;
                                        apr.DateRejected = oldApproval.DateRejected;
                                        apr.SetByUserID = oldApproval.SetByUserID;
                                        apr.AutoApproved = oldApproval.AutoApproved;
                                    }

                                    // if we were instructed to auto approve a user, and the group has that user, add them
                                    if (setUserIDAlreadyApproved.HasValue && oldApproval == null)
                                    {
                                        if (approvingUserIDs.Contains(setUserIDAlreadyApproved.Value))
                                        {
                                            apr.DateRejected = null;
                                            apr.DateApproved = DateTime.UtcNow;
                                            apr.AutoApproved = false;
                                            apr.SetByUserID = setUserIDAlreadyApproved.Value;
                                        }
                                        Auditor
                                            .New(AuditLevelEnum.Information, AuditTypeEnum.UserActivity, new UserInfoDTO(setUserIDAlreadyApproved.Value, null), "")
                                            .SetAuditDataObject(
                                                AuditUserActivity
                                                .New(new UserInfoDTO(setUserIDAlreadyApproved.Value, null), AuditUserActivityTypeEnum.PostTargetApproved)
                                                .SetPostTargetID(postTargetID)
                                                .SetDescription("PostApproval marked \"Approved\" at time of entry for user {0}", setUserIDAlreadyApproved.Value)
                                            )
                                            .Save(ProviderFactory.Logging)
                                        ;
                                    }

                                    // if syndication, see if there's an autoapproval record, and if so, set approved with autoapprove
                                    if
                                        (postSyndicatorID.HasValue)
                                    {
                                        var paps = (
                                                from p in autoApprovals
                                                where p.AccountID == target.TargetAccountID
                                                && p.SocialNetworkID == target.SocialNetworkID
                                                && p.SyndicatorID == postSyndicatorID.Value
                                                && approvingUserIDs.Contains(p.ApprovingUserID)
                                                select p
                                        );

                                        if (paps.Any())
                                        {
                                            var firstPap = paps.First();

                                            apr.DateApproved = DateTime.UtcNow;
                                            apr.AutoApproved = true;
                                            apr.SetByUserID = paps.First().ApprovingUserID;

                                            Auditor
                                                .New(AuditLevelEnum.Information, AuditTypeEnum.UserActivity, new UserInfoDTO(firstPap.ApprovingUserID, null), "")
                                                .SetAuditDataObject(
                                                    AuditUserActivity
                                                    .New(new UserInfoDTO(firstPap.ApprovingUserID, null), AuditUserActivityTypeEnum.PostTargetApproved)
                                                    .SetPostTargetID(postTargetID)
                                                    .SetDescription(
                                                        "PostApproval marked \"Approved\" at time of entry based on AutoApproval User {0}",
                                                        firstPap.ApprovingUserID
                                                    )
                                                )
                                                .Save(ProviderFactory.Logging)
                                            ;
                                        }
                                    }

                                    // save the approval record
                                    db.PostApprovals.AddObject(apr);
                                    db.SaveChanges();

                                    try
                                    {
                                        if (oldApproval == null && apr.AutoApproved == true && apr.SetByUserID.HasValue)
                                        {
                                            // this is not a second save, and it was auto approved
                                            if (!postApprovingUsers.ContainsKey(apr.SetByUserID.Value))
                                            {
                                                postApprovingUsers.Add(apr.SetByUserID.Value, apr.ID);
                                            }
                                        }
                                    }
                                    catch (Exception ex)
                                    {
                                        ProviderFactory.Logging.LogException(ex);
                                    }

                                    long? oldNotificationID = null;
                                    if (oldApproval != null)
                                    {
                                        oldNotificationID = oldApproval.NotificationID;
                                    }

                                    // if not approved already, queue the notification
                                    if (oldNotificationID.HasValue && oldNotificationID.Value != apr.ID)
                                    {
                                        // there exists an old notification, re-assign the notification to the new postapproval record
                                        string sql = string.Format("update notifications set entityid={0} where id={1}", apr.ID, oldNotificationID.Value);
                                        execNonQuery(sql);
                                    }
                                    else if (!apr.DateApproved.HasValue)
                                    {
                                        DAL.Notification noti = new DAL.Notification()
                                        {
                                            NotificationMessageTypeID =
                                                (postSyndicatorID.HasValue && postSyndicatorID.Value > 0) ?
                                                    (long)NotificationMessageTypeEnum.SyndicationPendingPostApproval
                                                    : (long)NotificationMessageTypeEnum.NonSyndicationPendingPostApproval
                                                ,
                                            AccountID = target.TargetAccountID,
                                            EntityID = apr.ID,
                                            DateCreated = DateTime.UtcNow
                                        };
                                        db.Notifications.AddObject(noti);
                                        db.SaveChanges();
                                    }


                                    // clear the approver hold for this target
                                    db.spSetNoApproverHoldDateForPostTarget(target.PostTargetID, null);
                                }
                            }

                        }
                    }
                }

                try
                {
                    if (postApprovingUsers.Any())
                    {
                        foreach (long userID in postApprovingUsers.Keys)
                        {
                            long postApprovalID = postApprovingUsers[userID];
                            ProviderFactory.Notifications.SendAutoApprovalNotification(postApprovalID);
                        }
                    }
                }
                catch (Exception ex)
                {
                    ProviderFactory.Logging.LogException(ex);
                }

            }
        }

        private class IPATargetInfo
        {
            public long TargetAccountID { get; set; }
            public long PostTargetID { get; set; }
            public long PostID { get; set; }
            public bool PostsRequireApproval { get; set; }
            public long SocialNetworkID { get; set; }

            public long? SyndicatorID { get; set; }
            public string SyndicatorName { get; set; }
            public long? SyndicatorAccountID { get; set; }
            public long? SyndicatorResellerID { get; set; }
            public bool? CanSyndicateWithoutApproval { get; set; }
        }
        private class IPAApprovingUsers
        {
            public long TargetAccountID { get; set; }
            public long UserID { get; set; }
            public string Username { get; set; }
            public long MemberOfAccountID { get; set; }
        }
        private class IPAAccountTemplate
        {
            public long AccountID { get; set; }
            public long NotificationTemplateID { get; set; }
            public long NotificationMessageTypeID { get; set; }
        }
        private class IPANotiSubs
        {
            public long ID { get; set; }
            public long UserID { get; set; }
            public long AccountID { get; set; }
            public long NotificationTemplateID { get; set; }
            public long NotificationMessageTypeID { get; set; }
            public long NotificationMethodID { get; set; }

        }
        private class IPAAutoApprovals
        {
            public long SyndicatorID { get; set; }
            public long ApprovingUserID { get; set; }
            public long AccountID { get; set; }
            public long SocialNetworkID { get; set; }
        }
        private class IPAPostApprovals
        {
            public long PostTargetID { get; set; }
            public long? UserID { get; set; }
            public long? SetByUserID { get; set; }
            public long? PostApprovalGroupID { get; set; }
            public bool AutoApproved { get; set; }
            public DateTime? DateApproved { get; set; }
            public DateTime? DateRejected { get; set; }
            public long? NotificationID { get; set; }
        }

        private void setPostTargetNoApproverHoldDate(long postTargetID, DateTime? NoApproverHoldDate)
        {
            execNonQuery(
                string.Format("update PostTargets set NoApproverHoldDate={0} where ID={1}",
                    NoApproverHoldDate.HasValue ? "'" + NoApproverHoldDate.ToString() + "'" : "null",
                    postTargetID
                )
            );
        }

        private void clearApproversForPostTarget(long postTargetID)
        {
            //execNonQuery(
            //    string.Format("delete PostApprovals where PostTargetID={0}", postTargetID)
            //);

            using (MainEntities db = ContextFactory.Main)
            {
                db.spClearApproversForPostTarget(postTargetID);
            }
        }

        private long getOrCreatePostApprovalGroup(List<long> userIDs)
        {
            using (MainEntities db = ContextFactory.Main)
            {
                if (db.Connection.State != System.Data.ConnectionState.Open)
                    db.Connection.Open();

                using (DbCommand cmd = db.CreateStoreCommand("spGetOrCreatePostApprovalGroup", System.Data.CommandType.StoredProcedure,
                    new SqlParameter("@UserIDs", string.Join("|", userIDs.ToArray()))
                ))
                {
                    using (DbDataReader reader = cmd.ExecuteReader())
                    {
                        if (reader.Read())
                        {
                            return Util.GetReaderValue<long>(reader, "PostApprovalGroupID", 0);
                        }
                    }
                }

            }

            return 0;
        }

        #endregion

        #endregion

        #region post search

        public PostSearchResultsDTO PostSearch(UserInfoDTO userInfo, PostSearchRequestDTO req)
        {
            List<long> aids = null;
            List<long> sids = null;
            if (req.AccountIDs != null && req.AccountIDs.Any())
            {
                AccountListBuilder builder = AccountListBuilder.New(new SecurityProvider());
                aids = builder
                    .AddAccountIDs(req.AccountIDs)
                    .ExpandToDescendants()
                    .FilterToAccountsWithPermission(userInfo, PermissionTypeEnum.accounts_view)
                    .GetIDs(SecurityProvider.PLATFORM_MAX_ACCOUNT_ID_LIST)
                ;
                sids = builder.GetSyndicatorIDs();
            }
            else
            {
                AccountListBuilder builder = AccountListBuilder.New(new SecurityProvider());
                aids = builder
                    .AddAccountsWithPermission(userInfo, PermissionTypeEnum.accounts_view)
                    .GetIDs(SecurityProvider.PLATFORM_MAX_ACCOUNT_ID_LIST)
                ;
                sids = builder.GetSyndicatorIDs();
            }

            ITimezoneProvider tz = ProviderFactory.TimeZones;
            TimeZoneInfo timezone = tz.GetTimezoneForUserID(userInfo.EffectiveUser.ID.Value);

            using (MainEntities db = ContextFactory.Main)
            {
                if (db.Connection.State != System.Data.ConnectionState.Open)
                    db.Connection.Open();

                using (DbCommand cmd = db.CreateStoreCommand(
                   "spSearchPosts5",
                    System.Data.CommandType.StoredProcedure,

                    new SqlParameter("@AccountIDs", (aids == null) ? null : string.Join("|", aids.ToArray())),
                    new SqlParameter("@IncludeSyndicatorIDs", (sids == null) ? null : string.Join("|", sids.ToArray())),
                    new SqlParameter("@PostIDs", (req.PostIDs == null) ? null : string.Join("|", req.PostIDs.ToArray())),
                    new SqlParameter("@FranchiseTypeIDs", (req.FranchiseTypeIDs == null) ? null : string.Join("|", req.FranchiseTypeIDs)),
                    new SqlParameter("@SocialNetworkIDs", (req.SocialNetworks == null) ? null : string.Join("|", (from sn in req.SocialNetworks select (long)sn).ToList())),

                    new SqlParameter("@MinScheduleDate", req.MinDateScheduled != null ? (DateTime?)req.MinDateScheduled.GetUTCForGivenTimezone(timezone) : null),
                    new SqlParameter("@MaxScheduleDate", req.MaxDateScheduled != null ? (DateTime?)req.MaxDateScheduled.GetUTCForGivenTimezone(timezone) : null),

                    new SqlParameter("@SortBy", req.SortBy),
                    new SqlParameter("@SortAscending", req.SortAscending),

                    new SqlParameter("@StartIndex", req.StartIndex),
                    new SqlParameter("@EndIndex", req.EndIndex)
               ))
                {
                    PostSearchResultsDTO res = new PostSearchResultsDTO();

                    using (DbDataReader reader = cmd.ExecuteReader())
                    {
                        if (reader.Read())
                        {
                            res.TotalRecords = Util.GetReaderValue<int>(reader, "Total", 0);
                            if (reader.NextResult())
                            {
                                while (reader.Read())
                                {
                                    PostSearchResultItemDTO item = new PostSearchResultItemDTO()
                                    {
                                        PostID = Util.GetReaderValue<long>(reader, "PostID", 0),
                                        Type = (PostTypeEnum)Util.GetReaderValue<long>(reader, "PostTypeID", 0),
                                        UserID = Util.GetReaderValue<long>(reader, "UserID", 0),

                                        SyndicatorID = Util.GetReaderValue<long?>(reader, "SyndicatorID", null),
                                        SyndicatorAccountID = Util.GetReaderValue<long?>(reader, "SyndicatorAccountID", null),

                                        Username = Util.GetReaderValue<string>(reader, "Username", ""),
                                        FirstName = Util.GetReaderValue<string>(reader, "FirstName", ""),
                                        LastName = Util.GetReaderValue<string>(reader, "LastName", ""),
                                        Title = Util.GetReaderValue<string>(reader, "Title", ""),

                                        FacebookCount = Util.GetReaderValue<int>(reader, "FacebookCount", 0),
                                        TwitterCount = Util.GetReaderValue<int>(reader, "TwitterCount", 0),
                                        GooglePlusCount = Util.GetReaderValue<int>(reader, "GooglePlusCount", 0),

                                        Total = Util.GetReaderValue<int>(reader, "Total", 0),
                                        Completed = Util.GetReaderValue<int>(reader, "Completed", 0),
                                        Failed = Util.GetReaderValue<int>(reader, "Failed", 0),

                                        DealerCount = Util.GetReaderValue<int>(reader, "DealerCount", 0),
                                        DealerApproved = Util.GetReaderValue<int>(reader, "DealerApproved", 0),
                                        DealerDenied = Util.GetReaderValue<int>(reader, "DealerDenied", 0),

                                        FacebookApproved = Util.GetReaderValue<int>(reader, "FacebookApproved", 0),
                                        FacebookDenied = Util.GetReaderValue<int>(reader, "FacebookDenied", 0),
                                        FacebookFailed = Util.GetReaderValue<int>(reader, "FacebookFailed", 0),
                                        FacebookSucceeded = Util.GetReaderValue<int>(reader, "FacebookSucceeded", 0),

                                        GooglePlusApproved = Util.GetReaderValue<int>(reader, "GooglePlusApproved", 0),
                                        GooglePlusDenied = Util.GetReaderValue<int>(reader, "GooglePlusDenied", 0),
                                        GooglePlusFailed = Util.GetReaderValue<int>(reader, "GooglePlusFailed", 0),
                                        GooglePlusSucceeded = Util.GetReaderValue<int>(reader, "GooglePlusSucceeded", 0),

                                        TwitterApproved = Util.GetReaderValue<int>(reader, "TwitterApproved", 0),
                                        TwitterDenied = Util.GetReaderValue<int>(reader, "TwitterDenied", 0),
                                        TwitterFailed = Util.GetReaderValue<int>(reader, "TwitterFailed", 0),
                                        TwitterSucceeded = Util.GetReaderValue<int>(reader, "TwitterSucceeded", 0),

                                        DealersFailed = Util.GetReaderValue<int>(reader, "DealerFailureCount", 0),
                                        DealersSucceeded = Util.GetReaderValue<int>(reader, "DealerSuccessCount", 0),

                                        Date = SIDateTime.CreateSIDateTime(Util.GetReaderValue<DateTime?>(reader, "TheDate", null), timezone),

                                        MinPublishedDate = SIDateTime.CreateSIDateTime(Util.GetReaderValue<DateTime?>(reader, "MinPublishedDate", null), timezone),
                                        MaxPublishedDate = SIDateTime.CreateSIDateTime(Util.GetReaderValue<DateTime?>(reader, "MaxPublishedDate", null), timezone),
                                        MinScheduleDate = SIDateTime.CreateSIDateTime(Util.GetReaderValue<DateTime?>(reader, "MinScheduleDate", null), timezone),
                                        MaxScheduleDate = SIDateTime.CreateSIDateTime(Util.GetReaderValue<DateTime?>(reader, "MaxScheduleDate", null), timezone),
                                        MinFailedDate = SIDateTime.CreateSIDateTime(Util.GetReaderValue<DateTime?>(reader, "MinFailedDate", null), timezone),
                                        MaxFailedDate = SIDateTime.CreateSIDateTime(Util.GetReaderValue<DateTime?>(reader, "MaxFailedDate", null), timezone),
                                        MinPostExpirationDate = SIDateTime.CreateSIDateTime(Util.GetReaderValue<DateTime?>(reader, "MinPostExpirationDate", null), timezone),
                                        MaxPostExpirationDate = SIDateTime.CreateSIDateTime(Util.GetReaderValue<DateTime?>(reader, "MaxPostExpirationDate", null), timezone),

                                        ApprovalExpirationDate = SIDateTime.CreateSIDateTime(Util.GetReaderValue<DateTime?>(reader, "ApprovalExpirationDate", null), timezone),

                                        AllowDelete = false,
                                        AllowEdit = false
                                    };

                                    res.Items.Add(item);
                                }

                            }

                            Dictionary<long, List<long>> postAccounts = new Dictionary<long, List<long>>();
                            if (reader.NextResult())
                            {
                                // get the target list
                                while (reader.Read())
                                {
                                    long postID = Util.GetReaderValue<long>(reader, "PostID", 0);
                                    long accountID = Util.GetReaderValue<long>(reader, "AccountID", 0);

                                    if (postID > 0 && accountID > 0)
                                    {
                                        if (!postAccounts.ContainsKey(postID))
                                            postAccounts.Add(postID, new List<long>());

                                        if (!postAccounts[postID].Contains(accountID))
                                            postAccounts[postID].Add(accountID);
                                    }
                                }
                            }

                            // can edit / can delete

                            foreach (PostSearchResultItemDTO item in res.Items)
                            {
                                // it is a syndicated post
                                if (item.SyndicatorID.HasValue && item.SyndicatorID.Value > 0 && item.SyndicatorAccountID.HasValue && item.SyndicatorAccountID.Value > 0)
                                {
                                    // the user can publish on the syndicator account
                                    if (ProviderFactory.Security.HasPermission(userInfo.EffectiveUser.ID.Value, item.SyndicatorAccountID.Value, PermissionTypeEnum.social_publish))
                                    {
                                        item.AllowDelete = true;

                                        // it has not been published
                                        if (item.MinPublishedDate == null)
                                        {
                                            // it has not been scheduled
                                            if (item.MinScheduleDate != null && item.MinScheduleDate.GetUTCDate(timezone) >= DateTime.UtcNow.AddMinutes(60))
                                            {
                                                // approval has not expired

                                                if (item.ApprovalExpirationDate == null || item.ApprovalExpirationDate.GetUTCForGivenTimezone(timezone) > DateTime.UtcNow)
                                                {
                                                    item.AllowEdit = true;
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }

                    return res;
                }
            }

        }

        public PostTargetSearchResultsDTO PostTargetSearch(UserInfoDTO userInfo, PostTargetSearchRequestDTO req)
        {

            List<long> aids = null;
            List<long> sids = null;

            if (req.AccountIDs != null && req.AccountIDs.Any())
            {
                AccountListBuilder builder = AccountListBuilder
                    .New(new SecurityProvider())
                    .AddAccountIDs(req.AccountIDs)
                    .ExpandToDescendants()
                    .FilterToAccountsWithPermission(userInfo, PermissionTypeEnum.social_view)
                ;
                aids = builder.GetIDs(SecurityProvider.PLATFORM_MAX_ACCOUNT_ID_LIST);
                sids = builder.GetSyndicatorIDs();
            }
            else
            {
                AccountListBuilder builder = AccountListBuilder
                    .New(new SecurityProvider())
                    .AddAccountsWithPermission(userInfo, PermissionTypeEnum.social_view)
                ;
                aids = builder.GetIDs(30000);
                sids = builder.GetSyndicatorIDs();
            }



            List<long> pubList;
            List<long> pubListSynd;
            {
                AccountListBuilder builder = AccountListBuilder
                    .New(new SecurityProvider())
                    .AddAccountsWithPermission(userInfo, PermissionTypeEnum.social_publish)
                ;
                pubList = builder.GetIDs(30000);
                pubListSynd = builder.GetSyndicatorIDs();
            }

            ITimezoneProvider tz = ProviderFactory.TimeZones;
            TimeZoneInfo timezone = tz.GetTimezoneForUserID(userInfo.EffectiveUser.ID.Value);

            using (MainEntities db = ContextFactory.Main)
            {
                if (db.Connection.State != System.Data.ConnectionState.Open)
                    db.Connection.Open();

                using (DbCommand cmd = db.CreateStoreCommand(
                   "spSearchPostTargets4",
                    System.Data.CommandType.StoredProcedure,

                    new SqlParameter("@includeSyndicatorIDs", (sids == null) ? null : string.Join("|", sids.ToArray())),
                    new SqlParameter("@accountIDs", (aids == null) ? null : string.Join("|", aids.ToArray())),
                    new SqlParameter("@postIDs", (req.PostIDs == null) ? null : string.Join("|", req.PostIDs.ToArray())),
                    new SqlParameter("@FranchiseTypeIDs", (req.FranchiseTypeIDs == null) ? null : string.Join("|", req.FranchiseTypeIDs)),
                    new SqlParameter("@SocialNetworkIDs", (req.SocialNetworks == null) ? null : string.Join("|", (from sn in req.SocialNetworks select (long)sn).ToList())),

                    new SqlParameter("@MinScheduleDate", req.MinDateScheduled != null ? (DateTime?)req.MinDateScheduled.GetUTCForGivenTimezone(timezone) : null),
                    new SqlParameter("@MaxScheduleDate", req.MaxDateScheduled != null ? (DateTime?)req.MaxDateScheduled.GetUTCForGivenTimezone(timezone) : null),

                    new SqlParameter("@PostTypeID", req.PostType.HasValue ? (long?)req.PostType.Value : null),
                    new SqlParameter("@PostStatus", req.PostStatus.HasValue ? (long?)req.PostStatus.Value : null),
                    new SqlParameter("@PostedByUserID", req.PostedByUserID),

                    new SqlParameter("@SortBy", req.SortBy),
                    new SqlParameter("@SortAscending", req.SortAscending),

                    new SqlParameter("@StartIndex", req.StartIndex),
                    new SqlParameter("@EndIndex", req.EndIndex)
               ))
                {
                    PostTargetSearchResultsDTO res = new PostTargetSearchResultsDTO();

                    using (DbDataReader reader = cmd.ExecuteReader())
                    {
                        if (reader.Read())
                        {
                            res.TotalRecords = Util.GetReaderValue<int>(reader, "Total", 0);
                            if (reader.NextResult())
                            {
                                while (reader.Read())
                                {
                                    PostTargetSearchResultItemDTO item = new PostTargetSearchResultItemDTO()
                                    {
                                        PostID = Util.GetReaderValue<long>(reader, "PostID", 0),
                                        PostTargetID = Util.GetReaderValue<long>(reader, "PostTargetID", 0),
                                        CredentialID = Util.GetReaderValue<long>(reader, "CredentialID", 0),
                                        SocialNetwork = (SocialNetworkEnum)Util.GetReaderValue<long>(reader, "SocialNetworkID", 0),
                                        Type = (PostTypeEnum)Util.GetReaderValue<long>(reader, "PostTypeID", 0),
                                        UserID = Util.GetReaderValue<long>(reader, "UserID", 0),
                                        Username = Util.GetReaderValue<string>(reader, "Username", ""),
                                        FirstName = Util.GetReaderValue<string>(reader, "FirstName", ""),
                                        LastName = Util.GetReaderValue<string>(reader, "LastName", ""),
                                        Status = (PostStatusEnum)Util.GetReaderValue<int>(reader, "PostStatusEnum", 0),
                                        Title = Util.GetReaderValue<string>(reader, "Title", ""),
                                        Actions = Util.GetReaderValue<int>(reader, "Actions", 0),
                                        Impressions = Util.GetReaderValue<int>(reader, "Impressions", 0),
                                        AccountName = Util.GetReaderValue<string>(reader, "AccountName", ""),
                                        AccountID = Util.GetReaderValue<long>(reader, "AccountID", 0),
                                        SocialNetworkPageURL = Util.GetReaderValue<string>(reader, "SocialNetworkPageURL", ""),
                                        SocialNetworkScreenName = Util.GetReaderValue<string>(reader, "SocialNetworkScreenName", ""),
                                        FailureResponse = Util.GetReaderValue<string>(reader, "FailureResponse", ""),
                                        PostImageURL = Util.GetReaderValue<string>(reader, "PostImageURL", ""),
                                        SyndicatorID = Util.GetReaderValue<long?>(reader, "SyndicatorID", null),

                                        PostExpirationDate = SIDateTime.CreateSIDateTime(Util.GetReaderValue<DateTime?>(reader, "PostExpirationDate", null), timezone),
                                        ApprovalExpirationDate = SIDateTime.CreateSIDateTime(Util.GetReaderValue<DateTime?>(reader, "ApprovalExpirationDate", null), timezone),

                                        CanEdit = false,
                                        CanDelete = true,
                                        CanRepost = true
                                    };

                                    if (item.SyndicatorID.HasValue)
                                    {
                                        if (pubListSynd.Contains(item.SyndicatorID.Value)) item.CanEdit = true;
                                    }
                                    else
                                    {
                                        if (pubList.Contains(item.AccountID)) item.CanEdit = true;
                                    }

                                    if (item.ApprovalExpirationDate != null && item.ApprovalExpirationDate.GetUTCForGivenTimezone(timezone) < DateTime.UtcNow)
                                    {
                                        item.CanEdit = false;
                                    }

                                    res.Items.Add(item);


                                    DateTime? schedDate = Util.GetReaderValue<DateTime?>(reader, "ScheduleDate", null);
                                    DateTime dateCreated = Util.GetReaderValue<DateTime>(reader, "DateCreated", DateTime.UtcNow);
                                    item.CreateDate = new SIDateTime(dateCreated, timezone);

                                    if (item.SyndicatorID.HasValue)
                                    {
                                        item.PostTargetType = PostTargetTypeEnum.Syndicated;
                                    }
                                    else if (schedDate.HasValue)
                                    {
                                        item.PostTargetType = PostTargetTypeEnum.Scheduled;
                                    }
                                    else
                                    {
                                        item.PostTargetType = PostTargetTypeEnum.Immediate;
                                    }

                                    switch (item.Status)
                                    {
                                        case PostStatusEnum.Scheduled:
                                            item.StatusName = "Scheduled";
                                            if (schedDate.HasValue)
                                            {
                                                item.StatusDate = new SIDateTime(schedDate.Value, timezone);
                                                item.StatusTimspan = DateTime.UtcNow - schedDate.Value;
                                            }
                                            break;

                                        case PostStatusEnum.Processing:
                                            item.StatusName = "Processing";
                                            DateTime? puDate = Util.GetReaderValue<DateTime?>(reader, "PickedUpDate", null);
                                            if (puDate.HasValue)
                                            {
                                                item.StatusDate = new SIDateTime(puDate.Value, timezone);
                                                item.StatusTimspan = DateTime.UtcNow - puDate.Value;
                                            }
                                            break;
                                        case PostStatusEnum.Published:
                                            item.StatusName = "Published";
                                            DateTime? pubDate = Util.GetReaderValue<DateTime?>(reader, "PublishedDate", null);
                                            if (pubDate.HasValue)
                                            {
                                                item.StatusDate = new SIDateTime(pubDate.Value, timezone);
                                                item.StatusTimspan = DateTime.UtcNow - pubDate.Value;
                                            }
                                            break;
                                        case PostStatusEnum.Failed:
                                            {
                                                item.StatusName = "Failed";
                                                DateTime? puDate2 = Util.GetReaderValue<DateTime?>(reader, "PickedUpDate", null);
                                                if (puDate2.HasValue)
                                                {
                                                    item.StatusDate = new SIDateTime(puDate2.Value, timezone);
                                                    item.StatusTimspan = DateTime.UtcNow - puDate2.Value;
                                                }
                                            }
                                            break;
                                        case PostStatusEnum.WaitingOnApproval:
                                            {
                                                item.StatusName = "Waiting On Approval";
                                                if (schedDate.HasValue)
                                                {
                                                    item.StatusDate = new SIDateTime(schedDate.Value, timezone);
                                                    item.StatusTimspan = DateTime.UtcNow - schedDate.Value;
                                                }
                                            }
                                            break;
                                        case PostStatusEnum.SyndicationwithNoApproverHold:
                                            {
                                                item.StatusName = "No Approvers Hold";
                                                if (schedDate.HasValue)
                                                {
                                                    item.StatusDate = new SIDateTime(schedDate.Value, timezone);
                                                    item.StatusTimspan = DateTime.UtcNow - schedDate.Value;
                                                }
                                            }
                                            break;
                                        case PostStatusEnum.Rejected:
                                            {
                                                item.StatusName = "Rejected";
                                                if (schedDate.HasValue)
                                                {
                                                    item.StatusDate = new SIDateTime(schedDate.Value, timezone);
                                                    item.StatusTimspan = DateTime.UtcNow - schedDate.Value;
                                                }
                                            }
                                            break;
                                        default:
                                            break;
                                    }
                                }

                            }
                        }
                    }

                    return res;
                }
            }

            return null;
        }

        #endregion

        #region social network statistics

        public bool SaveFaceBookPage(Page facebookPageNew, long credentialId)
        {
            using (MainEntities db = ContextFactory.Main)
            {
                DateTime _QueryDate = Convert.ToDateTime(DateTime.UtcNow.ToShortDateString());

                FaceBookPage faceBookPage = db.FaceBookPages.Where(p => p.CredentialID == credentialId && p.QueryDate == _QueryDate).FirstOrDefault();

                if (faceBookPage == null)
                {
                    faceBookPage = new FaceBookPage()
                    {
                        CheckIns = facebookPageNew.checkins,
                        CredentialID = credentialId,
                        DateCreated = DateTime.UtcNow,
                        UnreadNotify = facebookPageNew.unread_notif_count,
                        NewLike = facebookPageNew.new_like_count,
                        Likes = facebookPageNew.likes,
                        UnreadMessage = facebookPageNew.unread_message_count,
                        UnseenMessage = facebookPageNew.unseen_message_count,
                        WereHere = facebookPageNew.were_here_count,
                        TalkingAbout = facebookPageNew.talking_about_count,
                        QueryDate = DateTime.UtcNow
                    };
                    db.FaceBookPages.AddObject(faceBookPage);
                }
                else
                {
                    faceBookPage.CheckIns = facebookPageNew.checkins;
                    faceBookPage.UnreadNotify = facebookPageNew.unread_notif_count;
                    faceBookPage.NewLike = facebookPageNew.new_like_count;
                    faceBookPage.Likes = facebookPageNew.likes;
                    faceBookPage.UnreadMessage = facebookPageNew.unread_message_count;
                    faceBookPage.UnseenMessage = facebookPageNew.unseen_message_count;
                    faceBookPage.WereHere = facebookPageNew.were_here_count;
                    faceBookPage.TalkingAbout = facebookPageNew.talking_about_count;
                }


                db.SaveChanges();

                long? accountID = (from c in db.Credentials where c.ID == faceBookPage.CredentialID select c.AccountID).FirstOrDefault();

                if (accountID.HasValue && accountID.Value > 0)
                {
                    DateTime queryDate = DateTime.Now.Date;
                    execNonQuery(
                        string.Format(
                            "IF Not EXISTS(SELECT * FROM [UpdateQueue.FacebookPage] where AccountID = {0} and QueryDate = CAST('{1}' AS date))	BEGIN	insert [UpdateQueue.FacebookPage] (AccountID, QueryDate) values ({0},'{1}')	END",
                            accountID, queryDate.ToString("yyyy-MM-dd")
                        )
                    );
                }
                return true;
            }
        }

        public bool SaveFaceBookPost(long credentialId, string feedId, string resultId, string name, string message, PostTypeEnum PostTypeID, string linkURL, string pictureURL, DateTime FBCreatedTime, DateTime FBUpdatedTime, string statusType)
        {
            bool result = false;

            //PostTypeEnum type
            using (MainEntities db = ContextFactory.Main)
            {
                long? SIPostTargetID = null;

                PostTarget postTarget = db.PostTargets.Where(p => p.CredentialID == credentialId && p.ResultID == resultId).FirstOrDefault();

                if (postTarget != null)
                {
                    SIPostTargetID = postTarget.ID;
                }
                else
                {
                    //PostImageResult postImageResult = 

                    long postTargetID = (from pt in db.PostTargets
                                         join pir in db.PostImageResults on pt.ID equals pir.PostTargetID
                                         where pt.CredentialID == credentialId &&
                                             pir.ResultID == resultId
                                         select pir.PostTargetID).FirstOrDefault();

                    if (postTargetID > 0)
                    {
                        SIPostTargetID = postTargetID;
                    }
                }

                FaceBookPost faceBookPost = db.FaceBookPosts.Where(p => p.CredentialID == credentialId && p.ResultID == resultId).FirstOrDefault();

                //FaceBookPost faceBookPosts; 
                if (faceBookPost == null)
                {
                    faceBookPost = new FaceBookPost()
                    {
                        CredentialID = credentialId,
                        FeedID = feedId,
                        ResultID = resultId,
                        SIPostTargetID = SIPostTargetID,
                        DateCreated = DateTime.UtcNow,
                        FBCreatedTime = FBCreatedTime,
                        FBUpdatedTime = FBUpdatedTime,
                        Name = name,
                        Message = message,
                        PostTypeID = (long)PostTypeID,
                        LinkURL = linkURL,
                        PictureURL = pictureURL,
                        StatusType = statusType
                    };

                    db.FaceBookPosts.AddObject(faceBookPost);
                }
                else
                {
                    faceBookPost.FBUpdatedTime = FBUpdatedTime;
                    faceBookPost.Name = name;
                    faceBookPost.Message = message;
                    faceBookPost.LinkURL = linkURL;
                    faceBookPost.PictureURL = pictureURL;
                    faceBookPost.SIPostTargetID = SIPostTargetID;
                    faceBookPost.PostTypeID = (long)PostTypeID;
                    faceBookPost.StatusType = statusType;
                }

                db.SaveChanges();

                result = true;
            }

            return result;
        }

        public bool SaveGooglePlusPost(long credentialId, List<GooglePlusPostDTO> listgooglePlusPostDTO)
        {
            int statementCount = 0;
            StringBuilder sbSQL = new StringBuilder();
            if (listgooglePlusPostDTO.Any())
            {
                foreach (GooglePlusPostDTO post in listgooglePlusPostDTO)
                {

                    sbSQL.AppendFormat(" EXEC spUpsertGooglePlusPost ");
                    sbSQL.AppendFormat(" @CredentialID={0}", credentialId);
                    sbSQL.AppendFormat(" ,@ResultID='{0}'", Util.SafeSQL(post.ResultID));
                    sbSQL.AppendFormat(" ,@GooglePlusPostID='{0}'", Util.SafeSQL(post.GooglePlusPostID));
                    sbSQL.AppendFormat(" ,@Name='{0}'", Util.SafeSQL(post.Name));
                    sbSQL.AppendFormat(" ,@Message='{0}'", Util.SafeSQL(post.Message));
                    sbSQL.AppendFormat(" ,@PostTypeID={0}", post.PostTypeID);
                    sbSQL.AppendFormat(" ,@LinkURL='{0}'", Util.SafeSQL(post.LinkURL));
                    sbSQL.AppendFormat(" ,@PictureURL='{0}'", Util.SafeSQL(post.PictureURL));
                    sbSQL.AppendFormat(" ,@GooglePlusCreatedTime='{0}'", post.GooglePlusCreatedTime);
                    sbSQL.AppendFormat(" ,@GooglePlusUpdatedTime='{0}'", post.GooglePlusUpdatedTime);
                    sbSQL.Append("\r\n");

                    statementCount++;
                    if (statementCount > 100)
                    {
                        statementCount = 0;
                        execNonQuery(sbSQL.ToString());
                        sbSQL.Clear();
                    }
                }

                if (statementCount > 0)
                {
                    execNonQuery(sbSQL.ToString());
                }
            }
            return true;
        }

        public bool SaveGooglePlusPost(long credentialId, string resultId, string googlePlusPostID, string name, string message, PostTypeEnum PostTypeID, string linkURL, string pictureURL, DateTime GooglePlusCreatedTime, DateTime GooglePlusUpdatedTime)
        {
            bool result = false;

            //PostTypeEnum type
            using (MainEntities db = ContextFactory.Main)
            {
                long? SIPostTargetID = null;

                PostTarget postTarget = db.PostTargets.Where(p => p.CredentialID == credentialId && p.ResultID == resultId).FirstOrDefault();

                if (postTarget != null)
                    SIPostTargetID = postTarget.ID;

                GooglePlusPost googlePlusPost = db.GooglePlusPosts.Where(p => p.CredentialID == credentialId && p.ResultID == resultId).FirstOrDefault();

                //FaceBookPost faceBookPosts; 
                if (googlePlusPost == null)
                {
                    googlePlusPost = new GooglePlusPost()
                    {
                        CredentialID = credentialId,
                        ResultID = resultId,
                        GooglePlusPostID = googlePlusPostID,
                        SIPostTargetID = SIPostTargetID,
                        DateCreated = DateTime.UtcNow,
                        GooglePlusCreatedTime = GooglePlusCreatedTime,
                        GooglePlusUpdatedTime = GooglePlusUpdatedTime,
                        Name = name,
                        Message = message,
                        PostTypeID = (long)PostTypeID,
                        LinkURL = linkURL,
                        PictureURL = pictureURL
                    };

                    db.GooglePlusPosts.AddObject(googlePlusPost);
                }
                else
                {
                    googlePlusPost.GooglePlusUpdatedTime = GooglePlusUpdatedTime;
                    googlePlusPost.Message = message;
                    googlePlusPost.LinkURL = linkURL;
                    googlePlusPost.PictureURL = pictureURL;
                }

                db.SaveChanges();

                result = true;
            }

            return result;
        }

        public bool SaveFaceBookPost(PostStatistics facebookPost, long credentialId, string feedId, string resultId, long FacebookPostID)
        {

            StringBuilder sbSQL = new StringBuilder();

            int statementCount = 0;

            if (facebookPost.comments != null && facebookPost.comments.data.Any())
            {
                foreach (DataComments comment in facebookPost.comments.data)
                {

                    sbSQL.AppendFormat(" EXEC spUpsertFacebookComment ");
                    if (comment.from == null)
                    {
                        comment.from = new From();
                        comment.from.id = "000000000";
                        comment.from.name = "Undefined User";
                        comment.from.pictureurl = "";
                    }
                    sbSQL.AppendFormat(" @FromID='{0}'", Util.SafeSQL(comment.from.id));
                    sbSQL.AppendFormat(" ,@FromName='{0}'", Util.SafeSQL(comment.from.name));
                    sbSQL.AppendFormat(" ,@PictureURL='{0}'", Util.SafeSQL(comment.from.pictureurl));
                    sbSQL.AppendFormat(" ,@FacebookPostID={0}", FacebookPostID);
                    sbSQL.AppendFormat(" ,@FBCommentID='{0}'", Util.SafeSQL(comment.id));
                    sbSQL.AppendFormat(" ,@Message='{0}'", Util.SafeSQL(comment.message ?? string.Empty));
                    sbSQL.AppendFormat(" ,@FBCreateTime='{0}'", comment.created_time);
                    sbSQL.AppendFormat(" ,@UserLikes={0}", comment.user_likes ? 1 : 0);
                    sbSQL.AppendFormat(" ,@LikeCount={0}", comment.like_count);
                    sbSQL.Append("\r\n");

                    statementCount++;
                    if (statementCount > 50)
                    {
                        statementCount = 0;
                        execNonQuery(sbSQL.ToString());
                        sbSQL.Clear();
                    }

                    if (comment.likes != null && comment.likes.data.Any())
                    {
                        foreach (DataLikes like in comment.likes.data)
                        {
                            sbSQL.AppendFormat(" EXEC  spUpsertFacebookCommentLike ");
                            sbSQL.AppendFormat(" @FromID='{0}' ", Util.SafeSQL(like.id));
                            sbSQL.AppendFormat(" ,@FromName='{0}' ", Util.SafeSQL(like.name));
                            sbSQL.AppendFormat(" ,@PictureURL='' "); //todo: update caller so it provides picture url for likes
                            sbSQL.AppendFormat(" ,@FBCommentID='{0}' ", Util.SafeSQL(comment.id));
                            sbSQL.Append("\r\n");

                            statementCount++;
                            if (statementCount > 50)
                            {
                                statementCount = 0;
                                execNonQuery(sbSQL.ToString());
                                sbSQL.Clear();
                            }
                        }
                    }

                }
            }

            if (facebookPost.sharedposts != null && facebookPost.sharedposts.data.Any())
            {
                foreach (DataSharedposts sharedPost in facebookPost.sharedposts.data)
                {
                    sbSQL.AppendFormat(" EXEC spUpsertFacebookShare ");
                    if (sharedPost.from == null)
                    {
                        sharedPost.from = new From();
                        sharedPost.from.id = "000000000";
                        sharedPost.from.name = "Undefined User";
                        sharedPost.from.pictureurl = "";
                    }
                    sbSQL.AppendFormat(" @FromID='{0}'", Util.SafeSQL(sharedPost.from.id));
                    sbSQL.AppendFormat(" ,@FromName='{0}'", Util.SafeSQL(sharedPost.from.name));
                    sbSQL.AppendFormat(" ,@PictureURL='{0}'", Util.SafeSQL(sharedPost.from.pictureurl));
                    sbSQL.AppendFormat(" ,@FacebookPostID={0}", FacebookPostID);
                    sbSQL.AppendFormat(" ,@FBSharedPostID='{0}'", Util.SafeSQL(sharedPost.id));
                    sbSQL.AppendFormat(" ,@FBCreateTime='{0}'", sharedPost.created_time);
                    sbSQL.AppendFormat(" ,@Message='{0}'", Util.SafeSQL(sharedPost.message ?? string.Empty));
                    sbSQL.Append("\r\n");

                    statementCount++;
                    if (statementCount > 50)
                    {
                        statementCount = 0;
                        execNonQuery(sbSQL.ToString());
                        sbSQL.Clear();
                    }

                }
            }

            if (facebookPost.likes != null && facebookPost.likes.data.Any())
            {
                foreach (DataLikes like in facebookPost.likes.data)
                {
                    sbSQL.AppendFormat(" EXEC spUpsertFacebookPostLike ");
                    sbSQL.AppendFormat(" @FromID='{0}' ", Util.SafeSQL(like.id));
                    sbSQL.AppendFormat(" ,@FromName='{0}' ", Util.SafeSQL(like.name));
                    sbSQL.AppendFormat(" ,@PictureURL='' "); //todo: update caller so it provides picture url for likes
                    sbSQL.AppendFormat(" ,@FacebookPostID={0} ", FacebookPostID);
                    sbSQL.Append("\r\n");

                    statementCount++;
                    if (statementCount > 50)
                    {
                        statementCount = 0;
                        execNonQuery(sbSQL.ToString());
                        sbSQL.Clear();
                    }
                }
            }

            if (statementCount > 0)
            {
                execNonQuery(sbSQL.ToString());
            }
            DateTime queryDate = DateTime.Now;

            if (FacebookPostID > 0)
            {
                execNonQuery(
                    string.Format(
                        "IF Not EXISTS(SELECT * FROM [UpdateQueue.FacebookPostInsights] where FacebookPostID = {0} and QueryDate = CAST('{1}' AS date))	BEGIN	insert [UpdateQueue.FacebookPostInsights] (FacebookPostID, QueryDate) values ({2},'{3}')	END",
                        FacebookPostID, queryDate.ToString("yyyy-MM-dd"), FacebookPostID, queryDate.ToString("MM/dd/yyyy")
                    )
                );
            }

            return true;

        }

        public bool SaveGooglePlusPostStatistics(GooglePlusPostStatistics googlePlusPostStatistics, long GooglePlusPostID)
        {
            StringBuilder sbSQL = new StringBuilder();
            int statementCount = 0;

            if (googlePlusPostStatistics.Comments != null)
            {
                foreach (var comment in googlePlusPostStatistics.Comments.items)
                {
                    sbSQL.AppendFormat(" EXEC spUpsertGooglePlusComment ");
                    sbSQL.AppendFormat(" @FromID='{0}'", Util.SafeSQL(comment.actor.id));
                    sbSQL.AppendFormat(" ,@FromName='{0}'", Util.SafeSQL(comment.actor.displayName));
                    sbSQL.AppendFormat(" ,@ProfileURL='{0}'", Util.SafeSQL(comment.actor.url));
                    sbSQL.AppendFormat(" ,@PictureURL='{0}'", Util.SafeSQL(comment.actor.image.url));
                    sbSQL.AppendFormat(" ,@GooglePlusPostID={0}", GooglePlusPostID);
                    sbSQL.AppendFormat(" ,@GooglePlusCommentID='{0}'", Util.SafeSQL(comment.id));

                    string message = comment.@object.content;
                    string ReplyTo = string.Empty;

                    var doc = new HtmlDocument();
                    doc.OptionFixNestedTags = true;
                    doc.LoadHtml(message);

                    HtmlNode node = doc.DocumentNode.SelectSingleNode("//span[@class='proflinkWrapper']/a");
                    if (node != null)
                    {
                        //Reply
                        int lastindex = message.LastIndexOf("</span>");
                        message = message.Substring(lastindex + 7);
                        ReplyTo = node.GetAttributeValue("href", "");
                    }

                    sbSQL.AppendFormat(" ,@Message='{0}'", Util.SafeSQL(HttpUtility.HtmlDecode(message)));
                    sbSQL.AppendFormat(" ,@GooglePlusPublishedDate='{0}'", comment.published);
                    sbSQL.AppendFormat(" ,@GooglePlusUpdatedDate='{0}'", comment.updated);
                    sbSQL.AppendFormat(" ,@PlusonersCount={0}", comment.plusoners.totalItems);
                    sbSQL.AppendFormat(" ,@ReplyTo='{0}'", Util.SafeSQL(ReplyTo));
                    sbSQL.Append("\r\n");

                    statementCount++;
                    if (statementCount > 50)
                    {
                        statementCount = 0;
                        execNonQuery(sbSQL.ToString());
                        sbSQL.Clear();
                    }
                }
            }
            if (googlePlusPostStatistics.Plusoners != null)
            {
                foreach (var plusoner in googlePlusPostStatistics.Plusoners.items)
                {
                    sbSQL.AppendFormat(" EXEC spUpsertGooglePlusPostPlusoner ");
                    sbSQL.AppendFormat(" @FromID='{0}'", Util.SafeSQL(plusoner.id));
                    sbSQL.AppendFormat(" ,@FromName='{0}'", Util.SafeSQL(plusoner.displayName));
                    sbSQL.AppendFormat(" ,@ProfileURL='{0}'", Util.SafeSQL(plusoner.url));
                    sbSQL.AppendFormat(" ,@GooglePlusPostID={0}", GooglePlusPostID);
                    sbSQL.Append("\r\n");

                    statementCount++;
                    if (statementCount > 50)
                    {
                        statementCount = 0;
                        execNonQuery(sbSQL.ToString());
                        sbSQL.Clear();
                    }
                }
            }

            if (googlePlusPostStatistics.Resharers != null)
            {
                foreach (var resharer in googlePlusPostStatistics.Resharers.items)
                {
                    sbSQL.AppendFormat(" EXEC spUpsertGooglePlusPostResharer ");
                    sbSQL.AppendFormat(" @FromID='{0}'", Util.SafeSQL(resharer.id));
                    sbSQL.AppendFormat(" ,@FromName='{0}'", Util.SafeSQL(resharer.displayName));
                    sbSQL.AppendFormat(" ,@ProfileURL='{0}'", Util.SafeSQL(resharer.url));
                    sbSQL.AppendFormat(" ,@GooglePlusPostID={0}", GooglePlusPostID);
                    sbSQL.Append("\r\n");

                    statementCount++;
                    if (statementCount > 50)
                    {
                        statementCount = 0;
                        execNonQuery(sbSQL.ToString());
                        sbSQL.Clear();
                    }
                }
            }
            if (statementCount > 0)
            {
                execNonQuery(sbSQL.ToString());
            }
            return true;
        }

        private void execNonQuery(string sql)
        {
            using (MainEntities db = ContextFactory.Main)
            {
                if (db.Connection.State != System.Data.ConnectionState.Open)
                    db.Connection.Open();

                using (DbCommand cmd = db.CreateStoreCommand(sql, System.Data.CommandType.Text))
                {
                    cmd.ExecuteNonQuery();
                }
            }
        }

        public bool SaveFaceBookInsights(Insights facebookInsights, long credentialId, DateTime queryDate, long? FacebookPostID)
        {
            bool Success;

            DateTime minDateTime = new DateTime(1753, 1, 2);
            if (queryDate < minDateTime) queryDate = minDateTime;

            using (MainEntities db = ContextFactory.Main)
            {
                int savecount = 0;
                var insightNames = (from i in db.FaceBookInsightsNames select new { ID = i.ID, Name = i.Name, PageOrPost = i.PageOrPost, Period = i.Period }).ToList();

                long accountID = (from c in db.Credentials where c.ID == credentialId select c.AccountID).FirstOrDefault();

                StringBuilder sbSQL = new StringBuilder();

                foreach (DataInsights insight in facebookInsights.data)
                {
                    //get Insight Name
                    FaceBookInsightsName faceBookInsightsName = null;
                    string PageOrPost = string.Empty;

                    if (FacebookPostID == null)
                    {
                        PageOrPost = "page";
                    }
                    else
                    {
                        PageOrPost = "post";
                    }

                    long? insightNameID = (from i in insightNames where i.Name == insight.name && i.PageOrPost == PageOrPost && i.Period == insight.period select i.ID).FirstOrDefault();

                    if (insightNameID == 0)
                    {
                        faceBookInsightsName = new FaceBookInsightsName()
                        {
                            Name = insight.name,
                            DateCreated = DateTime.UtcNow,
                            Description = insight.description,
                            Title = insight.title,
                            Period = insight.period,
                            PageOrPost = PageOrPost
                        };
                        db.FaceBookInsightsNames.AddObject(faceBookInsightsName);
                        db.SaveChanges();
                        insightNameID = faceBookInsightsName.ID;
                    }


                    sbSQL.AppendFormat(" EXEC spUpsertInsight ");
                    sbSQL.AppendFormat(" @CredentialID={0}", credentialId);
                    sbSQL.AppendFormat(" ,@QueryDate='{0}'", queryDate.ToShortDateString());
                    sbSQL.AppendFormat(" ,@FacebookInsightsNameID={0}", insightNameID.Value);
                    sbSQL.AppendFormat(" ,@FacebookPostID={0}", FacebookPostID.HasValue ? FacebookPostID.Value.ToString(CultureInfo.InvariantCulture) : "NULL");
                    sbSQL.AppendFormat(" ,@ValueJSON='{0}'", Util.SafeSQL(JsonConvert.SerializeObject(insight.values)));


                    //get singular value
                    foreach (Value value in insight.values)
                    {
                        bool hasValue = false;
                        int intValue = 0;
                        if (value._value.StartsWith("{"))
                        {
                            Dictionary<string, object> resultDictionary = JsonConvert.DeserializeObject<Dictionary<string, object>>(value._value);
                            if (resultDictionary != null && resultDictionary.Count > 0)
                            {
                                foreach (KeyValuePair<string, object> keyV in resultDictionary)
                                {
                                    int tempValue;
                                    if (keyV.Key != "total")
                                    {
                                        if (int.TryParse(keyV.Value.ToString(), out tempValue))
                                        {
                                            intValue += tempValue;
                                            hasValue = true;
                                        }
                                    }
                                }
                            }
                        }
                        else if (!value._value.StartsWith("["))
                        {
                            int tempValue = 0;
                            if (int.TryParse(value._value, out tempValue))
                            {
                                intValue = tempValue;
                                hasValue = true;
                            }
                        }
                        if (hasValue)
                        {
                            sbSQL.AppendFormat(" ,@Value={0}", intValue);
                            sbSQL.Append("\r\n");
                            //faceBookInsight.Value = intValue;
                        }
                        else
                        {
                            sbSQL.AppendFormat(" ,@Value={0}", 0);
                            sbSQL.Append("\r\n");
                        }

                    }

                    //db.FaceBookInsights.AddObject(faceBookInsight);
                    savecount += db.SaveChanges();
                }


                if (db.Connection.State != System.Data.ConnectionState.Open)
                    db.Connection.Open();

                if (sbSQL.Length > 0)
                {
                    using (DbCommand cmd = db.CreateStoreCommand(sbSQL.ToString(), System.Data.CommandType.Text))
                    {
                        cmd.ExecuteNonQuery();
                    }
                    if (FacebookPostID.HasValue)
                    {
                        execNonQuery(
                            string.Format(
                                 "IF Not EXISTS(SELECT * FROM [UpdateQueue.FacebookPostInsights] where FacebookPostID = {0} and QueryDate = CAST('{1}' AS date))	BEGIN	insert [UpdateQueue.FacebookPostInsights] (FacebookPostID, QueryDate) values ({0},'{1}')	END",
                                FacebookPostID, queryDate.ToString("yyyy-MM-dd")
                            )
                        );
                    }
                    else
                    {
                        execNonQuery(
                            string.Format(
                                 "IF Not EXISTS(SELECT * FROM [UpdateQueue.FacebookPage] where AccountID = {0} and QueryDate = CAST('{1}' AS date))	BEGIN	insert [UpdateQueue.FacebookPage] (AccountID, QueryDate) values ({0},'{1}')	END",
                                accountID, queryDate.ToString("yyyy-MM-dd")
                            )
                        );
                    }
                }

                Success = true;
            }

            return Success;
        }

        public bool SaveTwitterPage(long credentialId, int statuses_count, int followingCount, int followers_count, int listed_count, int favourites_count, int friends_count, int retweet_count, string error)
        {
            bool result = false;

            using (MainEntities db = ContextFactory.Main)
            {
                db.spSaveTwitterPage(0, credentialId
                                        , statuses_count
                                        , followingCount
                                        , followers_count
                                        , listed_count
                                        , favourites_count
                                        , friends_count
                                        , retweet_count
                                        , error);

                result = true;

                // get the accountid
                long? accountID = (from c in db.Credentials where c.ID == credentialId select c.AccountID).FirstOrDefault();
                if (accountID.HasValue && accountID > 0)
                {
                    DateTime queryDate = DateTime.Now.Date;
                    execNonQuery(
                        string.Format(
                            "IF Not EXISTS(SELECT * FROM [UpdateQueue.TwitterPage] where AccountID = {0} and QueryDate = CAST('{1}' AS date))	BEGIN	insert [UpdateQueue.TwitterPage] (AccountID, QueryDate) values ({0},'{1}')	END",
                            accountID, queryDate.ToString("yyyy-MM-dd")
                        )
                    );
                }
            }



            return result;

        }

        public bool SaveTwitterPostStatistics(long PostTargetID, bool? IsFavorited, int? RetweetCount, bool IsRetweeted, string Error)
        {
            bool result = false;

            using (MainEntities db = ContextFactory.Main)
            {
                db.spSaveTwitterPost(0
                                    , PostTargetID
                                    , IsFavorited
                                    , RetweetCount
                                    , IsRetweeted
                                    , Error);

                result = true;

                DateTime queryDate = DateTime.Now.Date;
                execNonQuery(
                    string.Format(
                        "IF Not EXISTS(SELECT * FROM [UpdateQueue.TwitterPost] where PostTargetID = {0} and QueryDate = CAST('{1}' AS date))	BEGIN	insert [UpdateQueue.TwitterPost] (PostTargetID, QueryDate) values ({0},'{1}')	END",
                        PostTargetID, queryDate.ToString("yyyy-MM-dd")
                    )
                );
            }

            return result;
        }

        public bool SaveGooglePlusPage(long credentialId, int PlusOneCount, int FollowerCount, int PostCount, string error)
        {
            bool result = false;

            using (MainEntities db = ContextFactory.Main)
            {
                db.spSaveGooglePlusPage(0, credentialId
                                        , PlusOneCount
                                        , FollowerCount
                                        , PostCount
                                        , error);

                result = true;
            }

            return result;
        }
        public List<long> GetFacebookPostIDs()
        {
            using (MainEntities db = ContextFactory.Main)
            {
                return db.FaceBookPosts.Where(x => x.ID == 12405).Select(p => p.ID).ToList();
            }
        }
        public List<long> GetFacebookPostIDsNeedingStatsUpdate()
        {
            using (MainEntities db = ContextFactory.Main)
            {
                if (db.Connection.State != System.Data.ConnectionState.Open)
                    db.Connection.Open();

                using (DbCommand cmd = db.CreateStoreCommand("spGetFacebookPostStatWorklist"))
                {
                    using (DbDataReader reader = cmd.ExecuteReader())
                    {
                        return db.Translate<long>(reader).ToList();
                    }
                }
            }
        }

        public List<long> GetGooglePlusPostIDsNeedingStatsUpdate()
        {
            using (MainEntities db = ContextFactory.Main)
            {
                if (db.Connection.State != System.Data.ConnectionState.Open)
                    db.Connection.Open();

                using (DbCommand cmd = db.CreateStoreCommand("spGetGooglePlusPostStatWorklist"))
                {
                    using (DbDataReader reader = cmd.ExecuteReader())
                    {
                        return db.Translate<long>(reader).ToList();
                    }
                }
            }
        }

        public void MarkFacebookPostRefreshed(long facebookPostID)
        {
            using (MainEntities db = ContextFactory.Main)
            {
                if (db.Connection.State != System.Data.ConnectionState.Open)
                    db.Connection.Open();

                using (DbCommand cmd = db.CreateStoreCommand(
                    string.Format("update FacebookPosts set DateLastRefreshed=getutcdate() where ID={0}", facebookPostID)
                ))
                {
                    cmd.ExecuteNonQuery();
                }
            }
        }

        public void MarkGooglePlusPostRefreshed(long googlePlusPostID)
        {
            using (MainEntities db = ContextFactory.Main)
            {
                if (db.Connection.State != System.Data.ConnectionState.Open)
                    db.Connection.Open();

                using (DbCommand cmd = db.CreateStoreCommand(
                    string.Format("update GooglePlusposts set DateLastRefreshed=getutcdate() where ID={0}", googlePlusPostID)
                ))
                {
                    cmd.ExecuteNonQuery();
                }
            }
        }

        public List<long> GetTwitterPostIDs()
        {
            using (MainEntities db = ContextFactory.Main)
            {
                var result = (from p in db.PostTargets
                              join c in db.Credentials on p.CredentialID equals c.ID
                              join s in db.SocialApps on c.SocialAppID equals s.ID
                              where s.SocialNetworkID == 3 &&
                                    p.ResultID != "" &&
                                    p.ResultID != null
                              select (p.ID)).ToList();

                return result;
            }
        }

        public FacebookPostInfoDTO GetFacebookPostInfo(long FacebookPostID)
        {
            FacebookPostInfoDTO dto = null;

            using (MainEntities db = ContextFactory.Main)
            {
                dto = (
                    from p in db.FaceBookPosts
                    where p.ID == FacebookPostID
                    select new FacebookPostInfoDTO()
                    {
                        FeedID = p.FeedID,
                        CredentialID = p.CredentialID,
                        ResultID = p.ResultID,
                        PostType = (PostTypeEnum)p.PostTypeID,
                        StatusType = p.StatusType
                    }
                ).FirstOrDefault();

                dto.Credential = GetSocialCredentialInfo(dto.CredentialID);


            }

            return dto;
        }

        public GooglePlusPostInfoDTO GetGooglePlusPostInfo(long GooglePlusPostID)
        {
            GooglePlusPostInfoDTO dto = null;

            using (MainEntities db = ContextFactory.Main)
            {
                dto = (
                    from p in db.GooglePlusPosts
                    where p.ID == GooglePlusPostID
                    select new GooglePlusPostInfoDTO()
                    {
                        GooglePlusPostID = p.GooglePlusPostID,
                        CredentialID = p.CredentialID,
                        ResultID = p.ResultID,
                        PostType = (PostTypeEnum)p.PostTypeID
                    }
                ).FirstOrDefault();

                dto.Credential = GetSocialCredentialInfo(dto.CredentialID);
            }

            return dto;
        }

        #endregion

        public void DeletePost(UserInfoDTO userInfo, long postID)
        {
            Auditor
                .New(AuditLevelEnum.Error, AuditTypeEnum.PublishActivity, userInfo, "")
                .SetAuditDataObject(
                    AuditPublishActivity
                        .New(postID, AuditPublishActivityTypeEnum.PostDeletionInitiated)
                        .SetPostID(postID)
                        .SetDescription("Post Deletion initiated by userID [{0}] for PostID [{1}]", userInfo.EffectiveUser.ID.Value, postID)
                )
                .Save(ProviderFactory.Logging)
            ;

            using (MainEntities db = ContextFactory.Main)
            {
                //long accountID = (from p in db.Posts where p.ID == postID select p.AccountID).FirstOrDefault();
                Post post = (from p in db.Posts where p.ID == postID select p).FirstOrDefault();
                if (ProviderFactory.Security.HasPermission(userInfo.EffectiveUser.ID.Value, post.AccountID, PermissionTypeEnum.social_admin))
                {
                    DeletePostData data = new DeletePostData()
                    {
                        PostID = postID
                    };
                    SaveEntityDTO<JobDTO> res = ProviderFactory.Jobs.Save(new SaveEntityDTO<JobDTO>(
                        new JobDTO()
                        {
                            JobDataObject = data,
                            JobType = data.JobType,
                            DateScheduled = SIDateTime.Now,
                            JobStatus = JobStatusEnum.Queued,
                            Priority = 2000,
                            OriginJobID = null
                        }, userInfo)
                    );
                    if (res.Problems.Any())
                    {
                        Auditor
                            .New(AuditLevelEnum.Error, AuditTypeEnum.PublishActivity, userInfo, "")
                            .SetAuditDataObject(
                                AuditPublishActivity
                                    .New(postID, AuditPublishActivityTypeEnum.PostDeletionFailedToQueue)
                                    .SetJobID(res.Entity.ID.Value)
                                    .SetPostID(postID)
                                    .SetDescription(string.Join(", ", res.Problems.ToArray()))
                            )
                            .Save(ProviderFactory.Logging)
                        ;
                    }
                    else
                    {
                        post.DeletionQueuedDate = DateTime.Now;
                        db.SaveChanges();
                        Auditor
                            .New(AuditLevelEnum.Information, AuditTypeEnum.PublishActivity, userInfo, "")
                            .SetAuditDataObject(
                                AuditPublishActivity
                                    .New(postID, AuditPublishActivityTypeEnum.PostDeletionQueued)
                                    .SetJobID(res.Entity.ID.Value)
                                    .SetPostID(postID)
                            )
                            .Save(ProviderFactory.Logging)
                        ;
                    }
                }
            }
        }

        public bool MarkPostTargetDeleted(UserInfoDTO userInfo, long PostTargetID)
        {
            bool result = false;
            using (MainEntities db = ContextFactory.Main)
            {
                PostTarget PostTarget = db.PostTargets.Where(p => p.ID == PostTargetID).FirstOrDefault();

                if (PostTarget != null)
                {
                    PostTarget.Deleted = true;

                    db.SaveChanges();

                    Auditor
                      .New(AuditLevelEnum.Information, AuditTypeEnum.UserActivity, userInfo, "")
                      .SetAuditDataObject(
                          AuditUserActivity
                          .New(userInfo, AuditUserActivityTypeEnum.PostTargetDeleted)
                           .SetPostTargetID(PostTargetID)
                           .SetDescription("Post Target Mark as Deleted")
                      )
                      .Save(ProviderFactory.Logging)
                  ;

                    result = true;
                }
            }
            return result;
        }

        public bool DeletePostImageResult(UserInfoDTO userInfo, long PostImageResultID)
        {
            bool result = false;
            using (MainEntities db = ContextFactory.Main)
            {
                PostImageResult postImageResult = db.PostImageResults.Where(p => p.ID == PostImageResultID).FirstOrDefault();

                if (postImageResult != null)
                {
                    postImageResult.Deleted = true;

                    db.SaveChanges();

                    Auditor
                      .New(AuditLevelEnum.Information, AuditTypeEnum.UserActivity, userInfo, "")
                      .SetAuditDataObject(
                          AuditUserActivity
                          .New(userInfo, AuditUserActivityTypeEnum.PostImageResultDeleted)
                          .SetPostImageResultID(PostImageResultID)
                           .SetDescription("Post Image Result Mark as Deleted")
                      )
                      .Save(ProviderFactory.Logging)
                  ;

                    result = true;
                }
            }

            return result;
        }

        public bool UpdateCredential(long credentialID, string screenName, string URL, string PictureURL = "")
        {
            bool Success = false;
            using (MainEntities db = ContextFactory.Main)
            {
                Credential credential = db.Credentials.Where(c => c.ID == credentialID).FirstOrDefault();

                if (credential != null)
                {
                    credential.ScreenName = screenName;
                    credential.URI = URL;

                    if (!string.IsNullOrWhiteSpace(PictureURL))
                        credential.PictureURL = PictureURL;

                    db.SaveChanges();

                    Success = true;
                }
            }

            return Success;
        }

        public PostImageDTO GetPostImageInfo(long postID)
        {
            using (MainEntities db = ContextFactory.Main)
            {
                return (from pi in db.PostImages
                        where pi.PostID == postID
                        select new PostImageDTO
                        {
                            ID = pi.ID,
                            URL = pi.URL
                        }).FirstOrDefault();
            }
        }

        public List<PublishModeDTO> GetPublishModes(UserInfoDTO userInfo)
        {
            List<PublishModeDTO> list = new List<PublishModeDTO>();

            bool userHasPublish = ProviderFactory.Security.HasPermission(userInfo.EffectiveUser.ID.Value, PermissionTypeEnum.social_publish);
            bool accountHasSocial = ProviderFactory.Security.HasFeature(userInfo.EffectiveUser.CurrentContextAccountID, FeatureTypeEnum.SocialCore);
            bool accountHasSyndicator = ProviderFactory.Security.HasFeature(userInfo.EffectiveUser.CurrentContextAccountID, FeatureTypeEnum.SyndicatorCore);

            if (userHasPublish)
            {
                if (accountHasSocial)
                {
                    list.Add(new PublishModeDTO() { Name = "Publish", SyndicatorID = null, ForceApprovals = false, AllowPostNow = true });
                }

                if (accountHasSyndicator)
                {
                    using (MainEntities db = ContextFactory.Main)
                    {
                        if (db.Connection.State != System.Data.ConnectionState.Open)
                            db.Connection.Open();

                        using (DbCommand cmd = db.CreateStoreCommand("spGetSyndicatorList", System.Data.CommandType.StoredProcedure))
                        {
                            using (DbDataReader reader = cmd.ExecuteReader())
                            {
                                List<SyndicatorInfo> silist = db.Translate<SyndicatorInfo>(reader).ToList();
                                foreach (SyndicatorInfo info in silist)
                                {
                                    if (ProviderFactory.Security.HasFeature(userInfo.EffectiveUser.CurrentContextAccountID, (FeatureTypeEnum)info.SyndicatorFeatureTypeID))
                                    {
                                        list.Add(new PublishModeDTO()
                                        {
                                            Name = info.Name,
                                            SyndicatorID = info.SyndicatorID,
                                            LogoURL = info.LogoURL,
                                            AllowPostNow = !info.AlwaysRequireApproval,
                                            ForceApprovals = info.AlwaysRequireApproval
                                        });
                                    }
                                }
                            }
                        }
                    }
                }
            }

            //

            return list;
        }

        public List<DashboardSocialLocationDTO> GetDashBoardSocialLocationData(UserInfoDTO userInfo, bool ShowConnected = false, bool ShowDisconnected = false, bool ShowNeverConnected = false)
        {
            List<DashboardSocialLocationDTO> items = new List<DashboardSocialLocationDTO>();

            ITimezoneProvider tz = ProviderFactory.TimeZones;
            TimeZoneInfo timeZone = tz.GetTimezoneForUserID(userInfo.EffectiveUser.ID.Value);

            List<long> accountIDs =
                AccountListBuilder
                    .New(new SecurityProvider())
                    .AddAccountID(userInfo.EffectiveUser.CurrentContextAccountID)
                    .ExpandToDescendants()
                    .FilterToAccountsWithPermission(userInfo, PermissionTypeEnum.accounts_view)
                    .GetIDs(SecurityProvider.PLATFORM_MAX_ACCOUNT_ID_LIST)
            ;
            DashboardOverviewDTO res = new DashboardOverviewDTO();

            using (MainEntities db = ContextFactory.Main)
            {
                if (db.Connection.State != System.Data.ConnectionState.Open)
                    db.Connection.Open();

                DbCommand cmd = null;
                List<SqlParameter> parameters = new List<SqlParameter>();

                using (cmd = db.CreateStoreCommand(
                   "spGetSocialLocationData",
                   System.Data.CommandType.StoredProcedure,
                    new SqlParameter("@AccountIDs", string.Join("|", accountIDs.ToArray())),
                    new SqlParameter("@ShowConnected", ShowConnected),
                    new SqlParameter("@ShowDisconnected", ShowDisconnected),
                    new SqlParameter("@ShowNeverConnected", ShowNeverConnected)
               ))
                {
                    using (DbDataReader reader = cmd.ExecuteReader())
                    {
                        //reader.NextResult();

                        if (reader.Read()) // Accounts Detail
                        {
                            do
                            {
                                DashboardSocialLocationDTO item = new DashboardSocialLocationDTO();

                                item.AccountID = Util.GetReaderValue<long>(reader, "AccountID", 0);
                                item.AccountName = Util.GetReaderValue<string>(reader, "AccountName", "");
                                item.StreetName = Util.GetReaderValue<string>(reader, "StreetName", "");
                                item.CityName = Util.GetReaderValue<string>(reader, "CityName", "");
                                item.StateAbbrev = Util.GetReaderValue<string>(reader, "StateAbbrev", "");
                                item.StateLong = Util.GetReaderValue<string>(reader, "StateLong", "");
                                item.ZipCode = Util.GetReaderValue<string>(reader, "ZipCode", "");

                                items.Add(item);
                            } while (reader.Read());


                            if (reader.NextResult()) // SocialNetworks Detail
                            {
                                while (reader.Read())
                                {
                                    DashboardSocialLocationSocialNetWorkDTO dashboardSocialLocationSocialNetWork = new DashboardSocialLocationSocialNetWorkDTO();

                                    dashboardSocialLocationSocialNetWork.CredentialID = Util.GetReaderValue<long>(reader, "CredentialID", 0);
                                    dashboardSocialLocationSocialNetWork.UniqueID = Util.GetReaderValue<string>(reader, "UniqueID", "");
                                    dashboardSocialLocationSocialNetWork.URI = Util.GetReaderValue<string>(reader, "URI", "");
                                    dashboardSocialLocationSocialNetWork.PictureURL = Util.GetReaderValue<string>(reader, "PictureURL", "");
                                    int SocialNetworkID = Util.GetReaderValue<int>(reader, "SocialNetworkID", 0);
                                    dashboardSocialLocationSocialNetWork.SocialNetwork = (SocialNetworkEnum)(SocialNetworkID);
                                    dashboardSocialLocationSocialNetWork.IsValid = Util.GetReaderValue<bool>(reader, "IsValid", false);
                                    dashboardSocialLocationSocialNetWork.IsActive = Util.GetReaderValue<bool>(reader, "IsActive", false);


                                    long AccountID = Util.GetReaderValue<long>(reader, "AccountID", 0);
                                    var dto = items.Where(x => x.AccountID == AccountID).FirstOrDefault();

                                    dto.DashboardSocialLocationSocialNetWork.Add(dashboardSocialLocationSocialNetWork);
                                }
                                if (reader.NextResult()) // Activity Detail
                                {
                                    while (reader.Read())
                                    {
                                        long AccountID = Util.GetReaderValue<long>(reader, "AccountID", 0);
                                        var dto = items.Where(x => x.AccountID == AccountID).FirstOrDefault();

                                        dto.FacebookPosts = Util.GetReaderValue<int>(reader, "FacebookPosts", 0);
                                        dto.FacebookPostComments = Util.GetReaderValue<int>(reader, "FacebookPostComments", 0);
                                        dto.FacebookPostLikes = Util.GetReaderValue<int>(reader, "FacebookPostLikes", 0);
                                        dto.FacebookShares = Util.GetReaderValue<int>(reader, "FacebookShares", 0);
                                        dto.GooglePlusPosts = Util.GetReaderValue<int>(reader, "GooglePlusPosts", 0);
                                        dto.GooglePlusComments = Util.GetReaderValue<int>(reader, "GooglePlusComments", 0);
                                        dto.GooglePlusPlusoners = Util.GetReaderValue<int>(reader, "GooglePlusPlusoners", 0);
                                        dto.GooglePlusResharers = Util.GetReaderValue<int>(reader, "GooglePlusResharers", 0);
                                        dto.TwitterPosts = Util.GetReaderValue<int>(reader, "TwitterPosts", 0);
                                        dto.TwitterPostRetweetCount = Util.GetReaderValue<int>(reader, "TwitterPostRetweetCount", 0);
                                        dto.LastPostDateTime = SIDateTime.CreateSIDateTime(Util.GetReaderValue<DateTime?>(reader, "LastPost", null), timeZone);
                                    }
                                }// Activity Detail
                            }// SocialNetworks Detail
                        }// Accounts Detail
                    }// reader using
                }//cmd using
            }//db using

            return items;
        }

        public bool SaveAutoApproval(UserInfoDTO userInfo, long syndicatorID, long AccountID, long SocialNetworkID)
        {
            bool result = false;

            using (MainEntities db = ContextFactory.Main)
            {
                var postAutoApproval = db.PostAutoApprovals.Where(p => p.AccountID == AccountID && p.ApprovingUserID == userInfo.EffectiveUser.ID.GetValueOrDefault() && p.SyndicatorID == syndicatorID && p.SocialNetworkID == SocialNetworkID).SingleOrDefault();

                if (postAutoApproval == null)
                {
                    postAutoApproval = new PostAutoApproval();
                    postAutoApproval.SyndicatorID = syndicatorID;
                    postAutoApproval.ApprovingUserID = userInfo.EffectiveUser.ID.GetValueOrDefault();
                    postAutoApproval.AccountID = AccountID;
                    postAutoApproval.SocialNetworkID = SocialNetworkID;
                    postAutoApproval.RejectionCount = 0;
                    postAutoApproval.DateCreated = DateTime.UtcNow;

                    db.PostAutoApprovals.AddObject(postAutoApproval);

                    db.SaveChanges();

                    Auditor
                       .New(AuditLevelEnum.Information, AuditTypeEnum.UserActivity, userInfo, "")
                       .SetAuditDataObject(
                           AuditUserActivity
                           .New(userInfo, AuditUserActivityTypeEnum.AddAccountSocialNetworkAutoApproval)
                           .SetAccountID(AccountID)
                           .SetSyndicatorID(syndicatorID)
                           .SetDescription("Added Auto Approval for SocialNetwork: " + ((SocialNetworkEnum)SocialNetworkID).ToString())
                       )
                       .Save(ProviderFactory.Logging)
                   ;

                    result = true;
                }
            }

            return result;

        }

        public bool RemoveAutoApproval(UserInfoDTO userInfo, long syndicatorID, long AccountID, long SocialNetworkID)
        {
            bool result = false;

            using (MainEntities db = ContextFactory.Main)
            {
                var postAutoApproval = db.PostAutoApprovals.Where(p => p.AccountID == AccountID && p.ApprovingUserID == userInfo.EffectiveUser.ID.GetValueOrDefault() && p.SyndicatorID == syndicatorID && p.SocialNetworkID == SocialNetworkID).SingleOrDefault();

                if (postAutoApproval != null)
                {
                    db.PostAutoApprovals.DeleteObject(postAutoApproval);

                    db.SaveChanges();

                    Auditor
                       .New(AuditLevelEnum.Information, AuditTypeEnum.UserActivity, userInfo, "")
                       .SetAuditDataObject(
                           AuditUserActivity
                           .New(userInfo, AuditUserActivityTypeEnum.RemoveAccountSocialNetworkAutoApproval)
                           .SetAccountID(AccountID)
                           .SetSyndicatorID(syndicatorID)
                           .SetDescription("Deleted Auto Approval for SocialNetwork: " + ((SocialNetworkEnum)SocialNetworkID).ToString())
                       )
                       .Save(ProviderFactory.Logging)
                   ;

                    result = true;
                }
            }

            return result;

        }

        public List<SocialDashboardDataFacebook> GetSocialDashboardData_facebook(UserInfoDTO userInfo, DateTime startDate, DateTime endDate)
        {
            List<long> accountIDs =
                AccountListBuilder
                    .New(new SecurityProvider())
                    .AddAccountID(userInfo.EffectiveUser.CurrentContextAccountID)
                    .ExpandToDescendants()
                    .FilterToAccountsWithPermission(userInfo, PermissionTypeEnum.accounts_view)
                    .GetIDs(500)
            ;

            using (PMDWEntities db = ContextFactory.Reporting)
            {
                return db.CreateStoreCommand("spGetSocialDashboardData_facebook", System.Data.CommandType.StoredProcedure,
                                             new SqlParameter("@StartDate", startDate),
                                             new SqlParameter("@EndDate", endDate),
                                             new SqlParameter("@AccountIDs", string.Join("|", accountIDs.ToArray())))
                         .Materialize<SocialDashboardDataFacebook>().ToList();
            }
        }

        public List<SocialFacebookPostData> GetSocialFacebookPostData(UserInfoDTO userInfo, DateTime startDate, DateTime endDate)
        {
            List<long> accountIDs =
                AccountListBuilder
                    .New(new SecurityProvider())
                    .AddAccountID(userInfo.EffectiveUser.CurrentContextAccountID)
                    .ExpandToDescendants()
                    .FilterToAccountsWithPermission(userInfo, PermissionTypeEnum.accounts_view)
                    .GetIDs(500)
            ;

            using (PMDWEntities db = ContextFactory.Reporting)
            {
                return db.CreateStoreCommand("spGetSocialPostData_Facebook", System.Data.CommandType.StoredProcedure,
                                             new SqlParameter("@StartDate", startDate),
                                             new SqlParameter("@EndDate", endDate),
                                             new SqlParameter("@AccountIDs", string.Join("|", accountIDs.ToArray())))
                         .Materialize<SocialFacebookPostData>().ToList();
            }

        }

        public List<SocialDashboardDataTwitter> GetSocialDashboardData_Twitter(UserInfoDTO userInfo, DateTime startDate, DateTime endDate)
        {
            List<long> accountIDs =
                AccountListBuilder
                    .New(new SecurityProvider())
                    .AddAccountID(userInfo.EffectiveUser.CurrentContextAccountID)
                    .ExpandToDescendants()
                    .FilterToAccountsWithPermission(userInfo, PermissionTypeEnum.accounts_view)
                    .GetIDs(500);

            using (PMDWEntities db = ContextFactory.Reporting)
            {
                return db.CreateStoreCommand("spGetSocialPageData_Twitter", System.Data.CommandType.StoredProcedure,
                                             new SqlParameter("@StartDate", startDate),
                                             new SqlParameter("@EndDate", endDate),
                                             new SqlParameter("@AccountIDs", string.Join("|", accountIDs.ToArray())))
                         .Materialize<SocialDashboardDataTwitter>().ToList();
            }
        }

        public List<SocialTwitterPostData> GetSocialTwitterPostData(UserInfoDTO userInfo, DateTime startDate, DateTime endDate)
        {
            List<long> accountIDs =
                AccountListBuilder
                    .New(new SecurityProvider())
                    .AddAccountID(userInfo.EffectiveUser.CurrentContextAccountID)
                    .ExpandToDescendants()
                    .FilterToAccountsWithPermission(userInfo, PermissionTypeEnum.accounts_view)
                    .GetIDs(500)
            ;

            using (PMDWEntities db = ContextFactory.Reporting)
            {
                return db.CreateStoreCommand("spGetSocialPostData_Twitter", System.Data.CommandType.StoredProcedure,
                                             new SqlParameter("@StartDate", startDate),
                                             new SqlParameter("@EndDate", endDate),
                                             new SqlParameter("@AccountIDs", string.Join("|", accountIDs.ToArray())))
                         .Materialize<SocialTwitterPostData>().ToList();
            }
        }

        public bool SetSyndicationUnsubscribe(SyndicationUnsubscribeDTO accessTokenBaseDto)
        {
            bool result = false;
            using (MainEntities db = ContextFactory.Main)
            {
                var EmailExclusion = (from a in db.EmailExclusions
                                      where a.UserID == accessTokenBaseDto.UserID
                                              && a.NotificationMessageTypeID == accessTokenBaseDto.NotificationMessageTypeID
                                              && a.NotificationTemplateID == accessTokenBaseDto.NotificationTemplateID
                                      select a).FirstOrDefault();

                if (EmailExclusion == null)
                {
                    EmailExclusion emailExclusion = new EmailExclusion();

                    emailExclusion.UserID = accessTokenBaseDto.UserID;
                    emailExclusion.NotificationMessageTypeID = accessTokenBaseDto.NotificationMessageTypeID;
                    emailExclusion.NotificationTemplateID = accessTokenBaseDto.NotificationTemplateID;
                    emailExclusion.CreatedDate = DateTime.UtcNow;

                    db.EmailExclusions.AddObject(emailExclusion);
                    db.SaveChanges();

                    db.spDeleteNotificationSubscriptionsForUser(accessTokenBaseDto.UserID, accessTokenBaseDto.NotificationTemplateID, accessTokenBaseDto.NotificationMessageTypeID);

                    Auditor
                        .New(AuditLevelEnum.Information, AuditTypeEnum.UserActivity, new UserInfoDTO(accessTokenBaseDto.UserID, null), "SetSyndicationUnsubscribe")
                        .SetAuditDataObject(
                            AuditUserActivity
                                .New(new UserInfoDTO(accessTokenBaseDto.UserID, null), AuditUserActivityTypeEnum.SyndicationUnsubscribe)
                                .SetDescription("User has Unsubscribe from Syndication Notification Email.")
                                .SetUserIDActedUpon(accessTokenBaseDto.UserID)
                        ).Save(ProviderFactory.Logging);

                    result = true;
                }
            }

            return result;
        }

        public List<FBInsightDateDTO> GetDatesforFaceBookInsightstoBackFill()
        {
            List<FBInsightDateDTO> list = new List<FBInsightDateDTO>();


            List<long> accountIDs = new List<long>();

            DateTime startDate = DateTime.Now.AddDays(-9);
            DateTime endDate = DateTime.Now.AddDays(-2);

            using (MainEntities db = ContextFactory.Main)
            {
                return db.CreateStoreCommand("spGetDatesforFaceBookInsightstoBackFill", System.Data.CommandType.StoredProcedure,
                                             new SqlParameter("@AccountIDs", string.Join("|", accountIDs.ToArray())),
                                             new SqlParameter("@StartDate", startDate),
                                             new SqlParameter("@EndDate", endDate))
                         .Materialize<FBInsightDateDTO>().ToList();
            }
        }


        //public PostApprovalDTO GetPostApprovals(UserInfoDTO userInfo, long VirtualGroupID, long SocialNetworkID, DateTime StartDate, DateTime EndDate, int SortBy, bool SortAscending, int StartIndex, int  EndIndex, string Status = "all")

        public PostApprovalResultsDTO GetPostApprovals(UserInfoDTO userInfo, PostApprovalHistoryRequestDTO req)
        {

            List<PostApprovalResult> listPostApprovalResult = new List<PostApprovalResult>();
            TimeZoneInfo timeZone = ProviderFactory.TimeZones.GetTimezoneForUserID(userInfo.EffectiveUser.ID.Value);

            AccountListBuilder builder = AccountListBuilder
                .New((SecurityProvider)ProviderFactory.Security)
                .AddAccountID(userInfo.EffectiveUser.CurrentContextAccountID)
                .ExpandToDescendants()
            ;
            if (req.VirtualGroupID > 0)
            {
                builder.FilterToAccountsFromVirtualGroup(req.VirtualGroupID);
            }
            List<long> aids = builder.GetIDs(SecurityProvider.PLATFORM_MAX_ACCOUNT_ID_LIST);

            PostApprovalResultsDTO results = new PostApprovalResultsDTO()
            {
                StartIndex = req.StartIndex,
                EndIndex = req.EndIndex,
                SortAscending = req.SortAscending,
                SortBy = req.SortBy
            };

            using (MainEntities db = ContextFactory.Main)
            {
                if (db.Connection.State != System.Data.ConnectionState.Open)
                    db.Connection.Open();

                string socialNetworkIDs = "";
                if (req.SocialNetworkID > 0)
                {
                    socialNetworkIDs = req.SocialNetworkID.ToString();
                }

                using (DbCommand cmd3 = db.CreateStoreCommand(
                      "spGetPostApprovals",
                      System.Data.CommandType.StoredProcedure,
                        new SqlParameter("@AccountIDs", string.Join("|", aids.ToArray())),
                    //new SqlParameter("@VirtualGroupID", req.VirtualGroupID), -- account list builder handles this
                        new SqlParameter("@SocialNetworkIDs", socialNetworkIDs),
                        new SqlParameter("@StartDate", req.StartDate.Date),
                        new SqlParameter("@EndDate", req.EndDate.Date),
                        new SqlParameter("@ApprovalStatus", (long)req.Status),
                        new SqlParameter("@SortBy", req.SortBy),
                        new SqlParameter("@SortAscending", req.SortAscending),
                        new SqlParameter("@StartIndex", req.StartIndex),
                        new SqlParameter("@EndIndex", req.EndIndex)
                      ))
                {
                    using (DbDataReader reader = cmd3.ExecuteReader())
                    {
                        if (reader.Read())
                        {
                            //the total records
                            results.TotalRecords = reader.GetInt32(0);

                            if (reader.NextResult())
                            {
                                while (reader.Read())
                                {

                                    listPostApprovalResult.Add(new PostApprovalResult()
                                    {
                                        PostTargetID = Util.GetReaderValue<long>(reader, "PostTargetID", 0),
                                        ApprovalStatus = (ApprovalStatusEnum)Util.GetReaderValue<int>(reader, "ApprovalStatus", 0),

                                        SocialNetworkName = Util.GetReaderValue<string>(reader, "SocialNetworkName", ""),
                                        SocialNetwork = (SocialNetworkEnum)Util.GetReaderValue<long>(reader, "SocialNetworkID", 0),

                                        AccountID = Util.GetReaderValue<long>(reader, "AccountID", 0),
                                        AccountName = Util.GetReaderValue<string>(reader, "AccountName", ""),
                                        ScreenName = Util.GetReaderValue<string>(reader, "ScreenName", ""),
                                        SocialNetworkPictureURL = Util.GetReaderValue<string>(reader, "SocialNetworkPictureURL", ""),
                                        SocialNetworkUniqueID = Util.GetReaderValue<string>(reader, "SocialNetworkUniqueID", ""),
                                        SocialNetworkURL = Util.GetReaderValue<string>(reader, "SocialNetworkURL", ""),

                                        CredentialIsActive = Util.GetReaderValue<bool>(reader, "CredentialIsActive", false),
                                        CredentialIsValid = Util.GetReaderValue<bool>(reader, "CredentialIsValid", false),

                                        SyndicatorID = Util.GetReaderValue<long?>(reader, "SyndicatorID", null),
                                        SyndicatorName = Util.GetReaderValue<string>(reader, "SyndicatorName", ""),

                                        PostID = Util.GetReaderValue<long>(reader, "PostID", 0),
                                        PostTypeID = Util.GetReaderValue<long>(reader, "PostTypeID", 0),

                                        Message = Util.GetReaderValue<string>(reader, "Message", ""),
                                        Link = Util.GetReaderValue<string>(reader, "Link", ""),
                                        LinkTitle = Util.GetReaderValue<string>(reader, "LinkTitle", ""),
                                        LinkDescription = Util.GetReaderValue<string>(reader, "LinkDescription", ""),
                                        LinkImageURL = Util.GetReaderValue<string>(reader, "LinkImageURL", ""),

                                        ApprovedByUserID = Util.GetReaderValue<long?>(reader, "ApprovedByUserID", null),
                                        ApprovedByUserName = Util.GetReaderValue<string>(reader, "ApprovedByUserName", ""),
                                        ApprovedByUserEmail = Util.GetReaderValue<string>(reader, "ApprovedByUserEmail", ""),

                                        TargetResultID = Util.GetReaderValue<string>(reader, "TargetResultID", ""),

                                        ImagesAttempted = Util.GetReaderValue<int>(reader, "ImagesAttempted", 0),
                                        ImagesDeleted = Util.GetReaderValue<int>(reader, "ImagesDeleted", 0),
                                        ImagesFailed = Util.GetReaderValue<int>(reader, "ImagesFailed", 0),
                                        ImagesSucceeeded = Util.GetReaderValue<int>(reader, "ImagesSucceeded", 0),

                                        PostTargetDeleted = Util.GetReaderValue<bool>(reader, "PostTargetDeleted", false),
                                        PostApprovalID = Util.GetReaderValue<long>(reader, "PostApprovalID", 0),
                                        AutoApproved = Util.GetReaderValue<bool>(reader, "AutoApproved", false),

                                        Title = Util.GetReaderValue<string>(reader, "Title", ""),

                                        PostedByUserName = Util.GetReaderValue<string>(reader, "PostedByUserName", ""),
                                        PostedByUserID = Util.GetReaderValue<long>(reader, "PostedByUserID", 0),


                                        ScheduleDate = SIDateTime.CreateSIDateTime(Util.GetReaderValue<DateTime?>(reader, "ScheduleDate", null), timeZone),
                                        PickedUpDate = SIDateTime.CreateSIDateTime(Util.GetReaderValue<DateTime?>(reader, "PickedUpDate", null), timeZone),
                                        FailedDate = SIDateTime.CreateSIDateTime(Util.GetReaderValue<DateTime?>(reader, "FailedDate", null), timeZone),
                                        DateApproved = SIDateTime.CreateSIDateTime(Util.GetReaderValue<DateTime?>(reader, "DateApproved", null), timeZone),
                                        DateRejected = SIDateTime.CreateSIDateTime(Util.GetReaderValue<DateTime?>(reader, "DateRejected", null), timeZone),
                                        PostCreationDate = SIDateTime.CreateSIDateTime(Util.GetReaderValue<DateTime?>(reader, "PostCreationDate", null), timeZone),
                                        PublishedDate = SIDateTime.CreateSIDateTime(Util.GetReaderValue<DateTime?>(reader, "PublishedDate", null), timeZone),
                                        NoApproverHoldDate = SIDateTime.CreateSIDateTime(Util.GetReaderValue<DateTime?>(reader, "NoApproverHoldDate", null), timeZone)

                                    });
                                }
                            }
                        }
                        results.Items = listPostApprovalResult;

                        //listPostApprovalDTO = db.Translate<PostApprovalDTO>(reader).ToList();

                        if (listPostApprovalResult != null)
                        {
                            List<long> PostIDs = listPostApprovalResult.Select(x => x.PostID).Distinct().ToList();
                            List<PostSyndicationOptionsInfo> listPostSyndicationOptionsInfo = GetPostSyndicationOptionsByPostIDs(PostIDs);


                            foreach (var postApprovalResult in listPostApprovalResult)
                            {
                                PostSyndicationOptionsInfo postSyndicationOptionsInfo = listPostSyndicationOptionsInfo.Where(x => x.PostID == postApprovalResult.PostID).FirstOrDefault();

                                #region Allow Edit
                                bool AllowEdit = false;

                                if (postApprovalResult.SyndicatorID != null)
                                {
                                    #region Syndicated Posts
                                    if (postApprovalResult.ScheduleDate.LocalDate > DateTime.Now)
                                    {
                                        if (postSyndicationOptionsInfo.AllowChangeImage ||
                                                postSyndicationOptionsInfo.AllowChangeMessage ||
                                                postSyndicationOptionsInfo.AllowChangeImage ||
                                                postSyndicationOptionsInfo.AllowChangeTargets ||
                                                postSyndicationOptionsInfo.AllowChangeLinkURL ||
                                                postSyndicationOptionsInfo.AllowChangeLinkThumb ||
                                                postSyndicationOptionsInfo.AllowChangeLinkText)
                                        {
                                            if (string.IsNullOrEmpty(postApprovalResult.TargetResultID) && postApprovalResult.PickedUpDate == null && (postApprovalResult.PostTargetDeleted == null || postApprovalResult.PostTargetDeleted == false) &&
                                                    postApprovalResult.DateApproved == null && postApprovalResult.DateRejected == null)
                                            {
                                                AllowEdit = true;
                                            }
                                            else
                                            {
                                                AllowEdit = false;
                                            }
                                        }
                                        else
                                        {
                                            AllowEdit = false;
                                        }
                                    }
                                    else
                                    {
                                        AllowEdit = false;
                                    }

                                    #endregion
                                }
                                else
                                {
                                    #region Distributed Posts

                                    if (string.IsNullOrEmpty(postApprovalResult.TargetResultID) && postApprovalResult.PickedUpDate == null && (postApprovalResult.PostTargetDeleted == null || postApprovalResult.PostTargetDeleted == false) &&
                                                   postApprovalResult.DateApproved == null && postApprovalResult.DateRejected == null)
                                    {
                                        AllowEdit = true; 
                                    }
                                    else
                                    {
                                        AllowEdit = false;
                                    }

                                    #endregion
                                }
                                #endregion

                                postApprovalResult.AllowEdit = AllowEdit;

                                #region Allow Deny

                                bool AllowDeny = false;

                                if (postApprovalResult.SyndicatorID != null)
                                {
                                    #region Syndicated Posts
                                    if (postApprovalResult.ScheduleDate.LocalDate > DateTime.UtcNow)
                                    {
                                        if (string.IsNullOrEmpty(postApprovalResult.TargetResultID) && postApprovalResult.PickedUpDate == null && (postApprovalResult.PostTargetDeleted == null || postApprovalResult.PostTargetDeleted == false) &&
                                                postApprovalResult.DateApproved == null && postApprovalResult.DateRejected == null)
                                        {
                                            AllowDeny = true;
                                        }
                                        else
                                        {
                                            AllowDeny = false;
                                        }
                                    }
                                    else
                                    {
                                        AllowDeny = false;
                                    }

                                    #endregion
                                }
                                else
                                {
                                    #region Distributed Posts

                                    if (string.IsNullOrEmpty(postApprovalResult.TargetResultID) && postApprovalResult.PickedUpDate == null && (postApprovalResult.PostTargetDeleted == null || postApprovalResult.PostTargetDeleted == false) &&
                                                   postApprovalResult.DateApproved == null && postApprovalResult.DateRejected == null)
                                    {
                                        AllowDeny = true;
                                    }
                                    else
                                    {
                                        AllowDeny = false;
                                    }

                                    #endregion
                                }

                                #endregion

                                postApprovalResult.AllowDeny = AllowDeny;

                                #region Allow Approve

                                bool AllowApprove = false;

                                if (postApprovalResult.SyndicatorID != null)
                                {
                                    #region Syndicated Posts
                                    if (postApprovalResult.ScheduleDate.LocalDate > DateTime.UtcNow)
                                    {
                                        if (string.IsNullOrEmpty(postApprovalResult.TargetResultID) && postApprovalResult.PickedUpDate == null && (postApprovalResult.PostTargetDeleted == null || postApprovalResult.PostTargetDeleted == false) &&
                                                postApprovalResult.DateApproved == null && postApprovalResult.DateRejected == null)
                                        {
                                            AllowApprove = true;
                                        }
                                        else
                                        {
                                            AllowApprove = false;
                                        }
                                    }
                                    else
                                    {
                                        AllowApprove = false;
                                    }

                                    #endregion
                                }
                                else
                                {
                                    #region Distributed Posts

                                    if (string.IsNullOrEmpty(postApprovalResult.TargetResultID) && postApprovalResult.PickedUpDate == null && (postApprovalResult.PostTargetDeleted == null || postApprovalResult.PostTargetDeleted == false) &&
                                                   postApprovalResult.DateApproved == null && postApprovalResult.DateRejected == null)
                                    {
                                        AllowApprove = true;
                                    }
                                    else
                                    {
                                        AllowApprove = false;
                                    }

                                    #endregion
                                }

                                #endregion

                                postApprovalResult.AllowApprove = AllowApprove;

                                #region Allow UnApprove

                                bool AllowUnApprove = false;

                                if (postApprovalResult.SyndicatorID != null)
                                {
                                    #region Syndicated Posts
                                    if (postApprovalResult.ScheduleDate.LocalDate > DateTime.UtcNow)
                                    {
                                        if (string.IsNullOrEmpty(postApprovalResult.TargetResultID) && postApprovalResult.PickedUpDate == null && (postApprovalResult.PostTargetDeleted == null || postApprovalResult.PostTargetDeleted == false) &&
                                                postApprovalResult.DateApproved != null && postApprovalResult.DateRejected == null)
                                        {
                                            AllowUnApprove = true;
                                            AllowDeny = true;
                                            postApprovalResult.AllowDeny = AllowDeny;
                                        }
                                        else
                                        {
                                            AllowUnApprove = false;
                                        }
                                    }
                                    else
                                    {
                                        AllowUnApprove = false;
                                    }

                                    #endregion
                                }
                                else
                                {
                                    #region Distributed Posts

                                    if (string.IsNullOrEmpty(postApprovalResult.TargetResultID) && postApprovalResult.PickedUpDate == null && (postApprovalResult.PostTargetDeleted == null || postApprovalResult.PostTargetDeleted == false) &&
                                                   postApprovalResult.DateApproved != null && postApprovalResult.DateRejected == null)
                                    {
                                        AllowUnApprove = true;
                                        AllowDeny = true;
                                        postApprovalResult.AllowDeny = AllowDeny;
                                    }
                                    else
                                    {
                                        AllowUnApprove = false;
                                    }

                                    #endregion
                                }

                                #endregion

                                postApprovalResult.AllowUnApprove = AllowUnApprove;

                                #region Allow UnDeny

                                bool AllowUnDeny = false;

                                if (postApprovalResult.SyndicatorID != null)
                                {
                                    #region Syndicated Posts
                                    if (postApprovalResult.ScheduleDate.LocalDate > DateTime.UtcNow)
                                    {
                                        if (string.IsNullOrEmpty(postApprovalResult.TargetResultID) && postApprovalResult.PickedUpDate == null && (postApprovalResult.PostTargetDeleted == null || postApprovalResult.PostTargetDeleted == false) &&
                                                postApprovalResult.DateApproved == null && postApprovalResult.DateRejected != null)
                                        {
                                            AllowUnDeny = true;
                                            AllowApprove = true;
                                            postApprovalResult.AllowApprove = AllowApprove;
                                        }
                                        else
                                        {
                                            AllowUnDeny = false;
                                        }
                                    }
                                    else
                                    {
                                        AllowUnDeny = false;
                                    }

                                    #endregion
                                }
                                else
                                {
                                    #region Distributed Posts

                                    if (string.IsNullOrEmpty(postApprovalResult.TargetResultID) && postApprovalResult.PickedUpDate == null && (postApprovalResult.PostTargetDeleted == null || postApprovalResult.PostTargetDeleted == false) &&
                                                   postApprovalResult.DateApproved == null && postApprovalResult.DateRejected != null)
                                    {
                                        AllowUnDeny = true;
                                        AllowApprove = true;
                                        postApprovalResult.AllowApprove = AllowApprove;
                                    }
                                    else
                                    {
                                        AllowUnDeny = false;
                                    }

                                    #endregion
                                }

                                #endregion

                                postApprovalResult.AllowUnDeny = AllowUnDeny;
                            }
                        }

                    }
                }

            }
            return results;
        }



        private List<PostSyndicationOptionsInfo> GetPostSyndicationOptionsByPostIDs(List<long> PostIDs)
        {
            List<PostSyndicationOptionsInfo> listPostSyndicationOptionsInfo = new List<PostSyndicationOptionsInfo>();

            using (MainEntities db = ContextFactory.Main)
            {
                if (db.Connection.State != System.Data.ConnectionState.Open)
                    db.Connection.Open();

                using (DbCommand cmd3 = db.CreateStoreCommand(
                      "spGetPostSyndicationOptionsByPostIDs",
                      System.Data.CommandType.StoredProcedure,
                          new SqlParameter("@PostIDs", string.Join("|", PostIDs.ToArray()))
                      ))
                {
                    using (DbDataReader reader = cmd3.ExecuteReader())
                    {
                        listPostSyndicationOptionsInfo = db.Translate<PostSyndicationOptionsInfo>(reader).ToList();

                    }
                }
            }

            return listPostSyndicationOptionsInfo;
        }

        private class PostSyndicationOptionsInfo
        {
            public long PostID { get; set; }
            public bool AllowChangePublishDate { get; set; }
            public bool AllowChangeMessage { get; set; }
            public bool AllowChangeImage { get; set; }
            public bool AllowChangeTargets { get; set; }
            public bool AllowChangeLinkURL { get; set; }
            public bool AllowChangeLinkThumb { get; set; }
            public bool AllowChangeLinkText { get; set; }
        }

        private class SyndicatorInfo
        {
            public long AccountID { get; set; }
            public long SyndicatorID { get; set; }
            public string Name { get; set; }
            public long SyndicatorFeatureTypeID { get; set; }
            public long SyndicateeFeatureTypeID { get; set; }
            public string LogoURL { get; set; }
            public bool AlwaysRequireApproval { get; set; }
        }

        private class LookupInfo
        {
            public int ID { get; set; }
            public string Name { get; set; }
        }


        public void ProcessFacebookHistoricData(long AccountID, string startDate, string endDate)
        {
            if (AccountID > 0)
            {
                //string startDate = "";
                //string endDate = "";
                //SetDateForFacebookInsights(ref startDate, ref endDate, socialCredentialsDTO.UniqueID, socialCredentialsDTO.Token);

                SocialCredentialDTO credential = GetSocialCredentials(SocialNetworkEnum.Facebook, AccountID, true).FirstOrDefault();
                if (credential != null)
                {
                    SIFacebookPageParameters pageParameters = new SIFacebookPageParameters();
                    pageParameters.UniqueID = credential.UniqueID;
                    pageParameters.Token = credential.Token;
                    pageParameters.StartDate = startDate;
                    pageParameters.EndDate = endDate;

                    SIFacebookPageConnectionParameters ConnectionParameters = new SIFacebookPageConnectionParameters(pageParameters);
                    FacebookPageConnection pageconnection = new FacebookPageConnection(ConnectionParameters);

                    FacebookInsights objFacebookInsights = pageconnection.GetFacebookInsights();

                    //FaceBookJsonResponse faceBookInsights = FacebookAPI.GetJsonFaceBookPageInsights(socialCredentialsDTO.UniqueID, socialCredentialsDTO.Token, startDate, endDate);

                    if (string.IsNullOrWhiteSpace(objFacebookInsights.facebookResponse.message))
                    {
                        string serializeValue = JsonConvert.SerializeObject(objFacebookInsights);

                        string prefix = "AWS.";

                        string accessKey = ProviderFactory.Reviews.GetProcessorSetting(prefix + "AWSAccessKey");
                        string secretKey = ProviderFactory.Reviews.GetProcessorSetting(prefix + "AWSSecretKey");
                        string serviceURL = ProviderFactory.Reviews.GetProcessorSetting(prefix + "DynamoDBServiceURL");
                        string table = ProviderFactory.Reviews.GetProcessorSetting(prefix + "DynamoDBSocialRawTable");

                        DynamoDB dynamoDB = new DynamoDB(accessKey, secretKey, serviceURL, table);

                        var dynoDbResult = dynamoDB.PutValue(serializeValue);

                        if (dynoDbResult.IsSuccess)
                        {
                            FacebookIntegratorData integratorData = new FacebookIntegratorData()
                                {
                                    FacebookEnum = FacebookProcessorActionEnum.FacebookPageInsightsIntegrator,
                                    Key = dynoDbResult.Key,
                                    CredentialID = credential.ID,
                                    Since = startDate,
                                    Until = endDate
                                };

                            JobDTO jobIntegrator = new JobDTO()
                                {
                                    JobDataObject = integratorData,
                                    JobType = integratorData.JobType,
                                    DateScheduled = new SIDateTime(DateTime.UtcNow, TimeZoneInfo.Utc),
                                    JobStatus = JobStatusEnum.Queued,
                                    Priority = 50
                                };

                            SaveEntityDTO<JobDTO> saveInfo = ProviderFactory.Jobs.Save(new SaveEntityDTO<JobDTO>(jobIntegrator, UserInfoDTO.System));
                            //Console.WriteLine(saveInfo.Entity.ID);
                        }
                        else
                        {

                        }
                    }
                    else
                    {
                        //Log Facebook Error
                    }
                }
            }
        }


        public string GetPageAccessTokenByUserAccessToken(string UniqueID, string Token)
        {
            string AccessToken = string.Empty;

            if (!string.IsNullOrEmpty(Token) && !string.IsNullOrEmpty(UniqueID))
            {
                Facebook.Entities.FacebookAccount accounts = Facebook.FacebookAPI.GetFacebookAccounts(Token);

                if (accounts != null)
                {
                    Facebook.Entities.Account account = accounts.data.Where(x => x.id == UniqueID).FirstOrDefault();
                    if (account != null)
                    {
                        AccessToken = account.access_token;
                    }
                }
            }

            return AccessToken;
        }
        #region processing portion of post and target deletion

        public void ProcessPostDeletion(UserInfoDTO userInfo, long postID, long jobID)
        {
            Auditor
                .New(AuditLevelEnum.Information, AuditTypeEnum.PublishActivity, userInfo, "")
                .SetAuditDataObject(
                    AuditPublishActivity
                        .New(postID, AuditPublishActivityTypeEnum.PostDeletionProcessing)
                        .SetJobID(jobID)
                        .SetPostID(postID)
                )
                .Save(ProviderFactory.Logging)
            ;
            using (MainEntities db = ContextFactory.Main)
            {
                // get a list of targets for the post and queue the deletion jobs
                List<PostTarget> targets = (from pt in db.PostTargets where pt.PostID == postID select pt).ToList();

                foreach (PostTarget target in targets)
                {
                    try
                    {

                        // get a list of each post image result for this target
                        List<PostImageResult> pirs = (from r in db.PostImageResults where r.PostTargetID == target.ID select r).ToList();

                        foreach (PostImageResult pir in pirs)
                        {

                            DeletePostTargetData data = new DeletePostTargetData()
                            {
                                PostTargetID = target.ID,
                                PostImageResultID = pir.ID
                            };
                            SaveEntityDTO<JobDTO> res = ProviderFactory.Jobs.Save(new SaveEntityDTO<JobDTO>(
                                new JobDTO()
                                {
                                    JobDataObject = data,
                                    JobType = data.JobType,
                                    DateScheduled = SIDateTime.Now.Add(new TimeSpan(0, 0, 20)),
                                    JobStatus = JobStatusEnum.Queued,
                                    Priority = 2000,
                                    OriginJobID = jobID
                                }, userInfo)
                            );
                            if (res.Problems.Any())
                            {
                                // do something!
                            }
                            else
                            {
                                Auditor
                                    .New(AuditLevelEnum.Information, AuditTypeEnum.PublishActivity, userInfo, "")
                                    .SetAuditDataObject(
                                        AuditPublishActivity
                                            .New(postID, AuditPublishActivityTypeEnum.PostTargetDeletionQueued)
                                            .SetJobID(res.Entity.ID.Value)
                                            .SetPostID(postID)
                                            .SetPostImageID(pir.PostImageID)
                                            .SetPostTargetID(pir.PostTargetID)
                                            .SetDescription("Queued post image deletion for PostImageResultID [{0}]", pir.ID)
                                    )
                                    .Save(ProviderFactory.Logging)
                                ;

                                PostTargetDeletion del = new PostTargetDeletion()
                                {
                                    DateCreated = DateTime.Now,
                                    PostTargetID = target.ID,
                                    PostImageResultID = pir.ID,
                                    JobID = res.Entity.ID.Value
                                };
                                db.PostTargetDeletions.AddObject(del);


                                pir.Deleted = true;
                                db.SaveChanges();
                            }
                        }

                        // now the target itself
                        DeletePostTargetData data2 = new DeletePostTargetData()
                           {
                               PostTargetID = target.ID,
                               PostImageResultID = null
                           };
                        SaveEntityDTO<JobDTO> res2 = ProviderFactory.Jobs.Save(new SaveEntityDTO<JobDTO>(
                            new JobDTO()
                            {
                                JobDataObject = data2,
                                JobType = data2.JobType,
                                DateScheduled = SIDateTime.Now.Add(new TimeSpan(0, 0, 20)),
                                JobStatus = JobStatusEnum.Queued,
                                Priority = 2000,
                                OriginJobID = jobID
                            }, userInfo)
                        );
                        if (res2.Problems.Any())
                        {
                            // do something!
                        }
                        else
                        {
                            Auditor
                                .New(AuditLevelEnum.Information, AuditTypeEnum.PublishActivity, userInfo, "")
                                .SetAuditDataObject(
                                    AuditPublishActivity
                                        .New(postID, AuditPublishActivityTypeEnum.PostTargetDeletionQueued)
                                        .SetJobID(res2.Entity.ID.Value)
                                        .SetPostID(postID)
                                        .SetPostTargetID(target.ID)
                                        .SetDescription("Queued post target deletion for PostTargetID [{0}]", target.ID)
                                )
                                .Save(ProviderFactory.Logging)
                            ;

                            PostTargetDeletion del = new PostTargetDeletion()
                            {
                                DateCreated = DateTime.Now,
                                PostTargetID = target.ID,
                                PostImageResultID = null,
                                JobID = res2.Entity.ID.Value
                            };
                            db.PostTargetDeletions.AddObject(del);

                            target.Deleted = true;
                            db.SaveChanges();
                        }
                    }
                    catch (Exception ex)
                    {
                        ProviderFactory.Logging.LogException(ex);
                    }
                }


            }
        }

        public void ProcessPostTargetDeletion(UserInfoDTO userInfo, long postTargetID, long? postImageResultID, long jobID)
        {
            using (MainEntities db = ContextFactory.Main)
            {
                // get the post id
                var pInfo = (
                    from pt in db.PostTargets
                    join c in db.Credentials on pt.CredentialID equals c.ID
                    join p in db.Posts on pt.PostID equals p.ID
                    join sa in db.SocialApps on c.SocialAppID equals sa.ID
                    where pt.ID == postTargetID
                    select new
                    {
                        PostTargetID = pt.ID,
                        PostID = pt.PostID,
                        AccountID = c.AccountID,
                        CredentialID = c.ID,
                        PostTypeID = p.PostTypeID,
                        SocialNetworkID = sa.SocialNetworkID,
                        ResultID = pt.ResultID,
                        FeedID = pt.FeedID
                    }
                ).FirstOrDefault();


                var piInfo = (
                    from pir in db.PostImageResults
                    where postImageResultID.HasValue
                    && pir.ID == postImageResultID.Value
                    select pir
                ).FirstOrDefault();

                Auditor
                    .New(AuditLevelEnum.Information, AuditTypeEnum.PublishActivity, userInfo, "")
                    .SetAuditDataObject(
                        AuditPublishActivity
                            .New(pInfo.PostID, AuditPublishActivityTypeEnum.PostTargetDeletionProcessing)
                            .SetJobID(jobID)
                            .SetPostID(pInfo.PostID)
                            .SetPostTargetID(postTargetID)
                            .SetPostImageID(piInfo != null ? (long?)piInfo.PostImageID : null)
                            .SetDescription("Started processing deletion for PostTargetID [{0}], PostImageResultID [{1}]", postTargetID, postImageResultID.GetValueOrDefault())
                    )
                    .Save(ProviderFactory.Logging)
                ;

                // find the corresponding posttargetdeletion record
                PostTargetDeletion ptd = (
                    from d in db.PostTargetDeletions
                    where d.PostTargetID == postTargetID
                    && (
                        (postImageResultID.HasValue && d.PostImageResultID.HasValue && d.PostImageResultID.Value == postImageResultID.Value)
                        || (!postImageResultID.HasValue && !d.PostImageResultID.HasValue)
                        )
                    select d
                ).FirstOrDefault();

                if (ptd == null)
                {
                    Auditor
                        .New(AuditLevelEnum.Warning, AuditTypeEnum.PublishActivity, userInfo, "")
                        .SetAuditDataObject(
                            AuditPublishActivity
                                .New(pInfo.PostID, AuditPublishActivityTypeEnum.CouldNotMatchPostTargetDeletionRecord)
                                .SetDescription("Could not find deletion record for PostTargetID [{0}], PostImageResultID [{1}]", postTargetID, postImageResultID.GetValueOrDefault())
                                .SetJobID(jobID)
                                .SetPostTargetID(postTargetID)
                                .SetPostImageID(piInfo != null ? (long?)piInfo.PostImageID : null)
                                .SetAccountID(pInfo.AccountID)
                                .SetPostID(pInfo.PostID)
                        )
                        .Save(ProviderFactory.Logging)
                    ;

                    return;
                }
                else
                {
                    Auditor
                        .New(AuditLevelEnum.Information, AuditTypeEnum.PublishActivity, userInfo, "")
                        .SetAuditDataObject(
                            AuditPublishActivity
                                .New(pInfo.PostID, AuditPublishActivityTypeEnum.MatchedPostTargetDeletionRecord)
                                .SetDescription("Matched deletion record [{2}] for PostTargetID [{0}], PostImageResultID [{1}]", postTargetID, postImageResultID.GetValueOrDefault(), ptd.ID)
                                .SetJobID(jobID)
                                .SetPostTargetID(postTargetID)
                                .SetPostImageID(piInfo != null ? (long?)piInfo.PostImageID : null)
                                .SetAccountID(pInfo.AccountID)
                                .SetPostID(pInfo.PostID)
                        )
                        .Save(ProviderFactory.Logging)
                    ;

                    ptd.DatePickedUp = DateTime.UtcNow;
                    db.SaveChanges();
                }

                // mark the post target deleted
                PostTarget pt2 = (from x in db.PostTargets where x.ID == postTargetID select x).First();
                pt2.Deleted = true;
                db.SaveChanges();

                // delete the post
                string message = "";
                bool result = true;
                if (1 == 1)
                {
                    switch ((SocialNetworkEnum)pInfo.SocialNetworkID)
                    {
                        case SocialNetworkEnum.Facebook:

                            if (piInfo != null)
                            {
                                // image
                                if (piInfo != null && !string.IsNullOrWhiteSpace(piInfo.ResultID))
                                    result = deleteFacebookPost(pInfo.CredentialID, piInfo.ResultID, ref message);
                            }
                            else
                            {
                                // other                                
                                if (pInfo != null && !string.IsNullOrWhiteSpace(pInfo.ResultID))
                                    result = deleteFacebookPost(pInfo.CredentialID, pInfo.ResultID, ref message);
                                else
                                    result = true;
                            }

                            break;
                        case SocialNetworkEnum.Google:
                            if (piInfo != null)
                            {
                                // image
                                if (piInfo != null && !string.IsNullOrWhiteSpace(piInfo.ResultID))
                                    result = deleteGooglePlusActivity(pInfo.CredentialID, piInfo.FeedID, ref message);
                            }
                            else
                            {
                                // other                       
                                if (pInfo != null && !string.IsNullOrWhiteSpace(pInfo.ResultID))
                                    result = deleteGooglePlusActivity(pInfo.CredentialID, pInfo.FeedID, ref message);
                                else
                                    result = true;
                            }

                            break;
                        case SocialNetworkEnum.Twitter:
                            if (piInfo != null)
                            {
                                // image
                                if (piInfo != null && !string.IsNullOrWhiteSpace(piInfo.ResultID))
                                    result = deleteTweet(userInfo, pInfo.CredentialID, piInfo.ResultID, ref message);
                            }
                            else
                            {
                                // other
                                if (pInfo != null && !string.IsNullOrWhiteSpace(pInfo.ResultID))
                                    result = deleteTweet(userInfo, pInfo.CredentialID, pInfo.ResultID, ref message);
                                else
                                    result = true;
                            }
                            break;

                        default:
                            break;
                    }

                    // log the result
                    if (result)
                    {
                        Auditor
                            .New(AuditLevelEnum.Information, AuditTypeEnum.PublishActivity, userInfo, "")
                            .SetAuditDataObject(
                                AuditPublishActivity
                                    .New(pInfo.PostID, AuditPublishActivityTypeEnum.DeletionSucceeded)
                                    .SetDescription("Delete succeeded for PostTargetID [{0}], PostImageResultID [{1}]", postTargetID, postImageResultID.GetValueOrDefault())
                                    .SetJobID(jobID)
                                    .SetPostTargetID(postTargetID)
                                    .SetPostImageID(piInfo != null ? (long?)piInfo.PostImageID : null)
                                    .SetAccountID(pInfo.AccountID)
                                    .SetPostID(pInfo.PostID)
                            )
                            .Save(ProviderFactory.Logging)
                        ;

                        ptd.DateSucceeded = DateTime.Now;
                        db.SaveChanges();
                    }
                    else
                    {
                        Auditor
                            .New(AuditLevelEnum.Warning, AuditTypeEnum.PublishActivity, userInfo, "")
                            .SetAuditDataObject(
                                AuditPublishActivity
                                    .New(pInfo.PostID, AuditPublishActivityTypeEnum.DeletionFailed)
                                    .SetDescription("Delete failed for PostTargetID [{0}], PostImageResultID [{1}]", postTargetID, postImageResultID.GetValueOrDefault())
                                    .SetJobID(jobID)
                                    .SetPostTargetID(postTargetID)
                                    .SetPostImageID(piInfo != null ? (long?)piInfo.PostImageID : null)
                                    .SetAccountID(pInfo.AccountID)
                                    .SetPostID(pInfo.PostID)
                            )
                            .Save(ProviderFactory.Logging)
                        ;

                        ptd.DateFailed = DateTime.Now;
                        ptd.ResultText = message;
                        db.SaveChanges();
                    }
                }
                else
                {

                }
            }



        }


        private bool deleteFacebookPost(long credentialID, string resultID, ref string message)
        {
            SocialCredentialDTO socialCredentialsDTO = ProviderFactory.Social.GetSocialCredentialInfo(credentialID);

            if (socialCredentialsDTO != null && !string.IsNullOrEmpty(resultID))
            {
                SIFacebookStreamParameters streamParameters = new SIFacebookStreamParameters();

                streamParameters.Token = socialCredentialsDTO.Token;
                streamParameters.PostID = resultID;

                SIFacebookStreamConnectionParameters ConnectionParameters = new SIFacebookStreamConnectionParameters(streamParameters);
                FacebookStreamConnection streamconnection = new FacebookStreamConnection(ConnectionParameters);

                FacebookStreamAction objFacebookStreamAction = streamconnection.DeletePost();

                message = objFacebookStreamAction.FacebookResponse.message;

                if (message.ToLower() == "(#100) This post could not be loaded".ToLower())
                {
                    objFacebookStreamAction.IsSuccess = true;
                    message = string.Empty;
                }

                return objFacebookStreamAction.IsSuccess;
            }
            return false;
        }
        private bool deleteGooglePlusActivity(long credentialID, string activityID, ref string message)
        {
            bool result = false;

            SocialCredentialDTO socialCredentialsDTO = ProviderFactory.Social.GetSocialCredentialInfo(credentialID);

            if (socialCredentialsDTO != null && !string.IsNullOrEmpty(activityID))
            {
                string access_token = getGooglePlusAccessToken(socialCredentialsDTO);
                if (!string.IsNullOrEmpty(access_token))
                {
                    SIGooglePlusStreamParameters streamParameters = new SIGooglePlusStreamParameters();

                    streamParameters.accessToken = access_token;
                    streamParameters.UniqueID = socialCredentialsDTO.UniqueID;
                    streamParameters.activityID = activityID;

                    SIGooglePlusStreamConnectionParameters StreamConnectionParameters = new SIGooglePlusStreamConnectionParameters(streamParameters);
                    GooglePlusStreamConnection streamconnection = new GooglePlusStreamConnection(StreamConnectionParameters);

                    GooglePlusResponse PostResponse = streamconnection.DeleteGooglePlusActivity();

                    if (PostResponse != null)
                    {
                        if (PostResponse.error.code == 0)
                            result = true;
                        else
                            message = PostResponse.error.message;
                    }
                }
            }

            return result;
        }

        private bool deleteTweet(UserInfoDTO userInfo, long credentialID, string statusID, ref string message)
        {
            bool Success = false;

            SocialCredentialDTO socialCredentialsDTO = ProviderFactory.Social.GetSocialCredentialInfo(credentialID);

            if (socialCredentialsDTO != null)
            {
                SITwitterStreamParameters streamParameters = new SITwitterStreamParameters();

                streamParameters.oauth_token = socialCredentialsDTO.Token;
                streamParameters.oauth_token_secret = socialCredentialsDTO.TokenSecret;
                streamParameters.oauth_consumer_key = socialCredentialsDTO.AppID;
                streamParameters.oauth_consumer_secret = socialCredentialsDTO.AppSecret;
                streamParameters.statusID = statusID;

                SITwitterStreamConnectionParameters ConnectionParameters = new SITwitterStreamConnectionParameters(streamParameters);
                TwitterStreamConnection streamconnection = new TwitterStreamConnection(ConnectionParameters);

                TwitterRequestResult result = streamconnection.DeleteTweet();
                Success = result == TwitterRequestResult.Success;
                message = result.ToString();

                if (result == TwitterRequestResult.FileNotFound)
                {
                    Success = true;
                    message = string.Empty;
                }

            }
            return Success;
        }
        private string getGooglePlusAccessToken(SocialCredentialDTO socialCredentialsDTO)
        {
            //Call Connection Class to get data from SDK   
            SIGooglePlusPageParameters pageParameters = new SIGooglePlusPageParameters();

            pageParameters.UniqueID = socialCredentialsDTO.UniqueID;
            pageParameters.client_id = socialCredentialsDTO.AppID;
            pageParameters.client_secret = socialCredentialsDTO.AppSecret;
            pageParameters.refresh_token = socialCredentialsDTO.Token;
            pageParameters.grant_type = "refresh_token";

            // Get Access Token                                
            string access_token = string.Empty;

            SIGooglePlusPageConnectionParameters ConnectionParameters = new SIGooglePlusPageConnectionParameters(pageParameters);
            GooglePlusPageConnection pageconnection = new GooglePlusPageConnection(ConnectionParameters);

            GooglePlusAccessTokenRequestResponse objGooglePlusAccessTokenRequestResponse = pageconnection.GetGooglePlusAccessToken();

            access_token = objGooglePlusAccessTokenRequestResponse.access_token;

            return access_token;
        }

        #endregion


        public void TestQueuerForPostAndAccount(long postID, long accountID)
        {
            //using (MainEntities db = ContextFactory.Main)
            //{
            //    PublishJobListQueuer
            //        .New(accountID, postID, true)
            //        .LoadTargets(db, this)
            //        .QueueJobs(ProviderFactory.Jobs)
            //        .SetTargetsPickedUp(db)
            //    ;
            //}   
        }

        public void DeleteSocialData(long CredentialID)
        {
            try
            {
                using (MainEntities db = ContextFactory.Main)
                {
                    //long AccountID = (from c in db.Credentials
                    //                  where c.ID == CredentialID
                    //                  select c.AccountID).FirstOrDefault();


                    SocialCredentialDTO socialCredentialsDTO = GetSocialCredentialInfo(CredentialID);

                    if (socialCredentialsDTO != null)
                    {
                        db.spDeleteSocialData(socialCredentialsDTO.ID, socialCredentialsDTO.AccountID, socialCredentialsDTO.SocialNetworkID);
                    }
                }
            }
            catch (Exception ex)
            {
                ProviderFactory.Logging.LogException(ex);
            }
        }

    }
}
