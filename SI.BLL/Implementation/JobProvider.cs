﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Common;
using System.Data.SqlClient;
using Microsoft.Data.Extensions;
using Newtonsoft;

using SI.DAL;
using SI.DTO;


namespace SI.BLL
{
    internal class JobProvider : IJobProvider
    {

        public DTO.SystemStatusDTO GetSystemStatus(long userID)
        {
            if (!ProviderFactory.Security.HasPermission(userID, PermissionTypeEnum.system_admin))
                return null;

            using (SystemDBEntities db = ContextFactory.System)
            {

                SystemStatusDTO dto = new SystemStatusDTO();

                if (db.Connection.State != System.Data.ConnectionState.Open)
                    db.Connection.Open();

                DbCommand cmd = null;
                List<SqlParameter> parameters = new List<SqlParameter>();


                cmd = db.CreateStoreCommand(
                    "spGetSystemStatus",
                    System.Data.CommandType.StoredProcedure
                );

                using (cmd)
                {
                    using (DbDataReader reader = cmd.ExecuteReader())
                    {
                        dto.Controllers = new List<ControllerStatusDTO>();
                        while (reader.Read())
                        {
                            ControllerStatusDTO cstat = new ControllerStatusDTO()
                            {
                                ID = Util.GetReaderValue<long>(reader, "ID", 0),
                                MachineName = Util.GetReaderValue<string>(reader, "MachineName", ""),
                                IPAddress = Util.GetReaderValue<string>(reader, "IPAddress", ""),
                                JobCount = Util.GetReaderValue<int>(reader, "JobCount", 0),
                                DateCreated = Util.GetReaderValue<DateTime>(reader, "DateCreated", DateTime.Now).ToString("MM/dd/yyyy hh:mm:ss tt"),
                                LastHeartbeat = Util.GetReaderValue<DateTime>(reader, "LastHeartbeat", DateTime.Now).ToString("MM/dd/yyyy hh:mm:ss tt"),
                                IsAlive = Util.GetReaderValue<DateTime>(reader, "LastHeartbeat", DateTime.Now).AddSeconds(5) >= DateTime.Now,
                                LoadRating = Util.GetReaderValue<int>(reader, "LoadRating", 0),
                                CurrentLoad = Util.GetReaderValue<int>(reader, "CurrentLoad", 0)
                            };

                            dto.Controllers.Add(cstat);
                        }


                        if (reader.NextResult())
                        {
                            dto.JobStatusSummaryItems = db.Translate<JobStatusSummaryItemDTO>(reader).ToList();
                        }
                    }
                }

                return dto;
            }
        }

        public long RegisterController(string machineName, string ipAddress, int currentLoad)
        {
            using (SystemDBEntities db = ContextFactory.System)
            {
                Controller ctl = (from c in db.Controllers where c.MachineName.Trim().ToLower() == machineName.Trim().ToLower() select c).FirstOrDefault();
                if (ctl == null)
                {
                    ctl = new Controller() { MachineName = machineName, IPAddress = ipAddress, LastHeartbeat = DateTime.Now, DateCreated = DateTime.Now };
                    db.Controllers.AddObject(ctl);
                }
                else
                {
                    ctl.LastHeartbeat = DateTime.Now;
                    ctl.IPAddress = ipAddress;
                }

                db.SaveChanges();

                return ctl.ID;
            }
        }

        public DTO.SaveEntityDTO<DTO.JobDTO> Save(DTO.SaveEntityDTO<DTO.JobDTO> sde)
        {
            SaveEntityDTO<List<JobDTO>> sde2 = new SaveEntityDTO<List<JobDTO>>(new List<JobDTO> { sde.Entity }, sde.UserInfo);
            sde2 = Save(sde2);

            sde.Problems = sde2.Problems;
            if (sde2.Entity.Any())
                sde.Entity = sde2.Entity[0];

            return sde;
        }


        public SaveEntityDTO<List<JobDTO>> Save(SaveEntityDTO<List<JobDTO>> sde)
        {
            ITimezoneProvider tz = ProviderFactory.TimeZones;
            TimeZoneInfo userTimeZone = tz.GetTimezoneForUserID(sde.UserInfo.EffectiveUser.ID.Value);

            using (SystemDBEntities db = ContextFactory.System)
            {

                List<JobDTO> jobs = sde.Entity;

                StringBuilder sb = new StringBuilder();
                sb.AppendLine(" set nocount on create table #t (id bigint not null) ");

                List<Job> jlist = new List<Job>();
                int counter = 0;

                foreach (JobDTO job in jobs)
                {
                    if (job.JobDataObject != null)
                    {
                        if (job.JobDataObject.JobType != job.JobType)
                        {
                            throw new Exception(string.Format("Attempted to save a job where JobData was of type [{0}] and Job's type was [{1}].", job.JobDataObject.JobType.ToString(), job.JobType.ToString()));
                        }
                    }

                    if (string.IsNullOrWhiteSpace(job.JobData) && job.JobDataObject != null)
                    {
                        job.JobData = Newtonsoft.Json.JsonConvert.SerializeObject(job.JobDataObject);
                    }

                    string chainedJobs = "null";
                    if (job.ChainedJobs != null && job.ChainedJobs.Any())
                    {
                        chainedJobs = string.Format("'{0}'", Newtonsoft.Json.JsonConvert.SerializeObject(job.ChainedJobs).Replace("'", "''"));
                    }

                    sb.AppendFormat(" insert #t(id) exec spSaveJob @id={0}", job.ID.HasValue ? job.ID.Value.ToString(CultureInfo.InvariantCulture) : "null");
                    sb.AppendFormat(", @jobTypeID={0}", (long)job.JobType);
                    sb.AppendFormat(", @jobStatusID={0}", (long)job.JobStatus);
                    sb.AppendFormat(", @priority={0}", job.Priority);
                    sb.AppendFormat(", @controllerID={0}", job.ControllerID.HasValue ? job.ControllerID.ToString() : "null");
                    sb.AppendFormat(", @processID={0}", job.ProcessID.HasValue ? job.ProcessID.ToString() : "null");
                    sb.AppendFormat(", @dateScheduled='{0}'", job.DateScheduled.GetUTCDate(userTimeZone).ToString("MM/dd/yyyy hh:mm:ss tt"));
                    if (job.DateCompleted != null)
                        sb.AppendFormat(", @dateCompleted='{0}'", job.DateCompleted.GetUTCDate(userTimeZone).ToString("MM/dd/yyyy hh:mm:ss tt"));
                    else
                        sb.AppendFormat(", @dateCompleted=null");
                    sb.AppendFormat(", @jobData={0}", string.IsNullOrWhiteSpace(job.JobData) ? "null" : "'" + job.JobData.Replace("'", "''") + "'");
                    sb.AppendFormat(", @chainedJobs={0}", chainedJobs);
                    sb.AppendFormat(", @continueChainIfFailed={0}", job.ContinueChainIfFailed ? "1" : "0");
                    sb.AppendFormat(", @originJobID={0}", job.OriginJobID.HasValue ? job.OriginJobID.Value.ToString(CultureInfo.InvariantCulture) : "null");
                    sb.AppendLine(" ");

                    counter++;
                    if (counter > 50)
                    {
                        counter = 0;
                        sb.AppendLine(" select j.* from Jobs j where exists ( select * from #t t where t.id=j.id ) ");
                        sb.AppendLine(" drop table #t ");
                        List<Job> jlist2 = db.ExecuteStoreQuery<Job>(sb.ToString()).ToList();
                        jlist.AddRange(jlist2);
                        sb.Clear();
                        sb.AppendLine(" set nocount on create table #t (id bigint not null) ");
                    }
                }
                if (counter > 0)
                {
                    sb.AppendLine(" select j.* from Jobs j where exists ( select * from #t t where t.id=j.id ) ");
                    sb.AppendLine(" drop table #t ");
                    List<Job> jlist2 = db.ExecuteStoreQuery<Job>(sb.ToString()).ToList();
                    jlist.AddRange(jlist2);
                    sb.Clear();
                }

                jobs.Clear();
                foreach (Job item in jlist)
                {
                    jobs.Add(Job2DTO(item, userTimeZone));
                }

                return new SaveEntityDTO<List<JobDTO>>(jobs, new UserInfoDTO(0, null));

            }
        }



        public List<DTO.JobDTO> GetJobs(List<long> jobIDs)
        {
            TimeZoneInfo timeZone = TimeZoneInfo.Utc;

            using (SystemDBEntities db = ContextFactory.System)
            {
                return (
                    from j in db.Jobs
                    where jobIDs.Contains(j.ID)
                    select j
                )
                .ToList()
                .Select(j => Job2DTO(j, timeZone))
                .ToList();
            }
        }

        public JobListDTO GetJobList(long controllerID, int maxLoad)
        {
            using (SystemDBEntities db = ContextFactory.System)
            {

                if (db.Connection.State != System.Data.ConnectionState.Open)
                    db.Connection.Open();

                DbCommand cmd = null;
                List<SqlParameter> parameters = new List<SqlParameter>();

                cmd = db.CreateStoreCommand(
                    "spGetJobList",
                    System.Data.CommandType.StoredProcedure,
                    new SqlParameter("controllerID", controllerID),
                    new SqlParameter("maxLoad", maxLoad)
                );

                //List<long> IDs = new List<long>();

                JobListDTO jlist = new JobListDTO();

                using (cmd)
                {
                    using (DbDataReader reader = cmd.ExecuteReader())
                    {

                        if (reader.Read())
                        {
                            jlist.JobTypeID = Util.GetReaderValue<long?>(reader, "JobTypeID", null);
                        }

                        if (reader.NextResult())
                        {
                            while (reader.Read())
                            {
                                jlist.JobIDs.Add(Util.GetReaderValue<long>(reader, "ID", 0));
                            }
                        }
                    }
                }

                return jlist;

            }
        }


        private JobDTO Job2DTO(Job obj, TimeZoneInfo timeZone)
        {
            JobDTO dto = new JobDTO()
            {
                ControllerID = obj.ControllerID,
                OriginJobID = obj.OriginJobID,
                ProcessID = obj.ProcessID,
                DateCompleted = (obj.DateCompleted.HasValue) ? new SIDateTime(obj.DateCompleted.Value, timeZone) : null,
                DateScheduled = new SIDateTime(obj.DateScheduled, timeZone),
                DateStarted = (obj.DateStarted.HasValue) ? new SIDateTime(obj.DateStarted.Value, timeZone) : null,
                JobData = obj.JobData,
                JobStatus = (JobStatusEnum)obj.JobStatusID,
                JobType = (JobTypeEnum)obj.JobTypeID,
                ID = obj.ID,
                Priority = obj.Priority,
                ContinueChainIfFailed = obj.ContinueChainIfFailed
            };

            if (!string.IsNullOrWhiteSpace(obj.ChainedJobs))
                dto.ChainedJobs = Newtonsoft.Json.JsonConvert.DeserializeObject<Queue<ChainedJobSpec>>(obj.ChainedJobs);
            else
                dto.ChainedJobs = new Queue<ChainedJobSpec>();

            return dto;
        }


        public SaveEntityDTO<ControllerDTO> Save(SaveEntityDTO<ControllerDTO> dto)
        {
            if (!ProviderFactory.Security.HasPermission(dto.UserInfo.EffectiveUser.ID.Value, PermissionTypeEnum.system_admin))
                return new SaveEntityDTO<ControllerDTO>(dto.UserInfo) { Problems = new List<string>() { Locale.Localize("system.nopermission") } };

            Controller obj = null;
            using (SystemDBEntities db = ContextFactory.System)
            {
                if (dto.Entity.ID.HasValue)
                {
                    obj = (from c in db.Controllers where c.ID.Equals(dto.Entity.ID.Value) select c).FirstOrDefault();
                }

                if (dto == null)
                {
                    obj = new Controller()
                    {
                        DateCreated = DateTime.Now,
                        LastHeartbeat = DateTime.Now
                    };
                }

                obj.MachineName = dto.Entity.MachineName;
                obj.IPAddress = dto.Entity.IPAddress;
                obj.LoadRating = dto.Entity.LoadRating;

                if (obj.EntityState == System.Data.EntityState.Detached)
                    db.Controllers.AddObject(obj);

                db.SaveChanges();

                return new SaveEntityDTO<ControllerDTO>(GetController(dto.UserInfo.EffectiveUser.ID.Value, obj.ID).Entity, new UserInfoDTO(0, null));
            }




        }

        public GetEntityDTO<ControllerDTO> GetController(long userID, long controllerID)
        {
            if (!ProviderFactory.Security.HasPermission(userID, PermissionTypeEnum.system_admin))
                return new GetEntityDTO<ControllerDTO>() { Problems = new List<string>() { Locale.Localize("system.nopermission") } };

            using (SystemDBEntities db = ContextFactory.System)
            {
                return new GetEntityDTO<ControllerDTO>(
                    (
                        from c in db.Controllers
                        where c.ID.Equals(controllerID)
                        select new ControllerDTO()
                        {
                            ID = c.ID,
                            IPAddress = c.IPAddress,
                            LoadRating = c.LoadRating,
                            MachineName = c.MachineName
                        }
                    ).FirstOrDefault()
                );
            }
        }


        public List<JobTypeDTO> GetJobTypes()
        {
            List<JobTypeDTO> jtypes = new List<JobTypeDTO>();
            using (SystemDBEntities db = ContextFactory.System)
            {
                const string sql = "select ID, Name, BatchSize, LoadRating, MaxSecondsPerJob, BundleHash from JobTypes with(nolock)";

                if (db.Connection.State != System.Data.ConnectionState.Open)
                    db.Connection.Open();

                DbCommand cmd = null;

                cmd = db.CreateStoreCommand(
                    sql
                );

                using (cmd)
                {
                    using (DbDataReader reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            jtypes.Add(new JobTypeDTO()
                            {
                                ID = Util.GetReaderValue<long>(reader, "ID", 0),
                                Name = Util.GetReaderValue<string>(reader, "Name", ""),
                                BatchSize = Util.GetReaderValue<int>(reader, "BatchSize", 0),
                                LoadRating = Util.GetReaderValue<int>(reader, "LoadRating", 0),
                                MaxSecondsPerJob = Util.GetReaderValue<int>(reader, "MaxSecondsPerJob", 0),
                                BundleHash = Util.GetReaderValue<string>(reader, "BundleHash", "")
                            });
                        }
                    }
                }
            }

            return jtypes;
        }

        public void StoreJobBundle(long userID, long jobTypeID, byte[] zipContents)
        {
            using (SystemDBEntities db = ContextFactory.System)
            {
                string hash = Security.GetFilenameSafeHash(zipContents, 15);

                JobType jt = (from j in db.JobTypes where j.ID.Equals(jobTypeID) select j).FirstOrDefault();
                jt.Bundle = zipContents;
                jt.BundleHash = hash;

                db.SaveChanges();
            }
        }

        public byte[] GetJobBundle(long userID, long jobTypeID)
        {
            using (SystemDBEntities db = ContextFactory.System)
            {
                return (
                    from jt in db.JobTypes
                    where jt.ID == jobTypeID
                    select jt.Bundle
                ).FirstOrDefault();
            }
        }

        private JobTypeDTO JobType2DTO(JobType obj)
        {
            return new JobTypeDTO()
            {
                ID = obj.ID,
                BatchSize = obj.BatchSize,
                BundleHash = obj.BundleHash,
                LoadRating = obj.LoadRating,
                Name = obj.Name,
                MaxSecondsPerJob = obj.MaxSecondsPerJob
            };
        }


        public string GetJobTypeBundleHash(long jobTypeID)
        {
            using (SystemDBEntities db = ContextFactory.System)
            {
                return (from j in db.JobTypes where j.ID.Equals(jobTypeID) select j.BundleHash).FirstOrDefault();
            }
        }


        public SaveEntityDTO<JobTypeDTO> Save(SaveEntityDTO<JobTypeDTO> job)
        {
            if (!ProviderFactory.Security.HasPermission(job.UserInfo.EffectiveUser.ID.Value, PermissionTypeEnum.system_admin))
                return new SaveEntityDTO<JobTypeDTO>(job.UserInfo) { Problems = new List<string>() { "Unauthorized!" } };

            using (SystemDBEntities db = ContextFactory.System)
            {
                JobType jt = (from d in db.JobTypes where d.ID.Equals(job.Entity.ID) select d).FirstOrDefault();
                jt.LoadRating = job.Entity.LoadRating;
                jt.BatchSize = job.Entity.BatchSize;
                jt.MaxSecondsPerJob = job.Entity.MaxSecondsPerJob;
                db.SaveChanges();

                return new SaveEntityDTO<JobTypeDTO>(JobType2DTO(jt), new UserInfoDTO(0, null));
            }

        }




        public void SetJobStatus(long jobID, JobStatusEnum status)
        {
            string sql = "";
            switch (status)
            {
                case JobStatusEnum.Queued:
                case JobStatusEnum.PickedUp:
                case JobStatusEnum.Assigned:
                case JobStatusEnum.Parked:
                    sql = string.Format("update jobs set JobStatusID={0} where id={1}", (long)status, jobID);
                    break;

                case JobStatusEnum.Started:
                    sql = string.Format("update jobs set JobStatusID={0}, DateStarted=getdate() where id={1}", (long)status, jobID);
                    break;

                case JobStatusEnum.Completed:
                case JobStatusEnum.Failed:
                    sql = string.Format("update jobs set JobStatusID={0}, ControllerID=null, DateCompleted=getdate() where id={1}", (long)status, jobID);
                    break;
                
                default:
                    break;
            }

            using (SystemDBEntities db = ContextFactory.System)
            {
                db.ExecuteStoreCommand(sql);
            }

            //using (SystemDBEntities db = ContextFactory.System)
            //{
            //    Job job = (from j in db.Jobs where j.ID.Equals(jobID) select j).FirstOrDefault();
            //    if (job != null)
            //    {
            //        job.JobStatusID = (long)status;
            //        switch (status)
            //        {
            //            case JobStatusEnum.Queued:
            //                break;
            //            case JobStatusEnum.PickedUp:
            //                break;
            //            case JobStatusEnum.Assigned:
            //                break;

            //            case JobStatusEnum.Started:
            //                job.DateStarted = DateTime.Now;
            //                break;

            //            case JobStatusEnum.Completed:
            //            case JobStatusEnum.Failed:
            //                job.DateCompleted = DateTime.Now;
            //                job.ControllerID = null;
            //                break;

            //            default:
            //                break;
            //        }

            //        db.SaveChanges();
            //    }
            //}
        }


        public string GetProcessorSetting(string key)
        {
            using (SystemDBEntities db = ContextFactory.System)
            {
                return (from s in db.ProcessorSettings where s.Name.Trim().ToLower() == key.Trim().ToLower() select s.Value).FirstOrDefault();
            }
        }


        public List<ScheduleDTO> GetSchedules()
        {
            using (SystemDBEntities db = ContextFactory.System)
            {
                return (
                    from s in
                        (from s in db.Schedules select s).ToList()
                    select new ScheduleDTO()
                    {
                        ID = s.ID,
                        Name = s.Name,
                        DateLastExecuted = s.DateLastExecuted,
                        JobData = s.JobData,
                        JobType = (JobTypeEnum)s.JobTypeID,
                        JobPriority = s.JobPriority,
                        ScheduleSpec = ScheduleSpecDTO.FromString(s.ScheduleSpec)
                    }
                ).ToList();
            }
        }

        public void SaveSchedule(ScheduleDTO dto)
        {
            using (SystemDBEntities db = ContextFactory.System)
            {
                Schedule sched = null;
                if (!dto.ID.HasValue)
                {
                    sched = new Schedule();
                }
                else
                {
                    sched = (from s in db.Schedules where s.ID == dto.ID.Value select s).FirstOrDefault();
                }

                sched.Name = dto.Name;
                sched.JobTypeID = (long)dto.JobType;
                sched.ScheduleSpec = dto.ScheduleSpec.ToString();
                sched.JobData = dto.JobData;
                sched.JobPriority = dto.JobPriority;
                sched.DateLastExecuted = dto.DateLastExecuted;

                if (sched.EntityState == System.Data.EntityState.Detached)
                    db.Schedules.AddObject(sched);

                db.SaveChanges();
            }
        }


        public void PerformDatabaseCleanup()
        {
            using (SystemDBEntities db = ContextFactory.System)
            {
                db.spCleanupJobs();
            }
            using (MainEntities db = ContextFactory.Main)
            {
                db.spCleanupMain();
            }
        }


        public void ReconcileControllerProcessInstances(long controllerID, List<long> jobIDs)
        {

        }


        public void SavePerformanceSnapshot(PerfSnapshotDTO snap)
        {
            using (SystemDBEntities db = ContextFactory.System)
            {
                if (db.Connection.State != System.Data.ConnectionState.Open)
                    db.Connection.Open();

                StringBuilder sb = new StringBuilder();

                sb.AppendFormat(" declare @id bigint exec spInsertPerfSnap {0}, {1}, {2}, @id OUTPUT \r\n", snap.ControllerID, snap.CPULoad, snap.AvailableRAM);
                foreach (PerfSnapshotJobDTO job in snap.Jobs)
                {
                    sb.AppendFormat(" exec spInsertPerfSnapJob @id, {0}, {1} ", job.JobTypeID, job.CPULoad);
                }

                db.ExecuteStoreCommand(sb.ToString());
            }
        }


        public void LogJobPickups(long controllerID, List<long> jobIDs)
        {
            try
            {
                if (jobIDs.Any())
                {

                    using (SystemDBEntities db = ContextFactory.System)
                    {
                        if (db.Connection.State != System.Data.ConnectionState.Open)
                            db.Connection.Open();

                        StringBuilder sb = new StringBuilder();
                        foreach (long jobID in jobIDs)
                        {
                            sb.AppendFormat(" exec spLogPickup {0},{1} \r\n ", jobID, controllerID);
                        }

                        db.ExecuteStoreCommand(sb.ToString());
                    }

                }

            }
            catch (Exception ex)
            {
                ProviderFactory.Logging.LogException(ex);
            }
        }
    }
}
