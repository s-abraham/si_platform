﻿using System.Data.Objects;
using System.Globalization;
using SI.DAL;
using SI.DTO;
using Microsoft.Data.Extensions;
using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using System.Xml.Linq;
using System.Dynamic;
using SI.BLL.Extensions;
using Notification;
using System.Reflection;


namespace SI.BLL
{
    internal class SecurityProvider : ISecurityProvider
    {
        public static string PLATFORM_NOTIFICATION_MESSAGE_TYPE_IDS = "2|7|14";
        public static int PLATFORM_MAX_ACCOUNT_ID_LIST = 10000;

        #region private reader classes

        private class PermissionCacheItem
        {
            public PermissionCacheItem()
            {
                DateCached = DateTime.UtcNow;
                Settings = new Dictionary<string, string>();
            }
            public DateTime DateCached { get; set; }
            public bool IsSuperAdmin { get; set; }
            public List<PermissionListItem> Permissions { get; set; }
            public Dictionary<string, string> Settings { get; set; }
        }
        private class PermissionListItem
        {
            public long AccountID { get; set; }
            public long PermissionTypeID { get; set; }
            public bool IncludeChildren { get; set; }
        }
        private class AccountTreeItem
        {
            public long ID { get; set; }
            public string Name { get; set; }
            public long? ParentAccountID { get; set; }
            public int Level { get; set; }
            public long? ResellerID { get; set; }
            public long AccountTypeID { get; set; }
        }
        private class PlatformNotificationCount
        {
            public long AccountID { get; set; }
            public long NotificationMessageTypeID { get; set; }
            public long UserID { get; set; }
            public int NotificationCount { get; set; }
            public int RolledNotificationCount { get; set; }
        }

        #endregion

        #region users

        public List<long> GetAllUserIDs()
        {
            using (MainEntities db = ContextFactory.Main)
            {
                return (from u in db.Users select u.ID).ToList();
            }
        }

        private UserDTO getUser(long? userID, string username, string emailAddress, UserInfoDTO userInfo, bool includeSSOSecretKey)
        {

            if (username != null)
                username = username.Trim();

            if (emailAddress != null)
                emailAddress = emailAddress.Trim();

            if (string.IsNullOrWhiteSpace(username))
                username = null;
            if (string.IsNullOrWhiteSpace(emailAddress))
                emailAddress = null;

            int permissionCacheMinutes = Settings.GetSetting<int>("security.permissioncacheminutes");
            bool loadPermissions = true;
            PermissionCacheItem cache = null;

            if (userID.HasValue)
            {
                lock (_permissionCache)
                {
                    if (_permissionCache.TryGetValue(userID.Value, out cache))
                    {
                        if (cache.DateCached.AddMinutes(permissionCacheMinutes) >= DateTime.UtcNow)
                        {
                            loadPermissions = false;
                        }
                    }
                }
            }


            Dictionary<string, string> settings = null;

            using (MainEntities db = ContextFactory.Main)
            {

                if (db.Connection.State != System.Data.ConnectionState.Open)
                    db.Connection.Open();

                DbCommand cmd = null;
                List<SqlParameter> parameters = new List<SqlParameter>();

                if (userID.HasValue)
                {
                    cmd = db.CreateStoreCommand(
                    "spGetUser",
                    System.Data.CommandType.StoredProcedure,
                        new SqlParameter("userID", userID.Value),
                        new SqlParameter("returnPermissions", loadPermissions),
                        new SqlParameter("returnRoles", false)
                    );

                }
                else if (username != null)
                {
                    cmd = db.CreateStoreCommand(
                    "spGetUser",
                    System.Data.CommandType.StoredProcedure,
                        new SqlParameter("username", username),
                        new SqlParameter("returnPermissions", loadPermissions),
                        new SqlParameter("returnRoles", false)
                    );
                }
                else
                {
                    cmd = db.CreateStoreCommand(
                    "spGetUser",
                    System.Data.CommandType.StoredProcedure,
                        new SqlParameter("emailAddress", emailAddress),
                        new SqlParameter("returnPermissions", loadPermissions),
                        new SqlParameter("returnRoles", false)
                    );
                }

                using (cmd)
                {
                    using (DbDataReader reader = cmd.ExecuteReader())
                    {
                        if (!reader.Read())
                        {
                            return null;
                        }

                        UserDTO dto = userDTOfromReader(reader, null, includeSSOSecretKey);

                        if (dto != null)
                        {
                            ITimezoneProvider tz = ProviderFactory.TimeZones;
                            TimeZoneInfo timeZone = tz.GetTimezoneForUserID(dto.ID.GetValueOrDefault());
                            SIDateTime currentDateTime = new SIDateTime(DateTime.UtcNow, timeZone);

                            dto.CreateDate = new SIDateTime(Util.GetReaderValue<DateTime>(reader, "datecreated", DateTime.UtcNow), timeZone);
                            dto.Reseller = GetResellerByAccountID(userInfo, dto.MemberOfAccountID, timeZone);
                            dto.CurrentDateTime = currentDateTime;
                        }

                        if (loadPermissions)
                        {
                            if (reader.NextResult())
                            {
                                List<PermissionListItem> perms = db.Translate<PermissionListItem>(reader).ToList();

                                lock (_permissionCache)
                                {
                                    if (_permissionCache.TryGetValue(dto.ID.Value, out cache))
                                    {
                                        _permissionCache.Remove(dto.ID.Value);
                                    }

                                    cache = new PermissionCacheItem() { Permissions = perms };
                                    _permissionCache.Add(dto.ID.Value, cache);
                                }

                            }
                        }

                        cache.IsSuperAdmin = dto.SuperAdmin;

                        // user settings
                        if (reader.NextResult())
                        {
                            settings = db.Translate<UserSetting>(reader).ToDictionary(t => t.Name.Trim().ToLower(), t => t.Value);
                            cache.Settings = settings;
                        }


                        // find the account context
                        dto.CurrentContextAccountID = GetUserSetting<long>(new UserInfoDTO(dto.ID.Value, null), "CurrentContextAccountID", dto.MemberOfAccountID);

                        using (DbCommand cmd2 = db.CreateStoreCommand("spGetPlatformNotificationCounts", System.Data.CommandType.StoredProcedure,
                            new SqlParameter("@UserID", dto.ID.Value),
                            new SqlParameter("@NotificationMessageTypeIDs", PLATFORM_NOTIFICATION_MESSAGE_TYPE_IDS),
                            new SqlParameter("@AccountIDs", dto.CurrentContextAccountID.ToString(CultureInfo.InvariantCulture))
                        ))
                        {
                            using (DbDataReader reader2 = cmd2.ExecuteReader())
                            {
                                List<PlatformNotificationCount> ncounts = db.Translate<PlatformNotificationCount>(reader2).ToList();
                                dto.NotificationCount = Convert.ToInt32((from n in ncounts select n.RolledNotificationCount).Sum());
                            }
                        }

                        // stuff everyone can see                                             

                        dto.CanDashboardOverviewBasic = true;

                        dto.ViewAccountPicker = true;

                        dto.CanPreferencesMenu = true;
                        dto.CanChangePasswordMenu = true;
                        dto.CanMessageSupportMenu = true;

                        dto.CanProductConfigurationMenu = true;

                        bool hasAccountAdmin = HasPermission(dto.ID.Value, PermissionTypeEnum.accounts_admin);

                        bool hasUserAdmin = HasPermission(dto.ID.Value, PermissionTypeEnum.users_admin);
                        bool hasInternalAdmin = HasPermission(dto.ID.Value, PermissionTypeEnum.internal_admin);
                        bool hasVGAdmin = HasPermission(dto.ID.Value, PermissionTypeEnum.virtualGroups_admin);


                        bool hasPaidSocial = HasFeature(dto.CurrentContextAccountID, FeatureTypeEnum.SocialCore);
                        bool hasSocialPerm = HasPermission(dto.ID.Value, PermissionTypeEnum.social_view);
                        bool hasSocialPublish = HasPermission(dto.ID.Value, PermissionTypeEnum.social_publish) && (HasFeature(dto.CurrentContextAccountID, FeatureTypeEnum.SocialCore) || HasFeature(dto.CurrentContextAccountID, FeatureTypeEnum.SyndicatorCore));
                        bool hasSocialAdmin = HasPermission(dto.ID.Value, PermissionTypeEnum.social_admin) && HasFeature(dto.CurrentContextAccountID, FeatureTypeEnum.SocialCore);
                        bool hasSocialSyndicator = HasFeature(dto.CurrentContextAccountID, FeatureTypeEnum.SyndicatorCore);
                        bool hasSocialSyndicatee = HasFeature(dto.CurrentContextAccountID, FeatureTypeEnum.SyndicateeCore);
                        bool hasCoreReputation = HasFeature(dto.CurrentContextAccountID, FeatureTypeEnum.ReputationCore);
                        bool hasReputationPerm = HasPermission(dto.ID.Value, PermissionTypeEnum.reviews_view);

                        dto.CanSyndicationHelpPage = HasFeature(dto.CurrentContextAccountID, FeatureTypeEnum.LexusSyndicatee);

                        if (HasFeature(dto.CurrentContextAccountID, FeatureTypeEnum.LexusSyndicator))
                        {
                            dto.Syndicators.Add("Lexus");
                        }

                        if (HasFeature(dto.CurrentContextAccountID, FeatureTypeEnum.CarfaxSyndicator))
                        {
                            dto.Syndicators.Add("Carfax");
                        }

                        if (HasFeature(dto.CurrentContextAccountID, FeatureTypeEnum.AllySyndicator))
                        {
                            dto.Syndicators.Add("Ally");
                        }

                        if (HasFeature(dto.CurrentContextAccountID, FeatureTypeEnum.AutoConverseSyndicator))
                        {
                            dto.Syndicators.Add("Auto Converse");
                        }

                        bool hasPaidProduct = hasPaidSocial || hasCoreReputation || hasSocialSyndicator;

                        var accountsUserHasAccess = AccountsWithPermission(dto.ID.Value, PermissionTypeEnum.accounts_view);

                        if ((accountsUserHasAccess == null) || (accountsUserHasAccess.Count == 1))
                        {
                            dto.HasMultipleAccountAccess = false;
                        }
                        else
                        {
                            dto.HasMultipleAccountAccess = true;
                        }

                        var hasMultipleAccountsWithPaidProducts = GetAccountsWithUserPermissionAndFeature(dto.ID.Value, PermissionTypeEnum.accounts_view, FeatureTypeEnum.PaidProduct);

                        if ((hasMultipleAccountsWithPaidProducts == null) || (hasMultipleAccountsWithPaidProducts.Count == 1) || hasSocialSyndicator)
                        {
                            dto.HasMultipleAccountsWithPaidProducts = false;
                        }
                        else
                        {
                            dto.HasMultipleAccountsWithPaidProducts = true;
                        }

                        dto.CurrentAccountHasChildren = accountHasChildren(dto.CurrentContextAccountID);

                        /**************************************************************/
                        // ADMIN
                        /**************************************************************/

                        dto.CanAdminAccount = (hasAccountAdmin && hasPaidProduct) || (hasInternalAdmin) || (dto.SuperAdmin);
                        dto.CanAdminUsers = (hasInternalAdmin && hasPaidProduct) || (dto.SuperAdmin);
                        //dto.CanAdminVirtualGroups = hasVGAdmin && hasPaidProduct;
                        dto.CanAdminVirtualGroups = false;
                        dto.CanAdminSystem = dto.SuperAdmin;
                        dto.CanAdminExceptions = dto.SuperAdmin;
                        dto.CanAdminFlushCache = dto.SuperAdmin;


                        dto.ViewAdminAccountsFilter = hasAccountAdmin || (hasInternalAdmin) || (dto.SuperAdmin);
                        dto.ViewAdminAccountsGrid = hasAccountAdmin || (hasInternalAdmin) || (dto.SuperAdmin);
                        dto.ViewAdminAccountPackagesFilter = hasInternalAdmin;
                        dto.CanAdminAccountEdit = hasAccountAdmin || (hasInternalAdmin) || (dto.SuperAdmin);

                        //dto.CanSupportCenterMenu = hasInternalAdmin;


                        /**************************************************************/
                        // SOCIAL
                        /**************************************************************/


                        dto.CanPublish = hasSocialPublish;
                        dto.AllowSyndicationEdit = hasSocialSyndicatee;

                        dto.CanDealerPostActivity = (hasPaidSocial || hasSocialSyndicator || hasSocialSyndicatee) && hasSocialPerm;
                        dto.CanSocialLocation = hasPaidSocial && hasSocialPerm;
                        dto.CanSocialDashboard = hasPaidSocial && hasSocialPerm;

                        dto.ViewDashboardOverviewSocial = hasPaidSocial && hasSocialPerm;
                        dto.ViewDashboardOverviewSocialGrid = hasPaidSocial && hasSocialPerm;

                        dto.ViewDealerPostActivityGridFilter = (hasPaidSocial || hasSocialSyndicator || hasSocialSyndicatee) && hasSocialPerm;
                        dto.ViewDealerPostActivityGrid = (hasPaidSocial || hasSocialSyndicator || hasSocialSyndicatee) && hasSocialPerm;
                        dto.ViewPostManagementGrid = dto.ViewDealerPostActivityGrid;

                        dto.CanPublishDataInserts = (hasPaidSocial || hasSocialSyndicator) && hasSocialPerm;
                        dto.ViewPublishAccountsFilter = (hasPaidSocial || hasSocialSyndicator) && hasSocialPerm;
                        dto.EnableUserPreferencesNotificationsSocial = hasPaidSocial && hasSocialPerm;

                        dto.CanDealerPostActivityCompose = hasSocialPublish || hasSocialSyndicator;
                        dto.CanPostManagementCompose = dto.CanDealerPostActivityCompose;
                        dto.CanSocialPostManagement = hasSocialSyndicator && hasSocialPerm;

                        dto.CanPublishSendNow = hasSocialPublish || hasSocialSyndicator;
                        dto.CanPublishSchedulePost = hasSocialPublish || hasSocialSyndicator;

                        dto.CanPublishSyndicationOptions = hasSocialSyndicator;
                        dto.CanPublishSyndicate = hasSocialSyndicator;

                        dto.CanApprovalHistory = hasInternalAdmin || dto.SuperAdmin;

                        /**************************************************************/
                        // REPUTATION
                        /**************************************************************/


                        dto.EnableUserPreferencesNotificationsReputation = hasReputationPerm && hasCoreReputation;

                        dto.ViewDashboardOverviewReputation = hasReputationPerm && hasCoreReputation;
                        dto.ViewDashboardOverviewReputationGrid = hasReputationPerm && hasCoreReputation;

                        dto.ViewReviewStreamCountPerSite = hasReputationPerm;
                        dto.ViewReviewStreamPositiveNegative = hasReputationPerm;
                        dto.ViewReviewStreamGrid = hasReputationPerm;
                        dto.ViewReviewStreamFilter = hasReputationPerm;
                        dto.ViewReviewStreamGridAction = hasReputationPerm;
                        dto.CanReviewStreamRespondedAction = hasReputationPerm;
                        dto.CanReviewStreamMarkReadAction = hasReputationPerm;
                        dto.CanReviewStreamEmailAction = hasReputationPerm;
                        dto.CanReviewStreamShareAction = hasReputationPerm;
                        dto.CanReviewStreamIgnoreAction = hasReputationPerm;
                        dto.ViewReviewSiteGrid = hasReputationPerm;
                        dto.CanDashboardReputationLocation = hasReputationPerm && hasCoreReputation;

                        dto.CanReputationStream = hasReputationPerm && hasCoreReputation;
                        dto.CanReputationSites = hasReputationPerm && hasCoreReputation;
                        dto.CanCompetitiveAnalysis = hasReputationPerm && hasCoreReputation;

                        /**************************************************************/
                        // REPORTING
                        /**************************************************************/

                        // Delete dto.CanReportUsers = HasPermission(dto.ID.Value, PermissionTypeEnum.users_view);

                        //dto.CanReportSAM = HasPermission(dto.ID.Value, PermissionTypeEnum.internal_admin);
                        dto.CanMenuReportsPostPerformance = hasSocialSyndicator && hasSocialPerm;
                        dto.CanMenuReportsPostStream = (hasPaidSocial || hasSocialSyndicator) && hasSocialPerm;
                        dto.CanReportSAM = false;
                        dto.CanReportsMenu = hasPaidSocial || hasCoreReputation || hasInternalAdmin || hasSocialSyndicator;
                        dto.CanSocialStream = dto.CanPublish; //TODO change to real
                        dto.CanViewSocialReports = (hasPaidSocial && hasSocialPerm) || hasInternalAdmin;
                        dto.CanViewReputationReports = (hasCoreReputation && hasReputationPerm) || hasInternalAdmin;
                        dto.CanViewSyndicatorReports = (hasSocialSyndicator && hasSocialPerm) || hasInternalAdmin;
                        dto.CanViewAdminReports = hasInternalAdmin;
                        dto.CanMenuSocialSnapshot = hasPaidSocial && hasSocialPerm;
                        dto.CanMenuReputationSnapshot = hasReputationPerm && hasCoreReputation;
                        dto.CanMenuReportPostSummary = hasPaidSocial && hasSocialPerm;

                        //dto.CanViewReputationReports 

                        /**************************************************************/
                        // CLEANUP
                        /**************************************************************/
                        dto.ViewAccountPickerNotifications = hasPaidProduct;
                        dto.ViewUserPreferencesNotifications = hasPaidProduct;
                        dto.CanNotificationsMenu = hasPaidProduct;
                        dto.CanViewNotificationGlobe = hasPaidProduct || hasSocialSyndicatee || hasSocialSyndicator;
                        return dto;
                    }

                }


            }
        }

        private bool accountHasChildren(long accountID)
        {
            Dictionary<long, AccountTree> tree = getAccountTree();
            if (!tree.ContainsKey(accountID))
                return false;

            AccountTree item = tree[accountID];
            return (item.Children.Any());
        }

        public BasicUserInfoDTO GetBasicUserInfoByID(long userID)
        {
            using (MainEntities db = ContextFactory.Main)
            {
                var info = (
                    from u in db.Users
                    join a in db.Accounts on u.MemberOfAccountID equals a.ID
                    where u.ID == userID
                    select new
                    {
                        ID = u.ID,
                        FirstName = u.FirstName,
                        LastName = u.LastName,
                        Username = u.Username,
                        MemberOfAccountID = u.MemberOfAccountID,
                        EmailAddress = u.EmailAddress,
                        MOADisplayName = a.DisplayName,
                        MOAName = a.Name,
                        IsActive = !u.AccountLocked
                    }
                ).FirstOrDefault();

                if (info == null) return null;

                string accountName = info.MOAName.Trim();
                if (!string.IsNullOrWhiteSpace(info.MOADisplayName))
                {
                    accountName = info.MOADisplayName.Trim();
                }

                BasicUserInfoDTO uinfo = new BasicUserInfoDTO()
                {

                    ID = info.ID,
                    EmailAddress = info.EmailAddress,
                    FirstName = info.FirstName,
                    LastName = info.LastName,
                    IsActive = info.IsActive,
                    Username = info.Username,

                    MemberOfAccountID = info.MemberOfAccountID,
                    MemberOfAccountName = accountName
                };

                return uinfo;
            }
        }

        private UserDTO userDTOfromReader(DbDataReader reader, TimeZoneInfo timeZone, bool includeSSOSecretKey)
        {
            SIDateTime siCreateDateTime = null;
            SIDateTime siModifiedDateTime = null;
            SIDateTime siDateAccountDeactivated = null;
            if (timeZone != null)
            {
                siCreateDateTime = new SIDateTime(Util.GetReaderValue<DateTime>(reader, "DateCreated", DateTime.MinValue), timeZone);
                siModifiedDateTime = new SIDateTime(Util.GetReaderValue<DateTime>(reader, "DateModified", DateTime.MinValue), timeZone);

                DateTime? dateDeactivated = Util.GetReaderValue<DateTime?>(reader, "AccountDeactivatedDate", null);
                if (dateDeactivated.HasValue)
                {
                    siDateAccountDeactivated = new SIDateTime(dateDeactivated.Value, timeZone);
                }
            }

            UserDTO dto = new UserDTO()
            {
                ID = Util.GetReaderValue<long>(reader, "ID", 0),
                RootAccountID = Util.GetReaderValue<long>(reader, "RootAccountID", 0),
                MemberOfAccountID = Util.GetReaderValue<long>(reader, "MemberOfAccountID", 0),
                Username = Util.GetReaderValue<string>(reader, "Username", ""),
                FirstName = Util.GetReaderValue<string>(reader, "FirstName", ""),
                LastName = Util.GetReaderValue<string>(reader, "LastName", ""),
                SuperAdmin = Util.GetReaderValue<bool>(reader, "SuperAdmin", false),
                Password = Util.GetReaderValue<string>(reader, "Password", ""),
                EmailAddress = Util.GetReaderValue<string>(reader, "EmailAddress", ""),
                CreateDate = siCreateDateTime,
                RoleTypeID = Util.GetReaderValue<long?>(reader, "RoleTypeID", null),
                RoleTypeName = Util.GetReaderValue<string>(reader, "RoleTypeName", ""),
                AccountName = Util.GetReaderValue<string>(reader, "AccountName", ""),
                CascadeRole = Util.GetReaderValue<bool>(reader, "CascadeRole", false),
                AccountLocked = Util.GetReaderValue<bool>(reader, "AccountLocked", false),
                InvalidLoginAttempts = Util.GetReaderValue<int>(reader, "InvalidLoginAttempts", 0),
                DateModified = siModifiedDateTime,
                AccountDeactivatedDate = siDateAccountDeactivated
            };
            if (includeSSOSecretKey)
            {
                dto.SSOSecretKey = Util.GetReaderValue<Guid>(reader, "SSOSecretKey", Guid.Empty);
            }

            return dto;
        }

        private ValidateUserResultDTO validateUser(long? userID, long? impersonatingUserID, string username, string password)
        {
            UserDTO dto = getUser(userID, username, null, new UserInfoDTO(0, null), false);

            if (dto == null)
            {
                return new ValidateUserResultDTO() { Outcome = ValidateUserOutcome.UserNotFound };
            }

            if (!userID.HasValue)
            {
                if (dto.Password != Security.GetPasswordHash(password))
                {
                    using (MainEntities db = ContextFactory.Main)
                    {
                        User user = (from u in db.Users where u.ID == dto.ID.Value select u).FirstOrDefault();
                        if (user != null)
                        {
                            Auditor
                                .New(AuditLevelEnum.Information, AuditTypeEnum.UserActivity, UserInfoDTO.System, "")
                                .SetAuditDataObject(
                                    AuditUserActivity
                                        .New(UserInfoDTO.System, AuditUserActivityTypeEnum.UserInvalidPassword)
                                        .SetUserIDActedUpon(userID)
                                )
                                .Save(ProviderFactory.Logging)
                            ;

                            user.InvalidLoginAttempts++;
                            if (user.InvalidLoginAttempts >= Settings.GetSetting<int>("policy.maxfailedloginattempts", 3))
                            {
                                user.AccountLocked = true;
                                Auditor
                                    .New(AuditLevelEnum.Information, AuditTypeEnum.UserActivity, UserInfoDTO.System, "")
                                    .SetAuditDataObject(
                                        AuditUserActivity
                                            .New(UserInfoDTO.System, AuditUserActivityTypeEnum.UserLocked)
                                            .SetUserIDActedUpon(userID)
                                    )
                                    .Save(ProviderFactory.Logging)
                                ;
                            }
                            db.SaveChanges();
                            if (user.AccountLocked)
                            {
                                return new ValidateUserResultDTO() { Outcome = ValidateUserOutcome.AccountLocked };
                            }
                        }
                    }
                    return new ValidateUserResultDTO() { Outcome = ValidateUserOutcome.InvalidPassword };
                }
                else
                {
                    if (dto.InvalidLoginAttempts > 0)
                    {
                        using (MainEntities db = ContextFactory.Main)
                        {
                            User user = (from u in db.Users where u.ID == dto.ID.Value select u).FirstOrDefault();
                            user.InvalidLoginAttempts = 0;
                            db.SaveChanges();
                        }
                        dto.InvalidLoginAttempts = 0;
                    }
                }
            }

            UserDTO iUser = null;
            if (impersonatingUserID.HasValue)
            {
                iUser = getUser(impersonatingUserID.Value, null, null, UserInfoDTO.System, false);
            }
            UserInfoDTO userInfo = new DTO.UserInfoDTO(dto, iUser);

            ValidateUserResultDTO result = new ValidateUserResultDTO()
            {
                Outcome = ValidateUserOutcome.Success,
                UserInfo = userInfo
            };

            return result;

        }

        public bool CanImpersonateUser(UserInfoDTO userInfo, long userIDToImpersonate)
        {
            using (MainEntities db = ContextFactory.Main)
            {
                long? memberOfAccountID = (from u in db.Users where u.ID == userIDToImpersonate select u.MemberOfAccountID).FirstOrDefault();
                if (memberOfAccountID.HasValue)
                {
                    if (HasPermission(userInfo.LoggedInUser.ID.Value, memberOfAccountID.Value, PermissionTypeEnum.users_impersonate))
                    {
                        return true;
                    }
                }
                return false;
            }
        }

        public ValidateUserResultDTO ValidateUser(UserInfoDTO userInfo)
        {
            ValidateUserResultDTO result = validateUser(
                userInfo.LoggedInUser.ID.Value,
                userInfo.ImpersonatingUser == null ? null : userInfo.ImpersonatingUser.ID,
                null,
                null
            );

            return result;
        }

        public ValidateUserResultDTO ValidateUser(string username, string password)
        {
            return validateUser(null, null, username, password);
        }

        public ValidateUserResultDTO ValidateUser(Guid accessToken)
        {
            using (MainEntities db = ContextFactory.Main)
            {
                if (db.Connection.State != System.Data.ConnectionState.Open)
                    db.Connection.Open();

                DbCommand cmd = null;

                cmd = db.CreateStoreCommand(
                "spValidateAccessToken",
                System.Data.CommandType.StoredProcedure,
                    new SqlParameter("AccessToken", accessToken)
                );

                using (DbDataReader reader = cmd.ExecuteReader())
                {
                    if (reader.Read())
                    {
                        long userID = Util.GetReaderValue<long>(reader, "UserID", 0);
                        if (userID > 0)
                        {
                            return ValidateUser(new UserInfoDTO(userID, null));
                        }
                    }
                }
            }
            return new ValidateUserResultDTO()
            {
                Outcome = ValidateUserOutcome.InvalidPassword,
                UIOptions = null,
                UserInfo = null
            };
        }

        public Guid CreateSSOAccessToken(Guid resellerSecretKey, Guid userSecretKey, int expiresInMinutes)
        {
            using (MainEntities db = ContextFactory.Main)
            {

                if (db.Connection.State != System.Data.ConnectionState.Open)
                    db.Connection.Open();

                DbCommand cmd = null;

                cmd = db.CreateStoreCommand(
                "spCreateAccessToken",
                System.Data.CommandType.StoredProcedure,
                    new SqlParameter("ResellerSecretKey", resellerSecretKey),
                    new SqlParameter("UserSecretKey", userSecretKey),
                    new SqlParameter("MinutesTillExpiration", expiresInMinutes)
                );

                using (DbDataReader reader = cmd.ExecuteReader())
                {
                    if (reader.Read())
                    {
                        return Util.GetReaderValue<Guid>(reader, "AccessToken", Guid.Empty);
                    }
                }
            }

            return Guid.Empty;
        }

        public void SetUserCurrentContextAccountID(UserInfoDTO userInfo, long accountID)
        {
            SaveUserSetting(userInfo, "CurrentContextAccountID", accountID.ToString(CultureInfo.InvariantCulture));
            string accountName = GetAccountNameByID(accountID);

            Auditor
                .New(AuditLevelEnum.Information, AuditTypeEnum.UserActivity, userInfo, "")
                .SetAuditDataObject(
                    AuditUserActivity
                        .New(userInfo, AuditUserActivityTypeEnum.ChangedContextAccount)
                        .SetAccountID(accountID)
                        .SetDescription("Context changed to account [{0}]", accountName)
                )
                .Save(ProviderFactory.Logging)
            ;

        }

        public long GetSystemUserID()
        {
            return 1;
        }

        public UserDTO GetUserByID(long userID, UserInfoDTO userInfo)
        {

            bool includeSSOSecretKey =
                HasPermission(userInfo.EffectiveUser.ID.Value, PermissionTypeEnum.reseller_admin)
                || HasPermission(userInfo.EffectiveUser.ID.Value, PermissionTypeEnum.internal_admin)
            ;

            return getUser(userID, null, null, userInfo, includeSSOSecretKey);

        }

        public UserDTO GetUserByEmailAddress(string emailAddress, UserInfoDTO userInfo)
        {
            bool includeSSOSecretKey =
                HasPermission(userInfo.EffectiveUser.ID.Value, PermissionTypeEnum.reseller_admin)
                || HasPermission(userInfo.EffectiveUser.ID.Value, PermissionTypeEnum.internal_admin)
            ;

            return getUser(null, null, emailAddress, userInfo, includeSSOSecretKey);

        }

        public UserDTO GetUserByUsername(string username, UserInfoDTO userInfo)
        {
            bool includeSSOSecretKey =
                HasPermission(userInfo.EffectiveUser.ID.Value, PermissionTypeEnum.reseller_admin)
                || HasPermission(userInfo.EffectiveUser.ID.Value, PermissionTypeEnum.internal_admin)
            ;

            return getUser(null, username, null, userInfo, includeSSOSecretKey);

        }

        public UserDTO GetUserForEmailByUsername(string userName)
        {
            return getUser(null, userName, null, null, false);
        }


        public List<UserAccountAccessListItemDTO> GetUserAccessListByAccountID(long accountID, UserInfoDTO userInfo)
        {
            List<UserAccountAccessListItemDTO> results = new List<UserAccountAccessListItemDTO>();

            if (!HasPermission(userInfo.EffectiveUser.ID.Value, accountID, PermissionTypeEnum.accounts_admin))
            {
                return new List<UserAccountAccessListItemDTO>();
            }

            bool isInternalAdmin = HasPermission(userInfo.EffectiveUser.ID.Value, accountID, PermissionTypeEnum.internal_admin);

            ITimezoneProvider tz = ProviderFactory.TimeZones;
            TimeZoneInfo timeZone = tz.GetTimezoneForUserID(userInfo.EffectiveUser.ID.Value);
            using (MainEntities db = ContextFactory.Main)
            {
                if (db.Connection.State != System.Data.ConnectionState.Open)
                    db.Connection.Open();

                using (DbCommand cmd = db.CreateStoreCommand(
                   "spGetAccountAcccessList",
                    System.Data.CommandType.StoredProcedure,
                    new SqlParameter("@accountID", accountID)
               ))
                {
                    using (DbDataReader reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            RoleTypeEnum role = (RoleTypeEnum)Util.GetReaderValue<long>(reader, "RoleTypeID", 0);
                            if (role != RoleTypeEnum.basic_internal_admin || isInternalAdmin)
                            {
                                results.Add(new UserAccountAccessListItemDTO()
                                {
                                    RoleName = Util.GetReaderValue<string>(reader, "RoleName", ""),
                                    UserID = Util.GetReaderValue<long>(reader, "UserID", 0),
                                    RoleID = Util.GetReaderValue<long>(reader, "RoleID", 0),
                                    UserEmailAddress = Util.GetReaderValue<string>(reader, "EmailAddress", ""),
                                    UserFirstName = Util.GetReaderValue<string>(reader, "FirstName", ""),
                                    UserLastName = Util.GetReaderValue<string>(reader, "LastName", ""),
                                    Username = Util.GetReaderValue<string>(reader, "Username", ""),
                                    RoleType = role,
                                    InheritedFromAccountID = Util.GetReaderValue<long?>(reader, "InheritedFromAccountID", 0),
                                    InheritedFromAccountName = Util.GetReaderValue<string>(reader, "InheritedFromAccountName", null),
                                    AccountID = accountID,
                                    MemberOfAccountID = Util.GetReaderValue<long>(reader, "MemberOfAccountID", 0),
                                    Cascading = Util.GetReaderValue<bool>(reader, "IncludeChildren", false)
                                });
                            }
                        }
                    }
                }
            }

            List<long> aids = AccountsWithPermission(userInfo.EffectiveUser.ID.Value, PermissionTypeEnum.users_view);
            results = (from r in results where aids.Contains(r.MemberOfAccountID) select r).ToList();

            return results;
        }

        public CreateUserResultDTO SaveUser(UserDTO user, UserInfoDTO userInfo, bool skipApplyRoles, bool skipReloadTree)
        {
            ITimezoneProvider tz = ProviderFactory.TimeZones;
            TimeZoneInfo timeZone = tz.GetTimezoneForUserID(userInfo.EffectiveUser.ID.Value);

            bool includeSSOSecretKey =
                HasPermission(userInfo.EffectiveUser.ID.Value, PermissionTypeEnum.reseller_admin)
                || HasPermission(userInfo.EffectiveUser.ID.Value, PermissionTypeEnum.internal_admin)
            ;

            if (user.ID == 0)
                user.ID = null;

            if (string.IsNullOrWhiteSpace(user.Username))
            {
                return new CreateUserResultDTO() { Outcome = CreateUserOutcome.UsernameRequired };
            }

            if (string.IsNullOrWhiteSpace(user.EmailAddress))
            {
                return new CreateUserResultDTO() { Outcome = CreateUserOutcome.EmailAddressRequired };
            }

            UserDTO dto = GetUserByUsername(user.Username, userInfo);

            if ((dto != null && !user.ID.HasValue) || (dto != null && user.ID.HasValue && user.ID.Value != dto.ID))
            {
                return new CreateUserResultDTO() { Outcome = CreateUserOutcome.UsernameAlreadyExists };
            }

            if (!Security.EmailIsValid(user.EmailAddress))
            {
                return new CreateUserResultDTO() { Outcome = CreateUserOutcome.InvalidEmailAddress };
            }

            dto = GetUserByEmailAddress(user.EmailAddress, userInfo);
            if ((dto != null && !user.ID.HasValue) || (dto != null && user.ID.HasValue && user.ID.Value != dto.ID))
            {
                return new CreateUserResultDTO() { Outcome = CreateUserOutcome.EmailAddressAlreadyExists };
            }

            User userObj = null;

            using (MainEntities db = ContextFactory.Main)
            {
                bool newUser = false;
                if (!user.ID.HasValue)
                {
                    newUser = true;
                    userObj = new User();
                    userObj.DateCreated = DateTime.UtcNow;
                    userObj.DateModified = DateTime.UtcNow;
                    userObj.SSOSecretKey = Guid.NewGuid();
                }
                else
                {
                    userObj = (from u in db.Users where u.ID.Equals(user.ID.Value) select u).FirstOrDefault();

                    lock (_permissionCache)
                    {
                        if (_permissionCache.ContainsKey(userObj.ID))
                            _permissionCache.Remove(userObj.ID);
                    }
                }

                userObj.Username = user.Username.Trim();
                if (!string.IsNullOrEmpty(user.Password))
                    userObj.Password = Security.GetPasswordHash(user.Password.Trim());
                if (!string.IsNullOrEmpty(user.EmailAddress))
                    userObj.EmailAddress = user.EmailAddress.Trim();

                userObj.FirstName = user.FirstName;
                userObj.LastName = user.LastName;
                userObj.RootAccountID = user.RootAccountID;
                userObj.MemberOfAccountID = user.MemberOfAccountID;
                userObj.TimezoneID = user.TimezoneID;

                if (userObj.EntityState == System.Data.EntityState.Detached)
                {
                    userObj.InvalidLoginAttempts = 0;
                    userObj.AccountLocked = false;

                    db.Users.AddObject(userObj);
                }

                db.SaveChanges();

                if (!skipApplyRoles) db.spApplyRoles(userObj.ID);
                if (!skipReloadTree) reloadTree();

                //queue a notification subscription check
                ProviderFactory.Notifications.AddSubscriptionCheck(userObj.ID, null);

                ProviderFactory.TimeZones.ClearTimezoneCacheForUser(userObj.ID);

                string adesc;
                if (newUser)
                {
                    adesc = string.Format("New user [{0}] was created", userObj.Username);


                }
                else
                {
                    adesc = string.Format("User [{0}] record was updated", userObj.Username);
                }

                Auditor
                    .New(AuditLevelEnum.Information, AuditTypeEnum.UserActivity, userInfo, "")
                    .SetAuditDataObject(
                        AuditUserActivity
                        .New(userInfo, newUser ? AuditUserActivityTypeEnum.CreatedNewUser : AuditUserActivityTypeEnum.ChangedExistingUser)
                            .SetUserIDActedUpon(userObj.ID)
                            .SetDescription(adesc)
                    )
                    .Save(ProviderFactory.Logging)
                ;

                if (newUser)
                {
                    Auditor
                        .New(AuditLevelEnum.Information, AuditTypeEnum.UserActivity, userInfo, "")
                        .SetAuditDataObject(
                            AuditUserActivity
                                .New(userInfo, AuditUserActivityTypeEnum.AccountMembershipAdded)
                                .SetUserIDActedUpon(userObj.ID)
                                .SetAccountID(userObj.MemberOfAccountID)
                                .SetDescription("User [{0}] is now a member of account ID [{1}]", user.Username, userObj.MemberOfAccountID)
                        )
                        .Save(ProviderFactory.Logging)
                    ;
                }

                if (newUser)
                {
                    //Send New USer Registration email to User

                    //ProviderFactory.Platform.SendNewUserEmail(userObj.ID, userInfo);
                }

                return new CreateUserResultDTO()
                {
                    Outcome = CreateUserOutcome.Success,
                    User = getUser(userObj.ID, null, null, userInfo, includeSSOSecretKey)
                };
            }

        }

        public ValidateUserOutcome ChangeUserPassword(UserInfoDTO userInfo, string password, string currentPassword)
        {

            ValidateUserOutcome outcome = ValidateUserOutcome.UserNotFound;

            UserDTO user = ProviderFactory.Security.GetUserByID(userInfo.EffectiveUser.ID.Value, userInfo);
            if (user != null)
            {
                bool validCurrentPassword = currentPassword == null;
                if (!validCurrentPassword)
                {
                    ValidateUserResultDTO validateUserResultDto = ProviderFactory.Security.ValidateUser(user.Username, currentPassword);
                    if (validateUserResultDto.Outcome == ValidateUserOutcome.Success)
                        validCurrentPassword = true;
                }

                if (validCurrentPassword && !string.IsNullOrEmpty(password.Trim()))
                {
                    using (MainEntities db = ContextFactory.Main)
                    {
                        User userObj = db.Users.Where(u => u.ID == user.ID).FirstOrDefault();
                        if (userObj != null)
                        {
                            userObj.Password = Security.GetPasswordHash(password.Trim());
                            db.SaveChanges();

                            Auditor
                                .New(AuditLevelEnum.Information, AuditTypeEnum.UserActivity, userInfo, "")
                                .SetAuditDataObject(
                                    AuditUserActivity
                                        .New(userInfo, AuditUserActivityTypeEnum.ChangedPassword)
                                        .SetUserIDActedUpon(userInfo.EffectiveUser.ID)
                                        .SetDescription("User [{0}] changed their password", user.Username)
                                )
                                .Save(ProviderFactory.Logging)
                            ;

                            outcome = ValidateUserOutcome.Success;
                        }
                    }
                }
                else
                    outcome = ValidateUserOutcome.InvalidPassword;
            }

            return outcome;
        }


        public void UnlockUserAccount(UserInfoDTO userInfo, long userID)
        {
            //todo: check for permission

            using (MainEntities db = ContextFactory.Main)
            {
                User user = (from u in db.Users where u.ID == userID select u).FirstOrDefault();
                if (user != null)
                {
                    user.AccountLocked = false;
                    user.InvalidLoginAttempts = 0;
                    db.SaveChanges();
                }
            }
        }


        private static Dictionary<long, PermissionCacheItem> _permissionCache = new Dictionary<long, PermissionCacheItem>();

        public void SaveUserSetting(UserInfoDTO uInfo, string key, string value)
        {
            lock (_permissionCache)
            {
                if (_permissionCache.ContainsKey(uInfo.EffectiveUser.ID.Value))
                    _permissionCache.Remove(uInfo.EffectiveUser.ID.Value);
            }

            key = key.Trim().ToLower();
            if (string.IsNullOrWhiteSpace(value))
            {
                value = null;
            }
            using (MainEntities db = ContextFactory.Main)
            {
                db.spSaveUserSetting(uInfo.EffectiveUser.ID.Value, key, value);
            }
        }

        public string GetUserSetting(UserInfoDTO uInfo, string settingKey)
        {
            lock (_permissionCache)
            {
                PermissionCacheItem pci;
                if (!_permissionCache.TryGetValue(uInfo.EffectiveUser.ID.Value, out pci))
                {
                    ValidateUser(uInfo);
                }

                if (_permissionCache.TryGetValue(uInfo.EffectiveUser.ID.Value, out pci))
                {
                    Dictionary<string, string> settings = pci.Settings;

                    settingKey = settingKey.Trim().ToLower();
                    if (settings.ContainsKey(settingKey))
                        return settings[settingKey];
                }
            }
            return null;
        }

        public string GetUserSetting(UserInfoDTO userInfo, string settingKey, string defaultValue)
        {

            string val = GetUserSetting(userInfo, settingKey);
            if (val == null)
                return defaultValue;
            else
                return val;
        }

        public T GetUserSetting<T>(UserInfoDTO userInfo, string settingKey, T defaultValue)
        {
            settingKey = settingKey.Trim().ToLower();
            var setVal = GetUserSetting(userInfo, settingKey);
            if (string.IsNullOrEmpty(setVal))
                return defaultValue;
            return (T)Convert.ChangeType((object)setVal, typeof(T));
        }

        public T GetUserSetting<T>(UserInfoDTO userInfo, string settingKey)
        {

            return (T)Convert.ChangeType((object)GetUserSetting(userInfo, settingKey), typeof(T));
        }

        public ChangeUsersMemberAccountResultDTO ChangeUserMemberOfAccount(UserInfoDTO userInfo, long userID, long newMemberOfAccountID)
        {
            // get the role that the use has at their current memer account

            using (MainEntities db = ContextFactory.Main)
            {
                long? roleTypeID = (
                    from u in db.Users
                    join au in db.Accounts_Users on u.ID equals au.UserID
                    join r in db.Roles on au.ID equals r.Account_UserID
                    where au.AccountID == u.MemberOfAccountID
                    select r.RoleTypeID
                ).FirstOrDefault();

                if (!roleTypeID.HasValue || roleTypeID.Value == 0)
                {
                    roleTypeID = (long)RoleTypeEnum.basic_readonly;
                }

                return ChangeUserMemberOfAccount(userInfo, userID, newMemberOfAccountID, roleTypeID.Value, false);
            }

        }

        public ChangeUsersMemberAccountResultDTO ChangeUserMemberOfAccount(UserInfoDTO userInfo, long userID, long newMemberOfAccountID, long roleTypeID, bool cascadeRole)
        {
            ChangeUsersMemberAccountResultDTO result = new ChangeUsersMemberAccountResultDTO() { Success = false };

            if (!HasPermission(userInfo.EffectiveUser.ID.GetValueOrDefault(), newMemberOfAccountID, PermissionTypeEnum.users_admin))
            {
                result.Problems.Add("Insufficient permissions to move user.");
                return result;
            }

            using (MainEntities db = ContextFactory.Main)
            {
                User user = (from u in db.Users where u.ID == userID select u).FirstOrDefault();

                if (!HasPermission(userInfo.EffectiveUser.ID.GetValueOrDefault(), user.MemberOfAccountID, PermissionTypeEnum.users_admin))
                {
                    result.Problems.Add("Insufficient permissions to move user.");
                    return result;
                }

                removeRole(userInfo, userID, user.MemberOfAccountID);

                long oldAccountID = user.MemberOfAccountID;
                string oldAccountName = GetAccountNameByID(oldAccountID);
                string newAccountName = GetAccountNameByID(newMemberOfAccountID);

                user.MemberOfAccountID = newMemberOfAccountID;
                user.RootAccountID = newMemberOfAccountID;
                db.SaveChanges();

                Auditor
                    .New(AuditLevelEnum.Information, AuditTypeEnum.UserActivity, userInfo, "")
                    .SetAuditDataObject(
                        AuditUserActivity
                            .New(userInfo, AuditUserActivityTypeEnum.AccountMembershipRemoved)
                            .SetUserIDActedUpon(userID)
                            .SetRoleTypeID(roleTypeID)
                            .SetAccountID(oldAccountID)
                            .SetDescription("User [{0}] is no longer a member of account [{1}]", user.Username, oldAccountName)
                    )
                    .Save(ProviderFactory.Logging)
                ;
                Auditor
                    .New(AuditLevelEnum.Information, AuditTypeEnum.UserActivity, userInfo, "")
                    .SetAuditDataObject(
                        AuditUserActivity
                            .New(userInfo, AuditUserActivityTypeEnum.AccountMembershipAdded)
                            .SetUserIDActedUpon(userID)
                            .SetRoleTypeID(roleTypeID)
                            .SetAccountID(newMemberOfAccountID)
                            .SetDescription("User [{0}] is now a member of account [{1}]", user.Username, newAccountName)
                    )
                    .Save(ProviderFactory.Logging)
                ;

                AddRole(userInfo, user.ID, newMemberOfAccountID, roleTypeID, cascadeRole, false, false);
            }

            result.Success = true;
            return result;

        }

        public UserSearchResultDTO SearchUsers(UserSearchRequestDTO request)
        {
            ITimezoneProvider tz = ProviderFactory.TimeZones;
            TimeZoneInfo timeZone = tz.GetTimezoneForUserID(request.UserInfo.EffectiveUser.ID.Value);

            bool viewSSOKeys =
                HasPermission(request.UserInfo.EffectiveUser.ID.Value, PermissionTypeEnum.reseller_admin)
                || HasPermission(request.UserInfo.EffectiveUser.ID.Value, PermissionTypeEnum.internal_admin);
            ;

            using (MainEntities db = ContextFactory.Main)
            {
                if (db.Connection.State != System.Data.ConnectionState.Open)
                    db.Connection.Open();

                string search = "";
                if (!string.IsNullOrWhiteSpace(request.SearchString))
                {
                    string[] searchParts = request.SearchString.Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);
                    if (searchParts.Length > 0)
                    {
                        search = string.Join("|", searchParts);
                    }
                }

                using (DbCommand cmd = db.CreateStoreCommand(
                    "spSearchUsers3",
                     System.Data.CommandType.StoredProcedure,

                     new SqlParameter("@SearchStringParts", search),
                     new SqlParameter("@CallingUserID", request.UserInfo.EffectiveUser.ID.GetValueOrDefault()),

                     new SqlParameter("@MemberOfAccountID", request.MemberOfAccountID.GetValueOrDefault()),
                     new SqlParameter("@NotMemberOfAccountID", request.NotMemberOfAccountID.GetValueOrDefault()),
                     new SqlParameter("@UsersWithoutRoleInAccountID", request.UsersWithoutRoleInAccountID.GetValueOrDefault()),

                     new SqlParameter("@IncludeDeactivatedUsers", request.IncludeDeactivatedUsers),


                     new SqlParameter("@SortBy", request.SortBy),
                     new SqlParameter("@SortAscending", request.SortAscending),

                     new SqlParameter("@StartIndex", request.StartIndex),
                     new SqlParameter("@EndIndex", request.EndIndex)
                ))
                {
                    UserSearchResultDTO results = new UserSearchResultDTO()
                    {
                        StartIndex = request.StartIndex,
                        EndIndex = request.EndIndex,
                        SortBy = request.SortBy,
                        SortAscending = request.SortAscending
                    };

                    using (DbDataReader reader = cmd.ExecuteReader())
                    {
                        if (reader.Read())
                        {
                            //the total records
                            results.TotalRecords = reader.GetInt32(0);

                            if (reader.NextResult())
                            {
                                results.Items = db.Translate<UserSearchResultItemDTO>(reader).ToList();

                                foreach (UserSearchResultItemDTO item in results.Items)
                                {
                                    if (!viewSSOKeys)
                                    {
                                        item.SSOSecretKey = Guid.Empty;
                                    }

                                    item.CanEditMemberOfAccount = HasPermission(request.UserInfo.EffectiveUser.ID.Value, item.MemberOfAccountID, PermissionTypeEnum.accounts_admin);
                                    item.CanEditUserOnMemberOfAccount= HasPermission(request.UserInfo.EffectiveUser.ID.Value, item.MemberOfAccountID, PermissionTypeEnum.users_admin);
                                    
                                }
                            }
                        }
                    }

                    foreach (UserSearchResultItemDTO item in results.Items)
                    {
                        item.DateCreated = new SIDateTime(item.DateCreatedUTC, timeZone);
                    }

                    return results;
                }
            }

            return null;
        }

        public void SetUserActiveState(UserInfoDTO loggedInUser, long userID, bool active)
        {
            using (MainEntities db = ContextFactory.Main)
            {
                User user = (from u in db.Users where u.ID == userID select u).FirstOrDefault();

                // user doesn't exist...
                if (user == null) return;

                // already deactivated?
                if (user.AccountDeactivatedDate.HasValue && active == false) return;

                //already active?
                if (!user.AccountDeactivatedDate.HasValue && active == true) return;

                //logged in user does not have permission to admin users for the account the user belongs to
                if (!HasPermission(loggedInUser.EffectiveUser.ID.Value, user.MemberOfAccountID, PermissionTypeEnum.users_admin)) return;

                if (active)
                {
                    user.AccountDeactivatedDate = null;
                    user.Username = user.Username.Replace("_DEL_", "");
                    if (!string.IsNullOrWhiteSpace(user.EmailAddress))
                        user.EmailAddress = user.EmailAddress.Replace("_DEL_", "");
                    user.Password = Security.GetPasswordHash("Password$");
                }
                else
                {
                    user.Password = null;
                    user.Username = string.Format("{0}_DEL_", user.Username);
                    if (!string.IsNullOrWhiteSpace(user.EmailAddress))
                        user.EmailAddress = string.Format("{0}_DEL_", user.EmailAddress);
                    user.AccountDeactivatedDate = DateTime.UtcNow;
                }

                db.SaveChanges();
            }
        }

        public List<UserActivityDTO> GetUserActivityByUserID(long UserID, int NumberOfDaysBack, UserInfoDTO userInfo)
        {
            ITimezoneProvider tz = ProviderFactory.TimeZones;
            TimeZoneInfo timeZone = tz.GetTimezoneForUserID(userInfo.EffectiveUser.ID.Value);

            List<UserActivityDTO> listUserActivityDTO = new List<UserActivityDTO>();

            using (MainEntities db = ContextFactory.Main)
            {
                if (db.Connection.State != System.Data.ConnectionState.Open)
                    db.Connection.Open();

                using (DbCommand cmd3 = db.CreateStoreCommand(
                      "spGetUserActivityByUserID",
                      System.Data.CommandType.StoredProcedure,
                          new SqlParameter("@UserID", UserID),
                          new SqlParameter("@DaysBack", NumberOfDaysBack)
                      ))
                {
                    using (DbDataReader reader = cmd3.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            listUserActivityDTO.Add(new UserActivityDTO()
                            {
                                AccountName = Util.GetReaderValue<string>(reader, "AccountName", ""),
                                Activity = Util.GetReaderValue<string>(reader, "Activity", ""),
                                ActivityDate = new SIDateTime(Util.GetReaderValue<DateTime>(reader, "ActivityDate", DateTime.MinValue), timeZone)
                            });
                        }
                    }
                }
            }
            return listUserActivityDTO;
        }

        #endregion

        #region accounts

        private GetEntityDTO<AccountDTO, AccountOptionsDTO> getAccountByID(UserInfoDTO userInfo, long accountID, bool skipSecurityChecks)
        {
            ITimezoneProvider tz = ProviderFactory.TimeZones;
            TimeZoneInfo timeZone = tz.GetTimezoneForUserID(userInfo.EffectiveUser.ID.Value);

            GetEntityDTO<AccountDTO, AccountOptionsDTO> result = new DTO.GetEntityDTO<DTO.AccountDTO, AccountOptionsDTO>();

            bool hasAccountAdmin = HasPermission(userInfo.EffectiveUser.ID.Value, accountID, PermissionTypeEnum.accounts_admin);

            if (userInfo.EffectiveUser.ID.Value > 0 && !skipSecurityChecks)
            {
                if (!hasAccountAdmin)
                {
                    result.Problems.Add(Locale.Localize("system.nopermission"));
                    return result;
                }
            }

            using (MainEntities db = ContextFactory.Main)
            {
                if (db.Connection.State != System.Data.ConnectionState.Open)
                    db.Connection.Open();

                using
                    (DbCommand cmd = db.CreateStoreCommand(
                            "spGetAccount", System.Data.CommandType.StoredProcedure,
                            new SqlParameter("accountID", accountID)
                        )
                    )
                {
                    using (DbDataReader reader = cmd.ExecuteReader())
                    {
                        //Account account = db.Translate<Account>(reader).FirstOrDefault();

                        AccountDTO dto = null;
                        if (reader.Read())
                        {
                            dto = new AccountDTO()
                            {
                                ID = Util.GetReaderValue<long>(reader, "ID", 0),
                                AccountTypeID = Util.GetReaderValue<long>(reader, "AccountTypeID", 0),
                                ParentAccountID = Util.GetReaderValue<long?>(reader, "ParentAccountID", null),
                                ParentAccountName = Util.GetReaderValue<string>(reader, "ParentAccountName", null),
                                CRMSystemTypeID = Util.GetReaderValue<long?>(reader, "CRMSystemTypeID", null),
                                VerticalID = Util.GetReaderValue<long?>(reader, "VerticalID", null),

                                AccountDepth = Util.GetReaderValue<int>(reader, "AccountDepth", 0),

                                Name = Util.GetReaderValue<string>(reader, "Name", null),
                                DisplayName = Util.GetReaderValue<string>(reader, "DisplayName", null),

                                PostalCode = Util.GetReaderValue<string>(reader, "ZipCode", null),

                                CountryID = Util.GetReaderValue<long?>(reader, "CountryID", null),
                                CountryName = Util.GetReaderValue<string>(reader, "CountryName", null),

                                StateID = Util.GetReaderValue<long?>(reader, "StateID", null),
                                StateName = Util.GetReaderValue<string>(reader, "StateName", null),
                                StateAbbreviation = Util.GetReaderValue<string>(reader, "StateAbbreviation", null),

                                CityID = Util.GetReaderValue<long?>(reader, "CityID", null),
                                CityName = Util.GetReaderValue<string>(reader, "CityName", null),

                                StreetAddress1 = Util.GetReaderValue<string>(reader, "Street1", null),
                                StreetAddress2 = Util.GetReaderValue<string>(reader, "Street2", null),

                                PhoneNumber = Util.GetReaderValue<string>(reader, "Phone", null),

                                EdmundsDealershipID = Util.GetReaderValue<long?>(reader, "EdmundsDealershipID", null),

                                LeadsEmailAddress = Util.GetReaderValue<string>(reader, "LeadsEmailAddress", null),
                                EmailAddress = Util.GetReaderValue<string>(reader, "EmailAddress", null),
                                WebsiteURL = Util.GetReaderValue<string>(reader, "WebsiteURL", null),

                                OriginSystemIdentifier = Util.GetReaderValue<string>(reader, "OriginSystemIdentifier", null),

                                PostsRequireApproval = Util.GetReaderValue<bool>(reader, "PostsRequireApproval", false),

                                TimezoneID = Util.GetReaderValue<long>(reader, "TimezoneID", 0),

                                DateCreated = new SIDateTime(Util.GetReaderValue<DateTime>(reader, "DateCreated", DateTime.MinValue), timeZone),
                                DateModified = new SIDateTime(Util.GetReaderValue<DateTime>(reader, "DateModified", DateTime.MinValue), timeZone)
                            };
                        }


                        if (dto != null)
                        {
                            dto.ResellerForThisAccount = GetResellerByAccountID(userInfo, dto.ID.Value, timeZone);
                            if (HasPermission(userInfo.EffectiveUser.ID.Value, dto.ID.Value, PermissionTypeEnum.internal_admin))
                            {
                                dto.IsDemoAccount = Util.GetReaderValue<bool?>(reader, "IsDemoAccount", null);
                            }
                        }

                        if (dto != null && reader.NextResult())
                        {

                            List<SI.DAL.FranchiseType> ftypes = db.Translate<SI.DAL.FranchiseType>(reader).ToList();
                            dto.FranchiseTypes = (from f in ftypes select f.ID).ToArray();

                            result.Entity = dto;
                        }
                    }
                }
            }

            bool hasAccountView = HasPermission(userInfo.EffectiveUser.ID.Value, result.Entity.ID.Value, PermissionTypeEnum.accounts_view);
            bool hasProductsView = HasPermission(userInfo.EffectiveUser.ID.Value, result.Entity.ID.Value, PermissionTypeEnum.products_view);
            bool hasProductsAdmin = HasPermission(userInfo.EffectiveUser.ID.Value, result.Entity.ID.Value, PermissionTypeEnum.products_admin);
            bool hasResellerAdmin = HasPermission(userInfo.EffectiveUser.ID.Value, result.Entity.ID.Value, PermissionTypeEnum.reseller_admin);
            bool hasSocialAdmin = HasPermission(userInfo.EffectiveUser.ID.Value, result.Entity.ID.Value, PermissionTypeEnum.social_admin);
            bool hasUserAdmin = HasPermission(userInfo.EffectiveUser.ID.Value, result.Entity.ID.Value, PermissionTypeEnum.users_admin);
            bool hasUserView = HasPermission(userInfo.EffectiveUser.ID.Value, result.Entity.ID.Value, PermissionTypeEnum.users_view);
            bool hasInternalAdmin = HasPermission(userInfo.EffectiveUser.ID.Value, result.Entity.ID.Value, PermissionTypeEnum.internal_admin);


            result.Options = new DTO.AccountOptionsDTO()
            {
                //ViewChildren = HasPermission(userInfo.EffectiveUser.ID.Value, result.Entity.ID.Value, PermissionTypeEnum.accounts_view),
                ViewChildren = hasInternalAdmin,
                AddChildren = hasAccountAdmin,

                //ViewProducts = hasProductsView,
                ViewProducts = hasInternalAdmin,
                ConfigProducts = true,
                AddProducts = hasInternalAdmin,
                RemoveProducts = hasInternalAdmin,

                AddUsers = hasInternalAdmin,
                EditUsers = hasInternalAdmin,
                ViewUsers = hasInternalAdmin,

                //ViewSocialConfigTab = hasSocialAdmin,
                ViewSocialConfigTab = true,
                ViewDemoCheckbox = hasInternalAdmin,

                ViewExternalID = hasAccountAdmin,

                ViewAccountSettings = hasInternalAdmin,
                ViewAlerts = hasInternalAdmin,
                ViewSchedule = hasInternalAdmin,
                ViewInventory = hasInternalAdmin,
                ViewCompetitors = hasInternalAdmin,

                ViewSSOSecretKey = hasResellerAdmin || hasInternalAdmin,

                RemoveFromParent = false
            };

            if (result.Entity.ParentAccountID.HasValue)
            {
                result.Options.RemoveFromParent = hasAccountAdmin;
            }

            return result;
        }

        public GetEntityDTO<AccountDTO, AccountOptionsDTO> GetAccountByID(UserInfoDTO userInfo, long accountID)
        {
            return getAccountByID(userInfo, accountID, false);
        }

        public GetEntityDTO<AccountDTO, AccountOptionsDTO> GetAccountByName(UserInfoDTO userInfo, string accountName)
        {
            ITimezoneProvider tz = ProviderFactory.TimeZones;
            TimeZoneInfo timeZone = tz.GetTimezoneForUserID(userInfo.EffectiveUser.ID.Value);

            accountName = accountName.Trim();

            using (MainEntities db = ContextFactory.Main)
            {
                long? accountID = (
                    from a in db.Accounts
                    where a.Name.Equals(accountName, StringComparison.InvariantCultureIgnoreCase)
                    select a.ID
                ).FirstOrDefault();

                if (accountID.HasValue && accountID.Value > 0)
                {
                    return GetAccountByID(userInfo, accountID.Value);
                }

                return null;
            }
        }

        public GetEntityDTO<AccountDTO, AccountOptionsDTO> GetAccountByNameForImport(UserInfoDTO userInfo, string accountName)
        {
            ITimezoneProvider tz = ProviderFactory.TimeZones;
            TimeZoneInfo timeZone = tz.GetTimezoneForUserID(userInfo.EffectiveUser.ID.Value);

            accountName = accountName.Trim();

            using (MainEntities db = ContextFactory.Main)
            {
                var accountIDs = (
                    from a in db.Accounts
                    where a.Name.Equals(accountName, StringComparison.InvariantCultureIgnoreCase)
                            || a.DisplayName.Equals(accountName, StringComparison.InvariantCultureIgnoreCase)
                    select a
                ).ToList();

                if (accountIDs != null)
                {
                    if (accountIDs.Count == 0)
                    {
                        return null;
                    }
                    else if (accountIDs.Count == 1)
                    {
                        long _accountID = (from a in accountIDs
                                           select a.ID).FirstOrDefault();

                        return GetAccountByID(userInfo, _accountID);
                    }
                    else
                    {
                        long? _accountID = (from a in accountIDs
                                            select a.ID).FirstOrDefault();

                        if (_accountID.HasValue && _accountID.Value > 0)
                        {
                            return GetAccountByID(userInfo, _accountID.Value);
                        }
                        else
                        {
                            return null;
                        }
                    }
                }
                else
                {
                    return null;
                }

                //long? accountID = (
                //    from a in db.Accounts
                //    where a.Name.Equals(accountName, StringComparison.InvariantCultureIgnoreCase)
                //            || a.DisplayName.Equals(accountName, StringComparison.InvariantCultureIgnoreCase)
                //    select a.ID
                //).FirstOrDefault();

                //if (accountID.HasValue && accountID.Value > 0)
                //{
                //    return GetAccountByID(userInfo, accountID.Value);
                //}

                return null;
            }
        }

        public DTO.GetEntityDTO<DTO.AccountDTO, AccountOptionsDTO> GetNewAccount(UserInfoDTO userInfo, long? parentAccountID)
        {
            throw new NotImplementedException();
        }

        public SaveEntityDTO<AccountDTO> Save(SaveEntityDTO<AccountDTO> dto)
        {
            List<string> probs = new List<string>();

            if (dto.Entity.ID.HasValue)
            {
                if (!HasPermission(dto.UserInfo.EffectiveUser.ID.Value, dto.Entity.ID.Value, PermissionTypeEnum.accounts_admin))
                {
                    probs.Add(Locale.Localize("system.nopermission"));
                }
            }
            else
            {
                if (!HasPermission(dto.UserInfo.EffectiveUser.ID.Value, dto.Entity.ParentAccountID.Value, PermissionTypeEnum.accounts_admin))
                {
                    probs.Add(Locale.Localize("system.nopermission"));
                }
            }

            if (!dto.Entity.ParentAccountID.HasValue)
            {
                probs.Add("You cannot add an account without a parent account.");
            }

            if (probs.Any())
                return new DTO.SaveEntityDTO<DTO.AccountDTO>(dto.UserInfo) { Problems = probs };

            AccountType aType = null;
            if (dto.Entity.AccountTypeID > 0)
            {
                aType = GetAccountTypeByID(dto.Entity.AccountTypeID);
            }
            if (aType == null)
            {
                aType = GetAccountType(dto.Entity.Type);
            }

            bool newAccount = (!dto.Entity.ID.HasValue) || dto.Entity.ID.Value == 0;

            using (MainEntities db = ContextFactory.Main)
            {
                Account acc = null;

                AccountTypeEnum accountType = (AccountTypeEnum)dto.Entity.AccountTypeID;

                //bool addressRequired = (accountType == AccountTypeEnum.Company);
                bool addressValid = true;

                // a valid address has:
                // country, state, city, zip, street1

                if (accountType == AccountTypeEnum.Company && string.IsNullOrWhiteSpace(dto.Entity.StreetAddress1))
                {
                    probs.Add("You must enter a street address.");
                    addressValid = false;
                }

                if (!dto.Entity.CityID.HasValue && string.IsNullOrWhiteSpace(dto.Entity.CityName))
                {
                    probs.Add("City is required.");
                    addressValid = false;
                }
                if (string.IsNullOrWhiteSpace(dto.Entity.PostalCode))
                {
                    probs.Add("PostalCode is required.");
                    addressValid = false;
                }

                if (!dto.Entity.CountryID.HasValue)
                {
                    probs.Add("Country is required.");
                    addressValid = false;
                }
                if (!dto.Entity.StateID.HasValue && string.IsNullOrWhiteSpace(dto.Entity.StateAbbreviation))
                {
                    probs.Add("State is required.");
                    addressValid = false;
                }

                if (string.IsNullOrWhiteSpace(dto.Entity.PostalCode))
                {
                    probs.Add("Postal code is required.");
                    addressValid = false;
                }

                Country country = null;
                State state = null;
                ZipCode zip = null;
                City city = null;

                if (addressValid)
                {
                    if (!dto.Entity.StateID.HasValue)
                    {
                        //can we look up the state?
                        if (!string.IsNullOrWhiteSpace(dto.Entity.StateAbbreviation) && dto.Entity.CountryID.HasValue)
                        {
                            List<StateDTO> states = ProviderFactory.Lists.GetStatesForCountry(dto.Entity.CountryID.Value);
                            StateDTO theState = (from s in states where s.Abbreviation.Trim().ToLower() == dto.Entity.StateAbbreviation.Trim().ToLower() select s).FirstOrDefault();
                            if (theState != null)
                                dto.Entity.StateID = theState.ID;
                            else
                            {
                                probs.Add("The state could not be found.");
                                addressValid = false;
                            }
                        }
                    }

                    //Country
                    if (dto.Entity.CountryID.HasValue)
                    {
                        country = (from c in db.Countries where c.ID.Equals(dto.Entity.CountryID.Value) select c).FirstOrDefault();
                    }

                    if (country == null)
                    {
                        probs.Add("Invalid country supplied,");
                        addressValid = false;
                    }

                    //State
                    if (dto.Entity.StateID.HasValue)
                    {
                        state = (from s in db.States where s.ID.Equals(dto.Entity.StateID.Value) select s).FirstOrDefault();
                    }

                    if (state == null && (dto.Entity.AccountTypeID != (long)AccountTypeEnum.Grouping))
                    {
                        probs.Add("Invalid state supplied,");
                    }

                    // zip code
                    if (!string.IsNullOrWhiteSpace(dto.Entity.PostalCode))
                    {
                        zip = (
                            from z in db.ZipCodes
                            where z.ZipCode1.Trim().ToLower() == dto.Entity.PostalCode.Trim().ToLower()
                            && z.StateID == state.ID
                            select z
                        ).FirstOrDefault();

                        if (zip == null && state != null)
                        {
                            zip = new ZipCode()
                            {
                                ZipCode1 = dto.Entity.PostalCode.Trim(),
                                DateModified = DateTime.UtcNow,
                                DateCreated = DateTime.UtcNow,
                                StateID = state.ID
                            };
                            db.ZipCodes.AddObject(zip);
                            db.SaveChanges();
                        }
                    }

                    if (zip == null)
                    {
                        probs.Add("Postal code could not be found.");
                        addressValid = false;
                    }

                    // city
                    if (dto.Entity.CityID.HasValue)
                    {
                        city = (from c in db.Cities where c.ID.Equals(dto.Entity.CityID.Value) select c).FirstOrDefault();
                    }

                    if (city == null)
                    {
                        //try to find by name
                        IEnumerable<City> cities = (
                            from c in db.Cities
                            where c.Name.Trim().ToLower() == dto.Entity.CityName.Trim().ToLower()
                            && c.StateID.Equals(state.ID)
                            select c
                        );
                        if (cities.Count() == 1)
                        {
                            city = cities.First();
                        }
                        else if (cities.Count() > 1)
                        {
                            if (zip != null)
                            {
                                city = (
                                    from c in cities
                                    where c.ZipCodeID.HasValue
                                    && c.ZipCodeID.Value == zip.ID
                                    select c
                                ).FirstOrDefault();
                            }
                        }
                        if (city == null && !string.IsNullOrWhiteSpace(dto.Entity.CityName))
                        {
                            //add the city
                            city = new City()
                            {
                                StateID = state.ID,
                                Name = dto.Entity.CityName.Trim(),
                                ZipCodeID = zip != null ? zip.ID : (long?)null
                            };
                            db.Cities.AddObject(city);
                            db.SaveChanges();
                        }

                        if (city == null)
                        {
                            probs.Add("Could not find or add city.");
                            addressValid = false;
                        }
                    }

                }

                if (probs.Any() || !addressValid)
                    return new DTO.SaveEntityDTO<DTO.AccountDTO>(dto.UserInfo) { Problems = probs };

                if (!dto.Entity.ID.HasValue)
                {
                    GetEntityDTO<AccountDTO> dto2 = GetAccountByName(dto.UserInfo, dto.Entity.Name);
                    if (dto2 != null)
                        probs.Add(Locale.Localize("accountnameexists"));

                    acc = new Account() { DateCreated = DateTime.UtcNow, ParentAccountID = dto.UserInfo.MemberOfPrimaryResellerID };
                    acc.Name = dto.Entity.Name;
                    acc.DateCreated = DateTime.UtcNow;
                    acc.DateModified = acc.DateCreated;
                    acc.TimezoneID = 15;// dto.UserInfo.EffectiveUser.TimezoneID.GetValueOrDefault(); //TODO TEMP FIX
                    acc.AccountTypeID = dto.Entity.AccountTypeID;

                    db.Accounts.AddObject(acc);
                }
                else
                {
                    Account acheck = (from a in db.Accounts where a.ID != dto.Entity.ID.Value && a.Name.Equals(dto.Entity.Name, StringComparison.InvariantCultureIgnoreCase) select a).FirstOrDefault();
                    if (acheck != null)
                    {
                        probs.Add(Locale.Localize("accountnameexists"));
                    }
                    else
                    {
                        acc = (from a in db.Accounts where a.ID == dto.Entity.ID.Value select a).FirstOrDefault();
                    }
                }

                Address address = null;
                if (acc.BillingAddressID.HasValue)
                {
                    address = (from a in db.Addresses where a.ID.Equals(acc.BillingAddressID.Value) select a).FirstOrDefault();
                }

                // save the phone
                Phone phone = null;
                if (acc.BillingPhoneID.HasValue)
                {
                    //phone = db.Phones.Where(p => p.ID == acc.BillingPhoneID).FirstOrDefault();
                    phone = (from p in db.Phones where p.ID == acc.BillingPhoneID select p).FirstOrDefault();
                }

                if (string.IsNullOrWhiteSpace(dto.Entity.PhoneNumber))
                {
                    if (phone != null)
                    {
                        db.Phones.DeleteObject(phone);
                        phone = null;
                    }
                }
                else
                {
                    if (phone != null)
                    {
                        phone.Number = dto.Entity.PhoneNumber;
                    }
                    else
                    {
                        phone = new Phone()
                        {
                            PhoneTypeID = 1,
                            DateCreated = DateTime.UtcNow,
                            Number = dto.Entity.PhoneNumber
                        };
                        db.Phones.AddObject(phone);
                    }
                }

                db.SaveChanges();
                db.spApplyRoles(null);
                reloadTree();

                if (address != null && !addressValid)
                {
                    acc.BillingAddressID = null;
                }

                //must have address check if need to create
                if (address == null)
                {
                    address = new Address()
                    {
                        CityID = city.ID,
                        Street1 = dto.Entity.StreetAddress1.IsNullOptional(""),
                        Street2 = dto.Entity.StreetAddress2
                    };
                    db.Addresses.AddObject(address);
                    db.SaveChanges();
                }
                else
                {
                    address.CityID = city.ID;
                    address.Street1 = dto.Entity.StreetAddress1.IsNullOptional("");
                    address.Street2 = dto.Entity.StreetAddress2;
                }
                acc.BillingAddressID = address.ID;

                // set the account objects

                acc.BillingPhoneID = (phone == null) ? (long?)null : phone.ID;
                acc.ParentAccountID = dto.Entity.ParentAccountID;
                acc.AccountTypeID = aType.ID;

                acc.PostsRequireApproval = dto.Entity.PostsRequireApproval;

                acc.CRMSystemTypeID = dto.Entity.CRMSystemTypeID;

                acc.Name = dto.Entity.Name;

                if (string.IsNullOrWhiteSpace(dto.Entity.DisplayName))
                    dto.Entity.DisplayName = null;

                acc.DisplayName = dto.Entity.DisplayName;

                acc.LeadsEmailAddress = dto.Entity.LeadsEmailAddress;
                acc.EmailAddress = dto.Entity.EmailAddress;
                acc.WebsiteURL = dto.Entity.WebsiteURL;

                if (HasPermission(dto.UserInfo.EffectiveUser.ID.Value, acc.ID, PermissionTypeEnum.accounts_admin))
                {
                    acc.OriginSystemIdentifier = dto.Entity.OriginSystemIdentifier;
                }
                if (HasPermission(dto.UserInfo.EffectiveUser.ID.Value, acc.ID, PermissionTypeEnum.internal_admin))
                {
                    acc.IsDemoAccount = dto.Entity.IsDemoAccount;
                }

                acc.EdmundsDealershipID = dto.Entity.EdmundsDealershipID;

                bool clearTimeCache = (dto.Entity.ID.HasValue && acc.TimezoneID != dto.Entity.TimezoneID);
                acc.TimezoneID = dto.Entity.TimezoneID;
                acc.DateModified = DateTime.UtcNow;

                acc.VerticalID = dto.Entity.VerticalID;
                //fix up the franchise types

                foreach (Accounts_FranchiseTypes ft in (from x in db.Accounts_FranchiseTypes where x.AccountID.Equals(acc.ID) select x))
                {
                    db.Accounts_FranchiseTypes.DeleteObject(ft);
                }
                if (dto.Entity.FranchiseTypes != null)
                {
                    foreach (long ftype in dto.Entity.FranchiseTypes)
                    {
                        Accounts_FranchiseTypes ft = new Accounts_FranchiseTypes()
                            {
                                AccountID = acc.ID,
                                FranchiseTypeID = ftype
                            };
                        db.Accounts_FranchiseTypes.AddObject(ft);
                    }
                }

                db.SaveChanges();

                if (clearTimeCache)
                {
                    ProviderFactory.TimeZones.ClearTimezoneCache();
                }

                string adesc = "";
                if (newAccount)
                {
                    adesc = string.Format("New account [{0}] was created", string.IsNullOrWhiteSpace(acc.DisplayName) ? acc.Name : acc.DisplayName);
                }
                else
                {
                    adesc = string.Format("Account changes saved for [{0}]", string.IsNullOrWhiteSpace(acc.DisplayName) ? acc.Name : acc.DisplayName);
                }

                Auditor
                    .New(AuditLevelEnum.Information, AuditTypeEnum.UserActivity, dto.UserInfo, "")
                    .SetAuditDataObject(
                        AuditUserActivity
                        .New(dto.UserInfo, newAccount ? AuditUserActivityTypeEnum.CreatedNewAccount : AuditUserActivityTypeEnum.ChangedExistingAccount)
                            .SetAccountID(acc.ID)
                            .SetDescription(adesc)
                    )
                    .Save(ProviderFactory.Logging)
                ;

                if (newAccount)
                {
                    Auditor
                       .New(AuditLevelEnum.Information, AuditTypeEnum.UserActivity, dto.UserInfo, "")
                       .SetAuditDataObject(
                           AuditUserActivity
                           .New(dto.UserInfo, AuditUserActivityTypeEnum.SubAccountAdded)
                               .SetAccountID(acc.ParentAccountID)
                               .SetObjectID(acc.ID)
                       )
                       .Save(ProviderFactory.Logging)
                   ;
                }

                //queue a notification subscription check
                ProviderFactory.Notifications.AddSubscriptionCheck(null, acc.ID);

                //queue the reporting updates                
                if (acc.ParentAccountID.HasValue)
                    queueAccountListUpdate(acc.ParentAccountID.Value);
                queueAccountListUpdate(acc.ID);
                List<long> descIDs = GetAccountDescendants(acc.ID);
                foreach (long aid in descIDs)
                {
                    queueAccountListUpdate(aid);
                }

                return new DTO.SaveEntityDTO<DTO.AccountDTO>(dto.UserInfo) { Entity = GetAccountByID(dto.UserInfo, acc.ID).Entity };
            }
        }

        private void queueAccountListUpdate(long accountID)
        {
            execNonQuery(
                string.Format(
                    "IF Not EXISTS(SELECT * FROM [UpdateQueue.AccountList] where AccountID = {0}) BEGIN insert [UpdateQueue.AccountList] (AccountID) Values({0}) END",
                    accountID
                )
            );
        }

        public ChangeParentAccountResultDTO ChangeParentAccount(UserInfoDTO userInfo, long accountID, long newParentAccountID, bool skipChecks)
        {
            ChangeParentAccountResultDTO result = new ChangeParentAccountResultDTO() { Success = false };

            if (!HasPermission(userInfo.EffectiveUser.ID.Value, accountID, "accounts.admin"))
            {
                result.Problems.Add("User does not have admin permissions on source account.");
            }
            if (!HasPermission(userInfo.EffectiveUser.ID.Value, newParentAccountID, "accounts.admin"))
            {
                result.Problems.Add("User does not have admin permissions on new parent account.");
            }
            if (accountID == newParentAccountID)
            {
                result.Problems.Add("Cannot add an account as a child of itself.");
            }

            if (!result.Problems.Any())
            {
                Dictionary<long, AccountTree> tree = getAccountTree();
                lock (tree)
                {
                    AccountTree source = tree[accountID];
                    AccountTree newParent = tree[newParentAccountID];

                    if (source.Parent != null && source.Parent.ID == newParentAccountID)
                    {
                        result.Problems.Add("Old and new parents are the same!");
                    }
                    else
                    {
                        if (accountHasDescendant(accountID, newParentAccountID))
                        {
                            result.Problems.Add("Cannot add an account to one of its descendants.");
                        }
                    }
                }
            }

            if (!result.Problems.Any() || skipChecks)
            {
                using (MainEntities db = ContextFactory.Main)
                {
                    Account account = (from a in db.Accounts where a.ID == accountID select a).FirstOrDefault();
                    Auditor
                        .New(AuditLevelEnum.Information, AuditTypeEnum.UserActivity, userInfo, "")
                        .SetAuditDataObject(
                            AuditUserActivity
                            .New(userInfo, AuditUserActivityTypeEnum.SubAccountRemoved)
                                .SetAccountID(account.ParentAccountID.Value)
                                .SetObjectID(accountID)
                        )
                        .Save(ProviderFactory.Logging)
                    ;
                    account.ParentAccountID = newParentAccountID;
                    db.SaveChanges();
                    Auditor
                        .New(AuditLevelEnum.Information, AuditTypeEnum.UserActivity, userInfo, "")
                        .SetAuditDataObject(
                            AuditUserActivity
                            .New(userInfo, AuditUserActivityTypeEnum.SubAccountAdded)
                                .SetAccountID(account.ParentAccountID.Value)
                                .SetObjectID(accountID)
                        )
                        .Save(ProviderFactory.Logging)
                    ;
                    if (!skipChecks) reloadTree();
                    result.Success = true;

                    // subscription checks for this account and all of its descendants
                    ProviderFactory.Notifications.AddSubscriptionCheck(null, accountID);
                    List<long> descendants = GetAccountDescendants(accountID);
                    foreach (long descID in descendants)
                    {
                        ProviderFactory.Notifications.AddSubscriptionCheck(null, descID);
                    }


                    return result;
                }
            }

            return result;

        }

        public ChangeParentAccountResultDTO DetachAccount(UserInfoDTO userInfo, long accountID)
        {
            long resellerAccountID = findAccountResellerAncestor(accountID);
            if (resellerAccountID == 0)
            {
                ChangeParentAccountResultDTO result = new ChangeParentAccountResultDTO() { Success = false };
                result.Problems.Add("Could not find reseller!");
                return result;
            }

            return ChangeParentAccount(userInfo, accountID, resellerAccountID, false);

        }

        public long? GetAccountIDByEdmundsDealershipID(long edmundsDealershipID)
        {
            using (MainEntities db = ContextFactory.Main)
            {
                return (from a in db.Accounts where a.EdmundsDealershipID.HasValue && a.EdmundsDealershipID.Value == edmundsDealershipID select a.ID).FirstOrDefault();
            }
        }

        public string GetAccountNameByID(long accountID)
        {
            using (MainEntities db = ContextFactory.Main)
            {
                return (from a in db.Accounts where a.ID == accountID select a.DisplayName == null ? a.Name : a.DisplayName).FirstOrDefault();
            }
        }

        public long? GetAccountCountryByID(long accountID)
        {
            using (MainEntities db = ContextFactory.Main)
            {
                return (from a in db.Accounts
                        join ads in db.Addresses on a.BillingAddressID equals ads.ID
                        join city in db.Cities on ads.CityID equals city.ID
                        join st in db.States on city.StateID equals st.ID
                        where a.ID == accountID
                        select st.CountryID
                       ).FirstOrDefault();
            }
        }

        public List<AccountAutoApproveStatusDTO> GetAccountAutoApproveStatus(UserInfoDTO userInfo, long accountID)
        {
            List<AccountAutoApproveStatusDTO> result = new List<AccountAutoApproveStatusDTO>();

            ITimezoneProvider tz = ProviderFactory.TimeZones;
            TimeZoneInfo timeZone = tz.GetTimezoneForUserID(userInfo.EffectiveUser.ID.Value);

            List<UserActivityDTO> listUserActivityDTO = new List<UserActivityDTO>();

            using (MainEntities db = ContextFactory.Main)
            {
                if (db.Connection.State != System.Data.ConnectionState.Open)
                    db.Connection.Open();

                using (DbCommand cmd3 = db.CreateStoreCommand(
                      "spGetAutoApprovelStatusByAccountID",
                      System.Data.CommandType.StoredProcedure,
                          new SqlParameter("@AccountID", accountID)                          
                      ))
                {
                    using (DbDataReader reader = cmd3.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            result.Add(new AccountAutoApproveStatusDTO()
                            {
                                AccountName = Util.GetReaderValue<string>(reader, "AccountName", ""),
                                AccountID = Util.GetReaderValue<long>(reader, "AccountID", 0),
                                SyndicatorID = Util.GetReaderValue<long>(reader, "SyndicatorID", 0),
                                SyndicatorName = Util.GetReaderValue<string>(reader, "SyndicatorName", ""),                                
                                SocialNetworkName = Util.GetReaderValue<string>(reader, "SocialNetworkName", ""),
                                PostAutoApprovalID = Util.GetReaderValue<long>(reader, "PostAutoApprovalID", 0),
                                AutoApproveStatus = Util.GetReaderValue<bool>(reader, "AutoApproveStatus", false),
                                SocialNetwork = (SocialNetworkEnum)Util.GetReaderValue<long>(reader, "SocialNetworkID", 0),
                                DateCreated = new SIDateTime(Util.GetReaderValue<DateTime>(reader, "DateCreated", DateTime.MinValue), timeZone)
                            });
                        }
                    }
                }
            }

            return result;            
        }

        public bool AddRemoveAccountAutoApproval(UserInfoDTO userInfo, long PostAutoApprovalID, long SyndicatorID, long ApprovingUserID, long AccountID, long SocialNetworkID, bool isDisabled)
        {
            bool result = false;

            using (MainEntities db = ContextFactory.Main)
            {
                if (PostAutoApprovalID > 0 && isDisabled)
                {
                    //Delete
                    PostAutoApproval postAutoApproval = (from pa in db.PostAutoApprovals where pa.ID == PostAutoApprovalID select pa).FirstOrDefault();

                    db.PostAutoApprovals.DeleteObject(postAutoApproval);
                    db.SaveChanges();

                    result = true;

                    Auditor
                        .New(AuditLevelEnum.Information, AuditTypeEnum.AccountActivity, userInfo, "")
                        .SetAuditDataObject(
                            AuditAccountActivity
                                .New(AccountID, AccountActivityTypeEnum.SocialDataBackFillDataQueued)
                                .SetDescription("Remove Auto Approval for SyndicatorID [{0}], SocialNetworkID [{1}], UserID [{2}].", postAutoApproval.SyndicatorID, postAutoApproval.SocialNetworkID, postAutoApproval.ApprovingUserID)
                        )
                    .Save(ProviderFactory.Logging);

                }
                else if (PostAutoApprovalID == 0 && isDisabled)
                {
                    //Add
                    PostAutoApproval postAutoApproval = new PostAutoApproval();

                    postAutoApproval.AccountID = AccountID;
                    postAutoApproval.ApprovingUserID = ApprovingUserID;
                    postAutoApproval.DateCreated = DateTime.UtcNow;
                    postAutoApproval.RejectionCount = 0;
                    postAutoApproval.SocialNetworkID = SocialNetworkID;
                    postAutoApproval.SyndicatorID = SyndicatorID;

                    db.PostAutoApprovals.AddObject(postAutoApproval);
                    db.SaveChanges();

                    result = true;

                    Auditor
                        .New(AuditLevelEnum.Information, AuditTypeEnum.AccountActivity, userInfo, "")
                        .SetAuditDataObject(
                            AuditAccountActivity
                                .New(AccountID, AccountActivityTypeEnum.SocialDataBackFillDataQueued)
                                .SetDescription("Added Auto Approval for  SyndicatorID [{0}], SocialNetworkID [{1}], UserID [{2}].", SyndicatorID, SocialNetworkID, ApprovingUserID)
                        )
                    .Save(ProviderFactory.Logging);
                }
            }

            return result;
        }

        #endregion

        #region account search

        private AccountSearchResultDTO searchAccountsByPermission(AccountSearchRequestDTO request, PermissionTypeEnum permissionType)
        {
            ITimezoneProvider tz = ProviderFactory.TimeZones;
            TimeZoneInfo timeZone = tz.GetTimezoneForUserID(request.UserInfo.EffectiveUser.ID.Value);

            using (MainEntities db = ContextFactory.Main)
            {
                if (db.Connection.State != System.Data.ConnectionState.Open)
                    db.Connection.Open();

                string search = "";
                if (!string.IsNullOrWhiteSpace(request.SearchString))
                {
                    string[] searchParts = request.SearchString.Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);
                    if (searchParts.Length > 0)
                    {
                        search = string.Join("|", searchParts);
                    }
                }

                using (DbCommand cmd = db.CreateStoreCommand(
                    "spSearchAccounts4",
                     System.Data.CommandType.StoredProcedure,

                     new SqlParameter("@SearchStringParts", search),
                     new SqlParameter("@FranchiseTypeIDs", (request.FranchiseTypeIDs == null) ? null : string.Join("|", request.FranchiseTypeIDs.ToArray())),
                     new SqlParameter("@PackageIDs", (request.PackageIDs == null) ? null : string.Join("|", request.PackageIDs.ToArray())),
                     new SqlParameter("@AccountTypeIDs", (request.AccountTypeIDs == null) ? null : string.Join("|", request.AccountTypeIDs.ToArray())),
                     new SqlParameter("@HasChildAccounts", request.OnlyIfHasChildAccounts),

                     new SqlParameter("@ExcludeAccountIDs", (request.ExcludeAccountIDs == null) ? null : string.Join("|", request.ExcludeAccountIDs.ToArray())),

                     new SqlParameter("@ConnectedSocialNetworkIDs", (request.ConnectedSocialNetworks == null) ? null : string.Join("|", (from c in request.ConnectedSocialNetworks select (long)c).ToArray())),

                     new SqlParameter("@OriginSystemIdentifier", request.OriginSystemIdentifier),

                     new SqlParameter("@InVirtualGroupID", request.InVirtualGroupID),
                     new SqlParameter("@NotInVirtualGroupID", request.NotInVirtualGroupID),

                     new SqlParameter("@ParentAccountID", request.ParentAccountID.HasValue ? request.ParentAccountID.Value : 0),
                     new SqlParameter("@NotParentAccountID", request.NotParentAccountID.HasValue ? request.NotParentAccountID.Value : 0),

                     new SqlParameter("@CallingUserID", request.UserInfo.EffectiveUser.ID.GetValueOrDefault()),
                     new SqlParameter("@AccountIDAndDescendants", request.AccountIDAndDescendants),

                     new SqlParameter("@PermissionTypeID", (long)permissionType),

                     new SqlParameter("@SortBy", request.SortBy),
                     new SqlParameter("@SortAscending", request.SortAscending),

                     new SqlParameter("@StartIndex", request.StartIndex),
                     new SqlParameter("@EndIndex", request.EndIndex)
                ))
                {
                    AccountSearchResultDTO results = new AccountSearchResultDTO()
                    {
                        StartIndex = request.StartIndex,
                        EndIndex = request.EndIndex,
                        SortBy = request.SortBy,
                        SortAscending = request.SortAscending,
                        SearchString = request.SearchString
                    };

                    using (DbDataReader reader = cmd.ExecuteReader())
                    {
                        if (reader.Read())
                        {
                            //the total records
                            results.TotalRecords = reader.GetInt32(0);

                            if (reader.NextResult())
                            {
                                results.Items = db.Translate<AccountListItemDTO>(reader).ToList();

                                // get the overall platform notification counts for each result record
                                if (reader.NextResult())
                                {
                                    List<PlatformNotificationCount> ncounts = db.Translate<PlatformNotificationCount>(reader).ToList();

                                    foreach (AccountListItemDTO item in results.Items)
                                    {
                                        item.NotificationCount = Convert.ToInt32((from n in ncounts where n.AccountID == item.ID select n.NotificationCount).Sum());
                                        item.RolledNotificationCount = Convert.ToInt32((from n in ncounts where n.AccountID == item.ID select n.RolledNotificationCount).Sum());
                                        item.AlertCount = item.RolledNotificationCount;
                                    }

                                    if (reader.NextResult())
                                    {
                                        //connected social networks
                                        while (reader.Read())
                                        {
                                            long accountID = Util.GetReaderValue<long>(reader, "AccountID", 0);
                                            SocialNetworkEnum network = (SocialNetworkEnum)Util.GetReaderValue<long>(reader, "SocialNetworkID", 0);

                                            AccountListItemDTO item = (from r in results.Items where r.ID == accountID select r).FirstOrDefault();
                                            if (item != null)
                                            {
                                                item.ConnectedSocialNetworks.Add(network);
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }

                    Dictionary<long, AccountTree> tree = getAccountTree();
                    bool canViewExternalID = HasPermission(request.UserInfo.EffectiveUser.ID.Value, PermissionTypeEnum.reseller_admin);
                    foreach (AccountListItemDTO item in results.Items)
                    {
                        if (!canViewExternalID)
                            item.OriginSystemIdentifier = null;
                        item.DateCreated = new SIDateTime(item.DateCreatedUTC, timeZone);
                        //ResellerDTO rdto = GetResellerByAccountID(request.UserInfo, item.ID, timeZone);
                        //if (rdto != null)
                        //{
                        //    item.ThisAccountsResellerID = rdto.ID;
                        //    item.ThisAccountsResellerName = (from t in tree where t.Key == rdto.AccountID select t.Value.Name).FirstOrDefault();
                        //}
                    }

                    return results;
                }
            }

        }

        public AccountSearchResultDTO SearchAccounts(AccountSearchRequestDTO request)
        {
            return searchAccountsByPermission(request, PermissionTypeEnum.accounts_view);
        }
        public AccountSearchResultDTO SearchAccountsForVirtualGroupAdmin(AccountSearchRequestDTO request)
        {
            return searchAccountsByPermission(request, PermissionTypeEnum.virtualGroups_admin);
        }

        public AccountSearchResultDTO SearchAccountsForPublish(AccountSearchRequestDTO request)
        {
            return searchAccountsByPermission(request, PermissionTypeEnum.social_publish);
        }

        #endregion

        #region syndication

        internal List<long> getSyndicatorIDsFromAccountList(List<long> accountIDs)
        {
            using (MainEntities db = ContextFactory.Main)
            {
                List<Syndicator> syndicators = (from s in db.Syndicators select s).ToList();

                List<long> ids = (
                    from s in syndicators
                    where accountIDs.Contains(s.AccountID)
                    select s.ID
                ).ToList();

                return ids;
            }
        }

        #endregion

        #region permissions and roles

        public PermissionTypeDTO GetPermissionType(string permissionCode)
        {
            return getPermissionTypeByCode(permissionCode);
        }

        public PermissionTypeDTO GetPermissionType(PermissionTypeEnum permissionType)
        {
            return getPermissionType(permissionType);
        }

        public RoleChangeResultDTO AddRole(UserInfoDTO userInfo, long userID, long accountID, long roleTypeID, bool includeChildren, bool skipApplyRoles, bool skipReloadTree)
        {
            RoleChangeResultDTO result = new RoleChangeResultDTO() { Success = false };

            try
            {

                if (!HasPermission(userInfo.EffectiveUser.ID.Value, accountID, PermissionTypeEnum.users_admin))
                {
                    result.Problems.Add(Locale.Localize("system.nopermission"));
                    return result;
                }
                if (userInfo.EffectiveUser.ID.Value == userID)
                {
                    result.Problems.Add(Locale.Localize("cantassignownrole"));
                    return result;
                }

                RoleTypeDTO roleType = (from rt in GetRoleTypes(UserInfoDTO.System) where rt.ID.Value == roleTypeID select rt).FirstOrDefault();
                string accountName = GetAccountNameByID(accountID);
                string username = getUser(userID, null, null, UserInfoDTO.System, false).Username;

                using (MainEntities db = ContextFactory.Main)
                {
                    Accounts_Users auser = (from au in db.Accounts_Users where au.UserID.Equals(userID) && au.AccountID.Equals(accountID) select au).FirstOrDefault();
                    if (auser == null)
                    {
                        auser = new Accounts_Users() { AccountID = accountID, UserID = userID };
                        db.Accounts_Users.AddObject(auser);
                        db.SaveChanges();
                    }

                    Role role = (from r in db.Roles where r.Account_UserID.Equals(auser.ID) select r).FirstOrDefault();
                    if (role == null)
                    {
                        role = new Role() { Account_UserID = auser.ID, RoleTypeID = roleTypeID, IncludeChildren = includeChildren };
                        db.Roles.AddObject(role);
                    }
                    else
                    {
                        role.IncludeChildren = includeChildren;
                        role.RoleTypeID = roleTypeID;
                    }

                    db.SaveChanges();

                    if (!skipApplyRoles) db.spApplyRoles(userID);
                    if (!skipReloadTree) reloadTree();

                    //queue a notification subscription check
                    ProviderFactory.Notifications.AddSubscriptionCheck(userID, null);

                    Auditor
                        .New(AuditLevelEnum.Information, AuditTypeEnum.UserActivity, userInfo, "")
                        .SetAuditDataObject(
                            AuditUserActivity
                                .New(userInfo, AuditUserActivityTypeEnum.RoleAdded)
                                .SetUserIDActedUpon(userID)
                                .SetRoleTypeID(roleTypeID)
                                .SetAccountID(accountID)
                                .SetDescription("User [{0}] was granted role [{1}] for account [{2}]. IncludeChildren: {3}", username, roleType.Name, accountName, includeChildren)
                        )
                        .Save(ProviderFactory.Logging)
                    ;

                }
            }
            catch (Exception ex)
            {
                return result;
                throw new Exception(ex.ToString(), ex);
            }

            result.Success = true;
            return result;

        }

        public RoleChangeResultDTO RemoveRole(UserInfoDTO userInfo, long roleID)
        {
            RoleChangeResultDTO result = new RoleChangeResultDTO() { Success = false };

            using (MainEntities db = ContextFactory.Main)
            {

                Role role = (from r in db.Roles where r.ID.Equals(roleID) select r).FirstOrDefault();
                if (role != null)
                {
                    long userID = (from au in db.Accounts_Users where au.ID.Equals(role.Account_UserID) select au.UserID).FirstOrDefault();
                    long accountID = (from au in db.Accounts_Users where au.ID.Equals(role.Account_UserID) select au.AccountID).FirstOrDefault();

                    if (!HasPermission(userID, accountID, PermissionTypeEnum.users_admin))
                    {
                        result.Problems.Add(Locale.Localize("system.nopermission"));
                        return result;
                    }
                    if (userInfo.EffectiveUser.ID.Value == userID)
                    {
                        result.Problems.Add(Locale.Localize("cantassignownrole"));
                        return result;
                    }

                    removeRole(userInfo, userID, accountID);

                    //queue a notification subscription check
                    ProviderFactory.Notifications.AddSubscriptionCheck(userID, null);
                }

            }

            result.Success = true;
            return result;
        }

        private void removeRole(UserInfoDTO userInfo, long userID, long accountID)
        {
            using (MainEntities db = ContextFactory.Main)
            {
                Accounts_Users auser = (from au in db.Accounts_Users where au.AccountID == accountID && au.UserID == userID select au).FirstOrDefault();
                if (auser != null)
                {
                    Role role = (from r in db.Roles where r.Account_UserID == auser.ID select r).FirstOrDefault();
                    if (role != null)
                    {
                        long roleTypeID = role.RoleTypeID;
                        bool includeChildren = role.IncludeChildren;
                        RoleTypeDTO roleType = (from rt in GetRoleTypes(UserInfoDTO.System) where rt.ID.Value == roleTypeID select rt).FirstOrDefault();
                        string accountName = GetAccountNameByID(accountID);
                        string username = getUser(userID, null, null, UserInfoDTO.System, false).Username;

                        db.Roles.DeleteObject(role);
                        db.SaveChanges();

                        db.spApplyRoles(userID);
                        reloadTree();

                        Auditor
                            .New(AuditLevelEnum.Information, AuditTypeEnum.UserActivity, userInfo, "")
                            .SetAuditDataObject(
                                AuditUserActivity
                                    .New(userInfo, AuditUserActivityTypeEnum.RoleRemoved)
                                    .SetUserIDActedUpon(userID)
                                    .SetRoleTypeID(roleTypeID)
                                    .SetAccountID(accountID)
                                    .SetDescription("User [{0}] had role [{1}] removed for account [{2}]. IncludeChildren: {3}", username, roleType.Name, accountName, includeChildren)
                            )
                            .Save(ProviderFactory.Logging)
                        ;
                    }

                }
            }
        }

        public List<RoleTypeDTO> GetRoleTypes(UserInfoDTO userInfo)
        {
            ITimezoneProvider tz = ProviderFactory.TimeZones;
            TimeZoneInfo timeZone = tz.GetTimezoneForUserID(userInfo.EffectiveUser.ID.GetValueOrDefault());

            List<RoleTypeDTO> roleTypes = new List<RoleTypeDTO>();
            using (MainEntities db = ContextFactory.Main)
            {
                foreach (RoleType p in db.RoleTypes.Where(r => r.Hidden == false))
                {
                    roleTypes.Add(RoleType2DTO(p, timeZone));
                }
            }

            return roleTypes;
        }

        public void SaveRoleType(RoleTypeDTO dto)
        {
            ITimezoneProvider tz = ProviderFactory.TimeZones;

            using (MainEntities db = ContextFactory.Main)
            {
                RoleType roleType;
                if (dto.ID.HasValue)
                {
                    foreach (RoleTypes_PermissionTypes item in (from pft in db.RoleTypes_PermissionTypes where pft.RoleTypeID.Equals(dto.ID.Value) select pft))
                    {
                        db.RoleTypes_PermissionTypes.DeleteObject(item);
                    }
                    db.SaveChanges();

                    roleType = (from p in db.RoleTypes where p.ID.Equals(dto.ID.Value) select p).FirstOrDefault();
                    roleType.Name = dto.Name;
                }
                else
                {
                    roleType = new RoleType() { Name = dto.Name, DateCreated = DateTime.UtcNow };
                    db.RoleTypes.AddObject(roleType);
                }

                db.SaveChanges();

                foreach (long permissionTypeID in dto.PermissionTypeIDs)
                {
                    RoleTypes_PermissionTypes pft = new RoleTypes_PermissionTypes() { RoleTypeID = roleType.ID, PermissionTypeID = permissionTypeID };
                    db.RoleTypes_PermissionTypes.AddObject(pft);
                }

                db.SaveChanges();

                db.spApplyRoles(null);

            }
        }

        public void DeleteRoleType(long roleTypeID)
        {
            using (MainEntities db = ContextFactory.Main)
            {
                db.spDeleteRoleType(roleTypeID);
            }
        }

        public List<AccountPermissionDTO> UsersWithAccountPermission(long accountID, PermissionTypeEnum? permissionType, bool includeSuperUsers)
        {
            List<AccountPermissionDTO> accountpermissions = new List<AccountPermissionDTO>();
            using (MainEntities db = ContextFactory.Main)
            {
                if (db.Connection.State != System.Data.ConnectionState.Open)
                    db.Connection.Open();

                using (DbCommand cmd = db.CreateStoreCommand("spGetAccountPermissionsList", System.Data.CommandType.StoredProcedure,
                    new SqlParameter("@AccountID", accountID),
                    new SqlParameter("@PermissionTypeID", (long?)permissionType),
                    new SqlParameter("@IncludeSuperusers", includeSuperUsers)
                ))
                {
                    using (DbDataReader reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            accountpermissions.Add(new AccountPermissionDTO()
                            {
                                UserID = Util.GetReaderValue<long>(reader, "UserID", 0),
                                PermissionType = (PermissionTypeEnum)Util.GetReaderValue<long>(reader, "PermissionTypeID", 0)
                            });
                        }
                    }
                }

            }
            return accountpermissions;
        }

        #endregion

        #region competitors

        public SaveEntityDTO<bool> AddCompetitor(long accountID, long competitorID, int maxCompetitors, UserInfoDTO userInfo)
        {
            bool result = true;
            string modelErrors = "";
            List<string> probs = new List<string>();

            using (MainEntities db = ContextFactory.Main)
            {
                List<Accounts_Competitors> competitors = (from ac in db.Accounts_Competitors where ac.AccountID.Equals(accountID) select ac).ToList();
                if (competitors.Count >= maxCompetitors)
                {
                    result = false;
                    modelErrors = "Max. " + maxCompetitors + " Competitors Allowed.";
                    probs.Add(modelErrors);
                }
                else
                {
                    var newCompetitor = new Accounts_Competitors() { AccountID = accountID, CompetitorAccountID = competitorID, DateEntered = DateTime.UtcNow, Active = true };
                    db.Accounts_Competitors.AddObject(newCompetitor);
                    int savedCount = db.SaveChanges();

                    if (savedCount == 1)
                    {
                        result = true;
                        Auditor
                            .New(AuditLevelEnum.Information, AuditTypeEnum.UserActivity, userInfo, "")
                            .SetAuditDataObject(
                                AuditUserActivity
                                    .New(userInfo, AuditUserActivityTypeEnum.CompetitorAdded)
                                    .SetAccountID(accountID)
                                    .SetObjectID(competitorID)
                            )
                            .Save(ProviderFactory.Logging)
                        ;
                    }
                    else
                    {
                        result = false;
                        modelErrors = "Unable to update at this time.";
                        probs.Add(modelErrors);
                    }
                }

                if (probs.Any())
                {
                    return new DTO.SaveEntityDTO<bool>(userInfo) { Problems = probs };
                }

                return new DTO.SaveEntityDTO<bool>(userInfo) { Entity = result };

            }
        }

        public SaveEntityDTO<bool> RemoveCompetitor(long recID, UserInfoDTO userInfo)
        {

            using (MainEntities db = ContextFactory.Main)
            {
                Accounts_Competitors x = (from ac in db.Accounts_Competitors where ac.ID == recID select ac).FirstOrDefault();

                if (x != null)
                {
                    long accountID = x.AccountID;
                    long competitorID = x.CompetitorAccountID;

                    db.DeleteObject(x);
                    db.SaveChanges();

                    Auditor
                        .New(AuditLevelEnum.Information, AuditTypeEnum.UserActivity, userInfo, "")
                        .SetAuditDataObject(
                            AuditUserActivity
                                .New(userInfo, AuditUserActivityTypeEnum.CompetitorRemoved)
                                .SetAccountID(accountID)
                                .SetObjectID(competitorID)
                        )
                        .Save(ProviderFactory.Logging)
                    ;
                }
            }

            return new DTO.SaveEntityDTO<bool>(userInfo) { Entity = true };
        }

        internal List<AccountCompetitorListItem> GetAccountCompetitorIDs(List<long> accountIDs)
        {
            using (MainEntities db = ContextFactory.Main)
            {
                if (db.Connection.State != System.Data.ConnectionState.Open)
                    db.Connection.Open();

                using (DbCommand cmd = db.CreateStoreCommand("spGetAccountCompetitorIDs",
                    System.Data.CommandType.StoredProcedure, new SqlParameter("AccountIDs", string.Join("|", accountIDs.ToArray()))))
                {
                    using (DbDataReader reader = cmd.ExecuteReader())
                    {
                        List<AccountCompetitorListItem> list = db.Translate<AccountCompetitorListItem>(reader).ToList();
                        return list;
                    }
                }
            }
        }

        #endregion

        #region addresses

        //public AddressDTO GetAddressByID(long addressID)
        //{
        //    using (MainEntities db = ContextFactory.Main)
        //    {
        //        if (db.Connection.State != System.Data.ConnectionState.Open)
        //            db.Connection.Open();

        //        using
        //            (DbCommand cmd = db.CreateStoreCommand(
        //                    "spGetAddress", System.Data.CommandType.StoredProcedure,
        //                    new SqlParameter("AddressID", addressID)
        //                )
        //            )
        //        {
        //            using (DbDataReader reader = cmd.ExecuteReader())
        //            {
        //                return db.Translate<AddressDTO>(reader).FirstOrDefault();
        //            }
        //        }
        //    }

        //}

        //public SaveEntityDTO<AddressDTO> Save(SaveEntityDTO<AddressDTO> add)
        //{
        //    using (MainEntities db = ContextFactory.Main)
        //    {
        //        List<string> probs = new List<string>();

        //        //if a zip was entered, it must be valid
        //        long? zipCodeID = null;
        //        if (string.IsNullOrWhiteSpace(add.Entity.ZipCode))
        //        {
        //            probs.Add(Locale.Localize("zipcoderequired"));
        //        }

        //        if (!string.IsNullOrWhiteSpace(add.Entity.ZipCode))
        //        {
        //            zipCodeID = (from z in db.ZipCodes where z.ZipCode1.Trim().ToLower() == add.Entity.ZipCode.Trim().ToLower() select z.ID).FirstOrDefault();
        //            if (!zipCodeID.HasValue && add.Entity.CountryID == 1)
        //            {
        //                probs.Add(Locale.Localize("zipcodenotfound"));
        //            }
        //            else if (!zipCodeID.HasValue && add.Entity.StateID.HasValue && add.Entity.CountryID != 1)
        //            {
        //                ZipCode newZip = new ZipCode()
        //                {
        //                    StateID = add.Entity.StateID.Value,
        //                    DateCreated = DateTime.UtcNow,
        //                    DateModified = DateTime.UtcNow,
        //                    ZipCode1 = add.Entity.ZipCode
        //                };
        //                using (MainEntities dbMain = ContextFactory.Main)
        //                {
        //                    dbMain.ZipCodes.AddObject(newZip);
        //                    dbMain.SaveChanges();
        //                }
        //                zipCodeID = newZip.ID;
        //            }
        //        }

        //        //fields look ok?
        //        if (string.IsNullOrWhiteSpace(add.Entity.Street1))
        //        {
        //            probs.Add(Locale.Localize("streetaddressrequired"));
        //        }

        //        if (probs.Count() > 0)
        //        {
        //            return new DTO.SaveEntityDTO<DTO.AddressDTO>(add.UserInfo) { Problems = probs };
        //        }

        //        //look up the country
        //        Country country = (from c in db.Countries where c.ID.Equals(add.Entity.CountryID) select c).FirstOrDefault();

        //        //might need to create a state
        //        State state = null;

        //        if (add.Entity.StateID.HasValue)
        //        {
        //            state = (from s in db.States where s.ID.Equals(add.Entity.StateID.Value) select s).FirstOrDefault();
        //        }
        //        if (!add.Entity.StateID.HasValue)
        //        {
        //            long? languageToAddToState = null;
        //            if (add.Entity.LanguageID != null && (!country.LanguageID.HasValue || add.Entity.LanguageID.Value != country.LanguageID))
        //            {
        //                languageToAddToState = add.Entity.LanguageID;
        //            }

        //            state = new State()
        //            {
        //                CountryID = add.Entity.CountryID,
        //                Name = add.Entity.StateName,
        //                Abbreviation = add.Entity.StateAbbreviation,
        //                LanguageID = languageToAddToState
        //            };
        //            db.States.AddObject(state);
        //            db.SaveChanges();
        //            add.Entity.StateID = state.ID;
        //        }


        //        //might need to create a city                
        //        if (!add.Entity.CityID.HasValue)
        //        {
        //            long? languageToAddToCity = add.Entity.LanguageID;

        //            if (languageToAddToCity.HasValue && state.LanguageID.HasValue && state.LanguageID.Value == languageToAddToCity.Value)
        //                languageToAddToCity = null;
        //            if (languageToAddToCity.HasValue && country.LanguageID.HasValue && country.LanguageID.Value == languageToAddToCity.Value)
        //                languageToAddToCity = null;

        //            City city = new City()
        //            {
        //                Name = add.Entity.CityName,
        //                StateID = add.Entity.StateID.Value,
        //                ZipCodeID = zipCodeID,
        //                LanguageID = languageToAddToCity
        //            };

        //            db.Cities.AddObject(city);
        //            db.SaveChanges();
        //            add.Entity.CityID = city.ID;
        //        }

        //        Address address = null;
        //        if (add.Entity.ID.HasValue)
        //        {
        //            address = (from a in db.Addresses where a.ID.Equals(add.Entity.ID.Value) select a).FirstOrDefault();
        //        }
        //        if (address == null)
        //        {
        //            address = new Address();
        //        }

        //        address.Street1 = add.Entity.Street1;
        //        address.Street2 = add.Entity.Street2;
        //        address.Street3 = add.Entity.Street3;
        //        address.CityID = add.Entity.CityID.Value;
        //        address.Latitude = add.Entity.Latitude;
        //        address.Longitude = add.Entity.Longitude;

        //        if (address.EntityState == System.Data.EntityState.Detached)
        //            db.Addresses.AddObject(address);

        //        db.SaveChanges();

        //        return new DTO.SaveEntityDTO<DTO.AddressDTO>(add.UserInfo) { Entity = GetAddressByID(address.ID) };
        //    }
        //}

        #endregion

        #region menu stuff

        private static XDocument _menuDoc = null;
        private static XDocument GetMenus()
        {
            if (_menuDoc == null)
            {
                string menuXML = Util.GetResourceTextFile("menus.xml", Assembly.GetAssembly(typeof(ProviderFactory)));
                _menuDoc = XDocument.Parse(menuXML, LoadOptions.None);
            }

            return _menuDoc;
        }

        public List<MenuItemDTO> GetMenuItems(long userID, string baseURL, string membersPath)
        {
            XDocument menuDoc = GetMenus();

            List<MenuItemDTO> items = new List<MenuItemDTO>();

            foreach (XElement xMain in menuDoc.Root.Elements("menu"))
            {
                MenuItemDTO menu = new MenuItemDTO()
                {
                    Name = Locale.Localize("menu." + xMain.Attribute("name").Value),
                    URL = ""
                };

                int subMenusAdded = 0;
                foreach (XElement xSub in xMain.Elements("menu"))
                {
                    bool allowed = false;


                    string pString = xSub.Attribute("requires").Value.ToLower();
                    if (string.IsNullOrWhiteSpace(pString))
                    {
                        allowed = true;
                    }
                    else
                    {
                        List<string> perms = pString.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries).ToList();
                        foreach (string perm in perms)
                        {
                            PermissionTypeEnum pType;
                            if (Enum.TryParse<PermissionTypeEnum>(perm.Replace(".", "_").ToLower(), out pType))
                            {
                                if (HasPermission(userID, pType))
                                {
                                    allowed = true;
                                    break;
                                }

                            }
                        }
                    }

                    if (allowed)
                    {
                        MenuItemDTO subMenu = new MenuItemDTO()
                        {
                            Name = Locale.Localize("menu." + xSub.Attribute("name").Value),
                            URL = string.Format("{0}/{1}", baseURL, xSub.Attribute("url").Value)
                        };

                        if (string.IsNullOrWhiteSpace(menu.URL))
                            menu.URL = subMenu.URL;

                        if (xSub.Attribute("url").Value.Trim().ToLower() == membersPath)
                        {
                            subMenu.Selected = true;
                            menu.Selected = true;
                        }
                        menu.Items.Add(subMenu);
                    }
                }

                if (menu.Items.Any())
                {
                    items.Add(menu);
                }
            }

            return items;
        }

        #endregion

        #region lazy loaded lookups

        #region permissions

        private static object _permLock = new object();
        private static Dictionary<string, PermissionTypeDTO> _permsByCode = null;
        private static Dictionary<long, PermissionTypeDTO> _permsByID = null;

        private static PermissionTypeDTO getPermissionTypeByID(long permissionTypeID)
        {
            if (_permsByID == null) fillPermissionLists();
            return _permsByID[permissionTypeID];
        }

        private static PermissionTypeDTO getPermissionType(PermissionTypeEnum type)
        {
            string code = type.ToString().ToLower().Replace("_", ".").Trim();
            return getPermissionTypeByCode(code);
        }

        private static PermissionTypeDTO getPermissionTypeByCode(string permissionCode)
        {
            if (_permsByCode == null) fillPermissionLists();

            PermissionTypeDTO dto = null;
            if (_permsByCode.TryGetValue(permissionCode.Trim().ToLower(), out dto))
                return dto;
            else
            {
                PermissionType type = new PermissionType()
                {
                    Code = permissionCode.Trim().ToLower(),
                    Name = permissionCode
                };

                using (MainEntities db = ContextFactory.Main)
                {
                    db.PermissionTypes.AddObject(type);
                    db.SaveChanges();
                }

                fillPermissionLists();

                return _permsByCode[permissionCode.Trim().ToLower()];
            }

        }

        private static void fillPermissionLists()
        {
            List<PermissionTypeDTO> types = null;

            using (MainEntities db = ContextFactory.Main)
            {
                types = (
                    from t in db.PermissionTypes
                    select new PermissionTypeDTO()
                    {
                        Code = t.Code,
                        Name = t.Name,
                        ID = t.ID
                    }
                ).ToList();
            }

            lock (_permLock)
            {
                _permsByCode = new Dictionary<string, PermissionTypeDTO>();
                _permsByID = new Dictionary<long, PermissionTypeDTO>();

                foreach (PermissionTypeDTO dto in types)
                {
                    _permsByCode.Add(dto.Code.Trim().ToLower(), dto);
                    _permsByID.Add(dto.ID, dto);
                }
            }
        }

        #endregion

        //private FeatureType GetFeatureType(FeatureTypeDTO type)
        //{
        //    string featureName = type.ToString().ToLower().Trim();
        //    using (MainEntities db = ContextFactory.Main)
        //    {
        //        FeatureType dbType = (
        //            from pt in db.FeatureTypes
        //            where pt.Name.Equals(featureName)
        //            select pt
        //        ).FirstOrDefault();

        //        if (dbType == null)
        //        {
        //            dbType = new FeatureType()
        //            {
        //                Name = featureName
        //            };
        //            db.FeatureTypes.AddObject(dbType);
        //            db.SaveChanges();
        //        }

        //        return dbType;
        //    }
        //}

        private AccountType GetAccountType(AccountTypeEnum type)
        {
            string typeName = type.ToString().ToLower().Trim();
            using (MainEntities db = ContextFactory.Main)
            {
                AccountType dbType = (
                    from pt in db.AccountTypes
                    where pt.Name.Equals(typeName)
                    select pt
                ).FirstOrDefault();

                if (dbType == null)
                {
                    dbType = new AccountType()
                    {
                        Name = typeName
                    };
                    db.AccountTypes.AddObject(dbType);
                    db.SaveChanges();
                }

                return dbType;
            }
        }

        private AccountType GetAccountTypeByID(long accountTypeID)
        {
            using (MainEntities db = ContextFactory.Main)
            {
                return (from a in db.AccountTypes where a.ID.Equals(accountTypeID) select a).FirstOrDefault();
            }
        }


        #endregion

        #region mapping helpers

        private static UserDTO User2DTO(User u, TimeZoneInfo tz)
        {
            if (u == null) return null;

            return new UserDTO
            {
                ID = u.ID,
                EmailAddress = u.EmailAddress,
                SuperAdmin = u.SuperAdmin,
                Username = u.Username,
                FirstName = u.FirstName,
                LastName = u.LastName,
                MemberOfAccountID = u.MemberOfAccountID,
                TimezoneID = u.TimezoneID,
                CreateDate = new SIDateTime(u.DateCreated, tz)
            };
        }

        private static AccountTypeEnum AccountType2DTO(string typeName)
        {
            AccountTypeEnum dto = AccountTypeEnum.Unknown;
            Enum.TryParse<AccountTypeEnum>(typeName, true, out dto);
            return dto;
        }

        //private static FeatureTypeDTO FeatureType2DTO(string typeName)
        //{
        //    FeatureTypeDTO dto = FeatureTypeDTO.Unknown;
        //    Enum.TryParse<FeatureTypeDTO>(typeName, true, out dto);
        //    return dto;
        //}

        private static CRMSystemTypeDTO CRMSystemType2DTO(CRMSystemType type)
        {
            if (type == null) return null;

            return new CRMSystemTypeDTO()
            {
                ID = type.ID,
                Name = type.Name
            };
        }



        private static RoleTypeDTO RoleType2DTO(RoleType p, TimeZoneInfo timeZone)
        {
            RoleTypeDTO dto = new RoleTypeDTO()
            {
                ID = p.ID,
                Name = p.Name,
                DateCreated = new SIDateTime(p.DateCreated, timeZone),
                PermissionTypeIDs = (from f in p.RoleTypes_PermissionTypes select f.PermissionTypeID).ToList()
            };

            return dto;
        }

        #endregion

        #region account tree

        private static Object _treeLock = new Object();
        private static Dictionary<long, AccountTree> _tree = null;

        internal Dictionary<long, AccountTree> getAccountTree()
        {
            lock (_treeLock)
            {
                if (_tree != null)
                    return _tree;

                List<AccountTreeItem> items = null;

                using (MainEntities db = ContextFactory.Main)
                {
                    if (db.Connection.State != System.Data.ConnectionState.Open)
                        db.Connection.Open();
                    using (DbCommand cmd = db.CreateStoreCommand("spGetAccountTree2", System.Data.CommandType.StoredProcedure))
                    {
                        using (DbDataReader reader = cmd.ExecuteReader())
                        {
                            items = db.Translate<AccountTreeItem>(reader).ToList();
                        }
                    }
                }


                _tree = new Dictionary<long, AccountTree>();

                foreach (AccountTreeItem item in items)
                {
                    AccountTree tree = new AccountTree() { ID = item.ID, Depth = item.Level, ResellerID = item.ResellerID, Name = item.Name, AccountType = (AccountTypeEnum)item.AccountTypeID };
                    _tree.Add(item.ID, tree);
                    if (item.ParentAccountID.HasValue)
                    {
                        AccountTree parent = _tree[item.ParentAccountID.Value];
                        parent.Children.Add(tree);
                        tree.Parent = parent;
                    }
                }
            }

            return _tree;
        }


        public List<long> GetAccountDescendants(long accountID)
        {
            Dictionary<long, AccountTree> tree = getAccountTree();
            AccountTree treeItem = null;
            if (tree.TryGetValue(accountID, out treeItem))
            {
                return getAccountDescendants(treeItem);
            }
            return new List<long>();
        }

        public List<AccountTree> GetAccountAncestors(long accountID)
        {
            return getAccountAncestors(accountID);
        }

        private void reloadTree()
        {
            lock (_treeLock)
            {
                _tree = null;
                getAccountTree();
            }
        }

        private bool accountHasDescendant(long accountID, long checkForAccountID)
        {
            Dictionary<long, AccountTree> tree = getAccountTree();
            lock (tree)
            {
                AccountTree account;
                if (!tree.TryGetValue(accountID, out account))
                    return false;

                foreach (AccountTree item in account.Children)
                {
                    if (item.ID == checkForAccountID)
                        return true;
                }
                foreach (AccountTree item in account.Children)
                {
                    if (accountHasDescendant(item.ID, checkForAccountID))
                        return true;
                }
            }

            return false;
        }

        private bool accountHasAncestor(long accountID, long checkForAccountID)
        {
            Dictionary<long, AccountTree> tree = getAccountTree();
            lock (tree)
            {
                AccountTree account;
                if (!tree.TryGetValue(accountID, out account))
                    return false;

                if (account.Parent == null)
                    return false;

                if (account.Parent.ID == checkForAccountID)
                    return true;

                return accountHasAncestor(account.Parent.ID, checkForAccountID);

            }

        }

        private List<AccountTree> getAccountAncestors(long accountID)
        {
            List<AccountTree> ancestors = new List<AccountTree>();
            Dictionary<long, AccountTree> tree = getAccountTree();
            lock (tree)
            {
                AccountTree item = tree[accountID];
                if (item != null && item.Parent != null)
                {
                    ancestors.Add(item.Parent);
                    ancestors.AddRange(getAccountAncestors(item.Parent.ID));
                }
            }
            return ancestors;
        }

        private List<long> getAccountDescendants(AccountTree treeItem)
        {
            List<long> ids = new List<long>();
            ids.AddRange((
                    from t in treeItem.Children
                    select t.ID
                ).ToList()
            );

            foreach (AccountTree item in treeItem.Children)
            {
                ids.AddRange(getAccountDescendants(item));
            }

            return ids;
        }

        private long findAccountResellerAncestor(long accountID)
        {
            Dictionary<long, AccountTree> tree = getAccountTree();
            lock (tree)
            {
                AccountTree account;
                if (!tree.TryGetValue(accountID, out account))
                    return 0;

                if (account.ResellerID.HasValue)
                    return account.ID;

                if (account.Parent == null)
                    return 0;

                if (account.Parent.ResellerID.HasValue)
                {
                    return account.Parent.ID;
                }


                return findAccountResellerAncestor(account.Parent.ID);

            }
        }


        public void ApplyRolesAndReloadTree()
        {
            using (MainEntities db = ContextFactory.Main)
            {
                db.spApplyRoles(null);
            }
            reloadTree();
        }

        #endregion

        #region permissions

        private PermissionCacheItem getPermissionCacheItem(long userID)
        {
            if (userID == -1)
                return null;

            PermissionCacheItem cache = null;
            lock (_permissionCache)
            {
                if (!_permissionCache.TryGetValue(userID, out cache))
                {
                    validateUser(userID, null, null, null);
                    cache = _permissionCache[userID];
                }

            }
            return cache;
        }


        private bool HasPermission(long userID, long accountID, string permissionCode)
        {
            if (userID == -1) return true;

            PermissionTypeEnum ptype = (PermissionTypeEnum)Enum.Parse(typeof(PermissionTypeEnum), permissionCode.Replace(".", "_"));
            return HasPermission(userID, accountID, ptype);
        }

        public bool HasPermission(long userID, long accountID, PermissionTypeEnum type)
        {
            getAccountTree();

            if (userID == -1) return true;

            long permissionTypeID = getPermissionType(type).ID;

            //get the cached permission set for this user
            PermissionCacheItem cache = getPermissionCacheItem(userID);

            //super admins always get everything
            if (cache.IsSuperAdmin) return true;

            //get the permissions for this type
            List<PermissionListItem> plist = (from p in cache.Permissions where p.PermissionTypeID.Equals(permissionTypeID) select p).ToList();

            //no permissions, no go
            if (!plist.Any())
                return false;

            //no permissions of this type, no go
            if ((from p in plist where p.AccountID.Equals(accountID) select p).Any())
                return true;

            lock (_tree)
            {
                AccountTree tree = null;
                if (!getAccountTree().TryGetValue(accountID, out tree))
                    return false;

                do
                {
                    if (tree.ID == accountID)
                    {
                        if ((from p in plist where p.PermissionTypeID.Equals(permissionTypeID) && p.AccountID == tree.ID select p).Any())
                            return true;
                    }
                    else
                    {
                        if ((from p in plist where p.PermissionTypeID.Equals(permissionTypeID) && p.AccountID == tree.ID && p.IncludeChildren select p).Any())
                            return true;
                    }

                    if (tree.Parent == null) break;
                    tree = tree.Parent;

                } while (true);

                return false;

            }

        }

        private bool HasPermission(long userID, string permissionCode)
        {
            if (userID == -1) return true;

            PermissionTypeEnum ptype = (PermissionTypeEnum)Enum.Parse(typeof(PermissionTypeEnum), permissionCode.Replace(".", "_"));
            return HasPermission(userID, ptype);
        }

        public bool HasPermission(long userID, PermissionTypeEnum type)
        {
            if (userID == -1) return true;

            long permissionTypeID = getPermissionType(type).ID;
            PermissionCacheItem cache = getPermissionCacheItem(userID);
            if (cache.IsSuperAdmin) return true;

            return (from p in cache.Permissions where p.PermissionTypeID.Equals(permissionTypeID) select p).Any();

        }

        public List<long> AccountsWithPermission(long userID, PermissionTypeEnum type)
        {
            Dictionary<long, AccountTree> tree = getAccountTree();

            if (userID == -1) return (from t in tree.Keys select t).ToList();

            PermissionCacheItem cache = getPermissionCacheItem(userID);
            if (cache.IsSuperAdmin) return (from t in tree.Keys select t).ToList();

            long permissionTypeID = getPermissionType(type).ID;

            List<long> ids = new List<long>();
            List<PermissionListItem> items = (from p in cache.Permissions where p.PermissionTypeID.Equals(permissionTypeID) select p).ToList();

            foreach (PermissionListItem item in items)
            {
                ids.Add(item.AccountID);
                if (item.IncludeChildren)
                {
                    ids.AddRange(GetAccountDescendants(item.AccountID));
                }
            }

            return ids;

        }

        internal List<long> GetHighestLevelAccountsWithPermission(long userID, PermissionTypeEnum type)
        {
            long permissionTypeID = getPermissionType(type).ID;
            PermissionCacheItem cache = getPermissionCacheItem(userID);

            // if super admin, always return the root tree node...
            if (cache.IsSuperAdmin)
            {
                lock (_tree)
                {
                    return new List<long> { (from t in _tree.Values where t.Parent == null select t.ID).First() };
                }
            }

            //get all accounts where the user has the permission
            List<long> accountIDs = (from c in cache.Permissions where c.PermissionTypeID.Equals(permissionTypeID) select c.AccountID).ToList();

            //0 accounts shortcut, always none
            if (accountIDs.Count() == 0)
                return new List<long>();

            //1 account shortcut, always the one
            if (accountIDs.Count() == 1)
                return new List<long>() { accountIDs[0] };

            lock (_tree)
            {
                //get the top level for these accounts
                int maxLevel = (from t in _tree where accountIDs.Contains(t.Key) select t.Value.Depth).Min();

                //return those matching this level
                return (from t in _tree where accountIDs.Contains(t.Key) && t.Value.Depth.Equals(maxLevel) select t.Key).Distinct().ToList();
            }

        }

        public List<long> GetAccountsWithUserPermissionAndFeature(long userID, PermissionTypeEnum permission, FeatureTypeEnum feature)
        {

            List<long> pAccounts = AccountsWithPermission(userID, permission);
            List<long> fAccounts = GetAccountsWithFeature(feature);

            return (
                from p in pAccounts
                where fAccounts.Contains(p)
                select p
            ).ToList();

        }

        #endregion

        #region features

        public List<long> GetAccountsWithFeature(FeatureTypeEnum featureType)
        {
            using (MainEntities db = ContextFactory.Main)
            {
                return (from l in db.spGetAccountsWithFeature((long)featureType) select l.Value).ToList();
            }
        }

        private List<AccountFeature> getFeaturesForAccounts(List<long> accountIDs)
        {
            using (MainEntities db = ContextFactory.Main)
            {
                return (
                    from l in db.spGetFeaturesForAccounts(string.Join("|", accountIDs.ToArray()))
                    select new AccountFeature()
                    {
                        AccountID = l.AccountID,
                        FeatureType = (FeatureTypeEnum)l.FeatureTypeID
                    }
               ).ToList();
            }
        }

        public bool HasFeature(long accountID, FeatureTypeEnum featureType)
        {
            using (MainEntities db = ContextFactory.Main)
            {
                //return db.spAccountHasFeature(accountID, (long)featureType).FirstOrDefault() > 0;
                if (db.Connection.State != System.Data.ConnectionState.Open)
                    db.Connection.Open();

                using (DbCommand cmd = db.CreateStoreCommand(
                                "spAccountHasFeature",
                                System.Data.CommandType.StoredProcedure,
                                new SqlParameter("@AccountID", accountID),
                                new SqlParameter("@FeatureTypeID", (long)featureType)
                           )
                      )
                {
                    using (DbDataReader reader = cmd.ExecuteReader())
                    {
                        if (reader.Read())
                        {
                            return Util.GetReaderValue<bool>(reader, "HasFeature", false);
                        }
                    }
                }
            }

            return false;
        }


        private class AccountFeature
        {
            public long AccountID { get; set; }
            public FeatureTypeEnum FeatureType { get; set; }
        }

        #endregion

        #region resellers

        // a cache of resellers.  the key is the account id of the reseller.
        private static object _resellerCacheLock = new object();
        private static Dictionary<long, Reseller> _resellerCache = null;
        public ResellerDTO GetResellerByAccountID(UserInfoDTO userInfo, long accountID, TimeZoneInfo tz)
        {
            //long resellerAccountID = findAccountResellerAncestor(accountID);
            //if (resellerAccountID == 0)
            //    return null;

            using (MainEntities db = ContextFactory.Main)
            {
                ObjectParameter output = new ObjectParameter("ResellerID", typeof(long));

                long resellerID;

                try
                {

                    db.spGetAccountReseller(accountID, output);
                    if (output == null) return null;
                    if (output.Value == null) return null;
                    if (output.Value.GetType() == typeof(System.DBNull)) return null;
                    if (output.Value.GetType() != typeof(long)) return null;
                    resellerID = (long)output.Value;

                }
                catch (Exception ex)
                {
                    ProviderFactory.Logging.LogException(ex);
                    return null;
                }

                lock (_resellerCacheLock)
                {
                    if (_resellerCache == null)
                        reloadResellerCache();
                    ResellerDTO reseller = (
                        from r in _resellerCache
                        where r.Value.ID == resellerID
                        select new ResellerDTO()
                        {
                            ID = r.Value.ID,
                            AccountID = r.Value.AccountID,
                            ThemeName = r.Value.ThemeName,
                            DateCreated = new SIDateTime(r.Value.DateCreated, tz)
                        }
                    ).FirstOrDefault();

                    return reseller;

                }
            }


        }
        private void reloadResellerCache()
        {
            _resellerCache = new Dictionary<long, Reseller>();
            using (MainEntities db = ContextFactory.Main)
            {
                List<Reseller> resellers = (from r in db.Resellers select r).ToList();
                foreach (Reseller reseller in resellers)
                {
                    _resellerCache.Add(reseller.AccountID, reseller);
                }
            }
        }
        #endregion

        #region general access tokens

        public AccessTokenBaseDTO GetAccessToken(Guid token, bool logUse)
        {
            using (MainEntities db = ContextFactory.Main)
            {
                AccessToken accessToken = (from t in db.AccessTokens where t.Token == token select t).FirstOrDefault();
                if (accessToken != null)
                {
                    AccessTokenBaseDTO baseDTO = null;
                    switch ((AccessTokenTypeEnum)accessToken.AccessTokenTypeID)
                    {
                        case AccessTokenTypeEnum.PasswordReset:
                            baseDTO = new PasswordResetAccessTokenDTO(accessToken.DataPacket);
                            break;
                        case AccessTokenTypeEnum.PostApproval:
                            baseDTO = new PostApprovalAccessTokenDTO(accessToken.DataPacket);
                            break;
                        case AccessTokenTypeEnum.SyndicationInvitation:
                            baseDTO = new SyndicationInviteAccessTokenDTO(accessToken.DataPacket);
                            break;
                        case AccessTokenTypeEnum.SyndicationUnsubscribe:
                            baseDTO = new SyndicationUnsubscribeDTO(accessToken.DataPacket);
                            break;
                        case AccessTokenTypeEnum.SyndicationSelfRegistration:
                            baseDTO = new SyndicationSelfRegistrationDTO(accessToken.DataPacket);
                            break;
                        case AccessTokenTypeEnum.SocialProductConfig:
                            baseDTO = new SocialProductConfigAccessTokenDTO(accessToken.DataPacket);
                            break;
                        case AccessTokenTypeEnum.PostApprovalHistory:
                            baseDTO = new PostApprovalHistoryAccessTokenDTO(accessToken.DataPacket);
                            break;
                    }

                    if (baseDTO != null)
                    {
                        baseDTO.ID = accessToken.ID;
                        baseDTO.RemainingUses = accessToken.RemainingUses;
                        baseDTO.Token = accessToken.Token;
                        baseDTO.UserID = accessToken.UserID;
                        if (accessToken.DateExpires.HasValue)
                        {
                            baseDTO.TimeTillExpiration = accessToken.DateExpires.Value - DateTime.UtcNow;
                        }

                        if (logUse && accessToken.RemainingUses.HasValue)
                        {
                            accessToken.RemainingUses = accessToken.RemainingUses.Value - 1;
                            db.SaveChanges();
                        }

                        return baseDTO;
                    }
                }
            }

            return null;
        }

        public AccessTokenBaseDTO CreateAccessToken(AccessTokenBaseDTO accessToken)
        {
            using (MainEntities db = ContextFactory.Main)
            {
                AccessToken token = new AccessToken()
                {
                    AccessTokenTypeID = (long)accessToken.Type(),
                    DateCreated = DateTime.UtcNow,
                    RemainingUses = accessToken.RemainingUses,
                    DataPacket = accessToken.GetDataPacket(),
                    UserID = accessToken.UserID,
                    Token = Guid.NewGuid()
                };

                if (accessToken.TimeTillExpiration.HasValue)
                {
                    token.DateExpires = DateTime.UtcNow.Add(accessToken.TimeTillExpiration.Value);
                }

                db.AccessTokens.AddObject(token);
                db.SaveChanges();

                return GetAccessToken(token.Token, false);
            }
        }

        #endregion

        #region user preferrences

        public UserPreferencesDTO GetUserPreferences(UserInfoDTO userInfo)
        {
            UserPreferencesDTO dto = new UserPreferencesDTO();

            dto.NotificationsEnabled = GetUserSetting(userInfo, "notifications", "on") == "on";

            return dto;
        }
        public void SaveUserPreferences(UserInfoDTO userInfo, UserPreferencesDTO dto)
        {
            UserPreferencesDTO oldPrefs = GetUserPreferences(userInfo);
            SaveUserSetting(userInfo, "notifications", dto.NotificationsEnabled ? "on" : "off");
            if (oldPrefs.NotificationsEnabled != dto.NotificationsEnabled)
            {
                ProviderFactory.Notifications.AddSubscriptionCheck(userInfo.EffectiveUser.ID.Value, null);
            }
        }

        #endregion

        #region virtual groups

        public List<VirtualGroupTypeDTO> GetVirtualGroupTypes()
        {
            using (MainEntities db = ContextFactory.Main)
            {
                List<VirtualGroupTypeDTO> list = new List<VirtualGroupTypeDTO>();
                foreach (VirtualGroupType type in db.VirtualGroupTypes)
                {
                    VirtualGroupTypeDTO dto = new VirtualGroupTypeDTO()
                    {
                        ID = type.ID,
                        Name = type.Name,
                        IsSystem = type.IsSystem
                    };
                    list.Add(dto);
                }
                return list;
            }
        }

        public SaveEntityDTO<VirtualGroupTypeDTO> Save(SaveEntityDTO<VirtualGroupTypeDTO> dto)
        {
            // must be internal admin to edit virtual group types
            if (!HasPermission(dto.UserInfo.EffectiveUser.ID.Value, PermissionTypeEnum.internal_admin))
            {
                dto.Problems.Add(Locale.Localize("system.nopermission"));
                return dto;
            }

            using (MainEntities db = ContextFactory.Main)
            {
                if (dto.Entity.ID.GetValueOrDefault(0) > 0)
                {

                    VirtualGroupType type = (from t in db.VirtualGroupTypes where t.ID == dto.Entity.ID.Value select t).FirstOrDefault();
                    type.Name = dto.Entity.Name;
                    type.IsSystem = dto.Entity.IsSystem;
                    db.SaveChanges();
                }
                else
                {
                    VirtualGroupType type = new VirtualGroupType();
                    type.Name = dto.Entity.Name;
                    type.IsSystem = dto.Entity.IsSystem;
                    db.VirtualGroupTypes.AddObject(type);
                    db.SaveChanges();
                    dto.Entity.ID = type.ID;
                }

                return dto;
            }
        }

        public List<VirtualGroupDTO> GetVirtualGroups(UserInfoDTO userInfo)
        {
            ITimezoneProvider tz = ProviderFactory.TimeZones;
            TimeZoneInfo timeZone = tz.GetTimezoneForUserID(userInfo.EffectiveUser.ID.Value);

            // if they have vg admin on at least one account, they can edit any vg they can see
            bool canEdit = HasPermission(userInfo.EffectiveUser.ID.Value, PermissionTypeEnum.virtualGroups_admin);

            List<long> accountIDs =
                AccountListBuilder
                    .New(new SecurityProvider())
                    .AddAccountsWithPermission(userInfo, PermissionTypeEnum.accounts_view)
                    .GetIDs(SecurityProvider.PLATFORM_MAX_ACCOUNT_ID_LIST)
            ;

            using (MainEntities db = ContextFactory.Main)
            {
                List<VirtualGroup> allgroups = db.VirtualGroups.ToList();
                List<VirtualGroup> groups = (
                    from vg in allgroups
                    join avg in db.Accounts_VirtualGroups on vg.ID equals avg.VirtualGroupID
                    where (accountIDs.Contains(avg.AccountID) && (vg.AccountID.HasValue == false)) || (vg.AccountID.HasValue && accountIDs.Contains(vg.AccountID.Value))
                    select vg
                ).Distinct().ToList();

                List<long> vgids = new List<long>();
                List<VirtualGroupDTO> list = new List<VirtualGroupDTO>();
                foreach (VirtualGroup group in groups)
                {
                    if (!vgids.Contains(group.ID))
                    {
                        vgids.Add(group.ID);
                        list.Add(new VirtualGroupDTO()
                        {
                            DateCreated = new SIDateTime(group.DateCreated, timeZone),
                            ID = group.ID,
                            IsSystem = group.IsSystem,
                            VirtualGroupTypeID = group.VirtualGroupTypeID,
                            Name = group.Name,
                            EditableByUser = canEdit
                        });
                    }
                }

                return list;
            }
        }

        public SaveEntityDTO<VirtualGroupDTO> Save(SaveEntityDTO<VirtualGroupDTO> dto)
        {
            bool isInternalAdmin = HasPermission(dto.UserInfo.EffectiveUser.ID.Value, PermissionTypeEnum.internal_admin);

            bool newGroup = dto.Entity.ID.GetValueOrDefault(0) == 0;
            if (!HasPermission(dto.UserInfo.EffectiveUser.ID.Value, PermissionTypeEnum.virtualGroups_create) && newGroup)
            {
                dto.Problems.Add(Locale.Localize("system.nopermission"));
                return dto;
            }
            using (MainEntities db = ContextFactory.Main)
            {

                if (newGroup)
                {
                    VirtualGroup group = new VirtualGroup()
                    {
                        DateCreated = DateTime.UtcNow,
                        IsSystem = dto.Entity.IsSystem,
                        VirtualGroupTypeID = dto.Entity.VirtualGroupTypeID,
                        Name = dto.Entity.Name
                    };
                    db.VirtualGroups.AddObject(group);
                    db.SaveChanges();
                    dto.Entity.ID = group.ID;
                    return dto;
                }
                else
                {
                    VirtualGroup group = (from vg in db.VirtualGroups where vg.ID == dto.Entity.ID.Value select vg).FirstOrDefault();
                    if (group.IsSystem.GetValueOrDefault(true) && !isInternalAdmin)
                    {
                        dto.Problems.Add(Locale.Localize("system.nopermission"));
                        return dto;
                    }

                    group.IsSystem = dto.Entity.IsSystem;
                    group.VirtualGroupTypeID = dto.Entity.VirtualGroupTypeID;
                    group.Name = dto.Entity.Name;
                    db.SaveChanges();
                    return dto;
                }
            }
        }

        public VirtualGroupActionResultDTO VirtualGroupAddAccounts(UserInfoDTO userInfo, long virtualGroupID, List<long> accountIDs)
        {
            VirtualGroupActionResultDTO result = new VirtualGroupActionResultDTO();

            foreach (long accountID in accountIDs)
            {
                if (!HasPermission(userInfo.EffectiveUser.ID.Value, accountID, PermissionTypeEnum.virtualGroups_admin))
                {
                    result.Problems.Add(Locale.Localize("system.nopermission"));
                    return result;
                }
            }

            using (MainEntities db = ContextFactory.Main)
            {
                List<long> alreadyAdded = (
                    from av in db.Accounts_VirtualGroups
                    where accountIDs.Contains(av.AccountID)
                    && av.VirtualGroupID == virtualGroupID
                    select av.AccountID
                ).ToList();

                foreach (long accountID in accountIDs)
                {
                    if (!alreadyAdded.Contains(accountID))
                    {
                        Accounts_VirtualGroups avg = new Accounts_VirtualGroups()
                        {
                            AccountID = accountID,
                            VirtualGroupID = virtualGroupID
                        };
                        db.Accounts_VirtualGroups.AddObject(avg);
                    }
                }
                db.SaveChanges();
            }

            return result;
        }

        public VirtualGroupActionResultDTO VirtualGroupRemoveAccounts(UserInfoDTO userInfo, long virtualGroupID, List<long> accountIDs)
        {
            VirtualGroupActionResultDTO result = new VirtualGroupActionResultDTO();

            foreach (long accountID in accountIDs)
            {
                if (!HasPermission(userInfo.EffectiveUser.ID.Value, accountID, PermissionTypeEnum.virtualGroups_admin))
                {
                    result.Problems.Add(Locale.Localize("system.nopermission"));
                    return result;
                }
            }

            using (MainEntities db = ContextFactory.Main)
            {
                List<Accounts_VirtualGroups> toRemove = (
                    from av in db.Accounts_VirtualGroups
                    where accountIDs.Contains(av.AccountID)
                    && av.VirtualGroupID == virtualGroupID
                    select av
                ).ToList();

                foreach (Accounts_VirtualGroups avg in toRemove)
                {
                    db.Accounts_VirtualGroups.DeleteObject(avg);
                }
                db.SaveChanges();
            }

            return result;
        }

        public List<long> GetVirtualGroupAccountIDs(UserInfoDTO userInfo, long virtualGroupID)
        {
            List<long> aids = AccountListBuilder
                .New(this)
                .AddAccountsWithPermission(userInfo, PermissionTypeEnum.accounts_view)
                .GetIDs(SecurityProvider.PLATFORM_MAX_ACCOUNT_ID_LIST)
            ;

            using (MainEntities db = ContextFactory.Main)
            {
                List<long> accountIDs = (
                    from vg in db.Accounts_VirtualGroups
                    where vg.VirtualGroupID == virtualGroupID
                    select vg.AccountID
                ).ToList();

                accountIDs = (from a in accountIDs where aids.Contains(a) select a).ToList();

                return accountIDs;
            }
        }

        internal List<long> GetVirtualGroupAccountIDs(long virtualGroupID)
        {
            using (MainEntities db = ContextFactory.Main)
            {
                List<long> accountIDs = (
                    from vg in db.Accounts_VirtualGroups
                    where vg.VirtualGroupID == virtualGroupID
                    select vg.AccountID
                ).ToList();

                return accountIDs;
            }
        }


        #endregion

        #region SendGrid

        public SendGridEmailFailureDTO SaveSendgridEmailFailures(string emailAddress, SendGridStatusEnum sendGridStatus, DateTime dateCreated)
        {
            using (MainEntities db = ContextFactory.Main)
            {
                // get the user that pertains to this email address
                long? userID = (
                    from u in db.Users
                    where u.EmailAddress != null
                    && u.EmailAddress.Trim().ToLower() == emailAddress.Trim().ToLower()
                    select u.ID
                ).FirstOrDefault();

                string emailHash = Security.GetHash(string.Format("{0}-{1}", emailAddress, (long)sendGridStatus), 20);

                SendgridEmailFailure sendgridEmailFailure = (from s in db.SendgridEmailFailures
                                                             where s.EmailHash == emailHash
                                                             select s).FirstOrDefault();

                if (userID.HasValue && userID.Value > 0)
                {
                    AuditUserActivityTypeEnum type = AuditUserActivityTypeEnum.SendGridBouncedEmailAddress;
                    switch (sendGridStatus)
                    {
                        case SendGridStatusEnum.HardBounced:
                        case SendGridStatusEnum.Softbounced:
                            type = AuditUserActivityTypeEnum.SendGridBouncedEmailAddress;
                            break;
                        case SendGridStatusEnum.Blocked:
                            type = AuditUserActivityTypeEnum.SendGridBlockedEmailAddress;
                            break;
                        case SendGridStatusEnum.MarkedInvalid:
                            type = AuditUserActivityTypeEnum.SengGridMarkedEmailAddressInvalid;
                            break;
                        default:
                            break;
                    }

                    Auditor
                        .New(AuditLevelEnum.Information, AuditTypeEnum.UserActivity, UserInfoDTO.System, "")
                        .SetAuditDataObject(
                            AuditUserActivity
                                .New(UserInfoDTO.System, type)
                                .SetUserIDActedUpon(userID.Value)
                                .SetDescription("Email Address {0} Added to SendgridEmailFailure List Status: {1}",
                                                emailAddress, sendGridStatus.ToString())
                        )
                    .Save(ProviderFactory.Logging);

                }
                if (sendgridEmailFailure == null)
                {
                    //Insert
                    sendgridEmailFailure = new SendgridEmailFailure();
                    dateCreated = dateCreated.ToUniversalTime();
                    sendgridEmailFailure.DateCreated = dateCreated;
                    sendgridEmailFailure.EmailAddress = emailAddress;
                    sendgridEmailFailure.EmailHash = emailHash;
                    sendgridEmailFailure.PrevouslyFound = false;
                    switch (sendGridStatus)
                    {
                        case SendGridStatusEnum.HardBounced:
                            sendgridEmailFailure.ProcessingStatusID = (long)SendGridProcessingStatusEnum.Queued;
                            break;
                        case SendGridStatusEnum.Softbounced:
                            sendgridEmailFailure.ProcessingStatusID = (long)SendGridProcessingStatusEnum.Queued;
                            break;
                        case SendGridStatusEnum.Blocked:
                            sendgridEmailFailure.ProcessingStatusID = (long)SendGridProcessingStatusEnum.Ignored;
                            break;
                        case SendGridStatusEnum.MarkedInvalid:
                            sendgridEmailFailure.ProcessingStatusID = (long)SendGridProcessingStatusEnum.Ignored;
                            break;
                        default:
                            break;
                    }
                    sendgridEmailFailure.SendGridStatusID = (long)sendGridStatus;

                    db.SendgridEmailFailures.AddObject(sendgridEmailFailure);
                    db.SaveChanges();

                }
                else
                {
                    //Update
                    sendgridEmailFailure.PrevouslyFound = true;
                    switch (sendGridStatus)
                    {
                        case SendGridStatusEnum.HardBounced:
                            sendgridEmailFailure.ProcessingStatusID = (long)SendGridProcessingStatusEnum.Ignored;
                            break;
                        case SendGridStatusEnum.Softbounced:
                            sendgridEmailFailure.ProcessingStatusID = (long)SendGridProcessingStatusEnum.Ignored;
                            break;
                        case SendGridStatusEnum.Blocked:
                            sendgridEmailFailure.ProcessingStatusID = (long)SendGridProcessingStatusEnum.Ignored;
                            break;
                        case SendGridStatusEnum.MarkedInvalid:
                            sendgridEmailFailure.ProcessingStatusID = (long)SendGridProcessingStatusEnum.Ignored;
                            break;
                        default:
                            break;
                    }
                    db.SaveChanges();
                }

                return new SendGridEmailFailureDTO()
                {
                    DateCreated = sendgridEmailFailure.DateCreated.Value,
                    EmailAddress = sendgridEmailFailure.EmailAddress,
                    PreviouslyFound = sendgridEmailFailure.PrevouslyFound.Value,
                    ProcessingStatus = (SendGridProcessingStatusEnum)sendgridEmailFailure.ProcessingStatusID,
                    SendGridStatus = (SendGridStatusEnum)sendgridEmailFailure.SendGridStatusID
                };

            }
        }

        public SendGridEmailFailureDTO SetSendgridEmailProcessingStatus(string emailAddress, SendGridStatusEnum sendGridStatus, SendGridProcessingStatusEnum processingStatus)
        {
            try
            {
                using (MainEntities db = ContextFactory.Main)
                {
                    string emailHash = Security.GetHash(string.Format("{0}-{1}", emailAddress, (long)sendGridStatus), 20);

                    SendgridEmailFailure failure = (
                        from f in db.SendgridEmailFailures
                        where f.EmailHash == emailHash
                        select f
                    ).FirstOrDefault();

                    if (failure != null)
                    {
                        failure.ProcessingStatusID = (long)processingStatus;
                        db.SaveChanges();

                        return new SendGridEmailFailureDTO()
                        {
                            DateCreated = failure.DateCreated.Value,
                            EmailAddress = failure.EmailAddress,
                            PreviouslyFound = failure.PrevouslyFound.Value,
                            ProcessingStatus = (SendGridProcessingStatusEnum)failure.ProcessingStatusID,
                            SendGridStatus = (SendGridStatusEnum)failure.SendGridStatusID
                        };
                    }
                    else
                    {
                        return null;
                    }


                }
            }
            catch (Exception ex)
            {
                ProviderFactory.Logging.LogException(ex);
                return null;
            }
        }

        public List<SendGridEmailFailureDTO> GetSendGridEmailFailures(SendGridProcessingStatusEnum processingStatus)
        {
            List<SendGridEmailFailureDTO> toReturn = new List<SendGridEmailFailureDTO>();
            using (MainEntities db = ContextFactory.Main)
            {
                List<SendgridEmailFailure> failures = (
                    from f in db.SendgridEmailFailures
                    where f.ProcessingStatusID == (long)processingStatus
                    select f
                ).ToList();

                foreach (SendgridEmailFailure item in failures)
                {
                    SendGridEmailFailureDTO failure = new SendGridEmailFailureDTO()
                    {
                        DateCreated = item.DateCreated.GetValueOrDefault(DateTime.Now),
                        EmailAddress = item.EmailAddress,
                        PreviouslyFound = item.PrevouslyFound.GetValueOrDefault(false),
                        ProcessingStatus = (SendGridProcessingStatusEnum)item.ProcessingStatusID,
                        SendGridStatus = (SendGridStatusEnum)item.SendGridStatusID
                    };

                    failure.MatchedUserID = (
                        from u in db.Users
                        where u.EmailAddress != null
                        && u.EmailAddress == failure.EmailAddress
                        select u.ID
                    ).FirstOrDefault();

                    toReturn.Add(failure);
                }

                return toReturn;
            }
        }

        public void GenerateSendGridCollectionReport(List<SendGridEmailFailureDTO> items)
        {
            string template = Util.GetResourceTextFile("SendGridCollectionReport.html", Assembly.GetAssembly(typeof(ProviderFactory)));

            int newBounces = (from i in items where i.PreviouslyFound == false && (i.SendGridStatus == SendGridStatusEnum.HardBounced || i.SendGridStatus == SendGridStatusEnum.Softbounced) select i).Count();
            int newBlocks = (from i in items where i.PreviouslyFound == false && i.SendGridStatus == SendGridStatusEnum.Blocked select i).Count();
            int newInvalids = (from i in items where i.PreviouslyFound == false && i.SendGridStatus == SendGridStatusEnum.MarkedInvalid select i).Count();

            int oldBounces = (from i in items where i.PreviouslyFound == true && (i.SendGridStatus == SendGridStatusEnum.HardBounced || i.SendGridStatus == SendGridStatusEnum.Softbounced) select i).Count();
            int oldBlocks = (from i in items where i.PreviouslyFound == true && i.SendGridStatus == SendGridStatusEnum.Blocked select i).Count();
            int oldInvalids = (from i in items where i.PreviouslyFound == true && i.SendGridStatus == SendGridStatusEnum.MarkedInvalid select i).Count();

            template = template.Replace("[new_bounces]", newBounces.ToString());
            template = template.Replace("[new_blocks]", newBlocks.ToString());
            template = template.Replace("[new_invalids]", newInvalids.ToString());
            template = template.Replace("[old_bounces]", oldBounces.ToString());
            template = template.Replace("[old_blocks]", oldBlocks.ToString());
            template = template.Replace("[old_invalids]", oldInvalids.ToString());

            StringBuilder rep = new StringBuilder();
            foreach (SendGridEmailFailureDTO item in items)
            {
                rep.Append("<tr>");
                rep.AppendFormat("<td>{0}</td>", item.EmailAddress);
                rep.AppendFormat("<td>{0}</td>", item.SendGridStatus.ToString());
                rep.AppendFormat("<td>{0}</td>", item.ProcessingStatus.ToString());
                rep.AppendFormat("<td>{0}</td>", item.PreviouslyFound ? "true" : "false");
                rep.AppendFormat("<td>{0}</td>", item.DateCreated.ToString("MM/dd/yyyy hh:mm:ss tt"));
                rep.Append("</tr>");
            }

            template = template.Replace("[detail]", rep.ToString());

            EmailManager emailManager = new EmailManager();
            bool sendResult = emailManager.sendNotification(
                "Diagnostics",
                "SendGrid Phase 1 Collection Diagnostics Report",
                template,
                Settings.GetSetting("site.SendFromEmailAddress", "notifications@socialdealer.com"),
                "reynolds@autostartups.com",
                "s.abraham@socialdealer.com",
                null
            );

        }
        public void GenerateSendGridActionReport(List<SendGridEmailFailureDTO> items)
        {
            string template = Util.GetResourceTextFile("SendGridActionReport.html", Assembly.GetAssembly(typeof(ProviderFactory)));

            StringBuilder rep = new StringBuilder();
            foreach (SendGridEmailFailureDTO item in items)
            {
                rep.Append("<tr>");
                rep.AppendFormat("<td>{0}</td>", item.EmailAddress);
                rep.AppendFormat("<td>{0}</td>", item.SendGridStatus.ToString());
                rep.AppendFormat("<td>{0}</td>", item.ProcessingStatus.ToString());
                rep.AppendFormat("<td>{0}</td>", item.PreviouslyFound ? "true" : "false");
                rep.AppendFormat("<td>{0}</td>", item.DateCreated.ToString("MM/dd/yyyy hh:mm:ss tt"));
                rep.Append("</tr>");
            }

            template = template.Replace("[detail]", rep.ToString());

            EmailManager emailManager = new EmailManager();
            bool sendResult = emailManager.sendNotification(
                "Diagnostics",
                "SendGrid Phase 2 Collection Diagnostics Report",
                template,
                Settings.GetSetting("site.SendFromEmailAddress", "notifications@socialdealer.com"),
                "reynolds@autostartups.com",
                "s.abraham@socialdealer.com",
                null
            );
        }

        #endregion

        //this is extremely dangerous!!!
        public void ResetDatabase()
        {
            //using (MainEntities db = ContextFactory.Main)
            //{
            //    db.spResetData();
            //}
        }

        public void FlushAccountAndPermissionCache()
        {
            lock (_permLock)
            {
                _permsByCode = null;
                _permsByID = null;
            }

            using (MainEntities db = ContextFactory.Main)
            {
                db.spApplyRoles(null);
                reloadTree();
                reloadResellerCache();
            }
        }

        private void execNonQuery(string sql)
        {
            using (MainEntities db = ContextFactory.Main)
            {
                if (db.Connection.State != System.Data.ConnectionState.Open)
                    db.Connection.Open();

                using (DbCommand cmd = db.CreateStoreCommand(sql, System.Data.CommandType.Text))
                {
                    cmd.ExecuteNonQuery();
                }
            }
        }




    }
}


