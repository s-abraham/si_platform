﻿using SI.BLL.Extensions;
using SI.DAL;
using SI.DTO;
using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Data.Extensions;
using System.Data.SqlClient;
using SI.BLL.Implementation;

namespace SI.BLL
{
    public class ProductsProvider : IProductsProvider
    {
        public List<DTO.PackageDTO> GetAvailablePackagesForAccount(long accountID, DTO.UserInfoDTO userInfo)
        {
            //todo: check permissions to possibly limit results, also modify proc to limit packages by region or account configuration
            ITimezoneProvider tz = ProviderFactory.TimeZones;
            TimeZoneInfo timeZone = tz.GetTimezoneForUserID(userInfo.EffectiveUser.ID.Value);

            List<DTO.PackageDTO> packages = new List<DTO.PackageDTO>();

            using (MainEntities db = ContextFactory.Main)
            {
                foreach (spGetAvailablePackagesForAccount_Result item in db.spGetAvailablePackagesForAccount(accountID))
                {
                    packages.Add(new DTO.PackageDTO()
                    {
                        PackageID = item.PackageID,
                        ResellerID = item.ResellerID,
                        ResellerName = item.ResellerName,
                        PackageName = item.PackageName,
                        ID = item.Resellers_PackagesID,
                        DateCreated = new SIDateTime(item.DateCreated, timeZone)
                    });
                }
            }

            return packages;
        }

        private List<PackageDTO> _Packages = null;
        public List<PackageDTO> GetPackages()
        {
            using (MainEntities db = ContextFactory.Main)
            {
                _Packages = (
                   from i in db.Packages
                   orderby i.Name
                   select new PackageDTO()
                   {
                       PackageID = i.ID,
                       PackageName = i.Name,
                   }
                ).ToList();
            }

            return _Packages;
        }

        public List<AccountPackageListItemDTO> GetSubscribedPackagesForAccount(long accountID, DTO.UserInfoDTO userInfo)
        {
            //todo: check permissions to possibly limit results

            ITimezoneProvider tz = ProviderFactory.TimeZones;
            TimeZoneInfo timeZone = tz.GetTimezoneForUserID(userInfo.EffectiveUser.ID.Value);

            List<AccountPackageListItemDTO> packages = new List<AccountPackageListItemDTO>();

            using (MainEntities db = ContextFactory.Main)
            {
                foreach (spGetSubscribedPackages_Result item in db.spGetSubscribedPackages(accountID))
                {
                    packages.Add(new AccountPackageListItemDTO()
                    {
                        AccountID = item.AccountID,
                        SubscriptionID = item.SubscriptionID,
                        PackageID = item.PackageID,
                        PackageName = item.PackageName,
                        ResellerName = item.ResellerName,
                        ResellerID = item.ResellerID,
                        DateAdded = new SIDateTime(item.DateAdded, timeZone)
                    });
                }
            }

            return packages;
        }

        public UpdatePackageResultDTO AddPackage(long accountID, long resellerID, long packageID, DTO.UserInfoDTO userInfo, decimal? price, int trialDays)
        {
            //todo: check permissions to see if user is authorized to add packages

            ITimezoneProvider tz = ProviderFactory.TimeZones;
            TimeZoneInfo timeZone = tz.GetTimezoneForUserID(userInfo.EffectiveUser.ID.Value);

            UpdatePackageResultDTO result = new UpdatePackageResultDTO() { Outcome = UpdatePackageOutcome.Failure };

            using (MainEntities db = ContextFactory.Main)
            {
                //find the reseller package
                Resellers_Packages rp = (from repac in db.Resellers_Packages where repac.ResellerID == resellerID && repac.PackageID == packageID && repac.Active == true select repac).FirstOrDefault();
                if (rp == null)
                {
                    result.Problems.Add("The specified reseller is not authorized to sell the specified package.");
                    return result;
                }

                //find an existing subscription to this package for the account

                Subscription sub = (from s in db.Subscriptions where s.AccountID == accountID && s.Reseller_PackageID == rp.ID select s).FirstOrDefault();

                if (sub != null)
                {
                    if (sub.Active == false) //re-enable
                    {
                        sub.Active = true;
	                    sub.EndDate = null;
						if (trialDays > 0)
							sub.EndDate = DateTime.UtcNow.AddDays(trialDays);
                        sub.StartDate = DateTime.UtcNow;
                        sub.Price = price.GetValueOrDefault(0);
                        db.SaveChanges();

                        Auditor
                            .New(AuditLevelEnum.Information, AuditTypeEnum.UserActivity, userInfo, "")
                            .SetAuditDataObject(
                                AuditUserActivity
                                .New(userInfo, AuditUserActivityTypeEnum.SubscriptionChanged)
                                    .SetPackageID(packageID)
                                    .SetAccountID(accountID)
                                    .SetResellerID(resellerID)
                                    .SetSubscriptionID(sub.ID)
                                    .SetDescription("Subscription reactivated")
                            )
                            .Save(ProviderFactory.Logging)
                        ;

                        ProviderFactory.Notifications.AddSubscriptionCheck(null, accountID);

                        return new UpdatePackageResultDTO() { Outcome = UpdatePackageOutcome.Success };
                    }
                    else
                    {
                        //set the dates just in case...
						sub.EndDate = null;
						if (trialDays > 0)
							sub.EndDate = DateTime.UtcNow.AddDays(trialDays); 
						sub.StartDate = DateTime.UtcNow; 
                        db.SaveChanges();

                        Auditor
                            .New(AuditLevelEnum.Information, AuditTypeEnum.UserActivity, userInfo, "")
                            .SetAuditDataObject(
                                AuditUserActivity
                                .New(userInfo, AuditUserActivityTypeEnum.SubscriptionChanged)
                                    .SetPackageID(packageID)
                                    .SetAccountID(accountID)
                                    .SetResellerID(resellerID)
                                    .SetSubscriptionID(sub.ID)
                                    .SetDescription("Subscription already found, setting start and end dates to null.  Active.")
                            )
                            .Save(ProviderFactory.Logging)
                        ;

                        ProviderFactory.Notifications.AddSubscriptionCheck(null, accountID);

                        return new UpdatePackageResultDTO() { Outcome = UpdatePackageOutcome.Success };
                    }
                }
                else
                {
                    sub = new Subscription()
                    {
                        AccountID = accountID,
                        Reseller_PackageID = rp.ID,
						StartDate = DateTime.UtcNow,
                        EndDate = null,
                        Active = true,
                        Price = price.GetValueOrDefault(0),
                        DateCreated = DateTime.UtcNow
                    };
					if (trialDays > 0)
						sub.EndDate = DateTime.UtcNow.AddDays(trialDays); 

                    db.Subscriptions.AddObject(sub);
                    db.SaveChanges();
                    Auditor
                        .New(AuditLevelEnum.Information, AuditTypeEnum.UserActivity, userInfo, "")
                        .SetAuditDataObject(
                            AuditUserActivity
                            .New(userInfo, AuditUserActivityTypeEnum.SubscriptionAdded)
                                .SetPackageID(packageID)
                                .SetAccountID(accountID)
                                .SetResellerID(resellerID)
                                .SetSubscriptionID(sub.ID)
                        )
                        .Save(ProviderFactory.Logging)
                    ;

                    ProviderFactory.Notifications.AddSubscriptionCheck(null, accountID);

                    return new UpdatePackageResultDTO() { Outcome = UpdatePackageOutcome.Success };
                }

            }

        }

        public UpdatePackageResultDTO RemovePackage(long subscriptionID, DTO.UserInfoDTO userInfo)
        {
            //todo: permisisons to remove test

            UpdatePackageResultDTO result = new UpdatePackageResultDTO() { Outcome = UpdatePackageOutcome.Failure };

            using (MainEntities db = ContextFactory.Main)
            {
                Subscription sub = (from s in db.Subscriptions where s.ID == subscriptionID select s).FirstOrDefault();
                if (sub == null)
                {
                    result.Problems.Add("Subscription not found.");
                    return result;
                }

                Auditor
                    .New(AuditLevelEnum.Information, AuditTypeEnum.UserActivity, userInfo, "")
                    .SetAuditDataObject(
                        AuditUserActivity
                        .New(userInfo, AuditUserActivityTypeEnum.SubscriptionRemoved)
                            .SetSubscriptionID(sub.ID)
                    )
                    .Save(ProviderFactory.Logging)
                ;


                db.Subscriptions.DeleteObject(sub);
                db.SaveChanges();

                ProviderFactory.Notifications.AddSubscriptionCheck(null, sub.AccountID);

                return new UpdatePackageResultDTO() { Outcome = UpdatePackageOutcome.Success };
            }
        }


        public SignupSetupInfoDTO GetSignupInfo(SyndicationSelfRegistrationDTO token)
        {
            SignupSetupInfoDTO info = new SignupSetupInfoDTO();
            if (token.VerticalMarketID.HasValue)
            {
                info.VerticalMarkets = (from v in GetVerticalMarkets() where v.ID == token.VerticalMarketID select v).ToList();
                if (!info.VerticalMarkets.Any())
                    info.VerticalMarkets = GetVerticalMarkets();
            }
            else
            {
                info.VerticalMarkets = GetVerticalMarkets();
            }

            info.Countries = ProviderFactory.Lists.GetCountries();

            using (MainEntities db = ContextFactory.Main)
            {
                if (db.Connection.State != System.Data.ConnectionState.Open)
                    db.Connection.Open();

                using (DbCommand cmd = db.CreateStoreCommand(
                    "spGetResellerPackageInfos", System.Data.CommandType.StoredProcedure,
                    new SqlParameter("@ResellerPackageIDs", string.Join("|", token.PackageIDs))
                ))
                {
                    using (DbDataReader reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            info.PackageInfos.Add(new SignupPackageInfoDTO()
                            {
                                ResellerPackageID = Util.GetReaderValue<long>(reader, "ResellerPackageID", 0),
                                PackageID = Util.GetReaderValue<long>(reader, "PackageID", 0),
                                ResellerID = Util.GetReaderValue<long>(reader, "ResellerID", 0),
                                PackageName = Util.GetReaderValue<string>(reader, "PackageName", ""),
                                ResellerName = Util.GetReaderValue<string>(reader, "ResellerName", ""),
                                Default = false,
                                Price = null
                            });
                        }
                    }
                }
            }

            if (token.Prices != null && token.Prices.Any())
            {
                for (int x = 0; x < token.Prices.Length; x++)
                {
                    if (token.PackageIDs.Length > x)
                    {
                        long rpID = token.PackageIDs[x];
                        decimal price = token.Prices[x];
                        SignupPackageInfoDTO pinfo = (from i in info.PackageInfos where i.ResellerPackageID == rpID select i).FirstOrDefault();
                        if (pinfo != null)
                        {
                            pinfo.Price = price;
                        }
                    }
                }
            }

            if (token.DefaultPackageIDs != null && token.DefaultPackageIDs.Any())
            {
                foreach (long defID in token.DefaultPackageIDs)
                {
                    SignupPackageInfoDTO pinfo = (from i in info.PackageInfos where i.ResellerPackageID == defID select i).FirstOrDefault();
                    if (pinfo != null)
                    {
                        pinfo.Default = true;
                    }
                }
            }

            return info;
        }

        public List<VerticalMarketDTO> GetVerticalMarkets()
        {
            using (MainEntities db = ContextFactory.Main)
            {
                return (
                    from v in db.Verticals
                    select new VerticalMarketDTO()
                    {
                        ID = v.ID,
                        Name = v.Name
                    }
                ).ToList();
            }
        }


        public SaveEntityDTO<SignupSubmitInfoDTO> SignupSubmit(SaveEntityDTO<SignupSubmitInfoDTO> dto)
        {
            dto.Problems.Clear();

            try
            {
                NotificationProvider np = (NotificationProvider)ProviderFactory.Notifications;
                np.sendSyndicationSignupSupportEmail(dto.Entity);
            }
            catch (Exception ex)
            {
                ProviderFactory.Logging.LogException(ex);
            }            

            SignupSubmitInfoDTO info = dto.Entity;
            SecurityProvider sec = new SecurityProvider();
            Dictionary<long, AccountTree> tree = sec.getAccountTree();

            using (MainEntities db = ContextFactory.Main)
            {
                long socialNetworkID = (long)info.SocialNetwork;
                long resellerAccountID = (from r in db.Resellers where r.ID == info.ResellerID select r.AccountID).FirstOrDefault();
                long preferredParentAccountID = info.PreferredParentAccountID.GetValueOrDefault(resellerAccountID);

                #region credential search

                // search for the unique id
                var credInfos = (
                    from c in db.Credentials
                    join a in db.Accounts on c.AccountID equals a.ID
                    join sa in db.SocialApps on c.SocialAppID equals sa.ID
                    join sn in db.SocialNetworks on sa.SocialNetworkID equals sn.ID
                    join addr in db.Addresses on a.BillingAddressID equals addr.ID
                    join cit in db.Cities on addr.CityID equals cit.ID
                    join zip in db.ZipCodes on cit.ZipCodeID equals zip.ID
                    where c.IsActive == true
                    && c.UniqueID == info.UniqueID
                    && sa.SocialNetworkID == socialNetworkID
                    select new
                    {
                        CredentialID = c.ID,
                        AccountID = c.AccountID,
                        AccountName = a.Name,
                        AccountDisplayName = a.DisplayName,
                        SocialAppID = sa.ID,
                        SocialNetworkName = sn.Name,
                        ParentAccountID = a.ParentAccountID,
                        CityName = cit.Name,
                        Zip = zip.ZipCode1,
                        AccountNameToCheck = (a.DisplayName == null && a.DisplayName == "") ? a.Name : a.DisplayName
                    }
                ).ToList();

                var credInfo = credInfos.FirstOrDefault();
                if (credInfos.Count() > 1)
                {
                    // display name
                    var check = (from c in credInfos where c.AccountDisplayName != null && c.AccountDisplayName == info.AccountName select c).ToList();
                    if (check.Count() == 1) credInfo = check[0];

                    // account name
                    if (credInfo == null)
                    {
                        check = (from c in credInfos where c.AccountName == info.AccountName select c).ToList();
                        if (check.Count() == 1) credInfo = check[0];
                    }

                    if (credInfo == null)
                    {
                        // TODO: contact support email with all information necessary to recreate the signup. (Use SupportUsers table, create a routine)
                        dto.Problems.Add("There were problems with your signup, our support staff will complete the process for you shortly.");
                        return dto;
                    }
                }

                long? accountID = null;

                if (credInfo != null)
                {
                    accountID = credInfo.AccountID;

                    Auditor
                        .New(AuditLevelEnum.Information, AuditTypeEnum.AccountActivity, UserInfoDTO.System, "")
                        .SetAuditDataObject(
                            AuditAccountActivity
                                .New(accountID.Value, AccountActivityTypeEnum.SignupMatched)
                                .SetDescription(
                                    "Matched account during signup on UniqueID [{0}], CredentialID [{1}] for {2} using SocialAppID [{3}]. AccessToken:{4}",
                                    info.UniqueID, credInfo.CredentialID, credInfo.SocialNetworkName, credInfo.SocialAppID, dto.Entity.AccessTokenUsed
                                )
                        )
                    .Save(ProviderFactory.Logging);

                    if (credInfo.ParentAccountID != resellerAccountID)
                    {
                        // 220171 is Edmunds
                        if ((from a in sec.GetAccountAncestors(accountID.Value) where a.ID == 220171 select a).Any())
                        {
                            ProviderFactory.Security.ChangeParentAccount(UserInfoDTO.System, accountID.Value, preferredParentAccountID, false);
                        }
                    }

                }

                #endregion

                #region specific naming search

                string accountName = string.Format("{0}-{1}-{2}", info.ScreenName.IsNullOptional("No Screen Name").Trim().ToLower(), info.UniqueID.ToLower(), info.SocialNetwork.ToString().ToLower());

                if (!accountID.HasValue)
                {
                    Account account = (from a in db.Accounts where a.Name == accountName select a).FirstOrDefault();
                    if (account != null)
                    {
                        accountID = account.ID;

                        Auditor
                            .New(AuditLevelEnum.Information, AuditTypeEnum.AccountActivity, UserInfoDTO.System, "")
                            .SetAuditDataObject(
                                AuditAccountActivity
                                    .New(accountID.Value, AccountActivityTypeEnum.SignupMatched)
                                    .SetDescription(
                                        "Matched account during signup on SocialNetwork based Name [{0}], Screen Name [{1}] for {2}. AccessToken:{3}",
                                        accountName, info.ScreenName, info.SocialNetwork.ToString(), dto.Entity.AccessTokenUsed
                                    )
                            )
                            .Save(ProviderFactory.Logging);

                        if (account.ParentAccountID.HasValue && account.ParentAccountID.Value != resellerAccountID)
                        {
                            // 220171 is Edmunds
                            if ((from a in sec.GetAccountAncestors(accountID.Value) where a.ID == 220171 select a).Any())
                            {
                                ProviderFactory.Security.ChangeParentAccount(UserInfoDTO.System, accountID.Value, preferredParentAccountID, false);
                            }
                        }
                    }
                }

                #endregion

                #region fuzzy search

                if (!accountID.HasValue)
                {
                    // find matching account name or display name
                    var alist = (
                        from a in db.Accounts
                        join ba in db.Addresses on a.BillingAddressID equals ba.ID
                        join c in db.Cities on ba.CityID equals c.ID
                        join z in db.ZipCodes on c.ZipCodeID equals z.ID
                        join ph in db.Phones on a.BillingPhoneID equals ph.ID
                        select new
                        {
                            AccountID = a.ID,
                            AccountName = a.Name,
                            AccountDisplayName = a.DisplayName,
                            CityID = c.ID,
                            CityName = c.Name,
                            Phone = ph.Number,
                            PostalCode = z.ZipCode1
                        }
                    ).ToList();

                    if (alist.Any())
                    {
                        foreach (var item in alist)
                        {
                            if (
                                (!string.IsNullOrWhiteSpace(item.AccountDisplayName) && item.AccountDisplayName.Trim().ToLower() == info.AccountName.Trim().ToLower())
                                ||
                                (item.AccountName.Trim().ToLower() == info.AccountName.Trim().ToLower())
                            )
                            {   // account name or display name matches

                                string pcode = info.PostalCode.ToLower().Replace(" ", "").Replace("-", "");
                                string pcode2 = item.PostalCode.ToLower().Replace(" ", "").Replace("-", "");
                                bool zipsMatch = (pcode == pcode2);

                                string phone = info.BillingPhone.ToLower().Replace("(", "").Replace(".", "").Replace("(", "").Replace("-", "").Replace(" ", "");
                                string phone2 = item.Phone.ToLower().Replace("(", "").Replace(".", "").Replace("(", "").Replace("-", "").Replace(" ", "");
                                bool phonesMatch = (phone == phone2);

                                string city = info.CityName.ToLower().Replace(" ", "");
                                string city2 = item.CityName.ToLower().Replace(" ", "");
                                bool citiesMatch = (city == city2);

                                if (
                                    (zipsMatch && (phonesMatch || citiesMatch))
                                    || (phonesMatch && citiesMatch)
                                    )
                                {
                                    //MATCH!

                                    accountID = item.AccountID;

                                    Auditor
                                        .New(AuditLevelEnum.Information, AuditTypeEnum.AccountActivity, UserInfoDTO.System, "")
                                        .SetAuditDataObject(
                                            AuditAccountActivity
                                                .New(accountID.Value, AccountActivityTypeEnum.SignupMatched)
                                                .SetDescription(
                                                    "Matched account during signup on Fuzzy search: PostalCodeMatch:{0}, PhoneMatch:{1}, CityMatch:{2}, AccountID:{3}, AccessToken:{4}",
                                                    zipsMatch.ToString(), phonesMatch.ToString(), citiesMatch.ToString(), item.AccountID, dto.Entity.AccessTokenUsed
                                                )
                                        )
                                    .Save(ProviderFactory.Logging);

                                    break;
                                }
                            }
                        }
                    }
                }

                #endregion

                #region create new account

                if (!accountID.HasValue)
                {
                    // create new account
                    AccountDTO newAccount = new AccountDTO()
                    {
                        OriginSystemIdentifier = string.Format("Signup.{0}.{1}", info.SocialNetwork.ToString(), info.ScreenName),
                        Name = accountName,
                        DisplayName = info.AccountName.Trim(),
                        CountryID = info.CountryID,
                        StateID = info.StateID,
                        PostalCode = info.PostalCode,
                        CityName = info.CityName,
                        EmailAddress = info.CompanyEmailAddress,
                        WebsiteURL = info.CompanyWebsiteURL,
                        FranchiseTypes = info.FranchiseTypeIDs.ToArray(),
                        IsDemoAccount = false,
                        ParentAccountID = preferredParentAccountID,
                        PhoneNumber = info.BillingPhone,
                        PostsRequireApproval = false,
                        StreetAddress1 = info.StreetAddress,
                        TimezoneID = info.TimezoneID,
                        Type = AccountTypeEnum.Company,
                        AccountTypeID = (long)AccountTypeEnum.Company,
						VerticalID = info.VerticalID
                       	
                    };

                    SaveEntityDTO<AccountDTO> entity = ProviderFactory.Security.Save(new SaveEntityDTO<AccountDTO>(newAccount, UserInfoDTO.System));

                    if (entity.Problems.Any())
                    {
                        dto.Problems.AddRange(entity.Problems);
                        return dto;
                    }

                    accountID = entity.Entity.ID.Value;

                    Auditor
                        .New(AuditLevelEnum.Information, AuditTypeEnum.AccountActivity, UserInfoDTO.System, "")
                        .SetAuditDataObject(
                            AuditAccountActivity
                                .New(accountID.Value, AccountActivityTypeEnum.SignupCreated)
                                .SetDescription(
                                    "Created new account during signup.  Name: [{0}], Display: [{1}], AccessToken:{2}",
                                    entity.Entity.Name, entity.Entity.DisplayName, dto.Entity.AccessTokenUsed
                                )
                        )
                    .Save(ProviderFactory.Logging);


                }

                #endregion

                if (!accountID.HasValue)
                {
                    dto.Problems.Add("Could not find or create an account!");
                    return dto;
                }


                #region user

                User user = (from u in db.Users where u.Username == info.UserEmailAddress.Trim() select u).FirstOrDefault();
                if (user == null)
                {
                    //create a new user account
                    user = new User()
                    {
                        AccountLocked = false,
                        DateCreated = DateTime.UtcNow,
                        DateModified = DateTime.UtcNow,
                        EmailAddress = info.UserEmailAddress.Trim(),
                        FirstName = info.UserFirstName.Trim(),
                        LastName = info.UserLastName.Trim(),
                        IsInternal = false,
                        InvalidLoginAttempts = 0,
                        MemberOfAccountID = accountID.Value,
                        Password = null,
                        RootAccountID = accountID.Value,
                        SSOSecretKey = Guid.NewGuid(),
                        SuperAdmin = false,
                        Username = info.UserEmailAddress.Trim(),
                        TimezoneID = null
                    };
                    db.Users.AddObject(user);
                    db.SaveChanges();
                }

                // grant user admin access to the account
                sec.AddRole(UserInfoDTO.System, user.ID, accountID.Value, (long)RoleTypeEnum.basic_admin, false, false, false);

                #endregion

                #region credential

                if (credInfo == null)
                {
                    SocialConnectionInfoDTO sci = new SocialConnectionInfoDTO()
                    {
                        SocialAppID = info.SocialAppID,
                        PictureURL = info.PictureURL,
                        ScreenName = info.ScreenName,
                        AccountID = accountID.Value,
                        SocialNetwork = info.SocialNetwork,
                        Token = info.Token,
                        TokenSecret = info.TokenSecret,
                        UniqueID = info.UniqueID,
                        URI = info.URI
                    };
                    SaveEntityDTO<SocialConnectionInfoDTO> res = ProviderFactory.Social.Save(new SaveEntityDTO<SocialConnectionInfoDTO>(sci, UserInfoDTO.System));
                    if (res.Problems.Any())
                    {
                        dto.Problems.AddRange(res.Problems);
                    }
                }

                #endregion

                #region subscriptions

	            int? trialDays = db.Syndicators.Where(s => s.ID == dto.Entity.SyndicatorID).Select(s => s.PackageTrialPeriod).FirstOrDefault();
	            if (!trialDays.HasValue)
		            trialDays = 0;

                //List<AccountPackageListItemDTO> subs = this.GetSubscribedPackagesForAccount(accountID.Value, UserInfoDTO.System);
                foreach (SignupSubmitSelectedPackageInfoDTO pack in info.SelectedResellerPackages)
                {
                    Resellers_Packages rpack = (from rp in db.Resellers_Packages where rp.ID == pack.ResellerPackageID select rp).FirstOrDefault();
                    if (rpack != null)
                    {
						AddPackage(accountID.Value, rpack.ResellerID, rpack.PackageID, UserInfoDTO.System, pack.Price, trialDays.GetValueOrDefault());
                    }

                }

                #endregion

                #region Welcome Email

                AccessTokenBaseDTO accessTokenForEmail = new PasswordResetAccessTokenDTO("");
                accessTokenForEmail.TimeTillExpiration = new TimeSpan(30, 0, 0, 0); //reset good for 30 days
                accessTokenForEmail.UserID = user.ID;
                AccessTokenBaseDTO token = ProviderFactory.Security.CreateAccessToken(accessTokenForEmail);

                NotificationProvider noti = (NotificationProvider)ProviderFactory.Notifications;
                noti.sendWelcomeEmail(user.ID, user.Username, accountID.Value, user.EmailAddress, dto.Entity.ResellerID, token);

                #endregion

                #region Syndication Signup Notification

                DAL.Notification ssnoti = new DAL.Notification()
                {
                    DateCreated = DateTime.UtcNow,
                    NotificationMessageTypeID = (long)NotificationMessageTypeEnum.SyndicationSignupCC,
                    AccountID = resellerAccountID,
                    EntityID = user.ID
                };
                db.Notifications.AddObject(ssnoti);
                db.SaveChanges();

                #endregion

            }

            return dto;
        }
    }
}
