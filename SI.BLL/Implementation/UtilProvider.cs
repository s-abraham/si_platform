﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Serialization;
using SI.BLL.Interface;
using SI.DAL;

namespace SI.BLL.Implementation
{
    internal class UtilProvider : IUtilProvider, ITimezoneProvider
    {
        public List<string> GetFilteredWords()
        {
            using (MainEntities db = ContextFactory.Main)
            {
                return (
                    from e in db.FilteredWords
                    select e.Value
                        ).ToList();
            }
        }

        private object _tzUserLock = new object();
        private Dictionary<long, long> _userTimezones = new Dictionary<long, long>();

        public TimeZoneInfo GetTimezoneByID(long timeZoneID)
        {
            return TimeZoneInfo.FindSystemTimeZoneById(getTimezoneList()[timeZoneID].MicrosoftID);
        }


        public TimeZoneInfo GetTimezoneForUserID(long userID)
        {
            long timeZoneID = 15;
            lock (_tzUserLock)
            {
                if (!_userTimezones.TryGetValue(userID, out timeZoneID))
                {
	                if (userID > 0)
	                {
		                using (MainEntities db = ContextFactory.Main)
		                {
			                long? tzid = db.spGetUserTimezone(userID).First();
                            if (tzid.HasValue)
                            {
                                _userTimezones.Add(userID, tzid.Value);
                                timeZoneID = tzid.Value;
                            }
		                }
	                }
                }
            }

            Dictionary<long, Timezone> timezones = getTimezoneList();
            if (timezones.ContainsKey(timeZoneID))
                return TimeZoneInfo.FindSystemTimeZoneById((timezones[timeZoneID]).MicrosoftID);

            return TimeZoneInfo.Utc;

        }

        private object _tzAccountLock = new object();
        private Dictionary<long, long> _accountTimezones = new Dictionary<long, long>();
        public TimeZoneInfo GetTimezoneForAccountID(long accountID)
        {
            long timeZoneID = 0;
            lock (_tzUserLock)
            {
                if (!_accountTimezones.TryGetValue(accountID, out timeZoneID))
                {
                    using (MainEntities db = ContextFactory.Main)
                    {
                        long? tzID = (
                            from a in db.Accounts
                            where a.ID == accountID
                            select a.TimezoneID
                        ).FirstOrDefault();

                        if (!tzID.HasValue)
                            return null;

                        //long? tzid = db.spGetUserTimezone(accountID).First();
                        _accountTimezones.Add(accountID, tzID.Value);
                        timeZoneID = tzID.Value;
                    }
                }
            }

            return TimeZoneInfo.FindSystemTimeZoneById((getTimezoneList()[timeZoneID]).MicrosoftID);

        }

	    public long? FindByName(string timeZoneName)
	    {
		    Dictionary<long, Timezone> timezones = getTimezoneList();
            foreach (var zone in timezones)
            {
	            if (zone.Value.MicrosoftID == timeZoneName)
		            return zone.Value.ID;
            }
		    return null;
	    }

	    public void ClearTimezoneCacheForUser(long userID)
        {
            lock (_tzUserLock)
            {
                if (_userTimezones.ContainsKey(userID))
                    _userTimezones.Remove(userID);
            }
        }

        public void ClearTimezoneCache()
        {
            lock (_tzUserLock)
                lock (_tzLock)
                {
                    _userTimezones.Clear();
                    _tzList = null;
                }
        }


        public string SerializeObject<T>(T obj)
        {
            XmlSerializer xmls = new XmlSerializer(typeof(T));
            using (MemoryStream ms = new MemoryStream())
            {
                XmlSerializerNamespaces ns = new XmlSerializerNamespaces();

                using (XmlWriter writer = new XmlTextWriter(ms, new UTF8Encoding()))
                {
                    xmls.Serialize(writer, obj, ns);
                }
                return Encoding.UTF8.GetString(ms.ToArray());
            }
        }

        public string Trim(string xml)
        {
            //xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" 
            //<?xml version="1.0" encoding="utf-8"?>
            //xmlns:xsd="http://www.w3.org/2001/XMLSchema"

            string[] trimCommentandNamespaces = new string[] { "<?xml version=\"1.0\" encoding=\"utf-8\"?>", " xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"", " xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\"" };
            foreach (string s in trimCommentandNamespaces)
            {
                xml = xml.Replace(s, "");
            }

            return xml;
        }

        public T FromXml<T>(String xml)
        {
            T returnedXmlClass = default(T);

            try
            {
                using (TextReader reader = new StringReader(xml))
                {
                    try
                    {
                        returnedXmlClass = (T)new XmlSerializer(typeof(T)).Deserialize(reader);
                    }
                    catch (InvalidOperationException)
                    {
                        // String passed is not XML, simply return defaultXmlClass
                    }
                }
            }
            catch (Exception ex)
            {
            }

            return returnedXmlClass;
        }

 
        private object _tzLock = new object();
        private Dictionary<long, Timezone> _tzList = null;
        private Dictionary<long, Timezone> getTimezoneList()
        {
            lock (_tzLock)
            {
                if (_tzList == null)
                {
                    using (MainEntities db = ContextFactory.Main)
                    {
                        List<Timezone> l = (from t in db.Timezones select t).ToList();
                        _tzList = new Dictionary<long, Timezone>();
                        foreach (Timezone item in l)
                        {
                            _tzList.Add(item.ID, item);
                        }
                    }
                }
            }

            return _tzList;
        }



        public void RefreshTimezoneDatabase()
        {
            using (MainEntities db = ContextFactory.Main)
            {
                foreach (TimeZoneInfo tzi in TimeZoneInfo.GetSystemTimeZones())
                {
                    string microsoftID = tzi.Id;
                    string name = tzi.DisplayName;
                    decimal offset = Convert.ToDecimal(tzi.BaseUtcOffset.TotalHours);

                    Timezone zone = (from z in db.Timezones where z.MicrosoftID == microsoftID select z).FirstOrDefault();
                    if (zone == null)
                    {
                        zone = new Timezone()
                        {
                            MicrosoftID = microsoftID,
                            Name = name,
                            BaseUTCOffsetHours = offset,
                            Active = false
                        };
                        db.Timezones.AddObject(zone);
                        db.SaveChanges();
                    }
                }
            }
        }

    }
}
