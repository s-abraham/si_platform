﻿using System;
using System.Collections.Generic;
using System.Data.Objects;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SI.BLL.Interface;
using SI.DAL;
using Microsoft.Data.Extensions;
using SI.DTO;
using System.Data.SqlClient;
using System.Data.Common;
using Newtonsoft.Json;
using Notification;
using System.IO;


namespace SI.BLL
{
    internal class ReportsProvider : IReportsProvider
    {
        public void ProcessDailyEnterpriseReputationSummaryReport()
        {
            List<ReportSubscriptionToRun> listReportSubscriptionToRun = new List<ReportSubscriptionToRun>();
            using (MainEntities db = ContextFactory.Main)
            {                
                foreach (var result in db.spGetReportSubscriptionList(2)) // Enterprise Reputation Daily Summary Report (2)
                {
                    ScheduleSpecDTO spec = ScheduleSpecDTO.FromString(result.ScheduleSpec);
                    DateTime nextRunDate = spec.GetNextRunDate(result.LastPickedUp);
                    if (nextRunDate <= DateTime.UtcNow)
                    {
                        listReportSubscriptionToRun.Add(new ReportSubscriptionToRun 
                                                            { 
                                                                ReportSubscriptionID = result.ID,
                                                                ReportTemplateID = result.ReportTemplateID, 
                                                                UserID = result.UserID,
                                                                EmailAddress = result.EmailAddress
                                                            });                        
                    }                   
                }
                if (listReportSubscriptionToRun.Any())
                {
                    List<long> subIDsToProcess = new List<long>();
                    //subIDsToProcess.AddRange(subscriptionIDs.Take(50));
                    subIDsToProcess = (from s in listReportSubscriptionToRun select s.ReportSubscriptionID).ToList();
                    db.spUpdateReportSubscriptionLastPickedUp(string.Join("|", subIDsToProcess.ToArray()));
                }
            }
            foreach (var reportSubscriptionToRun in listReportSubscriptionToRun)
            {
                try
                {
                    SendDailyEnterpriseReputationSummaryReport(reportSubscriptionToRun);
                }
                catch (Exception ex)
                {
                    ProviderFactory.Logging.LogException(ex);
                }
            }
        }

        public void ProcessTokenValidationReport()
        {
            List<ReportSubscriptionToRun> listReportSubscriptionToRun = new List<ReportSubscriptionToRun>();
            using (MainEntities db = ContextFactory.Main)
            {
                foreach (var result in db.spGetReportSubscriptionList(3)) // Enterprise Reputation Daily Summary Report (2)
                {
                    ScheduleSpecDTO spec = ScheduleSpecDTO.FromString(result.ScheduleSpec);
                    DateTime nextRunDate = spec.GetNextRunDate(result.LastPickedUp);
                    if (nextRunDate <= DateTime.UtcNow)
                    {
                        listReportSubscriptionToRun.Add(new ReportSubscriptionToRun
                        {
                            ReportSubscriptionID = result.ID,
                            ReportTemplateID = result.ReportTemplateID,
                            UserID = result.UserID,
                            EmailAddress = result.EmailAddress
                        });
                    }
                }
                if (listReportSubscriptionToRun.Any())
                {
                    List<long> subIDsToProcess = new List<long>();
                    //subIDsToProcess.AddRange(subscriptionIDs.Take(50));
                    subIDsToProcess = (from s in listReportSubscriptionToRun select s.ReportSubscriptionID).ToList();
                    db.spUpdateReportSubscriptionLastPickedUp(string.Join("|", subIDsToProcess.ToArray()));
                }
            }
            foreach (var reportSubscriptionToRun in listReportSubscriptionToRun)
            {
                try
                {
                    SendTokenValidationReport(reportSubscriptionToRun);
                }
                catch (Exception ex)
                {
                    ProviderFactory.Logging.LogException(ex);
                }
            }
        }

        public void ProcessReputationAndSocialReport()
        {
            List<ReportSubscriptionToRun> listReportSubscriptionToRun = new List<ReportSubscriptionToRun>();
            using (MainEntities db = ContextFactory.Main)
            {
                foreach (var result in db.spGetReportSubscriptionList(4)) // Enterprise Reputation Daily Summary Report (2)
                {
                    ScheduleSpecDTO spec = ScheduleSpecDTO.FromString(result.ScheduleSpec);
                    DateTime nextRunDate = spec.GetNextRunDate(result.LastPickedUp);
                    if (nextRunDate <= DateTime.UtcNow)
                    {
                        listReportSubscriptionToRun.Add(new ReportSubscriptionToRun
                        {
                            ReportSubscriptionID = result.ID,
                            ReportTemplateID = result.ReportTemplateID,
                            UserID = result.UserID,
                            EmailAddress = result.EmailAddress
                        });
                    }
                }
                if (listReportSubscriptionToRun.Any())
                {
                    List<long> subIDsToProcess = new List<long>();                    
                    subIDsToProcess = (from s in listReportSubscriptionToRun select s.ReportSubscriptionID).ToList();
                    db.spUpdateReportSubscriptionLastPickedUp(string.Join("|", subIDsToProcess.ToArray()));
                }
            }
            foreach (var reportSubscriptionToRun in listReportSubscriptionToRun)
            {
                try
                {
                    SendReputationAndSocialReport(reportSubscriptionToRun);
                }
                catch (Exception ex)
                {
                    ProviderFactory.Logging.LogException(ex);
                }
            }
        }

        public string GetReputationSnapshotReportHTML(long AccountID, DateTime date, bool printHtml)
        {
            string template = string.Empty;            
            try
            {
                //get the template
                //template = Util.GetResourceTextFile("ReputationSnapshot.html");

                using (MainEntities db = ContextFactory.Main)
                {
                    if (db.Connection.State != System.Data.ConnectionState.Open)
                        db.Connection.Open();

                    UserInfoDTO UserInfo = new UserInfoDTO(1, null); //system
                    var Notificationtemplate = new NotificationTemplate();
                    string ResellerLogo = string.Empty;

                    if (!printHtml)
                    {
                        Notificationtemplate = (from nt in db.NotificationTemplates where nt.ID == 56 select nt).SingleOrDefault();
                    }
                    else
                    {
                        Notificationtemplate = (from nt in db.NotificationTemplates where nt.ID == 59 select nt).SingleOrDefault();
                    }
                    //Get ResellerId by AccountID
                    long resellerID = ProviderFactory.Security.GetResellerByAccountID(UserInfo, AccountID, TimeZoneInfo.Utc).ID.Value;
                    if(resellerID != null && resellerID != 0)
                        ResellerLogo = (from r in db.Resellers where r.ID == resellerID select r.Logo).FirstOrDefault();

                    if (string.IsNullOrEmpty(ResellerLogo))
                    {
                        ResellerLogo = (from r in db.Resellers where r.ID == 5 select r.Logo).FirstOrDefault(); //ResellerID (5) Default "SocialDealer"
                    }

                    PlatformReputationHTMLReportDTO TemplateDTONew = JsonConvert.DeserializeObject<PlatformReputationHTMLReportDTO>(Notificationtemplate.Body);
                    template = TemplateDTONew.Body;

                    
                    GetEntityDTO<AccountDTO> dto2 = ProviderFactory.Security.GetAccountByID(UserInfo, AccountID);

                    using (DbCommand cmd3 = db.CreateStoreCommand(
                       "[report].spGetReputationSnapshotReport2", 
                       System.Data.CommandType.StoredProcedure,
                           new SqlParameter("@AccountIDs", AccountID),
                           new SqlParameter("@today", date)
                       ))
                    {
                        using (DbDataReader reader = cmd3.ExecuteReader())
                        {
                            List<NewReviewsbyReviewSource> ListnewReviewsbyReviewSource = db.Translate<NewReviewsbyReviewSource>(reader).ToList();

                            reader.NextResult();

                            OverAllReputaion overAllReputaion = db.Translate<OverAllReputaion>(reader).FirstOrDefault();

                            reader.NextResult();

                            List<NewReviewsbyMonth> ListNewReviewsbyMonth = db.Translate<NewReviewsbyMonth>(reader).ToList();

                            #region Reseller Logo for Print
                            if (printHtml)
                            {
                                template = template.Replace("[$ResellerLogo$]", ResellerLogo);
                            }

                            #endregion

                            #region Over AllReputaion
                           
                            string Month =Convert.ToDateTime(date).ToString("MMMM", CultureInfo.CreateSpecificCulture("en-US"));
                            string Year = Convert.ToDateTime(date).Year.ToString();

                            template = template.Replace("[$AccountName$]", dto2.Entity.DisplayName)
                                                .Replace("[$Month$]", Month)
                                                .Replace("[$Year$]", Year);

                            if (overAllReputaion != null)
                            {
                                template = template.Replace("[$TotalReviews$]", overAllReputaion.TotalReview.ToString("#,##0"))
                                                         .Replace("[$AvgRating$]", overAllReputaion.Rating.ToString());
                            }
                            #endregion

                            #region Monthly Reputation Data
                            if (ListNewReviewsbyMonth != null)
                            {

                                var newReviewsbyMonth = ListNewReviewsbyMonth.Where(n => n.MonthID == 6).FirstOrDefault();

                                #region Month 6
                                if (newReviewsbyMonth != null)
                                {
                                    string strMonthName = CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName(newReviewsbyMonth.Month);
                                    
                                    if (newReviewsbyMonth.MonthID == 6)
                                    {
                                        int TotalReviewPeriod = newReviewsbyMonth.TotalReviewPeriod;
                                        decimal NewReviewsRatingSum = newReviewsbyMonth.NewReviewsRatingSum;
                                        decimal NewReviewAvgRating = 0;
                                        
                                        if (TotalReviewPeriod > 0)
                                        {
                                            NewReviewAvgRating = Convert.ToDecimal(NewReviewsRatingSum / Convert.ToDecimal(TotalReviewPeriod));
                                        }

                                        double total = newReviewsbyMonth.TotalReviewPeriodPositive + newReviewsbyMonth.TotalReviewPeriodNegative;
                                        int PercentPositive = 0;
                                        int PercentNegative = 0;
                                        if (total > 0)
                                        {
                                            PercentNegative = (int)(((newReviewsbyMonth.TotalReviewPeriodNegative / total) * 100) + 0.5);
                                            PercentPositive = 100 - PercentNegative;

                                            //siteReview.PercentPositive = (int)((siteReview.TotalPositive/total)*100);

                                            if (PercentNegative + PercentPositive < 100 && PercentPositive < 100)
                                                PercentPositive++;
                                        }

                                        template = template.Replace("[$Month6$]", strMonthName)
                                                            .Replace("[$AvgRating_Month6$]", Math.Round(NewReviewAvgRating,1).ToString())
                                                            .Replace("[$NewReviews_Month6$]", newReviewsbyMonth.TotalReviewPeriod.ToString("#,##0"))
                                                            .Replace("[$PositivePercent_Month6$]", PercentPositive.ToString() + "%")
                                                            .Replace("[$NegativePercent_Month6$]", PercentNegative.ToString() + "%");
                                    }

                                }

                                template = template.Replace("[$Month6$]", string.Empty)
                                                           .Replace("[$AvgRating_Month6$]", "0")
                                                           .Replace("[$NewReviews_Month6$]", "0")
                                                           .Replace("[$PositivePercent_Month6$]", "0%")
                                                           .Replace("[$NegativePercent_Month6$]", "0%");
                                #endregion

                                #region Month 5

                                newReviewsbyMonth = ListNewReviewsbyMonth.Where(n => n.MonthID == 5).FirstOrDefault();

                                if (newReviewsbyMonth != null)
                                {
                                    string strMonthName = CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName(newReviewsbyMonth.Month);

                                    if (newReviewsbyMonth.MonthID == 5)
                                    {
                                        int TotalReviewPeriod = newReviewsbyMonth.TotalReviewPeriod;
                                        decimal NewReviewsRatingSum = newReviewsbyMonth.NewReviewsRatingSum;
                                        decimal NewReviewAvgRating = 0;

                                        if (TotalReviewPeriod > 0)
                                        {
                                            NewReviewAvgRating = Convert.ToDecimal(NewReviewsRatingSum / Convert.ToDecimal(TotalReviewPeriod));
                                        }

                                        double total = newReviewsbyMonth.TotalReviewPeriodPositive + newReviewsbyMonth.TotalReviewPeriodNegative;
                                        int PercentPositive = 0;
                                        int PercentNegative = 0;
                                        if (total > 0)
                                        {
                                            PercentNegative = (int)(((newReviewsbyMonth.TotalReviewPeriodNegative / total) * 100) + 0.5);
                                            PercentPositive = 100 - PercentNegative;

                                            //siteReview.PercentPositive = (int)((siteReview.TotalPositive/total)*100);

                                            if (PercentNegative + PercentPositive < 100 && PercentPositive < 100)
                                                PercentPositive++;
                                        }

                                        template = template.Replace("[$Month5$]", strMonthName)
                                                            .Replace("[$AvgRating_Month5$]", Math.Round(NewReviewAvgRating, 1).ToString())
                                                            .Replace("[$NewReviews_Month5$]", newReviewsbyMonth.TotalReviewPeriod.ToString("#,##0"))
                                                            .Replace("[$PositivePercent_Month5$]", PercentPositive.ToString() + "%")
                                                            .Replace("[$NegativePercent_Month5$]", PercentNegative.ToString() + "%");
                                    }

                                }
                                template = template.Replace("[$Month5$]", string.Empty)
                                                           .Replace("[$AvgRating_Month5$]", "0")
                                                           .Replace("[$NewReviews_Month5$]", "0")
                                                           .Replace("[$PositivePercent_Month5$]", "0%")
                                                           .Replace("[$NegativePercent_Month5$]", "0%");
                                #endregion

                                #region Month 4

                                newReviewsbyMonth = ListNewReviewsbyMonth.Where(n => n.MonthID == 4).FirstOrDefault();

                                if (newReviewsbyMonth != null)
                                {
                                    string strMonthName = CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName(newReviewsbyMonth.Month);

                                    if (newReviewsbyMonth.MonthID == 4)
                                    {
                                        int TotalReviewPeriod = newReviewsbyMonth.TotalReviewPeriod;
                                        decimal NewReviewsRatingSum = newReviewsbyMonth.NewReviewsRatingSum;
                                        decimal NewReviewAvgRating = 0;

                                        if (TotalReviewPeriod > 0)
                                        {
                                            NewReviewAvgRating = Convert.ToDecimal(NewReviewsRatingSum / Convert.ToDecimal(TotalReviewPeriod));
                                        }

                                        double total = newReviewsbyMonth.TotalReviewPeriodPositive + newReviewsbyMonth.TotalReviewPeriodNegative;
                                        int PercentPositive = 0;
                                        int PercentNegative = 0;
                                        if (total > 0)
                                        {
                                            PercentNegative = (int)(((newReviewsbyMonth.TotalReviewPeriodNegative / total) * 100) + 0.5);
                                            PercentPositive = 100 - PercentNegative;

                                            //siteReview.PercentPositive = (int)((siteReview.TotalPositive/total)*100);

                                            if (PercentNegative + PercentPositive < 100 && PercentPositive < 100)
                                                PercentPositive++;
                                        }

                                        template = template.Replace("[$Month4$]", strMonthName)
                                                            .Replace("[$AvgRating_Month4$]", Math.Round(NewReviewAvgRating, 1).ToString())
                                                            .Replace("[$NewReviews_Month4$]", newReviewsbyMonth.TotalReviewPeriod.ToString("#,##0"))
                                                            .Replace("[$PositivePercent_Month4$]", PercentPositive.ToString() + "%")
                                                            .Replace("[$NegativePercent_Month4$]", PercentNegative.ToString() + "%");
                                    }

                                }
                                template = template.Replace("[$Month4$]", string.Empty)
                                                           .Replace("[$AvgRating_Month4$]", "0")
                                                           .Replace("[$NewReviews_Month4$]", "0")
                                                           .Replace("[$PositivePercent_Month4$]", "0%")
                                                           .Replace("[$NegativePercent_Month4$]", "0%");
                                #endregion

                                #region Month 3

                                newReviewsbyMonth = ListNewReviewsbyMonth.Where(n => n.MonthID == 3).FirstOrDefault();

                                if (newReviewsbyMonth != null)
                                {
                                    string strMonthName = CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName(newReviewsbyMonth.Month);
                                    if (newReviewsbyMonth.MonthID == 3)
                                    {
                                        int TotalReviewPeriod = newReviewsbyMonth.TotalReviewPeriod;
                                        decimal NewReviewsRatingSum = newReviewsbyMonth.NewReviewsRatingSum;
                                        decimal NewReviewAvgRating = 0;

                                        if (TotalReviewPeriod > 0)
                                        {
                                            NewReviewAvgRating = Convert.ToDecimal(NewReviewsRatingSum / Convert.ToDecimal(TotalReviewPeriod));
                                        }

                                        double total = newReviewsbyMonth.TotalReviewPeriodPositive + newReviewsbyMonth.TotalReviewPeriodNegative;
                                        int PercentPositive = 0;
                                        int PercentNegative = 0;
                                        if (total > 0)
                                        {
                                            PercentNegative = (int)(((newReviewsbyMonth.TotalReviewPeriodNegative / total) * 100) + 0.5);
                                            PercentPositive = 100 - PercentNegative;

                                            //siteReview.PercentPositive = (int)((siteReview.TotalPositive/total)*100);

                                            if (PercentNegative + PercentPositive < 100 && PercentPositive < 100)
                                                PercentPositive++;
                                        }

                                        template = template.Replace("[$Month3$]", strMonthName)
                                                            .Replace("[$AvgRating_Month3$]", Math.Round(NewReviewAvgRating, 1).ToString())
                                                            .Replace("[$NewReviews_Month3$]", newReviewsbyMonth.TotalReviewPeriod.ToString("#,##0"))
                                                            .Replace("[$PositivePercent_Month3$]", PercentPositive.ToString() + "%")
                                                            .Replace("[$NegativePercent_Month3$]", PercentNegative.ToString() + "%");
                                    }
                                }
                                template = template.Replace("[$Month3$]", string.Empty)
                                                           .Replace("[$AvgRating_Month3$]", "0")
                                                           .Replace("[$NewReviews_Month3$]", "0")
                                                           .Replace("[$PositivePercent_Month3$]", "0%")
                                                           .Replace("[$NegativePercent_Month3$]", "0%");
                                #endregion

                                #region Month 2

                                newReviewsbyMonth = ListNewReviewsbyMonth.Where(n => n.MonthID == 2).FirstOrDefault();

                                if (newReviewsbyMonth != null)
                                {
                                    string strMonthName = CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName(newReviewsbyMonth.Month);
                                    if (newReviewsbyMonth.MonthID == 2)
                                    {
                                        int TotalReviewPeriod = newReviewsbyMonth.TotalReviewPeriod;
                                        decimal NewReviewsRatingSum = newReviewsbyMonth.NewReviewsRatingSum;
                                        decimal NewReviewAvgRating = 0;

                                        if (TotalReviewPeriod > 0)
                                        {
                                            NewReviewAvgRating = Convert.ToDecimal(NewReviewsRatingSum / Convert.ToDecimal(TotalReviewPeriod));
                                        }

                                        double total = newReviewsbyMonth.TotalReviewPeriodPositive + newReviewsbyMonth.TotalReviewPeriodNegative;
                                        int PercentPositive = 0;
                                        int PercentNegative = 0;
                                        if (total > 0)
                                        {
                                            PercentNegative = (int)(((newReviewsbyMonth.TotalReviewPeriodNegative / total) * 100) + 0.5);
                                            PercentPositive = 100 - PercentNegative;

                                            //siteReview.PercentPositive = (int)((siteReview.TotalPositive/total)*100);

                                            if (PercentNegative + PercentPositive < 100 && PercentPositive < 100)
                                                PercentPositive++;
                                        }

                                        template = template.Replace("[$Month2$]", strMonthName)
                                                            .Replace("[$AvgRating_Month2$]", Math.Round(NewReviewAvgRating, 1).ToString())
                                                            .Replace("[$NewReviews_Month2$]", newReviewsbyMonth.TotalReviewPeriod.ToString("#,##0"))
                                                            .Replace("[$PositivePercent_Month2$]", PercentPositive.ToString() + "%")
                                                            .Replace("[$NegativePercent_Month2$]", PercentNegative.ToString() + "%");
                                    }
                                }
                                template = template.Replace("[$Month2$]", string.Empty)
                                                           .Replace("[$AvgRating_Month2$]", "0")
                                                           .Replace("[$NewReviews_Month2$]", "0")
                                                           .Replace("[$PositivePercent_Month2$]", "0%")
                                                           .Replace("[$NegativePercent_Month2$]", "0%");
                                #endregion

                                #region Month 1

                                newReviewsbyMonth = ListNewReviewsbyMonth.Where(n => n.MonthID == 1).FirstOrDefault();

                                if (newReviewsbyMonth != null)
                                {
                                    string strMonthName = CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName(newReviewsbyMonth.Month);
                                    if (newReviewsbyMonth.MonthID == 1)
                                    {
                                        int TotalReviewPeriod = newReviewsbyMonth.TotalReviewPeriod;
                                        decimal NewReviewsRatingSum = newReviewsbyMonth.NewReviewsRatingSum;
                                        decimal NewReviewAvgRating = 0;

                                        if (TotalReviewPeriod > 0)
                                        {
                                            NewReviewAvgRating = Convert.ToDecimal(NewReviewsRatingSum / Convert.ToDecimal(TotalReviewPeriod));
                                        }

                                        double total = newReviewsbyMonth.TotalReviewPeriodPositive + newReviewsbyMonth.TotalReviewPeriodNegative;
                                        int PercentPositive = 0;
                                        int PercentNegative = 0;
                                        if (total > 0)
                                        {
                                            PercentNegative = (int)(((newReviewsbyMonth.TotalReviewPeriodNegative / total) * 100) + 0.5);
                                            PercentPositive = 100 - PercentNegative;

                                            //siteReview.PercentPositive = (int)((siteReview.TotalPositive/total)*100);

                                            if (PercentNegative + PercentPositive < 100 && PercentPositive < 100)
                                                PercentPositive++;
                                        }

                                        template = template.Replace("[$Month1$]", strMonthName)
                                                            .Replace("[$AvgRating_Month1$]", Math.Round(NewReviewAvgRating, 1).ToString())
                                                            .Replace("[$NewReviews_Month1$]", newReviewsbyMonth.TotalReviewPeriod.ToString("#,##0"))
                                                            .Replace("[$PositivePercent_Month1$]", PercentPositive.ToString() + "%")
                                                            .Replace("[$NegativePercent_Month1$]", PercentNegative.ToString() + "%");
                                    }
                                }
                                template = template.Replace("[$Month1$]", string.Empty)
                                                           .Replace("[$AvgRating_Month1$]", "0")
                                                           .Replace("[$NewReviews_Month1$]", "0")
                                                           .Replace("[$PositivePercent_Month1$]", "0%")
                                                           .Replace("[$NegativePercent_Month1$]", "0%");
                                #endregion

                            }
                            #endregion

                            #region Monthly Reputation Data By ReviewSource
                            if (ListnewReviewsbyReviewSource != null)
                            {
                                var newReviewsbyReviewSource = ListnewReviewsbyReviewSource.Where(n => n.ReviewSourceID == 100).FirstOrDefault();

                                #region Google
                                if (newReviewsbyReviewSource != null)
                                {
                                    int NewReviewsCount = newReviewsbyReviewSource.NewReviewsCount;
                                    decimal NewReviewsRatingSum = newReviewsbyReviewSource.NewReviewsRatingSum;
                                    decimal NewReviewAvgRating = 0;

                                    if (NewReviewsCount > 0)
                                    {
                                        NewReviewAvgRating = Convert.ToDecimal(NewReviewsRatingSum / Convert.ToDecimal(NewReviewsCount));
                                    }

                                    double total = newReviewsbyReviewSource.TotalPositiveReviews + newReviewsbyReviewSource.TotalNegativeReviews;
                                    int PercentPositive = 0;
                                    int PercentNegative = 0;
                                    if (total > 0)
                                    {
                                        PercentNegative = (int)(((newReviewsbyReviewSource.TotalNegativeReviews / total) * 100) + 0.5);
                                        PercentPositive = 100 - PercentNegative;

                                        if (PercentNegative + PercentPositive < 100 && PercentPositive < 100)
                                            PercentPositive++;
                                    }

                                    template = template.Replace("[$Google_NewReview$]", NewReviewsCount.ToString("#,##0"))
                                                        .Replace("[$Google_NewReviewRating$]", Math.Round(NewReviewAvgRating, 1).ToString())
                                                        .Replace("[$Google_Total$]", newReviewsbyReviewSource.SalesReviewCount.ToString("#,##0"))
                                                        .Replace("[$Google_PositivePercent$]", PercentPositive.ToString() + "%")
                                                        .Replace("[$Google_NegativePercent$]", PercentNegative.ToString() + "%")
                                                        .Replace("[$Google_Rating$]", newReviewsbyReviewSource.AvgRating.ToString());


                                }

                                template = template.Replace("[$Google_NewReview$]", "0")
                                                        .Replace("[$Google_NewReviewRating$]", "0")
                                                        .Replace("[$Google_Total$]", "0")
                                                        .Replace("[$Google_PositivePercent$]", "0%")
                                                        .Replace("[$Google_NegativePercent$]", "0%")
                                                        .Replace("[$Google_Rating$]", "0");
                                #endregion

                                #region Yelp
                                newReviewsbyReviewSource = ListnewReviewsbyReviewSource.Where(n => n.ReviewSourceID == 400).FirstOrDefault();

                                if (newReviewsbyReviewSource != null)
                                {
                                    int NewReviewsCount = newReviewsbyReviewSource.NewReviewsCount;
                                    decimal NewReviewsRatingSum = newReviewsbyReviewSource.NewReviewsRatingSum;
                                    decimal NewReviewAvgRating = 0;

                                    if (NewReviewsCount > 0)
                                    {
                                        NewReviewAvgRating = Convert.ToDecimal(NewReviewsRatingSum / Convert.ToDecimal(NewReviewsCount));
                                    }

                                    double total = newReviewsbyReviewSource.TotalPositiveReviews + newReviewsbyReviewSource.TotalNegativeReviews;
                                    int PercentPositive = 0;
                                    int PercentNegative = 0;
                                    if (total > 0)
                                    {
                                        PercentNegative = (int)(((newReviewsbyReviewSource.TotalNegativeReviews / total) * 100) + 0.5);
                                        PercentPositive = 100 - PercentNegative;

                                        if (PercentNegative + PercentPositive < 100 && PercentPositive < 100)
                                            PercentPositive++;
                                    }

                                    template = template.Replace("[$Yelp_NewReview$]", NewReviewsCount.ToString("#,##0"))
                                                        .Replace("[$Yelp_NewReviewRating$]", Math.Round(NewReviewAvgRating, 1).ToString())
                                                        .Replace("[$Yelp_Total$]", newReviewsbyReviewSource.SalesReviewCount.ToString("#,##0"))
                                                        .Replace("[$Yelp_PositivePercent$]", PercentPositive.ToString() + "%")
                                                        .Replace("[$Yelp_NegativePercent$]", PercentNegative.ToString() + "%")
                                                        .Replace("[$Yelp_Rating$]", newReviewsbyReviewSource.AvgRating.ToString());


                                }

                                template = template.Replace("[$Yelp_NewReview$]", "0")
                                                        .Replace("[$Yelp_NewReviewRating$]", "0")
                                                        .Replace("[$Yelp_Total$]", "0")
                                                        .Replace("[$Yelp_PositivePercent$]", "0%")
                                                        .Replace("[$Yelp_NegativePercent$]", "0%")
                                                        .Replace("[$Yelp_Rating$]", "0");
                                #endregion

                                #region Cars.com
                                newReviewsbyReviewSource = ListnewReviewsbyReviewSource.Where(n => n.ReviewSourceID == 800).FirstOrDefault();

                                if (newReviewsbyReviewSource != null)
                                {
                                    int NewReviewsCount = newReviewsbyReviewSource.NewReviewsCount;
                                    decimal NewReviewsRatingSum = newReviewsbyReviewSource.NewReviewsRatingSum;
                                    decimal NewReviewAvgRating = 0;

                                    if (NewReviewsCount > 0)
                                    {
                                        NewReviewAvgRating = Convert.ToDecimal(NewReviewsRatingSum / Convert.ToDecimal(NewReviewsCount));
                                    }

                                    double total = newReviewsbyReviewSource.TotalPositiveReviews + newReviewsbyReviewSource.TotalNegativeReviews;
                                    int PercentPositive = 0;
                                    int PercentNegative = 0;
                                    if (total > 0)
                                    {
                                        PercentNegative = (int)(((newReviewsbyReviewSource.TotalNegativeReviews / total) * 100) + 0.5);
                                        PercentPositive = 100 - PercentNegative;

                                        if (PercentNegative + PercentPositive < 100 && PercentPositive < 100)
                                            PercentPositive++;
                                    }

                                    template = template.Replace("[$CarsDotcom_NewReview$]", NewReviewsCount.ToString("#,##0"))
                                                        .Replace("[$CarsDotcom_NewReviewRating$]", Math.Round(NewReviewAvgRating, 1).ToString())
                                                        .Replace("[$CarsDotcom_Total$]", newReviewsbyReviewSource.SalesReviewCount.ToString("#,##0"))
                                                        .Replace("[$CarsDotcom_PositivePercent$]", PercentPositive.ToString() + "%")
                                                        .Replace("[$CarsDotcom_NegativePercent$]", PercentNegative.ToString() + "%")
                                                        .Replace("[$CarsDotcom_Rating$]", newReviewsbyReviewSource.AvgRating.ToString());


                                }

                                template = template.Replace("[$CarsDotcom_NewReview$]", "0")
                                                        .Replace("[$CarsDotcom_NewReviewRating$]", "0")
                                                        .Replace("[$CarsDotcom_Total$]", "0")
                                                        .Replace("[$CarsDotcom_PositivePercent$]", "0%")
                                                        .Replace("[$CarsDotcom_NegativePercent$]", "0%")
                                                        .Replace("[$CarsDotcom_Rating$]", "0");
                                #endregion

                                #region Edmunds
                                newReviewsbyReviewSource = ListnewReviewsbyReviewSource.Where(n => n.ReviewSourceID == 700).FirstOrDefault();

                                if (newReviewsbyReviewSource != null)
                                {
                                    int NewReviewsCount = newReviewsbyReviewSource.NewReviewsCount;
                                    decimal NewReviewsRatingSum = newReviewsbyReviewSource.NewReviewsRatingSum;
                                    decimal NewReviewAvgRating = 0;

                                    if (NewReviewsCount > 0)
                                    {
                                        NewReviewAvgRating = Convert.ToDecimal(NewReviewsRatingSum / Convert.ToDecimal(NewReviewsCount));
                                    }

                                    double total = newReviewsbyReviewSource.TotalPositiveReviews + newReviewsbyReviewSource.TotalNegativeReviews;
                                    int PercentPositive = 0;
                                    int PercentNegative = 0;
                                    if (total > 0)
                                    {
                                        PercentNegative = (int)(((newReviewsbyReviewSource.TotalNegativeReviews / total) * 100) + 0.5);
                                        PercentPositive = 100 - PercentNegative;

                                        if (PercentNegative + PercentPositive < 100 && PercentPositive < 100)
                                            PercentPositive++;
                                    }

                                    template = template.Replace("[$Edmunds_NewReview$]", NewReviewsCount.ToString("#,##0"))
                                                        .Replace("[$Edmunds_NewReviewRating$]", Math.Round(NewReviewAvgRating, 1).ToString())
                                                        .Replace("[$Edmunds_Total$]", newReviewsbyReviewSource.SalesReviewCount.ToString("#,##0"))
                                                        .Replace("[$Edmunds_PositivePercent$]", PercentPositive.ToString() + "%")
                                                        .Replace("[$Edmunds_NegativePercent$]", PercentNegative.ToString() + "%")
                                                        .Replace("[$Edmunds_Rating$]", newReviewsbyReviewSource.AvgRating.ToString());


                                }

                                template = template.Replace("[$Edmunds_NewReview$]", "0")
                                                        .Replace("[$Edmunds_NewReviewRating$]", "0")
                                                        .Replace("[$Edmunds_Total$]", "0")
                                                        .Replace("[$Edmunds_PositivePercent$]", "0%")
                                                        .Replace("[$Edmunds_NegativePercent$]", "0%")
                                                        .Replace("[$Edmunds_Rating$]", "0");
                                #endregion

                                #region YahooLocal
                                newReviewsbyReviewSource = ListnewReviewsbyReviewSource.Where(n => n.ReviewSourceID == 300).FirstOrDefault();

                                if (newReviewsbyReviewSource != null)
                                {
                                    int NewReviewsCount = newReviewsbyReviewSource.NewReviewsCount;
                                    decimal NewReviewsRatingSum = newReviewsbyReviewSource.NewReviewsRatingSum;
                                    decimal NewReviewAvgRating = 0;

                                    if (NewReviewsCount > 0)
                                    {
                                        NewReviewAvgRating = Convert.ToDecimal(NewReviewsRatingSum / Convert.ToDecimal(NewReviewsCount));
                                    }

                                    double total = newReviewsbyReviewSource.TotalPositiveReviews + newReviewsbyReviewSource.TotalNegativeReviews;
                                    int PercentPositive = 0;
                                    int PercentNegative = 0;
                                    if (total > 0)
                                    {
                                        PercentNegative = (int)(((newReviewsbyReviewSource.TotalNegativeReviews / total) * 100) + 0.5);
                                        PercentPositive = 100 - PercentNegative;

                                        if (PercentNegative + PercentPositive < 100 && PercentPositive < 100)
                                            PercentPositive++;
                                    }

                                    template = template.Replace("[$YahooLocal_NewReview$]", NewReviewsCount.ToString("#,##0"))
                                                        .Replace("[$YahooLocal_NewReviewRating$]", Math.Round(NewReviewAvgRating, 1).ToString())
                                                        .Replace("[$YahooLocal_Total$]", newReviewsbyReviewSource.SalesReviewCount.ToString("#,##0"))
                                                        .Replace("[$YahooLocal_PositivePercent$]", PercentPositive.ToString() + "%")
                                                        .Replace("[$YahooLocal_NegativePercent$]", PercentNegative.ToString() + "%")
                                                        .Replace("[$YahooLocal_Rating$]", newReviewsbyReviewSource.AvgRating.ToString());


                                }

                                template = template.Replace("[$YahooLocal_NewReview$]", "0")
                                                        .Replace("[$YahooLocal_NewReviewRating$]", "0")
                                                        .Replace("[$YahooLocal_Total$]", "0")
                                                        .Replace("[$YahooLocal_PositivePercent$]", "0%")
                                                        .Replace("[$YahooLocal_NegativePercent$]", "0%")
                                                        .Replace("[$YahooLocal_Rating$]", "0");
                                #endregion

                                #region YellowPages
                                newReviewsbyReviewSource = ListnewReviewsbyReviewSource.Where(n => n.ReviewSourceID == 600).FirstOrDefault();

                                if (newReviewsbyReviewSource != null)
                                {
                                    int NewReviewsCount = newReviewsbyReviewSource.NewReviewsCount;
                                    decimal NewReviewsRatingSum = newReviewsbyReviewSource.NewReviewsRatingSum;
                                    decimal NewReviewAvgRating = 0;

                                    if (NewReviewsCount > 0)
                                    {
                                        NewReviewAvgRating = Convert.ToDecimal(NewReviewsRatingSum / Convert.ToDecimal(NewReviewsCount));
                                    }

                                    double total = newReviewsbyReviewSource.TotalPositiveReviews + newReviewsbyReviewSource.TotalNegativeReviews;
                                    int PercentPositive = 0;
                                    int PercentNegative = 0;
                                    if (total > 0)
                                    {
                                        PercentNegative = (int)(((newReviewsbyReviewSource.TotalNegativeReviews / total) * 100) + 0.5);
                                        PercentPositive = 100 - PercentNegative;

                                        if (PercentNegative + PercentPositive < 100 && PercentPositive < 100)
                                            PercentPositive++;
                                    }

                                    template = template.Replace("[$YellowPages_NewReview$]", NewReviewsCount.ToString("#,##0"))
                                                        .Replace("[$YellowPages_NewReviewRating$]", Math.Round(NewReviewAvgRating, 1).ToString())
                                                        .Replace("[$YellowPages_Total$]", newReviewsbyReviewSource.SalesReviewCount.ToString("#,##0"))
                                                        .Replace("[$YellowPages_PositivePercent$]", PercentPositive.ToString() + "%")
                                                        .Replace("[$YellowPages_NegativePercent$]", PercentNegative.ToString() + "%")
                                                        .Replace("[$YellowPages_Rating$]", newReviewsbyReviewSource.AvgRating.ToString());


                                }

                                template = template.Replace("[$YellowPages_NewReview$]", "0")
                                                        .Replace("[$YellowPages_NewReviewRating$]", "0")
                                                        .Replace("[$YellowPages_Total$]", "0")
                                                        .Replace("[$YellowPages_PositivePercent$]", "0%")
                                                        .Replace("[$YellowPages_NegativePercent$]", "0%")
                                                        .Replace("[$YellowPages_Rating$]", "0");
                                #endregion

                                #region DealerRater
                                newReviewsbyReviewSource = ListnewReviewsbyReviewSource.Where(n => n.ReviewSourceID == 200).FirstOrDefault();

                                if (newReviewsbyReviewSource != null)
                                {
                                    int NewReviewsCount = newReviewsbyReviewSource.NewReviewsCount;
                                    decimal NewReviewsRatingSum = newReviewsbyReviewSource.NewReviewsRatingSum;
                                    decimal NewReviewAvgRating = 0;

                                    if (NewReviewsCount > 0)
                                    {
                                        NewReviewAvgRating = Convert.ToDecimal(NewReviewsRatingSum / Convert.ToDecimal(NewReviewsCount));
                                    }

                                    double total = newReviewsbyReviewSource.TotalPositiveReviews + newReviewsbyReviewSource.TotalNegativeReviews;
                                    int PercentPositive = 0;
                                    int PercentNegative = 0;
                                    if (total > 0)
                                    {
                                        PercentNegative = (int)(((newReviewsbyReviewSource.TotalNegativeReviews / total) * 100) + 0.5);
                                        PercentPositive = 100 - PercentNegative;

                                        if (PercentNegative + PercentPositive < 100 && PercentPositive < 100)
                                            PercentPositive++;
                                    }

                                    template = template.Replace("[$DealerRater_NewReview$]", NewReviewsCount.ToString("#,##0"))
                                                        .Replace("[$DealerRater_NewReviewRating$]", Math.Round(NewReviewAvgRating, 1).ToString())
                                                        .Replace("[$DealerRater_Total$]", newReviewsbyReviewSource.SalesReviewCount.ToString("#,##0"))
                                                        .Replace("[$DealerRater_PositivePercent$]", PercentPositive.ToString() + "%")
                                                        .Replace("[$DealerRater_NegativePercent$]", PercentNegative.ToString() + "%")
                                                        .Replace("[$DealerRater_Rating$]", newReviewsbyReviewSource.AvgRating.ToString());


                                }

                                template = template.Replace("[$DealerRater_NewReview$]", "0")
                                                        .Replace("[$DealerRater_NewReviewRating$]", "0")
                                                        .Replace("[$DealerRater_Total$]", "0")
                                                        .Replace("[$DealerRater_PositivePercent$]", "0%")
                                                        .Replace("[$DealerRater_NegativePercent$]", "0%")
                                                        .Replace("[$DealerRater_Rating$]", "0");
                                #endregion

                                #region JudysBook
                                newReviewsbyReviewSource = ListnewReviewsbyReviewSource.Where(n => n.ReviewSourceID == 1000).FirstOrDefault();

                                if (newReviewsbyReviewSource != null)
                                {
                                    int NewReviewsCount = newReviewsbyReviewSource.NewReviewsCount;
                                    decimal NewReviewsRatingSum = newReviewsbyReviewSource.NewReviewsRatingSum;
                                    decimal NewReviewAvgRating = 0;

                                    if (NewReviewsCount > 0)
                                    {
                                        NewReviewAvgRating = Convert.ToDecimal(NewReviewsRatingSum / Convert.ToDecimal(NewReviewsCount));
                                    }

                                    double total = newReviewsbyReviewSource.TotalPositiveReviews + newReviewsbyReviewSource.TotalNegativeReviews;
                                    int PercentPositive = 0;
                                    int PercentNegative = 0;
                                    if (total > 0)
                                    {
                                        PercentNegative = (int)(((newReviewsbyReviewSource.TotalNegativeReviews / total) * 100) + 0.5);
                                        PercentPositive = 100 - PercentNegative;

                                        if (PercentNegative + PercentPositive < 100 && PercentPositive < 100)
                                            PercentPositive++;
                                    }

                                    template = template.Replace("[$JudysBook_NewReview$]", NewReviewsCount.ToString("#,##0"))
                                                        .Replace("[$JudysBook_NewReviewRating$]", Math.Round(NewReviewAvgRating, 1).ToString())
                                                        .Replace("[$JudysBook_Total$]", newReviewsbyReviewSource.SalesReviewCount.ToString("#,##0"))
                                                        .Replace("[$JudysBook_PositivePercent$]", PercentPositive.ToString() + "%")
                                                        .Replace("[$JudysBook_NegativePercent$]", PercentNegative.ToString() + "%")
                                                        .Replace("[$JudysBook_Rating$]", newReviewsbyReviewSource.AvgRating.ToString());


                                }

                                template = template.Replace("[$JudysBook_NewReview$]", "0")
                                                        .Replace("[$JudysBook_NewReviewRating$]", "0")
                                                        .Replace("[$JudysBook_Total$]", "0")
                                                        .Replace("[$JudysBook_PositivePercent$]", "0%")
                                                        .Replace("[$JudysBook_NegativePercent$]", "0%")
                                                        .Replace("[$JudysBook_Rating$]", "0");
                                #endregion

                                #region Citysearch
                                newReviewsbyReviewSource = ListnewReviewsbyReviewSource.Where(n => n.ReviewSourceID == 900).FirstOrDefault();

                                if (newReviewsbyReviewSource != null)
                                {
                                    int NewReviewsCount = newReviewsbyReviewSource.NewReviewsCount;
                                    decimal NewReviewsRatingSum = newReviewsbyReviewSource.NewReviewsRatingSum;
                                    decimal NewReviewAvgRating = 0;

                                    if (NewReviewsCount > 0)
                                    {
                                        NewReviewAvgRating = Convert.ToDecimal(NewReviewsRatingSum / Convert.ToDecimal(NewReviewsCount));
                                    }

                                    double total = newReviewsbyReviewSource.TotalPositiveReviews + newReviewsbyReviewSource.TotalNegativeReviews;
                                    int PercentPositive = 0;
                                    int PercentNegative = 0;
                                    if (total > 0)
                                    {
                                        PercentNegative = (int)(((newReviewsbyReviewSource.TotalNegativeReviews / total) * 100) + 0.5);
                                        PercentPositive = 100 - PercentNegative;

                                        if (PercentNegative + PercentPositive < 100 && PercentPositive < 100)
                                            PercentPositive++;
                                    }

                                    template = template.Replace("[$Citysearch_NewReview$]", NewReviewsCount.ToString("#,##0"))
                                                        .Replace("[$Citysearch_NewReviewRating$]", Math.Round(NewReviewAvgRating, 1).ToString())
                                                        .Replace("[$Citysearch_Total$]", newReviewsbyReviewSource.SalesReviewCount.ToString("#,##0"))
                                                        .Replace("[$Citysearch_PositivePercent$]", PercentPositive.ToString() + "%")
                                                        .Replace("[$Citysearch_NegativePercent$]", PercentNegative.ToString() + "%")
                                                        .Replace("[$Citysearch_Rating$]", newReviewsbyReviewSource.AvgRating.ToString());


                                }

                                template = template.Replace("[$Citysearch_NewReview$]", "0")
                                                        .Replace("[$Citysearch_NewReviewRating$]", "0")
                                                        .Replace("[$Citysearch_Total$]", "0")
                                                        .Replace("[$Citysearch_PositivePercent$]", "0%")
                                                        .Replace("[$Citysearch_NegativePercent$]", "0%")
                                                        .Replace("[$Citysearch_Rating$]", "0");
                                #endregion

                                #region CarDealerReviews
                                newReviewsbyReviewSource = ListnewReviewsbyReviewSource.Where(n => n.ReviewSourceID == 500).FirstOrDefault();

                                if (newReviewsbyReviewSource != null)
                                {
                                    int NewReviewsCount = newReviewsbyReviewSource.NewReviewsCount;
                                    decimal NewReviewsRatingSum = newReviewsbyReviewSource.NewReviewsRatingSum;
                                    decimal NewReviewAvgRating = 0;

                                    if (NewReviewsCount > 0)
                                    {
                                        NewReviewAvgRating = Convert.ToDecimal(NewReviewsRatingSum / Convert.ToDecimal(NewReviewsCount));
                                    }

                                    double total = newReviewsbyReviewSource.TotalPositiveReviews + newReviewsbyReviewSource.TotalNegativeReviews;
                                    int PercentPositive = 0;
                                    int PercentNegative = 0;
                                    if (total > 0)
                                    {
                                        PercentNegative = (int)(((newReviewsbyReviewSource.TotalNegativeReviews / total) * 100) + 0.5);
                                        PercentPositive = 100 - PercentNegative;

                                        if (PercentNegative + PercentPositive < 100 && PercentPositive < 100)
                                            PercentPositive++;
                                    }

                                    template = template.Replace("[$CarDealerReviews_NewReview$]", NewReviewsCount.ToString("#,##0"))
                                                        .Replace("[$CarDealerReviews_NewReviewRating$]", Math.Round(NewReviewAvgRating, 1).ToString())
                                                        .Replace("[$CarDealerReviews_Total$]", newReviewsbyReviewSource.SalesReviewCount.ToString("#,##0"))
                                                        .Replace("[$CarDealerReviews_PositivePercent$]", PercentPositive.ToString() + "%")
                                                        .Replace("[$CarDealerReviews_NegativePercent$]", PercentNegative.ToString() + "%")
                                                        .Replace("[$CarDealerReviews_Rating$]", newReviewsbyReviewSource.AvgRating.ToString());


                                }

                                template = template.Replace("[$CarDealerReviews_NewReview$]", "0")
                                                        .Replace("[$CarDealerReviews_NewReviewRating$]", "0")
                                                        .Replace("[$CarDealerReviews_Total$]", "0")
                                                        .Replace("[$CarDealerReviews_PositivePercent$]", "0%")
                                                        .Replace("[$CarDealerReviews_NegativePercent$]", "0%")
                                                        .Replace("[$CarDealerReviews_Rating$]", "0");
                                #endregion
                            }

                            #endregion

                        }
                    }
                }
                
            }
            catch (Exception ex)
            {
            }

            return template;
        }

		public string GetSocialSnapshotReportHTML(long AccountID, DateTime date, bool printHtml)
        {
            string template = string.Empty;
            try
            {
                using (MainEntities db = ContextFactory.Main)
                {
                    if (db.Connection.State != System.Data.ConnectionState.Open)
                        db.Connection.Open();


                    UserInfoDTO UserInfo = new UserInfoDTO(1, null); //system
                    var Notificationtemplate = new NotificationTemplate();
                    string ResellerLogo = string.Empty;

                    if (!printHtml)
                    {
                        Notificationtemplate = (from nt in db.NotificationTemplates where nt.ID == 57 select nt).SingleOrDefault();
                    }
                    else
                    {
                        Notificationtemplate = (from nt in db.NotificationTemplates where nt.ID == 60 select nt).SingleOrDefault();
                    }
                    //Get ResellerId by AccountID
                    long resellerID = ProviderFactory.Security.GetResellerByAccountID(UserInfo, AccountID, TimeZoneInfo.Utc).ID.Value;
                    if (resellerID != null && resellerID != 0)
                        ResellerLogo = (from r in db.Resellers where r.ID == resellerID select r.Logo).FirstOrDefault();

                    if (string.IsNullOrEmpty(ResellerLogo))
                    {
                        ResellerLogo = (from r in db.Resellers where r.ID == 5 select r.Logo).FirstOrDefault(); //ResellerID (5) Default "SocialDealer"
                    }

                    PlatformSocialHTMLReport TemplateDTONew = JsonConvert.DeserializeObject<PlatformSocialHTMLReport>(Notificationtemplate.Body);
                    template = TemplateDTONew.Body;

                    GetEntityDTO<AccountDTO> dto2 = ProviderFactory.Security.GetAccountByID(UserInfo, AccountID);

                    using (DbCommand cmd3 = db.CreateStoreCommand(
                       "[report].spGetSocialSnapshotReport",
                       System.Data.CommandType.StoredProcedure,
                           new SqlParameter("@AccountIDs", AccountID),
                           new SqlParameter("@today", date)
                       ))
                    {
                        using (DbDataReader reader = cmd3.ExecuteReader())
                        {

                            List<MonthData> ListMonthData = db.Translate<MonthData>(reader).ToList();

                            reader.NextResult();

                            List<AudienceFacebook> ListAudienceFacebook = db.Translate<AudienceFacebook>(reader).ToList();

                            reader.NextResult();

                            List<AudienceTwitter> ListAudienceTwitter = db.Translate<AudienceTwitter>(reader).ToList();

                            reader.NextResult();

                            List<AudienceGooglePlus> ListAudienceGooglePlus = db.Translate<AudienceGooglePlus>(reader).ToList();

                            reader.NextResult();

                            List<FacebookPostData> ListFacebookPostData = db.Translate<FacebookPostData>(reader).ToList();

                            reader.NextResult();

                            List<TwitterPostData> ListTwitterPostData = db.Translate<TwitterPostData>(reader).ToList();

                            reader.NextResult();

                            List<GooglePlusPostData> ListGooglePlusPostData = db.Translate<GooglePlusPostData>(reader).ToList();

                            #region Reseller Logo for Print
                            if (printHtml)
                            {
                                template = template.Replace("[$ResellerLogo$]", ResellerLogo);
                            }

                            #endregion

                            #region Month/Year/AccountName

                            string Month = Convert.ToDateTime(date).ToString("MMMM", CultureInfo.CreateSpecificCulture("en-US"));
                            string Year = Convert.ToDateTime(date).Year.ToString();

                            template = template.Replace("[$AccountName$]", dto2.Entity.DisplayName)
                                                .Replace("[$Month$]", Month)
                                                .Replace("[$Year$]", Year);

                            #endregion

                            #region Month Name
                            if (ListMonthData != null)
                            {
                                foreach (var monthData in ListMonthData)
                                {
                                    int month = monthData.StartDate.Month;
                                    string strMonthName = string.Empty;
                                    if (monthData.ID == 6)
                                    {
                                        strMonthName = CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName(month);
                                        template = template.Replace("[$Month6$]", strMonthName);
                                    }
                                    if (monthData.ID == 5)
                                    {
                                        strMonthName = CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName(month);
                                        template = template.Replace("[$Month5$]", strMonthName);
                                    }
                                    if (monthData.ID == 4)
                                    {
                                        strMonthName = CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName(month);
                                        template = template.Replace("[$Month4$]", strMonthName);
                                    }
                                    if (monthData.ID == 3)
                                    {
                                        strMonthName = CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName(month);
                                        template = template.Replace("[$Month3$]", strMonthName);
                                    }
                                    if (monthData.ID == 2)
                                    {
                                        strMonthName = CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName(month);
                                        template = template.Replace("[$Month2$]", strMonthName);
                                    }
                                    if (monthData.ID == 1)
                                    {
                                        strMonthName = CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName(month);
                                        template = template.Replace("[$Month1$]", strMonthName);
                                    }
                                }
                            }
                            #endregion

                            #region Audience/NewAudience

                            int FacebookAudience = 0;
                            int TwitterAudience = 0;
                            int GooglePlusAudience = 0;
                            int NewFacebookAudience = 0;
                            int NewTwitterAudience = 0;
                            int NewGooglePlusAudience = 0;
     
                            int Audience = 0;
                            int NewAudience = 0;

                            if (ListAudienceFacebook != null)
                            {
                                var fb = ListAudienceFacebook.Where(f => f.MonthID == 1).SingleOrDefault();
                                if (fb != null)
                                {
                                    FacebookAudience = fb.Fans;
                                    NewFacebookAudience = fb.NewFans;
                                }
                            }

                            if (ListAudienceTwitter != null)
                            {
                                var tw = ListAudienceTwitter.Where(t => t.MonthID == 1).SingleOrDefault();
                                if (tw != null)
                                {
                                    TwitterAudience = tw.Followers;
                                    NewTwitterAudience = tw.NewFollowers;
                                }
                            }

                            if (ListAudienceGooglePlus != null)
                            {
                                var gp = ListAudienceGooglePlus.Where(g => g.MonthID == 1).SingleOrDefault();
                                if (gp != null)
                                {
                                    GooglePlusAudience = gp.PlusOneCount;
                                    NewGooglePlusAudience = gp.NewPlusOneCount;
                                }
                            }

                            Audience = FacebookAudience + TwitterAudience + GooglePlusAudience;
                            NewAudience = NewFacebookAudience + NewTwitterAudience + NewGooglePlusAudience;

                            template = template.Replace("[$TotalAudiences$]", Audience.ToString("#,##0"))
                                                .Replace("[$NewAudience$]", NewAudience.ToString("#,##0"));
                                                

                            #endregion

                            #region Audience (Monthly)

                            if (ListAudienceFacebook != null)
                            {
                                var audienceFacebook = ListAudienceFacebook.Where(n => n.MonthID == 6).FirstOrDefault();
                                var audienceTwitter = ListAudienceTwitter.Where(n => n.MonthID == 6).FirstOrDefault();
                                var audienceGooglePlus = ListAudienceGooglePlus.Where(n => n.MonthID == 6).FirstOrDefault();

                                int TotalAudience_Faebook_Month = 0;
                                int TotalAudience_Twitter_Month = 0;
                                int TotalAudience_GooglePlus_Month = 0;
                                int NewTotalAudience_Faebook_Month = 0;
                                int NewTotalAudience_Twitter_Month = 0;
                                int NewTotalAudience_GooglePlus_Month = 0;

                                int TotalAudience_Month6 = 0;
                                int NewAudience_Month6 = 0;

                                #region Month 6
                                if (audienceFacebook != null)
                                {
                                    TotalAudience_Faebook_Month = audienceFacebook.Fans;
                                    NewTotalAudience_Faebook_Month = audienceFacebook.NewFans;
                                }

                                if (audienceTwitter != null)
                                {
                                    TotalAudience_Twitter_Month = audienceTwitter.Followers;
                                    NewTotalAudience_Twitter_Month = audienceTwitter.NewFollowers;
                                }

                                if (audienceGooglePlus != null)
                                {
                                    TotalAudience_GooglePlus_Month = audienceGooglePlus.PlusOneCount;
                                    NewTotalAudience_GooglePlus_Month = audienceGooglePlus.NewPlusOneCount;
                                }

                                TotalAudience_Month6 = TotalAudience_Faebook_Month + TotalAudience_Twitter_Month + TotalAudience_GooglePlus_Month;
                                NewAudience_Month6 = NewTotalAudience_Faebook_Month + NewTotalAudience_Twitter_Month + NewTotalAudience_GooglePlus_Month;

                                template = template.Replace("[$TotalAudience_Month6$]", TotalAudience_Month6.ToString("#,##0"))
                                                    .Replace("[$NewAudience_Month6$]", NewAudience_Month6.ToString("#,##0"))
                                                    .Replace("[$TotalAudience_Faebook_Month6$]", NewTotalAudience_Faebook_Month.ToString("#,##0"))
                                                    .Replace("[$TotalAudience_Twitter_Month6$]", NewTotalAudience_Twitter_Month.ToString("#,##0"))
                                                    .Replace("[$TotalAudience_Google_Month6$]", NewTotalAudience_GooglePlus_Month.ToString("#,##0"));

                                #endregion

                                #region Month 5
                                audienceFacebook = ListAudienceFacebook.Where(n => n.MonthID == 5).FirstOrDefault();
                                audienceTwitter = ListAudienceTwitter.Where(n => n.MonthID == 5).FirstOrDefault();
                                audienceGooglePlus = ListAudienceGooglePlus.Where(n => n.MonthID == 5).FirstOrDefault();

                                TotalAudience_Faebook_Month = 0;
                                TotalAudience_Twitter_Month = 0;
                                TotalAudience_GooglePlus_Month = 0;
                                NewTotalAudience_Faebook_Month = 0;
                                NewTotalAudience_Twitter_Month = 0;
                                NewTotalAudience_GooglePlus_Month = 0;

                                int TotalAudience_Month5 = 0;
                                int NewAudience_Month5 = 0;

                                if (audienceFacebook != null)
                                {
                                    TotalAudience_Faebook_Month = audienceFacebook.Fans;
                                    NewTotalAudience_Faebook_Month = audienceFacebook.NewFans;
                                }

                                if (audienceTwitter != null)
                                {
                                    TotalAudience_Twitter_Month = audienceTwitter.Followers;
                                    NewTotalAudience_Twitter_Month = audienceTwitter.NewFollowers;
                                }

                                if (audienceGooglePlus != null)
                                {
                                    TotalAudience_GooglePlus_Month = audienceGooglePlus.PlusOneCount;
                                    NewTotalAudience_GooglePlus_Month = audienceGooglePlus.NewPlusOneCount;
                                }

                                TotalAudience_Month5 = TotalAudience_Faebook_Month + TotalAudience_Twitter_Month + TotalAudience_GooglePlus_Month;
                                NewAudience_Month5 = NewTotalAudience_Faebook_Month + NewTotalAudience_Twitter_Month + NewTotalAudience_GooglePlus_Month;

                                template = template.Replace("[$TotalAudience_Month5$]", TotalAudience_Month5.ToString("#,##0"))
                                                    .Replace("[$NewAudience_Month5$]", NewAudience_Month5.ToString("#,##0"))
                                                    .Replace("[$TotalAudience_Faebook_Month5$]", NewTotalAudience_Faebook_Month.ToString("#,##0"))
                                                    .Replace("[$TotalAudience_Twitter_Month5$]", NewTotalAudience_Twitter_Month.ToString("#,##0"))
                                                    .Replace("[$TotalAudience_Google_Month5$]", NewTotalAudience_GooglePlus_Month.ToString("#,##0"));


                                #endregion

                                #region Month 4
                                audienceFacebook = ListAudienceFacebook.Where(n => n.MonthID == 4).FirstOrDefault();
                                audienceTwitter = ListAudienceTwitter.Where(n => n.MonthID == 4).FirstOrDefault();
                                audienceGooglePlus = ListAudienceGooglePlus.Where(n => n.MonthID == 4).FirstOrDefault();

                                TotalAudience_Faebook_Month = 0;
                                TotalAudience_Twitter_Month = 0;
                                TotalAudience_GooglePlus_Month = 0;
                                NewTotalAudience_Faebook_Month = 0;
                                NewTotalAudience_Twitter_Month = 0;
                                NewTotalAudience_GooglePlus_Month = 0;

                                int TotalAudience_Month4 = 0;
                                int NewAudience_Month4 = 0;

                                if (audienceFacebook != null)
                                {
                                    TotalAudience_Faebook_Month = audienceFacebook.Fans;
                                    NewTotalAudience_Faebook_Month = audienceFacebook.NewFans;
                                }

                                if (audienceTwitter != null)
                                {
                                    TotalAudience_Twitter_Month = audienceTwitter.Followers;
                                    NewTotalAudience_Twitter_Month = audienceTwitter.NewFollowers;
                                }

                                if (audienceGooglePlus != null)
                                {
                                    TotalAudience_GooglePlus_Month = audienceGooglePlus.PlusOneCount;
                                    NewTotalAudience_GooglePlus_Month = audienceGooglePlus.NewPlusOneCount;
                                }

                                TotalAudience_Month4 = TotalAudience_Faebook_Month + TotalAudience_Twitter_Month + TotalAudience_GooglePlus_Month;
                                NewAudience_Month4 = NewTotalAudience_Faebook_Month + NewTotalAudience_Twitter_Month + NewTotalAudience_GooglePlus_Month;

                                template = template.Replace("[$TotalAudience_Month4$]", TotalAudience_Month4.ToString("#,##0"))
                                                    .Replace("[$NewAudience_Month4$]", NewAudience_Month4.ToString("#,##0"))
                                                    .Replace("[$TotalAudience_Faebook_Month4$]", NewTotalAudience_Faebook_Month.ToString("#,##0"))
                                                    .Replace("[$TotalAudience_Twitter_Month4$]", NewTotalAudience_Twitter_Month.ToString("#,##0"))
                                                    .Replace("[$TotalAudience_Google_Month4$]", NewTotalAudience_GooglePlus_Month.ToString("#,##0"));


                                #endregion

                                #region Month 3
                                audienceFacebook = ListAudienceFacebook.Where(n => n.MonthID == 3).FirstOrDefault();
                                audienceTwitter = ListAudienceTwitter.Where(n => n.MonthID == 3).FirstOrDefault();
                                audienceGooglePlus = ListAudienceGooglePlus.Where(n => n.MonthID == 3).FirstOrDefault();

                                TotalAudience_Faebook_Month = 0;
                                TotalAudience_Twitter_Month = 0;
                                TotalAudience_GooglePlus_Month = 0;
                                NewTotalAudience_Faebook_Month = 0;
                                NewTotalAudience_Twitter_Month = 0;
                                NewTotalAudience_GooglePlus_Month = 0;

                                int TotalAudience_Month3 = 0;
                                int NewAudience_Month3 = 0;

                                if (audienceFacebook != null)
                                {
                                    TotalAudience_Faebook_Month = audienceFacebook.Fans;
                                    NewTotalAudience_Faebook_Month = audienceFacebook.NewFans;
                                }

                                if (audienceTwitter != null)
                                {
                                    TotalAudience_Twitter_Month = audienceTwitter.Followers;
                                    NewTotalAudience_Twitter_Month = audienceTwitter.NewFollowers;
                                }

                                if (audienceGooglePlus != null)
                                {
                                    TotalAudience_GooglePlus_Month = audienceGooglePlus.PlusOneCount;
                                    NewTotalAudience_GooglePlus_Month = audienceGooglePlus.NewPlusOneCount;
                                }

                                TotalAudience_Month3 = TotalAudience_Faebook_Month + TotalAudience_Twitter_Month + TotalAudience_GooglePlus_Month;
                                NewAudience_Month3 = NewTotalAudience_Faebook_Month + NewTotalAudience_Twitter_Month + NewTotalAudience_GooglePlus_Month;

                                template = template.Replace("[$TotalAudience_Month3$]", TotalAudience_Month3.ToString("#,##0"))
                                                    .Replace("[$NewAudience_Month3$]", NewAudience_Month3.ToString("#,##0"))
                                                    .Replace("[$TotalAudience_Faebook_Month3$]", NewTotalAudience_Faebook_Month.ToString("#,##0"))
                                                    .Replace("[$TotalAudience_Twitter_Month3$]", NewTotalAudience_Twitter_Month.ToString("#,##0"))
                                                    .Replace("[$TotalAudience_Google_Month3$]", NewTotalAudience_GooglePlus_Month.ToString("#,##0"));


                                #endregion

                                #region Month 2
                                audienceFacebook = ListAudienceFacebook.Where(n => n.MonthID == 2).FirstOrDefault();
                                audienceTwitter = ListAudienceTwitter.Where(n => n.MonthID == 2).FirstOrDefault();
                                audienceGooglePlus = ListAudienceGooglePlus.Where(n => n.MonthID == 2).FirstOrDefault();

                                TotalAudience_Faebook_Month = 0;
                                TotalAudience_Twitter_Month = 0;
                                TotalAudience_GooglePlus_Month = 0;
                                NewTotalAudience_Faebook_Month = 0;
                                NewTotalAudience_Twitter_Month = 0;
                                NewTotalAudience_GooglePlus_Month = 0;

                                int TotalAudience_Month2 = 0;
                                int NewAudience_Month2 = 0;

                                if (audienceFacebook != null)
                                {
                                    TotalAudience_Faebook_Month = audienceFacebook.Fans;
                                    NewTotalAudience_Faebook_Month = audienceFacebook.NewFans;
                                }

                                if (audienceTwitter != null)
                                {
                                    TotalAudience_Twitter_Month = audienceTwitter.Followers;
                                    NewTotalAudience_Twitter_Month = audienceTwitter.NewFollowers;
                                }

                                if (audienceGooglePlus != null)
                                {
                                    TotalAudience_GooglePlus_Month = audienceGooglePlus.PlusOneCount;
                                    NewTotalAudience_GooglePlus_Month = audienceGooglePlus.NewPlusOneCount;
                                }

                                TotalAudience_Month2 = TotalAudience_Faebook_Month + TotalAudience_Twitter_Month + TotalAudience_GooglePlus_Month;
                                NewAudience_Month2 = NewTotalAudience_Faebook_Month + NewTotalAudience_Twitter_Month + NewTotalAudience_GooglePlus_Month;

                                template = template.Replace("[$TotalAudience_Month2$]", TotalAudience_Month2.ToString("#,##0"))
                                                    .Replace("[$NewAudience_Month2$]", NewAudience_Month2.ToString("#,##0"))
                                                    .Replace("[$TotalAudience_Faebook_Month2$]", NewTotalAudience_Faebook_Month.ToString("#,##0"))
                                                    .Replace("[$TotalAudience_Twitter_Month2$]", NewTotalAudience_Twitter_Month.ToString("#,##0"))
                                                    .Replace("[$TotalAudience_Google_Month2$]", NewTotalAudience_GooglePlus_Month.ToString("#,##0"));


                                #endregion

                                #region Month 1
                                audienceFacebook = ListAudienceFacebook.Where(n => n.MonthID == 1).FirstOrDefault();
                                audienceTwitter = ListAudienceTwitter.Where(n => n.MonthID == 1).FirstOrDefault();
                                audienceGooglePlus = ListAudienceGooglePlus.Where(n => n.MonthID == 1).FirstOrDefault();

                                TotalAudience_Faebook_Month = 0;
                                TotalAudience_Twitter_Month = 0;
                                TotalAudience_GooglePlus_Month = 0;
                                NewTotalAudience_Faebook_Month = 0;
                                NewTotalAudience_Twitter_Month = 0;
                                NewTotalAudience_GooglePlus_Month = 0;

                                int TotalAudience_Month1 = 0;
                                int NewAudience_Month1 = 0;

                                if (audienceFacebook != null)
                                {
                                    TotalAudience_Faebook_Month = audienceFacebook.Fans;
                                    NewTotalAudience_Faebook_Month = audienceFacebook.NewFans;
                                }

                                if (audienceTwitter != null)
                                {
                                    TotalAudience_Twitter_Month = audienceTwitter.Followers;
                                    NewTotalAudience_Twitter_Month = audienceTwitter.NewFollowers;
                                }

                                if (audienceGooglePlus != null)
                                {
                                    TotalAudience_GooglePlus_Month = audienceGooglePlus.PlusOneCount;
                                    NewTotalAudience_GooglePlus_Month = audienceGooglePlus.NewPlusOneCount;
                                }

                                TotalAudience_Month1 = TotalAudience_Faebook_Month + TotalAudience_Twitter_Month + TotalAudience_GooglePlus_Month;
                                NewAudience_Month1 = NewTotalAudience_Faebook_Month + NewTotalAudience_Twitter_Month + NewTotalAudience_GooglePlus_Month;

                                template = template.Replace("[$TotalAudience_Month1$]", TotalAudience_Month1.ToString("#,##0"))
                                                    .Replace("[$NewAudience_Month1$]", NewAudience_Month1.ToString("#,##0"))
                                                    .Replace("[$TotalAudience_Faebook_Month1$]", NewTotalAudience_Faebook_Month.ToString("#,##0"))
                                                    .Replace("[$TotalAudience_Twitter_Month1$]", NewTotalAudience_Twitter_Month.ToString("#,##0"))
                                                    .Replace("[$TotalAudience_Google_Month1$]", NewTotalAudience_GooglePlus_Month.ToString("#,##0"));


                                #endregion

                            }
                            else
                            {                                
                                template = template.Replace("[$TotalAudience_Month6$]", "0")
                                                   .Replace("[$NewAudience_Month6$]", "0")
                                                   .Replace("[$TotalAudience_Faebook_Month6$]", "0")
                                                   .Replace("[$TotalAudience_Twitter_Month6$]", "0")
                                                   .Replace("[$TotalAudience_Google_Month6$]", "0");

                                template = template.Replace("[$TotalAudience_Month5$]", "0")
                                                   .Replace("[$NewAudience_Month5$]", "0")
                                                   .Replace("[$TotalAudience_Faebook_Month5$]", "0")
                                                   .Replace("[$TotalAudience_Twitter_Month5$]", "0")
                                                   .Replace("[$TotalAudience_Google_Month5$]", "0");

                                template = template.Replace("[$TotalAudience_Month4$]", "0")
                                                   .Replace("[$NewAudience_Month4$]", "0")
                                                   .Replace("[$TotalAudience_Faebook_Month4$]", "0")
                                                   .Replace("[$TotalAudience_Twitter_Month4$]", "0")
                                                   .Replace("[$TotalAudience_Google_Month4$]", "0");

                                template = template.Replace("[$TotalAudience_Month3$]", "0")
                                                   .Replace("[$NewAudience_Month3$]", "0")
                                                   .Replace("[$TotalAudience_Faebook_Month3$]", "0")
                                                   .Replace("[$TotalAudience_Twitter_Month3$]", "0")
                                                   .Replace("[$TotalAudience_Google_Month3$]", "0");

                                template = template.Replace("[$TotalAudience_Month2$]", "0")
                                                   .Replace("[$NewAudience_Month2$]", "0")
                                                   .Replace("[$TotalAudience_Faebook_Month2$]", "0")
                                                   .Replace("[$TotalAudience_Twitter_Month2$]", "0")
                                                   .Replace("[$TotalAudience_Google_Month2$]", "0");

                                template = template.Replace("[$TotalAudience_Month1$]", "0")
                                                   .Replace("[$NewAudience_Month1$]", "0")
                                                   .Replace("[$TotalAudience_Faebook_Month1$]", "0")
                                                   .Replace("[$TotalAudience_Twitter_Month1$]", "0")
                                                   .Replace("[$TotalAudience_Google_Month1$]", "0");
                            }
                            #endregion

                            #region Facebook Post Stats

                            if (ListFacebookPostData != null)
                            {
                                var facebookPostData = ListFacebookPostData.Where(n => n.MonthID == 6).FirstOrDefault();
                                
                                int Faebook_Post = 0;
                                int Faebook_NewLikes = 0;
                                int Faebook_Comments = 0;
                                int Faebook_Clicks = 0;
                                int Faebook_Shares = 0;
                                int Faebook_OrganicImpressions = 0;
                                int Faebook_PaidImpressions = 0;

                                #region Month 6
                                if (facebookPostData != null)
                                {
                                    Faebook_Post = facebookPostData.Posts;
                                    Faebook_NewLikes = facebookPostData.NewLikes;
                                    Faebook_Comments = facebookPostData.Comments;
                                    Faebook_Clicks = facebookPostData.Clicks;
                                    Faebook_Shares = facebookPostData.Shares;
                                    Faebook_OrganicImpressions = facebookPostData.OrganicImpressions;
                                    Faebook_PaidImpressions = facebookPostData.PaidImpressions;
                                }

                                template = template.Replace("[$Post_Month6$]", Faebook_Post.ToString("#,##0"))
                                                   .Replace("[$NewLikes_Month6$]", Faebook_NewLikes.ToString("#,##0"))
                                                   .Replace("[$Comments_Month6$]", Faebook_Comments.ToString("#,##0"))
                                                   .Replace("[$Clicks_Month6$]", Faebook_Clicks.ToString("#,##0"))
                                                   .Replace("[$Shares_Month6$]", Faebook_Shares.ToString("#,##0"))
                                                   .Replace("[$OrganicImpressions_Month6$]", Faebook_OrganicImpressions.ToString("#,##0"))
                                                   .Replace("[$PaidImpressions_Month6$]", Faebook_PaidImpressions.ToString("#,##0"));

                                #endregion

                                #region Month 5
                                Faebook_Post = 0;
                                Faebook_NewLikes = 0;
                                Faebook_Comments = 0;
                                Faebook_Clicks = 0;
                                Faebook_Shares = 0;
                                Faebook_OrganicImpressions = 0;
                                Faebook_PaidImpressions = 0;

                                facebookPostData = ListFacebookPostData.Where(n => n.MonthID == 5).FirstOrDefault();

                                if (facebookPostData != null)
                                {
                                    Faebook_Post = facebookPostData.Posts;
                                    Faebook_NewLikes = facebookPostData.NewLikes;
                                    Faebook_Comments = facebookPostData.Comments;
                                    Faebook_Clicks = facebookPostData.Clicks;
                                    Faebook_Shares = facebookPostData.Shares;
                                    Faebook_OrganicImpressions = facebookPostData.OrganicImpressions;
                                    Faebook_PaidImpressions = facebookPostData.PaidImpressions;
                                }

                                template = template.Replace("[$Post_Month5$]", Faebook_Post.ToString("#,##0"))
                                                   .Replace("[$NewLikes_Month5$]", Faebook_NewLikes.ToString("#,##0"))
                                                   .Replace("[$Comments_Month5$]", Faebook_Comments.ToString("#,##0"))
                                                   .Replace("[$Clicks_Month5$]", Faebook_Clicks.ToString("#,##0"))
                                                   .Replace("[$Shares_Month5$]", Faebook_Shares.ToString("#,##0"))
                                                   .Replace("[$OrganicImpressions_Month5$]", Faebook_OrganicImpressions.ToString("#,##0"))
                                                   .Replace("[$PaidImpressions_Month5$]", Faebook_PaidImpressions.ToString("#,##0"));

                                #endregion

                                #region Month 4
                                Faebook_Post = 0;
                                Faebook_NewLikes = 0;
                                Faebook_Comments = 0;
                                Faebook_Clicks = 0;
                                Faebook_Shares = 0;
                                Faebook_OrganicImpressions = 0;
                                Faebook_PaidImpressions = 0;

                                facebookPostData = ListFacebookPostData.Where(n => n.MonthID == 4).FirstOrDefault();

                                if (facebookPostData != null)
                                {
                                    Faebook_Post = facebookPostData.Posts;
                                    Faebook_NewLikes = facebookPostData.NewLikes;
                                    Faebook_Comments = facebookPostData.Comments;
                                    Faebook_Clicks = facebookPostData.Clicks;
                                    Faebook_Shares = facebookPostData.Shares;
                                    Faebook_OrganicImpressions = facebookPostData.OrganicImpressions;
                                    Faebook_PaidImpressions = facebookPostData.PaidImpressions;
                                }

                                template = template.Replace("[$Post_Month4$]", Faebook_Post.ToString("#,##0"))
                                                   .Replace("[$NewLikes_Month4$]", Faebook_NewLikes.ToString("#,##0"))
                                                   .Replace("[$Comments_Month4$]", Faebook_Comments.ToString("#,##0"))
                                                   .Replace("[$Clicks_Month4$]", Faebook_Clicks.ToString("#,##0"))
                                                   .Replace("[$Shares_Month4$]", Faebook_Shares.ToString("#,##0"))
                                                   .Replace("[$OrganicImpressions_Month4$]", Faebook_OrganicImpressions.ToString("#,##0"))
                                                   .Replace("[$PaidImpressions_Month4$]", Faebook_PaidImpressions.ToString("#,##0"));

                                #endregion

                                #region Month 3
                                Faebook_Post = 0;
                                Faebook_NewLikes = 0;
                                Faebook_Comments = 0;
                                Faebook_Clicks = 0;
                                Faebook_Shares = 0;
                                Faebook_OrganicImpressions = 0;
                                Faebook_PaidImpressions = 0;

                                facebookPostData = ListFacebookPostData.Where(n => n.MonthID == 3).FirstOrDefault();

                                if (facebookPostData != null)
                                {
                                    Faebook_Post = facebookPostData.Posts;
                                    Faebook_NewLikes = facebookPostData.NewLikes;
                                    Faebook_Comments = facebookPostData.Comments;
                                    Faebook_Clicks = facebookPostData.Clicks;
                                    Faebook_Shares = facebookPostData.Shares;
                                    Faebook_OrganicImpressions = facebookPostData.OrganicImpressions;
                                    Faebook_PaidImpressions = facebookPostData.PaidImpressions;
                                }

                                template = template.Replace("[$Post_Month3$]", Faebook_Post.ToString("#,##0"))
                                                   .Replace("[$NewLikes_Month3$]", Faebook_NewLikes.ToString("#,##0"))
                                                   .Replace("[$Comments_Month3$]", Faebook_Comments.ToString("#,##0"))
                                                   .Replace("[$Clicks_Month3$]", Faebook_Clicks.ToString("#,##0"))
                                                   .Replace("[$Shares_Month3$]", Faebook_Shares.ToString("#,##0"))
                                                   .Replace("[$OrganicImpressions_Month3$]", Faebook_OrganicImpressions.ToString("#,##0"))
                                                   .Replace("[$PaidImpressions_Month3$]", Faebook_PaidImpressions.ToString("#,##0"));

                                #endregion

                                #region Month 2
                                Faebook_Post = 0;
                                Faebook_NewLikes = 0;
                                Faebook_Comments = 0;
                                Faebook_Clicks = 0;
                                Faebook_Shares = 0;
                                Faebook_OrganicImpressions = 0;
                                Faebook_PaidImpressions = 0;

                                facebookPostData = ListFacebookPostData.Where(n => n.MonthID == 2).FirstOrDefault();

                                if (facebookPostData != null)
                                {
                                    Faebook_Post = facebookPostData.Posts;
                                    Faebook_NewLikes = facebookPostData.NewLikes;
                                    Faebook_Comments = facebookPostData.Comments;
                                    Faebook_Clicks = facebookPostData.Clicks;
                                    Faebook_Shares = facebookPostData.Shares;
                                    Faebook_OrganicImpressions = facebookPostData.OrganicImpressions;
                                    Faebook_PaidImpressions = facebookPostData.PaidImpressions;
                                }

                                template = template.Replace("[$Post_Month2$]", Faebook_Post.ToString("#,##0"))
                                                   .Replace("[$NewLikes_Month2$]", Faebook_NewLikes.ToString("#,##0"))
                                                   .Replace("[$Comments_Month2$]", Faebook_Comments.ToString("#,##0"))
                                                   .Replace("[$Clicks_Month2$]", Faebook_Clicks.ToString("#,##0"))
                                                   .Replace("[$Shares_Month2$]", Faebook_Shares.ToString("#,##0"))
                                                   .Replace("[$OrganicImpressions_Month2$]", Faebook_OrganicImpressions.ToString("#,##0"))
                                                   .Replace("[$PaidImpressions_Month2$]", Faebook_PaidImpressions.ToString("#,##0"));

                                #endregion

                                #region Month 1
                                Faebook_Post = 0;
                                Faebook_NewLikes = 0;
                                Faebook_Comments = 0;
                                Faebook_Clicks = 0;
                                Faebook_Shares = 0;
                                Faebook_OrganicImpressions = 0;
                                Faebook_PaidImpressions = 0;

                                facebookPostData = ListFacebookPostData.Where(n => n.MonthID == 1).FirstOrDefault();

                                if (facebookPostData != null)
                                {
                                    Faebook_Post = facebookPostData.Posts;
                                    Faebook_NewLikes = facebookPostData.NewLikes;
                                    Faebook_Comments = facebookPostData.Comments;
                                    Faebook_Clicks = facebookPostData.Clicks;
                                    Faebook_Shares = facebookPostData.Shares;
                                    Faebook_OrganicImpressions = facebookPostData.OrganicImpressions;
                                    Faebook_PaidImpressions = facebookPostData.PaidImpressions;
                                }

                                template = template.Replace("[$Post_Month1$]", Faebook_Post.ToString("#,##0"))
                                                   .Replace("[$NewLikes_Month1$]", Faebook_NewLikes.ToString("#,##0"))
                                                   .Replace("[$Comments_Month1$]", Faebook_Comments.ToString("#,##0"))
                                                   .Replace("[$Clicks_Month1$]", Faebook_Clicks.ToString("#,##0"))
                                                   .Replace("[$Shares_Month1$]", Faebook_Shares.ToString("#,##0"))
                                                   .Replace("[$OrganicImpressions_Month1$]", Faebook_OrganicImpressions.ToString("#,##0"))
                                                   .Replace("[$PaidImpressions_Month1$]", Faebook_PaidImpressions.ToString("#,##0"));

                                #endregion
                                
                            }
                            else
                            {
                                template = template.Replace("[$Post_Month6$]", "0")
                                                   .Replace("[$NewLikes_Month6$]", "0")
                                                   .Replace("[$Comments_Month6$]", "0")
                                                   .Replace("[$Clicks_Month6$]", "0")
                                                   .Replace("[$Shares_Month6$]", "0")
                                                   .Replace("[$OrganicImpressions_Month6$]", "0")
                                                   .Replace("[$PaidImpressions_Month6$]", "0");

                                template = template.Replace("[$Post_Month5$]", "0")
                                                  .Replace("[$NewLikes_Month5$]", "0")
                                                  .Replace("[$Comments_Month5$]", "0")
                                                  .Replace("[$Clicks_Month5$]", "0")
                                                  .Replace("[$Shares_Month5$]", "0")
                                                  .Replace("[$OrganicImpressions_Month5$]", "0")
                                                  .Replace("[$PaidImpressions_Month5$]", "0");

                                template = template.Replace("[$Post_Month4$]", "0")
                                                  .Replace("[$NewLikes_Month4$]", "0")
                                                  .Replace("[$Comments_Month4$]", "0")
                                                  .Replace("[$Clicks_Month4$]", "0")
                                                  .Replace("[$Shares_Month4$]", "0")
                                                  .Replace("[$OrganicImpressions_Month4$]", "0")
                                                  .Replace("[$PaidImpressions_Month4$]", "0");

                                template = template.Replace("[$Post_Month3$]", "0")
                                                  .Replace("[$NewLikes_Month3$]", "0")
                                                  .Replace("[$Comments_Month3$]", "0")
                                                  .Replace("[$Clicks_Month3$]", "0")
                                                  .Replace("[$Shares_Month3$]", "0")
                                                  .Replace("[$OrganicImpressions_Month3$]", "0")
                                                  .Replace("[$PaidImpressions_Month3$]", "0");

                                template = template.Replace("[$Post_Month2$]", "0")
                                                  .Replace("[$NewLikes_Month2$]", "0")
                                                  .Replace("[$Comments_Month2$]", "0")
                                                  .Replace("[$Clicks_Month2$]", "0")
                                                  .Replace("[$Shares_Month2$]", "0")
                                                  .Replace("[$OrganicImpressions_Month2$]", "0")
                                                  .Replace("[$PaidImpressions_Month2$]", "0");

                                template = template.Replace("[$Post_Month1$]", "0")
                                                  .Replace("[$NewLikes_Month1$]", "0")
                                                  .Replace("[$Comments_Month1$]", "0")
                                                  .Replace("[$Clicks_Month1$]", "0")
                                                  .Replace("[$Shares_Month1$]", "0")
                                                  .Replace("[$OrganicImpressions_Month1$]", "0")
                                                  .Replace("[$PaidImpressions_Month1$]", "0");
                            }
                            #endregion

                            #region Twitter Post Stats
                            if (ListTwitterPostData != null)
                            {
                                var twitterPostData = ListTwitterPostData.Where(n => n.MonthID == 6).FirstOrDefault();

                                int Tweets_Month = 0;
                                int ReTweets_Month = 0;

                                #region Month 6
                                if (twitterPostData != null)
                                {
                                    Tweets_Month = twitterPostData.Tweets;
                                    ReTweets_Month = twitterPostData.ReTweetCount;                                    
                                }

                                template = template.Replace("[$Tweets_Month6$]", Tweets_Month.ToString("#,##0"))
                                                    .Replace("[$Re-Tweets_Month6$]", ReTweets_Month.ToString("#,##0"));
                                #endregion

                                #region Month 5
                                Tweets_Month = 0;
                                ReTweets_Month = 0;

                                twitterPostData = ListTwitterPostData.Where(n => n.MonthID == 5).FirstOrDefault();
                                if (twitterPostData != null)
                                {
                                    Tweets_Month = twitterPostData.Tweets;
                                    ReTweets_Month = twitterPostData.ReTweetCount;
                                }

                                template = template.Replace("[$Tweets_Month5$]", Tweets_Month.ToString("#,##0"))
                                                    .Replace("[$Re-Tweets_Month5$]", ReTweets_Month.ToString("#,##0"));
                                #endregion

                                #region Month 4
                                Tweets_Month = 0;
                                ReTweets_Month = 0;

                                twitterPostData = ListTwitterPostData.Where(n => n.MonthID == 4).FirstOrDefault();
                                if (twitterPostData != null)
                                {
                                    Tweets_Month = twitterPostData.Tweets;
                                    ReTweets_Month = twitterPostData.ReTweetCount;
                                }

                                template = template.Replace("[$Tweets_Month4$]", Tweets_Month.ToString("#,##0"))
                                                    .Replace("[$Re-Tweets_Month4$]", ReTweets_Month.ToString("#,##0"));
                                #endregion

                                #region Month 3
                                Tweets_Month = 0;
                                ReTweets_Month = 0;

                                twitterPostData = ListTwitterPostData.Where(n => n.MonthID == 3).FirstOrDefault();
                                if (twitterPostData != null)
                                {
                                    Tweets_Month = twitterPostData.Tweets;
                                    ReTweets_Month = twitterPostData.ReTweetCount;
                                }

                                template = template.Replace("[$Tweets_Month3$]", Tweets_Month.ToString("#,##0"))
                                                    .Replace("[$Re-Tweets_Month3$]", ReTweets_Month.ToString("#,##0"));
                                #endregion

                                #region Month 2
                                Tweets_Month = 0;
                                ReTweets_Month = 0;

                                twitterPostData = ListTwitterPostData.Where(n => n.MonthID == 2).FirstOrDefault();
                                if (twitterPostData != null)
                                {
                                    Tweets_Month = twitterPostData.Tweets;
                                    ReTweets_Month = twitterPostData.ReTweetCount;
                                }

                                template = template.Replace("[$Tweets_Month2$]", Tweets_Month.ToString("#,##0"))
                                                    .Replace("[$Re-Tweets_Month2$]", ReTweets_Month.ToString("#,##0"));
                                #endregion

                                #region Month 1
                                Tweets_Month = 0;
                                ReTweets_Month = 0;

                                twitterPostData = ListTwitterPostData.Where(n => n.MonthID == 1).FirstOrDefault();
                                if (twitterPostData != null)
                                {
                                    Tweets_Month = twitterPostData.Tweets;
                                    ReTweets_Month = twitterPostData.ReTweetCount;
                                }

                                template = template.Replace("[$Tweets_Month1$]", Tweets_Month.ToString("#,##0"))
                                                    .Replace("[$Re-Tweets_Month1$]", ReTweets_Month.ToString("#,##0"));
                                #endregion
                            }
                            else
                            {
                                template = template.Replace("[$Tweets_Month6$]", "0")
                                                    .Replace("[$Re-Tweets_Month6$]", "0");
                                                                
                                template = template.Replace("[$Tweets_Month5$]", "0")
                                                    .Replace("[$Re-Tweets_Month5$]", "0");

                                template = template.Replace("[$Tweets_Month4$]", "0")
                                                    .Replace("[$Re-Tweets_Month4$]", "0");
                                                                
                                template = template.Replace("[$Tweets_Month3$]", "0")
                                                    .Replace("[$Re-Tweets_Month3$]", "0");

                                template = template.Replace("[$Tweets_Month2$]", "0")
                                                    .Replace("[$Re-Tweets_Month2$]", "0");

                                template = template.Replace("[$Tweets_Month1$]", "0")
                                                    .Replace("[$Re-Tweets_Month1$]", "0");

                            }
                            #endregion

                            #region GooglePlus Post Stats
                            if (ListGooglePlusPostData != null)
                            {
                                var googlePlusPostData = ListGooglePlusPostData.Where(n => n.MonthID == 6).FirstOrDefault();

                                int GooglePlus_Posts = 0;
                                int GooglePlus_Comments = 0;
                                int GooglePlus_PlusOnes = 0; 
                                int GooglePlus_ReShares = 0;
                                                                
                                #region Month 6
                                if (googlePlusPostData != null)
                                {
                                    GooglePlus_Posts = googlePlusPostData.Posts;
                                    GooglePlus_Comments = googlePlusPostData.Comments;
                                    GooglePlus_PlusOnes = googlePlusPostData.PlusOnes;
                                    GooglePlus_ReShares = googlePlusPostData.ReShares;
                                }

                                template = template.Replace("[$Google+Posts_Month6$]", GooglePlus_Posts.ToString("#,##0"))
                                                     .Replace("[$Google+Comments_Month6$]", GooglePlus_Comments.ToString("#,##0"))
                                                     .Replace("[$Google+PlusOnes_Month6$]", GooglePlus_PlusOnes.ToString("#,##0"))
                                                     .Replace("[$Google+ReShares_Month6$]", GooglePlus_ReShares.ToString("#,##0"));                               
                                #endregion

                                #region Month 5
                                GooglePlus_Posts = 0;
                                GooglePlus_Comments = 0;
                                GooglePlus_PlusOnes = 0;
                                GooglePlus_ReShares = 0;

                                googlePlusPostData = ListGooglePlusPostData.Where(n => n.MonthID == 5).FirstOrDefault();

                                if (googlePlusPostData != null)
                                {
                                    GooglePlus_Posts = googlePlusPostData.Posts;
                                    GooglePlus_Comments = googlePlusPostData.Comments;
                                    GooglePlus_PlusOnes = googlePlusPostData.PlusOnes;
                                    GooglePlus_ReShares = googlePlusPostData.ReShares;
                                }

                                template = template.Replace("[$Google+Posts_Month5$]", GooglePlus_Posts.ToString("#,##0"))
                                                     .Replace("[$Google+Comments_Month5$]", GooglePlus_Comments.ToString("#,##0"))
                                                     .Replace("[$Google+PlusOnes_Month5$]", GooglePlus_PlusOnes.ToString("#,##0"))
                                                     .Replace("[$Google+ReShares_Month5$]", GooglePlus_ReShares.ToString("#,##0"));
                                #endregion

                                #region Month 4
                                GooglePlus_Posts = 0;
                                GooglePlus_Comments = 0;
                                GooglePlus_PlusOnes = 0;
                                GooglePlus_ReShares = 0;

                                googlePlusPostData = ListGooglePlusPostData.Where(n => n.MonthID == 4).FirstOrDefault();

                                if (googlePlusPostData != null)
                                {
                                    GooglePlus_Posts = googlePlusPostData.Posts;
                                    GooglePlus_Comments = googlePlusPostData.Comments;
                                    GooglePlus_PlusOnes = googlePlusPostData.PlusOnes;
                                    GooglePlus_ReShares = googlePlusPostData.ReShares;
                                }

                                template = template.Replace("[$Google+Posts_Month4$]", GooglePlus_Posts.ToString("#,##0"))
                                                     .Replace("[$Google+Comments_Month4$]", GooglePlus_Comments.ToString("#,##0"))
                                                     .Replace("[$Google+PlusOnes_Month4$]", GooglePlus_PlusOnes.ToString("#,##0"))
                                                     .Replace("[$Google+ReShares_Month4$]", GooglePlus_ReShares.ToString("#,##0"));
                                #endregion

                                #region Month 3
                                GooglePlus_Posts = 0;
                                GooglePlus_Comments = 0;
                                GooglePlus_PlusOnes = 0;
                                GooglePlus_ReShares = 0;

                                googlePlusPostData = ListGooglePlusPostData.Where(n => n.MonthID == 3).FirstOrDefault();

                                if (googlePlusPostData != null)
                                {
                                    GooglePlus_Posts = googlePlusPostData.Posts;
                                    GooglePlus_Comments = googlePlusPostData.Comments;
                                    GooglePlus_PlusOnes = googlePlusPostData.PlusOnes;
                                    GooglePlus_ReShares = googlePlusPostData.ReShares;
                                }

                                template = template.Replace("[$Google+Posts_Month3$]", GooglePlus_Posts.ToString("#,##0"))
                                                     .Replace("[$Google+Comments_Month3$]", GooglePlus_Comments.ToString("#,##0"))
                                                     .Replace("[$Google+PlusOnes_Month3$]", GooglePlus_PlusOnes.ToString("#,##0"))
                                                     .Replace("[$Google+ReShares_Month3$]", GooglePlus_ReShares.ToString("#,##0"));
                                #endregion

                                #region Month 2
                                GooglePlus_Posts = 0;
                                GooglePlus_Comments = 0;
                                GooglePlus_PlusOnes = 0;
                                GooglePlus_ReShares = 0;

                                googlePlusPostData = ListGooglePlusPostData.Where(n => n.MonthID == 2).FirstOrDefault();

                                if (googlePlusPostData != null)
                                {
                                    GooglePlus_Posts = googlePlusPostData.Posts;
                                    GooglePlus_Comments = googlePlusPostData.Comments;
                                    GooglePlus_PlusOnes = googlePlusPostData.PlusOnes;
                                    GooglePlus_ReShares = googlePlusPostData.ReShares;
                                }

                                template = template.Replace("[$Google+Posts_Month2$]", GooglePlus_Posts.ToString("#,##0"))
                                                     .Replace("[$Google+Comments_Month2$]", GooglePlus_Comments.ToString("#,##0"))
                                                     .Replace("[$Google+PlusOnes_Month2$]", GooglePlus_PlusOnes.ToString("#,##0"))
                                                     .Replace("[$Google+ReShares_Month2$]", GooglePlus_ReShares.ToString("#,##0"));
                                #endregion

                                #region Month 1
                                GooglePlus_Posts = 0;
                                GooglePlus_Comments = 0;
                                GooglePlus_PlusOnes = 0;
                                GooglePlus_ReShares = 0;

                                googlePlusPostData = ListGooglePlusPostData.Where(n => n.MonthID == 1).FirstOrDefault();

                                if (googlePlusPostData != null)
                                {
                                    GooglePlus_Posts = googlePlusPostData.Posts;
                                    GooglePlus_Comments = googlePlusPostData.Comments;
                                    GooglePlus_PlusOnes = googlePlusPostData.PlusOnes;
                                    GooglePlus_ReShares = googlePlusPostData.ReShares;
                                }

                                template = template.Replace("[$Google+Posts_Month1$]", GooglePlus_Posts.ToString("#,##0"))
                                                     .Replace("[$Google+Comments_Month1$]", GooglePlus_Comments.ToString("#,##0"))
                                                     .Replace("[$Google+PlusOnes_Month1$]", GooglePlus_PlusOnes.ToString("#,##0"))
                                                     .Replace("[$Google+ReShares_Month1$]", GooglePlus_ReShares.ToString("#,##0"));
                                #endregion

                            }
                            else
                            {
                                template = template.Replace("[$Google+Posts_Month6$]", "0")
                                                    .Replace("[$Google+Comments_Month6$]", "0")
                                                    .Replace("[$Google+PlusOnes_Month6$]", "0")
                                                    .Replace("[$Google+ReShares_Month6$]", "0");

                                template = template.Replace("[$Google+Posts_Month5$]", "0")
                                                    .Replace("[$Google+Comments_Month5$]", "0")
                                                    .Replace("[$Google+PlusOnes_Month5$]", "0")
                                                    .Replace("[$Google+ReShares_Month5$]", "0");

                                template = template.Replace("[$Google+Posts_Month4$]", "0")
                                                    .Replace("[$Google+Comments_Month4$]", "0")
                                                    .Replace("[$Google+PlusOnes_Month4$]", "0")
                                                    .Replace("[$Google+ReShares_Month4$]", "0");

                                template = template.Replace("[$Google+Posts_Month3$]", "0")
                                                    .Replace("[$Google+Comments_Month3$]", "0")
                                                    .Replace("[$Google+PlusOnes_Month3$]", "0")
                                                    .Replace("[$Google+ReShares_Month3$]", "0");

                                template = template.Replace("[$Google+Posts_Month2$]", "0")
                                                    .Replace("[$Google+Comments_Month2$]", "0")
                                                    .Replace("[$Google+PlusOnes_Month2$]", "0")
                                                    .Replace("[$Google+ReShares_Month2$]", "0");

                                template = template.Replace("[$Google+Posts_Month1$]", "0")
                                                    .Replace("[$Google+Comments_Month1$]", "0")
                                                    .Replace("[$Google+PlusOnes_Month1$]", "0")
                                                    .Replace("[$Google+ReShares_Month1$]", "0");

                            }
                            #endregion
                        }
                    }
                }
            }
            catch (Exception ex)
            {
            }

            return template;

        }

        public string GetReputationAndSocialReportHTML(long AccountID, long UserID, bool printHtml)
        {
            string EmailBody = string.Empty;

            if (UserID > 0)
            {
                using (MainEntities db = ContextFactory.Main)
                {
                    if (db.Connection.State != System.Data.ConnectionState.Open)
                        db.Connection.Open();

                    //ReportTemplate template = (from rt in db.ReportTemplates where rt.ID == 4 select rt).FirstOrDefault();
                    //var Notificationtemplate = (from nt in db.NotificationTemplates where nt.ID == 58 select nt).SingleOrDefault();


                    UserInfoDTO UserInfo = new UserInfoDTO(1, null); //system
                    UserDTO userDTO = ProviderFactory.Security.GetUserByID(UserID, UserInfoDTO.System);
                                        
                    var Notificationtemplate = new NotificationTemplate();
                    string ResellerLogo = string.Empty;

                    if (!printHtml)
                    {
                        Notificationtemplate = (from nt in db.NotificationTemplates where nt.ID == 58 select nt).SingleOrDefault();
                    }
                    else
                    {
                        Notificationtemplate = (from nt in db.NotificationTemplates where nt.ID == 63 select nt).SingleOrDefault();
                    }

                    //Get ResellerId by AccountID
                    long resellerID = ProviderFactory.Security.GetResellerByAccountID(UserInfo, AccountID, TimeZoneInfo.Utc).ID.Value;
                    if (resellerID != null && resellerID != 0)
                        ResellerLogo = (from r in db.Resellers where r.ID == resellerID select r.Logo).FirstOrDefault();

                    if (string.IsNullOrEmpty(ResellerLogo))
                    {
                        ResellerLogo = (from r in db.Resellers where r.ID == 5 select r.Logo).FirstOrDefault(); //ResellerID (5) Default "SocialDealer"
                    }

                    ReputationAndSocialReportDTO TemplateDTO = JsonConvert.DeserializeObject<ReputationAndSocialReportDTO>(Notificationtemplate.Body);                    

                    List<long> accountIDs = new List<long>();

                    //long ReportSubscriptionID = (from rs in db.ReportSubscriptions where rs.UserID == UserID && rs.ReportTemplateID == 4 select rs.ID).FirstOrDefault();
                    accountIDs = ProviderFactory.Security.AccountsWithPermission(UserID, PermissionTypeEnum.reviews_view);
                    
                    if (accountIDs.Count > 500)
                        accountIDs = accountIDs.Take(500).ToList();

                    if (accountIDs != null && accountIDs.Count > 0)
                    {                        

                        DateTime LastMonthLastDate = DateTime.Today.AddDays(0 - DateTime.Today.Day);
                        DateTime LastMonthFirstDate = LastMonthLastDate.AddDays(1 - LastMonthLastDate.Day);

                        string startDate = LastMonthFirstDate.ToShortDateString();
                        string endDate = LastMonthLastDate.ToShortDateString();

                        using (DbCommand cmd3 = db.CreateStoreCommand(
                            "report.spGetReputationSocialReport2",
                            System.Data.CommandType.StoredProcedure,
                                new SqlParameter("@AccountIDs", string.Join("|", accountIDs.ToArray())),
                                new SqlParameter("@startdate", startDate),
                                new SqlParameter("@enddate", endDate)
                            ))
                        {
                            using (DbDataReader reader = cmd3.ExecuteReader())
                            {
                                List<ReputationAndSocialReportDetail> details = db.Translate<ReputationAndSocialReportDetail>(reader).ToList();

                                reader.NextResult();

                                List<ReputationAndSocialReportSummary> summarys = db.Translate<ReputationAndSocialReportSummary>(reader).ToList();


                                if (details.Any() && summarys.Any())
                                {
                                    #region HTML
                                    EmailBody = TemplateDTO.Body;

                                    #region Reseller Logo for Print
                                    if (printHtml)
                                    {
                                        EmailBody = EmailBody.Replace("[$ResellerLogo$]", ResellerLogo);
                                    }

                                    #endregion

                                    EmailBody = EmailBody.Replace("[$Month$]", Convert.ToDateTime(startDate).ToString("MMMM", CultureInfo.CreateSpecificCulture("en-US")))
                                                         .Replace("[$Year$]", Convert.ToDateTime(startDate).Year.ToString());

                                    string content = string.Empty;
                                    int iCounter = 1;
                                    foreach (var detail in details)
                                    {
                                        string TemplateRepeatingBlock = TemplateDTO.TemplateRepeatingBlock;

                                        TemplateRepeatingBlock = TemplateRepeatingBlock.Replace("[$ID$]", iCounter.ToString("#,##0"))
                                                                                        .Replace("[$AccountName$]", detail.AccountName)
                                                                                        .Replace("[$TotalReviews$]", detail.TotalReviews.GetValueOrDefault().ToString("#,##0"))
                                                                                        .Replace("[$PositiveReviews$]", detail.TotalReviewPositive.GetValueOrDefault().ToString("#,##0"))
                                                                                        .Replace("[$NegativeReviews$]", detail.TotalReviewNegative.GetValueOrDefault().ToString("#,##0"))
                                                                                        .Replace("[$Rating$]", Math.Round(detail.Rating.GetValueOrDefault(), 1).ToString())
                                                                                        .Replace("[$NewReview$]", detail.TotalReviewPeriod.GetValueOrDefault().ToString("#,##0"))
                                                                                        .Replace("[$Fans$]", detail.Fans.GetValueOrDefault().ToString("#,##0"))
                                                                                        .Replace("[$NewFans$]", detail.NewFans.GetValueOrDefault().ToString("#,##0"))
                                                                                        .Replace("[$EngagedUsers$]", detail.EngagedUsers.GetValueOrDefault().ToString("#,##0"))
                                                                                        .Replace("[$FanImpressions$]", detail.FanImpressions.GetValueOrDefault().ToString("#,##0"))
                                                                                        .Replace("[$TotalImpressions$]", detail.TotalImpressions.GetValueOrDefault().ToString("#,##0"))
                                                                                        .Replace("[$PaidPostImpressions$]", detail.PaidPostImpressions.GetValueOrDefault().ToString("#,##0"))
                                                                                        .Replace("[$Followers$]", detail.Followers.GetValueOrDefault().ToString("#,##0"))
                                                                                        .Replace("[$Subscribers$]", detail.Subscribers.GetValueOrDefault().ToString("#,##0"))
                                                                                        .Replace("[$PlusOnes$]", detail.PlusOnes.GetValueOrDefault().ToString("#,##0"));

                                        content += TemplateRepeatingBlock;

                                        iCounter++;
                                    }

                                    string SummaryContent = string.Empty;
                                    iCounter = 1;
                                    foreach (var summary in summarys)
                                    {
                                        string TemplateRepeatingBlockSummary = TemplateDTO.TemplateRepeatingBlockSummary;
                                        int TotalReviews = summary.TotalReviews.GetValueOrDefault();

                                        TemplateRepeatingBlockSummary = TemplateRepeatingBlockSummary.Replace("[$ID$]", iCounter.ToString("#,##0"))
                                                                                                        .Replace("[$Name$]", summary.AccountName)
                                                                                                        .Replace("[$Total Reviews$]", summary.TotalReviews.GetValueOrDefault().ToString("#,##0"));

                                        #region YTDGrowth

                                        string YTDGrowthColor = "black";
                                        int YTDGrowth = 0;
                                        string YTDGrowth_str = "0";
                                        YTDGrowth = summary.TotalReviews.GetValueOrDefault() - summary.TotalReviewsLastYearLastMonth.GetValueOrDefault();

                                        if (YTDGrowth > 0)
                                        {
                                            YTDGrowthColor = "black";
                                            YTDGrowth_str = YTDGrowth.ToString("#,##0");
                                        }
                                        else if (YTDGrowth < 0)
                                        {
                                            YTDGrowthColor = "red";
                                            YTDGrowth_str = "(" + YTDGrowth.ToString("#,##0") + ")";
                                        }
                                        if (YTDGrowth == 0)
                                        {
                                            YTDGrowthColor = "black";
                                            YTDGrowth_str = YTDGrowth.ToString("#,##0");
                                        }
                                        TemplateRepeatingBlockSummary = TemplateRepeatingBlockSummary.Replace("[$YTD Growth Color$]", YTDGrowthColor)
                                                                                                       .Replace("[$YTD Growth$]", YTDGrowth_str.Replace("-", ""));

                                        #endregion

                                        #region YTD % Growth

                                        decimal YTDPercentGrowth = 0;
                                        if (TotalReviews > 0)
                                        {
                                            YTDPercentGrowth = Convert.ToDecimal(Convert.ToDecimal(YTDGrowth) / Convert.ToDecimal(TotalReviews)) * Convert.ToDecimal(100);
                                            YTDPercentGrowth = Math.Round(YTDPercentGrowth, 1);
                                        }
                                        else
                                        {
                                            YTDPercentGrowth = 0;
                                        }
                                        TemplateRepeatingBlockSummary = TemplateRepeatingBlockSummary.Replace("[$YTD % Growth$]", YTDPercentGrowth.ToString() + "%");
                                        #endregion

                                        #region Positive

                                        int TotalPosNeg = summary.TotalReviewPositive.GetValueOrDefault() + summary.TotalReviewNegative.GetValueOrDefault();
                                        int TotalPos = summary.TotalReviewPositive.GetValueOrDefault();

                                        decimal PercentPositive = 0;
                                        string PercentPositiveColor = "";
                                        if (TotalPosNeg > 0)
                                        {
                                            PercentPositive = Convert.ToDecimal(Convert.ToDecimal(TotalPos) / Convert.ToDecimal(TotalPosNeg));
                                            //PercentPositive = Math.Round(PercentPositive, 2);
                                        }
                                        else
                                        {
                                            PercentPositive = 0;
                                        }

                                        if (PercentPositive >= Convert.ToDecimal(0.61) && PercentPositive <= Convert.ToDecimal(0.79))
                                        {
                                            PercentPositiveColor = "#ffeb9c";
                                        }
                                        else if (PercentPositive >= Convert.ToDecimal(0) && PercentPositive <= Convert.ToDecimal(0.60))
                                        {
                                            PercentPositiveColor = "#ffc7ce";
                                        }
                                        else if (PercentPositive >= Convert.ToDecimal(0.80) && PercentPositive <= Convert.ToDecimal(1))
                                        {
                                            PercentPositiveColor = "#c6efce";
                                        }

                                        PercentPositive = Math.Round(PercentPositive * Convert.ToDecimal(100), 1);

                                        TemplateRepeatingBlockSummary = TemplateRepeatingBlockSummary.Replace("[$% Positive$]", PercentPositive.ToString() + "%")
                                                                                                        .Replace("[$% Positive Color$]", PercentPositiveColor);

                                        #endregion

                                        TemplateRepeatingBlockSummary = TemplateRepeatingBlockSummary.Replace("[$Rating$]", Math.Round(summary.Rating.GetValueOrDefault(), 1).ToString());

                                        #region Rating Chg

                                        decimal Rating = summary.Rating.GetValueOrDefault();
                                        decimal RatingLastYearLastMonth = summary.RatingLastYearLastMonth.GetValueOrDefault();
                                        decimal RatingChg = 0;
                                        string RatingChgColor = "";

                                        if (RatingLastYearLastMonth > 0)
                                        {
                                            RatingChg = Convert.ToDecimal((Convert.ToDecimal(Convert.ToDecimal(Rating) - Convert.ToDecimal(RatingLastYearLastMonth)) / Convert.ToDecimal(RatingLastYearLastMonth)));
                                            //RatingChg = Math.Round(RatingChg, 2);
                                        }
                                        else
                                        {
                                            RatingChg = 0;
                                        }

                                        if (RatingChg <= Convert.ToDecimal(0) && RatingChg >= Convert.ToDecimal(-5))
                                        {
                                            RatingChgColor = "red";
                                        }
                                        else
                                        {
                                            RatingChgColor = "black";
                                        }

                                        RatingChg = Math.Round(RatingChg * Convert.ToDecimal(100), 1);

                                        TemplateRepeatingBlockSummary = TemplateRepeatingBlockSummary.Replace("[$Rating Chg$]", RatingChg.ToString() + "%")
                                                                                                        .Replace("[$Rating Chg Color$]", RatingChgColor);

                                        #endregion

                                        int TotalFans = summary.TotalFans.GetValueOrDefault();
                                        TemplateRepeatingBlockSummary = TemplateRepeatingBlockSummary.Replace("[$Total Fans$]", TotalFans.ToString("#,##0"));

                                        int YTDFanGrowth = TotalFans - summary.TotalFansLastYear.GetValueOrDefault();
                                        TemplateRepeatingBlockSummary = TemplateRepeatingBlockSummary.Replace("[$YTD Fan Growth$]", YTDFanGrowth.ToString("#,##0"));

                                        #region YTD % Fan Growth

                                        decimal YTDPercentFanGrowth = 0;
                                        if (TotalFans > 0)
                                        {
                                            YTDPercentFanGrowth = Convert.ToDecimal(Convert.ToDecimal(YTDFanGrowth) / Convert.ToDecimal(TotalFans)) * Convert.ToDecimal(100);
                                        }
                                        else
                                        {
                                            YTDPercentFanGrowth = 0;
                                        }
                                        TemplateRepeatingBlockSummary = TemplateRepeatingBlockSummary.Replace("[$YTD % Growth$]", YTDPercentFanGrowth.ToString() + "%");

                                        #endregion

                                        TemplateRepeatingBlockSummary = TemplateRepeatingBlockSummary.Replace("[$YTD Engaged Users$]", summary.TotalEngagedUsers.GetValueOrDefault().ToString("#,##0"))
                                                                                                        .Replace("[$YTD Fan Impressions$]", summary.TotalFanImpressions.GetValueOrDefault().ToString("#,##0"))
                                                                                                        .Replace("[$YTD Total Impressions$]", summary.TotalTotalImpressions.GetValueOrDefault().ToString("#,##0"))
                                                                                                        .Replace("[$YTD Paid Post Impressions$]", summary.TotalPaidPostImpressions.GetValueOrDefault().ToString("#,##0"));

                                        int TotalFollowers = summary.TotalFollowers.GetValueOrDefault();
                                        TemplateRepeatingBlockSummary = TemplateRepeatingBlockSummary.Replace("[$Total Followers$]", TotalFollowers.ToString("#,##0"));

                                        int YTDFollowersGrowth = TotalFollowers - summary.TotalFollowersLastYear.GetValueOrDefault();
                                        TemplateRepeatingBlockSummary = TemplateRepeatingBlockSummary.Replace("[$YTD Followers Growth$]", YTDFollowersGrowth.ToString("#,##0"));

                                        #region YTD % Followers Growth

                                        decimal YTDPercentFollowersGrowth = 0;
                                        if (TotalFollowers > 0)
                                        {
                                            YTDPercentFollowersGrowth = Convert.ToDecimal(Convert.ToDecimal(YTDFollowersGrowth) / Convert.ToDecimal(TotalFollowers)) * Convert.ToDecimal(100);
                                        }
                                        else
                                        {
                                            YTDPercentFollowersGrowth = 0;
                                        }
                                        TemplateRepeatingBlockSummary = TemplateRepeatingBlockSummary.Replace("[$YTD Growth % Twitter$]", YTDPercentFollowersGrowth.ToString() + "%");

                                        #endregion

                                        int TotalSubscribers = summary.TotalSubscribers.GetValueOrDefault();
                                        TemplateRepeatingBlockSummary = TemplateRepeatingBlockSummary.Replace("[$Total Subscribers$]", TotalSubscribers.ToString("#,##0"));

                                        int YTDSubscribersGrowth = TotalSubscribers - summary.TotalSubscribersLastYear.GetValueOrDefault();
                                        TemplateRepeatingBlockSummary = TemplateRepeatingBlockSummary.Replace("[$YTD Subscribers Growth$]", YTDSubscribersGrowth.ToString("#,##0"));

                                        int TotalPlusOnes = summary.TotalPlusOnes.GetValueOrDefault();
                                        TemplateRepeatingBlockSummary = TemplateRepeatingBlockSummary.Replace("[$Total PlusOne's$]", TotalPlusOnes.ToString("#,##0"));

                                        int YTDPlusOnesGrowth = TotalPlusOnes - summary.TotalPlusOnesLastYear.GetValueOrDefault();
                                        TemplateRepeatingBlockSummary = TemplateRepeatingBlockSummary.Replace("[$YTD PlusOne's Growth$]", YTDPlusOnesGrowth.ToString("#,##0"));

                                        SummaryContent += TemplateRepeatingBlockSummary;

                                        iCounter++;
                                    }

                                    EmailBody = EmailBody.Replace("[$ContentTemplateRepeatingBlock$]", content);
                                    EmailBody = EmailBody.Replace("[$SummaryTemplateRepeatingBlock$]", SummaryContent);

                                    #endregion
                                }
                            }

                        }
                    }

                }
            }

            return EmailBody;
        }
        
		public List<EnterpriseReputationSocialReportDTO> GetReputationAndSocialReport(UserInfoDTO userInfo, List<long> accountIDs, long? virtualGroupID)
		{
			List<EnterpriseReputationSocialReportDTO> reportDTO = new List<EnterpriseReputationSocialReportDTO>();

            AccountListBuilder builder = AccountListBuilder
                .New((SecurityProvider)ProviderFactory.Security)
                .AddAccountIDs(accountIDs)
            ;
            if (virtualGroupID.HasValue)
            {
                builder
                    .AddAccountsFromVirtualGroup(virtualGroupID.Value)
                    .FilterToAccountsWithPermission(userInfo, PermissionTypeEnum.accounts_view)
                ;
            }
            accountIDs = builder.GetIDs(SecurityProvider.PLATFORM_MAX_ACCOUNT_ID_LIST);

			using (MainEntities db = ContextFactory.Main)
			{
				if (db.Connection.State != System.Data.ConnectionState.Open)
					db.Connection.Open();

				if (accountIDs != null && accountIDs.Count > 0)
				{
					DateTime LastMonthLastDate = DateTime.Today.AddDays(0 - DateTime.Today.Day);
					DateTime LastMonthFirstDate = LastMonthLastDate.AddDays(1 - LastMonthLastDate.Day);

					string startDate = LastMonthFirstDate.ToShortDateString();
					string endDate = LastMonthLastDate.ToShortDateString();

					using (DbCommand cmd3 = db.CreateStoreCommand(
                        "report.spGetReputationSocialReport2",
						System.Data.CommandType.StoredProcedure,
						new SqlParameter("@AccountIDs", string.Join("|", accountIDs.ToArray())),
						new SqlParameter("@startdate", startDate),
						new SqlParameter("@enddate", endDate)
						))
					{
						using (DbDataReader reader = cmd3.ExecuteReader())
						{
							List<ReputationAndSocialReportDetail> details = db.Translate<ReputationAndSocialReportDetail>(reader).ToList();

							reader.NextResult();

							List<ReputationAndSocialReportSummary> summarys = db.Translate<ReputationAndSocialReportSummary>(reader).ToList();


							foreach (ReputationAndSocialReportSummary summary in summarys)
							{
								EnterpriseReputationSocialReportDTO dto = new EnterpriseReputationSocialReportDTO();

								dto.AccountID = summary.AccountID;
								dto.AccountName = summary.AccountName;
								dto.TotalReviews = summary.TotalReviews.GetValueOrDefault();
								dto.YTDGrowth = summary.TotalReviews.GetValueOrDefault() - summary.TotalReviewsLastYearLastMonth.GetValueOrDefault();
								
								dto.YTDPercentGrowth = 0;
								if (dto.TotalReviews > 0)
								{
									dto.YTDPercentGrowth = Convert.ToDecimal(Convert.ToDecimal(dto.YTDGrowth) / Convert.ToDecimal(dto.TotalReviews)) * 100M;
									dto.YTDPercentGrowth = Math.Round(dto.YTDPercentGrowth, 1);
								}

								dto.PercentPositive = 0;
								int TotalPosNeg = summary.TotalReviewPositive.GetValueOrDefault() + summary.TotalReviewNegative.GetValueOrDefault();
								int TotalPos = summary.TotalReviewPositive.GetValueOrDefault();

								//dto.PercentPostitiveColor = "";
								if (TotalPosNeg > 0)
									dto.PercentPositive = Convert.ToDecimal(Convert.ToDecimal(TotalPos) / Convert.ToDecimal(TotalPosNeg));

								dto.PercentPositive = Math.Round(dto.PercentPositive * 100M, 1);
								dto.Rating = Math.Round(summary.Rating.GetValueOrDefault(), 1);

								dto.RatingPercentChange = 0;
								decimal RatingLastYearLastMonth = summary.RatingLastYearLastMonth.GetValueOrDefault();
								if (RatingLastYearLastMonth > 0)
									dto.RatingPercentChange = Convert.ToDecimal((Convert.ToDecimal(Convert.ToDecimal(summary.Rating.GetValueOrDefault()) - Convert.ToDecimal(RatingLastYearLastMonth)) / Convert.ToDecimal(RatingLastYearLastMonth)));
								dto.RatingPercentChange = Math.Round(dto.RatingPercentChange * 100M, 1);

								dto.TotalFans = summary.TotalFans.GetValueOrDefault();
								dto.YTDFanGrowth = dto.TotalFans - summary.TotalFansLastYear.GetValueOrDefault();

								dto.YTDPercentFanGrowth = 0;
								if (dto.TotalFans > 0)
									dto.YTDPercentFanGrowth = Convert.ToDecimal(Convert.ToDecimal(dto.YTDFanGrowth) / Convert.ToDecimal(dto.TotalFans)) * 100M;

								dto.YTDEngagedUsers = summary.TotalEngagedUsers.GetValueOrDefault();
								dto.YTDFanImpressions = summary.TotalFanImpressions.GetValueOrDefault();
								dto.YTDTotalImpressions = summary.TotalTotalImpressions.GetValueOrDefault();
								dto.YTDPaidPostImpressions = summary.TotalPaidPostImpressions.GetValueOrDefault();

								dto.TotalFollowers = summary.TotalFollowers.GetValueOrDefault();
								dto.YTDFollowersGrowth = dto.TotalFollowers - summary.TotalFollowersLastYear.GetValueOrDefault();

								dto.YTDTwitterPercentGrowth = 0;
								if (dto.TotalFollowers > 0)
									dto.YTDTwitterPercentGrowth = Convert.ToDecimal(Convert.ToDecimal(dto.YTDFollowersGrowth) / Convert.ToDecimal(dto.TotalFollowers)) * 100M;

								dto.TotalSubscribers = summary.TotalSubscribers.GetValueOrDefault();
								dto.YTDSubscribersGrowth = dto.TotalSubscribers - summary.TotalSubscribersLastYear.GetValueOrDefault();
								dto.TotalPlusOnes = summary.TotalPlusOnes.GetValueOrDefault();
								dto.YTDPlusOnesGrowth = dto.TotalPlusOnes - summary.TotalPlusOnesLastYear.GetValueOrDefault();

								reportDTO.Add(dto);
							}

						}
					}
				}
			}

			return reportDTO;
		}

        public List<EnterpriseReputationSocialDetailReportDTO> GetReputationAndSocialDetailReport(UserInfoDTO userInfo, List<long> accountIDs, long? virtualGroupID)
	    {

            AccountListBuilder builder = AccountListBuilder
                .New((SecurityProvider)ProviderFactory.Security)
                .AddAccountIDs(accountIDs)
            ;
            if (virtualGroupID.HasValue)
            {
                builder
                    .AddAccountsFromVirtualGroup(virtualGroupID.Value)
                    .FilterToAccountsWithPermission(userInfo, PermissionTypeEnum.accounts_view)
                ;
            }
            accountIDs = builder.GetIDs(SecurityProvider.PLATFORM_MAX_ACCOUNT_ID_LIST);

			List<EnterpriseReputationSocialDetailReportDTO> reportDTO = new List<EnterpriseReputationSocialDetailReportDTO>();

			using (MainEntities db = ContextFactory.Main)
			{
				if (db.Connection.State != System.Data.ConnectionState.Open)
					db.Connection.Open();

				if (accountIDs != null && accountIDs.Count > 0)
				{
					DateTime LastMonthLastDate = DateTime.Today.AddDays(0 - DateTime.Today.Day);
					DateTime LastMonthFirstDate = LastMonthLastDate.AddDays(1 - LastMonthLastDate.Day);

					string startDate = LastMonthFirstDate.ToShortDateString();
					string endDate = LastMonthLastDate.ToShortDateString();

					using (DbCommand cmd3 = db.CreateStoreCommand(
                        "report.spGetReputationSocialReport2",
						System.Data.CommandType.StoredProcedure,
						new SqlParameter("@AccountIDs", string.Join("|", accountIDs.ToArray())),
						new SqlParameter("@startdate", startDate),
						new SqlParameter("@enddate", endDate)
						))
					{
						using (DbDataReader reader = cmd3.ExecuteReader())
						{
							List<ReputationAndSocialReportDetail> details = db.Translate<ReputationAndSocialReportDetail>(reader).ToList();

							foreach (ReputationAndSocialReportDetail detail in details)
							{
								EnterpriseReputationSocialDetailReportDTO dto = new EnterpriseReputationSocialDetailReportDTO();

								dto.AccountID = detail.AccountID;
								dto.AccountName = detail.AccountName;
								dto.TotalReviews = detail.TotalReviews.GetValueOrDefault();
								dto.PositiveReviews = detail.TotalReviewPositive.GetValueOrDefault();
								dto.NegativeReviews = detail.TotalReviewNegative.GetValueOrDefault();
								dto.Rating = detail.Rating.GetValueOrDefault();
								dto.NewReview = detail.TotalReviews.GetValueOrDefault();
								dto.Fans = detail.Fans.GetValueOrDefault();
								dto.NewFans = detail.NewFans.GetValueOrDefault();
								dto.EngagedUsers = detail.EngagedUsers.GetValueOrDefault();
								dto.FanImpressions = detail.FanImpressions.GetValueOrDefault();
								dto.TotalImpressions = detail.TotalImpressions.GetValueOrDefault();
								dto.PaidPostImpressions = detail.PaidPostImpressions.GetValueOrDefault();
								dto.Followers = detail.Followers.GetValueOrDefault();
								dto.SubScribers = detail.Subscribers.GetValueOrDefault();
								dto.PlusOnes = detail.PlusOnes.GetValueOrDefault();

								reportDTO.Add(dto);
							}

						}
					}
				}
			}

			return reportDTO;
	    }

        public ReportTemplateandEmailReportDTO GetReportTemplateAndEmailReport(long UserID)
        {
            ReportTemplateandEmailReportDTO result = new ReportTemplateandEmailReportDTO();

            if (UserID != 0)
            {
                using (MainEntities db = ContextFactory.Main)
                {
                    if (db.Connection.State != System.Data.ConnectionState.Open)
                        db.Connection.Open();

                    using (DbCommand cmd3 = db.CreateStoreCommand(
                        "spGetEmailReportTemplates",
                        System.Data.CommandType.StoredProcedure,
                            new SqlParameter("@UserID", UserID)
                        ))
                    {
                        using (DbDataReader reader = cmd3.ExecuteReader())
                        {
                            result.emailReportTemplates = db.Translate<EmailReportTemplateDTO>(reader).ToList();

                            reader.NextResult();

                            result.emailedReportLogs = db.Translate<EmailedReportLogDTO>(reader).ToList();
                            
                        }
                    }
                }
            }

            return result;
        }

        public bool SetReportSubscriptions(long UserID, UserInfoDTO userInfo, long ReportTemplateID, string ScheduleSpec, long ReportSubscriptionID, bool isDisable)
        {
            bool Success = false;

            using (MainEntities db = ContextFactory.Main)
            {                
                if (isDisable && ReportSubscriptionID > 0)
                {
                    ReportSubscription reportSubscription = db.ReportSubscriptions.Where(r => r.ID == ReportSubscriptionID).FirstOrDefault();

                    if (reportSubscription != null)
                    {
                        reportSubscription.isActive = false;

                        db.SaveChanges();

                        Auditor
                      .New(AuditLevelEnum.Information, AuditTypeEnum.UserActivity, userInfo, "")
                      .SetAuditDataObject(
                          AuditUserActivity
                          .New(userInfo, AuditUserActivityTypeEnum.DisableReportSubscription)
                              .SetDescription("Disable Report Subscription ReportSubscriptionID : " + ReportSubscriptionID)
                      )
                      .Save(ProviderFactory.Logging);
                    }
                    Success = true;

                   
                }
                else if (UserID > 0 && ReportTemplateID != 0 && !string.IsNullOrEmpty(ScheduleSpec))
                {
                    ReportSubscription reportSubscription = new ReportSubscription();

                    reportSubscription.UserID = UserID;
                    reportSubscription.ReportTemplateID = ReportTemplateID;
                    reportSubscription.ScheduleSpec = ScheduleSpec;
                    reportSubscription.DateCreated = DateTime.UtcNow;

                    db.SaveChanges();

                    Success = true;

                    Auditor
                       .New(AuditLevelEnum.Information, AuditTypeEnum.UserActivity, userInfo, "")
                       .SetAuditDataObject(
                           AuditUserActivity
                           .New(userInfo, AuditUserActivityTypeEnum.AddedReportSubscription)
                               .SetDescription("Added Report Subscription, UserID : "+ UserID + " , ReportTemplateID : " + ReportTemplateID)
                       )
                       .Save(ProviderFactory.Logging);
                }
            }

            return Success;
        }

        public string GetReportLogByID(long ReportLogID)
        {
            string result = string.Empty;

            using (MainEntities db = ContextFactory.Main)
            {
                if (db.Connection.State != System.Data.ConnectionState.Open)
                    db.Connection.Open();

                using (DbCommand cmd3 = db.CreateStoreCommand(
                    "spGetReportLogByID",
                    System.Data.CommandType.StoredProcedure,
                        new SqlParameter("@ReportLogID", ReportLogID)
                    ))
                {
                    using (DbDataReader reader = cmd3.ExecuteReader())
                    {
						EmailedReportLogDetailsDTO dto = db.Translate<EmailedReportLogDetailsDTO>(reader).FirstOrDefault();
	                    if (dto != null)
		                    result = dto.Body;
                    }
                }
            }

            return result;
        }

        public void SendTokenValidationByUser()
        {
            try
            {
                using (MainEntities db = ContextFactory.Main)
                {
                    if (db.Connection.State != System.Data.ConnectionState.Open)
                        db.Connection.Open();

                    using (DbCommand cmd3 = db.CreateStoreCommand(
                        "spGetInvalidTokenAndAccountInfo",
                        System.Data.CommandType.StoredProcedure))
                    {
                        using (DbDataReader reader = cmd3.ExecuteReader())
                        {
                            List<InvalidToken> InvalidTokens = db.Translate<InvalidToken>(reader).ToList();

                            if (InvalidTokens.Any())
                            {
                                //Get the Template by ResellerID and NotificationMessageTypeID
                                NotificationTemplate template = (from t in db.NotificationTemplates
                                                                 where t.ID == 72
                                                                 select t).FirstOrDefault();


                                ClientFaceingTokenValidationDTO TemplateDTO = JsonConvert.DeserializeObject<ClientFaceingTokenValidationDTO>(template.Body);
                                if (template != null)
                                {
                                    foreach (long userID in (from ts in InvalidTokens select ts.UserID).Distinct())
                                    {
                                        try
                                        {
                                            #region HTML

                                            List<InvalidToken> listToSend = (
                                                        from ts4 in InvalidTokens
                                                        where ts4.UserID == userID
                                                        select ts4
                                                    ).ToList();

                                            if (listToSend.Any())
                                            {
                                                string EmailBody = TemplateDTO.Body48Hours;
                                                string Subject = TemplateDTO.Subject48Hours;

                                                
                                                EmailBody = EmailBody.Replace("[$UserName$]", string.Format("{0} {1}", listToSend.FirstOrDefault().FirstName, listToSend.FirstOrDefault().LastName));
                                                string AccountList = string.Empty;

                                                foreach (var ToSend in listToSend)
	                                            {
                                                    //int dd = ToSend.Hours / 24;

                                                    //if (dd == 2 || dd == 4)
                                                    {
                                                        Auditor
                                                          .New(AuditLevelEnum.Information, AuditTypeEnum.AccountActivity, UserInfoDTO.System, "")
                                                          .SetAuditDataObject(
                                                              AuditAccountActivity
                                                                  .New(ToSend.AccountID, AccountActivityTypeEnum.InvalidTokenEmailSent)
                                                                  .SetDescription("Invalid Token Email Sent for {0}, CredentialID [{1}].", ToSend.SocialNetworkName, ToSend.CredentialID)
                                                          )
                                                        .Save(ProviderFactory.Logging);

                                                        string ReConnectURL = string.Empty;

                                                        SocialProductConfigAccessTokenDTO tokenRequest = new SocialProductConfigAccessTokenDTO()
                                                        {
                                                            UserID = listToSend.FirstOrDefault().UserID,
                                                            AccountID = ToSend.AccountID,

                                                            RemainingUses = 100,
                                                            TimeTillExpiration = new TimeSpan(1, 0, 0, 0)
                                                        };
                                                        AccessTokenBaseDTO accessToken = ProviderFactory.Security.CreateAccessToken(tokenRequest);


                                                        string callback = string.Format("{0}/access", Settings.GetSetting("site.baseurl", "socialintegration.com"));
                                                        ReConnectURL = string.Format("{0}/{1}", callback, accessToken.Token.ToString());


                                                        AccountList = AccountList + @"<li>" + ToSend.SocialNetworkName + " : <a href=\"" + ToSend.URI + "\">" + ToSend.ScreenName + "</a>&nbsp;&nbsp;&nbsp;&nbsp;<a href=\"" + ReConnectURL + "\">ReConnect</a></li></li>";
                                                    }
                                                }

                                                EmailBody = EmailBody.Replace("[$AccountList$]", AccountList);

                                                //Insert Into NotificationTarget
                                                NotificationTarget notificationTarget = new NotificationTarget();

                                                notificationTarget.NotificationMethodID = template.NotificationMethodID;
                                                notificationTarget.Address = listToSend.FirstOrDefault().EmailAddress;
                                                notificationTarget.Subject = Subject;
                                                notificationTarget.Body = EmailBody;
                                                notificationTarget.NotificationStatusID = Convert.ToInt64(NotificationStatusesEnum.Pending);

                                                long NotificationTargetID = addNotificationTarget(notificationTarget);

                                                //Send Email
                                                string SendFromEmailAddress = Settings.GetSetting("site.SendFromEmailAddress", "notifications@socialdealer.com");

                                                EmailManager emailManager = new EmailManager();
                                                bool sendResult = emailManager.sendNotification(
                                                    "New Review", notificationTarget.Subject, notificationTarget.Body,
                                                    SendFromEmailAddress, notificationTarget.Address,
                                                    string.Empty, string.Empty)
                                                ;

                                                if (sendResult == true)
                                                {
                                                    updateNotificationTarget(NotificationTargetID, NotificationStatusesEnum.Success);
                                                }
                                                else
                                                {
                                                    updateNotificationTarget(NotificationTargetID, NotificationStatusesEnum.Failure);
                                                }


                                            }
                                            #endregion
                                        }
                                        catch(Exception ex)
                                        {
                                         
                                        }
                                    }
                                }
                            }

                        }
                    }
                }
            }
            catch (Exception ex)
            {
            }
        }

	    #region private helpers

        private void SendDailyEnterpriseReputationSummaryReport(ReportSubscriptionToRun item)
        {

            if (item != null)
            {
                using (MainEntities db = ContextFactory.Main)
                {
                    if (db.Connection.State != System.Data.ConnectionState.Open)
                        db.Connection.Open();

                    ReportTemplate template = (from rt in db.ReportTemplates where rt.ID == item.ReportTemplateID select rt).FirstOrDefault();

                    EnterpriseReputationSummaryReportDTO TemplateDTO = JsonConvert.DeserializeObject<EnterpriseReputationSummaryReportDTO>(template.Body);

                    List<long> accountIDs = new List<long>();

                    if (item.ReportSubscriptionID == 14 || item.ReportSubscriptionID == 15) // for Lexus Syndicatee (Let list of all Acounts that has Lexus Syndicatee Packages Subscriptions)
                    {
                        accountIDs = (from s in db.Subscriptions where s.Reseller_PackageID == 15 || s.Reseller_PackageID == 38 select s.AccountID).Distinct().ToList();
                    }
                    else // Get List of the Accounts form ReportSubscriptionAccounts for ReportSubscriptionID
                    {
                        accountIDs = (from rsa in db.ReportSubscriptionAccounts where rsa.ReportSubscriptionID == item.ReportSubscriptionID select rsa.AccountID).ToList();
                    }

                    // If ReportSubscription Does not have any Account Subscription then take all the Accounts that User has Access with PermissionType = accounts_view 
                    // and FeatureType = ReputationCore
                    if (accountIDs == null || accountIDs.Count == 0) 
                    {
                        accountIDs = ProviderFactory.Security.GetAccountsWithUserPermissionAndFeature(item.UserID, PermissionTypeEnum.accounts_view, FeatureTypeEnum.ReputationCore);
                    }

                    // Accounts Count >= 200 then take top 200 Accounts
                    //if (accountIDs.Count >= 200)
                    //{
                    //    accountIDs = accountIDs.Take(200).ToList();
                    //}

                    using (DbCommand cmd3 = db.CreateStoreCommand(
                        "spReportGetEnterpriseReputationSummary2",
                        System.Data.CommandType.StoredProcedure,
                            new SqlParameter("@AccountIDs", string.Join("|", accountIDs.ToArray()))
                        ))
                    {
                        using (DbDataReader reader = cmd3.ExecuteReader())
                        {
                            ReportGetEnterpriseReputationDailySummary summary = db.Translate<ReportGetEnterpriseReputationDailySummary>(reader).FirstOrDefault();

                            reader.NextResult();

                            List<ReportGetEnterpriseReputationDaily> details = db.Translate<ReportGetEnterpriseReputationDaily>(reader).ToList();

                            if (details.Any())
                            {
                                #region HTML
                                string EmailBody = TemplateDTO.body;

                                EmailBody = EmailBody.Replace("[$Date$]", DateTime.Now.AddDays(-1).ToShortDateString());

                                EmailBody = EmailBody.Replace("[$TotalReviews$]", summary.TotalReviews.ToString("#,##0"))
                                                     .Replace("[$AvgGroupRating$]", Math.Round(summary.AvgRating, 1).ToString())
                                                     .Replace("[$NewGroupReviews$]", summary.NewReviews.ToString("#,##0"))
                                                     .Replace("[$PositiveGroupNewReviews$]", summary.NewPositiveReviews.ToString("#,##0"))
                                                     .Replace("[$NegativeGroupNewReviews$]", summary.NewNegativeReviews.ToString("#,##0"));


                                string content = string.Empty;
                                foreach (var detail in details)
                                {
                                    string TemplateRepeatingBlock = TemplateDTO.TemplateRepeatingBlock;

                                    TemplateRepeatingBlock = TemplateRepeatingBlock.Replace("[$AccountName$]", detail.AccountName)
                                                                                    .Replace("[$TotalReviews$]", detail.TotalReviews.ToString("#,##0"))
                                                                                    .Replace("[$AvgRating$]", Math.Round(detail.AvgRating, 1).ToString())
                                                                                    .Replace("[$NewReviews$]", detail.NewReviews.ToString("#,##0"))
                                                                                    .Replace("[$PositiveNewReviews$]", detail.NewPositiveReviews.ToString("#,##0"))
                                                                                    .Replace("[$NegativeNewReviews$]", detail.NewNegativeReviews.ToString("#,##0"));

                                    content += TemplateRepeatingBlock;
                                }

                                EmailBody = EmailBody.Replace("[$DealerReputationSummary$] ", content);

                                string Subject = template.Subject + " (" + DateTime.Now.AddDays(-1).ToShortDateString() + ")";

                                //Insert Into ReportLog (Target)
                                ReportLog reportLog = new ReportLog();

                                reportLog.SubscriptionID = item.ReportSubscriptionID;
                                reportLog.EmailAddress = item.EmailAddress;
                                reportLog.Subject = Subject;
                                reportLog.Body = EmailBody;

                                long ReportLogID = addReportLog(reportLog);

                                //Send Email
                                string SendFromEmailAddress = Settings.GetSetting("site.SendFromEmailAddress", "notifications@socialdealer.com");

                                EmailManager emailManager = new EmailManager();
                                bool sendResult = emailManager.sendNotification(
                                    "Enterprise Reputation Summary Daily Report", reportLog.Subject, reportLog.Body,
                                    SendFromEmailAddress, reportLog.EmailAddress,
                                    string.Empty, string.Empty)
                                ;

                                updateReportLogByID(ReportLogID, sendResult);

                                #endregion
                            }
                        }
                    }
                }
            }
        }

        private void SendTokenValidationReport(ReportSubscriptionToRun item)
        {
            if (item != null)
            {
                using (MainEntities db = ContextFactory.Main)
                {
                    if (db.Connection.State != System.Data.ConnectionState.Open)
                        db.Connection.Open();

                    ReportTemplate template = (from rt in db.ReportTemplates where rt.ID == item.ReportTemplateID select rt).FirstOrDefault();

                    SocialTokenValidationReportTemplateDTO TemplateDTO = JsonConvert.DeserializeObject<SocialTokenValidationReportTemplateDTO>(template.Body);

                    //item.UserID 
                    UserDTO userDTO = ProviderFactory.Security.GetUserByID(item.UserID, UserInfoDTO.System);

                    List<long> accountIDs = new List<long>();
                    //RoleTypeEnum
                    if (userDTO.RoleTypeID == 4)
                    {                        
                        //Select all Accounts Edmunds Import 
                        accountIDs = (from rsa in db.Accounts where rsa.ParentAccountID != 220171 select rsa.ID).ToList();
                    }
                    else
                    {
                        accountIDs = (from rsa in db.ReportSubscriptionAccounts where rsa.ReportSubscriptionID == item.ReportSubscriptionID select rsa.AccountID).ToList();
                    }
                    using (DbCommand cmd3 = db.CreateStoreCommand(
                        "report.spTokenValidationDetail",
                        System.Data.CommandType.StoredProcedure,
                            new SqlParameter("@AccountIDs", string.Join("|", accountIDs.ToArray()))
                        ))
                    {
                        using (DbDataReader reader = cmd3.ExecuteReader())
                        {
                            TokenValidationSummary summary = db.Translate<TokenValidationSummary>(reader).FirstOrDefault();

                            reader.NextResult();

                            List<TokenValidationDetail> details = db.Translate<TokenValidationDetail>(reader).ToList();

                            if (details.Any())
                            {
                                #region HTML
                                string EmailBody = TemplateDTO.body;

                                EmailBody = EmailBody.Replace("[$Date$]", DateTime.Now.ToShortDateString());

                                EmailBody = EmailBody.Replace("[$TotalAccount$]", summary.TotalAccount.ToString("#,##0"))
                                                     .Replace("[$ValidAccount$]", summary.ValidAccount.ToString("#,##0"))
                                                     .Replace("[$InValidAccount$]", summary.InValidAccount.ToString("#,##0"))
                                                     .Replace("[$TotalFacebook$]", summary.TotalFacebook.ToString("#,##0"))
                                                     .Replace("[$ValidFacebook$]", summary.ValidFacebook.ToString("#,##0"))
                                                     .Replace("[$InValidFacebook$]", summary.InValidFacebook.ToString("#,##0"))
                                                     .Replace("[$TotalTwitter$]", summary.TotalTwitter.ToString("#,##0"))
                                                     .Replace("[$ValidTwitter$]", summary.ValidTwitter.ToString("#,##0"))
                                                     .Replace("[$InValidTwitter$]", summary.InValidTwitter.ToString("#,##0"))
                                                     .Replace("[$TotalGoogle+$]", summary.TotalGooglePlus.ToString("#,##0"))
                                                     .Replace("[$ValidGoogle+$]", summary.ValidGooglePlus.ToString("#,##0"))
                                                     .Replace("[$InValidGoogle+$]", summary.InValidGooglePlus.ToString("#,##0"));


                                TimeZoneInfo timeZone = ProviderFactory.TimeZones.GetTimezoneForUserID(item.UserID);

                                string content = string.Empty;
                                foreach (var detail in details)
                                {
                                    string TemplateRepeatingBlock = TemplateDTO.TemplateRepeatingBlock;

                                    string SubscribedPackages = string.Empty;

                                    List<string> Package = (from s in db.Subscriptions
                                                            join rp in db.Resellers_Packages on s.Reseller_PackageID equals rp.ID
                                                            join p in db.Packages on rp.PackageID equals p.ID
                                                            where s.AccountID == detail.ID
                                                            select p.Name).ToList();

                                    if (Package != null)
                                        SubscribedPackages = string.Join(", ", Package.ToArray());

                                    TemplateRepeatingBlock = TemplateRepeatingBlock.Replace("[$AccountName$]", detail.AccountName)
                                                                                    .Replace("[$AccountID$]", detail.ID.ToString())
                                                                                    .Replace("[$SubscribedPackages$]", SubscribedPackages)                                                                                    
                                                                                    .Replace("[$SocialNetwork$]", detail.SocialNetworkName)
                                                                                    .Replace("[$PaggURL$]]", detail.URI)
                                                                                    .Replace("[$PageName$]", detail.ScreenName);

                                    string TokenValidateLink = string.Empty;
                                    if (detail.SocialNetworkID == 1)
                                    {
                                        TokenValidateLink = string.Format("https://graph.facebook.com/debug_token?input_token={0}&access_token={1}", detail.Token, detail.AppToken);
                                        TemplateRepeatingBlock = TemplateRepeatingBlock.Replace("[$TokenValidateLink$]", TokenValidateLink);
                                        TemplateRepeatingBlock = TemplateRepeatingBlock.Replace("[$Validate$]", "Validate");
                                    }
                                    else
                                    {
                                        TemplateRepeatingBlock = TemplateRepeatingBlock.Replace("[$TokenValidateLink$]", string.Empty);
                                        TemplateRepeatingBlock = TemplateRepeatingBlock.Replace("[$Validate$]", string.Empty);
                                    }

                                    if (detail.DateLastValidated != null)
                                    {
                                        DateTime dt = new SIDateTime(detail.DateLastValidated.Value, timeZone).LocalDate.Value;
                                        TemplateRepeatingBlock = TemplateRepeatingBlock.Replace("[$DateLastUpdated$]", dt.ToString());
                                    }
                                    else
                                    {
                                        TemplateRepeatingBlock = TemplateRepeatingBlock.Replace("[$DateLastUpdated$]", string.Empty);
                                    }

                                    content += TemplateRepeatingBlock;
                                }

                                EmailBody = EmailBody.Replace("[$TemplateRepeatingBlock$]", content);

                                string Subject = template.Subject + " (" + DateTime.Now.ToShortDateString() + ")";

                                //Insert Into ReportLog (Target)
                                ReportLog reportLog = new ReportLog();

                                reportLog.SubscriptionID = item.ReportSubscriptionID;
                                reportLog.EmailAddress = item.EmailAddress;
                                reportLog.Subject = Subject;
                                reportLog.Body = EmailBody;

                                long ReportLogID = addReportLog(reportLog);

                                //Send Email
                                string SendFromEmailAddress = Settings.GetSetting("site.SendFromEmailAddress", "notifications@socialdealer.com");

                                EmailManager emailManager = new EmailManager();
                                bool sendResult = emailManager.sendNotification(
                                    "Token Validation Report", reportLog.Subject, reportLog.Body,
                                    SendFromEmailAddress, reportLog.EmailAddress,
                                    string.Empty, string.Empty)
                                ;

                                updateReportLogByID(ReportLogID, sendResult);

                                #endregion
                            }
                        }
                    }
                }
            }
        }

        private void SendReputationAndSocialReport(ReportSubscriptionToRun item)
        {
            if (item != null)
            {
                using (MainEntities db = ContextFactory.Main)
                {
                    if (db.Connection.State != System.Data.ConnectionState.Open)
                        db.Connection.Open();

                    ReportTemplate template = (from rt in db.ReportTemplates where rt.ID == item.ReportTemplateID select rt).FirstOrDefault();

                    ReputationAndSocialReportDTO TemplateDTO = JsonConvert.DeserializeObject<ReputationAndSocialReportDTO>(template.Body);

                    UserDTO userDTO = ProviderFactory.Security.GetUserByID(item.UserID, UserInfoDTO.System);

                    List<long> accountIDs = new List<long>();

                    accountIDs = (from rsa in db.ReportSubscriptionAccounts where rsa.ReportSubscriptionID == item.ReportSubscriptionID select rsa.AccountID).ToList();

                    DateTime LastMonthLastDate = DateTime.Today.AddDays(0 - DateTime.Today.Day);
                    DateTime LastMonthFirstDate = LastMonthLastDate.AddDays(1 - LastMonthLastDate.Day);

                    string startDate = LastMonthFirstDate.ToShortDateString();
                    string endDate = LastMonthLastDate.ToShortDateString();

                    using (DbCommand cmd3 = db.CreateStoreCommand(
                        "report.spGetReputationSocialReport",
                        System.Data.CommandType.StoredProcedure,
                            new SqlParameter("@AccountIDs", string.Join("|", accountIDs.ToArray())),
                            new SqlParameter("@startdate", startDate),
                            new SqlParameter("@enddate", endDate)
                        ))
                    {
                        using (DbDataReader reader = cmd3.ExecuteReader())
                        {
                            List<ReputationAndSocialReportDetail> details = db.Translate<ReputationAndSocialReportDetail>(reader).ToList();

                            reader.NextResult();

                            List<ReputationAndSocialReportSummary> summarys = db.Translate<ReputationAndSocialReportSummary>(reader).ToList();


                            if (details.Any() && summarys.Any())
                            {
                                #region HTML
                                string EmailBody = TemplateDTO.Body;

                                EmailBody = EmailBody.Replace("[$Month$]", Convert.ToDateTime(startDate).ToString("MMMM", CultureInfo.CreateSpecificCulture("en-US")))
                                                     .Replace("[$Year$]", Convert.ToDateTime(startDate).Year.ToString());

                                string content = string.Empty;
                                int iCounter = 1;
                                foreach (var detail in details)
                                {
                                    string TemplateRepeatingBlock = TemplateDTO.TemplateRepeatingBlock;
                                  
                                    TemplateRepeatingBlock = TemplateRepeatingBlock.Replace("[$ID$]", iCounter.ToString("#,##0"))
                                                                                    .Replace("[$AccountName$]", detail.AccountName)
                                                                                    .Replace("[$TotalReviews$]", detail.TotalReviews.GetValueOrDefault().ToString("#,##0"))
                                                                                    .Replace("[$PositiveReviews$]", detail.TotalReviewPositive.GetValueOrDefault().ToString("#,##0"))
                                                                                    .Replace("[$NegativeReviews$]", detail.TotalReviewNegative.GetValueOrDefault().ToString("#,##0"))
                                                                                    .Replace("[$Rating$]", Math.Round(detail.Rating.GetValueOrDefault(),1).ToString())
                                                                                    .Replace("[$NewReview$]", detail.TotalReviewPeriod.GetValueOrDefault().ToString("#,##0"))
                                                                                    .Replace("[$Fans$]", detail.Fans.GetValueOrDefault().ToString("#,##0"))
                                                                                    .Replace("[$NewFans$]", detail.NewFans.GetValueOrDefault().ToString("#,##0"))
                                                                                    .Replace("[$EngagedUsers$]", detail.EngagedUsers.GetValueOrDefault().ToString("#,##0"))
                                                                                    .Replace("[$FanImpressions$]", detail.FanImpressions.GetValueOrDefault().ToString("#,##0"))
                                                                                    .Replace("[$TotalImpressions$]", detail.TotalImpressions.GetValueOrDefault().ToString("#,##0"))
                                                                                    .Replace("[$PaidPostImpressions$]", detail.PaidPostImpressions.GetValueOrDefault().ToString("#,##0"))
                                                                                    .Replace("[$Followers$]", detail.Followers.GetValueOrDefault().ToString("#,##0"))
                                                                                    .Replace("[$Subscribers$]", detail.Subscribers.GetValueOrDefault().ToString("#,##0"))
                                                                                    .Replace("[$PlusOnes$]", detail.PlusOnes.GetValueOrDefault().ToString("#,##0"));

                                    content += TemplateRepeatingBlock;

                                    iCounter++;
                                }

                                string SummaryContent = string.Empty;
                                iCounter = 1;
                                foreach (var summary in summarys)
                                {
                                    string TemplateRepeatingBlockSummary = TemplateDTO.TemplateRepeatingBlockSummary;
                                    int TotalReviews = summary.TotalReviews.GetValueOrDefault();

                                    TemplateRepeatingBlockSummary = TemplateRepeatingBlockSummary.Replace("[$ID$]", iCounter.ToString("#,##0"))
                                                                                                    .Replace("[$Name$]", summary.AccountName)
                                                                                                    .Replace("[$Total Reviews$]", summary.TotalReviews.GetValueOrDefault().ToString("#,##0"));

                                    #region YTDGrowth
                            
                                    string YTDGrowthColor = "black"; 
                                    int YTDGrowth = 0;
                                    string YTDGrowth_str = "0";
                                    YTDGrowth = summary.TotalReviews.GetValueOrDefault() - summary.TotalReviewsLastYearLastMonth.GetValueOrDefault();

                                    if (YTDGrowth > 0)
                                    {
                                        YTDGrowthColor = "black";
                                        YTDGrowth_str = YTDGrowth.ToString("#,##0");
                                    }
                                    else if (YTDGrowth < 0)
                                    {
                                        YTDGrowthColor = "red";
                                        YTDGrowth_str = "(" + YTDGrowth.ToString("#,##0") + ")";
                                    }
                                    if (YTDGrowth == 0)
                                    {
                                        YTDGrowthColor = "black";
                                        YTDGrowth_str = YTDGrowth.ToString("#,##0");
                                    }
                                    TemplateRepeatingBlockSummary = TemplateRepeatingBlockSummary.Replace("[$YTD Growth Color$]", YTDGrowthColor)
                                                                                                   .Replace("[$YTD Growth$]", YTDGrowth_str.Replace("-",""));

                                    #endregion

                                    #region YTD % Growth
                                    
                                    decimal YTDPercentGrowth = 0;                                    
                                    if (TotalReviews > 0)
                                    {
                                        YTDPercentGrowth = Convert.ToDecimal(Convert.ToDecimal(YTDGrowth) / Convert.ToDecimal(TotalReviews)) * Convert.ToDecimal(100);
                                        YTDPercentGrowth = Math.Round(YTDPercentGrowth, 1);
                                    }
                                    else
                                    {
                                        YTDPercentGrowth = 0;
                                    }
                                    TemplateRepeatingBlockSummary = TemplateRepeatingBlockSummary.Replace("[$YTD % Growth$]", YTDPercentGrowth.ToString() + "%");
                                    #endregion

                                    #region Positive
                                    
                                    int TotalPosNeg = summary.TotalReviewPositive.GetValueOrDefault() + summary.TotalReviewNegative.GetValueOrDefault();
                                    int TotalPos = summary.TotalReviewPositive.GetValueOrDefault();

                                    decimal PercentPositive = 0;
                                    string PercentPositiveColor = "";
                                    if (TotalPosNeg > 0)
                                    {
                                        PercentPositive = Convert.ToDecimal(Convert.ToDecimal(TotalPos) / Convert.ToDecimal(TotalPosNeg));
                                        //PercentPositive = Math.Round(PercentPositive, 2);
                                    }
                                    else
                                    {
                                        PercentPositive = 0;
                                    }

                                    if (PercentPositive >= Convert.ToDecimal(0.61) && PercentPositive <= Convert.ToDecimal(0.79))
                                    {
                                        PercentPositiveColor = "#ffeb9c";
                                    }
                                    else if (PercentPositive >= Convert.ToDecimal(0) && PercentPositive <= Convert.ToDecimal(0.60))
                                    {
                                        PercentPositiveColor = "#ffc7ce";
                                    }
                                    else if (PercentPositive >= Convert.ToDecimal(0.80) && PercentPositive <= Convert.ToDecimal(1))
                                    {
                                        PercentPositiveColor = "#c6efce";
                                    }

                                    PercentPositive = Math.Round(PercentPositive * Convert.ToDecimal(100),1);

                                    TemplateRepeatingBlockSummary = TemplateRepeatingBlockSummary.Replace("[$% Positive$]", PercentPositive.ToString() + "%")
                                                                                                    .Replace("[$% Positive Color$]", PercentPositiveColor);

                                    #endregion
                                                                        
                                    TemplateRepeatingBlockSummary = TemplateRepeatingBlockSummary.Replace("[$Rating$]", Math.Round(summary.Rating.GetValueOrDefault(), 1).ToString());

                                    #region Rating Chg
                                   
                                    decimal Rating = summary.Rating.GetValueOrDefault();
                                    decimal RatingLastYearLastMonth = summary.RatingLastYearLastMonth.GetValueOrDefault();
                                    decimal RatingChg = 0;
                                    string RatingChgColor = "";
                                    
                                    if (RatingLastYearLastMonth > 0)
                                    {
                                        RatingChg = Convert.ToDecimal((Convert.ToDecimal(Convert.ToDecimal(Rating) - Convert.ToDecimal(RatingLastYearLastMonth)) / Convert.ToDecimal(RatingLastYearLastMonth)));
                                        //RatingChg = Math.Round(RatingChg, 2);
                                    }
                                    else
                                    {
                                        RatingChg = 0;
                                    }

                                    if (RatingChg <= Convert.ToDecimal(0) && RatingChg >= Convert.ToDecimal(-5))
                                    {
                                        RatingChgColor = "red";
                                    }
                                    else
                                    {
                                        RatingChgColor = "black";
                                    }

                                    RatingChg = Math.Round(RatingChg * Convert.ToDecimal(100), 1);

                                    TemplateRepeatingBlockSummary = TemplateRepeatingBlockSummary.Replace("[$Rating Chg$]", RatingChg.ToString() + "%")
                                                                                                    .Replace("[$Rating Chg Color$]", RatingChgColor);

                                    #endregion

                                    int TotalFans = summary.TotalFans.GetValueOrDefault();
                                    TemplateRepeatingBlockSummary = TemplateRepeatingBlockSummary.Replace("[$Total Fans$]", TotalFans.ToString("#,##0"));

                                    int YTDFanGrowth = TotalFans - summary.TotalFansLastYear.GetValueOrDefault();
                                    TemplateRepeatingBlockSummary = TemplateRepeatingBlockSummary.Replace("[$YTD Fan Growth$]", YTDFanGrowth.ToString("#,##0"));

                                    #region YTD % Fan Growth
                                    
                                    decimal YTDPercentFanGrowth = 0;
                                    if (TotalFans > 0)
                                    {
                                        YTDPercentFanGrowth = Convert.ToDecimal(Convert.ToDecimal(YTDFanGrowth) / Convert.ToDecimal(TotalFans)) * Convert.ToDecimal(100);
                                    }
                                    else
                                    {
                                        YTDPercentFanGrowth = 0;
                                    }
                                    TemplateRepeatingBlockSummary = TemplateRepeatingBlockSummary.Replace("[$YTD % Growth$]", YTDPercentFanGrowth.ToString() + "%");

                                    #endregion

                                    TemplateRepeatingBlockSummary = TemplateRepeatingBlockSummary.Replace("[$YTD Engaged Users$]", summary.TotalEngagedUsers.GetValueOrDefault().ToString("#,##0"))
                                                                                                    .Replace("[$YTD Fan Impressions$]", summary.TotalFanImpressions.GetValueOrDefault().ToString("#,##0"))
                                                                                                    .Replace("[$YTD Total Impressions$]", summary.TotalTotalImpressions.GetValueOrDefault().ToString("#,##0"))
                                                                                                    .Replace("[$YTD Paid Post Impressions$]", summary.TotalPaidPostImpressions.GetValueOrDefault().ToString("#,##0"));

                                    int TotalFollowers = summary.TotalFollowers.GetValueOrDefault();
                                    TemplateRepeatingBlockSummary = TemplateRepeatingBlockSummary.Replace("[$Total Followers$]", TotalFollowers.ToString("#,##0"));

                                    int YTDFollowersGrowth = TotalFollowers - summary.TotalFollowersLastYear.GetValueOrDefault();
                                    TemplateRepeatingBlockSummary = TemplateRepeatingBlockSummary.Replace("[$YTD Followers Growth$]", YTDFollowersGrowth.ToString("#,##0"));

                                    #region YTD % Followers Growth

                                    decimal YTDPercentFollowersGrowth = 0;
                                    if (TotalFollowers > 0)
                                    {
                                        YTDPercentFollowersGrowth = Convert.ToDecimal(Convert.ToDecimal(YTDFollowersGrowth) / Convert.ToDecimal(TotalFollowers)) * Convert.ToDecimal(100);
                                    }
                                    else
                                    {
                                        YTDPercentFollowersGrowth = 0;
                                    }
                                    TemplateRepeatingBlockSummary = TemplateRepeatingBlockSummary.Replace("[$YTD Growth % Twitter$]", YTDPercentFollowersGrowth.ToString() + "%");

                                    #endregion

                                    int TotalSubscribers = summary.TotalSubscribers.GetValueOrDefault();
                                    TemplateRepeatingBlockSummary = TemplateRepeatingBlockSummary.Replace("[$Total Subscribers$]", TotalSubscribers.ToString("#,##0"));

                                    int YTDSubscribersGrowth = TotalSubscribers - summary.TotalSubscribersLastYear.GetValueOrDefault();
                                    TemplateRepeatingBlockSummary = TemplateRepeatingBlockSummary.Replace("[$YTD Subscribers Growth$]", YTDSubscribersGrowth.ToString("#,##0"));

                                    int TotalPlusOnes = summary.TotalPlusOnes.GetValueOrDefault();
                                    TemplateRepeatingBlockSummary = TemplateRepeatingBlockSummary.Replace("[$Total PlusOne's$]", TotalPlusOnes.ToString("#,##0"));

                                    int YTDPlusOnesGrowth = TotalPlusOnes - summary.TotalPlusOnesLastYear.GetValueOrDefault();
                                    TemplateRepeatingBlockSummary = TemplateRepeatingBlockSummary.Replace("[$YTD PlusOne's Growth$]", YTDPlusOnesGrowth.ToString("#,##0"));

                                    SummaryContent += TemplateRepeatingBlockSummary;

                                    iCounter++;
                                }

                                EmailBody = EmailBody.Replace("[$ContentTemplateRepeatingBlock$]", content);
                                EmailBody = EmailBody.Replace("[$SummaryTemplateRepeatingBlock$]", SummaryContent);

                                string Subject = template.Subject + " (" + Convert.ToDateTime(startDate).ToString("MMMM", CultureInfo.CreateSpecificCulture("en-US")) + " / " + Convert.ToDateTime(startDate).Year.ToString() + ")";

                                //Insert Into ReportLog (Target)
                                ReportLog reportLog = new ReportLog();

                                reportLog.SubscriptionID = item.ReportSubscriptionID;
                                reportLog.EmailAddress = item.EmailAddress;
                                reportLog.Subject = Subject;
                                reportLog.Body = EmailBody;

                                long ReportLogID = addReportLog(reportLog);

                                //Send Email
                                string SendFromEmailAddress = Settings.GetSetting("site.SendFromEmailAddress", "notifications@socialdealer.com");

                                EmailManager emailManager = new EmailManager();
                                bool sendResult = emailManager.sendNotification(
                                    "Reputation & Social Report", reportLog.Subject, reportLog.Body,
                                    SendFromEmailAddress, "a.patel@socialdealer.com",//reportLog.EmailAddress,
                                    string.Empty, string.Empty)
                                ;

                                updateReportLogByID(ReportLogID, sendResult);

                                #endregion
                            }
                        }

                    }

                }
            }
        }

        

        private long addReportLog(ReportLog reportLog)
        {
            long result = 0;

            using (MainEntities db = ContextFactory.Main)
            {
                DAL.ReportLog rl = new DAL.ReportLog();

                rl.SubscriptionID = reportLog.SubscriptionID;
                rl.EmailAddress = reportLog.EmailAddress;
                rl.Subject = reportLog.Subject;
                rl.Body = reportLog.Body;                
                rl.DateSent = null;
                rl.DateFailed = null;

                db.ReportLogs.AddObject(rl);

                db.SaveChanges();

                result = rl.ID;
            }

            return result;

        }

        private void updateReportLogByID(long ReportLogID, bool sendResult)
        {
            using (MainEntities db = ContextFactory.Main)
            {
                ReportLog reportLog = db.ReportLogs.Where(c => c.ID == ReportLogID).FirstOrDefault();

                if (reportLog != null)
                {
                    if (sendResult)
                    {
                        reportLog.DateSent = DateTime.UtcNow;
                    }
                    else
                    {
                        reportLog.DateFailed = DateTime.UtcNow;
                    }

                    db.SaveChanges();
                }
            }
        }

        private long addNotificationTarget(NotificationTarget notificationTarget)
        {
            long result = 0;

            using (MainEntities db = ContextFactory.Main)
            {
                db.NotificationTargets.AddObject(notificationTarget);
                db.SaveChanges();
                result = notificationTarget.ID;
            }

            return result;

        }

        private void updateNotificationTarget(long NotificationTargetID, NotificationStatusesEnum NotificationStatusID)
        {
            using (MainEntities db = ContextFactory.Main)
            {
                db.spUpdateNotificationTarget(NotificationTargetID, DateTime.UtcNow, (long)NotificationStatusID);
            }
        }


        #endregion
    }

    #region Private Class

    class InvalidToken
    {
        public long UserID { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public long CredentialID { get; set; }
        public long AccountID { get; set; }
        public string UniqueID { get; set; }
        public string URI { get; set; }
        public string ScreenName { get; set; }
        public string EmailAddress { get; set; }
        public DateTime DateLastValidated { get; set; }
        public int Hours { get; set; }
        public string SocialNetworkName { get; set; }
    }

    class NewReviewsbyReviewSource
    {
        public long AccountID { get; set; }
        public long ReviewSourceID { get; set; }
        public int DisplayOrder { get; set; }
        public string ReviewSourceName { get; set; }

        public int NewReviewsCount { get; set; }
        public int NewReviewsPositiveCount { get; set; }
        public int NewReviewsNegativeCount { get; set; }
        public decimal NewReviewsRatingSum { get; set; }

        public int SalesReviewCount { get; set; }
        public int TotalPositiveReviews { get; set; }
        public int TotalNegativeReviews { get; set; }
        public decimal AvgRating { get; set; }
    }

    class OverAllReputaion
    {
        public int TotalReview { get; set; }
        public decimal Rating { get; set; }
    }

    class NewReviewsbyMonth
    {
        public int MonthID { get; set; }
        public int Month { get; set; }
        public long AccountID { get; set; }
        public int TotalReviewPeriod { get; set; }
        public decimal NewReviewsRatingSum { get; set; }
        public int TotalReviewPeriodPositive { get; set; }
        public int TotalReviewPeriodNegative { get; set; }
    }

    class ReportGetEnterpriseReputationDailySummary
    {
        public decimal AvgRating { get; set; }
        public int TotalReviews { get; set; }
        public int NewReviews { get; set; }
        public int NewPositiveReviews { get; set; }
        public int NewNegativeReviews { get; set; }
    }

    class ReportGetEnterpriseReputationDaily
    {
        public long AccountID { get; set; }
        public string AccountName { get; set; }
        public int TotalReviews { get; set; }
        public decimal AvgRating { get; set; }
        public int NewReviews { get; set; }
        public int NewPositiveReviews { get; set; }
        public int NewNegativeReviews { get; set; }
    }

    class ReportSubscriptionToRun
    {
        public long ReportSubscriptionID { get; set; }
        public long UserID { get; set; }
        public string EmailAddress { get; set; }
        public long ReportTemplateID { get; set; }

    }

    class TokenValidationSummary
    {
        public int TotalAccount { get; set; }
        public int ValidAccount { get; set; }
        public int InValidAccount { get; set; }

        public int TotalFacebook { get; set; }
        public int ValidFacebook { get; set; }
        public int InValidFacebook { get; set; }

        public int TotalGooglePlus { get; set; }
        public int ValidGooglePlus { get; set; }
        public int InValidGooglePlus { get; set; }

        public int TotalTwitter { get; set; }
        public int ValidTwitter { get; set; }
        public int InValidTwitter { get; set; }
    }

    class TokenValidationDetail
    {
        public long ID { get; set; }
        public string AccountName { get; set; }
        public long ParentAccountID { get; set; }
        public string ParentAccount { get; set; }

        public string CityName { get; set; }
        public string StateName { get; set; }
        public long ResellerID { get; set; }

        public string ResellerName { get; set; }
        public long SocialNetworkID { get; set; }
        public string SocialNetworkName { get; set; }

        public string UniqueID { get; set; }
        public string ScreenName { get; set; }
        public string Token { get; set; }

        public string TokenSecret { get; set; }
        public string URI { get; set; }
        public bool IsValid { get; set; }

        public DateTime? DateLastValidated { get; set; }
        public string AppToken { get; set; }
        
    }

    class ReputationAndSocialReportDetail
    {
        public long AccountID { get; set; }
		public string AccountName { get; set; }
        //public string ParentAccountID { get; set; }
        //public string ParentAccountName { get; set; }
        //public string CityName { get; set; }
        //public string StateLong { get; set; }
		public int? TotalReviews { get; set; }
        public int? TotalReviewPositive { get; set; }
        public int? TotalReviewNegative { get; set; }
        public decimal? Rating { get; set; }
        public int? TotalReviewPeriod { get; set; }
        //public string TotalReviewPeriodPositive { get; set; }
        //public string TotalReviewPeriodNegative { get; set; }
		//public string  AvgRatingPeriod { get; set; }
        public int? Fans { get; set; }
        public int? NewFans { get; set; }
        public int? EngagedUsers { get; set; }
        public int? FanImpressions { get; set; }
        public int? TotalImpressions { get; set; }
        public int? PaidPostImpressions { get; set; }
        public int? Followers { get; set; }
        public int? Subscribers { get; set; }
        public int? PlusOnes { get; set; }
    }

    class ReputationAndSocialReportSummary
    {
        public long AccountID { get; set; }
        public string AccountName { get; set; }        
        public int? TotalReviews { get; set; }
        public int? TotalReviewsLastYearLastMonth { get; set; }
        public int? TotalReviewPositive { get; set; }
        public int? TotalReviewNegative { get; set; }
        public decimal? Rating { get; set; }
        public decimal? RatingLastYearLastMonth { get; set; }
        public int? TotalFans { get; set; }
        public int? TotalFansLastYear { get; set; }        
        public int? TotalEngagedUsers { get; set; }
        public int? TotalFanImpressions { get; set; }
        public int? TotalTotalImpressions { get; set; }
        public int? TotalPaidPostImpressions { get; set; }
        public int? TotalFollowers { get; set; }
        public int? TotalFollowersLastYear { get; set; }
        public int? TotalSubscribers { get; set; }
        public int? TotalSubscribersLastYear { get; set; }
        public int? TotalPlusOnes { get; set; }
        public int? TotalPlusOnesLastYear { get; set; }
    }

    class AudienceFacebook
    {
        public int MonthID { get; set; }
        public int Month { get; set; }
        public long AccountID { get; set; }
        public int Fans { get; set; }
        public int NewFans { get; set; }
        public int EngagedUsers { get; set; }
        public int FanImpressions { get; set; }
        public int TotalImpressions { get; set; }
        public int PaidPostImpressions { get; set; }
    }

    class AudienceTwitter
    {
        public int MonthID { get; set; }
        public int Month { get; set; }
        public long AccountID { get; set; }
        public int Followers { get; set; }
        public int NewFollowers { get; set; }        
    }

    class AudienceGooglePlus
    {
        public int MonthID { get; set; }
        public int Month { get; set; }
        public long AccountID { get; set; }
        public int PlusOneCount { get; set; }
        public int NewPlusOneCount { get; set; }
    }

    class FacebookPostData
    {
        public int MonthID { get; set; }
        public int Month { get; set; }
        public long AccountID { get; set; }
        public int Posts { get; set; }
        public int NewLikes { get; set; }
        public int Comments { get; set; }
        public int Clicks { get; set; }
        public int Shares { get; set; }
        public int OrganicImpressions { get; set; }
        public int PaidImpressions { get; set; }
    }

    class TwitterPostData
    {
        public int MonthID { get; set; }
        public int Month { get; set; }
        public long AccountID { get; set; }
        public int Tweets { get; set; }
        public int ReTweetCount { get; set; }     
    }

    class GooglePlusPostData
    {
        public int MonthID { get; set; }
        public int Month { get; set; }
        public long AccountID { get; set; }
        public int Posts { get; set; }
        public int PlusOnes { get; set; }
        public int Comments { get; set; }
        public int ReShares { get; set; }
    }

    class MonthData
    {
        public int ID { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
    }

    #endregion
}
