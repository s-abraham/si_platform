﻿using SI.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SI.BLL
{
    public class Auditor
    {
        public Auditor(AuditLevelEnum level, AuditTypeEnum type, UserInfoDTO userInfo, string message)
        {
            Message = message;
            Level = level;
            Type = type;
            UserInfo = userInfo;
        }
        public Auditor(AuditLevelEnum level, AuditTypeEnum type, UserInfoDTO userInfo, string messageFormat, params object[] args) : this(level, type, userInfo, string.Format(messageFormat, args)) { }

        public static Auditor New(AuditLevelEnum level, AuditTypeEnum type, UserInfoDTO userInfo, string message)
        {
            return new Auditor(level, type, userInfo, message);
        }
        public static Auditor New(AuditLevelEnum level, AuditTypeEnum type, UserInfoDTO userInfo, string messageFormat, params object[] args)
        {
            return new Auditor(level, type, userInfo, messageFormat, args);
        }

        public AuditLevelEnum Level { get; set; }
        public AuditTypeEnum Type { get; set; }
        public string Message { get; set; }
        public UserInfoDTO UserInfo { get; set; }
        public long? AccountID { get; set; }
        public long? CredentialID { get; set; }
        public long? Account_ReviewSourceID { get; set; }
        public long? ReviewID { get; set; }
        public object AuditDataObject { get; set; }

        public Auditor SetMessage(string message)
        {
            Message = message;
            return this;
        }
        public Auditor SetMessage(string messageFormat, params object[] args)
        {
            return this.SetMessage(string.Format(messageFormat, args));
        }
        public Auditor SetAccountID(long? value)
        {
            AccountID = value;
            return this;
        }
        public Auditor SetCredentialID(long? value)
        {
            CredentialID = value;
            return this;
        }
        public Auditor SetAccount_ReviewSourceID(long? value)
        {
            Account_ReviewSourceID = value;
            return this;
        }
        public Auditor SetReviewID(long? value)
        {
            ReviewID = value;
            return this;
        }
        public Auditor SetAuditDataObject(object value)
        {
            AuditDataObject = value;
            return this;
        }


        public void Save(ILoggingProvider provider)
        {
            provider.Audit(Level, Type, Message, AuditDataObject, UserInfo, AccountID, CredentialID, Account_ReviewSourceID, ReviewID);
        }

    }
}
