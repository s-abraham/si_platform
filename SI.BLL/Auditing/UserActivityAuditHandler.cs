﻿using SI.DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SI.BLL
{
    public class UserActivityAuditHandler : AuditHandlerBase
    {
        protected override DTO.AuditTypeEnum AuditType()
        {
            return DTO.AuditTypeEnum.UserActivity;
        }

        public override void ProcessAudits(List<DTO.AuditDTO> audits)
        {
            using (MainEntities db = ContextFactory.Main)
            {
                foreach (DTO.AuditDTO audit in audits)
                {
                    try
                    {
                        DTO.AuditUserActivity dto = Newtonsoft.Json.JsonConvert.DeserializeObject<DTO.AuditUserActivity>(audit.Info);
                        db.spLogUserActivity3(
                            (long)dto.Type,
                            dto.Type.ToString().ToLower(),
                            dto.UserID,
                            dto.ImpersonatedUserID,
                            dto.UserIDActedUpon,
                            dto.AccountID,
                            dto.RoleTypeID,
                            dto.SSOToken,
                            dto.AccessToken,
                            dto.CredentialID,
                            dto.Description,
                            audit.DateCreated.GetUTCDate(TimeZoneInfo.Utc),
                            dto.ReviewSourceID,
                            dto.ObjectID,
                            dto.PostID,
                            dto.PostTargetID,
                            dto.PackageID,
                            dto.ResellerID,
                            dto.SubscriptionID,
                            dto.SyndicatorID
                        );
                    }
                    catch (Exception ex)
                    {
                        ProviderFactory.Logging.LogException(ex);
                    }
                }
            }
        }
    }
}
