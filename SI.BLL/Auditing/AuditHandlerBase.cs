﻿using SI.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SI.BLL
{
    public abstract class AuditHandlerBase
    {
        protected abstract AuditTypeEnum AuditType();
        public void ProcessAudit(AuditDTO audit)
        {
            ProcessAudits(new List<AuditDTO>() { audit });
        }
        public abstract void ProcessAudits(List<AuditDTO> audits);
    }
}
