﻿using SI.DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SI.BLL
{
    public class AccountActivityAuditHandler : AuditHandlerBase
    {
        protected override DTO.AuditTypeEnum AuditType()
        {
            return DTO.AuditTypeEnum.AccountActivity;
        }

        public override void ProcessAudits(List<DTO.AuditDTO> audits)
        {
            using (MainEntities db = ContextFactory.Main)
            {
                foreach (DTO.AuditDTO audit in audits)
                {
                    try
                    {
                        DTO.AuditAccountActivity dto = Newtonsoft.Json.JsonConvert.DeserializeObject<DTO.AuditAccountActivity>(audit.Info);
                        db.spAuditAccountActivity(dto.AccountID, (long)dto.Type, dto.ResellerPackageID, dto.PackageID, (long?)dto.FeatureType, dto.Description);
                    }
                    catch (Exception ex)
                    {
                        ProviderFactory.Logging.LogException(ex);
                    }
                }
            }
        }
    }
}
