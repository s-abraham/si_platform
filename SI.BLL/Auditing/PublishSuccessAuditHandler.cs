﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SI.BLL
{
    public class PublishSuccessAuditHandler : AuditHandlerBase
    {
        protected override DTO.AuditTypeEnum AuditType()
        {
            return DTO.AuditTypeEnum.PublishSuccess;
        }        

        public override void ProcessAudits(List<DTO.AuditDTO> audits)
        {
            //TODO: Finish this
        }
    }
}
