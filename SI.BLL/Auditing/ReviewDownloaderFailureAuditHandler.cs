﻿using SI.DAL;
using SI.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SI.BLL
{
    public class ReviewDownloaderFailureAuditHandler : AuditHandlerBase
    {
        protected override DTO.AuditTypeEnum AuditType()
        {
            return DTO.AuditTypeEnum.ReviewDownloaderFailure;
        }

        public override void ProcessAudits(List<DTO.AuditDTO> audits)
        {
            using (MainEntities db = ContextFactory.Main)
            {
                try
                {

                    foreach (AuditDTO item in audits)
                    {
                        if (!string.IsNullOrWhiteSpace(item.Info))
                        {
                            AuditReviewDownloaderFailure data = Newtonsoft.Json.JsonConvert.DeserializeObject<AuditReviewDownloaderFailure>(item.Info);
                            if (data != null)
                            {
                                C_DownloaderFailures failure = new C_DownloaderFailures()
                                {
                                    Account_ReviewSourceID = item.Account_ReviewSourceID.GetValueOrDefault(0),
                                    AccountID = item.AccountID.GetValueOrDefault(0),
                                    JobID = data.JobID,
                                    FailureAuditDate = item.DateCreated.LocalDate.Value,
                                    ReviewSourceID = data.ReviewSourceID
                                };
                                if (data.FailedXPaths != null && data.FailedXPaths.Any())
                                {
                                    failure.FailureReason = string.Join("|", data.FailedXPaths.ToArray());
                                }
                                else
                                {
                                    failure.FailureReason = "";
                                }
                                if (!string.IsNullOrWhiteSpace(data.WebException))
                                {
                                    failure.FailureReason = string.Format("{0} - WebException: {1}", failure.FailureReason, data.WebException);
                                }

                                db.C_DownloaderFailures.AddObject(failure);
                                db.SaveChanges();
                            }
                        }
                    }

                }
                catch (Exception ex)
                {
                    ProviderFactory.Logging.LogException(ex);
                }
            }
        }
    }
}
