﻿using SI.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace SI.BLL
{
    public class NewReviewDetectedAuditHandler : AuditHandlerBase
    {
        protected override DTO.AuditTypeEnum AuditType()
        {
            return DTO.AuditTypeEnum.NewReviewDetected;
        }

        public override void ProcessAudits(List<AuditDTO> audits)
        {

            List<QueueNotificationDataDTO> toQueue = new List<QueueNotificationDataDTO>();
            foreach (AuditDTO audit in audits)
            {
                AuditNewReviewDetected data = JsonConvert.DeserializeObject<AuditNewReviewDetected>(audit.Info);
                ReviewDTO review = ProviderFactory.Reviews.GetReviewByID(data.ReviewID);
                if (review != null && review.ReviewDate != null && review.ReviewDate.LocalDate.HasValue && review.ReviewDate.LocalDate.Value.AddDays(30) >= DateTime.Now)
                {
                    toQueue.Add(new QueueNotificationDataDTO()
                    {
                        Type = NotificationMessageTypeEnum.NewReviewAlert,
                        EntityID = data.ReviewID,
                        AccountID = data.AccountID
                    });
                }
            }

            if (toQueue.Any())
            {
                ProviderFactory.Notifications.QueueNotifications(toQueue);
            }
        }
    }
}
