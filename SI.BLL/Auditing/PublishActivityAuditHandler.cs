﻿using SI.DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SI.BLL
{
    public class PublishActivityAuditHandler : AuditHandlerBase
    {
        protected override DTO.AuditTypeEnum AuditType()
        {
            return DTO.AuditTypeEnum.PublishActivity;
        }

        public override void ProcessAudits(List<DTO.AuditDTO> audits)
        {
            using (MainEntities db = ContextFactory.Main)
            {
                foreach (DTO.AuditDTO audit in audits)
                {
                    try
                    {
                        if (!string.IsNullOrWhiteSpace(audit.Info))
                        {
                            DTO.AuditPublishActivity data = Newtonsoft.Json.JsonConvert.DeserializeObject<DTO.AuditPublishActivity>(audit.Info);
                            C_PublishActivity activity = new C_PublishActivity()
                            {
                                AccountID = data.AccountID,
                                AuditPublishActivityTypeID = (long)data.Type,
                                JobID = data.JobID,
                                Description = data.Description,
                                PostID = data.PostID,
                                PostImageID = data.PostImageID,
                                PostTargetID = data.PostTargetID,
                                DateCreated = DateTime.UtcNow
                            };
                            db.C_PublishActivity.AddObject(activity);
                        }
                    }
                    catch (Exception ex)
                    {
                        ProviderFactory.Logging.LogException(ex);
                    }
                }
                db.SaveChanges();
            }
        }
    }
}
