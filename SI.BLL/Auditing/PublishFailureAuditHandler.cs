﻿using SI.DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SI.BLL
{
    public class PublishFailureAuditHandler : AuditHandlerBase
    {
        protected override DTO.AuditTypeEnum AuditType()
        {
            return DTO.AuditTypeEnum.PublishFailure;
        }

        public override void ProcessAudits(List<DTO.AuditDTO> audits)
        {
            using (MainEntities db = ContextFactory.Main)
            {
                List<DTO.AuditPublishFailure> failureDatas = new List<DTO.AuditPublishFailure>();

                foreach (DTO.AuditDTO audit in audits)
                {
                    try
                    {
                        if (!string.IsNullOrWhiteSpace(audit.Info))
                        {
                            DTO.AuditPublishFailure data = Newtonsoft.Json.JsonConvert.DeserializeObject<DTO.AuditPublishFailure>(audit.Info);
                            if (data != null)
                            {
                                failureDatas.Add(data);

                                C_PublishFailures failure = new C_PublishFailures()
                                {
                                    JobID = data.JobID,
                                    PostTargetID = data.PostTargetID,
                                    CredentialID = data.CredentialID,
                                    SocialAppID = data.SocialAppID,
                                    SocialNetworkID = data.SocialNetworkID,
                                    AccountID = data.AccountID,
                                    PostImageID = data.PostImageID,
                                    SocialNetworkResponse = data.SocialNetworkResponse,
                                    DateCreated = DateTime.UtcNow
                                };

                                db.C_PublishFailures.AddObject(failure);
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        ProviderFactory.Logging.LogException(ex);
                    }
                }

                db.SaveChanges();
            }
        }
    }
}
