﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Linq;

namespace SI.BLL
{
    public static class Locale
    {
        private static Dictionary<string, Dictionary<string, string>> _stringTable = new Dictionary<string, Dictionary<string, string>>();

        public static string Localize(string key)
        {
            lock (_stringTable)
            {
                if (!_stringTable.Any())
                    loadStringTable();

                CultureInfo culture = Thread.CurrentThread.CurrentCulture;
                CultureInfo cultureUI = Thread.CurrentThread.CurrentUICulture;

                string ckey = culture.Name.Trim().ToLower();

                Dictionary<string, string> table = null;
                if (_stringTable.TryGetValue(ckey, out table))
                {
                    string val;
                    if (table.TryGetValue(key, out val))
                    {
                        return val;
                    }
                }

                ckey = cultureUI.Name.Trim().ToLower();

                if (_stringTable.TryGetValue(ckey, out table))
                {
                    string val;
                    if (table.TryGetValue(key, out val))
                    {
                        return val;
                    }
                }
            }

            return key;
        }

        private static void loadStringTable()
        {
            lock (_stringTable)
            {
                _stringTable.Clear();

                List<Assembly> asses = new List<Assembly>() { Assembly.GetExecutingAssembly(), Assembly.GetCallingAssembly(), Assembly.GetEntryAssembly() };
                foreach (string resourceName in Util.GetResourceNamesContaining(asses, "locale-"))
                {
                    string content = Util.GetResourceTextFile(resourceName, Assembly.GetAssembly(typeof(ProviderFactory)));
                    XDocument doc = XDocument.Parse(content);
                    XElement loc = doc.Root;

                    if (loc.HasAttributes & loc.Attribute("id") != null)
                    {
                        string localeID = loc.Attribute("id").Value.Trim().ToLower();
                        if (!_stringTable.ContainsKey(localeID))
                        {
                            Dictionary<string, string> table = new Dictionary<string, string>();
                            _stringTable.Add(localeID, table);

                            foreach (XElement elm in loc.Descendants())
                            {
                                string type = elm.Name.ToString().Trim().ToLower();
                                string val = elm.Value;
                                if (!table.ContainsKey(type))
                                {
                                    table.Add(type, val);
                                }
                            }
                        }
                    }
                }
            }
        }

        public static void RegisterCustomCultures()
        {
			//if (!cultureExists("pl-US"))
			//{
			//	CultureAndRegionInfoBuilder car1 = new CultureAndRegionInfoBuilder("pl-US", CultureAndRegionModifiers.None);
			//	car1.LoadDataFromCultureInfo(CultureInfo.CreateSpecificCulture("pl-US"));
			//	car1.LoadDataFromRegionInfo(new RegionInfo("en-US"));
			//	car1.CultureEnglishName = "Piglatin (United States)";
			//	car1.CultureNativeName = "Iglatinpay (Itedunay Atestay)";
			//	car1.CurrencyNativeName = "Ollarsday (USD)";
			//	car1.RegionNativeName = "Piglandia";
			//	car1.Register();
			//}
        }

        private static bool cultureExists(string customCultureName)
        {
            foreach (CultureInfo ci in CultureInfo.GetCultures(CultureTypes.UserCustomCulture))
            {
                if (ci.Name.Equals(customCultureName, StringComparison.OrdinalIgnoreCase))
                    return true;
            }
            return false;
        }
    }

}
