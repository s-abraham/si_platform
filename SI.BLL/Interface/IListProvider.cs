﻿using SI.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
 

namespace SI.BLL
{
    public interface IListProvider
    {
        /// <summary>
        /// Creates or saves a country
        /// </summary>
        /// <param name="country">The country DTO to save</param>
        /// <returns>The outcome of the operation</returns>
        SaveEntityDTO<CountryDTO> SaveCountry(CountryDTO country);

        /// <summary>
        /// Gets a list of all countries.
        /// </summary>
        /// <returns>A list of country DTO objects.</returns>
        List<CountryDTO> GetCountries();

        /// <summary>
        /// Retrieves a country by it's ID
        /// </summary>
        /// <param name="countryID">The ID of the country to retrieve</param>
        /// <returns>A country DTO</returns>
        CountryDTO GetCountryByID(long countryID);

        /// <summary>
        /// Searches for a country with the supplied prefix
        /// </summary>
        /// <param name="prefix">The prefix of the country to look for</param>
        /// <returns>A country DTO</returns>
        CountryDTO GetCountryByPrefix(string prefix);

        /// <summary>
        /// Creates or saves a state
        /// </summary>
        /// <param name="state">The State DTO to create or save</param>
        /// <returns>The result of the operation</returns>
        SaveEntityDTO<StateDTO> SaveState(StateDTO state);

        /// <summary>
        /// Retrieves a sate based on it's ID
        /// </summary>
        /// <param name="stateID">The ID of the state to retrieve</param>
        /// <returns>A state DTO object</returns>
        StateDTO GetStateByID(long stateID);

        /// <summary>
        /// Retrieves a state based on it's abbreviation
        /// </summary>
        /// <param name="countryID">The country ID to search within</param>
        /// <param name="abbreviation">The abbreviation of the state to find</param>
        /// <returns>A state DTO</returns>
        StateDTO GetStateByAbbreviation(long countryID, string abbreviation);

        /// <summary>
        /// Retrieves a state based on it's name
        /// </summary>
        /// <param name="countryID">The country ID to search within</param>
        /// <param name="abbreviation">The name of the state to find</param>
        /// <returns>A state DTO</returns>
        StateDTO GetStateByName(long countryID, string name);

        /// <summary>
        /// Gets a list of states for a given country
        /// </summary>
        /// <param name="countryID">The ID of the country to retrieve states for</param>
        /// <returns>A list of state DTO objects</returns>
        List<StateDTO> GetStatesForCountry(long countryID);

        /// <summary>
        /// Retreives a zipcode
        /// </summary>
        /// <param name="zipCode">The zipcode to retrieve</param>
        /// <returns>A ZipCode DTO</returns>
        ZipCodeDTO GetZipCode(long zipCode);

        /// <summary>
        /// Retrieves a random zip code from the database
        /// </summary>
        /// <returns>A Zipcode DTO</returns>
        ZipCodeDTO GetRandomZipCode();

        /// <summary>
        /// gets a city by it's ID
        /// </summary>
        /// <param name="cityID">The ID of the city to retrieve</param>
        /// <returns>A City DTO</returns>
        CityDTO GetCityByID(long cityID);

        /// <summary>
        /// Gets a list of all cities for a given zip code
        /// </summary>
        /// <param name="zipCode">A zipcode to search</param>
        /// <returns>A list of cities for the supplied zip code</returns>
        List<CityDTO> GetCitiesForZipCode(string zipCode);

        /// <summary>
        /// Gets a list of cities for a given state
        /// </summary>
        /// <param name="stateID">A state ID</param>
        /// <returns>A list of cities for the supplied state ID</returns>
        List<CityDTO> GetCitiesForState(long stateID);


        /// <summary>
        /// Gets a list of franchise types
        /// </summary>
        /// <returns>A list of franchise types</returns>
        List<FranchiseTypeDTO> GetFranchiseTypes();

        /// <summary>
        /// Gets a a list of CRM System Types
        /// </summary>
        /// <returns>A list of CRM System Types</returns>
        List<CRMSystemTypeDTO> GetCRMSystemTypes();

        /// <summary>
        /// Creates a new franchise type
        /// </summary>
        FranchiseTypeDTO AddFranchiseType(string Name);

	    List<AccountTypesDTO> GetAccountTypes();

        /// <summary>
        /// Gets a list of Categorys
        /// </summary>
        /// <returns>A list of Categorys</returns>
        List<CategoryDTO> GetCategory();

        /// <summary>
        /// Gets a list of active system time zones
        /// </summary>
        List<TimeZoneDTO> GetTimeZones();

        /// <summary>
        /// Get list of Competitor by AccountID
        /// </summary>
        /// <param name="accountID"></param>
        /// <returns></returns>
        List<CompetitorDTO> GetCompetitorByAccountID(long accountID);

        /// <summary>
        /// Get list of Competitor by Distance
        /// </summary>
        /// <param name="accountID"></param>
        /// <returns></returns>
        //List<CompetitorDTO> GetAccountsByDistance(long accountID);
        List<CompetitorDTO> GetAccountsByDistance(UserInfoDTO userInfoDto, long accountID, int RangeInMiles = 50);
        

        /// <summary>
        /// Get List of Categories
        /// </summary>
        /// <returns></returns>
        List<CategoriesDTO> GetPostCategoriesByVerticalID(long verticalID);

	    /// <summary>
	    /// Get List of Vertical Markets
	    /// </summary>
	    /// <returns></returns>
	    List<VerticalMarketDTO> GetVerticalMarkets();
    }
}
