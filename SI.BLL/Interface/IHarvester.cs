﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SI.DTO;
using SI.BLL.Implementation;

namespace SI.BLL.Interface
{
    public interface IHarvesterProvider
    {

        /// <summary>
        /// Gets a harvester processor setting
        /// </summary>
        /// <param name="key">Enum of reviewSource</param>
        HarvestorxPaths GetHarvesterSettings(ReviewSourceEnum reviewSource);
    }

}
