﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SI.BLL
{
    public interface ISyndicatorImport
    {
        void ImportCARFAX(Guid UserID);

        void importRides();

        void importAllyFromExcel();

        void importAllyUsersFromExcel_SPN_824();
    }
}
