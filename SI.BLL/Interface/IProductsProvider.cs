﻿using SI.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SI.BLL
{
    public interface IProductsProvider
    {
        List<PackageDTO> GetAvailablePackagesForAccount(long accountID, UserInfoDTO userInfo);
        List<AccountPackageListItemDTO> GetSubscribedPackagesForAccount(long accountID, UserInfoDTO userInfo);

		UpdatePackageResultDTO AddPackage(long accountID, long resellerID, long packageID, UserInfoDTO userInfo, decimal? price, int trialDays);
        UpdatePackageResultDTO RemovePackage(long subscriptionID, UserInfoDTO userInfo);
        List<PackageDTO> GetPackages();

        /// <summary>
        /// For a given self registration access token, return information needed to present initial registration page
        /// </summary>
        SignupSetupInfoDTO GetSignupInfo(SyndicationSelfRegistrationDTO token);

        /// <summary>
        /// Gets a list of vertical markets
        /// </summary>
        List<VerticalMarketDTO> GetVerticalMarkets();

        /// <summary>
        /// Completes a signup process, saving information for an account, user and packages subscribed
        /// </summary>
        SaveEntityDTO<SignupSubmitInfoDTO> SignupSubmit(SaveEntityDTO<SignupSubmitInfoDTO> dto);

    }
}
