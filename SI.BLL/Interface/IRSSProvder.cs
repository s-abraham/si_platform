﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Connectors.Entities;
using SI.DTO;

namespace SI.BLL.Interface
{
    public interface IRSSProvider
    {
        bool SaveRSSData(RSSEntities rssEntities);

        /// <summary>
        /// Queues RSS Download jobs
        /// </summary>
        /// <param name="spreadAcrossMinutes">All of the queued jobs will be spread over this number of minutes</param>
        /// <param name="daysBack">The number of days back to process RSS items for</param>
        /// <param name="maxItems">The maximum number of items to download from any given feed</param>
        /// <param name="downloadImages">Whether or not to download images</param>
        void QueueRSSDownloads(int spreadAcrossMinutes, int daysBack, int maxItems, bool downloadImages);

        /// <summary>
        /// Retrieves an RSS Feed definition by it's ID
        /// </summary>
        RSSFeedDTO GetRSSFeed(long rssFeedID);

        /// <summary>
        /// Saves an RSS Feed Definition
        /// </summary>
        SaveEntityDTO<RSSFeedDTO> Save(SaveEntityDTO<RSSFeedDTO> dto);

        /// <summary>
        /// Searches RSS Items
        /// </summary>
        RSSSearchResultDTO SearchRSSItems(UserInfoDTO userInfo, RSSSearchRequestDTO req);
    }
}
