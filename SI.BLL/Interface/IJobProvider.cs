﻿using SI.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SI.BLL
{
    public interface IJobProvider
    {

        /// <summary>
        /// Saves a new or updated job to the database.
        /// </summary>
        DTO.SaveEntityDTO<DTO.JobDTO> Save(SaveEntityDTO<JobDTO> job);

        /// <summary>
        /// Saves a list of jobs in batch
        /// </summary>
        DTO.SaveEntityDTO<List<DTO.JobDTO>> Save(SaveEntityDTO<List<JobDTO>> job);


        /// <summary>
        /// Saves a job type
        /// </summary>
        DTO.SaveEntityDTO<DTO.JobTypeDTO> Save(SaveEntityDTO<JobTypeDTO> job);

        /// <summary>
        /// Gets an overall system status overview for use in the system console
        /// </summary>
        /// <param name="userID">The user id requesting the data</param>
        SystemStatusDTO GetSystemStatus(long userID);

        /// <summary>
        /// Gets a new list of work for a controller to process.  It will only return a single batch worth of jobs
        /// </summary>
        /// <param name="controllerID">The controller ID requesting the work</param>
        /// <param name="maxNewLoad">The maximum amount of load the jobs returned should amount to.</param>
        /// <returns>A list of Job IDs</returns>
        JobListDTO GetJobList(long controllerID, int maxNewLoad);

        /// <summary>
        /// Gets a list of jobs
        /// </summary>
        /// <param name="jobIDs">The list of job IDs that will determine the jobs in the returned list</param>
        List<JobDTO> GetJobs(List<long> jobIDs);
            

        /// <summary>
        /// Dual purposes, one to register a controller when it comes online, another to
        /// occasionally provide a heartbeat.
        /// </summary>
        /// <param name="machineName">The machine name of the controller's host machine</param>
        /// <param name="ipAddress">The IP Address of the controller</param>
        /// <returns>The ID of the controller</returns>
        long RegisterController(string machineName, string ipAddress, int currentLoad);

        /// <summary>
        /// Saves a new controller or updates to an existing controller
        /// </summary>
        SaveEntityDTO<ControllerDTO> Save(SaveEntityDTO<ControllerDTO> dto);


        /// <summary>
        /// Gets a controller based on its ID
        /// </summary>        
        GetEntityDTO<ControllerDTO> GetController(long userID, long controllerID);

        /// <summary>
        /// The controller sends a list of known jobIDs that it is acting upon.  If any IDs in the database are 
        /// assigned to this controller, but not in this list, they are orphaned.  Clean them up appropriately.
        /// </summary>
        void ReconcileControllerProcessInstances(long controllerID, List<long> jobIDs);

        /// <summary>
        /// Gets a list of all job types
        /// </summary>
        List<JobTypeDTO> GetJobTypes();

        /// <summary>
        /// Gets the current bundle has for a given job type
        /// </summary>
        string GetJobTypeBundleHash(long jobTypeID);

        /// <summary>
        /// Stores the zipped bundle of files that constitute a job's executable and support dlls
        /// </summary>
        void StoreJobBundle(long userID, long jobTypeID, byte[] zipContents);

        /// <summary>
        /// Returns the binary contents of a zip file that contains the executable and support DLLs for a job type
        /// </summary>
        byte[] GetJobBundle(long userID, long jobTypeID);

        /// <summary>
        /// Sets the job status and internally sets dates depending on the status set
        /// </summary>
        /// <param name="jobID">The ID of the job to update the status for</param>
        /// <param name="status">The status value to set</param>
        void SetJobStatus(long jobID, JobStatusEnum status);

        /// <summary>
        /// gets a processor setting based on a passed in key
        /// </summary>
        string GetProcessorSetting(string key);

        /// <summary>
        /// Gets a list of all scheduled
        /// </summary>
        List<ScheduleDTO> GetSchedules();

        /// <summary>
        /// Saves changes to a schedule, or saves a new schedule.
        /// </summary>
        void SaveSchedule(ScheduleDTO dto);

        /// <summary>
        /// Logs that a given set of jobs have been picked up by a controller
        /// </summary>
        void LogJobPickups(long controllerID, List<long> jobIDs);

        /// <summary>
        /// Performs routine cleanup of the job, exception and audit tables
        /// </summary>
        void PerformDatabaseCleanup();

        /// <summary>
        /// Saves a performance snapshot from a controller service instance
        /// </summary>
        void SavePerformanceSnapshot(PerfSnapshotDTO snap);

    }
}
