﻿
using SI.DAL;
using SI.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SI.BLL
{
    public interface ILoggingProvider
    {
        /// <summary>
        /// Logs an exception to the database
        /// </summary>
        /// <param name="ex">The exception to log</param>
        void LogException(Exception ex);

        /// <summary>
        /// Logs an exception and also saves the JobID it is associated with
        /// </summary>
        void LogException(Exception ex, long jobID);

        /// <summary>
        /// Logs a web exception to the database
        /// </summary>
        /// <param name="ex">The exception to log</param>
        /// <param name="URL">The URL that was called</param>
        void LogException(Exception ex, string URL);


        void Audit(
            AuditLevelEnum level,
            AuditTypeEnum type,
            string message,
            object dataObject = null,
            UserInfoDTO userInfo = null,
            long? accountID = null,
            long? credentialID = null,
            long? account_ReviewUrlID = null,
            long? reviewID = null
        );

        /// <summary>
        /// gets a batch of audits to process for the audit processor
        /// </summary>
        List<AuditDTO> GetBatchOfAuditsToProcess();

        /// <summary>
        /// Sets the audits identified by the IDs passed in to processed
        /// </summary>
        void SetAuditsAsProcessed(List<long> auditIDs);

		/// <summary>
		/// Get Exception log for UI
		/// </summary>
		List<ExceptionLogDTO> GetExceptions(int skip, int take, ref int totalCount);

        /// <summary>
        /// Gets a handler for a given type of audit
        /// </summary>
        AuditHandlerBase GetAuditHandler(AuditTypeEnum type);
    }
}
