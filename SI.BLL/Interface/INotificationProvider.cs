﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SI.DAL;
 
using SI.DTO;

namespace SI.BLL.Interface
{
    public interface INotificationProvider
    {

        /// <summary>
        /// Queue Notifications 
        /// Insert a record in Notification Table.
        /// </summary>
        void QueueNotification(NotificationMessageTypeEnum Type, long EntityID, long AccountID);

        /// <summary>
        /// Queues a batch of notifications
        /// Insert records in Notification Table.
        /// </summary>
        void QueueNotifications(List<QueueNotificationDataDTO> items);

        void ProcessNotifications();

        /// <summary>
        /// Gets a list of notifications for the globe dropdown
        /// </summary>
        List<NotificationSummaryDTO> GetGlobeNotificationList(UserInfoDTO userInfo);

        /// <summary>
        /// Gets a list of notifications for the notifications screen
        /// </summary>
        List<NotificationsAlertDTO> GetNotificationList(UserInfoDTO userInfo);


		/// <summary>
		/// send out the email to user to reset their password
		/// </summary>
		/// <param name="fullName"></param>
		/// <param name="emailAddress"></param>
		/// <param name="token"></param>
		/// <param name="accessUrl"></param>
		/// <returns></returns>
	    bool SendResetPasswordEmail(string fullName, string emailAddress, string accessUrl);

        /// <summary>
        /// Subscribes ( or unsubscribes ) a user to certain notifications for a given account
        /// </summary>
        /// <param name="userInfo">The user being subscribed or unsubscribed</param>
        /// <param name="accountID">The account ID to subscribe or unsubscribe to</param>
        /// <param name="messageType">The message type</param>
        /// <param name="methods">The list of notification method to subscribe to</param>
        /// <param name="resellerID">The reseller to use for choosing templates</param>
        /// <param name="schedule">Teh schedule in which the notifications will be sent</param>
        void SetNotificationMessageTypeSubscriptions(
            long userID, long accountID,
            NotificationMessageTypeEnum messageType, List<NotificationMethodEnum> methods,
            long resellerID, ScheduleSpecDTO schedule
        );

        /// <summary>
        /// Applies the default subscription array for all of the authorized users for a given account
        /// </summary>
        void ApplyDefaultNotificationSubscriptionsForAccount(long accountID);

        /// <summary>
        /// Applies the default subscription array for all of the accounts for a given user
        /// </summary>        
        void ApplyDefaultNotificationSubscriptionsForUser(long userID);

        /// <summary>
        /// Gets all pending notification subscription checks, and removes them from the table
        /// </summary>
        List<PendingNotificationSubscriptionCheckDTO> GetPendingNotificationSubscriptionChecks();

        /// <summary>
        /// Queues a subscription check for a given user or account
        /// </summary>
        void AddSubscriptionCheck(long? userID, long? accountID);

        void GenerateInitialTargetHashes();

        /// <summary>
        /// Gets all notification message types that a user is or can be subscribed to
        /// </summary>
        List<UserNotificationTypeInfoDTO> GetAvailableNotificationsForUser(long userID);

        /// <summary>
        /// For a given user, adjust the enabled state of the list passed in
        /// </summary>
        void SetUserNotificationStates(UserInfoDTO userInfo, long userIDtoChange, List<SetNotificationStateDTO> states);

        /// <summary>
        /// Retrieves the most recent emailed notifications sent to a given user
        /// </summary>
        /// <param name="maxItems">The maximum number of items to return</param>
        List<RecentNotificationItemDTO> GetMostRecentEmailNotificationsForUser(long userID, int maxItems);

        /// <summary>
        /// Gets the body text for a given notification target
        /// </summary>
        string GetBodyTextForNotificationTargetID(long notificationTargetID);

        /// <summary>
        /// Sends an auto-approval notification to the user who auto-approved a post.
        /// </summary>
        void SendAutoApprovalNotification(long postApprovalID);
    }
}
