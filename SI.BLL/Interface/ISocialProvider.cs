﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
//using Connectors.Entities;
using SI.DTO;
using Facebook.Entities;
using Google.Model;

namespace SI.BLL.Interface
{
    public interface ISocialProvider
    {
		/// <summary>
		/// Gets the HTML of the Link entered in a Post
		/// </summary>
	    LinkPreviewDTO GetLinkPreview(string url);

		/// <summary>
		/// Gets the SocialApps that can be used for a given Reseller
		/// </summary>
        SocialConfigDTO GetSocialConfig(UserInfoDTO userInfo, long resellerID, SocialNetworkEnum network);        
		
		/// <summary>
		/// Gets the Credentials for an account
		/// </summary>
		List<SocialCredentialDTO> GetAccountSocialCredentials(UserInfoDTO userInfo, long accountId);
	    
		/// <summary>
        /// Gets the Credentials
		/// </summary>
        List<SocialCredentialDTO> GetSocialCredentials();
        
        /// <summary>
        /// Gets a list of credentials, only valid credentials will be returned
        /// </summary>
        List<SocialCredentialDTO> GetSocialCredentials(SocialNetworkEnum? socialNetworkID, long? accountID, bool onlyValidAndActive);
        
		/// <summary>
		/// Gets all of the Social Networks config in the System
		/// </summary>
		List<SocialNetworkDTO> GetSocialNetworks();

        /// <summary>
        /// Gets all of the Syndicators in the System
        /// </summary>
        List<SyndicatorsDTO> GetSyndicators();

        /// <summary>
        /// Gets all of the FacebookPostIDs
        /// </summary>
        List<long> GetFacebookPostIDs();

        /// <summary>
        /// Gets all of the FacebookPostIDs
        /// </summary>
        List<long> GetFacebookPostIDsNeedingStatsUpdate();

        /// <summary>
        /// Gets all of the GooglePlusPostIDs
        /// </summary>
        List<long> GetGooglePlusPostIDsNeedingStatsUpdate();

        /// <summary>
        /// Sets the DateLastRefreshed date on a FacebookPost to now.
        /// </summary>
        void MarkFacebookPostRefreshed(long facebookPostID);


        /// <summary>
        /// Sets the DateLastRefreshed date on a GooglePlusPost to now.
        /// </summary>
        void MarkGooglePlusPostRefreshed(long googlePlusPostID);

        /// <summary>
        /// Gets all of the TwitterPostIDs
        /// </summary>
        List<long> GetTwitterPostIDs();

        
		/// <summary>
		/// Saves the Credentials for a given SocialApp
		/// </summary>
		//bool SaveSocialConnection(UserInfoDTO userInfo, long credentialId, long accountId, long socialappid, string token, string tokenSecret, string id, string url, string pictureURL = "", string screenName = "");

        /// <summary>
        /// Saves or updates a social connection
        /// </summary>
        SaveEntityDTO<SocialConnectionInfoDTO> Save(SaveEntityDTO<SocialConnectionInfoDTO> dto);

        /// <summary>
        /// Update SocialConnection
        /// </summary>        
        bool UpdateSocialConnection(long credentialId, bool isValid);

		/// <summary>
		/// Disconnections/Flags Inactive a Credential
		/// </summary>
        bool DisconnectionSocialConnection(UserInfoDTO userInfo, long configId, long accountId);

		/// <summary>
		/// Gets the Credentials used for refreshing a token
		/// </summary>
        SocialRefreshDTO GetSocialRefresh(UserInfoDTO userInfo, long credentialId, long accountId);

		/// <summary>
		/// Flag a Credential as Active and Valid
		/// </summary>
        bool UpdateValidCredential(UserInfoDTO userInfo, long credentialID, long accountID);
		
        /// <summary>
        /// Saves a new post, or saves changes to an existing post
        /// </summary>
        SaveEntityDTO<PostDTO> Save(SaveEntityDTO<PostDTO> dto);

        /// <summary>
        /// Gets a post given a postID
        /// </summary>
        GetEntityDTO<PostDTO> GetPostByID(UserInfoDTO userInfo, long postID, bool openingForEdit);

        /// <summary>
        /// Revise a post at the account level
        /// </summary>
        SaveEntityDTO<RevisePostDTO> SavePostRevisionForAccount(SaveEntityDTO<RevisePostDTO> revision);

        /// <summary>
        /// Gets a post for revision at the account level.
        /// </summary>
        GetEntityDTO<RevisePostDTO> GetPostForRevision(UserInfoDTO userInfo, long postID, long accountID);

        /// <summary>
        /// Gets a list of the potential accounts/social networks to be selected from on the publish page.
        /// </summary>
        PublishAccountNetworkListSearchResultDTO GetPublishAccountNetworkListItems(
            UserInfoDTO userInfo, PublishAccountNetworkListSearchRequestDTO req
        );


        /// <summary>
        /// Sets the jobid for a given post image and target
        /// </summary>
        void SetPostImageJobID(long postImageID, long postTargetID, long jobID);

        /// <summary>
        /// Sets the jobid for a given post target
        /// </summary>
        void SetPostTargetJobID(long postTargetID, long jobID);

        /// <summary>
        /// Sets the result information for a given post target
        /// </summary>
        void SetPostTargetResults(long postTargetID, string resultID, string feedID);

        /// <summary>
        /// Sets the result information for a given post target
        /// </summary>
        void SetPostTargetResults(long postTargetID);

        /// <summary>
        /// Sets the result information for a given post target
        /// </summary>
        void SetPostImageResults(long postImageID, long postTargetID, string resultID, string feedID);

        /// <summary>
        /// Sets a failed status on a given post target
        /// </summary>
        void SetPostTargetFailed(long postTargetID);

        /// <summary>
        /// Sets a failed status on a given post postImageID and postTargetID
        /// </summary>
        void SetPostImageResultFailed(long postImageID, long postTargetID);


        /// <summary>
        /// Update Credential
        /// </summary>        
        bool UpdateCredential(long credentialID, string screenName, string URL, string PictureURL = "");

        /// <summary>
        /// Returns information required by the processor to handle a publishing action
        /// </summary>
        /// <param name="postTargetID">The post target to get info for</param>
        /// <param name="postImageID">If null, post target information.  If set, this joins in the PostImage record</param>
        PostTargetPublishInfoDTO GetPostTargetPublishInfo(long postTargetID, long? postImageID);


        /// <summary>
        /// Returns information required by the processor to handle a publishing action
        /// </summary>        
        List<PostImageResultDTO> GetPostImageResulInfo(long postTargetID);

        /// <summary>
        /// Get the Post PostImage by postID
        /// </summary>        
        PostImageDTO GetPostImageInfo(long postID);


        /// <summary>
        /// Returns information for a credential and its social app
        /// </summary>
        SocialCredentialDTO GetSocialCredentialInfo(long credentialID);

        /// <summary>
        /// Given a post and account, this routine will queue jobs to post to any number of targets for that account.
        /// </summary>
        void QueuePost(long postID, long accountID);

        /// <summary>
        /// Gets a list of posts to queue publish jobs for
        /// </summary>
        List<PostToQueueDTO> GetPostsToQueue();

        /// <summary>
        /// Searches for posts
        /// </summary>
        PostSearchResultsDTO PostSearch(UserInfoDTO userInfo, PostSearchRequestDTO req);

        /// <summary>
        /// Searches for post targets
        /// </summary>
        PostTargetSearchResultsDTO PostTargetSearch(UserInfoDTO userInfo, PostTargetSearchRequestDTO req);

		/// <summary>
		/// Save the raw Facebook Page from Integrator
		/// </summary>
		bool SaveFaceBookPage(Page facebookPageNew, long credentialId);

		/// <summary>
		/// Save the raw Facebook Post from Integrator
		/// </summary>
        bool SaveFaceBookPost(PostStatistics facebookPost, long credentialId, string feedId, string resultId, long FacebookPostID);

        /// <summary>
        /// Save the Facebook Post to FacebookPost Table
        /// </summary>        
        bool SaveFaceBookPost(long credentialId, string feedId, string resultId, string name, string message, PostTypeEnum type, string linkURL, string pictureURL, DateTime FBCreatedTime, DateTime FBUpdatedTime, string statusType);

        /// <summary>
        /// Save the GooglePlus Post to FacebookPost Table
        /// </summary>        
        bool SaveGooglePlusPost(long credentialId, List<GooglePlusPostDTO> listgooglePlusPostDTO);

        /// <summary>
        /// Save the GooglePlus Post to FacebookPost Table
        /// </summary>        
        bool SaveGooglePlusPost(long credentialId, string resultId, string googlePlusPostID, string name, string message, PostTypeEnum type, string linkURL, string pictureURL, DateTime GooglePlusCreatedTime, DateTime GooglePlusUpdatedTime);

	    /// <summary>
	    /// Save the raw Facebook Post Insights from Integrator
	    /// </summary>
        bool SaveFaceBookInsights(Insights facebookInsights, long credentialId, DateTime queryDate, long? FacebookPostID);

        /// <summary>
        /// Save the Twitter Page Information from Integrator
        /// </summary>        
        bool SaveTwitterPage(long credentialId, int statuses_count, int followingCount, int followers_count, int listed_count, int favourites_count, int friends_count, int retweet_count, string error);

        /// <summary>
        /// Save the GooglePlus Page Information from Integrator
        /// </summary>        
        bool SaveGooglePlusPage(long credentialId, int PlusOneCount, int FollowerCount, int PostCount, string error);

        /// <summary>
        /// Save the GooglePlus Post Statistics from Integrator
        /// </summary>        
        bool SaveGooglePlusPostStatistics(GooglePlusPostStatistics googlePlusPostStatistics, long GooglePlusPostID);

        /// <summary>
        /// Save the Twitter Post Statistics from Integrator
        /// </summary>        
        bool SaveTwitterPostStatistics(long PostTargetID, bool? IsFavorited, int? RetweetCount, bool IsRetweeted, string Error);

        /// <summary>
        /// Get FacebookPostInfoDTO By FacebookPostID
        /// </summary>        
        FacebookPostInfoDTO GetFacebookPostInfo(long FacebookPostID);

        /// <summary>
        /// Get GoolgePlusPostInfoDTO By GooglePlusPostID
        /// </summary>        
        GooglePlusPostInfoDTO GetGooglePlusPostInfo(long GooglePlusPostID);
            
		/// <summary>
		/// Save Twitter tokens from syndication which gets the account id from the user and checks for existing credentials for the account
		/// </summary>
		//bool SaveTwitterConnectionFromSyndication(UserInfoDTO userInfo, long? socialAppId, string token, string tokenSecret, string userId, string webSite, string pictureUrl, string screenName, Guid accessToken);

		/// <summary>
		/// Save Facebook tokens from syndication which gets the account id from the user and checks for existing credentials for the account
		/// </summary>
        //bool SaveSocialConnectionFromSyndication(UserInfoDTO userInfo, long? socialappid, string pageToken, string pageId, string screenName, string website, string pictureURL, Guid accessToken);

        /// <summary>
        /// Gets the available modes for publishing based upon the user and current context account
        /// </summary>
        List<PublishModeDTO> GetPublishModes(UserInfoDTO userInfo);

        /// <summary>
        /// Gets a list of pending approvals for a given user
        /// </summary>
        List<PendingApprovalInfoDTO> GetPendingApprovals(UserInfoDTO userInfo);

        /// <summary>
        /// Submits a response to an approval notification
        /// </summary>
        ApprovalResponseResultDTO SubmitApprovalResponse(UserInfoDTO userInfo, long postApprovalID, ApprovalResponseEnum result);

        void InitPostApprovals(long postID, long? setUserIDAlreadyApproved);

        /// <summary>
        /// Get Dashboard Location SocialData
        /// </summary>        
        List<DashboardSocialLocationDTO> GetDashBoardSocialLocationData(UserInfoDTO userInfo, bool ShowConnected = false, bool ShowDisconnected = false, bool ShowNeverConnected = false);

        /// <summary>
        /// Mark PostTarget AS Deleted
        /// </summary>        
        bool MarkPostTargetDeleted(UserInfoDTO userInfo, long PostTargetID);

        /// <summary>
        /// Queues a job that will result in all of the post's targets being 
        /// marked deleted and removed, where possible, from the social networks 
        /// they were published to
        /// </summary>
        void DeletePost(UserInfoDTO userInfo, long postID);

        /// <summary>
        /// To be called from processor, NOT from UI.  Processed a post for deletion, including
        /// queueing the post target deletion jobs
        /// </summary>
        void ProcessPostDeletion(UserInfoDTO userInfo, long postID, long jobID);

        /// <summary>
        /// To be called from processor, NOT from UI.  Processed a post target deletion.
        /// </summary>
        void ProcessPostTargetDeletion(UserInfoDTO userInfo, long postTargetID, long? postImageResultID, long jobID);

        /// <summary>
        /// Mark PostImageResult AS Deleted
        /// </summary>        
        bool DeletePostImageResult(UserInfoDTO userInfo, long PostImageResultID);

        /// <summary>
        /// Processes any queued post approver inits
        /// </summary>
        void ProcessPostApprovalInits();

        /// Set Auto Approval
        /// </summary>        
        bool SaveAutoApproval(UserInfoDTO userInfo, long syndicatorID, long AccountID, long SocialNetworkID);
        
        /// <summary>
        /// Remove/Delete Auto Approval
        /// </summary>        
        bool RemoveAutoApproval(UserInfoDTO userInfo, long syndicatorID, long AccountID, long SocialNetworkID);

        /// <summary>
        /// Tokenizes a message
        /// </summary>
        string TokenizeMessage(string message, string AccountName, string AccountPhone, string AccountURL);

        /// <summary>
        /// SocialDashboardData_facebook
        /// </summary>       
        List<SocialDashboardDataFacebook> GetSocialDashboardData_facebook(UserInfoDTO userInfo, DateTime startDate, DateTime endDate);

        /// <summary>
        /// GetSocialFacebookPostData
        /// </summary>
        List<SocialFacebookPostData> GetSocialFacebookPostData(UserInfoDTO userInfo, DateTime startDate, DateTime endDate);


        /// <summary>
        /// GetSocialTwitterPostData
        /// </summary>
        List<SocialTwitterPostData> GetSocialTwitterPostData(UserInfoDTO userInfo, DateTime startDate, DateTime endDate);

        /// <summary>
        /// GetSocialDashboardData Twitter
        /// </summary>
        List<SocialDashboardDataTwitter> GetSocialDashboardData_Twitter(UserInfoDTO userInfo, DateTime startDate, DateTime endDate);

        /// <summary>
        /// Unsubscribe from Syndication Pending Approval Email
        /// </summary>
		bool SetSyndicationUnsubscribe(SyndicationUnsubscribeDTO accessTokenBaseDto);

        /// <summary>
        /// Process Facebook Historic Data
        /// </summary>
        void ProcessFacebookHistoricData(long AccountID, string startDate, string endDate);

        /// <summary>
        /// Gets summary information about a post
        /// </summary>
        GetEntityDTO<PostSummaryDTO> GetPostSummary(UserInfoDTO userInfo, long postID);

        void TestQueuerForPostAndAccount(long postID, long accountID);

        List<FBInsightDateDTO> GetDatesforFaceBookInsightstoBackFill();

        string GetPageAccessTokenByUserAccessToken(string UniqueID, string Token);

        bool UpdateSocialConnectionToken(long credentialId, string Token);
	    
		/// <summary>
		/// Get the franchises for the account config
		/// </summary>
		List<FranchiseTypeDTO> GetAccountFranchises(long accountID);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="CredentialID"></param>
        void DeleteSocialData(long CredentialID);

        /// <summary>
        /// GetPostApprovals
        /// </summary>                
        PostApprovalResultsDTO GetPostApprovals(UserInfoDTO userInfo, PostApprovalHistoryRequestDTO req);

    }
}
