﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SI.DTO;

namespace SI.BLL
{
    public interface IPlatformNotification
    {
        
        /// <summary>
        /// Send Email Notification for New User Created 
        /// </summary>      
        void SendNewUserEmail(long userID, UserInfoDTO userInfo);

        /// <summary>
        /// SendReviewDiagnosticsEmail
        /// </summary>
        void SendReviewDiagnosticsEmail();

        /// <summary>
        /// Send Syndication Signup Email
        /// </summary>
        void SendSyndicationSignupEmail(Guid accessToken, SocialNetworkEnum socialNetwork);

        /// <summary>
        /// Send Enrollment Welcome Email
        /// </summary>
        void SendEnrollmentWelcomeEmail(Guid accessToken);

	    /// <summary>
	    /// Send Review Email to User
	    /// </summary>
	    /// <param name="reviewShareEmail"></param>
	    /// <param name="userID"></param>
	    /// <returns>True if sent</returns>
	    bool SendReviewShareEmail(ReviewShareEmailDTO reviewShareEmail, long userID);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="socialCredentialsDTO"></param>
        void SentTokenValidationEmail(SocialCredentialDTO socialCredentialsDTO);        
    }
}
