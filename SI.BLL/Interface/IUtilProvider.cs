﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SI.BLL.Interface
{
    public interface IUtilProvider
    {
        List<string> GetFilteredWords();

        string SerializeObject<T>(T obj);

        string Trim(string xml);

        T FromXml<T>(String xml);

        void RefreshTimezoneDatabase();
    }
}
