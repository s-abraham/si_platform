﻿using SI.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SI.BLL
{
    public interface ISecurityProvider
    {
        #region users

        /// <summary>
        /// Retrieves the "system" user
        /// </summary>
        long GetSystemUserID();

        /// <summary>
        /// Retrieves a user based upon the user's ID
        /// </summary>
        /// <param name="userID">The user's ID</param>
        /// <returns>A user DTO or null if not found</returns>
        UserDTO GetUserByID(long userID, UserInfoDTO userInfo);

        /// <summary>
        /// Gets basic user info by a user's ID
        /// </summary>
        BasicUserInfoDTO GetBasicUserInfoByID(long userID);

        /// <summary>
        /// Retrieves a user based upon the user's email address
        /// </summary>
        /// <param name="emailAddress">The email address of the user to search for</param>
        /// <returns>A user DTO or null if not found</returns>
        UserDTO GetUserByEmailAddress(string emailAddress, UserInfoDTO userInfo);

        /// <summary>
        /// Retrieves a user based upon the user's username
        /// </summary>
        /// <param name="username">The username of the user to search for</param>
        /// <returns>A user DTO or null if not found</returns>
        UserDTO GetUserByUsername(string username, UserInfoDTO userInfo);

        /// <summary>
        /// Get users email from username, for emailing 
        /// </summary>
        /// <param name="userName"></param>
        /// <returns>email address</returns>
        UserDTO GetUserForEmailByUsername(string userName);

        /// <summary>
        /// Saves a new user.
        /// </summary>
        CreateUserResultDTO SaveUser(UserDTO user, UserInfoDTO userInfo, bool skipApplyRoles, bool skipReloadTree);

        /// <summary>
        /// Sets the account context for a given user
        /// </summary>
        void SetUserCurrentContextAccountID(UserInfoDTO userInfo, long accountID);

        /// <summary>
        /// Unlocks a user account...
        /// </summary>
        /// <param name="userInfo">The user info of the user attempting to unlock the account</param>
        /// <param name="userID">The id of the user to unlock</param>
        void UnlockUserAccount(UserInfoDTO userInfo, long userID);

        /// <summary>
        /// Adds a role to a user for a given account.  If a role already exists, it will be replaced by this new role.
        /// In this way, if you wish to change whether a role includes children, call this routine with the new value.
        /// </summary>
        RoleChangeResultDTO AddRole(UserInfoDTO userInfo, long userID, long accountID, long roleTypeID, bool includeChildren, bool skipApplyRoles, bool skipReloadTree);

        /// <summary>
        /// Removes a permission
        /// </summary>
        /// <param name="permissionID">The id of the permission to be removed</param>
        RoleChangeResultDTO RemoveRole(UserInfoDTO userInfo, long roleID);


        /// <summary>
        /// Validates a users credentials and returns a security context if validated.
        /// </summary>
        /// <param name="username">The user's username</param>
        /// <param name="password">The user's cleartext password</param>
        /// <returns>The result of the validation attempt and security context if successful</returns>
        ValidateUserResultDTO ValidateUser(string username, string password);

        /// <summary>
        /// Assumes user is already authenticated, and retrieves security context
        /// </summary>
        /// <returns>The security context if the user is found</returns>
        ValidateUserResultDTO ValidateUser(UserInfoDTO userInfo);

        /// <summary>
        /// Validates a user based on a short-lived SSO security token
        /// </summary>
        /// <param name="accessToken">A guid access token carried on the query string of the special SSO login url</param>
        ValidateUserResultDTO ValidateUser(Guid accessToken);

        /// <summary>
        /// Creates an access token for a given user that expires in a specified number of minutes
        /// </summary>
        /// <param name="resellerSecretKey">The secret key of the reseller requesting the key</param>
        /// <param name="userSecretKey">The secret key of the user to be granted an access token for</param>
        /// <param name="expiresInMinutes">The number of minutes the access token will be good for</param>
        /// <returns>A new Access Token, or null if the request is invalid</returns>
        Guid CreateSSOAccessToken(Guid resellerSecretKey, Guid userSecretKey, int expiresInMinutes);

        /// <summary>
        /// Saves a user setting
        /// </summary>
        /// <param name="userID">The Id of the user</param>
        /// <param name="key">The key of the setting to save</param>
        /// <param name="value">The value of the setting to save.  If null, the setting will be deleted.</param>
        void SaveUserSetting(UserInfoDTO userInfo, string key, string value);

        /// <summary>
        /// Retrieves a user setting, or null if it does not exist
        /// </summary>
        string GetUserSetting(UserInfoDTO userInfo, string settingKey);

        /// <summary>
        /// Changes an existing user to be a member of a different account than they currently are
        /// </summary>
        ChangeUsersMemberAccountResultDTO ChangeUserMemberOfAccount(UserInfoDTO userInfo, long userID, long newMemberOfAccountID, long roleTypeID, bool cascadeRole);

        /// <summary>
        /// Changes an existing user to be a member of a different account than they currently are.
        /// The user will have the same role at the new account that they had at the previous account.  Cascading will be set to false.
        /// </summary>
        ChangeUsersMemberAccountResultDTO ChangeUserMemberOfAccount(UserInfoDTO userInfo, long userID, long newMemberOfAccountID);

        /// <summary>
        /// Change User Password
        /// </summary>
        /// <param name="userInfoDto"></param>
        /// <param name="password">Clear text password</param>
        /// <returns>ValidateUserOutcome</returns>
        ValidateUserOutcome ChangeUserPassword(UserInfoDTO userInfoDto, string password, string currentPassword);

        /// <summary>
        /// Searches for user records
        /// </summary>
        UserSearchResultDTO SearchUsers(UserSearchRequestDTO request);

        /// <summary>
        /// Retrieves a general access token, and optionally logs a use against it
        /// </summary>
        /// <param name="token">GUID of the token to look up</param>
        /// <param name="logUse">Logs a use against the token by decrementing its RemainngUses field if not null</param>
        AccessTokenBaseDTO GetAccessToken(Guid token, bool logUse);

        /// <summary>
        /// Creates a new access token.  Guid passed in will be ignored and a new guid will be generated for the 
        /// token when created.
        /// </summary>
        /// <param name="accessToken"></param>
        /// <returns></returns>
        AccessTokenBaseDTO CreateAccessToken(AccessTokenBaseDTO accessToken);

        /// <summary>
        /// Gets a list of all user ids in the system...
        /// </summary>
        List<long> GetAllUserIDs();

        /// <summary>
        /// If logged in user has appropriate permission, set a user to active or inactive
        /// </summary>
        /// <param name="loggedInUser">The logged in user making the change</param>
        /// <param name="userID">The ID of the user to sret active or inactive</param>
        /// <param name="active">If true, activate user, otherwise deactivate user</param>
        void SetUserActiveState(UserInfoDTO loggedInUser, long userID, bool active);

        /// <summary>
        /// Getsa reseller for a given account.
        /// </summary>
        ResellerDTO GetResellerByAccountID(UserInfoDTO userInfo, long accountID, TimeZoneInfo tz);

        /// <summary>
        /// GetUserActivityByUserID
        /// </summary>        
        List<UserActivityDTO> GetUserActivityByUserID(long UserID, int NumberOfDaysBack, UserInfoDTO userInfo);

        /// <summary>
        /// Determines if a logged in user can impersonate another user
        /// </summary>
        bool CanImpersonateUser(UserInfoDTO userInfo, long userIDToImpersonate);

        #endregion

        #region role types


        /// <summary>
        /// Returns a list of all role types
        /// </summary>        
        List<RoleTypeDTO> GetRoleTypes(UserInfoDTO userInfo);

        /// <summary>
        /// Creates or saves changes to a role type
        /// </summary>        
        void SaveRoleType(RoleTypeDTO dto);

        /// <summary>
        /// Deletes a type of role from the system, this will remove this type of role from 
        /// all users and accounts in the process.
        /// </summary>        
        void DeleteRoleType(long roleTypeID);

        #endregion

        #region permission types

        //PermissionTypeDTO GetPermissionType(string permissionCode);
        PermissionTypeDTO GetPermissionType(PermissionTypeEnum type);

        #endregion

        #region utility stuff

        void FlushAccountAndPermissionCache();

        #endregion

        #region accounts

        /// <summary>
        /// Returns the account identified by it's ID
        /// </summary>
        /// <param name="accountID">The ID of the account to retrieve.</param>
        /// <returns>An account DTO Object</returns>
        GetEntityDTO<AccountDTO, AccountOptionsDTO> GetAccountByID(UserInfoDTO userInfo, long accountID);

        long? GetAccountIDByEdmundsDealershipID(long edmundsDealershipID);

        /// <summary>
        /// Returns an account with the name provided
        /// </summary>
        /// <param name="accountName">The name of the account to search for</param>
        /// <returns>An Account DTO</returns>
        GetEntityDTO<AccountDTO, AccountOptionsDTO> GetAccountByName(UserInfoDTO userInfo, string accountName);

        /// <summary>
        /// Returns an unsaved defaulted account.
        /// </summary>
        /// <param name="userID">The ID of the user requesting a new account</param>
        /// <param name="parentAccountID">The optional parent that this account will belong to</param>
        /// <returns>A blank account object with defaults filled in</returns>
        GetEntityDTO<AccountDTO, AccountOptionsDTO> GetNewAccount(UserInfoDTO userInfo, long? parentAccountID);

        /// <summary>
        /// Creates or saves changes to an account.
        /// </summary>
        /// <param name="account">An account DTO</param>
        /// <returns>The outcome of the create or save operation.</returns>
        SaveEntityDTO<AccountDTO> Save(SaveEntityDTO<AccountDTO> dto);

        /// <summary>
        /// Searches for account records
        /// </summary>
        AccountSearchResultDTO SearchAccounts(AccountSearchRequestDTO request);

        /// <summary>
        /// The same as Search Accounts, but a list for only publishing
        /// </summary>
        AccountSearchResultDTO SearchAccountsForPublish(AccountSearchRequestDTO request);

        /// <summary>
        /// Searches for accounts where the user has virtual group admin permission
        /// </summary>
        AccountSearchResultDTO SearchAccountsForVirtualGroupAdmin(AccountSearchRequestDTO request);

        /// <summary>
        /// Get List of the User that has access for given account
        /// </summary>
        /// <param name="accountID"></param>
        /// <returns></returns>
        List<UserAccountAccessListItemDTO> GetUserAccessListByAccountID(long accountID, UserInfoDTO userInfo);

        /// <summary>
        /// gets the name of an account by its ID
        /// </summary>
        string GetAccountNameByID(long accountID);

        /// <summary>
        /// Changes the parent of an existing account
        /// </summary>        
        /// <param name="accountID">The id of the account in which to change the parent of</param>
        /// <param name="newParentAccountID">The ID of the new parent account</param>
        /// <returns></returns>
        ChangeParentAccountResultDTO ChangeParentAccount(UserInfoDTO userInfo, long accountID, long newParentAccountID, bool skipChecks);

        /// <summary>
        /// Removes an account from the current parent, and adds it to the first reseller found in the ancestors of the
        /// previous parent.
        /// </summary>
        ChangeParentAccountResultDTO DetachAccount(UserInfoDTO userInfo, long accountID);

        /// <summary>
        /// Gets a list of account ids that are descendants of the provided account id
        /// </summary>
        List<long> GetAccountDescendants(long accountID);

        /// <summary>
        /// Returns an account with the name provided
        /// </summary>
        /// <param name="accountName">The name of the account to search for</param>
        /// <returns>An Account DTO</returns>
        GetEntityDTO<AccountDTO, AccountOptionsDTO> GetAccountByNameForImport(UserInfoDTO userInfo, string accountName);

        /// <summary>
        /// GetAccountAutoApproveStatus
        /// </summary>        
        List<AccountAutoApproveStatusDTO> GetAccountAutoApproveStatus(UserInfoDTO userInfo, long accountID);

        /// <summary>
        /// AddRemoveAccountAutoApproval
        /// </summary>        
        bool AddRemoveAccountAutoApproval(UserInfoDTO userInfo, long PostAutoApprovalID, long SyndicatorID, long ApprovingUserID, long AccountID, long SocialNetworkID, bool isDisabled);

        #endregion

        #region addresses

        ///// <summary>
        ///// Retrieves an address by it's ID
        ///// </summary>
        ///// <param name="addressID">The ID of the address to retrieve</param>
        ///// <returnsAn Address DTO></returns>
        //AddressDTO GetAddressByID(long addressID);

        ///// <summary>
        ///// Creates or saves an address.
        ///// </summary>
        ///// <param name="address">The address DTO to create or save.</param>
        ///// <returns>Teh outcome of the operation.</returns>
        //SaveEntityDTO<AddressDTO> Save(SaveEntityDTO<AddressDTO> address);

        #endregion

        #region menus

        /// <summary>
        /// Returns a menu structure for use in the web site
        /// </summary>
        /// <param name="userID">The ID of the user who is logged in</param>
        /// <param name="baseURL">The base URL of the site</param>
        /// <param name="membersPath">The members base path for the site</param>
        /// <returns>A list of menu items</returns>
        List<MenuItemDTO> GetMenuItems(long userID, string baseURL, string membersPath);

        #endregion

        #region permissions and the account tree

        /// <summary>
        /// Determines if a user has a given permission for a given account
        /// </summary>
        /// <param name="userID">The user to check.</param>
        /// <param name="accountID">The account to check.</param>
        /// <param name="type">The permission type the user must have.</param>
        /// <returns>True if the user has the permission for the specified account.</returns>
        bool HasPermission(long userID, long accountID, PermissionTypeEnum type);

        /// <summary>
        /// Determines if a user has a given permission for any account in the system.
        /// </summary>
        /// <param name="userID">The user to check.</param>
        /// <param name="type">The type of permission to check.</param>
        /// <returns>True if there exists any account where the given user has the specified user.</returns>
        bool HasPermission(long userID, PermissionTypeEnum type);

        /// <summary>
        /// Returns every account id that the given user possesses the given permission for
        /// </summary>
        List<long> AccountsWithPermission(long userID, PermissionTypeEnum type);

        /// <summary>
        /// Determines if a given account has a given feature
        /// </summary>
        bool HasFeature(long accountID, FeatureTypeEnum featureType);

        /// <summary>
        /// Gets a list of all account ids that have a given feature
        /// </summary>
        List<long> GetAccountsWithFeature(FeatureTypeEnum featureType);

        /// <summary>
        /// Gets a list of userid, permissiontype pairs for a given account
        /// </summary>
        /// <param name="accountID">The account id to test</param>
        /// <param name="permissionType">The permission type granted</param>
        /// <param name="includeSuperUsers">Include super user derived permissions in the results.</param>
        /// <returns></returns>
        List<AccountPermissionDTO> UsersWithAccountPermission(long accountID, PermissionTypeEnum? permissionType, bool includeSuperUsers);



        #endregion

        #region competitors


        /// <summary>
        /// Add competitor.
        /// </summary>
        /// <param name="accountID">The ID of the account</param>
        /// <param name="CompetitorAccountID">Competitor Account ID.</param>
        SaveEntityDTO<bool> AddCompetitor(long accountID, long competitoraccountID, int maxcompetitor, UserInfoDTO UserInfo);

        /// <summary>
        /// Remove competitor.
        /// </summary>
        /// <param name="ID">Record ID</param>
        SaveEntityDTO<bool> RemoveCompetitor(long ID, UserInfoDTO UserInfo);

        #endregion

        #region miscellaneous

        /// <summary>
        /// Reloads the cached account tree and also calls spApplyRoles
        /// </summary>
        void ApplyRolesAndReloadTree();

        /// <summary>
        /// NEVER CALL THIS!!!!!
        /// This resets all of the data in the database
        /// </summary>
        void ResetDatabase();

        #endregion

        #region preferences
        
        /// <summary>
        /// Gets user preferences for the given user
        /// </summary>
        UserPreferencesDTO GetUserPreferences(UserInfoDTO userInfo);

        /// <summary>
        /// Saves a given user's preferences
        /// </summary>
        void SaveUserPreferences(UserInfoDTO userInfo, UserPreferencesDTO preferences);

        #endregion

        #region virtual groups

        /// <summary>
        /// Gets a list of virtual group types
        /// </summary>
        List<VirtualGroupTypeDTO> GetVirtualGroupTypes();

        /// <summary>
        /// Saves a virtual group type, or creates a new type
        /// </summary>
        SaveEntityDTO<VirtualGroupTypeDTO> Save(SaveEntityDTO<VirtualGroupTypeDTO> entity);

        /// <summary>
        /// Gets a list of virtual groups that a user had access to
        /// </summary>
        List<VirtualGroupDTO> GetVirtualGroups(UserInfoDTO userInfo);

        /// <summary>
        /// Saves a virtual group or creates a new group
        /// </summary>
        SaveEntityDTO<VirtualGroupDTO> Save(SaveEntityDTO<VirtualGroupDTO> entity);

        /// <summary>
        /// Add accounts to a virtual group
        /// </summary>
        VirtualGroupActionResultDTO VirtualGroupAddAccounts(UserInfoDTO userInfo, long virtualGroupID, List<long> accountIDs);

        /// <summary>
        /// Remove accounts from a virtual group
        /// </summary>
        VirtualGroupActionResultDTO VirtualGroupRemoveAccounts(UserInfoDTO userInfo, long virtualGroupID, List<long> accountIDs);

        /// <summary>
        /// Gets all account IDs from a virtual group where the user has "account view" permission.
        /// </summary>
        List<long> GetVirtualGroupAccountIDs(UserInfoDTO userInfo, long virtualGroupID);

        #endregion

        #region SendGrid

        SendGridEmailFailureDTO SaveSendgridEmailFailures(string emailAddress, SendGridStatusEnum sendGridStatus, DateTime dateCreated);

        SendGridEmailFailureDTO SetSendgridEmailProcessingStatus(string emailAddress, SendGridStatusEnum sendGridStatus, SendGridProcessingStatusEnum processingStatus);

        /// <summary>
        /// Gets all sendgrid email failures having a specific processing status
        /// </summary>
        List<SendGridEmailFailureDTO> GetSendGridEmailFailures(SendGridProcessingStatusEnum processingStatus);

        /// <summary>
        /// Generates a disgnostic email containing the results of the Phase 1 collection of sendgrid bounces, blocks and invalid emails
        /// </summary>
        void GenerateSendGridCollectionReport(List<SendGridEmailFailureDTO> items);

        /// <summary>
        /// generate a diagnostic email with the actions taken on SendGrid blocks, bounces and invalid emails
        /// </summary>
        void GenerateSendGridActionReport(List<SendGridEmailFailureDTO> items);
        
        #endregion

        /// <summary>
		/// Get the CountryID for the Account
		/// </summary>
		/// <param name="AccountID"></param>
		/// <returns>CountryID</returns>
	    long? GetAccountCountryByID(long accountID);

        List<long> GetAccountsWithUserPermissionAndFeature(long userID, PermissionTypeEnum permission, FeatureTypeEnum feature);
    }
}
