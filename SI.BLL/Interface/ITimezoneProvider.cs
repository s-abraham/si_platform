﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SI.BLL
{
    /// <summary>
    /// These functions are for BLL use only
    /// </summary>
    public interface ITimezoneProvider
    {
        /// <summary>
        /// Gets the timezone for a user.  If the user has no timezone set, it will default to the account timezone that they are a member of
        /// </summary>
        TimeZoneInfo GetTimezoneForUserID(long userID);

        /// <summary>
        /// Gets the timezone for a user.  If the user has no timezone set, it will default to the account timezone that they are a member of
        /// </summary>
        TimeZoneInfo GetTimezoneByID(long timeZoneID);

        /// <summary>
        /// Clear the timezone memory cache.  (if an account is saved with a changed timezone, we would call this)
        /// </summary>
        void ClearTimezoneCache();

        /// <summary>
        /// Clears the timezone cache for only a specific user
        /// </summary>
        void ClearTimezoneCacheForUser(long userID);

        /// <summary>
        /// Gets the timezone for a Account.  Account must have 
        /// </summary>
        TimeZoneInfo GetTimezoneForAccountID(long accountID);

	    long? FindByName(string timeZoneName);
    }
}
