﻿using Connectors;
using SI.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace SI.BLL
{
    public interface IReviewProvider
    {
        void SendReviewDiagnosticsEmail();

        /// <summary>
        /// Gets a list of work to be used to schedule validator jobs.
        /// </summary>
        /// <returns>A list of worklist items</returns>
        List<ReviewWorklistItemDTO> GetReviewValidatorWorklist(
            ReviewSourceEnum reviewSource,
            int minMinutesSinceLastValidation, 
            bool onlyMissingURL, 
            bool onlyInvalidURL
        );

        void QueueReviewDownloaderJobs(
            UserInfoDTO userInfo, bool downloadDetails,
            int spreadJobsOutOverXMinutes,
            List<ReviewSourceEnum> reviewSources, long accountID,
            JobStatusEnum initialJobStatus, int priority
        );

        /// <summary>
        /// Clears all raw reviews for a given account and review source.
        /// </summary>
        void ClearRawReviewsForAccountAndSource(long accountID, ReviewSourceEnum reviewSource);

        /// <summary>
        /// Clears all raw reviews for given account and review source pairs.
        /// </summary>
        void ClearRawReviewsForAccountAndSource(List<ReviewSourceAndAccountPairDTO> accountSources);

        /// <summary>
        /// Returns all reviews and summaries for a given account.  Only for use in the review integrator, this is an
        /// unauthenticated pull.  NOT FOR USE ON WEB SITE.  No pagination or search capabilities.
        /// </summary>
        /// <param name="accountID">The account id to retrieve reviews for</param>
        /// <returns>A list of reviews for a given account</returns>
        BatchReviewIntegratorPullDTO GetReviewsForAccountAndSource(long accountID, long reviewSourceID);

        /// <summary>
        /// Returns all raw reviews for a given account.  These will come from the
        /// raw review table in the import database. Thes reviews have not been
        /// tested for duplication against existing main review database.
        /// </summary>
        /// <param name="accountID">The account id to retrieve raw reviews for</param>
        /// <returns>A list of raw reviews for a given account</returns>
        RawReviewDataDTO GetRawReviewDataForAccountAndSource(long accountID, long reviewSourceID, long userID);
        
        /// <summary>
        /// Performs batch updates based upon the outcome of the
        /// review integrator processor.
        /// </summary>
        /// <param name="data">Specialized collection of updates to perform</param>
        void BatchReviewIntegratorUpdate(BatchReviewIntegratorUpdateDTO data);

        /// <summary>
        /// Gets a list of country ids for a list of accounts
        /// </summary>
        List<long> GetCountryIDsForAccounts(List<long> accountIDs);

        /// <summary>
        /// Gets a list of review sources valid for a given account.
        /// This will be based on the account's country.  If no address is entered, the list will be empty.
        /// </summary>
        List<ReviewSourceDTO> GetReviewSources(long accountID);

        /// <summary>
        /// Gets a list of review sources valid for all of the accounts that a user has access to
        /// This will be based on all of the accounts countries.  If no addresses are entered for those accounts, the list will be empty.
        /// </summary>
        List<ReviewSourceDTO> GetReviewSources(UserInfoDTO userInfo);

        /// <summary>
        /// Gets a processor setting
        /// </summary>
        /// <param name="key">The case-insensitive setting key</param>
        string GetProcessorSetting(string key);

        /// <summary>
        /// Gets a single review by its ID
        /// </summary>
        ReviewDTO GetReviewByID(long reviewID);



        /// <summary>
        /// Gets the review url for a given account and review source.
        /// </summary>
        /// <returns>A review url if found, otherwise null</returns>
        ReviewUrlDTO GetReviewUrl(UserInfoDTO userInfo, long accountID, ReviewSourceEnum reviewSource);

        List<ReviewUrlDTO> GetReviewUrls(UserInfoDTO userInfo, List<long> account_reviewSourceIDs);

        /// <summary>
        /// gets a review url by it's Account_ReviewSourceID
        /// </summary>
        /// <returns>A review url if found, otherwise null</returns>
        ReviewUrlDTO GetReviewUrl(UserInfoDTO userInfo, long AccountReviewSourceID);

        /// <summary>
        /// Inserts or updates a review url
        /// </summary>
        /// <param name="se"></param>
        /// <returns>ReviewUrlDTO</returns>
        ReviewUrlDTO SaveReviewUrl(UserInfoDTO userInfo, ReviewUrlDTO se);

        /// <summary>
        /// Saves new review summaries.  Not for updating.
        /// </summary>
        /// <param name="dto"></param>
        /// <returns>Returns the ID of the review summary</returns>
        long SaveNewRawReviewSummary(RawReviewSummaryDTO dto, long jobID);

        void SaveNewRawReviewSummaries(List<RawReviewSummaryDTO> dtos, long jobID);

        /// <summary>
        /// Batch saves raw reviews and their comments.  Not for updating.
        /// </summary>
        /// <returns>Returns a list of IDs for the new raw review records</returns>
        List<long> SaveNewRawReviews(List<RawReviewDTO> list, long jobID);


        /// <summary>
        /// Gets a default configured review connection object
        /// </summary>
        SIReviewConnection GetReviewConnection(ReviewSourceEnum reviewSource, int timeOutSeconds = 30, bool useProxy = true, int maxRedirects = 50);

        /// <summary>
        /// Get Reputation Data for Review Stream
        /// </summary>
        /// <returns>Returns a list of Reviews.</returns>
        ReviewStreamSearchResultDTO GetReviewStreamData(UserInfoDTO userInfo, ReviewStreamSearchRequestDTO req);


        /// <summary>
        /// gets a review url by it's Account_ReviewSourceID
        /// </summary>
        /// <returns>A review url if found, otherwise null</returns>
        List<AccountReviewUrlDTO> GetReviewUrls(long accountID);

        
        /// <summary>
        /// Gets data for the dashboard overview and related screens
        /// </summary>
        /// <param name="userInfo">The user info object that confines the results to the appropriate accounts</param>
        /// <param name="specificAccountID">If not null, only results for this accoutn will be returned. (future current account context support)</param>
        /// <returns>Dashboard overview data.</returns>
        DashboardOverviewDTO GetDashboardOverviewData(UserInfoDTO userInfo);

        /// <summary>
        /// Gets the data for the competitive analysis report
        /// </summary>
        CompAnalysisDataDTO GetCompetitiveAnalysisData(UserInfoDTO userInfo);

        /// <summary>
        ///  Update ReadDate, RespondedDate and IgnoredDate for Review
        /// </summary>        
		bool UpdateReviewByID(UserInfoDTO userInfo, long ReviewID, string menuaction);

        #region diagnostic tools for resolving downloader integrator problems

        /// <summary>
        /// Creates a parked downloader job for the given account_reviewsourceid and returns the JobID
        /// </summary>
        long CreateParkedDownloaderJob(long accountReviewSourceID);

        /// <summary>
        /// Creates a parked downloader job for the given accountid and review source and returns the JobID
        /// </summary>
        long CreateParkedDownloaderJob(long accountID, ReviewSourceEnum reviewSource);

        /// <summary>
        /// Creates a parked review integrator job for the given account and review source
        /// </summary>
        long CreateParkedIntegratorJob(long accountID, ReviewSourceEnum reviewSource);

        /// <summary>
        /// Checks to see if 2 reviews match using the current matching routine
        /// </summary>
        bool CheckIfReviewsMatch(long reviewID1, long reviewID2);

        #endregion

		/// <summary>
		/// Review counts per review source used on review stream
		/// </summary>
		/// <param name="userInfo"></param>
		/// <param name="req"></param>
		/// <returns></returns>
	    ReviewStreamCountPerSiteResultDTO GetReviewCountPerSite(UserInfoDTO userInfo, ReviewStreamCountPerSiteRequestDTO req);
    }
}
