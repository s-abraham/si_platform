﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SI.DTO;

namespace SI.BLL
{
    public interface IReportsProvider
    {
        /// <summary>
        /// Process Daily Enterprise Reputation Summary Report
        /// </summary>
        /// <param name="Type"></param>
        void ProcessDailyEnterpriseReputationSummaryReport();

        /// <summary>
        /// Process Token Validation Report
        /// </summary>        
        void ProcessTokenValidationReport();


        /// <summary>
        /// Process Reputation And Social Report
        /// </summary>        
        void ProcessReputationAndSocialReport();

        /// <summary>
        /// Get Reputation Snapshot Report HTML
        /// </summary>        
        string GetReputationSnapshotReportHTML(long AccountID, DateTime date, bool printHtml);

        /// <summary>
        /// Get Social Snapshot ReportHTML
        /// </summary>        
        string GetSocialSnapshotReportHTML(long AccountID, DateTime date, bool printHtml);

        /// <summary>
        /// GetR eputation And Social Report HTML
        /// </summary>        
        string GetReputationAndSocialReportHTML(long AccountID, long UserID, bool printHtml);

		/// <summary>
		/// 
		/// </summary>
		/// <param name="userInfo"></param>
		/// <param name="accountIDs"></param>
		/// <returns></returns>
	    List<EnterpriseReputationSocialReportDTO> GetReputationAndSocialReport(UserInfoDTO userInfo, List<long> accountIDs, long? virtualGroupID);
		/// <summary>
		/// 
		/// </summary>
		/// <param name="userInfo"></param>
		/// <param name="accounts"></param>
		/// <returns></returns>
        List<EnterpriseReputationSocialDetailReportDTO> GetReputationAndSocialDetailReport(UserInfoDTO userInfo, List<long> accounts, long? virtualGroupID);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="UserID"></param>
        /// <returns></returns>
        ReportTemplateandEmailReportDTO GetReportTemplateAndEmailReport(long UserID);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="UserID"></param>
        /// <param name="userInfo"></param>
        /// <param name="ReportTemplateID"></param>
        /// <param name="ScheduleSpec"></param>
        /// <param name="ReportSubscriptionID"></param>
        /// <param name="isDisable"></param>
        /// <returns></returns>
        bool SetReportSubscriptions(long UserID, UserInfoDTO userInfo, long ReportTemplateID, string ScheduleSpec, long ReportSubscriptionID, bool isDisable);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="ReportLogID"></param>
        /// <returns></returns>
        string GetReportLogByID(long ReportLogID);

        /// <summary>
        /// 
        /// </summary>
        void SendTokenValidationByUser();
    }
}
