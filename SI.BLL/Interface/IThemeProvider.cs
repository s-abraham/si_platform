﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SI.BLL
{
    public interface IThemeProvider
    {
        /// <summary>
        /// Gets the name of the theme to use based upon the URL that was called.
        /// </summary>
        /// <param name="url">Teh url that was called.</param>
        /// <returns></returns>
        string GetThemeName(string url);
    }
}
