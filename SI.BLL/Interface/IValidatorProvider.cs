﻿using Connectors;
using SI.DTO;
using System;
using System.Collections.Generic;
using Connectors.Parameters;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Connectors.URL_Validators.Parameters;

namespace SI.BLL
{
    public interface IValidatorProvider
    {
        /// <summary>
        /// Gets a processor setting
        /// </summary>
        /// <param name="key">The case-insensitive setting key</param>
        string GetProcessorSetting(string key);

        List<DTO.ReviewSourceDTO> GetReviewSources();

        /// <summary>
        /// Fills main XPaths
        /// </summary>
        void fillBaseReviewDownloaderXPaths(string namePrefix, XPathParameters xpathParameters);

        /// <summary>
        /// Gets a default configured Validtor connection object
        /// </summary>
        SIReviewURLValidatorConnection GetValidatorConnection(ReviewSourceEnum reviewSource, int timeOutSeconds = 30, bool useProxy = true, int maxRedirects = 50);
    }
}
