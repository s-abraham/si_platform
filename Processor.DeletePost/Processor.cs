﻿using JobLib;
using SI.BLL;
using SI.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Processor.DeletePost
{
    public class Processor : ProcessorBase<DeletePostData>
    {
        // Need to pass the args through for the default constructor
        public Processor(string[] args) : base(args) { }

        protected override void start()
        {
            JobDTO job = getNextJob();
            while (job != null)
            {
                try
                {
                    setJobStatus(job.ID.Value, JobStatusEnum.Started);

                    DeletePostData data = getJobData(job.ID.Value);
                    ProviderFactory.Social.ProcessPostDeletion(UserInfoDTO.System, data.PostID, job.ID.Value);

                    setJobStatus(job.ID.Value, JobStatusEnum.Completed);
                }
                catch (Exception ex)
                {
                    setJobStatus(job.ID.Value, JobStatusEnum.Failed);
                    ProviderFactory.Logging.LogException(ex);
                    Auditor
                        .New(AuditLevelEnum.Exception, AuditTypeEnum.ProcessorException, UserInfoDTO.System, ex.ToString())
                        .Save(ProviderFactory.Logging)
                    ;
                }


                job = getNextJob();
            }
        }
    }

}
