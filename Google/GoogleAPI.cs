﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Web.Script.Serialization;
using Google.Model;
using Newtonsoft.Json;
using SI;
using SI.DTO;



namespace Google
{
	public static class GoogleAPI
	{
		public const string GoogleAPIAuthURL = "https://accounts.google.com/o/oauth2/auth?redirect_uri={0}&response_type=code&client_id={1}&scope={2}&approval_prompt=force&access_type=offline&state={3}";
		public const string GoogleAPITokenURL = "https://accounts.google.com/o/oauth2/token";
		public const string GoogleAPIUserInfoURL = "https://www.googleapis.com/oauth2/v2/userinfo";
		public const string GoogleAPIPeopleURL = "https://www.googleapis.com/plus/v1/people/{0}";        

		public const string GoogleAPIPagesForUserURL = "https://www.googleapis.com/plusPages/v2/people/{0}/people/pages";
        public const string GooglePlusAPIURLActivityCreateURL = "https://www.googleapis.com/plusPages/v2/people/{0}/activities?access_token={1}";
        public const string GooglePlusAPIURLMediaCreateURL = "https://www.googleapis.com/upload/plusPages/v2/people/{0}/media/cloud?access_token={1}";

        public const string GooglePlusAPIActivityGetSpecificURL = "https://www.googleapis.com/plus/v1/activities/{0}?key={1}&quotaUser={2}";
        public const string GooglePlusAPIActivityGetListForAPageURL = "https://www.googleapis.com/plusPages/v2/people/{0}/activities/public?key={1}&quotaUser={2}&maxResults={3}&pageToken={4}";
        public const string GooglePlusAPICommentGetForAnActivityURL = "https://www.googleapis.com/plus/v1/activities/{0}/comments?key={1}&quotaUser={2}";

        public const string GooglePlusAPIPlusonersGetForAnActivityURL = "https://www.googleapis.com/plus/v1/activities/{0}/people/plusoners?maxResults=100&key={1}";
        public const string GooglePlusAPIResharersGetForAnActivityURL = "https://www.googleapis.com/plus/v1/activities/{0}/people/resharers?maxResults=100&key={1}";
                
        
        public const string GooglePlusGetSpecificPageURL = "https://www.googleapis.com/plus/v1/people/{0}?key={1}&quotaUser={2}";
        public const string GooglePlusAPIURLCommentCreate = "https://www.googleapis.com/plusPages/v2/activities/{0}/comments?access_token={1}&onBehalfOf={2}&quotaUser={2}";
        public const string GooglePlusAPIURLCommentDelete = "https://www.googleapis.com/plusPages/v2/comments/{0}?access_token={1}&amp;onBehalfOf={2}&quotaUser={3}";
        public const string GooglePlusAPIURLActivityDelete = "https://www.googleapis.com/plusPages/v2/activities/{0}?access_token={1}&onBehalfOf={2}&quotaUser={3}";

		public const string YouTubeAPIChannelsList = "https://www.googleapis.com/youtube/v3/channels/?part=contentDetails";

        public static string UserAgent = "Mozilla/5.0 (Windows NT 6.2) AppleWebKit/536.6 (KHTML, like Gecko) Chrome/20.0.1090.0 Safari/536.6";
        public const int GooglePlusAPIMaxResults = 100;

        private static int MaxRedirects = 2;
        private static int Timeout = 60 * 1000;
        private static SInet _WebNet = null;

        #region PAGE

        public static GoogleToken GetGoogleTokenFromCode(string callback, string clientID, string clientSecret, string code)
        {
            GoogleToken googleToken = null;

            using (SInet siNet = new SInet(false, 100, "", 30000))
            {
                Dictionary<String, String> postVars = new Dictionary<string, string>();
                postVars.Add("code", code);
                postVars.Add("redirect_uri", callback);
                postVars.Add("client_id", clientID);
                postVars.Add("client_secret", clientSecret);
                postVars.Add("grant_type", "authorization_code");

                string json = siNet.DownloadString(GoogleAPITokenURL, postVars);

                if (!string.IsNullOrEmpty(json))
                    googleToken = JsonConvert.DeserializeObject<GoogleToken>(json);
            }

            return googleToken;
        }

        public static string GetGoogleUserInfoID(string token, string tokenType)
        {
            string json = GetJson(GoogleAPIUserInfoURL, token, tokenType);
            if (!string.IsNullOrEmpty(json))
            {
                dynamic userID = JsonConvert.DeserializeObject<dynamic>(json);
                if (userID != null)
                    return userID.id;
            }

            return string.Empty;
        }

        public static GooglePeopleDetails GetGooglePeople(string userId, string token, string tokenType)
        {
            string json = GetJson(string.Format(GoogleAPIPeopleURL, userId), token, tokenType);
            if (!string.IsNullOrEmpty(json))
            {
                return JsonConvert.DeserializeObject<GooglePeopleDetails>(json);
            }

            return null;
        }

        public static GooglePagesForUser GetGooglePagesForUser(string userId, string token, string tokenType = null)
        {
            string json = GetJson(string.Format(GoogleAPIPagesForUserURL, userId), token, tokenType);
			if (!string.IsNullOrEmpty(json))
			{
				return JsonConvert.DeserializeObject<GooglePagesForUser>(json);
			}

            return null;
        }

		public static string GetYouTubeChannels(string token, string tokenType)
		{
			string json = GetJson(YouTubeAPIChannelsList, token, tokenType);
			if (!string.IsNullOrEmpty(json))
			{
				//return JsonConvert.DeserializeObject<GooglePagesForUser>(json);
				return json;
			}

            return null;

		}

        public static GooglePlusAccessTokenRequestResponse GetGooglePlusAccessToken(string clientID, string clientSecret, string refreshToken, string grantType)
        {
            GooglePlusErrorResponse objGooglePlusResponse = new GooglePlusErrorResponse();
            //GooglePlusParams googlePlusGetTokenParams = new GooglePlusParams();
            GooglePlusAccessTokenRequestResponse result = new GooglePlusAccessTokenRequestResponse();
            grantType = "refresh_token";
            try
            {
                if (!string.IsNullOrEmpty(clientID) && !string.IsNullOrEmpty(clientSecret) && !string.IsNullOrEmpty(refreshToken) && !string.IsNullOrEmpty(grantType))
                {
                    using (SInet siNet = new SInet(false, 100, "", 30000))
                    {
                        //Dictionary<String, String> postVars = new Dictionary<string, string>();
                        //postVars.Add("client_id", clientID);
                        //postVars.Add("client_secret", clientSecret);
                        //postVars.Add("refresh_token", refreshToken);
                        //postVars.Add("grant_type", grantType);


                        var postParamBuilder = new StringBuilder();

                        postParamBuilder.Append("client_id=" + clientID);
                        postParamBuilder.Append("&client_secret=" + clientSecret);
                        postParamBuilder.Append("&refresh_token=" + refreshToken);
                        postParamBuilder.Append("&grant_type=" + grantType);


                        GooglePlusRequest objGooglePlusRequest = new GooglePlusRequest();
                        objGooglePlusRequest.Parameters = postParamBuilder.ToString();

                        objGooglePlusRequest.RequestURL = GoogleAPITokenURL;
                        objGooglePlusRequest.Method = "POST";
                        //objGooglePlusRequest.TimeOut = Convert.ToInt32(GooglePlusAPITimeOut);

                        //string json = siNet.DownloadString(GoogleAPITokenURL, postVars);
                        string json = GetJson(objGooglePlusRequest);

                        if (!string.IsNullOrEmpty(json))
                        {
                            var jss = new JavaScriptSerializer();
                            if (json.Contains("error"))
                            {
                                objGooglePlusResponse = jss.Deserialize<GooglePlusErrorResponse>(json);
                            }
                            else
                            {
                                result = jss.Deserialize<GooglePlusAccessTokenRequestResponse>(json);
                            }
                        }
                        else
                        {
                            objGooglePlusResponse.message = "Not able to Get AccessToken.";
                        }
                    }
                }
                else
                {
                    objGooglePlusResponse.message = "You have entered invalid parameters";
                }

            }

            catch (Exception ex)
            {
                #region ERROR HANDLING

                objGooglePlusResponse.message = ex.Message;

                #endregion
            }

            result.errorResponse = objGooglePlusResponse;

            return result;
        }

        public static GooglePlusGetActivityListByPageResponse GetGooglePlusActivitiesListForAPage(string pageID,string apiKey, string sdUserId = "00000000-0000-0000-0000-000000000000")
        {            
            GooglePlusGetActivityListByPageResponse rawResult = new GooglePlusGetActivityListByPageResponse();
            GooglePlusGetActivityListByPageResponse result = new GooglePlusGetActivityListByPageResponse();

            try
            {
                rawResult = GetGooglePlusActivitiesPartialList(pageID, apiKey, string.Empty);

                // Getting Count of the Results
                int resCount = rawResult.items.Count;

                // When the count of results is 100 or more, looping through each of those 100 items and adding to Final Result List
                while (resCount >= 100)
                {
                    foreach (var rawitem in rawResult.items)
                    {
                        result.items.Add(rawitem);
                    }
                    string nextToken = rawResult.nextPageToken;
                    rawResult = GetGooglePlusActivitiesPartialList(pageID, apiKey, nextToken);
                    resCount = rawResult.items.Count;
                }

                // When the count of results is less than 100, looping through each of those items and adding to Final Result List
                foreach (var rawitem in rawResult.items)
                {
                    result.items.Add(rawitem);
                }

                result.kind = rawResult.kind;
                result.etag = rawResult.etag;
                result.nextPageToken = rawResult.nextPageToken;
                result.title = rawResult.title;
                result.error = rawResult.error;
            }

            catch (Exception ex)
            {
                #region ERROR HANDLING
                              
                #endregion
            }

            return result;
        }

        public static GooglePlusGetSpecificPageResponse GetGooglePlusPagebyPageID(string pageID,string apiKey, string sdUserId = "00000000-0000-0000-0000-000000000000")
        {            
            GooglePlusResponse objGooglePlusResponse = new GooglePlusResponse();
            GooglePlusGetSpecificPageResponse result = new GooglePlusGetSpecificPageResponse();

            try
            {
                if (!string.IsNullOrEmpty(pageID))
                {
                    string URL = string.Format(GooglePlusGetSpecificPageURL, pageID, apiKey, sdUserId);

                    string rawResponse = string.Empty;

                    try
                    {
                        rawResponse = GetJson(URL);

                        if (!string.IsNullOrEmpty(rawResponse))
                        {
                            var jss = new JavaScriptSerializer();

                            if (rawResponse.Contains("error"))
                            {
                                objGooglePlusResponse = jss.Deserialize<GooglePlusResponse>(rawResponse);
                            }
                            else
                            {
                                result = jss.Deserialize<GooglePlusGetSpecificPageResponse>(rawResponse);
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        #region ERROR HANDLING
                        objGooglePlusResponse.error.message = ex.Message;
                        #endregion
                    }
                }
                else
                {
                    objGooglePlusResponse.error.message = "You have entered invalid parameters";
                }
            }
            catch (Exception ex)
            {
                #region ERROR HANDLING
                objGooglePlusResponse.error.message = ex.Message;
                #endregion
            }

            result.error = objGooglePlusResponse.error;

            return result;
        }

        public static bool ValidateToken(string appKey, string appSecret, string accessToken)
        {   
            try
            {
                var objGooglePlusAccessTokenRequestResponse = new GooglePlusAccessTokenRequestResponse();

                objGooglePlusAccessTokenRequestResponse = GetGooglePlusAccessToken(appKey, appSecret, accessToken, "refresh_token");
                if (!String.IsNullOrEmpty(objGooglePlusAccessTokenRequestResponse.access_token))
                {
                    return true;                                       
                    
                }
                else
                {
                    return false;                    
                }
            }
            catch (Exception ex)
            {
                #region ERROR HANDLING
                               
                #endregion

                return false;
            }
        }

        public static GooglePlusAccessTokenRequestResponse ValidateToken2(string appKey, string appSecret, string accessToken)
        {
            var objGooglePlusAccessTokenRequestResponse = new GooglePlusAccessTokenRequestResponse();

            try
            {
                objGooglePlusAccessTokenRequestResponse = GetGooglePlusAccessToken(appKey, appSecret, accessToken, "refresh_token");
                //if (!String.IsNullOrEmpty(objGooglePlusAccessTokenRequestResponse.access_token))
                //{
                //    return true;

                //}
                //else
                //{
                //    return false;
                //}
            }
            catch (Exception ex)
            {
                #region ERROR HANDLING

                #endregion
            }

            return objGooglePlusAccessTokenRequestResponse;
        }
        #endregion
        
        #region PUBLISH

        public static GooglePlusPostResponse AddGooglePlusActivity(string accessToken, string pageID, string accessType, string objectType, string activityType, string postContent, string attachmentID, string postURL, string circleID, string userID, string sdUserId = "00000000-0000-0000-0000-000000000000")
        {
            GooglePlusPostResponse objGooglePlusPostResponse = new GooglePlusPostResponse();
            GooglePlusResponse objGooglePlusResponse = new GooglePlusResponse();
            GooglePlusAddActivityRequest postParams = new GooglePlusAddActivityRequest();
            GooglePlusAddorUpdateorGetSpecificActivityResponse rawresult = new GooglePlusAddorUpdateorGetSpecificActivityResponse();
            try
            {
                if (!string.IsNullOrEmpty(pageID) && !string.IsNullOrEmpty(accessToken) && !string.IsNullOrEmpty(postContent))
                {
                    // below if statement checks if certain invalid conditions are met. if they are, the method skips down to an error message.. If not, the method continues its sequence
                    if (!((!string.IsNullOrEmpty(attachmentID) && objectType != "photo" && objectType != "video") // An attachment id is entered, but objectType is not set to "photo" or "video" 
                    || ((objectType == "photo") && string.IsNullOrEmpty(attachmentID)) // The objectType is set to "photo"  but an attachment id isn't entered
                    || (objectType != "" && objectType != "video" && objectType != "photo" && objectType != "article"))) // objectType is something other than null, video,photo or article
                    {
                        string URL = string.Format(GooglePlusAPIURLActivityCreateURL, pageID, accessToken, sdUserId);


                        activityType = "post";

                        postParams.obj.originalContent = postContent;
                        postParams.verb = activityType;

                        var objGooglePlusAddActivityItem = new GooglePlusAddActivityItem();

                        if (accessType == "circle")
                        {
                            objGooglePlusAddActivityItem.id = circleID;
                            objGooglePlusAddActivityItem.type = accessType;

                            postParams.access.items.Add(objGooglePlusAddActivityItem);
                        }
                        else if (accessType == "person")
                        {
                            objGooglePlusAddActivityItem.type = accessType;
                            objGooglePlusAddActivityItem.id = userID;

                            postParams.access.items.Add(objGooglePlusAddActivityItem);
                        }
                        else if (accessType == "myCircles" || accessType == "extendedCircles")
                        {
                            objGooglePlusAddActivityItem.type = accessType;
                            objGooglePlusAddActivityItem.id = string.Empty;

                            postParams.access.items.Add(objGooglePlusAddActivityItem);
                        }
                        else
                        {
                            objGooglePlusAddActivityItem.type = "public";
                            objGooglePlusAddActivityItem.id = string.Empty;
                            postParams.access.items.Add(objGooglePlusAddActivityItem);
                        }

                        var objGooglePlusAddActivityAttachment = new GooglePlusAddActivityAttachment();

                        objGooglePlusAddActivityAttachment.objectType = objectType;
                        if(string.IsNullOrEmpty(postURL))
                            postURL = string.Empty;

                        objGooglePlusAddActivityAttachment.url = postURL;
                        objGooglePlusAddActivityAttachment.id = attachmentID;

                        postParams.obj.attachments.Add(objGooglePlusAddActivityAttachment);

                        string json = JsonConvert.SerializeObject(postParams);

                        string rawResponse = string.Empty;

                        try
                        {
                            GooglePlusRequest objGooglePlusRequest = new GooglePlusRequest();
                            objGooglePlusRequest.Parameters = json;

                            objGooglePlusRequest.RequestURL = URL;
                            objGooglePlusRequest.Method = "POSTviaJSON";

                            rawResponse = GetJson(objGooglePlusRequest);

                            if (!string.IsNullOrEmpty(rawResponse))
                            {
                                var jss = new JavaScriptSerializer();

                                if (rawResponse.Contains("error"))
                                {
                                    objGooglePlusResponse = jss.Deserialize<GooglePlusResponse>(rawResponse);
                                }

                                else
                                {
                                    rawresult = jss.Deserialize<GooglePlusAddorUpdateorGetSpecificActivityResponse>(rawResponse);
                                }
                            }
                        }
                        catch (Exception ex)
                        {
                            #region ERROR HANDLING

                            objGooglePlusResponse.error.message = ex.Message;

                            #endregion
                        }
                    }
                    else
                    {
                        objGooglePlusResponse.error.message = "Please specify a valid objectType (photo, album, video or an article). If posting a photo/album, please specify an attachment ID";
                    }
                }

                else
                {
                    objGooglePlusResponse.error.message = "You have entered invalid parameters";
                }


                objGooglePlusPostResponse.error = objGooglePlusResponse.error;
                objGooglePlusPostResponse.postURL = rawresult.url;

                if (!string.IsNullOrEmpty(rawresult.published))
                {
                    objGooglePlusPostResponse.IsSuccess = true;
                }

                else { objGooglePlusPostResponse.IsSuccess = false; }

                objGooglePlusPostResponse.finalActivityresponse = rawresult;
            }

            catch (Exception ex)
            {
                #region ERROR HANDLING

                objGooglePlusResponse.error.message = ex.Message;

                #endregion
            }

            return objGooglePlusPostResponse;
        }

        public static GooglePlusPostResponse PostGooglePlusPhoto(string accesstoken, string pageID, string accessType, string activityType, string postContent, string postURL, string pathToImage, string circleID, string userID, string sdUserId = "00000000-0000-0000-0000-000000000000")
        {
            GooglePlusPostResponse objGooglePlusPostResponse = new GooglePlusPostResponse();
            GooglePlusResponse objGooglePlusResponse = new GooglePlusResponse();
            var rawresult = new GooglePlusUploadPhotoResponse();

            try
            {

                if (!string.IsNullOrEmpty(pageID) && !string.IsNullOrEmpty(accesstoken) && !string.IsNullOrEmpty(pathToImage) && !string.IsNullOrEmpty(postContent))
                {
                    rawresult = UploadGooglePlusPhotoToTempServer(accesstoken, pageID, pathToImage, sdUserId);
                    objGooglePlusPostResponse = AddGooglePlusActivity(accesstoken, pageID, accessType, "photo", activityType, postContent, rawresult.id, postURL, circleID, userID, sdUserId);
                }

                else
                {
                    objGooglePlusResponse.error.message = "You have entered invalid parameters";
                }

            }

            catch (Exception ex)
            {
                #region ERROR HANDLING
                #endregion
            }

            return objGooglePlusPostResponse;

        }

        public static GooglePlusPostResponse AddGooglePlusComment(string accesstoken, string pageID, string activityID, string commentContent, string sdUserId = "00000000-0000-0000-0000-000000000000")
        {
            GooglePlusPostResponse objGooglePlusPostResponse = new GooglePlusPostResponse();            
            GooglePlusResponse objGooglePlusResponse = new GooglePlusResponse();
            GooglePlusAddCommentRequest postParams = new GooglePlusAddCommentRequest();
            GooglePlusAddorUpdateOrGetSpecificCommentResponse rawresult = new GooglePlusAddorUpdateOrGetSpecificCommentResponse();

            try
            {
                if (!string.IsNullOrEmpty(pageID) && !string.IsNullOrEmpty(accesstoken) && !string.IsNullOrEmpty(activityID) && !string.IsNullOrEmpty(commentContent))
                {
                    string URL = string.Format(GooglePlusAPIURLCommentCreate, activityID, accesstoken, pageID, sdUserId);

                    postParams.obj.originalContent = commentContent;

                    string json = JsonConvert.SerializeObject(postParams);

                    string rawResponse = string.Empty;

                    try
                    {
                        GooglePlusRequest objGooglePlusRequest = new GooglePlusRequest();
                        objGooglePlusRequest.Parameters = json;

                        objGooglePlusRequest.RequestURL = URL;
                        objGooglePlusRequest.Method = "POSTviaJSON";
                        
                        //rawResponse = ExecuteCommand_Post(objGooglePlusRequest);
                        rawResponse = GetJson(objGooglePlusRequest);

                        if (!string.IsNullOrEmpty(rawResponse))
                        {
                            var jss = new JavaScriptSerializer();

                            if (rawResponse.Contains("error"))
                            {
                                objGooglePlusResponse = jss.Deserialize<GooglePlusResponse>(rawResponse);
                            }
                            else
                            {
                                rawresult = jss.Deserialize<GooglePlusAddorUpdateOrGetSpecificCommentResponse>(rawResponse);
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        #region ERROR HANDLING
                        objGooglePlusResponse.error.message = ex.Message;
                        #endregion
                    }
                }
                else
                {
                    objGooglePlusResponse.error.message = "You have entered invalid parameters";
                }

                rawresult.error = objGooglePlusResponse.error;
                objGooglePlusPostResponse.postURL = rawresult.selfLink;

                if (!string.IsNullOrEmpty(rawresult.published))
                {
                    objGooglePlusPostResponse.IsSuccess = true;
                }
                else 
                { 
                    objGooglePlusPostResponse.IsSuccess = false; 
                }

                objGooglePlusPostResponse.finalCommentresponse = rawresult;
            }
            catch (Exception ex)
            {
                #region ERROR HANDLING
                objGooglePlusResponse.error.message = ex.Message;
                #endregion
            }

            return objGooglePlusPostResponse;
        }

        public static GooglePlusResponse DeleteGooglePlusComment(string accesstoken, string pageID, string commentID, string sdUserId = "00000000-0000-0000-0000-000000000000")
        {            
            GooglePlusResponse objGooglePlusResponse = new GooglePlusResponse();

            try
            {
                if (!string.IsNullOrEmpty(pageID) && !string.IsNullOrEmpty(accesstoken) && !string.IsNullOrEmpty(commentID))
                {
                    string URL = string.Format(GooglePlusAPIURLCommentDelete, commentID, accesstoken, pageID, sdUserId);

                    string rawResponse = string.Empty;

                    try
                    {
                        var objGooglePlusRequest = new GooglePlusRequest();

                        objGooglePlusRequest.RequestURL = URL;
                        objGooglePlusRequest.Method = "DELETE";

                        rawResponse = GetJson(objGooglePlusRequest);

                        if (!string.IsNullOrEmpty(rawResponse))
                        {
                            var jss = new JavaScriptSerializer();

                            if (rawResponse.Contains("error"))
                            {
                                objGooglePlusResponse = jss.Deserialize<GooglePlusResponse>(rawResponse);
                            }
                        }
                        else
                        {
                            objGooglePlusResponse.error.message = "You have succesfully deleted the comment";
                        }
                    }
                    catch (Exception ex)
                    {
                        #region ERROR HANDLING
                        objGooglePlusResponse.error.message = ex.Message;
                        #endregion
                    }
                }
                else
                {
                    objGooglePlusResponse.error.message = "You have entered invalid parameters";
                }
            }
            catch (Exception ex)
            {
                #region ERROR HANDLING   
                objGooglePlusResponse.error.message = ex.Message;
                #endregion
            }

            return objGooglePlusResponse;
        }

        public static GooglePlusResponse DeleteGooglePlusActivity(string accesstoken, string pageID, string activityID, string sdUserId = "00000000-0000-0000-0000-000000000000")
        {            
            GooglePlusResponse objGooglePlusResponse = new GooglePlusResponse();

            try
            {
                if (!string.IsNullOrEmpty(pageID) && !string.IsNullOrEmpty(accesstoken) && !string.IsNullOrEmpty(activityID))
                {
                    string URL = string.Format(GooglePlusAPIURLActivityDelete, activityID, accesstoken, pageID, sdUserId);

                    string rawResponse = string.Empty;

                    try
                    {
                        var objGooglePlusRequest = new GooglePlusRequest();

                        objGooglePlusRequest.RequestURL = URL;
                        objGooglePlusRequest.Method = "DELETE";                        
                                                
                        rawResponse = GetJson(objGooglePlusRequest);

                        if (!string.IsNullOrEmpty(rawResponse))
                        {
                            var jss = new JavaScriptSerializer();

                            if (rawResponse.Contains("error"))
                            {
                                objGooglePlusResponse = jss.Deserialize<GooglePlusResponse>(rawResponse);
                            }
                        }
                        else
                        {
                            objGooglePlusResponse.error.message = "You have succesfully deleted the activity";
                        }
                    }
                    catch (Exception ex)
                    {
                        #region ERROR HANDLING
                        #endregion
                    }
                }
                else
                {
                    objGooglePlusResponse.error.message = "You have entered invalid parameters";
                }
            }
            catch (Exception ex)
            {
                #region ERROR HANDLING
                #endregion
            }

            return objGooglePlusResponse;
        }

        #endregion

        #region STATISTICS

        public static GooglePlusAddorUpdateorGetSpecificActivityResponse GetGooglePlusActivityByActivityID(string activityID, string apiKey, string sdUserId = "00000000-0000-0000-0000-000000000000")
        {
            GooglePlusResponse objGooglePlusResponse = new GooglePlusResponse();
            GooglePlusAddorUpdateorGetSpecificActivityResponse result = new GooglePlusAddorUpdateorGetSpecificActivityResponse();

            try
            {
                if (!string.IsNullOrEmpty(activityID))
                {
                    string URL = string.Format(GooglePlusAPIActivityGetSpecificURL, activityID, apiKey, sdUserId);

                    string rawResponse = string.Empty;

                    try
                    {
                        rawResponse = GetJson(URL);

                        if (!string.IsNullOrEmpty(rawResponse))
                        {
                            var jss = new JavaScriptSerializer();

                            if (rawResponse.StartsWith(@"{""error"":"))
                            {
                                objGooglePlusResponse = jss.Deserialize<GooglePlusResponse>(rawResponse);
                            }
                            else
                            {
                                result = jss.Deserialize<GooglePlusAddorUpdateorGetSpecificActivityResponse>(rawResponse);
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        #region ERROR HANDLING

                        objGooglePlusResponse.error.message = ex.Message;

                        #endregion
                    }
                }

                else
                {
                    objGooglePlusResponse.error.message = "You have entered invalid parameters";
                }
            }

            catch (Exception ex)
            {
                #region ERROR HANDLING
                objGooglePlusResponse.error.message = ex.Message;
                #endregion
            }

            result.error = objGooglePlusResponse.error;

            return result;
        }

        public static GooglePlusGetCommentsListResponse GetGooglePlusCommentsListForAnActivity(string activityID,string apiKey, string sdUserId = "00000000-0000-0000-0000-000000000000")
        {            
            GooglePlusResponse objGooglePlusResponse = new GooglePlusResponse();
            GooglePlusGetCommentsListResponse result = new GooglePlusGetCommentsListResponse();

            try
            {
                if (!string.IsNullOrEmpty(activityID))
                {
                    string URL = string.Format(GooglePlusAPICommentGetForAnActivityURL, activityID, apiKey, sdUserId);

                    string rawResponse = string.Empty;

                    try
                    {
                        rawResponse = GetJson(URL);

                        if (!string.IsNullOrEmpty(rawResponse))
                        {
                            var jss = new JavaScriptSerializer();

                            if (rawResponse.StartsWith(@"{""error"":"))
                            {
                                objGooglePlusResponse = jss.Deserialize<GooglePlusResponse>(rawResponse);
                            }

                            else
                            {
                                result = jss.Deserialize<GooglePlusGetCommentsListResponse>(rawResponse);
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        #region ERROR HANDLING
                        objGooglePlusResponse.error.message = ex.Message;                             
                        #endregion
                    }
                }

                else
                {
                    objGooglePlusResponse.error.message = "You have entered invalid parameters";
                }

                result.error = objGooglePlusResponse.error;
            }

            catch (Exception ex)
            {
                #region ERROR HANDLING
                objGooglePlusResponse.error.message = ex.Message;                             
                #endregion
            }

            return result;
        }

        public static GooglePlusGetResharersListResponse GetGooglePlusResharersListForAnActivity(string activityID, string apiKey, string sdUserId = "00000000-0000-0000-0000-000000000000")
        {
            GooglePlusResponse objGooglePlusResponse = new GooglePlusResponse();
            GooglePlusGetResharersListResponse result = new GooglePlusGetResharersListResponse();

            try
            {
                if (!string.IsNullOrEmpty(activityID))
                {
                    string URL = string.Format(GooglePlusAPIResharersGetForAnActivityURL, activityID, apiKey, sdUserId);

                    string rawResponse = string.Empty;

                    try
                    {
                        rawResponse = GetJson(URL);

                        if (!string.IsNullOrEmpty(rawResponse))
                        {
                            var jss = new JavaScriptSerializer();

                            if (rawResponse.StartsWith(@"{""error"":"))
                            {
                                objGooglePlusResponse = jss.Deserialize<GooglePlusResponse>(rawResponse);
                            }
                            else
                            {
                                result = jss.Deserialize<GooglePlusGetResharersListResponse>(rawResponse);
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        #region ERROR HANDLING
                        objGooglePlusResponse.error.message = ex.Message;
                        #endregion
                    }
                }
                else
                {
                    objGooglePlusResponse.error.message = "You have entered invalid parameters";
                }

                result.error = objGooglePlusResponse.error;
            }

            catch (Exception ex)
            {
                #region ERROR HANDLING
                objGooglePlusResponse.error.message = ex.Message;
                #endregion
            }

            return result;
        }

        public static GooglePlusGetPlusonersListResponse GetGooglePlusPlusonersListForAnActivity(string activityID, string apiKey, string sdUserId = "00000000-0000-0000-0000-000000000000")
        {
            GooglePlusResponse objGooglePlusResponse = new GooglePlusResponse();
            GooglePlusGetPlusonersListResponse result = new GooglePlusGetPlusonersListResponse();

            try
            {
                if (!string.IsNullOrEmpty(activityID))
                {
                    string URL = string.Format(GooglePlusAPIPlusonersGetForAnActivityURL, activityID, apiKey, sdUserId);

                    string rawResponse = string.Empty;

                    try
                    {
                        rawResponse = GetJson(URL);

                        if (!string.IsNullOrEmpty(rawResponse))
                        {
                            var jss = new JavaScriptSerializer();

                            if (rawResponse.StartsWith(@"{""error"":"))
                            {
                                objGooglePlusResponse = jss.Deserialize<GooglePlusResponse>(rawResponse);
                            }
                            else
                            {
                                result = jss.Deserialize<GooglePlusGetPlusonersListResponse>(rawResponse);
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        #region ERROR HANDLING
                        objGooglePlusResponse.error.message = ex.Message;
                        #endregion
                    }
                }
                else
                {
                    objGooglePlusResponse.error.message = "You have entered invalid parameters";
                }

                result.error = objGooglePlusResponse.error;
            }

            catch (Exception ex)
            {
                #region ERROR HANDLING
                objGooglePlusResponse.error.message = ex.Message;
                #endregion
            }

            return result;
        }

        public static PostTypeEnum GetPostType(string type)
        {
            PostTypeEnum postType = new PostTypeEnum();

            if (type.ToLower() == "link")
                postType = PostTypeEnum.Link;
            if (type.ToLower() == "photo")
                postType = PostTypeEnum.Photo;
            if (type.ToLower() == "status")
                postType = PostTypeEnum.Status;
            if (type.ToLower() == "event")
                postType = PostTypeEnum.Event;

            return postType;
        }

        #endregion
        
        #region PRIVATE

        private static GooglePlusGetActivityListByPageResponse GetGooglePlusActivitiesPartialList(string pageID, string apiKey, string pageToken, string sdUserId = "00000000-0000-0000-0000-000000000000")
        {            
            GooglePlusResponse objGooglePlusResponse = new GooglePlusResponse();
            GooglePlusGetActivityListByPageResponse result = new GooglePlusGetActivityListByPageResponse();

            try
            {
                if (!string.IsNullOrEmpty(pageID))
                {
                    string URL = string.Format(GooglePlusAPIActivityGetListForAPageURL, pageID, apiKey, sdUserId, GooglePlusAPIMaxResults, pageToken);

                    string rawResponse = string.Empty;

                    try
                    {                      
                        rawResponse = rawResponse = GetJson(URL);

                        if (!string.IsNullOrEmpty(rawResponse))
                        {
                            var jss = new JavaScriptSerializer();

                            if (rawResponse.StartsWith(@"{""error"":"))
                            {
                                objGooglePlusResponse = jss.Deserialize<GooglePlusResponse>(rawResponse);
                            }
                            else
                            {
                                result = jss.Deserialize<GooglePlusGetActivityListByPageResponse>(rawResponse);
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        #region ERROR HANDLING

                        objGooglePlusResponse.error.message = ex.Message;

                        #endregion
                    }
                }

                else
                {
                    objGooglePlusResponse.error.message = "You have entered invalid parameters";
                }
            }

            catch (Exception ex)
            {
                #region ERROR HANDLING
                objGooglePlusResponse.error.message = ex.Message;
                #endregion
            }

            result.error = objGooglePlusResponse.error;
            return result;
        }

        private static GooglePlusUploadPhotoResponse UploadGooglePlusPhotoToTempServer(string access_token, string pageID, string pathToImage, string sdUserId = "00000000-0000-0000-0000-000000000000")
        {            
            GooglePlusResponse objGooglePlusResponse = new GooglePlusResponse();
            GooglePlusUploadPhotoResponse result = new GooglePlusUploadPhotoResponse();
            string rawResponse = string.Empty;

            try
            {
                if (!string.IsNullOrEmpty(pageID) && !string.IsNullOrEmpty(access_token) && !string.IsNullOrEmpty(pathToImage))
                {
                    // Create Boundary
                    string boundary = "--" + DateTime.Now.Ticks.ToString("x");
                    
                    string URL = string.Format(GooglePlusAPIURLMediaCreateURL, pageID, access_token, sdUserId);
                    UserAgent = "Mozilla/4.0 (compatible; Windows NT)";

                    // Create HttpWebRequest
                    HttpWebRequest httpRequest = GetRequest(URL, UserAgent);

                    //httpRequest = (HttpWebRequest)HttpWebRequest.Create(url);
                    httpRequest.ServicePoint.Expect100Continue = false;
                    //httpRequest.UserAgent = "Mozilla/4.0 (compatible; Windows NT)";
                    httpRequest.ContentType = "multipart/form-data; start=\"<image>\"; type=\"image/jpeg\"; boundary=" + boundary;
                    httpRequest.KeepAlive = false;
                    httpRequest.Method = "POST";

                    // New String Builder
                    StringBuilder sb = new StringBuilder();

                    // Header
                    string headerTemplate = "--{0}\r\nContent-Disposition: form-data; name=\"image\"; filename=\"image\"\r\nContent-Type: image/jpeg\r\n\r\n";

                    sb.AppendFormat(headerTemplate, boundary, "source", pathToImage, @"application/octet-stream");

                    // File  
                    string formString = sb.ToString();
                    byte[] formBytes = Encoding.UTF8.GetBytes(formString);
                    byte[] trailingBytes = Encoding.UTF8.GetBytes("\r\n--" + boundary + "--\r\n");
                    byte[] image = DownloadData(pathToImage, null);

                    // Memory Stream
                    MemoryStream imageMemoryStream = new MemoryStream();
                    imageMemoryStream.Write(image, 0, image.Length);

                    // Set Content Length
                    long imageLength = imageMemoryStream.Length;
                    long contentLength = formBytes.Length + imageLength + trailingBytes.Length;
                    httpRequest.ContentLength = contentLength;

                    // Get Request Stream
                    httpRequest.AllowWriteStreamBuffering = false;
                    Stream strm_out = httpRequest.GetRequestStream();

                    // Write to Stream
                    strm_out.Write(formBytes, 0, formBytes.Length);
                    byte[] buffer = new Byte[checked((uint)Math.Min(4096, (int)imageLength))];
                    int bytesRead = 0;
                    int bytesTotal = 0;
                    imageMemoryStream.Seek(0, SeekOrigin.Begin);
                    while ((bytesRead = imageMemoryStream.Read(buffer, 0, buffer.Length)) != 0)
                    {
                        strm_out.Write(buffer, 0, bytesRead); bytesTotal += bytesRead;
                    }
                    strm_out.Write(trailingBytes, 0, trailingBytes.Length);

                    // Close Stream
                    strm_out.Close();
                                        
                    rawResponse = httpDownloadString(httpRequest);

                    if (!string.IsNullOrEmpty(rawResponse))
                    {
                        var jss = new JavaScriptSerializer();

                        result = jss.Deserialize<GooglePlusUploadPhotoResponse>(rawResponse);
                    }
                }
                else
                {
                    objGooglePlusResponse.error.message = "You have entered invalid parameters";
                }
            }
            catch (Exception ex)
            {
                #region ERROR HANDLING
                #endregion
            }

            result.error = objGooglePlusResponse.error;
            return result;
        }

		private static string GetJson(string url, string token = null, string tokenType = null)
		{
			using (SInet siNet = new SInet(false, 100, "", 30000))
			{
				HttpWebRequest request = siNet.GetWebRequest(url);

				if (token != null)
				{
					if (tokenType != null)
						request.Headers.Add(HttpRequestHeader.Authorization, string.Format("{0} {1}", tokenType, token));
					else
						request.Headers.Add(HttpRequestHeader.Authorization, token);
				}
				try
				{
					using (HttpWebResponse resp = (HttpWebResponse) request.GetResponse())
					{
						using (Stream rStream = resp.GetResponseStream())
						{
							using (StreamReader sr = new StreamReader(rStream))
							{
								return sr.ReadToEnd();
							}
						}
					}
				}
				catch (WebException webex)
				{
					using (var reader = new StreamReader(webex.Response.GetResponseStream()))
					{
						var responseText = reader.ReadToEnd();
						//loggin
						return "";
					}
				}
				catch (Exception)
				{
					return "";
				}
			}
		}

        private static string GetJson(GooglePlusRequest _googlePlusRequest)
        {
            // This method handles posting in form-urlencoded format, JSON format & also the the "PUT" (Update) command

            //PublishManager objPublishManager = new PublishManager();
            string rawResponse = string.Empty;
            try
            {
                #region HTTP REQUEST "POST/DELETE/PUT"
                UTF8Encoding encoding = new UTF8Encoding();

                if (!String.IsNullOrEmpty(_googlePlusRequest.RequestURL))
                {                    
                    HttpWebRequest httpRequest = GetRequest(_googlePlusRequest.RequestURL, UserAgent);
                    //HttpWebRequest httpRequest = (HttpWebRequest)HttpWebRequest.Create(_googlePlusRequest.RequestURL);

                    if (!String.IsNullOrEmpty(_googlePlusRequest.Method))
                        httpRequest.Method = _googlePlusRequest.Method;

                    byte[] postBytes;
                    if (httpRequest.Method == "POST")
                    {
                        httpRequest.ContentType = "application/x-www-form-urlencoded";
                        httpRequest.Host = "accounts.google.com";
                        httpRequest.Accept = "*/*";
                    }
                    else if (httpRequest.Method == "POSTviaJSON")
                    {
                        httpRequest.Method = "POST";
                        httpRequest.ContentType = "application/json";
                    }
                    else if (httpRequest.Method == "PUT")
                    {
                        httpRequest.ContentType = "application/json";
                    }
                    postBytes = encoding.GetBytes(_googlePlusRequest.Parameters);
                    using (Stream reqStream = httpRequest.GetRequestStream())
                    {
                        reqStream.Write(postBytes, 0, postBytes.Length);
                        reqStream.Flush();
                        reqStream.Close();
                    }

                    httpRequest.Credentials = CredentialCache.DefaultCredentials;

                    rawResponse = httpDownloadString(httpRequest);

                }
                #endregion

            }
            catch (Exception ex)
            {
                #region ERROR HANDLING
                #endregion
            }
            return rawResponse;
        }

        private static HttpWebRequest GetRequest(string requestUrl, string userAgent = null)
        {
            if (_WebNet == null)
                _WebNet = new SInet(false, 2, null, 30 * 1000);
            if (!string.IsNullOrWhiteSpace(userAgent))
            {
                _WebNet.UserAgent = userAgent;
            }

            return _WebNet.GetWebRequest(requestUrl);
        }

        private static string httpDownloadString(HttpWebRequest httpRequest)
        {
            string rawResponse = string.Empty;
            try
            {
                //Get Response

                using (HttpWebResponse svcResponse = (HttpWebResponse)httpRequest.GetResponse())
                {
                    using (Stream responseStream = svcResponse.GetResponseStream())
                    {
                        try
                        {
                            using (StreamReader readr = new StreamReader(responseStream, Encoding.UTF8))
                            {
                                rawResponse = readr.ReadToEnd();
                                rawResponse = rawResponse.Trim();
                            }
                        }
                        catch (Exception ex)
                        {
                            #region ERROR HANDLING
                            rawResponse = @"{""error"": {    ""message"": """ + "" + ex.Message + "" + @""", ""type"": ""Exception"",    ""code"": 000  }}";
                            #endregion
                        }
                    }

                }
            }
            catch (WebException webex)
            {
                using (var reader = new StreamReader(webex.Response.GetResponseStream()))
                {
                    var responseText = reader.ReadToEnd();
                    rawResponse = responseText;
                }
            }
            catch (Exception ex)
            {
                #region ERROR HANDLING
                rawResponse = @"{""error"": {    ""message"": """ + "" + ex.Message + "" + @""", ""type"": ""Exception"",    ""code"": 000  }}";
                #endregion
            }

            return rawResponse;
        }

        private static byte[] DownloadData(string requestUrl, Dictionary<string, string> PostData, string userAgent = null)
        {
            try
            {
                if (_WebNet == null)
                    _WebNet = new SInet(false, MaxRedirects, null, Timeout);

                if (!string.IsNullOrWhiteSpace(userAgent))
                {
                    _WebNet.UserAgent = userAgent;
                }

                if (PostData == null)
                    PostData = new Dictionary<string, string>();

                if (PostData.Count == 0)
                    return _WebNet.DownloadData(requestUrl, null);
                return _WebNet.DownloadData(requestUrl, PostData);
            }
            catch (WebException webEx)
            {
            }
            return null;
        }

        #endregion

	
	}
}
