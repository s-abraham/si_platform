﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Google.Model
{
	public class GoogleToken
	{
		public string access_token { get; set; }
		public string token_type { get; set; }
		public string expires_in { get; set; }
		public string id_token { get; set; }
		public string refresh_token { get; set; }
	}

	

	public class GooglePeopleDetails
	{
		public string kind { get; set; }
		public string etag { get; set; }
		public string gender { get; set; }
		public string objectType { get; set; }
		public string id { get; set; }
		public string displayName { get; set; }
		public Name name { get; set; }
		public string url { get; set; }
		public Image image { get; set; }
		public bool isPlusUser { get; set; }
		public string language { get; set; }
		public AgeRange ageRange { get; set; }
		public bool verified { get; set; }

		public class Name
		{
			public string familyName { get; set; }
			public string givenName { get; set; }
		}

		public class Image
		{
			public string url { get; set; }
		}

		public class AgeRange
		{
			public int min { get; set; }
		}


	}

	public class GooglePagesForUser
	{
		public string kind { get; set; }
		public string etag { get; set; }
		public string title { get; set; }
		public int totalItems { get; set; }
		public List<Item> items { get; set; }

		public class Image
		{
			public string url { get; set; }
		}

		public class Item
		{
			public string kind { get; set; }
			public string etag { get; set; }
			public string objectType { get; set; }
			public string id { get; set; }
			public string displayName { get; set; }
			public string url { get; set; }
			public Image image { get; set; }
		}
	}


    #region GOOGLEPLUS ACCESS TOKEN RESPONSE

    public class GooglePlusAccessTokenRequestResponse
    {
        public GooglePlusAccessTokenRequestResponse()
        {
            errorResponse = new GooglePlusErrorResponse();
            access_token = string.Empty;
            token_type = string.Empty;
            id_token = string.Empty;
        }

        public GooglePlusErrorResponse errorResponse;
        public string access_token { get; set; }
        public string token_type { get; set; }
        public int expires_in { get; set; }
        public string id_token { get; set; }
    }


    #endregion

    public class GooglePlusErrorResponse
    {
        public GooglePlusErrorResponse()
        {
            error = string.Empty;
            error_description = string.Empty;
            message = string.Empty;

        }

        public string error { get; set; }
        public string error_description { get; set; }
        public string message { get; set; }
    }

    #region GOOGLE PLUS GENERAL SEARCH PARAMETERS
    public class GooglePlusParams
    {
        public GooglePlusParams()
        {
            name = string.Empty;
            coordinates = string.Empty;
            coordinates = string.Empty;
            radius = string.Empty;
            duration = 0;
            summary = string.Empty;
            Locationtype = string.Empty;
            reference = string.Empty;
            event_id = string.Empty;
            keyword = string.Empty;
            lanuguage = string.Empty;
            rankby = string.Empty;
            types = string.Empty;
            pagetoken = string.Empty;
            client_id = string.Empty;
            client_secret = string.Empty;
            refresh_token = string.Empty;
            grant_type = string.Empty;
            redirect_URL = string.Empty;
            pathToImage = string.Empty;
        }

        public string name { get; set; }

        public string coordinates { get; set; }  // also used for location

        public string radius { get; set; }  // also used for accuracy

        public int duration { get; set; }

        public string summary { get; set; }

        public string Locationtype { get; set; }

        public string reference { get; set; }

        public string event_id { get; set; }

        public string keyword { get; set; }

        public string lanuguage { get; set; }

        public string rankby { get; set; }

        public string types { get; set; }

        public string pagetoken { get; set; }

        public string client_id { get; set; }

        public string client_secret { get; set; }

        public string refresh_token { get; set; }

        public string grant_type { get; set; }

        public string redirect_URL { get; set; }

        public string pathToImage { get; set; }
    }

    #endregion

    public class GooglePlusPostResponse
    {

        public GooglePlusPostResponse()
        {
            finalActivityresponse = new GooglePlusAddorUpdateorGetSpecificActivityResponse();
            finalCommentresponse = new GooglePlusAddorUpdateOrGetSpecificCommentResponse();
            error = new GooglePlusError();
            IsSuccess = false;
            postURL = string.Empty;
        }

        public string postURL { get; set; }
        public bool IsSuccess { get; set; }
        public GooglePlusError error { get; set; }
        public GooglePlusAddorUpdateorGetSpecificActivityResponse finalActivityresponse { get; set; }
        public GooglePlusAddorUpdateOrGetSpecificCommentResponse finalCommentresponse { get; set; }

    }

    public class GooglePlusAddorUpdateorGetSpecificActivityResponse
    {
        public GooglePlusAddorUpdateorGetSpecificActivityResponse()
        {
            actor = new Actor();
            @object = new Object();
            provider = new Provider();
            access = new Access();
            error = new GooglePlusError();
            kind = string.Empty;
            etag = string.Empty;
            title = string.Empty;
            published = string.Empty;
            updated = string.Empty;
            id = string.Empty;
            url = string.Empty;
            verb = string.Empty;
        }
        public string kind { get; set; }
        public string etag { get; set; }
        public string title { get; set; }
        public string published { get; set; }
        public string updated { get; set; }
        public string id { get; set; }
        public string url { get; set; }
        public Actor actor { get; set; }
        public string verb { get; set; }
        public Object @object { get; set; }
        public Provider provider { get; set; }
        public Access access { get; set; }
        public GooglePlusError error { get; set; }

    }

    public class Actor
    {
        public Actor()
        {
            image = new Image();
            id = string.Empty;
            displayName = string.Empty;
            url = string.Empty;
        }
        public string id { get; set; }
        public string displayName { get; set; }
        public string url { get; set; }
        public Image image { get; set; }
    }

    public class Image
    {
        public Image()
        {
            url = string.Empty;

        }
        public string url { get; set; }
    }

    public class Provider
    {
        public Provider()
        {
            title = string.Empty;
        }
        public string title { get; set; }
    }

    public class Image2
    {
        public Image2()
        {
            url = string.Empty;
            type = string.Empty;
            height = 0;
            width = 0;
        }
        public string url { get; set; }
        public string type { get; set; }
        public int height { get; set; }
        public int width { get; set; }
    }

    public class Access
    {
        public Access()
        {
            items = new List<Item2>();
            kind = string.Empty;
            description = string.Empty;
        }

        public string kind { get; set; }
        public string description { get; set; }
        public List<Item2> items { get; set; }
    }

    public class Item2
    {
        public Item2()
        {
            type = string.Empty;
        }
        public string type { get; set; }
    }


    public class GooglePlusAddorUpdateOrGetSpecificCommentResponse
    {
        public GooglePlusAddorUpdateOrGetSpecificCommentResponse()
        {
            actor = new Actor();
            @object = new AddCommentResponseObject();
            inReplyTo = new List<InReplyTo>();
            plusoners = new CommentsPlusoners();
            error = new GooglePlusError();
            kind = string.Empty;
            etag = string.Empty;
            verb = string.Empty;
            id = string.Empty;
            published = string.Empty;
            updated = string.Empty;
            selfLink = string.Empty;
        }

        public string kind { get; set; }
        public string etag { get; set; }
        public string verb { get; set; }
        public string id { get; set; }
        public string published { get; set; }
        public string updated { get; set; }
        public Actor actor { get; set; }
        public AddCommentResponseObject @object { get; set; }
        public string selfLink { get; set; }
        public List<InReplyTo> inReplyTo { get; set; }
        public CommentsPlusoners plusoners { get; set; }
        public GooglePlusError error { get; set; }
    }

    public class InReplyTo
    {
        public InReplyTo()
        {
            id = string.Empty;
            url = string.Empty;
        }

        public string id { get; set; }
        public string url { get; set; }
    }

    public class CommentsPlusoners
    {
        public CommentsPlusoners()
        {
            totalItems = 0;
        }

        public int totalItems { get; set; }
    }

    public class AddCommentResponseObject
    {
        public AddCommentResponseObject()
        {
            objectType = string.Empty;
            content = string.Empty;
            originalContent = string.Empty;
        }

        public string objectType { get; set; }
        public string content { get; set; }
        public string originalContent { get; set; }
    }

    public class MoreDetail
    {
        public MoreDetail()
        {
            domain = string.Empty;
            reason = string.Empty;
            message = string.Empty;
        }
        public string domain { get; set; }
        public string reason { get; set; }
        public string message { get; set; }

        public override string ToString()
        {
            if (!string.IsNullOrEmpty(domain) || !string.IsNullOrEmpty(reason) || !string.IsNullOrEmpty(message))
            {
                string str = "domain : " + domain + " , reason :" + reason + " , message :" + message;
                return str;
            }
            else
            {
                return string.Empty;
            }
        }
    }    

    public class GooglePlusError
    {
        public GooglePlusError()
        {
            errors = new List<MoreDetail>();
            code = 0;
            message = string.Empty;
        }

        public List<MoreDetail> errors { get; set; }
        public int code { get; set; }
        public string message { get; set; }

        public override string ToString()
        {
            if (code != 0 || !string.IsNullOrEmpty(message) || errors.Count > 0)
            {
                string str = "code : " + code + " , Message :" + message + " , errors" + errors.ToString();
                return str;
            }
            else
            {
                return string.Empty;
            }


        }
    }

    public class GooglePlusResponse
    {
        public GooglePlusResponse()
        {
            error = new GooglePlusError();
            tokenError = new GooglePlusErrorResponse();
        }

        public GooglePlusError error { get; set; }
        public GooglePlusErrorResponse tokenError { get; set; } // Use this to get error from TokenResponse     
    }

    [DataContract]
    public class GooglePlusAddActivityRequest
    {

        public GooglePlusAddActivityRequest()
        {
            obj = new GooglePlusAddActivityObject();
            access = new GooglePlusAddActivityAccess();
            verb = string.Empty;
        }

        [DataMember(Name = "object")]
        public GooglePlusAddActivityObject obj { get; set; }
        [DataMember(Name = "access")]
        public GooglePlusAddActivityAccess access { get; set; }
        [DataMember(Name = "verb")]
        public string verb { get; set; }
    }

    public class GooglePlusAddActivityObject
    {
        public GooglePlusAddActivityObject()
        {
            attachments = new List<GooglePlusAddActivityAttachment>();
            originalContent = string.Empty;
        }

        public List<GooglePlusAddActivityAttachment> attachments { get; set; }
        public string originalContent { get; set; }
    }

    public class GooglePlusAddActivityAttachment
    {
        public GooglePlusAddActivityAttachment()
        {
            id = string.Empty;
            url = string.Empty;
            objectType = string.Empty;
        }
        public string id { get; set; }
        public string url { get; set; }
        public string objectType { get; set; }
    }

    public class GooglePlusAddActivityAccess
    {
        public GooglePlusAddActivityAccess()
        {
            items = new List<GooglePlusAddActivityItem>();
        }
        public List<GooglePlusAddActivityItem> items { get; set; }
    }

    public class GooglePlusAddActivityItem
    {
        public GooglePlusAddActivityItem()
        {
            type = string.Empty;
            id = string.Empty;
        }

        public string type { get; set; }
        public string id { get; set; }
    }

    public class GooglePlusRequest
    {
        public GooglePlusRequest()
        {
            RequestURL = string.Empty;
            Parameters = string.Empty;
            Method = string.Empty;
            Status = string.Empty;
            Filename = string.Empty;
            Message = string.Empty;
            TimeOut = 0;

        }
        public string RequestURL { get; set; }
        public string Parameters { get; set; }
        public string Method { get; set; }
        public int TimeOut { get; set; }
        public byte[] bytes { get; set; }
        public string Filename { get; set; }
        public string Message { get; set; }
        public string Status { get; set; }
    }

    public class GooglePlusUploadPhotoResponse
    {
        public GooglePlusUploadPhotoResponse()
        {
            author = new Author();
            statusForViewer = new AddPhotoStatusForViewer();
            error = new GooglePlusError();
            kind = string.Empty;
            etag = string.Empty;
            id = string.Empty;
            published = string.Empty;
            height = 0;
            width = 0;
            mediaUrl = string.Empty;
            sizeBytes = string.Empty;
        }

        public string kind { get; set; }
        public string etag { get; set; }
        public string id { get; set; }
        public string published { get; set; }
        public Author author { get; set; }
        public int height { get; set; }
        public int width { get; set; }
        public string mediaUrl { get; set; }
        public string sizeBytes { get; set; }
        public AddPhotoStatusForViewer statusForViewer { get; set; }
        public GooglePlusError error { get; set; }
    }

    public class AddPhotoStatusForViewer
    {
        public AddPhotoStatusForViewer()
        {
            canComment = false;
            canPlusone = false;
        }
        public bool canComment { get; set; }
        public bool canPlusone { get; set; }
    }

    public class Author
    {
        public Author()
        {
            id = string.Empty;
            displayName = string.Empty;
            url = string.Empty;
        }
        public string id { get; set; }
        public string displayName { get; set; }
        public string url { get; set; }
        public Image image { get; set; }
    }

    #region GOOGLEPLUS GET ACTIVITY LIST RESPONSE

    public class GooglePlusGetActivityListByPageResponse
    {
        public GooglePlusGetActivityListByPageResponse()
        {
            items = new List<ActivityItem>();
            error = new GooglePlusError();
            kind = string.Empty;
            etag = string.Empty;
            nextPageToken = string.Empty;
            title = string.Empty;
        }
        public string kind { get; set; }
        public string etag { get; set; }
        public string nextPageToken { get; set; }
        public string title { get; set; }
        public List<ActivityItem> items { get; set; }
        public GooglePlusError error { get; set; }

    }

    public class ActivityItem
    {
        public ActivityItem()
        {
            actor = new Actor();
            @object = new Object();
            provider = new Provider();
            access = new Access();
            kind = string.Empty;
            etag = string.Empty;
            title = string.Empty;
            published = string.Empty;
            updated = string.Empty;
            id = string.Empty;
            url = string.Empty;
            verb = string.Empty;
        }
        public string kind { get; set; }
        public string etag { get; set; }
        public string title { get; set; }
        public string published { get; set; }
        public string updated { get; set; }
        public string id { get; set; }
        public string url { get; set; }
        public Actor actor { get; set; }
        public string verb { get; set; }
        public Object @object { get; set; }
        public Provider provider { get; set; }
        public Access access { get; set; }
    }
    #endregion

    public class GooglePlusGetPlusonersListResponse
    {
        public GooglePlusGetPlusonersListResponse()
        {
            items = new List<GooglePlusGetPlusonersListItem>();
            error = new GooglePlusError();
            kind = string.Empty;
            etag = string.Empty;
            title = string.Empty;
        }

        public string kind { get; set; }
        public string etag { get; set; }
        public string title { get; set; }
        public List<GooglePlusGetPlusonersListItem> items { get; set; }
        public GooglePlusError error { get; set; }
    }
    public class GooglePlusGetPlusonersListItem
    {
        public GooglePlusGetPlusonersListItem()
        {
            kind = string.Empty;
            etag = string.Empty;            
            id = string.Empty;
            displayName = string.Empty;            
            url = string.Empty;
        }
        public string kind { get; set; }
        public string etag { get; set; }        
        public string id { get; set; }
        public string displayName { get; set; }
        public string url { get; set; }        
    }

    public class GooglePlusGetResharersListResponse
    {
        public GooglePlusGetResharersListResponse()
        {
            items = new List<GooglePlusGetResharersListItem>();
            error = new GooglePlusError();
            kind = string.Empty;
            etag = string.Empty;
            title = string.Empty;
        }

        public string kind { get; set; }
        public string etag { get; set; }
        public string title { get; set; }
        public List<GooglePlusGetResharersListItem> items { get; set; }
        public GooglePlusError error { get; set; }
    }
    public class GooglePlusGetResharersListItem
    {
        public GooglePlusGetResharersListItem()
        {
            kind = string.Empty;
            etag = string.Empty;
            id = string.Empty;
            displayName = string.Empty;
            url = string.Empty;
        }
        public string kind { get; set; }
        public string etag { get; set; }
        public string id { get; set; }
        public string displayName { get; set; }
        public string url { get; set; }
    }

    public class GooglePlusGetCommentsListResponse
    {
        public GooglePlusGetCommentsListResponse()
        {
            items = new List<GooglePlusGetCommentsListItem>();
            error = new GooglePlusError();
            kind = string.Empty;
            etag = string.Empty;
            title = string.Empty;
        }

        public string kind { get; set; }
        public string etag { get; set; }
        public string title { get; set; }
        public List<GooglePlusGetCommentsListItem> items { get; set; }
        public GooglePlusError error { get; set; }
    }

    public class GooglePlusGetCommentsListItem
    {
        public GooglePlusGetCommentsListItem()
        {
            actor = new Actor();
            @object = new CommentsListObject();
            inReplyTo = new List<InReplyTo>();
            plusoners = new CommentsPlusoners();
            kind = string.Empty;
            etag = string.Empty;
            verb = string.Empty;
            id = string.Empty;
            published = string.Empty;
            updated = string.Empty;
            selfLink = string.Empty;
        }
        public string kind { get; set; }
        public string etag { get; set; }
        public string verb { get; set; }
        public string id { get; set; }
        public string published { get; set; }
        public string updated { get; set; }
        public Actor actor { get; set; }
        public CommentsListObject @object { get; set; }
        public string selfLink { get; set; }
        public List<InReplyTo> inReplyTo { get; set; }
        public CommentsPlusoners plusoners { get; set; }

    }

    public class CommentsListObject
    {
        public CommentsListObject()
        {
            objectType = string.Empty;
            content = string.Empty;
        }
        public string objectType { get; set; }
        public string content { get; set; }
    }

    public class GooglePlusGetSpecificPageResponse
    {
        public GooglePlusGetSpecificPageResponse()
        {
            image = new Image();
            error = new GooglePlusError();
            kind = string.Empty;
            etag = string.Empty;
            objectType = string.Empty;
            displayName = string.Empty;
            tagline = string.Empty;
            aboutMe = string.Empty;
            url = string.Empty;
            plusOneCount = 0;
            circledByCount = 0;
            verified = false;
        }

        public string kind { get; set; }
        public string etag { get; set; }
        public string objectType { get; set; }
        public string id { get; set; }
        public string displayName { get; set; }
        public string tagline { get; set; }
        public string aboutMe { get; set; }
        public string url { get; set; }
        public Image image { get; set; }
        public bool isPlusUser { get; set; }
        public int plusOneCount { get; set; }
        public int circledByCount { get; set; }
        public bool verified { get; set; }
        public GooglePlusError error { get; set; }
    }

    public class GooglePlusPageStatistics
    {
        public string UniqueID { get; set; }
        public int PlusOneCount { get; set; }
        public int FollowerCount { get; set; }
        public int PostCount { get; set; }
        public string Error { get; set; }

    }

    public class GooglePlusPostStatistics
    {
        public GooglePlusPostStatistics()
        {
            ActivityInfo = new GooglePlusAddorUpdateorGetSpecificActivityResponse();
            Comments = new GooglePlusGetCommentsListResponse();
            Plusoners = new GooglePlusGetPlusonersListResponse();
            Resharers = new GooglePlusGetResharersListResponse();
        }

        public GooglePlusAddorUpdateorGetSpecificActivityResponse ActivityInfo { get; set; }
        public GooglePlusGetCommentsListResponse Comments { get; set; }
        public GooglePlusGetPlusonersListResponse Plusoners { get; set; }
        public GooglePlusGetResharersListResponse Resharers { get; set; }
    }

    [DataContract]
    public class GooglePlusAddCommentRequest
    {

        public GooglePlusAddCommentRequest()
        {
            obj = new GooglePlusAddCommentObject();
        }
        [DataMember(Name = "object")]
        public GooglePlusAddCommentObject obj { get; set; }
    }

    public class GooglePlusAddCommentObject
    {
        public GooglePlusAddCommentObject()
        {
            originalContent = string.Empty;
        }
        public string originalContent { get; set; }
    }
}
