﻿using Connectors.Reviews;
using JobLib;
using SI;
using SI.BLL;
using SI.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace Processor.ReviewIntegrator
{
    public class Processor : ProcessorBase<ReviewIntegratorData>
    {
        public Processor(string[] args)
            : base(args)
        {
            UserInfoDTO mUser = new UserInfoDTO(1, null);
            mUser.EffectiveUser.SuperAdmin = true;
            _reviewSources = ProviderFactory.Reviews.GetReviewSources(mUser);
        }

        private static List<ReviewSourceDTO> _reviewSources = null;

        protected override void start()
        {
            JobDTO job = getNextJob();
            while (job != null)
            {
                try
                {
                    setJobStatus(job.ID.Value, JobStatusEnum.Started);
                    ReviewIntegratorData data = getJobData(job.ID.Value);

                    // get all raw and db reviews for a given account ----------------------------------------------
                    RawReviewDataDTO rawData = ProviderFactory.Reviews.GetRawReviewDataForAccountAndSource(data.AccountID, data.ReviewSourceID, 0);
                    List<ReviewDTO> rawList = rawData.RawReviews;
                    if (rawList == null)
                        rawList = new List<ReviewDTO>();

                    BatchReviewIntegratorPullDTO pull = ProviderFactory.Reviews.GetReviewsForAccountAndSource(data.AccountID, data.ReviewSourceID);

                    // calc the first 20 words of each review so avoid duplicate effort ----------------------------
                    Dictionary<long, List<string>> rawWords = new Dictionary<long, List<string>>();
                    Dictionary<long, List<string>> dbWords = new Dictionary<long, List<string>>();


                    foreach (ReviewDTO review in rawList)
                    {
                        //rawWords.Add(review.ID.Value, _validWordCharsRegex.Replace(review.ReviewText.ToLower(), "").Split(new char[] { ' ' }, 20, StringSplitOptions.RemoveEmptyEntries).ToList());
                        rawWords.Add(review.ID.Value, ReviewMatching.GetWordList(review.ReviewText));
                    }

                    // need to check for raw dupes before proceeding with regular matching
                    int i = rawList.Count() - 1;
                    while (i >= 1)
                    {
                        if (rawList.Count() > 1)
                        {
                            for (int j = 0; j < i; j++)
                            {
                                ReviewDTO review1 = rawList[i];
                                ReviewDTO review2 = rawList[j];
                                List<string> wordlist1 = rawWords[review1.ID.Value];
                                List<string> wordlist2 = rawWords[review2.ID.Value];

                                if (ReviewMatching.ReviewsMatch(review1, wordlist1, review2, wordlist2, 10))
                                {
                                    rawList.RemoveAt(i);
                                    rawWords.Remove(review1.ID.Value);
                                    break;
                                }
                            }
                        }
                        i--;
                    }



                    foreach (ReviewDTO review in pull.Reviews)
                    {
                        //dbWords.Add(review.ID.Value, _validWordCharsRegex.Replace(review.ReviewText.ToLower(), "").Split(new char[] { ' ' }, 20, StringSplitOptions.RemoveEmptyEntries).ToList());
                        dbWords.Add(review.ID.Value, ReviewMatching.GetWordList(review.ReviewText));
                    }

                    // our match collection is review source agnostic

                    Dictionary<long, long> matches = new Dictionary<long, long>();

                    // we will handle processing one review source at a time.
                    // to match, reviews must come from the same source

                    // set up lists we can chip away at for the nested loop ----------------------------------------

                    List<ReviewDTO> rawToCheck = new List<ReviewDTO>(rawList);
                    List<ReviewDTO> dbToCheck = new List<ReviewDTO>(pull.Reviews);

                    if (rawToCheck != null)
                    {
                        for (int indexRaw = rawToCheck.Count() - 1; indexRaw >= 0; indexRaw--)
                        {
                            ReviewDTO rawReview = rawToCheck[indexRaw];
                            List<string> rawWordList = rawWords[rawReview.ID.Value];
                            for (int indexDB = dbToCheck.Count() - 1; indexDB >= 0; indexDB--)
                            {
                                ReviewDTO dbReview = dbToCheck[indexDB];
                                List<string> dbWordList;
                                if (dbWords.TryGetValue(dbReview.ID.Value, out dbWordList))
                                {
                                    int maxDaysApartToMatch = 0;
                                    switch ((ReviewSourceEnum)data.ReviewSourceID)
                                    {
                                        case ReviewSourceEnum.Google:
                                            maxDaysApartToMatch = 720;
                                            break;

                                        case ReviewSourceEnum.YellowPages:
                                            maxDaysApartToMatch = 1;
                                            break;

                                        default:
                                            maxDaysApartToMatch = 0;
                                            break;
                                    }
                                    if (ReviewMatching.ReviewsMatch(rawReview, rawWordList, dbReview, dbWordList, maxDaysApartToMatch))
                                    {
                                        matches.Add(rawReview.ID.Value, dbReview.ID.Value);
                                        dbToCheck.RemoveAt(indexDB);
                                        break;
                                    }
                                }
                            }
                        }
                    }

                    List<ReviewDTO> toInsert = (from l in rawList where !matches.Keys.Contains(l.ID.Value) select l).ToList();
                    List<ReviewDTO> toUpdate = (from l in rawList where matches.Keys.Contains(l.ID.Value) select l).ToList();

                    List<ReviewDTO> allCurrent = new List<ReviewDTO>();

                    allCurrent.AddRange(toInsert);
                    allCurrent.AddRange(toUpdate);

                    List<long> toFilter = (from l in pull.Reviews where !matches.Values.Contains(l.ID.Value) && !l.Filtered select l.ID.Value).ToList();
                    List<long> toUnFilter = (from l in pull.Reviews where matches.Values.Contains(l.ID.Value) && l.Filtered select l.ID.Value).ToList();


                    BatchReviewIntegratorUpdateDTO batch = new BatchReviewIntegratorUpdateDTO();
                    batch.ReviewIDsSetFiltered = toFilter;
                    batch.ReviewIDsSetUnfiltered = toUnFilter;

                    //always delete all raw reviews we've inspected as part of the batch
                    //batch.RawReviewIDsToDelete = (from l in rawList select l.ID.Value).ToList();

                    foreach (ReviewDTO dto in toInsert)
                    {
                        dto.ID = null;
                        batch.ReviewsToSave.Add(dto);
                    }

                    foreach (ReviewDTO rawReview in toUpdate)
                    {
                        ReviewDTO dbReview = (from l in pull.Reviews where l.ID.Value == matches[rawReview.ID.Value] select l).First();

                        string dbReviewText = "";
                        string rawReviewText = "";
                        if (!string.IsNullOrWhiteSpace(dbReview.ReviewText))
                            dbReviewText = dbReview.ReviewText.Trim();
                        if (!string.IsNullOrWhiteSpace(rawReview.ReviewText))
                            rawReviewText = rawReview.ReviewText.Trim();

                        // we only want to update if certain things have changed that we are OK to update
                        bool doChange = false;
                        //if (dbReview.ReviewDate != rawReview.ReviewDate) doChange = true;
                        if (!doChange && dbReview.Rating != rawReview.Rating) doChange = true;
                        if (!doChange && dbReviewText.Length != rawReviewText.Length) doChange = true;

                        if (doChange)
                        {
                            // shift the id to the id in the db record, this will flag it as a save for that record...
                            rawReview.ID = dbReview.ID.Value;
                            rawReview.DateEntered = dbReview.DateEntered;
                            rawReview.DateUpdated = SIDateTime.Now;
                            batch.ReviewsToSave.Add(rawReview);
                        }
                    }

                    // calculate the current summary
                    ReviewSummaryDTO summary = new ReviewSummaryDTO()
                    {
                        AccountID = data.AccountID,
                        ReviewSourceID = data.ReviewSourceID,
                        NewReviewsCount = toInsert.Count(),
                        ReviewSummaryDataSource = ReviewSummaryDataSourceEnum.Scrape,
                        SummaryDate = SIDateTime.Now
                    };

                    if (allCurrent != null)
                    {
                        summary.TotalNegativeReviews = (from t in allCurrent where Convert.ToDouble(t.Rating) <= 2.5 && t.Rating > 0 select t).Count();
                        summary.TotalPositiveReviews = (from t in allCurrent where Convert.ToDouble(t.Rating) > 2.5 && t.Rating > 0 select t).Count();
                    }
                    if (toInsert.Any())
                    {
                        summary.NewReviewsAverageRating = Convert.ToDouble((from t in toInsert select t.Rating).Average());
                        summary.NewReviewsNegativeCount = (from t in toInsert where Convert.ToDouble(t.Rating) <= 2.5 && t.Rating > 0 select t).Count();
                        summary.NewReviewsPositiveCount = (from t in toInsert where Convert.ToDouble(t.Rating) > 2.5 select t).Count();
                        summary.NewReviewsRatingSum = (from t in toInsert select Convert.ToDouble(t.Rating)).Sum();
                    }

                    if (rawData.Summary != null)
                    {
                        summary.SalesAverageRating = Convert.ToDouble(rawData.Summary.SalesAverageRating);
                        summary.ServiceAverageRating = Convert.ToDouble(rawData.Summary.ServiceAverageRating);
                        summary.SalesReviewCount = rawData.Summary.SalesReviewCount.GetValueOrDefault(0);
                        summary.ServiceReviewCount = rawData.Summary.ServiceReviewCount.GetValueOrDefault(0);
                    }

                    batch.ReviewSummaries.Add(summary);
                    pull.Summaries.Add(summary);

                    DateTime fixedStartDate = new DateTime(2012, 1, 1);

                    //find the earliest date
                    DateTime? earliestDate = (
                        from s in pull.Summaries
                        orderby s.SummaryDate.LocalDate.Value ascending
                        select s.SummaryDate.LocalDate
                    ).FirstOrDefault();

                    // check to see if we need to backfill
                    if (earliestDate > fixedStartDate)
                    {
                        DateTime currentBackfillDate = fixedStartDate;
                        while (currentBackfillDate < earliestDate)
                        {
                            batch.ReviewSummaries.Add(CreateBackfillFromDetailData(currentBackfillDate, allCurrent, data.ReviewSourceID, data.AccountID));
                            currentBackfillDate = currentBackfillDate.AddDays(1);
                        }
                    }

                    // check for required gap fills

                    List<SummaryGap> gaps = new List<SummaryGap>();
                    foreach (ReviewSummaryDTO end in pull.Summaries)
                    {
                        if (end.HasAfter && !end.HasBefore)
                        {
                            ReviewSummaryDTO begin = (
                                from rs2 in pull.Summaries
                                where rs2.HasBefore && !rs2.HasAfter
                                && rs2.SummaryDate.LocalDate.Value < end.SummaryDate.LocalDate.Value
                                orderby rs2.SummaryDate.LocalDate.Value descending
                                select rs2
                            ).FirstOrDefault();

                            if (begin != null && begin.SalesAverageRating > 0.0 && end.SalesAverageRating > 0.0)
                            {
                                gaps.Add(new SummaryGap() { Begin = begin, End = end });
                            }
                        }
                    }

                    // fill the gaps
                    foreach (SummaryGap gap in gaps)
                    {
                        List<ReviewSummaryDTO> fills = CreateGapFillSummaries(gap);
                        if (fills != null && fills.Any())
                        {
                           batch.ReviewSummaries.AddRange(fills);
                        }
                    }


                    //add the raw reviews to delete
                    if (rawData.RawReviews != null)
                    {
                        batch.RawReviewIDsToDelete = (from r in rawData.RawReviews select r.ID.GetValueOrDefault()).Distinct().ToList();
                    }

                    // update the reviews
                    ProviderFactory.Reviews.BatchReviewIntegratorUpdate(batch);



                    setJobStatus(job.ID.Value, JobStatusEnum.Completed);
                }
                catch (Exception ex)
                {
                    setJobStatus(job.ID.Value, JobStatusEnum.Failed);
                    ProviderFactory.Logging.LogException(ex, job.ID.Value);
                }

                job = getNextJob();
            }

        }

        private class SummaryGap
        {
            public ReviewSummaryDTO Begin { get; set; }
            public ReviewSummaryDTO End { get; set; }
        }

        private List<ReviewSummaryDTO> CreateGapFillSummaries(SummaryGap gap)
        {
            List<ReviewSummaryDTO> fills = new List<ReviewSummaryDTO>();

            // given a begin and end summary, create the fill records
            ReviewSummaryDTO begin = gap.Begin;
            ReviewSummaryDTO end = gap.End;

            double diff_days = ((end.SummaryDate.LocalDate.Value.Date - begin.SummaryDate.LocalDate.Value.Date).Days);

            double sales_average_rating_step = (end.SalesAverageRating - begin.SalesAverageRating) / diff_days;
            double service_average_rating_step = (end.ServiceAverageRating - begin.ServiceAverageRating) / diff_days;
            double sales_count_step = Convert.ToDouble(end.SalesReviewCount - begin.SalesReviewCount) / diff_days;
            double service_count_step = Convert.ToDouble(end.ServiceReviewCount - begin.ServiceReviewCount) / diff_days;
            double pos_review_count_step = Convert.ToDouble(end.TotalPositiveReviews - begin.TotalPositiveReviews) / diff_days;
            double neg_review_count_step = Convert.ToDouble(end.TotalNegativeReviews - begin.TotalNegativeReviews) / diff_days;

            double sales_average_rating = begin.SalesAverageRating;
            double service_average_rating = begin.ServiceAverageRating;
            double sales_count = begin.SalesReviewCount;
            double service_count = begin.ServiceReviewCount;
            double pos_review_count = begin.TotalPositiveReviews;
            double neg_review_count = begin.TotalNegativeReviews;


            DateTime curDate = begin.SummaryDate.LocalDate.Value.Date.AddDays(1);
            DateTime endDate = end.SummaryDate.LocalDate.Value.Date;
            ReviewSummaryDTO prevSummary = begin;

            while (curDate < endDate)
            {
                sales_average_rating += sales_average_rating_step;
                service_average_rating += service_average_rating_step;
                sales_count += sales_count_step;
                service_count += service_count_step;
                pos_review_count += pos_review_count_step;
                neg_review_count += neg_review_count_step;

                int posReviewCount = Convert.ToInt32(pos_review_count);
                int negReviewCount = Convert.ToInt32(neg_review_count);

                int newReviewCount = (posReviewCount + negReviewCount) - (prevSummary.TotalPositiveReviews + prevSummary.TotalNegativeReviews);
                int newPosCount = posReviewCount - prevSummary.TotalPositiveReviews;
                int newNegCount = negReviewCount - prevSummary.TotalNegativeReviews;

                if (newReviewCount < 0) newReviewCount = 0;
                if (newPosCount < 0) newPosCount = 0;
                if (newNegCount < 0) newNegCount = 0;

                ReviewSummaryDTO newSummary = new ReviewSummaryDTO()
                {
                    SummaryDate = SIDateTime.CreateSIDateTime(curDate, TimeZoneInfo.Utc),
                    ReviewSourceID = begin.ReviewSourceID,
                    AccountID = begin.AccountID,
                    ReviewSummaryDataSource = ReviewSummaryDataSourceEnum.Gapfill,

                    SalesAverageRating = sales_average_rating,
                    ServiceAverageRating = service_average_rating,
                    SalesReviewCount = Convert.ToInt32(sales_count),
                    ServiceReviewCount = Convert.ToInt32(service_count),

                    TotalPositiveReviews = posReviewCount,
                    TotalNegativeReviews = negReviewCount,
                    NewReviewsCount = newReviewCount,

                    NewReviewsNegativeCount = newNegCount,
                    NewReviewsPositiveCount = newPosCount,

                    HasAfter = true,
                    HasBefore = true
                };
                fills.Add(newSummary);

                prevSummary = newSummary;

                curDate = curDate.AddDays(1);
            }

            return fills;
        }

        private ReviewSummaryDTO CreateBackfillFromDetailData(DateTime reviewDate, List<ReviewDTO> allReviews, long reviewSourceID, long accountID)
        {
            reviewDate = new DateTime(reviewDate.Year, reviewDate.Month, reviewDate.Day);

            List<ReviewDTO> newReviews = (
                    from r in allReviews
                    where r.ReviewDate.LocalDate.Value >= reviewDate
                    && r.ReviewDate.LocalDate.Value < reviewDate.AddDays(1)
                    && r.Filtered == false
                    select r
                ).ToList();

            List<ReviewDTO> currentReviews = (
                    from r in allReviews
                    where r.ReviewDate.LocalDate.Value < reviewDate.AddDays(1)
                    && r.Filtered == false
                    select r
                ).ToList();


            ReviewSummaryDTO summary = new ReviewSummaryDTO()
            {
                AccountID = accountID,
                ReviewSourceID = reviewSourceID,
                NewReviewsCount = newReviews.Count(),
                ReviewSummaryDataSource = ReviewSummaryDataSourceEnum.Backfill,
                SummaryDate = new SIDateTime(reviewDate, TimeZoneInfo.Utc)
            };

            if (currentReviews != null && currentReviews.Any())
            {
                summary.TotalNegativeReviews = (from t in currentReviews where Convert.ToDouble(t.Rating) <= 2.5 && t.Rating > 0 select t).Count();
                summary.TotalPositiveReviews = (from t in currentReviews where Convert.ToDouble(t.Rating) > 2.5 && t.Rating > 0 select t).Count();
                summary.SalesAverageRating = Convert.ToDouble((from t in currentReviews select t.Rating).Average());
                summary.ServiceAverageRating = Convert.ToDouble((from t in currentReviews select t.Rating).Average());
                summary.SalesReviewCount = (from t in currentReviews select t.Rating).Count();
                summary.ServiceReviewCount = (from t in currentReviews select t.Rating).Count();
            }
            else
            {
                summary.TotalNegativeReviews = 0;
                summary.TotalPositiveReviews = 0;
                summary.SalesAverageRating = 0;
                summary.ServiceAverageRating = 0;
                summary.SalesReviewCount = 0;
                summary.ServiceReviewCount = 0;
            }
            if (newReviews.Any())
            {
                summary.NewReviewsAverageRating = Convert.ToDouble((from t in newReviews select t.Rating).Average());
                summary.NewReviewsNegativeCount = (from t in newReviews where Convert.ToDouble(t.Rating) <= 2.5 && t.Rating > 0 select t).Count();
                summary.NewReviewsPositiveCount = (from t in newReviews where Convert.ToDouble(t.Rating) > 2.5 select t).Count();
                summary.NewReviewsRatingSum = (from t in newReviews select Convert.ToDouble(t.Rating)).Sum();
            }
            else
            {
                summary.NewReviewsAverageRating = 0;
                summary.NewReviewsNegativeCount = 0;
                summary.NewReviewsPositiveCount = 0;
                summary.NewReviewsRatingSum = 0;
            }

            return summary;
        }
    }
}
