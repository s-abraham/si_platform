﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AWS;
using JobLib;
using Newtonsoft.Json;
using SI.BLL;
using SI.DTO;
using Twitter.Entities;

namespace Processor.Social.TwitterIntegrator
{
    public class Processor : ProcessorBase<TwitterIntegratorData>
    {
        private const string prefix = "AWS.";

        public Processor(string[] args) : base(args) { }

        protected override void start()
        {
            string accessKey = ProviderFactory.Reviews.GetProcessorSetting(prefix + "AWSAccessKey");
            string secretKey = ProviderFactory.Reviews.GetProcessorSetting(prefix + "AWSSecretKey");
            string serviceURL = ProviderFactory.Reviews.GetProcessorSetting(prefix + "DynamoDBServiceURL");
            string table = ProviderFactory.Reviews.GetProcessorSetting(prefix + "DynamoDBSocialRawTable");

            DynamoDB dynamoDB = new DynamoDB(accessKey, secretKey, serviceURL, table);

            JobDTO job = getNextJob();

            while (job != null)
            {
                try
                {
                    setJobStatus(job.ID.Value, JobStatusEnum.Started);
                    TwitterIntegratorData data = getJobData(job.ID.Value);

                    //Get the data from dynamoDB based on Key 
                    var dynDBResult = dynamoDB.GetValue(data.Key);
                    if (dynDBResult.IsSuccess)
                    {
                        switch (data.TwitterActionEnum)
                        {
                            case TwitterProcessorActionEnum.TwitterPageInformationIntegrator:
                                {
                                    #region TwitterPageInformationIntegrator
                                    TwitterPageInsightsResponse objTwitterPageInsightsResponse = JsonConvert.DeserializeObject<TwitterPageInsightsResponse>(dynDBResult.Value);

                                    if (objTwitterPageInsightsResponse != null)
                                    {
                                        bool Success = ProviderFactory.Social.SaveTwitterPage(data.CredentialID.Value, objTwitterPageInsightsResponse.statuses_count
                                                                                                                , objTwitterPageInsightsResponse.friends_count
                                                                                                                , objTwitterPageInsightsResponse.followers_count
                                                                                                                , objTwitterPageInsightsResponse.listed_count
                                                                                                                , objTwitterPageInsightsResponse.favourites_count
                                                                                                                , objTwitterPageInsightsResponse.friends_count
                                                                                                                , objTwitterPageInsightsResponse.status.retweet_count
                                                                                                                , objTwitterPageInsightsResponse.errors.error);

                                        if (!Success)
                                        {
                                            setJobStatus(job.ID.Value, JobStatusEnum.Failed);
                                            ProviderFactory.Logging.Audit(AuditLevelEnum.Warning, AuditTypeEnum.FacebookIntegratorFailure,
                                                                            string.Format("Could not save Twitter Page Information DynamoDB Key: {0}, JobID {1}", data.Key, job.ID), null, null, null, data.CredentialID);
                                        }
                                        else
                                        {
                                            setJobStatus(job.ID.Value, JobStatusEnum.Completed);
                                        }
                                    }
                                    else
                                    {
                                        setJobStatus(job.ID.Value, JobStatusEnum.Failed);
                                        ProviderFactory.Logging.Audit(AuditLevelEnum.Warning, AuditTypeEnum.FacebookIntegratorFailure,
                                                                        string.Format("Deserialized Page object failure DynamoDB Key: {0}. JobID {1}", data.Key, job.ID));
                                    }
                                    #endregion
                                }
                                break;
                            case TwitterProcessorActionEnum.TwitterPostStatisticsIntegrator:
                                {
                                    #region TwitterPostStatisticsIntegrator
                                    TwitterPostStatistics PostStatistics = JsonConvert.DeserializeObject<TwitterPostStatistics>(dynDBResult.Value);

                                    if (PostStatistics != null)
                                    {
                                        bool Success = ProviderFactory.Social.SaveTwitterPostStatistics(data.PostTargetID, PostStatistics.IsFavorited, PostStatistics.RetweetCount, PostStatistics.IsRetweeted, PostStatistics.Error);

                                        if (!Success)
                                        {
                                            setJobStatus(job.ID.Value, JobStatusEnum.Failed);
                                            ProviderFactory.Logging.Audit(AuditLevelEnum.Warning, AuditTypeEnum.FacebookIntegratorFailure,
                                                                            string.Format("Could not save Twitter Post Statistics DynamoDB Key: {0}, JobID {1}", data.Key, job.ID), null, null, null, data.CredentialID);
                                        }
                                        else
                                        {
                                            setJobStatus(job.ID.Value, JobStatusEnum.Completed);
                                        }
                                    }
                                    else
                                    {
                                        setJobStatus(job.ID.Value, JobStatusEnum.Failed);
                                        ProviderFactory.Logging.Audit(AuditLevelEnum.Warning, AuditTypeEnum.FacebookIntegratorFailure,
                                                                        string.Format("Deserialized Post object failure DynamoDB Key: {0}. JobID {1}", data.Key, job.ID));
                                    }
                                    #endregion
                                }

                                break;                            
                        }
                    }
                    else
                    {
                        setJobStatus(job.ID.Value, JobStatusEnum.Failed);
                        ProviderFactory.Logging.Audit(AuditLevelEnum.Error, AuditTypeEnum.FacebookIntegratorFailure,
                                                      string.Format("Could not GetValue from DynamoDB Key: {0}. JobID {1}", data.Key, job.ID));
                    }
                }
                catch (Exception ex)
                {
                    setJobStatus(job.ID.Value, JobStatusEnum.Failed);
                    ProviderFactory.Logging.LogException(ex);
                }

                job = getNextJob();
            }
        }
    }
}
