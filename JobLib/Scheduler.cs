﻿using SI.BLL;
using SI.Core;
using SI.DTO;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace JobLib
{
    public class Scheduler : IDisposable
    {
        AbortableBackgroundWorker _workerMain = null;

        public void Start()
        {
            _workerMain = new AbortableBackgroundWorker();
            _workerMain.DoWork += _workerMain_DoWork;
            _workerMain.RunWorkerAsync();
        }
        void _workerMain_DoWork(object sender, DoWorkEventArgs e)
        {
            do
            {
                checkSchedules();
                Thread.Sleep(5000);
            } while (true);
        }

        private void checkSchedules()
        {
            try
            {

                List<ScheduleDTO> schedules = ProviderFactory.Jobs.GetSchedules();
                foreach (ScheduleDTO item in schedules)
                {
                    DateTime nextRunDate = item.ScheduleSpec.GetNextRunDate(item.DateLastExecuted);
                    if (nextRunDate <= DateTime.UtcNow)
                    {
                        Console.WriteLine("Queueing {0} for schedule {1}", item.JobType, item.Name);
                        JobDTO job = new JobDTO()
                        {
                            JobType = item.JobType,
                            JobData = item.JobData,
                            Priority = item.JobPriority,
                            DateScheduled = new SI.SIDateTime(DateTime.UtcNow, TimeZoneInfo.Utc),
                            JobStatus = JobStatusEnum.Queued
                        };

                        SaveEntityDTO<JobDTO> result = ProviderFactory.Jobs.Save(new SaveEntityDTO<JobDTO>(job, new UserInfoDTO(1, null)));

                        item.DateLastExecuted = DateTime.UtcNow;
                        ProviderFactory.Jobs.SaveSchedule(item);
                    }
                }

            }
            catch (Exception ex)
            {
                try
                {
                    ProviderFactory.Logging.LogException(ex);
                }
                catch (Exception) { }
            }
        }

        public void Stop()
        {
            _workerMain.Abort();
        }

        void IDisposable.Dispose()
        {
            Stop();
        }
    }
}
