﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using SI.Core;
using System.Threading;
using SI.BLL;
using System.Net;
using SI.DTO;
using System.Diagnostics;

namespace JobLib
{
    public class Controller
    {
        private string _machineName = Environment.MachineName;
        private string _ipAddress = "";
        private long _controllerID = 0;

        private object _jobTypeLocker = new object();
        private List<JobTypeDTO> _jobTypes = null;

        private List<ProcessorInstance> _instances = new List<ProcessorInstance>();

        AbortableBackgroundWorker _workerMain = null;

        // performance counters
        PerformanceCounter _cpuCounter = new PerformanceCounter("Processor", "% Processor Time", "_Total", true);
        PerformanceCounter _memCounter = new PerformanceCounter("Memory", "Available MBytes", true);


        public Controller()
        {
            IPAddress[] addresses = Dns.GetHostEntry(Dns.GetHostName()).AddressList;
            _ipAddress = (from x in addresses where x.AddressFamily == System.Net.Sockets.AddressFamily.InterNetwork select x.ToString()).FirstOrDefault();
            lock (_jobTypeLocker)
            {
                _jobTypes = ProviderFactory.Jobs.GetJobTypes();
            }
        }

        public void Start()
        {
            _workerMain = new AbortableBackgroundWorker();
            _workerMain.DoWork += _workerMain_DoWork;
            _workerMain.RunWorkerAsync();
        }

        void _workerMain_DoWork(object sender, DoWorkEventArgs e)
        {
            try
            {
                _cpuCounter.NextValue();
                _memCounter.NextValue();
            }
            catch (Exception)
            {
            }
            int i = 0;
            do
            {
                lock (_instances)
                {
                    int cpu = 0;
                    try
                    {
                        cpu = Convert.ToInt32(_cpuCounter.NextValue());
                    }
                    catch (Exception)
                    {
                    }

                    if (cpu <= 95)
                    {
                        checkForNewWork();
                    }
                    checkForDeadProcesses();
                    i++;
                    if (i >= 5)
                    {
                        i = 0;
                        _jobTypes = ProviderFactory.Jobs.GetJobTypes();
                        syncJobDatabase();
                        savePerfSnapshot();
                    }
                }

                Thread.Sleep(3000);
            } while (true);
        }

        void pi_ProcessComplete(object sender)
        {
            ProcessorInstance pi = (ProcessorInstance)sender;
            lock (_instances)
            {
                pi.ProcessComplete -= pi_ProcessComplete;
                _instances.Remove(pi);
                killProcessorInstance(pi);
                checkForNewWork();
            }

        }

        private void killProcessorInstance(ProcessorInstance pi)
        {
            try
            {
                pi.Process.CloseMainWindow();
            }
            catch (Exception) { }
            try
            {
                pi.Process.Close();
            }
            catch (Exception) { }
            try
            {
                pi.Process.Dispose();
            }
            catch (Exception) { }
            try
            {
                pi.Process.Kill();
            }
            catch (Exception) { }
        }

        private int savePerfSnapshot()
        {
            try
            {

                // get total cpu load and available ram
                float cpu = _cpuCounter.NextValue();
                float ram = _memCounter.NextValue();
                PerfSnapshotDTO snap = new PerfSnapshotDTO()
                {
                    ControllerID = this._controllerID,
                    CPULoad = Convert.ToInt32(cpu),
                    AvailableRAM = Convert.ToInt32(ram)
                };

                lock (_instances)
                {
                    foreach (ProcessorInstance inst in _instances)
                    {
                        try
                        {
                            snap.Jobs.Add(new PerfSnapshotJobDTO()
                            {
                                JobTypeID = inst.JobList.JobTypeID.Value,
                                CPULoad = 0
                            });
                        }
                        catch (Exception ex2)
                        {
                            ProviderFactory.Logging.LogException(ex2);
                            return snap.CPULoad;
                        }
                    }
                }

                ProviderFactory.Jobs.SavePerformanceSnapshot(snap);

                return snap.CPULoad;
            }
            catch (Exception ex)
            {
                ProviderFactory.Logging.LogException(ex);
                return 0;
            }
        }

        private void syncJobDatabase()
        {
            //if (_controllerID > 0)
            //{
            //    lock (_instances)
            //    {
            //        // get all the job IDs that we are acting upon...
            //        List<long> jobIDs = new List<long>();
            //        foreach (ProcessorInstance inst in _instances)
            //        {
            //            jobIDs.AddRange(inst.JobList.JobIDs);
            //        }

            //    }
            //}
        }

        private void checkForNewWork()
        {
            try
            {

                int currentLoad = getCurrentLoad();

                _controllerID = ProviderFactory.Jobs.RegisterController(_machineName, _ipAddress, currentLoad);
                ControllerDTO dto = ProviderFactory.Jobs.GetController(-1, _controllerID).Entity;

                if (currentLoad < dto.LoadRating)
                {
                    //we need more work

                    JobListDTO jlist = ProviderFactory.Jobs.GetJobList(_controllerID, dto.LoadRating - currentLoad);
                    if (jlist.JobIDs.Any() && jlist.JobTypeID.HasValue)
                    {
                        // log the pickup for diagnostic purposes
                        ProviderFactory.Jobs.LogJobPickups(_controllerID, jlist.JobIDs);

                        ProviderFactory.Logging.Audit(AuditLevelEnum.Information, AuditTypeEnum.ControllerAction,
                            string.Format("controller id [{2}] launching batch of [{0}] jobs of type [{1}]", jlist.JobIDs.Count(), jlist.JobTypeID, _controllerID)
                        );
                        lock (_instances)
                        {
                            ProcessorInstance pi = new ProcessorInstance(jlist);
                            pi.ProcessComplete += pi_ProcessComplete;
                            if (pi.Start())
                            {
                                _instances.Add(pi);
                            }
                            else
                            {
                                pi.ProcessComplete -= pi_ProcessComplete;
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                try
                {
                    ProviderFactory.Logging.LogException(ex);
                }
                catch (Exception)
                {
                }
            }
        }

        private void checkForDeadProcesses()
        {
            try
            {
                lock (_instances)
                {
                    for (int i = _instances.Count() - 1; i >= 0; i--)
                    {
                        ProcessorInstance instance = _instances[i];
                        long jobTypeID = instance.JobList.JobTypeID.Value;
                        int? maxSecsPerJob = null;
                        lock (_jobTypeLocker)
                        {
                            maxSecsPerJob = (from j in _jobTypes where j.ID == jobTypeID select j.MaxSecondsPerJob).FirstOrDefault();
                        }

                        if (!maxSecsPerJob.HasValue)
                            maxSecsPerJob = 60;
                        int secsTillKill = maxSecsPerJob.Value * instance.JobList.JobIDs.Count();


                        if (instance.DateCreated.AddSeconds(secsTillKill) < DateTime.Now)
                        {
                            killProcessorInstance(instance);
                            _instances.Remove(instance);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                try
                {
                    ProviderFactory.Logging.LogException(ex);
                }
                catch (Exception)
                {
                }
            }
        }

        public void Stop()
        {
            _workerMain.Abort();
        }

        protected int getCurrentLoad()
        {
            try
            {


                int currentLoad = 0;

                //get the current load
                lock (_instances)
                {
                    foreach (ProcessorInstance item in _instances)
                    {
                        lock (_jobTypeLocker)
                        {
                            currentLoad += (from t in _jobTypes where t.ID.Equals(item.JobList.JobTypeID.Value) select t.LoadRating).FirstOrDefault();
                        }
                    }
                }

                return currentLoad;
            }
            catch (Exception ex)
            {
                try
                {
                    ProviderFactory.Logging.LogException(ex);
                }
                catch (Exception)
                {
                }
            }
            return -1;
        }

    }
}
