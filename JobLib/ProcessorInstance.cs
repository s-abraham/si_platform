﻿using ICSharpCode.SharpZipLib.Zip;
using SI;
using SI.BLL;
using SI.Core;
using SI.DTO;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JobLib
{

    /// <summary>
    /// Launches and monitors a processor instance.  Uses the jobID list supplied
    /// to determine the processor JobType, pull teh bundle, and find the executable
    /// </summary>
    public class ProcessorInstance
    {
        public JobListDTO JobList { get; set; }
        public Process Process { get; set; }
        public string ExePathname { get; set; }
        public DateTime DateCreated { get; set; }

        public bool Running { get; set; }

        // performance counters
        
        #region events

        public delegate void ProcessCompleteHandler(object sender);
        public event ProcessCompleteHandler ProcessComplete;
        private void RaiseProcessComplete()
        {
            if (ProcessComplete != null)
                ProcessComplete(this);
        }

        #endregion

        private AbortableBackgroundWorker _worker = null;

        public ProcessorInstance(JobListDTO jlist)
        {
            try
            {
                Running = false;
                DateCreated = DateTime.Now;

                JobList = jlist;
                string jhash = ProviderFactory.Jobs.GetJobTypeBundleHash(jlist.JobTypeID.Value);
                verifyBundle(jhash);
                ExePathname = getExeName(jhash);
            }
            catch (Exception ex)
            {
                try
                {
                    ProviderFactory.Logging.LogException(ex);
                    Auditor
                        .New(AuditLevelEnum.Exception, AuditTypeEnum.ControllerException, new UserInfoDTO(1, null), "ProcessorInstance::ctor: {0}", ex.ToString())
                        .Save(ProviderFactory.Logging)
                    ;
                }
                catch (Exception) { }
            }

        }

        public bool Start()
        {
            try
            {
                if (string.IsNullOrWhiteSpace(ExePathname) || !File.Exists(ExePathname))
                    return false;

                _worker = new AbortableBackgroundWorker();
                _worker.DoWork += _worker_DoWork;
                _worker.RunWorkerAsync();

                return true;
            }
            catch (Exception ex)
            {
                try
                {
                    ProviderFactory.Logging.LogException(ex);
                    Auditor
                        .New(AuditLevelEnum.Exception, AuditTypeEnum.ControllerException, new UserInfoDTO(1, null), "ProcessorInstance::Start: {0}", ex.ToString())
                        .Save(ProviderFactory.Logging)
                    ;
                }
                catch (Exception) { }
            }
            return false;
        }
                
        void _worker_DoWork(object sender, System.ComponentModel.DoWorkEventArgs e)
        {
            try
            {
                Process = new Process();

                Process proc = Process;
                proc.StartInfo.FileName = ExePathname;
                proc.StartInfo.Arguments = String.Join(",", JobList.JobIDs);
                proc.StartInfo.CreateNoWindow = true;
                proc.StartInfo.WindowStyle = ProcessWindowStyle.Hidden;
                proc.StartInfo.UseShellExecute = false;
                proc.Start();

                Running = true;

                proc.WaitForExit();

                RaiseProcessComplete();

                Running = false;
            }
            catch (Exception ex)
            {
                try
                {
                    ProviderFactory.Logging.LogException(ex);
                    Auditor
                        .New(AuditLevelEnum.Exception, AuditTypeEnum.ControllerException, new UserInfoDTO(1, null), "ProcessorInstance::_worker_DoWork: {0}", ex.ToString())
                        .Save(ProviderFactory.Logging)
                    ;
                }
                catch (Exception) { }
            }

        }

        private void verifyBundle(string jhash)
        {
            try
            {
                string exeBase = Settings.GetSetting("processing.exepath");
                if (!Directory.Exists(exeBase))
                    Directory.CreateDirectory(exeBase);

                string bundlePath = string.Format("{0}\\{1}", exeBase, jhash);

                if (!Directory.Exists(bundlePath))
                {
                    Auditor
                        .New(AuditLevelEnum.Information, AuditTypeEnum.ControllerAction, new UserInfoDTO(1, null), "ProcessorInstance::verifyBundle: Adding _exe directory and loading bundle hash: {0}", jhash)
                        .Save(ProviderFactory.Logging)
                    ;

                    //we need the bundle
                    byte[] bundleData = ProviderFactory.Jobs.GetJobBundle(1, JobList.JobTypeID.Value);
                    Directory.CreateDirectory(bundlePath);

                    using (MemoryStream mStream = new MemoryStream(bundleData))
                    {
                        mStream.Position = 0;
                        using (ZipInputStream ziStream = new ZipInputStream(mStream))
                        {
                            ZipEntry entry = null;
                            while ((entry = ziStream.GetNextEntry()) != null)
                            {
                                string fileName = Path.GetFileName(entry.Name);
                                Auditor
                                    .New(AuditLevelEnum.Information, AuditTypeEnum.ControllerAction, new UserInfoDTO(1, null), "ProcessorInstance::verifyBundle: Reading bundled file: {0}", fileName)
                                    .Save(ProviderFactory.Logging)
                                ;

                                if (!string.IsNullOrWhiteSpace(fileName))
                                {
                                    string pathName = string.Format("{0}\\{1}", bundlePath, fileName);
                                    using (FileStream sWriter = File.Create(pathName))
                                    {
                                        Auditor
                                            .New(AuditLevelEnum.Information, AuditTypeEnum.ControllerAction, new UserInfoDTO(1, null), "ProcessorInstance::verifyBundle: Creating file: {0}", pathName)
                                            .Save(ProviderFactory.Logging)
                                        ;

                                        byte[] buffer = new byte[2048];
                                        int bytesRead;

                                        bytesRead = ziStream.Read(buffer, 0, buffer.Length);
                                        while (bytesRead > 0)
                                        {
                                            sWriter.Write(buffer, 0, bytesRead);
                                            bytesRead = ziStream.Read(buffer, 0, buffer.Length);
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                try
                {
                    ProviderFactory.Logging.LogException(ex);
                    Auditor
                        .New(AuditLevelEnum.Exception, AuditTypeEnum.ControllerException, new UserInfoDTO(1, null), "ProcessorInstance::verifyBundle: {0}", ex.ToString())
                        .Save(ProviderFactory.Logging)
                    ;
                }
                catch (Exception) { }
            }

        }

        private string getExeName(string jhash)
        {
            try
            {
                string exeBase = Settings.GetSetting("processing.exepath");
                string bundlePath = string.Format("{0}\\{1}", exeBase, jhash);

                return Directory.GetFiles(bundlePath, "*.exe").FirstOrDefault();
            }
            catch (Exception ex)
            {
                try
                {
                    ProviderFactory.Logging.LogException(ex);
                    Auditor
                        .New(AuditLevelEnum.Exception, AuditTypeEnum.ControllerException, new UserInfoDTO(1, null), "ProcessorInstance::getExeName: {0}", ex.ToString())
                        .Save(ProviderFactory.Logging)
                    ;
                }
                catch (Exception) { }
                return null;
            }

        }


    }
}













