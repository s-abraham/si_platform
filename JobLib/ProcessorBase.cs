﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using SI;
using SI.BLL;
using SI.DTO;

using Newtonsoft;

namespace JobLib
{
    public abstract class ProcessorBase<T> where T : JobDataBase
    {

        public ProcessorBase(string[] args)
        {
            List<long> jobIDs = new List<long>();
            foreach (string id in args[0].Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries))
            {
                long jobID = Convert.ToInt64(id);
                jobIDs.Add(jobID);
                ProviderFactory.Jobs.SetJobStatus(jobID, JobStatusEnum.Assigned);
            }

            List<JobDTO> jobs = ProviderFactory.Jobs.GetJobs(jobIDs);
            loadJobs(jobs);

        }

        public void Start()
        {
            try
            {
                start();
            }
            catch (Exception ex)
            {
                // we got an exception that shouldn't have happened.
                // in this case, log the exception, audit it, and set
                // add jobs in this batch to queued or parked where appropriate.

                try
                {
                    ProviderFactory.Logging.LogException(ex);
                }
                catch (Exception) { }

                try
                {
                    AuditProcessorException audit = new AuditProcessorException()
                    {
                        ExceptionMessage = ex.ToString(),
                        JobIDs = (from j in _jobs.Keys select j).ToList()
                    };
                    ProviderFactory.Logging.Audit(AuditLevelEnum.Exception, AuditTypeEnum.ProcessorException, "Uncaught processor exception on \"start\" method.", audit);
                }
                catch (Exception) { }


                try
                {
                    foreach (JobDTO job in _jobs.Values)
                    {
                        try
                        {
                            switch (job.JobStatus)
                            {
                                case JobStatusEnum.Queued:
                                    break;

                                case JobStatusEnum.PickedUp:
                                case JobStatusEnum.Assigned:
                                    ProviderFactory.Jobs.SetJobStatus(job.ID.Value, JobStatusEnum.Queued);
                                    break;

                                case JobStatusEnum.Started:
                                    ProviderFactory.Jobs.SetJobStatus(job.ID.Value, JobStatusEnum.Parked);
                                    break;

                                case JobStatusEnum.Completed:
                                case JobStatusEnum.Failed:
                                case JobStatusEnum.Parked:
                                default:
                                    break;
                            }
                        }
                        catch (Exception) { }
                    }
                }
                catch (Exception ex2)
                {
                    ProviderFactory.Logging.LogException(ex2);
                }
            }
        }

        private Queue<JobDTO> _jobQueue = new Queue<JobDTO>();
        private Dictionary<long, JobDTO> _jobs = new Dictionary<long, JobDTO>();
        private Dictionary<long, T> _datas = new Dictionary<long, T>();

        protected List<T> getAllJobDataItems()
        {
            return _datas.Values.ToList();
        }

        protected List<JobDTO> getAllJobs()
        {
            return _jobs.Values.ToList();
        }

        /// <summary>
        /// Loads the jobs into the processor
        /// </summary>
        /// <param name="jobs"></param>
        protected void loadJobs(List<JobDTO> jobs)
        {
            foreach (JobDTO job in jobs)
            {
                _jobQueue.Enqueue(job);
                _jobs.Add(job.ID.Value, job);
                try
                {
                    T jobData = Newtonsoft.Json.JsonConvert.DeserializeObject<T>(job.JobData);
                    _datas.Add(job.ID.Value, jobData);
                }
                catch (Exception ex)
                {
                    ProviderFactory.Logging.LogException(ex, job.ID.Value);
                    throw new Exception(ex.ToString(), ex);
                }                
            }
        }

        /// <summary>
        /// Gets the next job from the internal queue of jobs in the batch to perform.  Returns null if thee are no more jobs in the batch.
        /// </summary>
        protected JobDTO getNextJob()
        {
            lock (_jobQueue)
            {
                if (!_jobQueue.Any())
                    return null;
                return _jobQueue.Dequeue();
            }
        }

        protected JobDTO getJob(long jobID)
        {
            return _jobs[jobID];
        }

        /// <summary>
        /// Shortcut helper to set a job's status
        /// </summary>
        protected void setJobStatus(long jobID, JobStatusEnum status)
        {
            JobDataBase data = (JobDataBase)(object)getJobData(jobID);
            JobDTO job = getJob(jobID);

            ProviderFactory.Jobs.SetJobStatus(jobID, status);
            job.JobStatus = status;

            bool runChains = false;

            switch (status)
            {
                case JobStatusEnum.Completed:
                    {
                        runChains = true;
                    }
                    break;
                case JobStatusEnum.Failed:
                    {
                        // check for retries
                        if (data.RemainingRetries > 0)
                        {
                            data.RemainingRetries--;

                            JobDTO newJob = new JobDTO()
                            {
                                ID = null,
                                DateScheduled = new SIDateTime(DateTime.UtcNow.AddMinutes(1), 1),
                                JobType = data.JobType,
                                JobDataObject = data,
                                JobStatus = JobStatusEnum.Queued,
                                Priority = job.Priority,
                                ChainedJobs = job.ChainedJobs,
                                ContinueChainIfFailed = job.ContinueChainIfFailed
                            };

                            ProviderFactory.Jobs.Save(new SaveEntityDTO<JobDTO>(newJob, UserInfoDTO.System));
                        }
                        else
                        {
                            int remainingChainedJobs = 0;
                            if (job.ChainedJobs != null)
                                remainingChainedJobs = job.ChainedJobs.Count();

                            ProviderFactory.Logging.Audit(AuditLevelEnum.Warning, AuditTypeEnum.JobFailure, "Job Failure", new AuditJobFailure()
                            {
                                JobData = job.JobData,
                                JobID = job.ID.Value,
                                JobType = data.JobType,
                                RemainingChainedJobs = remainingChainedJobs
                            });

                            if (job.ContinueChainIfFailed)
                                runChains = true;
                        }
                    }
                    break;

                default:
                    break;
            }

            if (runChains)
            {
                if (job.ChainedJobs != null && job.ChainedJobs.Any())
                {
                    ChainedJobSpec spec = job.ChainedJobs.Dequeue();
                    //newJobData.JobChain = data.JobChain;

                    JobDTO newJob = new JobDTO()
                    {
                        ID = null,
                        DateScheduled = SIDateTime.Now,
                        OriginJobID = job.ID.Value,
                        JobType = spec.JobType,
                        JobData = spec.JobData,
                        JobStatus = JobStatusEnum.Queued,
                        Priority = spec.Priority,
                        ChainedJobs = job.ChainedJobs,
                        ContinueChainIfFailed = spec.ContinueChainIfFailed
                    };

                    ProviderFactory.Jobs.Save(new SaveEntityDTO<JobDTO>(newJob, new UserInfoDTO(0, null)));
                }
            }
        }

        /// <summary>
        /// Returns the job data for a given job
        /// </summary>
        /// <param name="jobID">Teh ID of the job to retrieve data for</param>        
        protected T getJobData(long jobID)
        {
            lock (_datas)
            {
                return _datas[jobID];
            }
        }

        /// <summary>
        /// Begin processing the batch in the implemented class
        /// </summary>
        protected abstract void start();

    }
}
