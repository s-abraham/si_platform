﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AWS;
//using Connectors.Entities;
using JobLib;
using Newtonsoft.Json;
using SI.BLL;
using SI.DTO;
using Facebook.Entities;

namespace Processor.Social.FacebookIntegrator
{
    public class Processor : ProcessorBase<FacebookIntegratorData>
    {
        private const string prefix = "AWS.";

        public Processor(string[] args) : base(args) { }

        protected override void start()
        {
            string accessKey = ProviderFactory.Reviews.GetProcessorSetting(prefix + "AWSAccessKey");
            string secretKey = ProviderFactory.Reviews.GetProcessorSetting(prefix + "AWSSecretKey");
            string serviceURL = ProviderFactory.Reviews.GetProcessorSetting(prefix + "DynamoDBServiceURL");
            string table = ProviderFactory.Reviews.GetProcessorSetting(prefix + "DynamoDBSocialRawTable");

            DynamoDB dynamoDB = new DynamoDB(accessKey, secretKey, serviceURL, table);

            JobDTO job = getNextJob();

            while (job != null)
            {
                try
                {
                    setJobStatus(job.ID.Value, JobStatusEnum.Started);
                    FacebookIntegratorData data = getJobData(job.ID.Value);

                    //Get the data from dynamoDB based on Key 
                    var dynDBResult = dynamoDB.GetValue(data.Key);
	                if (dynDBResult.IsSuccess)
	                {
		                switch (data.FacebookEnum)
		                {
                            case FacebookProcessorActionEnum.FacebookPageIntegrator:
                                {
                                    #region FacebookPageIntegrator
                                    Page facebookPageNew = JsonConvert.DeserializeObject<Page>(dynDBResult.Value);
                                    if (facebookPageNew != null)
                                    {
                                        bool Success = ProviderFactory.Social.SaveFaceBookPage(facebookPageNew, data.CredentialID);
                                        if (!Success)
                                        {
                                            setJobStatus(job.ID.Value, JobStatusEnum.Failed);
                                            ProviderFactory.Logging.Audit(AuditLevelEnum.Warning, AuditTypeEnum.FacebookIntegratorFailure,
                                                                          string.Format("Could not save FaceBookPages DynamoDB Key: {0}. JobID {1}", data.Key, job.ID), null, null, null, data.CredentialID);
                                        }
                                        else
                                        {
                                            setJobStatus(job.ID.Value, JobStatusEnum.Completed);
                                        }
                                    }
                                    else
                                    {
                                        setJobStatus(job.ID.Value, JobStatusEnum.Failed);
                                        ProviderFactory.Logging.Audit(AuditLevelEnum.Warning, AuditTypeEnum.FacebookIntegratorFailure,
                                                                      string.Format("Deserialized Page object failure DynamoDB Key: {0}. JobID {1}", data.Key, job.ID));
                                    }
                                    #endregion
                                }
                                break;

                            case FacebookProcessorActionEnum.FacebookPostIntegrator:
                                {
                                    #region FacebookPostIntegrator
                                    //PostStatistics facebookPost = JsonConvert.DeserializeObject<PostStatistics>(dynDBResult.Value);
                                    FacebookPostStatistics objFacebookPostStatisticsNew = JsonConvert.DeserializeObject<FacebookPostStatistics>(dynDBResult.Value);

                                    //ToDo : Add Audit Log for FaceBook Error.                                
                                    PostStatistics facebookPost = objFacebookPostStatisticsNew.postStatistics;

                                    if (facebookPost != null && facebookPost.id != null)
                                    {
                                        bool Success = ProviderFactory.Social.SaveFaceBookPost(facebookPost, data.CredentialID, data.FeedID, data.ResultID, data.FacebookPostID);
                                        if (!Success)
                                        {
                                            setJobStatus(job.ID.Value, JobStatusEnum.Failed);
                                            ProviderFactory.Logging.Audit(AuditLevelEnum.Warning, AuditTypeEnum.FacebookIntegratorFailure,
                                                                            string.Format("Could not save FaceBookPost DynamoDB Key: {0}, FeedID: {2}. JobID {1}", data.Key, job.ID, data.FeedID), null, null, null, data.CredentialID);
                                        }
                                        else
                                        {
                                            setJobStatus(job.ID.Value, JobStatusEnum.Completed);
                                        }
                                    }
                                    else
                                    {
                                        setJobStatus(job.ID.Value, JobStatusEnum.Failed);
                                        ProviderFactory.Logging.Audit(AuditLevelEnum.Warning, AuditTypeEnum.FacebookIntegratorFailure,
                                                                        string.Format("Deserialized Post object failure DynamoDB Key: {0}. JobID {1}", data.Key, job.ID));
                                    }
                                    #endregion
                                }
                                break;
                            case FacebookProcessorActionEnum.FacebookPostInsightsIntegrator:
                                {
                                    #region FacebookPostInsightsIntegrator
                                    //Insights facebookPostInsights = JsonConvert.DeserializeObject<Insights>(dynDBResult.Value);
                                    FacebookInsights objFacebookInsightsNew = JsonConvert.DeserializeObject<FacebookInsights>(dynDBResult.Value);

                                    Insights facebookPostInsights = objFacebookInsightsNew.insights;

                                    if (facebookPostInsights != null)
                                    {
                                        bool Success = ProviderFactory.Social.SaveFaceBookInsights(facebookPostInsights, data.CredentialID, DateTime.Now, data.FacebookPostID);
                                        if (!Success)
                                        {
                                            setJobStatus(job.ID.Value, JobStatusEnum.Failed);
                                            ProviderFactory.Logging.Audit(AuditLevelEnum.Warning, AuditTypeEnum.FacebookIntegratorFailure,
                                                                            string.Format("Could not save FaceBookPostInsights DynamoDB Key: {0}. JobID {1}", data.Key, job.ID), null, null, null, data.CredentialID);
                                        }
                                        else
                                        {
                                            setJobStatus(job.ID.Value, JobStatusEnum.Completed);
                                        }
                                    }
                                    else
                                    {
                                        setJobStatus(job.ID.Value, JobStatusEnum.Failed);
                                        ProviderFactory.Logging.Audit(AuditLevelEnum.Warning, AuditTypeEnum.FacebookIntegratorFailure,
                                                                        string.Format("Deserialized Post Insights object failure DynamoDB Key: {0}. JobID {1}", data.Key, job.ID));
                                    }
                                    #endregion
                                }
                                break;

                            case FacebookProcessorActionEnum.FacebookPageInsightsIntegrator:
                                {
                                    #region FacebookPageInsightsIntegrator
                                    //Insights facebookInsights = JsonConvert.DeserializeObject<Insights>(dynDBResult.Value);
                                    FacebookInsights objFacebookInsights = JsonConvert.DeserializeObject<FacebookInsights>(dynDBResult.Value);
                                    Insights facebookInsights = objFacebookInsights.insights;

                                    if (facebookInsights != null)
                                    {
                                        DateTime queryDate = Convert.ToDateTime(data.Since);

                                        bool Success = ProviderFactory.Social.SaveFaceBookInsights(facebookInsights, data.CredentialID, queryDate, null);

                                        if (!Success)
                                        {
                                            setJobStatus(job.ID.Value, JobStatusEnum.Failed);
                                            ProviderFactory.Logging.Audit(AuditLevelEnum.Warning, AuditTypeEnum.FacebookIntegratorFailure,
                                                                          string.Format("Could not save FaceBookPageInsights DynamoDB Key: {0}. JobID {1}", data.Key, job.ID), null, null, null, data.CredentialID);
                                        }
                                        else
                                        {
                                            setJobStatus(job.ID.Value, JobStatusEnum.Completed);
                                        }
                                    }
                                    else
                                    {
                                        setJobStatus(job.ID.Value, JobStatusEnum.Failed);
                                        ProviderFactory.Logging.Audit(AuditLevelEnum.Warning, AuditTypeEnum.FacebookIntegratorFailure,
                                                                      string.Format("Deserialized Page Insights object failure DynamoDB Key: {0}. JobID {1}", data.Key, job.ID));
                                    }
                                    #endregion
                                }
                                break;

                            
		                }
	                }
					else
					{
						setJobStatus(job.ID.Value, JobStatusEnum.Failed);
						ProviderFactory.Logging.Audit(AuditLevelEnum.Error, AuditTypeEnum.FacebookIntegratorFailure,
													  string.Format("Could not GetValue from DynamoDB Key: {0}. JobID {1}", data.Key, job.ID));
					}
                }
                catch (Exception ex)
                {
                    setJobStatus(job.ID.Value, JobStatusEnum.Failed);
                    ProviderFactory.Logging.LogException(ex, job.ID.Value);
                }

                job = getNextJob();
            }
        }
    }
}
