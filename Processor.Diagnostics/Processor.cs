﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SI.BLL;
using SI.DTO;
using JobLib;

namespace Processor.Diagnostics
{
    public class Processor : ProcessorBase<DiagnosticsProcessorData>
    {
        public Processor(string[] args) : base(args) { }

        protected override void start()
        {
            JobDTO job = getNextJob();
            while (job != null)
            {
                try
                {
                    setJobStatus(job.ID.Value, JobStatusEnum.Started);
                    DiagnosticsProcessorData data = getJobData(job.ID.Value);

                    switch (data.Action)
                    {

                        case DiagnosticProcessorActionEnum.PerformDatabaseCleanup:
                            ProviderFactory.Jobs.PerformDatabaseCleanup();
                            break;

                        case DiagnosticProcessorActionEnum.Send12HourReputationDiagReport:
                            ProviderFactory.Reviews.SendReviewDiagnosticsEmail();
                            break;

                        case DiagnosticProcessorActionEnum.ApplyDefaultNotificationSubscriptions:

                            #region Notification Subscription Check

                            List<PendingNotificationSubscriptionCheckDTO> checks = ProviderFactory.Notifications.GetPendingNotificationSubscriptionChecks();
                            foreach (PendingNotificationSubscriptionCheckDTO check in checks)
                            {
                                if (check.AccountID.HasValue)
                                {
                                    try
                                    {
                                        ProviderFactory.Notifications.ApplyDefaultNotificationSubscriptionsForAccount(check.AccountID.Value);
                                    }
                                    catch (Exception ex)
                                    {
                                        ProviderFactory.Logging.LogException(ex, job.ID.Value);
                                    }
                                }
                                if (check.UserID.HasValue)
                                {
                                    try
                                    {
                                        ProviderFactory.Notifications.ApplyDefaultNotificationSubscriptionsForUser(check.UserID.Value);
                                    }
                                    catch (Exception ex)
                                    {
                                        ProviderFactory.Logging.LogException(ex, job.ID.Value);
                                    }
                                }
                            }

                            #endregion

                            break;

                        case  DiagnosticProcessorActionEnum.ProcessPostApproverInits:
                            ProviderFactory.Social.ProcessPostApprovalInits();
                            break;

                        default:
                            break;
                    }
                    setJobStatus(job.ID.Value, JobStatusEnum.Completed);

                }
                catch (Exception ex)
                {
                    setJobStatus(job.ID.Value, JobStatusEnum.Failed);
                    ProviderFactory.Logging.LogException(ex, job.ID.Value);

                    Auditor
                        .New(AuditLevelEnum.Exception, AuditTypeEnum.ProcessorException, UserInfoDTO.System, "Diagnostics processor exception: {0}", ex.ToString())
                        .Save(ProviderFactory.Logging)
                    ;
                }
                job = getNextJob();
            }
        }
    }
}
