﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Web.Script.Serialization;
using SI;

namespace Bitly
{
    public abstract class BitlyAPI
    {
        protected static SInet _WebNet = null;

        protected static void AuthorizeApp()
        {
            var clientId = "0b5cc608f5c7c855b6d345bc533461123ea7ac60";
            var clientSecret = "53383dbde9c40127d03ea1787cff6177909c6974";
            var redirectURL = "http://www.socialdealer.com/";
            var state = "123";
            var baseURL = "https://bitly.com/oauth/authorize?client_id={0}&state={1}&redirect_uri={2}";
            var authorizeURL = String.Format(baseURL, clientId, state, redirectURL);
            var code = "0c46d696e89aaeaa3a8946ee66d58ec916c55c61";
            var accessToken = "d0b48eec5c0f1ec06de95d9ba41fe36f7393809c";

            //var response = SInet.GetFinalRedirectedUrl(authorizeURL);

            var baseOAuthBaseURL = "https://api-ssl.bitly.com/oauth/access_token?client_id={0}&client_secret={1}&code={2}&redirect_uri={3}&state={4}";
            var OAuthURL = string.Format(baseOAuthBaseURL, clientId, clientSecret, code, redirectURL, state);
            var response = SInet.GetFinalRedirectedUrl(OAuthURL);



            ////https://bitly.com/oauth/authorize?client_id=0b5cc608f5c7c855b6d345bc533461123ea7ac60&state=123&redirect_uri=http://www.socialdealer.com/

        }

        protected static string FixURL(string url)
        {
            var result = string.Empty;

            //Ensure that long urls have slash between domain name and path component

            var host = new System.Uri(url).Host;
            var domain = host.Substring(host.LastIndexOf('.', host.LastIndexOf('.') - 1) + 1);
            var pos1 = url.IndexOf(domain);
            var protocol = url.Substring(0, pos1);

            if (url.Substring(pos1 + domain.Length, 1) != "/")
            {
                result = domain + "/" + url.Substring(pos1 + domain.Length);
                result = protocol + result;
            }
            else
            {
                result = url;
            }

            //Ensure it is URL-Encoded and spaces converted (Convert to %20 or +)
            result = System.Web.HttpUtility.UrlEncode(result);

            return result;
        }

        protected static T ParseJSON<T>(string jsonInput)
        {
            var jss = new JavaScriptSerializer();
            return (T)jss.Deserialize<T>(jsonInput);
        }

        protected static string GetRequestString(string requestUrl)
        {
            if (_WebNet == null)
            {
                _WebNet = new SInet(false, 5, null, 60 * 1000);
            }

            return _WebNet.DownloadString(requestUrl);
        }

        protected static dynamic GetRequestDynamic(string requestUrl)
        {
            dynamic shortenResult;
            var rawResult = string.Empty;


            if (_WebNet == null)
            {
                _WebNet = new SInet(false, 5, null, 60 * 1000);
            }

            rawResult = _WebNet.DownloadString(requestUrl);

            if (rawResult != null)
            {
                shortenResult = Newtonsoft.Json.JsonConvert.DeserializeObject<dynamic>(rawResult);
            }
            else
            {
                rawResult = "";
                shortenResult = Newtonsoft.Json.JsonConvert.DeserializeObject<dynamic>(rawResult);
            }

            return shortenResult;
        }

        protected static dynamic GetRequestJSON(string requestUrl, string userAgent = null)
        {

            if (_WebNet == null)
                _WebNet = new SInet(false, 5, null, 60 * 1000);
            if (!string.IsNullOrWhiteSpace(userAgent))
            {
                _WebNet.UserAgent = userAgent;
            }

            var rawHtml = string.Empty;

            return _WebNet.DownloadDocument(requestUrl, out rawHtml);
        }

        protected static HttpWebRequest GetRequest(string requestUrl, string userAgent = null)
        {
            if (_WebNet == null)
                _WebNet = new SInet(false, 5, null, 60 * 1000);
            if (!string.IsNullOrWhiteSpace(userAgent))
            {
                _WebNet.UserAgent = userAgent;
            }

            return _WebNet.GetWebRequest(requestUrl);
        }
    }
}
