﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SI;

namespace Bitly
{
    public class LinkMetrics : BitlyAPI
    {
        public static ClicksCountResult GetClicks(string shortURL)
        {
            var result = new ClicksCountResult();

            if (!string.IsNullOrEmpty(shortURL))
            {
                var baseURL = "https://api-ssl.bitly.com/v3/link/clicks?access_token={0}&link={1}";
                var accessToken = "d0b48eec5c0f1ec06de95d9ba41fe36f7393809c";
                var processURL = String.Format(baseURL, accessToken, shortURL);
                dynamic dynamicResult = GetRequestDynamic(processURL);

                if (dynamicResult != null)
                {
                    result = new ClicksCountResult();

                    result.StatusCode = Convert.ToInt32(dynamicResult.status_code ?? 0);
                    result.StatusTxt = dynamicResult.status_txt ?? "";

                    if (result.StatusTxt == "OK")
                    {
                        result.LinkClicks = Convert.ToInt32(dynamicResult.data.link_clicks.Value ?? 0);
                        result.TzOffset = Convert.ToInt32(dynamicResult.data.tz_offset.Value ?? 0);
                        result.Unit = dynamicResult.data.unit.Value ?? "";
                        result.UnitReferenceTs = Convert.ToInt32(dynamicResult.data.unit_reference_ts.Value ?? 0);
                        result.Units = Convert.ToInt32(dynamicResult.data.units.Value ?? 0);
                    }
                }

            }

            return result;
        }

        public static ReferrersResult GetReferrers(string shortURL)
        {
            var result = new ReferrersResult();

            if (!string.IsNullOrEmpty(shortURL))
            {
                var baseURL = "https://api-ssl.bitly.com/v3/link/referrers?access_token={0}&link={1}";
                var accessToken = "d0b48eec5c0f1ec06de95d9ba41fe36f7393809c";
                var processURL = String.Format(baseURL, accessToken, shortURL);
                dynamic dynamicResult = GetRequestDynamic(processURL);

                if (dynamicResult != null)
                {
                    result = new ReferrersResult();
                    result.StatusCode = Convert.ToInt32(dynamicResult.status_code ?? 0);
                    result.StatusTxt = dynamicResult.status_txt ?? "";

                    if (result.StatusTxt == "OK")
                    {
                        if (dynamicResult.data.referrers != null && dynamicResult.data.referrers.Count > 0)
                        {
                            foreach (var referrer in dynamicResult.data.referrers)
                            {
                                var referrerItem = new Referrer
                                {
                                    Clicks = Convert.ToInt32(referrer.clicks.Value ?? 0),
                                    ReferrerName = referrer.referrer.Value ?? ""
                                };

                                result.ReferrersList.Add(referrerItem);
                            }
                        }

                        result.TotalReferrersCount = result.ReferrersList.Count;
                        result.TzOffset = Convert.ToInt32(dynamicResult.data.tz_offset.Value ?? 0);
                        result.Unit = dynamicResult.data.unit.Value ?? "";
                        result.Units = Convert.ToInt32(dynamicResult.data.units.Value ?? 0);
                    }
                }
            }

            return result;
        }

        public static SharesResult GetShares(string shortURL)
        {
            var result = new SharesResult();

            if (!string.IsNullOrEmpty(shortURL))
            {
                var baseURL = "https://api-ssl.bitly.com/v3/link/shares?access_token={0}&link={1}";
                var accessToken = "d0b48eec5c0f1ec06de95d9ba41fe36f7393809c";
                var processURL = String.Format(baseURL, accessToken, shortURL);
                dynamic dynamicResult = GetRequestDynamic(processURL);

                if (dynamicResult != null)
                {
                    result = new SharesResult();
                    result.StatusCode = Convert.ToInt32(dynamicResult.status_code ?? 0);
                    result.StatusTxt = dynamicResult.status_txt ?? "";

                    if (result.StatusTxt == "OK")
                    {
                        if (dynamicResult.data.shares != null && dynamicResult.data.shares.Count > 0)
                        {
                            foreach (var share in dynamicResult.data.shares)
                            {
                                var shareItem = new Share()
                                {
                                    ShareType = share.share_type.Value ?? "",
                                    ShareCount = Convert.ToInt32(share.shares.Value ?? 0)
                                };

                                result.SharesList.Add(shareItem);
                            }
                        }

                        result.TzOffset = Convert.ToInt32(dynamicResult.data.tz_offset.Value ?? 0);
                        result.Unit = dynamicResult.data.unit.Value ?? "";
                        result.Units = Convert.ToInt32(dynamicResult.data.units.Value ?? 0);
                    }
                }
            }

            return result;
        }

        public static LinkStats GetLinkStats(string shortURL)
        {
            var result = new LinkStats();

            if (!string.IsNullOrEmpty(shortURL))
            {
                var getClicksResult = LinkMetrics.GetClicks("http://bit.ly/155lijn");

                if (getClicksResult.LinkClicks != null)
                {
                    result.ClickCount = getClicksResult.LinkClicks;
                }

                var getReferrersResult = LinkMetrics.GetReferrers("http://bit.ly/155lijn");

                if (getReferrersResult.TotalReferrersCount != null)
                {
                    result.ReferrerCount = getReferrersResult.TotalReferrersCount;
                }

                var getSharesResult = LinkMetrics.GetShares("http://bit.ly/155lijn");

                if (getSharesResult.TotalSharesCount != null)
                {
                    result.ShareCount = getSharesResult.TotalSharesCount;
                }

            }
            return result;
        }
    }
}
