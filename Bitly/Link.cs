﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Script.Serialization;
using SI;

namespace Bitly
{
    public class Link : BitlyAPI
    {
        public static ShortenResult Shorten(string longURL)
        {
            var result = new ShortenResult();
            
            if (!string.IsNullOrEmpty(longURL))
            {
                longURL = FixURL(longURL);
                var baseURL = "https://api-ssl.bitly.com/v3/shorten?access_token={0}&longurl={1}";
                var accessToken = "d0b48eec5c0f1ec06de95d9ba41fe36f7393809c";
                var processURL = String.Format(baseURL, accessToken, longURL);
                dynamic dynamicResult = GetRequestDynamic(processURL);
                
                if (dynamicResult != null)
                {
                    result = new ShortenResult();
                    result.StatusCode = Convert.ToInt32(dynamicResult.status_code ?? 0);
                    result.StatusTxt = dynamicResult.status_txt ?? "";

                    if (result.StatusTxt == "OK")
                    {
                        result.LongURL = dynamicResult.data.long_url ?? "";
                        result.GlobalHash = dynamicResult.data.global_hash ?? "";
                        result.ShortURL = dynamicResult.data.url ?? "";
                        result.Hash = dynamicResult.data.hash ?? "";
                        result.NewHash = dynamicResult.data.new_hash ?? "";
                    }
                }
                
            }

            return result;
        }

        public static LookupResult Lookup(string longURL)
        {
            var result = new LookupResult();
            
            if (!string.IsNullOrEmpty(longURL))
            {
                longURL = FixURL(longURL);
                var baseURL = "https://api-ssl.bitly.com/v3/lookup?access_token={0}&url={1}";
                var accessToken = "d0b48eec5c0f1ec06de95d9ba41fe36f7393809c";
                var processURL = String.Format(baseURL, accessToken, longURL);
                dynamic dynamicResult = GetRequestDynamic(processURL);

                if (dynamicResult != null)
                {
                    result = new LookupResult();
                    result.StatusCode = Convert.ToInt32(dynamicResult.status_code ?? 0);
                    result.StatusTxt = dynamicResult.status_txt ?? "";

                    if (result.StatusTxt == "OK")
                    {
                        result.URL = dynamicResult.data.linklookup.aggregate_link ?? "";
                        result.AggregateLink = dynamicResult.data.linklookup.url ?? "";
                    }
                }

            }

            return result;
        }

        public static ExpandResult Expand(string shortURL, string hash = "")
        {
            var result = new ExpandResult();

            if (!string.IsNullOrEmpty(shortURL))
            {
                var baseURL = "https://api-ssl.bitly.com/v3/expand?access_token={0}&shortUrl={1}";
                var accessToken = "d0b48eec5c0f1ec06de95d9ba41fe36f7393809c";
                var processURL = String.Format(baseURL, accessToken, shortURL);
                dynamic dynamicResult = GetRequestDynamic(processURL);

                if (dynamicResult != null)
                {
                    result = new ExpandResult();
                    result.StatusCode = Convert.ToInt32(dynamicResult.status_code ?? 0);
                    result.StatusTxt = dynamicResult.status_txt ?? "";

                    if (result.StatusTxt == "OK")
                    {
                        result.GlobalHash = dynamicResult.data.expand[0].global_hash ?? "";
                        result.UserHash = dynamicResult.data.expand[0].user_hash ?? "";
                        result.ShortURL = dynamicResult.data.expand[0].short_url ?? "";
                        result.LongURL = dynamicResult.data.expand[0].long_url ?? "";
                    }
                }
            }

            return result;
        }

        public static InfoResult Info(string shortURL, string hash = "", bool explanUser = false)
        {
            var result = new InfoResult();

            if (!string.IsNullOrEmpty(shortURL))
            {
                var baseURL = "https://api-ssl.bitly.com/v3/info?access_token={0}&shortUrl={1}";
                var accessToken = "d0b48eec5c0f1ec06de95d9ba41fe36f7393809c";
                var processURL = String.Format(baseURL, accessToken, shortURL);
                dynamic dynamicResult = GetRequestDynamic(processURL);

                if (dynamicResult != null)
                {
                    result = new InfoResult();
                    result.StatusCode = Convert.ToInt32(dynamicResult.status_code ?? 0);
                    result.StatusTxt = dynamicResult.status_txt ?? "";

                    if (result.StatusTxt == "OK")
                    {
                        result.CreatedAt = dynamicResult.data.info.created_at ?? "";
                        result.CreatedBy = dynamicResult.data.info.created_by ?? "";
                        result.GlobalHash = dynamicResult.data.info.global_hash ?? "";
                        result.UserHash = dynamicResult.data.info.user_hash ?? "";
                        result.ShortURL = dynamicResult.data.info.short_url ?? "";
                        result.Title = dynamicResult.data.info.title ?? "";
                    }
                }
            }

            return result;
        }

        
    }
}
