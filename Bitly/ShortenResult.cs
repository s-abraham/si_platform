﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bitly
{

    public class LinkStats
    {
        public LinkStats()
        {
            
        }
        
        public int ShareCount { get; set; }
        public int ReferrerCount { get; set; }
        public int ClickCount { get; set; }
    }
    
    public class SharesResult : BitlyResult
    {
        public SharesResult()
        {
            SharesList = new List<Share>();
        }
        
        public List<Share> SharesList { get; set; }

        public int TotalSharesCount { get; set; }
        public int TzOffset { get; set; }
        public string Unit { get; set; }
        public int Units { get; set; }
    }

    public class ReferrersResult : BitlyResult
    {
        public ReferrersResult()
        {
            ReferrersList = new List<Referrer>();
        }

        public List<Referrer> ReferrersList { get; set; }

        public int TotalReferrersCount { get; set; }
        public int TzOffset { get; set; }
        public string Unit { get; set; }
        public int Units { get; set; }
    }

    public class ClicksCountResult : BitlyResult
    {
        public int LinkClicks { get; set; }
        public int TzOffset { get; set; }
        public string Unit { get; set; }
        public int UnitReferenceTs { get; set; }
        public int Units { get; set; }
    }

    public class Referrer
    {
        public int Clicks { get; set; }
        public string ReferrerName { get; set; }
    }

    public class Share
    {
        public string ShareType { get; set; }
        public int ShareCount { get; set; }
    }

    public class ExpandResult : BitlyResult
    {
        public string ShortURL { get; set; }
        public string GlobalHash { get; set; }
        public string LongURL { get; set; }
        public string UserHash { get; set; }
        
    }

    public class LinkInfoLookupResult : BitlyResult
    {
        public string AggregateLink { get; set; }
        public string LongURL { get; set; }
        public string Link { get; set; }
    }

    public class InfoResult : BitlyResult
    {
        public string CreatedAt { get; set; }
        public string CreatedBy { get; set; }
        public string ShortURL { get; set; }
        public string GlobalHash { get; set; }
        public string Title { get; set; }
        public string UserHash { get; set; }
    }

    public class LookupResult : BitlyResult
    {
        public string AggregateLink { get; set; }
        public string URL { get; set; }
    }

    public class ShortenResult : BitlyResult
    {
        public string LongURL { get; set; }
        public string ShortURL { get; set; }
        public string Hash { get; set; }
        public string GlobalHash { get; set; }
        public string NewHash { get; set; }
    }

    public class BitlyResult
    {
        public int StatusCode { get; set; }
        public string StatusTxt { get; set; }

    }
}
