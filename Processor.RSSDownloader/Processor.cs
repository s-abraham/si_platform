﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Connectors.Content;
using Connectors.Content.Parameters;
using Connectors.Entities;
using Connectors.Social;
using SI;
using SI.BLL;
using SI.DTO;
using JobLib;
using Newtonsoft.Json;
using SI;
using SI.BLL;
using SI.DTO;

namespace Processor.RSSDownloader
{
    public class Processor : ProcessorBase<RSSDownloaderData>
    {
        // Need to pass the args through for the default constructor
        public Processor(string[] args) : base(args) { }

        //static void Main(string[] args)
        protected override void start()
        {
            JobDTO job = getNextJob();

            while (job != null)
            {
                try
                {
                    setJobStatus(job.ID.Value, JobStatusEnum.Started);
                    RSSDownloaderData data = getJobData(job.ID.Value);
                    RSSFeedDTO feed = ProviderFactory.RSS.GetRSSFeed(data.RSSFeedID);

                    #region GetRSSData

                    SIRSSParameters rssParameters = new SIRSSParameters();
                    rssParameters.RssUrl = feed.URL;


                    SIRSSConnectionParameters connectionParameters = new SIRSSConnectionParameters(rssParameters);
                    connectionParameters.rssParameters.MaxItems = data.MaxItems;
                    connectionParameters.rssParameters.IncludeImages = data.DownloadImages;
                    connectionParameters.rssParameters.RSSFeedID = data.RSSFeedID;
                    connectionParameters.rssParameters.DaysBack = data.DaysBack;


                    RSSFeedConnection rssFeedConnection = new RSSFeedConnection(connectionParameters);

                    RSSEntities objRSSResponse = rssFeedConnection.DownloadRSSItems();

                    if (objRSSResponse.RSSItems.Any())
                    {
                        bool IsSuccess = ProviderFactory.RSS.SaveRSSData(objRSSResponse);                        
                    }
                    else
                    {
                        //todo: audit here
                    }

                    #endregion

                    setJobStatus(job.ID.Value, JobStatusEnum.Completed);

                }
                catch (Exception ex)
                {
                    setJobStatus(job.ID.Value, JobStatusEnum.Failed);
                    ProviderFactory.Logging.LogException(ex);
                    ProviderFactory.Logging.Audit(AuditLevelEnum.Exception, AuditTypeEnum.ProcessorException, ex.Message,
                        null, null, null, null, null, null);
                }
                job = getNextJob();
            }
        }

    }
}
