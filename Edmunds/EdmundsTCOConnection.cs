﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;

namespace EdmundsAPI.APIClasses
{
    public class EdmundsTCOConnection
    {
        #region "Definitions"

        public enum TCOMethods
        {
            newtruecosttoownbystyleidandzip,
            usedtruecosttoownbystyleidandzip,
            resalevaluesbystyleidandzip,
            depreciation_usedratesbystyleidandzip,
            depreciation_newratesbystyleidandzip,
            newtotalcashpricebystyleidandzip,
            usedtotalcashpricebystyleidandzip,
            getmakeswithtcodata,
            getmodelswithtcodata,
            getstyleswithtcodatabysubmodel
        }

        public class TCOParameters
        {
            public TCOMethods tcoMethod;
            public string StyleID { get; set; }
            public string Zipcode { get; set; }
            public string MakeID { get; set; }
            public string Make { get; set; }
            public string Model { get; set; }
            public string Year { get; set; }
            public string SubModel { get; set; }
            public string MakeYear { get; set; }
        }

        public class Make
        {
            public string id { get; set; }
            public string name { get; set; }
            public string niceName { get; set; }
        }

        public class Makes
        {
            public List<Make> make { get; set; }
        }

        public class TCOItem
        {
            public string id { get; set; }
            public int price { get; set; }
            public string styleLongName { get; set; }
            public string trim { get; set; }
        }

        public class Styles
        {
            public string StyleName { get; set; }
            public List<TCOItem> tocItem { get; set; }
        }

        public class TCOItemResult
        {
            public bool Status;
            public Styles Value { get; set; }
            public string ErrorMessage;
        }
        public class TCOConnectionResult
        {
            public bool Status;
            public string Value;
            public string ErrorMessage;
        }

        #endregion

        #region "Public Methods"

        public TCOItemResult GetNewTCOByStyleIDAndZip(TCOParameters tcoParameters)
        {
            TCOItemResult tcoItemResult = new TCOItemResult();

            if (!string.IsNullOrEmpty(tcoParameters.StyleID) && !string.IsNullOrEmpty(tcoParameters.Zipcode))
            {
                string tcoUrl = GenerateTCOAPIUrl(tcoParameters);
            }
            else
            {
                tcoItemResult.ErrorMessage = "StyleID & Zipcode are required";
                tcoItemResult.Status = false;
            }
            return tcoItemResult;
        }

        #endregion

        #region "Private Methods"

        private string GenerateTCOAPIUrl(TCOParameters tcoParameters)
        {
            string generatedUrl = "http://api.edmunds.com/v1/api/tco/newtruecosttoownbystyleidandzip/";

            if (tcoParameters.tcoMethod == TCOMethods.newtruecosttoownbystyleidandzip)
            {
                generatedUrl += tcoParameters.StyleID + "/" + tcoParameters.Zipcode;
            }

            return generatedUrl + "apikey=n2xnnwjb34ezmufpbwucrnae&fmt=json";
        }

        private TCOConnectionResult DownloadTCOAPIAsString(string tcoAPIUrl)
        {
            TCOConnectionResult tcoConnectionResult = new TCOConnectionResult();

            try
            {
                WebClient client = new WebClient();

                tcoConnectionResult.Value = client.DownloadString(tcoAPIUrl);
                tcoConnectionResult.Status = true;
            }
            catch (WebException webEx)
            {
                tcoConnectionResult.ErrorMessage = (webEx.InnerException != null) ? webEx.InnerException.Message : webEx.Message;
                tcoConnectionResult.Status = false;
            }
            catch (Exception ex)
            {
                tcoConnectionResult.ErrorMessage = (ex.InnerException != null) ? ex.InnerException.Message : ex.Message;
                tcoConnectionResult.Status = false;
            }

            return tcoConnectionResult;
        }

        private TCOItemResult GetTCO(string tcoAPIUrl)
        {
            List<Styles> styles = new List<Styles>();
            TCOItemResult tcoItemResult = new TCOItemResult();

            try
            {
                TCOConnectionResult tcoConnectionResult = DownloadTCOAPIAsString(tcoAPIUrl);

                if (tcoConnectionResult != null && tcoConnectionResult.Status)
                {
                    if (!string.IsNullOrEmpty(tcoConnectionResult.Value))
                    {
                        dynamic tcoDetails = Newtonsoft.Json.JsonConvert.DeserializeObject<dynamic>(tcoConnectionResult.Value);

                        if (tcoDetails != null && tcoDetails.Count > 0)
                        {
                            foreach (var tcoDetail in tcoDetails)
                            {
                                try
                                {

                                }
                                catch (Exception ex)
                                {
                                    tcoItemResult.ErrorMessage = (ex.InnerException != null) ? ex.InnerException.Message : ex.Message;
                                    tcoItemResult.Status = false;
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                tcoItemResult.ErrorMessage = (ex.InnerException != null) ? ex.InnerException.Message : ex.Message;
                tcoItemResult.Status = false;
            }

            return tcoItemResult;
        }

        private Styles PopulateTCOItem(TCOItem tcoItem)
        {
            Styles styles = new Styles();

            return styles;
        }
        #endregion
    }
}