﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Diagnostics;

namespace EdmundsAPI.APIClasses
{
    public class EdmundsDealersConnection
    {
        #region "Definitions"
        ///<summary>
        /// Parameters to make the API call
        /// </summary>
        public class DealerAPIParameters
        {
            public string ZipCode { get; set; }
            public string MakeName { get; set; }
            public string Model { get; set; }
            public string StyleId { get; set; }
            public string Radius { get; set; }
        }

        public class DealerConnectionResult
        {
            public bool Status;
            public string Value;
            public string ErrorMessage;
        }

        public class DealerItemResult
        {
            public bool Status;
            public List<DealerItem> Value;
            public string ErrorMessage;
        }

        public class Address
        {
            public string street { get; set; }
            public string apartment { get; set; }
            public string city { get; set; }
            public string stateCode { get; set; }
            public string stateName { get; set; }
            public string county { get; set; }
            public string country { get; set; }
            public string zipcode { get; set; }
            public double latitude { get; set; }
            public double longitude { get; set; }
        }

        public class Operations
        {
            public string Wednesday { get; set; }
            public string Tuesday { get; set; }
            public string Thursday { get; set; }
            public string Saturday { get; set; }
            public string Friday { get; set; }
            public string Monday { get; set; }
            public string Sunday { get; set; }
        }

        public class Contactinfo
        {
            public string dealer_website { get; set; }
            public string email_address { get; set; }
            public string phone { get; set; }
        }

        public class DealerItem
        {
            public string id { get; set; }
            public string locationId { get; set; }
            public Address address { get; set; }
            public string name { get; set; }
            public string logicalName { get; set; }
            public string type { get; set; }
            public string make { get; set; }
            public Operations operations { get; set; }
            public Contactinfo contactinfo { get; set; }
            public DateTime? publishDate { get; set; }
            public bool active { get; set; }
            public string ppStatus { get; set; }
            public DateTime? syncPublishDate { get; set; }
        }

        #endregion

        #region "Edmunds Dealer API"

        /// <summary>
        /// Geneate Edmunds dealer request url
        /// </summary>
        /// <returns>Dealer api url based on parameters</returns>
        private string GenerateDealerAPIUrl(DealerAPIParameters searchParameters)
        {
            string generatedUrl = "http://api.edmunds.com/v1/api/dealer?";

            generatedUrl += "zipcode=" + searchParameters.ZipCode;

            if (!string.IsNullOrEmpty(searchParameters.MakeName))
            {
                generatedUrl += "&makeName=" + searchParameters.MakeName;
            }

            if (!string.IsNullOrEmpty(searchParameters.Model))
            {
                generatedUrl += "&model=" + searchParameters.Model;
            }

            if (!string.IsNullOrEmpty(searchParameters.StyleId))
            {
                generatedUrl += "&styleid=" + searchParameters.StyleId;
            }

            if (!string.IsNullOrEmpty(searchParameters.Radius))
            {
                generatedUrl += "&radius=" + searchParameters.Radius;
            }

            return generatedUrl + "&api_key=sxvv9bgrmn7w24k3stgegx2s&fmt=json";
        }

        /// <summary>
        /// Call Edmunds Dealer API and extract dealer information
        /// </summary>
        /// <param name="searchParameters">object that has zipcode, make, model, styleId, radius</param>
        /// <returns>Populates DealerItem object</returns>
        public DealerItemResult GetDealerAPI(DealerAPIParameters searchParameters)
        {
            DealerItemResult dealerItemResult = new DealerItemResult();
            List<DealerItem> dealershipList = new List<DealerItem>();

            //try catch block
            try
            {
                if (!string.IsNullOrEmpty(searchParameters.ZipCode))
                {
                    //call function that generates the API call url
                    string dealerAPIUrl = GenerateDealerAPIUrl(searchParameters);

                    //another function, retun object that has status bool
                    DealerConnectionResult dealerConnectionResult = DownloadDealerAPIAsString(dealerAPIUrl);

                    if (dealerConnectionResult != null && dealerConnectionResult.Status)
                    {
                        if (!string.IsNullOrEmpty(dealerConnectionResult.Value))
                        {
                            dynamic dealershipDetails = Newtonsoft.Json.JsonConvert.DeserializeObject<dynamic>(dealerConnectionResult.Value);

                            if (dealershipDetails != null && dealershipDetails.Count > 0)
                            {
                                foreach (var dealer in dealershipDetails.dealerHolder)
                                {
                                    try
                                    {
                                        dealershipList.Add(PopulateDealershipDetails(dealer));
                                    }
                                    catch (Exception ex)
                                    {
                                        dealerItemResult.ErrorMessage = ex.Message;
                                        dealerItemResult.Status = false;
                                    }
                                }

                                dealerItemResult.Value = dealershipList;
                                dealerItemResult.Status = true;
                            }
                            else
                            {
                                dealerItemResult.Status = true;
                                Debug.WriteLine("No Dealers Found");
                            }
                        }
                    }
                }
                else
                {
                    dealerItemResult.ErrorMessage = "Zipcode is required";
                    dealerItemResult.Status = false;
                }
            }
            catch (Exception ex)
            {
                dealerItemResult.ErrorMessage = ex.Message;
                dealerItemResult.Status = false;
            }

            return dealerItemResult;
        }

        /// <summary>
        /// Connect to Edmund's Dealer API
        /// </summary>
        /// <returns>Populates Connection object that has the connection status, request string, and erromessage if any</returns>
        private DealerConnectionResult DownloadDealerAPIAsString(string dealerAPIUrl)
        {
            DealerConnectionResult dealerConnectionResult = new DealerConnectionResult();

            try
            {
                WebClient client = new WebClient();

                dealerConnectionResult.Value = client.DownloadString(dealerAPIUrl);
                dealerConnectionResult.Status = true;
            }
            catch (WebException webEx)
            {
                dealerConnectionResult.ErrorMessage = webEx.Message;
                dealerConnectionResult.Status = false;
            }
            catch (Exception ex)
            {
                dealerConnectionResult.ErrorMessage = ex.InnerException.Message;
                dealerConnectionResult.Status = false;
            }

            return dealerConnectionResult;
        }

        /// <summary>
        /// Call Edmunds Dealer API and extract dealership details
        /// </summary>
        /// <param name="dealerShipDetails">dealerholder object.</param>
        /// <returns>Populates the DealerItem object</returns>
        private DealerItem PopulateDealershipDetails(dynamic dealerShipDetails)
        {
            DealerItem dealerItem = new DealerItem();

            dealerItem.id = dealerShipDetails.id;
            dealerItem.locationId = dealerShipDetails.locationId;

            if (dealerShipDetails.address != null)
            {
                dealerItem.address = PopulateDealershipAddress(dealerShipDetails.address);
            }

            dealerItem.name = dealerShipDetails.name;
            dealerItem.logicalName = dealerShipDetails.logicalName;
            dealerItem.type = dealerShipDetails.type;
            dealerItem.make = dealerShipDetails.make;

            if (dealerShipDetails.operations != null)
            {
                dealerItem.operations = PopulateDealershipOperations((dealerShipDetails.operations));
            }

            if (dealerShipDetails.contactinfo != null)
            {
                dealerItem.contactinfo = PopulateDealershipContactInfo(dealerShipDetails.contactinfo);
            }

            dealerItem.publishDate = StringExtensions.ToDateTime((string)dealerShipDetails.publishDate);
            dealerItem.active = dealerShipDetails.active;
            dealerItem.ppStatus = dealerShipDetails.ppStatus;
            dealerItem.syncPublishDate = StringExtensions.ToDateTime((string)dealerShipDetails.syncPublishDate);

            return dealerItem;
        }

        /// <summary>
        /// Call Edmunds Dealer API and extract dealership address information
        /// </summary>
        /// <param name="dealershipAddress">dealership address object.</param>
        /// <returns>Populates the Address object</returns>
        private Address PopulateDealershipAddress(dynamic dealershipAddress)
        {
            Address address = new Address();

            address.street = dealershipAddress.street;
            address.apartment = dealershipAddress.apartment;
            address.city = dealershipAddress.city;
            address.stateCode = dealershipAddress.stateCode;
            address.stateName = dealershipAddress.stateName;
            address.county = dealershipAddress.county;
            address.country = dealershipAddress.country;
            address.zipcode = dealershipAddress.zipcode;
            address.latitude = dealershipAddress.latitude;
            address.longitude = dealershipAddress.longitude;

            return address;
        }

        /// <summary>
        /// Call Edmunds Dealer API and extract dealership Operations information
        /// </summary>
        /// <param name="dealershipOperations">dealership operations object</param>
        /// <returns>Populates the Operations object</returns>
        private Operations PopulateDealershipOperations(dynamic dealershipOperations)
        {
            Operations operations = new Operations();

            operations.Wednesday = dealershipOperations.Wednesday;
            operations.Tuesday = dealershipOperations.Tuesday;
            operations.Thursday = dealershipOperations.Thursday;
            operations.Saturday = dealershipOperations.Saturday;
            operations.Friday = dealershipOperations.Friday;
            operations.Monday = dealershipOperations.Monday;
            operations.Sunday = dealershipOperations.Sunday;

            return operations;
        }

        /// <summary>
        /// Call Edmunds Dealer API and extract dealership Contact information
        /// </summary>
        /// <param name="dealershipContactInfo">dealership ContactInfo object</param>
        /// <returns>Populates the ContactInfo object</returns>
        private Contactinfo PopulateDealershipContactInfo(dynamic dealershipContactInfo)
        {
            Contactinfo contactinfo = new Contactinfo();

            contactinfo.dealer_website = dealershipContactInfo.dealer_website;
            contactinfo.email_address = dealershipContactInfo.email_address;
            contactinfo.phone = dealershipContactInfo.phone;

            return contactinfo;
        }

        #endregion
    }
}