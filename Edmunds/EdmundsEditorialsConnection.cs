﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Text;
using System.Xml;
using System.Xml.Serialization;
using System.Diagnostics;

namespace EdmundsAPI.APIClasses
{
    public class EdmundsEditorialsConnection
    {
        #region "Definition"
        ///<summary>
        /// Parameters to make the API call
        /// </summary>
        public class EditorialAPIParameters
        {
            private CategoryFilters _categoryFilter;
            private string _categoryFilterValue;
            public EditorialFilters EditorialFilter { get; set; }
            public CategoryFilters CategoryFilter
            {
                get { return _categoryFilter; }
                set
                {
                    _categoryFilter = value;

                    switch (value)
                    {
                        case CategoryFilters.Reviews:
                            _categoryFilterValue = "reviews";
                            break;
                        case CategoryFilters.Car_News:
                            _categoryFilterValue = "car+news";
                            break;
                        case CategoryFilters.Road_Tests:
                            _categoryFilterValue = "road+tests";
                            break;
                        case CategoryFilters.Car_Buying:
                            _categoryFilterValue = "car+buying";
                            break;
                        case CategoryFilters.Auto_Finance:
                            _categoryFilterValue = "auto+finance";
                            break;
                        case CategoryFilters.Insurance:
                            _categoryFilterValue = "insurance";
                            break;
                        case CategoryFilters.Warranties:
                            _categoryFilterValue = "warranties";
                            break;
                        case CategoryFilters.Car_Technology:
                            _categoryFilterValue = "car+technology";
                            break;
                        case CategoryFilters.Driving:
                            _categoryFilterValue = "driving";
                            break;
                        case CategoryFilters.Fuel_Economy:
                            _categoryFilterValue = "fuel+economy";
                            break;
                        case CategoryFilters.How_To:
                            _categoryFilterValue = "how-to";
                            break;
                        case CategoryFilters.Vehicle_Safety:
                            _categoryFilterValue = "vehicle+safety";
                            break;
                        case CategoryFilters.Maintenance_Percent26_Repair:
                            _categoryFilterValue = "maintenance+%26+repair";
                            break;
                    }
                }
            }

            public string CategoryFilterValue
            {
                get { return _categoryFilterValue; }
                set { _categoryFilterValue = value; }
            }

            public string Make { get; set; }
            public string Model { get; set; }
            public string Year { get; set; }
            public string ArticleIndex { get; set; }
            public string NumberOfArticles { get; set; }
        }

        public enum EditorialFilters
        {
            CategoryOnly,
            ModelOnly,
            VehicleOnly,
            ModelWithCategory,
            VehicleWithCategory
        }

        public enum CategoryFilters
        {
            Reviews,
            Car_News,
            Road_Tests,
            Car_Buying,
            Auto_Finance,
            Insurance,
            Warranties,
            Car_Technology,
            Driving,
            Fuel_Economy,
            How_To,
            Vehicle_Safety,
            Maintenance_Percent26_Repair
        }

        public class EditorialConnectionResult
        {
            public bool Status;
            public string Value;
            public string ErrorMessage;
        }

        public class EditorialItem
        {
            public string link { get; set; }
            public string title { get; set; }
            public DateTime? published { get; set; }
            public string content { get; set; }
        }

        public class EditorialItemResult
        {
            public bool Status;
            public List<EditorialItem> Value;
            public string ErrorMessage;
        }

        #endregion

        #region "Public Methods"

        /// <summary>
        /// Call Edmunds Editorial API with category parameter
        /// </summary>
        /// <param name="editorialApiParameters">object that has category, make, model, year, & limit</param>
        /// <returns>Populates EditorialItemResult object</returns>
        public EditorialItemResult GetCategoryOnlyEditorials(EditorialAPIParameters editorialApiParameters)
        {
            EditorialItemResult editorialItemResult = new EditorialItemResult();

            //set the filter
            //EditorialParameters.Filter = EditorialFilters.CategoryOnly;

            try
            {
                //category is required
                if (!string.IsNullOrEmpty(editorialApiParameters.CategoryFilterValue))
                {
                    //get the api url
                    string editorialAPIUrl = GenerateEditorialAPIUrl(editorialApiParameters);

                    //get the editorialItem object
                    editorialItemResult = GetEditorials(editorialAPIUrl);
                }
                else
                {
                    editorialItemResult.ErrorMessage = "Category is required.";
                    editorialItemResult.Status = false;
                }
            }
            catch (Exception ex)
            {
                editorialItemResult.ErrorMessage = (ex.InnerException != null) ? ex.InnerException.Message : ex.Message;
                editorialItemResult.Status = false;
            }

            return editorialItemResult;
        }

        /// <summary>
        /// Call Edmunds Editorial API with make & model parameters
        /// </summary>
        /// <param name="editorialApiParameters">object that has category, make, model, year, & limit</param>
        /// <returns>Populates EditorialItemResult object</returns>
        public EditorialItemResult GetModelOnlyEditorials(EditorialAPIParameters editorialApiParameters)
        {
            EditorialItemResult editorialItemResult = new EditorialItemResult();

            try
            {
                //make and model are required
                if (!string.IsNullOrEmpty(editorialApiParameters.Make) && !string.IsNullOrEmpty(editorialApiParameters.Model))
                {
                    //get the api url
                    string editorialAPIUrl = GenerateEditorialAPIUrl(editorialApiParameters);

                    //get the editorialItem object
                    editorialItemResult = GetEditorials(editorialAPIUrl);
                }
                else
                {
                    editorialItemResult.ErrorMessage = "Make & Model are required.";
                    editorialItemResult.Status = false;
                }
            }
            catch (Exception ex)
            {
                editorialItemResult.ErrorMessage = (ex.InnerException != null) ? ex.InnerException.Message : ex.Message;
                editorialItemResult.Status = false;
            }

            return editorialItemResult;
        }

        /// <summary>
        /// Call Edmunds Editorial API with make, model, year parameters
        /// </summary>
        /// <param name="editorialApiParameters">object that has category, make, model, year, & limit</param>
        /// <returns>Populates EditorialItemResult object</returns>
        public EditorialItemResult GetVehicleOnlyEditorials(EditorialAPIParameters editorialApiParameters)
        {
            EditorialItemResult editorialItemResult = new EditorialItemResult();

            try
            {
                //make, model & year are required
                if (!string.IsNullOrEmpty(editorialApiParameters.Make) && !string.IsNullOrEmpty(editorialApiParameters.Model)
                    && !string.IsNullOrEmpty(editorialApiParameters.Year))
                {
                    //get the api url
                    string editorialAPIUrl = GenerateEditorialAPIUrl(editorialApiParameters);

                    //get the editorialItem object
                    editorialItemResult = GetEditorials(editorialAPIUrl);
                }
                else
                {
                    editorialItemResult.ErrorMessage = "Make, Model, & Year are required.";
                    editorialItemResult.Status = false;
                }
            }
            catch (Exception ex)
            {
                editorialItemResult.ErrorMessage = (ex.InnerException != null) ? ex.InnerException.Message : ex.Message;
                editorialItemResult.Status = false;
            }

            return editorialItemResult;
        }

        /// <summary>
        /// Call Edmunds Editorial API with make, model & category parameters
        /// </summary>
        /// <param name="editorialApiParameters">object that has category, make, model, year, & limit</param>
        /// <returns>Populates EditorialItemResult object</returns>
        public EditorialItemResult GetModelWithCategoryEditorials(EditorialAPIParameters editorialApiParameters)
        {
            EditorialItemResult editorialItemResult = new EditorialItemResult();

            try
            {
                //make, model & category are required
                if (!string.IsNullOrEmpty(editorialApiParameters.Make) && !string.IsNullOrEmpty(editorialApiParameters.Model)
                    && !string.IsNullOrEmpty(editorialApiParameters.CategoryFilterValue))
                {
                    //get the api url
                    string editorialAPIUrl = GenerateEditorialAPIUrl(editorialApiParameters);

                    //get the editorialItem object
                    editorialItemResult = GetEditorials(editorialAPIUrl);
                }
                else
                {
                    editorialItemResult.ErrorMessage = "Make, Model, & Category are required.";
                    editorialItemResult.Status = false;
                }
            }
            catch (Exception ex)
            {
                editorialItemResult.ErrorMessage = (ex.InnerException != null) ? ex.InnerException.Message : ex.Message;
                editorialItemResult.Status = false;
            }

            return editorialItemResult;
        }

        /// <summary>
        /// Call Edmunds Editorial API with make, model, year, & category parameters
        /// </summary>
        /// <param name="editorialApiParameters">object that has category, make, model, year, & limit</param>
        /// <returns>Populates EditorialItemResult object</returns>
        public EditorialItemResult GetVehicleWithCategoryEditorials(EditorialAPIParameters editorialApiParameters)
        {
            EditorialItemResult editorialItemResult = new EditorialItemResult();

            try
            {
                //make, model, year & category are required
                if (!string.IsNullOrEmpty(editorialApiParameters.Make) && !string.IsNullOrEmpty(editorialApiParameters.Model)
                    && !string.IsNullOrEmpty(editorialApiParameters.Year) && !string.IsNullOrEmpty(editorialApiParameters.CategoryFilterValue))
                {
                    //get the api url
                    string editorialAPIUrl = GenerateEditorialAPIUrl(editorialApiParameters);

                    //get the editorialItem object
                    editorialItemResult = GetEditorials(editorialAPIUrl);
                }
                else
                {
                    editorialItemResult.ErrorMessage = "Make, Model, Year, & Category are required.";
                    editorialItemResult.Status = false;
                }
            }
            catch (Exception ex)
            {
                editorialItemResult.ErrorMessage = (ex.InnerException != null) ? ex.InnerException.Message : ex.Message;
                editorialItemResult.Status = false;
            }

            return editorialItemResult;
        }

        #endregion

        #region "Private Methods"
        /// <summary>
        /// Geneate Edmunds editorial request url
        /// </summary>
        /// <returns>Editorial api url based on parameters</returns>
        private string GenerateEditorialAPIUrl(EditorialAPIParameters editorialApiParameters)
        {
            string generatedUrl = "http://api.edmunds.com/v1/content/?fmt=json&api_key=6uz2p4qha6u4dr9d73jdj4yb";
            string limitParameter = string.Empty;

            if (!string.IsNullOrEmpty(editorialApiParameters.ArticleIndex) && (!string.IsNullOrEmpty(editorialApiParameters.NumberOfArticles)))
            {
                limitParameter = "&limit=" + editorialApiParameters.ArticleIndex + "," + editorialApiParameters.NumberOfArticles;
            }

            if (editorialApiParameters.EditorialFilter == EditorialFilters.CategoryOnly)
            {
                generatedUrl += "&category=" + editorialApiParameters.CategoryFilterValue + limitParameter;
            }

            if (editorialApiParameters.EditorialFilter == EditorialFilters.ModelOnly)
            {
                generatedUrl += "&make=" + editorialApiParameters.Make + "&model=" + editorialApiParameters.Model + limitParameter;
            }

            if (editorialApiParameters.EditorialFilter == EditorialFilters.VehicleOnly)
            {
                generatedUrl += "&make=" + editorialApiParameters.Make + "&model=" + editorialApiParameters.Model + "&year=" + editorialApiParameters.Year + limitParameter;
            }

            if (editorialApiParameters.EditorialFilter == EditorialFilters.ModelWithCategory)
            {
                generatedUrl += "&make=" + editorialApiParameters.Make + "&model=" + editorialApiParameters.Model + "&category=" + editorialApiParameters.CategoryFilterValue + limitParameter;
            }

            if (editorialApiParameters.EditorialFilter == EditorialFilters.VehicleWithCategory)
            {
                generatedUrl += "&make=" + editorialApiParameters.Make + "&model=" + editorialApiParameters.Model + "&year=" + editorialApiParameters.Year + "&category=" + editorialApiParameters.CategoryFilterValue + limitParameter;
            }

            return generatedUrl;
        }

        /// <summary>
        /// Connect to Edmund's Editorial API
        /// </summary>
        /// <returns>Populates Connection object that has the connection status, request string, and erromessage if any</returns>
        private EditorialConnectionResult DownloadEditorialAPIAsString(string editorialAPIUrl)
        {
            EditorialConnectionResult editorialConnectionResult = new EditorialConnectionResult();

            try
            {
                WebClient client = new WebClient();

                editorialConnectionResult.Value = client.DownloadString(editorialAPIUrl);
                editorialConnectionResult.Status = true;
            }
            catch (WebException webEx)
            {
                editorialConnectionResult.ErrorMessage = (webEx.InnerException != null) ? webEx.InnerException.Message : webEx.Message;
                editorialConnectionResult.Status = false;
            }
            catch (Exception ex)
            {
                editorialConnectionResult.ErrorMessage = (ex.InnerException != null) ? ex.InnerException.Message : ex.Message;
                editorialConnectionResult.Status = false;
            }

            return editorialConnectionResult;
        }

        /// <summary>
        /// Call Edmunds Editorial API and extract editorial information
        /// </summary>
        /// <param name=""></param>
        /// <returns>Populates EditorialItem object</returns>
        private EditorialItemResult GetEditorials(string editorialAPIUrl)
        {
            List<EditorialItem> editorialItems = new List<EditorialItem>();
            EditorialItemResult editorialItemResult = new EditorialItemResult();

            try
            {
                EditorialConnectionResult editorialConnectionResult = DownloadEditorialAPIAsString(editorialAPIUrl);

                if (editorialConnectionResult != null && editorialConnectionResult.Status)
                {
                    if (!string.IsNullOrEmpty(editorialConnectionResult.Value))
                    {
                        dynamic editorialDetails = Newtonsoft.Json.JsonConvert.DeserializeObject<dynamic>(editorialConnectionResult.Value);

                        if (editorialDetails != null && editorialDetails.Count > 0)
                        {
                            foreach (var editorial in editorialDetails)
                            {
                                try
                                {
                                    editorialItems.Add(PopulateEditorials(editorial));
                                }
                                catch (Exception ex)
                                {

                                    editorialItemResult.ErrorMessage = (ex.InnerException != null) ? ex.InnerException.Message : ex.Message;
                                    editorialItemResult.Status = false;
                                }
                            }

                            editorialItemResult.Value = editorialItems;
                            editorialItemResult.Status = true;
                        }
                        else
                        {
                            editorialItemResult.Status = true;
                            Debug.WriteLine("No Editorials Found");
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                editorialItemResult.ErrorMessage = (ex.InnerException != null) ? ex.InnerException.Message : ex.Message;
                editorialItemResult.Status = false;
            }

            return editorialItemResult;
        }

        /// <summary>
        /// Call Edmunds Editorial API and extract editorial details
        /// </summary>
        /// <param name="editorialDetails">editorialItem object.</param>
        /// <returns>Populates the DealerHolder object</returns>
        private EditorialItem PopulateEditorials(dynamic editorialDetails)
        {
            EditorialItem editorialItem = new EditorialItem();

            editorialItem.link = editorialDetails.link;
            editorialItem.title = editorialDetails.title;
            editorialItem.published = StringExtensions.ToDateTime((string)editorialDetails.published);
            editorialItem.content = editorialDetails.content;

            return editorialItem;
        }

        /// <summary>
        /// Serialized editorialItem value object to XML
        /// </summary>
        /// <param"></param>
        /// <returns>XML string of editorials</returns>
        private static string SerializeObject<T>(T obj)
        {
            XmlSerializer xmls = new XmlSerializer(typeof(T));
            using (MemoryStream ms = new MemoryStream())
            {
                XmlSerializerNamespaces ns = new XmlSerializerNamespaces();

                using (XmlWriter writer = new XmlTextWriter(ms, new UTF8Encoding()))
                {
                    xmls.Serialize(writer, obj, ns);
                }
                return Encoding.UTF8.GetString(ms.ToArray());
            }
        }

        #endregion
    }
}