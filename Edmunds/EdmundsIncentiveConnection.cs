﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Web;

namespace EdmundsAPI.APIClasses
{
    public class EdmundsIncentiveConnection
    {
        #region "Definitions"

        public enum IncentiveMethods
        {
            findbyid,
            findincentivesbycategoryandzipcode,
            findincentivesbymakeid,
            findincentivesbymakeidandzipcode,
            findincentivesbymodelyearidandzipcode,
            findincentivesbystyleid,
            findincentivesbystyleidandzipcode
        }

        public enum VehicleCategory
        {
            Large,
            SUV,
            Hybrid,
            Coupe,
            Car,
            Crossover,
            Convertible,
            Sedan,
            Midsize,
            Compact,
            Factory_Tuner,
            Luxury,
            Wagon,
            Performance,
            Fourdr_SUV,
            High_Performance
        }

        public class IncentiveAPIParameters
        {
            private VehicleCategory _vechicleCategory;
            private string _vehicleCategoryValue;

            public IncentiveMethods IncentiveMethod { get; set; }
            public string IncentiveID { get; set; }
            public VehicleCategory Category
            {
                get { return _vechicleCategory; }
                set
                {
                    _vechicleCategory = value;
                    switch (value)
                    {
                        case VehicleCategory.Factory_Tuner:
                            _vehicleCategoryValue = "Factory Tuner";
                            break;
                        case VehicleCategory.Fourdr_SUV:
                            _vehicleCategoryValue = "4dr SUV";
                            break;
                        default:
                            _vehicleCategoryValue = value.ToString();
                            break;
                    }
                }
            }
            public string Zipcode { get; set; }
            public string MakeID { get; set; }
            public string ModelYearID { get; set; }
            public string StyleID { get; set; }
        }

        public class IncentiveVehicle
        {
            public string link { get; set; }
        }

        public class MakeId
        {
            public string link { get; set; }
        }

        public class StyleId
        {
            public string link { get; set; }
        }

        public class IncentiveItem
        {
            public int termMonths { get; set; }
            public double apr { get; set; }
            public string creditRatingTier { get; set; }
            public int id { get; set; }
            public string type { get; set; }
            public string contentType { get; set; }
            public string sourceType { get; set; }
            public int subprogramId { get; set; }
            public DateTime startDate { get; set; }
            public DateTime endDate { get; set; }
            public string comments { get; set; }
            public string restrictions { get; set; }
            public string name { get; set; }
            public bool primary { get; set; }
            public List<IncentiveVehicle> incentiveVehicles { get; set; }
            public List<string> zipcodeExceptions { get; set; }
            public List<string> categories { get; set; }
            public List<string> regions { get; set; }
            public string incentiveType { get; set; }
            public List<object> optionIds { get; set; }
            public List<MakeId> makeIds { get; set; }
            public List<StyleId> styleIds { get; set; }
            public int rebateAmount { get; set; }
        }

        public class IncentiveItemResult
        {
            public bool Status;
            public List<IncentiveItem> Value { get; set; }
            public string ErrorMessage;
        }

        public class IncentiveConnectionResult
        {
            public bool Status;
            public string Value;
            public string ErrorMessage;
        }

        #endregion

        #region "Public Methods"

        /// <summary>
        /// Call Edmunds Incentive API with search parameter
        /// </summary>
        /// <param name="incentiveApiParameters"></param>
        /// <returns>Populates IncentiveItemResult object</returns>
        public IncentiveItemResult GetIncentivesByID(IncentiveAPIParameters incentiveApiParameters)
        {
            IncentiveItemResult incentiveItemResult = new IncentiveItemResult();

            try
            {
                if (!string.IsNullOrEmpty(incentiveApiParameters.IncentiveID))
                {
                    string incentiveUrl = GenerateEditorialAPIUrl(incentiveApiParameters);

                    incentiveItemResult = GetIncentives(incentiveUrl);
                }
                else
                {
                    incentiveItemResult.ErrorMessage = "Incentive ID is required.";
                    incentiveItemResult.Status = false;
                }
            }
            catch (Exception ex)
            {
                incentiveItemResult.ErrorMessage = (ex.InnerException != null) ? ex.InnerException.Message : ex.Message;
                incentiveItemResult.Status = false;
            }

            return incentiveItemResult;
        }

        /// <summary>
        /// Call Edmunds Incentive API with search parameter
        /// </summary>
        /// <param name="incentiveApiParameters"></param>
        /// <returns>Populates IncentiveItemResult object</returns>
        public IncentiveItemResult GetIncentivesByCategoryAndZipcode(IncentiveAPIParameters incentiveApiParameters)
        {
            IncentiveItemResult incentiveItemResult = new IncentiveItemResult();

            try
            {
                if (!(string.IsNullOrEmpty(incentiveApiParameters.Zipcode)))
                {
                    string incentiveUrl = GenerateEditorialAPIUrl(incentiveApiParameters);

                    incentiveItemResult = GetIncentives(incentiveUrl);
                }
                else
                {
                    incentiveItemResult.ErrorMessage = "Zipcode are required.";
                    incentiveItemResult.Status = false;
                }
            }
            catch (Exception ex)
            {
                incentiveItemResult.ErrorMessage = (ex.InnerException != null) ? ex.InnerException.Message : ex.Message;
                incentiveItemResult.Status = false;
            }

            return incentiveItemResult;
        }

        /// <summary>
        /// Call Edmunds Incentive API with search parameter
        /// </summary>
        /// <param name="incentiveApiParameters"></param>
        /// <returns>Populates IncentiveItemResult object</returns>
        public IncentiveItemResult GetIncentivesByMakeID(IncentiveAPIParameters incentiveApiParameters)
        {
            IncentiveItemResult incentiveItemResult = new IncentiveItemResult();

            try
            {
                if (!string.IsNullOrEmpty(incentiveApiParameters.MakeID))
                {
                    string incentiveUrl = GenerateEditorialAPIUrl(incentiveApiParameters);

                    incentiveItemResult = GetIncentives(incentiveUrl);
                }
                else
                {
                    incentiveItemResult.ErrorMessage = "Make ID is required.";
                    incentiveItemResult.Status = false;
                }
            }
            catch (Exception ex)
            {
                incentiveItemResult.ErrorMessage = (ex.InnerException != null) ? ex.InnerException.Message : ex.Message;
                incentiveItemResult.Status = false;
            }

            return incentiveItemResult;
        }

        /// <summary>
        /// Call Edmunds Incentive API with search parameter
        /// </summary>
        /// <param name="incentiveApiParameters"></param>
        /// <returns>Populates IncentiveItemResult object</returns>
        public IncentiveItemResult GetIncentivesByMakeIDAndZipcode(IncentiveAPIParameters incentiveApiParameters)
        {
            IncentiveItemResult incentiveItemResult = new IncentiveItemResult();

            try
            {
                if (!string.IsNullOrEmpty(incentiveApiParameters.MakeID) && !string.IsNullOrEmpty(incentiveApiParameters.Zipcode))
                {
                    string incentiveUrl = GenerateEditorialAPIUrl(incentiveApiParameters);

                    incentiveItemResult = GetIncentives(incentiveUrl);
                }
                else
                {
                    incentiveItemResult.ErrorMessage = "Make ID & Zipcode are required.";
                    incentiveItemResult.Status = false;
                }
            }
            catch (Exception ex)
            {
                incentiveItemResult.ErrorMessage = (ex.InnerException != null) ? ex.InnerException.Message : ex.Message;
                incentiveItemResult.Status = false;
            }

            return incentiveItemResult;
        }

        /// <summary>
        /// Call Edmunds Incentive API with search parameter
        /// </summary>
        /// <param name="incentiveApiParameters"></param>
        /// <returns>Populates IncentiveItemResult object</returns>
        public IncentiveItemResult GetIncentivesByModelYearIDAndZipcode(IncentiveAPIParameters incentiveApiParameters)
        {
            IncentiveItemResult incentiveItemResult = new IncentiveItemResult();

            try
            {
                if (!string.IsNullOrEmpty(incentiveApiParameters.ModelYearID) && !string.IsNullOrEmpty(incentiveApiParameters.Zipcode))
                {
                    string incentiveUrl = GenerateEditorialAPIUrl(incentiveApiParameters);

                    incentiveItemResult = GetIncentives(incentiveUrl);
                }
                else
                {
                    incentiveItemResult.ErrorMessage = "Model Year ID & Zipcode are required.";
                    incentiveItemResult.Status = false;
                }
            }
            catch (Exception ex)
            {
                incentiveItemResult.ErrorMessage = (ex.InnerException != null) ? ex.InnerException.Message : ex.Message;
                incentiveItemResult.Status = false;
            }

            return incentiveItemResult;
        }

        /// <summary>
        /// Call Edmunds Incentive API with search parameter
        /// </summary>
        /// <param name="incentiveApiParameters"></param>
        /// <returns>Populates IncentiveItemResult object</returns>
        public IncentiveItemResult GetIncentivesByStyleID(IncentiveAPIParameters incentiveApiParameters)
        {
            IncentiveItemResult incentiveItemResult = new IncentiveItemResult();

            try
            {
                if (!string.IsNullOrEmpty(incentiveApiParameters.StyleID))
                {
                    string incentiveUrl = GenerateEditorialAPIUrl(incentiveApiParameters);

                    incentiveItemResult = GetIncentives(incentiveUrl);
                }
                else
                {
                    incentiveItemResult.ErrorMessage = "Style ID is required.";
                    incentiveItemResult.Status = false;
                }
            }
            catch (Exception ex)
            {
                incentiveItemResult.ErrorMessage = (ex.InnerException != null) ? ex.InnerException.Message : ex.Message;
                incentiveItemResult.Status = false;
            }

            return incentiveItemResult;
        }

        /// <summary>
        /// Call Edmunds Incentive API with search parameter
        /// </summary>
        /// <param name="incentiveApiParameters"></param>
        /// <returns>Populates IncentiveItemResult object</returns>
        public IncentiveItemResult GetIncentivesByStyleIDAndZipcode(IncentiveAPIParameters incentiveApiParameters)
        {
            IncentiveItemResult incentiveItemResult = new IncentiveItemResult();

            try
            {
                if (!string.IsNullOrEmpty(incentiveApiParameters.StyleID) && !string.IsNullOrEmpty(incentiveApiParameters.Zipcode))
                {
                    string incentiveUrl = GenerateEditorialAPIUrl(incentiveApiParameters);

                    incentiveItemResult = GetIncentives(incentiveUrl);
                }
                else
                {
                    incentiveItemResult.ErrorMessage = "Style ID & Zipcode are required.";
                    incentiveItemResult.Status = false;
                }
            }
            catch (Exception ex)
            {
                incentiveItemResult.ErrorMessage = (ex.InnerException != null) ? ex.InnerException.Message : ex.Message;
                incentiveItemResult.Status = false;
            }

            return incentiveItemResult;
        }

        #endregion

        #region "Private Methods"

        /// <summary>
        /// Geneate Edmunds editorial request url
        /// </summary>
        /// <returns>Editorial api url based on parameters</returns>
        private string GenerateEditorialAPIUrl(IncentiveAPIParameters incentiveApiParameters)
        {
            string generatedUrl = "http://api.edmunds.com/v1/api/incentive/incentiverepository/";

            if (incentiveApiParameters.IncentiveMethod == IncentiveMethods.findbyid)
            {
                generatedUrl += "findbyid?id=" + incentiveApiParameters.IncentiveID;
            }

            if (incentiveApiParameters.IncentiveMethod == IncentiveMethods.findincentivesbycategoryandzipcode)
            {
                generatedUrl += "findincentivesbycategoryandzipcode?category=" + incentiveApiParameters.Category + "&zipcode=" + incentiveApiParameters.Zipcode;
            }

            if (incentiveApiParameters.IncentiveMethod == IncentiveMethods.findincentivesbymakeid)
            {
                generatedUrl += "findincentivesbymakeid?makeid=" + incentiveApiParameters.MakeID;
            }

            if (incentiveApiParameters.IncentiveMethod == IncentiveMethods.findincentivesbymakeidandzipcode)
            {
                generatedUrl += "findincentivesbymakeidandzipcode?makeid=" + incentiveApiParameters.MakeID + "&zipcode=" + incentiveApiParameters.Zipcode;
            }

            if (incentiveApiParameters.IncentiveMethod == IncentiveMethods.findincentivesbymodelyearidandzipcode)
            {
                generatedUrl += "findincentivesbymodelyearidandzipcode?modelyearid=" + incentiveApiParameters.ModelYearID + "&zipcode=" + incentiveApiParameters.Zipcode;
            }

            if (incentiveApiParameters.IncentiveMethod == IncentiveMethods.findincentivesbystyleid)
            {
                generatedUrl += "findincentivesbystyleid?styleid=" + incentiveApiParameters.StyleID;
            }

            if (incentiveApiParameters.IncentiveMethod == IncentiveMethods.findincentivesbystyleidandzipcode)
            {
                generatedUrl += "findincentivesbystyleidandzipcode?styleid=" + incentiveApiParameters.StyleID + "&zipcode=" + incentiveApiParameters.Zipcode;
            }

            return generatedUrl += "&api_key=n2xnnwjb34ezmufpbwucrnae&fmt=json";
        }

        /// <summary>
        /// Connect to Edmund's Editorial API
        /// </summary>
        /// <returns>Populates Connection object that has the connection status, request string, and erromessage if any</returns>
        private IncentiveConnectionResult DownloadIncentiveAPIAsString(string incentiveAPIUrl)
        {
            IncentiveConnectionResult incentiveConnectionResult = new IncentiveConnectionResult();

            try
            {
                WebClient client = new WebClient();

                incentiveConnectionResult.Value = client.DownloadString(incentiveAPIUrl);
                incentiveConnectionResult.Status = true;
            }
            catch (WebException webEx)
            {
                incentiveConnectionResult.ErrorMessage = (webEx.InnerException != null) ? webEx.InnerException.Message : webEx.Message;
                incentiveConnectionResult.Status = false;
            }
            catch (Exception ex)
            {
                incentiveConnectionResult.ErrorMessage = (ex.InnerException != null) ? ex.InnerException.Message : ex.Message;
                incentiveConnectionResult.Status = false;
            }

            return incentiveConnectionResult;
        }

        /// <summary>
        /// Call Edmunds Editorial API and extract editorial information
        /// </summary>
        /// <param name=""></param>
        /// <returns>Populates EditorialItem object</returns>
        private IncentiveItemResult GetIncentives(string incentiveAPIUrl)
        {
            List<IncentiveItem> incentiveItems = new List<IncentiveItem>();
            IncentiveItemResult incentiveItemResult = new IncentiveItemResult();

            try
            {
                IncentiveConnectionResult incentiveConnectionResult = DownloadIncentiveAPIAsString(incentiveAPIUrl);

                if (incentiveConnectionResult != null && incentiveConnectionResult.Status)
                {
                    if (!string.IsNullOrEmpty(incentiveConnectionResult.Value))
                    {
                        dynamic incentiveDetails = Newtonsoft.Json.JsonConvert.DeserializeObject<dynamic>(incentiveConnectionResult.Value);

                        if (incentiveDetails != null && incentiveDetails.Count > 0)
                        {
                            foreach (var incentive in incentiveDetails)
                            {
                                try
                                {
                                    incentiveItems.Add(PopulateIncentives(incentive));
                                }
                                catch (Exception ex)
                                {

                                    incentiveItemResult.ErrorMessage = (ex.InnerException != null) ? ex.InnerException.Message : ex.Message;
                                    incentiveItemResult.Status = false;
                                }
                            }

                            incentiveItemResult.Value = incentiveItems;
                            incentiveItemResult.Status = true;
                        }
                        else
                        {
                            incentiveItemResult.Status = true;
                            Debug.WriteLine("No Incentives Found");
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                incentiveItemResult.ErrorMessage = (ex.InnerException != null) ? ex.InnerException.Message : ex.Message;
                incentiveItemResult.Status = false;
            }

            return incentiveItemResult;
        }

        /// <summary>
        /// Call Edmunds Editorial API and extract editorial details
        /// </summary>
        /// <param name="editorialDetails">editorialItem object.</param>
        /// <returns>Populates the DealerHolder object</returns>
        private IncentiveItem PopulateIncentives(dynamic incentiveDetails)
        {
            IncentiveItem incentiveItem = new IncentiveItem();

            incentiveItem.id = incentiveDetails.id;
            incentiveItem.type = incentiveDetails.title;
            incentiveItem.contentType = incentiveDetails.contentType;
            incentiveItem.sourceType = incentiveDetails.sourceType;
            incentiveItem.startDate = StringExtensions.ToDateTime((string)incentiveDetails.startDate);

            return incentiveItem;
        }

        #endregion
    }
}