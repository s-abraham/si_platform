﻿using System;

namespace EdmundsAPI.APIClasses
{
    public static class StringExtensions
    {
        /// <summary>
        /// Private function to convert string to datetime datatype
        /// </summary>
        /// <param name="value">date in string</param>
        /// <returns>string date parsed into datetime</returns>
        public static DateTime ToDateTime(string value)
        {
            if (string.IsNullOrEmpty(value))
                return default(DateTime);

            DateTime retVal;

            if (DateTime.TryParse(value, out retVal))
                return retVal;

            return default(DateTime);
        }
    }
}