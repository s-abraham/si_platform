﻿using Connectors.Parameters;
using JobLib;
using SI.BLL;
using SI.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Processor.CitySearchReviewDownloader
{
    public class Processor : ProcessorBase<CitySearchReviewDownloaderData>
    {
        // Need to pass the args through for the default constructor
        public Processor(string[] args) : base(args) { }

        public override void Start()
        {
            JobDTO job = getNextJob();

            while (job != null)
            {
                try
                {
                    setJobStatus(job.ID.Value, JobStatusEnum.Started);
                    job = getNextJob();

                    var px = new SICitySearchReviewXPathParameters()
                    {
                         ParentnodeReviewSummaryXPath  = ProviderFactory.Reviews.GetProcessorSetting("CitySearch.ParentnodeReviewSummaryXPath"),
		                 ReviewCountCollectedXPath  = ProviderFactory.Reviews.GetProcessorSetting("CitySearch.ReviewCountCollectedXPath"),
		                 ParentNodeReviewDetailsXPath  = ProviderFactory.Reviews.GetProcessorSetting("CitySearch.ParentNodeReviewDetailsXPath"),
		                 FullReviewURLXPath  = string.Empty,
                         ReviewTextFullParentXPath = string.Empty,
		                 RatingValueXPath  = ProviderFactory.Reviews.GetProcessorSetting("CitySearch.RatingValueXPath"),
		                 ReviewerNameXPath  = ProviderFactory.Reviews.GetProcessorSetting("CitySearch.ReviewerNameXPath"),
		                 ReviewDateXPath  = ProviderFactory.Reviews.GetProcessorSetting("CitySearch.ReviewDateXPath"),
		                 ReviewTextFullXPath  = ProviderFactory.Reviews.GetProcessorSetting("CitySearch.ReviewTextFullXPath"),
                         ReviewsPerPage = Convert.ToInt32(ProviderFactory.Reviews.GetProcessorSetting("CitySearch.ReviewsPerPage"))
                    };
                }
                catch (Exception ex)
                {
                    try
                    {
                        //ProviderFactory.SDReviews.LogEdmundsException(job.ID.Value, "General Exception", edmundsConnection.HttpRequestResults, ex.ToString(), job.JobData, data.EdmundsDealerID, htmlUrl);
                        setJobStatus(job.ID.Value, JobStatusEnum.Failed);
                    }
                    catch (Exception)
                    {

                    }
                }
                
            }
        }
    }
}
