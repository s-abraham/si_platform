﻿using JobLib;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Scheduler
{
    class Program
    {
        static void Main(string[] args)
        {
            JobLib.Scheduler c = new JobLib.Scheduler();
            c.Start();
            do
            {
                Thread.Sleep(500);
            } while (true);
        }
    }
}
