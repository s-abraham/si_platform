﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Connectors.Content.Parameters;
using Connectors.Parameters;

namespace Connectors.Content
{
    public abstract class SIContentConnection : SIConnection
    {
        protected SIContentConnection(SIContentConnectionParameters siContentConnectionParameters) : base(siContentConnectionParameters)
        {

        }

    }
}
