﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Bitly;
using Connectors.Content.Parameters;

namespace Connectors.Content
{
    public class BitlyConnection : SIContentConnection
    {
        private BitlyContentConnectionParameters connectionParameters;


        #region Initializer
        public BitlyConnection(BitlyContentConnectionParameters siContentConnectionParameters) : base(siContentConnectionParameters)
        {
            connectionParameters = siContentConnectionParameters;
        }
        #endregion


        public LinkStats GetLinkStats(string shortURL)
        {
            var result = new LinkStats();

            result = LinkMetrics.GetLinkStats(shortURL);

            return result;
        }

        public ShortenResult ShortenURL(string longURL)
        {
            var result = new ShortenResult();

            result = Link.Shorten(longURL);

            return result;
        }

        public ExpandResult ExpandURL(string shortURL)
        {
            var result = new ExpandResult();

            result = Link.Expand(shortURL);

            return result;
        }

        public LookupResult Lookup(string longURL)
        {
            var result = new LookupResult();

            result = Link.Lookup(longURL);

            return result;
        }

        public InfoResult Info(string shortURL)
        {
            var result = new InfoResult();

            result = Link.Info(shortURL);

            return result;
        }

        protected override void dispose()
        {
            
        }
    }
}
