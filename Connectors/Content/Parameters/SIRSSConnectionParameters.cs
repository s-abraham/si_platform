﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Connectors.Content.Parameters
{
    public class SIRSSConnectionParameters : SIContentConnectionParameters
    {
        #region Properties

        public SIRSSParameters rssParameters;
        
        #endregion

        public SIRSSConnectionParameters(SIRSSParameters siRSSParameters, int timeOutSeconds = 30, bool useProxy = false, int maxRedirects = 1) : base(timeOutSeconds, useProxy, maxRedirects)
        {
            rssParameters = siRSSParameters;
        }
    }
}
