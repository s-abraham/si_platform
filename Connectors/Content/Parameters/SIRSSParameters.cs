﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Connectors.Content.Parameters
{
    public class SIRSSParameters
    {
        public long RSSFeedID { get; set; }
        public string RssUrl { get; set; }
        public int DaysBack { get; set; }
        public int MaxItems { get; set; }
        public string IncludeKeywords { get; set; }
        public string ExcludeKeywords { get; set; }
        public bool IncludeImages { get; set; }
        public DateTime OldestDate { get; set; }

        //List<string> rssUrls, int? daysBack, int? maxItems, string includeKeywords, string excludeKeywords, bool includeImages = false
    }
}
