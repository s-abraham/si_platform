﻿namespace Connectors.Content.Parameters
{
    public class EdmundsVehicleContentConnectionParameters : SIContentConnectionParameters
    {
        public EdmundsVehicleContentConnectionParameters(int timeOutSeconds, bool useProxy, int maxRedirects) : base(timeOutSeconds, useProxy, maxRedirects)
        {

        }
    }
}