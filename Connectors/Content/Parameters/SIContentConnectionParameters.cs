﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Connectors.Parameters;

namespace Connectors.Content.Parameters
{
    public abstract class SIContentConnectionParameters: SIConnectionParameters
	{
        protected SIContentConnectionParameters(int timeOutSeconds, bool useProxy, int maxRedirects) : base(timeOutSeconds, useProxy, maxRedirects)
		{
			
		}
	}
}
