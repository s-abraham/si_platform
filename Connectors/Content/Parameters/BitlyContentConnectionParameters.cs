﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Connectors.Content.Parameters
{
    public class BitlyContentConnectionParameters : SIContentConnectionParameters
    {
        public BitlyContentConnectionParameters(int timeOutSeconds, bool useProxy, int maxRedirects) : base(timeOutSeconds, useProxy, maxRedirects)
        {

        }

    }
}
