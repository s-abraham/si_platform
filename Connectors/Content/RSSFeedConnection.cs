﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Runtime.InteropServices;
using System.Security.Policy;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;
using System.ServiceModel.Syndication;
using System.Xml;
using Connectors.Content.Parameters;
using Connectors.Entities;
using Google.Model;
using HtmlAgilityPack;
using Microsoft.SqlServer.Server;
using SI;

namespace Connectors.Content
{
    public class RSSFeedConnection
    {
        private Stopwatch _sw = new Stopwatch();
        //private string _rssUrl = string.Empty;
        //private int _daysBack = 0;
        //private DateTime _oldestDate;
        //private int? _maxItems = null;
        //private string _includeKeywords = string.Empty;
        //private string _excludeKeywords = string.Empty;
        //private bool _includeImages = false;

        private SIRSSConnectionParameters _siRSSConnectionParameters;

        public RSSFeedConnection()
        {

        }

        public RSSFeedConnection(SIRSSConnectionParameters siRSSConnectionParameters)
        {
            _siRSSConnectionParameters = siRSSConnectionParameters;
            // =  ?? ;

            if (siRSSConnectionParameters.rssParameters.DaysBack == 0)
            {
                _siRSSConnectionParameters.rssParameters.OldestDate = DateTime.Today.AddDays(-365);
            }
            else
            {
                _siRSSConnectionParameters.rssParameters.OldestDate = DateTime.Today.AddDays(-1 * _siRSSConnectionParameters.rssParameters.DaysBack);
            }
        }


        public void RunRSSTest()
        {
            string url = "http://feeds.feedburner.com/caranddriver/blog";
            XmlReader reader = XmlReader.Create(url);
            SyndicationFeed feed = SyndicationFeed.Load(reader);
            reader.Close();
            var tags = new List<string>();


            foreach (SyndicationItem item in feed.Items)
            {
                Debug.WriteLine("Subject: " + item.Title.Text);
                Debug.WriteLine("Link: " + item.Links[0].Uri.AbsoluteUri);

                Debug.WriteLine("Summary: " + item.Summary.Text);
                Debug.WriteLine("PubDate: " + item.PublishDate.ToString());

                tags.AddRange(item.Categories.Select(c => c.Name.ToString().ToLower()));
            }
        }


        public RSSEntities DownloadRSSItems()
        {
            var result = new RSSEntities();
            var urlsProcessedCount = 0;
            urlsProcessedCount++;
            var recordCount = 0;
            result.RSSFeedID = _siRSSConnectionParameters.rssParameters.RSSFeedID;
            result.RSSUrl = _siRSSConnectionParameters.rssParameters.RssUrl;

            var feedResults = FetchFeed();

            if (feedResults.Items.Any())
            {
                foreach (var feedItem in feedResults.Items)
                {
                    recordCount++;
                    //Debug.WriteLine("#" + recordCount);
                    if (recordCount <= _siRSSConnectionParameters.rssParameters.MaxItems)
                    {
                        var extractedItem = ExtractRSSItem(feedItem);

                        if (extractedItem.PubDate >= _siRSSConnectionParameters.rssParameters.OldestDate)
                        {
                            result.RSSItems.Add(extractedItem);
                        }
                        else
                        {

                        }
                    }
                    result.Success = true;
                }



                Debug.WriteLine("Found {0} results", feedResults.Items.Count());
                Debug.WriteLine("  {0} - {1}", 1, _sw.ElapsedMilliseconds);
                _sw.Restart();
            }

            return result;
        }

        private List<string> ExtractImageLinks2(string url)
        {
            var results = new List<string>();

            var document = new HtmlWeb().Load(url);
            results = document.DocumentNode.Descendants("img")
                           .Select(e => e.GetAttributeValue("src", null))
                           .Where(s => !String.IsNullOrEmpty(s))
                           .Where(f => f.EndsWith(".png") || f.EndsWith(".jpg")).ToList();
            return results;
        }

        private List<string> ExtractImageLinks(string url)
        {
            _sw.Restart();
            var results = new List<string>();

            //WebClient client = new WebClient();
            //client.Proxy = null;
            //string html = client.DownloadString(url);


            //HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
            //request.Proxy = null; // <-- this is the good stuff
            //HttpWebResponse response = (HttpWebResponse)request.GetResponse();
            //Stream responseStream = response.GetResponseStream();
            //StreamReader streamReader = new StreamReader(responseStream);
            //string html = streamReader.ReadToEnd();

            System.Net.ServicePointManager.Expect100Continue = false;
            using (WebClient client = new WebClient())
            {
                client.Proxy = null;
                client.Headers.Add("user-agent", "Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.2; .NET CLR 1.0.3705;)");

                var data = client.OpenRead(url);
                string html = "";
                using (StreamReader reader = new StreamReader(data))
                {
                    html = reader.ReadToEnd();
                    data.Close();
                    reader.Close();
                }

                HtmlDocument doc = new HtmlDocument();
                doc.LoadHtml(html);

                List<HtmlNode> imageNodes = null;
                imageNodes = (from HtmlNode node in doc.DocumentNode.SelectNodes("//img")
                              where node.Name == "img"
                              && (node.Attributes["src"].Value.EndsWith("png") || node.Attributes["src"].Value.EndsWith("jpg"))
                              select node).ToList();

                foreach (HtmlNode node in imageNodes)
                {
                    results.Add(node.Attributes["src"].Value);
                }
            }

            return results;
        }

        private RSSItem ExtractRSSItem(SyndicationItem item)
        {
            _sw.Start();
            var result = new RSSItem();

            result.Title = item.Title.Text;
            //Debug.WriteLine("{0} - {1}", 1, _sw.ElapsedMilliseconds);
            //_sw.Restart();

            try
            {
                result.Summary = item.Summary.Text;
            }
            catch
            {
                result.Summary = "";
            }

            try
            {
                HtmlDocument htmlDoc = new HtmlDocument();
                htmlDoc.LoadHtml(result.Summary);
                result.SummaryAsText = htmlDoc.DocumentNode.InnerText;
            }
            catch
            {
                result.SummaryAsText = "";
            }

            try
            {
                result.PubDate = DateTime.Parse(item.PublishDate.ToString());
            }
            catch
            {
                result.PubDate = DateTime.Now;
            }

            try
            {
                result.Link = item.Links[0].Uri.AbsoluteUri;

                if (!string.IsNullOrEmpty(result.Link))
                {
                    if (_siRSSConnectionParameters.rssParameters.IncludeImages)
                    {
                        var imagesList = ExtractImageLinks(result.Link);

                        if (imagesList.Any())
                        {
                            result.Images = imagesList;
                        }
                    }
                }
            }
            catch
            {
                result.Link = "";
            }

            try
            {
                result.Categories.AddRange(item.Categories.Select(c => c.Name.ToString().ToLower()));
            }
            catch
            {
                result.Categories = new List<string>();
            }

            result.Hash = Security.GetHash(result.Title.Replace(" ", "").Trim().ToLower(), 25);
            result.PermaLink = "";
            result.Thumbnail = "";
            result.SourceRSS = _siRSSConnectionParameters.rssParameters.RssUrl;
            return result;
        }

        private SyndicationFeed FetchFeed()
        {
            var result = new SyndicationFeed();

            if (!string.IsNullOrEmpty(_siRSSConnectionParameters.rssParameters.RssUrl))
            {
                string url = _siRSSConnectionParameters.rssParameters.RssUrl;
                XmlReader reader = XmlReader.Create(url);
                result = SyndicationFeed.Load(reader);
                reader.Close();
            }
            return result;
        }

    }


}
