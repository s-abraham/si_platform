﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json.Linq;

namespace Connectors.Content.Parameters
{
    public class EdmundsVehicleContentConnection : SIContentConnection
    {
        private EdmundsVehicleContentConnectionParameters connectionParameters;

        #region Initializer
        public EdmundsVehicleContentConnection(EdmundsVehicleContentConnectionParameters siContentConnectionParameters) : base(siContentConnectionParameters)
        {
            connectionParameters = siContentConnectionParameters;
        }
        #endregion

        public void GetAPI()
        {
            var url = "http://api.edmunds.com/v1/api/tco/getstyleswithtcodatabysubmodel?make=bmw&model=3series&year=2011&submodel=sedan&api_key=n2xnnwjb34ezmufpbwucrnae&fmt=atom";
            ConnectionStatus httpStatus = ConnectorHttpRequest(url);

            if (httpStatus == ConnectionStatus.Success)
            {
                if (!string.IsNullOrEmpty(HttpRequestResults))
                {

                    var jsSerializer = new System.Web.Script.Serialization.JavaScriptSerializer();
                    Dictionary<string, object> resultDictionary = (Dictionary<string, object>)jsSerializer.DeserializeObject(HttpRequestResults);

                    if (resultDictionary != null)
                    {
                        var styles = (Dictionary<string, Object>)resultDictionary.FirstOrDefault(x => x.Key != null && x.Key == "styles").Value;
                        
                        var i = 23;
                        foreach (dynamic pair in styles)
                        {
                            var id = pair.Value["id"];
                            var price = pair.Value["price"];
                        }
                    }
                }
            }
        }

        protected override void dispose()
        {
            
        }
    }
}
