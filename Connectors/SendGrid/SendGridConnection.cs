﻿using SendGrid;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Connectors
{
    public class SendGridConnection : SIConnection
    {
        private SendGridConnectionParameters ConnectionParameters;

        #region INITIALIZER
        public SendGridConnection(SendGridConnectionParameters sendGridConnectionParameters)
            : base(sendGridConnectionParameters)
        {
            ConnectionParameters = sendGridConnectionParameters;
        }
        #endregion

        #region METHODS

        public List<BouncedEmail> GetBouncedEmails()
        {
            return SendGridAPI.GetBouncedEmails(ConnectionParameters.parameters.StartDate, ConnectionParameters.parameters.EndDate, ConnectionParameters.parameters.BounceType, ConnectionParameters.parameters.EmailAddress);
        }

        public List<BlockedEmail> GetBlockedEmails()
        {
            return SendGridAPI.GetBlockedEmails(ConnectionParameters.parameters.StartDate, ConnectionParameters.parameters.EndDate);
        }

        public List<InvalidEmail> GetInvalidEmails()
        {
            return SendGridAPI.GetInvalidEmails(ConnectionParameters.parameters.StartDate, ConnectionParameters.parameters.EndDate);
        }

        public bool DeleteBouncedEmail()
        {
            return SendGridAPI.DeleteBouncedEmail(ConnectionParameters.parameters.EmailAddress, ConnectionParameters.parameters.BounceType);
        }

        public bool DeleteblockedEmail()
        {
            return SendGridAPI.DeleteBlockedEmail(ConnectionParameters.parameters.EmailAddress);
        }

        public bool DeleteInvalidEmail()
        {
            return SendGridAPI.DeleteInvalidEmail(ConnectionParameters.parameters.EmailAddress);
        }

        #endregion

        #region OVERRIDE METHODS
        protected override void dispose()
        {

        }
        #endregion
    }
}
