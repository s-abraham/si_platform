﻿using SendGrid;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Connectors
{
    public class SendGridParameters
    {
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public SendGridBounceTypeEnum BounceType { get; set; }
        public string EmailAddress { get; set; }
    }
}
