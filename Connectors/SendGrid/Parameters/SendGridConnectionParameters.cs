﻿using Connectors.Parameters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Connectors
{
    public class SendGridConnectionParameters : SIConnectionParameters
    {
        #region Properties

        public SendGridParameters parameters;

        #endregion

        public SendGridConnectionParameters(SendGridParameters sendGridParameters, int timeOutSeconds = 30, bool useProxy = false, int maxRedirects = 1)
			: base(timeOutSeconds, useProxy, maxRedirects)
		{
            parameters = sendGridParameters;            
		}
    }
}
