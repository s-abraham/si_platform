﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using Connectors.Entities;
using HtmlAgilityPack;
using SI;
using SI.Extensions;

namespace Connectors.Publish
{
	/// <summary>
	/// 
	/// </summary>
	public static class HtmlPreviewConnection
	{
		public static LinkPreview GetLinkPreview(string requestUrl)
		{
			LinkPreview linkPreview = new LinkPreview();

			using (SInet WebSInet = new SInet(true, 10, "", 15000))
			{
				string HttpRequestResults = string.Empty;
				try
				{
					HttpRequestResults = WebSInet.DownloadString(requestUrl, null);
				}
				catch (Exception) { HttpRequestResults = string.Empty; } //needed for urls that can not be resolved;
				
				if (!string.IsNullOrEmpty(HttpRequestResults))
				{
					HtmlDocument htmlDocument = new HtmlDocument();
					htmlDocument.OptionFixNestedTags = true;
					htmlDocument.LoadHtml(HttpRequestResults);

					HtmlNode titleNode = htmlDocument.DocumentNode.SelectSingleNode("//*/head/title");
					if (titleNode != null)
						linkPreview.PageTitle = HttpUtility.HtmlDecode(titleNode.InnerText.Trim());

					HtmlNode descriptonNode = htmlDocument.DocumentNode.SelectSingleNode("//*/head/meta[@name='description']");
					if (descriptonNode != null)
					{
						linkPreview.PageDescription = HttpUtility.HtmlDecode(descriptonNode.GetAttributeValue("content", ""));

					}

					Uri requestUri = new Uri(requestUrl);

					linkPreview.PageImages = htmlDocument.DocumentNode.Descendants("img")
						.Select(e => e.GetAttributeValue("src", "").Contains("http://") ? e.GetAttributeValue("src", "") :  requestUri.Scheme +"://" + requestUri.Authority + "/" + e.GetAttributeValue("src", ""))
						.Where(s => !String.IsNullOrEmpty(s) && (s.ToLower().EndsWith(".jpg") || s.ToLower().EndsWith(".png") || s.ToLower().EndsWith(".jpeg"))).Distinct().Take(40).ToList();
					
					linkPreview.IsValidUrl = true;

				}
			}
			return linkPreview;
		}
	}
}
