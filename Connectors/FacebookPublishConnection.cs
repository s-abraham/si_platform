﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Script.Serialization;
using Connectors.Entities;
using Connectors.Parameters;

namespace Connectors
{
    public class FacebookPublishConnection : SISocialConnection
    {
        private SIFacebookPublishConnectionParameters ConnectionParameters;

        #region INITIALIZER
        public FacebookPublishConnection(SIFacebookPublishConnectionParameters siFacebookPublishConnectionParameters)
            : base(siFacebookPublishConnectionParameters)
        {
            ConnectionParameters = siFacebookPublishConnectionParameters;
        }
        #endregion

        #region VARIABLE
        #endregion

        #region WALL POST

        public FacebookResponse CreateWallPost()
        {
            FacebookResponse ObjFacebookResponse = new FacebookResponse();
            //PublishManager objPublishManager = new PublishManager();
            try
            {
                //if (1 == 1)
                //{
                    if (string.IsNullOrEmpty(ConnectionParameters.publishParameters.FacebookGraphAPIURLWallPostCreate))
                    {
                        ConnectionParameters.publishParameters.FacebookGraphAPIURLWallPostCreate = "https://graph.facebook.com/{0}/feed?access_token={1}";
                    }

                    string URL = string.Format(ConnectionParameters.publishParameters.FacebookGraphAPIURLWallPostCreate, ConnectionParameters.publishParameters.UniqueID, ConnectionParameters.publishParameters.Token);
                                    
                    string rawResponse = string.Empty;
                    try
                    {
                        HttpRequestResults = string.Empty;
                        ConnectionStatus httpStatus = ConnectorHttpRequest(URL);

                        if (httpStatus == ConnectionStatus.Success)
                        {
                            if (!string.IsNullOrEmpty(HttpRequestResults))
                            {
                                rawResponse = HttpRequestResults;

                                var jss = new JavaScriptSerializer();
                                if (rawResponse.Contains("error"))
                                {
                                    rawResponse = rawResponse.Replace(@"{""error"":", "");
                                    rawResponse = rawResponse.Substring(0, rawResponse.Length - 1);
                                }
                                ObjFacebookResponse = jss.Deserialize<FacebookResponse>(rawResponse);
                            }
                            else { }
                        }
                    }
                    catch (Exception ex)
                    {
                        #region ERROR HANDLING

                        //PublishExceptionLog ObjPublishExceptionLog = new PublishExceptionLog();
                        //ObjPublishExceptionLog.DealerLocationSocialNetworkPostID = Convert.ToInt32(obj.PostID);
                        //ObjPublishExceptionLog.DealerLocationSocialNetworkPostQueueID = Convert.ToInt32(obj.PostQueueID);
                        //ObjPublishExceptionLog.ExceptionMessage = ex.Message;
                        //ObjPublishExceptionLog.InnerException = ex.InnerException == null ? "" : ex.InnerException.Message;
                        //ObjPublishExceptionLog.PostResponse = string.Empty;
                        //ObjPublishExceptionLog.StackTrace = ex.StackTrace;
                        //ObjPublishExceptionLog.MethodName = "FaceBook\\CreateFacebookWallPost";
                        //ObjPublishExceptionLog.ExtraInfo = rawResponse;
                        //objPublishManager.AddPublishExceptionLog(ObjPublishExceptionLog);

                        //if (!string.IsNullOrEmpty(rawResponse))
                        //{
                        //    ObjFacebookResponse.message = ex.Message;
                        //    ObjFacebookResponse.code = string.Empty;
                        //    ObjFacebookResponse.type = "Exception\\CreateFacebookWallPost";
                        //    ObjFacebookResponse.ID = string.Empty;
                        //}

                        #endregion
                    }
                
            }
            catch (Exception ex)
            {
                #region ERROR HANDLING

                //PublishExceptionLog ObjPublishExceptionLog = new PublishExceptionLog();
                //ObjPublishExceptionLog.DealerLocationSocialNetworkPostID = Convert.ToInt32(obj.PostID);
                //ObjPublishExceptionLog.DealerLocationSocialNetworkPostQueueID = Convert.ToInt32(obj.PostQueueID);
                //ObjPublishExceptionLog.ExceptionMessage = ex.Message;
                //ObjPublishExceptionLog.InnerException = ex.InnerException == null ? "" : ex.InnerException.Message;
                //ObjPublishExceptionLog.PostResponse = string.Empty;
                //ObjPublishExceptionLog.StackTrace = ex.StackTrace;
                //ObjPublishExceptionLog.MethodName = "FaceBook\\CreateFacebookWallPost";
                //ObjPublishExceptionLog.ExtraInfo = string.Empty;
                //objPublishManager.AddPublishExceptionLog(ObjPublishExceptionLog);

                //ObjFacebookResponse.ID = string.Empty;
                //ObjFacebookResponse.message = ex.Message;
                //ObjFacebookResponse.type = "Exception\\CreateFacebookWallPost";
                //ObjFacebookResponse.code = string.Empty;

                #endregion
            }
            return ObjFacebookResponse;
        }

        #endregion

        #region Photo

        //public FacebookResponse UploadFacebookPhoto(string Token, string message, string filename, string Album_id)
        //{
        //    FacebookResponse ObjFacebookResponse = new FacebookResponse();
        //    //PublishManager objPublishManager = new PublishManager();
        //    try
        //    {
        //        if (!string.IsNullOrEmpty(Token))
        //        {
        //            if (string.IsNullOrEmpty(ConnectionParameters.publishParameters.FacebookGraphAPIURLUploadPhoto))
        //            {
        //                ConnectionParameters.publishParameters.FacebookGraphAPIURLUploadPhoto = "https://graph.facebook.com/";
        //            }
                  
        //            string rawResponse = string.Empty;
        //            try
        //            {
        //                FacebookRequest objFacebookRequest = new FacebookRequest();

        //                //objFacebookRequest.RequestURL = FacebookGraphAPIURLUploadPhoto;
        //                //objFacebookRequest.Method = "POST";
        //                //objFacebookRequest.TimeOut = Convert.ToInt32(FacebookAPITimeOut);
        //                objFacebookRequest.Message = message;
        //                objFacebookRequest.Filename = Guid.NewGuid().ToString();
        //                objFacebookRequest.Album_id = Album_id;
        //                byte[] bytes = null;
        //                objFacebookRequest.bytes = DownloadData(filename,null,null);

        //                // Create Path                
        //                if (!String.IsNullOrEmpty(_facebookRequest.Album_id))
        //                {
        //                    _facebookRequest.RequestURL += _facebookRequest.Album_id + "/";
        //                }
        //                _facebookRequest.RequestURL += "photos";

        //                UserAgent = "Mozilla/4.0 (compatible; Windows NT)";
        //                ContentType = "multipart/form-data; boundary=" + boundary;

        //                // Create Boundary
        //                string boundary = "---------------------------" + DateTime.Now.Ticks.ToString("x");

        //                // Create Path                
        //                if (!String.IsNullOrEmpty(_facebookRequest.Album_id))
        //                {
        //                    _facebookRequest.RequestURL += _facebookRequest.Album_id + "/";
        //                }
        //                _facebookRequest.RequestURL += "photos";

        //                // New String Builder
        //                StringBuilder sb = new StringBuilder();

        //                // Add Form Data
        //                string formdataTemplate = "--{0}\r\nContent-Disposition: form-data; name=\"{1}\"\r\n\r\n{2}\r\n";

        //                // Access Token
        //                sb.AppendFormat(formdataTemplate, boundary, "access_token", Token);

        //                // Message
        //                sb.AppendFormat(formdataTemplate, boundary, "message", _facebookRequest.Message);

        //                // Header
        //                string headerTemplate = "--{0}\r\nContent-Disposition: form-data; name=\"{1}\"; filename=\"{2}\"\r\nContent-Type: {3}\r\n\r\n";

        //                sb.AppendFormat(headerTemplate, boundary, "source", _facebookRequest.Filename, @"application/octet-stream");

        //                // File
        //                string formString = sb.ToString();
        //                byte[] formBytes = Encoding.UTF8.GetBytes(formString);
        //                byte[] trailingBytes = Encoding.UTF8.GetBytes("\r\n--" + boundary + "--\r\n");
        //                byte[] image _facebookRequest.bytes;

        //                // Memory Stream
        //                MemoryStream imageMemoryStream = new MemoryStream();
        //                imageMemoryStream.Write(image, 0, image.Length);

        //                // Set Content Length
        //                long imageLength = imageMemoryStream.Length;
        //                //long contentLength = formBytes.Length + imageLength + trailingBytes.Length;
        //                //httpRequest.ContentLength = contentLength;

        //                // Get Request Stream
        //                //httpRequest.AllowWriteStreamBuffering = false;
        //                //Stream strm_out = httpRequest.GetRequestStream();

        //                // Write to Stream
        //                //strm_out.Write(formBytes, 0, formBytes.Length);
        //                byte[] buffer = new Byte[checked((uint)Math.Min(4096, (int)imageLength))];



        //                //List<byte> newbytes = new List<byte>();
        //                //newbytes.AddRange(formBytes);
        //                //newbytes.AddRange(trailingBytes);
        //                //newbytes.AddRange(image);
        //                //newbytes.AddRange(buffer);

        //                //byte[] bufferArry = newbytes.ToArray();


        //                //rawResponse = ExecuteCommand_Photo(objFacebookRequest, Token);

        //                if (!string.IsNullOrEmpty(rawResponse))
        //                {
        //                    var jss = new JavaScriptSerializer();
        //                    if (rawResponse.Contains("error"))
        //                    {
        //                        rawResponse = rawResponse.Replace(@"{""error"":", "");
        //                        rawResponse = rawResponse.Substring(0, rawResponse.Length - 1);
        //                    }
        //                    ObjFacebookResponse = jss.Deserialize<FacebookResponse>(rawResponse);
        //                }
        //                else
        //                {
        //                }
        //                ObjFacebookResponse.PhotoURL = filename;
        //                //result = app.Post(dlSocialNetwork.UniqueID + "/feed", parameters);
        //            }

        //            catch (Exception ex)
        //            {
        //                #region ERROR HANDLING

        //                //PublishExceptionLog ObjPublishExceptionLog = new PublishExceptionLog();
        //                //ObjPublishExceptionLog.DealerLocationSocialNetworkPostID = Convert.ToInt32(obj.PostID);
        //                //ObjPublishExceptionLog.DealerLocationSocialNetworkPostQueueID = Convert.ToInt32(obj.PostQueueID);
        //                //ObjPublishExceptionLog.ExceptionMessage = ex.Message;
        //                //ObjPublishExceptionLog.InnerException = ex.InnerException == null ? "" : ex.InnerException.Message;
        //                //ObjPublishExceptionLog.PostResponse = string.Empty;
        //                //ObjPublishExceptionLog.StackTrace = ex.StackTrace;
        //                //ObjPublishExceptionLog.MethodName = "FaceBook\\UploadFacebookPhoto";
        //                //ObjPublishExceptionLog.ExtraInfo = rawResponse;
        //                //objPublishManager.AddPublishExceptionLog(ObjPublishExceptionLog);

        //                //if (!string.IsNullOrEmpty(rawResponse))
        //                //{
        //                //    ObjFacebookResponse.message = ex.Message;
        //                //    ObjFacebookResponse.code = string.Empty;
        //                //    ObjFacebookResponse.type = "Exception\\UploadFacebookPhoto";
        //                //    ObjFacebookResponse.ID = string.Empty;
        //                //}

        //                #endregion
        //            }


        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        #region ERROR HANDLING

        //        //PublishExceptionLog ObjPublishExceptionLog = new PublishExceptionLog();
        //        //ObjPublishExceptionLog.DealerLocationSocialNetworkPostID = Convert.ToInt32(obj.PostID);
        //        //ObjPublishExceptionLog.DealerLocationSocialNetworkPostQueueID = Convert.ToInt32(obj.PostQueueID);
        //        //ObjPublishExceptionLog.ExceptionMessage = ex.Message;
        //        //ObjPublishExceptionLog.InnerException = ex.InnerException == null ? "" : ex.InnerException.Message;
        //        //ObjPublishExceptionLog.PostResponse = string.Empty;
        //        //ObjPublishExceptionLog.StackTrace = ex.StackTrace;
        //        //ObjPublishExceptionLog.MethodName = "FaceBook\\UploadFacebookPhoto";
        //        //ObjPublishExceptionLog.ExtraInfo = string.Empty;
        //        //objPublishManager.AddPublishExceptionLog(ObjPublishExceptionLog);

        //        //ObjFacebookResponse.ID = string.Empty;
        //        //ObjFacebookResponse.message = ex.Message;
        //        //ObjFacebookResponse.type = "Exception\\UploadFacebookPhoto";
        //        //ObjFacebookResponse.code = string.Empty;

        //        #endregion
        //    }

        //    return ObjFacebookResponse;
        //}
                
        #endregion

        #region CREATE ALBUM

        public FacebookResponse CreateAlbum()
        {
            FacebookResponse ObjFacebookResponse = new FacebookResponse();
            //PublishManager objPublishManager = new PublishManager();
            try
            {
                //https://graph.facebook.com/142257429131489/albums?name=Photo Album 2&message=Test Photo Album from Post Method 2&access_token=AAAAANqD36fQBAK0dzaCrQbZCGhXnxRTL2abdGBtKxUoWRdhCddQQlXqV2lVlX3ZBBqU3sOO28i3pPYsDUR2ZBm0pNXXDrZAHe4IflcqI5UIEvnXeNPuG
                if (string.IsNullOrEmpty(ConnectionParameters.publishParameters.FacebookGraphAPIURLAlbumCreate))
                {
                    ConnectionParameters.publishParameters.FacebookGraphAPIURLAlbumCreate = "https://graph.facebook.com/{0}/albums?access_token={1}";
                }

                if (!string.IsNullOrEmpty(ConnectionParameters.publishParameters.album_Name) && !string.IsNullOrEmpty(ConnectionParameters.publishParameters.album_Message) && !string.IsNullOrEmpty(ConnectionParameters.publishParameters.UniqueID) && !string.IsNullOrEmpty(ConnectionParameters.publishParameters.Token))
                {
                    string URL = string.Format(ConnectionParameters.publishParameters.FacebookGraphAPIURLAlbumCreate, ConnectionParameters.publishParameters.UniqueID, ConnectionParameters.publishParameters.Token);

                    string rawResponse = string.Empty;
                    try
                    {                        
                        HttpRequestResults = string.Empty;
                        ConnectionStatus httpStatus = ConnectorHttpRequest(URL);
                        if (httpStatus == ConnectionStatus.Success)
                        {
                            if (!string.IsNullOrEmpty(HttpRequestResults))
                            {
                                rawResponse = HttpRequestResults;

                                var jss = new JavaScriptSerializer();
                                if (rawResponse.Contains("error"))
                                {
                                    rawResponse = rawResponse.Replace(@"{""error"":", "");
                                    rawResponse = rawResponse.Substring(0, rawResponse.Length - 1);
                                }
                                ObjFacebookResponse = jss.Deserialize<FacebookResponse>(rawResponse);
                            }
                            else
                            {
                            }  
                        }
                    }
                    catch (Exception ex)
                    {
                        #region ERROR HANDLING

                        //PublishExceptionLog ObjPublishExceptionLog = new PublishExceptionLog();
                        //ObjPublishExceptionLog.DealerLocationSocialNetworkPostID = 0;
                        //ObjPublishExceptionLog.DealerLocationSocialNetworkPostQueueID = 0;
                        //ObjPublishExceptionLog.ExceptionMessage = ex.Message;
                        //ObjPublishExceptionLog.InnerException = ex.InnerException == null ? "" : ex.InnerException.Message;
                        //ObjPublishExceptionLog.PostResponse = string.Empty;
                        //ObjPublishExceptionLog.StackTrace = ex.StackTrace;
                        //ObjPublishExceptionLog.MethodName = "FaceBook\\CreateFacebookAlbum";
                        //ObjPublishExceptionLog.ExtraInfo = rawResponse;
                        //objPublishManager.AddPublishExceptionLog(ObjPublishExceptionLog);

                        //if (!string.IsNullOrEmpty(rawResponse))
                        //{
                        //    ObjFacebookResponse.message = ex.Message;
                        //    ObjFacebookResponse.code = string.Empty;
                        //    ObjFacebookResponse.type = "Exception";
                        //    ObjFacebookResponse.ID = string.Empty;
                        //}

                        #endregion
                    }
                }
                else
                {
                    ObjFacebookResponse.message = "Invalid Parameters";
                }
            }
            catch (Exception ex)
            {
                #region ERROR HANDLING

                //PublishExceptionLog ObjPublishExceptionLog = new PublishExceptionLog();
                //ObjPublishExceptionLog.DealerLocationSocialNetworkPostID = Convert.ToInt32(0);
                //ObjPublishExceptionLog.DealerLocationSocialNetworkPostQueueID = Convert.ToInt32(0);
                //ObjPublishExceptionLog.ExceptionMessage = ex.Message;
                //ObjPublishExceptionLog.InnerException = ex.InnerException == null ? "" : ex.InnerException.Message;
                //ObjPublishExceptionLog.PostResponse = string.Empty;
                //ObjPublishExceptionLog.StackTrace = ex.StackTrace;
                //ObjPublishExceptionLog.MethodName = "FaceBook\\CreateFacebookAlbum";
                //ObjPublishExceptionLog.ExtraInfo = string.Empty;
                //objPublishManager.AddPublishExceptionLog(ObjPublishExceptionLog);

                //ObjFacebookResponse.ID = string.Empty;
                //ObjFacebookResponse.message = ex.Message;
                //ObjFacebookResponse.type = "Exception\\CreateFacebookAlbum";
                //ObjFacebookResponse.code = string.Empty;

                #endregion
            }

            return ObjFacebookResponse;
        }

        #endregion

        #region CREATE QUESTION

        //string UniqueID, string Token, string Question, string options, string AllowNewOptions
        public FacebookResponse CreateQuestion()
        {
            FacebookResponse ObjFacebookResponse = new FacebookResponse();
            //PublishManager objPublishManager = new PublishManager();
            try
            {
                //https://graph.facebook.com/142257429131489/questions&options=['IBM','EDS','MICROSOFT']&allow_new_options=False&question=which is your fav. Cell Phone 11?
                if (string.IsNullOrEmpty(ConnectionParameters.publishParameters.FacebookGraphAPIURLQuestionCreate))
                {
                    ConnectionParameters.publishParameters.FacebookGraphAPIURLQuestionCreate = "https://graph.facebook.com/{0}/questions?access_token={1}";
                }

                string URL = string.Format(ConnectionParameters.publishParameters.FacebookGraphAPIURLQuestionCreate, ConnectionParameters.publishParameters.UniqueID, ConnectionParameters.publishParameters.Token);
                                
                if (!string.IsNullOrEmpty(ConnectionParameters.publishParameters.UniqueID) && !string.IsNullOrEmpty(ConnectionParameters.publishParameters.Token) && !string.IsNullOrEmpty(ConnectionParameters.publishParameters.Question_Text))
                {                    
                    if (ConnectionParameters.publishParameters.Question_OptionsCount <= 10)
                    {
                        string rawResponse = string.Empty;
                        try
                        {                            
                            HttpRequestResults = string.Empty;
                            ConnectionStatus httpStatus = ConnectorHttpRequest(URL);
                            if (httpStatus == ConnectionStatus.Success)
                            {
                                if (!string.IsNullOrEmpty(HttpRequestResults))
                                {
                                    rawResponse = HttpRequestResults;
                                    var jss = new JavaScriptSerializer();
                                    if (rawResponse.Contains("error"))
                                    {
                                        rawResponse = rawResponse.Replace(@"{""error"":", "");
                                        rawResponse = rawResponse.Substring(0, rawResponse.Length - 1);
                                    }
                                    ObjFacebookResponse = jss.Deserialize<FacebookResponse>(rawResponse);
                                }
                                else
                                {
                                }
                            }
                        }
                        catch (Exception ex)
                        {
                            #region ERROR HANDLING

                            //PublishExceptionLog ObjPublishExceptionLog = new PublishExceptionLog();
                            //ObjPublishExceptionLog.DealerLocationSocialNetworkPostID = 0;
                            //ObjPublishExceptionLog.DealerLocationSocialNetworkPostQueueID = 0;
                            //ObjPublishExceptionLog.ExceptionMessage = ex.Message;
                            //ObjPublishExceptionLog.InnerException = ex.InnerException == null ? "" : ex.InnerException.Message;
                            //ObjPublishExceptionLog.PostResponse = string.Empty;
                            //ObjPublishExceptionLog.StackTrace = ex.StackTrace;
                            //ObjPublishExceptionLog.MethodName = "FaceBook\\CreateFacebookQuestion";
                            //ObjPublishExceptionLog.ExtraInfo = rawResponse;
                            //objPublishManager.AddPublishExceptionLog(ObjPublishExceptionLog);

                            //if (!string.IsNullOrEmpty(rawResponse))
                            //{
                            //    ObjFacebookResponse.message = ex.Message;
                            //    ObjFacebookResponse.code = string.Empty;
                            //    ObjFacebookResponse.type = "Exception";
                            //    ObjFacebookResponse.ID = string.Empty;
                            //}

                            #endregion
                        }
                    }
                    else
                    {
                        ObjFacebookResponse.ID = string.Empty;
                        ObjFacebookResponse.message = "Exception";
                        ObjFacebookResponse.type = "The poll you tried to create has too few or too many options. Please try again.";
                        ObjFacebookResponse.code = "1525008";
                    }

                }

            }
            catch (Exception ex)
            {
                #region ERROR HANDLING

                //PublishExceptionLog ObjPublishExceptionLog = new PublishExceptionLog();
                //ObjPublishExceptionLog.DealerLocationSocialNetworkPostID = 0;
                //ObjPublishExceptionLog.DealerLocationSocialNetworkPostQueueID = 0;
                //ObjPublishExceptionLog.ExceptionMessage = ex.Message;
                //ObjPublishExceptionLog.InnerException = ex.InnerException == null ? "" : ex.InnerException.Message;
                //ObjPublishExceptionLog.PostResponse = string.Empty;
                //ObjPublishExceptionLog.StackTrace = ex.StackTrace;
                //ObjPublishExceptionLog.MethodName = "FaceBook\\CreateFacebookQuestion";
                //ObjPublishExceptionLog.ExtraInfo = string.Empty;
                //objPublishManager.AddPublishExceptionLog(ObjPublishExceptionLog);

                //ObjFacebookResponse.ID = string.Empty;
                //ObjFacebookResponse.message = ex.Message;
                //ObjFacebookResponse.type = "Exception\\CreateFacebookQuestion";
                //ObjFacebookResponse.code = string.Empty;

                #endregion
            }

            return ObjFacebookResponse;
        }

        public static string ConverttoFacebookStyleOption(string options,out int OptionsCount)
        {
            string result = string.Empty;
            OptionsCount = 0;            
            try
            {
                string[] words = options.Split('|');
                OptionsCount = words.Count();
                string options_Temp = string.Empty;
                foreach (var word in words)
                {
                    string word_temp = System.Web.HttpUtility.UrlEncode(word);
                    options_Temp = options_Temp + "'" + word_temp + "',";
                }
                result = "[" + options_Temp.Substring(0, options_Temp.Length - 1) + "]";                               
            }
            catch (Exception ex)
            {
            }

            return result;
        }

        private string FirstCharToUpper(string input)
        {
            if (String.IsNullOrEmpty(input))
                throw new ArgumentException("ARGH!");
            return input.First().ToString().ToUpper() + String.Join("", input.Skip(1));
        }

        #endregion

        #region OVERRIDE METHODS
        protected override void dispose()
        {

        }
        #endregion
    }
}
