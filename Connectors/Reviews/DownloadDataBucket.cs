﻿using HtmlAgilityPack;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Connectors.Reviews
{
    public class DownloadDataBucket
    {
        public DownloadDataBucket()
        {
            Country = "US";
        }

        public HtmlDocument HtmlDocument { get; set; }
        public string Country { get; set; }
    }
}
