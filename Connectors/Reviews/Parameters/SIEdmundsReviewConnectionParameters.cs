﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Connectors.Parameters
{
	public class SIEdmundsReviewConnectionParameters : SIReviewConnectionParameters
	{
        public override string ProcessorSettingsPrefix()
        {
            return "Edmunds";
        }

		#region Properties
		
		public SIEdmundsReviewXPathParameters XPathParameters;

		#endregion

        public SIEdmundsReviewConnectionParameters(SIEdmundsReviewXPathParameters siEdmundsReviewXPathParameters, int timeOutSeconds = 30, bool useProxy = true, int maxRedirects = 50)
            : base(timeOutSeconds, useProxy, maxRedirects, siEdmundsReviewXPathParameters)
		{
			
			XPathParameters = siEdmundsReviewXPathParameters;

		}


        
    }
}
