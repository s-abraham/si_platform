﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Connectors.Parameters
{
	public class SIYahooLocalReviewConnectionParameters : SIReviewConnectionParameters
	{
        public override string ProcessorSettingsPrefix()
        {
            return "Yahoo";
        }

		#region Properties
		
		public SIYahooLocalReviewXPathParameters XPathParameters;

		#endregion

		public SIYahooLocalReviewConnectionParameters(SIYahooLocalReviewXPathParameters siYahooLocalReviewXPathParameters, int timeOutSeconds = 30, bool useProxy = true, int maxRedirects = 50)
            : base(timeOutSeconds, useProxy, maxRedirects, siYahooLocalReviewXPathParameters)
		{			
			XPathParameters = siYahooLocalReviewXPathParameters;

		}

	}
}
