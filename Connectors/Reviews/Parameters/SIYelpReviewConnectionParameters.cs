﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Connectors.Parameters
{
	public class SIYelpReviewConnectionParameters : SIReviewConnectionParameters
	{
        public override string ProcessorSettingsPrefix()
        {
            return "Yelp";
        }

		#region Properties
		
		public SIYelpReviewXPathParameters XPathParameters;

		#endregion

		public SIYelpReviewConnectionParameters(SIYelpReviewXPathParameters siYelpReviewXPathParameters, int timeOutSeconds = 30, bool useProxy = true, int maxRedirects = 50)
            : base(timeOutSeconds, useProxy, maxRedirects, siYelpReviewXPathParameters)
		{
			XPathParameters = siYelpReviewXPathParameters;
		}
	}
}
