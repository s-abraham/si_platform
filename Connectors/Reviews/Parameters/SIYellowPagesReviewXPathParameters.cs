﻿using SI.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Connectors.Parameters
{
    public class SIYellowPagesReviewXPathParameters : SIBaseReviewXPathParameters
	{
        public string ReviewerName { get; set; }
        public string ReviewDate { get; set; }
        public string ReviewTextFull { get; set; }
        public string RatingCollected { get; set; }

        public string ExtraInfoKeys { get; set; }
        public string ExtraInfoValues { get; set; }
        public string FrenchReviewXPath { get; set; }

        public string ReviewCommentXPath { get; set; }
        public string ReviewCommentNameXPath { get; set; }
        public string ReviewCommentTextXPath { get; set; }

        public string PagingNextXPath { get; set; }

        public string ListingId { get; set; }
        public string ReviewCommentDate { get; set; }

        public string CA_ReviewerName { get; set; }
        public string CA_ReviewDate { get; set; }
        public string CA_ReviewTextFull { get; set; }
        public string CA_RatingCollected { get; set; }

        public string CA_RatingValuePath { get; set; }
        public string CA_ReviewCountPath { get; set; }
        public string CA_ReviewDetailIteratorPath { get; set; }
        public string CA_NoReviewTextPath { get; set; }
        public string CA_NoRatingPath { get; set; }

        public string CA_ExtraInfoKeys { get; set; }
        public string CA_ExtraInfoValues { get; set; }
        public string CA_FrenchReviewXPath { get; set; }

        public string CA_ReviewCommentXPath { get; set; }
        public string CA_ReviewCommentNameXPath { get; set; }
        public string CA_ReviewCommentTextXPath { get; set; }

        public string CA_PagingNextXPath { get; set; }

        public string CA_ListingId { get; set; }
        public string CA_ReviewCommentDate { get; set; }

        
	}
}
