﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Connectors.Parameters
{
	public abstract class SIReviewConnectionParameters : SIConnectionParameters
	{	

        public abstract string ProcessorSettingsPrefix();

        public SIBaseReviewXPathParameters BaseXPaths { get; set; }

		protected SIReviewConnectionParameters(int timeOutSeconds, bool useProxy, int maxRedirects, SIBaseReviewXPathParameters xPaths) : base(timeOutSeconds, useProxy, maxRedirects)
		{
            BaseXPaths = xPaths;
		}
	}
}
