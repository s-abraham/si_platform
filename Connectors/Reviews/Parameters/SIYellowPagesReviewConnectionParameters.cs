﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Connectors.Parameters
{
	public class SIYellowPagesReviewConnectionParameters : SIReviewConnectionParameters
	{
        public override string ProcessorSettingsPrefix()
        {
            return "YellowPages";
        }

		#region Properties
		
		public SIYellowPagesReviewXPathParameters XPathParameters;

		#endregion

		public SIYellowPagesReviewConnectionParameters(SIYellowPagesReviewXPathParameters siYellowPageReviewXPathParameters,  int timeOutSeconds = 30, bool useProxy = true, int maxRedirects = 50)
            : base(timeOutSeconds, useProxy, maxRedirects, siYellowPageReviewXPathParameters)
		{
			
            //HtmlRequestUrl = htmlRequestUrl;

			XPathParameters = siYellowPageReviewXPathParameters;

		}

	}
}
