﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Connectors.Parameters
{
    public class SIFacebookReviewXPathParemeters : SIBaseReviewXPathParameters
    {
        public string UniqueID { get; set; }
        public string Token { get; set; }

        public string APIURL { get; set; }
    }
}
