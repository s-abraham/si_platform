﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Connectors.Parameters
{
	public class SIJudysBookReviewConnectionParameters : SIReviewConnectionParameters
	{

        public override string ProcessorSettingsPrefix()
        {
            return "JudysBook";
        }

		#region Properties

		public SIJudysBookReviewXPathParameters XPathParameters;

		#endregion

		public SIJudysBookReviewConnectionParameters(SIJudysBookReviewXPathParameters siJudysBookReviewXPathParameters, int timeOutSeconds = 30, bool useProxy = true, int maxRedirects = 50)
            : base(timeOutSeconds, useProxy, maxRedirects, siJudysBookReviewXPathParameters)
		{
			//HtmlRequestUrl = htmlRequestUrl;

			XPathParameters = siJudysBookReviewXPathParameters;

		}
	}
}
