﻿using SI.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Connectors.Parameters
{
    public class SIGoogleReviewXPathParameters : SIBaseReviewXPathParameters
	{        
        public string freq { get; set; }
        public string At { get; set; }
        public string ApiURL { get; set; }
       /*
        public string F_Req { get; set; }
        public string At { get; set; }

		public string ParentnodeReviewSummaryXPath { get; set; } // //*[@id='contentPane']/div/div[2]/div/div/div[3]
		public string RatingValueXPath { get; set; } // .//div[1]/div[1]/div[2]/div[3]/div/div[1]/div/div[1]/div/div/span[1]
		public string ReviewCountCollectedXPath { get; set; } // .//div[1]/div[1]/div[2]/div[3]/div/div[1]/div/div[1]/div/div/span[2]
		public string ListNodeReviewDetailsXPath { get; set; } // .//div[@class='Xna kS']

		public string ReviewDetailRatingValueXPath { get; set; } // div[2]/div[1]/div[1]/span[contains(@class,'c-Rga-DDa c-Rga-DDa-nyb')]
		public string ReviewDetailRatingHalfValueXPath { get; set; } // div[2]/div[1]/div[1]/span[contains(@class,'c-Rga-DDa c-Rga-DDa-oyb')]
		public string ReviewerNameXPath { get; set; } // div[2]/span/a
		public string ReviewDateXPath { get; set; } // div[2]/div[1]/div[2]/span
		public string ReviewTextFullXPath { get; set; } // div[2]/div[2]/span

		public string ReviewCommentParentXPath { get; set; } // div[2]/div[5]/div
		public string ReviewCommentDateXPath { get; set; } // div[2]/div[5]/div/div[1]/span[2]
		public string ReviewCommentTextXPath { get; set; } // div[2]/div[5]/div/div[2]

		public string ReviewAPIPlaceIDXPath { get; set; } // .//div[@class='a-f-e c-b c-b-haDnnc cf3rA L0a X9']
        */
	}
}
