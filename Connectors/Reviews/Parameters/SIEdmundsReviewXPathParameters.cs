﻿using SI.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Connectors.Parameters
{
    public class SIEdmundsReviewXPathParameters : SIBaseReviewXPathParameters
	{
        public string APIURLTemplate { get; set; }

        public string NoServicesReviewText { get; set; }
        public string NoServicesReviewText2 { get; set; }
        public string NoSalesReviewText2 { get; set; }
        public string NoSalesReviewText { get; set; }
        public string NoServicesReviewTextXPath { get; set; }
        public string NoSalesReviewTextXPath { get; set; }
        public string NoServiceRatingPath { get; set; }
        public string NoServiceRatingText { get; set; }

        public string ReviewCountServicePath { get; set; }
        public string RatingValueServicePath { get; set; }

        public string ExtraValueSalesPath { get; set; }
        public string ExtraValueServicesPath { get; set; }

        /*
		public string RatingCollectedSalesXPath { get; set; }
		public string ReviewCountCollectedSalesParentXPath { get; set; }
		public string ReviewCountCollectedSalesXPath { get; set; }

		public string RatingCollectedServiceXPath { get; set; }
		public string ReviewCountCollectedServiceParentXPath { get; set; }
		public string ReviewCountCollectedServiceXPath { get; set; }

		public string SalesExtraInfoXPath { get; set; }
		public string ServiceExtraInfoXPath { get; set; }

		//reviews
		public string ParentnodeReviewDetailsXPath { get; set; }
		public string RatingValueXPath { get; set; }
		public string ReviewerNameXPath { get; set; }
		public string ReviewTextFullURLXPath { get; set; }
		public string ReviewFullTextXPath { get; set; }
		public string ReviewDateXPath { get; set; }
		public int ReviewsPerPage { get; set; }


		public string ReviewCommentsXPath { get; set; }
		public string ReviewCommentDateXPath { get; set; }

		public string ReviewTitleExtraInfoXPath { get; set; }
		public string ReviewRecommendationExtraInfoXPath { get; set; }
		public string ReviewPurchasedVehicleExtraInfoXPath { get; set; }


		public string ParentnodeSeviceReviewDetailsXPath { get; set; }
		public string ServiceReviewTextFullURLXPath { get; set; }
		public string ServiceReviewFullTextXPath { get; set; }
         
         */
	}
}
