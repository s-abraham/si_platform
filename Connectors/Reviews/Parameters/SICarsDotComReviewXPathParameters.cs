﻿using SI.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Connectors.Parameters
{
    public class SICarsDotComReviewXPathParameters : SIBaseReviewXPathParameters
	{
        public string ReviewerNameXPath { get; set; }
        public string ReviewDateXPath { get; set; }
        public string ReviewTextFullXPath { get; set; }
        public string RatingValueXPath { get; set; }

        //public string ExtraInfoKeys { get; set; }
        //public string ExtraInfoValues { get; set; }

        public string ReviewDetailIteratorPath { get; set; }

        public string ParentnodeExtraValueXPath { get; set; }
        
        public string ParentNodeExtraValueSummaryXPath { get; set; }
        public string XtraValueWrapperXPath { get; set; }
        public string XtraValueNodeXPath { get; set; }
        public string XtraKeyNodeXPath { get; set; }
        public string XtraNoWrapperValueNodeXPath { get; set; }
        public string XtraNoWrapperKeyNodeXPath { get; set; }
        public string ExtraValueSummaryRecommendedXPath { get; set; }
        public string ExtraValueSummaryRecommendedText { get; set; }

        public string ParentnodeExtraValueParaXPath { get; set; }

        public string ParentNodeReviewCommentXPath { get; set; }
        public string CommenterNameXPath { get; set; }
        public string CommentDateXPath { get; set; }
        public string CommentXPath { get; set; }        

        //public string ParentnodeReviewSummaryXPath { get; set; }
        //public string RatingCollectedXPath { get; set; }
        //public string ReviewCountCollectedXPath { get; set; }

        //public string ParentnodeReviewDetailsXPath { get; set; }
        //public string RatingValueXPath { get; set; }
        //public string ReviewTextFullXPath { get; set; }

        //public string ReviewerNameXPath { get; set; }
        //public string ReviewDateXPath { get; set; }

        ///// <summary>
        ///// Optional Extra value XPaths
        ///// </summary>
        //public string ParentnodeExtraValueXPath { get; set; }
        //public string XtraValueWrapperXPath { get; set; }
        //public string XtraValueNodeXPath { get; set; }
        //public string XtraKeyNodeXPath { get; set; }
        //public string XtraNoWrapperValueNodeXPath { get; set; }
        //public string XtraNoWrapperKeyNodeXPath { get; set; }
        //public string ParentnodeExtraValueParaXPath { get; set; }
	}
}
