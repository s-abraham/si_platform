﻿using SI.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Connectors.Parameters
{
    public class SIYelpReviewXPathParameters : SIBaseReviewXPathParameters
	{
        //public string ParentNodeReviewDetailsXPath { get; set; }
        //public string ReviewCountParentXPath { get; set; }
        //public string ReviewCountCollectedXPath { get; set; }
        //public string RatingValueXPath { get; set; }
        //public string ListNodeReviewDetailsXPath { get; set; }
        public string ReviewerNameXPath { get; set; }
        public string ReviewDateXPath { get; set; }
        public string ReviewTextFullXPath { get; set; }
        public string FrenchReviewXPath { get; set; }

		/// <summary>
		/// Optional Extra value XPaths
		/// </summary>
		public string NoReviewXPath { get; set; }
		public int ReviewsPerPage { get; set; }
        public string FilteredReviewsCountXPath { get; set; }

	}
}
