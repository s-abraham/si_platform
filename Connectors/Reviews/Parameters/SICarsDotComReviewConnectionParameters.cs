﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Connectors.Parameters
{
	public class SICarsDotComReviewConnectionParameters : SIReviewConnectionParameters
	{
        public override string ProcessorSettingsPrefix()
        {
            return "CarsDotCom";
        }

		#region Properties
		
		public SICarsDotComReviewXPathParameters XPathParameters;

		#endregion

		public SICarsDotComReviewConnectionParameters(SICarsDotComReviewXPathParameters siCarsDotComReviewXPathParameters, int timeOutSeconds = 30, bool useProxy = true, int maxRedirects = 50)
            : base(timeOutSeconds, useProxy, maxRedirects, siCarsDotComReviewXPathParameters)
		{
			//HtmlRequestUrl = htmlRequestUrl;
			XPathParameters = siCarsDotComReviewXPathParameters;
		}

	}
}
