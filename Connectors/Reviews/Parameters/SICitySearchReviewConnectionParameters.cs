﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Connectors.Parameters
{
	public class SICitySearchReviewConnectionParameters : SIReviewConnectionParameters
	{
        public override string ProcessorSettingsPrefix()
        {
            return "CitySearch";
        }

		#region Properties
		
		public SICitySearchReviewXPathParameters XPathParameters;

		#endregion

		public SICitySearchReviewConnectionParameters(SICitySearchReviewXPathParameters siCitySearchReviewXPathParameters, int timeOutSeconds = 30, bool useProxy = true, int maxRedirects = 50)
            : base(timeOutSeconds, useProxy, maxRedirects, siCitySearchReviewXPathParameters)
		{
			//HtmlRequestUrl = htmlRequestUrl;

			XPathParameters = siCitySearchReviewXPathParameters;

		}
	}
}
