﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Connectors.Parameters
{
	public class SIGoogleReviewConnectionParameters : SIReviewConnectionParameters
	{
        public override string ProcessorSettingsPrefix()
        {
            return "Google";
        }

		public SIGoogleReviewXPathParameters XPathParameters;

        public SIGoogleReviewConnectionParameters(SIGoogleReviewXPathParameters siGoogleReviewXPathParameters, int timeOutSeconds = 30, bool useProxy = true, int maxRedirects = 50)
            : base(timeOutSeconds, useProxy, maxRedirects, siGoogleReviewXPathParameters)
        {
            XPathParameters = siGoogleReviewXPathParameters;
        }
	}
}
