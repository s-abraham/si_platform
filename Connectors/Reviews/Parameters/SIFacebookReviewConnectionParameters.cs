﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Connectors.Parameters;

namespace Connectors.Parameters
{
    public class SIFacebookReviewConnectionParameters : SIReviewConnectionParameters
    {
        public override string ProcessorSettingsPrefix()
        {
            return "Facebook";
        }

        #region Properties

        public SIFacebookReviewXPathParemeters XPathParameters;

        #endregion

        //public string ApiRequestUrl { get; set; }
        public string HtmlRequestUrl { get; set; }

        
        public SIFacebookReviewConnectionParameters(SIFacebookReviewXPathParemeters siFacebookReviewXPathParameters, int timeOutSeconds = 30, bool useProxy = true, int maxRedirects = 50)
            : base(timeOutSeconds, useProxy, maxRedirects, siFacebookReviewXPathParameters)
        {
            
            XPathParameters = siFacebookReviewXPathParameters;

        }

        //public SIFacebookReviewConnectionParameters(string apiRequestUrl, SIFacebookReviewXPathParemeters siFacebookReviewXPathParameters, string htmlRequestUrl, int timeOutSeconds = 30, bool useProxy = true, int maxRedirects = 50)
        //    : base(timeOutSeconds, useProxy, maxRedirects, siFacebookReviewXPathParameters)
        //{
        //    ApiRequestUrl = apiRequestUrl;
        //    HtmlRequestUrl = htmlRequestUrl;

        //    XPathParameters = siFacebookReviewXPathParameters;

        //}
    }

}
