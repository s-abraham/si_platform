﻿using SI.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Connectors.Parameters
{
    public class SICitySearchReviewXPathParameters : SIBaseReviewXPathParameters
	{
        public string ReviewRatingPath { get; set; }
        public string ReviewerNameXPath { get; set; }
        public string ReviewDateXPath { get; set; }
        public string ReviewTextFullXPath { get; set; }
	}
}
