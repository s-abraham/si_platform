﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Connectors.Parameters
{
	public class SIDealerRaterReviewConnectionParameters : SIReviewConnectionParameters
	{
        public override string ProcessorSettingsPrefix()
        {
            return "DealerRater";
        }

		#region Properties
	
		public SIDealerRaterReviewXPathParameters Parameters;

		#endregion

		public SIDealerRaterReviewConnectionParameters(SIDealerRaterReviewXPathParameters siDealerRaterReviewXPathParameters, int timeOutSeconds = 30, bool useProxy = true, int maxRedirects = 50)
            : base(timeOutSeconds, useProxy, maxRedirects, siDealerRaterReviewXPathParameters)
		{

            Parameters = siDealerRaterReviewXPathParameters;

		}

	}
}
