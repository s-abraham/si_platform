﻿using SI.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Connectors.Parameters
{
    public class SIJudysBookReviewXPathParameters : SIBaseReviewXPathParameters
	{
        public string ReviewURLXPath { get; set; }
        public string FullReviewURLXPath { get; set; }

        public string ReviewRatingValuePath { get; set; }
        public string ReviewerNameXPath { get; set; }
        public string ReviewDateXPath { get; set; }
        public string ReviewTextFullXPath { get; set; }
        public string ReviewTextFullParentXPath { get; set; }        
	}
}
