﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Connectors.Parameters;

namespace Connectors.Reviews.Parameters
{
    public class SIInsiderPagesReviewConnectionParameters : SIReviewConnectionParameters
    {
         public override string ProcessorSettingsPrefix()
        {
            return "InsiderPages";
        }

         public SIInsiderPagesReviewXPathParameters XPathParameters;

        public SIInsiderPagesReviewConnectionParameters(SIInsiderPagesReviewXPathParameters siInsiderPagesReviewXPathParameters, int timeOutSeconds = 30, bool useProxy = true, int maxRedirects = 50)
            : base(timeOutSeconds, useProxy, maxRedirects, siInsiderPagesReviewXPathParameters)
        {
            XPathParameters = siInsiderPagesReviewXPathParameters;
        }
    }
}
