﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Connectors.Parameters
{
	public class SICarDealerReviewsReviewConnectionParameters : SIReviewConnectionParameters
	{
        public override string ProcessorSettingsPrefix()
        {
            return "CarDealerReviews";
        }

		#region Properties

		public SICarDealerReviewsReviewXPathParameters XPathParameters;

		#endregion

		public SICarDealerReviewsReviewConnectionParameters(SICarDealerReviewsReviewXPathParameters siCarDealerReviewsReviewXPathParameters, int timeOutSeconds = 30, bool useProxy = true, int maxRedirects = 50)
            : base(timeOutSeconds, useProxy, maxRedirects, siCarDealerReviewsReviewXPathParameters)
		{
			XPathParameters = siCarDealerReviewsReviewXPathParameters;

		}

	}
}
