﻿using SI.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Connectors.Parameters
{
    public class SIDealerRaterReviewXPathParameters : SIBaseReviewXPathParameters
    {
            
        public string APIURLTemplate { get; set; }
                
        public string RatingValueXPath2 { get; set; }
        public string ReviewerNameXPath { get; set; }
        public string ReviewDateXPath { get; set; }
        public string ReviewTextFullXPath { get; set; }

        public string CommentIteratorXPath { get; set; }
        public string CommenterNameXPath { get; set; }
        public string CommentDateAddedXPath { get; set; }
        public string CommentTextFullXPath { get; set; }

        public string ExtraInfoKeys { get; set; }
        public string ExtraInfoXPaths { get; set; }

        public string PagingNextXPath { get; set; }

    //    public string RatingCollectedXPath { get; set; }

    //    /// <summary>
    //    /// Optional Extra value XPaths (not in app.config now)
    //    /// </summary>
    //    public int ReviewsPerPage { get; set; }
        public string ReasonForVisitXPath { get; set; }
    //    public string ServiceRatingXPath { get; set; }

        public string ExtraInfoKeysDetailXPath { get; set; }
        public string ExtraInfoValuesDetailXPath { get; set; }
        public string ReviewRecommendationExtraInfoXPath { get; set; }
    }
}
