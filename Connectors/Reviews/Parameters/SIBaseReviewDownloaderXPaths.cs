﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Connectors.Parameters
{
    public abstract class SIBaseReviewXPathParameters
    {
        public string AddressPath { get; set; }
        public string CityPath { get; set; }
        public string LatitudePath { get; set; }
        public string LongitudePath { get; set; }
        public string LocationNamePath { get; set; }
        public string ManufacturerPath { get; set; }
        public string NoRatingPath { get; set; }
        public string NoRatingText { get; set; }
        public string NoReviewPath { get; set; }
        public string NoReviewText { get; set; }
        public string PhonePath { get; set; }
        public string RatingValuePath { get; set; }
        public string ReviewCountPath { get; set; }
        public string ReviewDetailIteratorPath { get; set; }
        public string StatePath { get; set; }
        public string WebsiteURLPath { get; set; }
        public string ZipPath { get; set; }

        public string RatingValueXPath { get; set; }
    }
}
