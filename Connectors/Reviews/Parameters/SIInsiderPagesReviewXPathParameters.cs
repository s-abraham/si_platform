﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Connectors.Parameters;

namespace Connectors.Reviews.Parameters
{
    public class SIInsiderPagesReviewXPathParameters : SIBaseReviewXPathParameters
    {
        public string ReviewRatingPath { get; set; }
        public string ReviewerNameXPath { get; set; }
        public string ReviewDateXPath { get; set; }        
        public string ReviewTextFullXPath { get; set; }

        public string NextPathURLPath { get; set; }
    }
}
