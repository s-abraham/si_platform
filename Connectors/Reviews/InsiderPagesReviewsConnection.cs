﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Connectors.Parameters;
using Connectors.Reviews;
using Connectors.Reviews.Parameters;
using HtmlAgilityPack;
using Newtonsoft.Json.Linq;
using SI.DTO;
using SI.Extensions;
using SI;


namespace Connectors.Reviews
{
    public class InsiderPagesReviewsConnection : SIReviewConnection
    {
        private SIInsiderPagesReviewConnectionParameters ConnectionParameters;

        #region Initializer
        public InsiderPagesReviewsConnection(SIInsiderPagesReviewConnectionParameters siInsiderPagesReviewConnectionParameters)
            : base(siInsiderPagesReviewConnectionParameters)
        {
            ConnectionParameters = siInsiderPagesReviewConnectionParameters;
        }
        #endregion

        #region Protected Methods

        protected override ReviewUrlDTO TweakUrl(ReviewUrlDTO urlDTO)
        {
            return urlDTO;
        }

        protected override bool CheckHasCheckNoReviews(DownloadDataBucket bucket, List<string> failureReasons)
        {
            HtmlDocument doc = bucket.HtmlDocument;

            bool result = false;

            HtmlNode node = SelectSingleNode(doc.DocumentNode, BaseParameters.BaseXPaths.NoReviewPath, true, null);
            if (node != null)
            {
                if (!string.IsNullOrWhiteSpace(BaseParameters.BaseXPaths.NoReviewText))
                {
                    if (string.IsNullOrWhiteSpace(node.InnerText))
                    {
                        result = false;
                    }
                    else
                    {
                        if (node.InnerText.Trim().ToLower().Equals(BaseParameters.BaseXPaths.NoReviewText.Trim().ToLower()))
                        {
                            result = true;
                        }
                    }
                }
                else
                {
                    result = true;
                }
            }
            else
                result = true;

            return result;

        }

        protected override int ScrapeReviewCount(DownloadDataBucket bucket, List<string> failureReasons)
        {
            HtmlDocument doc = bucket.HtmlDocument;

            int reviewCount = 0;

            HtmlNode node = SelectSingleNode(doc.DocumentNode, BaseParameters.BaseXPaths.ReviewCountPath, true, null);
            if (node != null)
            {
                if (!string.IsNullOrWhiteSpace(node.InnerText))
                {
                    //int start = node.InnerText.Trim().IndexOf("of") + 2;
                    //int end = node.InnerText.Trim().IndexOf(".");                                       
                    //int.TryParse(node.InnerText.Trim().Substring(start, end - start).Trim(), out reviewCount);
                    string rtext = node.InnerText.Trim();
                    int.TryParse(rtext, out reviewCount);
                }
            }

            return reviewCount;
        }

        protected override bool CheckHasNoServiceReviews(DownloadDataBucket bucket, List<string> failureReasons)
        {
            return true;
        }

        protected override int ScrapeServiceReviewCount(DownloadDataBucket bucket, List<string> failureReasons)
        {
            return 0;
        }

        protected override bool CheckHasNoRating(DownloadDataBucket bucket, List<string> failureReasons)
        {
            HtmlDocument doc = bucket.HtmlDocument;

            bool result = false;

            HtmlNode node = SelectSingleNode(doc.DocumentNode, BaseParameters.BaseXPaths.NoRatingPath, true, null);
            if (node != null)
            {
                if (!string.IsNullOrWhiteSpace(BaseParameters.BaseXPaths.NoRatingText))
                {
                    if (string.IsNullOrWhiteSpace(node.InnerText))
                    {
                        result = false;
                    }
                    else
                    {
                        if (node.InnerText.Trim().ToLower().Contains(BaseParameters.BaseXPaths.NoRatingText.Trim().ToLower()))
                        {
                            result = true;
                        }
                    }
                }
            }

            return result;
        }

        protected override decimal ScrapeRating(DownloadDataBucket bucket, List<string> failureReasons)
        {
            HtmlDocument doc = bucket.HtmlDocument;

            decimal rating = 0;

            HtmlNode node = SelectSingleNode(doc.DocumentNode, BaseParameters.BaseXPaths.RatingValuePath, true, null);
            if (node != null)
            {
                if (!string.IsNullOrWhiteSpace(BaseParameters.BaseXPaths.RatingValuePath))
                {
                    if (!string.IsNullOrWhiteSpace(node.InnerText))
                    {
                        //Rating: 4.2/5 (22 votes cast)
                        //string rtext = node.GetAttributeValue("title","").Trim();
                        //decimal.TryParse(rtext, out rating);
                        string rtext = node.InnerText.Trim();
                        decimal.TryParse(rtext, out rating);
                    }
                }
            }

            return rating;
        }

        protected override bool CheckHasNoServiceRating(DownloadDataBucket bucket, List<string> failureReasons)
        {
            return false;
        }

        protected override decimal ScrapeServiceRating(DownloadDataBucket bucket, List<string> failureReasons)
        {
            return 0;
        }

        protected override List<KeyValuePair<string, string>> ScrapeExtraInfo(DownloadDataBucket bucket, List<string> failureReasons)
        {
            HtmlDocument doc = bucket.HtmlDocument;

            List<KeyValuePair<string, string>> ListkeyvaluePair = new List<KeyValuePair<string, string>>();

            return ListkeyvaluePair;
        }

        protected override List<Connectors.ReviewDownloadResult.ReviewDetail> ScrapeDetails(DownloadDataBucket bucket, ReviewUrlDTO originalUrl, List<string> failureReasons)
        {
            HtmlDocument currentDoc = bucket.HtmlDocument;

            List<Connectors.ReviewDownloadResult.ReviewDetail> results = new List<ReviewDownloadResult.ReviewDetail>();
            List<string> failedXPaths = new List<string>();

            scrapeDetailsLocal(currentDoc, results, failedXPaths);

            int pagesScraped = 1;
            int numberOfAttemptsToMakeForEachPage = 2;

            HtmlNode nextLink = SelectSingleNode(currentDoc.DocumentNode, ConnectionParameters.XPathParameters.NextPathURLPath);
            while (nextLink != null)
            {
                string nextURL = "http://www.insiderpages.com" + nextLink.GetAttributeValue("href", "");
                currentDoc = DownloadDocument(nextURL, null, numberOfAttemptsToMakeForEachPage);

                if (currentDoc == null)
                {
                    failureReasons.Add(string.Format("Failed to download page {0} at url {1}, tried {2} time(s)",
                        pagesScraped + 1, nextURL, numberOfAttemptsToMakeForEachPage));
                }

                scrapeDetailsLocal(currentDoc, results, failedXPaths);

                nextLink = SelectSingleNode(currentDoc.DocumentNode, ConnectionParameters.XPathParameters.NextPathURLPath);

                pagesScraped++;
                if (pagesScraped > 200) break;
            }

            return results;
        }

        protected override void dispose()
        {
            // nothing to do here
        }

        #endregion

        #region private Methods

        private void scrapeDetailsLocal(HtmlDocument headerDoc, List<Connectors.ReviewDownloadResult.ReviewDetail> results, List<string> failedXPaths)
        {
            HtmlNodeCollection details = SelectNodes(headerDoc.DocumentNode, BaseParameters.BaseXPaths.ReviewDetailIteratorPath, true, failedXPaths);

            if (details != null && details.Count() > 0)
            {
                foreach (HtmlNode node in details)
                {
                    try
                    {
                        Connectors.ReviewDownloadResult.ReviewDetail detail = scrapeSingleReviewDetail(node, failedXPaths);
                        if (detail != null)
                        {
                            results.Add(detail);
                        }
                    }
                    catch (Exception ex)
                    {

                    }
                }
            }
        }

        private Connectors.ReviewDownloadResult.ReviewDetail scrapeSingleReviewDetail(HtmlNode node, List<string> failedXPaths)
        {
            Connectors.ReviewDownloadResult.ReviewDetail review = new Connectors.ReviewDownloadResult.ReviewDetail();

            #region Rating
            review.Rating = 0;
            try
            {
                HtmlNode node2 = SelectSingleNode(node, ConnectionParameters.XPathParameters.ReviewRatingPath);
                if (node2 != null)
                {

                    if (!string.IsNullOrWhiteSpace(node2.InnerText))
                    {
                        string rtext = node2.InnerText.Trim();
                        decimal dValue;
                        if (decimal.TryParse(rtext, out dValue))
                            review.Rating = dValue;   
                    }

                }
                else
                {
                    review.Rating = 0;
                }


            }
            catch (Exception ex)
            {
            }
            #endregion
                        
            #region ReviewerName
            review.ReviewerName = string.Empty;
            try
            {
                HtmlNode node2 = SelectSingleNode(node, ConnectionParameters.XPathParameters.ReviewerNameXPath);
                if (node2 != null)
                {
                    if (!string.IsNullOrWhiteSpace(node2.InnerText))
                    {
                        string rtext = node2.InnerText.Trim();
                        review.ReviewerName = rtext;
                    }
                }
                else
                {
                    failedXPaths.Add(ConnectionParameters.XPathParameters.ReviewerNameXPath);
                }
            }
            catch (Exception ex)
            {
            }
            #endregion

            #region ReviewDate
            review.ReviewDate = SIDateTime.MinValue;
            try
            {
                HtmlNode node2 = SelectSingleNode(node, ConnectionParameters.XPathParameters.ReviewDateXPath);
                if (node2 != null)
                {
                    if (!string.IsNullOrWhiteSpace(node2.InnerText))
                    {
                        string rtext = node2.InnerText.Trim();
                        DateTime rDate;
                        if (DateTime.TryParse(rtext, out rDate))
                        {
                            review.ReviewDate = new SI.SIDateTime(rDate,0);
                        }
                    }
                }
                else
                {
                    failedXPaths.Add(ConnectionParameters.XPathParameters.ReviewDateXPath);
                }
            }
            catch (Exception ex)
            {

            }
            #endregion

            #region ReviewTextFull
            review.ReviewTextFull = string.Empty;
            try
            {
                HtmlNode node2 = SelectSingleNode(node, ConnectionParameters.XPathParameters.ReviewTextFullXPath);
                if (node2 != null)
                {
                    if (!string.IsNullOrWhiteSpace(node2.InnerText))
                    {
                        string rtext = node2.InnerText.Trim();
                        review.ReviewTextFull = rtext;
                    }
                }
                else
                {
                    failedXPaths.Add(ConnectionParameters.XPathParameters.ReviewTextFullXPath);
                }
            }
            catch (Exception ex)
            {

            }
            #endregion
            
            return review;


        }
        #endregion
    }
}
