﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using Connectors.Parameters;
using HtmlAgilityPack;
using SI;
using Connectors.Entities;
using SI.DTO;
using Connectors.Reviews;

namespace Connectors
{
    public abstract class SIReviewConnection : SIConnection
    {

        protected SIReviewConnection(SIReviewConnectionParameters siReviewConnectionParameters)
            : base(siReviewConnectionParameters)
        {
            BaseParameters = siReviewConnectionParameters;
        }

        protected SIReviewConnectionParameters BaseParameters { get; set; }

        /// <summary>
        /// Performs a scrape of the review summary and optionally the
        /// detail for a given review url set.  The connection object's
        /// implementation details determine which url(s) to use and 
        /// what methods are employed.
        /// </summary>
        /// <param name="dto">Set of URLs and external IDs to use for this scrape.</param>
        /// <param name="includeDetails">If true, will download the review details in addition to the summary data.</param>
        /// <returns>Returns an object that describes the outcome of the scrape attempt.</returns>
        public ReviewDownloadResult Scrape(ReviewUrlDTO dto, bool includeDetails)
        {

            dto= TweakUrl(dto);
            
            ReviewDownloadResult result = new ReviewDownloadResult() { };
            result.Status = ReviewDownloadResult.StatusEnum.Success;

            try
            {
                
                DownloadDataBucket bucket = DoDownload(dto, result.FailureReasons);

                if (dto.HtmlURL.Contains("yellowpages.ca"))
                {
                    bucket.Country = "CA";
                }

                if (bucket == null)
                {
                    result.FailureReasons.Add(string.Format("Could not download {0} Possible bad URL", dto.HtmlURL));
                    result.Status = ReviewDownloadResult.StatusEnum.Exception;

                    if (!string.IsNullOrWhiteSpace(ExceptionMessage))
                    {
                        result.FailureReasons.Add(string.Format("Exception downloading {0} = {1}", dto.HtmlURL, ExceptionMessage));

                        if (ExceptionMessage.ToLower().Contains("timeout"))
                            result.Status = ReviewDownloadResult.StatusEnum.Timeout;
                        else
                            result.Status = ReviewDownloadResult.StatusEnum.Exception;
                    }
                }
                else
                {
                    //HeaderHasNoReviewCount = true means that there is no data
                    result.HeaderHasNoReviewCount = CheckHasCheckNoReviews(bucket, result.FailureReasons);
                    result.SalesReviewCount = ScrapeReviewCount(bucket, result.FailureReasons);

                    if (!result.HeaderHasNoReviewCount && result.SalesReviewCount == 0)
                    {
                        result.Status = ReviewDownloadResult.StatusEnum.InvalidFormat;
                        result.FailureReasons.Add("Review count is zero and \"No Review\" element not found.");
                    }

                    result.HeaderHasNoServiceReviewCount = CheckHasNoServiceReviews(bucket, result.FailureReasons);
                    result.ServiceReviewCount = ScrapeServiceReviewCount(bucket, result.FailureReasons);
                                        
                    result.HeaderHasNoRating = CheckHasNoRating(bucket, result.FailureReasons);
                    result.SalesAverageRating = ScrapeRating(bucket, result.FailureReasons);
                    //TODO: Add HeaderHasNoServiceRating

                    result.ServiceAverageRating = ScrapeServiceRating(bucket, result.FailureReasons);

                    if (!result.HeaderHasNoReviewCount && result.SalesReviewCount == 0)
                    {
                        result.Status = ReviewDownloadResult.StatusEnum.InvalidFormat;
                        result.FailureReasons.Add("Rating is zero and \"No Rating\" element not found.");
                    }

                    //todo: add service

                    //if (!result.HeaderHasNoReviewCount || result.SalesReviewCount > 0)
                    //{
                        result.ExtraInfo = ScrapeExtraInfo(bucket, result.FailureReasons);
                    //}

                    if (includeDetails)
                    {
                        try
                        {
                            result.ReviewDetails = ScrapeDetails(bucket, dto, result.FailureReasons);
                        }
                        catch (Exception detEx)
                        {
                            result.Exception = detEx;
                            if (detEx.ToString().ToLower().Contains("timeout"))
                                result.Status = ReviewDownloadResult.StatusEnum.Timeout;
                            else
                                result.Status = ReviewDownloadResult.StatusEnum.Exception;
                        }
                    }

                    if (result.FailureReasons.Count > 0 && result.Status == ReviewDownloadResult.StatusEnum.Success)
                    {
                        result.Status = ReviewDownloadResult.StatusEnum.InvalidFormat;
                    }

                }
            }
            catch (Exception ex)
            {
                result.Exception = ex;
                result.Status = ReviewDownloadResult.StatusEnum.Exception;
            }
            
            return result;
        }

        /// <summary>
        /// The implementation class is given the opportunity to mangle the URLs and IDs
        /// used by the scraper prior to use.
        /// </summary>
        /// <param name="url">A set of URLs and IDs</param>
        /// <returns>The tweaked set of URLs and IDs</returns>
        protected abstract ReviewUrlDTO TweakUrl(ReviewUrlDTO url);

        /// <summary>
        /// Performs initial data download
        /// </summary>
        /// <returns>DownloadDataBucket object, or null if failed</returns>
        protected virtual DownloadDataBucket DoDownload(ReviewUrlDTO url, List<string> failureReasons)
        {
            var bucket = new DownloadDataBucket();

            if (!string.IsNullOrEmpty(url.HtmlURL.Trim()))
            {
                HtmlDocument doc = ConnectorHtmlDocument(url.HtmlURL, null);
                // if html fails, always fail with a null return. no html header, no go
                if (doc == null) return null;
                bucket = new DownloadDataBucket() { HtmlDocument = doc };
            }
            
            return bucket;
        }
       
        /// <summary>
        /// Checks to see if there exists an element that indicates that there are no reviews
        /// for this account.
        /// </summary>
        /// <param name="doc">The HTML document to search</param>
        /// <param name="failureReasons">A list of reasons that the operation failed</param>
        /// <returns>True if the "No Reviews" area is matched, otherwise false</returns>
        protected abstract bool CheckHasCheckNoReviews(DownloadDataBucket doc, List<string> failureReasons);

        /// <summary>
        /// Finds and parses the header review count from the supplied HTML document
        /// </summary>
        /// <param name="doc">The HTML Document to search</param>
        /// <param name="failureReasons">A list of reasons that the operation failed</param>
        /// <returns>The parsed review count, or zero if not found</returns>
        protected abstract int ScrapeReviewCount(DownloadDataBucket doc, List<string> failureReasons);

        protected abstract bool CheckHasNoServiceReviews(DownloadDataBucket doc, List<string> failureReasons);
        protected abstract int ScrapeServiceReviewCount(DownloadDataBucket doc, List<string> failureReasons);

        /// <summary>
        /// Checks to see if there exists an element that indicates that there is no rating
        /// for this account.
        /// </summary>
        /// <param name="doc">The HTML Document to search</param>
        /// <param name="failureReasons">A list of reasons that the operation failed</param>
        /// <returns>True if the "No Rating" area iis matched, otherwise false</returns>
        protected abstract bool CheckHasNoRating(DownloadDataBucket doc, List<string> failureReasons);

        /// <summary>
        /// Finds and parses the header rating value from the supplied HTML document.
        /// </summary>
        /// <param name="doc">The HTML document to search</param>
        /// <param name="failureReasons">A list of reasons that the operation failed</param>
        /// <returns>The parsed rating, or zero if not found</returns>
        protected abstract decimal ScrapeRating(DownloadDataBucket doc, List<string> failureReasons);

        protected abstract bool CheckHasNoServiceRating(DownloadDataBucket doc, List<string> failureReasons);
        protected abstract decimal ScrapeServiceRating(DownloadDataBucket doc, List<string> failureReasons);

        /// <summary>
        /// If available, searches for and parses out additional header level information
        /// to be stored as "Extra Info"
        /// </summary>
        /// <param name="doc">The HTML document to search</param>
        /// <param name="failureReasons">A list of reasons that the operation failed</param>
        /// <returns>A list of key/value pairs representing the additional information found</returns>
        protected abstract List<KeyValuePair<string, string>> ScrapeExtraInfo(DownloadDataBucket doc, List<string> failureReasons);

        /// <summary>
        /// Given a header doc and set of URLs and ExternalIDs, downloads the review details for a given account.
        /// </summary>
        /// <param name="headerDoc">The HTML document from which the header information was parsed.</param>
        /// <param name="originalUrl">The set of URLs initially passed to the Scrape method</param>
        /// <param name="failureReasons">A list of reasons that the operation failed</param>
        /// <returns>A list of zero or more review detail objects.</returns>
        protected abstract List<Connectors.ReviewDownloadResult.ReviewDetail> ScrapeDetails(DownloadDataBucket doc, ReviewUrlDTO originalUrl, List<string> failureReasons);


        protected static HtmlNode SelectSingleNode(HtmlNode node, string xPath, bool allowNull = false, List<string> failedXPaths = null)
        {
            if (node != null && !string.IsNullOrEmpty(xPath))
            {
                HtmlNode selectedNode = node.SelectSingleNode(xPath);
                if (!allowNull && selectedNode == null && failedXPaths != null)
                    failedXPaths.Add(xPath);
                return selectedNode;
            }
            return null;
        }

        protected static HtmlNodeCollection SelectNodes(HtmlNode node, string xPath, bool allowNull = false, List<string> failedXPaths = null)
        {
            if (node != null && !string.IsNullOrEmpty(xPath))
            {
                HtmlNodeCollection nodeCollection = node.SelectNodes(xPath);
                if (!allowNull && nodeCollection == null && failedXPaths != null)
                    failedXPaths.Add(xPath);
                return nodeCollection;
            }
            return null;
        }
        
    }
}
