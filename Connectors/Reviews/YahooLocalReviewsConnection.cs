﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Connectors.Parameters;
using Newtonsoft.Json.Linq;
using HtmlAgilityPack;
using SI.Extensions;
using Connectors.Reviews;
using SI.DTO;
using System.Web.Script.Serialization;
using Connectors.Entities;

namespace Connectors
{
    public class YahooLocalReviewsConnection : SIReviewConnection
    {
        private SIYahooLocalReviewConnectionParameters ConnectionParameters;
        private int _reviewCount = 0;

        #region Initializer
        public YahooLocalReviewsConnection(SIYahooLocalReviewConnectionParameters siYahooLocalReviewConnectionParameters)
            : base(siYahooLocalReviewConnectionParameters)
        {
            ConnectionParameters = siYahooLocalReviewConnectionParameters;
        }
        #endregion

        #region Protected Methods

        protected override DownloadDataBucket DoDownload(ReviewUrlDTO url, List<string> failureReasons)
        {
            if (string.IsNullOrEmpty(url.HtmlURL.Trim()))
            {
                return new DownloadDataBucket();
            }

            HtmlDocument doc = DownloadDocument(url.HtmlURL, null, 3, 2000);

            // if html fails, always fail with a null return. no html header, no go
            if (doc == null) return null;

            YahooLocalBucket bucket = new YahooLocalBucket() { HtmlDocument = doc };

            var isValidPage = CheckIsValidPage(bucket, failureReasons);

            if (!isValidPage)
            {
                //Page is not valid
                bucket = null;
            }
            else
            {
                //Page is valid
#if DEBUG
                if (string.IsNullOrWhiteSpace(url.ExternalID))
                {
                    url.ExternalID = ExtractSubstring(url.HtmlURL, "info-", "-");
                }
#endif

                if (!string.IsNullOrWhiteSpace(url.ExternalID))
                {
                    int rstart = 0;
                    string apiURL = string.Format(ConnectionParameters.XPathParameters.APIURLTemplate, url.ExternalID, rstart);
                    //_url = url;

                    string apiString = DownloadString(apiURL, null);

                    if (!string.IsNullOrWhiteSpace(apiString))
                    {
                        if (!apiString.ToLower().Contains("{}"))
                        {
                            try
                            {
                                var jss = new JavaScriptSerializer();
                                jss.RegisterConverters(new JavaScriptConverter[] { new DynamicJsonConverter() });
                                bucket.ApiJsonData = jss.Deserialize(apiString, typeof(object)) as dynamic;
                                //bucket.ApiJsonData = Newtonsoft.Json.JsonConvert.DeserializeObject<dynamic>(apiString);
                            }
                            catch (Exception)
                            {
                                // for now do nothing if we cannot parse the api json...
                                bucket.ApiJsonData = null;
                            }
                        }
                    }
                }
            }




            return bucket;
        }

        protected bool CheckIsValidPage(DownloadDataBucket bucket, List<string> failureReasons)
        {
            var foundNotFoundString = bucket.HtmlDocument.DocumentNode.InnerText.Contains("the page you are looking for was not found.");
            if (foundNotFoundString)
            {
                failureReasons.Add("ReviewSource says page is invalid");
                return false;
            }
            return true;
        }

        protected override SI.DTO.ReviewUrlDTO TweakUrl(SI.DTO.ReviewUrlDTO url)
        {
            return url;
        }

        protected override bool CheckHasNoServiceReviews(DownloadDataBucket bucket, List<string> failureReasons)
        {
            return false;
        }

        protected override bool CheckHasCheckNoReviews(DownloadDataBucket bucket, List<string> failureReasons)
        {
            HtmlDocument doc = bucket.HtmlDocument;

            bool result = false;

            int reviewCount = 0;

            YahooSummary objYahooSummary = GetYahooSummary(bucket);

            if (objYahooSummary != null)
            {
                //if (objYahooSummary.review_source.ToLower().Contains("yahoo"))
                {
                    reviewCount = objYahooSummary.nreview;
                }
            }

            if (reviewCount > 0)
            {
                result = true;
            }
            else if (reviewCount == 0)
            {
                result = true;
            }
            else
            {
                result = false;
            }

            //HtmlNode node = SelectSingleNode(doc.DocumentNode, BaseParameters.BaseXPaths.NoReviewPath, true, null);
            //if (node != null)
            //{
            //    if (!string.IsNullOrWhiteSpace(BaseParameters.BaseXPaths.NoReviewText))
            //    {
            //        if (string.IsNullOrWhiteSpace(node.InnerText))
            //        {
            //            result = false;
            //        }
            //        else
            //        {
            //            if (node.InnerText.Trim().ToLower().Contains(BaseParameters.BaseXPaths.NoReviewText.Trim().ToLower()))
            //            {
            //                result = true;
            //            }
            //        }
            //    }
            //    else
            //    {
            //        result = true;
            //    }
            //}

            return result;
        }

        protected override bool CheckHasNoServiceRating(DownloadDataBucket bucket, List<string> failureReasons)
        {
            return false;
        }

        protected override bool CheckHasNoRating(DownloadDataBucket bucket, List<string> failureReasons)
        {
            HtmlDocument doc = bucket.HtmlDocument;

            bool result = false;
            decimal rating = 0;

            YahooSummary objYahooSummary = GetYahooSummary(bucket);

            if (objYahooSummary != null)
            {
                //if (objYahooSummary.review_source.ToLower().Contains("yahoo"))
                {
                    rating = objYahooSummary.rating;
                }
            }

            if (rating > 0)
            {
                result = true;
            }
            else
            {
                result = false;
            }
            //HtmlNode node = SelectSingleNode(doc.DocumentNode, BaseParameters.BaseXPaths.NoRatingPath, true, null);
            //if (node != null)
            //{
            //    if (!string.IsNullOrWhiteSpace(BaseParameters.BaseXPaths.NoRatingText))
            //    {
            //        if (string.IsNullOrWhiteSpace(node.InnerText))
            //        {
            //            result = false;
            //        }
            //        else
            //        {
            //            result = true;
            //        }
            //    }
            //}

            return result;
        }

        protected override int ScrapeServiceReviewCount(DownloadDataBucket bucket, List<string> failureReasons)
        {
            return 0;
        }

        protected override int ScrapeReviewCount(DownloadDataBucket bucket, List<string> failureReasons)
        {
            HtmlDocument doc = bucket.HtmlDocument;

            int reviewCount = 0;

            YahooSummary objYahooSummary =  GetYahooSummary(bucket);

            if (objYahooSummary != null)
            {
                //if (objYahooSummary.review_source.ToLower().Contains("yahoo"))
                {
                    reviewCount = objYahooSummary.nreview;
                }
            }

            //HtmlNode node = SelectSingleNode(doc.DocumentNode, BaseParameters.BaseXPaths.ReviewCountPath, true, null);
            //HtmlNode node = SelectSingleNode(doc.DocumentNode, ".//span[@id='yl_biz_rnr']", true, null);
            //HtmlNode node = SelectSingleNode(doc.DocumentNode, ".//form[@name='hdn_value']", true, null);

            //var inputNodes = doc.DocumentNode.Descendants("input");
            //var node1 = inputNodes.Where(n => n.Id == "HDN_card");
            //HtmlNode node = (HtmlNode)node1.FirstOrDefault();
                       
            //if (node != null)
            //{
            //    //HtmlNode node1 = SelectSingleNode(node, ".//input[@id='HDN_card']", true, null);
            //    //var nodeValue = node.Attributes["Value"].Value;
            //    string value = node.GetAttributeValue("value", "");

            //    if (!string.IsNullOrWhiteSpace(value.Trim()))
            //    {
            //        var jss = new JavaScriptSerializer();
            //        YahooSummary objYahooSummary = new YahooSummary();

            //        value = "{" + value + "}";
            //        objYahooSummary = jss.Deserialize<YahooSummary>(value);

            //        if (objYahooSummary != null)
            //        {
            //            if (objYahooSummary.review_source.ToLower().Contains("yahoo"))
            //            {
            //                reviewCount = objYahooSummary.nreview;
            //            }
            //        }
            //    }

                //if (!string.IsNullOrWhiteSpace(node.InnerText))
                //{
                //    int.TryParse(node.InnerText.Replace("reviews on Yahoo", "").Replace("review on Yahoo", ""), out reviewCount);
                //}
            //}
            _reviewCount = reviewCount;
            return reviewCount;
        }

        protected override decimal ScrapeServiceRating(DownloadDataBucket bucket, List<string> failureReasons)
        {
            return 0;
        }

        protected override decimal ScrapeRating(DownloadDataBucket bucket, List<string> failureReasons)
        {
            HtmlDocument doc = bucket.HtmlDocument;

            decimal rating = 0;

            YahooSummary objYahooSummary =  GetYahooSummary(bucket);

            if (objYahooSummary != null)
            {
                //if (objYahooSummary.review_source.ToLower().Contains("yahoo"))
                {
                    rating = objYahooSummary.rating;
                }
            }

            //HtmlNode node = SelectSingleNode(doc.DocumentNode, BaseParameters.BaseXPaths.RatingValuePath, true, null);
            //if (node != null)
            //{
            //    if (!string.IsNullOrWhiteSpace(node.InnerText))
            //    {

            //        string ratingclass = node.GetAttributeValue("class", "");

            //        if (ratingclass.Length > 0)
            //        {
            //            string[] words = ratingclass.Split(' ');

            //            foreach (string word in words)
            //            {
            //                if (word.StartsWith("star"))
            //                {
            //                    string rtext = Convert.ToString((Convert.ToDecimal(word.Replace("star", "")) / Convert.ToDecimal(2)));
            //                    decimal.TryParse(rtext, out rating);
            //                    break;
            //                }
            //            }
            //        }
            //        else
            //        {
            //            rating = 0;
            //        }
            //    }

            //}

            return rating;
        }

        protected override List<KeyValuePair<string, string>> ScrapeExtraInfo(DownloadDataBucket bucket, List<string> failureReasons)
        {
            List<KeyValuePair<string, string>> ListkeyvaluePair = new List<KeyValuePair<string, string>>();

            YahooSummary objYahooSummary = GetYahooSummary(bucket);

            if (objYahooSummary != null)
            {
                string value = string.Empty;
                string key = string.Empty;

                key = "ReviewSource";
                value = objYahooSummary.review_source;

                ListkeyvaluePair.Add(new KeyValuePair<string, string>(key, value));
            }

            return ListkeyvaluePair;
        }

        protected override List<ReviewDownloadResult.ReviewDetail> ScrapeDetails(DownloadDataBucket bucket, SI.DTO.ReviewUrlDTO originalUrl, List<string> failureReasons)
        {
            List<ReviewDownloadResult.ReviewDetail> details = new List<ReviewDownloadResult.ReviewDetail>();
            int numberOfAttemptsToMakeForEachPage = 2;

            YahooSummary objYahooSummary = GetYahooSummary(bucket);
            if (objYahooSummary.review_source.ToLower().Contains("yahoo"))
            {
                YahooLocalBucket myBucket = (YahooLocalBucket)bucket;
                if (myBucket.ApiJsonData != null)
                {
                    details = scrapeDetails(myBucket.ApiJsonData);

                    if (_reviewCount > 100)
                    {
                        int reviewcountleft = _reviewCount - details.Count;
                        int pagecounter = ConnectionParameters.XPathParameters.ReviewsPerPage;

                        while (reviewcountleft > 0)
                        {
                            string apiURL = string.Format(ConnectionParameters.XPathParameters.APIURLTemplate, originalUrl.ExternalID, pagecounter);
                            string apiString = DownloadString(apiURL, null);

                            if (!string.IsNullOrEmpty(apiString))
                            {
                                var jss = new JavaScriptSerializer();
                                jss.RegisterConverters(new JavaScriptConverter[] { new DynamicJsonConverter() });
                                dynamic ApiPageJsonData = jss.Deserialize(apiString, typeof(object)) as dynamic;

                                if (ApiPageJsonData != null)
                                {
                                    List<ReviewDownloadResult.ReviewDetail> tempdetails = scrapeDetails(ApiPageJsonData);
                                    if (tempdetails.Count == 0)
                                        break;

                                    foreach (var item in tempdetails)
                                    {
                                        details.Add(item);
                                    }
                                }
                            }
                            else
                            {
                                failureReasons.Add(string.Format("Failed to download page {0} at url {1}, tried {2} time(s)",
                                                        pagecounter, apiURL, numberOfAttemptsToMakeForEachPage));
                            }
                            reviewcountleft = _reviewCount - details.Count;
                            pagecounter = pagecounter + ConnectionParameters.XPathParameters.ReviewsPerPage;
                        }

                    }
                }
            }
            else if (objYahooSummary.review_source.ToLower().Contains("yelp"))
            {
                //Find Yelp URL from Yahoo HTML and Download Review from Yelp
            }
            return details;
        }

        protected override void dispose()
        {
            // nothing to do here
        }

        #endregion

        #region private Methods/Class

        private class YahooLocalBucket : DownloadDataBucket
        {
            public YahooLocalBucket()
            {
                ApiJsonData = null;
            }
            public dynamic ApiJsonData { get; set; }
        }

        private class YahooSummary
        {
            public YahooSummary()
            {
                id = string.Empty;
                nreview = 0;
                rating = 0;
                review_source = string.Empty;
            }

            public string id { get; set; }
            public int nreview { get; set; }
            public decimal rating { get; set; }
            public string review_source { get; set; }
        }

        private YahooSummary GetYahooSummary(DownloadDataBucket bucket)
        {
            YahooSummary objYahooSummary = new YahooSummary();

            HtmlDocument doc = bucket.HtmlDocument;            

            var inputNodes = doc.DocumentNode.Descendants("input");
            var node1 = inputNodes.Where(n => n.Id == BaseParameters.BaseXPaths.ReviewCountPath); //"HDN_card"
            HtmlNode node = (HtmlNode)node1.FirstOrDefault();

            if (node != null)
            {
                string value = node.GetAttributeValue("value", "");
                if (!string.IsNullOrWhiteSpace(value.Trim()))
                {
                    var jss = new JavaScriptSerializer();

                    value = "{" + value + "}";
                    objYahooSummary = jss.Deserialize<YahooSummary>(value);
                }
            }

            return objYahooSummary;
        }

        private static string ExtractSubstring(string searchString, string startSequence, string endSequence)
        {
            var result = string.Empty;
            var start = 0;
            var end = 0;

            try
            {
                if (!string.IsNullOrEmpty(searchString))
                {
                    start = searchString.IndexOf(startSequence) + startSequence.Length;

                    if (searchString.IndexOf(endSequence, start) < 0)
                    {
                        end = searchString.Length;
                    }
                    else
                    {
                        end = endSequence == null ? searchString.Length : searchString.IndexOf(endSequence, start);
                    }


                    if (start >= 0 && end > 0)
                    {
                        var length = end - start;
                        result = searchString.Substring(start, length);
                    }
                }
            }
            catch (Exception ex)
            {
            }

            return result;


        }

        private List<ReviewDownloadResult.ReviewDetail> scrapeDetails(dynamic json)
        {
            List<ReviewDownloadResult.ReviewDetail> details = new List<ReviewDownloadResult.ReviewDetail>();
            int reviewCount = 0;
            int totalObjectsReturned = 0;

            if (json != null)
            {
                var yahooObjects = new List<object>();

                foreach (var item in json)
                {
                    yahooObjects.Add(item);
                }
                totalObjectsReturned = yahooObjects.Count;

                foreach (dynamic item in yahooObjects)
                {
                    reviewCount++;
                    try
                    {
                        if (totalObjectsReturned > 3)
                        {
                            if (reviewCount < yahooObjects.Count - 2)
                            {
                                ReviewDownloadResult.ReviewDetail review = new ReviewDownloadResult.ReviewDetail();

                                review.Rating = Convert.ToInt32(item.rating);
                                review.ReviewTextFull = item.posting;
                                review.ReviewerName = item.reviewer_alias;
                                DateTime rDate;
                                if (DateTime.TryParse(item.mod_time, out rDate))
                                {
                                    review.ReviewDate = new SI.SIDateTime(rDate, TimeZoneInfo.Utc);
                                }

                                if (item.reply_list.Count > 0)
                                {
                                    List<ReviewDownloadResult.ReviewComment> comments = new List<ReviewDownloadResult.ReviewComment>();
                                    foreach (System.Collections.Generic.Dictionary<string, object> Reviewcomment in item.reply_list)
                                    {
                                        try
                                        {
                                            ReviewDownloadResult.ReviewComment comment = new ReviewDownloadResult.ReviewComment();

                                            comment.Comment = Reviewcomment["CONTENTS"].ToString();
                                            comment.CommentDate = new SI.SIDateTime(DateTime.Parse(Reviewcomment["diff_time"].ToString()), 0);
                                            comment.CommenterName = Reviewcomment["COMMENTATOR_ALIAS"].ToString();

                                            comments.Add(comment);
                                        }
                                        catch (Exception ex)
                                        {
                                        }
                                    }
                                    review.Comments = comments;
                                }
                                details.Add(review);
                            }
                        }
                    }
                    catch (Exception ex)
                    {

                    }
                }

            }

            return details;
        }

        #endregion

        /*
		/// <summary>
		/// Call the Review API and extract the rating and review count
		/// </summary>
		/// <param name="request">API request URL</param>
		/// <returns>Populates the object Ratings and Review count</returns>
		public ConnectionStatus GetReviewsAPI(bool includeDetails = true)
		{
			ConnectionStatus returnStatus = ConnectionStatus.InvalidData;

			if (!string.IsNullOrEmpty(ConnectionParameters.ApiRequestUrl))
			{
				HttpRequestResults = string.Empty;

				ConnectionStatus httpStatus = ConnectorHttpRequest(ConnectionParameters.ApiRequestUrl);
				if (httpStatus == ConnectionStatus.Success)
				{
					if (!string.IsNullOrEmpty(HttpRequestResults))
					{
						JArray jAReviews = JArray.Parse(HttpRequestResults);
						if (jAReviews != null)
						{
							SalesReviewCount = 0;
							ServiceReviewCount = 0;
							SalesAverageRating = 0;
							ServiceAverageRating = 0;

							returnStatus = ConnectionStatus.Success;

							if (jAReviews.Count > 3)
							{
								SalesReviewCount = jAReviews.Last.Previous["REVIEW_COUNT"].ToString().ToInteger();

								int numberOfPages = SalesReviewCount / ConnectionParameters.XPathParameters.ReviewsPerPage;
								if ((SalesReviewCount % ConnectionParameters.XPathParameters.ReviewsPerPage) > 0)
									numberOfPages++;

								int numberOfReviewsPerPage = ConnectionParameters.XPathParameters.ReviewsPerPage;

								if (numberOfPages == 0)
								numberOfPages = 1;

								string noPageApiUrl = ConnectionParameters.ApiRequestUrl.Substring(0, ConnectionParameters.ApiRequestUrl.IndexOf("&rstart="));

								for (int pageCount = 0; pageCount < numberOfPages; pageCount++)
								{
									if (pageCount > 0)
									{
										string pageRequestUrl = string.Format("{0}&rstart={1}&rsort=2", noPageApiUrl, numberOfReviewsPerPage);
										if (ConnectorHttpRequest(pageRequestUrl) != ConnectionStatus.Success) break;
										if (string.IsNullOrEmpty(HttpRequestResults)) break;
										jAReviews = JArray.Parse(HttpRequestResults);

										numberOfReviewsPerPage += ConnectionParameters.XPathParameters.ReviewsPerPage;
									}

									if (jAReviews != null && jAReviews.Count > 3)
									{
										for (int revIndx = 0; revIndx < jAReviews.Count - 3; revIndx++)
										{
											JToken jReview = jAReviews[revIndx];

											Review review = new Review();
											review.Type = "sales";
											review.ReviewTextFull = jReview["posting"].ToString();
											review.Rating = jReview["rating"].ToString().ToDecimal();
											review.ReviewDate = jReview["mod_time"].ToString().ToDateTime();
											review.ReviewerName = jReview["reviewer_alias"].ToString();

											SalesAverageRating += review.Rating;

											if (jReview["reply_list"] != null)
											{
												foreach (JToken jComment in jReview["reply_list"].Children())
												{
													ReviewComment reviewComment = new ReviewComment();
													reviewComment.Comment = jComment["CONTENTS"].ToString();
													reviewComment.CommentDate = jComment["diff_time"].ToString().ToDateTime();

													review.Comments.Add(reviewComment);
												}
											}

											ReviewDetails.Add(review);
										}
									}
								}

								SalesAverageRating = SalesAverageRating / SalesReviewCount;

							}
						}
					}
				}
				else
					returnStatus = httpStatus;
			}

			return returnStatus;
		}
        
		public ConnectionStatus GetReviewsHtml(bool includeDetails = true)
		{
			ConnectionStatus returnStatus = ConnectionStatus.InvalidData;

			//TODO
			return returnStatus;
		}
        
        */

    }
}
