﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Connectors.Parameters;
using Connectors.Reviews;
using HtmlAgilityPack;
using SI.DTO;
using SI.Extensions;

namespace Connectors
{
    public class CarDealerReviewsConnection : SIReviewConnection
    {
        private SICarDealerReviewsReviewConnectionParameters ConnectionParameters;

        #region Initializer
        public CarDealerReviewsConnection(SICarDealerReviewsReviewConnectionParameters siCarDealerReviewsReviewConnectionParameters)
            : base(siCarDealerReviewsReviewConnectionParameters)
        {
            ConnectionParameters = siCarDealerReviewsReviewConnectionParameters;
        }
        #endregion

        #region Protected Methods

        protected override ReviewUrlDTO TweakUrl(ReviewUrlDTO urlDTO)
        {
            return urlDTO;
        }

        protected override bool CheckHasCheckNoReviews(DownloadDataBucket bucket, List<string> failureReasons)
        {
            HtmlDocument doc = bucket.HtmlDocument;

            bool result = false;

            HtmlNode node = SelectSingleNode(doc.DocumentNode, BaseParameters.BaseXPaths.NoReviewPath, true, null);
            if (node != null)
            {
                if (!string.IsNullOrWhiteSpace(BaseParameters.BaseXPaths.NoReviewText))
                {
                    if (string.IsNullOrWhiteSpace(node.InnerText))
                    {
                        result = false;
                    }
                    else
                    {
                        if (node.InnerText.Trim().ToLower().Contains(BaseParameters.BaseXPaths.NoReviewText.Trim().ToLower()))
                        {
                            result = true;
                        }
                    }
                }
                else
                {
                    result = true;
                }
            }

            return result;

        }

        protected override int ScrapeReviewCount(DownloadDataBucket bucket, List<string> failureReasons)
        {
            HtmlDocument doc = bucket.HtmlDocument;

            int reviewCount = 0;

            HtmlNode node = SelectSingleNode(doc.DocumentNode, BaseParameters.BaseXPaths.ReviewCountPath, true, null);
            if (node != null)
            {
                if (!string.IsNullOrWhiteSpace(node.InnerText))
                {
                    int.TryParse(node.InnerText, out reviewCount);
                }
            }

            return reviewCount;
        }

        protected override bool CheckHasNoServiceReviews(DownloadDataBucket bucket, List<string> failureReasons)
        {
            return true;
        }

        protected override int ScrapeServiceReviewCount(DownloadDataBucket bucket, List<string> failureReasons)
        {
            return 0;
        }

        protected override bool CheckHasNoRating(DownloadDataBucket bucket, List<string> failureReasons)
        {
            HtmlDocument doc = bucket.HtmlDocument;

            bool result = false;

            HtmlNode node = SelectSingleNode(doc.DocumentNode, BaseParameters.BaseXPaths.NoRatingPath, true, null);
            if (node != null)
            {
                if (!string.IsNullOrWhiteSpace(BaseParameters.BaseXPaths.NoRatingText))
                {
                    if (string.IsNullOrWhiteSpace(node.InnerText))
                    {
                        result = false;
                    }
                    else
                    {
                        if (node.InnerText.Trim().ToLower().Contains(BaseParameters.BaseXPaths.NoRatingText.Trim().ToLower()))
                        {
                            result = true;
                        }
                    }
                }
            }
            else
            {
                result = true;
            }
            return result;
        }

        protected override decimal ScrapeRating(DownloadDataBucket bucket, List<string> failureReasons)
        {
            HtmlDocument doc = bucket.HtmlDocument;

            decimal rating = 0;

            HtmlNode node = SelectSingleNode(doc.DocumentNode, BaseParameters.BaseXPaths.RatingValuePath, true, null);
            if (node != null)
            {
                if (!string.IsNullOrWhiteSpace(BaseParameters.BaseXPaths.RatingValuePath))
                {
                    if (!string.IsNullOrWhiteSpace(node.InnerText))
                    {
                        //Rating: 4.2/5 (22 votes cast)
                        int startindex = node.InnerText.IndexOf(":") + 1;
                        int endindex = node.InnerText.IndexOf("/");
                        string rtext = node.InnerText.Substring(startindex, endindex - startindex).Trim();
                        decimal.TryParse(rtext, out rating);
                    }
                }
            }

            return rating;
        }

        protected override bool CheckHasNoServiceRating(DownloadDataBucket bucket, List<string> failureReasons)
        {
            return false;
        }

        protected override decimal ScrapeServiceRating(DownloadDataBucket bucket, List<string> failureReasons)
        {
            return 0;
        }

        protected override List<KeyValuePair<string, string>> ScrapeExtraInfo(DownloadDataBucket bucket, List<string> failureReasons)
        {
            HtmlDocument doc = bucket.HtmlDocument;

            List<KeyValuePair<string, string>> ListkeyvaluePair = new List<KeyValuePair<string, string>>();

            return ListkeyvaluePair;
        }

        protected override List<Connectors.ReviewDownloadResult.ReviewDetail> ScrapeDetails(DownloadDataBucket bucket, ReviewUrlDTO originalUrl, List<string> failureReasons)
        {
            HtmlDocument headerDoc = bucket.HtmlDocument;

            List<Connectors.ReviewDownloadResult.ReviewDetail> results = new List<ReviewDownloadResult.ReviewDetail>();
            List<string> failedXPaths = new List<string>();

            return results;
        }

        protected override void dispose()
        {
            // nothing to do here
        }

        #endregion

        /*
		public ConnectionStatus GetReviewsHtml(bool includeDetails = true)
		{
			ConnectionStatus returnStatus = ConnectionStatus.InvalidData;

			if (!string.IsNullOrEmpty(ConnectionParameters.HtmlRequestUrl))
			{
				HttpRequestResults = string.Empty;

				ConnectionStatus httpStatus = ConnectorHttpRequest(ConnectionParameters.HtmlRequestUrl);
				if (httpStatus == ConnectionStatus.Success)
				{
					HtmlDocument htmlDocument = new HtmlDocument();
					htmlDocument.OptionFixNestedTags = true;
					htmlDocument.LoadHtml(HttpRequestResults);

					SalesReviewCount = 0;
					SalesAverageRating = 0;

					returnStatus = ConnectionStatus.Success;

					HtmlNode ReviewParentNode = SelectSingleNode(htmlDocument.DocumentNode, ConnectionParameters.XPathParameters.ParentnodeReviewSummaryXPath);
					if (ReviewParentNode != null)
					{
						HtmlNode SalesRatingNode = SelectSingleNode(ReviewParentNode, ConnectionParameters.XPathParameters.RatingCollectedXPath);
						if (SalesRatingNode != null)
							SalesAverageRating = SalesRatingNode.InnerText.IsNullOptional("0").Trim().ToDecimal();

						HtmlNode SalesCountChildNode = SelectSingleNode(ReviewParentNode, ConnectionParameters.XPathParameters.ReviewCountCollectedXPath);
						if (SalesCountChildNode != null)
							SalesReviewCount = SalesCountChildNode.InnerText.IsNullOptional("0").Trim().ToInteger();
					}
				}

			}

			return returnStatus;
		}

		protected override void dispose()
		{
			
		}
        */

    }

    }