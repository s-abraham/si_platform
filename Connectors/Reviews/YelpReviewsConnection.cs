﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Connectors.Parameters;
using HtmlAgilityPack;
using SI;
using SI.Extensions;
using SI.DTO;
using Connectors.Reviews;
using System.Text.RegularExpressions;
using System.Diagnostics;

namespace Connectors
{
    public class YelpReviewsConnection : SIReviewConnection
    {
        private SIYelpReviewConnectionParameters Parameters;

        #region Initializer
        public YelpReviewsConnection(SIYelpReviewConnectionParameters siYelpReviewConnectionParameters)
            : base(siYelpReviewConnectionParameters)
        {
            Parameters = siYelpReviewConnectionParameters;
        }
        #endregion

        protected override DownloadDataBucket DoDownload(ReviewUrlDTO url, List<string> failureReasons)
        {
            var bucket = new DownloadDataBucket();

            if (!string.IsNullOrEmpty(url.HtmlURL.Trim()))
            {
                HtmlDocument doc = DownloadDocument(url.HtmlURL, null, 3, 2000, "Mozilla/5.0 (Windows NT 6.1; WOW64; rv:26.0) Gecko/20100101 Firefox/26.0");
                // if html fails, always fail with a null return. no html header, no go
                if (doc == null) return null;
                bucket = new DownloadDataBucket() { HtmlDocument = doc };
            }

            return bucket;
        }

        private DownloadDataBucket DoDownload(string HtmlURL, List<string> failureReasons)
        {
            var bucket = new DownloadDataBucket();

            if (!string.IsNullOrEmpty(HtmlURL.Trim()))
            {
                HtmlDocument doc = DownloadDocument(HtmlURL, null, 3, 2000, "Mozilla/5.0 (Windows NT 6.1; WOW64; rv:26.0) Gecko/20100101 Firefox/26.0");
                // if html fails, always fail with a null return. no html header, no go
                if (doc == null) return null;
                bucket = new DownloadDataBucket() { HtmlDocument = doc };
            }

            return bucket;
        }

        protected override SI.DTO.ReviewUrlDTO TweakUrl(SI.DTO.ReviewUrlDTO url)
        {
            return url;
        }

        protected override bool CheckHasCheckNoReviews(DownloadDataBucket bucket, List<string> failureReasons)
        {
           

            HtmlDocument doc = bucket.HtmlDocument;

            bool result = false;

            if (!string.IsNullOrWhiteSpace(BaseParameters.BaseXPaths.NoReviewPath))
            {
                HtmlNode node = SelectSingleNode(doc.DocumentNode, BaseParameters.BaseXPaths.NoReviewPath, true, null);
                if (node != null)
                {
                    if (!string.IsNullOrWhiteSpace(BaseParameters.BaseXPaths.NoReviewText))
                    {
                        if (string.IsNullOrWhiteSpace(node.InnerText))
                        {
                            result = false;
                        }
                        else
                        {
                            if (node.InnerText.Trim().ToLower() == BaseParameters.BaseXPaths.NoReviewText.Trim().ToLower())
                            {
                                result = true;
                            }
                        }
                    }

                }
                else
                {
                    result = true;
                }
            }
            return result;
        }

        protected override int ScrapeReviewCount(DownloadDataBucket bucket, List<string> failureReasons)
        {
            HtmlDocument doc = bucket.HtmlDocument;

            int reviewCount = 0;

            HtmlNode node = SelectSingleNode(doc.DocumentNode, BaseParameters.BaseXPaths.ReviewCountPath, true, null);
            if (node != null)
            {
                if (!string.IsNullOrWhiteSpace(node.InnerText))
                {
                    int.TryParse(node.InnerText, out reviewCount);
                }
            }

            return reviewCount;
        }

        #region no service based metrics for yelp

        protected override bool CheckHasNoServiceReviews(DownloadDataBucket doc, List<string> failureReasons)
        {
            return true;
        }

        protected override int ScrapeServiceReviewCount(DownloadDataBucket doc, List<string> failureReasons)
        {
            return 0;
        }

        protected override bool CheckHasNoServiceRating(DownloadDataBucket doc, List<string> failureReasons)
        {
            return true;
        }

        protected override decimal ScrapeServiceRating(DownloadDataBucket doc, List<string> failureReasons)
        {
            return 0;
        }

        #endregion

        protected override bool CheckHasNoRating(DownloadDataBucket bucket, List<string> failureReasons)
        {
            HtmlDocument doc = bucket.HtmlDocument;

            bool result = false;

            if (!string.IsNullOrWhiteSpace(BaseParameters.BaseXPaths.NoRatingPath))
            {
                HtmlNode node = SelectSingleNode(doc.DocumentNode, BaseParameters.BaseXPaths.NoRatingPath, true, null);
                if (node != null)
                {
                    result = false;
                }
                else
                {
                    result = true;
                }
            }

            return result;
        }

        protected override decimal ScrapeRating(DownloadDataBucket bucket, List<string> failureReasons)
        {
            HtmlDocument doc = bucket.HtmlDocument;

            decimal rating = 0;

            HtmlNode node = SelectSingleNode(doc.DocumentNode, BaseParameters.BaseXPaths.RatingValuePath, true, null);
            if (node != null)
            {
                if (!string.IsNullOrWhiteSpace(BaseParameters.BaseXPaths.RatingValuePath))
                {
                    if (!string.IsNullOrWhiteSpace(node.GetAttributeValue("content", "")))
                    {
                        string rtext = node.GetAttributeValue("content", "").Trim();
                        decimal.TryParse(rtext, out rating);
                    }
                }
            }

            return rating;
        }



        protected override List<KeyValuePair<string, string>> ScrapeExtraInfo(DownloadDataBucket bucket, List<string> failureReasons)
        {
            {
                HtmlDocument doc = bucket.HtmlDocument;

                List<KeyValuePair<string, string>> ListkeyvaluePair = new List<KeyValuePair<string, string>>();
                List<string> failedXPaths = new List<string>();
                try
                {
                    string value = string.Empty;
                    string key = string.Empty;

                    HtmlNode node = SelectSingleNode(doc.DocumentNode, Parameters.XPathParameters.FilteredReviewsCountXPath);
                    if (node != null)
                    {

                        if (!string.IsNullOrWhiteSpace(node.InnerText))
                        {
                            value = node.InnerText.Trim();
                            string convertedVal = Regex.Match(value, @"\d+").Value;
                            key = " Filtered Reviews";
                            ListkeyvaluePair.Add(new KeyValuePair<string, string>(key, convertedVal));
                        }

                        else
                        {
                            failureReasons.Add(Parameters.XPathParameters.FilteredReviewsCountXPath);
                            return null;
                        }

                    }
                }
                catch (Exception ex)
                {
                    failureReasons.Add(BaseParameters.BaseXPaths.RatingValuePath);
                }

                return ListkeyvaluePair;
            }
        }

        protected override List<Connectors.ReviewDownloadResult.ReviewDetail> ScrapeDetails(DownloadDataBucket bucket, ReviewUrlDTO originalUrl, List<string> failureReasons)
        {

            HtmlDocument headerDoc = bucket.HtmlDocument;

            List<Connectors.ReviewDownloadResult.ReviewDetail> results = new List<ReviewDownloadResult.ReviewDetail>();
            List<string> failedXPaths = new List<string>();

            scrapeDetailsLocal(headerDoc, results, failedXPaths);
            //Debug.WriteLine("0 : " + results.Count);

            HtmlDocument currentDoc = headerDoc;

            int numberOfAttemptsToMakeForEachPage = 2;

            int pagesScraped = 1;
            string nextPageXBase = ".//a[contains(@href,'start=') and normalize-space(text())='{0}']";

            string nextPageX = string.Format(nextPageXBase, pagesScraped + 1);
            HtmlNode nextLink = SelectSingleNode(currentDoc.DocumentNode, nextPageX);
            while (nextLink != null)
            {
                string nextURL = string.Format("{0}", nextLink.GetAttributeValue("href", "")).Replace("&amp;", "&");
                //Debug.WriteLine(pagesScraped + " : " + nextURL);
                if (!nextURL.StartsWith("http://www.yelp.com"))
                    nextURL = "http://www.yelp.com" + nextURL;

                //currentDoc = DownloadDocument(nextURL, null, numberOfAttemptsToMakeForEachPage);
                var newbucket = new DownloadDataBucket();
                newbucket = DoDownload(nextURL, null);
                currentDoc = newbucket.HtmlDocument;

                if (currentDoc == null)
                {
                    failureReasons.Add(string.Format("Failed to download page {0} at url {1}, tried {2} time(s), Exception : {3}",
                        pagesScraped + 1, nextURL, numberOfAttemptsToMakeForEachPage, ExceptionMessage));
                    break;
                }

                scrapeDetailsLocal(currentDoc, results, failedXPaths);
                pagesScraped++;
                //Debug.WriteLine(results.Count);                

                nextPageX = string.Format(nextPageXBase, pagesScraped + 1);
                nextLink = SelectSingleNode(currentDoc.DocumentNode, nextPageX);
                if (pagesScraped > 100) break;
            }

            //string FrenchReviewXPath = FrenchReviewXPath;
            HtmlNode FrenchReviewNode = SelectSingleNode(currentDoc.DocumentNode, Parameters.XPathParameters.FrenchReviewXPath);
            if (FrenchReviewNode != null)
            {
                string FrenchReviewLink = FrenchReviewNode.GetAttributeValue("href", "").Trim();

                currentDoc = DownloadDocument(FrenchReviewLink, null, numberOfAttemptsToMakeForEachPage);

                if (currentDoc == null)
                {
                    failureReasons.Add(string.Format("Failed to download FrenchReview page {0} at url {1}, tried {2} time(s), Exception : {3}",
                        pagesScraped + 1, FrenchReviewLink, numberOfAttemptsToMakeForEachPage, ExceptionMessage));
                }
                else
                {
                    scrapeDetailsLocal(currentDoc, results, failedXPaths, LanguageEnum.French);
                }
            }

            foreach (string item in failedXPaths)
            {
                failureReasons.Add(string.Format("Failed XPath: {0}", item));
            }

            return results;
        }

        private void scrapeDetailsLocal(HtmlDocument headerDoc, List<Connectors.ReviewDownloadResult.ReviewDetail> results, List<string> failedXPaths, LanguageEnum language = LanguageEnum.English)
        {
            HtmlNodeCollection details = SelectNodes(headerDoc.DocumentNode, BaseParameters.BaseXPaths.ReviewDetailIteratorPath, true, failedXPaths);

            if (details != null && details.Count() > 0)
            {
                foreach (HtmlNode node in details)
                {
                    Connectors.ReviewDownloadResult.ReviewDetail detail = scrapeSingleReviewDetail(node, failedXPaths);
                    detail.language = Convert.ToInt32(language);
                    if (detail != null)
                    {
                        results.Add(detail);
                    }
                }
            }
        }

        private Connectors.ReviewDownloadResult.ReviewDetail scrapeSingleReviewDetail(HtmlNode node, List<string> failedXPaths)
        {
            Connectors.ReviewDownloadResult.ReviewDetail review = new Connectors.ReviewDownloadResult.ReviewDetail();

            review.Rating = 0;
            HtmlNode node2 = SelectSingleNode(node, BaseParameters.BaseXPaths.RatingValuePath);
            if (node2 != null)
            {
                if (node2.HasAttributes && node2.Attributes.Contains("content") && !string.IsNullOrWhiteSpace(node2.Attributes["content"].Value))
                {
                    string rtext = node2.Attributes["content"].Value;
                    decimal dValue;
                    if (decimal.TryParse(rtext, out dValue))
                        review.Rating = dValue;
                }
            }
            else
            {
                failedXPaths.Add(BaseParameters.BaseXPaths.RatingValuePath);
            }

            node2 = SelectSingleNode(node, Parameters.XPathParameters.ReviewerNameXPath);
            if (node2 != null)
            {
                if (!string.IsNullOrWhiteSpace(node2.InnerText))
                {
                    string rtext = node2.InnerText.Trim();
                    review.ReviewerName = rtext;
                }
            }
            else
            {
                failedXPaths.Add(Parameters.XPathParameters.ReviewerNameXPath);
            }

            node2 = SelectSingleNode(node, Parameters.XPathParameters.ReviewDateXPath);
            if (node2 != null)
            {
                if (!string.IsNullOrWhiteSpace(node2.GetAttributeValue("content", "")))
                {
                    string rtext = node2.GetAttributeValue("content", "").Trim();
                    DateTime rDate;
                    if (DateTime.TryParse(rtext, out rDate))
                    {
                        review.ReviewDate = new SI.SIDateTime(rDate, 0);
                    }
                }
            }
            else
            {
                failedXPaths.Add(Parameters.XPathParameters.ReviewDateXPath);
            }

            node2 = SelectSingleNode(node, Parameters.XPathParameters.ReviewTextFullXPath);
            if (node2 != null)
            {
                if (!string.IsNullOrWhiteSpace(node2.InnerText))
                {
                    string rtext = node2.InnerText.StripHtml().Trim();
                    review.ReviewTextFull = rtext;
                }
            }
            else
            {
                failedXPaths.Add(Parameters.XPathParameters.ReviewTextFullXPath);
            }

            // are there comments?
            //HtmlNodeCollection commentNodes = SelectNodes(node, ".//div[@class='review-comment']", true);
            HtmlNodeCollection commentNodes = SelectNodes(node, ".//div[contains(@class,'island biz-owner-reply')]", true);            

            if (commentNodes != null)
            {
                foreach (HtmlNode commentNode in commentNodes)
                {
                    // get the attribution
                    //HtmlNode attrNode = SelectSingleNode(commentNode, ".//span[@class='attribution']", false, failedXPaths);
                    HtmlNode attrNode = SelectSingleNode(commentNode, ".//div[contains(@class,'media-story')]//strong", false, failedXPaths); 
                    //if (attrNode != null && attrNode.FirstChild != null && !string.IsNullOrWhiteSpace(attrNode.FirstChild.InnerText))
                    if (attrNode != null && attrNode.FirstChild != null && !string.IsNullOrWhiteSpace(attrNode.InnerText))
                    {
                        string commenterName =
                            attrNode.InnerText.Replace("Comment from", "")
                            .Replace("\t", " ")
                            .Replace("\n", " ")
                        ;
                        while (commenterName.Contains("  "))
                            commenterName = commenterName.Replace("  ", " ");

                        commenterName = commenterName.Trim();

                        //HtmlNode dateNode = SelectSingleNode(attrNode, ".//span[@class='date']", false, failedXPaths); 
                        HtmlNode dateNode = SelectSingleNode(commentNode, ".//span[@class='bullet-after']", false, failedXPaths); 
                        if (dateNode != null && !string.IsNullOrWhiteSpace(dateNode.InnerText))
                        {
                            DateTime commentDate;
                            if (DateTime.TryParse(dateNode.InnerText, out commentDate))
                            {
                                var commentText = string.Empty;
                                //Check to see if comment text is stored in hidden field
                                
                                if (commentNode.InnerHtml.Contains("js-content-expander"))
                                {
                                    //Text is hidden
                                    HtmlNode commentTextNode = SelectSingleNode(commentNode, ".//span[contains(@class,'toggleable')][2]", false, failedXPaths);
                                    if (commentTextNode != null && !string.IsNullOrWhiteSpace(commentTextNode.InnerText))
                                    {
                                        commentText = commentTextNode.InnerText.Trim();
                                        review.Comments.Add(new ReviewDownloadResult.ReviewComment()
                                        {
                                            CommentDate = new SI.SIDateTime(commentDate, 0),
                                            CommenterName = commenterName,
                                            Comment = commentText
                                        });
                                    }
                                }
                                else
                                {
                                    //Text doesn't isn't hidden
                                    var rawDescription = commentNode.InnerHtml;
                                    commentText = StringUtils.ExtractSubstring(rawDescription, "</span>", "<div").Replace("\n", "").Trim();

                                    review.Comments.Add(new ReviewDownloadResult.ReviewComment()
                                    {
                                        CommentDate = new SI.SIDateTime(commentDate, 0),
                                        CommenterName = commenterName,
                                        Comment = commentText
                                    });
                                }
                            }
                        }
                    }
                }
            }

            return review;


        }


        protected override void dispose()
        {

        }


        //public ConnectionStatus GetReviewsHtml(bool includeDetails = true)
        //{
        //    ConnectionStatus returnStatus = ConnectionStatus.InvalidData;

        //    if (!string.IsNullOrEmpty(ConnectionParameters.HtmlRequestUrl))
        //    {
        //        HttpRequestResults = string.Empty;

        //        ConnectionStatus httpStatus = ConnectorHttpRequest(ConnectionParameters.HtmlRequestUrl);
        //        if (httpStatus == ConnectionStatus.Success)
        //        {
        //            HtmlDocument htmlDocument = new HtmlDocument();
        //            htmlDocument.OptionFixNestedTags = true;
        //            htmlDocument.LoadHtml(HttpRequestResults);

        //            HtmlNode ReviewParentNode = SelectSingleNode(htmlDocument.DocumentNode, ConnectionParameters.XPathParameters.ReviewCountParentXPath);
        //            if (ReviewParentNode != null)
        //            {
        //                SalesReviewCount = 0;
        //                SalesAverageRating = 0;

        //                HtmlNode SalesCountChildNode = SelectSingleNode(ReviewParentNode, ConnectionParameters.XPathParameters.ReviewCountCollectedXPath);
        //                if (SalesCountChildNode != null)
        //                    SalesReviewCount = SalesCountChildNode.InnerText.IsNullOptional("0").Trim().ToInteger();

        //                HtmlNode SalesRatingNode = SelectSingleNode(ReviewParentNode, ConnectionParameters.XPathParameters.RatingValueXPath);
        //                if (SalesRatingNode != null)
        //                    SalesAverageRating = SalesRatingNode.GetAttributeValue("content", "0").ToDecimal();


        //                if (includeDetails && SalesReviewCount > 0)
        //                {
        //                    int numberOfPages = SalesReviewCount / ConnectionParameters.XPathParameters.ReviewsPerPage;
        //                    if ((SalesReviewCount % ConnectionParameters.XPathParameters.ReviewsPerPage) > 0)
        //                        numberOfPages++;

        //                    int numberOfReviewsPerPage = ConnectionParameters.XPathParameters.ReviewsPerPage;

        //                    if (numberOfPages == 0)
        //                        numberOfPages = 1;

        //                    for (int pageCount = 0; pageCount < numberOfPages; pageCount++)
        //                    {
        //                        if (pageCount > 0)
        //                        {
        //                            string pageRequestUrl = string.Format("{0}?start={1}", ConnectionParameters.HtmlRequestUrl, numberOfReviewsPerPage);
        //                            if (ConnectorHttpRequest(pageRequestUrl) != ConnectionStatus.Success) break;
        //                            htmlDocument.LoadHtml(HttpRequestResults);
        //                            numberOfReviewsPerPage += ConnectionParameters.XPathParameters.ReviewsPerPage;
        //                        }

        //                        HtmlNode ReviewDetailsParentNode = SelectSingleNode(htmlDocument.DocumentNode, ConnectionParameters.XPathParameters.ParentNodeReviewDetailsXPath);
        //                        if (ReviewDetailsParentNode != null)
        //                        {
        //                            HtmlNodeCollection ReviewNodes = SelectNodes(ReviewDetailsParentNode, ConnectionParameters.XPathParameters.ListNodeReviewDetailsXPath);
        //                            if (ReviewNodes != null)
        //                            {
        //                                foreach (HtmlNode reviewNode in ReviewNodes)
        //                                {
        //                                    Review review = new Review();

        //                                    review.Type = "sales";

        //                                    review.Rating = 0;
        //                                    HtmlNode RatingNode = SelectSingleNode(reviewNode, ConnectionParameters.XPathParameters.RatingValueXPath);
        //                                    if (RatingNode != null)
        //                                        review.Rating = RatingNode.GetAttributeValue("content", "0").ToDecimal();

        //                                    HtmlNode ReviewerNameNode = SelectSingleNode(reviewNode, ConnectionParameters.XPathParameters.ReviewerNameXPath);
        //                                    if (ReviewerNameNode != null)
        //                                        review.ReviewerName = ReviewerNameNode.InnerText.IsNullOptional("");

        //                                    HtmlNode ReviewDateNode = SelectSingleNode(reviewNode, ConnectionParameters.XPathParameters.ReviewDateXPath);
        //                                    if (ReviewDateNode != null)
        //                                        review.ReviewDate = ReviewDateNode.InnerText.Trim().Replace("Updated - ", "").ToDateTime();

        //                                    HtmlNode ReviewTextNode = SelectSingleNode(reviewNode, ConnectionParameters.XPathParameters.ReviewTextFullXPath);
        //                                    if (ReviewTextNode != null)
        //                                        review.ReviewTextFull = ReviewTextNode.InnerText.IsNullOptional("");

        //                                    HtmlNode FullReviewURLNode = SelectSingleNode(reviewNode, ConnectionParameters.XPathParameters.FullReviewURLXPath);
        //                                    if (FullReviewURLNode != null)
        //                                        review.FullReviewURL = string.Format("http://www.yelp.com{0}", FullReviewURLNode.GetAttributeValue("href", ""));

        //                                    ReviewDetails.Add(review);
        //                                }
        //                            }
        //                        }
        //                    }
        //                }

        //                returnStatus = ConnectionStatus.Success;
        //            }
        //            else
        //            {
        //                //check if good url just no reviews
        //                if (SelectSingleNode(htmlDocument.DocumentNode, ConnectionParameters.XPathParameters.NoReviewXPath) != null)
        //                    returnStatus = ConnectionStatus.Success;
        //            }
        //        }
        //        else
        //            returnStatus = httpStatus;
        //    }

        //    return returnStatus;
        //}



    }
}
