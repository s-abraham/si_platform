﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Connectors.Entities;
using Connectors.Parameters;
using Connectors.Reviews.Parameters;
using HtmlAgilityPack;
using SI;
using SI.DTO;
using Connectors.Reviews;
using Facebook.Entities;
using Facebook;

namespace Connectors
{
    public class FacebookReviewsConnection : SIReviewConnection
    {
        private SIFacebookReviewConnectionParameters ConnectionParameters;

        #region Initializer
        public FacebookReviewsConnection(SIFacebookReviewConnectionParameters siFacebookReviewConnectionParameters)
            : base(siFacebookReviewConnectionParameters)
        {
            ConnectionParameters = siFacebookReviewConnectionParameters;
        }
        #endregion
        
        #region Protected Methods

        protected override DownloadDataBucket DoDownload(ReviewUrlDTO url, List<string> failureReasons)
        {
            var bucket = new FacebookReviewBucket();
                        
            if (!string.IsNullOrEmpty(url.ApiURL.Trim()))
            {
                //SocialCredentialDTO socialCredentialsDTO = null;
                //socialCredentialsDTO = ProviderFactory.Social.GetSocialCredentialInfo(url.ExternalID);
                //socialCredentialsDTO = new SocialCredentialDTO();
                //socialCredentialsDTO.Token = "CAAGfD25ViIcBAC1sjcdMextc9jh1uQ85ZAc2bQyvqaxaKWgORhZCxH8GI62unPC9MYeZA9cqZBmyHJJUMGzVaxfo3W8bGrbg5JNHTaIPDmR21G899vMT6dNuW9JekZB1bkIBdVZBAvKRwcthsdxcZC9ZAtMXHOHlI6qGKo5y6j1M87EDXVkbRrFp";
                //socialCredentialsDTO.UniqueID = "255547016773";

                //ConnectionParameters.XPathParameters.Token = socialCredentialsDTO.Token;
                //ConnectionParameters.XPathParameters.UniqueID = socialCredentialsDTO.UniqueID;

                ConnectionParameters.XPathParameters.APIURL = url.ApiURL;
                
                FacebookReviewResponse obj = GetFacebookReviewsFromAPI();

                if (obj != null)
                {
                    if (obj.IsSuccess == true)
                    {
                        try
                        {
                            bucket.FacebookReview = obj;
                        }
                        catch (Exception)
                        {
                            // for now do nothing if we cannot parse the api json...
                            bucket.FacebookReview = null;
                        }
                    }
                }
            }

            return bucket;
        }

        protected override ReviewUrlDTO TweakUrl(ReviewUrlDTO urlDTO)
        {
            return urlDTO;
        }

        protected override bool CheckHasCheckNoReviews(DownloadDataBucket bucket, List<string> failureReasons)
        {
            return true;
        }

        protected override int ScrapeReviewCount(DownloadDataBucket bucket, List<string> failureReasons)
        {
            HtmlDocument doc = bucket.HtmlDocument;

            FacebookReviewBucket myBucket = (FacebookReviewBucket)bucket;
            int reviewCount = 0;

            if (myBucket.FacebookReview != null)
            {
                if (myBucket.FacebookReview.IsSuccess == true)
                {
                    reviewCount = myBucket.FacebookReview.ReviewList.data.Count();
                }
            }
            return reviewCount;
        }

        protected override bool CheckHasNoServiceReviews(DownloadDataBucket bucket, List<string> failureReasons)
        {
            return true;
        }

        protected override int ScrapeServiceReviewCount(DownloadDataBucket bucket, List<string> failureReasons)
        {
            return 0;
        }

        protected override bool CheckHasNoRating(DownloadDataBucket bucket, List<string> failureReasons)
        {
            HtmlDocument doc = bucket.HtmlDocument;

            bool result = false;
            FacebookReviewBucket myBucket = (FacebookReviewBucket)bucket;
            if (myBucket.FacebookReview != null)
            {
                if (myBucket.FacebookReview.IsSuccess == true)
                {
                    if(myBucket.FacebookReview.ReviewList.data.Count > 0)
                        result = true;
                }
            }            

            return result;
        }

        protected override decimal ScrapeRating(DownloadDataBucket bucket, List<string> failureReasons)
        {
            HtmlDocument doc = bucket.HtmlDocument;

            decimal rating = 0;

            //TODO: Scrap from Facebook Page
            FacebookReviewBucket myBucket = (FacebookReviewBucket)bucket;
            if (myBucket.FacebookReview != null)
            {
                if (myBucket.FacebookReview.IsSuccess == true)
                {
                    if (myBucket.FacebookReview.ReviewList.data.Count > 0)
                        rating = (decimal)myBucket.FacebookReview.ReviewList.data.Where(r => r.has_rating).Average(r => r.rating);
                }
            }        
        
            return rating;
        }

        protected override bool CheckHasNoServiceRating(DownloadDataBucket bucket, List<string> failureReasons)
        {
            return false;
        }

        protected override decimal ScrapeServiceRating(DownloadDataBucket bucket, List<string> failureReasons)
        {
            return 0;
        }

        protected override List<KeyValuePair<string, string>> ScrapeExtraInfo(DownloadDataBucket bucket, List<string> failureReasons)
        {
            HtmlDocument doc = bucket.HtmlDocument;

            List<KeyValuePair<string, string>> ListkeyvaluePair = new List<KeyValuePair<string, string>>();

            return ListkeyvaluePair;
        }

        protected override List<Connectors.ReviewDownloadResult.ReviewDetail> ScrapeDetails(DownloadDataBucket bucket, ReviewUrlDTO originalUrl, List<string> failureReasons)
        {
            HtmlDocument headerDoc = bucket.HtmlDocument;

            List<Connectors.ReviewDownloadResult.ReviewDetail> results = new List<ReviewDownloadResult.ReviewDetail>();
            List<string> failedXPaths = new List<string>();

            FacebookReviewBucket myBucket = (FacebookReviewBucket)bucket;
            if (myBucket.FacebookReview != null)
            {
                if (myBucket.FacebookReview.IsSuccess == true)
                {
                    if (myBucket.FacebookReview.ReviewList.data.Count > 0)
                    {
                        foreach (var review in myBucket.FacebookReview.ReviewList.data)
                        {
                            try
                            {
                                Connectors.ReviewDownloadResult.ReviewDetail r = GetSingleReview(review);
                                results.Add(r);
                            }
                            catch(Exception ex)
                            {
                                failureReasons.Add("Invalid Facebook Review. " + ex.Message);
                            }
                        }
                    }
                }
            }        

            return results;
        }

        protected override void dispose()
        {
            // nothing to do here
        }

        #endregion

        #region private Methods/Class
        private class FacebookReviewBucket : DownloadDataBucket
        {
            public FacebookReviewBucket()
            {
                FacebookReview = null;
            }
            public FacebookReviewResponse FacebookReview { get; set; }
        }

        private FacebookReviewResponse GetFacebookReviewsFromAPI()
        {
            FacebookReviewResponse obj = new FacebookReviewResponse();
            //FacebookReviewResponse obj = FacebookAPI.GetReviews(ConnectionParameters.XPathParameters.APIURL);

            return obj;
        }

        private Connectors.ReviewDownloadResult.ReviewDetail GetSingleReview(FacebookReview newReview)
        {
            Connectors.ReviewDownloadResult.ReviewDetail review = new ReviewDownloadResult.ReviewDetail();

            review.FullReviewURL = "https://www.facebook.com/" + newReview.open_graph_story.id;
            review.language = (int)LanguageEnum.English;
            review.Rating = newReview.rating;
            review.ReviewDate = new SI.SIDateTime(Convert.ToDateTime(newReview.created_time), 0);
            review.ReviewerName = newReview.reviewer.name;
            review.ReviewTextFull = newReview.review_text;
            review.Type = newReview.open_graph_story.type;

            List<KeyValuePair<string, string>> ListkeyvaluePair = new List<KeyValuePair<string, string>>();
            review.ExtraInfo = new List<KeyValuePair<string, string>>();

            string key = "Review ID";
            string value = newReview.open_graph_story.id;
            ListkeyvaluePair.Add(new KeyValuePair<string, string>(key, value));

            key = "Comments Count";
            value = newReview.open_graph_story.comments.count;
            ListkeyvaluePair.Add(new KeyValuePair<string, string>(key, value));
            
            key = "Likes Count";
            value = newReview.open_graph_story.likes.count;
            ListkeyvaluePair.Add(new KeyValuePair<string, string>(key, value));
            
            key = "Application";
            value = newReview.open_graph_story.application.id + "," + newReview.open_graph_story.application.name;
            ListkeyvaluePair.Add(new KeyValuePair<string, string>(key, value));

            key = "Application";
            value = newReview.open_graph_story.application.id + "," + newReview.open_graph_story.application.name;
            ListkeyvaluePair.Add(new KeyValuePair<string, string>(key, value));
            
            return review;
            

        }
        #endregion

        public ConnectionStatus GetReviewsHtml(bool includeDetails = true)
        {
            ConnectionStatus returnStatus = ConnectionStatus.InvalidData;

            if (!string.IsNullOrEmpty(ConnectionParameters.HtmlRequestUrl))
            {
                HttpRequestResults = string.Empty;

                var cookieContainer = new CookieContainer();
                //cookieContainer.Add(new Uri("https://www.facebook.com"), new Cookie("", ""));
                //cookieContainer.Add(new Uri("https://www.facebook.com"), new Cookie("", ""));
                cookieContainer.Add(new Uri("https://www.facebook.com"), new Cookie("c_user", "100002916188585"));
                cookieContainer.Add(new Uri("https://www.facebook.com"), new Cookie("presence", "EM376595727EuserFA21B02916188585A2EstateFDsb2F0Et2F_5b_5dElm2FnullEuct2F1376585337BEtrFnullEtwF3934437717EatF1376595434241G376595727651CEchFDp_5f1B02916188585F4522CC"));
                cookieContainer.Add(new Uri("https://www.facebook.com"),new Cookie("act","1376595520739%2F13"));
                cookieContainer.Add(new Uri("https://www.facebook.com"), new Cookie("x-referer", "%2Fcastlechevrolet%3Fsoft%3Dside-area%23%2Fcastlechevrolet"));
                cookieContainer.Add(new Uri("https://www.facebook.com"), new Cookie("xs", "64%3Aemlue1BBmLgpOw%3A2%3A1376079730"));
                
                ConnectionStatus httpStatus = ConnectorHttpRequest(ConnectionParameters.HtmlRequestUrl, null, cookieContainer);
                if (httpStatus == ConnectionStatus.Success)
                {
                    HttpRequestResults = HttpRequestResults.Replace("\\u003C\\", "<").Replace("&gt;", ">").Replace("&lt;", "<").Replace("&quot;", "\"").Replace("\u003C", "<");
                    var testList = Regex.Split(HttpRequestResults, "ajaxify");
                    var temp1 = string.Empty;
                    var temp2 = string.Empty;
                    var posReview = 0;
                    var posStarReview = 0;
                    var pos2 = 0;
                    var pos3 = 0;
                    var pos4 = 0;
                    var counter = 0;

                    foreach (var item in testList)
                    {
                        counter++;
                        Debug.WriteLine("#" + counter);
                        
                        //if (item.IndexOf("div class=\\\"mvs\\\"") >= 1)
                        //{
                        //    temp1 = StringUtils.ExtractSubstring(item, "div class=\\\"mvs\\\"", "/div>");
                        //    Debug.WriteLine(temp1);
                        //}
                        
                        posReview = item.IndexOf("user.php?id=");

                        if (posReview > 0)
                        {
                            //Get Review Text

                            posStarReview = item.IndexOf("text_exposed_root");

                            if (posStarReview > 0)
                            {
                                // Is a starred review
                                pos2 = posStarReview + 20;

                                var positions = item.AllIndexesOf("text_exposed_hide");
                                
                                temp2 = item.Substring(pos2, positions[1] - pos2);
                                pos4 = temp2.LastIndexOf("</span>");
                                temp1 = temp2.Substring(0, pos4);
                                temp1 = StringUtils.HtmlStrip(temp1);
                            }
                            else
                            {
                                // Is not a starred review
                                pos2 = item.IndexOf("mvs", posReview) + 6;
                                pos3 = item.IndexOf("<", pos2);
                                temp1 = item.Substring(pos2, pos3 - pos2);
                            }
                            
                            Debug.WriteLine(temp1);

                            //Get Reviewer Name
                            pos2 = item.IndexOf(">", posReview) + 1;
                            pos3 = item.IndexOf("<", posReview);
                            temp1 = item.Substring(pos2, pos3-pos2);
                            Debug.WriteLine(temp1);
                        }
                    }

                }
                else
                {
                    
                }

            }

            
            return returnStatus;
        }

        private string ExtractToHTML(string httpResult)
        {
            var result = string.Empty;

            return result;
        }

        
    }
}
