﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Connectors.Parameters;
using Connectors.Reviews;
using HtmlAgilityPack;
using Newtonsoft.Json.Linq;
using SI.DTO;
using SI.Extensions;

namespace Connectors
{
    public class GoogleReviewsConnection : SIReviewConnection
    {
        private SIGoogleReviewConnectionParameters ConnectionParameters;

        // JSON indexes 
        private const int _root = 0;
        private const int _body = 0;
        private const int _section = 1;
        private const int _reviewsect = 11;
        private const int _reviews = 0;

        // indexes within each review array		
        //full text index
        private const int _fulltext = 3;
        private const int _fulltextshort = 4;
        //reviewer name indexes
        private const int _usersect = 0;
        private const int _userdetails = 0;
        private const int _username = 1;
        private const int _userid = 3;
        private const int _timeframe = 5;
        //rating
        private const int _ratingsect = 1;

        //comment
        private const int _commentsect = 26;
        private const int _commenttext = 0;
        private const int _commenttimeframe = 1;


        #region Initializer
        public GoogleReviewsConnection(SIGoogleReviewConnectionParameters googleReviewConnectionParameters)
            : base(googleReviewConnectionParameters)
        {
            //ReviewsPerPage = 10;
            ConnectionParameters = googleReviewConnectionParameters;

            //ConnectionParameters.ApiRequestUrl = @"https://plus.google.com/_/pages/local/loadreviews?_reqid=267916&rt=j";
            //ConnectionParameters.HtmlRequestUrl = @"https://plus.google.com/112351237584855391100/about?gl=US&hl=en-US&review=1";

            //PostData.Add("f.req", "[\"278050991257464660\",null,[null,null,[[28,0,1000,{}],[30,null,null,{}]]],[null,null,true,true,6,true]]");
            //PostData.Add("at", "AObGSAjXxm1VBhhQWAfeQkIeTA9IoGethA:1372251820988");

            //7991780716084416063  castle lookupid
        }
        #endregion

        #region Protected Methods

        protected override DownloadDataBucket DoDownload(ReviewUrlDTO url, List<string> failureReasons)
        {
            if (string.IsNullOrEmpty(url.HtmlURL.Trim()))
            {
                return new DownloadDataBucket();
            }

            HtmlDocument doc = DownloadDocument(url.HtmlURL, null, 3, 2000);

            // if html fails, always fail with a null return. no html header, no go
            if (doc == null) return null;

            GoogleBucket bucket = new GoogleBucket() { HtmlDocument = doc };
            if (string.IsNullOrWhiteSpace(url.ExternalID))
            {
                var finalRedirectedUrl = GetFinalRedirectedUrl(url.HtmlURL);
                if (!string.IsNullOrEmpty(finalRedirectedUrl))
                {
                    string ExternalID = scrapeGooglePlaceId(bucket, finalRedirectedUrl, url.HtmlURL);
                    url.ExternalID = ExternalID;
                }
            }

            if (!string.IsNullOrWhiteSpace(url.ExternalID))
            {
                Dictionary<string, string> postVars = new Dictionary<string, string>();
                postVars.Add("f.req", ConnectionParameters.XPathParameters.freq.Replace("placeid", url.ExternalID));
                postVars.Add("at", ConnectionParameters.XPathParameters.At);
                //postVars.Add("f.req", "[\"7991780716084416063\",null,[null,null,[[28,0,1000,{}],[30,null,null,{}]]],[null,null,true,true,6,true]]");
                //postVars.Add("at", "AObGSAjXxm1VBhhQWAfeQkIeTA9IoGethA:1372251820988");

                string apiString = DownloadString(ConnectionParameters.XPathParameters.ApiURL, postVars);

                if (!string.IsNullOrWhiteSpace(apiString))
                {
                    try
                    {
                        bucket.ApiJsonData = apiString;
                    }
                    catch (Exception)
                    {
                        // for now do nothing if we cannot parse the api json...
                        bucket.ApiJsonData = null;
                    }
                }
            }

            return bucket;
        }

        protected override SI.DTO.ReviewUrlDTO TweakUrl(SI.DTO.ReviewUrlDTO url)
        {
            return url;
        }

        protected override bool CheckHasNoServiceReviews(DownloadDataBucket bucket, List<string> failureReasons)
        {
            return true;
        }

        protected override bool CheckHasCheckNoReviews(DownloadDataBucket bucket, List<string> failureReasons)
        {
            HtmlDocument doc = bucket.HtmlDocument;

            bool result = false;

            HtmlNode node = SelectSingleNode(doc.DocumentNode, BaseParameters.BaseXPaths.NoReviewPath, true, null);
            if (node != null)
            {
                if (!string.IsNullOrWhiteSpace(BaseParameters.BaseXPaths.NoReviewText))
                {
                    if (string.IsNullOrWhiteSpace(node.InnerText))
                    {
                        result = false;
                    }
                    else
                    {
                        if (node.InnerText.Trim().ToLower().Contains(BaseParameters.BaseXPaths.NoReviewText.Trim().ToLower()))
                        {
                            if (node.InnerText.Trim().ToLower().Contains("be the first to review"))
                            {
                                result = true; 
                            }
                            else
                            {
                                result = false;
                            }
                        }
                    }
                }
                else
                {
                    result = true;
                }
            }

            return result;
        }

        protected override bool CheckHasNoServiceRating(DownloadDataBucket bucket, List<string> failureReasons)
        {
            return false;
        }

        protected override bool CheckHasNoRating(DownloadDataBucket bucket, List<string> failureReasons)
        {
            HtmlDocument doc = bucket.HtmlDocument;

            bool result = false;
            HtmlNode node = SelectSingleNode(doc.DocumentNode, ConnectionParameters.XPathParameters.NoRatingPath, true, null);

            if (node != null)
            {
                if (!string.IsNullOrWhiteSpace(ConnectionParameters.XPathParameters.NoRatingText))
                {
                    if (string.IsNullOrWhiteSpace(node.InnerText))
                    {
                        result = false;
                    }
                    else
                    {
                        //string rating = node.InnerText.Trim().Replace("Rated:", "").Trim();
                        if (node.InnerText.Trim().ToLower().Contains(ConnectionParameters.XPathParameters.NoRatingText.Trim().ToLower()))
                        {
                            result = true;
                        }
                    }
                }
            }
            return result;
        }

        protected override int ScrapeServiceReviewCount(DownloadDataBucket bucket, List<string> failureReasons)
        {
            return 0;
        }

        protected override int ScrapeReviewCount(DownloadDataBucket bucket, List<string> failureReasons)
        {
            HtmlDocument doc = bucket.HtmlDocument;

            int reviewCount = 0;

            HtmlNode node = SelectSingleNode(doc.DocumentNode, BaseParameters.BaseXPaths.ReviewCountPath, true, null);
            if (node != null)
            {
                if (!string.IsNullOrWhiteSpace(node.InnerText))
                {
                    int.TryParse(node.InnerText.Replace(" reviews", "").Replace(" review", ""), out reviewCount);
                }
            }
            else
            {
                node = SelectSingleNode(doc.DocumentNode, BaseParameters.BaseXPaths.RatingValuePath, true, null);
                if (node != null)
                {
                    if (!string.IsNullOrWhiteSpace(node.InnerText))
                    {
                        int.TryParse(node.InnerText.Replace(" reviews", "").Replace(" review", ""), out reviewCount);
                    }
                }
            }

            return reviewCount;
        }

        protected override decimal ScrapeServiceRating(DownloadDataBucket bucket, List<string> failureReasons)
        {
            return 0;
        }

        protected override decimal ScrapeRating(DownloadDataBucket bucket, List<string> failureReasons)
        {
            HtmlDocument doc = bucket.HtmlDocument;

            decimal rating = 0;

            HtmlNode node = SelectSingleNode(doc.DocumentNode, BaseParameters.BaseXPaths.RatingValuePath, true, null);
            if (node != null)
            {
                if (!string.IsNullOrWhiteSpace(BaseParameters.BaseXPaths.RatingValuePath))
                {
                    if (!string.IsNullOrWhiteSpace(node.InnerText))
                    {
                        string rtext = node.InnerText.ToLower();
                        decimal.TryParse(rtext, out rating);
                    }
                }
            }

            return rating;
        }

        protected override List<KeyValuePair<string, string>> ScrapeExtraInfo(DownloadDataBucket bucket, List<string> failureReasons)
        {
            HtmlDocument doc = bucket.HtmlDocument;

            List<KeyValuePair<string, string>> ListkeyvaluePair = new List<KeyValuePair<string, string>>();
            List<string> failedXPaths = new List<string>();

            return ListkeyvaluePair;

        }

        protected override List<ReviewDownloadResult.ReviewDetail> ScrapeDetails(DownloadDataBucket bucket, SI.DTO.ReviewUrlDTO originalUrl, List<string> failureReasons)
        {
            List<ReviewDownloadResult.ReviewDetail> details = new List<ReviewDownloadResult.ReviewDetail>();

            GoogleBucket myBucket = (GoogleBucket)bucket;
            if (myBucket.ApiJsonData != null)
            {
                details = scrapeDetails(myBucket.ApiJsonData);
            }

            return details;
        }

        protected override void dispose()
        {

        }

        #endregion

        #region private Methods/Class
        private class GoogleBucket : DownloadDataBucket
        {
            public GoogleBucket()
            {
                ApiJsonData = null;
            }
            public string ApiJsonData { get; set; }
        }

        private List<ReviewDownloadResult.ReviewDetail> scrapeDetails(string json)
        {
            List<ReviewDownloadResult.ReviewDetail> details = new List<ReviewDownloadResult.ReviewDetail>();

            if (json != null)
            {
                json = json.Substring(json.IndexOf("[[[")); //find the beginning [[[

                JArray jAReviews = JArray.Parse(json);
                if (jAReviews != null)
                {
                    if (jAReviews[_root][_body][_section][_reviewsect].ToString() != "")
                    {
                        JEnumerable<JToken> reviewTokens = jAReviews[_root][_body][_section][_reviewsect][_reviews].Children();
                        foreach (JToken reviewToken in reviewTokens)
                        {
                            ReviewDownloadResult.ReviewDetail review = new ReviewDownloadResult.ReviewDetail();

                            review.ReviewTextFull = reviewToken[_fulltext].ToString();
                            if (string.IsNullOrEmpty(review.ReviewTextFull))
                                review.ReviewTextFull = reviewToken[_fulltextshort].ToString();

                            review.ReviewerName = reviewToken[_usersect][_userdetails][_username].ToString();
                            review.FullReviewURL = string.Format("plus.google.com/u/0/{0}/reviews", reviewToken[_usersect][_userid]);
                            review.ReviewDate = new SI.SIDateTime(SentenceDateToDate(reviewToken[_timeframe].ToString()), 0);

                            review.Rating = 0;
                            if (reviewToken[_ratingsect] != null)
                                review.Rating = reviewToken[_ratingsect].ToString().ToDecimal() / 1000;

                            if (reviewToken[_commentsect].HasValues)
                            {
                                ReviewDownloadResult.ReviewComment comment = new ReviewDownloadResult.ReviewComment();
                                comment.Comment = reviewToken[_commentsect][_commenttext].ToString();
                                comment.CommentDate = new SI.SIDateTime(SentenceDateToDate(reviewToken[_commentsect][_commenttimeframe].ToString()), 0);

                                review.Comments.Add(comment);
                            }

                            details.Add(review);
                        }
                    }
                }

            }
            return details;
        }

        private DateTime SentenceDateToDate(string sentence)
        {
            DateTime result = DateTime.Now;
            int numPart = 1;
            sentence = sentence.ToLower();

            if (sentence.Contains("year"))
            {
                if (sentence.Contains("years"))
                    numPart = sentence.Substring(0, sentence.IndexOf("years")).ToInteger();
                result = DateTime.Now.AddYears(-1 * numPart);
            }
            else if (sentence.Contains("month"))
            {
                if (sentence.Contains("months"))
                    numPart = sentence.Substring(0, sentence.IndexOf("months")).ToInteger();
                result = DateTime.Now.AddMonths(-1 * numPart);
            }
            else if (sentence.Contains("week"))
            {
                if (sentence.Contains("weeks"))
                {
                    numPart = sentence.Substring(0, sentence.IndexOf("weeks")).ToInteger();
                    result = DateTime.Now.AddDays(-1 * numPart * 7);
                }
            }
            else if (sentence.Contains("day"))
            {
                if (sentence.Contains("days"))
                    numPart = sentence.Substring(0, sentence.IndexOf("days")).ToInteger();
                result = DateTime.Now.AddDays(-1 * numPart);
            }
            else if (sentence.Contains("hour"))
            {
                if (sentence.Contains("hours"))
                    numPart = sentence.Substring(0, sentence.IndexOf("hours")).ToInteger();
                result = DateTime.Now.AddHours(-1 * numPart);
            }
            else if (sentence.Contains("minute"))
            {
                if (sentence.Contains("minutes"))
                    numPart = sentence.Substring(0, sentence.IndexOf("minutes")).ToInteger();
                result = DateTime.Now.AddMinutes(-1 * numPart);
            }
            else if (sentence.Contains("second"))
            {
                if (sentence.Contains("seconds"))
                    numPart = sentence.Substring(0, sentence.IndexOf("seconds")).ToInteger();
                result = DateTime.Now.AddSeconds(-1 * numPart);
            }

            if (result > DateTime.Now)
                result = DateTime.Now;

            return Convert.ToDateTime(result.ToShortDateString());
        }

        private string scrapeGooglePlaceId(DownloadDataBucket bucket, string finalRedirectedUrl, string dtoHTMLURL)
        {           
            HtmlDocument doc = bucket.HtmlDocument;
            string dataId = string.Empty;
            string placeid = string.Empty;
            var tempurl = finalRedirectedUrl.Substring(finalRedirectedUrl.IndexOf("com") + 4);
            if (tempurl.Contains("?"))
            {
                dataId = tempurl.Substring(0, tempurl.IndexOf("?"));                
            }
            else if (tempurl.Contains("/"))
            {
                dataId = tempurl.Substring(0, tempurl.IndexOf("/"));
            }
            else
            {
                dataId = tempurl;
            }

            dataId = new String(dataId.Where(Char.IsDigit).ToArray());

            if (string.IsNullOrWhiteSpace(dataId))
            {
                tempurl = dtoHTMLURL.Substring(dtoHTMLURL.IndexOf("com") + 4);
                if (tempurl.Contains("?"))
                {
                    dataId = tempurl.Substring(0, tempurl.IndexOf("?"));
                }
                else if (tempurl.Contains("/"))
                {
                    dataId = tempurl.Substring(0, tempurl.IndexOf("/"));
                }
                else
                {
                    dataId = tempurl;
                }

                dataId = new String(dataId.Where(Char.IsDigit).ToArray());
            }
            HtmlNode node = SelectSingleNode(doc.DocumentNode, "//div[@data-oid='" + dataId + "']", true, null);
            if (node != null)
            {
                placeid = node.Attributes["data-placeid"].Value;               
            }

            if (string.IsNullOrEmpty(placeid))
            {               
                placeid = SelectSingleNode(doc.DocumentNode, "//div[@guidedhelpid='reviewbutton']//div", true, null).Attributes["data-placeid"].Value;
            }
            return placeid;
        }
        #endregion

        /*
         
		public ConnectionStatus GetReviewsAPI(bool includeDetails = true)
		{
			ConnectionStatus returnStatus = ConnectionStatus.InvalidData;

			if (!string.IsNullOrEmpty(ConnectionParameters.ApiRequestUrl))
			{
				HttpRequestResults = string.Empty;
				ConnectionStatus httpStatus = ConnectorHttpRequest(ConnectionParameters.ApiRequestUrl);
				if (httpStatus == ConnectionStatus.Success)
				{
					if (!string.IsNullOrEmpty(HttpRequestResults))
					{
						//skip preamble - The first chars are injected for AngularJS security reasons
						HttpRequestResults = HttpRequestResults.Substring(HttpRequestResults.IndexOf("[[[")); //find the beginning [[[

						JArray jAReviews = JArray.Parse(HttpRequestResults);
						if (jAReviews != null)
						{
							SalesReviewCount = 0;
							ServiceReviewCount = 0;
							SalesAverageRating = 0;
							ServiceAverageRating = 0;

							returnStatus = ConnectionStatus.Success;

							JEnumerable<JToken> reviewTokens = jAReviews[_root][_body][_section][_reviewsect][_reviews].Children();
							SalesReviewCount = reviewTokens.Count();

							foreach (JToken reviewToken in reviewTokens)
							{
								Review review = new Review();
								review.Type = "sales";

								review.ReviewTextFull = reviewToken[_fulltext].ToString();
								if (string.IsNullOrEmpty(review.ReviewTextFull))
									review.ReviewTextFull = reviewToken[_fulltextshort].ToString();

								review.ReviewerName = reviewToken[_usersect][_userdetails][_username].ToString();
								review.FullReviewURL = string.Format("plus.google.com/u/0/{0}/reviews", reviewToken[_usersect][_userid]);
								review.ReviewDate = SentenceDateToDate(reviewToken[_timeframe].ToString());

								review.Rating = 0;
								if (reviewToken[_ratingsect] != null)
									review.Rating = reviewToken[_ratingsect].ToString().ToDecimal() / 1000;

								if (reviewToken[_commentsect].HasValues)
								{
									ReviewComment comment = new ReviewComment();
									comment.Comment = reviewToken[_commentsect][_commenttext].ToString();
									comment.CommentDate = SentenceDateToDate(reviewToken[_commentsect][_commenttimeframe].ToString());

									review.Comments.Add(comment);
								}

								ReviewDetails.Add(review);
							}

							if (ReviewDetails.Any())
								SalesAverageRating = ReviewDetails.Average(r => r.Rating);
						}
					}
				}
			}
			
			return returnStatus;
		}

		private DateTime SentenceDateToDate(string sentence)
		{
			DateTime result = DateTime.Now;
			int numPart = 1;
			sentence = sentence.ToLower();

			if (sentence.Contains("year"))
			{
				if (sentence.Contains("years"))
					numPart = sentence.Substring(0, sentence.IndexOf("years")).ToInteger();
				result = DateTime.Now.AddYears(-1 * numPart);
			}
			else if (sentence.Contains("month"))
			{
				if (sentence.Contains("months"))
					numPart = sentence.Substring(0, sentence.IndexOf("months")).ToInteger();
				result = DateTime.Now.AddMonths(-1*numPart);
			}
			else if (sentence.Contains("week"))
			{
				if (sentence.Contains("weeks"))
				{
					numPart = sentence.Substring(0, sentence.IndexOf("weeks")).ToInteger();
					result = DateTime.Now.AddDays(-1*numPart*7);
				}
			}
			else if (sentence.Contains("day"))
			{
				if (sentence.Contains("days"))
					numPart = sentence.Substring(0, sentence.IndexOf("days")).ToInteger();
				result = DateTime.Now.AddDays(-1*numPart);
			}
			else if (sentence.Contains("hour"))
			{
				if (sentence.Contains("hours"))
					numPart = sentence.Substring(0, sentence.IndexOf("hours")).ToInteger();
				result = DateTime.Now.AddHours(-1*numPart);
			}
			else if (sentence.Contains("minute"))
			{
				if (sentence.Contains("minutes"))
					numPart = sentence.Substring(0, sentence.IndexOf("minutes")).ToInteger();
				result = DateTime.Now.AddMinutes(-1*numPart);
			}
			else if (sentence.Contains("second"))
			{
				if (sentence.Contains("seconds"))
					numPart = sentence.Substring(0, sentence.IndexOf("seconds")).ToInteger();
				result = DateTime.Now.AddSeconds(-1*numPart);
			}

			if (result > DateTime.Now)
				result = DateTime.Now;

			return result;
		}

		public ConnectionStatus GetReviewsHtml(bool includeDetails = true)
		{
			ConnectionStatus returnStatus = ConnectionStatus.InvalidData;

			if (!string.IsNullOrEmpty(ConnectionParameters.HtmlRequestUrl))
			{
				HttpRequestResults = string.Empty;

				ConnectionStatus httpStatus = ConnectorHttpRequest(ConnectionParameters.HtmlRequestUrl);
				if (httpStatus == ConnectionStatus.Success)
				{
					HtmlDocument htmlDocument = new HtmlDocument();
					htmlDocument.OptionFixNestedTags = true;
					htmlDocument.LoadHtml(HttpRequestResults);

					HtmlNode ReviewParentNode = SelectSingleNode(htmlDocument.DocumentNode, ConnectionParameters.XPathParameters.ParentnodeReviewSummaryXPath);
					if (ReviewParentNode != null)
					{
						SalesReviewCount = 0;
						SalesAverageRating = 0;

						HtmlNode SalesRatingChildNode = SelectSingleNode(ReviewParentNode, ConnectionParameters.XPathParameters.RatingValueXPath);
						if (SalesRatingChildNode != null)
							SalesAverageRating = SalesRatingChildNode.InnerText.IsNullOptional("0").Trim().ToDecimal();

						HtmlNode SalesCountChildNode = SelectSingleNode(ReviewParentNode, ConnectionParameters.XPathParameters.ReviewCountCollectedXPath);
						if (SalesCountChildNode != null)
							SalesReviewCount = SalesCountChildNode.InnerText.IsNullOptional("0").Trim().Replace(" reviews", "").ToInteger();

						if (includeDetails) //can't check review count because there could be less that 10 reviews, which they dont have a header for
						{
							HtmlNodeCollection ReviewNodes = SelectNodes(ReviewParentNode, ConnectionParameters.XPathParameters.ListNodeReviewDetailsXPath);
							if (ReviewNodes != null)
							{
								if (ReviewNodes.Count > 0 && SalesReviewCount == 0)
								{
									SalesAverageRating = 0.01m; // a marker to indicate reviews but less than 10
									SalesReviewCount = ReviewNodes.Count;
								}

								//scrap the initial count and get the rest from API call
								foreach (HtmlNode reviewNode in ReviewNodes)
								{
									Review review = new Review();

									review.Type = "sales";

									review.Rating = 0;
									HtmlNodeCollection RatingNodes = SelectNodes(reviewNode, ConnectionParameters.XPathParameters.ReviewDetailRatingValueXPath);
									if (RatingNodes != null)
										review.Rating = RatingNodes.Count; //a display of stars with a particular style.
									RatingNodes = SelectNodes(reviewNode, ConnectionParameters.XPathParameters.ReviewDetailRatingHalfValueXPath);
									if (RatingNodes != null)
									{
										if (ReviewNodes.Count > 0)
											review.Rating += .5m;
									}

									HtmlNode ReviewNameNode = SelectSingleNode(reviewNode, ConnectionParameters.XPathParameters.ReviewerNameXPath);
									if (ReviewNameNode != null)
										review.ReviewerName = ReviewNameNode.InnerText.IsNullOptional("").StripHtml();
									else
										review.ReviewerName = "A Google User";

									HtmlNode ReviewDateNode = SelectSingleNode(reviewNode, ConnectionParameters.XPathParameters.ReviewDateXPath);
									if (ReviewDateNode != null)
									{
										string reviewDateSentence = ReviewDateNode.InnerText.IsNullOptional("").Replace("reviewed ", "").Trim();
										review.ReviewDate = SentenceDateToDate(reviewDateSentence);
									}

									HtmlNode ReviewTextNode = SelectSingleNode(reviewNode, ConnectionParameters.XPathParameters.ReviewTextFullXPath);
									if (ReviewTextNode != null)
										review.ReviewTextFull = ReviewTextNode.InnerText.IsNullOptional("").StripHtml();

									HtmlNode ReviewCommentParentNode = SelectSingleNode(reviewNode, ConnectionParameters.XPathParameters.ReviewCommentParentXPath);
									if (ReviewCommentParentNode != null)
									{
										ReviewComment reviewComment = new ReviewComment();

										HtmlNode ReviewCommentDateNode = SelectSingleNode(reviewNode, ConnectionParameters.XPathParameters.ReviewCommentDateXPath);
										if (ReviewCommentDateNode != null)
										{
											string reviewDateSentence = ReviewCommentDateNode.InnerText.IsNullOptional("").Replace("-", "").Trim();
											reviewComment.CommentDate = SentenceDateToDate(reviewDateSentence);
										}	

										HtmlNode ReviewCommentTextNode = SelectSingleNode(reviewNode, ConnectionParameters.XPathParameters.ReviewCommentTextXPath);
										if (ReviewCommentTextNode != null)
											reviewComment.Comment = ReviewCommentTextNode.InnerText.IsNullOptional("");

										review.Comments.Add(reviewComment);
									}

								ReviewDetails.Add(review);
								}

								//walk down the line to get the rest
								int reviewCount = ReviewDetails.Count;
								if (SalesReviewCount > reviewCount)
								{
									//get the placeID that is used in the API call
									HtmlNode ReviewAPIPlaceIDNode = SelectSingleNode(ReviewParentNode, ConnectionParameters.XPathParameters.ReviewAPIPlaceIDXPath);
									if (ReviewAPIPlaceIDNode != null)
									{
										HtmlAttribute PlaceIDAttribute = ReviewAPIPlaceIDNode.Attributes["data-placeid"];
										if (PlaceIDAttribute != null)
										{
											string placeID = PlaceIDAttribute.Value;

											PostData.Clear();
											PostData.Add("f.req", "[\"" + placeID + "\",null,[null,null,[[28," + reviewCount + ",1000,{}],[30,null,null,{}]]],[null,null,true,true,6,true]]");
											PostData.Add("at", "AObGSAjXxm1VBhhQWAfeQkIeTA9IoGethA:1372251820988");
											
											if (string.IsNullOrEmpty(ConnectionParameters.ApiRequestUrl))
												ConnectionParameters.ApiRequestUrl = @"https://plus.google.com/_/pages/local/loadreviews?_reqid=1043554&rt=j";

											//save our counts
											decimal savedRating = SalesAverageRating;
											int savedCount = SalesReviewCount;

											returnStatus = GetReviewsAPI();
											if (returnStatus == ConnectionStatus.Success)
											{
												SalesAverageRating = savedRating;
												SalesReviewCount = savedCount;
											}
										}
									}
								}
							}
						}
					}
					else
						returnStatus = ConnectionStatus.InvalidData;
				}
			}

			return returnStatus;
		}
        
		protected override
			void Dispose()
		{
			
		}

        */
    }
}
