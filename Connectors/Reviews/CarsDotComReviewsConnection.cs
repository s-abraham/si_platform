﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Connectors.Parameters;
using HtmlAgilityPack;
using SI.Extensions;
using System.Threading;
using SI.DTO;
using Connectors.Reviews;
using SI;
namespace Connectors
{
	public class CarsDotComReviewsConnection : SIReviewConnection
	{
		private SICarsDotComReviewConnectionParameters ConnectionParameters;

        #region Initializer
		public CarsDotComReviewsConnection(SICarsDotComReviewConnectionParameters siCarsDotComReviewConnectionParameters)
			: base(siCarsDotComReviewConnectionParameters)
        {
			ConnectionParameters = siCarsDotComReviewConnectionParameters;
        }
        #endregion
        
        #region Protected Methods
        protected override DownloadDataBucket DoDownload(ReviewUrlDTO url, List<string> failureReasons)
        {
            var bucket = new DownloadDataBucket();

            if (!string.IsNullOrEmpty(url.HtmlURL.Trim()))
            {
                HtmlDocument doc = DownloadDocument(url.HtmlURL, null, 3, 2000);
                // if html fails, always fail with a null return. no html header, no go
                if (doc == null) return null;
                bucket = new DownloadDataBucket() { HtmlDocument = doc };
            }

            return bucket;
        }

        protected override ReviewUrlDTO TweakUrl(ReviewUrlDTO urlDTO)
        {
            string url = urlDTO.HtmlURL;           
            if (!url.Contains("reviews.action?dlId="))
            {
                urlDTO.HtmlURL = url.Replace("userReview.action", "reviews.action");
            }
            else
            {
                urlDTO.HtmlURL = url;
            }
            if (urlDTO.HtmlURL.Contains("reviews.action"))
            {
                string NewURL = urlDTO.HtmlURL.Substring(0, urlDTO.HtmlURL.IndexOf("reviews.action"));
                NewURL = NewURL + "reviews/";
                urlDTO.HtmlURL = NewURL;
            }
            
            List<string> failureReasons = new List<string>();
            DownloadDataBucket bucket = DoDownload(urlDTO, failureReasons);

            if (bucket != null && bucket.HtmlDocument != null)
            {

                HtmlNode node = SelectSingleNode(bucket.HtmlDocument.DocumentNode,
                    ".//div[@class='tabs']//a[contains(@name,'dealer-reviews')]", true, null);

                if (node != null)
                {
                    urlDTO.HtmlURL = "http://www.cars.com" + node.GetAttributeValue("href", "");
                }

                if (!urlDTO.HtmlURL.Contains("?count=2500"))
                    urlDTO.HtmlURL = urlDTO.HtmlURL + "?count=2500";
            }
            return urlDTO;

            // 
        }

        protected override bool CheckHasCheckNoReviews(DownloadDataBucket bucket, List<string> failureReasons)
        {
            HtmlDocument doc = bucket.HtmlDocument;

            bool result = false;

            HtmlNode node = SelectSingleNode(doc.DocumentNode, BaseParameters.BaseXPaths.NoReviewPath, true, null);
            if (node != null)
            {
                if (!string.IsNullOrWhiteSpace(BaseParameters.BaseXPaths.NoReviewText))
                {
                    if (string.IsNullOrWhiteSpace(node.InnerText))
                    {
                        result = false;
                    }
                    else
                    {
                        if (node.InnerText.Trim().ToLower().Contains(BaseParameters.BaseXPaths.NoReviewText.Trim().ToLower()))
                        {
                            result = true;
                        }
                    }
                }
                else
                {
                    result = true;
                }
            }

            return result;

        }

        protected override int ScrapeReviewCount(DownloadDataBucket bucket, List<string> failureReasons)
        {
            HtmlDocument doc = bucket.HtmlDocument;

            int reviewCount = 0;
            
            HtmlNode node = SelectSingleNode(doc.DocumentNode, BaseParameters.BaseXPaths.ReviewCountPath, true, null);
            if (node != null)
            {
                if (!string.IsNullOrWhiteSpace(node.InnerText))
                {
                    string convertedVal = node.InnerText.Replace("Reviews", "").Replace("Review", "");
                   int.TryParse(convertedVal, out reviewCount);                    
                }
            }

            return reviewCount;
            

        }

        protected override bool CheckHasNoServiceReviews(DownloadDataBucket bucket, List<string> failureReasons)
        {
            return true;
        }

        protected override int ScrapeServiceReviewCount(DownloadDataBucket bucket, List<string> failureReasons)
        {
            return 0;
        }

        protected override bool CheckHasNoRating(DownloadDataBucket bucket, List<string> failureReasons)
        {
            bool result = false;
                     
            return result;
        }

        protected override decimal ScrapeRating(DownloadDataBucket bucket, List<string> failureReasons)
        {
            HtmlDocument doc = bucket.HtmlDocument;

            decimal rating = 0;

            HtmlNode node = SelectSingleNode(doc.DocumentNode, BaseParameters.BaseXPaths.RatingValuePath, true, null);
            if (node != null)
            {
                if (!string.IsNullOrWhiteSpace(BaseParameters.BaseXPaths.RatingValuePath))
                {
                    if (!string.IsNullOrWhiteSpace(node.InnerText))
                    {
                        string rtext = node.InnerText.ToLower();
                        decimal.TryParse(rtext, out rating);
                    }
                }
            }


            return rating;
        }

        protected override bool CheckHasNoServiceRating(DownloadDataBucket bucket, List<string> failureReasons)
        {
            return false;
        }

        protected override decimal ScrapeServiceRating(DownloadDataBucket bucket, List<string> failureReasons)
        {
            return 0;
        }

        protected override List<KeyValuePair<string, string>> ScrapeExtraInfo(DownloadDataBucket bucket, List<string> failureReasons)
        {
            HtmlDocument doc = bucket.HtmlDocument;

            List<KeyValuePair<string, string>> ListkeyvaluePair = new List<KeyValuePair<string, string>>();
            List<string> failedXPaths = new List<string>();

            HtmlNodeCollection nodes = SelectNodes(doc.DocumentNode, ConnectionParameters.XPathParameters.ParentNodeExtraValueSummaryXPath, true, null);
            int ReviewCount = ScrapeReviewCount(bucket, failureReasons);

            if (nodes != null && nodes.Count > 0)
            {
                string value = string.Empty;
                string key = string.Empty;

                foreach (var htmlExtraValueDIV in nodes)
                {
                    try
                    {
                        HtmlNode node = SelectSingleNode(htmlExtraValueDIV, ConnectionParameters.XPathParameters.XtraValueWrapperXPath);

                        if (node != null)
                        {
                            if (!string.IsNullOrWhiteSpace(ConnectionParameters.XPathParameters.XtraValueNodeXPath) &&
                                    !string.IsNullOrWhiteSpace(ConnectionParameters.XPathParameters.XtraKeyNodeXPath))
                            {
                                value = Convert.ToString(SelectSingleNode(htmlExtraValueDIV,ConnectionParameters.XPathParameters.XtraValueNodeXPath).GetAttributeValue("title", "")).Trim();
                                key = Convert.ToString(SelectSingleNode(htmlExtraValueDIV,ConnectionParameters.XPathParameters.XtraKeyNodeXPath).InnerText).Trim();
                            }
                        }
                        else
                        {
                            if (!string.IsNullOrWhiteSpace(ConnectionParameters.XPathParameters.XtraNoWrapperValueNodeXPath) &&
                                    !string.IsNullOrWhiteSpace(ConnectionParameters.XPathParameters.XtraNoWrapperKeyNodeXPath))
                            {

                                value = Convert.ToString(SelectSingleNode(htmlExtraValueDIV,ConnectionParameters.XPathParameters.XtraNoWrapperValueNodeXPath).InnerText).Trim();
                                key = Convert.ToString(SelectSingleNode(htmlExtraValueDIV,ConnectionParameters.XPathParameters.XtraNoWrapperKeyNodeXPath).InnerText).Trim();
                            }
                        }

                        ListkeyvaluePair.Add(new KeyValuePair<string, string>(key, value));

                    }
                    catch (Exception ex)
                    {
                        failureReasons.Add(BaseParameters.BaseXPaths.RatingValuePath);
                    }
                }
            }
            else
            {                
                if (ReviewCount > 0)
                {
                    failedXPaths.Add(ConnectionParameters.XPathParameters.ParentNodeExtraValueSummaryXPath);
                }
            }

            try
            {
                string value = string.Empty;
                string key = string.Empty;

                HtmlNode node1 = SelectSingleNode(doc.DocumentNode, ConnectionParameters.XPathParameters.ExtraValueSummaryRecommendedXPath, true, null);

                if (node1 != null)
                {
                    if (!string.IsNullOrWhiteSpace(ConnectionParameters.XPathParameters.ExtraValueSummaryRecommendedXPath) &&
                                    !string.IsNullOrWhiteSpace(ConnectionParameters.XPathParameters.ExtraValueSummaryRecommendedText))
                    {
                        value = node1.InnerText;
                        key = ConnectionParameters.XPathParameters.ExtraValueSummaryRecommendedText;

                        ListkeyvaluePair.Add(new KeyValuePair<string, string>(key, value));
                    }
                }
                else
                {
                    if (ReviewCount > 0)
                    {
                        failedXPaths.Add(ConnectionParameters.XPathParameters.ExtraValueSummaryRecommendedXPath);
                    }
                }

            }
            catch (Exception ex)
            {
            }


            foreach (string item in failedXPaths)
            {
                failureReasons.Add(string.Format("Failed XPath: {0}", item));
            }


            return ListkeyvaluePair;
        }

        protected override List<Connectors.ReviewDownloadResult.ReviewDetail> ScrapeDetails(DownloadDataBucket bucket, ReviewUrlDTO originalUrl, List<string> failureReasons)
        {
            HtmlDocument headerDoc = bucket.HtmlDocument;

            List<Connectors.ReviewDownloadResult.ReviewDetail> results = new List<ReviewDownloadResult.ReviewDetail>();
            List<string> failedXPaths = new List<string>();

            scrapeDetailsLocal(headerDoc, results, failedXPaths);


            return results;
        }

        protected override void dispose()
        {
            // nothing to do here
        }
        #endregion

        #region private Methods
        private void scrapeDetailsLocal(HtmlDocument headerDoc, List<Connectors.ReviewDownloadResult.ReviewDetail> results, List<string> failedXPaths)
        {
            HtmlNodeCollection details = SelectNodes(headerDoc.DocumentNode, BaseParameters.BaseXPaths.ReviewDetailIteratorPath, true, failedXPaths);

            if (details != null && details.Count() > 0)
            {
                foreach (HtmlNode node in details)
                {
                    try
                    {
                        Connectors.ReviewDownloadResult.ReviewDetail detail = scrapeSingleReviewDetail(node, failedXPaths);
                        if (detail != null)
                        {  
                            results.Add(detail);
                        }
                    }
                    catch (Exception ex)
                    {

                    }
                }
            }
        }

        private Connectors.ReviewDownloadResult.ReviewDetail scrapeSingleReviewDetail(HtmlNode node, List<string> failedXPaths)
        {
            Connectors.ReviewDownloadResult.ReviewDetail review = new Connectors.ReviewDownloadResult.ReviewDetail();

            #region Rating
            review.Rating = 0;
            try
            {
                HtmlNode node2 = SelectSingleNode(node, BaseParameters.BaseXPaths.RatingValueXPath);
                if (node2 != null)
                {
                    if (!string.IsNullOrWhiteSpace(node2.InnerText))
                    {
                        string rtext = node2.InnerText.ToLower().Trim();
                        decimal dValue;
                        if (decimal.TryParse(rtext, out dValue))
                            review.Rating = dValue;
                    }
                }
                else
                {
                    failedXPaths.Add(BaseParameters.BaseXPaths.RatingValueXPath);
                }


            }
            catch (Exception ex)
            {
            }
            #endregion

            #region ReviewerName
            review.ReviewerName = string.Empty;
            try
            {
                HtmlNode node2 = SelectSingleNode(node, ConnectionParameters.XPathParameters.ReviewerNameXPath);
                if (node2 != null)
                {
                    if (!string.IsNullOrWhiteSpace(node2.InnerText))
                    {
                        string rtext = node2.InnerText.Trim();
                        review.ReviewerName = rtext;
                    }
                }
                else
                {
                    failedXPaths.Add(ConnectionParameters.XPathParameters.ReviewerNameXPath);
                }
            }
            catch (Exception ex)
            {
            }
            #endregion

            #region ReviewDate
            review.ReviewDate = SIDateTime.MinValue;
            try
            {
                HtmlNode node2 = SelectSingleNode(node, ConnectionParameters.XPathParameters.ReviewDateXPath);
                if (node2 != null)
                {
                    if (!string.IsNullOrWhiteSpace(node2.InnerText))
                    {
                        string rtext = node2.InnerText.Trim();
                        DateTime rDate;
                        if (DateTime.TryParse(rtext, out rDate))
                        {
                            review.ReviewDate = new SI.SIDateTime(rDate, 0);
                        }
                    }
                }
                else
                {
                    failedXPaths.Add(ConnectionParameters.XPathParameters.ReviewDateXPath);
                }
            }
            catch (Exception ex)
            {

            }
            #endregion

            #region ReviewTextFull
            review.ReviewTextFull = string.Empty;
            try
            {
                HtmlNode node2 = SelectSingleNode(node, ConnectionParameters.XPathParameters.ReviewTextFullXPath);
                if (node2 != null)
                {
                    if (!string.IsNullOrWhiteSpace(node2.InnerText))
                    {
                        string rtext = node2.InnerText.StripHtml().Trim();
                        review.ReviewTextFull = rtext;
                    }
                }
                else
                {
                    failedXPaths.Add(ConnectionParameters.XPathParameters.ReviewTextFullXPath);
                }
            }
            catch (Exception ex)
            {

            }
            #endregion

            #region ExtraValue
            review.ExtraInfo = null;
            try
            {
                review.ExtraInfo = scrapeReviewExtraInfo(node);
            }
            catch (Exception ex)
            {

            }
            #endregion

            #region Reply/Comment

            review.Comments = new List<ReviewDownloadResult.ReviewComment>();
            try
            {
                review.Comments = scrapeReviewComments(node);
            }
            catch (Exception ex)
            {
            }
            #endregion

            return review;


        }

        private List<ReviewDownloadResult.ReviewComment> scrapeReviewComments(HtmlNode ReviewNode)
        {
            List<ReviewDownloadResult.ReviewComment> resultComments = new List<ReviewDownloadResult.ReviewComment>();

            try
            {                
                //HtmlNodeCollection nodes = SelectNodes(ReviewNode, ConnectionParameters.XPathParameters.ParentNodeReviewCommentXPath, true, null);
                HtmlNode node = SelectSingleNode(ReviewNode, ConnectionParameters.XPathParameters.ParentNodeReviewCommentXPath, true, null);
                if (node != null)
                {
                    ReviewDownloadResult.ReviewComment comment = new ReviewDownloadResult.ReviewComment();

                    #region CommenterName

                    comment.CommenterName = string.Empty;
                    try
                    {
                        HtmlNode node2 = SelectSingleNode(node, ConnectionParameters.XPathParameters.CommenterNameXPath);
                        if (node2 != null)
                        {
                            if (!string.IsNullOrWhiteSpace(node2.InnerText))
                            {
                                string rtext = node2.InnerText.Trim();
                                comment.CommenterName = rtext.Replace(" responded to this review","");
                            }
                        }
                        else
                        {
                            
                        }
                    }
                    catch (Exception ex)
                    {
                    }
                    #endregion
                    
                    #region CommentDate

                    comment.CommentDate = SI.SIDateTime.MinValue;
                    try
                    {
                        HtmlNode node2 = SelectSingleNode(node, ConnectionParameters.XPathParameters.CommentDateXPath);
                        if (node2 != null)
                        {
                            if (!string.IsNullOrWhiteSpace(node2.InnerText))
                            {
                                string rtext = node2.InnerText.Trim();
                                DateTime rDate;
                                if (DateTime.TryParse(rtext, out rDate))
                                {
                                    comment.CommentDate = new SI.SIDateTime(rDate, 0);
                                }
                            }
                        }
                        else
                        {
                            
                        }
                    }
                    catch (Exception ex)
                    {

                    }
                    #endregion

                    #region CommentText
                    comment.Comment = string.Empty;
                    try
                    {
                        HtmlNode node2 = SelectSingleNode(node, ConnectionParameters.XPathParameters.CommentXPath);
                        if (node2 != null)
                        {
                            if (!string.IsNullOrWhiteSpace(node2.InnerText))
                            {
                                string rtext = node2.InnerText.StripHtml().Trim();
                                comment.Comment = rtext;
                            }
                        }
                        else
                        {                            
                        }
                    }
                    catch (Exception ex)
                    {

                    }
                    #endregion

                    resultComments.Add(comment);
                    
                }
            }
            catch (Exception ex)
            {

            }
            return resultComments;
        }

        private List<KeyValuePair<string, string>> scrapeReviewExtraInfo(HtmlNode ReviewNode)
        {
            List<KeyValuePair<string, string>> ListkeyvaluePair = new List<KeyValuePair<string, string>>();
            List<string> failedXPaths = new List<string>();

            HtmlNodeCollection nodes = SelectNodes(ReviewNode, ConnectionParameters.XPathParameters.ParentnodeExtraValueXPath, true, null);

            if (nodes != null && nodes.Count > 0)
            {
                string value = string.Empty;
                string key = string.Empty;

                foreach (var htmlExtraValueDIV in nodes)
                {
                    try
                    {
                        HtmlNode node = SelectSingleNode(htmlExtraValueDIV, ConnectionParameters.XPathParameters.XtraValueWrapperXPath);

                        if (node != null)
                        {
                            if (!string.IsNullOrWhiteSpace(ConnectionParameters.XPathParameters.XtraValueNodeXPath) &&
                                    !string.IsNullOrWhiteSpace(ConnectionParameters.XPathParameters.XtraKeyNodeXPath))
                            {   
                                var _node = htmlExtraValueDIV.SelectSingleNode(ConnectionParameters.XPathParameters.XtraValueNodeXPath);
                                if (_node != null)
                                {
                                    value = Convert.ToString(htmlExtraValueDIV.SelectSingleNode(ConnectionParameters.XPathParameters.XtraValueNodeXPath).GetAttributeValue("title", "")).Trim();
                                    key = Convert.ToString(htmlExtraValueDIV.SelectSingleNode(ConnectionParameters.XPathParameters.XtraKeyNodeXPath).InnerText).Trim();
                                }
                                else
                                {
                                    value = Convert.ToString(htmlExtraValueDIV.SelectSingleNode(".//span[@class='bar-rating-na']/span").InnerText).Trim();
                                    key = Convert.ToString(htmlExtraValueDIV.SelectSingleNode(ConnectionParameters.XPathParameters.XtraKeyNodeXPath).InnerText).Trim();
                                }                                
                            }
                        }
                        else
                        {
                            if (!string.IsNullOrWhiteSpace(ConnectionParameters.XPathParameters.XtraNoWrapperValueNodeXPath) &&
                                    !string.IsNullOrWhiteSpace(ConnectionParameters.XPathParameters.XtraNoWrapperKeyNodeXPath))
                            {

                                value = Convert.ToString(htmlExtraValueDIV.SelectSingleNode(ConnectionParameters.XPathParameters.XtraNoWrapperValueNodeXPath).InnerText).Trim();
                                key = Convert.ToString(htmlExtraValueDIV.SelectSingleNode(ConnectionParameters.XPathParameters.XtraNoWrapperKeyNodeXPath).InnerText).Trim();
                            }
                        }

                        ListkeyvaluePair.Add(new KeyValuePair<string, string>(key, value));

                    }
                    catch (Exception ex)
                    {
                        //failureReasons.Add(BaseParameters.BaseXPaths.RatingValuePath);
                    }
                }
            }
            else
            {
                failedXPaths.Add(ConnectionParameters.XPathParameters.ParentNodeExtraValueSummaryXPath);
            }

            try
            {
                string value = string.Empty;
                string key = string.Empty;

                nodes = SelectNodes(ReviewNode, ConnectionParameters.XPathParameters.ParentnodeExtraValueParaXPath, true, null);

                if (nodes != null && nodes.Count > 0)
                {
                    foreach (var htmlExtraValueDIV in nodes)
                    {
                        try
                        {
                            value = htmlExtraValueDIV.InnerText;
                            key = SelectSingleNode(htmlExtraValueDIV, ".//strong", true, null).InnerText;
                            
                            ListkeyvaluePair.Add(new KeyValuePair<string, string>(key, value));
                        }
                        catch (Exception ex)
                        {
                        }

                    }
                }

            }
            catch (Exception ex)
            {
            }


            //foreach (string item in failedXPaths)
            //{
            //    failureReasons.Add(string.Format("Failed XPath: {0}", item));
            //}


            return ListkeyvaluePair;
        }
        #endregion

        /*
		/// <summary>
		/// Get Cars.com Reviews via HTML Scraping
		/// </summary>
		/// <param name="includeDetails">True to include review details, false to skip</param>
		/// <returns></returns>
		public ConnectionStatus GetReviewsHtml(bool includeDetails = true)
		{
			ConnectionStatus returnStatus = ConnectionStatus.InvalidData;

			if (!string.IsNullOrEmpty(ConnectionParameters.XPathParameters.ParentnodeReviewSummaryXPath) && !string.IsNullOrEmpty(ConnectionParameters.XPathParameters.ReviewCountCollectedXPath))
			{

				if (!string.IsNullOrEmpty(ConnectionParameters.HtmlRequestUrl))
				{
					HttpRequestResults = string.Empty;

					ConnectionStatus httpStatus = ConnectorHttpRequest(ConnectionParameters.HtmlRequestUrl);
					if (httpStatus == ConnectionStatus.Success)
					{
						HtmlDocument htmlDocument = new HtmlDocument();
						htmlDocument.OptionFixNestedTags = true;
						htmlDocument.LoadHtml(HttpRequestResults);

						HtmlNode SalesParentNode = SelectSingleNode(htmlDocument.DocumentNode, ConnectionParameters.XPathParameters.ParentnodeReviewSummaryXPath);
						if (SalesParentNode != null)
						{
							SalesReviewCount = 0;
							SalesAverageRating = 0;

							HtmlNode SalesRatingChildNode = SelectSingleNode(SalesParentNode, ConnectionParameters.XPathParameters.RatingCollectedXPath);
							if (SalesRatingChildNode != null)
								SalesAverageRating = SalesRatingChildNode.InnerText.IsNullOptional("0").ToDecimal();

							HtmlNode SalesCountChildNode = SelectSingleNode(SalesParentNode, ConnectionParameters.XPathParameters.ReviewCountCollectedXPath);
							if (SalesCountChildNode != null)
								SalesReviewCount = SalesCountChildNode.GetAttributeValue("content", "0").ToInteger();

							if (SalesCountChildNode != null || SalesRatingChildNode != null)
								returnStatus = ConnectionStatus.Success;
						}

						if (includeDetails)
						{
							HtmlNodeCollection ReviewParentNodes = SelectNodes(htmlDocument.DocumentNode, ConnectionParameters.XPathParameters.ParentnodeReviewDetailsXPath);
							if (ReviewParentNodes != null)
							{
								foreach (HtmlNode reviewParentNode in ReviewParentNodes)
								{
									Review review = new Review();

									review.Rating = 0;
									HtmlNode ReviewRatingNode = SelectSingleNode(reviewParentNode, ConnectionParameters.XPathParameters.RatingValueXPath);
									if (ReviewRatingNode != null)
										review.Rating = ReviewRatingNode.InnerText.IsNullOptional("0").ToDecimal();

									HtmlNode ReviewFullTextNode = SelectSingleNode(reviewParentNode, ConnectionParameters.XPathParameters.ReviewTextFullXPath);
									if (ReviewFullTextNode != null)
										review.ReviewTextFull = ReviewFullTextNode.InnerText.IsNullOptional("").StripHtml();

									HtmlNode ReviewReviewerNode = SelectSingleNode(reviewParentNode, ConnectionParameters.XPathParameters.ReviewerNameXPath);
									if (ReviewReviewerNode != null)
										review.ReviewerName = ReviewReviewerNode.InnerText.IsNullOptional("");

									HtmlNode ReviewReviewDateNode = SelectSingleNode(reviewParentNode, ConnectionParameters.XPathParameters.ReviewDateXPath);
									if (ReviewReviewDateNode != null)
										review.ReviewDate = ReviewReviewDateNode.InnerText.IsNullOptional("").ToDateTime();

									HtmlNodeCollection XtraInfoNodes = SelectNodes(reviewParentNode, ConnectionParameters.XPathParameters.ParentnodeExtraValueXPath);
									if (XtraInfoNodes != null)
									{
										string value = string.Empty;
										string key = string.Empty;

										foreach (HtmlNode xtraNode in XtraInfoNodes)
										{
											HtmlNode XtraValueWrapperNode = SelectSingleNode(xtraNode, ConnectionParameters.XPathParameters.XtraValueWrapperXPath);
											if (XtraValueWrapperNode != null)
											{
												HtmlNode valueNode = SelectSingleNode(xtraNode, ConnectionParameters.XPathParameters.XtraValueNodeXPath);
												if (valueNode != null)
												{
													value = valueNode.GetAttributeValue("title", "");
													HtmlNode keyNode = SelectSingleNode(xtraNode, ConnectionParameters.XPathParameters.XtraKeyNodeXPath);
													if (keyNode != null)
														key = keyNode.InnerText.IsNullOptional("");
												}
												else
												{
													valueNode = SelectSingleNode(xtraNode, ConnectionParameters.XPathParameters.XtraNoWrapperValueNodeXPath);
													if (valueNode != null)
													{
														value = valueNode.InnerText.IsNullOptional("");
														HtmlNode keyNode = SelectSingleNode(xtraNode, ConnectionParameters.XPathParameters.XtraNoWrapperKeyNodeXPath);
														if (keyNode != null)
															key = keyNode.InnerText.IsNullOptional("");
													}
												}

												if (!string.IsNullOrEmpty(key))
													review.ExtraInfo.Add(new KeyValuePair<string, string>(key.Trim(), value));
											}
										}
									}

									HtmlNodeCollection XtraInfoParaNodes = SelectNodes(reviewParentNode, ConnectionParameters.XPathParameters.ParentnodeExtraValueParaXPath);
									if (XtraInfoParaNodes != null)
									{
										string value = string.Empty;
										string key = string.Empty;

										foreach (HtmlNode paraNode in XtraInfoParaNodes)
										{
											value = string.Empty;
											key = paraNode.InnerText.IsNullOptional("");
											HtmlNode valueNode = SelectSingleNode(paraNode, ".//strong");
											if (valueNode != null)
												value = valueNode.InnerText.IsNullOptional("");

											if (!string.IsNullOrEmpty(key))
												review.ExtraInfo.Add(new KeyValuePair<string, string>(key.Replace(value, "").Trim(), value));

										}
									}

									ReviewDetails.Add(review);
								}
							}
						}
					}
					else
						returnStatus = httpStatus;
				}
			}

			return returnStatus;
		}
		*/
    }
}
