﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Connectors.Parameters;
using Connectors.Reviews;
using HtmlAgilityPack;
using SI.DTO;
using SI.Extensions;
using SI;

namespace Connectors
{
    public class JudysBookReviewsConnection : SIReviewConnection
    {
        private SIJudysBookReviewConnectionParameters ConnectionParameters;

        #region Initializer
        public JudysBookReviewsConnection(SIJudysBookReviewConnectionParameters siJudysBookReviewConnectionParameters)
            : base(siJudysBookReviewConnectionParameters)
        {
            ConnectionParameters = siJudysBookReviewConnectionParameters;
        }
        #endregion

        //protected override DownloadDataBucket DoDownload(ReviewUrlDTO url)
        //{
        //    DownloadDataBucket myBucket = new DownloadDataBucket();

        //    myBucket.HtmlDocument = ConnectorHtmlDocument(url.HtmlURL, null);

        //    return myBucket;
        //}

        protected override SI.DTO.ReviewUrlDTO TweakUrl(SI.DTO.ReviewUrlDTO urlDTO)
        {
            return urlDTO;
        }

        protected override bool CheckHasCheckNoReviews(DownloadDataBucket bucket, List<string> failureReasons)
        {
            HtmlDocument doc = bucket.HtmlDocument;

            bool result = false;

            HtmlNode node = SelectSingleNode(doc.DocumentNode, BaseParameters.BaseXPaths.NoReviewPath, true, null);

            if (node != null)
            {
                if (!string.IsNullOrWhiteSpace(BaseParameters.BaseXPaths.NoReviewText))
                {
                    if (string.IsNullOrWhiteSpace(node.InnerText))
                    {
                        result = false;
                    }
                    else
                    {
                        if (node.InnerText.Trim().ToLower().Equals(BaseParameters.BaseXPaths.NoReviewText.Trim().ToLower()))
                        {
                            result = true;
                        }
                    }
                }
            }

            return result;
        }

        protected override int ScrapeReviewCount(DownloadDataBucket bucket, List<string> failureReasons)
        {
            HtmlDocument doc = bucket.HtmlDocument;

            int reviewCount = 0;

            HtmlNode node = SelectSingleNode(doc.DocumentNode, BaseParameters.BaseXPaths.ReviewCountPath, true, null);
            if (node != null)
            {
                if (!string.IsNullOrWhiteSpace(node.InnerText))
                {
                    int.TryParse(node.InnerText, out reviewCount);
                }
            }

            return reviewCount;
        }

        protected override bool CheckHasNoServiceReviews(DownloadDataBucket bucket, List<string> failureReasons)
        {
            return true;
        }

        protected override int ScrapeServiceReviewCount(DownloadDataBucket bucket, List<string> failureReasons)
        {
            return 0;
        }

        protected override bool CheckHasNoRating(DownloadDataBucket bucket, List<string> failureReasons)
        {
            HtmlDocument doc = bucket.HtmlDocument;

            bool result = false;

            HtmlNode node = SelectSingleNode(doc.DocumentNode, BaseParameters.BaseXPaths.NoRatingPath, true, null);

            if (node != null)
            {
                if (!string.IsNullOrWhiteSpace(BaseParameters.BaseXPaths.NoRatingText))
                {
                    if (string.IsNullOrWhiteSpace(node.InnerText))
                    {
                        result = false;
                    }
                    else
                    {
                        //string rating = node.InnerText.Trim().Replace("Rated:", "").Trim();
                        if (node.InnerText.Trim().ToLower().Contains(BaseParameters.BaseXPaths.NoRatingText.Trim().ToLower()))
                        {
                            result = true;
                        }
                    }
                }
            }

            return result;
        }

        protected override decimal ScrapeRating(DownloadDataBucket bucket, List<string> failureReasons)
        {
            HtmlDocument doc = bucket.HtmlDocument;

            decimal rating = 0;

            HtmlNode node = SelectSingleNode(doc.DocumentNode, BaseParameters.BaseXPaths.RatingValuePath, true, null);
            if (node != null)
            {
                if (!string.IsNullOrWhiteSpace(BaseParameters.BaseXPaths.RatingValuePath))
                {
                    //string strrating = node.InnerText.Trim().Replace("Rated:", "").Trim();
                    if (!string.IsNullOrWhiteSpace(node.InnerText.Trim()))
                    {
                        string rtext = node.InnerText.Trim().ToLower();
                        decimal.TryParse(rtext, out rating);
                    }
                }
            }


            return rating;
        }

        protected override bool CheckHasNoServiceRating(DownloadDataBucket bucket, List<string> failureReasons)
        {
            return true;
        }

        protected override decimal ScrapeServiceRating(DownloadDataBucket bucket, List<string> failureReasons)
        {
            return 0;
        }

        protected override List<KeyValuePair<string, string>> ScrapeExtraInfo(DownloadDataBucket bucket, List<string> failureReasons)
        {
            return null;
        }

        protected override List<ReviewDownloadResult.ReviewDetail> ScrapeDetails(DownloadDataBucket bucket, ReviewUrlDTO originalUrl, List<string> failureReasons)
        {
            HtmlDocument headerDoc = bucket.HtmlDocument;
            string ReviewURL = string.Empty;
            List<Connectors.ReviewDownloadResult.ReviewDetail> results = new List<ReviewDownloadResult.ReviewDetail>();
            List<string> failedXPaths = new List<string>();

            HtmlDocument currentDoc = headerDoc;

            int numberOfAttemptsToMakeForEachPage = 2;

            if (originalUrl.HtmlURL.Contains("cities/"))
            {
                ReviewURL = originalUrl.HtmlURL;
            }
            else
            {
                ReviewURL = GetReviewURL(headerDoc);
            }
             

            if (!string.IsNullOrWhiteSpace(ReviewURL))
            {
                currentDoc = DownloadDocument(ReviewURL, null, numberOfAttemptsToMakeForEachPage);
                scrapeDetailsLocal(currentDoc, results, failedXPaths, ReviewURL);
            }
            else
            {
                string originalUrlPath = originalUrl.HtmlURL;
                scrapeDetailsLocal(currentDoc, results, failedXPaths, originalUrlPath);
            }

            //scrapeDetailsLocal(headerDoc, results, failedXPaths, ReviewURL);
            int pagesScraped = 1;

            HtmlNode nextLink = SelectSingleNode(currentDoc.DocumentNode, "//a[contains(@class,'PagerHyperlinkStyle') and contains(text(),'Next')]");
            while (nextLink != null)
            {
                string nextURL = "http://www.judysbook.com/" + nextLink.GetAttributeValue("href", "").Substring(12);
                currentDoc = DownloadDocument(nextURL, null, numberOfAttemptsToMakeForEachPage);

                if (currentDoc == null)
                {
                    failureReasons.Add(string.Format("Failed to download page {0} at url {1}, tried {2} time(s)",
                        pagesScraped + 1, nextURL, numberOfAttemptsToMakeForEachPage));
                }

                scrapeDetailsLocal(currentDoc, results, failedXPaths, ReviewURL);

                nextLink = SelectSingleNode(currentDoc.DocumentNode, "//a[contains(@class,'PagerHyperlinkStyle') and contains(text(),'Next')]");

                pagesScraped++;
                if (pagesScraped > 200) break;
            }
            return results;

        }

        protected override void dispose()
        {

        }

        #region private Methods
        private string GetReviewURL(HtmlDocument doc)
        {
            string url = string.Empty;
            try
            {
                HtmlNode node = SelectSingleNode(doc.DocumentNode, ConnectionParameters.XPathParameters.ReviewURLXPath, true, null);
                if (node != null)
                {
                    url = "http://www.judysbook.com/" + node.GetAttributeValue("href", "");
                }
            }
            catch (Exception ex)
            {
            }

            return url;
        }

        private void scrapeDetailsLocal(HtmlDocument headerDoc, List<Connectors.ReviewDownloadResult.ReviewDetail> results, List<string> failedXPaths, string ReviewURL)
        {
            HtmlNodeCollection details = SelectNodes(headerDoc.DocumentNode, BaseParameters.BaseXPaths.ReviewDetailIteratorPath, true, failedXPaths);

            if (details != null && details.Count() > 0)
            {
                foreach (HtmlNode node in details)
                {
                    try
                    {
                        string fullreviewurl = ScrapFullReviewURL(node);

                        HtmlDocument currentDoc = DownloadDocument(fullreviewurl, null, 2);

                        if (currentDoc == null)
                        {
                            currentDoc = DownloadDocument(fullreviewurl, null, 2);
                        }

                        Connectors.ReviewDownloadResult.ReviewDetail detail = scrapeSingleReviewDetail(node, currentDoc, failedXPaths);
                        if (detail != null)
                        {
                            results.Add(detail);
                        }
                    }
                    catch (Exception ex)
                    {

                    }
                }
            }
        }

        private string ScrapFullReviewURL(HtmlNode Reviewnode)
        {
            string fullreviewurl = string.Empty;

            HtmlNode node = SelectSingleNode(Reviewnode, ConnectionParameters.XPathParameters.FullReviewURLXPath);
            if (node != null)
            {
                fullreviewurl = "http://www.judysbook.com/" + node.GetAttributeValue("href", "").Substring(12);
            }

            return fullreviewurl;

        }

        private Connectors.ReviewDownloadResult.ReviewDetail scrapeSingleReviewDetail(HtmlNode node, HtmlDocument currentDoc, List<string> failedXPaths)
        {
            Connectors.ReviewDownloadResult.ReviewDetail review = new Connectors.ReviewDownloadResult.ReviewDetail();
            HtmlNode FullReivewParentNode = SelectSingleNode(currentDoc.DocumentNode, ConnectionParameters.XPathParameters.ReviewTextFullParentXPath);

            #region Rating
            review.Rating = 0;
            try
            {
                HtmlNode node2 = SelectSingleNode(FullReivewParentNode, ConnectionParameters.XPathParameters.ReviewRatingValuePath);
                if (node2 != null)
                {
                    if (!string.IsNullOrWhiteSpace(node2.GetAttributeValue("value", "")))
                    {
                        string rtext = node2.GetAttributeValue("value", "").ToLower().Trim();
                        decimal dValue;
                        if (decimal.TryParse(rtext, out dValue))
                            review.Rating = dValue;
                    }
                }
            }
            catch (Exception ex)
            {
            }
            #endregion

            #region ReviewerName
            review.ReviewerName = string.Empty;
            try
            {
                HtmlNode node2 = SelectSingleNode(FullReivewParentNode, ConnectionParameters.XPathParameters.ReviewerNameXPath);
                if (node2 != null)
                {
                    if (!string.IsNullOrWhiteSpace(node2.InnerText))
                    {
                        string rtext = node2.InnerText.Trim();
                        review.ReviewerName = rtext;
                    }
                }
                else
                {
                    failedXPaths.Add(ConnectionParameters.XPathParameters.ReviewerNameXPath);
                }
            }
            catch (Exception ex)
            {
            }
            #endregion

            #region ReviewDate
            review.ReviewDate = SIDateTime.MinValue;
            try
            {
                if (FullReivewParentNode != null)
                {
                    HtmlNode node2 = SelectSingleNode(FullReivewParentNode, ConnectionParameters.XPathParameters.ReviewDateXPath);
                    if (node2 != null)
                    {
                        if (!string.IsNullOrWhiteSpace(node2.InnerText))
                        {
                            string rtext = node2.InnerText.Trim();
                            DateTime rDate;
                            if (DateTime.TryParse(rtext, out rDate))
                            {
                                review.ReviewDate = new SI.SIDateTime(rDate, 0);
                            }
                        }
                    }
                    else
                    {
                        failedXPaths.Add(ConnectionParameters.XPathParameters.ReviewDateXPath);
                    }
                }
            }
            catch (Exception ex)
            {

            }
            #endregion

            #region ReviewTextFull
            review.ReviewTextFull = string.Empty;
            try
            {
                HtmlNode node2 = SelectSingleNode(FullReivewParentNode, ConnectionParameters.XPathParameters.ReviewTextFullXPath);
                if (node2 != null)
                {
                    if (!string.IsNullOrWhiteSpace(node2.InnerText))
                    {
                        string rtext = node2.InnerText.StripHtml().Trim();
                        review.ReviewTextFull = rtext;
                    }
                }
                else
                {
                    failedXPaths.Add(ConnectionParameters.XPathParameters.ReviewTextFullXPath);
                }
            }
            catch (Exception ex)
            {

            }
            #endregion

            /*
            #region ReviewTextFull
            review.ReviewTextFull = string.Empty;
            try
            {
                HtmlNode node2 = SelectSingleNode(node, ConnectionParameters.XPathParameters.ReviewTextFullXPath);
                if (node2 != null)
                {
                    if (!string.IsNullOrWhiteSpace(node2.InnerText))
                    {
                        string rtext = node2.InnerText.StripHtml().Trim();
                        review.ReviewTextFull = rtext;
                    }
                }
                else
                {
                    failedXPaths.Add(ConnectionParameters.XPathParameters.ReviewTextFullXPath);
                }
            }
            catch (Exception ex)
            {

            }
            #endregion

            #region ExtraValue
            review.ExtraInfo = null;
            try
            {
                review.ExtraInfo = scrapeReviewExtraInfo(node);
            }
            catch (Exception ex)
            {

            }
            #endregion

            #region Reply/Comment

            review.Comments = new List<ReviewDownloadResult.ReviewComment>();
            try
            {
                review.Comments = scrapeReviewComments(node);
            }
            catch (Exception ex)
            {
            }
            #endregion

            */
            return review;


        }
        #endregion

        /*
		/// <summary>
		/// 
		/// </summary>
		/// <param name="includeDetails"></param>
		/// <returns></returns>
		public ConnectionStatus GetReviewsHtml(bool includeDetails = true)
		{
			ConnectionStatus returnStatus = ConnectionStatus.InvalidData;

			if (!string.IsNullOrEmpty(ConnectionParameters.HtmlRequestUrl))
			{
				HttpRequestResults = string.Empty;

				ConnectionStatus httpStatus = ConnectorHttpRequest(ConnectionParameters.HtmlRequestUrl);
				if (httpStatus == ConnectionStatus.Success)
				{
					HtmlDocument htmlDocument = new HtmlDocument();
					htmlDocument.OptionFixNestedTags = true;
					htmlDocument.LoadHtml(HttpRequestResults);

					HtmlNode ReviewParentNode = SelectSingleNode(htmlDocument.DocumentNode, ConnectionParameters.XPathParameters.ParentnodeReviewSummaryXPath);
					if (ReviewParentNode != null)
					{
						SalesReviewCount = 0;
						SalesAverageRating = 0;

						HtmlNode SalesCountChildNode = SelectSingleNode(ReviewParentNode, ConnectionParameters.XPathParameters.ReviewCountCollectedXPath);
						if (SalesCountChildNode != null)
							SalesReviewCount = SalesCountChildNode.InnerText.IsNullOptional("0").Trim().ToInteger();

						HtmlNode SalesRatingNode = SelectSingleNode(ReviewParentNode, ConnectionParameters.XPathParameters.RatingCollectedXPath);
						if (SalesRatingNode != null)
							SalesAverageRating = SalesRatingNode.GetAttributeValue("value", "0").Trim().ToDecimal();

						returnStatus = ConnectionStatus.Success;

						if (includeDetails && SalesReviewCount > 0)
						{
							int numberOfPages = SalesReviewCount / ConnectionParameters.XPathParameters.ReviewsPerPage;
							if ((SalesReviewCount % ConnectionParameters.XPathParameters.ReviewsPerPage) > 0)
								numberOfPages++;

							if (numberOfPages == 0)
								numberOfPages = 1;

							Uri requestUri = new Uri(ConnectionParameters.HtmlRequestUrl);
							string pageName = requestUri.Segments[requestUri.Segments.Length - 1];

							HtmlDocument htmlReviewDocument = new HtmlDocument();
							htmlReviewDocument.OptionFixNestedTags = true;

							for (int pageCount = 0; pageCount < numberOfPages; pageCount++)
							{
								Console.WriteLine(pageCount);

								if (pageCount > 0)
								{
									string pageRequestUrl = string.Format("{0}p{1}/t1/{2}", ConnectionParameters.HtmlRequestUrl.Replace(pageName, ""), pageCount + 1, pageName);
									if (ConnectorHttpRequest(pageRequestUrl) != ConnectionStatus.Success) break;
									htmlDocument.LoadHtml(HttpRequestResults);
								}

								HtmlNodeCollection ReviewNodes = SelectNodes(htmlDocument.DocumentNode, ConnectionParameters.XPathParameters.ParentNodeReviewDetailsXPath);
								if (ReviewNodes != null)
								{
									foreach (HtmlNode reviewNode in ReviewNodes)
									{
										Review review = new Review();
										review.Type = "sales";

										//drill down to full review
										HtmlNode ReviewFullNode = SelectSingleNode(reviewNode, ConnectionParameters.XPathParameters.FullReviewURLXPath);
										if (ReviewFullNode != null)
										{
											string FullReviewUrl = "";
											string hrefValue = ReviewFullNode.GetAttributeValue("href", "");
											if (!string.IsNullOrEmpty(hrefValue) && hrefValue.Contains("members"))
												FullReviewUrl = string.Format("http://www.judysbook.com/{0}", hrefValue.Substring(hrefValue.IndexOf("members")));

											if (!string.IsNullOrEmpty(FullReviewUrl))
											{
												if (ConnectorHttpRequest(FullReviewUrl) != ConnectionStatus.Success) break;
												htmlReviewDocument.LoadHtml(HttpRequestResults);

												HtmlNode ReviewFullParentNode = SelectSingleNode(htmlReviewDocument.DocumentNode, ConnectionParameters.XPathParameters.ReviewTextFullParentXPath);
												if (ReviewFullParentNode != null)
												{
													review.Rating = 5; //their default is a 5 not 0
													HtmlNode RatingNode = ReviewFullParentNode.SelectSingleNode(ConnectionParameters.XPathParameters.RatingValueXPath); //get this without tracking because not on all reviews (stupid I know)
													if (RatingNode != null)
														review.Rating = RatingNode.GetAttributeValue("value", "5").Trim().ToDecimal();

													HtmlNode ReviewerNameNode = SelectSingleNode(ReviewFullParentNode, ConnectionParameters.XPathParameters.ReviewerNameXPath);
													if (ReviewerNameNode != null)
													{
														string reviewerName = ReviewerNameNode.InnerText.IsNullOptional("");
														review.ReviewerName = reviewerName;
														if (!reviewerName.ToLower().Contains("guest"))
															review.ReviewerName = reviewerName.Substring(0, reviewerName.ToLower().IndexOf(" at")).Replace("by ", "");
													}

													HtmlNode ReviewDateNode = SelectSingleNode(ReviewFullParentNode, ConnectionParameters.XPathParameters.ReviewDateXPath);
													if (ReviewDateNode != null)
														review.ReviewDate = ReviewDateNode.InnerText.Trim().ToDateTime();

													HtmlNode ReviewTextNode = SelectSingleNode(ReviewFullParentNode, ConnectionParameters.XPathParameters.ReviewTextFullXPath);
													if (ReviewTextNode != null)
														review.ReviewTextFull = ReviewTextNode.InnerText.IsNullOptional("").StripHtml();

													review.FullReviewURL = FullReviewUrl;

													ReviewDetails.Add(review);
												}
											}
										}
									}
								}
							}
						}
					}
				}
			}

			return returnStatus;
		}

		protected override void dispose()
		{
			
		}

        */
    }
}
