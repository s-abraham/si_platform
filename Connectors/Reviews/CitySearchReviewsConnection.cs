﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Connectors.Parameters;
using Connectors.Reviews;
using HtmlAgilityPack;
using SI.DTO;
using SI.Extensions;
using SI;
using System.Text.RegularExpressions;

namespace Connectors
{
    public class CitySearchReviewsConnection : SIReviewConnection
    {
        private SICitySearchReviewConnectionParameters ConnectionParameters;

        #region Initializer
        public CitySearchReviewsConnection(SICitySearchReviewConnectionParameters siCitySearchReviewConnectionParameters)
            : base(siCitySearchReviewConnectionParameters)
        {
            ConnectionParameters = siCitySearchReviewConnectionParameters;
        }
        #endregion

        #region Protected Methods
        protected override DownloadDataBucket DoDownload(ReviewUrlDTO url, List<string> failureReasons)
        {
            var bucket = new DownloadDataBucket();

            if (!string.IsNullOrEmpty(url.HtmlURL.Trim()))
            {
                HtmlDocument doc = DownloadDocument(url.HtmlURL, null, 3, 2000, "Mozilla/5.0 (Windows NT 6.2; WOW64; rv:23.0) Gecko/20100101 Firefox/23.0");
                // if html fails, always fail with a null return. no html header, no go
                if (doc == null) return null;
                bucket = new DownloadDataBucket() { HtmlDocument = doc };
            }
            
            return bucket;
        }

        protected override ReviewUrlDTO TweakUrl(ReviewUrlDTO urlDTO)
        {
            return urlDTO;
        }

        protected override bool CheckHasCheckNoReviews(DownloadDataBucket bucket, List<string> failureReasons)
        {
            HtmlDocument doc = bucket.HtmlDocument;

            bool result = false;

            HtmlNode node = SelectSingleNode(doc.DocumentNode, BaseParameters.BaseXPaths.NoReviewPath, true, null);

            if (node != null)
            {
                if (!string.IsNullOrWhiteSpace(BaseParameters.BaseXPaths.NoReviewText))
                {
                    if (string.IsNullOrWhiteSpace(node.InnerText))
                    {
                        result = false;
                    }
                    else
                    {
                        if (node.InnerText.Trim().ToLower().Contains(BaseParameters.BaseXPaths.NoReviewText.Trim().ToLower()))
                        {
                            result = true;
                        }
                    }
                }
            }

            return result;

        }

        protected override int ScrapeReviewCount(DownloadDataBucket bucket, List<string> failureReasons)
        {
            HtmlDocument doc = bucket.HtmlDocument;

            int reviewCount = 0;


            HtmlNode node = SelectSingleNode(doc.DocumentNode, BaseParameters.BaseXPaths.ReviewCountPath, true, null);
            if (node != null)
            {
                if (!string.IsNullOrWhiteSpace(node.InnerText))
                {
                    var resultString = Regex.Match(node.InnerText, @"\d+").Value;
                    int.TryParse(resultString, out reviewCount);
                }
            }
            else
            {
                node = SelectSingleNode(doc.DocumentNode, "//div[@id='navButtons']//a[1]/span", true, null);
                if (node != null)
                {
                    if (!string.IsNullOrWhiteSpace(node.InnerText))
                    {
                        int.TryParse(node.InnerText.Trim(), out reviewCount);
                    }
                }

            }

            return reviewCount;


        }

        protected override bool CheckHasNoServiceReviews(DownloadDataBucket bucket, List<string> failureReasons)
        {
            return true;
        }

        protected override int ScrapeServiceReviewCount(DownloadDataBucket bucket, List<string> failureReasons)
        {
            return 0;
        }

        protected override bool CheckHasNoRating(DownloadDataBucket bucket, List<string> failureReasons)
        {
            HtmlDocument doc = bucket.HtmlDocument;

            bool result = false;

            HtmlNode node = SelectSingleNode(doc.DocumentNode, BaseParameters.BaseXPaths.NoRatingPath, true, null);
            if (node != null)
            {
                if (!string.IsNullOrWhiteSpace(BaseParameters.BaseXPaths.NoRatingText))
                {
                    if (string.IsNullOrWhiteSpace(node.InnerText))
                    {
                        result = false;
                    }
                    else
                    {
                        if (node.InnerText.Trim().ToLower().Contains(BaseParameters.BaseXPaths.NoRatingText.Trim().ToLower()))
                        {
                            result = true;
                        }
                    }
                }
            }

            return result;
        }

        protected override decimal ScrapeRating(DownloadDataBucket bucket, List<string> failureReasons)
        {
            HtmlDocument doc = bucket.HtmlDocument;

            decimal rating = 0;

            HtmlNode node = SelectSingleNode(doc.DocumentNode, BaseParameters.BaseXPaths.RatingValuePath, true, null);
            if (node != null)
            {
                if (!string.IsNullOrWhiteSpace(BaseParameters.BaseXPaths.RatingValuePath))
                {
                    if (!string.IsNullOrWhiteSpace(node.InnerText))
                    {
                        string rtext = Convert.ToString(Convert.ToDecimal((Convert.ToDecimal(node.InnerText.ToLower()) / 100) * 5));
                        decimal.TryParse(rtext, out rating);
                    }
                }
            }

            return rating;
        }

        protected override bool CheckHasNoServiceRating(DownloadDataBucket bucket, List<string> failureReasons)
        {
            return false;
        }

        protected override decimal ScrapeServiceRating(DownloadDataBucket bucket, List<string> failureReasons)
        {
            return 0;
        }

        protected override List<KeyValuePair<string, string>> ScrapeExtraInfo(DownloadDataBucket bucket, List<string> failureReasons)
        {
            HtmlDocument doc = bucket.HtmlDocument;

            List<KeyValuePair<string, string>> ListkeyvaluePair = new List<KeyValuePair<string, string>>();

            return ListkeyvaluePair;
        }

        protected override List<Connectors.ReviewDownloadResult.ReviewDetail> ScrapeDetails(DownloadDataBucket bucket, ReviewUrlDTO originalUrl, List<string> failureReasons)
        {
            HtmlDocument headerDoc = bucket.HtmlDocument;

            List<Connectors.ReviewDownloadResult.ReviewDetail> results = new List<ReviewDownloadResult.ReviewDetail>();
            List<string> failedXPaths = new List<string>();

            scrapeDetailsLocal(headerDoc, results, failedXPaths);

            HtmlDocument currentDoc = headerDoc;
            int numberOfAttemptsToMakeForEachPage = 2;
            int pagesScraped = 1;

            HtmlNode nextLink = SelectSingleNode(currentDoc.DocumentNode, ".//a[@id='seeMoreTips']");
            while (nextLink != null)
            {
                string nextURL = nextLink.GetAttributeValue("href", "").Replace("&amp;", "&");
                currentDoc = DownloadDocument(nextURL, null, numberOfAttemptsToMakeForEachPage);

                if (currentDoc == null)
                {
                    failureReasons.Add(string.Format("Failed to download page {0} at url {1}, tried {2} time(s)",
                        pagesScraped + 1, nextURL, numberOfAttemptsToMakeForEachPage));
                    break;
                }

                scrapeDetailsLocal(currentDoc, results, failedXPaths);

                nextLink = SelectSingleNode(currentDoc.DocumentNode, ".//a[@id='seeMoreTips']");
                pagesScraped++;

                if (pagesScraped > 200) break;
            }
            return results;
        }

        protected override void dispose()
        {
            // nothing to do here
        }

        #endregion

        #region private Methods

        private void scrapeDetailsLocal(HtmlDocument headerDoc, List<Connectors.ReviewDownloadResult.ReviewDetail> results, List<string> failedXPaths)
        {
            HtmlNodeCollection details = SelectNodes(headerDoc.DocumentNode, BaseParameters.BaseXPaths.ReviewDetailIteratorPath, true, failedXPaths);

            if (details != null && details.Count() > 0)
            {
                foreach (HtmlNode node in details)
                {
                    try
                    {
                        Connectors.ReviewDownloadResult.ReviewDetail detail = scrapeSingleReviewDetail(node, failedXPaths);
                        if (detail != null)
                        {
                            results.Add(detail);
                        }
                    }
                    catch (Exception ex)
                    {

                    }
                }
            }
        }

        private Connectors.ReviewDownloadResult.ReviewDetail scrapeSingleReviewDetail(HtmlNode node, List<string> failedXPaths)
        {
            Connectors.ReviewDownloadResult.ReviewDetail review = new Connectors.ReviewDownloadResult.ReviewDetail();

            #region Rating
            review.Rating = 0;
            try
            {
                HtmlNode node2 = SelectSingleNode(node, ConnectionParameters.XPathParameters.ReviewRatingPath);
                if (node2 != null)
                {
                    if (!string.IsNullOrWhiteSpace(node2.InnerText))
                    {
                        string rtext = node2.InnerText.ToLower().Trim();
                        decimal dValue;
                        if (rtext.ToLower().Trim() == "Doesn't Recommend".ToLower())
                        {
                            review.Rating = 1;
                        }
                        else if (rtext.ToLower().Trim() == "Recommends".ToLower())
                        {
                            review.Rating = 5;
                        }
                        if (decimal.TryParse(rtext, out dValue))
                            review.Rating = dValue;
                    }
                }
                else
                {
                    review.Rating = 0;
                }


            }
            catch (Exception ex)
            {
            }
            #endregion

            #region ReviewerName
            review.ReviewerName = string.Empty;
            try
            {
                HtmlNode node2 = SelectSingleNode(node, ConnectionParameters.XPathParameters.ReviewerNameXPath);
                if (node2 != null)
                {
                    if (!string.IsNullOrWhiteSpace(node2.InnerText))
                    {
                        string rtext = node2.InnerText.Trim();
                        review.ReviewerName = rtext;
                    }
                }
                else
                {
                    failedXPaths.Add(ConnectionParameters.XPathParameters.ReviewerNameXPath);
                }
            }
            catch (Exception ex)
            {
            }
            #endregion

            #region ReviewDate
            review.ReviewDate = SIDateTime.MinValue;
            try
            {
                HtmlNode node2 = SelectSingleNode(node, ConnectionParameters.XPathParameters.ReviewDateXPath);
                if (node2 != null)
                {
                    if (!string.IsNullOrWhiteSpace(node2.OuterHtml))
                    {
                        string rtext = node2.GetAttributeValue("title", "");
                        DateTime rDate;
                        if (DateTime.TryParse(rtext, out rDate))
                        {
                            review.ReviewDate = new SIDateTime(rDate, TimeZoneInfo.Utc);
                        }
                    }
                }
                else
                {
                    failedXPaths.Add(ConnectionParameters.XPathParameters.ReviewDateXPath);
                }
            }
            catch (Exception ex)
            {

            }
            #endregion

            #region ReviewTextFull
            review.ReviewTextFull = string.Empty;
            try
            {
                HtmlNode node2 = SelectSingleNode(node, ConnectionParameters.XPathParameters.ReviewTextFullXPath);
                if (node2 != null)
                {
                    if (!string.IsNullOrWhiteSpace(node2.InnerText))
                    {
                        string rtext = node2.InnerText.StripHtml().Trim();
                        review.ReviewTextFull = rtext;
                    }
                }
                else
                {
                    failedXPaths.Add(ConnectionParameters.XPathParameters.ReviewTextFullXPath);
                }
            }
            catch (Exception ex)
            {

            }
            #endregion

            return review;


        }


        #endregion

        /*
		public ConnectionStatus GetReviewsHtml(bool includeDetails = true)
		{
			ConnectionStatus returnStatus = ConnectionStatus.InvalidData;

			if (!string.IsNullOrEmpty(ConnectionParameters.HtmlRequestUrl))
			{
				HttpRequestResults = string.Empty;

				ConnectionStatus httpStatus = ConnectorHttpRequest(ConnectionParameters.HtmlRequestUrl);
				if (httpStatus == ConnectionStatus.Success)
				{
					HtmlDocument htmlDocument = new HtmlDocument();
					htmlDocument.OptionFixNestedTags = true;
					htmlDocument.LoadHtml(HttpRequestResults);

					HtmlNode ReviewParentNode = SelectSingleNode(htmlDocument.DocumentNode, ConnectionParameters.XPathParameters.ParentnodeReviewSummaryXPath);
					if (ReviewParentNode != null)
					{
						SalesReviewCount = 0;
						SalesAverageRating = 0;

						HtmlNode SalesCountChildNode = SelectNodes(ReviewParentNode, ConnectionParameters.XPathParameters.ReviewCountCollectedXPath).FirstOrDefault();
						if (SalesCountChildNode != null)
							SalesReviewCount = SalesCountChildNode.InnerText.IsNullOptional("0").Trim().Replace("(","").Replace(")","").ToInteger();

						if (SalesReviewCount > 0)
						{
							int numberOfPages = SalesReviewCount / ConnectionParameters.XPathParameters.ReviewsPerPage;
							if ((SalesReviewCount % ConnectionParameters.XPathParameters.ReviewsPerPage) > 0)
								numberOfPages++;

							if (numberOfPages == 0)
								numberOfPages = 1;

							for (int pageCount = 0; pageCount < numberOfPages; pageCount++)
							{
								if (pageCount > 0)
								{
									string pageRequestUrl = string.Format("{0}?page={1}", ConnectionParameters.HtmlRequestUrl, pageCount + 1);
									if (ConnectorHttpRequest(pageRequestUrl) != ConnectionStatus.Success) break;
									htmlDocument.LoadHtml(HttpRequestResults);
								}

								HtmlNodeCollection ReviewNodes = SelectNodes(htmlDocument.DocumentNode, ConnectionParameters.XPathParameters.ParentNodeReviewDetailsXPath);
								if (ReviewNodes != null)
								{
									foreach (HtmlNode reviewNode in ReviewNodes)
									{
										Review review = new Review();

										review.Rating = 1;
										HtmlNode RatingNode = SelectSingleNode(reviewNode, ConnectionParameters.XPathParameters.RatingValueXPath);
										if (RatingNode != null)
											review.Rating = 5;
										SalesAverageRating += review.Rating;

										HtmlNode ReviewerNameNode = SelectSingleNode(reviewNode, ConnectionParameters.XPathParameters.ReviewerNameXPath);
										if (ReviewerNameNode != null)
											review.ReviewerName = ReviewerNameNode.InnerText.IsNullOptional("");

										HtmlNode ReviewDateNode = SelectSingleNode(reviewNode, ConnectionParameters.XPathParameters.ReviewDateXPath);
										if (ReviewDateNode != null)
											review.ReviewDate = ReviewDateNode.GetAttributeValue("title", "").ToDateTime();

										HtmlNode ReviewTextNode = SelectSingleNode(reviewNode, ConnectionParameters.XPathParameters.ReviewTextFullXPath);
										if (ReviewTextNode != null)
											review.ReviewTextFull = ReviewTextNode.InnerText.IsNullOptional("").StripHtml();

										ReviewDetails.Add(review);
									}
								}
							}

							SalesReviewCount = ReviewDetails.Count;
							SalesAverageRating = SalesAverageRating / SalesReviewCount; //rounding? 
						}

						returnStatus = ConnectionStatus.Success;
					}
				}
				else
					returnStatus = httpStatus;
			}

			return returnStatus;
		}

        */

    }
}
