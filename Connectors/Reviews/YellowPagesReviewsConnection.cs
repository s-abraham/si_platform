﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Security.Policy;
using System.Text;
using System.Threading.Tasks;
using System.Web.Security;
using Connectors.Parameters;
using HtmlAgilityPack;
using SI.Extensions;
using System.Threading;
using SI.DTO;
using Connectors.Reviews;
using System.Text.RegularExpressions;
using Newtonsoft.Json;

namespace Connectors
{
    public class YellowPagesReviewsConnection : SIReviewConnection
    {
        private SIYellowPagesReviewConnectionParameters Parameters;

        #region Initializer

        public YellowPagesReviewsConnection(SIYellowPagesReviewConnectionParameters siYellowPagesReviewConnectionParameters)
            : base(siYellowPagesReviewConnectionParameters)
        {
            Parameters = siYellowPagesReviewConnectionParameters;
        }

        #endregion


        protected override DownloadDataBucket DoDownload(ReviewUrlDTO url, List<string> failureReasons)
        {
            var bucket = new DownloadDataBucket();

            if (!string.IsNullOrEmpty(url.HtmlURL.Trim()))
            {
                HtmlDocument doc = DownloadDocument(url.HtmlURL, null, 3, 2000, "Mozilla/5.0 (Windows NT 6.2; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/31.0.1650.63 Safari/537.36");
                // if html fails, always fail with a null return. no html header, no go
                if (doc == null) return null;
                bucket = new DownloadDataBucket() { HtmlDocument = doc };
            }

            return bucket;
        }

        protected override ReviewUrlDTO TweakUrl(ReviewUrlDTO urlDTO)
        {
            //string url = urlDTO.HtmlURL;
            //string[] uparts = url.Split(new char[] { '?' }, StringSplitOptions.RemoveEmptyEntries);
            //if (uparts.Length == 2)
            //{
            //    if (!uparts[0].ToLower().EndsWith("reviews"))
            //    {
            //        urlDTO.HtmlURL = string.Format("{0}/reviews?{1}", uparts[0], uparts[1]);
            //    }
            //}
            return urlDTO;
        }

        protected override bool CheckHasCheckNoReviews(DownloadDataBucket bucket, List<string> failureReasons)
        {
            HtmlDocument doc = bucket.HtmlDocument;
            bool result = true; 
            HtmlNode node;

            //Find first review article.
            if (bucket.Country == "CA")
            {
                node = SelectSingleNode(doc.DocumentNode, Parameters.XPathParameters.CA_NoReviewTextPath, true, null);
            }
            else
            {
                node = SelectSingleNode(doc.DocumentNode, BaseParameters.BaseXPaths.NoReviewPath, true, null);
            }
            
            if (node != null)
            {
                result = false;
            }

            return result;

        }

        protected override int ScrapeReviewCount(DownloadDataBucket bucket, List<string> failureReasons)
        {

            HtmlDocument doc = bucket.HtmlDocument;
            int reviewCount = 0;
            HtmlNode node;

            if (bucket.Country == "CA")
            {
                node = SelectSingleNode(doc.DocumentNode, Parameters.XPathParameters.CA_ReviewCountPath, true, null);
            }
            else
            {
                node = SelectSingleNode(doc.DocumentNode, BaseParameters.BaseXPaths.ReviewCountPath, true, null);
            }
            
            if (node != null)
            {
                if (!string.IsNullOrWhiteSpace(node.InnerText))
                {
                    var temp = Regex.Replace(node.InnerText, "[^0-9]+", string.Empty);
                    int.TryParse(temp, out reviewCount);
                }
            }

            return reviewCount;

        }

        protected override bool CheckHasNoServiceReviews(DownloadDataBucket bucket, List<string> failureReasons)
        {
            return true;
        }

        protected override int ScrapeServiceReviewCount(DownloadDataBucket bucket, List<string> failureReasons)
        {
            return 0;
        }

        protected override bool CheckHasNoRating(DownloadDataBucket bucket, List<string> failureReasons)
        {
            HtmlDocument doc = bucket.HtmlDocument;
            bool result = true;
            HtmlNode node;
            //Find Review Rating node.
            if (bucket.Country == "CA")
            {
                node = SelectSingleNode(doc.DocumentNode, Parameters.XPathParameters.CA_NoRatingPath, true, null);    
            }
            else
            {
                node = SelectSingleNode(doc.DocumentNode, BaseParameters.BaseXPaths.NoRatingPath, true, null);    
            }
            
            if (node != null)
            {
                result = false;
            }

            return result;

        }

        protected override decimal ScrapeRating(DownloadDataBucket bucket, List<string> failureReasons)
        {
            HtmlDocument doc = bucket.HtmlDocument;
            decimal rating = 0;
            HtmlNode node;

            if (bucket.Country == "CA")
            {
                node = SelectSingleNode(doc.DocumentNode, Parameters.XPathParameters.CA_RatingValuePath, true, null);

                if (node != null)
                {
                    decimal.TryParse(node.GetAttributeValue("alt", "0"), out rating);
                }
            }
            else
            {
                node = SelectSingleNode(doc.DocumentNode, BaseParameters.BaseXPaths.RatingValuePath, true, null);

                if (node != null)
                {
                    if (!string.IsNullOrWhiteSpace(BaseParameters.BaseXPaths.RatingValuePath))
                    {
                        if (!string.IsNullOrWhiteSpace(node.GetAttributeValue("content", "")))
                        {
                            //string rtext = node.InnerText.ToLower().Replace("stars", "").Replace("star", "");
                            decimal.TryParse(node.GetAttributeValue("content", ""), out rating);
                        }
                    }
                }
            }

            

            return rating;
        }

        protected override bool CheckHasNoServiceRating(DownloadDataBucket bucket, List<string> failureReasons)
        {
            return true;
        }

        protected override decimal ScrapeServiceRating(DownloadDataBucket bucket, List<string> failureReasons)
        {
            return 0;
        }

        protected override List<KeyValuePair<string, string>> ScrapeExtraInfo(DownloadDataBucket bucket, List<string> failureReasons)
        {
            return null;
        }

        protected override List<Connectors.ReviewDownloadResult.ReviewDetail> ScrapeDetails(DownloadDataBucket bucket, ReviewUrlDTO originalUrl, List<string> failureReasons)
        {
            HtmlDocument currentDoc = new HtmlDocument();
            List<Connectors.ReviewDownloadResult.ReviewDetail> results = new List<ReviewDownloadResult.ReviewDetail>();
            List<string> failedXPaths = new List<string>();
            int numberOfAttemptsToMakeForEachPage = 2;
            int pagesScraped = 1;
            var rawDataContent = string.Empty;
            var htmlDataContent = string.Empty;
            var ca_DataContentStart = "BVRRSecondaryRatingSummarySourceID\":\" ";
            var ca_DataContentEnd = "\"},";


            if (originalUrl.HtmlURL.Contains(".yellowpages.ca"))
            {
                #region Canadian Site Logic

                var dataContentUrl = ExtractDataContentUrl(originalUrl, ref failedXPaths);
                rawDataContent = DownloadString(dataContentUrl.HtmlURL, null);

                if (!String.IsNullOrEmpty(rawDataContent))
                {
                    htmlDataContent = SI.StringUtils.ExtractSubstringRegex(rawDataContent, ca_DataContentStart, ca_DataContentEnd);

                    if (!String.IsNullOrEmpty(htmlDataContent))
                    {
                        htmlDataContent = htmlDataContent.Replace("\\\"", "\"");
                        htmlDataContent = "<!DOCTYPE html><head></head><body>" + htmlDataContent + "</body></html>";
                        htmlDataContent = htmlDataContent.Replace("\\r\\n", Environment.NewLine).Replace("\\n", Environment.NewLine).Replace("\\r", Environment.NewLine).Replace("\\/", "/");
                        currentDoc.OptionFixNestedTags = true;
                        currentDoc.LoadHtml(htmlDataContent);
                        ScrapeDetailsLocal(currentDoc, results, failedXPaths, "CA");
                    }
                }
                else
                {
                    failureReasons.Add("Failed to download AJAX dataContent");
                }

                #endregion
            }
            else
            {
                int reviewCount = ScrapeReviewCount(bucket, failureReasons);
                if (reviewCount > 0)
                {
                    #region US Site Logic
                    currentDoc = bucket.HtmlDocument;
                    HtmlNode nextLink = SelectSingleNode(currentDoc.DocumentNode, Parameters.XPathParameters.ListingId);

                    if (nextLink != null)
                    {
                        string fullText = nextLink.GetAttributeValue("data-analytics", "");

                        Listing Listingobj = JsonConvert.DeserializeObject<Listing>(fullText);
                        //int startindex = fullText.IndexOf("listing_id") + 13;
                        //int endindex = fullText.IndexOf("listing_type") - 3;
                        //string listingId = fullText.Substring(startindex, endindex - startindex).Trim();

                        string listingId = string.Empty;
                        if (Listingobj != null)
                        {
                            listingId = Listingobj.listing_id;
                        }

                        while (nextLink != null)
                        {

                            string nextURL = string.Format("http://www.yellowpages.com/listings/{0}/reviews?page={1}", listingId, pagesScraped);
                            currentDoc = DownloadDocument(nextURL, null, numberOfAttemptsToMakeForEachPage);

                            if (currentDoc == null)
                            {
                                failureReasons.Add(string.Format("Failed to download page {0} at url {1}, tried {2} time(s)",
                                pagesScraped, nextURL, numberOfAttemptsToMakeForEachPage));
                                break;
                            }

                            ScrapeDetailsLocal(currentDoc, results, failedXPaths);

                            nextLink = SelectSingleNode(currentDoc.DocumentNode, Parameters.XPathParameters.PagingNextXPath);
                            pagesScraped++;
                            if (pagesScraped > 100) break;

                        }
                    }
                    else
                    {
                        failedXPaths.Add(Parameters.XPathParameters.ListingId);
                    }

                    foreach (string item in failedXPaths)
                    {
                        failureReasons.Add(string.Format("Failed XPath: {0}", item));
                    }
                    #endregion
                }
            }
            
            return results;
        }

        private ReviewUrlDTO ExtractDataContentUrl(ReviewUrlDTO originalUrl, ref List<string> failedXPaths)
        {
            //Written for Use with YellowPagesCA
            var externalId = string.Empty;
            var startText = "/";
            var endText = ".html?";
            var startPos = 0;
            var endPos = 0;
            var convertedUrl = new ReviewUrlDTO();


            endPos = originalUrl.HtmlURL.IndexOf(endText);
            startPos = originalUrl.HtmlURL.LastIndexOf(startText, endPos) + startText.Length;
            externalId = originalUrl.HtmlURL.Substring(startPos, endPos - startPos);

            if (!string.IsNullOrEmpty(externalId))
            {
                convertedUrl.ExternalID = externalId;
                convertedUrl.HtmlURL = "http://yellowpages.ugc.bazaarvoice.com/9013p-en_ca/" + externalId + "EN/reviews.djs?format=embeddedhtml";
            }
            else
            {
                failedXPaths.Add("Unable to extract DataContentUrl");
            }

            return convertedUrl;
        }

        private void ScrapeDetailsLocal(HtmlDocument headerDoc, List<Connectors.ReviewDownloadResult.ReviewDetail> results, List<string> failedXPaths, string countryCode = "US")
        {
            HtmlNodeCollection details;
            switch (countryCode)
            {
                case "CA":
                    details = SelectNodes(headerDoc.DocumentNode, Parameters.XPathParameters.CA_ReviewDetailIteratorPath, true, failedXPaths);
                    //details = headerDoc.DocumentNode.SelectNodes("//div[@id='BVRRDisplayContentReviewID_1']");

                    if (details != null && details.Count() > 0)
                    {
                        foreach (HtmlNode node in details)
                        {
                            Connectors.ReviewDownloadResult.ReviewDetail detail = ScrapeSingleReviewDetail_CA(node, failedXPaths);
                            if (detail != null)
                            {
                                results.Add(detail);
                            }
                        }
                    }
                    else
                    {
                        failedXPaths.Add("Unable to parse into CA ReviewDetailIteratorPath");
                    }
                    break;

                default: //This is for US 
                    details = SelectNodes(headerDoc.DocumentNode, BaseParameters.BaseXPaths.ReviewDetailIteratorPath, true, failedXPaths);

                    if (details != null && details.Count() > 0)
                    {
                        foreach (HtmlNode node in details)
                        {
                            Connectors.ReviewDownloadResult.ReviewDetail detail = ScrapeSingleReviewDetail(node, failedXPaths);
                            if (detail != null)
                            {
                                results.Add(detail);
                            }
                        }
                    }
                    else
                    {
                        failedXPaths.Add("Unable to parse into ReviewDetailIteratorPath");
                    }
                    break;
            }

        }
        
        private Connectors.ReviewDownloadResult.ReviewDetail ScrapeSingleReviewDetail(HtmlNode node, List<string> failedXPaths)
        {
            Connectors.ReviewDownloadResult.ReviewDetail review = new Connectors.ReviewDownloadResult.ReviewDetail();

            review.Rating = 0;
            HtmlNode node2 = SelectSingleNode(node, Parameters.XPathParameters.RatingCollected);
            if (node2 != null)
            {
                if (!string.IsNullOrWhiteSpace(node2.GetAttributeValue("content", "")))
                {
                    string rtext = node2.GetAttributeValue("content", "");
                    rtext = Regex.Replace(rtext, "[^0-9]+", string.Empty);
                    decimal dValue;
                    if (decimal.TryParse(rtext, out dValue))
                        review.Rating = dValue;
                }
            }
            else
            {
                failedXPaths.Add(Parameters.XPathParameters.RatingCollected);
            }

            node2 = SelectSingleNode(node, Parameters.XPathParameters.ReviewerName);
            if (node2 != null)
            {
                if (!string.IsNullOrWhiteSpace(node2.InnerText))
                {
                    string rtext = node2.InnerText.Trim();
                    review.ReviewerName = rtext;
                }
            }
            else
            {
                failedXPaths.Add(Parameters.XPathParameters.ReviewerName);
            }

            node2 = SelectSingleNode(node, Parameters.XPathParameters.ReviewDate);
            if (node2 != null)
            {
                if (!string.IsNullOrWhiteSpace(node2.InnerText))
                {

                    DateTime rDate;
                    if (DateTime.TryParse(node2.InnerText, out rDate))
                    {
                        review.ReviewDate = new SI.SIDateTime(rDate, 0);
                    }
                    else
                    {
                        failedXPaths.Add(Parameters.XPathParameters.ReviewDate);
                    }
                }

            }
            else
            {
                failedXPaths.Add(Parameters.XPathParameters.ReviewDate);
            }

            node2 = SelectSingleNode(node, Parameters.XPathParameters.ReviewTextFull);
            if (node2 != null)
            {
                if (!string.IsNullOrWhiteSpace(node2.InnerText))
                {
                    string rtext = node2.InnerText.StripHtml().Trim();
                    rtext = rtext.Replace("... view&nbsp;more", "");
                    review.ReviewTextFull = rtext;
                }
            }
            else
            {
                failedXPaths.Add(Parameters.XPathParameters.ReviewTextFull);
            }

            if (!string.IsNullOrEmpty(Parameters.XPathParameters.ExtraInfoKeys))
            {
                HtmlNode ExtraNode = SelectSingleNode(node, Parameters.XPathParameters.ExtraInfoValues);
                if (ExtraNode != null)
                    review.ExtraInfo.Add(new KeyValuePair<string, string>(Parameters.XPathParameters.ExtraInfoKeys, ExtraNode.InnerText.IsNullOptional("")));
            }

            #region Reply/Comment

            review.Comments = new List<ReviewDownloadResult.ReviewComment>();
            try
            {
                review.Comments = scrapeReviewComments(node, ref failedXPaths);
            }
            catch (Exception ex)
            {
            }
            #endregion

            return review;

        }

        private Connectors.ReviewDownloadResult.ReviewDetail ScrapeSingleReviewDetail_CA(HtmlNode node, List<string> failedXPaths)
        {
            Connectors.ReviewDownloadResult.ReviewDetail review = new Connectors.ReviewDownloadResult.ReviewDetail();

            review.Rating = 0;
            

            HtmlNode node2 = SelectSingleNode(node, Parameters.XPathParameters.CA_RatingCollected);
            if (node2 != null)
            {
                if (!string.IsNullOrWhiteSpace(node2.InnerText))
                {
                    string rtext = node2.InnerText;
                    rtext = Regex.Replace(rtext, "[^0-9]+", string.Empty);
                    decimal dValue;
                    if (decimal.TryParse(rtext, out dValue))
                        review.Rating = dValue;
                }
            }
            else
            {
                failedXPaths.Add(Parameters.XPathParameters.CA_RatingCollected);
            }

            node2 = SelectSingleNode(node, Parameters.XPathParameters.CA_ReviewerName);
            if (node2 != null)
            {
                if (!string.IsNullOrWhiteSpace(node2.InnerText))
                {
                    string rtext = node2.InnerText.Trim();
                    review.ReviewerName = rtext;
                }
            }
            else
            {
                failedXPaths.Add(Parameters.XPathParameters.CA_ReviewerName);
            }

            node2 = SelectSingleNode(node, Parameters.XPathParameters.CA_ReviewDate);
            if (node2 != null)
            {
                if (!string.IsNullOrWhiteSpace(node2.InnerText))
                {

                    DateTime rDate;
                    if (DateTime.TryParse(node2.InnerText, out rDate))
                    {
                        review.ReviewDate = new SI.SIDateTime(rDate, 0);
                    }
                    else
                    {
                        failedXPaths.Add(Parameters.XPathParameters.CA_ReviewDate);
                    }
                }

            }
            else
            {
                failedXPaths.Add(Parameters.XPathParameters.CA_ReviewDate);
            }

            node2 = SelectSingleNode(node, Parameters.XPathParameters.CA_ReviewTextFull);
            if (node2 != null)
            {
                if (!string.IsNullOrWhiteSpace(node2.InnerText))
                {
                    string rtext = node2.InnerText.StripHtml().Trim();
                    rtext = rtext.Replace("... view&nbsp;more", "");
                    review.ReviewTextFull = rtext;
                }
            }
            else
            {
                failedXPaths.Add(Parameters.XPathParameters.CA_ReviewTextFull);
            }

            //if (!string.IsNullOrEmpty(Parameters.XPathParameters.ExtraInfoKeys))
            //{
            //    HtmlNode ExtraNode = SelectSingleNode(node, Parameters.XPathParameters.ExtraInfoValues);
            //    if (ExtraNode != null)
            //        review.ExtraInfo.Add(new KeyValuePair<string, string>(Parameters.XPathParameters.ExtraInfoKeys, ExtraNode.InnerText.IsNullOptional("")));
            //}

            #region Reply/Comment

            //review.Comments = new List<ReviewDownloadResult.ReviewComment>();
            //try
            //{
            //    review.Comments = scrapeReviewComments(node, ref failedXPaths);
            //}
            //catch (Exception ex)
            //{
            //}
            #endregion

            return review;

        }

        private List<ReviewDownloadResult.ReviewComment> scrapeReviewComments(HtmlNode ReviewNode, ref List<string> failedXPaths)
        {
            List<ReviewDownloadResult.ReviewComment> resultComments = new List<ReviewDownloadResult.ReviewComment>();

            try
            {
                //HtmlNodeCollection nodes = SelectNodes(ReviewNode, ConnectionParameters.XPathParameters.ParentNodeReviewCommentXPath, true, null);
                HtmlNode node = SelectSingleNode(ReviewNode, Parameters.XPathParameters.ReviewCommentXPath, true, null);
                if (node != null)
                {
                    ReviewDownloadResult.ReviewComment comment = new ReviewDownloadResult.ReviewComment();

                    #region CommenterName

                    comment.CommenterName = string.Empty;
                    try
                    {
                        HtmlNode node2 = SelectSingleNode(node, Parameters.XPathParameters.ReviewCommentNameXPath);
                        if (node2 != null)
                        {
                            if (!string.IsNullOrWhiteSpace(node2.InnerText))
                            {
                                string rtext = node2.InnerText.Trim();
                                comment.CommenterName = rtext.Replace(".", "");
                            }
                        }
                        else
                        {
                            failedXPaths.Add(Parameters.XPathParameters.ReviewCommentNameXPath);
                        }
                    }
                    catch (Exception ex)
                    {
                    }
                    #endregion

                    #region CommentDate

                    comment.CommentDate = SI.SIDateTime.MinValue;
                    try
                    {
                        HtmlNode node2 = SelectSingleNode(node, Parameters.XPathParameters.ReviewCommentDate);
                        if (node2 != null)
                        {
                            if (!string.IsNullOrWhiteSpace(node2.InnerText))
                            {
                                string rtext = node2.InnerText.Trim();
                                DateTime rDate;
                                if (DateTime.TryParse(rtext, out rDate))
                                {
                                    comment.CommentDate = new SI.SIDateTime(rDate, 0);
                                }
                            }
                        }
                        else
                        {
                            failedXPaths.Add(Parameters.XPathParameters.ReviewCommentDate);
                        }
                    }
                    catch (Exception ex)
                    {

                    }
                    #endregion

                    #region CommentText
                    comment.Comment = string.Empty;
                    try
                    {
                        HtmlNode node2 = SelectSingleNode(node, Parameters.XPathParameters.ReviewCommentTextXPath);
                        if (node2 != null)
                        {
                            if (!string.IsNullOrWhiteSpace(node2.InnerText))
                            {
                                string rtext = node2.InnerText.StripHtml().Trim();
                                comment.Comment = rtext;
                            }
                        }
                        else
                        {
                            failedXPaths.Add(Parameters.XPathParameters.ReviewCommentTextXPath);
                        }
                    }
                    catch (Exception ex)
                    {

                    }
                    #endregion

                    resultComments.Add(comment);

                }
            }
            catch (Exception ex)
            {

            }
            return resultComments;
        }

        //public ConnectionStatus GetReviewsHtml(bool includeDetails = true)
        //{
        //    ConnectionStatus returnStatus = ConnectionStatus.InvalidData;

        //    if (!string.IsNullOrEmpty(ConnectionParameters.HtmlRequestUrl))
        //    {
        //        HttpRequestResults = string.Empty;

        //        ConnectionStatus httpStatus = ConnectorHttpRequest(ConnectionParameters.HtmlRequestUrl);
        //        if (httpStatus == ConnectionStatus.Success)
        //        {
        //            HtmlDocument htmlDocument = new HtmlDocument();
        //            htmlDocument.OptionFixNestedTags = true;
        //            htmlDocument.LoadHtml(HttpRequestResults);

        //            HtmlNode ReviewParentNode = SelectSingleNode(htmlDocument.DocumentNode, ConnectionParameters.XPathParameters.ParentnodeReviewSummaryXPath);
        //            if (ReviewParentNode != null)
        //            {
        //                SalesReviewCount = 0;
        //                SalesAverageRating = 0;

        //                HtmlNode SalesCountChildNode = SelectSingleNode(ReviewParentNode, ConnectionParameters.XPathParameters.ReviewCountCollectedXPath);
        //                if (SalesCountChildNode != null)
        //                    SalesReviewCount = SalesCountChildNode.InnerText.IsNullOptional("0").Trim().ToInteger();

        //                HtmlNode SalesRatingNode = SelectSingleNode(ReviewParentNode, ConnectionParameters.XPathParameters.RatingCollectedXPath);
        //                if (SalesRatingNode != null)
        //                    SalesAverageRating = SalesRatingNode.InnerText.IsNullOptional("").Replace(" stars", "").Trim().ToDecimal();

        //                if (includeDetails && SalesReviewCount > 0)
        //                {
        //                    int numberOfPages = SalesReviewCount / ConnectionParameters.XPathParameters.ReviewsPerPage;
        //                    if ((SalesReviewCount % ConnectionParameters.XPathParameters.ReviewsPerPage) > 0)
        //                        numberOfPages++;

        //                    if (numberOfPages == 0)
        //                        numberOfPages = 1;

        //                    for (int pageCount = 0; pageCount < numberOfPages; pageCount++)
        //                    {
        //                        if (pageCount > 0)
        //                        {
        //                            string pageRequestUrl = string.Format("{0}&page={1}", ConnectionParameters.HtmlRequestUrl, pageCount + 1);
        //                            if (ConnectorHttpRequest(pageRequestUrl) != ConnectionStatus.Success) break;
        //                            htmlDocument.LoadHtml(HttpRequestResults);
        //                        }

        //                        HtmlNodeCollection ReviewNodes = SelectNodes(htmlDocument.DocumentNode, ConnectionParameters.XPathParameters.ParentNodeReviewDetailsXPath);
        //                        if (ReviewNodes != null)
        //                        {
        //                            foreach (HtmlNode reviewNode in ReviewNodes)
        //                            {
        //                                Review review = new Review();

        //                                review.Rating = 0;
        //                                HtmlNode RatingNode = SelectSingleNode(reviewNode, ConnectionParameters.XPathParameters.RatingValueXPath);
        //                                if (RatingNode != null)
        //                                    review.Rating = RatingNode.InnerText.IsNullOptional("").Replace(" stars", "").Trim().ToDecimal();

        //                                HtmlNode ReviewerNameNode = SelectSingleNode(reviewNode, ConnectionParameters.XPathParameters.ReviewerNameXPath);
        //                                if (ReviewerNameNode != null)
        //                                    review.ReviewerName = ReviewerNameNode.InnerText.IsNullOptional("");

        //                                HtmlNode ReviewDateNode = SelectSingleNode(reviewNode, ConnectionParameters.XPathParameters.ReviewDateXPath);
        //                                if (ReviewDateNode != null)
        //                                    review.ReviewDate = ReviewDateNode.InnerText.Trim().ToDateTime();

        //                                HtmlNode ReviewTextNode = SelectSingleNode(reviewNode, ConnectionParameters.XPathParameters.ReviewTextFullXPath);
        //                                if (ReviewTextNode != null)
        //                                    review.ReviewTextFull = ReviewTextNode.InnerText.IsNullOptional("").StripHtml();

        //                                if (!string.IsNullOrEmpty(ConnectionParameters.XPathParameters.ExtraInfoKeysXPath))
        //                                {
        //                                    HtmlNode ExtraNode = SelectSingleNode(reviewNode, ConnectionParameters.XPathParameters.ExtraInfoValuesXPath);
        //                                    if (ExtraNode != null)
        //                                        review.ExtraInfo.Add(new KeyValuePair<string, string>(ConnectionParameters.XPathParameters.ExtraInfoKeysXPath, ExtraNode.InnerText.IsNullOptional("")));
        //                                }

        //                                ReviewDetails.Add(review);
        //                            }
        //                        }

        //                    }
        //                }

        //                returnStatus = ConnectionStatus.Success;
        //            }
        //        }
        //        else
        //            returnStatus = httpStatus;
        //    }

        //    return returnStatus;
        //}


        protected override void dispose()
        {

        }

    }

    class Listing
    {
        public string listing_id { get; set; }
    }
}
