﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Connectors.Parameters;
using Newtonsoft.Json.Linq;
using HtmlAgilityPack;
using SI.Extensions;
using Connectors.Reviews;
using SI.DTO;
using System.Text.RegularExpressions;

namespace Connectors
{
    public class DealerRaterReviewsConnection : SIReviewConnection
    {
        private SIDealerRaterReviewConnectionParameters Parameters;

        #region Initializer


        public DealerRaterReviewsConnection(SIDealerRaterReviewConnectionParameters siDealerRaterReviewConnectionParameters)
            : base(siDealerRaterReviewConnectionParameters)
        {
            Parameters = siDealerRaterReviewConnectionParameters;
        }

        #endregion


        private class DealerRaterBucket : DownloadDataBucket
        {
            public DealerRaterBucket()
            {
                ApiJsonData = null;
            }
            public dynamic ApiJsonData { get; set; }
        }

        protected override DownloadDataBucket DoDownload(ReviewUrlDTO url, List<string> failureReasons)
        {
            if (string.IsNullOrEmpty(url.HtmlURL.Trim()))
            {
                return new DownloadDataBucket();
            }

            HtmlDocument doc = DownloadDocument(url.HtmlURL, null, 3, 2000);

            // if html fails, always fail with a null return. no html header, no go
            if (doc == null) return null;

            DealerRaterBucket bucket = new DealerRaterBucket() { HtmlDocument = doc };

            if (string.IsNullOrEmpty(url.ExternalID))
            {
                var dealerRatorid = url.HtmlURL.Substring(url.HtmlURL.LastIndexOf("-") + 1);
                if (dealerRatorid.Contains("page"))
                {
                    dealerRatorid = dealerRatorid.Substring(0, dealerRatorid.IndexOf("/"));
                    dealerRatorid = dealerRatorid.Replace("/", "");
                }
                else
                {
                    dealerRatorid = dealerRatorid.Replace("/", "");
                }
                url.ExternalID = dealerRatorid;
            }

            if (!string.IsNullOrWhiteSpace(url.ExternalID))
            {
                string apiURL = string.Format(Parameters.Parameters.APIURLTemplate, url.ExternalID);
                string apiString = DownloadString(apiURL, null);

                if (!string.IsNullOrWhiteSpace(apiString))
                {
                    if (!apiString.ToLower().Contains("\"apiexception\""))
                    {
                        try
                        {
                            bucket.ApiJsonData = Newtonsoft.Json.JsonConvert.DeserializeObject<dynamic>(apiString);
                        }
                        catch (Exception)
                        {
                            // for now do nothing if we cannot parse the api json...
                            bucket.ApiJsonData = null;
                        }
                    }
                }
            }

            return bucket;
        }

        protected override SI.DTO.ReviewUrlDTO TweakUrl(SI.DTO.ReviewUrlDTO url)
        {
            if (url.HtmlURL.Contains("-service-"))
            {
                url.HtmlURL = url.HtmlURL.Replace("-service-", "-review-");
            }

            var dealerRatorid = url.HtmlURL.Substring(url.HtmlURL.LastIndexOf("-") + 1);
            if (dealerRatorid.Contains("page"))
            {
                dealerRatorid = dealerRatorid.Substring(0, dealerRatorid.IndexOf("/"));
                dealerRatorid = dealerRatorid.Replace("/", "");
            }
            else
            {
                dealerRatorid = dealerRatorid.Replace("/", "");
            }

            
            string requestURL = string.Format(
                "https://api.dealerrater.com/reviews/{0}?accessToken=A4545A1A-D2A1-4222-8174-9BA4EBF6C933",
                dealerRatorid
            );
            url.ApiURL = requestURL;

            return url;
        }

        #region service stuff - not used

        protected override bool CheckHasNoServiceReviews(DownloadDataBucket bucket, List<string> failureReasons)
        {
            return true;
        }

        protected override int ScrapeServiceReviewCount(DownloadDataBucket bucket, List<string> failureReasons)
        {
            return 0;
        }

        protected override bool CheckHasNoServiceRating(DownloadDataBucket bucket, List<string> failureReasons)
        {
            return true;
        }

        protected override decimal ScrapeServiceRating(DownloadDataBucket bucket, List<string> failureReasons)
        {
            return 0;
        }


        #endregion

        protected override bool CheckHasCheckNoReviews(DownloadDataBucket bucket, List<string> failureReasons)
        {
            HtmlDocument doc = bucket.HtmlDocument;

            bool result = false;

            if (!string.IsNullOrWhiteSpace(BaseParameters.BaseXPaths.NoReviewPath))
            {

                HtmlNode node = SelectSingleNode(doc.DocumentNode, BaseParameters.BaseXPaths.NoReviewPath, true, null);
                if (node != null)
                {
                    if (!string.IsNullOrWhiteSpace(BaseParameters.BaseXPaths.NoReviewText))
                    {
                        if (string.IsNullOrWhiteSpace(node.InnerText))
                        {
                            result = false;
                        }
                        else
                        {
                            if (node.InnerText.Trim().ToLower() == BaseParameters.BaseXPaths.NoReviewText.Trim().ToLower())
                            {
                                result = true;
                            }
                        }
                    }
                    else
                    {
                        result = true;
                    }
                }

            }

            return result;
        }

        protected override int ScrapeReviewCount(DownloadDataBucket bucket, List<string> failureReasons)
        {
            HtmlDocument doc = bucket.HtmlDocument;

            int reviewCount = 0;

            HtmlNode node = SelectSingleNode(doc.DocumentNode, BaseParameters.BaseXPaths.ReviewCountPath, true, null);
            if (node != null)
            {
                if (!string.IsNullOrWhiteSpace(node.InnerText))
                {
                    string text = node.InnerText.Trim().ToLower();
                    text = text.Replace("based on", "");
                    text = text.Replace("reviews", "");
                    int.TryParse(text, out reviewCount);
                }
            }

            return reviewCount;
        }

        protected override bool CheckHasNoRating(DownloadDataBucket bucket, List<string> failureReasons)
        {
            HtmlDocument doc = bucket.HtmlDocument;

            bool result = false;

            if (!string.IsNullOrWhiteSpace(BaseParameters.BaseXPaths.NoRatingPath))
            {
                HtmlNode node = SelectSingleNode(doc.DocumentNode, BaseParameters.BaseXPaths.NoRatingPath, true, null);
                if (node != null)
                {
                    result = true;
                }
            }

            return result;
        }

        protected override decimal ScrapeRating(DownloadDataBucket bucket, List<string> failureReasons)
        {
            HtmlDocument doc = bucket.HtmlDocument;

            decimal rating = 0;

            HtmlNode node = SelectSingleNode(doc.DocumentNode, BaseParameters.BaseXPaths.RatingValuePath, true, null);
            if (node != null)
            {
                if (!string.IsNullOrWhiteSpace(node.InnerText))
                {
                    string rtext = node.InnerText;
                    decimal.TryParse(rtext, out rating);
                }
            }

            return rating;
        }

        protected override List<KeyValuePair<string, string>> ScrapeExtraInfo(DownloadDataBucket bucket, List<string> failureReasons)
        {
            List<KeyValuePair<string, string>> values = new List<KeyValuePair<string, string>>();
            HtmlDocument doc = bucket.HtmlDocument;

            string keysString = Parameters.Parameters.ExtraInfoKeys;
            string pathsString = Parameters.Parameters.ExtraInfoXPaths;

            string[] keys = keysString.Split(new char[] { '|' }, StringSplitOptions.RemoveEmptyEntries);
            string[] paths = pathsString.Split(new char[] { '|' }, StringSplitOptions.RemoveEmptyEntries);

            if (keys.Length == paths.Length)
            {
                for (int i = 0; i < keys.Length; i++)
                {
                    string key = keys[i];
                    string xpath = paths[i];

                    HtmlNode node = SelectSingleNode(doc.DocumentNode, xpath, true, null);
                    HtmlNode node2 = SelectSingleNode(doc.DocumentNode, key, true, null);

                    if (node != null)
                    {
                        if (node.InnerText != null && !string.IsNullOrWhiteSpace(node.InnerText))
                        {
                            string rtext = node.InnerText;
                            string rkey = node2.InnerText;
                            values.Add(new KeyValuePair<string, string>(rkey, rtext));
                        }
                    }
                }
            }
            else
            {
                failureReasons.Add(string.Format("DealerRater ExtraInfo paths invalid.  Keys and Values have different counts: {0} != {1}", keysString, pathsString));
                return null;
            }

            return values;
        }

        private List<KeyValuePair<string, string>> scrapeExtraInfoDetail(HtmlNode node, List<string> failureReasons)
        {
            List<KeyValuePair<string, string>> values = new List<KeyValuePair<string, string>>();
            //HtmlDocument doc = bucket.HtmlDocument;

            string keysString = Parameters.Parameters.ExtraInfoKeysDetailXPath;
            string pathsString = Parameters.Parameters.ExtraInfoValuesDetailXPath;

            string[] keys = keysString.Split(new char[] { '|' }, StringSplitOptions.RemoveEmptyEntries);
            string[] paths = pathsString.Split(new char[] { '|' }, StringSplitOptions.RemoveEmptyEntries);

            if (keys.Length == paths.Length)
            {
                for (int i = 0; i < keys.Length; i++)
                {
                    string key = keys[i];
                    string xpath = paths[i];

                    HtmlNode valuenode = SelectSingleNode(node, xpath, true, null);
                    HtmlNode keynode = SelectSingleNode(node, key, true, null);

                    if (valuenode != null)
                    {
                        if (valuenode.InnerText != null && !string.IsNullOrWhiteSpace(valuenode.InnerText))
                        {
                            string rtext = valuenode.InnerText;
                            string rkey = keynode.InnerText;
                            values.Add(new KeyValuePair<string, string>(rkey, rtext));
                        }
                    }
                }
            }
            else
            {
                failureReasons.Add(string.Format("DealerRater Details ExtraInfo paths invalid.  Keys and Values have different counts: {0} != {1}", keysString, pathsString));
                return null;
            }

            return values;
        }

        protected override List<ReviewDownloadResult.ReviewDetail> ScrapeDetails(DownloadDataBucket bucket, SI.DTO.ReviewUrlDTO originalUrl, List<string> failureReasons)
        {
            List<ReviewDownloadResult.ReviewDetail> details = null;

            //if api exists, try that, otherwise try html

            DealerRaterBucket myBucket = (DealerRaterBucket)bucket;
            if (myBucket.ApiJsonData != null)
            {
                details = scrapeDetails(myBucket.ApiJsonData);
            }
            else
            {
                details = scrapeDetails(myBucket.HtmlDocument, failureReasons);
            }

            return details;
        }

        private List<ReviewDownloadResult.ReviewDetail> scrapeDetails(dynamic json)
        {
            List<ReviewDownloadResult.ReviewDetail> details = new List<ReviewDownloadResult.ReviewDetail>();

            dynamic reviewDetails = json;
            if (reviewDetails.error == null)
            {
                JArray jAReviews = JArray.Parse(reviewDetails.reviews.ToString());
                if (jAReviews != null)
                {
                    IEnumerable<JToken> jTokenReviews = jAReviews.Children();

                    foreach (JToken jReview in jTokenReviews)
                    {
                        ReviewDownloadResult.ReviewDetail review = new ReviewDownloadResult.ReviewDetail();

                        List<decimal> ratings = new List<decimal>();
                        decimal tempRating;

                        string rat1 = jReview["ratings"]["customerService"].ToString();
                        if (decimal.TryParse(rat1, out tempRating)) if (tempRating > 0) ratings.Add(tempRating);
                        review.ExtraInfo.Add(new KeyValuePair<string, string>("CustomerServiceRating", rat1));

                        string rat2 = jReview["ratings"]["quality"].ToString();
                        if (decimal.TryParse(rat2, out tempRating)) if (tempRating > 0) ratings.Add(tempRating);
                        review.ExtraInfo.Add(new KeyValuePair<string, string>("QualityofWorkRating", rat2));

                        string rat3 = jReview["ratings"]["friendliness"].ToString();
                        if (decimal.TryParse(rat3, out tempRating)) if (tempRating > 0) ratings.Add(tempRating);
                        review.ExtraInfo.Add(new KeyValuePair<string, string>("FriendlinessRating", rat3));

                        string rat4 = jReview["ratings"]["overallExperience"].ToString();
                        if (decimal.TryParse(rat4, out tempRating)) if (tempRating > 0) ratings.Add(tempRating);
                        review.ExtraInfo.Add(new KeyValuePair<string, string>("OverallExperienceRating", rat4));

                        string rat5 = jReview["ratings"]["price"].ToString();
                        if (decimal.TryParse(rat5, out tempRating)) if (tempRating > 0) ratings.Add(tempRating);
                        review.ExtraInfo.Add(new KeyValuePair<string, string>("PricingRating", rat5));

                        review.Type = (jReview["visitReason"].ToString() == "Service") ? "service" : "sales";
                        review.ReviewTextFull = jReview["comments"].ToString();

                        //review.Rating = jReview["ratings"]["overallExperience"].ToString().ToDecimal();
                        if (ratings.Count() > 0)
                        {
                            review.Rating = ratings.Sum() / ratings.Count();
                        }
                        else
                        {
                            review.Rating = 0;
                        }

                        review.ReviewDate = new SI.SIDateTime(jReview["dateWritten"].ToString().ToDateTime().ToShortDateString().ToDateTime(), 0);
                        review.ReviewerName = jReview["user"]["username"].ToString();
                        review.FullReviewURL = reviewDetails.reviewsUrl;

                        if (jReview["responses"] != null)
                        {
                            foreach (JToken jComment in jReview["responses"].Children())
                            {
                                ReviewDownloadResult.ReviewComment reviewComment = new ReviewDownloadResult.ReviewComment();
                                reviewComment.Comment = jComment["responseText"].ToString();
                                reviewComment.CommenterName = jComment["user"]["username"].ToString();
                                reviewComment.CommentDate = new SI.SIDateTime(jComment["dateEntered"].ToString().ToDateTime(), 0);

                                review.Comments.Add(reviewComment);
                            }
                        }

                        if (!string.IsNullOrEmpty(jReview["employees"].ToString()))
                            review.ExtraInfo.Add(new KeyValuePair<string, string>("NameOfEmployee", jReview["employees"].ToString()));
                        if (!string.IsNullOrEmpty(jReview["visitReason"].ToString()))
                            review.ExtraInfo.Add(new KeyValuePair<string, string>("ReasonForVisit", jReview["visitReason"].ToString()));
                        if (!string.IsNullOrEmpty(jReview["recommendDealer"].ToString()))
                            review.ExtraInfo.Add(new KeyValuePair<string, string>("DoYouRecommend", jReview["recommendDealer"].ToString()));


                        details.Add(review);

                    }
                }
            }

            return details;

        }

        private List<ReviewDownloadResult.ReviewDetail> scrapeDetails(HtmlDocument doc, List<string> failureReasons)
        {
            // next page link:   .//a[contains(@href,'/page') and contains(text(),'>>')]

            List<ReviewDownloadResult.ReviewDetail> reviews = new List<ReviewDownloadResult.ReviewDetail>();

            HtmlDocument docToTest = doc;
            HtmlNodeCollection reviewNodes = SelectNodes(docToTest.DocumentNode, Parameters.BaseXPaths.ReviewDetailIteratorPath, false);

            int numberOfAttemptsToMakeForEachPage = 2;
            int pagesScraped = 0;

            if (reviewNodes != null)
            {
            while (reviewNodes.Count() > 0)
            {
                foreach (HtmlNode reviewNode in reviewNodes)
                {
                    ReviewDownloadResult.ReviewDetail review = scrapeSingleReviewDetail(reviewNode, failureReasons);
                    if (review != null)
                    {
                        reviews.Add(review);
                    }
                }

                pagesScraped++;
                if (pagesScraped > 100) break;

                HtmlNode nextNode = SelectSingleNode(docToTest.DocumentNode, Parameters.Parameters.PagingNextXPath, true);
                if (nextNode != null)
                {
                    string nextURL = string.Format("http://www.dealerrater.com{0}", nextNode.Attributes["href"].Value);
                    docToTest = DownloadDocument(nextURL, null, numberOfAttemptsToMakeForEachPage, 3000);

                    if (docToTest == null)
                    {
                        failureReasons.Add(string.Format("Failed to download page {0} at url {1}, tried {2} time(s)",
                            pagesScraped + 1, nextURL, numberOfAttemptsToMakeForEachPage));
                        break;
                    }
                    else
                    {
                        reviewNodes = SelectNodes(docToTest.DocumentNode, Parameters.BaseXPaths.ReviewDetailIteratorPath, false);
                    }
                }
                else
                {
                    break;
                }

            }
        }

            return reviews;
        }

        private Connectors.ReviewDownloadResult.ReviewDetail scrapeSingleReviewDetail(HtmlNode node, List<string> failedXPaths)
        {
            Connectors.ReviewDownloadResult.ReviewDetail review = new Connectors.ReviewDownloadResult.ReviewDetail();

            review.Rating = 0;
            HtmlNode node2 = SelectSingleNode(node, Parameters.Parameters.RatingValueXPath2);
            if (node2 != null)
            {
                if (!string.IsNullOrWhiteSpace(node2.InnerText))
                {
                    string rtext = node2.InnerText.ToLower();
                    decimal dValue;
                    if (decimal.TryParse(rtext, out dValue))
                        review.Rating = dValue;
                }
            }
            else
            {
                failedXPaths.Add(Parameters.Parameters.RatingValueXPath2);
            }

            node2 = SelectSingleNode(node, Parameters.Parameters.ReviewerNameXPath);
            if (node2 != null)
            {
                if (!string.IsNullOrWhiteSpace(node2.InnerText))
                {
                    string rtext = node2.InnerText.Trim();
                    review.ReviewerName = rtext;
                }
            }
            else
            {
                failedXPaths.Add(Parameters.Parameters.ReviewerNameXPath);
            }

            node2 = SelectSingleNode(node, Parameters.Parameters.ReviewDateXPath);
            if (node2 != null)
            {
                if (node2.Attributes["title"] != null && !string.IsNullOrWhiteSpace(node2.Attributes["title"].Value))
                {
                    string rtext = node2.Attributes["title"].Value.Trim();
                    DateTime rDate;
                    if (DateTime.TryParse(rtext, out rDate))
                    {
                        review.ReviewDate = new SI.SIDateTime(rDate.ToShortDateString().ToDateTime(), 0);
                    }
                }
            }
            else
            {
                failedXPaths.Add(Parameters.Parameters.ReviewDateXPath);
            }

            node2 = SelectSingleNode(node, Parameters.Parameters.ReviewTextFullXPath);
            if (node2 != null)
            {
                if (!string.IsNullOrWhiteSpace(node2.InnerText))
                {
                    string rtext = node2.InnerText.StripHtml().Trim();
                    review.ReviewTextFull = rtext;
                }
            }
            else
            {
                failedXPaths.Add(Parameters.Parameters.ReviewTextFullXPath);
            }

            //Scrape extra Info for (Customer Service,Quality of Work,Friendliness,Overall Experience,Pricing,Employee(s) Dealt With)
            review.ExtraInfo = scrapeExtraInfoDetail(node, failedXPaths);
            /*
            node2 = SelectSingleNode(node, Parameters.Parameters.ReasonForVisitXPath);
            if (node2 != null)
            {
                if (!string.IsNullOrWhiteSpace(node2.InnerText))
                {
                    string rtext = node2.InnerText.StripHtml().Trim();
                    var reasonText = "Reason For Visit:";
                    var startPos = rtext.IndexOf(reasonText) + reasonText.Length;
                    var endPos = rtext.IndexOf('\r',startPos);
                    rtext = rtext.Substring(startPos, endPos-startPos).Trim();
                    review.ExtraInfo.Add(new KeyValuePair<string, string>("Reason for Visit:", rtext));
                }
            }
            else
            {
                failedXPaths.Add(Parameters.Parameters.ReasonForVisitXPath);
            }

            node2 = SelectSingleNode(node, Parameters.Parameters.ReviewRecommendationExtraInfoXPath);
            if (node2 != null)
            {
                if (!string.IsNullOrWhiteSpace(node2.InnerText))
                {
                    string rtext = node2.InnerText.StripHtml().Trim();
                    var reasonText = "I Recommend This Dealer:";
                    var startPos = rtext.IndexOf(reasonText) + reasonText.Length;
                    var endPos = rtext.IndexOf('\r', startPos);
                    rtext = rtext.Substring(startPos, endPos - startPos).Trim();
                    review.ExtraInfo.Add(new KeyValuePair<string, string>("Recommended:", rtext));
                }
            }
            else
            {
                failedXPaths.Add(Parameters.Parameters.ReviewRecommendationExtraInfoXPath);
            }
            */
            #region Comments

            HtmlNodeCollection commentNodes = SelectNodes(node, Parameters.Parameters.CommentIteratorXPath, true);
            if (commentNodes != null)
            {
                foreach (HtmlNode commentNode in commentNodes)
                {
                    HtmlNode commenterNameNode = SelectSingleNode(commentNode, Parameters.Parameters.CommenterNameXPath, false, failedXPaths);
                    if (commenterNameNode != null)
                    {
                        ReviewDownloadResult.ReviewComment reviewComment = new ReviewDownloadResult.ReviewComment();

                        var fullText = commenterNameNode.InnerHtml.Replace("<br />", "<br>");
                        fullText = fullText.Replace("<p>","<br>");
                        var fullTextSplit = Regex.Split(fullText, "<br>");
                        var commenterName = fullTextSplit[0].Trim();
                        if (commenterName != null)
                        {
                            var keyText = "Response From:";
                            var startPos = commenterName.IndexOf(keyText) + keyText.Length;
                            commenterName = commenterName.Substring(startPos).StripHtml().Trim();
                            reviewComment.CommenterName =commenterName;
                        }

                        var commentDate = fullTextSplit[1].StripHtml().Trim();
                        if (commentDate != null)
                        {
                            var keyText = "Added:";
                            var startPos = commentDate.IndexOf(keyText) + keyText.Length;
                            commentDate = commentDate.Substring(startPos).Trim();
                            DateTime rDate;
                            if (DateTime.TryParse(commentDate, out rDate))
                            {
                                reviewComment.CommentDate = new SI.SIDateTime(rDate.ToShortDateString().ToDateTime(), 0);
                            }
                        }

                        HtmlNode fullTextNode = SelectSingleNode(commentNode, Parameters.Parameters.CommentTextFullXPath, false, failedXPaths);
                        if (fullTextNode != null && !string.IsNullOrWhiteSpace(fullTextNode.InnerText))
                        {
                            string comment = fullTextNode.InnerText.Trim();
                                reviewComment.Comment = comment;
                        }

                           review.Comments.Add(reviewComment);

                     }      
                }
            }
            #endregion

            return review;


        }
        
        //private void getHeader()
        //{
        //    ConnectionStatus returnStatus = ConnectionStatus.InvalidData;

        //    if (!string.IsNullOrEmpty(ConnectionParameters.HtmlRequestUrl))
        //    {
        //        HttpRequestResults = string.Empty;

        //        ConnectionStatus httpReqStatus = ConnectorHttpRequest(ConnectionParameters.HtmlRequestUrl);
        //        if (httpReqStatus == ConnectionStatus.Success)
        //        {
        //            HtmlDocument htmlDocument = new HtmlDocument();
        //            htmlDocument.OptionFixNestedTags = true;
        //            htmlDocument.LoadHtml(HttpRequestResults);

        //            SalesReviewCount = 0;
        //            ServiceReviewCount = 0;
        //            SalesAverageRating = 0;
        //            ServiceAverageRating = 0;

        //            //get rating
        //            HtmlNode ReviewRatingParentNode = SelectSingleNode(htmlDocument.DocumentNode, ConnectionParameters.XPathParameters.RatingCollectedXPath);
        //            if (ReviewRatingParentNode != null)
        //                SalesAverageRating = ReviewRatingParentNode.InnerHtml.IsNullOptional("0").ToDecimal();

        //            //get count
        //            HtmlNode ReviewParentNode = SelectSingleNode(htmlDocument.DocumentNode, ConnectionParameters.XPathParameters.ParentnodeReviewSummaryXPath);
        //            if (ReviewParentNode != null)
        //            {
        //                int TotalReviewCount = 0;
        //                HtmlNode SalesCountChildNode = SelectSingleNode(ReviewParentNode, ConnectionParameters.XPathParameters.ReviewCountCollectedXPath);
        //                if (SalesCountChildNode != null)
        //                    TotalReviewCount = SalesCountChildNode.InnerText.IsNullOptional("0").Trim().ToInteger();

        //                if (includeDetails && TotalReviewCount > 0)
        //                {
        //                    int numberOfPages = TotalReviewCount / ConnectionParameters.XPathParameters.ReviewsPerPage;
        //                    if ((TotalReviewCount % ConnectionParameters.XPathParameters.ReviewsPerPage) > 0)
        //                        numberOfPages++;

        //                    if (numberOfPages == 0)
        //                        numberOfPages = 1;

        //                    for (int pageCount = 0; pageCount < numberOfPages; pageCount++)
        //                    {
        //                        if (pageCount > 0)
        //                        {
        //                            string pageRequestUrl = string.Format("{0}page{1}", ConnectionParameters.HtmlRequestUrl, pageCount + 1); //check if url ends with / ?
        //                            if (ConnectorHttpRequest(pageRequestUrl) != ConnectionStatus.Success) break;
        //                            htmlDocument.LoadHtml(HttpRequestResults);
        //                        }

        //                        HtmlNodeCollection ReviewNodes = SelectNodes(htmlDocument.DocumentNode, ConnectionParameters.XPathParameters.ParentNodeReviewDetailsXPath);
        //                        if (ReviewNodes != null)
        //                        {
        //                            foreach (HtmlNode reviewNode in ReviewNodes)
        //                            {
        //                                Review review = new Review();

        //                                review.Rating = 0;
        //                                HtmlNode RatingNode = SelectSingleNode(reviewNode, ConnectionParameters.XPathParameters.RatingValueXPath);
        //                                if (RatingNode != null)
        //                                    review.Rating = RatingNode.InnerText.IsNullOptional("0").ToDecimal();

        //                                HtmlNode ReviewerNameNode = SelectSingleNode(reviewNode, ConnectionParameters.XPathParameters.ReviewerNameXPath);
        //                                if (ReviewerNameNode != null)
        //                                    review.ReviewerName = ReviewerNameNode.InnerText.IsNullOptional("");

        //                                HtmlNode ReviewDateNode = SelectSingleNode(reviewNode, ConnectionParameters.XPathParameters.ReviewDateXPath);
        //                                if (ReviewDateNode != null)
        //                                    review.ReviewDate = ReviewDateNode.GetAttributeValue("title", "").ToDateTime();

        //                                HtmlNode ReviewTextNode = SelectSingleNode(reviewNode, ConnectionParameters.XPathParameters.ReviewTextFullXPath);
        //                                if (ReviewTextNode != null)
        //                                    review.ReviewTextFull = ReviewTextNode.InnerText.IsNullOptional("");

        //                                review.FullReviewURL = string.Empty;

        //                                HtmlNode ReasonForVisitNode = SelectSingleNode(reviewNode, ConnectionParameters.XPathParameters.ReasonForVisitXPath);
        //                                if (ReasonForVisitNode != null)
        //                                {
        //                                    string reason = ReasonForVisitNode.NextSibling.InnerText.IsNullOptional("");
        //                                    if (reason.Contains("Sales"))
        //                                    {
        //                                        review.Type = "sales";
        //                                        SalesReviewCount++;
        //                                    }
        //                                    else if (reason.Contains("Service"))
        //                                    {
        //                                        review.Type = "service";
        //                                        ServiceReviewCount++;

        //                                        HtmlNode ServiceRatingNode = SelectSingleNode(reviewNode, ConnectionParameters.XPathParameters.ServiceRatingXPath);
        //                                        if (ServiceRatingNode != null)
        //                                            ServiceAverageRating += ServiceRatingNode.InnerText.IsNullOptional("0").ToDecimal();
        //                                    }
        //                                }

        //                                ReviewDetails.Add(review);
        //                            }
        //                        }
        //                    }

        //                    if (ServiceReviewCount > 0)
        //                        ServiceAverageRating = ServiceAverageRating / ServiceReviewCount;
        //                }

        //                returnStatus = ConnectionStatus.Success;
        //            }
        //        }
        //        else
        //            returnStatus = httpReqStatus;

        //    }

        //    return returnStatus;
        //}


        //public ConnectionStatus GetReviewsAPI(bool includeDetails = true)
        //{
        //    ConnectionStatus returnStatus = ConnectionStatus.InvalidData;

        //    if (!string.IsNullOrEmpty(ConnectionParameters.ApiRequestUrl))
        //    {
        //        HttpRequestResults = string.Empty;

        //        ConnectionStatus httpStatus = ConnectorHttpRequest(ConnectionParameters.ApiRequestUrl);
        //        if (httpStatus == ConnectionStatus.Success)
        //        {
        //            if (!string.IsNullOrEmpty(HttpRequestResults))
        //            {
        //                dynamic reviewDetails = Newtonsoft.Json.JsonConvert.DeserializeObject<dynamic>(HttpRequestResults);
        //                if (reviewDetails != null)
        //                {
        //                    if (reviewDetails.error == null)
        //                    {
        //                        returnStatus = ConnectionStatus.Success;

        //                        SalesAverageRating = (decimal) reviewDetails.ratings.averageRating;

        //                        JArray jAReviews = JArray.Parse(reviewDetails.reviews.ToString());
        //                        if (jAReviews != null)
        //                        {
        //                            IEnumerable<JToken> jTokenReviews = jAReviews.Children();
        //                            ServiceReviewCount = jTokenReviews.Count(t => t["visitReason"].ToString() == "Service");
        //                            SalesReviewCount = jTokenReviews.Count(t => t["visitReason"].ToString().Contains("Sales"));

        //                            ServiceAverageRating = jTokenReviews.Where(r => r["visitReason"].ToString() == "Service")
        //                                                                .Select(r => r["ratings"]["overallExperience"].ToString().ToDecimal()).Average();

        //                            if (includeDetails)
        //                            {
        //                                foreach (JToken jReview in jTokenReviews)
        //                                {
        //                                    Review review = new Review();
        //                                    review.Type = (jReview["visitReason"].ToString() == "Service") ? "service" : "sales";
        //                                    review.ReviewTextFull = jReview["comments"].ToString();
        //                                    review.Rating = jReview["ratings"]["overallExperience"].ToString().ToDecimal();
        //                                    review.ReviewDate = jReview["dateWritten"].ToString().ToDateTime();
        //                                    review.ReviewerName = jReview["user"]["username"].ToString();
        //                                    review.FullReviewURL = reviewDetails.reviewsUrl;

        //                                    if (jReview["responses"] != null)
        //                                    {
        //                                        foreach (JToken jComment in jReview["responses"].Children())
        //                                        {
        //                                            ReviewComment reviewComment = new ReviewComment();
        //                                            reviewComment.Comment = jComment["responseText"].ToString();
        //                                            reviewComment.CommentDate = jComment["dateEntered"].ToString().ToDateTime();

        //                                            review.Comments.Add(reviewComment);
        //                                        }
        //                                    }

        //                                    if (!string.IsNullOrEmpty(jReview["employees"].ToString()))
        //                                        review.ExtraInfo.Add(new KeyValuePair<string, string>("NameOfEmployee", jReview["employees"].ToString()));
        //                                    if (!string.IsNullOrEmpty(jReview["visitReason"].ToString()))
        //                                        review.ExtraInfo.Add(new KeyValuePair<string, string>("ReasonForVisit", jReview["visitReason"].ToString()));
        //                                    if (!string.IsNullOrEmpty(jReview["recommendDealer"].ToString()))
        //                                        review.ExtraInfo.Add(new KeyValuePair<string, string>("DoYouRecommend", jReview["recommendDealer"].ToString()));

        //                                    review.ExtraInfo.Add(new KeyValuePair<string, string>("CustomerServiceRating", jReview["ratings"]["customerService"].ToString()));
        //                                    review.ExtraInfo.Add(new KeyValuePair<string, string>("QualityofWorkRating", jReview["ratings"]["quality"].ToString()));
        //                                    review.ExtraInfo.Add(new KeyValuePair<string, string>("FriendlinessRating", jReview["ratings"]["friendliness"].ToString()));
        //                                    review.ExtraInfo.Add(new KeyValuePair<string, string>("OverallExperienceRating", jReview["ratings"]["overallExperience"].ToString()));
        //                                    review.ExtraInfo.Add(new KeyValuePair<string, string>("PricingRating", jReview["ratings"]["price"].ToString()));

        //                                    ReviewDetails.Add(review);
        //                                }
        //                            }
        //                        }
        //                    }
        //                    else
        //                    {
        //                        returnStatus = ConnectionStatus.WebException;
        //                        ExceptionMessage = reviewDetails.error.message;
        //                    }

        //                }
        //            }
        //        }
        //        else
        //            returnStatus = httpStatus;
        //    }

        //    return returnStatus;
        //}
        
        protected override void dispose()
        {

        }

    }
}
