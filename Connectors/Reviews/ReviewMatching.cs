﻿using SI;
using SI.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace Connectors.Reviews
{
    public static class ReviewMatching
    {
        private static Regex _validWordCharsRegex = new Regex("[^a-zA-Z0-9 -]", RegexOptions.Compiled);

        public static bool ReviewsMatch(ReviewDTO r1, List<string> r1Words, ReviewDTO r2, List<string> r2Words, int maxDaysDifferenceForDateMatch)
        {
            if (r1.ReviewDate == null) r1.ReviewDate = SIDateTime.Now;
            if (r2.ReviewDate == null) r2.ReviewDate = SIDateTime.Now;

            if (r1.ReviewerName == null) r1.ReviewerName = "";
            if (r2.ReviewerName == null) r2.ReviewerName = "";

            double textMatchRating = GetTextMatchRating(r1Words, r2Words);
            int daysBetweenDates = Convert.ToInt32(Math.Abs((r1.ReviewDate - r2.ReviewDate).TotalDays));
            bool datesMatch = (daysBetweenDates <= maxDaysDifferenceForDateMatch);
            bool reviewerNameMatch = r1.ReviewerName.Trim().ToLower() == r2.ReviewerName.Trim().ToLower();
            bool ratingMatch = (Convert.ToInt32(r1.Rating * 10) == Convert.ToInt32(r2.Rating * 10));


            if (datesMatch && textMatchRating == 1.0) return true;

            if (datesMatch && reviewerNameMatch && textMatchRating >= .80) return true;

            if (datesMatch && reviewerNameMatch && ratingMatch && textMatchRating >= .70) return true;

            return false;
        }

        /// <summary>
        /// Gets a word list for matching purposes.  Uses the top 20 longest words found, in 
        /// the order in which each word was first found in original text.
        /// </summary>
        public static List<string> GetWordList(string rawText)
        {
            if (string.IsNullOrWhiteSpace(rawText))
                return new List<string>();

            // 20 longest distinct words
            List<string> list = (
                from l in _validWordCharsRegex.Replace(rawText.ToLower(), "").Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries)
                orderby l.Length descending
                select l
            ).Distinct().Take(20).ToList();

            // ordered by first instance in original text
            list = (
                from l in list
                orderby rawText.IndexOf(l)
                select l
            ).ToList();

            return list;
        }

        /// <summary>
        /// Finds the text match rating.  If the word lists match in order and content
        /// exactly, it's 1.0.  Otherwise the rating is the percentage of words that
        /// match between each list minus 0.1.
        /// </summary>
        public static double GetTextMatchRating(List<string> words1, List<string> words2)
        {
            if (string.Join("|", words1) == string.Join("|", words2)) return 1.0;

            int matched = 0;
            foreach (string word in words1)
            {
                if (words2.Contains(word))
                    matched++;
            }

            return (Convert.ToDouble(matched) / Convert.ToDouble(words1.Count())) - .1;
        }
    }
}
