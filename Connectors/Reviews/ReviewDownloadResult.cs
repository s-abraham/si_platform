﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SI;
using SI.DTO;

namespace Connectors
{
    public class ReviewDownloadResult
    {
        public ReviewDownloadResult()
        {
            Status = StatusEnum.None;
            HeaderHasNoRating = false;
            HeaderHasNoReviewCount = false;
            SalesAverageRating = 0;
            SalesReviewCount = 0;
            ServiceAverageRating = 0;
            ServiceReviewCount = 0;
            FailureReasons = new List<string>();
            Exception = null;
            ReviewDetails = new List<ReviewDetail>();
            ExtraInfo = new List<KeyValuePair<string, string>>();
            HeaderHasNoServiceRating = true;
        }

        public StatusEnum Status { get; set; }

        public bool HeaderHasNoReviewCount { get; set; }
        public bool HeaderHasNoRating { get; set; }

        public decimal SalesAverageRating { get; set; }
        public int SalesReviewCount { get; set; }

        #region service 

        public bool HeaderHasNoServiceReviewCount { get; set; }
        public bool HeaderHasNoServiceRating { get; set; }

        public decimal ServiceAverageRating { get; set; }
        public int ServiceReviewCount { get; set; }

        #endregion

        public List<string> FailureReasons { get; set; }
        public Exception Exception { get; set; }

        public List<ReviewDetail> ReviewDetails { get; set; }
        public List<KeyValuePair<string, string>> ExtraInfo { get; set; }

        public class ReviewDetail
        {
            public string Type { get; set; }
            public decimal Rating { get; set; }
            public string ReviewerName { get; set; }
            public SIDateTime ReviewDate { get; set; }
            public string ReviewTextFull { get; set; }
            public string FullReviewURL { get; set; }
            public List<KeyValuePair<string, string>> ExtraInfo { get; set; }
            public List<ReviewComment> Comments { get; set; }
            public int language { get; set; }
            public ReviewDetail()
            {
                ExtraInfo = new List<KeyValuePair<string, string>>();
                Comments = new List<ReviewComment>();
                language = Convert.ToInt32(LanguageEnum.English);
            }
        }

        public class ReviewComment
        {
            public SIDateTime CommentDate { get; set; }
            public string CommenterName { get; set; }
            public string Comment { get; set; }
        }

        public enum StatusEnum
        {
            None = 0,
            Success = 1,
            Exception = 2,
            InvalidFormat = 3,
            Timeout = 4
        }

    }
}
