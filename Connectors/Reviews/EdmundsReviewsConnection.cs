﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Text;
using Connectors.Parameters;
using Connectors.Reviews;
using HtmlAgilityPack;
using SI.DTO;
using SI.Extensions;

namespace Connectors
{
    public class EdmundsReviewsConnection : SIReviewConnection
    {
        private SIEdmundsReviewConnectionParameters ConnectionParameters;

        #region Initializer
        public EdmundsReviewsConnection(SIEdmundsReviewConnectionParameters siEdmundsReviewConnectionParameters)
            : base(siEdmundsReviewConnectionParameters)
        {
            ConnectionParameters = siEdmundsReviewConnectionParameters;
        }
        #endregion

        #region Protected Methods

        protected override DownloadDataBucket DoDownload(ReviewUrlDTO url, List<string> failureReasons)
        {
            HtmlDocument doc = DownloadDocument(url.HtmlURL, null, 3, 2000);

            // if html fails, always fail with a null return. no html header, no go
            if (doc == null) return null;

            EdmundsBucket bucket = new EdmundsBucket() { HtmlDocument = doc };

#if DEBUG
            if (string.IsNullOrWhiteSpace(url.ExternalID))
            {
                string EdmundsDealerIDXPath = "//a[contains(@class,'button small')]";
                url.ExternalID = SelectSingleNode(doc.DocumentNode, EdmundsDealerIDXPath).GetAttributeValue("data-merchant-id", "");
            }
#endif

            if (!string.IsNullOrWhiteSpace(url.ExternalID))
            {
                string apiURL = string.Format(ConnectionParameters.XPathParameters.APIURLTemplate, url.ExternalID);
                string apiString = DownloadString(apiURL, null);

                if (!string.IsNullOrWhiteSpace(apiString))
                {
                    if (!apiString.ToLower().Contains("{}"))
                    {
                        try
                        {
                            bucket.ApiJsonData = Newtonsoft.Json.JsonConvert.DeserializeObject<Connectors.Entities.EdmundsEntities.Edmunds>(apiString);
                        }
                        catch (Exception)
                        {
                            // for now do nothing if we cannot parse the api json...
                            bucket.ApiJsonData = null;
                        }
                    }
                }
            }

            return bucket;
        }

        protected override SI.DTO.ReviewUrlDTO TweakUrl(SI.DTO.ReviewUrlDTO url)
        {
            return url;
        }

        protected override bool CheckHasNoServiceReviews(DownloadDataBucket bucket, List<string> failureReasons)
        {
            HtmlDocument doc = bucket.HtmlDocument;

            bool result = false;

            HtmlNode node = SelectSingleNode(doc.DocumentNode, ConnectionParameters.XPathParameters.NoServicesReviewTextXPath, true, null);
            if (node != null)
            {
                if (!string.IsNullOrWhiteSpace(ConnectionParameters.XPathParameters.NoServicesReviewText) ||
                        !string.IsNullOrWhiteSpace(ConnectionParameters.XPathParameters.NoServicesReviewText2))
                {
                    if (string.IsNullOrWhiteSpace(node.InnerText))
                    {
                        result = false;
                    }
                    else
                    {
                        if (node.InnerText.Trim().ToLower().Contains(ConnectionParameters.XPathParameters.NoServicesReviewText.Trim().ToLower()) ||
                                node.InnerText.Trim().ToLower().Contains(ConnectionParameters.XPathParameters.NoServicesReviewText2.Trim().ToLower()))
                        {
                            result = true;
                        }
                    }
                }
                else
                {
                    result = true;
                }
            }

            return result;
        }

        protected override bool CheckHasCheckNoReviews(DownloadDataBucket bucket, List<string> failureReasons)
        {
            HtmlDocument doc = bucket.HtmlDocument;

            bool result = false;

            HtmlNode node = SelectSingleNode(doc.DocumentNode, ConnectionParameters.XPathParameters.NoSalesReviewTextXPath, true, null);
            if (node != null)
            {
                if (!string.IsNullOrWhiteSpace(ConnectionParameters.XPathParameters.NoSalesReviewText) ||
                        !string.IsNullOrWhiteSpace(ConnectionParameters.XPathParameters.NoSalesReviewText2))
                {
                    if (string.IsNullOrWhiteSpace(node.InnerText))
                    {
                        result = false;
                    }
                    else
                    {
                        if (node.InnerText.Trim().ToLower().Contains(ConnectionParameters.XPathParameters.NoSalesReviewText.Trim().ToLower()) ||
                                node.InnerText.Trim().ToLower().Contains(ConnectionParameters.XPathParameters.NoSalesReviewText2.Trim().ToLower()))
                        {
                            result = true;
                        }
                    }
                }
                else
                {
                    result = true;
                }
            }

            return result;
        }

        protected override bool CheckHasNoServiceRating(DownloadDataBucket bucket, List<string> failureReasons)
        {
            bool result = false;
            HtmlDocument doc = bucket.HtmlDocument;

            HtmlNode node = SelectSingleNode(doc.DocumentNode, ConnectionParameters.XPathParameters.NoServiceRatingPath, true, null);

            if (node != null)
            {
                if (!string.IsNullOrWhiteSpace(ConnectionParameters.XPathParameters.NoServiceRatingText))
                {
                    if (string.IsNullOrWhiteSpace(node.InnerText))
                    {
                        result = false;
                    }
                    else
                    {
                        //string rating = node.InnerText.Trim().Replace("Rated:", "").Trim();
                        if (node.InnerText.Trim().ToLower().Contains(ConnectionParameters.XPathParameters.NoServiceRatingText.Trim().ToLower()))
                        {
                            result = true;
                        }
                    }
                }
            }

            return result;
        }

        protected override bool CheckHasNoRating(DownloadDataBucket bucket, List<string> failureReasons)
        {
            HtmlDocument doc = bucket.HtmlDocument;

            bool result = false;
            HtmlNode node = SelectSingleNode(doc.DocumentNode, ConnectionParameters.XPathParameters.NoRatingPath, true, null);
            if (node != null)
            {
                if (!string.IsNullOrWhiteSpace(ConnectionParameters.XPathParameters.NoSalesReviewText) ||
                        !string.IsNullOrWhiteSpace(ConnectionParameters.XPathParameters.NoSalesReviewText2))
                {
                    if (string.IsNullOrWhiteSpace(node.InnerText))
                    {
                        result = false;
                    }
                    else
                    {
                        if (node.InnerText.Trim().ToLower().Contains(ConnectionParameters.XPathParameters.NoSalesReviewText.Trim().ToLower()) ||
                                node.InnerText.Trim().ToLower().Contains(ConnectionParameters.XPathParameters.NoSalesReviewText2.Trim().ToLower()))
                        {
                            result = true;
                        }
                    }
                }
                else
                {
                    result = true;
                }
            }

            return result;
        }

        protected override int ScrapeServiceReviewCount(DownloadDataBucket bucket, List<string> failureReasons)
        {
            HtmlDocument doc = bucket.HtmlDocument;

            int reviewCount = 0;

            HtmlNode node = SelectSingleNode(doc.DocumentNode, ConnectionParameters.XPathParameters.ReviewCountServicePath, true, null);
            if (node != null)
            {
                if (!string.IsNullOrWhiteSpace(node.InnerText))
                {
                    int.TryParse(node.InnerText.Replace(" reviews", ""), out reviewCount);
                }
            }

            return reviewCount;
        }

        protected override int ScrapeReviewCount(DownloadDataBucket bucket, List<string> failureReasons)
        {
            HtmlDocument doc = bucket.HtmlDocument;

            int reviewCount = 0;

            HtmlNode node = SelectSingleNode(doc.DocumentNode, ConnectionParameters.XPathParameters.ReviewCountPath, true, null);
            if (node != null)
            {
                if (!string.IsNullOrWhiteSpace(node.InnerText))
                {
                    int.TryParse(node.InnerText.Replace("reviews", "").Replace("review", ""), out reviewCount);
                }
            }

            return reviewCount;
        }

        protected override decimal ScrapeServiceRating(DownloadDataBucket bucket, List<string> failureReasons)
        {
            HtmlDocument doc = bucket.HtmlDocument;

            decimal rating = 0;

            HtmlNode node = SelectSingleNode(doc.DocumentNode, ConnectionParameters.XPathParameters.RatingValueServicePath, true, null);
            if (node != null)
            {
                if (!string.IsNullOrWhiteSpace(ConnectionParameters.XPathParameters.RatingValueServicePath))
                {
                    if (!string.IsNullOrWhiteSpace(node.GetAttributeValue("title", "")))
                    {
                        string rtext = node.GetAttributeValue("title", "").ToLower();
                        decimal.TryParse(rtext, out rating);
                    }
                }
            }

            return rating;
        }

        protected override decimal ScrapeRating(DownloadDataBucket bucket, List<string> failureReasons)
        {
            HtmlDocument doc = bucket.HtmlDocument;

            decimal rating = 0;

            HtmlNode node = SelectSingleNode(doc.DocumentNode, BaseParameters.BaseXPaths.RatingValuePath, true, null);
            if (node != null)
            {
                if (!string.IsNullOrWhiteSpace(BaseParameters.BaseXPaths.RatingValuePath))
                {
                    if (!string.IsNullOrWhiteSpace(node.GetAttributeValue("title", "")))
                    {
                        string rtext = node.GetAttributeValue("title", "").ToLower();
                        decimal.TryParse(rtext, out rating);
                    }
                }
            }

            return rating;
        }

        protected override List<KeyValuePair<string, string>> ScrapeExtraInfo(DownloadDataBucket bucket, List<string> failureReasons)
        {
            HtmlDocument doc = bucket.HtmlDocument;

            List<KeyValuePair<string, string>> ListkeyvaluePair = new List<KeyValuePair<string, string>>();
            List<string> failedXPaths = new List<string>();
            try
            {
                string value = string.Empty;
                string key = string.Empty;

                HtmlNode node = SelectSingleNode(doc.DocumentNode, ConnectionParameters.XPathParameters.ExtraValueSalesPath);
                if (node != null)
                {
                    value = node.InnerText.Trim();
                    key = "Sales";

                    ListkeyvaluePair.Add(new KeyValuePair<string, string>(key, value));
                }

                HtmlNode node1 = SelectSingleNode(doc.DocumentNode, ConnectionParameters.XPathParameters.ExtraValueServicesPath);
                if (node != null)
                {
                    value = node.InnerText.Trim();
                    key = "Services";

                    ListkeyvaluePair.Add(new KeyValuePair<string, string>(key, value));
                }

            }
            catch (Exception ex)
            {

            }

            return ListkeyvaluePair;

        }

        protected override List<ReviewDownloadResult.ReviewDetail> ScrapeDetails(DownloadDataBucket bucket, SI.DTO.ReviewUrlDTO originalUrl, List<string> failureReasons)
        {
            List<ReviewDownloadResult.ReviewDetail> details = null;

            EdmundsBucket myBucket = (EdmundsBucket)bucket;
            if (myBucket.ApiJsonData != null)
            {
                details = scrapeDetails(myBucket.ApiJsonData);
            }

            return details;
        }
        #endregion

        #region private Methods/Class

        private class EdmundsBucket : DownloadDataBucket
        {
            public EdmundsBucket()
            {
                ApiJsonData = null;
            }
            public Connectors.Entities.EdmundsEntities.Edmunds ApiJsonData { get; set; }
        }

        private List<ReviewDownloadResult.ReviewDetail> scrapeDetails(Connectors.Entities.EdmundsEntities.Edmunds objEdmunds)
        {
            List<ReviewDownloadResult.ReviewDetail> details = new List<ReviewDownloadResult.ReviewDetail>();

            if (objEdmunds != null)
            {
                #region SALES

                foreach (var salesReview in objEdmunds.salesReviews)
                {
                    ReviewDownloadResult.ReviewDetail review = new ReviewDownloadResult.ReviewDetail();

                    review.Type = "Sales";
                    review.Rating = salesReview.totalRating;
                    review.ReviewerName = salesReview.consumerName;
                    review.ReviewDate = new SI.SIDateTime(Convert.ToDateTime(salesReview.date.ToShortDateString()), 0);
                    review.ReviewTextFull = salesReview.reviewBody;
                    review.ExtraInfo.Add(new KeyValuePair<string, string>("SalesRecommend", salesReview.recommendedDealer.ToLower()));

                    List<ReviewDownloadResult.ReviewComment> comments = new List<ReviewDownloadResult.ReviewComment>();
                    foreach (var Reviewcomment in salesReview.comments)
                    {
                        ReviewDownloadResult.ReviewComment comment = new ReviewDownloadResult.ReviewComment();

                        comment.Comment = Reviewcomment.body;
                        comment.CommentDate = new SI.SIDateTime(Convert.ToDateTime(Reviewcomment.date.ToShortDateString()), 0);

                        comments.Add(comment);
                    }

                    review.Comments = comments;

                    details.Add(review);


                }
                #endregion

                #region SERVICE

                foreach (var serviceReviews in objEdmunds.serviceReviews)
                {
                    ReviewDownloadResult.ReviewDetail review = new ReviewDownloadResult.ReviewDetail();

                    review.Type = "Service";
                    review.Rating = serviceReviews.totalRating;
                    review.ReviewerName = serviceReviews.consumerName;
                    review.ReviewDate = new SI.SIDateTime(Convert.ToDateTime(serviceReviews.date.ToShortDateString()), 0);
                    review.ReviewTextFull = serviceReviews.reviewBody;
                    review.ExtraInfo.Add(new KeyValuePair<string, string>("ServiceRecommend", serviceReviews.recommendedDealer.ToLower()));

                    List<ReviewDownloadResult.ReviewComment> comments = new List<ReviewDownloadResult.ReviewComment>();
                    foreach (var Reviewcomment in serviceReviews.comments)
                    {
                        ReviewDownloadResult.ReviewComment comment = new ReviewDownloadResult.ReviewComment();

                        comment.Comment = Reviewcomment.body;
                        comment.CommentDate = new SI.SIDateTime(Convert.ToDateTime(Reviewcomment.date.ToShortDateString()), 0);

                        comments.Add(comment);
                    }

                    review.Comments = comments;

                    details.Add(review);
                }
                #endregion
            }
            return details;
        }
        #endregion

        /*

		#region Edmunds API
		/// <summary>
		/// Call the Edmunds Review API and extract the rating and review count
		/// </summary>
		/// <param name="includeDetails">To include the review details.</param>
		/// <returns>Populates the object Ratings and Review count</returns>
		public ConnectionStatus GetReviewsAPI(bool includeDetails = true)
		{
			ConnectionStatus returnStatus = ConnectionStatus.InvalidData;

			if (!string.IsNullOrEmpty(ConnectionParameters.ApiRequestUrl))
			{
				HttpRequestResults = string.Empty;

				ConnectionStatus httpStatus = ConnectorHttpRequest(ConnectionParameters.ApiRequestUrl);
				if (httpStatus == ConnectionStatus.Success)
				{
					if (!string.IsNullOrEmpty(HttpRequestResults))
					{
						dynamic reviewDetails = Newtonsoft.Json.JsonConvert.DeserializeObject<dynamic>(HttpRequestResults);
						if (reviewDetails != null)
						{
							returnStatus = ConnectionStatus.Success;

							if (reviewDetails.salesReviews != null && reviewDetails.salesReviews.Count > 0)
							{
								SalesAverageRating = (decimal)reviewDetails.salesReviews[0].averageRating;
								SalesReviewCount = reviewDetails.salesReviews.Count;

								if (includeDetails)
								{
									foreach (var salesReview in reviewDetails.salesReviews)
									{
										ReviewDetails.Add(PopulateReview("sales", salesReview));
									}
								}
							}

							if (reviewDetails.serviceReviews != null && reviewDetails.serviceReviews.Count > 0)
							{
								ServiceAverageRating = (decimal)reviewDetails.serviceReviews[0].averageRating;
								ServiceReviewCount = reviewDetails.serviceReviews.Count;

								if (includeDetails)
								{
									foreach (var serviceReview in reviewDetails.serviceReviews)
									{
										ReviewDetails.Add(PopulateReview("service", serviceReview));
									}
								}
							}
						}
					}
				}
				else
					returnStatus = httpStatus;
			}

			return returnStatus;
		}

		private Review PopulateReview(string reviewType, dynamic dealerReview)
		{
			Review review = new Review();
			review.Type = reviewType;
			review.Rating = dealerReview.totalRating;
			review.ReviewerName = dealerReview.consumerName;
			review.ReviewDate = ((string)dealerReview.date).ToDateTime();
			review.ReviewTextFull = ((string)dealerReview.reviewBody).StripHtml();

			review.ExtraInfo.Add(new KeyValuePair<string, string>("SalesRecommend", ((string)dealerReview.recommendedDealer).ToLower()));

			if (dealerReview.comments.Count > 0)
			{
				foreach (var comment in dealerReview.comments)
				{
					ReviewComment reviewComment = new ReviewComment();
					reviewComment.CommentDate = ((string)comment.date).ToDateTime();
					reviewComment.Comment = comment.body;

					review.Comments.Add(reviewComment);
				}
			}

			return review;
		}
		#endregion

		#region Edmunds HTML
		/// <summary>
		/// 
		/// </summary>
		/// <param name="includeDetails"></param>
		/// <returns></returns>
		public ConnectionStatus GetReviewsHtml(bool includeDetails = true)
        {
			ConnectionStatus returnStatus = ConnectionStatus.InvalidData;

			if (!string.IsNullOrEmpty(ConnectionParameters.XPathParameters.ReviewCountCollectedSalesParentXPath) ||
				!string.IsNullOrEmpty(ConnectionParameters.XPathParameters.ReviewCountCollectedServiceParentXPath) ||
				!string.IsNullOrEmpty(ConnectionParameters.XPathParameters.SalesExtraInfoXPath)) 
            {

				if (!string.IsNullOrEmpty(ConnectionParameters.HtmlRequestUrl))
                {
                    HttpRequestResults = string.Empty;

					//make sure on sales page, normalize
					Uri uriRequest = new Uri(ConnectionParameters.HtmlRequestUrl);
					if (uriRequest.Segments.Length > 0 && uriRequest.Segments[uriRequest.Segments.Length-1] != "sales.1.html")
					{
						ConnectionParameters.HtmlRequestUrl = string.Format("{0}://{1}", uriRequest.Scheme, uriRequest.Authority);
						for (int segCount = 0; segCount < uriRequest.Segments.Length; segCount++ )
						{
							ConnectionParameters.HtmlRequestUrl += uriRequest.Segments[segCount];
						}
						ConnectionParameters.HtmlRequestUrl += "/sales.1.html";
					}

					ConnectionStatus httpStatus = ConnectorHttpRequest(ConnectionParameters.HtmlRequestUrl);
					if (httpStatus == ConnectionStatus.Success)
                    {
                        HtmlDocument htmlDocument = new HtmlDocument();
                        htmlDocument.OptionFixNestedTags = true;
                        htmlDocument.LoadHtml(HttpRequestResults);

						if (!string.IsNullOrEmpty(ConnectionParameters.XPathParameters.ReviewCountCollectedSalesParentXPath))
	                    {
							HtmlNode SalesParentNode = SelectSingleNode(htmlDocument.DocumentNode, ConnectionParameters.XPathParameters.ReviewCountCollectedSalesParentXPath);
		                    if (SalesParentNode != null)
		                    {
			                    SalesReviewCount = 0;
			                    SalesAverageRating = 0;

								HtmlNode SalesCountChildNode = SelectSingleNode(SalesParentNode, ConnectionParameters.XPathParameters.ReviewCountCollectedSalesXPath);
			                    if (SalesCountChildNode != null)
				                    SalesReviewCount = SalesCountChildNode.InnerText.IsNullOptional("0").Trim().Replace("reviews", "").ToInteger();

								HtmlNode SalesRatingChildNode = SelectSingleNode(SalesParentNode, ConnectionParameters.XPathParameters.RatingCollectedSalesXPath);
			                    if (SalesRatingChildNode != null)
				                    SalesAverageRating = SalesRatingChildNode.GetAttributeValue("title", "0").ToDecimal();

			                    if (SalesCountChildNode != null || SalesRatingChildNode != null)
									returnStatus = ConnectionStatus.Success;
		                    }
	                    }

						if (!string.IsNullOrEmpty(ConnectionParameters.XPathParameters.ReviewCountCollectedServiceParentXPath))
	                    {
							HtmlNode ServiceParentNode = SelectSingleNode(htmlDocument.DocumentNode, ConnectionParameters.XPathParameters.ReviewCountCollectedServiceParentXPath);
		                    if (ServiceParentNode != null)
		                    {
			                    ServiceReviewCount = 0;
			                    ServiceAverageRating = 0;

								HtmlNode ServiceCountChildNode = SelectSingleNode(ServiceParentNode, ConnectionParameters.XPathParameters.ReviewCountCollectedServiceXPath);
			                    if (ServiceCountChildNode != null)
				                    ServiceReviewCount = ServiceCountChildNode.InnerText.IsNullOptional("0").Trim().Replace("reviews", "").ToInteger();

								HtmlNode ServiceRatingChildNode = SelectSingleNode(ServiceParentNode, ConnectionParameters.XPathParameters.RatingCollectedSalesXPath);
			                    if (ServiceRatingChildNode != null)
				                    ServiceAverageRating = ServiceRatingChildNode.GetAttributeValue("title", "0").ToDecimal();

			                    //if (ServiceCountChildNode != null || ServiceRatingChildNode != null)
								//	returnStatus = ConnectionStatus.Success;
		                    }
	                    }

						if (!string.IsNullOrEmpty(ConnectionParameters.XPathParameters.SalesExtraInfoXPath) && !string.IsNullOrEmpty(ConnectionParameters.XPathParameters.ReviewCountCollectedSalesParentXPath))
	                    {
							HtmlNode SalesParentNode = SelectSingleNode(htmlDocument.DocumentNode, ConnectionParameters.XPathParameters.ReviewCountCollectedSalesParentXPath);
		                    if (SalesParentNode != null)
		                    {
								HtmlNode SalesExtraInfoNode = SelectSingleNode(SalesParentNode, ConnectionParameters.XPathParameters.SalesExtraInfoXPath);
								if (SalesExtraInfoNode != null)
								{
									string salesExtraInfo = SalesExtraInfoNode.InnerText.IsNullOptional("");
									if (!string.IsNullOrEmpty(salesExtraInfo))
									{
										string tempNo = salesExtraInfo.Replace("Recommend:", "");
										tempNo = tempNo.Substring(tempNo.IndexOf("No")).Replace("No", "").Replace("(", "").Replace(")", "").Replace("\n", "").Trim();

										string tempYes = salesExtraInfo.Replace("Recommend:", "");
										tempYes = tempYes.Substring(0, tempYes.IndexOf("No")).Replace("Yes", "").Replace("(", "").Replace(")", "").Replace("\n", "").Trim();
										
										ExtraInfo.Add(new KeyValuePair<string, string>("SalesRecommend_Yes", tempYes));
										ExtraInfo.Add(new KeyValuePair<string, string>("SalesRecommend_No", tempNo));

										//returnStatus = ConnectionStatus.Success;
									}
								}
		                    }
	                    }

						if (!string.IsNullOrEmpty(ConnectionParameters.XPathParameters.ServiceExtraInfoXPath) && !string.IsNullOrEmpty(ConnectionParameters.XPathParameters.ReviewCountCollectedServiceParentXPath))
						{
							HtmlNode ServiceParentNode = SelectSingleNode(htmlDocument.DocumentNode, ConnectionParameters.XPathParameters.ReviewCountCollectedServiceParentXPath);
							if (ServiceParentNode != null)
							{
								HtmlNode ServiceExtraInfoNode = SelectSingleNode(ServiceParentNode, ConnectionParameters.XPathParameters.ServiceExtraInfoXPath);
								if (ServiceExtraInfoNode != null)
								{
									string serviceExtraInfo = ServiceExtraInfoNode.InnerText.IsNullOptional("");
									if (!string.IsNullOrEmpty(serviceExtraInfo))
									{
										string tempNo = serviceExtraInfo.Replace("Recommend:", "");
										tempNo = tempNo.Substring(tempNo.IndexOf("No")).Replace("No", "").Replace("(", "").Replace(")", "").Replace("\n", "").Trim();

										string tempYes = serviceExtraInfo.Replace("Recommend:", "");
										tempYes = tempYes.Substring(0, tempYes.IndexOf("No")).Replace("Yes", "").Replace("(", "").Replace(")", "").Replace("\n", "").Trim();

										ExtraInfo.Add(new KeyValuePair<string, string>("ServiceRecommend_Yes", tempYes));
										ExtraInfo.Add(new KeyValuePair<string, string>("ServiceRecommend_No", tempNo));

										//returnStatus = ConnectionStatus.Success;
									}
								}
							}
						}

						//Sales reviews
						if (includeDetails && !string.IsNullOrEmpty(ConnectionParameters.XPathParameters.ParentnodeReviewDetailsXPath) && SalesReviewCount > 0)
	                    {

							int numberOfPages = SalesReviewCount / ConnectionParameters.XPathParameters.ReviewsPerPage;
							if ((SalesReviewCount % ConnectionParameters.XPathParameters.ReviewsPerPage) > 0)
			                    numberOfPages++;

		                    if (numberOfPages == 0)
			                    numberOfPages = 1;

		                    for (int pageCount = 0; pageCount < numberOfPages; pageCount++)
		                    {
			                    if (pageCount > 0)
			                    {
									string pageRequestUrl = string.Format("{0}sales.{1}.html", ConnectionParameters.HtmlRequestUrl.Replace("sales.1.html", ""), pageCount + 1);
									if (ConnectorHttpRequest(pageRequestUrl) != ConnectionStatus.Success) break;
				                    htmlDocument.LoadHtml(HttpRequestResults);
			                    }

								HtmlNodeCollection ReviewParentNodes = SelectNodes(htmlDocument.DocumentNode, ConnectionParameters.XPathParameters.ParentnodeReviewDetailsXPath);
			                    if (ReviewParentNodes != null)
			                    {
				                    foreach (HtmlNode reviewParentNode in ReviewParentNodes)
				                    {
					                    Review review = new Review();
					                    review.Type = "sales";

					                    review.Rating = 0;
										HtmlNode ReviewRatingNode = SelectSingleNode(reviewParentNode, ConnectionParameters.XPathParameters.RatingValueXPath);
					                    if (ReviewRatingNode != null)
						                    review.Rating = ReviewRatingNode.GetAttributeValue("title", "0").ToDecimal();

										HtmlNode ReviewReviewerNode = SelectSingleNode(reviewParentNode, ConnectionParameters.XPathParameters.ReviewerNameXPath);
					                    if (ReviewReviewerNode != null)
						                    review.ReviewerName = ReviewReviewerNode.InnerText.IsNullOptional("");

										HtmlNode ReviewReviewDateNode = SelectSingleNode(reviewParentNode, ConnectionParameters.XPathParameters.ReviewDateXPath);
					                    if (ReviewReviewDateNode != null)
						                    review.ReviewDate = ReviewReviewDateNode.InnerText.IsNullOptional("").Replace(" PST", "").Replace(" PDT", "").ToDateTime();

										HtmlNode ReviewFullTextNode = SelectSingleNode(reviewParentNode, ConnectionParameters.XPathParameters.ReviewTextFullURLXPath);
					                    if (ReviewFullTextNode != null)
					                    {
						                    string FullTextURL = string.Format("http://www.edmunds.com{0}", ReviewFullTextNode.GetAttributeValue("href", ""));
						                    review.FullReviewURL = FullTextURL;

						                    HtmlDocument htmlFTDocument = ConnectorHtmlDocument(FullTextURL);
						                    if (htmlFTDocument != null)
						                    {
												HtmlNode FullTextReviewNode = SelectSingleNode(htmlFTDocument.DocumentNode, ConnectionParameters.XPathParameters.ReviewFullTextXPath);
							                    if (FullTextReviewNode != null)
								                    review.ReviewTextFull = FullTextReviewNode.InnerText.IsNullOptional("").Trim();

												HtmlNodeCollection CommentNodes = SelectNodes(htmlFTDocument.DocumentNode, ConnectionParameters.XPathParameters.ReviewCommentsXPath);
							                    if (CommentNodes != null)
							                    {
								                    foreach (HtmlNode commentNode in CommentNodes)
								                    {
									                    ReviewComment reviewComment = new ReviewComment();

														HtmlNode DateNode = SelectSingleNode(commentNode, ConnectionParameters.XPathParameters.ReviewCommentDateXPath);
									                    if (DateNode != null)
										                    reviewComment.CommentDate = DateNode.InnerText.IsNullOptional("").ToDateTime();

									                    HtmlNode CommentTextNode = SelectSingleNode(commentNode, "p");
									                    if (CommentTextNode != null)
										                    reviewComment.Comment = CommentTextNode.InnerText.IsNullOptional("");

									                    review.Comments.Add(reviewComment);
								                    }
							                    }
						                    }
					                    }

					                    //extra info
										HtmlNode TitleNode = SelectSingleNode(reviewParentNode, ConnectionParameters.XPathParameters.ReviewTitleExtraInfoXPath);
					                    if (TitleNode != null)
						                    review.ExtraInfo.Add(new KeyValuePair<string, string>("Title", TitleNode.InnerText.IsNullOptional("").Trim()));

										HtmlNode RecommendationNode = SelectSingleNode(reviewParentNode, ConnectionParameters.XPathParameters.ReviewRecommendationExtraInfoXPath);
					                    if (RecommendationNode != null)
						                    review.ExtraInfo.Add(new KeyValuePair<string, string>("PositiveRecommendation", RecommendationNode.InnerText.IsNullOptional("").Trim()));

										HtmlNode PurchasedVehicleNode = SelectSingleNode(reviewParentNode, ConnectionParameters.XPathParameters.ReviewPurchasedVehicleExtraInfoXPath);
					                    if (PurchasedVehicleNode != null)
						                    review.ExtraInfo.Add(new KeyValuePair<string, string>("PurchasedVehicle", PurchasedVehicleNode.InnerText.IsNullOptional("").Trim()));

					                    ReviewDetails.Add(review);
				                    }
			                    }
		                    }

	                    }

						//service reviews
						if (includeDetails && !string.IsNullOrEmpty(ConnectionParameters.XPathParameters.ParentnodeSeviceReviewDetailsXPath) && ServiceReviewCount > 0)
						{
							int numberOfPages = ServiceReviewCount / ConnectionParameters.XPathParameters.ReviewsPerPage;
							if ((ServiceReviewCount % ConnectionParameters.XPathParameters.ReviewsPerPage) > 0)
			                    numberOfPages++;

		                    if (numberOfPages == 0)
			                    numberOfPages = 1;

							for (int pageCount = 0; pageCount < numberOfPages; pageCount++)
							{
								//load page
								string pageRequestUrl = string.Format("{0}service.{1}.html", ConnectionParameters.HtmlRequestUrl.Replace("sales.1.html", ""), pageCount + 1);
								if (ConnectorHttpRequest(pageRequestUrl) != ConnectionStatus.Success) break;
								htmlDocument.LoadHtml(HttpRequestResults);

								HtmlNodeCollection ReviewParentNodes = SelectNodes(htmlDocument.DocumentNode, ConnectionParameters.XPathParameters.ParentnodeSeviceReviewDetailsXPath);
								if (ReviewParentNodes != null)
								{
									foreach (HtmlNode reviewParentNode in ReviewParentNodes)
									{
										Review review = new Review();
										review.Type = "service";

										review.Rating = 0;
										HtmlNode ReviewRatingNode = SelectSingleNode(reviewParentNode, ConnectionParameters.XPathParameters.RatingValueXPath);
										if (ReviewRatingNode != null)
											review.Rating = ReviewRatingNode.GetAttributeValue("title", "0").ToDecimal();

										HtmlNode ReviewReviewerNode = SelectSingleNode(reviewParentNode, ConnectionParameters.XPathParameters.ReviewerNameXPath);
										if (ReviewReviewerNode != null)
											review.ReviewerName = ReviewReviewerNode.InnerText.IsNullOptional("");

										HtmlNode ReviewReviewDateNode = SelectSingleNode(reviewParentNode, ConnectionParameters.XPathParameters.ReviewDateXPath);
										if (ReviewReviewDateNode != null)
											review.ReviewDate = ReviewReviewDateNode.InnerText.IsNullOptional("").Replace(" PST", "").Replace(" PDT", "").ToDateTime();

										HtmlNode ReviewFullTextNode = SelectSingleNode(reviewParentNode, ConnectionParameters.XPathParameters.ServiceReviewTextFullURLXPath);
										if (ReviewFullTextNode != null)
										{
											string FullTextURL = string.Format("http://www.edmunds.com{0}", ReviewFullTextNode.GetAttributeValue("href", ""));
											review.FullReviewURL = FullTextURL;

											HtmlDocument htmlFTDocument = ConnectorHtmlDocument(FullTextURL);
											if (htmlFTDocument != null)
											{
												HtmlNode FullTextReviewNode = SelectSingleNode(htmlFTDocument.DocumentNode, ConnectionParameters.XPathParameters.ServiceReviewFullTextXPath);
												if (FullTextReviewNode != null)
													review.ReviewTextFull = FullTextReviewNode.InnerText.IsNullOptional("").Trim();

												HtmlNodeCollection CommentNodes = SelectNodes(htmlFTDocument.DocumentNode, ConnectionParameters.XPathParameters.ReviewCommentsXPath);
												if (CommentNodes != null)
												{
													foreach (HtmlNode commentNode in CommentNodes)
													{
														ReviewComment reviewComment = new ReviewComment();

														HtmlNode DateNode = SelectSingleNode(commentNode, ConnectionParameters.XPathParameters.ReviewCommentDateXPath);
														if (DateNode != null)
															reviewComment.CommentDate = DateNode.InnerText.IsNullOptional("").ToDateTime();

														HtmlNode CommentTextNode = SelectSingleNode(commentNode, "p");
														if (CommentTextNode != null)
															reviewComment.Comment = CommentTextNode.InnerText.IsNullOptional("");

														review.Comments.Add(reviewComment);
													}
												}
											}
										}

										//extra info
										HtmlNode TitleNode = SelectSingleNode(reviewParentNode, ConnectionParameters.XPathParameters.ReviewTitleExtraInfoXPath);
										if (TitleNode != null)
											review.ExtraInfo.Add(new KeyValuePair<string, string>("Title", TitleNode.InnerText.IsNullOptional("").Trim()));

										HtmlNode RecommendationNode = SelectSingleNode(reviewParentNode, ConnectionParameters.XPathParameters.ReviewRecommendationExtraInfoXPath);
										if (RecommendationNode != null)
											review.ExtraInfo.Add(new KeyValuePair<string, string>("PositiveRecommendation", RecommendationNode.InnerText.IsNullOptional("").Trim()));
									
										//there are other extra info on the page, do we want??

										ReviewDetails.Add(review);
									}
								}
							}
						}
                    }
                    else
                        returnStatus = httpStatus;
                }
            }

            return returnStatus;
        }
		#endregion

        */
        protected override void dispose()
        {
            // nothing to do here
        }



    }
}
