﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Connectors.Parameters;
using Connectors.URL_Validators.Parameters;
using HtmlAgilityPack;

namespace Connectors
{
    public class YahooLocalReviewURLValidatorConnection : SIReviewURLValidatorConnection
    {
        public YahooLocalReviewURLValidatorConnection(YahooReviewURLValidatorParameters cp)
            : base(cp)
        {
            Parameters = cp;
        }

        public YahooReviewURLValidatorParameters Parameters { get; set; }


        protected override int numberOfHarvestMethods()
        {
            return 1;
        }

        protected override List<string> harvestReviewURL(int methodNumber)
        {
            switch (methodNumber)
            {
                case 1:
                    return harvestFromYahooLocal();

                default:
                    break;
            }

            return null;
        }

        private List<string> harvestFromYahooLocal()
        {
            string result = null;

            string queryUrlPath = Parameters.XPaths.QueryBasePath;
            string url = queryUrlPath
                .Replace("{LocationName}", Parameters.CompanyName)
                .Replace("{City}", Parameters.City)
                .Replace("{State}", Parameters.StateFull)
                .Replace("{Zip}", Parameters.Zip)
            ;

            HtmlDocument doc = DownloadDocument(url, null);

            HtmlNodeCollection nodes = doc.DocumentNode.SelectNodes("//div[@class='content'][1]/h3");//.GetAttributeValue("href", "");

            if (nodes.Count > 0)
            {
                HtmlNode htmlnode = nodes[0];
                result = htmlnode.SelectSingleNode(".//a[@class='yschttl spt']").GetAttributeValue("href", "");

                if (result.Contains("**"))
                {
                    result = result.Substring(result.IndexOf("**") + 2).Replace("%3a", ":");
                    return new List<string>() { result };
                }
            }

            return new List<string>(); ;
            
        }


        public override ReviewURLValidationResult ValidateReviewURL(HtmlAgilityPack.HtmlDocument doc)
        {
            ReviewURLValidationResult result = new ReviewURLValidationResult();
            result.IsValidURL = true;

            try
            {
                #region Config

                string headerPath = Parameters.XPaths.HeaderPath;
                string locationNamePath = Parameters.XPaths.LocationNamePath;
                string addressPath = Parameters.XPaths.AddressPath;
                string cityPath = Parameters.XPaths.CityPath;
                string statePath = Parameters.XPaths.StatePath;
                string zipPath = Parameters.XPaths.ZipPath;
                string phonePath = Parameters.XPaths.PhonePath;
                string WebsitetURLPath = Parameters.XPaths.WebsitetURLPath;
                string ParentnodeRatingXPath = Parameters.XPaths.ParentnodeRatingXPath;
                string ParentnodeReviewCountXPath = Parameters.XPaths.ParentnodeReviewCountXPath;
                string ReviewCountCollectedXPath = Parameters.XPaths.ReviewCountCollectedXPath;


                #endregion

                //Xpath = headerPath;
                HtmlNode headerNode = doc.DocumentNode.SelectSingleNode(headerPath);

                #region Details
                try
                {

                    #region AccountDetails
                    try
                    {
                        result.AccountName = headerNode.SelectSingleNode(locationNamePath).GetAttributeValue("content", "");
                    }
                    catch
                    {
                        result.IsValidURL = false;
                        result.Reasons.Add(string.Format("Could not resulve locationNamePath {0}.", locationNamePath));
                    }

                    try
                    {
                        result.AccountDetails.Address = headerNode.SelectSingleNode(addressPath).GetAttributeValue("content", "");
                    }
                    catch
                    {
                        result.AccountDetails.Address = string.Empty;
                        result.IsValidURL = false;
                        result.Reasons.Add(string.Format("Could not resulve addressPath {0}.", addressPath));
                    }

                    try
                    {
                        result.AccountDetails.City = headerNode.SelectSingleNode(cityPath).GetAttributeValue("content", "");
                    }
                    catch
                    {
                        result.AccountDetails.City = string.Empty;
                        result.IsValidURL = false;
                        result.Reasons.Add(string.Format("Could not resulve cityPath {0}.", cityPath));
                    }

                    try
                    {
                        result.AccountDetails.State = headerNode.SelectSingleNode(statePath).GetAttributeValue("content", "");
                    }
                    catch
                    {
                        result.AccountDetails.State = string.Empty;
                        result.IsValidURL = false;
                        result.Reasons.Add(string.Format("Could not resulve statePath {0}.", statePath));
                    }

                    try
                    {
                        result.AccountDetails.Zip = headerNode.SelectSingleNode(zipPath).GetAttributeValue("content", "");
                    }
                    catch
                    {
                        result.AccountDetails.Zip = string.Empty;
                        result.IsValidURL = false;
                        result.Reasons.Add(string.Format("Could not resulve zipPath {0}.", zipPath));
                    }

                    try
                    {
                        result.AccountDetails.Phone = headerNode.SelectSingleNode(phonePath).GetAttributeValue("content", "");

                        if (!string.IsNullOrEmpty(result.AccountDetails.Phone))
                        {
                            result.AccountDetails.Phone = result.AccountDetails.Phone.Replace("(", "").Replace(")", "").Replace("-", "").Replace(" ", "");
                        }
                    }
                    catch
                    {
                        result.AccountDetails.Phone = string.Empty;
                        result.IsValidURL = false;
                        result.Reasons.Add(string.Format("Could not resulve phonePath {0}.", phonePath));
                    }

                    try
                    {
                        result.AccountDetails.WebsitetURL = headerNode.SelectSingleNode(WebsitetURLPath).GetAttributeValue("content", "");
                    }
                    catch
                    {
                        result.AccountDetails.WebsitetURL = string.Empty;
                        result.IsValidURL = false;
                        result.Reasons.Add(string.Format("Could not resulve WebsitetURLPath {0}.", WebsitetURLPath));
                    }
                    #endregion

                    #region ReviewSummary
                    try
                    {
                        HtmlNode reviewNode = doc.DocumentNode.SelectSingleNode(ParentnodeRatingXPath);

                        string ratingclass = reviewNode.GetAttributeValue("class", "");

                        if (ratingclass.Length > 0)
                        {
                            string[] words = ratingclass.Split(' ');

                            foreach (string word in words)
                            {
                                if (word.StartsWith("star"))
                                {
                                    result.ReviewDetails.Rating = Convert.ToDecimal((Convert.ToDecimal(word.Replace("star", "")) / Convert.ToDecimal(2)));
                                    break;
                                }
                            }
                        }
                        else
                        {

                            result.ReviewDetails.Rating = 0;
                        }
                    }
                    catch (Exception ex)
                    {
                        result.ReviewDetails.Rating = 0;
                    }

                    try
                    {                        
                        HtmlNode reviewNode = doc.DocumentNode.SelectSingleNode(ParentnodeReviewCountXPath);
                                                
                        string Reviews = reviewNode.SelectSingleNode(ReviewCountCollectedXPath).InnerText;

                        if (Reviews.Length > 0)
                        {
                            result.ReviewDetails.ReviewCount = Convert.ToInt32(Reviews.Replace(" Reviews", "").Replace(" Review", ""));
                        }
                    }
                    catch (Exception ex)
                    {
                        result.ReviewDetails.ReviewCount = 0;
                    }
                    #endregion

                }
                catch
                {
                    result.AccountName = string.Empty;
                }

                #endregion
            }
            catch
            {

            }

            return result;
        }

        protected override void dispose()
        {
            //do nothing
        }

    }
}
