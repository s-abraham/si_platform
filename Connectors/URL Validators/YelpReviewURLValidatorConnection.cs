﻿using System.Text.RegularExpressions;
using Connectors.Parameters;
using HtmlAgilityPack;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SI.DTO;

namespace Connectors
{
    public class YelpReviewURLValidatorConnection : SIReviewURLValidatorConnection
    {
        public YelpReviewURLValidatorConnection(YelpReviewURLValidatorParameters cp) : base(cp)
        {
            Parameters = cp;
        }

        public YelpReviewURLValidatorParameters Parameters { get; set; }

        public override string HarvestReviewURL()
        {
            return "";
            //string queryUrlPath = Parameters.XPaths.QueryBasePath;
            //string url = queryUrlPath
            //    .Replace("{LocationName}", Parameters.CompanyName)
            //    .Replace("{City}", Parameters.City)
            //    .Replace("{State}", Parameters.StateFull)
            //    .Replace("{Zip}", Parameters.Zip)
            //;

            //HtmlDocument doc = DownloadDocument(url, null);

            //string parentURLPath = Convert.ToString(Parameters.XPaths.ParentURLPath);
            //string urlPath = Convert.ToString(Parameters.XPaths.URLPath);

            //HtmlNode reviewNode = doc.DocumentNode.SelectSingleNode(parentURLPath);

            //if (reviewNode != null)
            //{
            //    string reviewURL = reviewNode.SelectSingleNode(urlPath).GetAttributeValue("href", "");
            //    reviewURL = string.Format("http://www.yelp.com{0}", reviewURL);
            //    return reviewURL;
            //}
            //else
            //{
            //    return null;
            //}

        }

        public override ReviewURLValidationResult ExtractPageInfo(HtmlDocument responseDoc)
        {

            var result = new ReviewURLValidationResult();

            // Determine if Page is valid and retrievable

            if (responseDoc == null)
            {
                result.IsValidURL = false;
            }
            else
            {
                result.IsValidURL = true;
                // Retrieve Account Info from Page

                var temp1 = string.Empty;

                //Load Values into HtmlNodes 
                var htmlNodes = base.LoadNode(responseDoc, ReviewSourceEnum.Yelp, Parameters.XPaths);

                //Find Each Field
                #region LocationName
                if (htmlNodes.LocationNameNode != null)
                {
                    result.AccountName = htmlNodes.LocationNameNode.InnerText;

                }
                else
                {
                    result.MisMatchesList.Add("LocationName");
                    result.MismatchCount++;
                }
                #endregion

                #region Phone
                if (htmlNodes.PhoneNode != null)
                {
                    result.AccountDetails.Phone = htmlNodes.PhoneNode.InnerText;
                }
                else
                {
                    result.MisMatchesList.Add("Phone");
                    result.MismatchCount++;
                }
                #endregion

                #region Address
                if (htmlNodes.AddressNode != null)
                {
                    result.AccountDetails.Address = htmlNodes.AddressNode.InnerText;
                }
                else
                {
                    result.MisMatchesList.Add("Address");
                    result.MismatchCount++;
                }
                #endregion

                #region City
                if (htmlNodes.CityNode != null)
                {
                    result.AccountDetails.City = htmlNodes.CityNode.InnerText;
                }
                else
                {
                    result.MisMatchesList.Add("City");
                    result.MismatchCount++;
                }
                #endregion

                #region State
                if (htmlNodes.StateNode != null)
                {
                    result.AccountDetails.State = htmlNodes.StateNode.InnerText;
                }
                else
                {
                    result.MisMatchesList.Add("State");
                    result.MismatchCount++;
                }
                #endregion

                #region ZipCode
                if (htmlNodes.ZipNode != null)
                {
                    result.AccountDetails.Zip = htmlNodes.ZipNode.InnerText;
                }
                else
                {
                    result.MisMatchesList.Add("Zip");
                    result.MismatchCount++;
                }
                #endregion

                #region WebsiteURL
                if (htmlNodes.WebsiteURLNode != null)
                {
                    result.AccountDetails.WebsiteURL = htmlNodes.WebsiteURLNode.InnerText;
                }
                else
                {
                    result.MisMatchesList.Add("WebsiteURL");
                    result.MismatchCount++;
                }
                #endregion

                #region ReviewCount
                if (htmlNodes.ReviewCountNode != null)
                {
                    result.ReviewDetails.ReviewCount = Convert.ToInt32(htmlNodes.ReviewCountNode.InnerText);
                }
                else
                {
                    result.MisMatchesList.Add("ReviewCount");
                    result.MismatchCount++;
                }
                #endregion

                #region Rating
                if (htmlNodes.RatingValueNode != null)
                {
                    temp1 = htmlNodes.RatingValueNode.GetAttributeValue("content", "");
                    result.ReviewDetails.Rating = Convert.ToDecimal(temp1);
                }
                else
                {
                    result.MisMatchesList.Add("Rating");
                    result.MismatchCount++;
                }
                #endregion
                
            }
            return result;
            
        }

        protected override void dispose()
        {
            //nothing to dispose
        }

    }
}
