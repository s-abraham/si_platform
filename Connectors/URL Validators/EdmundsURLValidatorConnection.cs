﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Connectors.Parameters;
using Connectors.URL_Validators.Parameters;
using HtmlAgilityPack;
using SI.DTO;

namespace Connectors.URL_Validators
{
    public class EdmundsURLValidatorConnection : SIReviewURLValidatorConnection
    {
        public EdmundsURLValidatorConnection(EdmundsReviewURLValidatorParameters cp)
            : base(cp)
        {
            Parameters = cp;
        }

        public EdmundsReviewURLValidatorParameters Parameters { get; set; }
        
        public override string HarvestReviewURL()
        {
            return "";
        }

        public override ReviewURLValidationResult ExtractPageInfo(HtmlDocument responseDoc)
        {
            var result = new ReviewURLValidationResult();
            var temp1 = string.Empty;
            // Determine if Page is valid and retrievable

            if (responseDoc == null)
            {
                result.IsValidURL = false;
            }
            else
            {
                result.IsValidURL = true;
                // Retrieve Account Info from Page

                //Load Values into HtmlNodes 
                var htmlNodes = base.LoadNode(responseDoc, ReviewSourceEnum.Edmunds, Parameters.XPaths);

                //Find Each Field
                #region LocationName
                if (htmlNodes.LocationNameNode != null)
                {
                    result.AccountName = htmlNodes.LocationNameNode.InnerText.Replace("Sales Reviews", "").Replace("Service Reviews", "");
                }
                else
                {
                    result.MisMatchesList.Add("LocationName");
                    result.MismatchCount++;
                }
                #endregion

                #region Phone
                if (htmlNodes.PhoneNode != null)
                {
                    result.AccountDetails.Phone = htmlNodes.PhoneNode.InnerText;
                }
                else
                {
                    result.MisMatchesList.Add("Phone");
                    result.MismatchCount++;
                }
                #endregion

                #region Address
                if (htmlNodes.AddressNode != null)
                {
                    result.AccountDetails.Address = htmlNodes.AddressNode.InnerText;
                }
                else
                {
                    result.MisMatchesList.Add("Address");
                    result.MismatchCount++;
                }
                #endregion

                #region City
                if (htmlNodes.CityNode != null)
                {
                    result.AccountDetails.City = htmlNodes.CityNode.InnerText;
                }
                else
                {
                    result.MisMatchesList.Add("City");
                    result.MismatchCount++;
                }
                #endregion

                #region State
                if (htmlNodes.StateNode != null)
                {
                    result.AccountDetails.State = htmlNodes.StateNode.InnerText;
                }
                else
                {
                    result.MisMatchesList.Add("State");
                    result.MismatchCount++;
                }
                #endregion

                #region ZipCode
                if (htmlNodes.ZipNode != null)
                {
                    result.AccountDetails.Zip = htmlNodes.ZipNode.InnerText;
                }
                else
                {
                    result.MisMatchesList.Add("Zip");
                    result.MismatchCount++;
                }
                #endregion

                #region WebsiteURL
                if (htmlNodes.WebsiteURLNode != null)
                {
                    result.AccountDetails.WebsiteURL = htmlNodes.WebsiteURLNode.GetAttributeValue("href", "");
                }
                else
                {
                    result.MisMatchesList.Add("WebsiteURL");
                    result.MismatchCount++;
                }
                #endregion

                #region ReviewCount
                if (htmlNodes.ReviewCountNode != null)
                {
                    temp1 = htmlNodes.ReviewCountNode.InnerText.Replace("reviews", "").Replace("review", "");
                    result.ReviewDetails.ReviewCount = Convert.ToInt32(temp1);
                }
                else
                {
                    result.MisMatchesList.Add("ReviewCount");
                    result.MismatchCount++;
                }
                #endregion

                #region ReviewCountService
                if (htmlNodes.ReviewCountServiceNode != null)
                {
                    temp1 = htmlNodes.ReviewCountServiceNode.InnerText.Replace("reviews", "").Replace("review", "");
                    result.ReviewDetails.ReviewCountService = Convert.ToInt32(temp1);
                }
                else
                {
                    result.MisMatchesList.Add("ReviewCountService");
                    result.MismatchCount++;
                }
                #endregion

                #region Rating
                if (htmlNodes.RatingValueNode != null)
                {
                    temp1 = htmlNodes.RatingValueNode.GetAttributeValue("title", "0");
                    result.ReviewDetails.Rating = Convert.ToDecimal(temp1);
                }
                else
                {
                    result.MisMatchesList.Add("Rating");
                    result.MismatchCount++;
                }
                #endregion

                #region RatingService
                if (htmlNodes.RatingValueServiceNode != null)
                {
                    temp1 = htmlNodes.RatingValueServiceNode.GetAttributeValue("title", "0");
                    result.ReviewDetails.RatingService = Convert.ToDecimal(temp1);
                }
                else
                {
                    result.MisMatchesList.Add("RatingService");
                    result.MismatchCount++;
                }
                #endregion
            }
            return result;
        }

        protected override void dispose()
        {
         
        }
    }

}
