﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Connectors.Parameters;

namespace Connectors.URL_Validators.Parameters
{
    public class JudysBookReviewURLValidatorParameters : SIReviewURLValidatorParameters
    {
        public JudysBookReviewURLValidatorParameters(int timeoutSeconds, bool useProxy, int maxRedirects, string companyName, string city, string stateFull, string stateAbbrev, string zip, string streetAddress, JudysBookReviewURLValidatorXPathParameters xPaths) : base(timeoutSeconds, useProxy, maxRedirects, companyName, city, stateFull, stateAbbrev, zip, streetAddress)
        {            
            XPaths = xPaths;
        }

        public JudysBookReviewURLValidatorParameters(int timeoutSeconds, bool useProxy, int maxRedirects, JudysBookReviewURLValidatorXPathParameters xPaths)
            : base(timeoutSeconds, useProxy, maxRedirects)
        {
            XPaths = xPaths;
        }
        public JudysBookReviewURLValidatorXPathParameters XPaths { get; set; }
        
        public override string ProcessorSettingsPrefix()
        {
            return "JudysBook";
        }
    }
}
