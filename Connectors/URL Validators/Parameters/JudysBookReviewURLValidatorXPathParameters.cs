﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Connectors.URL_Validators.Parameters
{
    public class JudysBookReviewURLValidatorXPathParameters : XPathParameters
    {
        public string queryBasePath { get; set; }
        public string reviewCountPath { get; set; }
        public string ratingValuePath { get; set; }
        public string locationNamePath { get; set; }
        public string phonePath { get; set; }
        public string addressPath { get; set; }
        public string cityPath { get; set; }
        public string statePath { get; set; }
        public string zipPath { get; set; }
        public string websiteURLPath { get; set; }
        public string manufacturerPath { get; set; }
        public string latitudePath { get; set; }
        public string longitudePath { get; set; }
        public string reviewDetailIteratorPath { get; set; }
        public string noReviewText { get; set; }
        public string noReviewTextPath { get; set; }
        public string noRatingPath { get; set; } 
    }
}
