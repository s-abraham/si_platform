﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Connectors.Parameters
{
    public class YelpReviewURLValidatorParameters : SIReviewURLValidatorParameters
    {
        public YelpReviewURLValidatorParameters(int timeoutSeconds, bool useProxy, int maxRedirects, string companyName, string city, string stateFull, string stateAbbrev, string zip, string streetAddress, YelpReviewURLValidatorXPathParameters xPaths) 
            : base(timeoutSeconds, useProxy, maxRedirects, companyName, city, stateFull, stateAbbrev, zip, streetAddress)
        {            
            XPaths = xPaths;
        }

        public YelpReviewURLValidatorParameters(int timeoutSeconds, bool useProxy, int maxRedirects, YelpReviewURLValidatorXPathParameters xPaths) : base(timeoutSeconds, useProxy, maxRedirects)
        {
            XPaths = xPaths;
        }

        public YelpReviewURLValidatorXPathParameters XPaths { get; set; }

        public override string ProcessorSettingsPrefix()
        {
            return "Yelp";
        }

    }
}
