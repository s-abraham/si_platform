﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Connectors.Parameters
{
    public abstract class SIReviewURLValidatorParameters : SIConnectionParameters
    {
        public abstract string ProcessorSettingsPrefix();

        public SIReviewURLValidatorParameters(int timeoutSeconds, bool useProxy, int maxRedirects, string companyName, string city, string stateFull, string stateAbbrev, string zip, string streetAddress)
            : base(timeoutSeconds, useProxy, maxRedirects)
        {
            CompanyName = companyName;
            City = city;
            StateFull = stateFull;
            StateAbbrev = stateAbbrev;
            Zip = zip;
            StreetAddress = streetAddress;
        }

        public SIReviewURLValidatorParameters(int timeoutSeconds, bool useProxy, int maxRedirects)
            : base(timeoutSeconds, useProxy, maxRedirects)
        {

        }

        public string CompanyName { get; set; }
        public string City { get; set; }
        public string StateFull { get; set; }
        public string StateAbbrev { get; set; }
        public string Zip { get; set; }
        public string StreetAddress { get; set; }
    }
}
