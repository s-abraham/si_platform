﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Connectors.Parameters
{
    public class CarsdotcomReviewURLValidatorParameters : SIReviewURLValidatorParameters
    {
        public CarsdotcomReviewURLValidatorParameters(int timeoutSeconds, bool useProxy, int maxRedirects, string companyName, string city, string stateFull, string stateAbbrev, string zip, string streetAddress, CarsdotcomReviewURLValidatorXPathParameters xPaths)
            : base(timeoutSeconds, useProxy, maxRedirects, companyName, city, stateFull, stateAbbrev, zip, streetAddress)
        {
            XPaths = xPaths;
        }

        public CarsdotcomReviewURLValidatorParameters(int timeoutSeconds, bool useProxy, int maxRedirects, CarsdotcomReviewURLValidatorXPathParameters xPaths)
            : base(timeoutSeconds, useProxy, maxRedirects)
        {
            XPaths = xPaths;
        }

        public CarsdotcomReviewURLValidatorXPathParameters XPaths { get; set; }

        public override string ProcessorSettingsPrefix()
        {
            return "Carsdotcom";
        }

    }
}