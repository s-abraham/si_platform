﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Connectors.Parameters;

namespace Connectors.URL_Validators.Parameters
{
    public class YellowPagesReviewURLValidatorParameters : SIReviewURLValidatorParameters
    {
        public YellowPagesReviewURLValidatorParameters(int timeoutSeconds, bool useProxy, int maxRedirects,string companyName, string city, string stateFull, string stateAbbrev, string zip, string streetAddress,YellowPagesReviewURLValidatorXPathParameters xPaths)
            : base(timeoutSeconds, useProxy, maxRedirects, companyName, city, stateFull, stateAbbrev, zip, streetAddress)
        {            
            XPaths = xPaths;
        }

        public YellowPagesReviewURLValidatorParameters(int timeoutSeconds, bool useProxy, int maxRedirects, YellowPagesReviewURLValidatorXPathParameters xPaths) : base(timeoutSeconds, useProxy, maxRedirects)
        {
            XPaths = xPaths;
        }

        public YellowPagesReviewURLValidatorXPathParameters XPaths { get; set; }

        public override string ProcessorSettingsPrefix()
        {
            return "YellowPages";
        }
    }
}
