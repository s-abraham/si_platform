﻿namespace Connectors.URL_Validators.Parameters
{
    public abstract class XPathParameters
    {
        //public XPathParameters(string queryBasePath, string reviewCountPath, string ratingValuePath, string locationNamePath, string phonePath, string addressPath, string cityPath, string statePath, string zipPath, string websiteURLPath, string manufacturerPath, string latitudePath, string longitudePath, string reviewDetailIteratorPath, string noReviewText, string noReviewTextPath, string noRatingPath)
        //{
        //    QueryBasePath = queryBasePath;
        //    ReviewCountPath = reviewCountPath;
        //    RatingValuePath = ratingValuePath;
        //    LocationNamePath = locationNamePath;
        //    PhonePath = phonePath;
        //    AddressPath = addressPath;
        //    CityPath = cityPath;
        //    StatePath = statePath;
        //    ZipPath = zipPath;
        //    WebsiteURLPath = websiteURLPath;
        //    ManufacturerPath = manufacturerPath;
        //    LatitudePath = latitudePath;
        //    LongitudePath = longitudePath;
        //    ReviewDetailIteratorPath = reviewDetailIteratorPath;
        //    NoReviewText = noReviewText;
        //    NoReviewTextPath = noReviewTextPath;
        //    NoRatingPath = noRatingPath;
        //}

        public string QueryBasePath { get; set; }
        public string ReviewCountPath { get; set; }
        public string RatingValuePath { get; set; }
        public string LocationNamePath { get; set; }
        public string PhonePath { get; set; }
        public string AddressPath { get; set; }
        public string CityPath { get; set; }
        public string StatePath { get; set; }
        public string ZipPath { get; set; }
        public string WebsiteURLPath { get; set; }
        public string ManufacturerPath { get; set; }
        public string LatitudePath { get; set; }
        public string LongitudePath { get; set; }
        public string ReviewDetailIteratorPath { get; set; }
        public string NoReviewText { get; set; }
        public string NoReviewTextPath { get; set; }
        public string NoRatingPath { get; set; }
    }
}