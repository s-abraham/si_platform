﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Connectors.Parameters
{
    public class InsiderPagesReviewURLValidatorParameters : SIReviewURLValidatorParameters
    {
        public InsiderPagesReviewURLValidatorParameters(int timeoutSeconds, bool useProxy, int maxRedirects, string companyName, string city, string stateFull, string stateAbbrev, string zip, string streetAddress, InsiderPagesReviewURLValidatorXPathParameters xPaths)
            : base(timeoutSeconds, useProxy, maxRedirects, companyName, city, stateFull, stateAbbrev, zip, streetAddress)
        {
            XPaths = xPaths;
        }

        public InsiderPagesReviewURLValidatorParameters(int timeoutSeconds, bool useProxy, int maxRedirects, InsiderPagesReviewURLValidatorXPathParameters xPaths)
            : base(timeoutSeconds, useProxy, maxRedirects)
        {
            XPaths = xPaths;
        }

        public InsiderPagesReviewURLValidatorXPathParameters XPaths { get; set; }

        public override string ProcessorSettingsPrefix()
        {
            return "InsiderPages";
        }

    }
}
