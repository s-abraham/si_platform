﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Connectors.Entities;
using Connectors.URL_Validators.Parameters;

namespace Connectors.Parameters
{
    public class CarsdotcomReviewURLValidatorXPathParameters : XPathParameters
    {
        public string queryBasePath { get; set; }
        public string reviewCountPath { get; set; }
        public string ratingValuePath { get; set; }
        public string locationNamePath { get; set; }
        public string phonePath { get; set; }
        public string addressPath { get; set; }
        public string cityPath { get; set; }
        public string statePath { get; set; }
        public string zipPath { get; set; }
        public string websiteURLPath { get; set; }
        public string manufacturerPath { get; set; }
        public string latitudePath { get; set; }
        public string longitudePath { get; set; }
        public string reviewDetailIteratorPath { get; set; }
        public string noReviewText { get; set; }
        public string noReviewTextPath { get; set; }
        public string noRatingPath { get; set; } 

        public string SearchBaseURLPath { get; set; }
        public string Search_Makes { get; set; }
        public string Search_Radius { get; set; }
        public string Search_ZipCode { get; set; }
        public string Search_RestPath  { get; set; }
        public string QueryBasePath { get; set; }
        public string URLPath { get; set; }
    }
}

