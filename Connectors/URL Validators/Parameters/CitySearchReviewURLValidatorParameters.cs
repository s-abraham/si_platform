﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Connectors.Parameters;

namespace Connectors.URL_Validators.Parameters
{
    public class CitySearchReviewURLValidatorParameters : SIReviewURLValidatorParameters
    {
        public CitySearchReviewURLValidatorParameters(int timeoutSeconds, bool useProxy, int maxRedirects, string companyName, string city, string stateFull, string stateAbbrev, string zip, string streetAddress, CitySearchReviewURLValidatorXPathParameters xPaths)
            : base(timeoutSeconds, useProxy, maxRedirects, companyName, city, stateFull, stateAbbrev, zip, streetAddress)
        {            
            XPaths = xPaths;
        }

        public CitySearchReviewURLValidatorParameters(int timeoutSeconds, bool useProxy, int maxRedirects, CitySearchReviewURLValidatorXPathParameters xPaths)
            : base(timeoutSeconds, useProxy, maxRedirects)
        {
            XPaths = xPaths;
        }

        public CitySearchReviewURLValidatorXPathParameters XPaths { get; set; }

        public override string ProcessorSettingsPrefix()
        {
            return "CitySearch";
        }
    }
}
