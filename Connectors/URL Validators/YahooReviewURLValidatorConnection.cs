﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Connectors.Parameters;
using Connectors.URL_Validators.Parameters;
using HtmlAgilityPack;
using SI;
using SI.DTO;

namespace Connectors.URL_Validators
{
    public class YahooReviewURLValidatorConnection : SIReviewURLValidatorConnection
    {
        public YahooReviewURLValidatorConnection(YahooReviewURLValidatorParameters cp) : base(cp)
        {
            Parameters = cp;
        }

        public YahooReviewURLValidatorParameters Parameters { get; set; }
        
        public override string HarvestReviewURL()
        {
            return "";
        }



        public override ReviewURLValidationResult ExtractPageInfo(HtmlDocument responseDoc)
        {
            var result = new ReviewURLValidationResult();
            // Determine if Page is valid and retrievable

            if (responseDoc == null)
            {
                result.IsValidURL = false;
            }
            else
            {
                result.IsValidURL = true;
                // Retrieve Account Info from Page       

                var temp1 = string.Empty;
                //Load Values into HtmlNodes 
                var htmlNodes = base.LoadNode(responseDoc, ReviewSourceEnum.Yahoo, Parameters.XPaths);

                //Find Each Field
                #region LocationName
                if (htmlNodes.LocationNameNode != null)
                {
                    result.AccountName = htmlNodes.LocationNameNode.InnerText;

                }
                else
                {
                    result.MisMatchesList.Add("LocationName");
                    result.MismatchCount++;
                }
                #endregion

                #region Phone
                if (htmlNodes.PhoneNode != null)
                {
                    result.AccountDetails.Phone = htmlNodes.PhoneNode.InnerText;
                }
                else
                {
                    result.MisMatchesList.Add("Phone");
                    result.MismatchCount++;
                }
                #endregion

                #region Address, City, State, and Zip

                if (htmlNodes.AddressNode != null)
                {

                    var fullAddress = htmlNodes.AddressNode.InnerText;
                    var fullAddressSplit = fullAddress.Split(',');

                    if (fullAddressSplit.Count() == 3)
                    {
                        result.AccountDetails.Address = fullAddressSplit[0].Trim();
                        result.AccountDetails.City = fullAddressSplit[1].Trim();

                        var stateZipSplit = fullAddressSplit[2].Trim().Split(' ');

                        result.AccountDetails.State = stateZipSplit[0].Trim();
                        result.AccountDetails.Zip = stateZipSplit[1].Trim();
                    }
                    else
                    {
                        result.MisMatchesList.Add("Address");
                        result.MisMatchesList.Add("City");
                        result.MisMatchesList.Add("State");
                        result.MisMatchesList.Add("Zip");
                        result.MismatchCount = result.MismatchCount + 4;
                    }
                }
                else
                {
                    result.MisMatchesList.Add("Address");
                    result.MisMatchesList.Add("City");
                    result.MisMatchesList.Add("State");
                    result.MisMatchesList.Add("Zip");
                    result.MismatchCount = result.MismatchCount + 4;
                }
                #endregion

                #region WebsiteURL
                if (htmlNodes.WebsiteURLNode != null)
                {
                    temp1 = htmlNodes.WebsiteURLNode.GetAttributeValue("href", " ");

                    if (!string.IsNullOrEmpty(temp1))
                    {
                        temp1 = temp1.Substring(temp1.IndexOf("www."));
                        result.AccountDetails.WebsiteURL = temp1.Replace("%253A", ":").Replace("%252F", "/");
                    }
                    else
                    {
                        result.MisMatchesList.Add("WebsiteURL");
                        result.MismatchCount++;
                    }
                }
                else
                {
                    result.MisMatchesList.Add("WebsiteURL");
                    result.MismatchCount++;
                }
                #endregion

                #region ReviewCount
                if (htmlNodes.ReviewCountNode != null)
                {
                    temp1 = htmlNodes.ReviewCountNode.InnerText;
                    result.ReviewDetails.ReviewCount = Convert.ToInt32(temp1.Replace("Reviews", "").Replace("Review", ""));
                }
                else
                {
                    result.MisMatchesList.Add("ReviewCount");
                    result.MismatchCount++;
                }
                #endregion

                #region Rating
                if (htmlNodes.RatingValueNode != null)
                {
                    temp1 = htmlNodes.RatingValueNode.GetAttributeValue("class", " ");
                    temp1 = StringUtils.ExtractSubstring(temp1, "star", " ");
                    result.ReviewDetails.Rating = Convert.ToDecimal(temp1) / 2.0m;
                }
                else
                {
                    result.MisMatchesList.Add("Rating");
                    result.MismatchCount++;
                }
                #endregion

            }

            

            return result;
        }
        
        protected override void dispose()
        {
            
        }
    }
}
