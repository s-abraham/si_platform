﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Connectors.Parameters;
using Connectors.URL_Validators.Parameters;
using HtmlAgilityPack;
using SI.DTO;

namespace Connectors.URL_Validators
{
    public class JudysBookReviewURLValidatorConnection : SIReviewURLValidatorConnection
    {
        public JudysBookReviewURLValidatorConnection(JudysBookReviewURLValidatorParameters cp) : base(cp)
        {
            Parameters = cp;
        }

        public JudysBookReviewURLValidatorParameters Parameters { get; set; }

        public override string HarvestReviewURL()
        {
            return "";
        }

        public override ReviewURLValidationResult ExtractPageInfo(HtmlDocument responseDoc)
        {
            var result = new ReviewURLValidationResult();

            // Determine if Page is valid and retrievable

            if (responseDoc == null)
            {
                result.IsValidURL = false;
            }
            else
            {
                var temp1 = string.Empty;
                result.IsValidURL = true;
                // Retrieve Account Info from Page

                //Load Values into HtmlNodes 
                var htmlNodes = base.LoadNode(responseDoc, ReviewSourceEnum.JudysBook, Parameters.XPaths);

                //Find Each Field
                #region LocationName
                if (htmlNodes.LocationNameNode != null)
                {
                    result.AccountName = htmlNodes.LocationNameNode.InnerText;

                }
                else
                {
                    result.MisMatchesList.Add("LocationName");
                    result.MismatchCount++;
                }
                #endregion

                //#region Phone
                //if (htmlNodes.PhoneNode != null)
                //{
                //    result.AccountDetails.Phone = htmlNodes.PhoneNode.InnerText;
                //}
                //else
                //{
                //    result.MisMatchesList.Add("Phone");
                //    result.MismatchCount++;
                //}
                //#endregion

                #region Address, City, State, and Zip and Phone

                if (htmlNodes.AddressNode != null)
                {
                    var fullAddress = htmlNodes.AddressNode.InnerHtml.Replace("<br />","<br>");
                    var fullAddressSplit = Regex.Split(fullAddress, "<br>");

                    if (fullAddressSplit.Count() == 3)
                    {
                        result.AccountDetails.Address = fullAddressSplit[0].Trim();
                        if (fullAddressSplit[2].Trim() != null)
                        {
                            result.AccountDetails.Phone = fullAddressSplit[2].Trim();
                        }
                        else
                        {
                            result.MisMatchesList.Add("Phone");
                            result.MismatchCount++;
                        }

                        var cityStateZipSplit = fullAddressSplit[1].Split(',');

                         if (cityStateZipSplit.Count() == 2)
                        {
                            result.AccountDetails.City = cityStateZipSplit[0].Trim();

                            var stateZipSplit = cityStateZipSplit[1].Trim().Split(' ');

                            if (stateZipSplit.Count() == 3)
                            {
                                result.AccountDetails.Zip = stateZipSplit[2].Trim();
                                result.AccountDetails.StateFull = stateZipSplit[0].Trim() + " " + stateZipSplit[1].Trim();
                            }
                            else if (stateZipSplit.Count() >= 2)
                            {
                                result.AccountDetails.StateFull = stateZipSplit[0].Trim();
                                result.AccountDetails.Zip = stateZipSplit[1].Trim();
                            }
                            else
                            {
                                result.MisMatchesList.Add("State");
                                result.MisMatchesList.Add("Zip");
                                result.MismatchCount = result.MismatchCount + 2;
                            }
                        }
                        else
                        {
                            result.MisMatchesList.Add("City");
                            result.MisMatchesList.Add("State");
                            result.MisMatchesList.Add("Zip");
                            result.MismatchCount = result.MismatchCount + 3;
                        }

                    }
                    else
                    {
                        result.MisMatchesList.Add("Address");
                        result.MisMatchesList.Add("City");
                        result.MisMatchesList.Add("State");
                        result.MisMatchesList.Add("Zip");
                        result.MismatchCount = result.MismatchCount + 4;
                    }
                }
                else
                {
                    result.MisMatchesList.Add("Address");
                    result.MisMatchesList.Add("City");
                    result.MisMatchesList.Add("State");
                    result.MisMatchesList.Add("Zip");
                    result.MisMatchesList.Add("Phone");
                    result.MismatchCount = result.MismatchCount + 5;
                }
                #endregion
                
                #region WebsiteURL

                result.AccountDetails.WebsiteURL = "";

                #endregion

                #region ReviewCount
                if (htmlNodes.ReviewCountNode != null)
                {
                    temp1 = htmlNodes.ReviewCountNode.InnerText;
                    result.ReviewDetails.ReviewCount = Convert.ToInt32(temp1);
                }
                else
                {
                    result.MisMatchesList.Add("ReviewCount");
                    result.MismatchCount++;
                }
                #endregion

                #region Rating
                if (htmlNodes.RatingValueNode != null)
                {
                    temp1 = htmlNodes.RatingValueNode.InnerText;
                    result.ReviewDetails.Rating = Convert.ToDecimal(temp1);
                }
                else
                {
                    result.MisMatchesList.Add("Rating");
                    result.MismatchCount++;
                }
                #endregion
            }
            return result;
        }

        protected override void dispose()
        {

        }
    }
}
