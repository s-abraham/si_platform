﻿using Connectors.Parameters;
using Connectors.URL_Validators.Parameters;
using HtmlAgilityPack;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SI.DTO;
using System.Text.RegularExpressions;

namespace Connectors
{
    public abstract class SIReviewURLValidatorConnection : SIConnection
    {
        public SIReviewURLValidatorConnection(SIReviewURLValidatorParameters cp) : base(cp) { }

        /// <summary>
        /// Attempt to discover what the review url is for this company
        /// </summary>
        /// <returns>The url as a string, or null if not found.</returns>
        public abstract string HarvestReviewURL();

        public ReviewURLValidationResult ValidateReviewURL(string url, ReviewURLValidatorData accountInfo)
        {
            //HtmlDocument doc = DownloadDocument(url, null);
        /// <summary>
        /// Determine if the url is valid for this company.
        /// </summary>
        /// <param name="url">The url to check.</param>
        /// 
            //string httpResult = DownloadString(url, null);
            //HtmlDocument htmlDocument = new HtmlDocument();
            //htmlDocument.OptionFixNestedTags = true;
            //htmlDocument.LoadHtml(httpResult);

            HtmlDocument doc = DownloadDocument(url, null, 3, 2000, "Mozilla/5.0 (Windows NT 6.1; WOW64; rv:24.0) Gecko/20100101 Firefox/24.0");
            doc.OptionFixNestedTags = true;

            return ValidateReviewURL(doc, accountInfo);
        }

        public ReviewURLValidationResult ValidateReviewURL(HtmlDocument responseDoc, ReviewURLValidatorData accountInfo)
        {
            var pageInfo = new ReviewURLValidationResult();

            pageInfo = ExtractPageInfo(responseDoc);

            var matchTotal = GetMisMatchs(ref pageInfo, accountInfo);
                        
            return pageInfo;
        }

        public int GetMisMatchs(ref ReviewURLValidationResult validationResult, ReviewURLValidatorData accountInfo)
        {
            var validationMisMatch = 0;

            try
            {
                string reviewSourceAccountName = CleanInput(validationResult.AccountName.ToString());
                string accountInfoAccountName = CleanInput(accountInfo.CompanyName.ToString());
                if (reviewSourceAccountName != accountInfoAccountName)
                {
                    validationResult.ValidationMisMatch.Add(new MisMatchRecord("AccountName", accountInfoAccountName, reviewSourceAccountName));
                }
                string reviewSourceAddress = CleanInput(Convert.ToString(validationResult.AccountDetails.Address));
                string accountInfoAddress = CleanInput(accountInfo.StreetAddress.ToString());
                {
                    validationResult.ValidationMisMatch.Add(new MisMatchRecord("Address", accountInfoAddress, reviewSourceAddress));
                }
                string reviewSourceCity = CleanInput(validationResult.AccountDetails.City.ToString());
                string accountInfoCity = CleanInput(accountInfo.City.ToString());
                if (reviewSourceCity != accountInfoCity)
                {
                    validationResult.ValidationMisMatch.Add(new MisMatchRecord("City", accountInfoCity, reviewSourceCity));
                }
                string reviewSourceStateFull = CleanInput(validationResult.AccountDetails.StateFull.ToString());
                string accountInfoStateFull = CleanInput(accountInfo.StateFull.ToString());
                if (reviewSourceStateFull != accountInfoStateFull)
                {
                    validationResult.ValidationMisMatch.Add(new MisMatchRecord("StateFull", accountInfoStateFull, reviewSourceStateFull));
                }
                string reviewSourceStateAbbrev = CleanInput(validationResult.AccountDetails.State.ToString());
                string accountInfoStateAbbrev = CleanInput(accountInfo.StateAbbrev.ToString());
                if (reviewSourceStateAbbrev != accountInfoStateAbbrev)
                {
                    validationResult.ValidationMisMatch.Add(new MisMatchRecord("StateAbbrev", accountInfoStateAbbrev, reviewSourceStateAbbrev));
                }
                string reviewSourceZip = CleanInput(validationResult.AccountDetails.Zip.ToString());
                string accountInfoZip = CleanInput(accountInfo.Zip.ToString());
                if (reviewSourceZip != accountInfoZip)
                {
                    validationResult.ValidationMisMatch.Add(new MisMatchRecord("Zip", accountInfoZip, reviewSourceZip));
                }

                string reviewSourcePhone = Regex.Replace(validationResult.AccountDetails.Phone, "[^0-9]+", string.Empty);
                string accountInfoPhone =  Regex.Replace(accountInfo.Phone, "[^0-9]+", string.Empty);
                if (reviewSourcePhone != accountInfoPhone)
                {
                    validationResult.ValidationMisMatch.Add(new MisMatchRecord("Phone", accountInfoPhone, reviewSourcePhone));
                }

                if (validationResult.ValidationMisMatch != null)
                {
                    validationMisMatch = validationResult.ValidationMisMatch.Count;
                }

            }

            catch
            {
                //ProviderFactory.Logging.LogException(ex);
                validationMisMatch = 7;
                validationResult.ValidationMisMatch.Add(new MisMatchRecord("All Failed", "", ""));
            }

            return validationMisMatch;
        }
        public abstract ReviewURLValidationResult ExtractPageInfo(HtmlDocument doc);

        public class ReviewValidationNodes
        {
            public ReviewValidationNodes()
            {

            }

            public HtmlNode ReviewCountNode { get; set; }
            public HtmlNode RatingValueNode { get; set; }
            public HtmlNode ReviewCountServiceNode { get; set; }
            public HtmlNode RatingValueServiceNode { get; set; }

            public HtmlNode LocationNameNode { get; set; }
            public HtmlNode PhoneNode { get; set; }
            public HtmlNode AddressNode { get; set; }
            public HtmlNode CityNode { get; set; }
            public HtmlNode StateNode { get; set; }
            public HtmlNode ZipNode { get; set; }
            public HtmlNode WebsiteURLNode { get; set; }
            public HtmlNode ManufacturerNode { get; set; }
            public HtmlNode LatitudeNode { get; set; }
            public HtmlNode LongitudeNode { get; set; }
            public HtmlNode ReviewDetailIteratorNode { get; set; }
            public HtmlNode NoReviewTextNode { get; set; }
            public HtmlNode NoRatingNode { get; set; }

        }

        public ReviewValidationNodes LoadNode(HtmlDocument responseDoc, ReviewSourceEnum reviewSourceEnum, XPathParameters parameters)
        {
            var reviewValidationNodes = new ReviewValidationNodes();

            switch (reviewSourceEnum)
            {
                case ReviewSourceEnum.Google:
                    reviewValidationNodes.ReviewCountNode = responseDoc.DocumentNode.SelectSingleNode(parameters.ReviewCountPath);
                    reviewValidationNodes.RatingValueNode = responseDoc.DocumentNode.SelectSingleNode(parameters.RatingValuePath);
                    reviewValidationNodes.LocationNameNode = responseDoc.DocumentNode.SelectSingleNode(parameters.LocationNamePath);
                    reviewValidationNodes.PhoneNode = responseDoc.DocumentNode.SelectSingleNode(parameters.PhonePath);
                    reviewValidationNodes.AddressNode = responseDoc.DocumentNode.SelectSingleNode(parameters.AddressPath);
                    reviewValidationNodes.CityNode = responseDoc.DocumentNode.SelectSingleNode(parameters.CityPath);
                    reviewValidationNodes.StateNode = responseDoc.DocumentNode.SelectSingleNode(parameters.StatePath);
                    reviewValidationNodes.ZipNode = responseDoc.DocumentNode.SelectSingleNode(parameters.ZipPath);
                    reviewValidationNodes.WebsiteURLNode = responseDoc.DocumentNode.SelectSingleNode(parameters.WebsiteURLPath);
                    reviewValidationNodes.ManufacturerNode = null;
                    reviewValidationNodes.LatitudeNode = null;
                    reviewValidationNodes.LongitudeNode = null;
                    reviewValidationNodes.ReviewDetailIteratorNode = responseDoc.DocumentNode.SelectSingleNode(parameters.ReviewDetailIteratorPath);
                    reviewValidationNodes.NoReviewTextNode = responseDoc.DocumentNode.SelectSingleNode(parameters.NoReviewTextPath);
                    //reviewValidationNodes.NoRatingNode = responseDoc.DocumentNode.SelectSingleNode(parameters.NoRatingPath);
                    break;
                case ReviewSourceEnum.DealerRater:
                    reviewValidationNodes.ReviewCountNode = responseDoc.DocumentNode.SelectSingleNode(parameters.ReviewCountPath);
                    reviewValidationNodes.RatingValueNode = responseDoc.DocumentNode.SelectSingleNode(parameters.RatingValuePath);
                    reviewValidationNodes.LocationNameNode = responseDoc.DocumentNode.SelectSingleNode(parameters.LocationNamePath);
                    reviewValidationNodes.PhoneNode = responseDoc.DocumentNode.SelectSingleNode(parameters.PhonePath);
                    reviewValidationNodes.AddressNode = responseDoc.DocumentNode.SelectSingleNode(parameters.AddressPath);
                    reviewValidationNodes.CityNode = responseDoc.DocumentNode.SelectSingleNode(parameters.CityPath);
                    reviewValidationNodes.StateNode = responseDoc.DocumentNode.SelectSingleNode(parameters.StatePath);
                    reviewValidationNodes.ZipNode = responseDoc.DocumentNode.SelectSingleNode(parameters.ZipPath);
                    reviewValidationNodes.WebsiteURLNode = responseDoc.DocumentNode.SelectSingleNode(parameters.WebsiteURLPath);
                    reviewValidationNodes.ManufacturerNode = responseDoc.DocumentNode.SelectSingleNode(parameters.ManufacturerPath);
                    reviewValidationNodes.LatitudeNode = responseDoc.DocumentNode.SelectSingleNode(parameters.LatitudePath);
                    reviewValidationNodes.LongitudeNode = responseDoc.DocumentNode.SelectSingleNode(parameters.LongitudePath);
                    reviewValidationNodes.ReviewDetailIteratorNode = responseDoc.DocumentNode.SelectSingleNode(parameters.ReviewDetailIteratorPath);
                    reviewValidationNodes.NoReviewTextNode = responseDoc.DocumentNode.SelectSingleNode(parameters.NoReviewTextPath);
                    //reviewValidationNodes.NoRatingNode = responseDoc.DocumentNode.SelectSingleNode(parameters.NoRatingPath);
                    break;
                case ReviewSourceEnum.Yahoo:
                    reviewValidationNodes.ReviewCountNode = responseDoc.DocumentNode.SelectSingleNode(parameters.ReviewCountPath);
                    reviewValidationNodes.RatingValueNode = responseDoc.DocumentNode.SelectSingleNode(parameters.RatingValuePath);
                    reviewValidationNodes.LocationNameNode = responseDoc.DocumentNode.SelectSingleNode(parameters.LocationNamePath);
                    reviewValidationNodes.PhoneNode = responseDoc.DocumentNode.SelectSingleNode(parameters.PhonePath);
                    reviewValidationNodes.AddressNode = responseDoc.DocumentNode.SelectSingleNode(parameters.AddressPath);
                    reviewValidationNodes.CityNode = responseDoc.DocumentNode.SelectSingleNode(parameters.CityPath);
                    reviewValidationNodes.StateNode = responseDoc.DocumentNode.SelectSingleNode(parameters.StatePath);
                    reviewValidationNodes.ZipNode = responseDoc.DocumentNode.SelectSingleNode(parameters.ZipPath);
                    reviewValidationNodes.WebsiteURLNode = responseDoc.DocumentNode.SelectSingleNode(parameters.WebsiteURLPath);
                    reviewValidationNodes.ManufacturerNode = null;
                    reviewValidationNodes.LatitudeNode = null;
                    reviewValidationNodes.LongitudeNode = null;
                    reviewValidationNodes.ReviewDetailIteratorNode = responseDoc.DocumentNode.SelectSingleNode(parameters.ReviewDetailIteratorPath);
                    reviewValidationNodes.NoReviewTextNode = responseDoc.DocumentNode.SelectSingleNode(parameters.NoReviewTextPath);
                    //reviewValidationNodes.NoRatingNode = responseDoc.DocumentNode.SelectSingleNode(parameters.NoRatingPath);
                    break;
                case ReviewSourceEnum.Yelp:
                    reviewValidationNodes.ReviewCountNode = responseDoc.DocumentNode.SelectSingleNode(parameters.ReviewCountPath);
                    reviewValidationNodes.RatingValueNode = responseDoc.DocumentNode.SelectSingleNode(parameters.RatingValuePath);
                    reviewValidationNodes.LocationNameNode = responseDoc.DocumentNode.SelectSingleNode(parameters.LocationNamePath);
                    reviewValidationNodes.PhoneNode = responseDoc.DocumentNode.SelectSingleNode(parameters.PhonePath);
                    reviewValidationNodes.AddressNode = responseDoc.DocumentNode.SelectSingleNode(parameters.AddressPath);
                    reviewValidationNodes.CityNode = responseDoc.DocumentNode.SelectSingleNode(parameters.CityPath);
                    reviewValidationNodes.StateNode = responseDoc.DocumentNode.SelectSingleNode(parameters.StatePath);
                    reviewValidationNodes.ZipNode = responseDoc.DocumentNode.SelectSingleNode(parameters.ZipPath);
                    reviewValidationNodes.WebsiteURLNode = responseDoc.DocumentNode.SelectSingleNode(parameters.WebsiteURLPath);
                    reviewValidationNodes.ManufacturerNode = null;
                    reviewValidationNodes.LatitudeNode = null;
                    reviewValidationNodes.LongitudeNode = null;
                    reviewValidationNodes.ReviewDetailIteratorNode = responseDoc.DocumentNode.SelectSingleNode(parameters.ReviewDetailIteratorPath);
                    reviewValidationNodes.NoReviewTextNode = null;
                    //reviewValidationNodes.NoRatingNode = responseDoc.DocumentNode.SelectSingleNode(parameters.NoRatingPath);
                    break;
                case ReviewSourceEnum.CarDealer:
                    reviewValidationNodes.ReviewCountNode = responseDoc.DocumentNode.SelectSingleNode(parameters.ReviewCountPath);
                    reviewValidationNodes.RatingValueNode = responseDoc.DocumentNode.SelectSingleNode(parameters.RatingValuePath);
                    reviewValidationNodes.LocationNameNode = responseDoc.DocumentNode.SelectSingleNode(parameters.LocationNamePath);
                    reviewValidationNodes.PhoneNode = responseDoc.DocumentNode.SelectSingleNode(parameters.PhonePath);
                    reviewValidationNodes.AddressNode = responseDoc.DocumentNode.SelectSingleNode(parameters.AddressPath);
                    reviewValidationNodes.CityNode = responseDoc.DocumentNode.SelectSingleNode(parameters.CityPath);
                    reviewValidationNodes.StateNode = responseDoc.DocumentNode.SelectSingleNode(parameters.StatePath);
                    reviewValidationNodes.ZipNode = responseDoc.DocumentNode.SelectSingleNode(parameters.ZipPath);
                    reviewValidationNodes.WebsiteURLNode = responseDoc.DocumentNode.SelectSingleNode(parameters.WebsiteURLPath);
                    reviewValidationNodes.ManufacturerNode = null;
                    reviewValidationNodes.LatitudeNode = null;
                    reviewValidationNodes.LongitudeNode = null;
                    reviewValidationNodes.ReviewDetailIteratorNode = responseDoc.DocumentNode.SelectSingleNode(parameters.ReviewDetailIteratorPath);
                    reviewValidationNodes.NoReviewTextNode = null;
                    //reviewValidationNodes.NoRatingNode = responseDoc.DocumentNode.SelectSingleNode(parameters.NoRatingPath);
                    break;
                case ReviewSourceEnum.YellowPages:
                    reviewValidationNodes.ReviewCountNode = responseDoc.DocumentNode.SelectSingleNode(parameters.ReviewCountPath);
                    reviewValidationNodes.RatingValueNode = responseDoc.DocumentNode.SelectSingleNode(parameters.RatingValuePath);
                    reviewValidationNodes.LocationNameNode = responseDoc.DocumentNode.SelectSingleNode(parameters.LocationNamePath);
                    reviewValidationNodes.PhoneNode = responseDoc.DocumentNode.SelectSingleNode(parameters.PhonePath);
                    reviewValidationNodes.AddressNode = responseDoc.DocumentNode.SelectSingleNode(parameters.AddressPath);
                    reviewValidationNodes.CityNode = responseDoc.DocumentNode.SelectSingleNode(parameters.CityPath);
                    reviewValidationNodes.StateNode = responseDoc.DocumentNode.SelectSingleNode(parameters.StatePath);
                    reviewValidationNodes.ZipNode = responseDoc.DocumentNode.SelectSingleNode(parameters.ZipPath);
                    reviewValidationNodes.WebsiteURLNode = responseDoc.DocumentNode.SelectSingleNode(parameters.WebsiteURLPath);
                    reviewValidationNodes.ManufacturerNode = null;
                    reviewValidationNodes.LatitudeNode = null;
                    reviewValidationNodes.LongitudeNode = null;
                    reviewValidationNodes.ReviewDetailIteratorNode = responseDoc.DocumentNode.SelectSingleNode(parameters.ReviewDetailIteratorPath);
                    reviewValidationNodes.NoReviewTextNode = responseDoc.DocumentNode.SelectSingleNode(parameters.NoReviewTextPath);
                    //reviewValidationNodes.NoRatingNode = responseDoc.DocumentNode.SelectSingleNode(parameters.NoRatingPath);
                    break;
                case ReviewSourceEnum.Edmunds:
                    //var para2 = (EdmundsReviewURLValidatorXPathParameters) parameters;
                    reviewValidationNodes.ReviewCountNode = responseDoc.DocumentNode.SelectSingleNode(parameters.ReviewCountPath);
                    //reviewValidationNodes.ReviewCountServiceNode = responseDoc.DocumentNode.SelectSingleNode(para2.ReviewCountServicePath);
                    reviewValidationNodes.RatingValueNode = responseDoc.DocumentNode.SelectSingleNode(parameters.RatingValuePath);
                    //reviewValidationNodes.RatingValueServiceNode = responseDoc.DocumentNode.SelectSingleNode(para2.RatingValueServicePath);
                    reviewValidationNodes.LocationNameNode = responseDoc.DocumentNode.SelectSingleNode(parameters.LocationNamePath);
                    reviewValidationNodes.PhoneNode = responseDoc.DocumentNode.SelectSingleNode(parameters.PhonePath);
                    reviewValidationNodes.AddressNode = responseDoc.DocumentNode.SelectSingleNode(parameters.AddressPath);
                    reviewValidationNodes.CityNode = responseDoc.DocumentNode.SelectSingleNode(parameters.CityPath);
                    reviewValidationNodes.StateNode = responseDoc.DocumentNode.SelectSingleNode(parameters.StatePath);
                    reviewValidationNodes.ZipNode = responseDoc.DocumentNode.SelectSingleNode(parameters.ZipPath);
                    reviewValidationNodes.WebsiteURLNode = responseDoc.DocumentNode.SelectSingleNode(parameters.WebsiteURLPath);
                    reviewValidationNodes.ManufacturerNode = null;
                    reviewValidationNodes.LatitudeNode = null;
                    reviewValidationNodes.LongitudeNode = null;
                    reviewValidationNodes.ReviewDetailIteratorNode = responseDoc.DocumentNode.SelectSingleNode(parameters.ReviewDetailIteratorPath);
                    reviewValidationNodes.NoReviewTextNode = responseDoc.DocumentNode.SelectSingleNode(parameters.NoReviewTextPath);
                    //reviewValidationNodes.NoRatingNode = responseDoc.DocumentNode.SelectSingleNode(parameters.NoRatingPath);
                    break;
                case ReviewSourceEnum.CarsDotCom:
                    reviewValidationNodes.ReviewCountNode = responseDoc.DocumentNode.SelectSingleNode(parameters.ReviewCountPath);
                    reviewValidationNodes.RatingValueNode = responseDoc.DocumentNode.SelectSingleNode(parameters.RatingValuePath);
                    reviewValidationNodes.LocationNameNode = responseDoc.DocumentNode.SelectSingleNode(parameters.LocationNamePath);
                    reviewValidationNodes.PhoneNode = responseDoc.DocumentNode.SelectSingleNode(parameters.PhonePath);
                    reviewValidationNodes.AddressNode = responseDoc.DocumentNode.SelectSingleNode(parameters.AddressPath);
                    reviewValidationNodes.CityNode = responseDoc.DocumentNode.SelectSingleNode(parameters.CityPath);
                    reviewValidationNodes.StateNode = responseDoc.DocumentNode.SelectSingleNode(parameters.StatePath);
                    reviewValidationNodes.ZipNode = responseDoc.DocumentNode.SelectSingleNode(parameters.ZipPath);
                    //reviewValidationNodes.WebsiteURLNode = responseDoc.DocumentNode.SelectSingleNode(parameters.WebsiteURLPath);
                    reviewValidationNodes.ManufacturerNode = null;
                    reviewValidationNodes.LatitudeNode = null;
                    reviewValidationNodes.LongitudeNode = null;
                    reviewValidationNodes.ReviewDetailIteratorNode = responseDoc.DocumentNode.SelectSingleNode(parameters.ReviewDetailIteratorPath);
                    reviewValidationNodes.NoReviewTextNode = null;
                    break;
                case ReviewSourceEnum.CitySearch:
                    reviewValidationNodes.ReviewCountNode = responseDoc.DocumentNode.SelectSingleNode(parameters.ReviewCountPath);
                    reviewValidationNodes.RatingValueNode = responseDoc.DocumentNode.SelectSingleNode(parameters.RatingValuePath);
                    reviewValidationNodes.LocationNameNode = responseDoc.DocumentNode.SelectSingleNode(parameters.LocationNamePath);
                    reviewValidationNodes.PhoneNode = responseDoc.DocumentNode.SelectSingleNode(parameters.PhonePath);
                    reviewValidationNodes.AddressNode = responseDoc.DocumentNode.SelectSingleNode(parameters.AddressPath);
                    reviewValidationNodes.CityNode = responseDoc.DocumentNode.SelectSingleNode(parameters.CityPath);
                    reviewValidationNodes.StateNode = responseDoc.DocumentNode.SelectSingleNode(parameters.StatePath);
                    reviewValidationNodes.ZipNode = responseDoc.DocumentNode.SelectSingleNode(parameters.ZipPath);
                    reviewValidationNodes.WebsiteURLNode = responseDoc.DocumentNode.SelectSingleNode(parameters.WebsiteURLPath);
                    reviewValidationNodes.ManufacturerNode = null;
                    reviewValidationNodes.LatitudeNode = null;
                    reviewValidationNodes.LongitudeNode = null;
                    reviewValidationNodes.ReviewDetailIteratorNode = responseDoc.DocumentNode.SelectSingleNode(parameters.ReviewDetailIteratorPath);
                    reviewValidationNodes.NoReviewTextNode = responseDoc.DocumentNode.SelectSingleNode(parameters.NoReviewTextPath);
                    //reviewValidationNodes.NoRatingNode = responseDoc.DocumentNode.SelectSingleNode(parameters.NoRatingPath);
                    break;
                case ReviewSourceEnum.JudysBook:
                    reviewValidationNodes.ReviewCountNode = responseDoc.DocumentNode.SelectSingleNode(parameters.ReviewCountPath);
                    reviewValidationNodes.RatingValueNode = responseDoc.DocumentNode.SelectSingleNode(parameters.RatingValuePath);
                    reviewValidationNodes.LocationNameNode = responseDoc.DocumentNode.SelectSingleNode(parameters.LocationNamePath);
                    reviewValidationNodes.PhoneNode = responseDoc.DocumentNode.SelectSingleNode(parameters.PhonePath);
                    reviewValidationNodes.AddressNode = responseDoc.DocumentNode.SelectSingleNode(parameters.AddressPath);
                    reviewValidationNodes.CityNode = responseDoc.DocumentNode.SelectSingleNode(parameters.CityPath);
                    reviewValidationNodes.StateNode = responseDoc.DocumentNode.SelectSingleNode(parameters.StatePath);
                    reviewValidationNodes.ZipNode = responseDoc.DocumentNode.SelectSingleNode(parameters.ZipPath);
                    reviewValidationNodes.WebsiteURLNode = responseDoc.DocumentNode.SelectSingleNode(parameters.WebsiteURLPath);
                    reviewValidationNodes.ManufacturerNode = null;
                    reviewValidationNodes.LatitudeNode = null;
                    reviewValidationNodes.LongitudeNode = null;
                    reviewValidationNodes.ReviewDetailIteratorNode = responseDoc.DocumentNode.SelectSingleNode(parameters.ReviewDetailIteratorPath);
                    reviewValidationNodes.NoReviewTextNode = responseDoc.DocumentNode.SelectSingleNode(parameters.NoReviewTextPath);
                    //reviewValidationNodes.NoRatingNode = responseDoc.DocumentNode.SelectSingleNode(parameters.NoRatingPath);
                    break;
                case ReviewSourceEnum.InsiderPages:
                    //reviewValidationNodes.ReviewCountNode = null;
                    //reviewValidationNodes.RatingValueNode = null;
                    //reviewValidationNodes.LocationNameNode = responseDoc.DocumentNode.SelectSingleNode(parameters.LocationNamePath);
                    //reviewValidationNodes.PhoneNode = responseDoc.DocumentNode.SelectSingleNode(parameters.PhonePath);
                    //reviewValidationNodes.AddressNode = responseDoc.DocumentNode.SelectSingleNode(parameters.AddressPath);
                    //reviewValidationNodes.CityNode = responseDoc.DocumentNode.SelectSingleNode(parameters.CityPath);
                    //reviewValidationNodes.StateNode = responseDoc.DocumentNode.SelectSingleNode(parameters.StatePath);
                    //reviewValidationNodes.ZipNode = responseDoc.DocumentNode.SelectSingleNode(parameters.ZipPath);
                    //reviewValidationNodes.WebsiteURLNode = responseDoc.DocumentNode.SelectSingleNode(parameters.WebsiteURLPath);
                    //reviewValidationNodes.ManufacturerNode = null;
                    //reviewValidationNodes.LatitudeNode = null;
                    //reviewValidationNodes.LongitudeNode = null;
                    //reviewValidationNodes.ReviewDetailIteratorNode = null;
                    //reviewValidationNodes.NoReviewTextNode = null;
                    break;
            }

            return reviewValidationNodes;
        }

       static string CleanInput(string strIn)
        {
            // Replace invalid characters with empty strings. 
            try
            {
                string temp;
                temp = strIn.Replace(" Str.", " Street").Replace(" Ct.", " Court").Replace(" Blvd.", " Boulevard").Replace(" Dr.", " Drive").Replace(" Rd.", "Road").Replace(".", "").Trim().ToLower();
                return Regex.Replace(temp, @"[^\w\.@-]", "", RegexOptions.None, TimeSpan.FromSeconds(1.5));
            }
            // If we timeout when replacing invalid characters,  
            // we should return Empty. 
            catch (RegexMatchTimeoutException)
            {
                return String.Empty;
            }
        }

    }
}
