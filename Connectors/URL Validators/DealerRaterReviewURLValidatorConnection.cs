﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Connectors.Parameters;
using Connectors.URL_Validators.Parameters;
using HtmlAgilityPack;
using SI.DTO;

namespace Connectors.URL_Validators
{
    public class DealerRaterReviewURLValidatorConnection : SIReviewURLValidatorConnection
    {
        public DealerRaterReviewURLValidatorConnection(DealerRaterReviewURLValidatorParameters cp) : base(cp)
        {
            Parameters = cp;
        }

        public DealerRaterReviewURLValidatorParameters Parameters { get; set; }
        
        public override string HarvestReviewURL()
        {
            return "";
        }

        public override ReviewURLValidationResult ExtractPageInfo(HtmlDocument responseDoc)
        {
            var result = new ReviewURLValidationResult();

            // Determine if Page is valid and retrievable

            if (responseDoc == null)
            {
                result.IsValidURL = false;
            }
            else
            {
                result.IsValidURL = true;
                // Retrieve Account Info from Page

                var temp1 = string.Empty;

                //Load Values into HtmlNodes 
                var htmlNodes = base.LoadNode(responseDoc, ReviewSourceEnum.DealerRater, Parameters.XPaths);

                //Find Each Field
                #region LocationName
                if (htmlNodes.LocationNameNode != null)
                {
                    result.AccountName = htmlNodes.LocationNameNode.InnerText;
                }
                else
                {
                    result.AccountName = "";
                    result.MisMatchesList.Add("LocationName");
                    result.MismatchCount++;
                }
                #endregion

                #region Phone
                if (htmlNodes.PhoneNode != null)
                {
                    var fullText = htmlNodes.PhoneNode.InnerHtml.Replace("<br />", "<br>");
                    var fullTextSplit = Regex.Split(fullText, "<br>");
                    var salesPhone = Regex.Replace(fullTextSplit[0].Trim(), "[^0-9]+", string.Empty);
                    if (salesPhone != null)
                    {
                        result.AccountDetails.Phone = salesPhone.ToString();
                    }
                }
                else
                {
                    result.MisMatchesList.Add("Phone");
                    result.MismatchCount++;
                }
                #endregion

                #region Address
                if (htmlNodes.AddressNode != null)
                {
                    result.AccountDetails.Address = htmlNodes.AddressNode.InnerText;
                }
                else
                {
                    result.MisMatchesList.Add("Address");
                    result.MismatchCount++;
                }
                #endregion

                #region City
                if (htmlNodes.CityNode != null)
                {
                    result.AccountDetails.City = htmlNodes.CityNode.InnerText;
                }
                else
                {
                    result.MisMatchesList.Add("City");
                    result.MismatchCount++;
                }
                #endregion

                #region State
                if (htmlNodes.StateNode != null)
                {
                    result.AccountDetails.StateFull = htmlNodes.StateNode.InnerText;
                }
                else
                {
                    result.MisMatchesList.Add("State");
                    result.MismatchCount++;
                }
                #endregion

                #region ZipCode
                if (htmlNodes.ZipNode != null)
                {
                    result.AccountDetails.Zip = htmlNodes.ZipNode.InnerText;
                }
                else
                {
                    result.MisMatchesList.Add("Zip");
                    result.MismatchCount++;
                }
                #endregion

                #region WebsiteURL
                if (htmlNodes.WebsiteURLNode != null)
                {
                    temp1 = htmlNodes.WebsiteURLNode.GetAttributeValue("href", "");
                    temp1 = temp1.Replace("/redirect.aspx?url=", "");
                    result.AccountDetails.WebsiteURL = temp1;
                }
                else
                {
                    result.MisMatchesList.Add("WebsiteURL");
                    result.MismatchCount++;
                }
                #endregion

                #region ReviewCount
                if (htmlNodes.ReviewCountNode != null)
                {
                    temp1 = htmlNodes.ReviewCountNode.InnerText.Replace("based on", "").Replace("reviews", "").Replace("review", "").Trim();
                    result.ReviewDetails.ReviewCount = Convert.ToInt32(temp1);
                }
                else
                {
                    result.MisMatchesList.Add("ReviewCount");
                    result.MismatchCount++;
                }
                #endregion

                #region Rating
                if (htmlNodes.RatingValueNode != null)
                {
                    temp1 = htmlNodes.RatingValueNode.InnerText.Trim();
                    decimal dValue;
                    if (decimal.TryParse(temp1, out dValue))
                        result.ReviewDetails.Rating = dValue;
                }
                else
                {
                    result.MisMatchesList.Add("Rating");
                    result.MismatchCount++;
                }
                #endregion
            }
            return result;
        }

        protected override void dispose()
        {

        }
    }
}
