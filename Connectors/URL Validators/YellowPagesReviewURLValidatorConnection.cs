﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Connectors.Parameters;
using Connectors.URL_Validators.Parameters;
using HtmlAgilityPack;
using SI;
using SI.DTO;

namespace Connectors.URL_Validators
{
    public class YellowPagesReviewURLValidatorConnection : SIReviewURLValidatorConnection
    {
        public YellowPagesReviewURLValidatorConnection(YellowPagesReviewURLValidatorParameters cp) : base(cp)
        {
            Parameters = cp;
        }

        public YellowPagesReviewURLValidatorParameters Parameters { get; set; }
        
        public override string HarvestReviewURL()
        {
            return "";
        }

        public override ReviewURLValidationResult ExtractPageInfo(HtmlDocument responseDoc)
        {
            var result = new ReviewURLValidationResult();
            
            // Determine if Page is valid and retrievable

            if (responseDoc == null)
            {
                result.IsValidURL = false;
            }
            else
            {
                result.IsValidURL = true;
                // Retrieve Account Info from Page

                //Load Values into HtmlNodes 
                var htmlNodes = base.LoadNode(responseDoc, ReviewSourceEnum.YellowPages, Parameters.XPaths);
            
                //Find Each Field
                #region LocationName
                if (htmlNodes.LocationNameNode != null)
                {
                    result.AccountName = htmlNodes.LocationNameNode.InnerText;

                }
                else
                {
                    result.MisMatchesList.Add("LocationName");
                    result.MismatchCount++;
                } 
                #endregion

                #region Phone
                if (htmlNodes.PhoneNode != null)
                {
                    result.AccountDetails.Phone = htmlNodes.PhoneNode.InnerText;
                }
                else
                {
                    result.MisMatchesList.Add("Phone");
                    result.MismatchCount++;
                }
                #endregion

                #region Address
                if (htmlNodes.AddressNode != null)
                {
                    result.AccountDetails.Address = htmlNodes.AddressNode.InnerText;
                }
                else
                {
                    result.MisMatchesList.Add("Address");
                    result.MismatchCount++;
                }
                #endregion

                #region Address, City, State, Zip
                if (htmlNodes.CityNode != null)
                {
                    string temp1 = htmlNodes.CityNode.InnerText;

                    var cityStateZipSplit = Regex.Split(temp1, ",");

                    
                        if (cityStateZipSplit.Count() == 2)
                        {
                            result.AccountDetails.City = cityStateZipSplit[0].Trim();

                            var stateZipSplit = cityStateZipSplit[1].Trim().Split(' ');

                            if (stateZipSplit.Count() >= 2)
                            {
                                result.AccountDetails.State = stateZipSplit[0].Trim();
                                result.AccountDetails.Zip = stateZipSplit[1].Trim();
                            }
                            else
                            {
                                result.MisMatchesList.Add("State");
                                result.MisMatchesList.Add("Zip");
                                result.MismatchCount = result.MismatchCount + 2;
                            }
                        }
                        else
                        {
                            result.MisMatchesList.Add("City");
                            result.MisMatchesList.Add("State");
                            result.MisMatchesList.Add("Zip");
                            result.MismatchCount = result.MismatchCount + 3;
                        }
                }
                else
                {
                    result.MisMatchesList.Add("City, State, Zip");
                    result.MismatchCount++;
                }
                #endregion


                //#region City
                //if (htmlNodes.CityNode != null)
                //{
                //    result.AccountDetails.City = htmlNodes.CityNode.InnerText;
                //}
                //else
                //{
                //    result.MisMatchesList.Add("City");
                //    result.MismatchCount++;
                //}
                //#endregion

                //#region State
                //if (htmlNodes.StateNode != null)
                //{
                //    result.AccountDetails.State = htmlNodes.StateNode.InnerText;
                //}
                //else
                //{
                //    result.MisMatchesList.Add("State");
                //    result.MismatchCount++;
                //}
                //#endregion

                //#region ZipCode
                //if (htmlNodes.ZipNode != null)
                //{
                //    result.AccountDetails.Zip = htmlNodes.ZipNode.InnerText;
                //}
                //else
                //{
                //    result.MisMatchesList.Add("Zip");
                //    result.MismatchCount++;
                //}
                //#endregion

                #region WebsiteURL
                if (htmlNodes.WebsiteURLNode != null)
                {
                    if (htmlNodes.WebsiteURLNode.GetAttributeValue("href", "") != null)
                    {
                        result.AccountDetails.WebsiteURL = htmlNodes.WebsiteURLNode.GetAttributeValue("href", "");
                    }
                }
                else
                {
                    result.MisMatchesList.Add("WebsiteURL");
                    result.MismatchCount++;
                }
                #endregion

                #region ReviewCount
                int reviewCount = 0;
                if (htmlNodes.ReviewCountNode != null)
                {
                    var temp = Regex.Replace(htmlNodes.ReviewCountNode.InnerText, "[^0-9]+", string.Empty);
                    int.TryParse(temp, out reviewCount);
                    result.ReviewDetails.ReviewCount = reviewCount;
                }
                else
                {
                    result.MisMatchesList.Add("ReviewCount");
                    result.MismatchCount++;
                }
                #endregion

                #region Rating
                if (htmlNodes.RatingValueNode != null)
                {
                    decimal ratingValue;
                    var resultString = Regex.Match(htmlNodes.RatingValueNode.InnerText, @"\d+").Value;
                    if (decimal.TryParse(resultString, out ratingValue))
                        result.ReviewDetails.Rating = ratingValue;
                }
                else
                {
                    result.MisMatchesList.Add("Rating");
                    result.MismatchCount++;
                }

                #endregion
            }
            return result;
        }

        protected override void dispose()
        {
            
        }
    }
}
