﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;
using SI;

namespace Connectors
{
    public class ReviewURLValidationResult
    {
        private string _reviewsSourceName;
        private string _accountName;

        public ReviewURLValidationResult()
        {
            AccountDetails = new ReviewURLAccountInfo();
            Competitors = new List<ReviewURLAccountInfo>();
            ReviewDetails = new ReviewURLHeader();
            ReviewsSourceName = string.Empty;
            Reasons = new List<string>();
            ReviewsSourceID = 0;
            Manufacturer = new List<string>();
            IsValidURL = false;
            MisMatchesList = new List<string>();
            MismatchCount = 0;
            ValidationMisMatch = new List<MisMatchRecord>();
        }

        public string ReviewsSourceName
        {
            get
            {
                return _reviewsSourceName;
            }
            set
            {
                _reviewsSourceName = value.HtmlStrip();
            }
        }

        public int ReviewsSourceID { get; set; }

        public string AccountName
        {
            get { return _accountName; }
            set { _accountName = value.HtmlStrip(); }
        }

        public ReviewURLAccountInfo AccountDetails { get; set; }

        public List<ReviewURLAccountInfo> Competitors { get; set; }

        public bool IsValidURL { get; set; }
        public bool IsValidLocationURL { get; set; }

        //public List<string> ValidationMisMatch { get; set; }

        public List<MisMatchRecord> ValidationMisMatch { get; set; }


        public int MismatchCount { get; set; }
        
        public List<string> Reasons { get; set; }

        public ReviewURLHeader ReviewDetails { get; set; }

        public List<string> Manufacturer { get; set; }

        public List<string> MisMatchesList { get; set; }
    }

    public class MisMatchRecord
    {
        public MisMatchRecord(string fieldName, string validatorValue, string pageValue)
        {
            FieldName = fieldName;
            ValidatorValue = validatorValue;
            PageValue = pageValue;
        }

        public string FieldName { get; set; }
        public string ValidatorValue { get; set; }
        public string PageValue { get; set; }
    }

    public class ReviewURLAccountInfo
    {
        private string _address;
        private string _city;
        private string _state;
        private string _stateFull;
        private string _zip;
        private string _phone;
        private string _latitude;
        private string _longitude;
        private string _websiteUrl;

        public ReviewURLAccountInfo()
        {
            _address = "";
            _city = "";
            _state = "";
            _stateFull = "";
            _zip = "";
            _phone = "";
            _latitude = "";
            _longitude = "";
            _websiteUrl = "";
        }


        public string Address
        {
            get { return _address; }
            set { _address = value.HtmlStrip(); }
        }

        public string City
        {
            get { return _city; }
            set { _city = value.HtmlStrip(); }
        }

        public string State
        {
            get { return _state; }
            set
            {
                _state = value.HtmlStrip();
                if (string.IsNullOrWhiteSpace(_stateFull))
                {
                    _stateFull = SI.Lookups.GetStateFullName(_state);
                }
            }
        }

        public string StateFull
        {
            get { return _stateFull; }
            set
            {
                _stateFull = value.HtmlStrip();
                if (string.IsNullOrWhiteSpace(_state))
                {
                    _state = SI.Lookups.GetStateAbbreviation(_stateFull).ToString();
                }
            }
        }

        public string Zip
        {
            get { return _zip; }
            set { _zip = value.HtmlStrip(); }
        }

        public string GeoCode { get; set; }

        public string Phone
        {
            get { return _phone; }
            set { _phone = value.HtmlStrip(); }
        }

        public string WebsiteURL
        {
            get { return _websiteUrl; }
            set { _websiteUrl = value.HtmlStrip(); }
        }

        public string FacebookURL { get; set; }
        public string TwitterURL { get; set; }
        public string GooglePlusURL { get; set; }

        public string Latitude
        {
            get { return _latitude; }
            set { _latitude = value.HtmlStrip(); }
        }

        public string Longitude
        {
            get { return _longitude; }
            set { _longitude = value.HtmlStrip(); }
        }

    }

    public class ReviewURLHeader
    {
        public ReviewURLHeader()
        {
            ExtraInfo = new List<KeyValuePair<string, string>>();
        }

        public decimal Rating { get; set; }
        public decimal RatingService { get; set; }
        public int ReviewCount { get; set; }
        public int ReviewCountService { get; set; }
        public List<KeyValuePair<string, string>> ExtraInfo { get; set; }
    }
    
}
