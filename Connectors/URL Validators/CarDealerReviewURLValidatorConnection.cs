﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Connectors.Parameters;
using Connectors.URL_Validators.Parameters;
using HtmlAgilityPack;
using SI;
using SI.DTO;

namespace Connectors.URL_Validators
{
    public class CarDealerReviewURLValidatorConnection : SIReviewURLValidatorConnection
    {
        public CarDealerReviewURLValidatorConnection(CarDealerReviewURLValidatorParameters cp) : base(cp)
        {
            Parameters = cp;
        }

        public CarDealerReviewURLValidatorParameters Parameters { get; set; }
        

        public override string HarvestReviewURL()
        {
            return "";
        }

        public override ReviewURLValidationResult ExtractPageInfo(HtmlDocument responseDoc)
        {
            var result = new ReviewURLValidationResult();

            // Determine if Page is valid and retrievable

            if (responseDoc == null)
            {
                result.IsValidURL = false;
            }
            else
            {
                result.IsValidURL = true;
                // Retrieve Account Info from Page

                var temp1 = string.Empty;

                //Load Values into HtmlNodes 
                var htmlNodes = base.LoadNode(responseDoc, ReviewSourceEnum.CarDealer, Parameters.XPaths);

                //Find Each Field
                #region LocationName
                if (htmlNodes.LocationNameNode != null)
                {
                    result.AccountName = htmlNodes.LocationNameNode.InnerText;
                }
                else
                {
                    result.MisMatchesList.Add("LocationName");
                    result.MismatchCount++;
                }
                #endregion

                #region Phone
                if (htmlNodes.PhoneNode != null)
                {
                    temp1 = htmlNodes.PhoneNode.InnerText;
                    var phoneSplit = Regex.Split(temp1, "\n");

                    if (phoneSplit.Count() == 4)
                    {
                        temp1 = phoneSplit[1].Replace("Phone: ", "");
                        result.AccountDetails.Phone = temp1;
                    }

                    else
                    {
                        result.MisMatchesList.Add("City");
                        result.MismatchCount = result.MismatchCount++;
                    }
                }
                else
                {
                    result.MisMatchesList.Add("Phone");
                    result.MismatchCount++;
                }
                #endregion

                #region Address, City, State, Zip
                if (htmlNodes.AddressNode != null)
                {
                    temp1 = htmlNodes.AddressNode.InnerText;

                    var fullAddressSplit = Regex.Split(temp1, "\n");

                    if (fullAddressSplit.Count() == 4)
                    {
                        result.AccountDetails.Address = fullAddressSplit[2];

                        var cityStateZipSplit = fullAddressSplit[3].Split(',');

                        if (cityStateZipSplit.Count() == 2)
                        {
                            result.AccountDetails.City = cityStateZipSplit[0].Trim();

                            var stateZipSplit = cityStateZipSplit[1].Trim().Split(' ');

                            if (stateZipSplit.Count() >= 2)
                            {
                                result.AccountDetails.State = stateZipSplit[0].Trim();
                                result.AccountDetails.Zip = stateZipSplit[1].Trim();
                            }
                            else
                            {
                                result.MisMatchesList.Add("State");
                                result.MisMatchesList.Add("Zip");
                                result.MismatchCount = result.MismatchCount + 2;
                            }
                        }
                        else
                        {
                            result.MisMatchesList.Add("City");
                            result.MisMatchesList.Add("State");
                            result.MisMatchesList.Add("Zip");
                            result.MismatchCount = result.MismatchCount + 3;
                        }

                    }
                    else
                    {
                        result.MisMatchesList.Add("Address");
                        result.MisMatchesList.Add("City");
                        result.MisMatchesList.Add("State");
                        result.MisMatchesList.Add("Zip");
                        result.MismatchCount = result.MismatchCount + 4;
                    }
                }
                else
                {
                    result.MisMatchesList.Add("Address");
                    result.MismatchCount++;
                }
                #endregion

                #region WebsiteURL
                if (htmlNodes.WebsiteURLNode != null)
                {
                    result.AccountDetails.WebsiteURL = htmlNodes.WebsiteURLNode.GetAttributeValue("href","");
                }
                else
                {
                    result.MisMatchesList.Add("WebsiteURL");
                    result.MismatchCount++;
                }
                #endregion

                #region ReviewCount
                if (htmlNodes.ReviewCountNode != null)
                {
                    int reviewCount;
                    var resultString = Regex.Match(htmlNodes.ReviewCountNode.InnerText, @"\d+").Value;
                    if(int.TryParse(resultString, out reviewCount))
                    result.ReviewDetails.ReviewCount = reviewCount;
                }
                else
                {
                    result.MisMatchesList.Add("ReviewCount");
                    result.MismatchCount++;
                }
                #endregion

                #region Rating
                if (htmlNodes.RatingValueNode != null)
                {
                    temp1 = htmlNodes.RatingValueNode.InnerText;
                    var ratingSplit = Regex.Split(temp1, "/5");
                    temp1 = ratingSplit[0].Replace("Rating: ", "");
                    result.ReviewDetails.Rating = Convert.ToDecimal(temp1);
                }
                else
                {
                    result.MisMatchesList.Add("Rating");
                    result.MismatchCount++;
                }
                #endregion
            }
            return result;
        }

        protected override void dispose()
        {

        }
    }
}
