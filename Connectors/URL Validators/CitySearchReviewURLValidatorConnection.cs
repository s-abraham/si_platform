﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Connectors.Parameters;
using Connectors.URL_Validators.Parameters;
using HtmlAgilityPack;
using SI;
using SI.DTO;

namespace Connectors.URL_Validators
{
    public class CitySearchReviewURLValidatorConnection : SIReviewURLValidatorConnection
    {
        public CitySearchReviewURLValidatorConnection(CitySearchReviewURLValidatorParameters cp) : base(cp)
        {
            Parameters = cp;
        }

        public CitySearchReviewURLValidatorParameters Parameters { get; set; }

        public override string HarvestReviewURL()
        {
            return "";
        }
        public override ReviewURLValidationResult ExtractPageInfo(HtmlDocument responseDoc)
        {
            var result = new ReviewURLValidationResult();

            // Determine if Page is valid and retrievable

            if (responseDoc == null)
            {
                result.IsValidURL = false;
            }
            else
            {
                result.IsValidURL = true;
                // Retrieve Account Info from Page

                var temp1 = string.Empty;

                //Load Values into HtmlNodes 
                var htmlNodes = base.LoadNode(responseDoc, ReviewSourceEnum.CitySearch, Parameters.XPaths);

                //Find Each Field
                #region LocationName
                if (htmlNodes.LocationNameNode != null)
                {
                    temp1 = htmlNodes.LocationNameNode.InnerText.HTMLDecode();
                    result.AccountName = temp1;
                }
                else
                {
                    result.MisMatchesList.Add("LocationName");
                    result.MismatchCount++;
                }
                #endregion

                #region Phone
                if (htmlNodes.PhoneNode != null)
                {
                    result.AccountDetails.Phone = htmlNodes.PhoneNode.InnerText;
                }
                else
                {
                    result.MisMatchesList.Add("Phone");
                    result.MismatchCount++;
                }
                #endregion

                #region Address
                if (htmlNodes.AddressNode != null)
                {
                    result.AccountDetails.Address = htmlNodes.AddressNode.InnerText;
                }
                else
                {
                    result.MisMatchesList.Add("Address");
                    result.MismatchCount++;
                }
                #endregion

                #region City
                if (htmlNodes.CityNode != null)
                {
                    result.AccountDetails.City = htmlNodes.CityNode.InnerText;
                }
                else
                {
                    result.MisMatchesList.Add("City");
                    result.MismatchCount++;
                }
                #endregion

                #region State
                if (htmlNodes.StateNode != null)
                {
                    result.AccountDetails.State = htmlNodes.StateNode.InnerText;
                }
                else
                {
                    result.MisMatchesList.Add("State");
                    result.MismatchCount++;
                }
                #endregion

                #region ZipCode
                if (htmlNodes.ZipNode != null)
                {
                    result.AccountDetails.Zip = htmlNodes.ZipNode.InnerText;
                }
                else
                {
                    result.MisMatchesList.Add("Zip");
                    result.MismatchCount++;
                }
                #endregion

                #region WebsiteURL
                if (htmlNodes.WebsiteURLNode != null)
                {
                    result.AccountDetails.WebsiteURL = htmlNodes.WebsiteURLNode.InnerText;
                }
                else
                {
                    result.MisMatchesList.Add("WebsiteURL");
                    result.MismatchCount++;
                }
                #endregion

                #region ReviewCount
                if (htmlNodes.ReviewCountNode != null)
                {
                    result.ReviewDetails.ReviewCount = Convert.ToInt32(htmlNodes.ReviewCountNode.InnerText.ExtractNumeric());
                }
                else
                {
                    result.MisMatchesList.Add("ReviewCount");
                    result.MismatchCount++;
                }
                #endregion

                #region Rating
                if (htmlNodes.RatingValueNode != null)
                {
                    temp1 = htmlNodes.RatingValueNode.InnerText;
                    var ratingDecimal = Convert.ToDecimal(temp1) / 100m;
                    result.ReviewDetails.Rating = Convert.ToDecimal(ratingDecimal * 5m);
                }
                else
                {
                    result.MisMatchesList.Add("Rating");
                    result.MismatchCount++;
                }
                #endregion
            }
            return result;
        }

        protected override void dispose()
        {

        }
    }
}
