﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Script.Serialization;
using Connectors.Entities;
using Connectors.Parameters;

namespace Connectors
{
    public class FacebookPostStatisticConnection : SISocialConnection
    {
        private SIFacebookPostStatisticsConnectionParameters ConnectionParameters;

        #region INITIALIZER
        public FacebookPostStatisticConnection(SIFacebookPostStatisticsConnectionParameters siFacebookPostStatisticsConnectionParameters)
            : base(siFacebookPostStatisticsConnectionParameters)
        {
            ConnectionParameters = siFacebookPostStatisticsConnectionParameters;
        }
        #endregion

        #region VARIABLE
        #endregion

        #region POST

        public FacebookPostStatistics GetPostStatistics()
        {
            //PublishManager objPublishManager = new PublishManager();
            FacebookPostStatistics objFacebookPostStatistics = new FacebookPostStatistics();
            //string fields = "id,from,message,actions,privacy,type,status_type,created_time,updated_time,comments.limit(5000).fields(id,from,message,created_time,user_likes,like_count,likes.fields(id,name)),likes.limit(50000).fields(id,name),shares,application";
            string fields = string.Empty;
            if (ConnectionParameters.postStatisticsParameters.Posttype == PostType.FacebookStatus || ConnectionParameters.postStatisticsParameters.Posttype == PostType.FacebookLink)
            {
                fields = "id,from,name,link,created_time,updated_time,likes.limit(50000).fields(id,name),comments.limit(50000).fields(id,from,like_count,message,created_time,likes.limit(50000).fields(id,name),user_likes),message,privacy,type,application,is_published,object_id,shares";
                //if (GetUserPicture)
                //{
                //    fields = "id,from,name,link,created_time,likes.limit(50000).fields(id,name),comments.limit(50000).fields(id,from,like_count,message,created_time,likes.limit(50000).fields(id,name),user_likes),message,privacy,application,shares";
                //}
            }
            else if (ConnectionParameters.postStatisticsParameters.Posttype == PostType.FacebookPhoto || ConnectionParameters.postStatisticsParameters.Posttype == PostType.FacebookMultiplePhoto)
            {
                fields = "id,from,name,link,created_time,updated_time,likes.limit(50000).fields(id,name),comments.limit(50000).fields(id,from,like_count,message,created_time,likes.limit(50000).fields(id,name),user_likes),sharedposts.limit(5000).fields(created_time,from,id,name,message)";
            }

            FacebookResponse ObjFacebookResponse = new FacebookResponse();
            PostStatistics objPostStatistics = new PostStatistics();
            try
            {
                if (!string.IsNullOrEmpty(ConnectionParameters.postStatisticsParameters.ResultID) && !string.IsNullOrEmpty(ConnectionParameters.postStatisticsParameters.Token))
                {
                    if (string.IsNullOrEmpty(ConnectionParameters.postStatisticsParameters.FacebookGraphAPIURLPostStatistics))
                    {
                        ConnectionParameters.postStatisticsParameters.FacebookGraphAPIURLPostStatistics = "https://graph.facebook.com/{0}?fields={1}&access_token={2}";
                    }

                    string URL = string.Format(ConnectionParameters.postStatisticsParameters.FacebookGraphAPIURLPostStatistics, ConnectionParameters.postStatisticsParameters.ResultID, fields, ConnectionParameters.postStatisticsParameters.Token);

                    string rawResponse = string.Empty;

                    try
                    {                        
                        HttpRequestResults = string.Empty;

                        ConnectionStatus httpStatus = ConnectorHttpRequest(URL);
                        if (httpStatus == ConnectionStatus.Success)
                        {
                            if (!string.IsNullOrEmpty(HttpRequestResults))
                            {
                                rawResponse = HttpRequestResults;

                                var jss = new JavaScriptSerializer();
                                if (rawResponse.StartsWith(@"{""error"":"))
                                {
                                    rawResponse = rawResponse.Replace(@"{""error"":", "");
                                    rawResponse = rawResponse.Substring(0, rawResponse.Length - 1);

                                    ObjFacebookResponse = jss.Deserialize<FacebookResponse>(rawResponse);
                                }
                                else
                                {
                                    objPostStatistics = jss.Deserialize<PostStatistics>(rawResponse);

                                    if (ConnectionParameters.postStatisticsParameters.Posttype == PostType.FacebookPhoto || ConnectionParameters.postStatisticsParameters.Posttype == PostType.FacebookMultiplePhoto)
                                    {
                                        objPostStatistics.message = objPostStatistics.name;
                                    }

                                    #region GetUserPicture
                                    //if (GetUserPicture)
                                    //{
                                    //    if (objPostStatistics.comments != null)
                                    //    {
                                    //        foreach (var item in objPostStatistics.comments.data)
                                    //        {
                                    //            //Get User Picture
                                    //            FaceBookUserPicture objFaceBookUserPicture = FaceBookUserPicture(item.from.id);
                                    //            if (objFaceBookUserPicture != null)
                                    //            {
                                    //                if (objFaceBookUserPicture.picture.data != null)
                                    //                {
                                    //                    item.from.pictureurl = objFaceBookUserPicture.picture.data.url;
                                    //                }
                                    //                else
                                    //                {
                                    //                    item.from.pictureurl = string.Empty;
                                    //                }
                                    //            }
                                    //            else
                                    //            {
                                    //                item.from.pictureurl = string.Empty;
                                    //            }

                                    //        }
                                    //    }

                                    //    if (objPostStatistics != null)
                                    //    {
                                    //        //Get User Picture
                                    //        FaceBookUserPicture objFaceBookUserPicture = FaceBookUserPicture(objPostStatistics.from.id);
                                    //        if (objFaceBookUserPicture != null)
                                    //        {
                                    //            if (objFaceBookUserPicture.picture.data != null)
                                    //            {
                                    //                objPostStatistics.from.pictureurl = objFaceBookUserPicture.picture.data.url;
                                    //            }
                                    //            else
                                    //            {
                                    //                objPostStatistics.from.pictureurl = string.Empty;
                                    //            }
                                    //        }
                                    //        else
                                    //        {
                                    //            objPostStatistics.from.pictureurl = string.Empty;
                                    //        }
                                    //    }
                                    //}
                                    #endregion

                                    if (rawResponse == @"{""id"":""" + "" + ConnectionParameters.postStatisticsParameters.ResultID + "" + @"""}")
                                    {
                                        objPostStatistics.is_Deleted = true;
                                    }
                                }

                                objFacebookPostStatistics.facebookResponse = ObjFacebookResponse;
                                objFacebookPostStatistics.postStatistics = objPostStatistics;
                            }
                            else
                            {
                                ObjFacebookResponse.message = "Problem to Retrying PostStatistics from FaceBook.";

                                objFacebookPostStatistics.facebookResponse = ObjFacebookResponse;
                                objFacebookPostStatistics.postStatistics = objPostStatistics;
                            }
                        }
                    }

                    catch (Exception ex)
                    {
                        #region ERROR HANDLING

                        //PublishExceptionLog ObjPublishExceptionLog = new PublishExceptionLog();
                        //ObjPublishExceptionLog.DealerLocationSocialNetworkPostID = 0;
                        //ObjPublishExceptionLog.DealerLocationSocialNetworkPostQueueID = 0;
                        //ObjPublishExceptionLog.ExceptionMessage = ex.Message;
                        //ObjPublishExceptionLog.InnerException = ex.InnerException == null ? "" : ex.InnerException.Message;
                        //ObjPublishExceptionLog.PostResponse = string.Empty;
                        //ObjPublishExceptionLog.StackTrace = ex.StackTrace;
                        //ObjPublishExceptionLog.MethodName = "FaceBook\\GetFacebookPostStatistics";
                        //ObjPublishExceptionLog.ExtraInfo = rawResponse;
                        //objPublishManager.AddPublishExceptionLog(ObjPublishExceptionLog);

                        #endregion
                    }
                }
                else
                {
                    ObjFacebookResponse.message = "Invalid Parameters";

                    objFacebookPostStatistics.facebookResponse = ObjFacebookResponse;
                    objFacebookPostStatistics.postStatistics = objPostStatistics;
                }


            }
            catch (Exception ex)
            {
                #region ERROR HANDLING

                //PublishExceptionLog ObjPublishExceptionLog = new PublishExceptionLog();
                //ObjPublishExceptionLog.DealerLocationSocialNetworkPostID = 0;
                //ObjPublishExceptionLog.DealerLocationSocialNetworkPostQueueID = 0;
                //ObjPublishExceptionLog.ExceptionMessage = ex.Message;
                //ObjPublishExceptionLog.InnerException = ex.InnerException == null ? "" : ex.InnerException.Message;
                //ObjPublishExceptionLog.PostResponse = string.Empty;
                //ObjPublishExceptionLog.StackTrace = ex.StackTrace;
                //ObjPublishExceptionLog.MethodName = "FaceBook\\GetFacebookPostStatistics";
                //ObjPublishExceptionLog.ExtraInfo = string.Empty;
                //objPublishManager.AddPublishExceptionLog(ObjPublishExceptionLog);

                #endregion
            }
            
            return objFacebookPostStatistics;
        }

        public FacebookPostStatisticsFeedID GetPostStatisticsByFeedId()
        {
            //PublishManager objPublishManager = new PublishManager();
            FacebookPostStatisticsFeedID objFacebookPostStatisticsFeedID = new FacebookPostStatisticsFeedID();

            FacebookResponse ObjFacebookResponse = new FacebookResponse();
            PostStatisticsByFeedID objPostStatisticsByFeedID = new PostStatisticsByFeedID();
            try
            {
                if (!string.IsNullOrEmpty(ConnectionParameters.postStatisticsParameters.FeedID) && !string.IsNullOrEmpty(ConnectionParameters.postStatisticsParameters.Token))
                {
                    if (string.IsNullOrEmpty(ConnectionParameters.postStatisticsParameters.FacebookGraphAPIURLPostStatisticsByFeedID))
                    {
                        ConnectionParameters.postStatisticsParameters.FacebookGraphAPIURLPostStatisticsByFeedID = "https://graph.facebook.com/{0}?access_token={1}";
                    }

                    string URL = string.Format(ConnectionParameters.postStatisticsParameters.FacebookGraphAPIURLPostStatisticsByFeedID, ConnectionParameters.postStatisticsParameters.FeedID, ConnectionParameters.postStatisticsParameters.Token);

                    string rawResponse = string.Empty;

                    try
                    {
                        HttpRequestResults = string.Empty;

                        ConnectionStatus httpStatus = ConnectorHttpRequest(URL);
                        if (httpStatus == ConnectionStatus.Success)
                        {
                            if (!string.IsNullOrEmpty(HttpRequestResults))
                            {
                                rawResponse = HttpRequestResults;

                                var jss = new JavaScriptSerializer();
                                if (rawResponse.StartsWith(@"{""error"":"))
                                {
                                    rawResponse = rawResponse.Replace(@"{""error"":", "");
                                    rawResponse = rawResponse.Substring(0, rawResponse.Length - 1);

                                    ObjFacebookResponse = jss.Deserialize<FacebookResponse>(rawResponse);
                                }
                                else
                                {
                                    objPostStatisticsByFeedID = jss.Deserialize<PostStatisticsByFeedID>(rawResponse);

                                    #region GetUserPicture
                                    //if (GetUserPicture)
                                    //{
                                    //    if (objPostStatisticsByFeedID.comments != null)
                                    //    {
                                    //        foreach (var item in objPostStatisticsByFeedID.comments.data)
                                    //        {
                                    //            //Get User Picture
                                    //            FaceBookUserPicture objFaceBookUserPicture = FaceBookUserPicture(item.from.id);
                                    //            if (objFaceBookUserPicture != null)
                                    //            {
                                    //                if (objFaceBookUserPicture.picture.data != null)
                                    //                {
                                    //                    item.from.pictureurl = objFaceBookUserPicture.picture.data.url;
                                    //                }
                                    //                else
                                    //                {
                                    //                    item.from.pictureurl = string.Empty;
                                    //                }
                                    //            }
                                    //            else
                                    //            {
                                    //                item.from.pictureurl = string.Empty;
                                    //            }

                                    //        }
                                    //    }

                                    //    if (objPostStatisticsByFeedID != null)
                                    //    {
                                    //        //Get User Picture
                                    //        FaceBookUserPicture objFaceBookUserPicture = FaceBookUserPicture(objPostStatisticsByFeedID.from.id);
                                    //        if (objFaceBookUserPicture != null)
                                    //        {
                                    //            if (objFaceBookUserPicture.picture.data != null)
                                    //            {
                                    //                objPostStatisticsByFeedID.from.pictureurl = objFaceBookUserPicture.picture.data.url;
                                    //            }
                                    //            else
                                    //            {
                                    //                objPostStatisticsByFeedID.from.pictureurl = string.Empty;
                                    //            }
                                    //        }
                                    //        else
                                    //        {
                                    //            objPostStatisticsByFeedID.from.pictureurl = string.Empty;
                                    //        }
                                    //    }
                                    //}
                                    #endregion

                                    if (rawResponse == @"{""id"":""" + "" + ConnectionParameters.postStatisticsParameters.FeedID + "" + @"""}")
                                    {
                                        objPostStatisticsByFeedID.is_Deleted = true;
                                    }
                                }

                                objFacebookPostStatisticsFeedID.facebookResponse = ObjFacebookResponse;
                                objFacebookPostStatisticsFeedID.postStatisticsbyfeedid = objPostStatisticsByFeedID;

                            }
                            else
                            {
                                ObjFacebookResponse.message = "Problem to Retrying PostStatistics By FeedID from FaceBook.";

                                objFacebookPostStatisticsFeedID.facebookResponse = ObjFacebookResponse;
                                objFacebookPostStatisticsFeedID.postStatisticsbyfeedid = objPostStatisticsByFeedID;
                            }
                        }                                               
                    }
                    catch (Exception ex)
                    {
                        #region ERROR HANDLING

                        //PublishExceptionLog ObjPublishExceptionLog = new PublishExceptionLog();
                        //ObjPublishExceptionLog.DealerLocationSocialNetworkPostID = 0;
                        //ObjPublishExceptionLog.DealerLocationSocialNetworkPostQueueID = 0;
                        //ObjPublishExceptionLog.ExceptionMessage = ex.Message;
                        //ObjPublishExceptionLog.InnerException = ex.InnerException == null ? "" : ex.InnerException.Message;
                        //ObjPublishExceptionLog.PostResponse = string.Empty;
                        //ObjPublishExceptionLog.StackTrace = ex.StackTrace;
                        //ObjPublishExceptionLog.MethodName = "FaceBook\\GetFacebookPostStatisticsByFeedId";
                        //ObjPublishExceptionLog.ExtraInfo = rawResponse;
                        //objPublishManager.AddPublishExceptionLog(ObjPublishExceptionLog);

                        #endregion
                    }
                }
                else
                {
                    ObjFacebookResponse.message = "Invalid Parameters";

                    objFacebookPostStatisticsFeedID.facebookResponse = ObjFacebookResponse;
                    objFacebookPostStatisticsFeedID.postStatisticsbyfeedid = objPostStatisticsByFeedID;
                }


            }
            catch (Exception ex)
            {
                #region ERROR HANDLING

                //PublishExceptionLog ObjPublishExceptionLog = new PublishExceptionLog();
                //ObjPublishExceptionLog.DealerLocationSocialNetworkPostID = 0;
                //ObjPublishExceptionLog.DealerLocationSocialNetworkPostQueueID = 0;
                //ObjPublishExceptionLog.ExceptionMessage = ex.Message;
                //ObjPublishExceptionLog.InnerException = ex.InnerException == null ? "" : ex.InnerException.Message;
                //ObjPublishExceptionLog.PostResponse = string.Empty;
                //ObjPublishExceptionLog.StackTrace = ex.StackTrace;
                //ObjPublishExceptionLog.MethodName = "FaceBook\\GetFacebookPostStatisticsByFeedId";
                //ObjPublishExceptionLog.ExtraInfo = string.Empty;
                //objPublishManager.AddPublishExceptionLog(ObjPublishExceptionLog);

                #endregion
            }


            return objFacebookPostStatisticsFeedID;
        }

        #endregion

        #region EVENT

        public FacebookEventStatistics GetEventStatistics()
        {
            //PublishManager objPublishManager = new PublishManager();

            string fields = "name,id,rsvp_status";
            string limit = "50000";
            string summary = "1";
            EventStatistics objEventStatistics = new EventStatistics();
            FacebookResponse ObjFacebookResponse = new FacebookResponse();
            FacebookEventStatistics objFacebookEventStatistics = new FacebookEventStatistics();
            try
            {
                if (!string.IsNullOrEmpty(ConnectionParameters.postStatisticsParameters.ResultID) && !string.IsNullOrEmpty(ConnectionParameters.postStatisticsParameters.Token))
                {
                    if (string.IsNullOrEmpty(ConnectionParameters.postStatisticsParameters.FacebookGraphAPIURLEventStatistics))
                    {
                        ConnectionParameters.postStatisticsParameters.FacebookGraphAPIURLEventStatistics = "https://graph.facebook.com/{0}/invited?fields={1}&limit={2}&summary={3}&access_token={4}";
                    }

                    string URL = string.Format(ConnectionParameters.postStatisticsParameters.FacebookGraphAPIURLEventStatistics, ConnectionParameters.postStatisticsParameters.ResultID, fields, limit, summary, ConnectionParameters.postStatisticsParameters.Token);

                    string rawResponse = string.Empty;

                    try
                    {
                        HttpRequestResults = string.Empty;

                        ConnectionStatus httpStatus = ConnectorHttpRequest(URL);
                        if (httpStatus == ConnectionStatus.Success)
                        {
                            if (!string.IsNullOrEmpty(HttpRequestResults))
                            {
                                rawResponse = HttpRequestResults;

                                var jss = new JavaScriptSerializer();
                                if (rawResponse.StartsWith(@"{""error"":"))
                                {
                                    rawResponse = rawResponse.Replace(@"{""error"":", "");
                                    rawResponse = rawResponse.Substring(0, rawResponse.Length - 1);

                                    ObjFacebookResponse = jss.Deserialize<FacebookResponse>(rawResponse);
                                }
                                else
                                {
                                    objEventStatistics = jss.Deserialize<EventStatistics>(rawResponse);
                                }

                                objFacebookEventStatistics.facebookResponse = ObjFacebookResponse;
                                objFacebookEventStatistics.eventStatistics = objEventStatistics;
                            }
                            else
                            {
                                ObjFacebookResponse.message = "Problem to Retrying Event Statistics from FaceBook.";

                                objFacebookEventStatistics.facebookResponse = ObjFacebookResponse;
                                objFacebookEventStatistics.eventStatistics = objEventStatistics;
                            }

                        }                     
                    }

                    catch (Exception ex)
                    {
                        #region ERROR HANDLING

                        //PublishExceptionLog ObjPublishExceptionLog = new PublishExceptionLog();
                        //ObjPublishExceptionLog.DealerLocationSocialNetworkPostID = 0;
                        //ObjPublishExceptionLog.DealerLocationSocialNetworkPostQueueID = 0;
                        //ObjPublishExceptionLog.ExceptionMessage = ex.Message;
                        //ObjPublishExceptionLog.InnerException = ex.InnerException == null ? "" : ex.InnerException.Message;
                        //ObjPublishExceptionLog.PostResponse = string.Empty;
                        //ObjPublishExceptionLog.StackTrace = ex.StackTrace;
                        //ObjPublishExceptionLog.MethodName = "FaceBook\\GetFacebookEventStatistics";
                        //ObjPublishExceptionLog.ExtraInfo = rawResponse;
                        //objPublishManager.AddPublishExceptionLog(ObjPublishExceptionLog);

                        #endregion
                    }

                }
                else
                {
                    ObjFacebookResponse.message = "Invalid Parameters";

                    objFacebookEventStatistics.facebookResponse = ObjFacebookResponse;
                    objFacebookEventStatistics.eventStatistics = objEventStatistics;
                }
            }
            catch (Exception ex)
            {
                #region ERROR HANDLING

                //PublishExceptionLog ObjPublishExceptionLog = new PublishExceptionLog();
                //ObjPublishExceptionLog.DealerLocationSocialNetworkPostID = 0;
                //ObjPublishExceptionLog.DealerLocationSocialNetworkPostQueueID = 0;
                //ObjPublishExceptionLog.ExceptionMessage = ex.Message;
                //ObjPublishExceptionLog.InnerException = ex.InnerException == null ? "" : ex.InnerException.Message;
                //ObjPublishExceptionLog.PostResponse = string.Empty;
                //ObjPublishExceptionLog.StackTrace = ex.StackTrace;
                //ObjPublishExceptionLog.MethodName = "FaceBook\\GetFacebookEventStatistics";
                //ObjPublishExceptionLog.ExtraInfo = string.Empty;
                //objPublishManager.AddPublishExceptionLog(ObjPublishExceptionLog);

                #endregion
            }

            return objFacebookEventStatistics;
        }

        #endregion

        #region QUESTION

        public FacebookQuestionStatistics GetQuestionStatistics()
        {
            //PublishManager objPublishManager = new PublishManager();

            string fields = "created_time,id,from,is_published,question,updated_time,options.limit(300).fields(created_time,id,from,name,vote_count,votes.limit(50000).fields(id,name,gender))";

            QuestionStatistics objQuestionStatistics = new QuestionStatistics();
            FacebookResponse ObjFacebookResponse = new FacebookResponse();
            FacebookQuestionStatistics objFacebookQuestionStatistics = new FacebookQuestionStatistics();
            try
            {
                if (!string.IsNullOrEmpty(ConnectionParameters.postStatisticsParameters.ResultID) && !string.IsNullOrEmpty(ConnectionParameters.postStatisticsParameters.Token))
                {
                    if (string.IsNullOrEmpty(ConnectionParameters.postStatisticsParameters.FacebookGraphAPIURLQuestionStatistics))
                    {
                        ConnectionParameters.postStatisticsParameters.FacebookGraphAPIURLQuestionStatistics = "https://graph.facebook.com/{0}?fields={1}&access_token={2}";
                    }

                    string URL = string.Format(ConnectionParameters.postStatisticsParameters.FacebookGraphAPIURLQuestionStatistics, ConnectionParameters.postStatisticsParameters.ResultID, fields, ConnectionParameters.postStatisticsParameters.Token);

                    string rawResponse = string.Empty;

                    try
                    {
                        HttpRequestResults = string.Empty;

                        ConnectionStatus httpStatus = ConnectorHttpRequest(URL);
                        if (httpStatus == ConnectionStatus.Success)
                        {
                            if (!string.IsNullOrEmpty(HttpRequestResults))
                            {
                                rawResponse = HttpRequestResults;

                                var jss = new JavaScriptSerializer();
                                if (rawResponse.StartsWith(@"{""error"":"))
                                {
                                    rawResponse = rawResponse.Replace(@"{""error"":", "");
                                    rawResponse = rawResponse.Substring(0, rawResponse.Length - 1);

                                    ObjFacebookResponse = jss.Deserialize<FacebookResponse>(rawResponse);
                                }
                                else
                                {
                                    objQuestionStatistics = jss.Deserialize<QuestionStatistics>(rawResponse);
                                }

                                objFacebookQuestionStatistics.facebookResponse = ObjFacebookResponse;
                                objFacebookQuestionStatistics.questionStatistics = objQuestionStatistics;
                            }
                            else
                            {
                                ObjFacebookResponse.message = "Problem to Retrying Event Statistics from FaceBook.";

                                objFacebookQuestionStatistics.facebookResponse = ObjFacebookResponse;
                                objFacebookQuestionStatistics.questionStatistics = objQuestionStatistics;
                            }
                        }                       
                    }
                    catch (Exception ex)
                    {
                        #region ERROR HANDLING

                        //PublishExceptionLog ObjPublishExceptionLog = new PublishExceptionLog();
                        //ObjPublishExceptionLog.DealerLocationSocialNetworkPostID = 0;
                        //ObjPublishExceptionLog.DealerLocationSocialNetworkPostQueueID = 0;
                        //ObjPublishExceptionLog.ExceptionMessage = ex.Message;
                        //ObjPublishExceptionLog.InnerException = ex.InnerException == null ? "" : ex.InnerException.Message;
                        //ObjPublishExceptionLog.PostResponse = string.Empty;
                        //ObjPublishExceptionLog.StackTrace = ex.StackTrace;
                        //ObjPublishExceptionLog.MethodName = "FaceBook\\GetFacebookQuestionStatistics";
                        //ObjPublishExceptionLog.ExtraInfo = rawResponse;
                        //objPublishManager.AddPublishExceptionLog(ObjPublishExceptionLog);

                        #endregion
                    }

                }
                else
                {
                    ObjFacebookResponse.message = "Invalid Parameters";

                    objFacebookQuestionStatistics.facebookResponse = ObjFacebookResponse;
                    objFacebookQuestionStatistics.questionStatistics = objQuestionStatistics;
                }
            }
            catch (Exception ex)
            {
                #region ERROR HANDLING

                //PublishExceptionLog ObjPublishExceptionLog = new PublishExceptionLog();
                //ObjPublishExceptionLog.DealerLocationSocialNetworkPostID = 0;
                //ObjPublishExceptionLog.DealerLocationSocialNetworkPostQueueID = 0;
                //ObjPublishExceptionLog.ExceptionMessage = ex.Message;
                //ObjPublishExceptionLog.InnerException = ex.InnerException == null ? "" : ex.InnerException.Message;
                //ObjPublishExceptionLog.PostResponse = string.Empty;
                //ObjPublishExceptionLog.StackTrace = ex.StackTrace;
                //ObjPublishExceptionLog.MethodName = "FaceBook\\GetFacebookQuestionStatistics";
                //ObjPublishExceptionLog.ExtraInfo = string.Empty;
                //objPublishManager.AddPublishExceptionLog(ObjPublishExceptionLog);

                #endregion
            }

            return objFacebookQuestionStatistics;
        }

        #endregion

        #region OVERRIDE METHODS
        protected override void dispose()
        {

        }
        #endregion
    }
}
