﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Connectors.URLHarvester.Parameters;
using HtmlAgilityPack;
using SI;

namespace Connectors.URLHarvester
{
    public abstract class SIHarvesterConnection : SIConnection
    {
        /// <summary>
        /// Average Rating from API Results
        /// </summary>
        public string URL { get; set; }

        protected SIHarvesterConnection(SIHarvesterConnectionParameters siHarvesterConnectionParameters)
            : base(siHarvesterConnectionParameters)
        {
            URL = string.Empty;
        }

        protected string GetAttributeValue(HtmlNode node, string xPath, string attributeName, bool allowNull = false)
        {
            if (node != null)
            {
                string selectedAttributeValue  = string.Empty;
                if (xPath == string.Empty)
                    selectedAttributeValue = node.GetAttributeValue(attributeName, "");
                else
                    selectedAttributeValue = node.SelectSingleNode(xPath).GetAttributeValue(attributeName,"");                

                if (!allowNull && string.IsNullOrEmpty(selectedAttributeValue))
                    FailedXPaths.Add(xPath);
                return selectedAttributeValue;
            }
            return null;
        }

        protected HtmlNode SelectSingleNode(HtmlNode node, string xPath, bool allowNull = false)
        {
            if (node != null && !string.IsNullOrEmpty(xPath))
            {
                HtmlNode selectedNode = node.SelectSingleNode(xPath);
                if (!allowNull && selectedNode == null)
                    FailedXPaths.Add(xPath);
                return selectedNode;
            }
            return null;
        }

        protected HtmlNodeCollection SelectNodes(HtmlNode node, string xPath, bool allowNull = false)
        {
            if (node != null && !string.IsNullOrEmpty(xPath))
            {
                HtmlNodeCollection nodeCollection = node.SelectNodes(xPath);
                if (!allowNull && nodeCollection == null)
                    FailedXPaths.Add(xPath);
                return nodeCollection;
            }
            return null;
        }
    }
}
