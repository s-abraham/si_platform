﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Connectors.URLHarvester.Parameters
{
    public class SIURLHarvesterXPathParameters
    {
        public string GoogleSectionPath { get; set; }
        public string GoogleURLPath { get; set; }
        public string GoogleMoreReviewsPath { get; set; }

        public string GoogleMoreReviewsListPath { get; set; }
        
        public string Google_QueryBasePath1 { get; set; }
        public string Google_QueryBasePath2 { get; set; }

        public string Edmunds_QueryBasePath { get; set; }

        public string EdmundsDealeListParentXPath { get; set; }
        public string EdmundsDealeListChildXPath { get; set; }

        public string QueryBasePath { get; set; }
        public string QueryIterator { get; set; }
        public string QueryURL { get; set; }

    }
}
