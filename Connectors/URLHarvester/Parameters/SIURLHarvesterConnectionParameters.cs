﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Connectors.URLHarvester.Parameters
{
    public class SIURLHarvesterConnectionParameters : SIHarvesterConnectionParameters
    {
        #region Properties

		public SIURLHarvesterXPathParameters XPathParameters;

		#endregion

        public SIURLHarvesterConnectionParameters(SIURLHarvesterXPathParameters siURLHarvesterXPathParameters, string dealershipName,string physicalCity,string state,string statefull,string zipCode, int timeOutSeconds = 30, bool useProxy = true, int maxRedirects = 50)
			: base(timeOutSeconds, useProxy, maxRedirects)
		{
            DealershipName = dealershipName;
            PhysicalCity = physicalCity;
            State = state;
            Statefull = statefull;
            ZipCode = zipCode;

            XPathParameters = siURLHarvesterXPathParameters;
		}
    }
}
