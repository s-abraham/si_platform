﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Connectors.Parameters;

namespace Connectors.URLHarvester.Parameters
{
    public class SIHarvesterConnectionParameters : SIConnectionParameters
    {
        //any custom paramerts for social only
        public string DealershipName {get; set;}
        public string PhysicalCity {get; set;}
        public string State { get; set; }
        public string Statefull { get; set; }
        public string ZipCode { get; set; }

        protected SIHarvesterConnectionParameters(int timeOutSeconds, bool useProxy, int maxRedirects)
			: base(timeOutSeconds, useProxy, maxRedirects)
		{
			
		}
    }
}
