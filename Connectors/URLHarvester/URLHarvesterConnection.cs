﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Connectors.Entities;
using Connectors.URLHarvester.Parameters;
using HtmlAgilityPack;
using SI.DTO;

namespace Connectors.URLHarvester
{
    public class URLHarvesterConnection : SIHarvesterConnection
    {
        private SIURLHarvesterConnectionParameters ConnectionParameters;

        #region Initializer
        public URLHarvesterConnection(SIURLHarvesterConnectionParameters siURLHarvesterConnectionParameters)
            : base(siURLHarvesterConnectionParameters)
        {
            ConnectionParameters = siURLHarvesterConnectionParameters;
        }
        #endregion
        
        #region PUBLIC METHOD

        public void GetURLs()
        {
            List<ReviewURL> listReviewURL = new List<ReviewURL>();

            try
            {
                if (!string.IsNullOrEmpty(ConnectionParameters.DealershipName) && !string.IsNullOrEmpty(ConnectionParameters.DealershipName)
                        && !string.IsNullOrEmpty(ConnectionParameters.PhysicalCity) && !string.IsNullOrEmpty(ConnectionParameters.State)
                        && !string.IsNullOrEmpty(ConnectionParameters.Statefull) && !string.IsNullOrEmpty(ConnectionParameters.ZipCode))
                {
                    List<ReviewURL> listReviewURL_Temp = new List<ReviewURL>();
                    string RequestURL = string.Empty;
                    RequestURL = ConstructQueryUrl(ReviewSourceEnum.Google, false);

                    GetReviewURL(ReviewSourceEnum.Google, RequestURL, out listReviewURL, false);
                    if (listReviewURL.Count == 0)
                    {
                        RequestURL = ConstructQueryUrl(ReviewSourceEnum.Google, true);
                        GetReviewURL(ReviewSourceEnum.Google, RequestURL, out listReviewURL, false);
                    }

                    ReviewURL data = new ReviewURL();
                    ReviewURL objReviewURL;

                    data = listReviewURL.Where(x => x.ReviewSourceID == Convert.ToInt32(ReviewSourceEnum.Edmunds)).SingleOrDefault();
                    if (data == null)
                    {
                        objReviewURL = new ReviewURL();
                        RequestURL = ConstructQueryUrl(ReviewSourceEnum.Edmunds, false);

                        objReviewURL.URL = GetReviewURL(ReviewSourceEnum.Edmunds, RequestURL, out listReviewURL_Temp);
                        objReviewURL.ReviewSourceID = Convert.ToInt32(ReviewSourceEnum.Edmunds);
                        objReviewURL.ReviewSourceName = Convert.ToString(ReviewSourceEnum.Edmunds);
                        listReviewURL.Add(objReviewURL);
                    }
                }
            }
            catch (Exception ex)
            {
            }            
        }

        public string GetEdmundsURL()
        {
            string result = string.Empty;

            try
            {
                string RequestURL = string.Empty;
                List<ReviewURL> listReviewURL_Temp = new List<ReviewURL>();

                if (!string.IsNullOrEmpty(ConnectionParameters.DealershipName) && !string.IsNullOrEmpty(ConnectionParameters.ZipCode))
                {
                    RequestURL = ConstructQueryUrl(ReviewSourceEnum.Edmunds , false);

                    result = GetReviewURL(ReviewSourceEnum.Edmunds , RequestURL, out listReviewURL_Temp);
                }
            }
            catch (Exception ex)
            {
            }

            return result;
        }
        #endregion

        #region PRIVATE METHOD

        private string ConstructQueryUrl(ReviewSourceEnum reviewSource, bool isReTry)
        {
            var result = string.Empty;
            var queryUrlPath = string.Empty;
            try
            {
                //Load Base Urls from App.Config
                if (ConnectionParameters.ZipCode.Length < 5)
                {
                    ConnectionParameters.ZipCode = "0" + ConnectionParameters.ZipCode;
                }
                switch (reviewSource)
                {
                    case ReviewSourceEnum.Google :
                        if (!isReTry)
                        {
                            queryUrlPath = ConnectionParameters.XPathParameters.Google_QueryBasePath1;
                        }
                        else
                        {
                            queryUrlPath = ConnectionParameters.XPathParameters.Google_QueryBasePath2;                            
                        }
                        result = queryUrlPath.Replace("{LocationName}", ConnectionParameters.DealershipName)
                                             .Replace("{City}", ConnectionParameters.PhysicalCity)
                                             .Replace("{State}", ConnectionParameters.State)
                                             .Replace("{Zip}", ConnectionParameters.ZipCode);
                        break;
                    case ReviewSourceEnum.Edmunds :
                        queryUrlPath = ConnectionParameters.XPathParameters.Edmunds_QueryBasePath;
                        result = queryUrlPath.Replace("{LocationName}", ConnectionParameters.DealershipName.Replace(" ","+"))
                                                .Replace("{Zip}", ConnectionParameters.ZipCode);
                        break;
                    default:
                        throw new ArgumentOutOfRangeException("reviewSource");
                }
            }
            catch (Exception)
            {

                throw;
            }

            return result;
        }

        public string GetReviewURL(ReviewSourceEnum reviewSource, string queryUrl, out List<ReviewURL> listReviewURL, bool ScrapCarsDotComURL = true)
        {
            var result = string.Empty;
                        
            listReviewURL = new List<ReviewURL>();
            ReviewURL objReviewURL = new ReviewURL();

            try
            {
                if (!string.IsNullOrEmpty(queryUrl))
                {
                    //SetProxyDetails(queryUrl);

                    //var responseHtml = Common.ExtractHtml(_proxyDetails);
                    HttpRequestResults = string.Empty;

                    ConnectionStatus httpStatus = ConnectorHttpRequest(queryUrl,"Mozilla/5.0 (Windows NT 6.1; WOW64; rv:22.0) Gecko/20100101 Firefox/22.0");
                    if (httpStatus == ConnectionStatus.Success)
                    {
                        switch (reviewSource)
                        {
                            case ReviewSourceEnum.Google :
                                result = ExtractGoogleURL(HttpRequestResults, out listReviewURL, ScrapCarsDotComURL);
                                break;
                            case ReviewSourceEnum.Edmunds :
                                result = ExtractEdmundsURL(HttpRequestResults);
                                break;
                            case ReviewSourceEnum.Yelp:
                                result = ExtractYelpURL(HttpRequestResults);
                                break;           
                            default:
                                throw new ArgumentOutOfRangeException("reviewSource");
                        }
                    }                    
                }
                else
                {

                }
            }
            catch (Exception)
            {

                throw;
            }
            return result;
        }

        private string ExtractGoogleURL(string htmlText, out List<ReviewURL> listReviewURL, bool ScrapCarsDotComURL)
        {
            var result = string.Empty;
            var dealerRaterUrl = string.Empty;
            var yahooUrl = string.Empty;
            var insiderpagesUrl = string.Empty;
            var EdmundsUrl = string.Empty;

            var SectionPath = ConnectionParameters.XPathParameters.GoogleSectionPath;
            var URLPath = ConnectionParameters.XPathParameters.GoogleURLPath;
            var moreReviewsPath = ConnectionParameters.XPathParameters.GoogleMoreReviewsPath;
            listReviewURL = new List<ReviewURL>();
            ReviewURL objReviewURL;

            try
            {
                HtmlDocument htmlDocument = new HtmlDocument();
                htmlDocument.OptionFixNestedTags = true;
                htmlDocument.LoadHtml(htmlText);

                //Extract Google Review URL
                var reviewNode = SelectSingleNode(htmlDocument.DocumentNode, SectionPath);

                if (reviewNode != null)
                {
                    objReviewURL = new ReviewURL();
                    objReviewURL.URL = GetAttributeValue(htmlDocument.DocumentNode, URLPath, "href");
                    objReviewURL.ReviewSourceID = Convert.ToInt32(ReviewSourceEnum.Google );
                    objReviewURL.ReviewSourceName = Convert.ToString(ReviewSourceEnum.Google );
                    listReviewURL.Add(objReviewURL);
                }

                //Determine if page contains review urls for DealerRater, Yahoo.com, InsiderPages.com, and Edmunds.com
                //var moreReviews = SelectSingleNode(htmlDocument.DocumentNode, moreReviewsPath);

                var nodes = SelectNodes(htmlDocument.DocumentNode, ConnectionParameters.XPathParameters.GoogleMoreReviewsListPath);
                foreach (var node in nodes)
                {
                    try
                    {
                        //var url = node.GetAttributeValue("href", "");
                        var url = GetAttributeValue(node, string.Empty, "href");

                        if (!string.IsNullOrEmpty(url))
                        {
                            if (url.ToLower().Contains("www.dealerrater.com"))
                            {
                                objReviewURL = new ReviewURL();
                                objReviewURL.URL = url.ToLower().Substring(9, url.IndexOf("&") - 9);
                                objReviewURL.ReviewSourceID = Convert.ToInt32(ReviewSourceEnum.DealerRater);
                                objReviewURL.ReviewSourceName = Convert.ToString(ReviewSourceEnum.DealerRater);
                                listReviewURL.Add(objReviewURL);
                            }

                            if (url.ToLower().Contains("local.yahoo.com"))
                            {
                                objReviewURL = new ReviewURL();
                                objReviewURL.URL = url.ToLower().Substring(9, url.IndexOf("&") - 9);
                                objReviewURL.ReviewSourceID = Convert.ToInt32(ReviewSourceEnum.Yahoo);
                                objReviewURL.ReviewSourceName = Convert.ToString(ReviewSourceEnum.Yahoo);
                                listReviewURL.Add(objReviewURL);
                            }

                            if (url.ToLower().Contains("www.insiderpages.com"))
                            {
                                objReviewURL = new ReviewURL();
                                objReviewURL.URL = url.ToLower().Substring(9, url.IndexOf("&") - 9);
                                objReviewURL.ReviewSourceID = Convert.ToInt32(ReviewSourceEnum.InsiderPages);
                                objReviewURL.ReviewSourceName = Convert.ToString(ReviewSourceEnum.InsiderPages);
                                listReviewURL.Add(objReviewURL);
                            }

                            if (url.ToLower().Contains("www.cars.com") && ScrapCarsDotComURL)
                            {
                                objReviewURL = new ReviewURL();
                                objReviewURL.URL = url.ToLower().Substring(9, url.IndexOf("&") - 9) + "/";
                                objReviewURL.ReviewSourceID = Convert.ToInt32(ReviewSourceEnum.CarsDotCom);
                                objReviewURL.ReviewSourceName = Convert.ToString(ReviewSourceEnum.CarsDotCom);
                                listReviewURL.Add(objReviewURL);
                            }
                            if (url.ToLower().Contains("www.edmunds.com"))
                            {
                                objReviewURL = new ReviewURL();
                                objReviewURL.URL = url.ToLower().Substring(9, url.IndexOf("&") - 9) + "/";
                                objReviewURL.ReviewSourceID = Convert.ToInt32(ReviewSourceEnum.Edmunds );
                                objReviewURL.ReviewSourceName = Convert.ToString(ReviewSourceEnum.Edmunds );
                                listReviewURL.Add(objReviewURL);
                            }
                        }

                    }
                    catch
                    {

                    }
                }
            }
            catch (Exception ex)
            {

                // Retry With New Google Base URL
                return string.Empty; //objReviewURL.Google;

            }

            return result;
        }

        private string ExtractEdmundsURL(string htmlText)
        {
            var result = string.Empty;
            try
            {
                HtmlDocument htmlDocument = new HtmlDocument();
                htmlDocument.OptionFixNestedTags = true;
                htmlDocument.LoadHtml(htmlText);

                //string EdmundsDealeListParentXPath = "//div[@id='dealer-listing-parent']";
                //string EdmundsDealeListChildXPath = ".//div[contains(@id,'pdp_dealer_')][1]//li[contains(@class,'dlr-name')]//a";

                var Node = SelectSingleNode(htmlDocument.DocumentNode, ConnectionParameters.XPathParameters.EdmundsDealeListParentXPath);

                if (Node != null)
                {
                    result = GetAttributeValue(Node, ConnectionParameters.XPathParameters.EdmundsDealeListChildXPath, "href");
                }

                if (!string.IsNullOrEmpty(result))
                {
                    result = "http://www.edmunds.com/" + result;
                }
            }
            catch (Exception ex)
            {

            }

            return result;
        }

        private string ExtractYelpURL(string htmlText)
        {
            var result = string.Empty;
            try
            {
                HtmlDocument htmlDocument = new HtmlDocument();
                htmlDocument.OptionFixNestedTags = true;
                htmlDocument.LoadHtml(htmlText);

                var Node = SelectSingleNode(htmlDocument.DocumentNode, ConnectionParameters.XPathParameters.QueryIterator);

                if (Node != null)
                {
                    result = GetAttributeValue(Node, ConnectionParameters.XPathParameters.QueryURL, "href");
                }

                if (!string.IsNullOrEmpty(result))
                {
                    result = "http://www.edmunds.com/" + result;
                }
            }
            catch (Exception ex)
            {

            }

            return result;
        }
        
        #endregion

        protected override void dispose()
        {
            // nothing to do here
        }

    }
}
