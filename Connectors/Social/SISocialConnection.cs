﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using Connectors.Parameters;
using HtmlAgilityPack;

namespace Connectors
{
	public abstract class SISocialConnection : SIConnection
    {
        /// <summary>
        /// Average Rating from API Results
        /// </summary>
        public string ResponseJson { get; set; }



        protected SISocialConnection(SISocialConnectionParameters siSocialConnectionParameters)
            : base(siSocialConnectionParameters)
        {
            ResponseJson = string.Empty;
        }

        protected HtmlNode SelectSingleNode(HtmlNode node, string xPath, bool allowNull = false)
        {
            if (node != null && !string.IsNullOrEmpty(xPath))
            {
                HtmlNode selectedNode = node.SelectSingleNode(xPath);
                if (!allowNull && selectedNode == null)
                    FailedXPaths.Add(xPath);
                return selectedNode;
            }
            return null;
        }

        protected HtmlNodeCollection SelectNodes(HtmlNode node, string xPath, bool allowNull = false)
        {
            if (node != null && !string.IsNullOrEmpty(xPath))
            {
                HtmlNodeCollection nodeCollection = node.SelectNodes(xPath);
                if (!allowNull && nodeCollection == null)
                    FailedXPaths.Add(xPath);
                return nodeCollection;
            }
            return null;
        }

        protected string httpDownloadString(HttpWebRequest httpRequest)
        {
            string rawResponse = string.Empty;
            try
            {
                //Get Response

                using (HttpWebResponse svcResponse = (HttpWebResponse)httpRequest.GetResponse())
                {
                    using (Stream responseStream = svcResponse.GetResponseStream())
                    {
                        try
                        {
                            using (StreamReader readr = new StreamReader(responseStream, Encoding.UTF8))
                            {
                                rawResponse = readr.ReadToEnd();
                                rawResponse = rawResponse.Trim();
                            }
                        }
                        catch (Exception ex)
                        {
                            #region ERROR HANDLING
                            rawResponse = @"{""error"": {    ""message"": """ + "" + ex.Message + "" + @""", ""type"": ""Exception"",    ""code"": 000  }}";
                            #endregion
                        }
                    }

                }
            }
            catch (WebException webex)
            {
                using (var reader = new StreamReader(webex.Response.GetResponseStream()))
                {
                    var responseText = reader.ReadToEnd();
                    rawResponse = responseText;
                }
            }
            catch (Exception ex)
            {
                #region ERROR HANDLING
                rawResponse = @"{""error"": {    ""message"": """ + "" + ex.Message + "" + @""", ""type"": ""Exception"",    ""code"": 000  }}";
                #endregion
            }

            return rawResponse;
        }
	}

}
