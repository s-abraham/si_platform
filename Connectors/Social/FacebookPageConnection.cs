﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Script.Serialization;
//using Connectors.Entities;
using Connectors.Parameters;
using Facebook;
using Facebook.Entities;
using HtmlAgilityPack;
using Newtonsoft.Json;
using SI.DTO;

namespace Connectors
{
    public class FacebookPageConnection : SISocialConnection
    {

        private SIFacebookPageConnectionParameters ConnectionParameters;
       
        #region INITIALIZER
        public FacebookPageConnection(SIFacebookPageConnectionParameters siFacebookPageConnectionParameters)
            : base(siFacebookPageConnectionParameters)
        {
            ConnectionParameters = siFacebookPageConnectionParameters;
        }
        #endregion

        #region VARIABLE
        #endregion

        #region PAGE INSIGHTS

        /// <summary>
        /// Get Facebook Page Insights
        /// Call Facebook SDK to get Facebook Page Insights
        /// </summary>
        /// <param name="ConnectionParameters"></param>
        /// <returns></returns>
        public FacebookInsights GetFacebookInsights()
        {
            FacebookInsights objFacebookInsights = FacebookAPI.GetFacebookPageInsights(ConnectionParameters.pageParameters.UniqueID,
                                                                                    ConnectionParameters.pageParameters.Token,
                                                                                    ConnectionParameters.pageParameters.StartDate,
                                                                                    ConnectionParameters.pageParameters.EndDate,
                                                                                    ConnectionParameters.pageParameters.Periods);
            
            return objFacebookInsights;
        }

        public FaceBookUserPicture FaceBookUserPicture()
        {
            FaceBookUserPicture objFaceBookUserPicture = FacebookAPI.FaceBookUserPicture(ConnectionParameters.pageParameters.UniqueID);

            return objFaceBookUserPicture;
        }
        #endregion

        #region PAGE BASIC

        /// <summary>
        /// Get Facebook Page Basic Information
        /// Call Facebook SDK to get Facebook Page Information
        /// </summary>
        /// <param name="ConnectionParameters"></param>
        /// <returns></returns>
        public FacebookPage GetFacebookPageInformation()
        {
            FacebookPage objFacebookPage = FacebookAPI.GetFacebookPageInformation(ConnectionParameters.pageParameters.UniqueID,
                                                                                    ConnectionParameters.pageParameters.Token);

            return objFacebookPage;
        }

        #endregion

        #region PAGE FEEDs

        public FacebookFeed GetFacebookFeed()
        {
            FacebookFeed objFacebookFeed = FacebookAPI.GetFacebookFeed(ConnectionParameters.pageParameters.UniqueID,
                                                                                    ConnectionParameters.pageParameters.Token,
                                                                                    ConnectionParameters.pageParameters.StartDate,
                                                                                    ConnectionParameters.pageParameters.EndDate,
                                                                                    ConnectionParameters.pageParameters.limit);

            return objFacebookFeed;
        }

        public FacebookFeed GetSocialStreamFacebookFeed()
        {
            FacebookFeed facebookFeed = new FacebookFeed();
            Feed objfeed = new Feed();

            FacebookFeed objFacebookFeed = FacebookAPI.GetFacebookFeed(ConnectionParameters.pageParameters.UniqueID,
                                                                        ConnectionParameters.pageParameters.Token,
                                                                        ConnectionParameters.pageParameters.PageURL,
                                                                        ConnectionParameters.pageParameters.limit);

            List<DataFeed> listDataFeed = new List<DataFeed>();
            string Until = string.Empty;

            if (string.IsNullOrEmpty(objFacebookFeed.facebookResponse.message))
            {
                foreach (DataFeed feed in objFacebookFeed.feed.data)
                {
                    DataFeed datafeed = new DataFeed();

                    string feedid = string.Empty;
                    string resultid = string.Empty;
                    if (string.IsNullOrWhiteSpace(feed.object_id))
                    {
                        resultid = feed.id;
                        feedid = string.Empty;
                    }
                    else
                    {
                        resultid = feed.object_id;
                        feedid = feed.id;
                    }

                    if (string.IsNullOrEmpty(feedid))
                    {
                        feedid = resultid;
                    }
                    string linkURL = string.Empty;
                    string pictureURL = string.Empty;

                    PostTypeEnum PostType = FacebookAPI.GetPostType(feed.type.ToLower());

                    if (PostType == PostTypeEnum.Link)
                    {
                        linkURL = feed.link;
                        pictureURL = feed.picture;
                    }

                    if (PostType == PostTypeEnum.Photo)
                    {
                        pictureURL = feed.picture;
                    }

                    if (PostType == PostTypeEnum.Video)
                    {
                        linkURL = feed.link;
                        pictureURL = feed.picture;
                    }

                    if (PostType == PostTypeEnum.Offer)
                    {
                        linkURL = feed.link;
                        pictureURL = feed.picture;
                    }

                    if (string.IsNullOrWhiteSpace(feed.message))
                    {
                        feed.message = feed.caption;
                    }

                    if (string.IsNullOrWhiteSpace(feed.message))
                    {
                        feed.message = feed.name;
                    }


                    datafeed.created_time = feed.created_time;
                    datafeed.updated_time = feed.updated_time;
                    datafeed.id = feedid;
                    datafeed.name = feed.name;
                    datafeed.picture  = pictureURL;
                    datafeed.link = linkURL;
                    datafeed.description = feed.description;
                    datafeed.object_id = resultid;
                    datafeed.type = feed.type;
                    datafeed.message = feed.message;
                    datafeed.caption = feed.caption;
                    datafeed.application = feed.application;
                    datafeed.from = feed.from;

                    listDataFeed.Add(datafeed);
                }                
            }
            objfeed.data = listDataFeed;

            facebookFeed.facebookResponse = objFacebookFeed.facebookResponse;
            facebookFeed.feed = objfeed;
            facebookFeed.feed.paging = objFacebookFeed.feed.paging;

            return facebookFeed;
        }
        #endregion

        #region VALIDATE TOKEN

        public bool ValidateToken()
        {
            if (string.IsNullOrEmpty(ConnectionParameters.pageParameters.AppToken))
            {
                return FacebookAPI.ValidateTokenForOldCARFAXApp(ConnectionParameters.pageParameters.Token, ConnectionParameters.pageParameters.UniqueID);
            }
            else
            {
                return FacebookAPI.ValidateToken(ConnectionParameters.pageParameters.Token, ConnectionParameters.pageParameters.AppToken);
            }
            
        }

        public bool ValidateTokenForOldCARFAXApp()
        {
            return FacebookAPI.ValidateTokenForOldCARFAXApp(ConnectionParameters.pageParameters.Token, ConnectionParameters.pageParameters.UniqueID);           
        }

        public FacebookTokenInfo ValidateToken2()
        {   
            return FacebookAPI.ValidateToken2(ConnectionParameters.pageParameters.Token, ConnectionParameters.pageParameters.AppToken);            
        }

        #endregion

        /*
        #region INSIGHTS

        public FacebookPage GetPageInformation()
        {
            FacebookPage objFacebookPage = new FacebookPage();

            string fields = "name,is_published,website,username,about,location,phone,can_post,checkins,were_here_count,talking_about_count,unread_notif_count,new_like_count,offer_eligible,category,id,link,likes,influences,keywords,members,parent_page,unread_message_count,unseen_message_count,network";
            FacebookResponse ObjFacebookResponse = new FacebookResponse();
            Page page = new Page();
            try
            {
                if (string.IsNullOrEmpty(ConnectionParameters.pageParameters.FacebookGraphAPIURLPage))
                {
                    ConnectionParameters.pageParameters.FacebookGraphAPIURLPage = "https://graph.facebook.com/{0}?fields={1}&access_token={2}";
                }
              
                if (!string.IsNullOrEmpty(ConnectionParameters.pageParameters.UniqueID) && !string.IsNullOrEmpty(ConnectionParameters.pageParameters.Token))
                {
                    string URL = string.Format(ConnectionParameters.pageParameters.FacebookGraphAPIURLPage, ConnectionParameters.pageParameters.UniqueID, fields, ConnectionParameters.pageParameters.Token);

                    string rawResponse = string.Empty;

                    try
                    {                       
                        HttpRequestResults = string.Empty;

                        ConnectionStatus httpStatus = ConnectorHttpRequest(URL);
                        
                        if (httpStatus == ConnectionStatus.Success)
                        {
                            if (!string.IsNullOrEmpty(HttpRequestResults))
                            {
                                rawResponse = HttpRequestResults;

                                var jss = new JavaScriptSerializer();
                                if (rawResponse.StartsWith(@"{""error"":"))
                                {
                                    rawResponse = rawResponse.Replace(@"{""error"":", "");
                                    rawResponse = rawResponse.Substring(0, rawResponse.Length - 1);

                                    ObjFacebookResponse = jss.Deserialize<FacebookResponse>(rawResponse);

                                }
                                else
                                {
                                    page = jss.Deserialize<Page>(rawResponse);
                                }

                                objFacebookPage.facebookResponse = ObjFacebookResponse;
                                objFacebookPage.page = page;
                                objFacebookPage.responseJson = rawResponse;
                            }
                        }
                        else
                        {
                            ObjFacebookResponse.message = "Problem to Retrying Page Information from FaceBook.";

                            objFacebookPage.facebookResponse = ObjFacebookResponse;
                            objFacebookPage.page = page;
                            objFacebookPage.responseJson = rawResponse;
                        }
                    }
                    catch (Exception ex)
                    {
                        #region ERROR HANDLING
                        #endregion
                    }
                }
                else
                {
                    ObjFacebookResponse.message = "Invalid Parameters";

                    objFacebookPage.facebookResponse = ObjFacebookResponse;
                    objFacebookPage.page = page;
                }
            }
            catch (Exception ex)
            {
                #region ERROR HANDLING
                #endregion
            }

            return objFacebookPage;
        }
         
        public FacebookInsights GetPageInsights(string startDate, string endDate, string periods = "day,lifetime")
        {
            var objFacebookResponse = new FacebookResponse();
            var objFacebookInsights = new FacebookInsights();
            var objInsights = new Insights();
            //var objPublishManager = new PublishManager();
            try
            {
                var isValid = true;
                var errorMessage = string.Empty;

                var fromDate = Convert.ToDateTime(startDate);
                var toDate = Convert.ToDateTime(endDate);
                var toTime = string.Empty;
                if (fromDate > toDate)
                {
                    isValid = false;
                    errorMessage = "To Date Cannot be Greater than From Date.";
                }

                if (isValid)
                {
                    if (toDate.ToShortDateString() == DateTime.Now.ToShortDateString())
                    {
                        toTime = "T" + DateTime.Now.Hour.ToString() + ":00:00";
                    }
                    else
                    {
                        toTime = "T" + "23:59:00";
                    }
                    var since = fromDate.ToString("yyyy-MM-dd") + "T00:00:00";
                    var until = toDate.ToString("yyyy-MM-dd") + toTime;

                    if (string.IsNullOrEmpty(ConnectionParameters.pageParameters.FacebookGraphAPIURLInsights))
                    {
                        ConnectionParameters.pageParameters.FacebookGraphAPIURLInsights = "https://graph.facebook.com/{0}/insights?format=json&since={1}&until={2}&access_token={3}";                        
                    }

                    string URL = string.Empty;
                    //if (string.IsNullOrEmpty(FeedID))
                    //{
                    URL = string.Format(ConnectionParameters.pageParameters.FacebookGraphAPIURLInsights, ConnectionParameters.pageParameters.UniqueID, since, until, ConnectionParameters.pageParameters.Token);
                    //}
                    //else
                    //{
                    //    ConnectionParameters.pageParameters.FacebookGraphAPIURLInsights = "https://graph.facebook.com/{0}/insights?format=json&access_token={1}";
                    //    url = string.Format(ConnectionParameters.pageParameters.FacebookGraphAPIURLInsights, FeedID, ConnectionParameters.pageParameters.Token);
                    //}

                    string rawResponse = string.Empty;
                    try
                    {
                        //var objFacebookRequest = new FacebookRequest();

                        //objFacebookRequest.RequestURL = url;
                        //objFacebookRequest.Method = "GET";
                        //objFacebookRequest.TimeOut = Convert.ToInt32(ConnectionParameters.pageParameters.FacebookAPITimeOut);


                        HttpRequestResults = string.Empty;

                        ConnectionStatus httpStatus = ConnectorHttpRequest(URL);

                        //rawResponse = ExecuteCommand_Get(objFacebookRequest);

                        if (httpStatus == ConnectionStatus.Success)
                        {
                            if (!string.IsNullOrEmpty(HttpRequestResults))
                            {
                                rawResponse = HttpRequestResults;

                                var jss = new JavaScriptSerializer();
                                if (rawResponse.StartsWith(@"{""error"":"))
                                {
                                    rawResponse = rawResponse.Replace(@"{""error"":", "");
                                    rawResponse = rawResponse.Substring(0, rawResponse.Length - 1);

                                    objFacebookResponse = jss.Deserialize<FacebookResponse>(rawResponse);

                                    objFacebookInsights.facebookResponse = objFacebookResponse;
                                    objFacebookInsights.responseJson = rawResponse;
                                    objFacebookInsights.insights = objInsights;
                                }
                                else
                                {
                                    objInsights = JsonConvert.DeserializeObject<Insights>(rawResponse);

                                    if (objInsights.data.Count > 0)
                                    {

                                        IQueryable<DataInsights> IQDataInsights;
                                        IQDataInsights = objInsights.data.AsQueryable();

                                        IQueryable<DataInsights> iqDataInsights = IQDataInsights;


                                        if (string.IsNullOrEmpty(periods))
                                        {
                                            periods = "day,lifetime";
                                        }

                                        var words = periods.Split(',');
                                        string[] fbPeriod = { "day", "week", "days_28", "lifetime" };

                                        if (words.Count() == 1)
                                        {
                                            iqDataInsights = iqDataInsights.Where(o => o.period.ToLower() == words[0]);
                                        }
                                        else if (words.Count() == 2)
                                        {
                                            iqDataInsights = iqDataInsights.Where(o => o.period.ToLower() == words[0] || o.period.ToLower() == words[1]);
                                        }
                                        else if (words.Count() == 3)
                                        {
                                            iqDataInsights = iqDataInsights.Where(o => o.period.ToLower() == words[0] || o.period.ToLower() == words[1] || o.period.ToLower() == words[2]);
                                        }
                                        else if (words.Count() == 4)
                                        {
                                            iqDataInsights = iqDataInsights.Where(o => o.period.ToLower() == words[0] || o.period.ToLower() == words[1] || o.period.ToLower() == words[2] || o.period.ToLower() == words[3]);
                                        }

                                        var data = iqDataInsights.ToList();
                                        if (data.Count == 0)
                                        {
                                            errorMessage = "Some of the period is invalid.";
                                            objFacebookResponse.message = objFacebookResponse.message + errorMessage;
                                        }

                                        objFacebookInsights.facebookResponse = objFacebookResponse;
                                        objFacebookInsights.responseJson = rawResponse;
                                        objFacebookInsights.insights.data = new List<DataInsights>();
                                        objFacebookInsights.insights.data = data;
                                    }
                                    else
                                    {
                                        errorMessage = "No Insights Record Found.";
                                        objFacebookResponse.message = objFacebookResponse.message + errorMessage;

                                        objFacebookInsights.facebookResponse = objFacebookResponse;
                                        objFacebookInsights.responseJson = rawResponse;
                                        objFacebookInsights.insights.data = new List<DataInsights>();
                                    }
                                }

                            }
                            else
                            {
                                objFacebookResponse.message = "Problem in Retrying FeceBook Insights.";

                                objFacebookInsights.facebookResponse = objFacebookResponse;
                                objFacebookInsights.responseJson = rawResponse;
                                objFacebookInsights.insights = objInsights;
                            }

                        }                       
                    }
                    catch (Exception ex)
                    {
                        #region ERROR HANDLING

                        //var objPublishExceptionLog = new PublishExceptionLog();
                        //objPublishExceptionLog.DealerLocationSocialNetworkPostID = 0;
                        //objPublishExceptionLog.DealerLocationSocialNetworkPostQueueID = 0;
                        //objPublishExceptionLog.ExceptionMessage = ex.Message;
                        //objPublishExceptionLog.InnerException = ex.InnerException == null ? "" : ex.InnerException.Message;
                        //objPublishExceptionLog.PostResponse = string.Empty;
                        //objPublishExceptionLog.StackTrace = ex.StackTrace;
                        //objPublishExceptionLog.MethodName = "FaceBook\\GetFacebookInsights";
                        //objPublishExceptionLog.ExtraInfo = "DealerLocationID : " + dealerLocationId + "rawResponse : " + rawResponse;
                        //objPublishManager.AddPublishExceptionLog(objPublishExceptionLog);

                        //if (!string.IsNullOrEmpty(rawResponse))
                        //{
                        //    objFacebookResponse.message = ex.Message;
                        //    objFacebookResponse.code = string.Empty;
                        //    objFacebookResponse.type = "Exception\\GetFacebookInsights";
                        //    objFacebookResponse.ID = string.Empty;

                        //    objFacebookInsights.responseJson = rawResponse;
                        //}

                        #endregion
                    }
                }
                else
                {
                    objFacebookResponse.message = errorMessage;

                    objFacebookInsights.facebookResponse = objFacebookResponse;
                    objFacebookInsights.responseJson = string.Empty;
                    objFacebookInsights.insights = objInsights;
                }
            }
            catch (Exception ex)
            {
                #region ERROR HANDLING

                //var objPublishExceptionLog = new PublishExceptionLog();
                //objPublishExceptionLog.DealerLocationSocialNetworkPostID = 0;
                //objPublishExceptionLog.DealerLocationSocialNetworkPostQueueID = 0;
                //objPublishExceptionLog.ExceptionMessage = ex.Message;
                //objPublishExceptionLog.InnerException = ex.InnerException == null ? "" : ex.InnerException.Message;
                //objPublishExceptionLog.PostResponse = string.Empty;
                //objPublishExceptionLog.StackTrace = ex.StackTrace;
                //objPublishExceptionLog.MethodName = "FaceBook\\GetFacebookInsights";
                //objPublishExceptionLog.ExtraInfo = "DealerLocationID : " + dealerLocationId;
                //objPublishManager.AddPublishExceptionLog(objPublishExceptionLog);

                //objFacebookResponse.ID = string.Empty;
                //objFacebookResponse.message = ex.Message;
                //objFacebookResponse.type = "Exception\\GetFacebookInsights";
                //objFacebookResponse.code = string.Empty;

                #endregion
            }

            return objFacebookInsights;
        }

        public void SetDateForFacebookInsights(out string startDate, out string endDate)
        {
            startDate = DateTime.Now.AddDays(-7).ToShortDateString() + " 00:00:00";
            endDate = DateTime.Now.ToShortDateString() + " 23:59:59";

            try
            {
                //Get queryDate From Facebook
                var fb_startDate = DateTime.Now.AddDays(-7).ToShortDateString() + " 00:00:00";
                var fb_endDate = DateTime.Now.ToShortDateString() + " 23:59:59";

                var objFacebookInsights_temp = GetPageInsights(fb_startDate, fb_endDate, "day,lifetime");

                if (string.IsNullOrEmpty(objFacebookInsights_temp.facebookResponse.code))
                {
                    var obj = objFacebookInsights_temp.insights.data.FirstOrDefault();

                    DateTime _date = Convert.ToDateTime(obj.values.LastOrDefault().end_time.ToString().Substring(0, 10));
                    startDate = _date.ToShortDateString() + " 00:00:00";
                    endDate = _date.ToShortDateString() + " 23:59:59";
                }
            }
            catch (Exception ex)
            {

            }
        }
        #endregion

        #region FEED & RSS FEED INFORMATION

        public FacebookFeed GetFacebookFeedID(string startDate, string endDate, string limit)
        {
            //PublishManager objPublishManager = new PublishManager();
            FacebookFeed objFacebookFeed = new FacebookFeed();
            string fields = "created_time,id,object_id,type,application,from,message";
            string format = "json";

            bool isValid = true;
            string ErrorMessage = string.Empty;

            //created_time,id,object_id,type,application&until=2012-10-22T00:00:00.0000000-06:00&since=2012-01-01T00:00:00.0000000-06:00&format=json&limit=1000
            FacebookResponse ObjFacebookResponse = new FacebookResponse();
            Feed feed = new Feed();
            try
            {
                if (string.IsNullOrEmpty(ConnectionParameters.pageParameters.FacebookGraphAPIURLFeed))
                {
                    ConnectionParameters.pageParameters.FacebookGraphAPIURLFeed = "https://graph.facebook.com/{0}/feed?fields={1}&since={2}&until={3}&format={4}&limit={5}&access_token={6}";                    
                }

                DateTime _FromDate = Convert.ToDateTime(startDate);
                DateTime _ToDate = Convert.ToDateTime(endDate).AddDays(1);
                string toTime = string.Empty;
                if (_FromDate > _ToDate)
                {
                    isValid = false;
                    ErrorMessage = "To Date Cannot be Greater than From Date.";
                }

                if (isValid)
                {
                    if (!string.IsNullOrEmpty(ConnectionParameters.pageParameters.UniqueID) && !string.IsNullOrEmpty(ConnectionParameters.pageParameters.Token))
                    {
                        toTime = "T" + "23:59:00";
                        var since = _FromDate.ToString("yyyy-MM-dd") + "T00:00:00";
                        var until = _ToDate.ToString("yyyy-MM-dd") + toTime;

                        if (string.IsNullOrEmpty(limit))
                        {
                            limit = "10000";
                        }
                        string URL = string.Format(ConnectionParameters.pageParameters.FacebookGraphAPIURLFeed, ConnectionParameters.pageParameters.UniqueID, fields, since, until, format, limit, ConnectionParameters.pageParameters.Token);

                        string rawResponse = string.Empty;

                        try
                        {
                            //rawResponse = ExecuteCommand_Get(objFacebookRequest);
                            HttpRequestResults = string.Empty;

                            ConnectionStatus httpStatus = ConnectorHttpRequest(URL);
                            if (httpStatus == ConnectionStatus.Success)
                            {
                                if (!string.IsNullOrEmpty(HttpRequestResults))
                                {
                                    rawResponse = HttpRequestResults;

                                    var jss = new JavaScriptSerializer();
                                    if (rawResponse.StartsWith(@"{""error"":"))
                                    {
                                        rawResponse = rawResponse.Replace(@"{""error"":", "");
                                        rawResponse = rawResponse.Substring(0, rawResponse.Length - 1);

                                        ObjFacebookResponse = jss.Deserialize<FacebookResponse>(rawResponse);

                                    }
                                    else
                                    {
                                        feed = jss.Deserialize<Feed>(rawResponse);
                                    }

                                    objFacebookFeed.facebookResponse = ObjFacebookResponse;
                                    objFacebookFeed.feed = feed;
                                }
                                else
                                {
                                    ObjFacebookResponse.message = "Problem to Retrying feed Information from FaceBook.";

                                    objFacebookFeed.facebookResponse = ObjFacebookResponse;
                                    objFacebookFeed.feed = feed;

                                }
                            }                           
                        }
                        catch (Exception ex)
                        {
                            #region ERROR HANDLING

                            //PublishExceptionLog ObjPublishExceptionLog = new PublishExceptionLog();
                            //ObjPublishExceptionLog.DealerLocationSocialNetworkPostID = 0;
                            //ObjPublishExceptionLog.DealerLocationSocialNetworkPostQueueID = 0;
                            //ObjPublishExceptionLog.ExceptionMessage = ex.Message;
                            //ObjPublishExceptionLog.InnerException = ex.InnerException == null ? "" : ex.InnerException.Message;
                            //ObjPublishExceptionLog.PostResponse = string.Empty;
                            //ObjPublishExceptionLog.StackTrace = ex.StackTrace;
                            //ObjPublishExceptionLog.MethodName = "FaceBook\\GetFacebookFeedID";
                            //ObjPublishExceptionLog.ExtraInfo = "URL : " + URL;
                            //objPublishManager.AddPublishExceptionLog(ObjPublishExceptionLog);

                            #endregion
                        }
                    }
                    else
                    {

                        ObjFacebookResponse.message = "Invalid Parameters UniqueID or Token.";

                        objFacebookFeed.facebookResponse = ObjFacebookResponse;
                        objFacebookFeed.feed = feed;
                    }
                }
                else
                {
                    ObjFacebookResponse.message = ErrorMessage;

                    objFacebookFeed.facebookResponse = ObjFacebookResponse;
                    objFacebookFeed.feed = feed;
                }

            }
            catch (Exception ex)
            {
                #region ERROR HANDLING

                //PublishExceptionLog ObjPublishExceptionLog = new PublishExceptionLog();
                //ObjPublishExceptionLog.DealerLocationSocialNetworkPostID = 0;
                //ObjPublishExceptionLog.DealerLocationSocialNetworkPostQueueID = 0;
                //ObjPublishExceptionLog.ExceptionMessage = ex.Message;
                //ObjPublishExceptionLog.InnerException = ex.InnerException == null ? "" : ex.InnerException.Message;
                //ObjPublishExceptionLog.PostResponse = string.Empty;
                //ObjPublishExceptionLog.StackTrace = ex.StackTrace;
                //ObjPublishExceptionLog.MethodName = "FaceBook\\GetFacebookFeedID";
                //ObjPublishExceptionLog.ExtraInfo = "UniqueID : " + UniqueID + ", Token :" + Token + ", FromDate : " + FromDate + ", ToDate : " + ToDate;
                //objPublishManager.AddPublishExceptionLog(ObjPublishExceptionLog);

                #endregion
            }

            return objFacebookFeed;
        }

        public FacebookPageRSSFeed GetRSSFeedInformation(string RSSFeedURL)
        {
            //PublishManager objPublishManager = new PublishManager();
            FacebookPageRSSFeed objFacebookPageRSSFeed = new FacebookPageRSSFeed();
            try
            {
                if (!string.IsNullOrEmpty(RSSFeedURL))
                {
                    //RSSFeedURL = "https://www.facebook.com/feeds/notifications.php?id=255547016773&viewer=100002916188585&key=AWif3BKtbYxO61cY&format=json";
                    string URL = RSSFeedURL;
                    string rawResponse = string.Empty;
                    try
                    {                       
                        //rawResponse = ExecuteCommand_Get(objFacebookRequest);
                        HttpRequestResults = string.Empty;

                        ConnectionStatus httpStatus = ConnectorHttpRequest(URL);
                        if (httpStatus == ConnectionStatus.Success)
                        {
                            if (!string.IsNullOrEmpty(HttpRequestResults))
                            {
                                rawResponse = HttpRequestResults;
                                var jss = new JavaScriptSerializer();
                                objFacebookPageRSSFeed = jss.Deserialize<FacebookPageRSSFeed>(rawResponse);
                            }
                        }
                    }
                    catch (Exception ex)
                    {

                    }

                }
            }
            catch (Exception ex)
            {
            }

            return objFacebookPageRSSFeed;
        }

        public List<FaceBookPostIdType> ProcessRSSFeed(FacebookPageRSSFeed objFacebookPageRSSFeed)
        {
            List<FaceBookPostIdType> listFaceBookPostIdType = new List<FaceBookPostIdType>();
            try
            {
                if (objFacebookPageRSSFeed.entries != null)
                {
                    string PageTitle = objFacebookPageRSSFeed.title.Trim().ToLower();
                    if (!string.IsNullOrEmpty(PageTitle))
                    {
                        PageTitle = PageTitle.Substring(0, PageTitle.IndexOf("'s"));
                    }
                    foreach (var item in objFacebookPageRSSFeed.entries)
                    {
                        try
                        {
                            string title = item.title.Trim().ToLower();

                            if (!string.IsNullOrEmpty(title))
                            {
                                string alternate = string.Empty;
                                string ActionType = string.Empty;
                                string PostID = string.Empty;
                                string Level = string.Empty;
                                string postType = string.Empty;
                                string RSSFeedMessage = string.Empty;

                                if (title.Contains("wall post") && title.Contains("commented"))
                                {
                                    //Comment on Some one Wall Post
                                    Level = "post";
                                    postType = "status";
                                    //ActionType = "post";

                                    alternate = item.alternate.Trim().ToLower().Replace("%2f", "/");
                                    ActionType = Convert.ToString(FacebookActionType.CommentonUserWallPost);
                                    PostID = alternate.Substring(alternate.IndexOf("posts/") + 6, alternate.IndexOf("&comment") - (alternate.IndexOf("posts/") + 6));
                                }
                                else if (title.Contains("photo") && title.Contains("commented") && title.Contains(PageTitle))
                                {
                                    //Comment on Photo
                                    Level = "post";
                                    postType = "photo";
                                    //ActionType = "comment";

                                    alternate = item.alternate.Trim().ToLower();
                                    ActionType = Convert.ToString(FacebookActionType.Comment);
                                    PostID = alternate.Substring(alternate.IndexOf("fbid=") + 5, alternate.IndexOf("&set") - (alternate.IndexOf("fbid=") + 5));

                                }
                                else if (title.Contains("photo") && title.Contains("like") && title.Contains(PageTitle))
                                {
                                    //Photo Like
                                    Level = "post";
                                    postType = "photo";
                                    //ActionType = "like";

                                    alternate = item.alternate.Trim().ToLower();
                                    ActionType = Convert.ToString(FacebookActionType.Like);
                                    //PostID = alternate.Substring(alternate.IndexOf("fbid=") + 5, alternate.IndexOf("&set") - (alternate.IndexOf("fbid=") + 5));
                                }
                                else if (title.Contains("link") && title.Contains("commented") && title.Contains(PageTitle))
                                {
                                    //Comment on Link
                                    Level = "post";
                                    postType = "link";
                                    //ActionType = "comment";

                                    alternate = item.alternate.Trim().ToLower().Replace("%2f", "/");
                                    ActionType = Convert.ToString(FacebookActionType.Comment);
                                    PostID = alternate.Substring(alternate.IndexOf("posts/") + 6, alternate.IndexOf("&comment") - (alternate.IndexOf("posts/") + 6));
                                }
                                else if (title.Contains("link") && title.Contains("like") && title.Contains(PageTitle))
                                {
                                    //Likes on Link
                                    Level = "post";
                                    postType = "link";
                                    //ActionType = "comment";

                                    alternate = item.alternate.Trim().ToLower().Replace("%2f", "/");
                                    ActionType = Convert.ToString(FacebookActionType.Like);
                                    //PostID = alternate.Substring(alternate.IndexOf("posts/") + 6, alternate.IndexOf("&aref") - (alternate.IndexOf("posts/") + 6));

                                }
                                else if (title.Contains("status") && title.Contains("like") && title.Contains(PageTitle))
                                {
                                    //Post on TimeLine (New Post)
                                    Level = "post";
                                    postType = "status";
                                    //ActionType = "status";

                                    alternate = item.alternate.Trim().ToLower();
                                    ActionType = Convert.ToString(FacebookActionType.StatusLike);
                                    //PostID = alternate.Substring(alternate.IndexOf("fbid=") + 5, alternate.IndexOf("&id") - (alternate.IndexOf("fbid=") + 5));
                                }
                                else if (title.Contains("status") && title.Contains("commented") && title.Contains(PageTitle))
                                {
                                    //Post on TimeLine (New Post)
                                    Level = "post";
                                    postType = "status";
                                    //ActionType = "status";

                                    alternate = item.alternate.Trim().ToLower().Replace("%2f", "/");
                                    ActionType = Convert.ToString(FacebookActionType.Comment);
                                    PostID = alternate.Substring(alternate.IndexOf("posts/") + 6, alternate.IndexOf("&comment") - (alternate.IndexOf("posts/") + 6));
                                }
                                else if (title.Contains("status") && title.Contains("commented") && !title.Contains(PageTitle))
                                {
                                    //Post on TimeLine (New Post)
                                    Level = "post";
                                    postType = "status";
                                    //ActionType = "status";

                                    alternate = item.alternate.Trim().ToLower().Replace("%2f", "/");
                                    ActionType = Convert.ToString(FacebookActionType.CommentonUserWallPost);
                                    PostID = alternate.Substring(alternate.IndexOf("posts/") + 6, alternate.IndexOf("&comment") - (alternate.IndexOf("posts/") + 6));
                                }
                                else if (title.Contains(PageTitle) && title.Contains("like"))
                                {
                                    //Post on TimeLine (New Post)
                                    Level = "page";
                                    postType = "page";
                                    //ActionType = "pagelike";

                                    alternate = item.alternate.Trim().ToLower();
                                    ActionType = Convert.ToString(FacebookActionType.StatusLike);
                                }

                                if (!string.IsNullOrEmpty(PostID))
                                {
                                    var result = listFaceBookPostIdType.Where(x => x.PostID == PostID).SingleOrDefault();
                                    if (result == null)
                                    {
                                        listFaceBookPostIdType.Add(new FaceBookPostIdType { PostID = PostID, PostType = postType, ActionType = ActionType, RSSFeedMessage = item.title });
                                    }
                                }

                            }
                        }
                        catch (Exception ex)
                        {

                        }
                    }

                }
            }
            catch (Exception ex)
            {

            }

            return listFaceBookPostIdType;
        }

        #endregion

        #region PAGE/TOKEN PERMISSIONS
        public List<string> GetPagePermissions()
        {
            //PublishManager objPublishManager = new PublishManager();
            string message = string.Empty;
            string Xpath = string.Empty;
            //string ParentnodePermissionsDetailsXPath = "//table[contains(@class,'uiInfoTable')]";
            //string listnodePermissionsDetailsXPath = ".//tr";
            string td_text = string.Empty;
            List<string> listPermissions = new List<string>();
            try
            {
                if (!string.IsNullOrEmpty(ConnectionParameters.pageParameters.FacebookPermissionsURL))
                {
                    //string URL = (string.Format("https://developers.facebook.com/tools/debug/access_token?q={0}", Token));
                    string URL = string.Format(ConnectionParameters.pageParameters.FacebookPermissionsURL, ConnectionParameters.pageParameters.Token);
                    
                    //string responseHtml = GetResponse(URL);
                    HttpRequestResults = string.Empty;
                    ConnectionStatus httpStatus = ConnectorHttpRequest(URL);

                    if (httpStatus == ConnectionStatus.Success)
                    {
                        HtmlDocument htmlDocument = new HtmlDocument();
                        htmlDocument.OptionFixNestedTags = true;
                        htmlDocument.LoadHtml(HttpRequestResults);

                        string Error = string.Empty;
                        try
                        {
                            Xpath = ConnectionParameters.pageParameters.XPathParentError;
                            Error = SelectSingleNode(htmlDocument.DocumentNode, Xpath).InnerText;
                        }
                        catch
                        {
                            Error = string.Empty;
                        }

                        if (string.IsNullOrEmpty(Error))
                        {
                            Xpath = ConnectionParameters.pageParameters.XPathParentnodePermissionsDetails;
                            HtmlNode reviewNode = SelectSingleNode(htmlDocument.DocumentNode, Xpath);

                            Xpath = ConnectionParameters.pageParameters.XPathlistnodePermissionsDetails;
                            var links = SelectNodes(reviewNode, Xpath);

                            if (links != null && links.Count > 0)
                            {
                                string th_text = string.Empty;
                                bool isvalid = false;
                                foreach (var html in links)
                                {
                                    Xpath = ConnectionParameters.pageParameters.XPathTableheader;
                                    th_text = html.SelectSingleNode(Xpath).InnerText;

                                    if (th_text.ToLower().Contains("scopes"))
                                    {
                                        Xpath = ConnectionParameters.pageParameters.XPathLink;
                                        td_text = html.SelectSingleNode(Xpath).InnerText;
                                        isvalid = true;
                                        break;
                                    }
                                    else if (th_text.ToLower().Contains(ConnectionParameters.pageParameters.XPathError))
                                    {
                                        Xpath = ConnectionParameters.pageParameters.XPathErrorTd;
                                        td_text = html.SelectSingleNode(Xpath).InnerText;
                                        isvalid = false;
                                        break;
                                    }
                                }

                                if (!string.IsNullOrEmpty(td_text) && isvalid)
                                {
                                    string[] Permissions = td_text.Split(' ');
                                    foreach (string Permission in Permissions)
                                    {
                                        listPermissions.Add(Permission);
                                    }
                                }
                                else
                                {
                                    listPermissions.Add(td_text);
                                }
                            }
                        }
                        else
                        {
                            message = "The access token could not be decrypted";
                            listPermissions.Add(message);
                        }

                    }
                    //var doc = new HtmlDocument();
                    //doc.OptionFixNestedTags = true;
                    //doc.LoadHtml(responseHtml);

                    //string Error = string.Empty;
                    //try
                    //{
                    //    Error = doc.DocumentNode.SelectSingleNode("//div[contains(@class,'pam uiBoxRed')]").InnerText;
                    //}
                    //catch
                    //{
                    //    Error = string.Empty;
                    //}

                    //if (string.IsNullOrEmpty(Error))
                    //{
                    //    Xpath = ParentnodePermissionsDetailsXPath;
                    //    HtmlNode reviewNode = doc.DocumentNode.SelectSingleNode(ParentnodePermissionsDetailsXPath);

                    //    Xpath = listnodePermissionsDetailsXPath;
                    //    var links = reviewNode.SelectNodes(listnodePermissionsDetailsXPath);

                    //    if (links != null && links.Count > 0)
                    //    {
                    //        string th_text = string.Empty;
                    //        bool isvalid = false;
                    //        foreach (var html in links)
                    //        {
                    //            Xpath = ".//th";
                    //            th_text = html.SelectSingleNode(".//th").InnerText;

                    //            if (th_text.ToLower().Contains("scopes"))
                    //            {
                    //                td_text = html.SelectSingleNode(".//td/a").InnerText;
                    //                isvalid = true;
                    //                break;
                    //            }
                    //            else if (th_text.ToLower().Contains("error parsing url"))
                    //            {
                    //                //Xpath = ".//td";
                    //                td_text = html.SelectSingleNode(".//td").InnerText;
                    //                isvalid = false;
                    //                break;
                    //            }
                    //        }

                    //        if (!string.IsNullOrEmpty(td_text) && isvalid)
                    //        {
                    //            string[] Permissions = td_text.Split(' ');
                    //            foreach (string Permission in Permissions)
                    //            {
                    //                listPermissions.Add(Permission);
                    //            }
                    //        }
                    //        else
                    //        {
                    //            listPermissions.Add(td_text);
                    //        }
                    //    }
                    //}
                    //else
                    //{
                    //    message = "The access token could not be decrypted";
                    //    listPermissions.Add(message);
                    //}
                }
                else
                {
                    message = "Invalid Parameters UniqueID or Token.";
                    listPermissions.Add(message);
                }
            }
            catch (Exception ex)
            {
                #region ERROR HANDLING

                //PublishExceptionLog ObjPublishExceptionLog = new PublishExceptionLog();
                //ObjPublishExceptionLog.DealerLocationSocialNetworkPostID = 0;
                //ObjPublishExceptionLog.DealerLocationSocialNetworkPostQueueID = 0;
                //ObjPublishExceptionLog.ExceptionMessage = ex.Message;
                //ObjPublishExceptionLog.InnerException = ex.InnerException == null ? "" : ex.InnerException.Message;
                //ObjPublishExceptionLog.PostResponse = string.Empty;
                //ObjPublishExceptionLog.StackTrace = ex.StackTrace;
                //ObjPublishExceptionLog.MethodName = "FaceBook\\GetFacebookPermissions";
                //ObjPublishExceptionLog.ExtraInfo = "Token : " + Token;
                //objPublishManager.AddPublishExceptionLog(ObjPublishExceptionLog);

                #endregion
            }

            return listPermissions;
        }
        #endregion

        #region PAGE SEARCH

        public FacebookSearch GetPageSearch(string PageName)
        {
            FacebookResponse ObjFacebookResponse = new FacebookResponse();
            FacebookSearch objFacebookSearch = new FacebookSearch();
            Search objSearch = new Search();

            try
            {
                if (!string.IsNullOrEmpty(PageName))
                {
                    if (string.IsNullOrEmpty(ConnectionParameters.pageParameters.FacebookGraphAPIURLSearch))
                    {
                        ConnectionParameters.pageParameters.FacebookGraphAPIURLSearch = "https://graph.facebook.com/search?q={0}&type=page";
                    }

                    string URL = string.Format(ConnectionParameters.pageParameters.FacebookGraphAPIURLSearch, PageName);

                    string rawResponse = string.Empty;

                    try
                    {
                        HttpRequestResults = string.Empty;

                        ConnectionStatus httpStatus = ConnectorHttpRequest(URL);

                        if (httpStatus == ConnectionStatus.Success)
                        {
                            if (!string.IsNullOrEmpty(HttpRequestResults))
                            {
                                rawResponse = HttpRequestResults;

                                var jss = new JavaScriptSerializer();
                                FacebookPagePublic objFacebookPagePublic = new FacebookPagePublic();

                                if (rawResponse.StartsWith(@"{""error"":"))
                                {
                                    rawResponse = rawResponse.Replace(@"{""error"":", "");
                                    rawResponse = rawResponse.Substring(0, rawResponse.Length - 1);

                                    ObjFacebookResponse = jss.Deserialize<FacebookResponse>(rawResponse);
                                }
                                else
                                {
                                    SearchData _objSearchData = jss.Deserialize<SearchData>(rawResponse);

                                    if (_objSearchData.data != null)
                                    {
                                        if (_objSearchData.data.Count > 1)
                                        {
                                            foreach (var item in _objSearchData.data)
                                            {
                                                if (item.name.Trim().ToLower() == PageName.Trim().ToLower())
                                                {
                                                    objSearch.id = item.id;
                                                    objSearch.name = item.name;


                                                    objFacebookPagePublic = GetPageDetailPublic(_objSearchData.data.FirstOrDefault().id);
                                                    break;
                                                }
                                            }
                                        }
                                        else
                                        {
                                            objSearch.id = _objSearchData.data.FirstOrDefault().id;
                                            objSearch.name = _objSearchData.data.FirstOrDefault().name;

                                            objFacebookPagePublic = GetPageDetailPublic(_objSearchData.data.FirstOrDefault().id);
                                        }

                                    }
                                }

                                objFacebookSearch.data = objSearch;
                                objFacebookSearch.facebookPagePublic = objFacebookPagePublic;
                            }
                            else
                            {
                                ObjFacebookResponse.message = "Problem to Retrying PostStatistics from FaceBook.";
                                objFacebookSearch.data = objSearch;
                            }

                        }                        
                    }
                    catch (Exception ex)
                    {
                        #region ERROR HANDLING
                        #endregion
                    }
                }
                else
                {
                    ObjFacebookResponse.message = "Invalid Parameters";
                    objFacebookSearch.data = objSearch;
                }
            }
            catch (Exception ex)
            {
                #region ERROR HANDLING
                #endregion
            }

            objFacebookSearch.AccountName = PageName;

            return objFacebookSearch;
        }

        public FacebookPagePublic GetPageDetailPublic(string FacebookPageID)
        {
            FacebookResponse ObjFacebookResponse = new FacebookResponse();
            FacebookPagePublic objFacebookPage = new FacebookPagePublic();

            string fields = "id,checkins,likes,link,location,name,phone,talking_about_count,username,website,were_here_count";
            try
            {
                if (!string.IsNullOrEmpty(FacebookPageID))
                {
                    if (string.IsNullOrEmpty(ConnectionParameters.pageParameters.FacebookGraphAPIURLPageSearch))
                    {
                        ConnectionParameters.pageParameters.FacebookGraphAPIURLPageSearch = "https://graph.facebook.com/{0}?fields={1}";
                    }

                    string URL = string.Format(ConnectionParameters.pageParameters.FacebookGraphAPIURLPageSearch, FacebookPageID, fields);

                    string rawResponse = string.Empty;

                    try
                    {                      
                        HttpRequestResults = string.Empty;

                        ConnectionStatus httpStatus = ConnectorHttpRequest(URL);

                        if (httpStatus == ConnectionStatus.Success)
                        {
                            if (!string.IsNullOrEmpty(HttpRequestResults))
                            {
                                rawResponse = HttpRequestResults;

                                var jss = new JavaScriptSerializer();
                                if (rawResponse.StartsWith(@"{""error"":"))
                                {
                                    rawResponse = rawResponse.Replace(@"{""error"":", "");
                                    rawResponse = rawResponse.Substring(0, rawResponse.Length - 1);

                                    ObjFacebookResponse = jss.Deserialize<FacebookResponse>(rawResponse);
                                }
                                else
                                {
                                    objFacebookPage = jss.Deserialize<FacebookPagePublic>(rawResponse);
                                }
                            }
                            else
                            {
                                ObjFacebookResponse.message = "Problem to Retrying PostStatistics from FaceBook.";
                            }
                        }                       
                    }
                    catch (Exception ex)
                    {
                        #region ERROR HANDLING
                        #endregion
                    }
                }
                else
                {
                    ObjFacebookResponse.message = "Invalid Parameters";
                }


            }
            catch (Exception ex)
            {
                #region ERROR HANDLING
                #endregion
            }

            //objFacebookSearch.AccountName = PageName;

            return objFacebookPage;
        }

        #endregion

        #region ALBUM
        
        public FacebookAlbum GetAlbums()
        {
            //PublishManager objPublishManager = new PublishManager();
            FacebookAlbum FacebookAlbums = new FacebookAlbum();

            string fields = "id,name,can_upload,count";
            string limit = "1000";
            FacebookResponse ObjFacebookResponse = new FacebookResponse();
            Album albums = new Album();
            try
            {
                if (string.IsNullOrEmpty(ConnectionParameters.pageParameters.FacebookGraphAPIURLAlbumGet))
                {
                    ConnectionParameters.pageParameters.FacebookGraphAPIURLAlbumGet = "https://graph.facebook.com/{0}/albums?fields={1}&limit={2}&access_token={3}";
                }

                //if (DealerLocationID != 0 && (string.IsNullOrEmpty(UniqueID) && string.IsNullOrEmpty(Token)))
                //{
                //    DealerLocationSocialNetwork objDealerLocationSocialNetwork = objPublishManager.GetDealerLocationSocialNetworkbyDealerLocationID(DealerLocationID, 2);

                //    if (objDealerLocationSocialNetwork != null)
                //    {
                //        UniqueID = objDealerLocationSocialNetwork.UniqueID;
                //        Token = objDealerLocationSocialNetwork.Token;
                //    }
                //}

                string URL = string.Format(ConnectionParameters.pageParameters.FacebookGraphAPIURLAlbumGet, ConnectionParameters.pageParameters.UniqueID, fields, limit, ConnectionParameters.pageParameters.Token);

                string rawResponse = string.Empty;

                try
                {                    
                    HttpRequestResults = string.Empty;

                    ConnectionStatus httpStatus = ConnectorHttpRequest(URL);

                    if (httpStatus == ConnectionStatus.Success)
                    {
                        if (!string.IsNullOrEmpty(HttpRequestResults))
                        {
                            rawResponse = HttpRequestResults;

                            var jss = new JavaScriptSerializer();
                            if (rawResponse.StartsWith(@"{""error"":"))
                            {
                                rawResponse = rawResponse.Replace(@"{""error"":", "");
                                rawResponse = rawResponse.Substring(0, rawResponse.Length - 1);

                                ObjFacebookResponse = jss.Deserialize<FacebookResponse>(rawResponse);
                            }
                            else //if (rawResponse.StartsWith(@"{\n   \""data\"":"))
                            {
                                //rawResponse = rawResponse.Replace(@"{\n   \""data\"":", "");
                                //rawResponse = rawResponse.Substring(0, rawResponse.Length - 1);
                                //rawResponse = rawResponse.Substring(12, rawResponse.Length - 13);                                
                                albums = jss.Deserialize<Album>(rawResponse);
                            }

                            FacebookAlbums.facebookResponse = ObjFacebookResponse;
                            FacebookAlbums.albums = albums;
                        }
                        else
                        {
                            ObjFacebookResponse.message = "Problem to Retrying Album List from FaceBook.";

                            FacebookAlbums.facebookResponse = ObjFacebookResponse;
                            FacebookAlbums.albums = albums;
                        }
                    }
                }
                catch (Exception ex)
                {
                    #region ERROR HANDLING

                    //PublishExceptionLog ObjPublishExceptionLog = new PublishExceptionLog();
                    //ObjPublishExceptionLog.DealerLocationSocialNetworkPostID = 0;
                    //ObjPublishExceptionLog.DealerLocationSocialNetworkPostQueueID = 0;
                    //ObjPublishExceptionLog.ExceptionMessage = ex.Message;
                    //ObjPublishExceptionLog.InnerException = ex.InnerException == null ? "" : ex.InnerException.Message;
                    //ObjPublishExceptionLog.PostResponse = string.Empty;
                    //ObjPublishExceptionLog.StackTrace = ex.StackTrace;
                    //ObjPublishExceptionLog.MethodName = "FaceBook\\GetFacebookAlbums";
                    //ObjPublishExceptionLog.ExtraInfo = rawResponse;
                    //objPublishManager.AddPublishExceptionLog(ObjPublishExceptionLog);

                    #endregion
                }

            }
            catch (Exception ex)
            {
                #region ERROR HANDLING

                //PublishExceptionLog ObjPublishExceptionLog = new PublishExceptionLog();
                //ObjPublishExceptionLog.DealerLocationSocialNetworkPostID = 0;
                //ObjPublishExceptionLog.DealerLocationSocialNetworkPostQueueID = 0;
                //ObjPublishExceptionLog.ExceptionMessage = ex.Message;
                //ObjPublishExceptionLog.InnerException = ex.InnerException == null ? "" : ex.InnerException.Message;
                //ObjPublishExceptionLog.PostResponse = string.Empty;
                //ObjPublishExceptionLog.StackTrace = ex.StackTrace;
                //ObjPublishExceptionLog.MethodName = "FaceBook\\GetFacebookAlbums";
                //ObjPublishExceptionLog.ExtraInfo = string.Empty;
                //objPublishManager.AddPublishExceptionLog(ObjPublishExceptionLog);

                #endregion
            }

            return FacebookAlbums;
        }

        #endregion

        #region USER PICTURE

        public FaceBookUserPicture UserPicture(string UniqueID)
        {
            FaceBookUserPicture objFaceBookUserPicture = new FaceBookUserPicture();
            try
            {
                if (!string.IsNullOrEmpty(UniqueID))
                {
                    if (string.IsNullOrEmpty(ConnectionParameters.pageParameters.FacebookGraphAPIURLUserPicture))
                    {
                        ConnectionParameters.pageParameters.FacebookGraphAPIURLUserPicture = "https://graph.facebook.com/{0}?fields=picture.type%28small%29";
                    }

                    string URL = string.Format(ConnectionParameters.pageParameters.FacebookGraphAPIURLUserPicture, UniqueID);

                    string rawResponse = string.Empty;
                    try
                    {
                       
                        HttpRequestResults = string.Empty;

                        ConnectionStatus httpStatus = ConnectorHttpRequest(URL);

                        if (httpStatus == ConnectionStatus.Success)
                        {
                            if (!string.IsNullOrEmpty(HttpRequestResults))
                            {
                                rawResponse = HttpRequestResults;

                                var jss = new JavaScriptSerializer();
                                if (rawResponse.StartsWith(@"{""error"":"))
                                {
                                }
                                else
                                {
                                    objFaceBookUserPicture = jss.Deserialize<FaceBookUserPicture>(rawResponse);
                                }
                            }
                        }
                    }
                    catch (Exception ex)
                    {

                    }
                }
            }
            catch (Exception ex)
            {
            }

            return objFaceBookUserPicture;
        }

        #endregion
                
        */

        #region OVERRIDE METHODS
        protected override void dispose()
        {

        }
        #endregion
    }
}
