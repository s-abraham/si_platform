﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Script.Serialization;
using Connectors.Social.Parameters;
using Google;
using Google.Model;

namespace Connectors.Social
{
    public class GooglePlusPageConnection : SISocialConnection
    {
        private SIGooglePlusPageConnectionParameters ConnectionParameters;

        #region INITIALIZER
        public GooglePlusPageConnection(SIGooglePlusPageConnectionParameters siGooglePlusPageConnectionParameters)
            : base(siGooglePlusPageConnectionParameters)
        {
            ConnectionParameters = siGooglePlusPageConnectionParameters;
        }
        #endregion
           
     
        
        #region USER/PAGES

        public GooglePlusAccessTokenRequestResponse GetGooglePlusAccessToken()
        {
            GooglePlusAccessTokenRequestResponse obj = GoogleAPI.GetGooglePlusAccessToken(ConnectionParameters.pageParameters.client_id,
                                                                                            ConnectionParameters.pageParameters.client_secret,
                                                                                            ConnectionParameters.pageParameters.refresh_token,
                                                                                            ConnectionParameters.pageParameters.grant_type);

            return obj;
        }

        public GooglePlusGetActivityListByPageResponse GetGooglePlusActivitiesListForAPage()
        {
            GooglePlusGetActivityListByPageResponse obj = GoogleAPI.GetGooglePlusActivitiesListForAPage(ConnectionParameters.pageParameters.UniqueID,
                                                                                                            ConnectionParameters.pageParameters.apiKey);

            return obj;
        }

        public GooglePlusPageStatistics GetPageInformation()
        {
            GooglePlusPageStatistics objGooglePlusPageStatistics = new GooglePlusPageStatistics();

            #region PostCount

            GooglePlusGetActivityListByPageResponse objGooglePlusGetActivityListByPageResponse = GoogleAPI.GetGooglePlusActivitiesListForAPage(ConnectionParameters.pageParameters.UniqueID,
                                                                                                                                                ConnectionParameters.pageParameters.apiKey);

            #endregion

            #region PlusOneCount & FollowerCount

            GooglePlusGetSpecificPageResponse objGooglePlusGetSpecificPageResponse = GoogleAPI.GetGooglePlusPagebyPageID(ConnectionParameters.pageParameters.UniqueID,
                                                                                                                            ConnectionParameters.pageParameters.apiKey);

            #endregion

            string Error = string.Empty;
            int PostCount = objGooglePlusGetActivityListByPageResponse.items.Count;
            if (!string.IsNullOrEmpty(objGooglePlusGetActivityListByPageResponse.error.message))
            {
                Error = objGooglePlusGetActivityListByPageResponse.error.message;
            }

            int PlusOneCount = objGooglePlusGetSpecificPageResponse.plusOneCount;
            int FollowerCount = objGooglePlusGetSpecificPageResponse.circledByCount;

            if (!string.IsNullOrEmpty(objGooglePlusGetSpecificPageResponse.error.message))
            {
                Error = Error + objGooglePlusGetSpecificPageResponse.error.message;
            }

            objGooglePlusPageStatistics.PostCount = PostCount;
            objGooglePlusPageStatistics.PlusOneCount = PlusOneCount;
            objGooglePlusPageStatistics.FollowerCount = FollowerCount;
            objGooglePlusPageStatistics.UniqueID = ConnectionParameters.pageParameters.UniqueID;
            objGooglePlusPageStatistics.Error = Error;

            return objGooglePlusPageStatistics;
        }

        public GooglePlusGetSpecificPageResponse GetGooglePlusPagebyPageID()
        {
            GooglePlusGetSpecificPageResponse obj = GoogleAPI.GetGooglePlusPagebyPageID(ConnectionParameters.pageParameters.UniqueID, 
                                                                                                ConnectionParameters.pageParameters.apiKey);

            return obj;
        }
        #endregion

        #region TOKEN VERIFICATION

        public bool ValidateToken()
        {
            return GoogleAPI.ValidateToken(ConnectionParameters.pageParameters.client_id, ConnectionParameters.pageParameters.client_secret, ConnectionParameters.pageParameters.accessToken);
        }

        public GooglePlusAccessTokenRequestResponse ValidateToken2()
        {
            return GoogleAPI.ValidateToken2(ConnectionParameters.pageParameters.client_id, ConnectionParameters.pageParameters.client_secret, ConnectionParameters.pageParameters.accessToken);
        }


        #endregion

        #region OVERRIDE METHODS
        protected override void dispose()
        {

        }
        #endregion
    }
}
