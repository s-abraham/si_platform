﻿using Connectors.Social.Parameters;
using Google;
using Google.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Connectors.Social
{
    public class GooglePlusStreamConnection : SISocialConnection
    {
        private SIGooglePlusStreamConnectionParameters ConnectionParameters;

        #region INITIALIZER
        public GooglePlusStreamConnection(SIGooglePlusStreamConnectionParameters siGooglePlusStreamConnectionParameters)
            : base(siGooglePlusStreamConnectionParameters)
        {
            ConnectionParameters = siGooglePlusStreamConnectionParameters;
        }
        #endregion


        public GooglePlusGetActivityListByPageResponse GetGooglePlusActivitiesListForAPage()
        {
            GooglePlusGetActivityListByPageResponse result = GoogleAPI.GetGooglePlusActivitiesListForAPage(ConnectionParameters.StreamParameters.UniqueID,
                                                                                                            ConnectionParameters.StreamParameters.apiKey);
                                                                                                            

            return result;
        }

        public GooglePlusGetCommentsListResponse GetGooglePlusCommentsListForAnActivity()
        {
            GooglePlusGetCommentsListResponse result = GoogleAPI.GetGooglePlusCommentsListForAnActivity(ConnectionParameters.StreamParameters.activityID,
                                                                                                            ConnectionParameters.StreamParameters.apiKey);


            return result;           
        }

        public GooglePlusPostResponse AddGooglePlusComment()
        {
            GooglePlusPostResponse result = GoogleAPI.AddGooglePlusComment(ConnectionParameters.StreamParameters.accessToken,
                                                                                        ConnectionParameters.StreamParameters.UniqueID,
                                                                                        ConnectionParameters.StreamParameters.activityID,
                                                                                        ConnectionParameters.StreamParameters.commentContent);


            return result;
        }

        public GooglePlusResponse DeleteGooglePlusComment()
        {
            GooglePlusResponse result = GoogleAPI.DeleteGooglePlusComment(ConnectionParameters.StreamParameters.accessToken,
                                                                                        ConnectionParameters.StreamParameters.UniqueID,
                                                                                        ConnectionParameters.StreamParameters.commentID);


            return result;
        }

        public GooglePlusResponse DeleteGooglePlusActivity()
        {
            GooglePlusResponse result = GoogleAPI.DeleteGooglePlusActivity(ConnectionParameters.StreamParameters.accessToken,
                                                                                        ConnectionParameters.StreamParameters.UniqueID,
                                                                                        ConnectionParameters.StreamParameters.activityID);


            return result;
        }

        #region OVERRIDE METHODS
        protected override void dispose()
        {

        }
        #endregion
    }
}
