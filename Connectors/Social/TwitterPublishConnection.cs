﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Connectors.Social.Parameters;
using Twitter;
using Twitter.Entities;
using Twitterizer;

namespace Connectors.Social
{
    public class TwitterPublishConnection : SISocialConnection
    {
        private SITwitterPublishConnectionParameters ConnectionParameters;

        #region INITIALIZER
        public TwitterPublishConnection(SITwitterPublishConnectionParameters siTwitterPublishConnectionParameters)
            : base(siTwitterPublishConnectionParameters)
        {
            ConnectionParameters = siTwitterPublishConnectionParameters;
        }
        #endregion

        #region STATUS UPDATE

        public TwitterResponse StatusUpdate()
        {
            TwitterResponse objTwitterResponse = new TwitterResponse();

            objTwitterResponse = TwitterAPI.StatusUpdate(ConnectionParameters.PublishParameters.status,
                                                            ConnectionParameters.PublishParameters.link,
                                                            ConnectionParameters.PublishParameters.oauth_token,
                                                            ConnectionParameters.PublishParameters.oauth_token_secret,
                                                            ConnectionParameters.PublishParameters.oauth_consumer_key,
                                                            ConnectionParameters.PublishParameters.oauth_consumer_secret);

            return objTwitterResponse;
        }

        #endregion

        #region STATUS UPDATE WITH MEDIA

        public TwitterResponse StatusUpdateWithMedia()
        {
            TwitterResponse objTwitterResponse = new TwitterResponse();

            objTwitterResponse = TwitterAPI.StatusUpdateWithMedia(ConnectionParameters.PublishParameters.status,                                                                    
                                                                    ConnectionParameters.PublishParameters.imagePath,
                                                                    ConnectionParameters.PublishParameters.oauth_token,
                                                                    ConnectionParameters.PublishParameters.oauth_token_secret,
                                                                    ConnectionParameters.PublishParameters.oauth_consumer_key,
                                                                    ConnectionParameters.PublishParameters.oauth_consumer_secret);

            return objTwitterResponse;
        }

        public string StatusUpdateWithMediaLog()
        {
            string result = TwitterAPI.StatusUpdateWithMediaLog(ConnectionParameters.PublishParameters.status,
                                                                    ConnectionParameters.PublishParameters.imagePath,
                                                                    ConnectionParameters.PublishParameters.oauth_token,
                                                                    ConnectionParameters.PublishParameters.oauth_token_secret,
                                                                    ConnectionParameters.PublishParameters.oauth_consumer_key,
                                                                    ConnectionParameters.PublishParameters.oauth_consumer_secret);

            return result;
        }

        #endregion

        #region OVERRIDE METHODS
        protected override void dispose()
        {

        }
        #endregion
    }
}
