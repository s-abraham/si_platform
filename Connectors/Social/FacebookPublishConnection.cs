﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Web.Script.Serialization;
using Connectors.Parameters;
using Facebook;
using Facebook.Entities;

namespace Connectors
{
    public class FacebookPublishConnection : SISocialConnection
    {
        private SIFacebookPublishConnectionParameters ConnectionParameters;

        #region INITIALIZER
        public FacebookPublishConnection(SIFacebookPublishConnectionParameters siFacebookPublishConnectionParameters)
            : base(siFacebookPublishConnectionParameters)
        {
            ConnectionParameters = siFacebookPublishConnectionParameters;
        }
        #endregion

        #region VARIABLE
        #endregion

        #region WALL POST
        
        public FacebookResponse CreateWallPost()
        {
            FacebookResponse ObjFacebookResponse = new FacebookResponse();
            //PublishManager objPublishManager = new PublishManager();
            try
            {
                ObjFacebookResponse = FacebookAPI.CreateFacebookWallPost(ConnectionParameters.publishParameters.UniqueID,
                                                                            ConnectionParameters.publishParameters.Token,
                                                                            ConnectionParameters.publishParameters.message,
                                                                            ConnectionParameters.publishParameters.caption,
                                                                            ConnectionParameters.publishParameters.description,
                                                                            ConnectionParameters.publishParameters.name,
                                                                            ConnectionParameters.publishParameters.picture,
                                                                            ConnectionParameters.publishParameters.link,
                                                                            ConnectionParameters.publishParameters.source,
                                                                            ConnectionParameters.publishParameters.targeting);
            }
            catch (Exception ex)
            {
                #region ERROR HANDLING
                #endregion
            }
            return ObjFacebookResponse;
        }
        
        #endregion

        #region PHOTO

        public FacebookResponse UploadPhoto()
        {
            FacebookResponse ObjFacebookResponse = new FacebookResponse();            
            try
            {
                ObjFacebookResponse = FacebookAPI.UploadFacebookPhoto(ConnectionParameters.publishParameters.Token,
                                                                        ConnectionParameters.publishParameters.message,
                                                                        ConnectionParameters.publishParameters.picture,
                                                                        ConnectionParameters.publishParameters.album_id);

            }
            catch (Exception ex)
            {
                #region ERROR HANDLING
                #endregion
            }

            return ObjFacebookResponse;
        }
                
        #endregion

        #region CREATE ALBUM

        public FacebookResponse CreateAlbum()
        {
            FacebookResponse ObjFacebookResponse = new FacebookResponse();
            //PublishManager objPublishManager = new PublishManager();
            try
            {
                ObjFacebookResponse = FacebookAPI.CreateFacebookAlbum(ConnectionParameters.publishParameters.UniqueID,
                                                    ConnectionParameters.publishParameters.Token,
                                                    ConnectionParameters.publishParameters.album_Name,
                                                    ConnectionParameters.publishParameters.album_Message);
            }
            catch (Exception ex)
            {
                #region ERROR HANDLING
                #endregion
            }

            return ObjFacebookResponse;
        }

        #endregion
        
        #region OVERRIDE METHODS
        protected override void dispose()
        {

        }
        #endregion
    }
}
