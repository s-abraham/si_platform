﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Connectors.Social.Parameters;
using Twitter;
using Twitter.Entities;

namespace Connectors.Social
{
    public class TwitterPostStatisticConnection : SISocialConnection
    {
        private SITwitterPostStatisticsConnectionParameters ConnectionParameters;

        #region INITIALIZER
        public TwitterPostStatisticConnection(SITwitterPostStatisticsConnectionParameters siTwitterPostStatisticsConnectionParameters)
            : base(siTwitterPostStatisticsConnectionParameters)
        {
            ConnectionParameters = siTwitterPostStatisticsConnectionParameters;
        }
        #endregion


        #region STATISTICS

        public TwitterPostStatistics GetTwitterStatusStatistics()
        {
            TwitterPostStatistics PostStatistics = TwitterAPI.GetTwitterStatusStatistics(ConnectionParameters.postStatisticsParameters.statusID,
                                                                                            ConnectionParameters.postStatisticsParameters.oauth_token,
                                                                                            ConnectionParameters.postStatisticsParameters.oauth_token_secret,
                                                                                            ConnectionParameters.postStatisticsParameters.oauth_consumer_key,
                                                                                            ConnectionParameters.postStatisticsParameters.oauth_consumer_secret);

            return PostStatistics;
        }

        #endregion

        #region OVERRIDE METHODS
        protected override void dispose()
        {

        }
        #endregion
    }
}
