﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Connectors.Parameters;

namespace Connectors.Social.Parameters
{
    public class SITwitterPublishConnectionParameters : SISocialConnectionParameters
    {
       #region Properties

        public SITwitterPublishParameters PublishParameters;

       #endregion


        public SITwitterPublishConnectionParameters(SITwitterPublishParameters siTwitterPublishParameters, int timeOutSeconds = 30, bool useProxy = false, int maxRedirects = 1)
			: base(timeOutSeconds, useProxy, maxRedirects)
		{
            PublishParameters = siTwitterPublishParameters;            
		}
    }
}
