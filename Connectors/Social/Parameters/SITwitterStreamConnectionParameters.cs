﻿using Connectors.Parameters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Connectors.Social
{
    public class SITwitterStreamConnectionParameters : SISocialConnectionParameters
    {
        #region Properties

        public SITwitterStreamParameters StreamParameters;

       #endregion


        public SITwitterStreamConnectionParameters(SITwitterStreamParameters siTwitterStreamParameters, int timeOutSeconds = 30, bool useProxy = false, int maxRedirects = 1)
			: base(timeOutSeconds, useProxy, maxRedirects)
		{
            StreamParameters = siTwitterStreamParameters;            
		}
    }
}
