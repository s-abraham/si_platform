﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Connectors.Social.Parameters
{
    public class SIGooglePlusPageParameters
    {
        public string client_id { get; set; }
        public string redirect_URL { get; set; }
        public string authCode { get; set; } 
        public string client_secret { get; set; }         
        public string grant_type { get; set; }
        public string refresh_token { get; set; }
        public string accessToken { get; set; }
        public string sdUserId { get; set; }
        public string userID { get; set; }
        
        public string searchName { get; set; }

        public string UniqueID { get; set; }
        public string apiKey { get; set; }
        public string appKey { get; set; }
        public string appSecret { get; set; }

    }
}
