﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Connectors.Parameters
{
    public class SIFacebookPublishParameters
    {
        public string UniqueID { get; set; }
        public string Token { get; set; }

        // Wall Post
        
        public string message { get; set; }
        public string caption { get; set; }
        public string description { get; set; }
        public string name { get; set; }
        public string picture { get; set; }
        public string link { get; set; }
        public string source { get; set; }
        public string targeting { get; set; }


        // Create Album
        
        public string album_Name { get; set; }
        public string album_Message { get; set; }
                
        //Upload Photo
        public string album_id { get; set; }        
    }
}
