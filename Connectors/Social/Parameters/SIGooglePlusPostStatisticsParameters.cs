﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Connectors.Social.Parameters
{
    public class SIGooglePlusPostStatisticsParameters
    {
        public string pageID { get; set; }
        public string sdUserId { get; set; }
        public string activityID { get; set; }
        public string accesstoken { get; set; }

        public string apiKey { get; set; }

    }
}
