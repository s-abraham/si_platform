﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Connectors.Parameters;

namespace Connectors.Social.Parameters
{
    public class SITwitterPageConnectionParameters : SISocialConnectionParameters
    {
       #region Properties

        public SITwitterPageParameters pageParameters;

       #endregion


        public SITwitterPageConnectionParameters(SITwitterPageParameters siTwitterPageParameters, int timeOutSeconds = 30, bool useProxy = false, int maxRedirects = 1)
			: base(timeOutSeconds, useProxy, maxRedirects)
		{
            pageParameters = siTwitterPageParameters;            
		}
    }
}
