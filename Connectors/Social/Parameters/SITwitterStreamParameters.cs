﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Connectors.Social
{
    public class SITwitterStreamParameters
    {
        public string uniqueID { get; set; }
        public string oauth_token { get; set; }
        public string oauth_token_secret { get; set; }
        public string oauth_consumer_key { get; set; }
        public string oauth_consumer_secret { get; set; }
        public string screenName { get; set; }
        public string count { get; set; }
        public string sinceID { get; set; }
        public string statusID { get; set; }
        public string status { get; set; }
        public string inReplyToStatusID { get; set; }
    }
}
