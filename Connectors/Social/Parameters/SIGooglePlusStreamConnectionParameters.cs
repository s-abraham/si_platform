﻿using Connectors.Parameters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Connectors.Social
{
    public class SIGooglePlusStreamConnectionParameters : SISocialConnectionParameters
    {
         #region Properties

        public SIGooglePlusStreamParameters StreamParameters;

        #endregion

        public SIGooglePlusStreamConnectionParameters(SIGooglePlusStreamParameters siGooglePlusStreamParameters, int timeOutSeconds = 30, bool useProxy = false, int maxRedirects = 1)
			: base(timeOutSeconds, useProxy, maxRedirects)
		{
            StreamParameters = siGooglePlusStreamParameters;            
		}
    }
}
