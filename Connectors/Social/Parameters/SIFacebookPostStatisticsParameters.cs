﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Connectors.Entities;
using SI.DTO;

namespace Connectors.Parameters
{
    public class SIFacebookPostStatisticsParameters
    {
        public string UniqueID { get; set; }
        public string Token { get; set; }
        public string ResultID { get; set; }
        public string FeedID { get; set; }
        public PostTypeEnum Posttype { get; set; }

        public bool isSharedStory { get; set; }
    }
}
