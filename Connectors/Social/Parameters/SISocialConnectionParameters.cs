﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Connectors.Parameters
{
	public class SISocialConnectionParameters : SIConnectionParameters
	{
		//any custom paramerts for social only
        
		protected SISocialConnectionParameters(int timeOutSeconds, bool useProxy, int maxRedirects)
			: base(timeOutSeconds, useProxy, maxRedirects)
		{
			
		}
	}
}
