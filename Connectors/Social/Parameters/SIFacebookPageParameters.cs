﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Connectors.Parameters
{
    public class SIFacebookPageParameters
    {
        public SIFacebookPageParameters()
        {
            limit = "250";
        }

        public string UniqueID { get; set; }
        public string Token { get; set; }
        public string StartDate { get; set; }
        public string EndDate { get; set; }
        public string Periods { get; set; }
        public string limit { get; set; }
        public string AppToken { get; set; }

        public string PageURL { get; set; }
    }
}
