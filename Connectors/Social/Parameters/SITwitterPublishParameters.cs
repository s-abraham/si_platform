﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SI.DTO;

namespace Connectors.Social.Parameters
{
    public class SITwitterPublishParameters
    {
        public SITwitterPublishParameters()
        {
            TwitterMaxLength = 140;
        }
        public int TwitterMaxLength { get; set; }

        public string status { get; set; }
        public string link { get; set; }
        public string imagePath { get; set; }
        public string uniqueID {get; set;}
        public string oauth_token  {get; set;}
        public  string oauth_token_secret {get; set;}
        public string oauth_consumer_key {get; set;}
        public string oauth_consumer_secret { get; set; }

        public PostTypeEnum postType { get; set; }
    }
}
