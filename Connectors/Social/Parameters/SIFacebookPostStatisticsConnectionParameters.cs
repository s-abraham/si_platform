﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Connectors.Parameters
{
    public class SIFacebookPostStatisticsConnectionParameters : SISocialConnectionParameters
    {
        #region Properties

        public SIFacebookPostStatisticsParameters postStatisticsParameters;

       #endregion


        public SIFacebookPostStatisticsConnectionParameters(SIFacebookPostStatisticsParameters siFacebookPostStatisticsParameters, int timeOutSeconds = 30, bool useProxy = false, int maxRedirects = 1)
			: base(timeOutSeconds, useProxy, maxRedirects)
		{
            postStatisticsParameters = siFacebookPostStatisticsParameters;

		}
    }
}
