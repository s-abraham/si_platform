﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Connectors.Parameters
{
    public class SIFacebookPublishConnectionParameters : SISocialConnectionParameters
    {
        #region Properties

       public SIFacebookPublishParameters publishParameters;

       #endregion


       public SIFacebookPublishConnectionParameters(SIFacebookPublishParameters siFacebookPublishParameters, int timeOutSeconds = 30, bool useProxy = false, int maxRedirects = 1)
			: base(timeOutSeconds, useProxy, maxRedirects)
		{
            publishParameters = siFacebookPublishParameters;            
		}
    }
}
