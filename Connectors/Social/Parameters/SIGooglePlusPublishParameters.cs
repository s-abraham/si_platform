﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SI.DTO;

namespace Connectors.Social.Parameters
{
    public class SIGooglePlusPublishParameters
    {
        public string accessToken { get; set; }
        public string refreshToken { get; set; }
        public string grantType { get; set; }

        //public string pageID { get; set; }
        public string sdUserID { get; set; }
        public string circleID { get; set; }
        public string userID { get; set; }
        public string UniqueID { get; set; }

        public PostTypeEnum postType { get; set; }

        //ACTIVITIES/POSTS
        public string accessType { get; set; }
        public string objectType { get; set; }
        public string activityType { get; set; }
        public string Content { get; set; }
        public string attachmentID { get; set; }
        public string postURL { get; set; }        
        

        public string activityID { get; set; }

        public string apiKey { get; set; }
        public int GooglePlusAPIMaxResults { get; set; }

        public string commentContent { get; set; }
        public string commentID { get; set; }

        public string pathToImage { get; set; }

        public string mediaID { get; set; }

        /*
        public string GooglePlusAPIURLActivityCreate { get; set; }
        public string GooglePlusAPIURLActivityGetListForAPage { get; set; }
        public string GooglePlusAPIURLActivityGetListForACircle { get; set; }
        public string GooglePlusAPIURLActivityGetSpecific { get; set; }
        public string GooglePlusAPIURLActivityDelete { get; set; }
        public string GooglePlusAPIURLActivityUpdate { get; set; }
        public string GooglePlusAPIURLCommentCreate { get; set; }
        public string GooglePlusAPIURLCommentGetForAnActivity { get; set; }
        public string GooglePlusAPIURLCommentGetSpecific { get; set; }
        public string GooglePlusAPIURLCommentDelete { get; set; }
        public string GooglePlusAPIURLCommentUpdate { get; set; }
        public string GooglePlusAPIURLMediaCreate { get; set; }
        public string GooglePlusAPIURLMediaGetSpecific { get; set; }
        */
    }
}
