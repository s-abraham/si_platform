﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SI.DTO;

namespace Connectors.Parameters
{
    public class SIFacebookStreamParameters
    {
        public string UniqueID { get; set; }
        public string Token { get; set; }
        public string PostID { get; set; }
        public string CommentID { get; set; }
        public string Comment { get; set; }
        public string PageURL { get; set; }
        public string limit { get; set; }
    }
}
