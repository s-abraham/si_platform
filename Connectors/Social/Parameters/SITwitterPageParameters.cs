﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Connectors.Social.Parameters
{
    public class SITwitterPageParameters
    {
        public string uniqueID {get; set;}
        public string oauth_token  {get; set;}
        public  string oauth_token_secret {get; set;}
        public string oauth_consumer_key {get; set;}
        public string oauth_consumer_secret { get; set; }
    }
}
