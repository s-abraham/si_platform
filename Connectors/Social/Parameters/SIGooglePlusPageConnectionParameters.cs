﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Connectors.Parameters;

namespace Connectors.Social.Parameters
{
    public class SIGooglePlusPageConnectionParameters : SISocialConnectionParameters
    {
        #region Properties

        public SIGooglePlusPageParameters pageParameters;

        #endregion

        public SIGooglePlusPageConnectionParameters(SIGooglePlusPageParameters siGooglePlusPageParameters, int timeOutSeconds = 30, bool useProxy = false, int maxRedirects = 1)
			: base(timeOutSeconds, useProxy, maxRedirects)
		{
            pageParameters = siGooglePlusPageParameters;            
		}
    }
}
