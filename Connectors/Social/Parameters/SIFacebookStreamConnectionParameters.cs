﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Connectors.Parameters
{
    public class SIFacebookStreamConnectionParameters : SISocialConnectionParameters
    {
        #region Properties

        public SIFacebookStreamParameters streamParameters;

       #endregion


        public SIFacebookStreamConnectionParameters(SIFacebookStreamParameters siFacebookStreamParameters, int timeOutSeconds = 30, bool useProxy = false, int maxRedirects = 1)
			: base(timeOutSeconds, useProxy, maxRedirects)
		{
            streamParameters = siFacebookStreamParameters;
		}
    }
}
