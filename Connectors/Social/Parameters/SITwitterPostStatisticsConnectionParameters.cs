﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Connectors.Parameters;

namespace Connectors.Social.Parameters
{
    public class SITwitterPostStatisticsConnectionParameters : SISocialConnectionParameters
    {
        #region Properties

        public SITwitterPostStatisticsParameters postStatisticsParameters;

       #endregion


        public SITwitterPostStatisticsConnectionParameters(SITwitterPostStatisticsParameters siTwitterPostStatisticsParameters, int timeOutSeconds = 30, bool useProxy = false, int maxRedirects = 1)
			: base(timeOutSeconds, useProxy, maxRedirects)
		{
            postStatisticsParameters = siTwitterPostStatisticsParameters;

		}
    }
}
