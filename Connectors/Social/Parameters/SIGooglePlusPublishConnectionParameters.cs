﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Connectors.Parameters;

namespace Connectors.Social.Parameters
{
    public class SIGooglePlusPublishConnectionParameters : SISocialConnectionParameters
    {
        #region Properties

        public SIGooglePlusPublishParameters publishParameters;
        
        public string clientID { get; set; }
        public string clientSecret { get; set; }
        

        #endregion

        public SIGooglePlusPublishConnectionParameters(SIGooglePlusPublishParameters siGooglePlusPublishParameters, int timeOutSeconds = 30, bool useProxy = false, int maxRedirects = 1)
			: base(timeOutSeconds, useProxy, maxRedirects)
		{
            publishParameters = siGooglePlusPublishParameters;            
		}
    }
}
