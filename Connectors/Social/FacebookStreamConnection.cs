﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Script.Serialization;
using Connectors.Parameters;
using Facebook;
using Facebook.Entities;
using SI.DTO;

namespace Connectors.Social
{
    public class FacebookStreamConnection : SISocialConnection
    {
        private SIFacebookStreamConnectionParameters ConnectionParameters;

        #region INITIALIZER
        public FacebookStreamConnection(SIFacebookStreamConnectionParameters siFacebookStreamConnectionParameters)
            : base(siFacebookStreamConnectionParameters)
        {
            ConnectionParameters = siFacebookStreamConnectionParameters;
        }
        #endregion

        #region FeedActions
        public FacebookFeed GetSocialStreamFacebookFeed()
        {
            FacebookFeed facebookFeed = new FacebookFeed();
            Feed objfeed = new Feed();

            FacebookFeed objFacebookFeed = FacebookAPI.GetFacebookFeed(ConnectionParameters.streamParameters.UniqueID,
                                                                        ConnectionParameters.streamParameters.Token,
                                                                        ConnectionParameters.streamParameters.PageURL,
                                                                        ConnectionParameters.streamParameters.limit);

            List<DataFeed> listDataFeed = new List<DataFeed>();

            if (string.IsNullOrEmpty(objFacebookFeed.facebookResponse.message))
            {
                foreach (DataFeed feed in objFacebookFeed.feed.data)
                {
                    DataFeed datafeed = new DataFeed();

                    string feedid = string.Empty;
                    string resultid = string.Empty;
                    if (string.IsNullOrWhiteSpace(feed.object_id))
                    {
                        resultid = feed.id;
                        feedid = string.Empty;
                    }
                    else
                    {
                        resultid = feed.object_id;
                        feedid = feed.id;
                    }

                    if (string.IsNullOrEmpty(feedid))
                    {
                        feedid = resultid;
                    }
                    string linkURL = string.Empty;
                    string pictureURL = string.Empty;

                    PostTypeEnum PostType = FacebookAPI.GetPostType(feed.type.ToLower());

                    if (PostType == PostTypeEnum.Link)
                    {
                        linkURL = feed.link;
                        pictureURL = feed.picture;
                    }

                    if (PostType == PostTypeEnum.Photo)
                    {
                        pictureURL = feed.picture;
                    }

                    if (PostType == PostTypeEnum.Video)
                    {
                        linkURL = feed.link;
                        pictureURL = feed.picture;
                    }

                    if (PostType == PostTypeEnum.Offer)
                    {
                        linkURL = feed.link;
                        pictureURL = feed.picture;
                    }

                    if (string.IsNullOrWhiteSpace(feed.message))
                    {
                        feed.message = feed.caption;
                    }

                    if (string.IsNullOrWhiteSpace(feed.message))
                    {
                        feed.message = feed.name;
                    }


                    datafeed.created_time = feed.created_time;
                    datafeed.updated_time = feed.updated_time;
                    datafeed.id = feedid;
                    datafeed.name = feed.name;
                    datafeed.picture = pictureURL;
                    datafeed.link = linkURL;
                    datafeed.description = feed.description;
                    datafeed.object_id = resultid;
                    datafeed.type = feed.type;
                    datafeed.message = feed.message;
                    datafeed.caption = feed.caption;
                    datafeed.application = feed.application;
                    datafeed.from = feed.from;

                    listDataFeed.Add(datafeed);
                }
            }
            objfeed.data = listDataFeed;

            facebookFeed.facebookResponse = objFacebookFeed.facebookResponse;
            facebookFeed.feed = objfeed;
            facebookFeed.feed.paging = objFacebookFeed.feed.paging;

            return facebookFeed;
        }

        #endregion

        #region PostActions

        public FacebookStreamAction LikePost()
        {
            FacebookStreamAction objFacebookStreamAction = FacebookAPI.LikePost(ConnectionParameters.streamParameters.PostID,
                ConnectionParameters.streamParameters.Token);

            return objFacebookStreamAction;
        }

        public FacebookStreamAction UnLikePost()
        {
            FacebookStreamAction objFacebookStreamAction = FacebookAPI.UnLikePost(ConnectionParameters.streamParameters.PostID,
                ConnectionParameters.streamParameters.Token);

            return objFacebookStreamAction;
        }

        #endregion

        #region CommentActions

        public FacebookStreamAction CreateCommentonPost()
        {
            FacebookStreamAction objFacebookStreamAction = FacebookAPI.CreateCommentOnPost(ConnectionParameters.streamParameters.PostID,
                ConnectionParameters.streamParameters.Token, ConnectionParameters.streamParameters.Comment);

            return objFacebookStreamAction;
        }

        public FacebookStreamAction DeleteCommentonPost()
        {
            FacebookStreamAction objFacebookStreamAction = FacebookAPI.DeleteCommentOnPost(ConnectionParameters.streamParameters.CommentID,
                ConnectionParameters.streamParameters.Token);

            return objFacebookStreamAction;
        }

        public FacebookStreamAction CreateCommentonComment()
        {
            FacebookStreamAction objFacebookStreamAction = FacebookAPI.CreateCommentOnPost(ConnectionParameters.streamParameters.CommentID,
                ConnectionParameters.streamParameters.Token, ConnectionParameters.streamParameters.Comment);

            return objFacebookStreamAction;
        }

        public FacebookStreamAction LikeComment()
        {
            FacebookStreamAction objFacebookStreamAction = FacebookAPI.LikeComment(ConnectionParameters.streamParameters.CommentID,
                ConnectionParameters.streamParameters.Token);

            return objFacebookStreamAction;
        }

        public FacebookStreamAction UnLikeComment()
        {
            FacebookStreamAction objFacebookStreamAction = FacebookAPI.UnLikeComment(ConnectionParameters.streamParameters.CommentID,
                ConnectionParameters.streamParameters.Token);

            return objFacebookStreamAction;
        }

        public FacebookStreamAction DeletePost()
        {
            FacebookStreamAction objFacebookStreamAction = FacebookAPI.DeletePost(ConnectionParameters.streamParameters.PostID,
                ConnectionParameters.streamParameters.Token);

            return objFacebookStreamAction;
        }


        #endregion

        #region OVERRIDE METHODS
        protected override void dispose()
        {

        }
        #endregion
    }
}
