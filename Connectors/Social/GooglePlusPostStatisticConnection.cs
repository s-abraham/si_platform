﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Script.Serialization;
using Connectors.Social.Parameters;
using Google;
using Google.Model;

namespace Connectors.Social
{
    public class GooglePlusPostStatisticConnection : SISocialConnection
    {
        private SIGooglePlusPostStatisticsConnectionParameters ConnectionParameters;

        #region INITIALIZER
        public GooglePlusPostStatisticConnection(SIGooglePlusPostStatisticsConnectionParameters siGooglePlusPostStatisticsConnectionParameters)
            : base(siGooglePlusPostStatisticsConnectionParameters)
        {
            ConnectionParameters = siGooglePlusPostStatisticsConnectionParameters;
        }
        #endregion
        
        #region STATISTICS

        public GooglePlusAddorUpdateorGetSpecificActivityResponse GetGooglePlusActivityByActivityID()
        {
            GooglePlusAddorUpdateorGetSpecificActivityResponse obj = GoogleAPI.GetGooglePlusActivityByActivityID(ConnectionParameters.postStatisticsParameters.activityID,
                                                                                                                    ConnectionParameters.postStatisticsParameters.apiKey);

            return obj;
        }

        public GooglePlusGetCommentsListResponse GetGooglePlusCommentsListForAnActivity()
        {
            GooglePlusGetCommentsListResponse obj = GoogleAPI.GetGooglePlusCommentsListForAnActivity(ConnectionParameters.postStatisticsParameters.activityID,
                                                                                                                    ConnectionParameters.postStatisticsParameters.apiKey);

            return obj;
        }

        public GooglePlusGetPlusonersListResponse GetGooglePlusPlusonersListForAnActivity()
        {
            GooglePlusGetPlusonersListResponse obj = GoogleAPI.GetGooglePlusPlusonersListForAnActivity(ConnectionParameters.postStatisticsParameters.activityID,
                                                                                                                    ConnectionParameters.postStatisticsParameters.apiKey);

            return obj;
        }

        public GooglePlusGetResharersListResponse GetGooglePlusResharersListForAnActivity()
        {
            GooglePlusGetResharersListResponse obj = GoogleAPI.GetGooglePlusResharersListForAnActivity(ConnectionParameters.postStatisticsParameters.activityID,
                                                                                                                    ConnectionParameters.postStatisticsParameters.apiKey);

            return obj;
        }

        #endregion

        #region OVERRIDE METHODS
        protected override void dispose()
        {

        }
        #endregion
    }
}
