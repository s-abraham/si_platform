﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Script.Serialization;
using Connectors.Social.Parameters;
using Twitter;
using Twitter.Entities;
using Twitterizer;

namespace Connectors.Social
{
    public class TwitterPageConnection : SISocialConnection
    {

        private SITwitterPageConnectionParameters ConnectionParameters;

        #region INITIALIZER
        public TwitterPageConnection(SITwitterPageConnectionParameters siTwitterPageConnectionParameters)
            : base(siTwitterPageConnectionParameters)
        {
            ConnectionParameters = siTwitterPageConnectionParameters;
        }
        #endregion

        #region PAGE INSIGHTS

        public TwitterPageInsightsResponse GetTwitterPageInsights()
        {
            TwitterPageInsightsResponse objTwitterPageInsightsResponse = TwitterAPI.GetTwitterPageInsights(ConnectionParameters.pageParameters.uniqueID,
                                                                                                                ConnectionParameters.pageParameters.oauth_token,
                                                                                                                ConnectionParameters.pageParameters.oauth_token_secret,
                                                                                                                ConnectionParameters.pageParameters.oauth_consumer_key,
                                                                                                                ConnectionParameters.pageParameters.oauth_consumer_secret);

            return objTwitterPageInsightsResponse;
        }
        
        #endregion
        
        #region VERIFY TOKEN

        public bool ValidateToken()
        {
            bool resule = TwitterAPI.ValidateToken(ConnectionParameters.pageParameters.oauth_token,
                                                    ConnectionParameters.pageParameters.oauth_token_secret,
                                                    ConnectionParameters.pageParameters.oauth_consumer_key,
                                                    ConnectionParameters.pageParameters.oauth_consumer_secret);

            return resule;
        }

        public TwitterResponseToken ValidateToken2()
        {
            var resule= TwitterAPI.ValidateToken2(ConnectionParameters.pageParameters.oauth_token,
                                                    ConnectionParameters.pageParameters.oauth_token_secret,
                                                    ConnectionParameters.pageParameters.oauth_consumer_key,
                                                    ConnectionParameters.pageParameters.oauth_consumer_secret);

            return resule;
        }

        #endregion

        #region OVERRIDE METHODS
        protected override void dispose()
        {

        }
        #endregion
    }
}
