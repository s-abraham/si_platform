﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Web.Script.Serialization;
//using Connectors.Entities;
using Connectors.Social.Parameters;
using Google;
using Google.Model;
using Newtonsoft.Json;
using SI;
using SI.DTO;

namespace Connectors.Social
{
    public class GooglePlusPublishConnection : SISocialConnection
    {
        private SIGooglePlusPublishConnectionParameters ConnectionParameters;

        #region INITIALIZER
        public GooglePlusPublishConnection(SIGooglePlusPublishConnectionParameters siGooglePlusPublishConnectionParameters)
            : base(siGooglePlusPublishConnectionParameters)
        {
            ConnectionParameters = siGooglePlusPublishConnectionParameters;
        }
        #endregion

        public string GetGooglePlusAccessTokenbyRefreshToken()
        {            
            string accessToken = string.Empty;
            try
            {
                int MaxRetrytogetAccessToken = 3;
                GooglePlusAccessTokenRequestResponse objGooglePlusAccessTokenRequestResponse = new GooglePlusAccessTokenRequestResponse();
                for (int iCounter = 0; iCounter <= MaxRetrytogetAccessToken; iCounter++)
                {
                    objGooglePlusAccessTokenRequestResponse = GoogleAPI.GetGooglePlusAccessToken(ConnectionParameters.clientID, 
                                                                                                    ConnectionParameters.clientSecret, 
                                                                                                    ConnectionParameters.publishParameters.refreshToken, 
                                                                                                    ConnectionParameters.publishParameters.grantType);

                    if (objGooglePlusAccessTokenRequestResponse != null)
                    {
                        if (!string.IsNullOrEmpty(objGooglePlusAccessTokenRequestResponse.access_token))
                        {
                            accessToken = objGooglePlusAccessTokenRequestResponse.access_token;
                            break;
                        }
                        else
                        {
                            // Log Retry to get Access Token
                            #region LOG RETRY TO GET ACCESS TOKEN
                            #endregion
                        }
                    }
                    else
                    {
                        #region LOG RETRY TO GET ACCESS TOKEN
                        #endregion
                    }
                }
            }
            catch (Exception ex)
            {
                #region ERROR HANDLING
                #endregion
            }

            return accessToken;
        }

        #region ACTIVITIES/POSTS
            
        public GooglePlusPostResponse CreateGooglePlusActivity()
        {
            GooglePlusPostResponse objGooglePlusPostResponse = new GooglePlusPostResponse();

            //ConnectionParameters.publishParameters.accessToken = GetGooglePlusAccessTokenbyRefreshToken();


            if (ConnectionParameters.publishParameters.postType == PostTypeEnum.Status)
            {
                ConnectionParameters.publishParameters.objectType = string.Empty;                                                
            }
            else if (ConnectionParameters.publishParameters.postType == PostTypeEnum.Link)
            {
                ConnectionParameters.publishParameters.objectType = "article";
            }

            objGooglePlusPostResponse = GoogleAPI.AddGooglePlusActivity(ConnectionParameters.publishParameters.accessToken,
                                                                                ConnectionParameters.publishParameters.UniqueID,
                                                                                ConnectionParameters.publishParameters.accessType,
                                                                                ConnectionParameters.publishParameters.objectType,
                                                                                ConnectionParameters.publishParameters.activityType,
                                                                                ConnectionParameters.publishParameters.Content,
                                                                                ConnectionParameters.publishParameters.attachmentID,
                                                                                ConnectionParameters.publishParameters.postURL,
                                                                                ConnectionParameters.publishParameters.circleID,
                                                                                ConnectionParameters.publishParameters.userID);

            return objGooglePlusPostResponse;
        }
                
        #endregion
              
        #region MEDIA

        public GooglePlusPostResponse PostGooglePlusPhoto()
        {
            GooglePlusPostResponse objGooglePlusPostResponse = new GooglePlusPostResponse();

            objGooglePlusPostResponse = GoogleAPI.PostGooglePlusPhoto(ConnectionParameters.publishParameters.accessToken,
                                                                                ConnectionParameters.publishParameters.UniqueID,
                                                                                ConnectionParameters.publishParameters.accessType,
                                                                                ConnectionParameters.publishParameters.activityType,
                                                                                ConnectionParameters.publishParameters.Content,
                                                                                ConnectionParameters.publishParameters.postURL,
                                                                                ConnectionParameters.publishParameters.pathToImage,
                                                                                ConnectionParameters.publishParameters.circleID,
                                                                                ConnectionParameters.publishParameters.userID);

            return objGooglePlusPostResponse;
        }
                
        #endregion
               
        #region OVERRIDE METHODS
        protected override void dispose()
        {

        }
        #endregion
    }
}
