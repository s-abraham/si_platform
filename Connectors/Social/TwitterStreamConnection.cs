﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Twitter;
using Twitter.Entities;

namespace Connectors.Social
{
    public class TwitterStreamConnection : SISocialConnection
    {
        private SITwitterStreamConnectionParameters ConnectionParameters;

        #region INITIALIZER
        public TwitterStreamConnection(SITwitterStreamConnectionParameters siTwitterStreamConnectionParameters)
            : base(siTwitterStreamConnectionParameters)
        {
            ConnectionParameters = siTwitterStreamConnectionParameters;
        }
        #endregion


        #region PAGE

        public List<TwitterTweet> GetHomeTweets()
        {
            List<TwitterTweet> listTwitterTweet = TwitterAPI.GetHomeTweets(ConnectionParameters.StreamParameters.oauth_token,
                                                                            ConnectionParameters.StreamParameters.oauth_token_secret,
                                                                            ConnectionParameters.StreamParameters.oauth_consumer_key,
                                                                            ConnectionParameters.StreamParameters.oauth_consumer_secret,
                                                                            ConnectionParameters.StreamParameters.screenName,
                                                                            ConnectionParameters.StreamParameters.count,
                                                                            ConnectionParameters.StreamParameters.sinceID);

            return listTwitterTweet;
        }

        public List<TwitterTweet> GetUserTweets()
        {
            List<TwitterTweet> listTwitterTweet = TwitterAPI.GetUserTweets(ConnectionParameters.StreamParameters.oauth_token,
                                                                            ConnectionParameters.StreamParameters.oauth_token_secret,
                                                                            ConnectionParameters.StreamParameters.oauth_consumer_key,
                                                                            ConnectionParameters.StreamParameters.oauth_consumer_secret,
                                                                            ConnectionParameters.StreamParameters.screenName,
                                                                            ConnectionParameters.StreamParameters.count,
                                                                            ConnectionParameters.StreamParameters.sinceID);

            return listTwitterTweet;
        }

        public List<TwitterTweet> GetMentions()
        {
            List<TwitterTweet> listTwitterTweet = TwitterAPI.GetMentions(ConnectionParameters.StreamParameters.oauth_token,
                                                                            ConnectionParameters.StreamParameters.oauth_token_secret,
                                                                            ConnectionParameters.StreamParameters.oauth_consumer_key,
                                                                            ConnectionParameters.StreamParameters.oauth_consumer_secret,
                                                                            ConnectionParameters.StreamParameters.screenName,
                                                                            ConnectionParameters.StreamParameters.count,
                                                                            ConnectionParameters.StreamParameters.sinceID);

            return listTwitterTweet;
        }

        public TwitterRequestResult SetFavorite()
        {
            TwitterRequestResult result = TwitterAPI.SetFavorite(ConnectionParameters.StreamParameters.oauth_token,
                                                                            ConnectionParameters.StreamParameters.oauth_token_secret,
                                                                            ConnectionParameters.StreamParameters.oauth_consumer_key,
                                                                            ConnectionParameters.StreamParameters.oauth_consumer_secret,
                                                                            ConnectionParameters.StreamParameters.statusID);

            return result;
        }

        public TwitterRequestResult DeleteFavorite()
        {
            TwitterRequestResult result = TwitterAPI.DeleteFavorite(ConnectionParameters.StreamParameters.oauth_token,
                                                                            ConnectionParameters.StreamParameters.oauth_token_secret,
                                                                            ConnectionParameters.StreamParameters.oauth_consumer_key,
                                                                            ConnectionParameters.StreamParameters.oauth_consumer_secret,
                                                                            ConnectionParameters.StreamParameters.statusID);

            return result;
        }
              
        public TwitterTweet Retweet()
        {
            TwitterTweet tweet = TwitterAPI.Retweet(ConnectionParameters.StreamParameters.oauth_token,
                                                    ConnectionParameters.StreamParameters.oauth_token_secret,
                                                    ConnectionParameters.StreamParameters.oauth_consumer_key,
                                                    ConnectionParameters.StreamParameters.oauth_consumer_secret,
                                                    ConnectionParameters.StreamParameters.statusID,
                                                    ConnectionParameters.StreamParameters.screenName);

            return tweet;
        }

        public TwitterRequestResult ReplyToTweet()
        {
            TwitterRequestResult result = TwitterAPI.ReplyToTweet(ConnectionParameters.StreamParameters.oauth_token,
                                                                    ConnectionParameters.StreamParameters.oauth_token_secret,
                                                                    ConnectionParameters.StreamParameters.oauth_consumer_key,
                                                                    ConnectionParameters.StreamParameters.oauth_consumer_secret,
                                                                    ConnectionParameters.StreamParameters.status,
                                                                    ConnectionParameters.StreamParameters.inReplyToStatusID);

            return result;
        }

        public TwitterRequestResult DeleteTweet()
        {
            TwitterRequestResult result = TwitterAPI.DeleteTweet(ConnectionParameters.StreamParameters.oauth_token,
                                                                    ConnectionParameters.StreamParameters.oauth_token_secret,
                                                                    ConnectionParameters.StreamParameters.oauth_consumer_key,
                                                                    ConnectionParameters.StreamParameters.oauth_consumer_secret,
                                                                    ConnectionParameters.StreamParameters.statusID);

            return result;
        }

        #endregion


        #region OVERRIDE METHODS
        protected override void dispose()
        {

        }
        #endregion
    }
}
