﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Connectors.Parameters
{
    public class SIFacebookPageParameters
    {
        public string UniqueID { get; set; }
        public string Token { get; set; }

        public string FacebookGraphAPIURLPage { get; set; }
        public string FacebookGraphAPIURLInsights { get; set; }
        public string FacebookGraphAPIURLFeed { get; set; }
        public string FacebookPermissionsURL { get; set; }
        public string FacebookGraphAPIURLSearch { get; set; }
        public string FacebookGraphAPIURLPageSearch { get; set; }
        public string FacebookGraphAPIURLAlbumGet { get; set; }
        public string FacebookGraphAPIURLUserPicture { get; set; }

        public string XPathParentnodePermissionsDetails { get; set; }
        public string XPathlistnodePermissionsDetails { get; set; }
        public string XPathParentError { get; set; }
        public string XPathError { get; set; }
        public string XPathErrorTd { get; set; }
        public string XPathTableheader { get; set; }
        public string XPathLink { get; set; }
    }
}
