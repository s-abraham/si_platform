﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Connectors.Parameters
{
    public class SIFacebookPublishParameters
    {
        public string UniqueID { get; set; }
        public string Token { get; set; }

        // Wall Post
        public string FacebookGraphAPIURLWallPostCreate { get; set; }
        public string wallpost_message { get; set; }
        public string wallpost_caption { get; set; }
        public string wallpost_description { get; set; }
        public string wallpost_name { get; set; }
        public string wallpost_picture { get; set; }
        public string wallpost_link { get; set; }
        public string wallpost_source { get; set; }
        public string wallpost_targeting { get; set; }


        // Create Album
        public string FacebookGraphAPIURLAlbumCreate { get; set; }
        public string album_Name { get; set; }
        public string album_Message { get; set; }


        //Create Question
        public string FacebookGraphAPIURLQuestionCreate { get; set; }
        public string Question_Text { get; set; }
        public string Question_Options { get; set; }
        public int Question_OptionsCount { get; set; }
        public string Question_AllowNewOptions { get; set; }

        //Upload Photo
        public string FacebookGraphAPIURLUploadPhoto { get; set; }
        public string Album_id { get; set; }        
    }
}
