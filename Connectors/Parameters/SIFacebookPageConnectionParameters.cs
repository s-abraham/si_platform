﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Connectors.Parameters
{
    public class SIFacebookPageConnectionParameters : SISocialConnectionParameters
    {       

       #region Properties

       public SIFacebookPageParameters pageParameters;

       #endregion


       public SIFacebookPageConnectionParameters(SIFacebookPageParameters siFacebookPageParameters, int timeOutSeconds = 30, bool useProxy = false, int maxRedirects = 1)
			: base(timeOutSeconds, useProxy, maxRedirects)
		{
            pageParameters = siFacebookPageParameters;            
		}
               
    }
}
