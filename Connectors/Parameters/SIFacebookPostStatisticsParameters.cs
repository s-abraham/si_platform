﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Connectors.Entities;

namespace Connectors.Parameters
{
    public class SIFacebookPostStatisticsParameters
    {        
        public string Token { get; set; }
        public string ResultID { get; set; }
        public string FeedID { get; set; }
        public PostType Posttype { get; set; }

        public string FacebookGraphAPIURLPostStatistics { get; set; }
        public string FacebookGraphAPIURLPostStatisticsByFeedID { get; set; }
        public string FacebookGraphAPIURLEventStatistics { get; set; }
        public string FacebookGraphAPIURLQuestionStatistics { get; set; }
    }
}
