﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Connectors.Parameters
{
	public abstract class SIConnectionParameters
	{

		/// <summary>
		/// Http Request Timeout in seconds
		/// </summary>
		public int TimeoutSeconds { get; set; }
		/// <summary>
		/// Use Proxy, true be default
		/// </summary>
		public bool UseProxy { get; set; }
		/// <summary>
		/// Connection redirects, default set to 50
		/// </summary>
		public int MaxRedirects { get; set; }


		protected SIConnectionParameters(int timeOutSeconds, bool useProxy, int maxRedirects)
		{
			TimeoutSeconds = timeOutSeconds;
			UseProxy = useProxy;
			MaxRedirects = maxRedirects;
		}
	}
}
