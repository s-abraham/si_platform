﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Connectors.Parameters;

namespace Connectors.Lookups.Parameters
{
    public class SIEdmundsLookupParameters : SIConnectionParameters
    {
        //Any custom paramerts for lookups only
        public string DealerId { get; set; }
        public string Address { get; set; }

        public string DealerName { get; set; }
        public string DealerLogicalName { get; set; }
        public string ZipCode { get; set; }
        public string DealerType { get; set; }
        public string Make { get; set; }

        public string ApiRequestUrl { get; set; }

        public SIEdmundsLookupParameters(int timeOutSeconds = 30, bool useProxy = false, int maxRedirects = 1) : base(timeOutSeconds, useProxy, maxRedirects)
        {
            DealerId = string.Empty;
            Address = string.Empty;
            DealerName = string.Empty;
            DealerLogicalName = string.Empty;
            ZipCode = string.Empty;
            DealerType = string.Empty;
            Make = string.Empty;
            ApiRequestUrl = string.Empty;
        }
    }
}
