﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using Connectors.Parameters;
using HtmlAgilityPack;
using SI;

namespace Connectors.Lookups
{
    public abstract class SILookupConnection : SIConnection
    {
        protected SILookupConnection(SIConnectionParameters siconnectionParameters) : base(siconnectionParameters)
		{
			
		}
    }
}
