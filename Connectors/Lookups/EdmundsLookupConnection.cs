﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Connectors.Entities;
using Connectors.Lookups.Parameters;

namespace Connectors.Lookups
{
    public class EdmundsLookupConnection : SILookupConnection
    {
        #region Properties

        public SIEdmundsLookupParameters ConnectionParameters;

        #endregion

        #region INITIALIZER
        public EdmundsLookupConnection(SIEdmundsLookupParameters siedmundsLookupParameters) : base(siedmundsLookupParameters)
        {
            ConnectionParameters = siedmundsLookupParameters;
        }
        #endregion

        #region Public Methods

        public EdmundsEntities.DealerList GetDealers()
        {
            EdmundsEntities.DealerList dealerList = new EdmundsEntities.DealerList();

            if (!string.IsNullOrEmpty(ConnectionParameters.ApiRequestUrl))
            {
                HttpRequestResults = string.Empty;

                var httpStatus = ConnectorHttpRequest(ConnectionParameters.ApiRequestUrl);
                if (httpStatus == ConnectionStatus.Success)
                {
                    if (!string.IsNullOrEmpty(HttpRequestResults))
                    {
                        dynamic rawDealerList = Newtonsoft.Json.JsonConvert.DeserializeObject<dynamic>(HttpRequestResults);
                        if (rawDealerList != null)
                        {
                            dealerList.ConnectionStatus = ConnectionStatus.Success;

                            if (rawDealerList.dealerHolder.Count > 0)
                            {
                                foreach (var dealerItem in rawDealerList.dealerHolder)
                                {

                                }
                            }
                            


                        //    if (reviewDetails.salesReviews != null && reviewDetails.salesReviews.Count > 0)
                        //    {
                        //        SalesAverageRating = (decimal)reviewDetails.salesReviews[0].averageRating;
                        //        SalesReviewCount = reviewDetails.salesReviews.Count;

                        //        if (includeDetails)
                        //        {
                        //            foreach (var salesReview in reviewDetails.salesReviews)
                        //            {
                        //                ReviewDetails.Add(PopulateReview("sales", salesReview));
                        //            }
                        //        }
                        //    }

                        //    if (reviewDetails.serviceReviews != null && reviewDetails.serviceReviews.Count > 0)
                        //    {
                        //        ServiceAverageRating = (decimal)reviewDetails.serviceReviews[0].averageRating;
                        //        ServiceReviewCount = reviewDetails.serviceReviews.Count;

                        //        if (includeDetails)
                        //        {
                        //            foreach (var serviceReview in reviewDetails.serviceReviews)
                        //            {
                        //                ReviewDetails.Add(PopulateReview("service", serviceReview));
                        //            }
                        //        }
                        //    }
                        }
                    }
                }
                else
                    dealerList.ConnectionStatus = ConnectionStatus.InvalidData;
            }

            return dealerList;
        }

        #endregion



        protected override void dispose()
        {

        }
    }
}
