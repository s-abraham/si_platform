﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Connectors.Entities
{
	[Serializable]
    public class LinkPreview
	{
		public string PageTitle { get; set; }
		public string PageDescription { get; set; }
		public List<string> PageImages { get; set; }
		public string PageUrl { get; set; }
		public bool IsValidUrl { get; set; }

		public LinkPreview()
		{
			PageImages = new List<string>();
		}
	}
}
