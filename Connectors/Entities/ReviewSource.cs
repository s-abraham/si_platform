﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Connectors.Entities
{

    public enum URLHarvestReason
    {
        AccountnameMatch = 1,
        NotAbleToValidateTryAgain = 2,
        AccountNameAndStreetAddressNotMatch = 3,
        AccountNameNotMatchButStreetAddressMatch = 4,
        URLMissing = 5
    }
}
