﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace Connectors.Entities
{
    public class FaceBookEntities
    {

    }

    #region FACEBOOK ALBUM

    public class FacebookAlbum
    {
        public FacebookAlbum()
        {
            albums = new Album();
            facebookResponse = new FacebookResponse();
        }
        public Album albums { get; set; }
        public FacebookResponse facebookResponse { get; set; }
    }

    public class Album
    {
        public Album()
        {
            data = new List<Albumdata>();   
        }
        public List<Albumdata> data { get; set; }
        
    }

    public class Albumdata
    {
        public Albumdata()
        {
            id = string.Empty;
            name = string.Empty;
            can_upload = true;
            count = string.Empty;
        }
        public string id { get; set; }
        public string name { get; set; }
        public bool can_upload { get; set; }
        public string count { get; set; }
    }

    #endregion

    #region FACEBOOK RESPONSE

    public class FacebookResponse
    {
        public FacebookResponse()
        {
            ID = string.Empty;
            message = string.Empty;
            type = string.Empty;
            code = string.Empty;
            status = string.Empty;
            PhotoURL = string.Empty;
        }
        public string ID { get; set; }
        public string message { get; set; }
        public string type { get; set; }
        public string code { get; set; }
        public string status { get; set; }
        public string PhotoURL { get; set; }

        public override string ToString()
        {

            string str = "ID : " + ID + " , Message :" + message + " , Type : " + type + " , Code : " + code;

            return str;
        }
    }

    #endregion

    #region FACEBOOK REQUEST

    public class FacebookRequest
    {
        public FacebookRequest()
        {
            RequestURL = string.Empty;
            Parameters = string.Empty;
            Method = string.Empty;
            Status = string.Empty;
            Album_id = string.Empty;
            Filename = string.Empty;
            Message = string.Empty;

        }
        public string RequestURL { get; set; }
        public string Parameters { get; set; }
        public string Method { get; set; }
        public int TimeOut { get; set; }
        public string Album_id { get; set; }
        public byte[] bytes { get; set; }
        public string Filename { get; set; }
        public string Message { get; set; }
        public string Status { get; set; }
    }

    #endregion

    #region FACEBOOK QUESTION

    public class FacebookQuestion
    {
        public FacebookQuestion()
        {
            question = new List<Question>();
            facebookResponse = new FacebookResponse();
        }

        public List<Question> question { get; set; }
        public FacebookResponse facebookResponse { get; set; }
    }

    public class Question
    {
        public Question()
        {
            id = string.Empty;
            is_published = string.Empty;
            question = string.Empty;
            from = new From();
            options = new Options();
        }

        public DateTime created_time { get; set; }
        public string id { get; set; }
        public string is_published { get; set; }
        public string question { get; set; }
        public DateTime updated_time { get; set; }
        public From from { get; set; }
        public Options options { get; set; }
    }

    public class From
    {
        public From()
        {
            id = string.Empty;
            name = string.Empty;
            category = string.Empty;
            pictureurl = string.Empty;
        }

        public string id { get; set; }
        public string name { get; set; }
        public string category { get; set; }
        public string pictureurl { get; set; }
    }

    public class Options
    {
        public Options()
        {
            data = new List<Data>();
        }
        public List<Data> data { get; set; }
    }

    public class Data
    {
        public Data()
        {
            id = string.Empty;
            name = string.Empty;
            vote_count = string.Empty;
            from = new From();
            votes = new Votes();
        }
        public string id { get; set; }
        public string name { get; set; }
        public string vote_count { get; set; }
        public DateTime created_time { get; set; }
        public From from { get; set; }
        public Votes votes { get; set; }
    }

    public class FacebookQuestionStatistics
    {
        public FacebookQuestionStatistics()
        {
            questionStatistics = new QuestionStatistics();
            facebookResponse = new FacebookResponse();
        }

        public QuestionStatistics questionStatistics { get; set; }
        public FacebookResponse facebookResponse { get; set; }

    }
    public class QuestionStatistics
    {
        public QuestionStatistics()
        {
            options = new Options();
        }
        public DateTime created_time { get; set; }
        public string id { get; set; }
        public From from { get; set; }
        public bool is_published { get; set; }
        public string question { get; set; }
        public DateTime updated_time { get; set; }
        public Options options { get; set; }

    }

    public class Votes
    {
        public Votes()
        {
            data = new List<DataVote>();
        }
        public List<DataVote> data { get; set; }
    }

    public class DataVote
    {
        public DataVote()
        {
            id = string.Empty;
            name = string.Empty;
            gender = string.Empty;
        }

        public string id { get; set; }
        public string name { get; set; }
        public string gender { get; set; }
    }

    #endregion

    #region FACEBOOK EVENT

    public class FacebookEvent
    {
        public FacebookEvent()
        {
            events = new List<Event>();
            facebookResponse = new FacebookResponse();
        }
        public List<Event> events { get; set; }
        public FacebookResponse facebookResponse { get; set; }
    }

    public class Event
    {
        public Event()
        {
            id = string.Empty;
            name = string.Empty;
            description = string.Empty;
            rsvp_status = string.Empty;
            location = string.Empty;
        }
        public string id { get; set; }
        public string name { get; set; }
        public DateTime start_time { get; set; }
        public DateTime end_time { get; set; }
        public bool is_date_only { get; set; }
        public string description { get; set; }
        public string rsvp_status { get; set; }
        public string location { get; set; }

    }

    public class FacebookEventStatistics
    {
        public FacebookEventStatistics()
        {
            facebookResponse = new FacebookResponse();
            eventStatistics = new EventStatistics();
        }
        public EventStatistics eventStatistics { get; set; }
        public FacebookResponse facebookResponse { get; set; }
    }

    public class EventStatistics
    {
        public EventStatistics()
        {
            data = new List<DataEventStatistics>();
        }
        public Summary summary { get; set; }
        public List<DataEventStatistics> data { get; set; }

    }

    public class Summary
    {
        public Summary()
        {
            noreply_count = 0;
            maybe_count = 0;
            declined_count = 0;
            attending_count = 0;
            count = 0;
        }

        public int noreply_count { get; set; }
        public int maybe_count { get; set; }
        public int declined_count { get; set; }
        public int attending_count { get; set; }
        public int count { get; set; }
    }

    public class DataEventStatistics
    {
        public DataEventStatistics()
        {
            id = string.Empty;
            name = string.Empty;
            rsvp_status = string.Empty;
        }

        public string id { get; set; }
        public string name { get; set; }
        public string rsvp_status { get; set; }
    }
    #endregion

    #region FACEBOOK POST STATISTICS

    [XmlType(TypeName = "FacebookPostStatistics")]
    public class FacebookPostStatistics
    {
        public FacebookPostStatistics()
        {
            postStatistics = new PostStatistics();
            facebookResponse = new FacebookResponse();
        }
        [XmlElement("Statistics")]
        public PostStatistics postStatistics { get; set; }
        public FacebookResponse facebookResponse { get; set; }

    }

    [XmlType(TypeName = "FacebookPostStatistics")]
    public class FacebookPostStatisticsFeedID
    {
        public FacebookPostStatisticsFeedID()
        {
            postStatisticsbyfeedid = new PostStatisticsByFeedID();
            facebookResponse = new FacebookResponse();
        }
        public FacebookResponse facebookResponse { get; set; }
        [XmlElement("FeedIDStatistics")]
        public PostStatisticsByFeedID postStatisticsbyfeedid { get; set; }

    }

    [XmlType(TypeName = "Statistics")]
    public class PostStatisticsByFeedID
    {
        public PostStatisticsByFeedID()
        {
            is_Deleted = false;
            likes = new Likes();
            shares = new Shares();

        }

        public string id { get; set; }
        public From from { get; set; }
        public string message { get; set; }
        public string name { get; set; }
        public string link { get; set; }
        public Privacy privacy { get; set; }
        public Application application { get; set; }
        public bool is_published { get; set; }
        public string object_id { get; set; }
        public string type { get; set; }
        public string status_type { get; set; }
        public DateTime created_time { get; set; }
        public DateTime updated_time { get; set; }
        public Shares shares { get; set; }
        public CommentsFeedID comments { get; set; }
        public Likes likes { get; set; }
        public bool is_Deleted { get; set; }
        public Sharedposts sharedposts { get; set; }

    }

    [XmlType(TypeName = "Statistics")]
    public class PostStatistics
    {
        public PostStatistics()
        {
            is_Deleted = false;
            likes = new Likes();
            likescount = 0;
            commentscount = 0;
            sharescount = 0;
            iscountcalulated = false;

        }

        public string id { get; set; }
        public From from { get; set; }
        public string message { get; set; }
        public string name { get; set; }
        public string link { get; set; }
        public Privacy privacy { get; set; }
        public Application application { get; set; }
        public bool is_published { get; set; }
        public string object_id { get; set; }
        public string type { get; set; }
        public string status_type { get; set; }
        public DateTime created_time { get; set; }
        public DateTime updated_time { get; set; }
        public Shares shares { get; set; }
        public Comments comments { get; set; }
        public Likes likes { get; set; }
        public bool is_Deleted { get; set; }
        public Sharedposts sharedposts { get; set; }
        public int likescount { get; set; }
        public int commentscount { get; set; }
        public int sharescount { get; set; }
        public bool iscountcalulated { get; set; }

    }

    public class Likes
    {
        public Likes()
        {
            count = 0;
            data = new List<DataLikes>();
        }
        public List<DataLikes> data { get; set; }
        public int count { get; set; }
    }

    public class DataLikes
    {
        public string id { get; set; }
        public string name { get; set; }
    }

    public class Comments
    {
        public Comments()
        {
            count = 0;
        }

        public List<DataComments> data { get; set; }
        public int count { get; set; }
    }

    public class CommentsFeedID
    {
        public CommentsFeedID()
        {
            count = 0;
        }

        public List<DataCommentsFeedID> data { get; set; }
        public int count { get; set; }
    }
    public class DataCommentsFeedID
    {
        public string id { get; set; }
        public From from { get; set; }
        public string message { get; set; }
        public DateTime created_time { get; set; }
        public bool user_likes { get; set; }
        public int like_count { get; set; }
        public int likes { get; set; }
    }

    public class DataComments
    {
        public string id { get; set; }
        public From from { get; set; }
        public string message { get; set; }
        public DateTime created_time { get; set; }
        public bool user_likes { get; set; }
        public int like_count { get; set; }
        public Likes likes { get; set; }
    }

    public class Privacy
    {
        public string description { get; set; }
        public string value { get; set; }
    }

    public class Application
    {
        public string name { get; set; }
        public string Namespace { get; set; }
        public string id { get; set; }
    }

    public class Shares
    {
        public Shares()
        {
            count = 0;
        }
        public int count { get; set; }
    }

    #endregion

    #region FACEBOOK PHOTO STATISTICS

    [XmlType(TypeName = "FacebookPostStatistics")]
    public class FacebookPhotoStatistics
    {
        public FacebookPhotoStatistics()
        {
            photoStatistics = new PhotoStatistics();
            facebookResponse = new FacebookResponse();
        }
        [XmlElement("Statistics")]
        public PhotoStatistics photoStatistics { get; set; }
        public FacebookResponse facebookResponse { get; set; }
    }

    [XmlType(TypeName = "Statistics")]
    public class PhotoStatistics
    {
        public PhotoStatistics()
        {
            is_Deleted = false;
        }
        public string id { get; set; }
        public From from { get; set; }
        public string message { get; set; }
        public string name { get; set; }
        public string link { get; set; }
        public Privacy privacy { get; set; }
        public Application application { get; set; }
        public bool is_published { get; set; }
        public string object_id { get; set; }
        public string type { get; set; }
        public string status_type { get; set; }
        public DateTime created_time { get; set; }
        public DateTime updated_time { get; set; }
        public Shares shares { get; set; }
        public Comments comments { get; set; }
        public Likes likes { get; set; }
        public Sharedposts sharedposts { get; set; }
        public Tags tags { get; set; }
        public bool is_Deleted { get; set; }


        //privacy,type,application,object_id

        //public string id { get; set; }
        //public From from { get; set; }
        //public string name { get; set; }
        //public string picture { get; set; }
        //public string source { get; set; }
        //public string link { get; set; }
        //public DateTime created_time { get; set; }
        //public DateTime updated_time { get; set; }
        //public Likes likes { get; set; }
        //public Comments comments { get; set; }
        //public Tags tags { get; set; }
        //public Sharedposts sharedposts { get; set; }
    }

    public class Tags
    {
        public List<DataTags> data { get; set; }
    }

    public class DataTags
    {
        public string id { get; set; }
        public string name { get; set; }
        public DateTime created_time { get; set; }
        public string x { get; set; }
        public string y { get; set; }

    }

    public class Sharedposts
    {
        public List<DataSharedposts> data { get; set; }
    }

    public class DataSharedposts
    {
        public DataSharedposts()
        {
            message = string.Empty;
        }
        public DateTime created_time { get; set; }
        public From from { get; set; }
        public string id { get; set; }
        public string name { get; set; }
        public string message { get; set; }
    }

    #endregion

    #region FACEBOOK INSIGHTS

    public class FacebookInsights
    {
        public FacebookInsights()
        {
            facebookResponse = new FacebookResponse();
            responseJson = string.Empty;
            facebookResponse = new FacebookResponse();
            insights = new Insights();
        }
        [XmlIgnore]
        public string responseJson { get; set; }
        public Insights insights { get; set; }
        public FacebookResponse facebookResponse { get; set; }
    }

    public class Insights
    {
        public Insights()
        {
            data = new List<DataInsights>();
        }
        public List<DataInsights> data { get; set; }
    }

    public class DataInsights
    {
        public DataInsights()
        {
            values = new List<Value>();
        }

        public string id;
        public string name;
        public string period;
        public List<Value> values;
        public string title;
        public string description;
        public int sum;
    }

    public class InsightModel
    {
        public DateTime QueryDate { get; set; }
        public int LocationId { get; set; }
        public DateTime DateofQuery { get; set; }
        public DateTime DateUpdated { get; set; }
        public string UniqueId { get; set; }

        public string page_fan_adds_unique { get; set; }
        public string page_fan_adds { get; set; }
        public string page_fan_removes_unique { get; set; }
        public string page_fan_removes { get; set; }
        public string page_subscriber_adds_unique { get; set; }
        public string page_subscriber_adds { get; set; }
        public string page_subscriber_removes_unique { get; set; }
        public string page_subscriber_removes { get; set; }
        public string page_views_login_unique { get; set; }
        public string page_views_login { get; set; }
        public string page_views_logout { get; set; }
        public string page_views { get; set; }
        public string page_tab_views_login_top_unique { get; set; }
        public string page_tab_views_login_top { get; set; }
        public string page_tab_views_logout_top { get; set; }
        public string page_views_internal_referrals { get; set; }
        public string page_views_external_referrals { get; set; }
        public string page_story_adds_unique { get; set; }
        public string page_story_adds { get; set; }
        public string page_story_adds_by_story_type_unique { get; set; }
        public string page_story_adds_by_story_type { get; set; }
        public string page_impressions_by_age_gender_unique { get; set; }
        public string page_impressions_by_country_unique { get; set; }
        public string page_impressions_by_locale_unique { get; set; }
        public string page_impressions_by_city_unique { get; set; }
        public string page_story_adds_by_age_gender_unique { get; set; }
        public string page_story_adds_by_country_unique { get; set; }
        public string page_story_adds_by_city_unique { get; set; }
        public string page_story_adds_by_locale_unique { get; set; }
        public string page_impressions_unique { get; set; }
        public string page_impressions { get; set; }
        public string page_impressions_paid_unique { get; set; }
        public string page_impressions_paid { get; set; }
        public string page_impressions_organic_unique { get; set; }
        public string page_impressions_organic { get; set; }
        public string page_impressions_viral_unique { get; set; }
        public string page_impressions_viral { get; set; }
        public string page_impressions_by_story_type_unique { get; set; }
        public string page_impressions_by_story_type { get; set; }
        public string page_places_checkin_total { get; set; }
        public string page_places_checkin_total_unique { get; set; }
        public string page_places_checkin_mobile { get; set; }
        public string page_places_checkin_mobile_unique { get; set; }
        public string page_places_checkins_by_age_gender { get; set; }
        public string page_places_checkins_by_country { get; set; }
        public string page_places_checkins_by_city { get; set; }
        public string page_places_checkins_by_locale { get; set; }
        public string page_posts_impressions_unique { get; set; }
        public string page_posts_impressions { get; set; }
        public string page_posts_impressions_paid_unique { get; set; }
        public string page_posts_impressions_paid { get; set; }
        public string page_posts_impressions_organic_unique { get; set; }
        public string page_posts_impressions_organic { get; set; }
        public string page_posts_impressions_viral_unique { get; set; }
        public string page_posts_impressions_viral { get; set; }
        public string page_consumptions_unique { get; set; }
        public string page_consumptions { get; set; }
        public string page_consumptions_by_consumption_type_unique { get; set; }
        public string page_consumptions_by_consumption_type { get; set; }
        public string page_fans_by_like_source_unique { get; set; }
        public string page_fans_by_like_source { get; set; }
        public string page_subscribers_by_subscribe_source_unique { get; set; }
        public string page_subscribers_by_subscribe_source { get; set; }
        public string page_negative_feedback_unique { get; set; }
        public string page_negative_feedback { get; set; }
        public string page_negative_feedback_by_type_unique { get; set; }
        public string page_negative_feedback_by_type { get; set; }
        public string page_fans { get; set; }
        public string page_fans_locale { get; set; }
        public string page_fans_city { get; set; }
        public string page_fans_country { get; set; }
        public string page_fans_gender { get; set; }
        public string page_fans_age { get; set; }
        public string page_fans_gender_age { get; set; }
        public string page_friends_of_fans { get; set; }
        public string page_subscribers { get; set; }
        public string page_subscribers_locale { get; set; }
        public string page_subscribers_city { get; set; }
        public string page_subscribers_country { get; set; }
        public string page_subscribers_gender_age { get; set; }
        public string page_storytellers { get; set; }
        public string page_storytellers_by_story_type { get; set; }
        public string page_storytellers_by_age_gender { get; set; }
        public string page_storytellers_by_country { get; set; }
        public string page_storytellers_by_city { get; set; }
        public string page_storytellers_by_locale { get; set; }
        public string page_engaged_users { get; set; }
        public string page_impressions_frequency_distribution { get; set; }
        public string page_impressions_viral_frequency_distribution { get; set; }
        public string page_posts_impressions_frequency_distribution { get; set; }
        public string page_views_unique { get; set; }
        public string page_stories { get; set; }
        public string page_stories_by_story_type { get; set; }
        public string page_admin_num_posts { get; set; }
        public string page_admin_num_posts_by_type { get; set; }
    }

    public class Value
    {
        public Value()
        {
            end_time = string.Empty;
        }

        [XmlElement(ElementName = "Value")]
        public string _value { get; set; }
        public string end_time { get; set; }

        public object value
        {
            set { _value = value.ToString(); }
            get { return _value; }
        }

    }

    #endregion

    #region FACEBOOK PAGE


    public class FacebookPage
    {
        public FacebookPage()
        {
            facebookResponse = new FacebookResponse();
            responseJson = string.Empty;
            page = new Page();
        }

        [XmlIgnore]
        public string responseJson { get; set; }
        public Page page { get; set; }
        public FacebookResponse facebookResponse { get; set; }
    }

    public class Page
    {
        public Page()
        {
            location = new Location();
        }

        public string name { get; set; }
        public bool is_published { get; set; }
        public string website { get; set; }
        public string username { get; set; }
        public string about { get; set; }
        public Location location { get; set; }
        public string phone { get; set; }
        public bool can_post { get; set; }
        public int checkins { get; set; }
        public int were_here_count { get; set; }
        public int talking_about_count { get; set; }
        public int unread_notif_count { get; set; }
        public int new_like_count { get; set; }
        public bool offer_eligible { get; set; }
        public string category { get; set; }
        public string id { get; set; }
        public string link { get; set; }
        public int likes { get; set; }
        public string influences { get; set; }
        public string keywords { get; set; }
        public string members { get; set; }
        public string parent_page { get; set; }
        public int unread_message_count { get; set; }
        public int unseen_message_count { get; set; }
        public string network { get; set; }
    }

    public class Location
    {
        public Location()
        {

        }

        public string street { get; set; }
        public string city { get; set; }
        public string state { get; set; }
        public string country { get; set; }
        public string zip { get; set; }
        public decimal latitude { get; set; }
        public decimal longitude { get; set; }
    }

    #endregion

    #region FACEBOOK PLACE

    public class FacebookPlace
    {
        public FacebookPlace()
        {
            facebookResponse = new FacebookResponse();
            place = new Place();
        }

        public Place place { get; set; }
        public FacebookResponse facebookResponse { get; set; }
    }

    public class Place
    {
        public Place()
        {
            location = new Location();
        }

        public string id { get; set; }
        public string name { get; set; }
        public string category { get; set; }
        public Location location { get; set; }
    }
    #endregion

    #region FACEBOOK FEED

    public class FacebookFeed
    {
        public FacebookFeed()
        {
            feed = new Feed();
            facebookResponse = new FacebookResponse();
        }

        public Feed feed { get; set; }
        public FacebookResponse facebookResponse { get; set; }
    }

    public class Feed
    {
        public Feed()
        {
            data = new List<DataFeed>();
        }

        public List<DataFeed> data { get; set; }
    }

    public class WorkList
    {
        public WorkList()
        {
            LocationId = 0;
            DealerLocationSocialNetworkId = 0;
            SocialNetworkId = 0;
            UniqueId = string.Empty;
            Token = string.Empty;
            PageFriendlyName = string.Empty;
            TwitterScreenName = string.Empty;
        }

        public int LocationId { get; set; }

        public int DealerLocationSocialNetworkId { get; set; }

        public int SocialNetworkId { get; set; }

        public string UniqueId { get; set; }

        public string Token { get; set; }

        public string PageFriendlyName { get; set; }

        public string TwitterScreenName { get; set; }
    }

    public class DataFeed
    {
        public DataFeed()
        {
        }

        public DateTime created_time { get; set; }
        public string id { get; set; }
        public string object_id { get; set; }
        public string type { get; set; }
        public string message { get; set; }
        public Application application { get; set; }
        public From from { get; set; }
    }
    #endregion

    #region FACEBOOK POST

    //[DataContract(Name = "FacebookPost", Namespace = ""), XmlRoot]
    public class FacebookPost
    {
        public FacebookPost()
        {
            UniqueID = string.Empty;
            Token = string.Empty;
            message = string.Empty;
            caption = string.Empty;
            description = string.Empty;
            name = string.Empty;
            picture = string.Empty;
            link = string.Empty;
            dealerLocationSocialNetworkId = string.Empty;
            source = string.Empty;
            targeting = string.Empty;
        }
        public string UniqueID { get; set; }
        public string Token { get; set; }
        public string message { get; set; }
        public string caption { get; set; }
        public string description { get; set; }
        public string name { get; set; }
        public string picture { get; set; }
        public string link { get; set; }
        public string dealerLocationSocialNetworkId { get; set; }
        public string source { get; set; }
        public string targeting { get; set; }
    }


    #endregion

    #region FACEBOOK COMBINED POST STATISTICS


    [XmlType(TypeName = "FacebookCombinedPostStatistics")]
    public class FacebookCombinedPostStatistics
    {
        public FacebookCombinedPostStatistics()
        {
            facebookPostStatistics = new FacebookPostStatistics();
            facebookInsights = new FacebookInsights();
            //facebookResponse = new FacebookResponse();
        }

        [XmlElement("Statistics")]
        public FacebookPostStatistics facebookPostStatistics { get; set; }
        public FacebookInsights facebookInsights { get; set; }
        public int PostTypeID { get; set; }
        public string ResultID { get; set; }
        public string FeedID { get; set; }
        public string Comment { get; set; }
        public string PostDate { get; set; }

        //public FacebookResponse facebookResponse { get; set; }
    }
    #endregion

    #region FACEBOOK POST INSIGHT STATISTICS

    [XmlType(TypeName = "FacebookPostInsightStatistics")]
    public class PostInsightStatistics
    {
        public PostInsightStatistics()
        {
            data = new List<Datum>();
            paging = new Paging();
        }
        public List<Datum> data { get; set; }
        public Paging paging { get; set; }
        public object value { get; set; }
    }

    public class Datum
    {
        public Datum()
        {
            values = new List<DatumValue>();
        }

        public string id { get; set; }
        public string name { get; set; }
        public string period { get; set; }
        public List<DatumValue> values { get; set; }
        public string title { get; set; }
        public string description { get; set; }
    }

    public class DatumValue
    {
        public DatumValue()
        {
            end_time = string.Empty;
        }
        [XmlElement(ElementName = "Value")]
        public string _value { get; set; }
        public string end_time { get; set; }

        public object value
        {
            set { _value = value.ToString(); }
            get { return _value; }
        }
    }

    public class Paging
    {
        public Paging()
        {
        }

        public string previous { get; set; }
        public string next { get; set; }
    }
    #endregion

    #region FACEBOOK SOCIAL STATISTICS

    public class FacebookSocialStatisticsCollection
    {
        public FacebookSocialStatisticsCollection()
        {
            StatisticsCollection = new List<FacebookSocialStatistic>();
        }

        public List<FacebookSocialStatistic> StatisticsCollection { get; set; }
        public int TotalOrganicImpressions { get; set; }
        public int TotalReach { get; set; }
        public int EngagedUsers { get; set; }
        public int ViralImpressions { get; set; }
        public int Likes { get; set; }
    }

    public class FacebookSocialStatistic
    {
        public FacebookSocialStatistic()
        {

        }

        public string PostDate { get; set; }
        public string PostMessage { get; set; }
        public int TotalOrganicImpressions { get; set; }
        public int TotalReach { get; set; }
        public int EngagedUsers { get; set; }
        public int ViralImpressions { get; set; }
        public int Likes { get; set; }
        public int NumberOfPosts { get; set; }
    }

    #endregion

    public class FacebookObjectIDByResultID
    {
        public FacebookObjectIDByResultID()
        {
            facebookResponse = new FacebookResponse();
            object_id = string.Empty;
            id = string.Empty;
            created_time = DateTime.Now;
        }
        public string object_id { get; set; }
        public string id { get; set; }
        public DateTime created_time { get; set; }
        public FacebookResponse facebookResponse { get; set; }
    }

    public class FacebookPermissions
    {
        public FacebookPermissions()
        {
            permissions = new List<Permissions>();
        }
        public string message { get; set; }
        public List<Permissions> permissions { get; set; }
    }

    public class Permissions
    {
        public Permissions()
        {
            name = string.Empty;
            permission = false;
        }
        public string name { get; set; }
        public bool permission { get; set; }
    }

    public class TokenValidation
    {
        public TokenValidation()
        {
            tokenDetail = new List<TokenDetail>();
        }
        public string message { get; set; }
        public List<TokenDetail> tokenDetail { get; set; }
    }

    public class TokenDetail
    {
        public int DealerLocationID { get; set; }
        public string DealerLocationName { get; set; }
        public int DealerLocationSocialNetWorkID { get; set; }
        public int SocialNetWorkID { get; set; }
        public string SocialNetWorkName { get; set; }
        public bool isValidToken { get; set; }
    }

    public class FacebookPageFeed
    {
        public FacebookPageFeed()
        {
            postStatisticsbyfeedid = new PostStatisticsByFeedID();
            postStatistics = new PostStatistics();
            message = string.Empty;
            feedid = string.Empty;
            objectid = string.Empty;
            application = new Application();
            from = new From();
        }

        [XmlElement("Message")]
        public string message { get; set; }
        [XmlElement("Feedid")]
        public string feedid { get; set; }
        [XmlElement("Objectid")]
        public string objectid { get; set; }
        [XmlElement("Application")]
        public Application application { get; set; }
        [XmlElement("From")]
        public From from { get; set; }
        [XmlElement("FeedIDStatistics")]
        public PostStatisticsByFeedID postStatisticsbyfeedid { get; set; }
        [XmlElement("Statistics")]
        public PostStatistics postStatistics { get; set; }
    }

    public class FacebookPageRSSFeed
    {
        public FacebookPageRSSFeed()
        {
        }

        public string title { get; set; }
        public string link { get; set; }
        public string self { get; set; }
        public DateTime updated { get; set; }
        public string icon { get; set; }
        public List<Entries> entries { get; set; }
    }

    public class Entries
    {
        public Entries()
        {
        }

        public string title { get; set; }
        public string id { get; set; }
        public string alternate { get; set; }
        public DateTime published { get; set; }
        public DateTime updated { get; set; }
        public string verb { get; set; }
        public string target { get; set; }
        public string objects { get; set; }
        public string comments { get; set; }
        public string likes { get; set; }
        public string content { get; set; }

    }

    public class FaceBookPostIdType
    {
        public FaceBookPostIdType()
        {
            postStatistics = new PostStatistics();
            RSSFeedMessage = string.Empty;
        }
        public string PostType { get; set; }
        public string ActionType { get; set; }
        public string PostID { get; set; }
        public string RSSFeedMessage { get; set; }
        [XmlElement("Statistics")]
        public PostStatistics postStatistics { get; set; }
        [XmlElement("FeedIDStatistics")]
        public PostStatisticsByFeedID postStatisticsbyfeedid { get; set; }

    }

    public class FaceBookUserPicture
    {
        public FaceBookUserPicture()
        {
            id = string.Empty;
            picture = new Picture();
        }
        public string id { get; set; }
        public Picture picture { get; set; }

    }

    public class Picture
    {
        public Picture()
        {
            data = new PictureData();
        }

        public PictureData data { get; set; }
    }

    public class PictureData
    {
        public PictureData()
        {
            url = string.Empty;
            is_silhouette = false;
        }

        public string url { get; set; }
        public bool is_silhouette { get; set; }

    }

    #region FACEBOOK SEARCH

    public class SearchData
    {
        public SearchData()
        {

        }
        public List<Search> data { get; set; }
    }

    public class Search
    {
        public Search()
        {
            name = string.Empty;
            id = string.Empty;
        }
        public string name { get; set; }
        public string id { get; set; }
    }

    public class FacebookSearch
    {
        public FacebookSearch()
        {
            data = new Search();
            facebookPagePublic = new FacebookPagePublic();
            DealerLocationID = 0;
            AccountName = string.Empty;
        }
        public string AccountName { get; set; }
        public int DealerLocationID { get; set; }
        public Search data { get; set; }
        public FacebookPagePublic facebookPagePublic { get; set; }
    }

    #endregion

    #region FACEBOOK PAGE

    public class FacebookPagePublic
    {
        public FacebookPagePublic()
        {
            checkins = 0;
            likes = 0;
            link = string.Empty;
            location = new Location();
            name = string.Empty;
            phone = string.Empty;
            talking_about_count = 0;
            username = string.Empty;
            website = string.Empty;
            were_here_count = 0;
        }

        public int checkins { get; set; }
        public int likes { get; set; }
        public string link { get; set; }
        public Location location { get; set; }
        public string name { get; set; }
        public string phone { get; set; }
        public int talking_about_count { get; set; }
        public string username { get; set; }
        public string website { get; set; }
        public int were_here_count { get; set; }

    }

    //public class Location
    //{

    //    public string street { get; set; }
    //    public string city { get; set; }
    //    public string state { get; set; }
    //    public string country { get; set; }
    //    public string zip { get; set; }
    //    public string latitude { get; set; }
    //    public string longitude { get; set; }

    //}

    #endregion

}
