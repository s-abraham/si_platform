﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.Serialization;

namespace Connectors.Entities
{
    public class GooglePlusEntities
    {

    }


    #region General/Authentication

    #region GOOGLEPLUS REQUEST

    public class GooglePlusRequest
    {
        public GooglePlusRequest()
        {
            RequestURL = string.Empty;
            Parameters = string.Empty;
            Method = string.Empty;
            Status = string.Empty;
            Filename = string.Empty;
            Message = string.Empty;
            TimeOut = 0;

        }
        public string RequestURL { get; set; }
        public string Parameters { get; set; }
        public string Method { get; set; }
        public int TimeOut { get; set; }
        public byte[] bytes { get; set; }
        public string Filename { get; set; }
        public string Message { get; set; }
        public string Status { get; set; }
    }

    #endregion

    #region GOOGLEPLUS RESPONSE

    public class GooglePlusErrorResponse
    {
        public GooglePlusErrorResponse()
        {
            error = string.Empty;
            error_description = string.Empty;
            message = string.Empty;

        }

        public string error { get; set; }
        public string error_description { get; set; }
        public string message { get; set; }
    }

    public class GooglePlusResponse
    {
        public GooglePlusResponse()
        {
            error = new GooglePlusError();
            tokenError = new GooglePlusErrorResponse();
        }

        public GooglePlusError error { get; set; }
        public GooglePlusErrorResponse tokenError { get; set; } // Use this to get error from TokenResponse     
    }

    public class GooglePlusError
    {
        public GooglePlusError()
        {
            errors = new List<MoreDetail>();
            code = 0;
            message = string.Empty;
        }

        public List<MoreDetail> errors { get; set; }
        public int code { get; set; }
        public string message { get; set; }

        public override string ToString()
        {
            if (code != 0 || !string.IsNullOrEmpty(message) || errors.Count > 0)
            {
                string str = "code : " + code + " , Message :" + message + " , errors" + errors.ToString();
                return str;
            }
            else
            {
                return string.Empty;
            }


        }
    }

    public class MoreDetail
    {
        public MoreDetail()
        {
            domain = string.Empty;
            reason = string.Empty;
            message = string.Empty;
        }
        public string domain { get; set; }
        public string reason { get; set; }
        public string message { get; set; }

        public override string ToString()
        {
            if (!string.IsNullOrEmpty(domain) || !string.IsNullOrEmpty(reason) || !string.IsNullOrEmpty(message))
            {
                string str = "domain : " + domain + " , reason :" + reason + " , message :" + message;
                return str;
            }
            else
            {
                return string.Empty;
            }
        }
    }


    #endregion

    #region GOOGLE PLUS GENERAL SEARCH PARAMETERS
    public class GooglePlusParams
    {
        public GooglePlusParams()
        {
            name = string.Empty;
            coordinates = string.Empty;
            coordinates = string.Empty;
            radius = string.Empty;
            duration = 0;
            summary = string.Empty;
            Locationtype = string.Empty;
            reference = string.Empty;
            event_id = string.Empty;
            keyword = string.Empty;
            lanuguage = string.Empty;
            rankby = string.Empty;
            types = string.Empty;
            pagetoken = string.Empty;
            client_id = string.Empty;
            client_secret = string.Empty;
            refresh_token = string.Empty;
            grant_type = string.Empty;
            redirect_URL = string.Empty;
            pathToImage = string.Empty;
        }

        public string name { get; set; }

        public string coordinates { get; set; }  // also used for location

        public string radius { get; set; }  // also used for accuracy

        public int duration { get; set; }

        public string summary { get; set; }

        public string Locationtype { get; set; }

        public string reference { get; set; }

        public string event_id { get; set; }

        public string keyword { get; set; }

        public string lanuguage { get; set; }

        public string rankby { get; set; }

        public string types { get; set; }

        public string pagetoken { get; set; }

        public string client_id { get; set; }

        public string client_secret { get; set; }

        public string refresh_token { get; set; }

        public string grant_type { get; set; }

        public string redirect_URL { get; set; }

        public string pathToImage { get; set; }
    }

    #endregion

    #region GOOGLEPLUS GET REFRESH & ACCESS TOKEN RESPONSE
    public class GooglePlusRefreshTokenRequestResponse
    {
        public GooglePlusRefreshTokenRequestResponse()
        {
            errorResponse = new GooglePlusErrorResponse();
            token_type = string.Empty;
            expires_in = 0;
            refresh_token = string.Empty;
            access_token = string.Empty;
        }

        public GooglePlusErrorResponse errorResponse;
        public string token_type { get; set; }
        public int expires_in { get; set; }
        public string refresh_token { get; set; }
        public string access_token { get; set; }
    }

    #endregion

    #region GOOGLEPLUS ACCESS TOKEN RESPONSE

    public class GooglePlusAccessTokenRequestResponse
    {
        public GooglePlusAccessTokenRequestResponse()
        {
            errorResponse = new GooglePlusErrorResponse();
            access_token = string.Empty;
            token_type = string.Empty;
            id_token = string.Empty;
        }

        public GooglePlusErrorResponse errorResponse;
        public string access_token { get; set; }
        public string token_type { get; set; }
        public int expires_in { get; set; }
        public string id_token { get; set; }
    }

    #endregion

    #region GOOGLEPLUS CHECK TOKEN STATUS RESPONSE

    public class GooglePlusTokenStatusResponse
    {
        public GooglePlusTokenStatusResponse()
        {
            errorResponse = new GooglePlusErrorResponse();
            issued_to = string.Empty;
            audience = string.Empty;
            user_id = string.Empty;
            scope = string.Empty;
            expires_in = 0;
            email = string.Empty;
            access_type = string.Empty;
            verified_email = false;
        }

        public GooglePlusErrorResponse errorResponse { get; set; }
        public string issued_to { get; set; }
        public string audience { get; set; }
        public string user_id { get; set; }
        public string scope { get; set; }
        public int expires_in { get; set; }
        public string email { get; set; }
        public bool verified_email { get; set; }
        public string access_type { get; set; }
    }

    #endregion

    #endregion

    #region User/Pages

    #region GOOGLEPLUS FIND PAGE/S RESPONSE

    public class GooglePlusGetPageListResponse
    {
        public GooglePlusGetPageListResponse()
        {
            items = new List<GooglePlusGetPageListResponseItem>();
            error = new GooglePlusError();
            kind = string.Empty;
            etag = string.Empty;
            selfLink = string.Empty;
            title = string.Empty;
            nextPageToken = string.Empty;
        }
        public string kind { get; set; }
        public string etag { get; set; }
        public string selfLink { get; set; }
        public string title { get; set; }
        public string nextPageToken { get; set; }
        public List<GooglePlusGetPageListResponseItem> items { get; set; }
        public GooglePlusError error { get; set; }
    }

    public class GooglePlusGetPageListResponseItem
    {
        public GooglePlusGetPageListResponseItem()
        {
            image = new Image();
            kind = string.Empty;
            etag = string.Empty;
            objectType = string.Empty;
            id = string.Empty;
            displayName = string.Empty;
            url = string.Empty;
        }
        public string kind { get; set; }
        public string etag { get; set; }
        public string objectType { get; set; }
        public string id { get; set; }
        public string displayName { get; set; }
        public string url { get; set; }
        public Image image { get; set; }
    }

    #endregion

    #region GOOGLEPLUS GET USER RESPONSE

    public class GooglePlusGetUserResponse
    {
        public GooglePlusGetUserResponse()
        {
            error = new GooglePlusError();
            id = string.Empty;
            email = string.Empty;
            verified_email = false;
            name = string.Empty;
            given_name = string.Empty;
            family_name = string.Empty;
            link = string.Empty;
            gender = string.Empty;
            birthday = string.Empty;
            locale = string.Empty;
        }
        public GooglePlusError error { get; set; }
        public string id { get; set; }
        public string email { get; set; }
        public bool verified_email { get; set; }
        public string name { get; set; }
        public string given_name { get; set; }
        public string family_name { get; set; }
        public string link { get; set; }
        public string gender { get; set; }
        public string birthday { get; set; }
        public string locale { get; set; }
    }

    #endregion

    #region GOOGLEPLUS GET PAGE RESPONSE

    public class GooglePlusGetSpecificPageResponse
    {
        public GooglePlusGetSpecificPageResponse()
        {
            image = new Image();
            error = new GooglePlusError();
            kind = string.Empty;
            etag = string.Empty;
            objectType = string.Empty;
            displayName = string.Empty;
            tagline = string.Empty;
            aboutMe = string.Empty;
            url = string.Empty;
            plusOneCount = 0;
            circledByCount = 0;
            verified = false;
        }

        public string kind { get; set; }
        public string etag { get; set; }
        public string objectType { get; set; }
        public string id { get; set; }
        public string displayName { get; set; }
        public string tagline { get; set; }
        public string aboutMe { get; set; }
        public string url { get; set; }
        public Image image { get; set; }
        public bool isPlusUser { get; set; }
        public int plusOneCount { get; set; }
        public int circledByCount { get; set; }
        public bool verified { get; set; }
        public GooglePlusError error { get; set; }
    }

    #endregion

    #endregion

    #region Activities/Posts

    #region GOOGLEPLUS ADD/UPDATE ACTIVITY REQUEST

    [DataContract]
    public class GooglePlusAddActivityRequest
    {

        public GooglePlusAddActivityRequest()
        {
            obj = new GooglePlusAddActivityObject();
            access = new GooglePlusAddActivityAccess();
            verb = string.Empty;
        }

        [DataMember(Name = "object")]
        public GooglePlusAddActivityObject obj { get; set; }
        [DataMember(Name = "access")]
        public GooglePlusAddActivityAccess access { get; set; }
        [DataMember(Name = "verb")]
        public string verb { get; set; }
    }

    public class GooglePlusAddActivityObject
    {
        public GooglePlusAddActivityObject()
        {
            attachments = new List<GooglePlusAddActivityAttachment>();
            originalContent = string.Empty;
        }

        public List<GooglePlusAddActivityAttachment> attachments { get; set; }
        public string originalContent { get; set; }
    }

    public class GooglePlusAddActivityAttachment
    {
        public GooglePlusAddActivityAttachment()
        {
            id = string.Empty;
            url = string.Empty;
            objectType = string.Empty;
        }
        public string id { get; set; }
        public string url { get; set; }
        public string objectType { get; set; }
    }

    public class GooglePlusAddActivityAccess
    {
        public GooglePlusAddActivityAccess()
        {
            items = new List<GooglePlusAddActivityItem>();
        }
        public List<GooglePlusAddActivityItem> items { get; set; }
    }

    public class GooglePlusAddActivityItem
    {
        public GooglePlusAddActivityItem()
        {
            type = string.Empty;
            id = string.Empty;
        }

        public string type { get; set; }
        public string id { get; set; }
    }


    #endregion

    #region GOOGLEPLUS ADD/UPDATE/GET SPECIFIC ACTIVITY RESPONSE

    public class GooglePlusAddorUpdateorGetSpecificActivityResponse
    {
        public GooglePlusAddorUpdateorGetSpecificActivityResponse()
        {
            actor = new Actor();
            @object = new Object();
            provider = new Provider();
            access = new Access();
            error = new GooglePlusError();
            kind = string.Empty;
            etag = string.Empty;
            title = string.Empty;
            published = string.Empty;
            updated = string.Empty;
            id = string.Empty;
            url = string.Empty;
            verb = string.Empty;
        }
        public string kind { get; set; }
        public string etag { get; set; }
        public string title { get; set; }
        public string published { get; set; }
        public string updated { get; set; }
        public string id { get; set; }
        public string url { get; set; }
        public Actor actor { get; set; }
        public string verb { get; set; }
        public Object @object { get; set; }
        public Provider provider { get; set; }
        public Access access { get; set; }
        public GooglePlusError error { get; set; }

    }

    #endregion

    #region GOOGLEPLUS GET ACTIVITY LIST RESPONSE

    public class GooglePlusGetActivityListByPageResponse
    {
        public GooglePlusGetActivityListByPageResponse()
        {
            items = new List<ActivityItem>();
            error = new GooglePlusError();
            kind = string.Empty;
            etag = string.Empty;
            nextPageToken = string.Empty;
            title = string.Empty;
        }
        public string kind { get; set; }
        public string etag { get; set; }
        public string nextPageToken { get; set; }
        public string title { get; set; }
        public List<ActivityItem> items { get; set; }
        public GooglePlusError error { get; set; }

    }

    public class ActivityItem
    {
        public ActivityItem()
        {
            actor = new Actor();
            @object = new Object();
            provider = new Provider();
            access = new Access();
            kind = string.Empty;
            etag = string.Empty;
            title = string.Empty;
            published = string.Empty;
            updated = string.Empty;
            id = string.Empty;
            url = string.Empty;
            verb = string.Empty;
        }
        public string kind { get; set; }
        public string etag { get; set; }
        public string title { get; set; }
        public string published { get; set; }
        public string updated { get; set; }
        public string id { get; set; }
        public string url { get; set; }
        public Actor actor { get; set; }
        public string verb { get; set; }
        public Object @object { get; set; }
        public Provider provider { get; set; }
        public Access access { get; set; }
    }

    public class Actor
    {
        public Actor()
        {
            image = new Image();
            id = string.Empty;
            displayName = string.Empty;
            url = string.Empty;
        }
        public string id { get; set; }
        public string displayName { get; set; }
        public string url { get; set; }
        public Image image { get; set; }
    }

    public class Image
    {
        public Image()
        {
            url = string.Empty;

        }
        public string url { get; set; }
    }

    public class Object
    {
        public Object()
        {
            replies = new Replies();
            plusoners = new Plusoners();
            resharers = new Resharers();
            statusForViewer = new StatusForViewer();
            attachments = new List<Attachment>();
            objectType = string.Empty;
            content = string.Empty;
            url = string.Empty;
        }
        public string objectType { get; set; }
        public string content { get; set; }
        public string url { get; set; }
        public Replies replies { get; set; }
        public Plusoners plusoners { get; set; }
        public Resharers resharers { get; set; }
        public StatusForViewer statusForViewer { get; set; }
        public List<Attachment> attachments { get; set; }
    }

    public class Replies
    {
        public Replies()
        {
            totalItems = 0;
            selfLink = string.Empty;
        }

        public int totalItems { get; set; }
        public string selfLink { get; set; }
    }

    public class Plusoners
    {
        public Plusoners()
        {
            totalItems = 0;
            selfLink = string.Empty;
        }

        public int totalItems { get; set; }
        public string selfLink { get; set; }
    }

    public class Resharers
    {
        public Resharers()
        {
            totalItems = 0;
            selfLink = string.Empty;
        }

        public int totalItems { get; set; }
        public string selfLink { get; set; }
    }

    public class StatusForViewer
    {
        public StatusForViewer()
        {
            resharingDisabled = false;
            canComment = false;
            canPlusone = false;
            isPlusOned = false;
        }

        public bool resharingDisabled { get; set; }
        public bool canComment { get; set; }
        public bool canPlusone { get; set; }
        public bool isPlusOned { get; set; }
    }

    public class Attachment
    {
        public Attachment()
        {
            image = new Image2();
            fullImage = new FullImage();
            objectType = string.Empty;
            displayName = string.Empty;
            content = string.Empty;
            url = string.Empty;
            id = string.Empty;
        }
        public string objectType { get; set; }
        public string displayName { get; set; }
        public string content { get; set; }
        public string url { get; set; }
        public Image2 image { get; set; }
        public FullImage fullImage { get; set; }
        public string id { get; set; }
        public Embed embed { get; set; }
    }

    public class Embed
    {
        public Embed()
        {
            url = string.Empty;
            type = string.Empty;
        }
        public string url { get; set; }
        public string type { get; set; }
    }

    public class Image2
    {
        public Image2()
        {
            url = string.Empty;
            type = string.Empty;
            height = 0;
            width = 0;
        }
        public string url { get; set; }
        public string type { get; set; }
        public int height { get; set; }
        public int width { get; set; }
    }

    public class FullImage
    {
        public FullImage()
        {
            url = string.Empty;
            type = string.Empty;
            height = 0;
            width = 0;
        }
        public string url { get; set; }
        public string type { get; set; }
        public int? height { get; set; }
        public int? width { get; set; }
    }

    public class Provider
    {
        public Provider()
        {
            title = string.Empty;
        }
        public string title { get; set; }
    }

    public class Access
    {
        public Access()
        {
            items = new List<Item2>();
            kind = string.Empty;
            description = string.Empty;
        }

        public string kind { get; set; }
        public string description { get; set; }
        public List<Item2> items { get; set; }
    }

    public class Item2
    {
        public Item2()
        {
            type = string.Empty;
        }
        public string type { get; set; }
    }

    #endregion

    #region GOOGLEPLUS GET ACTIVITY LIST BY CIRCLE RESPONSE

    public class GooglePlusGetActivityListByCircleResponse
    {
        public GooglePlusGetActivityListByCircleResponse()
        {
            items = new List<GooglePlusGetActivityListByCircleResponseItem>();
            error = new GooglePlusError();
            kind = string.Empty;
            etag = string.Empty;
            title = string.Empty;
            nextPageToken = string.Empty;
        }

        public string kind { get; set; }
        public string etag { get; set; }
        public string nextPageToken { get; set; }
        public string title { get; set; }
        public List<GooglePlusGetActivityListByCircleResponseItem> items { get; set; }
        public GooglePlusError error { get; set; }
    }

    public class GooglePlusGetActivityListByCircleResponseItem
    {
        public GooglePlusGetActivityListByCircleResponseItem()
        {
            actor = new Actor();
            @object = new Object();
            provider = new Provider();
            access = new Access();
            kind = string.Empty;
            etag = string.Empty;
            title = string.Empty;
            published = string.Empty;
            updated = string.Empty;
            id = string.Empty;
            url = string.Empty;
            verb = string.Empty;
            annotation = string.Empty;
        }

        public string kind { get; set; }
        public string etag { get; set; }
        public string title { get; set; }
        public string published { get; set; }
        public string updated { get; set; }
        public string id { get; set; }
        public string url { get; set; }
        public Actor actor { get; set; }
        public string verb { get; set; }
        public Object @object { get; set; }
        public string annotation { get; set; }
        public Provider provider { get; set; }
        public Access access { get; set; }
    }


    #endregion

    #endregion

    #region Comments

    #region GOOGLEPLUS ADD/UPDATE REQUEST

    [DataContract]
    public class GooglePlusAddCommentRequest
    {

        public GooglePlusAddCommentRequest()
        {
            obj = new GooglePlusAddCommentObject();
        }
        [DataMember(Name = "object")]
        public GooglePlusAddCommentObject obj { get; set; }
    }

    public class GooglePlusAddCommentObject
    {
        public GooglePlusAddCommentObject()
        {
            originalContent = string.Empty;
        }
        public string originalContent { get; set; }
    }

    #endregion

    #region GOOGLEPLUS ADD/UPDATE/GET SPECIFIC COMMENT RESPONSE

    public class GooglePlusAddorUpdateOrGetSpecificCommentResponse
    {
        public GooglePlusAddorUpdateOrGetSpecificCommentResponse()
        {
            actor = new Actor();
            @object = new AddCommentResponseObject();
            inReplyTo = new List<InReplyTo>();
            plusoners = new CommentsPlusoners();
            error = new GooglePlusError();
            kind = string.Empty;
            etag = string.Empty;
            verb = string.Empty;
            id = string.Empty;
            published = string.Empty;
            updated = string.Empty;
            selfLink = string.Empty;
        }

        public string kind { get; set; }
        public string etag { get; set; }
        public string verb { get; set; }
        public string id { get; set; }
        public string published { get; set; }
        public string updated { get; set; }
        public Actor actor { get; set; }
        public AddCommentResponseObject @object { get; set; }
        public string selfLink { get; set; }
        public List<InReplyTo> inReplyTo { get; set; }
        public CommentsPlusoners plusoners { get; set; }
        public GooglePlusError error { get; set; }
    }

    public class InReplyTo
    {
        public InReplyTo()
        {
            id = string.Empty;
            url = string.Empty;
        }

        public string id { get; set; }
        public string url { get; set; }
    }

    public class AddCommentResponseObject
    {
        public AddCommentResponseObject()
        {
            objectType = string.Empty;
            content = string.Empty;
            originalContent = string.Empty;
        }

        public string objectType { get; set; }
        public string content { get; set; }
        public string originalContent { get; set; }
    }

    public class CommentsPlusoners
    {
        public CommentsPlusoners()
        {
            totalItems = 0;
        }

        public int totalItems { get; set; }
    }

    #endregion

    #region GOOGLEPLUS GET COMMENTS LIST RESPONSE

    public class GooglePlusGetCommentsListResponse
    {
        public GooglePlusGetCommentsListResponse()
        {
            items = new List<GooglePlusGetCommentsListItem>();
            error = new GooglePlusError();
            kind = string.Empty;
            etag = string.Empty;
            title = string.Empty;
        }

        public string kind { get; set; }
        public string etag { get; set; }
        public string title { get; set; }
        public List<GooglePlusGetCommentsListItem> items { get; set; }
        public GooglePlusError error { get; set; }
    }

    public class GooglePlusGetCommentsListItem
    {
        public GooglePlusGetCommentsListItem()
        {
            actor = new Actor();
            @object = new CommentsListObject();
            inReplyTo = new List<InReplyTo>();
            plusoners = new CommentsPlusoners();
            kind = string.Empty;
            etag = string.Empty;
            verb = string.Empty;
            id = string.Empty;
            published = string.Empty;
            updated = string.Empty;
            selfLink = string.Empty;
        }
        public string kind { get; set; }
        public string etag { get; set; }
        public string verb { get; set; }
        public string id { get; set; }
        public string published { get; set; }
        public string updated { get; set; }
        public Actor actor { get; set; }
        public CommentsListObject @object { get; set; }
        public string selfLink { get; set; }
        public List<InReplyTo> inReplyTo { get; set; }
        public CommentsPlusoners plusoners { get; set; }

    }

    public class CommentsListObject
    {
        public CommentsListObject()
        {
            objectType = string.Empty;
            content = string.Empty;
        }
        public string objectType { get; set; }
        public string content { get; set; }
    }


    #endregion

    #endregion

    #region Circles

    #region GOOGLEPLUS CREATE/UPDATE CIRCLE REQUEST

    public class GooglePlusCreateorUpdateCircleRequest
    {
        public GooglePlusCreateorUpdateCircleRequest()
        {
            displayName = string.Empty;
        }
        public string displayName { get; set; }
    }

    #endregion

    #region GOOGLEPLUS CREATE/UPDATE/GET SPECIFIC CIRCLE RESPONSE

    public class GooglePlusCreateOrUpdateOrGetSpecificCircleResponse
    {
        public GooglePlusCreateOrUpdateOrGetSpecificCircleResponse()
        {
            people = new People();
            error = new GooglePlusError();
            kind = string.Empty;
            id = string.Empty;
            etag = string.Empty;
            displayName = string.Empty;
            description = string.Empty;
            selfLink = string.Empty;
        }
        public string kind { get; set; }
        public string id { get; set; }
        public string etag { get; set; }
        public string displayName { get; set; }
        public string description { get; set; }
        public People people { get; set; }
        public string selfLink { get; set; }
        public GooglePlusError error { get; set; }
    }

    #endregion

    #region GOOGLEPLUS GET CIRCLES LIST BY PAGE RESPONSE

    public class GooglePlusGetCirclesListResponse
    {
        public GooglePlusGetCirclesListResponse()
        {
            items = new List<CirclesItem>();
            error = new GooglePlusError();
            kind = string.Empty;
            etag = string.Empty;
            title = string.Empty;
            selfLink = string.Empty;
            totalItems = 0;
        }
        public string kind { get; set; }
        public string etag { get; set; }
        public List<CirclesItem> items { get; set; }
        public string title { get; set; }
        public string selfLink { get; set; }
        public int totalItems { get; set; }
        public GooglePlusError error { get; set; }
    }

    public class CirclesItem
    {
        public CirclesItem()
        {
            people = new People();
            error = new GooglePlusError();
            kind = string.Empty;
            id = string.Empty;
            etag = string.Empty;
            displayName = string.Empty;
            description = string.Empty;
            selfLink = string.Empty;

        }
        public string kind { get; set; }
        public string id { get; set; }
        public string etag { get; set; }
        public string displayName { get; set; }
        public string description { get; set; }
        public People people { get; set; }
        public string selfLink { get; set; }
        public GooglePlusError error { get; set; }
    }

    public class People
    {
        public People()
        {
            totalItems = 0;
        }
        public int totalItems { get; set; }
    }
    #endregion

    #region GOOGLEPLUS GET MEMBER LIST FOR CHOSEN CIRCLE RESPONSE

    public class GooglePlusGetMembersListResponse
    {
        public GooglePlusGetMembersListResponse()
        {
            items = new List<GooglePlusGetMembersListItem>();
            error = new GooglePlusError();
            kind = string.Empty;
            etag = string.Empty;
            title = string.Empty;
            totalItems = 0;
        }
        public string kind { get; set; }
        public string etag { get; set; }
        public string title { get; set; }
        public int totalItems { get; set; }
        public List<GooglePlusGetMembersListItem> items { get; set; }
        public GooglePlusError error { get; set; }
    }

    public class GooglePlusGetMembersListItem
    {
        public GooglePlusGetMembersListItem()
        {
            image = new Image();
            kind = string.Empty;
            etag = string.Empty;
            objectType = string.Empty;
            displayName = string.Empty;
            url = string.Empty;
        }
        public string kind { get; set; }
        public string etag { get; set; }
        public string objectType { get; set; }
        public string id { get; set; }
        public string displayName { get; set; }
        public string url { get; set; }
        public Image image { get; set; }
    }

    #endregion

    #endregion

    #region Media

    #region GOOGLEPLUS UPLOAD PHOTO RESPONSE

    public class GooglePlusUploadPhotoResponse
    {
        public GooglePlusUploadPhotoResponse()
        {
            author = new Author();
            statusForViewer = new AddPhotoStatusForViewer();
            error = new GooglePlusError();
            kind = string.Empty;
            etag = string.Empty;
            id = string.Empty;
            published = string.Empty;
            height = 0;
            width = 0;
            mediaUrl = string.Empty;
            sizeBytes = string.Empty;
        }

        public string kind { get; set; }
        public string etag { get; set; }
        public string id { get; set; }
        public string published { get; set; }
        public Author author { get; set; }
        public int height { get; set; }
        public int width { get; set; }
        public string mediaUrl { get; set; }
        public string sizeBytes { get; set; }
        public AddPhotoStatusForViewer statusForViewer { get; set; }
        public GooglePlusError error { get; set; }
    }

    public class AddPhotoStatusForViewer
    {
        public AddPhotoStatusForViewer()
        {
            canComment = false;
            canPlusone = false;
        }
        public bool canComment { get; set; }
        public bool canPlusone { get; set; }
    }


    #endregion

    #region GOOGLEPLUS GET SPECIFIC MEDIA RESPONSE

    public class GooglePlusGetSpecificMediaResponse
    {
        public GooglePlusGetSpecificMediaResponse()
        {
            author = new Author();
            exif = new Exif();
            statusForViewer = new GetMediaStatusForViewer();
            error = new GooglePlusError();
            kind = string.Empty;
            etag = string.Empty;
            id = string.Empty;
            published = string.Empty;
            url = string.Empty;
            height = 0;
            width = 0;
            mediaUrl = string.Empty;
            albumId = string.Empty;
            sizeBytes = string.Empty;
        }
        public string kind { get; set; }
        public string etag { get; set; }
        public string id { get; set; }
        public string published { get; set; }
        public Author author { get; set; }
        public string url { get; set; }
        public int height { get; set; }
        public int width { get; set; }
        public string mediaUrl { get; set; }
        public string albumId { get; set; }
        public Exif exif { get; set; }
        public string sizeBytes { get; set; }
        public GetMediaStatusForViewer statusForViewer { get; set; }
        public GooglePlusError error { get; set; }
    }

    public class GetMediaStatusForViewer
    {
        public GetMediaStatusForViewer()
        {
            resharingDisabled = false;
            canComment = false;
            canPlusone = false;
        }
        public bool resharingDisabled { get; set; }
        public bool canComment { get; set; }
        public bool canPlusone { get; set; }

    }

    public class Exif
    {
        public Exif()
        {
            time = string.Empty; ;
        }
        public string time { get; set; }
    }

    public class Author
    {
        public Author()
        {
            id = string.Empty;
            displayName = string.Empty;
            url = string.Empty;
        }
        public string id { get; set; }
        public string displayName { get; set; }
        public string url { get; set; }
        public Image image { get; set; }
    }


    #endregion

    #endregion

    #region Places/Events

    #region GOOGLEPLUS PLACE SEARCH RESPONSE

    public class GooglePlusPlaceSearchResponse
    {
        public GooglePlusPlaceSearchResponse()
        {

            results = new List<PlaceSearchResult>();
            html_attributions = new List<object>();
            error = new GooglePlusError();
            next_page_token = string.Empty;
            status = string.Empty;
        }
        public List<object> html_attributions { get; set; }
        public string next_page_token { get; set; }
        public List<PlaceSearchResult> results { get; set; }
        public string status { get; set; }
        public GooglePlusError error { get; set; }
    }

    public class PlaceSearchResult
    {
        public PlaceSearchResult()
        {
            geometry = new Geometry();
            opening_hours = new OpeningHours();
            icon = string.Empty;
            id = string.Empty;
            name = string.Empty;
            reference = string.Empty;
            vicinity = string.Empty;
            rating = 0.0;
        }
        public Geometry geometry { get; set; }
        public string icon { get; set; }
        public string id { get; set; }
        public string name { get; set; }
        public string reference { get; set; }
        public List<string> types { get; set; }
        public string vicinity { get; set; }
        public OpeningHours opening_hours { get; set; }
        public double? rating { get; set; }
    }

    public class Geometry
    {
        public Geometry()
        {
            location = new GooglePlusLocation();
        }
        public GooglePlusLocation location { get; set; }
    }

    public class GooglePlusLocation
    {
        public GooglePlusLocation()
        {
            lat = 0.0;
            lng = 0.0;
        }
        public double lat { get; set; }
        public double lng { get; set; }
    }

    public class OpeningHours
    {
        public OpeningHours()
        {
            open_now = false;
        }
        public bool open_now { get; set; }
    }

    #endregion

    #region GOOGLEPLUS PLACE DETAIL RESPONSE

    public class GooglePlusPlaceDetailResponse
    {
        public GooglePlusPlaceDetailResponse()
        {
            result = new GooglePlaceDetailResult();
            html_attributions = new List<object>();
            error = new GooglePlusError();
            status = string.Empty;
        }
        public List<object> html_attributions { get; set; }
        public GooglePlaceDetailResult result { get; set; }
        public string status { get; set; }
        public GooglePlusError error { get; set; }
    }

    public class GooglePlaceDetailResult
    {
        public GooglePlaceDetailResult()
        {
            address_components = new List<AddressComponent>();
            geometry = new Geometry();
            opening_hours = new PlaceDetailOpeningHours();
            reviews = new List<Review>();
            formatted_address = string.Empty;
            formatted_phone_number = string.Empty;
            icon = string.Empty;
            id = string.Empty;
            international_phone_number = string.Empty;
            name = string.Empty;
            reference = string.Empty;
            url = string.Empty;
            utc_offset = 0;
            vicinity = string.Empty;
            website = string.Empty;
        }

        public List<AddressComponent> address_components { get; set; }
        public string formatted_address { get; set; }
        public string formatted_phone_number { get; set; }
        public Geometry geometry { get; set; }
        public string icon { get; set; }
        public string id { get; set; }
        public string international_phone_number { get; set; }
        public string name { get; set; }
        public PlaceDetailOpeningHours opening_hours { get; set; }
        public string reference { get; set; }
        public List<Review> reviews { get; set; }
        public List<string> types { get; set; }
        public string url { get; set; }
        public int utc_offset { get; set; }
        public string vicinity { get; set; }
        public string website { get; set; }
    }

    public class AddressComponent
    {
        public AddressComponent()
        {
            long_name = string.Empty;
            short_name = string.Empty;
            types = new List<string>();
        }
        public string long_name { get; set; }
        public string short_name { get; set; }
        public List<string> types { get; set; }
    }

    public class Review
    {
        public Review()
        {
            aspects = new List<Aspect>();
            author_name = string.Empty;
            text = string.Empty;
            time = 0;
        }
        public List<Aspect> aspects { get; set; }
        public string author_name { get; set; }
        public string text { get; set; }
        public int time { get; set; }
    }

    public class Aspect
    {
        public Aspect()
        {
            rating = 0;
            type = string.Empty;
        }
        public int rating { get; set; }
        public string type { get; set; }
    }

    public class PlaceDetailOpeningHours
    {
        public PlaceDetailOpeningHours()
        {
            periods = new List<Period>();
            open_now = false;
        }
        public bool open_now { get; set; }
        public List<Period> periods { get; set; }
    }

    public class Period
    {
        public Period()
        {
            close = new Close();
            open = new Open();
        }
        public Close close { get; set; }
        public Open open { get; set; }
    }

    public class Close
    {
        public Close()
        {
            day = 0;
            time = string.Empty;
        }
        public int day { get; set; }
        public string time { get; set; }
    }

    public class Open
    {
        public Open()
        {
            day = 0;
            time = string.Empty;
        }
        public int day { get; set; }
        public string time { get; set; }
    }
    #endregion

    #region GOOGLEPLUS PLACE ADD REQUEST

    public class GooglePlusAddPlaceRequest
    {
        public GooglePlusAddPlaceRequest()
        {
            location = new GooglePlusLocation();
            types = new List<string>();
            accuracy = 0;
            name = string.Empty;
        }
        public GooglePlusLocation location { get; set; }
        public int accuracy { get; set; }
        public string name { get; set; }
        public List<string> types { get; set; }
    }

    #endregion

    #region GOOGLEPLUS PLACE ADD RESPONSE

    public class GooglePlusAddPlaceResponse
    {
        public GooglePlusAddPlaceResponse()
        {
            error = new GooglePlusError();
            id = string.Empty;
            reference = string.Empty;
            status = string.Empty;
        }
        public string id { get; set; }
        public string reference { get; set; }
        public string status { get; set; }
        public GooglePlusError error { get; set; }
    }

    #endregion

    #region GOOGLEPLUS PLACE DELETE REQUEST

    public class GooglePlusDeletePlaceRequest
    {
        public GooglePlusDeletePlaceRequest()
        {
            reference = string.Empty;
        }
        public string reference { get; set; }
    }

    #endregion

    #region GOOGLEPLUS PLACE DELETE RESPONSE

    public class GooglePlusDeletePlaceResponse
    {
        public GooglePlusDeletePlaceResponse()
        {
            status = string.Empty;
        }
        public string status { get; set; }
    }

    #endregion

    #region GOOGLEPLUS EVENT ADD REQUEST
    public class GooglePlusEventAddRequest
    {
        public GooglePlusEventAddRequest()
        {
            duration = 0;
            reference = string.Empty;
            summary = string.Empty;
            url = string.Empty;
        }
        public int duration { get; set; }
        public string reference { get; set; }
        public string summary { get; set; }
        public string url { get; set; }
    }

    #endregion

    #region GOOGLEPLUS EVENT ADD RESPONSE

    public class GooglePlusAddEventResponse
    {
        public GooglePlusAddEventResponse()
        {
            error = new GooglePlusError();
            status = string.Empty;
            event_id = string.Empty;
        }
        public string status { get; set; }
        public string event_id { get; set; }
        public GooglePlusError error { get; set; }

    }
    #endregion

    #region GOOGLEPLUS EVENT DELETE REQUEST

    public class GooglePlusEventDeleteRequest
    {
        public GooglePlusEventDeleteRequest()
        {
            reference = string.Empty;
            event_id = string.Empty;
        }

        public string reference { get; set; }
        public string event_id { get; set; }
    }
    #endregion

    #region GOOGLEPLUS EVENT DELETE RESPONSE

    public class GooglePlusEventDeleteResponse
    {
        public GooglePlusEventDeleteResponse()
        {
            status = string.Empty;
        }
        public string status { get; set; }
    }

    #endregion

    #region GOOGLEPLUS EVENT DETAIL RESPONSE

    public class GooglePlusEventDetailResponse
    {
        public GooglePlusEventDetailResponse()
        {
            result = new GooglePlusEventDetailResponseResult();
            html_attributions = new List<object>();
            error = new GooglePlusError();
            status = string.Empty;
        }
        public List<object> html_attributions { get; set; }
        public GooglePlusEventDetailResponseResult result { get; set; }
        public string status { get; set; }
        public GooglePlusError error { get; set; }

    }

    public class GooglePlusEventDetailResponseResult
    {
        public GooglePlusEventDetailResponseResult()
        {
            events = new List<GooglePlusEvent>();
            geometry = new Geometry();
            icon = string.Empty;
            id = string.Empty;
            name = string.Empty;
            reference = string.Empty;
        }

        public List<GooglePlusEvent> events { get; set; }
        public Geometry geometry { get; set; }
        public string icon { get; set; }
        public string id { get; set; }
        public string name { get; set; }
        public string reference { get; set; }
        public List<string> types { get; set; }
    }

    public class GooglePlusEvent
    {
        public GooglePlusEvent()
        {
            event_id = string.Empty;
            scope = string.Empty;
            start_time = 0;
            summary = string.Empty;
            url = string.Empty;
        }
        public string event_id { get; set; }
        public string scope { get; set; }
        public int start_time { get; set; }
        public string summary { get; set; }
        public string url { get; set; }
    }

    #endregion

    #endregion

    #region Statistics

    #region GOOGLEPLUS STATISTICS PLUSONERS

    public class GooglePlusPlusonsersResponse
    {
        public GooglePlusPlusonsersResponse()
        {
            error = new GooglePlusError();
            kind = string.Empty;
            etag = string.Empty;
            title = string.Empty;
        }
        public string kind { get; set; }
        public string etag { get; set; }
        public string title { get; set; }
        public List<GooglePlusStatisticsResponseItem> items { get; set; }
        public GooglePlusError error { get; set; }
    }

    public class GooglePlusStatisticsResponseItem
    {
        public GooglePlusStatisticsResponseItem()
        {
            image = new Image();
            kind = string.Empty;
            etag = string.Empty;
            id = string.Empty;
            displayName = string.Empty;
            url = string.Empty;
        }
        public string kind { get; set; }
        public string etag { get; set; }
        public string id { get; set; }
        public string displayName { get; set; }
        public string url { get; set; }
        public Image image { get; set; }
    }

    #endregion

    #region GOOGLEPLUS STATISTICS RESHARERS

    public class GooglePlusResharersResponse
    {
        public GooglePlusResharersResponse()
        {
            items = new List<GooglePlusStatisticsResponseItem>();
            error = new GooglePlusError();
            kind = string.Empty;
            etag = string.Empty;
            title = string.Empty;
            totalItems = 0;
        }
        public string kind { get; set; }
        public string etag { get; set; }
        public string title { get; set; }
        public int totalItems { get; set; }
        public List<GooglePlusStatisticsResponseItem> items { get; set; }
        public GooglePlusError error { get; set; }
    }
    #endregion

    #endregion

    #region FinalPostResults
    public class GooglePlusPostResponse
    {

        public GooglePlusPostResponse()
        {
            finalActivityresponse = new GooglePlusAddorUpdateorGetSpecificActivityResponse();
            finalCommentresponse = new GooglePlusAddorUpdateOrGetSpecificCommentResponse();
            error = new GooglePlusError();
            IsSuccess = false;
            postURL = string.Empty;
        }

        public string postURL { get; set; }
        public bool IsSuccess { get; set; }
        public GooglePlusError error { get; set; }
        public GooglePlusAddorUpdateorGetSpecificActivityResponse finalActivityresponse { get; set; }
        public GooglePlusAddorUpdateOrGetSpecificCommentResponse finalCommentresponse { get; set; }

    }



    #endregion
}
