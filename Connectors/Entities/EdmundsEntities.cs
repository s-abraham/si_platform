﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Connectors.Entities
{
    public class EdmundsEntities: LookupsEntities
    {
        public class DealerList
        {
            public DealerList()
            {
                dealerHolder = new List<DealerObject>();
                ConnectionStatus = new SIConnection.ConnectionStatus();
            }

            public List<DealerObject> dealerHolder { get; set; }
            public SIConnection.ConnectionStatus ConnectionStatus { get; set; }
        }

        public class DealerObject
        {
            public DealerObject()
            {
                address = new Address();
                operations = new Operations();
                contactinfo = new Contactinfo();
            }

            public string id { get; set; }
            public string locationId { get; set; }
            public Address address { get; set; }
            public string name { get; set; }
            public string logicalName { get; set; }
            public string type { get; set; }
            public string make { get; set; }
            public Operations operations { get; set; }
            public Contactinfo contactinfo { get; set; }
            public string publishDate { get; set; }
            public bool active { get; set; }
            public string ppStatus { get; set; }
            public string syncPublishDate { get; set; }
        }

        public class Contactinfo
        {
            public string dealer_website { get; set; }
            public string email_address { get; set; }
            public string phone { get; set; }
        }

        public class Operations
        {
            public string Wednesday { get; set; }
            public string Tuesday { get; set; }
            public string Thursday { get; set; }
            public string Saturday { get; set; }
            public string Friday { get; set; }
            public string Monday { get; set; }
            public string Sunday { get; set; }
        }

        public class Address
        {
            public string street { get; set; }
            public string apartment { get; set; }
            public string city { get; set; }
            public string stateCode { get; set; }
            public string stateName { get; set; }
            public string county { get; set; }
            public string country { get; set; }
            public string zipcode { get; set; }
            public double latitude { get; set; }
            public double longitude { get; set; }
        }

        public class SalesReviews
        {
            public SalesReviews()
            {
                comments = new List<EdmundsComment>();
            }

            #region PROPERTIES

            public decimal averageRating { get; set; }
            public string title { get; set; }
            public string reviewBody { get; set; }
            public string consumerName { get; set; }
            public DateTime date { get; set; }
            public decimal totalRating { get; set; }
            public string recommendedDealer { get; set; }
            public List<EdmundsComment> comments { get; set; }

            #endregion
        }

        public class ServiceReviews
        {
            public ServiceReviews()
            {
                comments = new List<EdmundsComment>();
            }


            #region PROPERTIES

            public decimal averageRating { get; set; }
            public string title { get; set; }
            public string reviewBody { get; set; }
            public string consumerName { get; set; }
            public DateTime date { get; set; }
            public decimal totalRating { get; set; }
            public string recommendedDealer { get; set; }
            public List<EdmundsComment> comments { get; set; }

            #endregion
        }

        public class EdmundsComment
        {
            public DateTime date { get; set; }
            public string body { get; set; }
        }
        public class Edmunds
        {
            #region PROPERTIES

            public int dealerId { get; set; }
            public string dealerName { get; set; }
            public Address dealerAddress { get; set; }
            public Contactinfo dealerContactInfo { get; set; }
            public List<SalesReviews> salesReviews { get; set; }
            public List<ServiceReviews> serviceReviews { get; set; }

            #endregion
        }
    }
}
