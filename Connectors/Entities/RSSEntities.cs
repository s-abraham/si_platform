﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Connectors.Entities
{
    public class RSSEntities
    {
        public RSSEntities()
        {
            RSSItems = new List<RSSItem>();
        }

        public List<RSSItem> RSSItems { get; set; }
        public bool Success { get; set; }
        public long RSSFeedID { get; set; }
        public string RSSUrl { get; set; }
    }

    public class RSSItem
    {
        public RSSItem()
        {
            Images = new List<string>();
            Categories = new List<string>();
        }

        public string Title { get; set; }
        public string Link { get; set; }
        public string PermaLink { get; set; }
        public string Summary { get; set; }
        public string SummaryAsText { get; set; }
        public DateTime PubDate { get; set; }
        public List<string> Images { get; set; }
        public List<string> Categories { get; set; }
        public string Thumbnail { get; set; }
        public string SourceRSS { get; set; }
        public string Hash { get; set; }
    }
}

