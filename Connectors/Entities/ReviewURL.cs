﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Connectors.Entities
{
    [Serializable, DataContract]
    public class ReviewURL
    {
        [DataMember(Order = 1)]
        public int ReviewSourceID { get; set; }
        [DataMember(Order = 2)]
        public string ReviewSourceName { get; set; }
        [DataMember(Order = 3)]
        public string URL { get; set; }

    }
}
