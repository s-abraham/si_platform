﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Connectors.Entities
{
    public enum SocialNetworks
    {
        All = 0,
        FacebookOnly = 2,
        TwitterOnly = 3,
        LinkedInOnly = 4,
        YouTubeOnly = 5
    }

    public enum FacebookActionType
    {
        Like = 1,
        Comment = 2,
        Share = 3,
        NewPost = 4,
        StatusLike = 5,
        PageLike = 6,
        CommentonUserWallPost = 7
    }

    public enum PostType
    {
        FacebookStatus = 1,
        FacebookPhoto = 2,
        FacebookLink = 3,
        FacebookEvent = 4       
    }
}
