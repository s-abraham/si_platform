﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using Connectors.Parameters;
using HtmlAgilityPack;
using SI;
using System.Threading;

namespace Connectors
{
    public abstract class SIConnection : IDisposable
    {

        #region Definitions


        public enum ConnectionStatus
        {
            Success = 1,
            WebException = 2,
            InvalidData = 3,
            Timeout = 4
        }


        #endregion

        #region Properties
        /// <summary>
        /// Raw results from Http call
        /// </summary>
        public string HttpRequestResults { get; set; }

        /// <summary>
        /// If a Post then the post data
        /// </summary>
        public Dictionary<String, String> PostData { get; set; }


        public string ExceptionMessage { get; set; }
        public List<string> FailedXPaths { get; set; }

        #endregion

        private SInet _WebNet = null;
        private SIConnectionParameters ConnectionParameters;

        protected SIConnection(SIConnectionParameters siConnectionParameters)
        {
            ConnectionParameters = siConnectionParameters;
            PostData = new Dictionary<string, string>();
            FailedXPaths = new List<string>();
        }

        /// <summary>
        /// Http request to get raw JSON results from requested URL
        /// </summary>
        /// <param name="requestUrl">API URL to request</param>
        /// <returns>populates JSONResults</returns>
        protected ConnectionStatus ConnectorHttpRequest(string requestUrl, string userAgent = null, CookieContainer cookie = null)
        {
            try
            {
                if (_WebNet == null)
                    _WebNet = new SInet(ConnectionParameters.UseProxy, ConnectionParameters.MaxRedirects, null, ConnectionParameters.TimeoutSeconds * 1000);

                if (!string.IsNullOrWhiteSpace(userAgent))
                {
                    _WebNet.UserAgent = userAgent;
                }

                if (cookie != null)
                {
                    _WebNet.Cookie = cookie;
                }

                if (PostData.Count == 0)
                    HttpRequestResults = _WebNet.DownloadString(requestUrl, null);
                else
                    HttpRequestResults = _WebNet.DownloadString(requestUrl, PostData);

            }
            catch (WebException webEx)
            {
                if (webEx.Response != null)
                {
                    if (webEx.Response.Headers != null)
                    {
                        ExceptionMessage = ExceptionMessage + webEx.Response.Headers.GetKey(0).ToString() + " : " + webEx.Response.Headers.GetValues(0)[0].ToString();
                    }
                }

                ExceptionMessage = ExceptionMessage + " " + webEx.Message;
                
                if (webEx.Message.Contains("timed out"))
                    return ConnectionStatus.Timeout;
                return ConnectionStatus.WebException;
            }

            return ConnectionStatus.Success;
        }

        protected HtmlDocument ConnectorHtmlDocument(string requestUrl, string userAgent = null)
        {
            try
            {
                if (_WebNet == null)
                    _WebNet = new SInet(ConnectionParameters.UseProxy, ConnectionParameters.MaxRedirects, null, ConnectionParameters.TimeoutSeconds * 1000);

                if (!string.IsNullOrWhiteSpace(userAgent))
                {
                    _WebNet.UserAgent = userAgent;
                }

                if (PostData.Count == 0)
                    return _WebNet.DownloadDocument(requestUrl, null);
                return _WebNet.DownloadDocument(requestUrl, PostData);
            }
            catch (WebException webEx)
            {
                if (webEx.Response != null)
                {
                    if (webEx.Response.Headers != null)
                    {
                        ExceptionMessage = ExceptionMessage + webEx.Response.Headers.GetKey(0).ToString() + " : " + webEx.Response.Headers.GetValues(0)[0].ToString();                         
                    }
                }

                ExceptionMessage = ExceptionMessage + " " + webEx.Message;

            }
            return null;
        }

        protected string DownloadString(string requestUrl, string httpMethod, string contentType, byte[] requestStreamData, string userAgent = null)
        {
            try
            {
                if (_WebNet == null)
                    _WebNet = new SInet(ConnectionParameters.UseProxy, ConnectionParameters.MaxRedirects, null, ConnectionParameters.TimeoutSeconds * 1000);

                if (!string.IsNullOrWhiteSpace(userAgent))
                {
                    _WebNet.UserAgent = userAgent;
                }

                return _WebNet.DownloadString(requestUrl, httpMethod, contentType, requestStreamData);

            }
            catch (WebException webEx)
            {
                if (webEx.Response != null)
                {
                    if (webEx.Response.Headers != null)
                    {
                        ExceptionMessage = ExceptionMessage + webEx.Response.Headers.GetKey(0).ToString() + " : " + webEx.Response.Headers.GetValues(0)[0].ToString();
                    }
                }

                ExceptionMessage = ExceptionMessage + " " + webEx.Message;
            }
            return null;
        }

        protected string DownloadString(string requestUrl, Dictionary<string, string> postVars)
        {
            try
            {
                if (_WebNet == null)
                    _WebNet = new SInet(ConnectionParameters.UseProxy, ConnectionParameters.MaxRedirects, null, ConnectionParameters.TimeoutSeconds * 1000);

                return _WebNet.DownloadString(requestUrl, postVars);

            }
            catch (WebException webEx)
            {
                if (webEx.Response != null)
                {
                    if (webEx.Response.Headers != null)
                    {
                        ExceptionMessage = ExceptionMessage + webEx.Response.Headers.GetKey(0).ToString() + " : " + webEx.Response.Headers.GetValues(0)[0].ToString();
                    }
                }

                ExceptionMessage = ExceptionMessage + " " + webEx.Message;
            }
            return null;
        }

        protected HtmlDocument DownloadDocument(string requestUrl, Dictionary<string, string> postVars, int numberOfAttempts = 1, int millisecondsBetweenRetries = 3000, string userAgent = null)
        {
            var result = new HtmlDocument();
            while (numberOfAttempts >= 1)
            {
                try
                {
                    if (_WebNet == null)
                        _WebNet = new SInet(ConnectionParameters.UseProxy, ConnectionParameters.MaxRedirects, null, ConnectionParameters.TimeoutSeconds * 1000);

                    if (!string.IsNullOrWhiteSpace(userAgent))
                    {
                        _WebNet.UserAgent = userAgent;
                    }

                    result = _WebNet.DownloadDocument(requestUrl, postVars);
                    return result;

                }
                catch (WebException webEx)
                {
                    if (webEx.Response != null)
                    {
                        if (webEx.Response.Headers != null)
                        {
                            ExceptionMessage = ExceptionMessage + webEx.Response.Headers.GetKey(0).ToString() + " : " + webEx.Response.Headers.GetValues(0)[0].ToString();
                        }
                    }

                    ExceptionMessage = ExceptionMessage + " " + webEx.Message;
                }

                Thread.Sleep(millisecondsBetweenRetries);

                numberOfAttempts--;

            }

            return null;
        }

        public string GetFinalRedirectedUrl(string requestUrl)
        {
            var result = string.Empty;
            try
            {
                if (!string.IsNullOrEmpty(requestUrl))
                {
                    result = SInet.GetFinalRedirectedUrl(requestUrl);
                }
            }
            catch (WebException webEx)
            {
                if (webEx.Response != null)
                {
                    if (webEx.Response.Headers != null)
                    {
                        ExceptionMessage = ExceptionMessage + webEx.Response.Headers.GetKey(0).ToString() + " : " + webEx.Response.Headers.GetValues(0)[0].ToString();
                    }
                }

                ExceptionMessage = ExceptionMessage + " " + webEx.Message;
            }
            return result;
        }
        public byte[] DownloadData(string requestUrl, Dictionary<String, String> postVars, string userAgent = null)
        {
            try
            {
                if (_WebNet == null)
                    _WebNet = new SInet(ConnectionParameters.UseProxy, ConnectionParameters.MaxRedirects, null, ConnectionParameters.TimeoutSeconds * 1000);

                if (!string.IsNullOrWhiteSpace(userAgent))
                {
                    _WebNet.UserAgent = userAgent;
                }

                if (PostData.Count == 0)
                    return _WebNet.DownloadData(requestUrl, null);
                return _WebNet.DownloadData(requestUrl, PostData);
            }
            catch (WebException webEx)
            {
                if (webEx.Response != null)
                {
                    if (webEx.Response.Headers != null)
                    {
                        ExceptionMessage = ExceptionMessage + webEx.Response.Headers.GetKey(0).ToString() + " : " + webEx.Response.Headers.GetValues(0)[0].ToString();
                    }
                }

                ExceptionMessage = ExceptionMessage + " " + webEx.Message;
            }
            return null;
        }

        public HttpWebRequest GetRequest(string requestUrl, string userAgent = null)
        {
            if (_WebNet == null)
                _WebNet = new SInet(ConnectionParameters.UseProxy, ConnectionParameters.MaxRedirects, null, ConnectionParameters.TimeoutSeconds * 1000);
            if (!string.IsNullOrWhiteSpace(userAgent))
            {
                _WebNet.UserAgent = userAgent;
            }

            return _WebNet.GetWebRequest(requestUrl);
        }

        protected abstract void dispose();

        public void Dispose()
        {
            if (_WebNet != null)
                _WebNet.Dispose();
            dispose();
        }
    }
}
