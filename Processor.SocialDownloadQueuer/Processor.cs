﻿using JobLib;
using SI;
using SI.BLL;
using SI.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Processor.SocialDownloadQueuer
{
    public class Processor : ProcessorBase<SocialDownloadQueuerData>
    {
        // Need to pass the args through for the default constructor
        public Processor(string[] args) : base(args) { }

        protected override void start()
        {
            JobDTO job = getNextJob();

            while (job != null)
            {
                try
                {
                    setJobStatus(job.ID.Value, JobStatusEnum.Started);
                    SocialDownloadQueuerData data = getJobData(job.ID.Value);

                    switch (data.Action)
                    {
                        case SocialDownloadQueuerAction.FacebookPost:// queue downloaders that gather lists of posts and store them
                            {
                                #region FacebookPost
                                List<SocialCredentialDTO> creds = ProviderFactory.Social.GetSocialCredentials(SocialNetworkEnum.Facebook, null, true);

                                if (creds.Any())
                                {
                                    // spread jobs over 1/2 hour
                                    double secondsBetweenJobs = 1800.0 / Convert.ToDouble(creds.Count());
                                    DateTime curDate = DateTime.UtcNow;

                                    List<JobDTO> jobsToQueue = new List<JobDTO>();
                                    foreach (SocialCredentialDTO cred in creds)
                                    {
                                        FacebookDownloaderData facebookDownloaderData = new FacebookDownloaderData();

                                        facebookDownloaderData.CredentialID = cred.ID;
                                        facebookDownloaderData.FacebookEnum = FacebookProcessorActionEnum.FacebookPostDownLoader;
                                        facebookDownloaderData.NumberOfDaysBack = data.NumberOfDaysBack * -1;

                                        JobDTO job1 = new JobDTO()
                                        {
                                            JobDataObject = facebookDownloaderData,
                                            JobType = facebookDownloaderData.JobType,
                                            DateScheduled = new SIDateTime(curDate, UserInfoDTO.System.EffectiveUser.ID.Value),
                                            JobStatus = JobStatusEnum.Queued,
                                            Priority = 200,
                                            OriginJobID = job.ID.Value
                                        };

                                        jobsToQueue.Add(job1);

                                        curDate = curDate.AddSeconds(secondsBetweenJobs);
                                    }

                                    if (jobsToQueue.Any())
                                    {
                                        ProviderFactory.Jobs.Save(new SaveEntityDTO<List<JobDTO>>(jobsToQueue, UserInfoDTO.System));
                                    }
                                }

                                #endregion
                            }
                            break;
                        case SocialDownloadQueuerAction.FacebookPostStats:  // for posts we know about (see above), gather stats for them
                            {
                                #region FacebookPostStats

                                List<long> PostIDs = ProviderFactory.Social.GetFacebookPostIDsNeedingStatsUpdate();
                                //List<long> PostIDs = ProviderFactory.Social.GetFacebookPostIDs();

                                if (PostIDs.Any())
                                {
                                    double spreadSeconds = Convert.ToDouble(data.SpreadSeconds.GetValueOrDefault(900));
                                    double secondsBetweenJobs = spreadSeconds / Convert.ToDouble(PostIDs.Count());
                                    DateTime curDate = DateTime.UtcNow;

                                    List<JobDTO> jobsToQueue = new List<JobDTO>();

                                    foreach (long PostID in PostIDs)
                                    {
                                        FacebookDownloaderData facebookDownloaderData = new FacebookDownloaderData();

                                        facebookDownloaderData.PostID = PostID;
                                        facebookDownloaderData.FacebookEnum = FacebookProcessorActionEnum.FacebookPostStatisticsDownloader;

                                        JobDTO job1 = new JobDTO()
                                        {
                                            JobDataObject = facebookDownloaderData,
                                            JobType = facebookDownloaderData.JobType,
                                            DateScheduled = new SIDateTime(curDate, UserInfoDTO.System.EffectiveUser.ID.Value),
                                            JobStatus = JobStatusEnum.Queued,
                                            //JobStatus = JobStatusEnum.Parked,
                                            Priority = 200,
                                            OriginJobID = job.ID.Value
                                        };

                                        jobsToQueue.Add(job1);

                                        ProviderFactory.Social.MarkFacebookPostRefreshed(PostID);

                                        curDate = curDate.AddSeconds(secondsBetweenJobs);
                                    }

                                    if (jobsToQueue.Any())
                                    {
                                        ProviderFactory.Jobs.Save(new SaveEntityDTO<List<JobDTO>>(jobsToQueue, UserInfoDTO.System));
                                    }
                                }

                                #endregion
                            }
                            break;

                        case SocialDownloadQueuerAction.FacebookPageInsightsAndInfo:
                            {
                                #region FacebookPageInsightsAndInfo

                                List<JobDTO> jobsToQueue = new List<JobDTO>();
                                List<SocialCredentialDTO> creds = ProviderFactory.Social.GetSocialCredentials(SocialNetworkEnum.Facebook, data.AccountID, true);

                                if (creds.Any())
                                {
                                    // spread jobs over 20 hours
                                    double secondsBetweenJobs = 72000.0 / Convert.ToDouble(creds.Count());

                                    //temporary
                                    //secondsBetweenJobs = 5.0;

                                    DateTime curDate = DateTime.UtcNow;

                                    foreach (SocialCredentialDTO cred in creds)
                                    {
                                        FacebookDownloaderData fdd = new FacebookDownloaderData()
                                        {
                                            FacebookEnum = FacebookProcessorActionEnum.FacebookPageInsightsDownloader,
                                            CredentialID = cred.ID,
                                            RemainingRetries = 0
                                        };

                                        JobDTO job1 = new JobDTO()
                                        {
                                            JobDataObject = fdd,
                                            JobType = fdd.JobType,
                                            DateScheduled = new SIDateTime(curDate, 1),
                                            JobStatus = JobStatusEnum.Queued,
                                            Priority = 200,
                                            OriginJobID = job.ID.Value
                                        };

                                        FacebookDownloaderData fdd2 = new FacebookDownloaderData()
                                        {
                                            FacebookEnum = FacebookProcessorActionEnum.FacebookPageInformationDownloader,
                                            CredentialID = cred.ID,
                                            RemainingRetries = 0
                                        };
                                        JobDTO job2 = new JobDTO()
                                        {
                                            JobDataObject = fdd2,
                                            JobType = fdd2.JobType,
                                            DateScheduled = new SIDateTime(curDate, 1),
                                            JobStatus = JobStatusEnum.Queued,
                                            Priority = 200,
                                            OriginJobID = job.ID.Value
                                        };

                                        jobsToQueue.Add(job1);
                                        jobsToQueue.Add(job2);

                                        curDate = curDate.AddSeconds(secondsBetweenJobs);
                                    }

                                    if (jobsToQueue.Any())
                                    {
                                        ProviderFactory.Jobs.Save(new SaveEntityDTO<List<JobDTO>>(jobsToQueue, UserInfoDTO.System));
                                    }
                                }

                                #endregion
                            }
                            break;

                        case SocialDownloadQueuerAction.GooglePlusPageInfo:
                            {
                                #region GooglePlusPageInfo

                                List<JobDTO> jobsToQueue = new List<JobDTO>();
                                List<SocialCredentialDTO> creds = ProviderFactory.Social.GetSocialCredentials(SocialNetworkEnum.Google, data.AccountID, true);

                                foreach (SocialCredentialDTO cred in creds)
                                {
                                    GooglePlusDownloaderData gpd = new GooglePlusDownloaderData()
                                    {
                                        GooglePlusEnum = GooglePlusProcessorActionEnum.GooglePlusPageInformationDownloder,
                                        CredentialID = cred.ID,
                                        RemainingRetries = 0
                                    };

                                    JobDTO job1 = new JobDTO()
                                    {
                                        JobDataObject = gpd,
                                        JobType = gpd.JobType,
                                        DateScheduled = SIDateTime.Now,
                                        JobStatus = JobStatusEnum.Queued,
                                        Priority = 200,
                                        OriginJobID = job.ID.Value
                                    };

                                    jobsToQueue.Add(job1);
                                }

                                if (jobsToQueue.Any())
                                {
                                    ProviderFactory.Jobs.Save(new SaveEntityDTO<List<JobDTO>>(jobsToQueue, UserInfoDTO.System));
                                }

                                #endregion
                            }
                            break;
                        case SocialDownloadQueuerAction.GooglePlusPost:
                            {
                                #region GooglePlusPost
                                List<SocialCredentialDTO> creds = ProviderFactory.Social.GetSocialCredentials(SocialNetworkEnum.Google, data.AccountID, true);

                                if (creds.Any())
                                {
                                    // spread jobs over 1/2 hour
                                    double secondsBetweenJobs = 3500.0 / Convert.ToDouble(creds.Count());
                                    DateTime curDate = DateTime.UtcNow;

                                    List<JobDTO> jobsToQueue = new List<JobDTO>();
                                    foreach (SocialCredentialDTO cred in creds)
                                    {
                                        GooglePlusDownloaderData googlePlusDownloaderData = new GooglePlusDownloaderData();

                                        googlePlusDownloaderData.CredentialID = cred.ID;
                                        googlePlusDownloaderData.GooglePlusEnum = GooglePlusProcessorActionEnum.GooglePlusPostDownLoader;

                                        JobDTO job1 = new JobDTO()
                                        {
                                            JobDataObject = googlePlusDownloaderData,
                                            JobType = googlePlusDownloaderData.JobType,
                                            DateScheduled = new SIDateTime(curDate, 1),
                                            JobStatus = JobStatusEnum.Queued,
                                            //JobStatus = JobStatusEnum.Parked,
                                            Priority = 200,
                                            OriginJobID = job.ID.Value
                                        };

                                        jobsToQueue.Add(job1);

                                        curDate = curDate.AddSeconds(secondsBetweenJobs);
                                    }

                                    if (jobsToQueue.Any())
                                    {
                                        ProviderFactory.Jobs.Save(new SaveEntityDTO<List<JobDTO>>(jobsToQueue, UserInfoDTO.System));
                                    }
                                }

                                #endregion
                            }
                            break;
                        case SocialDownloadQueuerAction.GooglePlusPostStats:
                            {
                                #region GooglePlusPostStats

                                List<long> PostIDs = ProviderFactory.Social.GetGooglePlusPostIDsNeedingStatsUpdate();

                                if (PostIDs.Any())
                                {
                                    // spread jobs over 1/2 hour
                                    double secondsBetweenJobs = 1800.0 / Convert.ToDouble(PostIDs.Count());
                                    DateTime curDate = DateTime.UtcNow;

                                    List<JobDTO> jobsToQueue = new List<JobDTO>();

                                    foreach (long PostID in PostIDs)
                                    {
                                        GooglePlusDownloaderData googlePlusDownloaderData = new GooglePlusDownloaderData();

                                        googlePlusDownloaderData.PostID = PostID;
                                        googlePlusDownloaderData.GooglePlusEnum = GooglePlusProcessorActionEnum.GooglePlusPostStatisticsDownloder;

                                        JobDTO job1 = new JobDTO()
                                        {
                                            JobDataObject = googlePlusDownloaderData,
                                            JobType = googlePlusDownloaderData.JobType,
                                            DateScheduled = new SIDateTime(curDate, 1),
                                            JobStatus = JobStatusEnum.Queued,
                                            //JobStatus = JobStatusEnum.Parked,
                                            Priority = 200,
                                            OriginJobID = job.ID.Value
                                        };

                                        jobsToQueue.Add(job1);

                                        curDate = curDate.AddSeconds(secondsBetweenJobs);
                                    }

                                    if (jobsToQueue.Any())
                                    {
                                        ProviderFactory.Jobs.Save(new SaveEntityDTO<List<JobDTO>>(jobsToQueue, UserInfoDTO.System));
                                    }
                                }

                                #endregion
                            }
                            break;
                        case SocialDownloadQueuerAction.TwitterPageInfo:
                            {
                                #region TwitterPageInfo
                                List<JobDTO> jobsToQueue = new List<JobDTO>();
                                List<SocialCredentialDTO> creds = ProviderFactory.Social.GetSocialCredentials(SocialNetworkEnum.Twitter, data.AccountID, true);

                                if (creds.Any())
                                {
                                    // spread jobs over 1/2 hour
                                    double secondsBetweenJobs = 21600.0 / Convert.ToDouble(creds.Count());
                                    DateTime curDate = DateTime.UtcNow;


                                    foreach (SocialCredentialDTO cred in creds)
                                    {
                                        TwitterDownloaderData twd = new TwitterDownloaderData()
                                        {
                                            TwitterEnum = TwitterProcessorActionEnum.TwitterPageInformationDownloder,
                                            CredentialID = cred.ID,
                                            RemainingRetries = 0
                                        };

                                        JobDTO job1 = new JobDTO()
                                        {
                                            JobDataObject = twd,
                                            JobType = twd.JobType,
                                            DateScheduled = new SIDateTime(curDate, 1),
                                            JobStatus = JobStatusEnum.Queued,
                                            //JobStatus = JobStatusEnum.Parked,
                                            Priority = 200,
                                            OriginJobID = job.ID.Value
                                        };

                                        jobsToQueue.Add(job1);
                                    }

                                    if (jobsToQueue.Any())
                                    {
                                        ProviderFactory.Jobs.Save(new SaveEntityDTO<List<JobDTO>>(jobsToQueue, UserInfoDTO.System));
                                    }
                                }
                                #endregion
                            }
                            break;
                        case SocialDownloadQueuerAction.TwitterPostStats:
                            {
                                #region TwitterPostStats

                                List<long> PostTargetIDs = ProviderFactory.Social.GetTwitterPostIDs();

                                List<JobDTO> jobsToQueue = new List<JobDTO>();

                                if (PostTargetIDs.Any())
                                {
                                    // spread jobs over 1/2 hour
                                    double secondsBetweenJobs = 21600.0 / Convert.ToDouble(PostTargetIDs.Count());
                                    DateTime curDate = DateTime.UtcNow;

                                    foreach (long PostTargetID in PostTargetIDs)
                                    {
                                        TwitterDownloaderData twitterDownloaderData = new TwitterDownloaderData();

                                        twitterDownloaderData.PostTargetID = PostTargetID;
                                        twitterDownloaderData.TwitterEnum = TwitterProcessorActionEnum.TwitterPostStatisticsDownloder;

                                        JobDTO job1 = new JobDTO()
                                        {
                                            JobDataObject = twitterDownloaderData,
                                            JobType = twitterDownloaderData.JobType,
                                            DateScheduled = new SIDateTime(curDate, 1),
                                            JobStatus = JobStatusEnum.Queued,
                                            //JobStatus = JobStatusEnum.Parked,
                                            Priority = 200,
                                            OriginJobID = job.ID.Value
                                        };

                                        jobsToQueue.Add(job1);
                                    }
                                }
                                if (jobsToQueue.Any())
                                {
                                    ProviderFactory.Jobs.Save(new SaveEntityDTO<List<JobDTO>>(jobsToQueue, UserInfoDTO.System));
                                }

                                #endregion
                            }
                            break;

                        default:
                            break;
                    }

                    setJobStatus(job.ID.Value, JobStatusEnum.Completed);
                }
                catch (Exception ex)
                {
                    setJobStatus(job.ID.Value, JobStatusEnum.Failed);
                    ProviderFactory.Logging.LogException(ex);
                    Auditor
                        .New(AuditLevelEnum.Exception, AuditTypeEnum.ProcessorException, UserInfoDTO.System, ex.ToString())
                        .Save(ProviderFactory.Logging)
                    ;
                }


                job = getNextJob();
            }
        }

    }
}
