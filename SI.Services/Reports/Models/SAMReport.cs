﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SI.Services.Reports.Models
{
	public class SAMReport
	{
		public int AccountManagerID { get; set; }
		public string FirstName { get; set; }
		public string LastName { get; set; }
		public bool SocialDealerEmployee { get; set; }
		public int TotalDealers { get; set; }
		public int NumberOfReviews { get; set; }
		public int Read { get; set; }
		public int UnRead { get; set; }
		public int Responded { get; set; }
		public int UnRespondedTotal { get; set; }
		public int UnRespondedPositive { get; set; }
		public int UnRespondedNegative { get; set; }

		public SIDateTime ReviewDate { get; set;  } //placeholder for filter
	}
}
