﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Web.Mvc;
using ClosedXML.Excel;
using iTextSharp.text;
using iTextSharp.text.pdf;
using Newtonsoft.Json;
using SI.BLL;
using SI.Extensions;
using SI.Services.Framework;
using SI.Services.Framework.Models;
using SI.Services.Reports.Models;
using SI.DTO;

namespace SI.Services.Reports
{
	public static class ReportsService
	{

		public static byte[] CreateEnterpriseRepSocialExcel(string model, string data, string detailModel, string detailData)
		{
			XLWorkbook workbook = new XLWorkbook();
			IXLWorksheet worksheet = workbook.Worksheets.Add("Summary");

		#region Summary Worksheet

			var modelObject = JsonConvert.DeserializeObject<dynamic>(model);
			var dataObject = JsonConvert.DeserializeObject<dynamic>(data);

			worksheet.Cell(1, 1).SetValue("Enterprise Reputation and Social Report").Style.Font.SetBold(true).Font.SetFontSize(14);
			worksheet.Range("A1:E1").Row(1).Merge();

			worksheet.Cell("A2").SetValue("Summary");
			worksheet.Range("B2:G2").Row(1).Merge().SetValue("Reputation Summary").Style.Border.RightBorder = XLBorderStyleValues.Thin;
			worksheet.Range("H2:N2").Row(1).Merge().SetValue("Facebook Summary").Style.Border.RightBorder = XLBorderStyleValues.Thin;
			worksheet.Range("O2:Q2").Row(1).Merge().SetValue("Twitter Summary").Style.Border.RightBorder = XLBorderStyleValues.Thin;
			worksheet.Range("R2:S2").Row(1).Merge().SetValue("YouTube Summary").Style.Border.RightBorder = XLBorderStyleValues.Thin;
			worksheet.Range("T2:U2").Row(1).Merge().SetValue("Google Summary").Style.Border.RightBorder = XLBorderStyleValues.Thin;

			for (int mdx = 0; mdx < modelObject.Count; mdx++)
			{
				string header = (modelObject[mdx].title == null || modelObject[mdx].title == "&nbsp;") ? modelObject[mdx].field.ToString() : modelObject[mdx].title.ToString();
				worksheet.Cell(3, mdx + 1).SetValue(header);
			}

			worksheet.Row(2).Style.Fill.SetBackgroundColor(XLColor.LightSlateGray).Font.SetBold(true).Font.SetFontColor(XLColor.White).Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center);
			worksheet.Row(3).Style.Fill.SetBackgroundColor(XLColor.LightGray).Font.SetBold(true).Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center);

			for (int idx = 0; idx < dataObject.Count; idx++)
			{
				for (int mdx = 0; mdx < modelObject.Count; mdx++)
				{
					string fieldName = modelObject[mdx].field.ToString();

					string color = "";
					if (dataObject[idx][string.Format("{0}Color", fieldName)] != null)
						color = dataObject[idx][string.Format("{0}Color", fieldName)].ToString();

					string cellValue = dataObject[idx][fieldName].ToString();
					IXLCell cell = worksheet.Cell(idx + 4, mdx + 1).SetValue(cellValue);
					
					if (cellValue.IsNumeric())
					{
						cell.SetDataType(XLCellValues.Number);
						cell.Style.NumberFormat.SetNumberFormatId(3);
						if (cellValue.ToInteger() < 0)
							cell.Style.NumberFormat.SetNumberFormatId(38);
					}
					
					if (fieldName == "Rating")
					{
						cell.SetDataType(XLCellValues.Number);
						cell.SetValue(cellValue.ToDecimal());
					}

					if (modelObject[mdx].title.ToString().Contains("%"))
					{
						cell.Style.NumberFormat.SetNumberFormatId(10);
						cell.SetValue((cellValue.ToDecimal()/100M));
					}
					
					if (color == "#c6efce")
						cell.Style.Fill.SetBackgroundColor(XLColor.LightGreen);
					else if (color == "#ffeb9c")
						cell.Style.Fill.SetBackgroundColor(XLColor.LightYellow);
					else if (color == "#ffc7ce")
						cell.Style.Fill.SetBackgroundColor(XLColor.LightSalmon);
				}
			}

			worksheet.Columns().AdjustToContents();
			worksheet.SheetView.FreezeColumns(1);

		#endregion

			IXLWorksheet worksheetD = workbook.Worksheets.Add("Last Month Details");

			var modelDObject = JsonConvert.DeserializeObject<dynamic>(detailModel);
			var dataDObject = JsonConvert.DeserializeObject<dynamic>(detailData);

			worksheetD.Cell(1, 1).SetValue("Last Month Details").Style.Font.SetBold(true).Font.SetFontSize(14);
			worksheetD.Range("A1:E1").Row(1).Merge();

			for (int mdx = 0; mdx < modelDObject.Count; mdx++)
			{
				string header = (modelDObject[mdx].title == null || modelDObject[mdx].title == "&nbsp;") ? modelDObject[mdx].field.ToString() : modelDObject[mdx].title.ToString();
				worksheetD.Cell(2, mdx + 1).SetValue(header);
			}
			worksheetD.Row(2).Style.Fill.SetBackgroundColor(XLColor.LightGray).Font.SetBold(true).Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center);

			for (int idx = 0; idx < dataDObject.Count; idx++)
			{
				for (int mdx = 0; mdx < modelDObject.Count; mdx++)
				{
					string fieldName = modelDObject[mdx].field.ToString();

					string cellValue = dataDObject[idx][fieldName].ToString();
					IXLCell cell = worksheetD.Cell(idx + 3, mdx + 1).SetValue(cellValue);

					if (cellValue.IsNumeric())
					{
						cell.SetDataType(XLCellValues.Number);
						cell.Style.NumberFormat.SetNumberFormatId(3);
						if (cellValue.ToInteger() < 0)
							cell.Style.NumberFormat.SetNumberFormatId(38);
					}

					if (fieldName == "Rating")
					{
						cell.SetDataType(XLCellValues.Number);
						cell.SetValue(cellValue.ToDecimal());
					}
					
				}
			}

			worksheetD.Columns().AdjustToContents();
			worksheetD.SheetView.FreezeColumns(1);


			using (System.IO.MemoryStream stream = new System.IO.MemoryStream())
			{
				workbook.SaveAs(stream);
				byte[] file = stream.ToArray();

				return file;
			}
		}

		public static byte[] CreateEnterpriseRepSocialPdf(string model, string data)
		{
			Document document = new Document(PageSize.A4, 5, 5, 5, 5);

			var modelObject = JsonConvert.DeserializeObject<dynamic>(model);
			var dataObject = JsonConvert.DeserializeObject<dynamic>(data);

			using (System.IO.MemoryStream stream = new System.IO.MemoryStream())
			{
				PdfWriter.GetInstance(document, stream);

				document.Open();
				var dataTable = new PdfPTable(modelObject.Count);

				//header
				dataTable.DefaultCell.Padding = 3;
				dataTable.DefaultCell.BorderWidth = 2;
				dataTable.DefaultCell.HorizontalAlignment = Element.ALIGN_CENTER;
				
				dataTable.SetWidths(new int[] { 160, 80, 80, 80, 80, 80, 80, 80, 80, 80, 80, 80, 80, 80, 80, 80, 80, 80, 80, 80, 80 });

				for (int mdx = 0; mdx < modelObject.Count; mdx++)
				{
					string header = (modelObject[mdx].title == null || modelObject[mdx].title == "&nbsp;") ? modelObject[mdx].field.ToString() : modelObject[mdx].title.ToString();

					dataTable.AddCell(header);
				}

				//data
				dataTable.HeaderRows = 1;
				dataTable.DefaultCell.BorderWidth = 1;

				for (int idx = 0; idx < dataObject.Count; idx++)
				{
					for (int mdx = 0; mdx < modelObject.Count; mdx++)
					{
						string fieldName = modelObject[mdx].field.ToString();
						string cellValue = dataObject[idx][fieldName].ToString();

						dataTable.AddCell(cellValue);


					}
				}

				

				document.Add(dataTable);
				document.Close();

				byte[] file = stream.ToArray();

				return file;
			}


		}

		public static List<SAMReport> SAMReportGrid(UserInfoDTO userInfo, PagedGridRequest pagedGridRequest, string timeFrame)
		{
			List<SAMReport> samReports = new List<SAMReport>();

			SAMReport r1 = new SAMReport();
			r1.AccountManagerID = 1;
			r1.FirstName = "Mike";
			r1.LastName = "Kordos";
			r1.TotalDealers = 15;
			r1.NumberOfReviews = 400;
			r1.Read = 300;
			r1.UnRead = 100;
			r1.Responded = 10;
			r1.UnRespondedTotal = 1;
			r1.UnRespondedPositive = 0;
			r1.UnRespondedNegative = 1;
			samReports.Add(r1);

			SAMReport r2 = new SAMReport();
			r2.AccountManagerID = 2;
			r2.FirstName = "Ashley";
			r2.LastName = "Ross";
			r2.TotalDealers = 25;
			r2.NumberOfReviews = 400;
			r2.Read = 300;
			r2.UnRead = 200;
			r2.Responded = 20;
			r2.UnRespondedTotal = 2;
			r2.UnRespondedPositive = 0;
			r2.UnRespondedNegative = 2;
			samReports.Add(r2);

			SAMReport r3 = new SAMReport();
			r3.AccountManagerID = 3;
			r3.FirstName = "Morgan";
			r3.LastName = "Smith";
			r3.TotalDealers = 35;
			r3.NumberOfReviews = 400;
			r3.Read = 300;
			r3.UnRead = 300;
			r3.Responded = 30;
			r3.UnRespondedTotal = 3;
			r3.UnRespondedPositive = 0;
			r3.UnRespondedNegative = 3;
			samReports.Add(r3);

			SAMReport r4 = new SAMReport();
			r4.AccountManagerID = 4;
			r4.FirstName = "Nick";
			r4.LastName = "Jones";
			r4.TotalDealers = 45;
			r4.NumberOfReviews = 400;
			r4.Read = 300;
			r4.UnRead = 400;
			r4.Responded = 40;
			r4.UnRespondedTotal = 4;
			r4.UnRespondedPositive = 0;
			r4.UnRespondedNegative = 4;
			samReports.Add(r4);

			SAMReport r5 = new SAMReport();
			r5.AccountManagerID = 5;
			r5.FirstName = "John";
			r5.LastName = "Doe";
			r5.TotalDealers = 55;
			r5.NumberOfReviews = 400;
			r5.Read = 300;
			r5.UnRead = 500;
			r5.Responded = 50;
			r5.UnRespondedTotal = 5;
			r5.UnRespondedPositive = 0;
			r5.UnRespondedNegative = 5;
			samReports.Add(r5);

			SAMReport r6 = new SAMReport();
			r6.AccountManagerID = 6;
			r6.FirstName = "Barack";
			r6.LastName = "Obama";
			r6.TotalDealers = 65;
			r6.NumberOfReviews = 400;
			r6.Read = 300;
			r6.UnRead = 600;
			r6.Responded = 60;
			r6.UnRespondedTotal = 6;
			r6.UnRespondedPositive = 0;
			r6.UnRespondedNegative = 6;
			samReports.Add(r6);

			SAMReport r7 = new SAMReport();
			r7.AccountManagerID = 7;
			r7.FirstName = "Mike";
			r7.LastName = "Kordos";
			r7.TotalDealers = 75;
			r7.NumberOfReviews = 400;
			r7.Read = 300;
			r7.UnRead = 700;
			r7.Responded = 70;
			r7.UnRespondedTotal = 7;
			r7.UnRespondedPositive = 0;
			r7.UnRespondedNegative = 7;
			samReports.Add(r7);

			SAMReport r8 = new SAMReport();
			r8.AccountManagerID = 8;
			r8.FirstName = "Mike";
			r8.LastName = "Kordos";
			r8.TotalDealers = 85;
			r8.NumberOfReviews = 400;
			r8.Read = 300;
			r8.UnRead = 800;
			r8.Responded = 80;
			r8.UnRespondedTotal = 8;
			r8.UnRespondedPositive = 0;
			r8.UnRespondedNegative = 8;
			samReports.Add(r8);

			return samReports;
		}

		public static List<SelectListItem> GetAccounts(UserInfoDTO userInfo, string dealerFilter)
		{
			List<SelectListItem> items = new List<SelectListItem>();

			AccountSearchRequestDTO requestDto = new AccountSearchRequestDTO();
			requestDto.UserInfo = userInfo;
			requestDto.AccountIDAndDescendants = userInfo.EffectiveUser.CurrentContextAccountID;
			requestDto.SearchString = dealerFilter;
			requestDto.StartIndex = 0;
			requestDto.EndIndex = 100;
			requestDto.SortBy = 2;
			requestDto.SortAscending = true;

			AccountSearchResultDTO resultDtos = ProviderFactory.Security.SearchAccounts(requestDto);

			foreach (AccountListItemDTO account in resultDtos.Items)
			{
				SelectListItem selectListItem = new SelectListItem();
				selectListItem.Text = account.Name;
				selectListItem.Value = account.ID.ToString();

				if (account.ID == userInfo.EffectiveUser.CurrentContextAccountID)
					selectListItem.Selected = true;

				items.Add(selectListItem);
			}

			return items;
		}

	    public static List<SelectListItem> GetSocialNetworks()
	    {
            List<SelectListItem> results = new List<SelectListItem>();

	        var rawResult = ProviderFactory.Social.GetSocialNetworks();

	        foreach (var item in rawResult)
	        {
                SelectListItem selectListItem = new SelectListItem();
	            selectListItem.Text = item.Name;
	            selectListItem.Value = item.ID.ToString();
	        }

            return results;
	    }

	    public static List<SelectListItem> GetSyndicators()
	    {
            var results = new List<SelectListItem>();

	        var rawResult = ProviderFactory.Social.GetSyndicators();

            foreach (var item in rawResult)
            {
                SelectListItem selectListItem = new SelectListItem();
                selectListItem.Text = item.Name;
                selectListItem.Value = item.ID.ToString();
            }

            return results;
	    }

		public static List<EnterpriseReputationSocial> EnterpriseReputationSocialGrid(UserInfoDTO userInfo, PagedGridRequest pagedGridRequest, string accountIDs, long? VGID)
		{
			List<EnterpriseReputationSocial> report = new List<EnterpriseReputationSocial>();

			if (VGID.HasValue)
				accountIDs = "";

			List<long> accounts = accountIDs.Split(',').Select(n => n.ToLong()).ToList();

			List<EnterpriseReputationSocialReportDTO> reportDtos = ProviderFactory.Report.GetReputationAndSocialReport(userInfo, accounts, VGID);

			foreach (EnterpriseReputationSocialReportDTO dto in reportDtos)
			{
				EnterpriseReputationSocial ers = new EnterpriseReputationSocial();
				ers.AccountID = dto.AccountID;
				ers.AccountName = dto.AccountName;
				ers.PercentPositive = dto.PercentPositive;

				string color = "";
				if (dto.PercentPositive > 0 && dto.PercentPositive <= 60M)
					color = "#ffc7ce";
				else if (dto.PercentPositive > 60M && dto.PercentPositive < 80M)
					color = "#ffeb9c";
				else if (dto.PercentPositive >= 80M)
					color = "#c6efce";
				ers.PercentPositiveColor = color;

				ers.Rating = dto.Rating;
				ers.RatingPercentChange = dto.RatingPercentChange;
				ers.TotalFans = dto.TotalFans;
				ers.TotalFollowers = dto.TotalFollowers;
				ers.TotalPlusOnes = dto.TotalPlusOnes;
				ers.TotalReviews = dto.TotalReviews;
				ers.TotalSubscribers = dto.TotalSubscribers;
				ers.YTDEngagedUsers = dto.YTDEngagedUsers;
				ers.YTDFanGrowth = dto.YTDFanGrowth;
				ers.YTDFanImpressions = dto.YTDFanImpressions;
				ers.YTDFollowersGrowth = dto.YTDFollowersGrowth;
				ers.YTDGrowth = dto.YTDGrowth;
				ers.YTDPaidPostImpressions = dto.YTDPaidPostImpressions;
				ers.YTDPercentFanGrowth = dto.YTDPercentFanGrowth;
				ers.YTDPercentGrowth = dto.YTDPercentGrowth;
				ers.YTDPlusOnesGrowth = dto.YTDPlusOnesGrowth;
				ers.YTDSubscribersGrowth = dto.YTDSubscribersGrowth;
				ers.YTDTotalImpressions = dto.YTDTotalImpressions;
				ers.YTDTwitterPercentGrowth = dto.YTDTwitterPercentGrowth;

				report.Add(ers);


			}


			return report;
		}

		public static List<EnterpriseReputationSocialDetail> EnterpriseReputationSocialDetailGrid(UserInfoDTO userInfo, PagedGridRequest pagedGridRequest, string accountIDs, long? VGID)
		{
			List<EnterpriseReputationSocialDetail> report = new List<EnterpriseReputationSocialDetail>();

			if (VGID.HasValue)
				accountIDs = "";

			List<long> accounts = accountIDs.Split(',').Select(n => n.ToLong()).ToList();

			List<EnterpriseReputationSocialDetailReportDTO> reportDtos = ProviderFactory.Report.GetReputationAndSocialDetailReport(userInfo, accounts, VGID);

			foreach (EnterpriseReputationSocialDetailReportDTO dto in reportDtos)
			{
				EnterpriseReputationSocialDetail ers = new EnterpriseReputationSocialDetail();
				ers.AccountID = dto.AccountID;
				ers.AccountName = dto.AccountName;
				ers.TotalReviews = dto.TotalReviews;
				ers.PositiveReviews = dto.PositiveReviews;
				ers.NegativeReviews = dto.NegativeReviews;
				ers.Rating = dto.Rating;
				ers.NewReview = dto.NewReview;
				ers.Fans = dto.Fans;
				ers.NewFans = dto.NewFans;
				ers.EngagedUsers = dto.EngagedUsers;
				ers.FanImpressions = dto.FanImpressions;
				ers.TotalImpressions = dto.TotalImpressions;
				ers.PaidPostImpressions = dto.PaidPostImpressions;
				ers.Followers = dto.Followers;
				ers.SubScribers = dto.SubScribers;
				ers.PlusOnes = dto.PlusOnes;
				
				report.Add(ers);


			}

			return report;
		}
	}
}
