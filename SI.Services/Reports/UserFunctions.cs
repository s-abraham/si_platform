﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using Telerik.Reporting.Expressions;

namespace ReportLibrary
{
	public static class SIUserFunctions
	{
		//[Function(Category = "SI", Namespace = "SI.Services.ReportLibrary.SIUserFunctions", Description = "Adds minute offset to UTC datetime")]
		public static string AddMinuteOffset(DateTime utcDateTime, int offsetMinutes)
		{
			DateTime convertedDateTime = utcDateTime.AddMinutes(offsetMinutes);
			return convertedDateTime.ToString("MM/dd/yyyy  h:mm tt");
		}

		public static string ArrayToComma(Object[] list)
		{
			string [] items = new string[100];

			int idx = 0;
			foreach (var o in list)
			{
				items[idx] = (string) o;
				idx++;
			}

			return string.Join(",", items);
		}

		public static string UrlEncode(string url)
		{
			return HttpUtility.UrlEncode(url);
		}

		public static string HtmlEncode(string html)
		{
			return HttpUtility.HtmlEncode(html);
		}
	}
}
