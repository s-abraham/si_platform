﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace SI.Services.LandingPages.Models
{
	public class SyndicationSignup
	{
		public string HeaderBackgroundImage { get; set; }	
		public string HeaderLogoImage { get; set; }		
		public string HeaderBackgroundColor { get; set; }	
		public string PageTitle { get; set; }				
		public string InfoTitle { get; set; }				
		public string InfoPage1Paragraph { get; set; }		
		public string InfoPage2Paragraph { get; set; }		
		public string Instructions { get; set; }
        public string CoverflowImage1 { get; set; }
        public string CoverflowImage2 { get; set; }
        public string CoverflowImage3 { get; set; }
        public string CoverflowImage4 { get; set; }
        public string CoverflowImage5 { get; set; }
        public string CoverflowImage6 { get; set; }

		public bool UseShadowedBox { get; set; }
		public bool ShowSliderTitle { get; set; }
		public string Header2 { get; set; }
	}

	public class SyndicationRegistration
	{
		public string HeaderView { get; set; }        
		public string MarketingView { get; set; }
		public string WelcomeView { get; set; }
		public string FacebookPageID { get; set; }
		public string FacebookLoginURL { get; set; }
		public string FacebookStateContext { get; set; }
		public string ResellerName { get; set; }
		public long ResellerID { get; set; }
		public long? SyndicatorID { get; set; }
		public long? PreferedParentAccountID { get; set; }
		public long SocialAppID { get; set; }
		public string AccessToken { get; set; }
		public string TimeZoneAbrv { get; set; }

		[Required(ErrorMessage = "Account Name is required.")]
		public string AccountName { get; set; }
		[Required(ErrorMessage = "Address is required.")]
		public string Address { get; set; }
		[Required(ErrorMessage = "City is required.")]
		public string City { get; set; }
		public string State { get; set; }
		[Required(ErrorMessage = "State is required.")]
		public long? StateID { get; set; }
		[Required(ErrorMessage = "Country is required.")]
		public long? CountryID { get; set; }
		[Required(ErrorMessage = "Postal Code is required.")]
		public string Zipcode { get; set; }

		[Display(Name = "Phone Number")]
		[DataType(DataType.PhoneNumber)]
		[RegularExpression(@"([0-9\(\)\/\+ \-]*)$", ErrorMessage = "Not a valid Phone Number")]
		[Required(ErrorMessage = "Phone Number is required.")]
		public string Phone { get; set; }

		[Display(Name = "Website URL")]
		[DataType(DataType.Url)]
		[RegularExpression(@"((https?)://|(www)\.)[a-z0-9-]+(\.[a-z0-9-]+)+([/?].*)?$", ErrorMessage = "Not a valid URL")]
		public string WebSiteUrl { get; set; }

		public string FranchiseInterest { get; set; }
		public long[] FranchiseTypeIDs { get; set; }

		[Required(ErrorMessage = "First Name is required.")]
		public string FirstName { get; set; }
		[Required(ErrorMessage = "Last Name is required.")]
		public string LastName { get; set; }

		[Display(Name = "Phone Number")]
		[DataType(DataType.PhoneNumber)]
		[RegularExpression(@"([0-9\(\)\/\+ \-]*)$", ErrorMessage = "Not a valid Phone Number")]
		[Required(ErrorMessage = "Phone Number is required.")]
		public string PhonePerson { get; set; }

		[DataType(DataType.EmailAddress)]
		[RegularExpression(@"[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.[A-Za-z]{2,4}", ErrorMessage = "Email is not valid.")]
		[Required(ErrorMessage = "Email is required.")]
		public string Email { get; set; }

		public List<RegistrationPackages> Packages { get; set; }
		public List<RegistrationPackagesVerticalMarket> VerticalMarkets { get; set; }
		[Required(ErrorMessage = "Vertical Market is required.")]
		public long VerticalMarketID { get; set; }
		public string SelectedPackages { get; set; }

		public SyndicationRegistration()
		{
			Packages = new List<RegistrationPackages>();
			VerticalMarkets = new List<RegistrationPackagesVerticalMarket>();
		}
	}

	public class RegistrationPackages
	{
		public long ID { get; set; }
		public string Name { get; set; }
		public string Marketing { get; set; }
		public bool Selected { get; set; }
	}

	public class RegistrationPackagesVerticalMarket
	{
		public long ID { get; set; }
		public string Name { get; set; }
	}
}
