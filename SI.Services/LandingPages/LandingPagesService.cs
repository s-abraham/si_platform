﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Facebook;
using Facebook.Entities;
using SI.BLL;
using SI.DTO;
using SI.Extensions;
using SI.Services.Admin;
using SI.Services.Admin.Models;
using SI.Services.LandingPages.Models;
using Twitter;
using Twitter.Entities;

namespace SI.Services.LandingPages
{
    public static class LandingPagesService
    {
        public static string GetProcessorSetting(string key)
        {
            return ProviderFactory.Jobs.GetProcessorSetting(key);
        }


        public static string GetFaceBookToken(string code, long resellerID)
        {
            SocialConfigDTO socialConfigDto = ProviderFactory.Social.GetSocialConfig(null, resellerID, SocialNetworkEnum.Facebook);

			//socialConfigDto.AppCallback = "http://72.148.59.48:82/oAuth/Callback/";
			//socialConfigDto.AppID = "306195176076789";
			//socialConfigDto.AppSecret = "564678eedf1f5f9b1f96d453e4555f4a";
                        
            try
            {
                if (Util.GetLocalIPAddress() == "192.168.8.111")
                {
                    //this is likely Reynolds local dev machine
                    socialConfigDto.AppCallback = "http://70.88.98.242:22222/oAuth/Callback/";
                    //socialConfigDto.AppID = "1514743978747674";
                    //socialConfigDto.AppSecret = "547cf71bcfb0efee201ce825544dd087";
                    socialConfigDto.AppID = "436310969778694";
                    socialConfigDto.AppSecret = "cc7abf7140d0136792c3592f9272b1b4";
                }
            }
            catch (Exception)
            {
            }


            return FacebookAPI.GetFacebookAccessToken(socialConfigDto.AppID, socialConfigDto.AppCallback, socialConfigDto.AppSecret, code);
        }

        public static bool SaveTwitterConnection(UserInfoDTO userInfo, string authCode, long? socialAppId, Guid accessToken)
        {
            //bool result = false;
            SyndicationInviteAccessTokenDTO syndicationInviteToken = (SyndicationInviteAccessTokenDTO)ProviderFactory.Security.GetAccessToken(accessToken, true);
            if (syndicationInviteToken.RemainingUses.GetValueOrDefault(1) == 0) return false;

            if (!string.IsNullOrEmpty(authCode))
            {
                string[] tokens = authCode.Split(',');

                SocialConfigDTO socialConfigDto = ProviderFactory.Social.GetSocialConfig(userInfo, 5, SocialNetworkEnum.Twitter);
                if (socialConfigDto != null)
                {
                    TwitterOAuthResponse response = TwitterAPI.GetTwitterOAuthTokens(socialConfigDto.AppID, socialConfigDto.AppSecret, tokens[0], tokens[1]);
                    if (response.success)
                    {
                        string website = string.Format("http://Twitter.com/{0}", response.screenName);

                        AuditSocialConfig auditSocialConfig = new AuditSocialConfig();
                        auditSocialConfig.SocialAppId = socialAppId.GetValueOrDefault();
                        auditSocialConfig.Token = response.token;
                        auditSocialConfig.Website = response.webSite;

                        ProviderFactory.Logging.Audit(AuditLevelEnum.Information, AuditTypeEnum.SocialConfiguration, "Syndication Signup Social Config Save Twitter", auditSocialConfig, userInfo);

                        //result = ProviderFactory.Social.SaveTwitterConnectionFromSyndication(userInfo, socialAppId, response.token, response.tokenSecret, response.userID, website, response.pictureURL, response.screenName, accessToken);
                        //if (result == true)
                        //{
                        //    ProviderFactory.Platform.SendSyndicationSignupEmail(accessToken, SocialNetworkEnum.Twitter);
                        //}
                        SocialConnectionInfoDTO dto = new SocialConnectionInfoDTO()
                        {
                            AccountID = syndicationInviteToken.AccountID,
                            SocialNetwork = SocialNetworkEnum.Twitter,
                            PictureURL = response.pictureURL,
                            ScreenName = response.screenName,
                            Token = response.token,
                            TokenSecret = response.tokenSecret,
                            URI = website,
                            UniqueID = response.userID,
							SocialAppID = socialAppId.GetValueOrDefault()
                        };
                        SaveEntityDTO<SocialConnectionInfoDTO> result = ProviderFactory.Social.Save(new SaveEntityDTO<SocialConnectionInfoDTO>(dto, userInfo));

                        if (result.Problems.Any() == false)
                        {
                            bool SendWelcomeEmail = false;
                            //Check for the Sub Subscriptions if Subscriptions Not EXISTS then Send Welcome Email after Adding Subscriptions
                            List<AccountPackageListItemDTO> listAccountPackageListItemDTO = ProviderFactory.Products.GetSubscribedPackagesForAccount(syndicationInviteToken.AccountID, userInfo);
                            var accountPackageListItemDTO = listAccountPackageListItemDTO.Where(l => l.ResellerID == syndicationInviteToken.ResellerID && l.PackageID == syndicationInviteToken.PackageID).FirstOrDefault();

                            if (accountPackageListItemDTO == null)
                            {
                                SendWelcomeEmail = true;
                            }
                            
                            UpdatePackageResultDTO packResult = ProviderFactory.Products.AddPackage(syndicationInviteToken.AccountID, syndicationInviteToken.ResellerID, syndicationInviteToken.PackageID, userInfo, null, 0);
                            if (packResult.Outcome == UpdatePackageOutcome.Success)
                            {
                                ProviderFactory.Platform.SendSyndicationSignupEmail(accessToken, SocialNetworkEnum.Twitter);

                                if (SendWelcomeEmail)
                                {
                                    //Send Welcome Email
                                    ProviderFactory.Platform.SendEnrollmentWelcomeEmail(accessToken);
                                }
                                return true;
                            }
                        }

                        return false;
                    }
                }
            }

            return false;
        }

        public static bool SaveFacebookConnection(UserInfoDTO userInfo, string pageId, string website, long? socialappid, string facebookTempToken, Guid accessToken)
        {
            SyndicationInviteAccessTokenDTO syndicationInviteToken = (SyndicationInviteAccessTokenDTO)ProviderFactory.Security.GetAccessToken(accessToken, true);
            if (syndicationInviteToken.RemainingUses.GetValueOrDefault(1) == 0) return false;

            string pageToken = "";
            string screenName = "";
            string pictureURL = "";
            //bool result = false;

            FacebookAccount accounts = FacebookAPI.GetFacebookAccounts(facebookTempToken);
            if (accounts != null)
            {
                Account pageAccount = accounts.data.Where(t => t.id == pageId).FirstOrDefault();
	            if (pageAccount != null)
	            {
		            pageToken = pageAccount.access_token;
		            screenName = pageAccount.name;

		            FaceBookUserPicture picture = FacebookAPI.FaceBookUserPicture(pageId);
		            if (picture != null)
		            {
			            pictureURL = picture.picture.data.url;
		            }
	            }
            }

	        if (string.IsNullOrEmpty(pageToken))
	        {
				AuditSocialConfig asc = new AuditSocialConfig();
				asc.SocialAppId = socialappid.GetValueOrDefault();
				asc.PageID = pageId;
				asc.Token = pageToken;
				ProviderFactory.Logging.Audit(AuditLevelEnum.Error, AuditTypeEnum.SocialConfiguration, "Syndication Signup Social Config Save - could not get page Token", asc, userInfo);

				return false;
	        }

	        string websiteUrl = string.Format("http://www.facebook.com/home.php?#!/profile.php?id={0}", pageId);

            AuditSocialConfig auditSocialConfig = new AuditSocialConfig();
            auditSocialConfig.SocialAppId = socialappid.GetValueOrDefault();
            auditSocialConfig.PageID = pageId;
            auditSocialConfig.Token = pageToken;
            auditSocialConfig.Website = websiteUrl;

            ProviderFactory.Logging.Audit(AuditLevelEnum.Information, AuditTypeEnum.SocialConfiguration, "Syndication Signup Social Config Save", auditSocialConfig, userInfo);

            //result = ProviderFactory.Social.SaveSocialConnectionFromSyndication(userInfo, socialappid, pageToken, pageId, screenName, websiteUrl, pictureURL, accessToken);

            SocialConnectionInfoDTO dto = new SocialConnectionInfoDTO()
            {
                AccountID = syndicationInviteToken.AccountID,
                SocialNetwork = SocialNetworkEnum.Facebook,
                PictureURL = pictureURL,
                ScreenName = screenName,
                Token = pageToken,
                TokenSecret = string.Empty,
				URI = websiteUrl,
                UniqueID = pageId,
				SocialAppID = socialappid.GetValueOrDefault()
            };
            SaveEntityDTO<SocialConnectionInfoDTO> result = ProviderFactory.Social.Save(new SaveEntityDTO<SocialConnectionInfoDTO>(dto, userInfo));

            if (result.Problems.Any() == false)
            {
                bool SendWelcomeEmail = false;
                //Check for the Sub Subscriptions if Subscriptions Not EXISTS then Send Welcome Email after Adding Subscriptions
                List<AccountPackageListItemDTO> listAccountPackageListItemDTO = ProviderFactory.Products.GetSubscribedPackagesForAccount(syndicationInviteToken.AccountID, userInfo);
                var accountPackageListItemDTO = listAccountPackageListItemDTO.Where(l => l.ResellerID == syndicationInviteToken.ResellerID && l.PackageID == syndicationInviteToken.PackageID).FirstOrDefault();

                if (accountPackageListItemDTO == null)
                {
                    SendWelcomeEmail = true;
                }

                UpdatePackageResultDTO packResult = ProviderFactory.Products.AddPackage(syndicationInviteToken.AccountID, syndicationInviteToken.ResellerID, syndicationInviteToken.PackageID, userInfo, null, 0);

                if (packResult.Outcome == UpdatePackageOutcome.Success)
                {
                    ProviderFactory.Platform.SendSyndicationSignupEmail(accessToken, SocialNetworkEnum.Facebook);

                    if (SendWelcomeEmail)
                    {
                        //Send Welcome Email
                        ProviderFactory.Platform.SendEnrollmentWelcomeEmail(accessToken);
                    }
                    return true;
                }
            }

            return false;

            //bool SetAAccountASAutoApproval = false;

            //if (SetAAccountASAutoApproval)
            //{
            //    if (accessTokenBaseDto != null && accessTokenBaseDto.Type() == AccessTokenTypeEnum.SyndicationInvitation)
            //    {
            //        SetAutoApproval(userInfo, accessTokenBaseDto.SyndicatorID, accessTokenBaseDto.AccountID);
            //    }
            //}

            //return result;
        }

        public static void SetAutoApproval(UserInfoDTO userInfo, long syndicatorID, long AccountID)
        {
            var socialnetworks = ProviderFactory.Social.GetSocialNetworks();

            foreach (var socialnetworlk in socialnetworks)
            {
                ProviderFactory.Social.SaveAutoApproval(userInfo, syndicatorID, AccountID, socialnetworlk.ID);
            }
        }

        public static void RemoveAutoApproval(UserInfoDTO userInfo, long syndicatorID, long AccountID)
        {
            var socialnetworks = ProviderFactory.Social.GetSocialNetworks();

            foreach (var socialnetworlk in socialnetworks)
            {
                ProviderFactory.Social.RemoveAutoApproval(userInfo, syndicatorID, AccountID, socialnetworlk.ID);
            }
        }
        //TODO Dummy Data, get syndication type from guid and query for customizatons
        public static SyndicationSignup GetSyndicationSignUpCustom(long syndicatorID)
        {
            SyndicationSignup signup = new SyndicationSignup();

	        signup.UseShadowedBox = true;
	        signup.ShowSliderTitle = false;
			signup.Header2 = "";
            
            switch (syndicatorID)
            {
                case 2: //Carfax
                    signup.HeaderBackgroundImage = "carfax/carfax-cfc-header-bg.jpg";
                    signup.HeaderLogoImage = "carfax/carfax-cfc-header2.jpg";
                    signup.HeaderBackgroundColor = "#007fb2";
                    signup.PageTitle = "CARFAX Content Syndication Signup";

                    signup.InfoTitle = "Lets Get Social!";
                    signup.InfoPage1Paragraph =
                        @"<p>You have been selected to receive access to FREE and ENGAGING social media content for your Facebook and Twitter feeds.  Our program automatically submits for your approval content that gets your fans involved and gets your brand out there. Prior to every post, you will receive an email showing you the post and giving you the chance to approve or deny the post before it gets published on your pages.</p>" +
                        @"<p>Not to worry, we won’t post anything you haven’t approved. You will need your Facebook and/or Twitter business page username(s) and password(s) to complete the setup process. It only takes a couple minutes to complete the process. For setup questions, call SOCIALDEALER Support at 877-326-7624 ext #1. </p>";

                    signup.InfoPage2Paragraph =
                        @"<p>Okay let's get started with the first step in getting some great engaging content delivered right to your email all ready to post with just a quick app.</p>";

                    signup.Instructions =
                        @"<p>If you connect with Facebook, you must LOGOUT of Facebook before you start this process.</p><p>OR</p><p>You must be logged in to your personal Facebook profile that has admin rights to any Facebook Pages you wish to enroll. You CAN NOT be possessing a dealership page." +
                        @"</p><p>During the connection process, Facebook will prompt you about giving us permission to post on your behalf. Neither SOCIALDEALER nor the sydicator will be publishing content to your personal profile. We will only be publishing content to the pages you enroll." +
                        @"To enroll more than one page, please complete this process for each page.</p><p>For questions or assistance contact support at <a href='tel:8773267624'>877-326-7624</a> opt 1. You may also email <a href='mailto:support@socialdealer.com?subject=Syndication Signup'>support@socialdealer.com</a> or visit our <a href='https://socialdealer1.zendesk.com/home' target='_blank'>Online Support Site</a>.</p>";

                    signup.CoverflowImage1 = "/contentsyndication/carfax/carfax-cfc-post1.jpg";
                    signup.CoverflowImage2 = "/contentsyndication/carfax/carfax-cfc-post2.jpg";
                    signup.CoverflowImage3 = "/contentsyndication/carfax/carfax-cfc-post3.jpg";
                    signup.CoverflowImage4 = "/contentsyndication/carfax/carfax-cfc-post4.jpg";
                    signup.CoverflowImage5 = "/contentsyndication/carfax/carfax-cfc-post5.jpg";
                    signup.CoverflowImage6 = "/contentsyndication/carfax/carfax-cfc-post6.jpg";


                    break;

                case 1: //Lexus
                    signup.HeaderBackgroundImage = "lexus/cfc-header-bg.jpg";
                    signup.HeaderLogoImage = "lexus/cfc-header2.png";
                    signup.HeaderBackgroundColor = "#fff";
                    signup.PageTitle = "";
		            signup.UseShadowedBox = false;
		            signup.ShowSliderTitle = true;

                    signup.InfoTitle = "WELCOME TO THE LEXUS SOCIAL MEDIA PUBLISHING PROGRAM";
                    signup.InfoPage1Paragraph =
						@"<p>This exclusive program is designed to provide your dealership with direct access to social media content, assets and training to help promote the Lexus brand, products and customer experience as well as provide you with the needed support to enhance your social media efforts. </p>" +
                        @"<p>In this program you will receive:</p>" +
						@"<ul style='color: #666666; font-family: Arial; font-size: 11pt;list-style-image: url(http://cdn.socialintegration.com/contentsyndication/lexus/bullet.png);'>" +
						@"<li>Lexus branded content for your dealerships’ Facebook and Twitter business pages.</li><br />" +
						@"<li>An editorial calendar with information on posting frequency.</li><br />" +
						@"<li>Social media best practices for automotive dealers.</li><br />" +
						@"<li>Program support and additional social training from SOCIALDEALER.</li><br /></ul>" +
						@"<p><strong style='color: #000000'>IT'S EASY TO GET STARTED: </strong>" +
						@"Simply click on the CONNECT NOW button below to complete the setup process.  You will need your dealership’s Facebook and Twitter business page username(s) and password(s).</p>" +         
						@"<p>Once connected, you will receive an email that confirms your successful enrollment in the Lexus Dealer Social Media Publishing Program.</p>"+
						@"<p>Should you have additional questions about the program, please contact your Lexus Area Sales Support Manager.</p>" +
						@"<p><span style='color: #666666; font-family: Arial; font-size: 11pt;'>For technical setup questions, call <span style='color: #666666; font-family: Arial; font-size: 11pt; font-weight: bold;'>" +
						@"SOCIAL</span><span style='color: #fdc849; font-family: Arial; font-size: 11pt; font-weight: bold;'>DEALER</span>" +
						@" Support at <span style='color: #666666; font-family: Arial; font-size: 11pt; font-weight: bold;'>877-326-7624 ext #1</span> </span></p>";

		            signup.Header2 =
                        @"<div style='float: left;width: 100%;height: 70px;margin: 0 0 10px; text-align: center; background-image: url(http://cdn.socialintegration.com/contentsyndication/lexus/bg_header2.jpg);background-repeat: repeat-x;background-position: top center;'>" +
						@"<img src='http://cdn.socialintegration.com/contentsyndication/lexus/social_dealer_social_header3.png' width='960' height='76' usemap='#m_social_dealer_social_header3' />" +
			            @"<map name='m_social_dealer_social_header3' id='m_social_dealer_social_header3'>" +
			            @"<area shape='rect' coords='919,28,949,54' href=' https://twitter.com/lexus' target='_blank' alt='' />" +
			            @"<area shape='rect' coords='878,28,908,54' href='https://www.facebook.com/lexus' target='_blank' alt='' />" +
			            @"<area shape='rect' coords='410,34,593,54' href='http://www.lexus.com' target='_blank' alt='' />" +
			            @"<area shape='rect' coords='254,34,392,54' href='http://www.lexus.com' target='_blank' alt='' />" +
			            @"<area shape='rect' coords='82,34,237,54' href='http://www.lexus.com' target='_blank' alt='' />" +
			            @"<area shape='rect' coords='14,34,71,54' href='http://www.lexus.com' target='_blank' alt='' />" +
			            @"</map></div>";


                    signup.InfoPage2Paragraph =
                        @"<p>Okay let's get started with the first step in getting some great engaging content delivered right to your email all ready to post with just a quick app.</p>";

                    signup.Instructions =
                        @"<p>If you connect with Facebook, you must LOGOUT of Facebook before you start this process.</p><p>OR</p><p>You must be logged in to your personal Facebook profile that has admin rights to any Facebook Pages you wish to enroll. You CAN NOT be possessing a dealership page." +
                        @"</p><p>During the connection process, Facebook will prompt you about giving us permission to post on your behalf. Neither SOCIALDEALER nor the sydicator will be publishing content to your personal profile. We will only be publishing content to the pages you enroll." +
                        @"To enroll more than one page, please complete this process for each page.</p><p>For questions or assistance contact support at <a href='tel:8773267624'>877-326-7624</a> opt 1. You may also email <a href='mailto:support@socialdealer.com?subject=Syndication Signup'>support@socialdealer.com</a> or visit our <a href='https://socialdealer1.zendesk.com/home' target='_blank'>Online Support Site</a>.</p>";

                    signup.CoverflowImage1 = "/contentsyndication/lexus/lexus-cfc-post1.jpg";
                    signup.CoverflowImage2 = "/contentsyndication/lexus/lexus-cfc-post2.jpg";
                    signup.CoverflowImage3 = "/contentsyndication/lexus/lexus-cfc-post3.jpg";
                    signup.CoverflowImage4 = "/contentsyndication/lexus/lexus-cfc-post4.jpg";
                    signup.CoverflowImage5 = "/contentsyndication/lexus/lexus-cfc-post5.jpg";
                    signup.CoverflowImage6 = "/contentsyndication/lexus/lexus-cfc-post6.jpg";

                    break;

                case 3: //AutoConverse
                    //signup.HeaderBackgroundImage = "carfax/carfax-cfc-header-bg.jpg";
                    signup.HeaderBackgroundImage = "";
                    signup.HeaderLogoImage = "autoconverse/logo-autoconverse-new-spaced.png";
                    signup.HeaderBackgroundColor = "white";
                    signup.PageTitle = "AutoConverse Content Syndication Signup";

                    signup.InfoTitle = "SIGN UP FOR ACCESS TO GREAT SOCIAL MEDIA CONTENT FROM AUTOCONVERSE";
                    signup.InfoPage1Paragraph =
                        @"<p>Follow the instructions below to enroll your store with Connect4Content to receive great
                             social media content for your dealership. You will be prompted to connect your Facebook
                             page to the Social Integration platform where AutoConverse will be able to supply you with
                             content for your social networks. You will also be able to optionally connect your Twitter
                             handle (if desired).</p>" +

                        @"<br />To view the type of content you can expect to receive, visit the 
                                <a href='https://www.facebook.com/AutoConverseMagazine' target='_blank'> AutoConverse Facebook Page</a>.
                                Of course, you would only receive content associated with your brand." +

                        @"By enrolling your dealership you will:
                            <ul> 
                                <li>Receive great content for your Dealership Facebook Page</li> 
                                <li>Gain access to terrific Social Media tools and resources</li> 
                                <li>Receive a free Reputation + Social Media assessment of your dealership</li> 
                            </ul>";

                    signup.InfoPage2Paragraph =
                        @"<p>Please note that you will be able to approve (or deny) each individual piece of
                             information supplied to you prior to it appearing on your social networks.</p>";

                    signup.Instructions =
                        @"<p>For questions or assistance contact support at 1-866-726-9622 or email 
                             <a href='mailto:support@socialintegration.com?Subject=AutoConverse%20C4C%20Support' target='_top'>support@socialintegration.com</a> or 
                             <a href='http://www.socialintegration.com/contact-page/'> click here for online support</a>. 
                             </p> <p>To learn more about this program and our partnerships visit these informative blog posts:<br/> 
                             <a href='http://blogproautomotive.com/internet-marketing/live-event-social-content-syndication/'> Live Event Social Content Syndication: Old Concept, New Format</a></p>";

                    signup.CoverflowImage1 = "/contentsyndication/autoconverse/cas-sample-post.png";
                    signup.CoverflowImage2 = "/contentsyndication/autoconverse/cas-sample-post2.png";
                    signup.CoverflowImage3 = "/contentsyndication/autoconverse/cas-sample-post3.png";
                    signup.CoverflowImage4 = "/contentsyndication/autoconverse/cas-sample-post4.png";
                    signup.CoverflowImage5 = "/contentsyndication/autoconverse/cas-sample-post5.png";
                    signup.CoverflowImage6 = "/contentsyndication/autoconverse/cas-sample-post6.png";

                    break;

                case 4: //Ally
                    signup.HeaderBackgroundImage = "ally/cfc-header-bg.jpg";
                    signup.HeaderLogoImage = "ally/ally-cfc-header.png";
                    signup.HeaderBackgroundColor = "white";
                    signup.PageTitle = "ALLY Content Syndication Signup";

                    signup.InfoTitle = "Get Connected. Get Social.";
                    signup.InfoPage1Paragraph =
                        @"<p style=font-weight:bold; font-size: 13px;""><b>Enroll NOW for Ally Auto's Social Media Program.</b></p>
                        <p style='font-size: 13px;'>As a valued Ally Auto dealer, you have been invited to enroll in a <strong>free</strong> social media program that provides you access to Ally’s 
                        <strong>engaging</strong> automotive content for your dealership’s Facebook page. Get relevant automotive information that can help drive 
                        more consumers to your Facebook, and more consumers into your showroom. Simply enroll in the program and you will start 
                        receiving emails each week that provide the Ally post. The email gives you the option to approve or deny publishing the post. If 
                        you approve it, it is automatically posted to your Facebook page. It’s that simple!</p>" +
                        
                        //<p>As a valued Ally Auto dealer, you have been invited to enroll in a <strong>free</strong> social media program that provides you access to Ally’s <strong>engaging</strong> automotive content for your dealership’s Facebook page. Get relevant information today that will drive more consumers to your Facebook, and more consumers into your showroom. Simply enroll in the program and you will start receiving emails each week that provide the Ally post. The email gives you the option to approve or deny publishing the post. If you approve it, it is automatically posted to your Facebook page. It’s that simple!</p>" +
                        @"<p style='font-size: 13px;'><strong>TO ENROLL:</strong> Click on the ‘Connect Now’ button below.
                        You will need your Facebook business page username(s) 
                        and password(s) to complete the setup process. It only
                        takes a couple of minutes to complete.</p>" +

                        //@"<p><strong>To enroll:</strong> Click on the ‘Connect Now’ button. You will need your Facebook business page username(s) and password(s) to complete the setup process. It only takes a couple of minutes to complete.</p>" +
                        @"<p style='font-size: 13px;'>If you have setup questions, call 877-326-7624, Ext. 1.</p>" +
                        @"<p style='font-size: 13px;'>Remember, there is <strong>no cost</strong> to you. It is Ally’s way of showing you we are ‘all-in’ in serving our customers together.</p>";

                    //signup.InfoPage2Paragraph =
                    //    @"<p>Okay let's get started with the first step in getting some great engaging content delivered right to your email all ready to post with just a quick app.</p>";

                    signup.Instructions =
                        @"<p>If you connect with Facebook, you must LOGOUT of Facebook before you start this process.</p><p>OR</p><p>You must be logged in to your personal Facebook profile that has admin rights to any Facebook Pages you wish to enroll. You CAN NOT be possessing a dealership page." +
                        @"</p><p>During the connection process, Facebook will prompt you about giving us permission to post on your behalf. Neither SOCIALDEALER nor the sydicator will be publishing content to your personal profile. We will only be publishing content to the pages you enroll." +
                        @"To enroll more than one page, please complete this process for each page.</p><p>For questions or assistance contact support at <a href='tel:8773267624'>877-326-7624</a> opt 1. You may also email <a href='mailto:support@socialdealer.com?subject=Syndication Signup'>support@socialdealer.com</a> or visit our <a href='https://socialdealer1.zendesk.com/home' target='_blank'>Online Support Site</a>.</p>";

                    signup.CoverflowImage1 = "/contentsyndication/ally/ally-post-002.png";
                    signup.CoverflowImage2 = "/contentsyndication/ally/ally-post-003.png";
                    signup.CoverflowImage3 = "/contentsyndication/ally/ally-post-004b.png";
                    signup.CoverflowImage4 = "/contentsyndication/ally/ally-post-07.png";
                    signup.CoverflowImage5 = "/contentsyndication/ally/ally-post-08.png";
                    signup.CoverflowImage6 = "/contentsyndication/ally/ally-post-004b.png";

                    break;
                case 6: //Anthony Marano Company
                    signup.HeaderBackgroundImage = "carfax/carfax-cfc-header-bg.jpg";
                    signup.HeaderLogoImage = "marano/marano-oval-logo.png";
                    signup.HeaderBackgroundColor = "#007fb2";
                    signup.PageTitle = "Anthony Marano Company Content Syndication Signup";

                    signup.InfoTitle = "Lets Get Social!";
                    signup.InfoPage1Paragraph =
                        @"<p>You have been selected to receive access to FREE and ENGAGING social media content for your Facebook and Twitter feeds.  Our program automatically submits for your approval content that gets your fans involved and gets your brand out there. Prior to every post, you will receive an email showing you the post and giving you the chance to approve or deny the post before it gets published on your pages.</p>" +
                        @"<p>Not to worry, we won’t post anything you haven’t approved. You will need your Facebook and/or Twitter business page username(s) and password(s) to complete the setup process. It only takes a couple minutes to complete the process. For setup questions, call SOCIALDEALER Support at 877-326-7624 ext #1. </p>";

                    signup.InfoPage2Paragraph =
                        @"<p>Okay let's get started with the first step in getting some great engaging content delivered right to your email all ready to post with just a quick app.</p>";

                    signup.Instructions =
                        @"<p>If you connect with Facebook, you must LOGOUT of Facebook before you start this process.</p><p>OR</p><p>You must be logged in to your personal Facebook profile that has admin rights to any Facebook Pages you wish to enroll. You CAN NOT be possessing a dealership page." +
                        @"</p><p>During the connection process, Facebook will prompt you about giving us permission to post on your behalf. Neither SOCIALDEALER nor the sydicator will be publishing content to your personal profile. We will only be publishing content to the pages you enroll." +
                        @"To enroll more than one page, please complete this process for each page.</p><p>For questions or assistance contact support at <a href='tel:8773267624'>877-326-7624</a> opt 1. You may also email <a href='mailto:support@socialdealer.com?subject=Syndication Signup'>support@socialdealer.com</a> or visit our <a href='https://socialdealer1.zendesk.com/home' target='_blank'>Online Support Site</a>.</p>";

                    //TODO : Get the Image for Anthony Marano Company
                    signup.CoverflowImage1 = "/contentsyndication/carfax/carfax-cfc-post1.jpg";
                    signup.CoverflowImage2 = "/contentsyndication/carfax/carfax-cfc-post2.jpg";
                    signup.CoverflowImage3 = "/contentsyndication/carfax/carfax-cfc-post3.jpg";
                    signup.CoverflowImage4 = "/contentsyndication/carfax/carfax-cfc-post4.jpg";
                    signup.CoverflowImage5 = "/contentsyndication/carfax/carfax-cfc-post5.jpg";
                    signup.CoverflowImage6 = "/contentsyndication/carfax/carfax-cfc-post6.jpg";

                    break;
                case 8: //CATA
                    {
                       
                        signup.HeaderBackgroundImage = "";
                        signup.HeaderLogoImage = "cata/logo2.png";
                        signup.HeaderBackgroundColor = "#E8EDF1";
                        signup.PageTitle = "";
                        signup.UseShadowedBox = false;
                        signup.ShowSliderTitle = true;

                        signup.InfoTitle = "SIGN UP FOR ACCESS TO FREE SOCIAL MEDIA CONTENT FROM THE CATA";

                        signup.InfoPage1Paragraph =
                            @"<p style='font-size: 13px; line-height: 17px;'>Follow the instructions below to enroll your dealership with " +
                            @"<span style='color: #666666; font-family: Arial; font-size: 11pt; font-weight: bold; font-style: italic;'>Connect4Content</span>" +
                            @"to receive <span style='color: #666666; font-family: Arial; font-size: 11pt; font-weight: bold;'>FREE</span> " +
                            @"social media content for your dealership. You will be prompted to connect your dealership Facebook page " +
                            @"and Twitter handle to the <span style='color: #666666; font-family: Arial; font-size: 11pt; font-weight: bold;'>SOCIALDEALER</span> " +
                            @"platform where the CATA will be able to supply you with content " +
                            @"for your social networks.  All content you receive will be CATA-approved and you will be able to approve " +
                            @"(or deny) each individual piece of information prior to it appearing on your social networks." +
                            @"</p>" +

                            @"<p style='font-size: 13px; line-height: 17px;'>You will receive:</p>" +
                            @"<ul style='color: #666666; font-family: Arial; font-size: 11pt;list-style-image: url(http://cdn.socialintegration.com/contentsyndication/cata/bullet.jpg);'>" +
                            @"<li>Free CATA-approved posts for Facebook and Twitter</li><br />" +
                            @"<li>Free training and support from SOCIALDEALER</li><br />" +
                            @"<li>Access to free Reputation and Social Media tools</li></ul><br />" +

                            @"<p style='font-size: 13px; line-height: 17px;'><span style='color: #666666; font-family: Arial; font-size: 11pt;'> For questions or assistance contact support at  <span style='color: #666666; font-family: Arial; font-size: 11pt; font-weight: bold;'>877-326-7624 opt 1</span>  " +
                            @"or click on the chat button below. You may also email <span style='color: #666666; font-family: Arial; font-size: 11pt; font-weight: bold;'>support@socialdealer.com</span> or visit our Online Support Site. " +
                            @"</span></strong>" +


                            @"<p style='font-size: 13px; line-height: 17px;'><span style='color: #666666; font-family: Arial; font-size: 11pt;'>To learn more about this exciting program and this partnership visit these informative blog posts:</span></p>" +

                            @"<p style='font-size: 13px; line-height: 17px;'><span style='font-size: 13px; line-height: 17px;'><img src='http://cdn.socialintegration.com/contentsyndication/cata/blue-white-triangle-arrow.jpg' alt='Connect Now' width='15' />" +
                            @"<a href='http://www.socialdealer.com/SocialTrends/announcements/cata-socialdealer-chicago-auto-show/' target='_blank'>Chicago Auto Show Partners with SOCIALDEALER to Provide Local Dealers with Syndicated Social Media Content</a>" +
                            @"</span></p>" +

                            @"<p style='font-size: 13px; line-height: 17px;'><span style='font-size: 13px; line-height: 17px;'><img src='http://cdn.socialintegration.com/contentsyndication/cata/blue-white-triangle-arrow.jpg' alt='Connect Now' width='15' />" +
                            @"<a href='http://www.socialdealer.com/SocialTrends/announcements/cata-uso-bbq/' target='_blank'>The Chicago Automobile Trade Association Promotes USO BBQ with Social Advertising</a>" +
                            @"</span></p>";

                        signup.Header2 =
                            @"<div style='float: left;width: 100%;height: 70px;margin: 0 0 10px; text-align: center; background-image: url(http://cdn.socialintegration.com/contentsyndication/cata/bg_header2.jpg); background-repeat: repeat-x;background-position: top center;'>" +
                            @"<img src='http://cdn.socialintegration.com/contentsyndication/cata/header2.png' width='960' height='76' />" +
                            @"</div>";


                        signup.InfoPage2Paragraph =
                            @"<p style='font-size: 13px; line-height: 17px;'>Okay let's get started with the first step in getting some great engaging content delivered right to your email all ready to post with just a quick app.</p>";

                        signup.Instructions =
                            @"<p style='font-size: 13px; line-height: 17px;'>If you connect with Facebook, you must LOGOUT of Facebook before you start this process.</p><p>OR</p><p>You must be logged in to your personal Facebook profile that has admin rights to any Facebook Pages you wish to enroll. You CAN NOT be possessing a dealership page." +
                            @"</p><p style='font-size: 13px; line-height: 17px;'>During the connection process, Facebook will prompt you about giving us permission to post on your behalf. Neither SOCIALDEALER nor the sydicator will be publishing content to your personal profile. We will only be publishing content to the pages you enroll." +
                            @"To enroll more than one page, please complete this process for each page.</p><p>For questions or assistance contact support at <a href='tel:8773267624'>877-326-7624</a> opt 1. You may also email <a href='mailto:support@socialdealer.com?subject=Syndication Signup'>support@socialdealer.com</a> or visit our <a href='https://socialdealer1.zendesk.com/home' target='_blank'>Online Support Site</a>.</p>";

                        signup.CoverflowImage1 = "/contentsyndication/cata/USO_Slider2.jpg"; //Replace with new Image
                        signup.CoverflowImage2 = "/contentsyndication/cata/C4C-Slider-ImgA-CAS.jpg";
                        signup.CoverflowImage3 = "/contentsyndication/cata/C4C-Slider-ImgB-CAS.jpg";
                        signup.CoverflowImage4 = "/contentsyndication/cata/C4C-Slider-ImgC-CAS.jpg";
                        signup.CoverflowImage5 = "/contentsyndication/cata/C4C-Slider-ImgD-CAS.jpg";
                        signup.CoverflowImage6 = "/contentsyndication/cata/C4C-Slider-ImgE-CAS.jpg";
                    }
                    break;

                default:
                    signup.InfoTitle = "Lets Get Social!";
                    signup.HeaderBackgroundColor = "#007fb2";
                    signup.PageTitle = "Content Syndication Signup";
                    signup.InfoPage1Paragraph =
                        @"<p>You have been selected to receive access to FREE and ENGAGING social media content for your Facebook and Twitter feeds.  Our program automatically submits for your approval content that gets your fans involved and gets your brand out there. Prior to every post, you will receive an email showing you the post and giving you the chance to approve or deny the post before it gets published on your pages.</p>" +
                        @"<p>Not to worry, we won’t post anything you haven’t approved. You will need your Facebook and/or Twitter business page username(s) and password(s) to complete the setup process. It only takes a couple minutes to complete the process. For setup questions, call SOCIALDEALER Support at 877-326-7624 ext #1. </p>";

                    signup.InfoPage2Paragraph =
                        @"<p>Okay let's get started with the first step in getting some great engaging content delivered right to your email all ready to post with just a quick app.</p>";

                    signup.Instructions =
                        @"<p>If you connect with Facebook, you must LOGOUT of Facebook before you start this process.</p><p>OR</p><p>You must be logged in to your personal Facebook profile that has admin rights to any Facebook Pages you wish to enroll. You CAN NOT be possessing a dealership page." +
                        @"</p><p>During the connection process, Facebook will prompt you about giving us permission to post on your behalf. Neither SOCIALDEALER nor the sydicator will be publishing content to your personal profile. We will only be publishing content to the pages you enroll." +
                        @"To enroll more than one page, please complete this process for each page.</p><p>For questions or assistance contact support at <a href='tel:8773267624'>877-326-7624</a> opt 1. You may also email <a href='mailto:support@socialdealer.com?subject=Syndication Signup'>support@socialdealer.com</a> or visit our <a href='https://socialdealer1.zendesk.com/home' target='_blank'>Online Support Site</a>.</p>";

                    signup.CoverflowImage1 = "/contentsyndication/carfax/carfax-cfc-post1.jpg";
                    signup.CoverflowImage2 = "/contentsyndication/carfax/carfax-cfc-post2.jpg";
                    signup.CoverflowImage3 = "/contentsyndication/carfax/carfax-cfc-post3.jpg";
                    signup.CoverflowImage4 = "/contentsyndication/carfax/carfax-cfc-post4.jpg";
                    signup.CoverflowImage5 = "/contentsyndication/carfax/carfax-cfc-post5.jpg";
                    signup.CoverflowImage6 = "/contentsyndication/carfax/carfax-cfc-post6.jpg";

                    break;

            }

            return signup;
        }


		public static SyndicationRegistration GetSyndicationRegistrationCustom(SyndicationRegistration syndicationRegistration, long? syndicatorId, long resellerID)
		{
			
			if (syndicatorId.HasValue)
			{                
				switch (syndicatorId.Value)
				{
                    case 1:
                        {
                            syndicationRegistration.HeaderView = "_LexusRegistrationHeader";                            
                            syndicationRegistration.MarketingView = "_LexusRegistrationMarketing";
                            syndicationRegistration.WelcomeView = "_LexusRegistrationWelcome";
                            syndicationRegistration.ResellerName = "Lexus";
                        }
                        break;
                    case 4:
                        {
                            syndicationRegistration.HeaderView = "_AllyRegistrationHeader";
                            syndicationRegistration.MarketingView = "_AllyRegistrationMarketing";
                            syndicationRegistration.WelcomeView = "_LexusRegistrationWelcome";
                            syndicationRegistration.ResellerName = "Ally";
                        }
                        break;
                    case 2:
                        {
                            syndicationRegistration.HeaderView = "_CARFAXRegistrationHeader";
                            syndicationRegistration.MarketingView = "_CARFAXRegistrationMarketing";
                            syndicationRegistration.WelcomeView = "_LexusRegistrationWelcome";
                            syndicationRegistration.ResellerName = "CARFAX";
                        }
                        break;
                    case 3:
                        {
                            syndicationRegistration.HeaderView = "_AutoConverseRegistrationHeader";
                            syndicationRegistration.MarketingView = "_AutoConverseRegistrationMarketing";
                            syndicationRegistration.WelcomeView = "_LexusRegistrationWelcome";
                            syndicationRegistration.ResellerName = "AutoConverse";
                        }
                        break;
                    case 8:
                        {
                            syndicationRegistration.HeaderView = "_CATARegistrationHeader";
                            syndicationRegistration.MarketingView = "_CATARegistrationMarketing";
                            syndicationRegistration.WelcomeView = "_LexusRegistrationWelcome";
                            syndicationRegistration.ResellerName = "CATA";
                        }
                        break;
				}
			}
			else
			{
				//TODO RESELLER 
			}

			return syndicationRegistration;
		}

	    public static bool SaveSyndicationRegistration(SyndicationRegistration syndicationRegistration, string socialToken, ref List<string> problems)
	    {
			SignupSubmitInfoDTO signupSubmitInfoDto = new SignupSubmitInfoDTO();
		    signupSubmitInfoDto.AccessTokenUsed = syndicationRegistration.AccessToken;
		    signupSubmitInfoDto.SyndicatorID = syndicationRegistration.SyndicatorID;

			signupSubmitInfoDto.AccountName = syndicationRegistration.AccountName;
			signupSubmitInfoDto.BillingPhone = syndicationRegistration.Phone;
			signupSubmitInfoDto.CityName = syndicationRegistration.City;
			signupSubmitInfoDto.CompanyWebsiteURL = syndicationRegistration.WebSiteUrl;

			if (syndicationRegistration.FranchiseTypeIDs != null)
				signupSubmitInfoDto.FranchiseTypeIDs = syndicationRegistration.FranchiseTypeIDs.ToList();

			signupSubmitInfoDto.CountryID = syndicationRegistration.CountryID.GetValueOrDefault();
			signupSubmitInfoDto.PostalCode = syndicationRegistration.Zipcode;
			signupSubmitInfoDto.ResellerID = syndicationRegistration.ResellerID;
			signupSubmitInfoDto.StateID = syndicationRegistration.StateID.GetValueOrDefault();
			signupSubmitInfoDto.StreetAddress = syndicationRegistration.Address;
			signupSubmitInfoDto.VerticalID = syndicationRegistration.VerticalMarketID;
		    signupSubmitInfoDto.PreferredParentAccountID = syndicationRegistration.PreferedParentAccountID;

		    if (signupSubmitInfoDto.PreferredParentAccountID.HasValue && signupSubmitInfoDto.PreferredParentAccountID.Value == 0)
			    signupSubmitInfoDto.PreferredParentAccountID = null;

		    List<SignupSubmitSelectedPackageInfoDTO> selectedPackageInfoDto = new List<SignupSubmitSelectedPackageInfoDTO>();
		    string[] Packages = syndicationRegistration.SelectedPackages.Split(',');
		    foreach (string package in Packages)
		    {
			    SignupSubmitSelectedPackageInfoDTO dto = new SignupSubmitSelectedPackageInfoDTO();
			    dto.ResellerPackageID = package.ToLong();
			    selectedPackageInfoDto.Add(dto);
		    }
			signupSubmitInfoDto.SelectedResellerPackages = selectedPackageInfoDto;

		    signupSubmitInfoDto.UserEmailAddress = syndicationRegistration.Email;
		    signupSubmitInfoDto.UserFirstName = syndicationRegistration.FirstName;
		    signupSubmitInfoDto.UserLastName = syndicationRegistration.LastName;
			
			signupSubmitInfoDto.UniqueID = syndicationRegistration.FacebookPageID;
			signupSubmitInfoDto.SocialNetwork = SocialNetworkEnum.Facebook;
		    
			signupSubmitInfoDto.SocialAppID = syndicationRegistration.SocialAppID;

		    signupSubmitInfoDto.TimezoneID = 11; //CST
		    string timeZoneName = syndicationRegistration.TimeZoneAbrv;
		    int indx = timeZoneName.IndexOf("(");
			if (indx != -1)
			{
				timeZoneName = timeZoneName.Substring(indx + 1).Replace(")", "").Replace("Daylight", "Standard");
				long? timeZoneID = ProviderFactory.TimeZones.FindByName(timeZoneName);
				if (timeZoneID.HasValue)
					signupSubmitInfoDto.TimezoneID = timeZoneID.Value;
			}

		    signupSubmitInfoDto.Token = "";

			FacebookAccount accounts = FacebookAPI.GetFacebookAccounts(socialToken);
		    if (accounts != null)
		    {
				Account pageAccount = accounts.data.Where(t => t.id == syndicationRegistration.FacebookPageID).FirstOrDefault();
			    if (pageAccount != null)
			    {
				    signupSubmitInfoDto.Token = pageAccount.access_token;
				    signupSubmitInfoDto.ScreenName = pageAccount.name;

				    FacebookPage facebookPage = FacebookAPI.GetFacebookPageInformation(syndicationRegistration.FacebookPageID, pageAccount.access_token);
				    if (facebookPage != null && string.IsNullOrEmpty(facebookPage.facebookResponse.message))
				    {
					    signupSubmitInfoDto.URI = facebookPage.page.link;
					    signupSubmitInfoDto.WebsiteURL = facebookPage.page.website;

					    FaceBookUserPicture picture = FacebookAPI.FaceBookUserPicture(syndicationRegistration.FacebookPageID);
					    if (picture != null)
						    signupSubmitInfoDto.PictureURL = picture.picture.data.url;
				    }
			    }
		    }
			
			if (string.IsNullOrEmpty(signupSubmitInfoDto.Token))
		    {
				AuditSocialConfig asc = new AuditSocialConfig();
			    asc.SocialAppId = syndicationRegistration.SocialAppID;
				asc.PageID = syndicationRegistration.FacebookPageID;
				ProviderFactory.Logging.Audit(AuditLevelEnum.Error, AuditTypeEnum.SocialConfiguration, "Syndication Registration Social Config Save - could not get page Token", asc, null);

			    problems.Add("Access to Facebook has failed.");
			    return false;
		    }

		    SaveEntityDTO<SignupSubmitInfoDTO> saveEntityDto = new SaveEntityDTO<SignupSubmitInfoDTO>(signupSubmitInfoDto, UserInfoDTO.System);

		    SaveEntityDTO<SignupSubmitInfoDTO> saveDto = ProviderFactory.Products.SignupSubmit(saveEntityDto);
			if (saveDto.Problems.Any())
			{
				problems.AddRange(saveDto.Problems);
				return false;
			}

		    return true;
	    }

	    public static SyndicationRegistration PopulateSyndicationRegistration(SyndicationSelfRegistrationDTO selfRegistrationDto)
	    {
		    SyndicationRegistration syndicationRegistration = new SyndicationRegistration();

			syndicationRegistration.ResellerID = selfRegistrationDto.ResellerID;
			syndicationRegistration.SyndicatorID = selfRegistrationDto.SyndicatorID;
		    syndicationRegistration.AccessToken = selfRegistrationDto.Token.ToString();

			string FacebookStateContext = Guid.NewGuid().ToString();
			syndicationRegistration.FacebookStateContext = FacebookStateContext;

			SocialLoginUrl socialLoginUrl = AdminService.GetSocialLoginUrl(null, selfRegistrationDto.ResellerID, SocialNetworkEnum.Facebook, FacebookStateContext);
		    syndicationRegistration.FacebookLoginURL = socialLoginUrl.LoginUrl;
		    syndicationRegistration.SocialAppID = socialLoginUrl.SocialAppID;

			LandingPagesService.GetSyndicationRegistrationCustom(syndicationRegistration, selfRegistrationDto.SyndicatorID, selfRegistrationDto.ResellerID);

		    SignupSetupInfoDTO signupSetupInfoDto = ProviderFactory.Products.GetSignupInfo(selfRegistrationDto);

			foreach (SignupPackageInfoDTO package in signupSetupInfoDto.PackageInfos)
		    {
				RegistrationPackages regPack = new RegistrationPackages();
			    regPack.ID = package.ResellerPackageID;
			    regPack.Name = package.PackageName;
			    regPack.Selected = package.Default;

			    syndicationRegistration.Packages.Add(regPack);
		    }
		    syndicationRegistration.SelectedPackages = string.Join(",", syndicationRegistration.Packages.Where(p => p.Selected).Select(p => p.ID));

			foreach (VerticalMarketDTO vertical in signupSetupInfoDto.VerticalMarkets)
			{
				RegistrationPackagesVerticalMarket verticalMarket = new RegistrationPackagesVerticalMarket();
				verticalMarket.ID = vertical.ID;
				verticalMarket.Name = vertical.Name;

				syndicationRegistration.VerticalMarkets.Add(verticalMarket);
			}
		    syndicationRegistration.VerticalMarketID = selfRegistrationDto.VerticalMarketID.GetValueOrDefault();
		    syndicationRegistration.PreferedParentAccountID = selfRegistrationDto.PreferedParentAccountID.GetValueOrDefault();

		    return syndicationRegistration;
	    }
    }

}
