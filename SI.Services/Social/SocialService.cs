﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using SI.BLL;
using SI.Extensions;
using SI.Services.Framework.Models;
using SI.Services.Social.Models;
using SI.DTO;
using YouTube;
using YouTube.Entities;
using Connectors.Parameters;
using Facebook.Entities;
using Connectors;
using Newtonsoft.Json;
using Connectors.Social;
using Twitter.Entities;
using Google.Model;
using System.Web;
using Connectors.Social.Parameters;
using HtmlAgilityPack;
using TwitterPostStatistics = SI.Services.Social.Models.TwitterPostStatistics;

namespace SI.Services.Social
{
    public static class SocialService
    {


        public static List<PostActivity> DealerPostActivityGrid(UserInfoDTO userInfo, PagedGridRequest pagedGridRequest, ref int totalCount)
        {
            PostTargetSearchRequestDTO req = new PostTargetSearchRequestDTO(userInfo)
            {
                SortBy = 0, // 1=Status, 2=Schedule Date, 3=Title
                SortAscending = false,
                StartIndex = pagedGridRequest.Skip,
                EndIndex = pagedGridRequest.Skip + pagedGridRequest.Take
            };

            if (pagedGridRequest.Sorting.Any())
            {
                PagedGridSortDescriptor sort = pagedGridRequest.Sorting.First();
                //4=User, 5=Type, 6=Dealer
                switch (sort.Field)
                {
                    case "ScheduleDate": req.SortBy = 7; break;
                    case "Status": req.SortBy = 1; break;
                    case "StatusName": req.SortBy = 2; break;
                    case "Title": req.SortBy = 3; break;
                    case "PostedBy": req.SortBy = 4; break;
                    case "Type": req.SortBy = 5; break;
                    case "LocationName": req.SortBy = 6; break;
                }

                req.SortAscending = sort.Ascending;
            }

            foreach (PagedGridFilterDescriptor filter in pagedGridRequest.Filtering)
            {
                if (filter.Field == "OemID")
                    req.FranchiseTypeIDs.Add(filter.Value.ToLong());

                if (filter.Field == "NetworkID")
                    req.SocialNetworks.Add((SocialNetworkEnum)filter.Value.ToLong());

                if (filter.Field == "LocationID")
                    req.AccountIDs.Add(filter.Value.ToLong());

                if (filter.Field == "TimeFrameFilter")
                {
                    int days = filter.Value.ToInteger() * -1;
                    req.MinDateScheduled = new SIDateTime(userInfo.EffectiveUser.CurrentDateTime.LocalDate.Value.AddDays(days), userInfo.EffectiveUser.ID.Value);
                }

                if (filter.Field == "Status")
                {
                    switch (filter.Value)
                    {
                        case "Published":
                            req.PostStatus = PostStatusEnum.Published;
                            break;
                        case "Failed":
                            req.PostStatus = PostStatusEnum.Failed;
                            break;
                        case "Scheduled":
                            req.PostStatus = PostStatusEnum.Scheduled;
                            break;
                    }
                }

                if (filter.Field == "Type")
                {
                    switch (filter.Value)
                    {
                        case "Photo":
                            req.PostType = PostTypeEnum.Photo;
                            break;
                        case "Link":
                            req.PostType = PostTypeEnum.Link;
                            break;
                        case "Status":
                            req.PostType = PostTypeEnum.Status;
                            break;
                    }
                }

                if (filter.Field == "PostedBy")
                {
                    req.PostedByUserID = filter.Value.ToLong();
                }
            }

            PostTargetSearchResultsDTO res = ProviderFactory.Social.PostTargetSearch(userInfo, req);
            totalCount = res.TotalRecords;

            List<PostActivity> posts = new List<PostActivity>();

            foreach (PostTargetSearchResultItemDTO dto in res.Items)
            {
                PostActivity postActivity = new PostActivity();
                postActivity.Actions = dto.Actions.GetValueOrDefault();
                postActivity.Title = dto.Title;

                postActivity.PostID = dto.PostID;
                postActivity.PostTargetID = dto.PostTargetID;

                postActivity.PostedBy = string.Format("{0} {1}", dto.FirstName, dto.LastName);
                postActivity.Status = dto.Status.ToString();
                postActivity.StatusName = dto.StatusName;
                postActivity.Type = dto.Type.ToString();

                postActivity.FailureMessage = dto.FailureResponse;
                if (dto.Status == PostStatusEnum.Failed && string.IsNullOrEmpty(dto.FailureResponse))
                    postActivity.FailureMessage = "An unknown error has occurred. Please retry your Post.";

                postActivity.PostImageURL = dto.PostImageURL.IsNullOptional("");
                postActivity.TimeZoneName = "";

                if (dto.StatusDate != null)
                {
                    postActivity.ScheduleDate = dto.StatusDate.LocalDate;
                    postActivity.StatusDateText = dto.StatusDate.LocalDate.Value.ToString("M/dd/yy h:mm tt");
                    postActivity.TimeZoneName = dto.StatusDate.TimeZone.StandardName;
                }
                else
                {
                    postActivity.StatusDateText = dto.CreateDate.LocalDate.Value.ToString("M/dd/yy h:mm tt");
                    postActivity.TimeZoneName = string.Format("Immediate {0}", dto.CreateDate.TimeZone.StandardName);
                }

                postActivity.NetworkID = (long)dto.SocialNetwork;
                postActivity.NetworkName = dto.SocialNetwork.ToString().ToLower();
                postActivity.SocialUrl = dto.SocialNetworkPageURL;

                postActivity.LocationID = dto.AccountID;
                postActivity.LocationName = dto.AccountName;

                postActivity.CanEdit = dto.CanEdit;

                posts.Add(postActivity);
            }
            return posts;
        }

        public static List<MessageAccounts> MessageAccountsGrid(UserInfoDTO userInfo, PagedGridRequest pagedGridRequest, long? syndicatorID, ref int totalCount)
        {
            List<MessageAccounts> accounts = new List<MessageAccounts>();

            PublishAccountNetworkListSearchRequestDTO req = new PublishAccountNetworkListSearchRequestDTO();
            req.AccountSearchString = "";
            req.AccountIDs = new List<long>();
            req.FranchiseTypeIDs = new List<long>();
            req.SocialNetworkIDs = new List<long>();
            req.SyndicatorID = syndicatorID;

            foreach (PagedGridFilterDescriptor filter in pagedGridRequest.Filtering)
            {
                if (filter.Field == "DealerLocation.Name")
                    req.AccountSearchString = filter.Value;

                if (filter.Field == "DealerLocation.OemID")
                    req.FranchiseTypeIDs.Add(filter.Value.ToLong());

                if (filter.Field == "NetworkID")
                    req.SocialNetworkIDs.Add(filter.Value.ToLong());

                if (filter.Field == "DealerLocation.LocationID")
                    req.AccountIDs.Add(filter.Value.ToLong());

                if (filter.Field == "DealerLocation.VirtualGroupID")
                    req.VirtualGroupID = filter.Value.ToLong();

                if (filter.Field == "DealerLocation.StateAbrev")
                    req.StateID = filter.Value.ToLong();
            }

            req.SortBy = 2;
            req.SortAscending = true;

            if (pagedGridRequest.Sorting.Any())
            {
                PagedGridSortDescriptor sort = pagedGridRequest.Sorting.First();
                switch (sort.Field)
                {
                    case "DealerLocation.Name": req.SortBy = 1; break;
                    case "Network": req.SortBy = 2; break;
                    case "DealerLocation.StateAbrev": req.SortBy = 3; break;
                    case "LastPost": req.SortBy = 4; break;
                }

                req.SortAscending = sort.Ascending;
            }

            req.StartIndex = pagedGridRequest.Skip;
            req.EndIndex = req.StartIndex + pagedGridRequest.Take;

            PublishAccountNetworkListSearchResultDTO resultDto = ProviderFactory.Social.GetPublishAccountNetworkListItems(userInfo, req);

            foreach (PublishAccountNetworkListItemDTO result in resultDto.Items)
            {
                MessageAccounts account = new MessageAccounts();

                //TODO GET AUTO APPROVE FROM PRODUCT CONFIG
                account.IsAutoApprove = result.IsAutoApproved;

                account.DealerLocation.Name = result.Name;
                account.DealerLocation.LocationID = result.AccountID;
                account.DealerLocation.Address = result.Address;
                account.DealerLocation.City = result.City;
                account.DealerLocation.StateAbrev = result.StateAbbr;
                account.DealerLocation.Zipcode = result.Zipcode;

                if (result.LastPostDate != null)
                    account.LastPost = result.LastPostDate.LocalDate.GetValueOrDefault();

                if (result.NextScheduledPostDate != null)
                    account.SchedulePostDate = result.NextScheduledPostDate.LocalDate.GetValueOrDefault();

                account.NetworkID = result.SocialNetworkID;
                account.Network = result.Network;
                account.IsCredentialActive = result.IsCredentialActive;
                account.IsSocialAppActive = result.IsSocialAppActive;
                account.ResellerID = result.ResellerID;
                account.CredentialID = result.CredentialID;
                account.SocialNetworkUniqueID = result.SocialNetworkUniqueID;
                account.SocialNetworkScreenName = result.SocialNetworkScreenName;
                account.SocialNetworkPictureURL = result.SocialNetworkPictureURL;
                account.SocialNetworkURL = result.SocialNetworkURL;

                account.DealerLocation.OemID = result.Franchises.Select(f => f.ID).FirstOrDefault();
                account.DealerLocation.OemName = string.Join(", ", result.Franchises.Select(f => f.Name));

                account.TZName = result.TimezoneName.Substring(result.TimezoneName.IndexOf(")") + 2);
                account.TZOffsetMinutes = result.TimezoneOffsetMinutesFromLoggedInUsersZone;

                if (result.TimezoneOffsetMinutesFromLoggedInUsersZone < 0)
                {
                    TimeSpan timeToPost = TimeSpan.FromMinutes(15 + Math.Abs(result.TimezoneOffsetMinutesFromLoggedInUsersZone));
                    if (timeToPost.Hours > 0)
                        account.TimeToPost = string.Format("(in {0} hour{1}, {2} min{3})", timeToPost.Hours, timeToPost.Hours > 1 ? "s" : "", timeToPost.Minutes, timeToPost.Minutes > 1 ? "s" : "");
                    else
                        account.TimeToPost = string.Format("(in {0} min{1})", timeToPost.Minutes, timeToPost.Minutes > 1 ? "s" : "");
                }
                else
                    account.TimeToPost = "(in 15 mins)";

                account.UniquenessCount = resultDto.Items.Count(i => i.SocialNetworkUniqueID == account.SocialNetworkUniqueID);

                accounts.Add(account);
            }

            totalCount = resultDto.TotalRecords;

            return accounts;


        }

        public static LinkScrape AutoLinkScrap(string url)
        {
            LinkScrape linkScrape = new LinkScrape();

            if (url.ToLower().Contains("youtube.com"))
            {
                User YoutubeUser = new User();
                YouTubeVideoInfo youTubeVideoInfo = YoutubeUser.GetVideoInfoforPublish(url);
                if (youTubeVideoInfo != null)
                {
                    linkScrape.IsValidUrl = true;
                    linkScrape.PageDescription = youTubeVideoInfo.Description;
                    linkScrape.PageImages.Add(youTubeVideoInfo.Image);
                    linkScrape.PageTitle = youTubeVideoInfo.Title;
                    linkScrape.PageUrl = url;
                }
            }
            else
            {
                LinkPreviewDTO linkPreview = ProviderFactory.Social.GetLinkPreview(url);
                if (linkPreview != null)
                {
                    linkScrape.IsValidUrl = linkPreview.IsValidUrl;
                    linkScrape.PageDescription = linkPreview.PageDescription;
                    linkScrape.PageImages.AddRange(linkPreview.PageImages);
                    linkScrape.PageTitle = linkPreview.PageTitle;
                    linkScrape.PageUrl = linkPreview.PageUrl;
                }
            }
            return linkScrape;
        }

        public static bool SavePublish(ComposeMessage composeMessage, UserInfoDTO userInfo, ref List<string> problems)
        {
            bool Success = false;

            PostDTO postDto = new PostDTO();
            postDto.ID = composeMessage.PostID;
            postDto.PostCategoryID = composeMessage.CategoryID.GetValueOrDefault();
            postDto.SyndicatorID = composeMessage.SyndicatorID;

            if (composeMessage.IsRequiresApprovals && composeMessage.ApprovalExpires != "1/1/1900".ToDateTime())
                postDto.ApprovalExpirationDate = new SIDateTime(composeMessage.ApprovalExpires, userInfo.EffectiveUser.ID.GetValueOrDefault());

            if (composeMessage.IsSendNow == false)
                postDto.ScheduleDate = new SIDateTime(composeMessage.SendDate, userInfo.EffectiveUser.ID.GetValueOrDefault());

            postDto.Source = PostSourceEnum.Publish;

            if (composeMessage.SelectedAllAccounts == false)
            {
                string[] targets = composeMessage.Targets.Split(',');
                foreach (string target in targets)
                {
                    postDto.Targets.Add(new PostTargetDTO { CredentialID = target.ToLong() });
                }
            }
            else
            {
                List<MessageAccounts> accounts = new List<MessageAccounts>();

                PublishAccountNetworkListSearchRequestDTO req = new PublishAccountNetworkListSearchRequestDTO();
                req.AccountSearchString = "";
                req.AccountIDs = new List<long>();
                req.FranchiseTypeIDs = new List<long>();
                req.SocialNetworkIDs = new List<long>();
                req.SyndicatorID = composeMessage.SyndicatorID;
                req.SortBy = 2;
                req.SortAscending = true;
                req.StartIndex = 0;
                req.EndIndex = 5000;

                if (!string.IsNullOrEmpty(composeMessage.AccountFilterJson))
                {
                    List<KendoFilter> kfilters = new List<KendoFilter>();

                    dynamic compositeKendoFilters = JsonConvert.DeserializeObject<dynamic>(composeMessage.AccountFilterJson);
                    if (compositeKendoFilters != null)
                    {
                        foreach (var compositeKendoFilter in compositeKendoFilters)
                        {
                            if (compositeKendoFilter != null)
                            {
                                if (compositeKendoFilter.filters != null)
                                {
                                    foreach (var filter in compositeKendoFilter.filters)
                                    {
                                        KendoFilter kf = new KendoFilter();
                                        kf.field = filter.field;
                                        kf.@operator = filter.@operator;
                                        kf.value = filter.value;

                                        kfilters.Add(kf);
                                    }
                                }
                                else
                                {
                                    KendoFilter kf = new KendoFilter();
                                    kf.field = compositeKendoFilter.field;
                                    kf.@operator = compositeKendoFilter.@operator;
                                    kf.value = compositeKendoFilter.value;

                                    kfilters.Add(kf);
                                }
                            }
                        }
                    }

                    foreach (KendoFilter filter in kfilters)
                    {
                        if (filter.field == "DealerLocation.Name")
                            req.AccountSearchString = filter.value;

                        if (filter.field == "DealerLocation.OemID")
                            req.FranchiseTypeIDs.Add(filter.value.ToLong());

                        if (filter.field == "NetworkID")
                            req.SocialNetworkIDs.Add(filter.value.ToLong());

                        if (filter.field == "DealerLocation.LocationID")
                            req.AccountIDs.Add(filter.value.ToLong());

                        if (filter.field == "DealerLocation.VirtualGroupID")
                            req.VirtualGroupID = filter.value.ToLong();
                    }
                }

                PublishAccountNetworkListSearchResultDTO resultDto = ProviderFactory.Social.GetPublishAccountNetworkListItems(userInfo, req);

                foreach (PublishAccountNetworkListItemDTO result in resultDto.Items)
                {
                    MessageAccounts account = new MessageAccounts();
                    account.CredentialID = result.CredentialID;
                    account.SocialNetworkUniqueID = result.SocialNetworkUniqueID;

                    accounts.Add(account);
                }

                foreach (MessageAccounts target in accounts)
                {
                    postDto.Targets.Add(new PostTargetDTO { CredentialID = target.CredentialID.GetValueOrDefault() });
                }
            }

            postDto.Title = composeMessage.Title;

            if (composeMessage.IsNoThumbNail)
                composeMessage.LinkImageUrl = "";

            postDto.MasterRevision = new PostRevisionDTO() { Link = composeMessage.LinkUrl, Message = composeMessage.Message, ImageURL = composeMessage.LinkImageUrl, LinkDescription = composeMessage.LinkMessage, LinkTitle = composeMessage.LinkTitle };

            postDto.Type = PostTypeEnum.Status;
            if (!string.IsNullOrEmpty(composeMessage.LinkUrl) && string.IsNullOrEmpty(composeMessage.ImageUrl))
                postDto.Type = PostTypeEnum.Link;

            if (!string.IsNullOrEmpty(composeMessage.ImageUrl))
            {
                string[] images = composeMessage.ImageUrl.Split(',');
                foreach (string image in images)
                {
                    postDto.Images.Add(new PostImageDTO { URL = image });
                }

                postDto.Type = PostTypeEnum.Photo;
            }

            if (postDto.SyndicatorID.GetValueOrDefault(0) > 0)
            {
                postDto.SyndicationOptions = new SyndicationPublishOptionsDTO();

                postDto.SyndicationOptions.AllowChangeImage = composeMessage.PublishOptions.AllowChangeImage;
                postDto.SyndicationOptions.AllowChangeLinkText = composeMessage.PublishOptions.AllowChangeLinkText;
                postDto.SyndicationOptions.AllowChangeLinkThumb = composeMessage.PublishOptions.AllowChangeLinkThumb;
                postDto.SyndicationOptions.AllowChangeLinkURL = composeMessage.PublishOptions.AllowChangeLinkURL;
                postDto.SyndicationOptions.AllowChangeMessage = composeMessage.PublishOptions.AllowChangeMessage;
                postDto.SyndicationOptions.AllowChangePublishDate = composeMessage.PublishOptions.AllowChangePublishDate;
                postDto.SyndicationOptions.AllowChangeTargets = composeMessage.PublishOptions.AllowChangeTargets;
                postDto.SyndicationOptions.ForbidFacebookTargetChange = composeMessage.PublishOptions.ForbidFacebookTargetChange;
                postDto.SyndicationOptions.ForbidGoogleTargetChange = composeMessage.PublishOptions.ForbidGoogleTargetChange;
                postDto.SyndicationOptions.ForbidTwitterTargetChange = composeMessage.PublishOptions.ForbidTwitterTargetChange;
                postDto.SyndicationOptions.MaxDaysAfterOriginalPublishDate = composeMessage.PublishOptions.MaxDaysAfterOriginalPublishDate;
                postDto.SyndicationOptions.MaxDaysBeforeOriginalPublishDate = composeMessage.PublishOptions.MaxDaysBeforeOriginalPublishDate;
                postDto.SyndicationOptions.RequireImage = composeMessage.PublishOptions.RequireImage;
                postDto.SyndicationOptions.RequireLinkURL = composeMessage.PublishOptions.RequireLinkURL;
                postDto.SyndicationOptions.RequireMessage = composeMessage.PublishOptions.RequireMessage;
            }

            SaveEntityDTO<PostDTO> saveEntityDto = new SaveEntityDTO<PostDTO>(postDto, userInfo);

            SaveEntityDTO<PostDTO> savedEntityDto = ProviderFactory.Social.Save(saveEntityDto);

            if (!savedEntityDto.Problems.Any())
            {
                Success = true;
                composeMessage.PostID = savedEntityDto.Entity.ID.GetValueOrDefault();
            }
            else
                problems = savedEntityDto.Problems;

            return Success;
        }

        public static ComposeMessage GetPost(UserInfoDTO userInfo, long? postId)
        {
            ComposeMessage composeMessage = new ComposeMessage();

            GetEntityDTO<PostDTO> postDto = ProviderFactory.Social.GetPostByID(userInfo, postId.GetValueOrDefault(), true);

            if (!postDto.Problems.Any() && postDto.Entity != null)
            {
                composeMessage.PostID = postDto.Entity.ID.GetValueOrDefault();
                composeMessage.CategoryID = postDto.Entity.PostCategoryID;
                composeMessage.SyndicatorID = postDto.Entity.SyndicatorID;

                composeMessage.ImageUrl = "";
                if (postDto.Entity.Images.Any())
                    composeMessage.ImageUrl = string.Join(",", postDto.Entity.Images.Select(i => i.URL));

                composeMessage.IsAllowRevisions = false;
                composeMessage.LinkImageUrl = postDto.Entity.MasterRevision.ImageURL.IsNullOptional("");
                composeMessage.LinkMessage = postDto.Entity.MasterRevision.LinkDescription;
                composeMessage.LinkTitle = postDto.Entity.MasterRevision.LinkTitle;
                composeMessage.LinkUrl = postDto.Entity.MasterRevision.Link;
                composeMessage.Message = postDto.Entity.MasterRevision.Message;

                if (postDto.Entity.ScheduleDate != null)
                    composeMessage.SendDate = postDto.Entity.ScheduleDate.LocalDate.GetValueOrDefault();

                composeMessage.Targets = string.Join(",", postDto.Entity.Targets.Select(t => t.CredentialID));
                composeMessage.Title = postDto.Entity.Title;
                composeMessage.Problems = "";
                composeMessage.IsRequiresApprovals = postDto.Entity.ApprovalExpirationDate != null;

                SIDateTime approvalExpire = postDto.Entity.ApprovalExpirationDate ?? userInfo.EffectiveUser.CurrentDateTime;
                composeMessage.ApprovalExpires = approvalExpire.LocalDate.GetValueOrDefault();
                composeMessage.ApprovalExpiresMax = approvalExpire.Add(TimeSpan.FromDays(28)).LocalDate.GetValueOrDefault();

                if (postDto.Entity.Type == PostTypeEnum.Link)
                {
                    composeMessage.IsLinkPostType = true;
                    composeMessage.IsNoThumbNail = composeMessage.LinkImageUrl == "";
                }

                if (composeMessage.SyndicatorID.GetValueOrDefault(0) > 0)
                {
                    composeMessage.PublishOptions.AllowChangeImage = postDto.Entity.SyndicationOptions.AllowChangeImage;
                    composeMessage.PublishOptions.AllowChangeLinkText = postDto.Entity.SyndicationOptions.AllowChangeLinkText;
                    composeMessage.PublishOptions.AllowChangeLinkThumb = postDto.Entity.SyndicationOptions.AllowChangeLinkThumb;
                    composeMessage.PublishOptions.AllowChangeLinkURL = postDto.Entity.SyndicationOptions.AllowChangeLinkURL;
                    composeMessage.PublishOptions.AllowChangeMessage = postDto.Entity.SyndicationOptions.AllowChangeMessage;
                    composeMessage.PublishOptions.AllowChangePublishDate = postDto.Entity.SyndicationOptions.AllowChangePublishDate;
                    composeMessage.PublishOptions.AllowChangeTargets = postDto.Entity.SyndicationOptions.AllowChangeTargets;
                    composeMessage.PublishOptions.ForbidFacebookTargetChange = postDto.Entity.SyndicationOptions.ForbidFacebookTargetChange;
                    composeMessage.PublishOptions.ForbidGoogleTargetChange = postDto.Entity.SyndicationOptions.ForbidGoogleTargetChange;
                    composeMessage.PublishOptions.ForbidTwitterTargetChange = postDto.Entity.SyndicationOptions.ForbidTwitterTargetChange;
                    composeMessage.PublishOptions.MaxDaysAfterOriginalPublishDate = postDto.Entity.SyndicationOptions.MaxDaysAfterOriginalPublishDate;
                    composeMessage.PublishOptions.MaxDaysBeforeOriginalPublishDate = postDto.Entity.SyndicationOptions.MaxDaysBeforeOriginalPublishDate;
                    composeMessage.PublishOptions.RequireImage = postDto.Entity.SyndicationOptions.RequireImage;
                    composeMessage.PublishOptions.RequireLinkURL = postDto.Entity.SyndicationOptions.RequireLinkURL;
                    composeMessage.PublishOptions.RequireMessage = postDto.Entity.SyndicationOptions.RequireMessage;

                    //composeMessage.PublishOptions.MinNewPublishDate = postDto.Entity.SyndicationOptions.MinNewPublishDate.LocalDate;
                    //composeMessage.PublishOptions.MaxNewPublishDate = postDto.Entity.SyndicationOptions.MaxNewPublishDate.LocalDate;
                    //composeMessage.PublishOptions.OriginalPublishDate = postDto.Entity.SyndicationOptions.OriginalPublishDate.LocalDate.GetValueOrDefault();
                }

            }
            else
                composeMessage.Problems = string.Join(", ", postDto.Problems.Select(p => p));

            return composeMessage;
        }



        public static List<SelectListItem> GetDealerPostActivityAccounts(UserInfoDTO userInfo, string dealerFilter, long dealerIDFilter)
        {
            List<SelectListItem> items = new List<SelectListItem>();

            if (dealerIDFilter == 0)
            {
                AccountSearchRequestDTO requestDto = new AccountSearchRequestDTO();
                requestDto.UserInfo = userInfo;
                requestDto.SearchString = dealerFilter;
                requestDto.StartIndex = 0;
                requestDto.EndIndex = 50;
                requestDto.SortBy = 2;
                requestDto.SortAscending = true;

                AccountSearchResultDTO resultDtos = ProviderFactory.Security.SearchAccountsForPublish(requestDto);

                foreach (AccountListItemDTO account in resultDtos.Items)
                {
                    SelectListItem selectListItem = new SelectListItem();
                    selectListItem.Text = account.Name;
                    selectListItem.Value = account.ID.ToString();

                    items.Add(selectListItem);
                }
            }
            else
            {
                SelectListItem selectListItem = new SelectListItem();
                selectListItem.Text = ProviderFactory.Security.GetAccountNameByID(dealerIDFilter);
                selectListItem.Value = dealerIDFilter.ToString();
                selectListItem.Selected = true;

                items.Add(selectListItem);
            }

            return items;
        }

        public static string GetAccountName(long currentContextAccountId)
        {
            return ProviderFactory.Security.GetAccountNameByID(currentContextAccountId);
        }

        public static List<MessageTypes> GetPublishMessageTypes(UserInfoDTO userInfo)
        {
            List<MessageTypes> messageTypes = new List<MessageTypes>();

            List<PublishModeDTO> publishModeDtos = ProviderFactory.Social.GetPublishModes(userInfo);
            foreach (PublishModeDTO dto in publishModeDtos)
            {
                MessageTypes message = new MessageTypes();
                message.Text = dto.Name;
                message.Value = dto.Name;
                message.Logo = dto.LogoURL.IsNullOptional("");
                message.SyndicatorID = dto.SyndicatorID;
                message.AllowPostNow = dto.AllowPostNow;
                message.ForceApprovals = dto.ForceApprovals;

                if (dto.SyndicatorID.HasValue)
                {
                    message.Text = string.Format("Syndication - {0}", dto.Name);
                    if (userInfo.EffectiveUser.CanPublishSyndicate)
                        messageTypes.Add(message);
                }
                else
                    messageTypes.Add(message);
            }

            return messageTypes;
        }

        public static string GetPostTargetMessage(long? id)
        {
            PostTargetPublishInfoDTO dto = ProviderFactory.Social.GetPostTargetPublishInfo(id.GetValueOrDefault(), null);
            if (dto != null)
                return dto.Message;

            return string.Empty;
        }

        public static string GetPostTargetLink(long? id)
        {
            PostTargetPublishInfoDTO dto = ProviderFactory.Social.GetPostTargetPublishInfo(id.GetValueOrDefault(), null);
            if (dto != null)
                return dto.Link;

            return string.Empty;
        }

        public static List<ContentLibraryRSSItems> GetRSSItems(UserInfoDTO userInfo, PagedGridRequest pagedGridRequest, string search, int? timeframe, string franchise, ref int totalCount)
        {
            List<ContentLibraryRSSItems> rssItems = new List<ContentLibraryRSSItems>();

            RSSSearchRequestDTO req = new RSSSearchRequestDTO()
            {
                SortBy = 2, // //sort 1 = id, 2=pub date, 3= date created
                SortAscending = false,
                StartIndex = pagedGridRequest.Skip,
                EndIndex = pagedGridRequest.Skip + pagedGridRequest.Take,
                SearchText = search
            };

            if (!string.IsNullOrEmpty(franchise) && franchise != "All Franchises")
                req.Franchise = franchise.Trim();

            if (!timeframe.HasValue)
                timeframe = 30;

            req.MaxPubDate = SIDateTime.Now;
            req.MinPubDate = SIDateTime.Now.Subtract(new TimeSpan(timeframe.GetValueOrDefault() + 1, 0, 0, 0));


            RSSSearchResultDTO rssResult = ProviderFactory.RSS.SearchRSSItems(userInfo, req);
            totalCount = rssResult.TotalRecords;

            foreach (RSSItemDTO dto in rssResult.Items)
            {
                ContentLibraryRSSItems item = new ContentLibraryRSSItems();
                item.RSSItemID = dto.ID;

                item.ImageURL = "";
                if (dto.MainImage.IsNullOptional("").Contains("http"))
                    item.ImageURL = dto.MainImage;

                item.Link = dto.Link;
                item.PubDate = dto.PubDate.LocalDate.Value;
                item.Summary = dto.SummaryAsText;
                item.Title = dto.Title;

                rssItems.Add(item);
            }

            return rssItems;
        }

        public static ComposeMessage GetPostForRssItem(UserInfoDTO userInfo, long rssItemId, ComposeMessage composeMessage)
        {
            RSSSearchRequestDTO req = new RSSSearchRequestDTO()
            {
                SortBy = 1,
                SortAscending = false,
                StartIndex = 1,
                EndIndex = 1,
                RSSItemID = rssItemId
            };
            RSSSearchResultDTO rssResult = ProviderFactory.RSS.SearchRSSItems(userInfo, req);

            if (rssResult.TotalRecords > 0)
            {
                RSSItemDTO rssItemsDto = rssResult.Items.First();

                composeMessage.LinkUrl = rssItemsDto.Link;
                composeMessage.LinkTitle = rssItemsDto.Title;
                composeMessage.LinkMessage = rssItemsDto.SummaryAsText;
                composeMessage.LinkImageUrl = rssItemsDto.MainImage;
                composeMessage.IsLinkPostType = true;
                composeMessage.ForceLinkScrape = true;
            }

            return composeMessage;
        }

        public static ComposeMessage GetPostForRssItemImagePost(UserInfoDTO userInfo, long rssItemId, string serverPath, ComposeMessage composeMessage)
        {
            RSSSearchRequestDTO req = new RSSSearchRequestDTO()
            {
                SortBy = 1,
                SortAscending = false,
                StartIndex = 1,
                EndIndex = 1,
                RSSItemID = rssItemId
            };
            RSSSearchResultDTO rssResult = ProviderFactory.RSS.SearchRSSItems(userInfo, req);

            if (rssResult.TotalRecords > 0)
            {
                RSSItemDTO rssItemsDto = rssResult.Items.First();
                composeMessage.IsLinkPostType = false;

                if (rssItemsDto.MainImage != null && rssItemsDto.MainImage.Contains("http://"))
                {
                    string guidName = string.Format("{0}{1}", Guid.NewGuid().ToString(), Path.GetExtension(rssItemsDto.MainImage));

                    using (WebClient webClient = new WebClient())
                    {
                        webClient.DownloadFile(rssItemsDto.MainImage, Path.Combine(serverPath, guidName));
                    }

                    composeMessage.ImageUrl = guidName;
                }
            }

            return composeMessage;
        }

        public static List<PostActivityCalendar> DealerPostCalendar(UserInfoDTO userInfo, long LocationID, string start, string end, ref int totalCount)
        {
            PostTargetSearchRequestDTO req = new PostTargetSearchRequestDTO(userInfo)
            {
                SortBy = 0, // 1=Status, 2=Schedule Date, 3=Title
                SortAscending = false,
                StartIndex = 0,
                EndIndex = 5000
            };

            req.MinDateScheduled = new SIDateTime(start.ToDateTime(), userInfo.EffectiveUser.ID.GetValueOrDefault());
            req.MaxDateScheduled = new SIDateTime(end.ToDateTime().AddDays(1), userInfo.EffectiveUser.ID.GetValueOrDefault());
            req.AccountIDs.Add(LocationID);

            PostTargetSearchResultsDTO res = ProviderFactory.Social.PostTargetSearch(userInfo, req);

            List<PostActivityCalendar> posts = new List<PostActivityCalendar>();

            foreach (PostTargetSearchResultItemDTO dto in res.Items)
            {
                if (posts.FirstOrDefault(p => p.PostID == dto.PostID) == null)
                {
                    PostActivityCalendar postActivity = new PostActivityCalendar();
                    postActivity.Actions = dto.Actions.GetValueOrDefault();
                    postActivity.Title = dto.Title;
                    postActivity.Description = dto.Title;
                    postActivity.PostID = dto.PostID;
                    postActivity.PostTargetID = dto.PostTargetID;

                    postActivity.PostedBy = string.Format("{0} {1}", dto.FirstName, dto.LastName);
                    postActivity.Status = dto.Status.ToString();
                    postActivity.StatusName = dto.StatusName;
                    postActivity.Type = dto.Type.ToString();

                    postActivity.FailureMessage = dto.FailureResponse;
                    if (dto.Status == PostStatusEnum.Failed && string.IsNullOrEmpty(dto.FailureResponse))
                        postActivity.FailureMessage = "An unknown error has occurred. Please retry your Post.";

                    postActivity.PostImageURL = dto.PostImageURL.IsNullOptional("");

                    if (dto.StatusDate != null)
                    {
                        postActivity.Start = DateTime.SpecifyKind(dto.StatusDate.GetUTCDate(null), DateTimeKind.Utc);
                        postActivity.ScheduleDate = dto.StatusDate.LocalDate;
                        postActivity.StatusDateText = dto.StatusDate.LocalDate.Value.ToString("M/dd/yy h:mm tt");
                    }
                    else
                    {
                        postActivity.Start = DateTime.SpecifyKind(DateTime.UtcNow, DateTimeKind.Utc);
                    }

                    switch (dto.PostTargetType)
                    {
                        case PostTargetTypeEnum.Immediate:
                            postActivity.PostTypeResource = 1;
                            break;
                        case PostTargetTypeEnum.Scheduled:
                            postActivity.PostTypeResource = 2;
                            break;
                        case PostTargetTypeEnum.Syndicated:
                            postActivity.PostTypeResource = 3;
                            break;
                    }

                    postActivity.End = postActivity.Start.AddMinutes(30);
                    postActivity.IsAllDay = false;

                    postActivity.NetworkName = dto.SocialNetwork.ToString().ToLower();
                    postActivity.SocialUrl = dto.SocialNetworkPageURL;
                    postActivity.LocationName = dto.AccountName;

                    posts.Add(postActivity);
                }
            }

            totalCount = posts.Count;

            return posts;

        }

        #region Delete Post (DB/SocialNetwork)

        public static bool DeletePostfromSocialNetwork(UserInfoDTO userInfo, long PostTargetID, ref string message)
        {
            PostTargetPublishInfoDTO postInfo = ProviderFactory.Social.GetPostTargetPublishInfo(PostTargetID, null);
            bool dbresult = false;

            if (postInfo != null)
            {
                SocialCredentialDTO socialCredentialsDTO = ProviderFactory.Social.GetSocialCredentialInfo(postInfo.CredentialID);

                if (postInfo.PostType == PostTypeEnum.Photo)
                {
                    //string message = string.Empty;
                    bool result = false;
                    switch ((SocialNetworkEnum)socialCredentialsDTO.SocialNetworkID)
                    {
                        case SocialNetworkEnum.Facebook:
                            {
                                List<PostImageResultDTO> postImageResults = ProviderFactory.Social.GetPostImageResulInfo(PostTargetID);

                                if (postImageResults != null)
                                {
                                    if (postImageResults.Count > 0)
                                    {
                                        foreach (var postimageresult in postImageResults)
                                        {
                                            if (!string.IsNullOrEmpty(postimageresult.ResultID))
                                            {
                                                result = DeletePost(postInfo.CredentialID, postimageresult.ResultID, ref message);
                                                //Mark Post as Deleted in Database                    
                                                if (result)
                                                {
                                                    ProviderFactory.Social.DeletePostImageResult(userInfo, postimageresult.PostImageResultID);
                                                }
                                            }
                                            else //No ResultID then Delete only from DB
                                            {
                                                ProviderFactory.Social.DeletePostImageResult(userInfo, postimageresult.PostImageResultID);
                                            }

                                            dbresult = ProviderFactory.Social.MarkPostTargetDeleted(userInfo, postimageresult.PostTargetID);
                                        }
                                    }
                                    else //No ResultID then Delete only from DB
                                    {
                                        //dbresult = true;

                                        dbresult = ProviderFactory.Social.MarkPostTargetDeleted(userInfo, PostTargetID);
                                    }

                                }
                            }
                            break;
                        case SocialNetworkEnum.Google:
                            {
                                if (!string.IsNullOrEmpty(postInfo.ResultID))
                                {
                                    result = DeleteGooglePlusActivity(postInfo.CredentialID, postInfo.FeedID, ref message);
                                    if (result)
                                    {
                                        dbresult = ProviderFactory.Social.MarkPostTargetDeleted(userInfo, PostTargetID);
                                    }
                                }
                                else //No ResultID then Delete only from DB
                                {
                                    dbresult = ProviderFactory.Social.MarkPostTargetDeleted(userInfo, PostTargetID);
                                }
                            }
                            break;
                        case SocialNetworkEnum.Twitter:
                            {
                                if (!string.IsNullOrEmpty(postInfo.ResultID))
                                {
                                    result = DeleteTweet(userInfo, postInfo.CredentialID, postInfo.ResultID, ref message);
                                    if (result)
                                    {
                                        dbresult = ProviderFactory.Social.MarkPostTargetDeleted(userInfo, PostTargetID);
                                    }
                                }
                                else //No ResultID then Delete only from DB
                                {
                                    dbresult = ProviderFactory.Social.MarkPostTargetDeleted(userInfo, PostTargetID);
                                }
                            }
                            break;
                    }
                }
                else
                {
                    //Delete Post from Social Network
                    if (!string.IsNullOrEmpty(postInfo.ResultID))
                    {
                        //string message = string.Empty;
                        bool result = false;
                        switch ((SocialNetworkEnum)socialCredentialsDTO.SocialNetworkID)
                        {
                            case SocialNetworkEnum.Facebook:
                                {
                                    result = DeletePost(postInfo.CredentialID, postInfo.ResultID, ref message);
                                    //Mark Post as Deleted in Database                    
                                    if (result)
                                    {
                                        dbresult = ProviderFactory.Social.MarkPostTargetDeleted(userInfo, PostTargetID);
                                    }
                                }
                                break;
                            case SocialNetworkEnum.Google:
                                {
                                    result = DeleteGooglePlusActivity(postInfo.CredentialID, postInfo.FeedID, ref message);
                                    //Mark Post as Deleted in Database                    
                                    if (result)
                                    {
                                        dbresult = ProviderFactory.Social.MarkPostTargetDeleted(userInfo, PostTargetID);
                                    }
                                }
                                break;
                            case SocialNetworkEnum.Twitter:
                                {
                                    result = DeleteTweet(userInfo, postInfo.CredentialID, postInfo.ResultID, ref message);
                                    //Mark Post as Deleted in Database                    
                                    if (result)
                                    {
                                        dbresult = ProviderFactory.Social.MarkPostTargetDeleted(userInfo, PostTargetID);
                                    }
                                }
                                break;
                        }
                    }
                    else //No ResultID then Delete only from DB
                    {
                        dbresult = ProviderFactory.Social.MarkPostTargetDeleted(userInfo, PostTargetID);
                    }

                }
            }

            if (!string.IsNullOrEmpty(message))
            {
                Auditor.New(AuditLevelEnum.Information, AuditTypeEnum.UserActivity, userInfo, "")
                    .SetAuditDataObject(AuditUserActivity.New(userInfo, AuditUserActivityTypeEnum.PostTargetDeleted)
                                            .SetPostTargetID(PostTargetID)
                                            .SetDescription(message)
                                        )
                    .Save(ProviderFactory.Logging);
            }

            return dbresult;
        }

        #endregion

        #region Social Stream

        #region Facebook

        public static List<FacebookComment> GetFacebookComments(string fbPostId, long credentialID, int pageSize, string type)
        {
            List<FacebookComment> facebookComments = new List<FacebookComment>();

            SocialCredentialDTO socialCredentialsDTO = ProviderFactory.Social.GetSocialCredentialInfo(credentialID);

            if (socialCredentialsDTO != null)
            {
                SIFacebookPostStatisticsParameters postParameters = new SIFacebookPostStatisticsParameters();

                postParameters.Token = socialCredentialsDTO.Token;
                postParameters.UniqueID = socialCredentialsDTO.UniqueID;

                PostTypeEnum postType = PostTypeEnum.Link;
                switch (type)
                {
                    case "photo": postType = PostTypeEnum.Photo; break;
                    case "video": postType = PostTypeEnum.Video; break;
                    case "status": postType = PostTypeEnum.Status; break;
                }

                postParameters.Posttype = postType;

                SIFacebookPostStatisticsConnectionParameters postConnectionParameters = new SIFacebookPostStatisticsConnectionParameters(postParameters);
                FacebookPostStatisticConnection postconnection = new FacebookPostStatisticConnection(postConnectionParameters);
                postParameters.ResultID = fbPostId;

                FacebookPostStatistics objFacebookPostStatistics = postconnection.GetFacebookPostStatistics();

                if (string.IsNullOrWhiteSpace(objFacebookPostStatistics.facebookResponse.message))
                {
                    if (objFacebookPostStatistics.postStatistics.comments != null)
                    {
                        if (objFacebookPostStatistics.postStatistics.comments.data.Count > 0)
                        {
                            List<DataComments> comments = objFacebookPostStatistics.postStatistics.comments.data.OrderByDescending(c => c.created_time).Take(pageSize).ToList();
                            comments = comments.OrderBy(c => c.created_time).ToList();

                            foreach (DataComments comment in comments)
                            {
                                FacebookComment fbcom = new FacebookComment();
                                fbcom.PostUniqueID = fbPostId;
                                fbcom.CommentID = comment.id;
                                fbcom.CommentText = comment.message;
                                fbcom.LikesCount = comment.like_count;
                                fbcom.Timestamp = string.Format("{0:MMMM d yyyy a\\t h:mm}{1}", comment.created_time, comment.created_time.ToString("tt").ToLower());
                                fbcom.UserImageURL = comment.from.picture.data.url;
                                fbcom.UserName = comment.from.name;
                                fbcom.UserLikes = comment.user_likes;
                                fbcom.CredentialID = credentialID;

                                facebookComments.Add(fbcom);
                            }
                        }
                    }
                }
            }

            return facebookComments;

        }

        //public static List<FacebookStream> GetFacebookStreams(UserInfoDTO userInfo, List<long> AccountIDs, string Until = string.Empty)
        public static List<FacebookStream> GetFacebookStreams(UserInfoDTO userInfo, List<FacebookComment> facebookComments, long accountID, string PageURL = "")
        {
            List<FacebookStream> facebookStreams = new List<FacebookStream>();
            string Until = string.Empty;

            //long AccountID = AccountIDs.FirstOrDefault();
            long AccountID = accountID; // 220795; //220737; // 220246;

            TimeZoneInfo timeZone = ProviderFactory.TimeZones.GetTimezoneForUserID(userInfo.EffectiveUser.ID.GetValueOrDefault());
            string PostDateTime_TimeZone = timeZone.Id;

            SocialCredentialDTO socialCredentialsDTO = ProviderFactory.Social.GetSocialCredentials(SocialNetworkEnum.Facebook, AccountID, true).SingleOrDefault();

            if (socialCredentialsDTO != null)
            {
                SIFacebookStreamParameters streamParameters = new SIFacebookStreamParameters();

                streamParameters.UniqueID = socialCredentialsDTO.UniqueID;
                streamParameters.Token = socialCredentialsDTO.Token;
                streamParameters.PageURL = PageURL;
                streamParameters.limit = "10";

                SIFacebookStreamConnectionParameters ConnectionParameters = new SIFacebookStreamConnectionParameters(streamParameters);
                FacebookStreamConnection streamconnection = new FacebookStreamConnection(ConnectionParameters);

                FacebookFeed FBFeed = streamconnection.GetSocialStreamFacebookFeed();


                if (string.IsNullOrEmpty(FBFeed.facebookResponse.message))
                {
                    if (FBFeed.feed.paging != null)
                    {
                        if (!string.IsNullOrEmpty(FBFeed.feed.paging.next))
                        {
                            int startindex = FBFeed.feed.paging.next.IndexOf("until=");
                            if (startindex > 0)
                            {
                                Until = FBFeed.feed.paging.next.Substring(startindex + 6);
                            }
                        }
                    }

                    foreach (var feed in FBFeed.feed.data)
                    {
                        FacebookStream stream = new FacebookStream();

                        stream.PageName = socialCredentialsDTO.ScreenName;
                        stream.PageImageURL = socialCredentialsDTO.PictureURL;
                        stream.PostUniqueID = feed.object_id;
                        stream.PostedBy = "";
                        if (feed.from.name != stream.PageName)
                            stream.PostedBy = feed.from.name;

                        string[] feedPostIDs = feed.id.Split('_');
                        stream.PostURL = string.Format(@"https://www.facebook.com/{0}/posts/{1}", feedPostIDs[0], feedPostIDs[1]);

                        DateTime postLocalDate = new SIDateTime(feed.created_time.ToUniversalTime(), timeZone).LocalDate.Value;
                        TimeSpan dateDiff = DateTime.UtcNow.Subtract(feed.created_time.ToUniversalTime());
                        if (dateDiff.Days > 0)
                        {
                            if (dateDiff.Days == 1)
                                stream.PostTimeStamp = "Yesterday";
                            else
                            {
                                stream.PostTimeStamp = string.Format("{0:MMMM d}", postLocalDate);
                                if (DateTime.Now.Year != postLocalDate.Year)
                                    stream.PostTimeStamp = string.Format("{0} {1}", stream.PostTimeStamp, postLocalDate.Year);
                            }
                        }
                        else
                        {
                            if (dateDiff.Hours > 0)
                            {
                                if (dateDiff.Hours == 1)
                                    stream.PostTimeStamp = "about an hour ago";
                                else
                                    stream.PostTimeStamp = string.Format("{0} hours ago", dateDiff.Hours);
                            }
                            else
                                stream.PostTimeStamp = string.Format("{0} minutes ago", dateDiff.Minutes);
                        }

                        stream.Message = feed.message.IsNullOptional("").StripHtml();
                        stream.PostDate = postLocalDate.ToString();
                        stream.UserTimeZone = PostDateTime_TimeZone;
                        stream.PostImageURL = (feed.type.ToLower() == "photo" || feed.type.ToLower() == "video") ? feed.picture.Replace("_s", "_n").Replace("/t1/", "/t1/s403x403/") : "";
                        stream.LinkImageURL = feed.picture;
                        stream.LinkCaption = feed.name.IsNullOptional("");
                        stream.LinkURL = feed.link.IsNullOptional("") != "" ? feed.link : stream.PostURL;
                        stream.LinkText = feed.description.IsNullOptional("").StripHtml();
                        //stream.FeedID = feed.id; //Add FeedID in FacebookStream
                        stream.Type = feed.type.ToLower();
                        stream.CredentialID = socialCredentialsDTO.ID;
                        stream.Until = Until;
                        stream.InsightCount = 0;

                        PostTypeEnum postType = PostTypeEnum.Link;
                        switch (stream.Type)
                        {
                            case "photo": postType = PostTypeEnum.Photo; break;
                            case "video": postType = PostTypeEnum.Video; break;
                            case "status": postType = PostTypeEnum.Status; break;
                        }

                        SIFacebookPostStatisticsParameters postParameters = new SIFacebookPostStatisticsParameters();
                        postParameters.Token = socialCredentialsDTO.Token;
                        postParameters.UniqueID = socialCredentialsDTO.UniqueID;
                        postParameters.Posttype = postType;

                        SIFacebookPostStatisticsConnectionParameters postConnectionParameters = new SIFacebookPostStatisticsConnectionParameters(postParameters);
                        FacebookPostStatisticConnection postconnection = new FacebookPostStatisticConnection(postConnectionParameters);
                        postParameters.ResultID = stream.PostUniqueID;

                        FacebookPostStatistics objFacebookPostStatistics = postconnection.GetFacebookPostStatistics();

                        if (string.IsNullOrWhiteSpace(objFacebookPostStatistics.facebookResponse.message))
                        {
                            stream.CommentCount = objFacebookPostStatistics.postStatistics.commentscount;
                            stream.LikesCount = objFacebookPostStatistics.postStatistics.likescount;
                            stream.SharesCount = objFacebookPostStatistics.postStatistics.sharescount;

                            if (objFacebookPostStatistics.postStatistics.comments != null)
                            {
                                if (objFacebookPostStatistics.postStatistics.comments.data.Count > 0)
                                {
                                    List<DataComments> comments = objFacebookPostStatistics.postStatistics.comments.data.OrderByDescending(c => c.created_time).Take(3).ToList();
                                    comments = comments.OrderBy(c => c.created_time).ToList();

                                    foreach (DataComments comment in comments)
                                    {
                                        FacebookComment fbcom = new FacebookComment();
                                        fbcom.PostUniqueID = stream.PostUniqueID;
                                        fbcom.CommentID = comment.id;
                                        fbcom.CommentText = comment.message.StripHtml();
                                        fbcom.LikesCount = comment.like_count;
                                        fbcom.Timestamp = string.Format("{0:MMMM d yyyy a\\t h:mm}{1}", comment.created_time, comment.created_time.ToString("tt").ToLower());

                                        fbcom.UserImageURL = comment.from.picture.data.url;
                                        fbcom.UserName = comment.from.name;
                                        fbcom.UserLikes = comment.user_likes;
                                        fbcom.CredentialID = socialCredentialsDTO.ID;

                                        facebookComments.Add(fbcom);
                                    }
                                }
                            }
                        }

                        postParameters.FeedID = feed.id;
                        FacebookInsights objFacebookInsights = postconnection.GetFacebookPostInsights();

                        if (string.IsNullOrWhiteSpace(objFacebookInsights.facebookResponse.message))
                        {
                            var Insights = objFacebookInsights.insights.data.Where(x => x.name == "post_impressions_unique" && x.period == "lifetime").SingleOrDefault();
                            if (Insights != null)
                            {
                                foreach (Value value in Insights.values)
                                {
                                    bool hasValue = false;
                                    int intValue = 0;
                                    if (value._value.StartsWith("{"))
                                    {
                                        Dictionary<string, object> resultDictionary = JsonConvert.DeserializeObject<Dictionary<string, object>>(value._value);
                                        if (resultDictionary != null && resultDictionary.Count > 0)
                                        {
                                            foreach (KeyValuePair<string, object> keyV in resultDictionary)
                                            {
                                                int tempValue;
                                                if (keyV.Key != "total")
                                                {
                                                    if (int.TryParse(keyV.Value.ToString(), out tempValue))
                                                    {
                                                        intValue += tempValue;
                                                        hasValue = true;
                                                    }
                                                }
                                            }
                                        }
                                    }
                                    else if (!value._value.StartsWith("["))
                                    {
                                        int tempValue = 0;
                                        if (int.TryParse(value._value, out tempValue))
                                        {
                                            intValue = tempValue;
                                            hasValue = true;
                                        }
                                    }
                                    if (hasValue)
                                    {
                                        stream.InsightCount = intValue;
                                    }
                                }
                            }
                        }

                        facebookStreams.Add(stream);
                    }
                }
            }
            return facebookStreams;

        }

        public static bool DeletePost(long credentialID, string resultID, ref string message)
        {
            SocialCredentialDTO socialCredentialsDTO = ProviderFactory.Social.GetSocialCredentialInfo(credentialID);

            if (socialCredentialsDTO != null && !string.IsNullOrEmpty(resultID))
            {
                SIFacebookStreamParameters streamParameters = new SIFacebookStreamParameters();

                streamParameters.Token = socialCredentialsDTO.Token;
                streamParameters.PostID = resultID;

                SIFacebookStreamConnectionParameters ConnectionParameters = new SIFacebookStreamConnectionParameters(streamParameters);
                FacebookStreamConnection streamconnection = new FacebookStreamConnection(ConnectionParameters);

                FacebookStreamAction objFacebookStreamAction = streamconnection.DeletePost();

                message = objFacebookStreamAction.FacebookResponse.message;

                if (message.ToLower() == "(#100) This post could not be loaded".ToLower())
                {
                    objFacebookStreamAction.IsSuccess = true;
                    message = string.Empty;
                }

                return objFacebookStreamAction.IsSuccess;
            }
            return false;
        }

        public static bool LikePost(long credentialID, string resultID, ref string message)
        {
            bool Success = false;
            SocialCredentialDTO socialCredentialsDTO = ProviderFactory.Social.GetSocialCredentialInfo(credentialID);

            if (socialCredentialsDTO != null && !string.IsNullOrEmpty(resultID))
            {
                SIFacebookStreamParameters streamParameters = new SIFacebookStreamParameters();

                streamParameters.Token = socialCredentialsDTO.Token;
                streamParameters.PostID = resultID;

                SIFacebookStreamConnectionParameters ConnectionParameters = new SIFacebookStreamConnectionParameters(streamParameters);
                FacebookStreamConnection streamconnection = new FacebookStreamConnection(ConnectionParameters);

                FacebookStreamAction objFacebookStreamAction = streamconnection.LikePost();
                message = objFacebookStreamAction.FacebookResponse.message;
                return objFacebookStreamAction.IsSuccess;
            }

            return Success;
        }

        public static void UnLikePost(long credentialID, string resultID)
        {
            SocialCredentialDTO socialCredentialsDTO = ProviderFactory.Social.GetSocialCredentialInfo(credentialID);

            if (socialCredentialsDTO != null && !string.IsNullOrEmpty(resultID))
            {
                SIFacebookStreamParameters streamParameters = new SIFacebookStreamParameters();

                streamParameters.Token = socialCredentialsDTO.Token;
                streamParameters.PostID = resultID;

                SIFacebookStreamConnectionParameters ConnectionParameters = new SIFacebookStreamConnectionParameters(streamParameters);
                FacebookStreamConnection streamconnection = new FacebookStreamConnection(ConnectionParameters);

                FacebookStreamAction objFacebookStreamAction = streamconnection.UnLikePost();
            }
        }

        public static bool CreateCommentonPost(long credentialID, string resultID, string comment, ref string message)
        {
            SocialCredentialDTO socialCredentialsDTO = ProviderFactory.Social.GetSocialCredentialInfo(credentialID);

            if (socialCredentialsDTO != null && !string.IsNullOrEmpty(comment))
            {
                SIFacebookStreamParameters streamParameters = new SIFacebookStreamParameters();

                streamParameters.Token = socialCredentialsDTO.Token;
                streamParameters.Comment = comment;
                streamParameters.PostID = resultID;

                SIFacebookStreamConnectionParameters ConnectionParameters = new SIFacebookStreamConnectionParameters(streamParameters);
                FacebookStreamConnection streamconnection = new FacebookStreamConnection(ConnectionParameters);

                FacebookStreamAction objFacebookStreamAction = streamconnection.CreateCommentonPost();

                message = objFacebookStreamAction.FacebookResponse.message;
                return objFacebookStreamAction.IsSuccess;
            }

            return false;
        }

        public static bool DeleteCommentonPost(long credentialID, string commentID, ref string message)
        {
            SocialCredentialDTO socialCredentialsDTO = ProviderFactory.Social.GetSocialCredentialInfo(credentialID);

            if (socialCredentialsDTO != null && !string.IsNullOrEmpty(commentID))
            {
                SIFacebookStreamParameters streamParameters = new SIFacebookStreamParameters();

                streamParameters.Token = socialCredentialsDTO.Token;
                streamParameters.CommentID = commentID;

                SIFacebookStreamConnectionParameters ConnectionParameters = new SIFacebookStreamConnectionParameters(streamParameters);
                FacebookStreamConnection streamconnection = new FacebookStreamConnection(ConnectionParameters);

                FacebookStreamAction objFacebookStreamAction = streamconnection.DeleteCommentonPost();

                message = objFacebookStreamAction.FacebookResponse.message;
                return objFacebookStreamAction.IsSuccess;
            }

            return false;
        }

        public static bool LikeComment(long credentialID, string commentID, ref string message)
        {
            SocialCredentialDTO socialCredentialsDTO = ProviderFactory.Social.GetSocialCredentialInfo(credentialID);

            if (socialCredentialsDTO != null && !string.IsNullOrEmpty(commentID))
            {
                SIFacebookStreamParameters streamParameters = new SIFacebookStreamParameters();

                streamParameters.Token = socialCredentialsDTO.Token;
                streamParameters.CommentID = commentID;

                SIFacebookStreamConnectionParameters ConnectionParameters = new SIFacebookStreamConnectionParameters(streamParameters);
                FacebookStreamConnection streamconnection = new FacebookStreamConnection(ConnectionParameters);

                FacebookStreamAction objFacebookStreamAction = streamconnection.LikeComment();
                message = objFacebookStreamAction.FacebookResponse.message;
                return objFacebookStreamAction.IsSuccess;
            }

            return false;
        }

        public static bool UnLikeComment(long credentialID, string commentID, ref string message)
        {
            SocialCredentialDTO socialCredentialsDTO = ProviderFactory.Social.GetSocialCredentialInfo(credentialID);

            if (socialCredentialsDTO != null && !string.IsNullOrEmpty(commentID))
            {
                SIFacebookStreamParameters streamParameters = new SIFacebookStreamParameters();

                streamParameters.Token = socialCredentialsDTO.Token;
                streamParameters.CommentID = commentID;

                SIFacebookStreamConnectionParameters ConnectionParameters = new SIFacebookStreamConnectionParameters(streamParameters);
                FacebookStreamConnection streamconnection = new FacebookStreamConnection(ConnectionParameters);

                FacebookStreamAction objFacebookStreamAction = streamconnection.UnLikeComment();
                message = objFacebookStreamAction.FacebookResponse.message;
                return objFacebookStreamAction.IsSuccess;
            }

            return false;
        }

        #endregion

        #region Twitter

        public static List<TwitterStream> GetHomeTweets(UserInfoDTO userInfo, long accountID, int count = 0, string sinceID = "")
        {
            List<TwitterStream> twitterStreams = new List<TwitterStream>();

            long AccountID = accountID; // 220795; //220737; // 220246;

            TimeZoneInfo timeZone = ProviderFactory.TimeZones.GetTimezoneForUserID(userInfo.EffectiveUser.ID.GetValueOrDefault());

            SocialCredentialDTO socialCredentialsDTO = ProviderFactory.Social.GetSocialCredentials(SocialNetworkEnum.Twitter, AccountID, true).SingleOrDefault();

            if (socialCredentialsDTO != null)
            {
                SITwitterStreamParameters streamParameters = new SITwitterStreamParameters();

                streamParameters.oauth_token = socialCredentialsDTO.Token;
                streamParameters.oauth_token_secret = socialCredentialsDTO.TokenSecret;
                streamParameters.oauth_consumer_key = socialCredentialsDTO.AppID;
                streamParameters.oauth_consumer_secret = socialCredentialsDTO.AppSecret;
                streamParameters.screenName = socialCredentialsDTO.ScreenName;
                streamParameters.count = Convert.ToString(count);
                streamParameters.sinceID = sinceID;

                SITwitterStreamConnectionParameters ConnectionParameters = new SITwitterStreamConnectionParameters(streamParameters);
                TwitterStreamConnection streamconnection = new TwitterStreamConnection(ConnectionParameters);

                List<TwitterTweet> listTwitterTweet = streamconnection.GetHomeTweets();

                foreach (TwitterTweet tweet in listTwitterTweet)
                {
                    TwitterStream stream = PopulateTwitterStream(tweet, timeZone);
                    stream.CredentialID = socialCredentialsDTO.ID;
                    stream.Section = "home";

                    twitterStreams.Add(stream);
                }

            }

            return twitterStreams;
        }



        public static List<TwitterStream> GetUserTweets(UserInfoDTO userInfo, long accountID, int count = 0, string sinceID = "")
        {
            List<TwitterStream> twitterStreams = new List<TwitterStream>();

            long AccountID = accountID; // 220795; //220737; // 220246;

            TimeZoneInfo timeZone = ProviderFactory.TimeZones.GetTimezoneForUserID(userInfo.EffectiveUser.ID.GetValueOrDefault());

            SocialCredentialDTO socialCredentialsDTO = ProviderFactory.Social.GetSocialCredentials(SocialNetworkEnum.Twitter, AccountID, true).SingleOrDefault();

            if (socialCredentialsDTO != null)
            {
                SITwitterStreamParameters streamParameters = new SITwitterStreamParameters();

                streamParameters.oauth_token = socialCredentialsDTO.Token;
                streamParameters.oauth_token_secret = socialCredentialsDTO.TokenSecret;
                streamParameters.oauth_consumer_key = socialCredentialsDTO.AppID;
                streamParameters.oauth_consumer_secret = socialCredentialsDTO.AppSecret;
                streamParameters.screenName = socialCredentialsDTO.ScreenName;
                streamParameters.count = Convert.ToString(count);
                streamParameters.sinceID = sinceID;

                SITwitterStreamConnectionParameters ConnectionParameters = new SITwitterStreamConnectionParameters(streamParameters);
                TwitterStreamConnection streamconnection = new TwitterStreamConnection(ConnectionParameters);

                List<TwitterTweet> listTwitterTweet = streamconnection.GetUserTweets();

                foreach (var tweet in listTwitterTweet)
                {
                    TwitterStream stream = PopulateTwitterStream(tweet, timeZone);
                    stream.CredentialID = socialCredentialsDTO.ID;
                    stream.Section = "user";

                    twitterStreams.Add(stream);
                }

            }

            return twitterStreams;
        }

        public static List<TwitterStream> GetMentions(UserInfoDTO userInfo, long accountID, int count = 0, string sinceID = "")
        {
            List<TwitterStream> twitterStreams = new List<TwitterStream>();

            long AccountID = accountID; // 220795; //220737; // 220246;

            TimeZoneInfo timeZone = ProviderFactory.TimeZones.GetTimezoneForUserID(userInfo.EffectiveUser.ID.GetValueOrDefault());

            SocialCredentialDTO socialCredentialsDTO = ProviderFactory.Social.GetSocialCredentials(SocialNetworkEnum.Twitter, AccountID, true).SingleOrDefault();

            if (socialCredentialsDTO != null)
            {
                SITwitterStreamParameters streamParameters = new SITwitterStreamParameters();

                streamParameters.oauth_token = socialCredentialsDTO.Token;
                streamParameters.oauth_token_secret = socialCredentialsDTO.TokenSecret;
                streamParameters.oauth_consumer_key = socialCredentialsDTO.AppID;
                streamParameters.oauth_consumer_secret = socialCredentialsDTO.AppSecret;
                streamParameters.screenName = socialCredentialsDTO.ScreenName;
                streamParameters.count = Convert.ToString(count);
                streamParameters.sinceID = sinceID;

                SITwitterStreamConnectionParameters ConnectionParameters = new SITwitterStreamConnectionParameters(streamParameters);
                TwitterStreamConnection streamconnection = new TwitterStreamConnection(ConnectionParameters);

                List<TwitterTweet> listTwitterTweet = streamconnection.GetMentions();

                foreach (var tweet in listTwitterTweet)
                {
                    TwitterStream stream = PopulateTwitterStream(tweet, timeZone);
                    stream.CredentialID = socialCredentialsDTO.ID;
                    stream.Section = "mentions";

                    twitterStreams.Add(stream);
                }
            }

            return twitterStreams;
        }


        private static TwitterStream PopulateTwitterStream(TwitterTweet tweet, TimeZoneInfo timeZone)
        {
            TwitterStream stream = new TwitterStream();
            stream.ID = tweet.ID;
            stream.Text = tweet.Text;

            DateTime postLocalDate = new SIDateTime(tweet.CreatedDate.ToUniversalTime(), timeZone).LocalDate.Value;
            TimeSpan dateDiff = DateTime.UtcNow.Subtract(tweet.CreatedDate.ToUniversalTime());

            if (dateDiff.Days > 0)
            {
                stream.TimeStamp = string.Format("{0:MMM d}", postLocalDate);
            }
            else
            {
                if (dateDiff.Hours == 0)
                    stream.TimeStamp = string.Format("{0}m", dateDiff.Minutes > 0 ? dateDiff.Minutes : 1);
                else
                    stream.TimeStamp = string.Format("{0}h", dateDiff.Hours);
            }

            stream.TweetDate = postLocalDate.ToString("h:mm tt - d MMM yyyy");
            stream.UserTimeZone = timeZone.Id;
            stream.Since = tweet.Since;
            stream.Favorited = tweet.Favorited;
            stream.UserID = tweet.User.ID;
            stream.UserScreenName = tweet.User.ScreenName;
            stream.UserName = tweet.User.Name;
            stream.UserImageURL = tweet.User.ProfileImageUrl;
            stream.TweetImageURL = tweet.PhotoURL.IsNullOptional("");

            return stream;
        }

        public static bool SetFavorite(UserInfoDTO userInfo, long credentialID, string statusID, ref string message)
        {
            bool Success = false;

            SocialCredentialDTO socialCredentialsDTO = ProviderFactory.Social.GetSocialCredentialInfo(credentialID);

            if (socialCredentialsDTO != null)
            {
                SITwitterStreamParameters streamParameters = new SITwitterStreamParameters();

                streamParameters.oauth_token = socialCredentialsDTO.Token;
                streamParameters.oauth_token_secret = socialCredentialsDTO.TokenSecret;
                streamParameters.oauth_consumer_key = socialCredentialsDTO.AppID;
                streamParameters.oauth_consumer_secret = socialCredentialsDTO.AppSecret;
                streamParameters.screenName = socialCredentialsDTO.ScreenName;
                streamParameters.statusID = statusID;

                SITwitterStreamConnectionParameters ConnectionParameters = new SITwitterStreamConnectionParameters(streamParameters);
                TwitterStreamConnection streamconnection = new TwitterStreamConnection(ConnectionParameters);

                TwitterRequestResult result = streamconnection.SetFavorite();
                Success = result == TwitterRequestResult.Success;
                message = result.ToString();

            }
            return Success;
        }

        public static bool DeleteFavorite(UserInfoDTO userInfo, long credentialID, string statusID, ref string message)
        {
            bool Success = false;

            SocialCredentialDTO socialCredentialsDTO = ProviderFactory.Social.GetSocialCredentialInfo(credentialID);

            if (socialCredentialsDTO != null)
            {
                SITwitterStreamParameters streamParameters = new SITwitterStreamParameters();

                streamParameters.oauth_token = socialCredentialsDTO.Token;
                streamParameters.oauth_token_secret = socialCredentialsDTO.TokenSecret;
                streamParameters.oauth_consumer_key = socialCredentialsDTO.AppID;
                streamParameters.oauth_consumer_secret = socialCredentialsDTO.AppSecret;
                streamParameters.screenName = socialCredentialsDTO.ScreenName;
                streamParameters.statusID = statusID;

                SITwitterStreamConnectionParameters ConnectionParameters = new SITwitterStreamConnectionParameters(streamParameters);
                TwitterStreamConnection streamconnection = new TwitterStreamConnection(ConnectionParameters);

                TwitterRequestResult result = streamconnection.DeleteFavorite();
                Success = result == TwitterRequestResult.Success;
                message = result.ToString();
            }
            return Success;
        }

        public static bool ReplyToTweet(UserInfoDTO userInfo, long credentialID, string status, string inReplyToStatusID, ref string message)
        {
            bool Success = false;

            SocialCredentialDTO socialCredentialsDTO = ProviderFactory.Social.GetSocialCredentialInfo(credentialID);

            if (socialCredentialsDTO != null)
            {
                SITwitterStreamParameters streamParameters = new SITwitterStreamParameters();

                streamParameters.oauth_token = socialCredentialsDTO.Token;
                streamParameters.oauth_token_secret = socialCredentialsDTO.TokenSecret;
                streamParameters.oauth_consumer_key = socialCredentialsDTO.AppID;
                streamParameters.oauth_consumer_secret = socialCredentialsDTO.AppSecret;
                streamParameters.status = status;
                streamParameters.inReplyToStatusID = inReplyToStatusID;

                SITwitterStreamConnectionParameters ConnectionParameters = new SITwitterStreamConnectionParameters(streamParameters);
                TwitterStreamConnection streamconnection = new TwitterStreamConnection(ConnectionParameters);

                TwitterRequestResult result = streamconnection.ReplyToTweet();
                Success = result == TwitterRequestResult.Success;
                message = result.ToString();

            }
            return Success;
        }

        public static bool DeleteTweet(UserInfoDTO userInfo, long credentialID, string statusID, ref string message)
        {
            bool Success = false;

            SocialCredentialDTO socialCredentialsDTO = ProviderFactory.Social.GetSocialCredentialInfo(credentialID);

            if (socialCredentialsDTO != null)
            {
                SITwitterStreamParameters streamParameters = new SITwitterStreamParameters();

                streamParameters.oauth_token = socialCredentialsDTO.Token;
                streamParameters.oauth_token_secret = socialCredentialsDTO.TokenSecret;
                streamParameters.oauth_consumer_key = socialCredentialsDTO.AppID;
                streamParameters.oauth_consumer_secret = socialCredentialsDTO.AppSecret;
                streamParameters.statusID = statusID;

                SITwitterStreamConnectionParameters ConnectionParameters = new SITwitterStreamConnectionParameters(streamParameters);
                TwitterStreamConnection streamconnection = new TwitterStreamConnection(ConnectionParameters);

                TwitterRequestResult result = streamconnection.DeleteTweet();
                Success = result == TwitterRequestResult.Success;
                message = result.ToString();

                if (result == TwitterRequestResult.FileNotFound)
                {
                    Success = true;
                    message = string.Empty;
                }

            }
            return Success;
        }

        public static void Retweet(UserInfoDTO userInfo, long credentialID, string statusID)
        {
            TimeZoneInfo timeZone = ProviderFactory.TimeZones.GetTimezoneForUserID(userInfo.EffectiveUser.ID.GetValueOrDefault());
            string PostDateTime_TimeZone = timeZone.Id;

            SocialCredentialDTO socialCredentialsDTO = ProviderFactory.Social.GetSocialCredentialInfo(credentialID);

            if (socialCredentialsDTO != null)
            {
                SITwitterStreamParameters streamParameters = new SITwitterStreamParameters();

                streamParameters.oauth_token = socialCredentialsDTO.Token;
                streamParameters.oauth_token_secret = socialCredentialsDTO.TokenSecret;
                streamParameters.oauth_consumer_key = socialCredentialsDTO.AppID;
                streamParameters.oauth_consumer_secret = socialCredentialsDTO.AppSecret;
                streamParameters.screenName = socialCredentialsDTO.ScreenName;
                streamParameters.statusID = statusID;

                SITwitterStreamConnectionParameters ConnectionParameters = new SITwitterStreamConnectionParameters(streamParameters);
                TwitterStreamConnection streamconnection = new TwitterStreamConnection(ConnectionParameters);

                TwitterTweet tweet = streamconnection.Retweet();

            }
        }

        #endregion

        #region GooglePlus

        public static List<GooglePlusStream> GetGooglePlusStreams(UserInfoDTO userInfo, long accountID, int page = 1)
        {
            string apiKey = "AIzaSyD6EAaOZy0_JJ8sDw_DqfeAU4J6-q69-28";
            List<GooglePlusStream> results = new List<GooglePlusStream>();

            //long AccountID = AccountIDs.FirstOrDefault();
            long AccountID = accountID; // 220795; //220737; // 220246;

            TimeZoneInfo timeZone = ProviderFactory.TimeZones.GetTimezoneForUserID(userInfo.EffectiveUser.ID.GetValueOrDefault());
            string PostDateTime_TimeZone = timeZone.Id;

            SocialCredentialDTO socialCredentialsDTO = ProviderFactory.Social.GetSocialCredentials(SocialNetworkEnum.Google, AccountID, true).SingleOrDefault();

            if (socialCredentialsDTO != null)
            {
                SIGooglePlusStreamParameters streamParameters = new SIGooglePlusStreamParameters();

                streamParameters.UniqueID = socialCredentialsDTO.UniqueID;
                streamParameters.apiKey = apiKey;

                SIGooglePlusStreamConnectionParameters ConnectionParameters = new SIGooglePlusStreamConnectionParameters(streamParameters);
                GooglePlusStreamConnection streamconnection = new GooglePlusStreamConnection(ConnectionParameters);

                var posts = streamconnection.GetGooglePlusActivitiesListForAPage();

                if (string.IsNullOrEmpty(posts.error.message))
                {
                    foreach (ActivityItem post in posts.items.Skip((page - 1) * 20).Take(20))
                    {
                        string posttype = "status";
                        string LinkURL = string.Empty;
                        string PictureURL = string.Empty;
                        string PostMessage = string.Empty;
                        string LinkContent = string.Empty;
                        string LinkDisplayName = string.Empty;
                        string LinkImageURL = string.Empty;

                        GooglePlusStream stream = new GooglePlusStream();

                        DateTime postLocalDate = new SIDateTime(post.published.ToDateTime().ToUniversalTime(), timeZone).LocalDate.Value;

                        foreach (var item in (dynamic)post.@object)
                        {
                            string key = item.Key;

                            if (key.ToLower() == "content".ToLower())
                            {

                                PostMessage = HttpUtility.HtmlDecode(item.Value);

                                // GooglePlus Adding one extra char "?" to then end of string 
                                //var a = Encoding.ASCII.GetBytes(PostMessage);                                
                                if (!string.IsNullOrWhiteSpace(PostMessage))
                                {
                                    PostMessage = PostMessage.Substring(0, PostMessage.Length - 1);
                                }
                                //a = Encoding.ASCII.GetBytes(PostMessage);
                            }//key.ToLower() == "content"

                            if (key.ToLower() == "attachments".ToLower())
                            {
                                foreach (var attachment in item.Value[0])
                                {
                                    key = attachment.Key;
                                    if (key.ToLower() == "objecttype".ToLower())
                                    {
                                        if (attachment.Value.ToLower() == "article".ToLower())
                                        {
                                            posttype = "link";
                                        }
                                        else if (attachment.Value.ToLower() == "photo".ToLower())
                                        {
                                            posttype = "photo";
                                        }
                                        else if (attachment.Value.ToLower() == "video".ToLower())
                                        {
                                            posttype = "video";
                                        }
                                    }//key.ToLower() == "objecttype"

                                    if (posttype == "link" && key.ToLower() == "url".ToLower())
                                    {
                                        LinkURL = attachment.Value;
                                        PictureURL = string.Empty;
                                    }//posttype == "link" url

                                    if (posttype == "link" && key.ToLower() == "displayName".ToLower())
                                    {
                                        LinkDisplayName = attachment.Value;
                                    }//posttype == "link" displayName

                                    if (posttype == "link" && key.ToLower() == "content".ToLower())
                                    {
                                        LinkContent = attachment.Value;
                                    }//posttype == "link" content

                                    if (posttype == "link" && key.ToLower() == "image".ToLower())
                                    {
                                        foreach (var image in attachment.Value)
                                        {
                                            key = image.Key;

                                            if (key.ToLower() == "url".ToLower())
                                            {
                                                LinkImageURL = image.Value;
                                                break;
                                            }
                                        }//for attachment.Value
                                    }// if posttype == "photo"

                                    if (posttype == "photo" && key.ToLower() == "image".ToLower())
                                    {
                                        foreach (var image in attachment.Value)
                                        {
                                            key = image.Key;

                                            if (key.ToLower() == "url".ToLower())
                                            {
                                                PictureURL = image.Value;
                                                LinkURL = string.Empty;
                                                break;
                                            }
                                        }//for attachment.Value
                                    }// if posttype == "photo"
                                }//for item.Value[0]
                            }//if key.ToLower() == "attachments"
                        }//(dynamic)post.@object

                        stream.PageName = socialCredentialsDTO.ScreenName;
                        stream.PageImageURL = socialCredentialsDTO.PictureURL;
                        stream.PostUniqueID = post.id;
                        stream.PostedBy = "";
                        stream.PostURL = post.url;

                        stream.Message = PostMessage;
                        stream.PostDate = postLocalDate.ToString();
                        stream.UserTimeZone = PostDateTime_TimeZone;
                        stream.PostImageURL = PictureURL;
                        stream.LinkImageURL = LinkImageURL;
                        stream.LinkCaption = LinkDisplayName;
                        stream.LinkURL = LinkURL;
                        stream.LinkText = LinkContent;
                        stream.Type = posttype.ToLower();
                        stream.CredentialID = socialCredentialsDTO.ID;

                        SIGooglePlusPostStatisticsParameters postParameters = new SIGooglePlusPostStatisticsParameters();
                        postParameters.activityID = post.id;
                        postParameters.apiKey = apiKey;

                        stream.CommentCount = 0;

                        SIGooglePlusPostStatisticsConnectionParameters postConnectionParameters = new SIGooglePlusPostStatisticsConnectionParameters(postParameters);
                        GooglePlusPostStatisticConnection postconnection = new GooglePlusPostStatisticConnection(postConnectionParameters);

                        GooglePlusGetCommentsListResponse Comments = postconnection.GetGooglePlusCommentsListForAnActivity();
                        if (Comments != null)
                        {
                            stream.CommentCount = Comments.items.Count;
                        }

                        stream.LikesCount = 1;
                        GooglePlusGetPlusonersListResponse Plusoners = postconnection.GetGooglePlusPlusonersListForAnActivity();
                        if (Plusoners != null && Plusoners.items.Count > 0)
                        {
                            stream.LikesCount = Plusoners.items.Count;
                        }

                        GooglePlusGetResharersListResponse Resharers = postconnection.GetGooglePlusResharersListForAnActivity();
                        if (Resharers != null)
                        {
                            stream.SharesCount = Resharers.items.Count;
                        }

                        results.Add(stream);

                    }//posts.items
                }//string.IsNullOrEmpty(posts.error.message)
            }

            return results;
        }

        public static bool AddGooglePlusComment(long credentialID, string activityID, string commentContent, ref string message)
        {
            bool result = false;

            SocialCredentialDTO socialCredentialsDTO = ProviderFactory.Social.GetSocialCredentialInfo(credentialID);

            if (socialCredentialsDTO != null && !string.IsNullOrEmpty(activityID) && !string.IsNullOrEmpty(commentContent))
            {

                string access_token = GetGooglePlusAccessToke(socialCredentialsDTO);

                if (!string.IsNullOrEmpty(access_token))
                {
                    SIGooglePlusStreamParameters streamParameters = new SIGooglePlusStreamParameters();

                    streamParameters.accessToken = access_token;
                    streamParameters.UniqueID = socialCredentialsDTO.UniqueID;
                    streamParameters.activityID = activityID;
                    streamParameters.commentContent = commentContent;

                    SIGooglePlusStreamConnectionParameters StreamConnectionParameters = new SIGooglePlusStreamConnectionParameters(streamParameters);
                    GooglePlusStreamConnection streamconnection = new GooglePlusStreamConnection(StreamConnectionParameters);

                    GooglePlusPostResponse PostResponse = streamconnection.AddGooglePlusComment();

                    if (PostResponse != null)
                    {
                        if (PostResponse.IsSuccess)
                            result = true;
                        else
                            message = PostResponse.error.message;
                    }
                }
            }

            return result;
        }

        public static bool DeleteGooglePlusComment(long credentialID, string commentID, ref string message)
        {
            bool result = false;

            SocialCredentialDTO socialCredentialsDTO = ProviderFactory.Social.GetSocialCredentialInfo(credentialID);

            if (socialCredentialsDTO != null && !string.IsNullOrEmpty(commentID))
            {
                string access_token = GetGooglePlusAccessToke(socialCredentialsDTO);
                if (!string.IsNullOrEmpty(access_token))
                {
                    SIGooglePlusStreamParameters streamParameters = new SIGooglePlusStreamParameters();

                    streamParameters.accessToken = access_token;
                    streamParameters.UniqueID = socialCredentialsDTO.UniqueID;
                    streamParameters.commentID = commentID;

                    SIGooglePlusStreamConnectionParameters StreamConnectionParameters = new SIGooglePlusStreamConnectionParameters(streamParameters);
                    GooglePlusStreamConnection streamconnection = new GooglePlusStreamConnection(StreamConnectionParameters);

                    GooglePlusResponse PostResponse = streamconnection.DeleteGooglePlusComment();

                    if (PostResponse != null)
                    {
                        if (PostResponse.error.code == 0)
                            result = true;
                        else
                            message = PostResponse.error.message;
                    }
                }
            }

            return result;
        }

        public static bool DeleteGooglePlusActivity(long credentialID, string activityID, ref string message)
        {
            bool result = false;

            SocialCredentialDTO socialCredentialsDTO = ProviderFactory.Social.GetSocialCredentialInfo(credentialID);

            if (socialCredentialsDTO != null && !string.IsNullOrEmpty(activityID))
            {
                string access_token = GetGooglePlusAccessToke(socialCredentialsDTO);
                if (!string.IsNullOrEmpty(access_token))
                {
                    SIGooglePlusStreamParameters streamParameters = new SIGooglePlusStreamParameters();

                    streamParameters.accessToken = access_token;
                    streamParameters.UniqueID = socialCredentialsDTO.UniqueID;
                    streamParameters.activityID = activityID;

                    SIGooglePlusStreamConnectionParameters StreamConnectionParameters = new SIGooglePlusStreamConnectionParameters(streamParameters);
                    GooglePlusStreamConnection streamconnection = new GooglePlusStreamConnection(StreamConnectionParameters);

                    GooglePlusResponse PostResponse = streamconnection.DeleteGooglePlusActivity();

                    if (PostResponse != null)
                    {
                        if (PostResponse.error.code == 0)
                            result = true;
                        else
                            message = PostResponse.error.message;
                    }
                }
            }

            return result;
        }

        private static string GetGooglePlusAccessToke(SocialCredentialDTO socialCredentialsDTO)
        {
            //Call Connection Class to get data from SDK   
            SIGooglePlusPageParameters pageParameters = new SIGooglePlusPageParameters();

            pageParameters.UniqueID = socialCredentialsDTO.UniqueID;
            pageParameters.client_id = socialCredentialsDTO.AppID;
            pageParameters.client_secret = socialCredentialsDTO.AppSecret;
            pageParameters.refresh_token = socialCredentialsDTO.Token;
            pageParameters.grant_type = "refresh_token";

            // Get Access Token                                
            string access_token = string.Empty;

            SIGooglePlusPageConnectionParameters ConnectionParameters = new SIGooglePlusPageConnectionParameters(pageParameters);
            GooglePlusPageConnection pageconnection = new GooglePlusPageConnection(ConnectionParameters);

            GooglePlusAccessTokenRequestResponse objGooglePlusAccessTokenRequestResponse = pageconnection.GetGooglePlusAccessToken();

            access_token = objGooglePlusAccessTokenRequestResponse.access_token;

            return access_token;
        }


        public static List<GooglePlusComment> GetGoogleComments(UserInfoDTO userInfo, string gPostId, long credentialID, int pageSize)
        {
            List<GooglePlusComment> googlePlusComments = new List<GooglePlusComment>();

            TimeZoneInfo timeZone = ProviderFactory.TimeZones.GetTimezoneForUserID(userInfo.EffectiveUser.ID.GetValueOrDefault());
            string apiKey = "AIzaSyD6EAaOZy0_JJ8sDw_DqfeAU4J6-q69-28";

            SIGooglePlusPostStatisticsParameters postParameters = new SIGooglePlusPostStatisticsParameters();
            postParameters.activityID = gPostId;
            postParameters.apiKey = apiKey;

            SIGooglePlusPostStatisticsConnectionParameters postConnectionParameters = new SIGooglePlusPostStatisticsConnectionParameters(postParameters);
            GooglePlusPostStatisticConnection postconnection = new GooglePlusPostStatisticConnection(postConnectionParameters);

            GooglePlusGetCommentsListResponse Comments = postconnection.GetGooglePlusCommentsListForAnActivity();
            if (Comments != null)
            {
                List<GooglePlusGetCommentsListItem> commentsList = Comments.items.OrderByDescending(c => c.published.ToDateTime()).Take(pageSize).ToList();
                foreach (GooglePlusGetCommentsListItem comment in commentsList.OrderBy(c => c.published.ToDateTime()))
                {
                    GooglePlusComment googlepluscom = new GooglePlusComment();
                    googlepluscom.PostUniqueID = gPostId;
                    googlepluscom.CommentID = comment.id;

                    string message = comment.@object.content;
                    string ReplyTo = string.Empty;
                    var doc = new HtmlDocument();
                    doc.OptionFixNestedTags = true;
                    doc.LoadHtml(message);

                    HtmlNode node = doc.DocumentNode.SelectSingleNode("//span[@class='proflinkWrapper']/a");
                    if (node != null)
                    {
                        //Reply
                        int lastindex = message.LastIndexOf("</span>");
                        message = message.Substring(lastindex + 7);
                        ReplyTo = node.GetAttributeValue("href", "");
                    }

                    googlepluscom.CommentText = message;
                    googlepluscom.LikesCount = comment.plusoners.totalItems;

                    DateTime CommentLocalDate = new SIDateTime(comment.published.ToDateTime().ToUniversalTime(), timeZone).LocalDate.Value;
                    googlepluscom.Timestamp = CommentLocalDate.ToString();

                    googlepluscom.UserImageURL = comment.actor.image.url;
                    googlepluscom.UserName = comment.actor.displayName;

                    googlePlusComments.Add(googlepluscom);
                }
            }

            return googlePlusComments;
        }


        #endregion




        public static List<StreamAccount> SocialAccounts(UserInfoDTO userInfo)
        {
            AccountSearchRequestDTO req = new AccountSearchRequestDTO();
            req.UserInfo = userInfo;
            req.SearchString = "";
            req.SortBy = 2;
            req.SortAscending = true;
            req.StartIndex = 0;
            req.EndIndex = 500;
            req.OnlyIfHasChildAccounts = false;
            req.ConnectedSocialNetworks = new List<SocialNetworkEnum>();
            req.ConnectedSocialNetworks.Add(SocialNetworkEnum.Facebook);
            req.ConnectedSocialNetworks.Add(SocialNetworkEnum.Twitter);
            req.ConnectedSocialNetworks.Add(SocialNetworkEnum.Google);
            req.AccountIDAndDescendants = userInfo.EffectiveUser.CurrentContextAccountID;

            AccountSearchResultDTO accountSearchResult = ProviderFactory.Security.SearchAccounts(req);

            List<StreamAccount> streamAccounts = new List<StreamAccount>();

            foreach (AccountListItemDTO result in accountSearchResult.Items)
            {
                StreamAccount account = new StreamAccount();
                account.ID = result.ID;
                account.Name = result.Name;
                account.Notifications = result.NotificationCount.ToString();
                account.SpriteCssClasses = "ico-folder";
                account.Networks = string.Join(",", result.ConnectedSocialNetworks);

                streamAccounts.Add(account);
            }

            return streamAccounts;
        }



        #endregion

        public static List<PostActivityPostedBy> DealerPostActivityPostedBy(UserInfoDTO userInfo, long locationID, string timeFrame)
        {
            PostTargetSearchRequestDTO req = new PostTargetSearchRequestDTO(userInfo)
            {
                SortBy = 0, // 1=Status, 2=Schedule Date, 3=Title
                SortAscending = false,
                StartIndex = 0,
                EndIndex = 100000
            };

            req.AccountIDs.Add(locationID);
            int days = timeFrame.ToInteger() * -1;
            req.MinDateScheduled = new SIDateTime(userInfo.EffectiveUser.CurrentDateTime.LocalDate.Value.AddDays(days), userInfo.EffectiveUser.ID.Value);

            //TODO change req for just the Posted users
            PostTargetSearchResultsDTO res = ProviderFactory.Social.PostTargetSearch(userInfo, req);

            List<PostActivityPostedBy> postedBys = (from p in res.Items
                                                    select new PostActivityPostedBy
                                                        {
                                                            DisplayName = string.Format("{0} {1}", p.FirstName, p.LastName),
                                                            UserID = p.UserID
                                                        }
                        ).Distinct().ToList();

            return postedBys;
        }

        public static bool PostMenuAction(UserInfoDTO userInfo, long postTargetId, string menuaction, ref string message)
        {
            bool success = false;
            if (menuaction == "delete")
            {
                success = DeletePostfromSocialNetwork(userInfo, postTargetId, ref message);
            }

            return success;
        }

        public static void SetComposeMessageDefaults(ComposeMessage composeMessage, UserDTO userDto)
        {
            DateTime userNow = userDto.CurrentDateTime.LocalDate.GetValueOrDefault();
            int minutes = (15 - userNow.Minute % 15) + userNow.Minute;
            DateTime now15 = userNow.AddMinutes(minutes - userNow.Minute).AddSeconds(userNow.Second * -1).AddMilliseconds(userNow.Millisecond * -1);

            if ((now15 - userNow).Minutes < 15)
                now15.AddMinutes(15);

            int hoursToAdd = 0;
            if (userDto.CanPublishSyndicationOptions)
                hoursToAdd = 1;

            composeMessage.SendDate = new SIDateTime(now15.AddHours(hoursToAdd), userDto.ID.Value).LocalDate.GetValueOrDefault();
            composeMessage.ApprovalExpires = now15.AddMinutes(15);
            composeMessage.ApprovalExpiresMax = now15.AddMinutes(15).AddDays(28);
            composeMessage.UserTimeZone = ProviderFactory.TimeZones.GetTimezoneForUserID(userDto.ID.GetValueOrDefault()).StandardName;

            if (userDto.CanPublishSyndicationOptions)
                composeMessage.IsRequiresApprovals = true;

            composeMessage.UploadImagePath = Settings.GetSetting("site.PublishImageUploadURL");

        }

        public static List<FaceBookPostStatistics> FaceBookPostStatisticsGrid(UserInfoDTO userInfo, PagedGridRequest pagedGridRequest, ref int totalCount)
        {
            List<FaceBookPostStatistics> statisticses = new List<FaceBookPostStatistics>();

            string sortText = "PubishDate";
            bool sortAsc = false;
            if (pagedGridRequest.Sorting.Any())
            {
                PagedGridSortDescriptor sort = pagedGridRequest.Sorting.First();
                sortText = sort.Field;
                sortAsc = sort.Ascending;
            }

            DateTime userNow = userInfo.EffectiveUser.CurrentDateTime.LocalDate.GetValueOrDefault();
            
            List<SocialFacebookPostData> postData = ProviderFactory.Social.GetSocialFacebookPostData(userInfo, userNow.AddDays(-30), userNow);
            totalCount = postData.Count;

            List<SocialFacebookPostData> sortedPostDate = null;
            switch (sortText)
            {
                case "PubishDate":
                    sortedPostDate = sortAsc ? postData.OrderBy(p => p.PublishedDate).ToList() : postData.OrderByDescending(p => p.PublishedDate).ToList();
                    break;
                case "AccountName":
                    sortedPostDate = sortAsc ? postData.OrderBy(p => p.AccountName).ToList() : postData.OrderByDescending(p => p.AccountName).ToList();
                    break;
                case "PostType":
                    sortedPostDate = sortAsc ? postData.OrderBy(p => p.PostTypeID).ToList() : postData.OrderByDescending(p => p.PostTypeID).ToList();
                    break;
                case "Reach":
                    sortedPostDate = sortAsc ? postData.OrderBy(p => p.ImpressionsUnique).ToList() : postData.OrderByDescending(p => p.ImpressionsUnique).ToList();
                    break;
                case "Likes":
                    sortedPostDate = sortAsc ? postData.OrderBy(p => p.Likes).ToList() : postData.OrderByDescending(p => p.Likes).ToList();
                    break;
                case "Comments":
                    sortedPostDate = sortAsc ? postData.OrderBy(p => p.Comments).ToList() : postData.OrderByDescending(p => p.Comments).ToList();
                    break;
                case "Shares":
                    sortedPostDate = sortAsc ? postData.OrderBy(p => p.Shares).ToList() : postData.OrderByDescending(p => p.Shares).ToList();
                    break;
                case "Impressions":
                    sortedPostDate = sortAsc ? postData.OrderBy(p => p.Impressions).ToList() : postData.OrderByDescending(p => p.Impressions).ToList();
                    break;
            }

            foreach (SocialFacebookPostData dto in sortedPostDate.Skip(pagedGridRequest.Skip).Take(pagedGridRequest.Take))
            {
                FaceBookPostStatistics fbStats = new FaceBookPostStatistics();

                fbStats.PostID = dto.FacebookPostID.GetValueOrDefault() + dto.PostTargetID.GetValueOrDefault();
                fbStats.AccountName = dto.AccountName;
                fbStats.Reach = dto.ImpressionsUnique;
                fbStats.Impressions = dto.Impressions;
                fbStats.Likes = dto.Likes;
                fbStats.Shares = dto.Shares;
                fbStats.Comments = dto.Comments;
                fbStats.PubishDate = dto.PublishedDate;
                fbStats.PubishDateText = dto.PublishedDate.ToString("M/dd/yy");
                fbStats.PublishTime = dto.PublishedDate.ToString("h:mm tt");
                fbStats.TimeZoneName = "";

                fbStats.PostDescription = dto.Message.IsNullOptional("");

                fbStats.PostImageUrl = dto.PictureURL.IsNullOptional("");
                fbStats.PostLinkURL = dto.LinkURL.IsNullOptional("");
                fbStats.PostType = dto.PostTypeID.ToString();

                statisticses.Add(fbStats);
            }

            return statisticses;
        }

        public static List<TwitterPostStatistics> TwitterPostStatisticsGrid(UserInfoDTO userInfo, PagedGridRequest pagedGridRequest, ref int totalCount)
        {
            List<TwitterPostStatistics> statisticses = new List<TwitterPostStatistics>();

            string sortText = "PubishDate";
            bool sortAsc = false;
            if (pagedGridRequest.Sorting.Any())
            {
                PagedGridSortDescriptor sort = pagedGridRequest.Sorting.First();
                sortText = sort.Field;
                sortAsc = sort.Ascending;
            }

            DateTime userNow = userInfo.EffectiveUser.CurrentDateTime.LocalDate.GetValueOrDefault();

            List<SocialTwitterPostData> postData = ProviderFactory.Social.GetSocialTwitterPostData(userInfo, userNow.AddDays(-30), userNow);
            totalCount = postData.Count;

            List<SocialTwitterPostData> sortedPostDate = null;
            switch (sortText)
            {
                case "PubishDate":
                    sortedPostDate = sortAsc ? postData.OrderBy(p => p.PublishedDate).ToList() : postData.OrderByDescending(p => p.PublishedDate).ToList();
                    break;
                case "AccountName":
                    sortedPostDate = sortAsc ? postData.OrderBy(p => p.AccountName).ToList() : postData.OrderByDescending(p => p.AccountName).ToList();
                    break;
                case "PostType":
                    sortedPostDate = sortAsc ? postData.OrderBy(p => p.PostTypeID).ToList() : postData.OrderByDescending(p => p.PostTypeID).ToList();
                    break;
                case "ReTweets":
                    sortedPostDate = sortAsc ? postData.OrderBy(p => p.RetweetCount).ToList() : postData.OrderByDescending(p => p.RetweetCount).ToList();
                    break;
                case "IsFavorited":
                    sortedPostDate = sortAsc ? postData.OrderBy(p => p.IsFavorited).ToList() : postData.OrderByDescending(p => p.IsFavorited).ToList();
                    break;
                case "IsRetweeted":
                    sortedPostDate = sortAsc ? postData.OrderBy(p => p.IsRetweeted).ToList() : postData.OrderByDescending(p => p.IsRetweeted).ToList();
                    break;
            }

            foreach (SocialTwitterPostData dto in sortedPostDate.Skip(pagedGridRequest.Skip).Take(pagedGridRequest.Take))
            {
                TwitterPostStatistics twStats = new TwitterPostStatistics();

                twStats.PostID = dto.PostTargetID;
                twStats.AccountName = dto.AccountName;
                twStats.PubishDate = dto.PublishedDate;
                twStats.PubishDateText = dto.PublishedDate.ToString("M/dd/yy");
                twStats.PublishTime = dto.PublishedDate.ToString("h:mm tt");
                twStats.TimeZoneName = "";
                twStats.PostDescription = dto.Message.IsNullOptional("");
                twStats.PostImageUrl = dto.ImageURL.IsNullOptional("");
                twStats.PostLinkURL = dto.Link.IsNullOptional("");
                twStats.PostType = dto.PostTypeID.ToString();
                twStats.IsFavorited = dto.IsFavorited;
                twStats.IsRetweeted = dto.IsRetweeted;
                twStats.ReTweets = dto.RetweetCount;

                statisticses.Add(twStats);
            }

            return statisticses;
        }

        public static RevisePost GetPostForRevision(UserInfoDTO userInfo, long postID, long accountID)
        {
            RevisePost revisePost = new RevisePost();

            revisePost.UploadImagePath = Settings.GetSetting("site.PublishImageUploadURL");
            revisePost.UserTimeZone = ProviderFactory.TimeZones.GetTimezoneForUserID(userInfo.EffectiveUser.ID.GetValueOrDefault()).StandardName;

            GetEntityDTO<RevisePostDTO> revisePostDTO = ProviderFactory.Social.GetPostForRevision(userInfo, postID, accountID);
            if (!revisePostDTO.HasProblems)
            {
                revisePost.PostID = revisePostDTO.Entity.PostID;
                revisePost.SyndicatorName = revisePostDTO.Entity.SyndicatorName;
                revisePost.ImageUrl = "";
                if (revisePostDTO.Entity.PostImages.Any())
                    revisePost.ImageUrl = string.Join(",", revisePostDTO.Entity.PostImages.Select(i => i.URL));

                revisePost.LinkImageUrl = revisePostDTO.Entity.LinkThumbURL.IsNullOptional("");
                revisePost.LinkMessage = revisePostDTO.Entity.LinkDescription;
                revisePost.LinkTitle = revisePostDTO.Entity.LinkTitle;
                revisePost.LinkUrl = revisePostDTO.Entity.Link;
                revisePost.Message = revisePostDTO.Entity.Message;

                if (revisePostDTO.Entity.ScheduleDate != null)
                    revisePost.SendDate = revisePostDTO.Entity.ScheduleDate.LocalDate.GetValueOrDefault();

                revisePost.LocationName = revisePostDTO.Entity.AccountName;
                revisePost.AccountID = revisePostDTO.Entity.AccountID;

                revisePost.Problems = "";

                if (revisePostDTO.Entity.PostType == PostTypeEnum.Link)
                {
                    revisePost.IsLinkPostType = true;
                    revisePost.ForceLinkScrape = true;
                    revisePost.IsNoThumbNail = revisePost.LinkImageUrl == "";
                }

                foreach (RevisePostSocialNetworkInfoDTO net in revisePostDTO.Entity.SocialNetworks)
                {
                    RevisePost.SocialNetwork socialNetwork = new RevisePost.SocialNetwork();
                    socialNetwork.CredentialID = net.CredentialID;
                    socialNetwork.NetworkName = net.SocialNetwork.ToString();
                    socialNetwork.NetworkID = (long)net.SocialNetwork;
                    socialNetwork.NetworkURL = net.URL;
                    socialNetwork.ScreenName = net.ScreenName;
                    socialNetwork.Selected = net.Selected;

                    revisePost.SocialNetworks.Add(socialNetwork);
                }
                revisePost.TargetNetworks = string.Join(",", revisePostDTO.Entity.SocialNetworks.Where(n => n.Selected).Select(n => (long)n.SocialNetwork).ToList());

                revisePost.PublishOptions.AllowChangePublishDate = revisePostDTO.Entity.SyndicationOptions.AllowChangePublishDate;
                revisePost.PublishOptions.MaxDaysBeforeOriginalPublishDate = revisePostDTO.Entity.SyndicationOptions.MaxDaysBeforeOriginalPublishDate;
                revisePost.PublishOptions.MaxDaysAfterOriginalPublishDate = revisePostDTO.Entity.SyndicationOptions.MaxDaysAfterOriginalPublishDate;
                revisePost.PublishOptions.AllowChangeMessage = revisePostDTO.Entity.SyndicationOptions.AllowChangeMessage;
                revisePost.PublishOptions.RequireMessage = revisePostDTO.Entity.SyndicationOptions.RequireMessage;
                revisePost.PublishOptions.AllowChangeImage = revisePostDTO.Entity.SyndicationOptions.AllowChangeImage;
                revisePost.PublishOptions.RequireImage = revisePostDTO.Entity.SyndicationOptions.RequireImage;
                revisePost.PublishOptions.PickSingleImage = revisePostDTO.Entity.SyndicationOptions.PickSingleImage;
                revisePost.PublishOptions.AllowChangeTargets = revisePostDTO.Entity.SyndicationOptions.AllowChangeTargets;
                revisePost.PublishOptions.ForbidFacebookTargetChange = revisePostDTO.Entity.SyndicationOptions.ForbidFacebookTargetChange;
                revisePost.PublishOptions.ForbidTwitterTargetChange = revisePostDTO.Entity.SyndicationOptions.ForbidTwitterTargetChange;
                revisePost.PublishOptions.ForbidGoogleTargetChange = revisePostDTO.Entity.SyndicationOptions.ForbidGoogleTargetChange;
                revisePost.PublishOptions.AllowChangeLinkURL = revisePostDTO.Entity.SyndicationOptions.AllowChangeLinkURL;
                revisePost.PublishOptions.RequireLinkURL = revisePostDTO.Entity.SyndicationOptions.RequireLinkURL;
                revisePost.PublishOptions.AllowChangeLinkText = revisePostDTO.Entity.SyndicationOptions.AllowChangeLinkText;
                revisePost.PublishOptions.AllowChangeLinkThumb = revisePostDTO.Entity.SyndicationOptions.AllowChangeLinkThumb;

                if (revisePostDTO.Entity.SyndicationOptions.MinNewPublishDate == null)
                {
                    revisePost.PublishOptions.MinNewPublishDate = new DateTime(2012, 1, 1);
                }
                else
                {
                    revisePost.PublishOptions.MinNewPublishDate = revisePostDTO.Entity.SyndicationOptions.MinNewPublishDate.LocalDate;
                }

                if (revisePostDTO.Entity.SyndicationOptions.MaxNewPublishDate == null)
                {
                    revisePost.PublishOptions.MaxNewPublishDate = new DateTime(2050, 1, 1);
                }
                else
                {
                    revisePost.PublishOptions.MaxNewPublishDate = revisePostDTO.Entity.SyndicationOptions.MaxNewPublishDate.LocalDate;
                }

                revisePost.PublishOptions.OriginalPublishDate = revisePostDTO.Entity.SyndicationOptions.OriginalPublishDate.LocalDate.GetValueOrDefault();

            }
            else
                revisePost.Problems = string.Join(", ", revisePostDTO.Problems.Select(p => p));

            return revisePost;
        }


        public static bool SavePostRevisionForAccount(RevisePost revisePost, UserInfoDTO userInfo, ref List<string> problems)
        {
            bool Success = false;

            //get permissions
            GetEntityDTO<RevisePostDTO> orgPostDTO = ProviderFactory.Social.GetPostForRevision(userInfo, revisePost.PostID, revisePost.AccountID);
            if (!orgPostDTO.HasProblems)
            {
                RevisePostDTO revisePostDTO = new RevisePostDTO();
                revisePostDTO.PostID = revisePost.PostID;
                revisePostDTO.AccountID = revisePost.AccountID;
                revisePostDTO.ScheduleDate = new SIDateTime(revisePost.SendDate, userInfo.EffectiveUser.ID.GetValueOrDefault());

                if (orgPostDTO.Entity.SyndicationOptions.AllowChangeLinkThumb)
                {
                    if (revisePost.IsNoThumbNail)
                        revisePost.LinkImageUrl = "";
                }
                else
                    revisePost.LinkImageUrl = orgPostDTO.Entity.LinkThumbURL;

                revisePostDTO.Link = revisePost.LinkUrl;
                revisePostDTO.Message = revisePost.Message;
                revisePostDTO.LinkThumbURL = revisePost.LinkImageUrl;
                revisePostDTO.LinkDescription = revisePost.LinkMessage;
                revisePostDTO.LinkTitle = revisePost.LinkTitle;

                revisePostDTO.PostType = PostTypeEnum.Status;
                if (!string.IsNullOrEmpty(revisePost.LinkUrl) && string.IsNullOrEmpty(revisePost.ImageUrl))
                    revisePostDTO.PostType = PostTypeEnum.Link;

                if (orgPostDTO.Entity.SyndicationOptions.AllowChangeImage)
                {
                    if (!string.IsNullOrEmpty(revisePost.ImageUrl))
                    {
                        string[] images = revisePost.ImageUrl.Split(',');
                        foreach (string image in images)
                        {
                            long? imageID = orgPostDTO.Entity.PostImages.Where(i => i.URL == image).Select(i => i.ID).FirstOrDefault();
                            if (imageID.HasValue)
                                revisePostDTO.PostImages.Add(new PostImageDTO { URL = image, ID = imageID.Value });
                            else
                                revisePostDTO.PostImages.Add(new PostImageDTO { URL = image });
                        }

                        revisePostDTO.PostType = PostTypeEnum.Photo;
                    }
                }
                else
                {
                    revisePostDTO.PostImages.AddRange(orgPostDTO.Entity.PostImages);
                }

                foreach (string net in revisePost.TargetNetworks.Split(','))
                {
                    revisePostDTO.SocialNetworks.Add(new RevisePostSocialNetworkInfoDTO { Selected = true, SocialNetwork = (SocialNetworkEnum)net.ToLong() });
                }

                SaveEntityDTO<RevisePostDTO> saveEntityDto = new SaveEntityDTO<RevisePostDTO>(revisePostDTO, userInfo);
                SaveEntityDTO<RevisePostDTO> savedEntityDto = ProviderFactory.Social.SavePostRevisionForAccount(saveEntityDto);

                if (!savedEntityDto.Problems.Any())
                    Success = true;
                else
                    problems = savedEntityDto.Problems;

            }

            return Success;

        }

        public static bool SaveMobileApproval(UserInfoDTO userInfo, long postTargetID, long postID, long accountID, string message, DateTime scheduleDate, ref List<string> problems)
        {
            bool Success = false;

            GetEntityDTO<RevisePostDTO> revisePostDTO = ProviderFactory.Social.GetPostForRevision(userInfo, postID, accountID);
            if (!revisePostDTO.HasProblems)
            {
                revisePostDTO.Entity.Message = message;
                revisePostDTO.Entity.ScheduleDate = new SIDateTime(scheduleDate, userInfo.EffectiveUser.ID.GetValueOrDefault());

                SaveEntityDTO<RevisePostDTO> saveEntityDto = new SaveEntityDTO<RevisePostDTO>(revisePostDTO.Entity, userInfo);
                SaveEntityDTO<RevisePostDTO> savedEntityDto = ProviderFactory.Social.SavePostRevisionForAccount(saveEntityDto);

                if (!savedEntityDto.Problems.Any())
                    Success = true;
                else
                    problems = savedEntityDto.Problems;
            }
            else
                problems = revisePostDTO.Problems;

            return Success;
        }


        public static List<PostManagement> PostManagementGrid(UserInfoDTO userInfo, PagedGridRequest pagedGridRequest, int timeFrame)
        {
            List<PostManagement> posts = new List<PostManagement>();

            PostSearchRequestDTO req = new PostSearchRequestDTO(userInfo)
            {
                SortBy = 1,
                SortAscending = false,
                StartIndex = 1,
                EndIndex = 500
            };

            if (pagedGridRequest.Sorting.Any())
            {
                PagedGridSortDescriptor sort = pagedGridRequest.Sorting.First();
                switch (sort.Field)
                {
                    case "PostDate": req.SortBy = 1; break;
                    case "Title": req.SortBy = 3; break;
                    //case "Type": req.SortBy = 5; break;
                }

                req.SortAscending = sort.Ascending;
            }

            List<long> accounts = new List<long>();
            accounts.Add(userInfo.EffectiveUser.CurrentContextAccountID);
            req.AccountIDs = accounts;

            req.MinDateScheduled = new SIDateTime(userInfo.EffectiveUser.CurrentDateTime.LocalDate.Value.AddDays(timeFrame * -1), userInfo.EffectiveUser.ID.Value);

            PostSearchResultsDTO res = ProviderFactory.Social.PostSearch(userInfo, req);


            foreach (PostSearchResultItemDTO dto in res.Items)
            {
                PostManagement postManagement = new PostManagement();
                postManagement.Title = dto.Title;
                postManagement.PostID = dto.PostID;
                postManagement.Type = dto.Type.ToString();
                postManagement.ErrorCount = dto.Failed;
                postManagement.FacebookCount = dto.FacebookSucceeded;
                postManagement.GoogleCount = dto.GooglePlusSucceeded;
                postManagement.PostDate = dto.Date.LocalDate.GetValueOrDefault();
				postManagement.PostDateText = postManagement.PostDate.Value.ToString("M/dd/yyyy h:mm tt");
				postManagement.TimeZone = dto.Date.TimeZone.StandardName;
                postManagement.ScheduledCount = dto.Total;
                postManagement.SuccessCount = dto.Completed;
                postManagement.TwitterCount = dto.TwitterSucceeded;
                postManagement.CanDelete = dto.AllowDelete;
                postManagement.CanEdit = dto.AllowEdit;

                posts.Add(postManagement);
            }

            return posts;

        }

        public static bool PostManagementAction(UserInfoDTO userInfo, long postID, string menuaction, ref string message)
        {
            bool success = false;
            if (menuaction == "delete")
            {
                ProviderFactory.Social.DeletePost(userInfo, postID);
                success = true;
            }

            return success;
        }

        public static List<PostManagementV2> PostManagementV2Grid(UserInfoDTO userInfo, PagedGridRequest pagedGridRequest, int timeFrame)
        {
            List<PostManagementV2> posts = new List<PostManagementV2>();

            PostSearchRequestDTO req = new PostSearchRequestDTO(userInfo)
            {
                SortBy = 1,
                SortAscending = false,
                StartIndex = 1,
                EndIndex = 500
            };

            if (pagedGridRequest.Sorting.Any())
            {
                PagedGridSortDescriptor sort = pagedGridRequest.Sorting.First();
                switch (sort.Field)
                {
                    case "PostDate": req.SortBy = 1; break;
                    case "Title": req.SortBy = 3; break;
                    //case "Type": req.SortBy = 5; break;
                }

                req.SortAscending = sort.Ascending;
            }

            List<long> accounts = new List<long>();
            accounts.Add(userInfo.EffectiveUser.CurrentContextAccountID);
            req.AccountIDs = accounts;

            req.MinDateScheduled = new SIDateTime(userInfo.EffectiveUser.CurrentDateTime.LocalDate.Value.AddDays(timeFrame * -1), userInfo.EffectiveUser.ID.Value);

            PostSearchResultsDTO res = ProviderFactory.Social.PostSearch(userInfo, req);


            foreach (PostSearchResultItemDTO dto in res.Items)
            {
                PostManagementV2 postManagement = new PostManagementV2();
                postManagement.Title = dto.Title;
                postManagement.PostID = dto.PostID;
                postManagement.Type = dto.Type.ToString();
                postManagement.FailedCount = dto.Failed;
                postManagement.FacebookCount = dto.FacebookCount;
                postManagement.GoogleCount = dto.GooglePlusCount;
                postManagement.PostDate = dto.Date.LocalDate.GetValueOrDefault();
				postManagement.PostDateText = postManagement.PostDate.Value.ToString("M/dd/yyyy h:mm tt");
				postManagement.TimeZone = dto.Date.TimeZone.StandardName;
                postManagement.AccountsCount = dto.DealerCount;
                postManagement.ApprovedCount = dto.DealerApproved;
                postManagement.DeniedCount = dto.DealerDenied;
                postManagement.PostedCount = dto.Completed;
                postManagement.TwitterCount = dto.TwitterCount;
                postManagement.CanDelete = dto.AllowDelete;
                postManagement.CanEdit = dto.AllowEdit;

                posts.Add(postManagement);
            }

            return posts;

        }

        public static bool PostManagementV2Action(UserInfoDTO userInfo, long postID, string menuaction, ref string message)
        {
            bool success = false;
            if (menuaction == "delete")
            {
                ProviderFactory.Social.DeletePost(userInfo, postID);
                success = true;
            }

            return success;
        }

        public static PostManagementDetails PostManagementDetails(UserInfoDTO userInfo, long postId)
        {
            PostManagementDetails postManagementDetails = new PostManagementDetails();

            // not opening for edit
            GetEntityDTO<PostDTO> postDto = ProviderFactory.Social.GetPostByID(userInfo, postId, false);

            if (!postDto.Problems.Any() && postDto.Entity != null)
            {
                postManagementDetails.PostID = postId;
                postManagementDetails.Title = postDto.Entity.Title;
                postManagementDetails.Category = ProviderFactory.Lists.GetPostCategoriesByVerticalID(1).Where(c => c.ID == postDto.Entity.PostCategoryID).Select(c => c.Name).FirstOrDefault();
                postManagementDetails.Status = postDto.Entity.MasterRevision.Message;
                postManagementDetails.Link = postDto.Entity.MasterRevision.Link;
                postManagementDetails.LinkDesc = postDto.Entity.MasterRevision.LinkDescription;
                postManagementDetails.LinkTitle = postDto.Entity.MasterRevision.LinkTitle;
                postManagementDetails.Thumbnail = postDto.Entity.MasterRevision.ImageURL;

                postManagementDetails.CreatedDate = string.Format("{0:M/dd/yyyy h:mm tt} {1}", postDto.Entity.DateCreated.LocalDate.Value, postDto.Entity.MasterRevision.DateCreated.TimeZone.StandardName);

                if (postDto.Entity.MasterRevision.DateCreated != null)
                {
                    if (postDto.Entity.MasterRevision.DateCreated.LocalDate.Value.ToString() != postDto.Entity.DateCreated.LocalDate.Value.ToString())
                        postManagementDetails.LastRevCreatedDate = string.Format("{0:M/dd/yyyy h:mm tt} {1}", postDto.Entity.MasterRevision.DateCreated.LocalDate.Value, postDto.Entity.MasterRevision.DateCreated.TimeZone.StandardName);
                }
                if (postDto.Entity.ScheduleDate != null)
                    postManagementDetails.ScheduldedDate = string.Format("{0:M/dd/yyyy h:mm tt} {1}", postDto.Entity.ScheduleDate.LocalDate.Value, postDto.Entity.ScheduleDate.TimeZone.StandardName);

                //postManagementDetails.PublishedDate = string.Format("{0:M/dd/yyyy h:mm tt} {1}", postDto.Entity.ScheduleDate.LocalDate.Value, postDto.Entity.ScheduleDate.TimeZone.StandardName);
                if (postDto.Entity.ApprovalExpirationDate != null)
                    postManagementDetails.ApprovalExpirationDate = string.Format("{0:M/dd/yyyy h:mm tt} {1}", postDto.Entity.ApprovalExpirationDate.LocalDate.Value, postDto.Entity.ApprovalExpirationDate.TimeZone.StandardName);

                postManagementDetails.PostType = "status";
                if (postDto.Entity.Type == PostTypeEnum.Link)
                    postManagementDetails.PostType = "link";
                else if (postDto.Entity.Type == PostTypeEnum.Photo)
                    postManagementDetails.PostType = "photo";

                foreach (string image in postDto.Entity.Images.Select(i => i.URL).Distinct())
                {
                    postManagementDetails.Photos.Add(string.Format("/ImageSizer?image={0}{1}&h=96", Settings.GetSetting("site.PublishImageUploadURL"), image));
                }

                if (postDto.Entity.SyndicatorID.GetValueOrDefault(0) > 0)
                {
                    if (postDto.Entity.SyndicationOptions.AllowChangeImage)
                        postManagementDetails.EditOptions.Add(string.Format("Image ({0})", postDto.Entity.SyndicationOptions.RequireImage ? "Required" : "Not Required"));
                    if (postDto.Entity.SyndicationOptions.AllowChangeMessage)
                        postManagementDetails.EditOptions.Add(string.Format("Status Message ({0})", postDto.Entity.SyndicationOptions.RequireMessage ? "Required" : "Not Required"));
                    if (postDto.Entity.SyndicationOptions.AllowChangeLinkURL)
                        postManagementDetails.EditOptions.Add(string.Format("Link ({0})", postDto.Entity.SyndicationOptions.RequireLinkURL ? "Required" : "Not Required"));

                    if (postDto.Entity.SyndicationOptions.AllowChangeLinkText)
                        postManagementDetails.EditOptions.Add("Link Title");
                    if (postDto.Entity.SyndicationOptions.AllowChangeLinkThumb)
                        postManagementDetails.EditOptions.Add("Link Thumbnail");
                    if (postDto.Entity.SyndicationOptions.AllowChangePublishDate)
                        postManagementDetails.EditOptions.Add(string.Format("Publish Date (-{0} to +{1} Days)", postDto.Entity.SyndicationOptions.MaxDaysBeforeOriginalPublishDate, postDto.Entity.SyndicationOptions.MaxDaysAfterOriginalPublishDate));
                    if (postDto.Entity.SyndicationOptions.AllowChangeTargets)
                        postManagementDetails.EditOptions.Add("Accounts");
                    if (postDto.Entity.SyndicationOptions.ForbidFacebookTargetChange)
                        postManagementDetails.EditOptions.Add("No Facebook Accounts");
                    if (postDto.Entity.SyndicationOptions.ForbidTwitterTargetChange)
                        postManagementDetails.EditOptions.Add("No Twitter Accounts");
                    if (postDto.Entity.SyndicationOptions.ForbidGoogleTargetChange)
                        postManagementDetails.EditOptions.Add("No Google Accounts");
                }

                if (!postManagementDetails.EditOptions.Any())
                    postManagementDetails.EditOptions.Add("No Edit Options");

            }
            else
                postManagementDetails.Title = postDto.Problems.FirstOrDefault();


            return postManagementDetails;
        }

        public static List<SelectListItem> AccountFranchises(long? accountID)
        {
            List<SelectListItem> items = new List<SelectListItem>();
            items.Add(new SelectListItem() { Text = "All Franchises", Value = "0", Selected = true });

            List<FranchiseTypeDTO> franchiseTypeDtos = ProviderFactory.Social.GetAccountFranchises(accountID.GetValueOrDefault());
            if (franchiseTypeDtos != null)
            {
                foreach (FranchiseTypeDTO dto in franchiseTypeDtos)
                {
                    SelectListItem item = new SelectListItem() { Text = dto.Name, Value = dto.ID.ToString() };
                    items.Add(item);
                }
            }

            return items;
        }
    }

}
