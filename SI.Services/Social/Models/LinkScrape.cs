﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SI.Services.Social.Models
{
	public class LinkScrape
	{
		public string PageTitle { get; set; }
		public string PageDescription { get; set; }
		public List<string> PageImages { get; set; }
		public string PageUrl { get; set; }
		public bool IsValidUrl { get; set; }

		public LinkScrape()
		{
			PageImages = new List<string>();
		}
	}
}
