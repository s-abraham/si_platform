﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;



namespace SI.Services.Social.Models
{
	public class ComposeMessage
	{
		public long PostID { get; set; }
		[Required(ErrorMessage = "Title is required.")]
		public string Title { get; set; }
		[Required(ErrorMessage = "Category is required")]
		public long? CategoryID { get; set; }
		[Required(ErrorMessage = "Message is required.")]
		public string Message { get; set; }
		public string ImageUrl { get; set; }
		public string LinkUrl { get; set; }
		public string LinkImageUrl { get; set; }
		public string LinkTitle { get; set; }
		public string LinkMessage { get; set; }
		public bool ForceLinkScrape { get; set; }
		public string Targets { get; set; }
		public DateTime SendDate { get; set; }
		public bool IsNoThumbNail { get; set; }
		public bool IsAllowRevisions { get; set; }
		public bool IsSendNow { get; set; }
		public bool IsLinkPostType { get; set; }
		public string UploadImagePath { get; set;}
		public bool IsRequiresApprovals { get; set; }
		public bool SelectedAllAccounts { get; set; }
		public string AccountFilterJson { get; set; }
		
		[Required(AllowEmptyStrings = true)]
		public DateTime ApprovalExpires { get; set; }
        public DateTime ApprovalExpiresMax { get; set; }

		public long? SyndicatorID { get; set; }
		public string UserTimeZone { get; set; }

		[Required(ErrorMessage = "Message Type is required.")]
		public string MessageType { get; set; }

		public string Problems { get; set; }

		public SyndicationPublishOptions PublishOptions { get; set; }

		public ComposeMessage()
		{
			PublishOptions = new SyndicationPublishOptions();
		}
	}

	public class MessageAccounts
	{
		public long? CredentialID { get; set; }
		public bool IsCredentialActive { get; set; }
		public bool IsSocialAppActive { get; set; }
		public string Network { get; set; }
		public long NetworkID { get; set; }
		public long? SocialAppID { get; set; }
		public Location DealerLocation { get; set; }
		public DateTime? LastPost { get; set; }
		public DateTime? SchedulePostDate { get; set; }
		public long ResellerID { get; set; }
		public string TZName { get; set; }
		public int TZOffsetMinutes { get; set; }
		public string TimeToPost { get; set; }
		public int CalcOffsetMins { get; set; }
		public bool IsAutoApprove { get; set; }
		public string SocialNetworkUniqueID { get; set; }
		public int UniquenessCount { get; set; }
		public string SocialNetworkScreenName { get; set; }
		public string SocialNetworkPictureURL { get; set; }
		public string SocialNetworkURL { get; set; }

		public MessageAccounts()
		{
			DealerLocation = new Location();
		}


	}
	
	public class Location
	{
		public long LocationID { get; set; }
		public long VirtualGroupID { get; set; }
		public string Name { get; set; }
		public string Address { get; set; }
		public string City { get; set; }
		public string StateAbrev { get; set; }
		public long StateID { get; set; }
		public string Zipcode { get; set; }
		public string OemName { get; set; }
		public long OemID { get; set; }
		public string SiteUrl { get; set; }
	}

	public class MessageTypes
	{
		public string Text { get; set; }
		public string Value { get; set; }
		public string Logo { get; set; }
		public long? SyndicatorID { get; set; }
		public bool AllowPostNow { get; set; }
		public bool ForceApprovals { get; set; }
	}

	public class RevisePost
	{
		public long PostID { get; set; }
		public long AccountID { get; set; }
		public string Message { get; set; }
		public string ImageUrl { get; set; }
		public string LinkUrl { get; set; }
		public string LinkImageUrl { get; set; }
		public string LinkTitle { get; set; }
		public string LinkMessage { get; set; }
		public bool ForceLinkScrape { get; set; }
		public string LocationName { get; set; }
		public DateTime SendDate { get; set; }
		public bool IsNoThumbNail { get; set; }
		public bool IsAllowRevisions { get; set; }
		public bool IsLinkPostType { get; set; }
		public string UploadImagePath { get; set; }
		public string SyndicatorName { get; set; }
		public string UserTimeZone { get; set; }
		public SyndicationPublishOptions PublishOptions { get; set; }

		public List<SocialNetwork> SocialNetworks { get; set; }
		public string TargetNetworks { get; set; }

		public string Problems { get; set; }

		public RevisePost()
		{
			PublishOptions = new SyndicationPublishOptions();
			SocialNetworks = new List<SocialNetwork>();
		}

		public class SocialNetwork
		{
			public long CredentialID { get; set; }
			public string NetworkName { get; set; }
			public long NetworkID { get; set; }
			public string NetworkURL { get; set; }
			public bool Selected { get; set; }
			public string ScreenName { get; set; }
		}
	}

	public class SyndicationPublishOptions
	{
		public bool AllowChangePublishDate { get; set; }
		public int? MaxDaysBeforeOriginalPublishDate { get; set; }
		public int? MaxDaysAfterOriginalPublishDate { get; set; }
		public bool AllowChangeMessage { get; set; }
		public bool RequireMessage { get; set; }
		public bool AllowChangeImage { get; set; }
		public bool RequireImage { get; set; }
		public bool PickSingleImage { get; set; }
		public bool AllowChangeTargets { get; set; }
		public bool ForbidFacebookTargetChange { get; set; }
		public bool ForbidTwitterTargetChange { get; set; }
		public bool ForbidGoogleTargetChange { get; set; }
		public bool AllowChangeLinkURL { get; set; }
		public bool RequireLinkURL { get; set; }
		public bool AllowChangeLinkText { get; set; }
		public bool AllowChangeLinkThumb { get; set; }

		public DateTime? MinNewPublishDate { get; set; }
		public DateTime? MaxNewPublishDate { get; set; }
		public DateTime OriginalPublishDate { get; set; }
	}
	
	public class KendoFilter
	{
		public string field { get; set; }
		public string @operator { get; set; }
		public string value { get; set; }
	}

	public class SyndicationComposeMessage
	{
		public long PostID { get; set; }
		[Required(ErrorMessage = "Title is required.")]
		public string Title { get; set; }
		[Required(ErrorMessage = "Category is required")]
		public long? CategoryID { get; set; }
		[Required(ErrorMessage = "Message is required.")]
		public string Message { get; set; }
		public string ImageUrl { get; set; }
		public string LinkUrl { get; set; }
		public string LinkImageUrl { get; set; }
		public string LinkTitle { get; set; }
		public string LinkMessage { get; set; }
		public bool ForceLinkScrape { get; set; }
		public string Targets { get; set; }
		public DateTime SendDate { get; set; }
		public bool IsNoThumbNail { get; set; }
		public bool IsAllowRevisions { get; set; }
		public bool IsSendNow { get; set; }
		public bool IsLinkPostType { get; set; }
		public string UploadImagePath { get; set; }
		public bool IsRequiresApprovals { get; set; }
		public bool SelectedAllAccounts { get; set; }
		public string AccountFilterJson { get; set; }

		[Required(AllowEmptyStrings = true)]
		public DateTime ApprovalExpires { get; set; }
		public DateTime ApprovalExpiresMax { get; set; }

		public long? SyndicatorID { get; set; }
		public string UserTimeZone { get; set; }

		[Required(ErrorMessage = "Message Type is required.")]
		public string MessageType { get; set; }

		public string Problems { get; set; }

		public SyndicationPublishOptions PublishOptions { get; set; }

		public SyndicationComposeMessage()
		{
			PublishOptions = new SyndicationPublishOptions();
		}
	}

	public class PostManagementDetails
	{
		public long PostID { get; set; }
		public string PostType { get; set; }
		public string Title { get; set; }
		public string Category { get; set; }
		public string Status { get; set; }
		public string Link { get; set; }
		public string LinkDesc { get; set; }
		public string LinkTitle { get; set; }
		public string Thumbnail { get; set; }
		public List<string> Photos { get; set; }
		public string UploadImagePath { get; set; }

		public string CreatedDate { get; set; }
		public string LastRevCreatedDate { get; set; }
		public string ScheduldedDate { get; set; }
		public string PublishedDate { get; set; }
		public string ApprovalExpirationDate { get; set; }

		public List<string> EditOptions { get; set; }

		public PostManagementDetails()
		{
			Photos = new List<string>();
			EditOptions = new List<string>();
		}
	}
}
