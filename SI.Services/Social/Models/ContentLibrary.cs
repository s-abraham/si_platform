﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SI.Services.Social.Models
{
	public class ContentLibraryRSSItems
	{
		public long RSSItemID { get; set; }
		public string Title { get; set; }
		public string Link { get; set; }
		public string Summary { get; set; }
		public DateTime PubDate { get; set; }
		public string ImageURL { get; set; }


	}
}
