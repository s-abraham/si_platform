﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Kendo.Mvc.UI;

namespace SI.Services.Social.Models
{
	public class PostActivity
	{
		public long PostTargetID { get; set; }
		public long PostID { get; set; }
		public string NetworkName { get; set; }
		public string SocialUrl { get; set; }
		public string Message { get; set; }
		public string Title { get; set; }
		public int Actions { get; set; }
		public string Type { get; set; }
		public string PostImageURL { get; set; }
		public string Status { get; set; }
		public string StatusName { get; set; }
		public string StatusDateText { get; set; }
		public string TimeZoneName { get; set; }
		public DateTime? ScheduleDate { get; set; }
		public string PostedBy { get; set; }
		public string DetailReportUrl { get; set; }
		public string LocationName { get; set; }
		public string FailureMessage { get; set; }
		public bool CanEdit { get; set; }

		public DateTime PostDate { get; set; } //filtering
		public long OemID { get; set; }
		public long LocationID { get; set; }
		public long VirtualGroupID { get; set; }
		public long NetworkID { get; set; }
		public string TimeFrameFilter { get; set; }
	}

	public class PostActivityCalendar : ISchedulerEvent
	{
		public long PostTargetID { get; set; }
		public long PostID { get; set; }
		public string NetworkName { get; set; }
		public string SocialUrl { get; set; }
		public string Message { get; set; }

		public string Title { get; set; }
		public string Description { get; set; }
		public bool IsAllDay { get; set; }
		public DateTime Start { get; set; }
		public DateTime End { get; set; }
		public string StartTimezone { get; set; }
		public string EndTimezone { get; set; }
		public string RecurrenceRule { get; set; }
		public string RecurrenceException { get; set; }

		public int Actions { get; set; }
		public string Type { get; set; }
		public string PostImageURL { get; set; }
		public string Status { get; set; }
		public string StatusName { get; set; }
		public string StatusDateText { get; set; }
		public string TimeZoneName { get; set; }
		public DateTime? ScheduleDate { get; set; }

		public string PostedBy { get; set; }
		public string DetailReportUrl { get; set; }
		public string LocationName { get; set; }
		public string FailureMessage { get; set; }
		public int PostTypeResource { get; set; }
		
	}

	public class PostActivityPostedBy : IEquatable<PostActivityPostedBy>
	{
		public long UserID { get; set; }
		public string DisplayName { get; set; }

		public bool Equals(PostActivityPostedBy other)
		{
			if (DisplayName.ToLower() == other.DisplayName.ToLower())
				return true;

			return false;
		}

		public override int GetHashCode()
		{
			return DisplayName == null ? 0 : DisplayName.GetHashCode();
			
		}
	}

	public class PostManagement
	{
		public long PostID { get; set; }
		public int ScheduledCount { get; set; }
		public int SuccessCount { get; set; }
		public int ErrorCount { get; set; }

		public string Message { get; set; }
		public string Title { get; set; }
		public int Actions { get; set; }
		public string Type { get; set; }
		
		public DateTime? PostDate { get; set; }
		public string PostDateText { get; set; }
		public string TimeZone { get; set; }

		public int FacebookCount { get; set; }
		public int TwitterCount { get; set; }
		public int GoogleCount { get; set; }

		public bool CanEdit { get; set; }
		public bool CanDelete { get; set; }
		
	}

	public class PostManagementV2
	{
		public long PostID { get; set; }
		public int AccountsCount { get; set; }
		public int ApprovedCount { get; set; }
		public int DeniedCount { get; set; }
		public int PostedCount { get; set; }
		public int FailedCount { get; set; }

		public string Message { get; set; }
		public string Title { get; set; }
		public int Actions { get; set; }
		public string Type { get; set; }

		public DateTime? PostDate { get; set; }
		public string PostDateText { get; set; }
		public string TimeZone { get; set; }
		
		public int FacebookCount { get; set; }
		public int TwitterCount { get; set; }
		public int GoogleCount { get; set; }

		public bool CanEdit { get; set; }
		public bool CanDelete { get; set; }

	}
}
