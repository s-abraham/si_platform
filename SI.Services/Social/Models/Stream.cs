﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SI.Services.Social.Models
{
	public class FacebookStream
	{
		public string PageName { get; set; }
		public string PostedBy { get; set; }
		public string PostUniqueID { get; set; }
		public string PostDate { get; set; }
		public string PostTimeStamp { get; set; }
		public string UserTimeZone { get; set; }
		public string PageImageURL { get; set; }
		public string Message { get; set; }
		public string PostImageURL { get; set; }
		public string LinkImageURL { get; set; }
		public string LinkCaption { get; set; }
		public string LinkURL { get; set; }
		public string LinkText { get; set; }
		public string Type { get; set; }
		public long CredentialID { get; set; }
		public int LikesCount { get; set; }
		public int CommentCount { get; set; }
		public int SharesCount { get; set; }
		public int InsightCount { get; set; }
		public string NewComment { get; set; }
        public string Until { get; set; }
		public string PostURL { get; set; }
	}

	public class FacebookComment
	{
		public string PostUniqueID { get; set; }
		public string CommentID { get; set; }
		public string UserName { get; set; }
		public string UserImageURL { get; set; }
		public string CommentText { get; set; }
		public int LikesCount { get; set; }
		public string Timestamp { get; set; }
        public bool UserLikes { get; set; }
		public long CredentialID { get; set; }
	}

	public class StreamAccount
	{
		public long ID { get; set; }
		public string Name { get; set; }
		public string SpriteCssClasses { get; set; }
		public string Notifications { get; set; }
		public string Networks { get; set; }
	}

	public class TwitterStream
	{
		public string ID { get; set; }
		public string Text { get; set; }
		public string TimeStamp { get; set; }
		public string TweetDate { get; set; }
		public string UserTimeZone { get; set; }
		public string Since { get; set; }
		public bool Favorited { get; set; }
		public string TweetImageURL { get; set; }
		public string Section { get; set; }

		public string UserName { get; set; }
		public string UserScreenName { get; set; }
		public string UserID { get; set; }
		public string UserImageURL { get; set; }

		public long CredentialID { get; set; }

	}

}
