﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SI.Services.Social.Models
{
    public class GooglePlusStream
    {
        public GooglePlusStream()
        {

        }

        //public string objectid { set; get; } //Google+'s id
        //public string link { get; set; } //Google+'s url

        //private string _linktopage;

        //public string linktopage
        //{
        //    get
        //    {
        //        return link.Substring(0, link.IndexOf("/posts"));
        //    }
        //}

        //public DateTime createdtime { get; set; }//Google+'s published
        //private string _since;
        //public string since
        //{
        //    get
        //    {
        //        return FormatDateAgo(createdtime);
        //    }
        //}
        
        ////Google+'s published (Modified)
        //public string fromname { get; set; } //Google+'s actor.displayName
        //public string fromid { get; set; } //Google+'s actor.id
        //public string fromurl { get; set; } //Google+'s actor.url
        //public string frompictureUrl { get; set; } //Google+'s actor.image.url

        //public string caption { get; set; }  //Google+'s title
        //public string message { get; set; } //Google+'s @object.content
        
        //public int LikesCount { get; set; } 
        //public int CommentsCount { get; set; } 
        //public int ResharesCount { get; set; } 

        //public string PictureURL { get; set; } 
        //public string LinkURL { get; set; } 

        //public string type { get; set; } //Google+'s @object.objectType

        public string UserTimeZone { get; set; }
        public string PageImageURL { get; set; }
        public string PageName { get; set; }

        public string PostedBy { get; set; }
        public string PostUniqueID { get; set; }
        public string PostDate { get; set; }
		public string PostTimeStamp
        {
            get
            {
                return FormatDateAgo(PostDate);
            }
        }
       
        public string Message { get; set; }
        public string PostImageURL { get; set; }
        public string LinkImageURL { get; set; }
        public string LinkURL { get; set; }        
        public string LinkCaption { get; set; }        
        public string LinkText { get; set; }
        public string Type { get; set; }

        public long CredentialID { get; set; }
        
        public int LikesCount { get; set; }
        public int CommentCount { get; set; }
        public int SharesCount { get; set; }
        
        public string NewComment { get; set; }
        
        public string PostURL { get; set; }

        private string FormatDateAgo(string oDate)
        {
            var date = Convert.ToDateTime(oDate);
            var dateNow = DateTime.UtcNow;

            if (dateNow >= date)
            {
                if (dateNow.Year != date.Year)
                {
                    // get number of months since posting
                    var iMonthCount = 0;
                    var dateIndex = date;
                    while (dateIndex <= dateNow)
                    {
                        iMonthCount++;
                        dateIndex = dateIndex.AddMonths(1);
                    }

                    if (iMonthCount > 11)
                    {
                        return "Over a year ago";
                    }
                    else if (iMonthCount == 1)
                    {
                        return "1 month ago";
                    }
                    else
                    {
                        return iMonthCount.ToString() + " months ago";
                    }
                }

                var iMonthDiff = dateNow.Month - date.Month;
                if (iMonthDiff > 0)
                {
                    if (iMonthDiff == 1)
                    {
                        return "1 month ago";
                    }
                    return iMonthDiff.ToString() + " months ago";
                }

                TimeSpan ts = dateNow.Subtract(date);
                if (ts.Days > 0)
                {
                    if (ts.Days == 1)
                    {
                        return "1 day ago";
                    }
                    return ts.Days.ToString() + " days ago";
                }

                if (ts.Hours > 0)
                {
                    if (ts.Hours == 1)
                    {
                        return "1 hour ago";
                    }
                    return ts.Hours.ToString() + " hours ago";
                }

                if (ts.Minutes > 0)
                {
                    if (ts.Minutes == 1)
                    {
                        return "1 minute ago";
                    }
                    return ts.Minutes.ToString() + " minutes ago";
                }
            }

            return "0 minutes ago";
        }
    }

    public class GooglePlusComment
    {
        public string PostUniqueID { get; set; }
        public string CommentID { get; set; }
        public string UserName { get; set; }
        public string UserImageURL { get; set; }
        public string CommentText { get; set; }
        public int LikesCount { get; set; }
        public string Timestamp { get; set; }
    }
}
