﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SI.Services.Social.Models
{
	public class FaceBookPostStatistics
	{
		public long PostID { get; set; }
		public DateTime PubishDate { get; set; }
		public string PubishDateText { get; set; }
		public string PublishTime { get; set; }
		public string TimeZoneName { get; set; }
		public string AccountName { get; set; }
		public string PostImageUrl { get; set; }
		public string PostDescription { get; set; }
		public string PostLinkURL { get; set; }
		public string PostType { get; set; }
		public int? Reach { get; set; }
		public int? Likes { get; set; }
		public int? Comments { get; set; }
		public int? Shares { get; set; }
		public int? Impressions { get; set; }
	}

	public class TwitterPostStatistics
	{
		public long PostID { get; set; }
		public DateTime PubishDate { get; set; }
		public string PubishDateText { get; set; }
		public string PublishTime { get; set; }
		public string TimeZoneName { get; set; }
		public string AccountName { get; set; }
		public string PostImageUrl { get; set; }
		public string PostDescription { get; set; }
		public string PostLinkURL { get; set; }
		public string PostType { get; set; }
		public int? ReTweets { get; set; }
		public bool IsFavorited { get; set; }
		public bool IsRetweeted { get; set; }
		
	}
}
