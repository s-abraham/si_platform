﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SI.Services.SupportCenter.Models
{
	

	public class ThemesVertical
	{
		public int ID { get; set; }
		public string Name { get; set; }
		[DataType(DataType.Date)]
		public DateTime CreateDate { get; set; }
		public bool CurrentTheme { get; set; }
		public string ThemePreviewImage { get; set; }
	}

	public class Package
	{
		public int ID { get; set; }
		public string Name { get; set; }
		public bool Selected { get; set; }

	}

	public class Employee
	{
		public int ID { get; set; }
		[Required(ErrorMessage = "First Name is required.")]
		[Display(Name = "First Name")]
		public string FirstName { get; set; }
		
		[Display(Name = "Last Name")]
		[Required(ErrorMessage = "Last Name is required")]
		public string LastName { get; set; }

		[Required(ErrorMessage = "Role is required")]
		[Display(Name = "Role")]
		public int RoleID { get; set; }
		public string RoleName { get; set; }

		[DataType(DataType.Date)]
		public DateTime CreateDate { get; set; }
		[DataType(DataType.Date)]
		public DateTime ModifiedDate { get; set; }
		public int NumberOfAccounts { get; set; }
		public string Status { get; set; }
		
		[Required(ErrorMessage = "Email Address is required.")]
		[Display(Name = "Email Address")]
		[DataType(DataType.EmailAddress)]
		[RegularExpression(@"[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.[A-Za-z]{2,4}", ErrorMessage = "Email is not valid.")]
		public string Email { get; set; }
		
		[DataType(DataType.Password)]
		[StringLength(40, MinimumLength = 8, ErrorMessage = "Your password must be at least 8 characters!")]
		public string Password { get; set; } //!!! DO NOT POPULATE
		[Display(Name = "Verify Password")]
		[Compare("Password")]
		public string VerifyPassword { get; set; }
	}

	public class PlatformMessageConfig
	{
		public int ID { get; set; }
		public string Name { get; set; }
		public string ParameterDefinitions { get; set; }
		public bool RequiresAcknowledgment { get; set; }
		public string WhoCanAcknowlege { get; set; }
		public DateTime? ActiveStart { get; set; }
		public DateTime? ActiveEnd { get; set; }
		public DateTime? LastModified { get; set; }

	}

}
