﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using SI.Services.Framework.Models;
using SI.Services.SupportCenter.Models;

namespace SI.Services.SupportCenter
{
	public static class SupportCenterService
	{
		public static List<SelectListItem> GetRoleTypes()
		{
			List<SelectListItem> roles = new List<SelectListItem>();
			roles.Add(new SelectListItem { Selected = false, Text = "Developer", Value = "1" });
			roles.Add(new SelectListItem { Selected = false, Text = "Support", Value = "2" });
			roles.Add(new SelectListItem { Selected = false, Text = "Sales", Value = "3" });

			return roles;
		}

		public static List<Employee> EmployeeEditGrid(PagedGridRequest pagedGridRequest)
		{
			List<Employee> employees = new List<Employee>();
			Employee emp = new Employee();
			emp.ID = 1;
			emp.FirstName = "Reynolds";
			emp.LastName = "Kosloskey";
			emp.RoleID = 1;
			emp.RoleName = "Developer";
			emp.CreateDate = DateTime.Now;
			emp.ModifiedDate = DateTime.Now;
			emp.NumberOfAccounts = 125;
			emp.Status = "Active";
			emp.Email = "reynolds@autostartups.com";
			employees.Add(emp);

			Employee emp2 = new Employee();
			emp2.ID = 2;
			emp2.FirstName = "Aaron";
			emp2.LastName = "Neve";
			emp2.RoleID = 2;
			emp2.RoleName = "Support";
			emp2.CreateDate = DateTime.Now;
			emp2.ModifiedDate = DateTime.Now;
			emp2.NumberOfAccounts = 650;
			emp2.Status = "Active";
			emp2.Email = "Aaron@socialdealer.com";
			employees.Add(emp2);

			Employee emp3 = new Employee();
			emp3.ID = 3;
			emp3.FirstName = "Bob";
			emp3.LastName = "Thomson";
			emp3.RoleID = 3;
			emp3.RoleName = "Sales";
			emp3.CreateDate = DateTime.Now;
			emp3.ModifiedDate = DateTime.Now;
			emp3.NumberOfAccounts = 0;
			emp3.Status = "Inactive";
			emp3.Email = "bob@gmail.com";
			employees.Add(emp3);

			return employees;
		}

		public static List<PlatformMessageConfig> PlatformMessagesEditGrid(PagedGridRequest pagedGridRequest, string demoValue)
		{
			List<PlatformMessageConfig> configs = new List<PlatformMessageConfig>();

			PlatformMessageConfig conf = new PlatformMessageConfig();
			conf.ID = 1;
			conf.Name = "Welcome";
			conf.ActiveStart = DateTime.Now.AddDays(-10);
			conf.ActiveEnd = DateTime.Now.AddDays(30);
			conf.LastModified = DateTime.Now;
			conf.RequiresAcknowledgment = true;
			conf.WhoCanAcknowlege = "Any member of company";
			conf.ParameterDefinitions = WebUtility.HtmlDecode("{\"fields\": [{\"name\":\"Title\",\"type\":\"text\",\"value\":\"Welcome to SOCIALDEALER\"},{\"name\":\"BodyHtml\",\"type\":\"html\",\"htmlValue\":\"Hello from &lt;strong&gt;&lt;span style=\\\"color:#993300;\\\"&gt;SOCIALDEALER&lt;/span&gt;&lt;/strong&gt;, Inc.&lt;br /&gt;\"}]}");

			//DEMO ONLY to simulate saving
			if (!string.IsNullOrEmpty(demoValue))
				conf.ParameterDefinitions = WebUtility.HtmlDecode(demoValue);

			configs.Add(conf);

			return configs;
		}

		public static List<ThemesVertical> ThemesVerticalEditGrid(PagedGridRequest pagedGridRequest)
		{
			List<ThemesVertical> themes = new List<ThemesVertical>();
			ThemesVertical themesVertical = new ThemesVertical();
			themesVertical.ID = 1;
			themesVertical.Name = "Social Integration";
			themesVertical.CreateDate = DateTime.Now;
			themesVertical.ThemePreviewImage = "/assets/images/ThemePreview-autostartups.png";
			themes.Add(themesVertical);

			ThemesVertical themesVertical2 = new ThemesVertical();
			themesVertical2.ID = 2;
			themesVertical2.Name = "SOCIALDEALER";
			themesVertical2.CreateDate = DateTime.Now;
			themesVertical2.CurrentTheme = true;
			themesVertical2.ThemePreviewImage = "/assets/images/ThemePreview-socialdealer.png";
			themes.Add(themesVertical2);

			ThemesVertical themesVertical3 = new ThemesVertical();
			themesVertical3.ID = 3;
			themesVertical3.Name = "CBS Digital Media";
			themesVertical3.CreateDate = DateTime.Now;
			themesVertical3.ThemePreviewImage = "/assets/images/ThemePreview-cbsdigitalmedia.png";
			themes.Add(themesVertical3);

			return themes;
		}

		public static List<Package> PackagesEditGrid(PagedGridRequest pagedGridRequest)
		{
			List<Package> packages = new List<Package>();
			Package pack = new Package();
			pack.ID = 1;
			pack.Name = "Reputation Basic";
			pack.Selected = false;
			packages.Add(pack);

			Package pack2 = new Package();
			pack2.ID = 2;
			pack2.Name = "Reputation Pro";
			pack2.Selected = true;
			packages.Add(pack2);

			Package pack3 = new Package();
			pack3.ID = 3;
			pack3.Name = "Reputation Enterprise";
			pack3.Selected = false;
			packages.Add(pack3);

			return packages;
		}
	}
}
