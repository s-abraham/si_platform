﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SI.Services.Dashboard.Models
{
	public class ToDoSummary
	{
		public int ID { get; set; }
		public string ActionImage { get; set; }
		public string Text { get; set; }
		public string AlertLevel { get; set; }
	}

	public class NotificationSummary
	{
		public int ID { get; set; }
		public string ActionImage { get; set; }
		public string Text { get; set; }
		public string AlertLevel { get; set; }
		public int Count { get; set; }
	}

	public class MyPrefAlertEdit
	{
		public long? AccountID { get; set; }
		public long? UserID { get; set; }
		public bool DisableAll { get; set; }
		public string ReceiveDayOfWeek { get; set; }
		public TimeSpan? ReceiveTimeOfDay { get; set; }
	}

	public class MyPrefPassword
	{
		[Required]
		[Display(Name = "Current Password")]
		[DataType(DataType.Password)]
		public string CurrentPassword { get; set; }

		[Required]
		[DataType(DataType.Password)]
		[Display(Name = "Change Password")]
		[RegularExpression(@"((?=.*\d)(?=.*[!@#$%^&*()_+]).{8,40})", ErrorMessage = "Password requires one special case letter and one digit, minumum 8.")]
		public string Password { get; set; }

		[Required]
		[Display(Name = "Verify Password")]
		[Compare("Password")]
		public string VerifyPassword { get; set; }
	}

	public class SetPassword
	{
		public string CurrentPassword { get; set; }

		[Required]
		[DataType(DataType.Password)]
		[Display(Name = "Change Password")]
		[RegularExpression(@"((?=.*\d)(?=.*[!@#$%^&*()_+]).{8,40})", ErrorMessage = "Password requires one special case letter and one digit, minumum 8.")]
		public string Password { get; set; }

		[Required]
		[Display(Name = "Verify Password")]
		[Compare("Password")]
		public string VerifyPassword { get; set; }
	}
	

	public class MyPrefReportEdit
	{
		public long? AccountID { get; set; }
		public long? UserID { get; set; }
		public bool ReportDisableAll { get; set; }
		public string ReportReceiveDayOfWeek { get; set; }
		public TimeSpan? ReportReceiveTimeOfDay { get; set; }
	}
	
	public class MyPrefNotifications
	{
		public int ID { get; set; }
		public string Name { get; set; }
		public string Description { get; set; }
		public int AccountCount { get; set; }
		
		public bool IsAlert { get; set; }
		public bool IsReport { get; set; }

		public bool HasSMSNotifications { get; set; }
		public bool HasPlatformNotifications { get; set; }
		public bool HasEmailNotifications { get; set; }

		public bool PlatformNotification { get; set; } 
		public bool SMSNotification { get; set; }
		public bool EmailNotification { get; set; }
	}
	
	public class NotificationConfigItem
	{
		public int ID { get; set; }

		public List<long> AccountIDs { get; set; }

		public bool HasHourIntervalConfig { get; set; }
		public bool HasHourConfig { get; set; }
		public bool HasDayConfig { get; set; }
		public bool HasDayOfMonthConfig { get; set; }

		public string HourConfig { get; set; }
		public string DayConfig { get; set; }
		public int DayOfMonthConfig { get; set; }
		public int HourIntervalConfig { get; set; }

		public NotificationConfigItem()
		{
			AccountIDs = new List<long>();
		}
	}

	public class MyPrefDetails
	{
		public int id { get; set; }
		public IEnumerable<long> accounts { get; set; }
		public string hourConfig { get; set; }
		public string dayConfig { get; set; }
		public int hourInterval { get; set; }
		public int dayOfMonth { get; set; }
	}

	public class NotificationsToDo
	{
		public int ID { get; set; }
		public string Type { get; set; }
		public string ActionImage { get; set; }
		public string LocationName { get; set; }
		public string Description { get; set; }
		public DateTime ToDoDateLocal { get; set; }
		public bool Completed { get; set; }
		public DateTime? CompletedDateLocal { get; set; }
	}


	public class NotificationsAlert
	{
		public long ID { get; set; }
		public string Type { get; set; }
		public string ActionImage { get; set; }
		public string LocationName { get; set; }
		public string Description { get; set; }
		public string Status { get; set; }
		
	}

	public class NotificationsApprovals
	{
		public long ID { get; set; }
		public long PostTargetID { get; set; }
		public long PostID { get; set; }
		public long TargetAccountID { get; set; }
		public string Type { get; set; }
		public string ActionImage { get; set; }
		public string LocationName { get; set; }
		public string ScreenName { get; set; }
		public string SyndicatorName { get; set; }
		public string Description { get; set; }
		public string Status { get; set; }
		public DateTime AlertDateLocal { get; set; }
		public string AlertDateLocalText { get; set; }
		public string TimeZone { get; set; }
		public string ImageUrl { get; set; }
		public string Link { get; set; }
		public string LinkTitle { get; set; }
		public string LinkDescription { get; set; }
		public string LinkImageUrl { get; set; }
		public string NetworkName { get; set; }
		public string SocialNetworkURL { get; set; }
		public bool AllowChanges { get; set; }
		public bool AllowMessageChange { get; set; }
		public bool AllowScheduleDateChange { get; set; }

	}

	public class NotificationsApprovalHistory
	{
		public long ID { get; set; }
		public long PostTargetID { get; set; }
		public long PostID { get; set; }
		public long TargetAccountID { get; set; }
		public string Type { get; set; }
		public string ActionImage { get; set; }
		public string LocationName { get; set; }
		public string ScreenName { get; set; }
		public string SyndicatorName { get; set; }
		public string Description { get; set; }
		public string Status { get; set; }
		public DateTime AlertDateLocal { get; set; }
		public string AlertDateLocalText { get; set; }
		public string TimeZone { get; set; }

		public DateTime ActedOnDateLocal { get; set; }
		public string ActedOnDateLocalText { get; set; }

		public string ImageUrl { get; set; }
		public string Link { get; set; }
		public string LinkTitle { get; set; }
		public string LinkDescription { get; set; }
		public string LinkImageUrl { get; set; }
		public string NetworkName { get; set; }
		public string SocialNetworkURL { get; set; }

		public bool AllowEdit { get; set; }
		public bool AllowDeny { get; set; }
		public bool AllowApprove { get; set; }
		public bool AllowUnApprove { get; set; }
		public bool AllowUnDeny { get; set; }
		
	}
}
