﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SI.Services.Reputation.Models;

namespace SI.Services.Dashboard.Models
{
	public class Overview
	{
		public ReputationOverview reputationOverview { get; set; }
		public SocialOverview socialOverview { get; set; }

		public Overview()
		{
			reputationOverview = new ReputationOverview();
			socialOverview = new SocialOverview();
		}
	}

	public class ReputationOverview
	{
        public double AllNonZeroRatings { get; set; }
        public int ReviewDetailCount { get; set; }

		public int UnReadPostive { get; set; }
		public string UnReadPostiveFilter { get; set; }
		public int UnReadNegative { get; set; }
		public string UnReadNegativeFilter { get; set; }
		public int UnReadFromYesterday { get; set; }
		public string UnReadFromYesterdayFilter { get; set; }
		public int UnReadThisMonth { get; set; }
		public string UnReadThisMonthFilter { get; set; }
		public string UnReadFilter { get; set; }

		public int NumberOfDealers { get; set; }

		public ReviewsCounts ReviewsAllTime { get; set; }
		public ReviewsCounts ReviewsLast30 { get; set; }
		public ReviewsCounts ReviewsLast7 { get; set; }

		public int ToDoesRead { get; set; }
		public int ToDoesUnRead { get; set; }
		public int ToDoesResponsed { get; set; }
		public int ToDoesNoResponse { get; set; }

        public int GridMaxReviewCountPerSite { get; set; }
        public int[] ReviewCountPerSite { get; set; }
		public double[] AverageRatingsPerSite { get; set; }
		public string[] RatingSites { get; set; }

		public List<SiteReviewCounts> ReviewsBySite { get; set; }

		public int totalAllTimePercentNeg { get; set; }
		public int totalAllTimePercentPos { get; set; }
		public int totalLast30PercentNeg { get; set; }
		public int totalLast30PercentPos { get; set; }
		public int totalLast7PercentNeg { get; set; }
		public int totalLast7PercentPos { get; set; }
		public int CustomerSatisfaction { get; set; }
		
		public ReputationOverview()
		{
			ReviewsAllTime = new ReviewsCounts();
			ReviewsLast30 = new ReviewsCounts();
			ReviewsLast7 = new ReviewsCounts();
			ReviewsBySite = new List<SiteReviewCounts>();
		}
	}

	public class ReviewCountPerSite
	{
		public int Count { get; set; }
		public int PosCount { get; set; }
		public int NegCount { get; set; }
		public string Site { get; set; }

		public long SiteID { get; set; }

		public Stream.Location DealerLocation { get; set; }

		public ReviewCountPerSite()
		{
			DealerLocation = new Stream.Location();
		}
	}

	public class ReviewPosNegChart
	{
		public string category { get; set; }
		public string value { get; set; }
		public string color { get; set; }
	}

	public class ReviewsCounts
	{
		public int NumberOfReviews { get; set; }
		public string NumberOfReviewsFilter { get; set; }
		public int TotalPositive { get; set; }
		public string TotalPositiveFilter { get; set; }
		public int TotalNegative { get; set; }
		public string TotalNegativeFilter { get; set; }
		public double AverageRating { get; set; }
		
	}

	

	public class SiteReviewCounts
	{
		public string SiteName { get; set; }
		public int Total { get; set; }
		public int TotalPositive { get; set; }
		public int TotalNegative { get; set; }
		public int PercentPositive { get; set; }
		public int PercentNegative { get; set; }
		public double AverageRating { get; set; }
		public string SiteFilter { get; set; }
		public string category { get; set; }
		public int value { get; set; }
	}

	public class ReputationSitesReview
	{
		public DateTime MonthDate { get; set; }
		public string ReviewSource { get; set; }
		public double AvgRating { get; set; }

	}

	public class SocialOverview
	{
		public int NumberOfDealers { get; set; }
		public int ToDoFromYesterday { get; set; }
		public int ToDoThisMonth { get; set; }

		public SocialNetwork Facebook { get; set; }
		public SocialNetwork Twitter { get; set; }
		public SocialNetwork GooglePlus { get; set; }
		public SocialNetwork Blogger { get; set; }

		public int ToDoesComments { get; set; }
		public int ToDoesMessages { get; set; }
		public int ToDoesMentions { get; set; }

		public string[] Months { get; set; }
		public int[] FBLikes { get; set; }

		public int[] TWFollowers { get; set; }
		public int[] GoogleFollowers { get; set; }
		public int[] YTSubscribers { get; set; }

		public int[] Totals { get; set; }

        public int FacebookPageLikes_AllTime { get; set; }
        public int GooglePlusPlusOneCount_AllTime { get; set; }
        public int TwitterFollowersCount_AllTime { get; set; }

        public int FacebookPageLikes_30days { get; set; }
        public int GooglePlusPlusOneCount_30days { get; set; }
        public int TwitterFollowersCount_30days { get; set; }

        public int FacebookPageLikes_7days { get; set; }
        public int GooglePlusPlusOneCount_7days { get; set; }
        public int TwitterFollowersCount_7days { get; set; }

		public SocialOverview()
		{
			Facebook = new SocialNetwork { Name = "Facebook" };
			Twitter = new SocialNetwork { Name = "Twitter" };
			GooglePlus = new SocialNetwork { Name = "Google+" };
			Blogger = new SocialNetwork { Name = "Blogger" };
		}
	}

	public class SocialNetwork
	{
		public string Name { get; set; }
		public int PostTweetCount { get; set; }
		public int Comments { get; set; }
		public int Messages { get; set; }
		public int WallPosts { get; set; }
		public int Mentions { get; set; }
        public int SharesReTweets { get; set; }
		public int Likes { get; set; }
	}
}
