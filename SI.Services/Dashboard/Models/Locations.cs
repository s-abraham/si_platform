﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SI.Services.Dashboard.Models
{
	public class Dealerships
	{
		public Location DealerLocation { get; set; }
		public double AverageReviewScore { get; set; }
		public string AverageReviewScoreFilter { get; set; } //goes to review sites filtered on location

		public int NumberOfReviews { get; set; }
		public string NumberOfReviewsFilter { get; set; }
		public int PositiveReviews { get; set; }
		public string PositiveReviewsFilter { get; set; }
		public int NegativeReviews { get; set; }
		public string NegativeReviewsFilter { get; set; }
		public DateTime? LastReview { get; set; }
		public int ToDoTotal { get; set; }
		public string ToDoTotalFilter { get; set; }
		public int ToDoPositive { get; set; }
		public string ToDoPositiveFilter { get; set; }
		public int ToDoNegative { get; set; }
		public string ToDoNegativeFilter { get; set; }

		public DateTime ReviewDate { get; set; } //just a placeholder for filtering
		public string Category { get; set; }//just a placeholder for filtering

		public Dealerships()
		{
			DealerLocation = new Location();
		}

		
	}

	public class SocialDealerships
	{
		public Location DealerLocation { get; set; }
		public string Networks { get; set; }
		public string NetworkUri { get; set; }

		public int Activity { get; set; }
		public int Comments { get; set; }
		public int AdSpendSpent { get; set; }
		public int AdSpendRemainging { get; set; }

		public DateTime? PostDate { get; set; }
		public string PostDateText { get; set; }
		public string TimeZone { get; set; }

		public string SAMName { get; set; }
		public int ToDoesCount { get; set; }

		public string SiteName { get; set; } //just a placeholder for filtering
		public string SiteID { get; set; }
		

		public SocialDealerships()
		{
			DealerLocation = new Location();
			PostDateText = string.Empty;
			TimeZone = string.Empty;
		}
	}

	public class Location
	{
		public long LocationID { get; set; }
		public string Name { get; set; }
		public string Address { get; set; }
		public string City { get; set; }
		public string StateAbrev { get; set; }
		public long? StateID { get; set; }
		public string Zipcode { get; set; }
		public long? OemID { get; set; }

        public string AddressLine1 { get; set; }
        public string AddressLine2 { get; set; }
	}
}
