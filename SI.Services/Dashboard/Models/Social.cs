﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SI.Services.Dashboard.Models
{

	public class FBSocialDashboard
	{
		public List<SocialDashboard> ChartData { get; set; }

		public int FansTotal { get; set; }
		public int NewFansTotal { get; set; }
		public int LostFansTotal { get; set; }
		public int PTATTotal { get; set; }
		public int PageViewTotal { get; set; }
		public int ClickTotal { get; set; }
		public int PageImpressionsTotal { get; set; }
		public int PageImpressionsPaidTotal { get; set; }
		public int PostImpressionsTotal { get; set; }

		public DateTime StartDate { get; set; }
		public DateTime EndDate { get; set; }
		public string FaceBookTimeFrame { get; set; }
	}

	public class SocialDashboard
	{
		public DateTime PeriodEnd { get; set; }
		public int FansNew { get; set; }
		public int FansLost { get; set; }
		public int AvgFans { get; set; }
		public int Clicks { get; set; }
		public int PageImpressions { get; set; }
		public int PageImpressionsPaid { get; set; }
		public int PostImpressions { get; set; }
		public int PageViews { get; set; }
		//public int Stories { get; set; }
		//public int StoryAdds { get; set; }
		public int PTAT { get; set; }
		//public int LastFans { get; set; }

		

	}

	public class TWSocialDashboard
	{
		public List<TwitterChartData> ChartData { get; set; }

		public int Followers { get; set; }
		public int NewFollowers { get; set; }
		public int NewTweets { get; set; }
		public int Following { get; set; }
		public int Lists { get; set; }
		
		public DateTime StartDate { get; set; }
		public DateTime EndDate { get; set; }
		public string TwitterTimeFrame { get; set; }
	}

	public class TwitterChartData
	{
		public bool IsWeekly { get; set; }
		public DateTime PeriodEnd { get; set; }

		public int RetweetsCount { get; set; }
		public int NewFollowers { get; set; }
		public int TweetsCount { get; set; }
		public int NewTweetsCount { get; set; }
		public int FollowingCount { get; set; }
		public int FollowersCount { get; set; }
		public int ListedCount { get; set; }
		public int FriendsCount { get; set; }
	}
}
