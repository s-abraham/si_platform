﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SI.BLL;
using SI.DTO;
using SI.Extensions;
using SI.Services.Dashboard.Models;
using SI.Services.Framework.Models;

namespace SI.Services.Dashboard
{
    public static class DashboardService
    {
        public static List<Dealerships> LocationsGrid(UserInfoDTO userInfo, PagedGridRequest pagedGridRequest, string timeFrame)
        {
            List<Dealerships> dealershipses = new List<Dealerships>();

            DashboardOverviewDTO dto = ProviderFactory.Reviews.GetDashboardOverviewData(userInfo);

            foreach (long accountID in dto.ReputationOverview.ReviewCountsByAccountID.Keys)
            {
                if (accountID > 0)
                {
                    SimpleAccountInfoDTO account = dto.ReputationOverview.AccountList[accountID];
                    ReviewCountsDTO counts = dto.ReputationOverview.ReviewCountsByAccountID[accountID];
                    Dealerships dl1 = new Dealerships();

                    dl1.DealerLocation.LocationID = account.ID;
                    dl1.DealerLocation.Name = account.Name;
                    dl1.DealerLocation.AddressLine1 = account.AddressLine1.IsNullOptional("");
                    dl1.DealerLocation.AddressLine2 = account.AddressLine2.IsNullOptional("");

                    dl1.DealerLocation.OemID = 0;
                    dl1.AverageReviewScore = counts.AverageRating;
                    dl1.AverageReviewScoreFilter = string.Format("locations,eq,{0}", account.ID);
                    dl1.NumberOfReviews = counts.Total;
                    dl1.NumberOfReviewsFilter = string.Format("locations,eq,{0}", account.ID);
                    dl1.PositiveReviews = counts.TotalPositive;
                    dl1.PositiveReviewsFilter = string.Format("locations,eq,{0}-rating,gte,2.5", account.ID);
                    dl1.NegativeReviews = counts.TotalNegative;
                    dl1.NegativeReviewsFilter = string.Format("locations,eq,{0}-rating,lte,2.499", account.ID);
                    dl1.LastReview = counts.LastReviewDate != null ? counts.LastReviewDate.LocalDate : null;
                    dl1.ToDoTotal = 0;// result.ToDoNegative + result.ToDoPositive;
                    dl1.ToDoTotalFilter = string.Format("locations,eq,{0}-status,HasBeenRead,false-status,HasBeenResponded,false", account.ID);
                    dl1.ToDoNegative = 0; //result.ToDoNegative;
                    dl1.ToDoNegativeFilter = string.Format("locations,eq,{0}-status,HasBeenRead,false-status,HasBeenResponded,false-rating,lte,2.499", account.ID);
                    dl1.ToDoPositive = 0; //result.ToDoPositive;
                    dl1.ToDoPositiveFilter = string.Format("locations,eq,{0}-status,HasBeenRead,false-status,HasBeenResponded,false-rating,gte,2.5", account.ID);

                    dealershipses.Add(dl1);
                }
            }
            //foreach (DealershipsDTO result in dealershipsDtos)
            //{
            //    Dealerships dl1 = new Dealerships();


            //    dl1.DealerLocation.OemID = 0;
            //    dl1.AverageReviewScore = result.AverageReviewScore;
            //    dl1.AverageReviewScoreFilter = "locations,eq,1"; //this goes to ReviewSites
            //    dl1.NumberOfReviews = result.NumberOfReviews;
            //    dl1.NumberOfReviewsFilter = "locations,eq,1";
            //    dl1.PositiveReviews = result.PositiveReviews;
            //    dl1.PositiveReviewsFilter = "locations,eq,1-rating,gte,2.5";
            //    dl1.NegativeReviews = result.NegativeReviews;
            //    dl1.NegativeReviewsFilter = "locations,eq,1-rating,lte,2.499";
            //    dl1.LastReview = result.LastReview;
            //    dl1.ToDoTotal = result.ToDoNegative + result.ToDoPositive;
            //    dl1.ToDoTotalFilter = "locations,eq,1-status,HasBeenRead,false-status,HasBeenResponded,false";
            //    dl1.ToDoNegative = result.ToDoNegative;
            //    dl1.ToDoNegativeFilter = "locations,eq,1-status,HasBeenRead,false-status,HasBeenResponded,false-rating,lte,2.499";
            //    dl1.ToDoPositive = result.ToDoPositive;
            //    dl1.ToDoPositiveFilter = "locations,eq,1-status,HasBeenRead,false-status,HasBeenResponded,false-rating,gte,2.5";

            //    dealershipses.Add(dl1);
            //}

            return dealershipses;
        }

        public static List<NotificationSummary> NotificationListView(PagedGridRequest pagedGridRequest)
        {
            //TODO if no items then need to manually create a record for "NO Notification" etc.
            //TODO the numnber sent back is the height for the panel area, recommend max. 5;

            List<NotificationSummary> notificationssummarys = new List<NotificationSummary>();

            List<long> AccountIDs = new List<long>();
            AccountIDs.Add(200049);
            AccountIDs.Add(206585);
            AccountIDs.Add(218308);
            AccountIDs.Add(218727);
            AccountIDs.Add(208072);
            AccountIDs.Add(197930);

            //List<NotificationSummaryDTO> dtos = ProviderFactory.NotificationDB.GetNotificationsSummaryByAccountID(AccountIDs);
            //foreach (NotificationSummaryDTO item in dtos)
            //{
            //    NotificationSummary notificationssummary = new NotificationSummary();
            //    notificationssummary.ID = item.ID;
            //    notificationssummary.ActionImage = item.ActionImage;
            //    notificationssummary.AlertLevel = item.AlertLevel;
            //    notificationssummary.Text = item.Text;

            //    notificationssummarys.Add(notificationssummary);
            //}



            return notificationssummarys;
        }

        public static List<NotificationsToDo> NotificationManageToDoGrid(UserInfoDTO userInfo, PagedGridRequest pagedGridRequest, bool? showCompleted)
        {
            List<NotificationsToDo> notificationsToDos = new List<NotificationsToDo>();

            List<long> AccountIDs = new List<long>();
            AccountIDs.Add(200049);
            AccountIDs.Add(206585);
            AccountIDs.Add(218308);
            AccountIDs.Add(218727);
            AccountIDs.Add(208072);
            AccountIDs.Add(197930);

            //List<NotificationsToDoDTO> dtos = ProviderFactory.NotificationDB.GetNotificationsToDoByAccountID(AccountIDs, showCompleted);

            //foreach (NotificationsToDoDTO item in dtos)
            //{
            //    NotificationsToDo notificationstodo = new NotificationsToDo();

            //    notificationstodo.ID = item.ID;
            //    notificationstodo.ActionImage = item.ActionImage;
            //    notificationstodo.LocationName = item.LocationName;
            //    notificationstodo.Description = item.Description;
            //    notificationstodo.Type = item.Type;
            //    notificationstodo.ToDoDateLocal = item.ToDoDateLocal; //TODO CHANGE TO SIDATETIME
            //    //notificationstodo.CompletedDateLocal = item.CompletedDate //TODO need completed date

            //    notificationsToDos.Add(notificationstodo);
            //}

            return notificationsToDos;
        }

        public static List<NotificationsAlert> NotificationManageAlertGrid(UserInfoDTO userInfo, PagedGridRequest pagedGridRequest)
        {
            List<NotificationsAlert> notificationsAlerts = new List<NotificationsAlert>();

            List<long> AccountIDs = new List<long>();
            AccountIDs.Add(200049);
            AccountIDs.Add(206585);
            AccountIDs.Add(218308);
            AccountIDs.Add(218727);
            AccountIDs.Add(208072);
            AccountIDs.Add(197930);

            //List<NotificationsAlertDTO> dtos = ProviderFactory.NotificationDB.GetNotificationsByAccountID(AccountIDs);
            //foreach (NotificationsAlertDTO item in dtos)
            //{
            //    NotificationsAlert notificationalert = new NotificationsAlert();

            //    notificationalert.ID = item.ID;
            //    notificationalert.ActionImage = item.ActionImage;
            //    notificationalert.LocationName = item.LocationName;
            //    notificationalert.Description = item.Description;
            //    notificationalert.Type = item.Type;
            //    notificationalert.AlertDateLocal = item.AlertDateLocal; //TODO CHANGE TO SIDATETIME

            //    notificationsAlerts.Add(notificationalert);
            //}

            return notificationsAlerts;
        }

        public static Overview Overview(UserInfoDTO userInfo, long AccountID)
        {
            Overview overview = new Overview();

            DashboardOverviewDTO dto = ProviderFactory.Reviews.GetDashboardOverviewData(userInfo);

            overview.reputationOverview.NumberOfDealers = dto.ReputationOverview.NumberOfDealers;
            overview.reputationOverview.ReviewsAllTime.NumberOfReviews = dto.ReputationOverview.ReviewsAllTime.Total;
            overview.reputationOverview.ReviewsAllTime.NumberOfReviewsFilter = string.Format("locations,eq,{0}", userInfo.EffectiveUser.CurrentContextAccountID);
            overview.reputationOverview.ReviewsAllTime.TotalNegative = dto.ReputationOverview.ReviewsAllTime.TotalNegative;
            overview.reputationOverview.ReviewsAllTime.TotalNegativeFilter = string.Format("rating,lte,2.499-locations,eq,{0}", userInfo.EffectiveUser.CurrentContextAccountID);
            overview.reputationOverview.ReviewsAllTime.TotalPositive = dto.ReputationOverview.ReviewsAllTime.TotalPositive;

            overview.reputationOverview.ReviewsAllTime.TotalPositiveFilter = string.Format("rating,gte,2.5-locations,eq,{0}", userInfo.EffectiveUser.CurrentContextAccountID);
            overview.reputationOverview.ReviewsLast30.NumberOfReviews = dto.ReputationOverview.ReviewsLast30.Total;
            overview.reputationOverview.ReviewsLast30.NumberOfReviewsFilter = string.Format("dates,after,{0}-locations,eq,{1}", DateTime.Now.AddDays(-31).ToShortDateString(), userInfo.EffectiveUser.CurrentContextAccountID);
            overview.reputationOverview.ReviewsLast30.TotalPositive = dto.ReputationOverview.ReviewsLast30.TotalPositive;
            overview.reputationOverview.ReviewsLast30.TotalPositiveFilter = string.Format("dates,after,{0}-rating,gte,2.5-locations,eq,{1}", DateTime.Now.AddDays(-31).ToShortDateString(), userInfo.EffectiveUser.CurrentContextAccountID);
            overview.reputationOverview.ReviewsLast30.TotalNegative = dto.ReputationOverview.ReviewsLast30.TotalNegative;
            overview.reputationOverview.ReviewsLast30.TotalNegativeFilter = string.Format("dates,after,{0}-rating,lte,2.499-locations,eq,{1}", DateTime.Now.AddDays(-31).ToShortDateString(), userInfo.EffectiveUser.CurrentContextAccountID);

            overview.reputationOverview.ReviewsLast7.NumberOfReviews = dto.ReputationOverview.ReviewsLast7.Total;
            overview.reputationOverview.ReviewsLast7.NumberOfReviewsFilter = string.Format("dates,after,{0}-locations,eq,{1}", DateTime.Now.AddDays(-8).ToShortDateString(), userInfo.EffectiveUser.CurrentContextAccountID);
            overview.reputationOverview.ReviewsLast7.TotalPositive = dto.ReputationOverview.ReviewsLast7.TotalPositive;
            overview.reputationOverview.ReviewsLast7.TotalPositiveFilter = string.Format("dates,after,{0}-rating,gte,2.5-locations,eq,{1}", DateTime.Now.AddDays(-8).ToShortDateString(), userInfo.EffectiveUser.CurrentContextAccountID);
            overview.reputationOverview.ReviewsLast7.TotalNegative = dto.ReputationOverview.ReviewsLast7.TotalNegative;
            overview.reputationOverview.ReviewsLast7.TotalNegativeFilter = string.Format("dates,after,{0}-rating,lte,2.499-locations,eq,{1}", DateTime.Now.AddDays(-8).ToShortDateString(), userInfo.EffectiveUser.CurrentContextAccountID);

            overview.reputationOverview.UnReadPostive = 25;
            overview.reputationOverview.UnReadPostiveFilter = string.Format("status,HasBeenRead,false-status,HasBeenResponded,false-rating,gte,2.5-locations,eq,{0}", userInfo.EffectiveUser.CurrentContextAccountID);
            overview.reputationOverview.UnReadNegative = 5;
            overview.reputationOverview.UnReadNegativeFilter = string.Format("status,HasBeenRead,false-status,HasBeenResponded,false-rating,lte,2.499-locations,eq,{0}", userInfo.EffectiveUser.CurrentContextAccountID);
            overview.reputationOverview.UnReadFromYesterday = 5;
            overview.reputationOverview.UnReadFromYesterdayFilter = string.Format("status,HasBeenRead,false-status,HasBeenResponded,false-dates,after,{0}-locations,eq,{1}", DateTime.Now.AddDays(-2).ToShortDateString(), userInfo.EffectiveUser.CurrentContextAccountID);
            overview.reputationOverview.UnReadThisMonth = 15;
            overview.reputationOverview.UnReadThisMonthFilter = string.Format("status,HasBeenRead,false-status,HasBeenResponded,false-dates,after,{0}-locations,eq,{1}", DateTime.Now.AddMonths(-2).ToShortDateString(), userInfo.EffectiveUser.CurrentContextAccountID);
            overview.reputationOverview.UnReadFilter = string.Format("status,HasBeenRead,false-status,HasBeenResponded,false-locations,eq,{0}", userInfo.EffectiveUser.CurrentContextAccountID);

            overview.reputationOverview.ReviewsAllTime.AverageRating = 0;
            //double ratingsCount = dto.ReputationOverview.ReviewCountsBySource.Where(r => r.Value.AverageRating > 0).Count();
            //if (ratingsCount > 0)
            //{
            //    double totalRating = dto.ReputationOverview.ReviewCountsBySource.Sum(r => r.Value.AverageRating);
            //    overview.reputationOverview.ReviewsAllTime.AverageRating = totalRating / ratingsCount;
            //}

            double ratingsCount = dto.ReputationOverview.ReviewDetailCount;
            if (ratingsCount > 0)
            {
                double totalRating = dto.ReputationOverview.AllNonZeroRatings;
                overview.reputationOverview.ReviewsAllTime.AverageRating = (double)Math.Round(totalRating / ratingsCount);
            }

            double totalAllTimePN = dto.ReputationOverview.ReviewsAllTime.TotalNegative + dto.ReputationOverview.ReviewsAllTime.TotalPositive;
            if (totalAllTimePN > 0)
            {
                overview.reputationOverview.totalAllTimePercentNeg = (int)Math.Round((dto.ReputationOverview.ReviewsAllTime.TotalNegative / totalAllTimePN) * 100);
                overview.reputationOverview.totalAllTimePercentPos = (int)Math.Round((dto.ReputationOverview.ReviewsAllTime.TotalPositive / totalAllTimePN) * 100);
            }

            double totalLast30PN = dto.ReputationOverview.ReviewsLast30.TotalNegative + dto.ReputationOverview.ReviewsLast30.TotalPositive;
            if (totalLast30PN > 0)
            {
                overview.reputationOverview.totalLast30PercentNeg = (int)Math.Round((dto.ReputationOverview.ReviewsLast30.TotalNegative / totalLast30PN) * 100);
                overview.reputationOverview.totalLast30PercentPos = (int)Math.Round((dto.ReputationOverview.ReviewsLast30.TotalPositive / totalLast30PN) * 100);
            }

            double totalLast7PN = dto.ReputationOverview.ReviewsLast7.TotalNegative + dto.ReputationOverview.ReviewsLast7.TotalPositive;
            if (totalLast7PN > 0)
            {
                overview.reputationOverview.totalLast7PercentNeg = (int)Math.Round((dto.ReputationOverview.ReviewsLast7.TotalNegative / totalLast7PN) * 100);
                overview.reputationOverview.totalLast7PercentPos = (int)Math.Round((dto.ReputationOverview.ReviewsLast7.TotalPositive / totalLast7PN) * 100);
            }

            //List<ReviewSourceDTO> reviewSourceDtos = ProviderFactory.Reviews.GetReviewSources(userInfo);
            List<DTO.ReviewSourceDTO> reviewSourceDtos = ProviderFactory.Reviews.GetReviewSources(AccountID);

            foreach (var siteSource in dto.ReputationOverview.ReviewCountsBySource)
            {
                var reviewSourceDto = reviewSourceDtos.Where(r => r.ID == (long)siteSource.Key).FirstOrDefault();

                if (reviewSourceDto != null)
                {
                    SiteReviewCounts siteReview = new SiteReviewCounts();
                    siteReview.SiteName = reviewSourceDtos.Where(r => r.ID == (long)siteSource.Key).Select(r => r.Name).FirstOrDefault();
                    siteReview.Total = siteSource.Value.Total;
                    siteReview.category = siteReview.SiteName;
                    siteReview.value = siteReview.Total;

                    siteReview.AverageRating = siteSource.Value.AverageRating;
                    siteReview.TotalNegative = siteSource.Value.TotalNegative;
                    siteReview.TotalPositive = siteSource.Value.TotalPositive;

                    siteReview.PercentNegative = 0;
                    siteReview.PercentPositive = 0;
                    siteReview.SiteFilter = string.Format("sites,eq,{0}-locations,eq,{1}", (long)siteSource.Key, userInfo.EffectiveUser.CurrentContextAccountID);

                    double total = siteReview.TotalNegative + siteReview.TotalPositive;
                    if (total > 0)
                    {
                        siteReview.PercentNegative = (int)(((siteReview.TotalNegative / total) * 100) + 0.5);
                        siteReview.PercentPositive = 100 - siteReview.PercentNegative;

                        //siteReview.PercentPositive = (int)((siteReview.TotalPositive/total)*100);

                        if (siteReview.PercentNegative + siteReview.PercentPositive < 100 && siteReview.PercentPositive < 100)
                            siteReview.PercentPositive++;
                    }

                    overview.reputationOverview.ReviewsBySite.Add(siteReview);
                }

            }

            overview.reputationOverview.ReviewsBySite = overview.reputationOverview.ReviewsBySite.OrderBy(c => c.category).ToList();

            overview.reputationOverview.CustomerSatisfaction = 0;
            //var reviews = dto.ReputationOverview.ReviewCountsBySource.Where(r => r.Value.AverageRating > 0).ToList();
            //if (reviews.Any())
            //{
            //    double AllNonZeroRatings = reviews.Sum(r => r.Value.AverageRating);
            //    overview.reputationOverview.CustomerSatisfaction = (int)Math.Round((AllNonZeroRatings / reviews.Count) * 20.0);
            //}

            overview.reputationOverview.ReviewDetailCount = dto.ReputationOverview.ReviewDetailCount;
            overview.reputationOverview.AllNonZeroRatings = dto.ReputationOverview.AllNonZeroRatings;
            if (overview.reputationOverview.ReviewDetailCount > 0)
            {
                double AllNonZeroRatings = overview.reputationOverview.AllNonZeroRatings;
                overview.reputationOverview.CustomerSatisfaction = (int)Math.Round((AllNonZeroRatings / overview.reputationOverview.ReviewDetailCount) * 20.0);
            }

            overview.socialOverview.NumberOfDealers = dto.ReputationOverview.NumberOfDealers;

            overview.socialOverview.Facebook.PostTweetCount = dto.SocialOverview.FBPosts;
            overview.socialOverview.Facebook.Comments = dto.SocialOverview.FBPostComments;
            overview.socialOverview.Facebook.Likes = dto.SocialOverview.FBPostLikes;
            overview.socialOverview.Facebook.SharesReTweets = dto.SocialOverview.FBPostShares;

            overview.socialOverview.GooglePlus.PostTweetCount = dto.SocialOverview.GooglePlusPosts;
            overview.socialOverview.GooglePlus.Comments = dto.SocialOverview.GooglePlusPostComments;
            overview.socialOverview.GooglePlus.Likes = dto.SocialOverview.GooglePlusPostPlusoners;
            overview.socialOverview.GooglePlus.SharesReTweets = dto.SocialOverview.GooglePlusPostResharers;

            overview.socialOverview.Twitter.PostTweetCount = dto.SocialOverview.TwitterPosts;
            overview.socialOverview.Twitter.SharesReTweets = dto.SocialOverview.TwitterRetweetCount;

            overview.socialOverview.FacebookPageLikes_AllTime = dto.SocialOverview.FacebookPageLikes_AllTime;
            overview.socialOverview.GooglePlusPlusOneCount_AllTime = dto.SocialOverview.GooglePlusPlusOneCount_AllTime;
            overview.socialOverview.TwitterFollowersCount_AllTime = dto.SocialOverview.TwitterFollowersCount_AllTime;

            overview.socialOverview.FacebookPageLikes_30days = dto.SocialOverview.FacebookPageLikes_30days;
            overview.socialOverview.GooglePlusPlusOneCount_30days = dto.SocialOverview.GooglePlusPlusOneCount_30days;
            overview.socialOverview.TwitterFollowersCount_30days = dto.SocialOverview.TwitterFollowersCount_30days;

            overview.socialOverview.FacebookPageLikes_7days = dto.SocialOverview.FacebookPageLikes_7days;
            overview.socialOverview.GooglePlusPlusOneCount_7days = dto.SocialOverview.GooglePlusPlusOneCount_7days;
            overview.socialOverview.TwitterFollowersCount_7days = dto.SocialOverview.TwitterFollowersCount_7days;


            List<string> lMonths = new List<string>();
            List<int> lLikes = new List<int>();

            foreach (SocialMonthlyLikesDTO like in dto.SocialOverview.FBLikes3Months)
            {
                lMonths.Add(like.MonthDate.ToString("MMMM"));
                lLikes.Add(like.Likes);
            }
            overview.socialOverview.Months = lMonths.ToArray();
            overview.socialOverview.FBLikes = lLikes.ToArray();

            overview.socialOverview.TWFollowers = new int[] { 0 };
            overview.socialOverview.GoogleFollowers = new int[] { 0 };
            overview.socialOverview.YTSubscribers = new int[] { 0 };

            //TODO shortcut for totals since FB is the only one.
            overview.socialOverview.Totals = overview.socialOverview.FBLikes;




            return overview;
        }

        public static List<SocialDealerships> SocialLocationsGrid(UserInfoDTO userInfo, PagedGridRequest pagedGridRequest, string timeFrame, string network)
        {
            List<SocialDealerships> dealerships = new List<SocialDealerships>();

            bool ShowConnected = (network == "connected");
            bool ShowDisconnected = (network == "disconnected");
            bool ShowNeverConnected = (network == "never");

            List<DashboardSocialLocationDTO> listDashboardSocialLocationDTO = ProviderFactory.Social.GetDashBoardSocialLocationData(userInfo, ShowConnected, ShowDisconnected, ShowNeverConnected);

            foreach (var item in listDashboardSocialLocationDTO)
            {
                SocialDealerships dealer = new SocialDealerships();

                dealer.DealerLocation.LocationID = item.AccountID;
                dealer.DealerLocation.Name = item.AccountName;
                dealer.DealerLocation.AddressLine1 = item.StreetName.IsNullOptional("");
                dealer.DealerLocation.AddressLine2 = item.CityName.IsNullOptional("") + ", " + item.StateAbbrev.IsNullOptional("") + " " + item.ZipCode.IsNullOptional("");
                dealer.Activity = item.FacebookPostComments + item.FacebookPostLikes + item.FacebookShares +
                                    item.GooglePlusComments + item.GooglePlusPlusoners + item.GooglePlusResharers +
                                    item.TwitterPostRetweetCount;
                dealer.Comments = item.FacebookPostComments + item.GooglePlusComments + item.TwitterPostRetweetCount;

                if (item.LastPostDateTime != null)
                {
                    dealer.PostDate = item.LastPostDateTime.LocalDate.GetValueOrDefault();
                    dealer.PostDateText = dealer.PostDate.GetValueOrDefault().ToString("MMMM dd h:mm tt");
                    dealer.TimeZone = item.LastPostDateTime.TimeZone.StandardName;
                }

                List<string> networks = new List<string>();
                List<string> netUri = new List<string>();
                DashboardSocialLocationSocialNetWorkDTO facebookDto = item.DashboardSocialLocationSocialNetWork.FirstOrDefault(n => n.SocialNetwork == SocialNetworkEnum.Facebook && n.IsActive);
                if (facebookDto != null)
                {
                    if (facebookDto.IsValid)
                    {
                        networks.Add("facebook");
                        netUri.Add(facebookDto.URI);
                    }
                    else
                    {
                        networks.Add("facebooknv");
                        netUri.Add("javascript:ShowConfigWindow(" + item.AccountID + ")");
                    }
                }
                else
                {
                    networks.Add("facebooknc");
                    netUri.Add("javascript:ShowConfigWindow(" + item.AccountID + ")");
                }

                DashboardSocialLocationSocialNetWorkDTO twitterDto = item.DashboardSocialLocationSocialNetWork.FirstOrDefault(n => n.SocialNetwork == SocialNetworkEnum.Twitter && n.IsActive);
                if (twitterDto != null)
                {
                    if (twitterDto.IsValid)
                    {
                        networks.Add("twitter");
                        netUri.Add(twitterDto.URI);
                    }
                    else
                    {
                        networks.Add("twitternv");
                        netUri.Add("javascript:ShowConfigWindow(" + item.AccountID + ")");
                    }
                }
                else
                {
                    networks.Add("twitternc");
                    netUri.Add("javascript:ShowConfigWindow(" + item.AccountID + ")");
                }

                DashboardSocialLocationSocialNetWorkDTO googleDto = item.DashboardSocialLocationSocialNetWork.FirstOrDefault(n => n.SocialNetwork == SocialNetworkEnum.Google && n.IsActive);
                if (googleDto != null)
                {
                    if (googleDto.IsValid)
                    {
                        networks.Add("google");
                        netUri.Add(googleDto.URI);
                    }
                    else
                    {
                        networks.Add("googlenv");
                        netUri.Add("javascript:ShowConfigWindow(" + item.AccountID + ")");
                    }

                }
                else
                {
                    networks.Add("googlenc");
                    netUri.Add("javascript:ShowConfigWindow(" + item.AccountID + ")");
                }

                dealer.Networks = string.Join(",", networks);
                dealer.NetworkUri = string.Join("|", netUri);

                dealerships.Add(dealer);

            }

            return dealerships;

        }

        public static List<ToDoSummary> ToDoListView(UserInfoDTO userInfo, PagedGridRequest pagedGridRequest, string assetImagePath)
        {
            //TODO if no items then need to manually create a record for "NO TODO" etc.

            //TODO the numnber sent back is the height for the panel area, recommend max. 5;

            List<ToDoSummary> toDos = new List<ToDoSummary>();

            if (userInfo.EffectiveUser.Username == "muser")
            {
                ToDoSummary toDo = new ToDoSummary
                    {
                        ActionImage = assetImagePath + "facebook.png",
                        ID = 1,
                        Text = "Penton Honda received a negative review. You need to respond to it.",
                        AlertLevel = "exclamation"
                    };
                toDos.Add(toDo);

                ToDoSummary toDo2 = new ToDoSummary
                    {
                        ActionImage = assetImagePath + "approve_post.png",
                        ID = 1,
                        Text = "You need to approve a post from Penton Mitsubishi.",
                        AlertLevel = "alert"
                    };
                toDos.Add(toDo2);

                ToDoSummary toDo3 = new ToDoSummary
                    {
                        ActionImage = assetImagePath + "comment.png",
                        ID = 1,
                        Text = "You have a new direct Message.",
                        AlertLevel = "information"
                    };
                toDos.Add(toDo3);

                ToDoSummary toDo4 = new ToDoSummary
                    {
                        ActionImage = assetImagePath + "google_plus.png",
                        ID = 1,
                        Text = "Penton Honda received a positive review.",
                        AlertLevel = "accept"
                    };
                toDos.Add(toDo4);

                ToDoSummary toDo5 = new ToDoSummary
                    {
                        ActionImage = assetImagePath + "link_break.png",
                        ID = 1,
                        Text = "Penton Honda Twitter connection is broken.",
                        AlertLevel = "exclamation"
                    };
                toDos.Add(toDo5);
            }

            return toDos;
        }

        public static List<MyPrefNotifications> MyPrefGridList(UserInfoDTO userInfo, PagedGridRequest pagedGridRequest)
        {
            List<MyPrefNotifications> myPref = new List<MyPrefNotifications>();

            if (userInfo.EffectiveUser.Username == "muser")
            {
                MyPrefNotifications notif = new MyPrefNotifications();
                notif.ID = 1;
                notif.Name = "New Review Alert";
                notif.Description = "this is the description for New Review Alert";
                notif.IsAlert = true;
                notif.SMSNotification = true;
                notif.EmailNotification = true;
                notif.PlatformNotification = true;

                notif.HasEmailNotifications = true;
                notif.HasSMSNotifications = true;
                notif.HasPlatformNotifications = false;
                notif.AccountCount = 2;

                myPref.Add(notif);

                MyPrefNotifications alert2 = new MyPrefNotifications();
                alert2.ID = 2;
                alert2.Name = "Negative Review Report";
                alert2.Description = "This is the description of Negative Review Report";
                alert2.IsReport = true;
                alert2.SMSNotification = false;
                alert2.EmailNotification = true;
                alert2.PlatformNotification = true;

                alert2.HasEmailNotifications = true;
                alert2.HasPlatformNotifications = true;
                alert2.HasSMSNotifications = false;
                alert2.AccountCount = 1;

                myPref.Add(alert2);

                MyPrefNotifications alert3 = new MyPrefNotifications();
                alert3.ID = 3;
                alert3.Name = "Daily Reputation Report";
                alert3.Description = "This is the description of Daily Reputation Report";
                alert3.IsReport = true;
                alert3.SMSNotification = false;
                alert3.EmailNotification = true;
                alert3.PlatformNotification = false;

                alert3.HasEmailNotifications = true;
                alert3.HasPlatformNotifications = true;
                alert3.HasSMSNotifications = false;
                alert3.AccountCount = 1;

                myPref.Add(alert3);

                MyPrefNotifications alert4 = new MyPrefNotifications();
                alert4.ID = 4;
                alert4.Name = "Monthly Reputation Report";
                alert4.Description = "This is the description of Monthly Reputation Report";
                alert4.IsReport = true;
                alert4.SMSNotification = false;
                alert4.EmailNotification = false;
                alert4.PlatformNotification = true;

                alert4.HasEmailNotifications = true;
                alert4.HasPlatformNotifications = true;
                alert4.HasSMSNotifications = false;
                alert4.AccountCount = 1;

                myPref.Add(alert4);
            }

            return myPref;
        }

        public static NotificationConfigItem NotificationConfig(int configID)
        {
            NotificationConfigItem notificationConfigItem = new NotificationConfigItem();
            notificationConfigItem.ID = configID;

            if (configID == 1)
            {
                notificationConfigItem.AccountIDs.Add(197925);
                notificationConfigItem.AccountIDs.Add(197926);
                notificationConfigItem.HasHourIntervalConfig = true;
                notificationConfigItem.HourIntervalConfig = 2;
            }
            if (configID == 2)
            {
                notificationConfigItem.HasDayConfig = true;
                notificationConfigItem.DayConfig = "Friday";

            }
            if (configID == 3)
            {
                notificationConfigItem.HasHourConfig = true;
                notificationConfigItem.HourConfig = "08:00 AM";
            }
            if (configID == 4)
            {
                notificationConfigItem.HasDayOfMonthConfig = true;
                notificationConfigItem.DayOfMonthConfig = 15;
            }

            return notificationConfigItem;
        }

        public static bool CompleteToDo(int notificationID)
        {
            return true;
        }

        public static bool AddAlertToDos(int notificationID)
        {
            return true;
        }

        public static bool ClearAlert(int notificationID)
        {
            return true;
        }

        public static bool ClearAllAlerts(UserInfoDTO userInfo)
        {
            return true;
        }

        public static bool UserPreferencesPassword(UserInfoDTO userInfo, MyPrefPassword myPrefPassword, ref string problem)
        {
            ValidateUserOutcome outcome = ProviderFactory.Security.ChangeUserPassword(userInfo, myPrefPassword.Password, myPrefPassword.CurrentPassword);
            problem = outcome.ToString();

            return outcome == ValidateUserOutcome.Success;
        }

        public static List<NotificationSummary> GetGlobeNotifications(UserInfoDTO userInfo)
        {
            List<NotificationSummary> notifications = new List<NotificationSummary>();

            List<NotificationSummaryDTO> notificationSummaryDTO = ProviderFactory.Notifications.GetGlobeNotificationList(userInfo);
            foreach (NotificationSummaryDTO dto in notificationSummaryDTO)
            {
                NotificationSummary notification = new NotificationSummary();
                notification.Text = dto.Text;
                notification.AlertLevel = dto.AlertLevel.ToString();
                notification.Count = dto.Count;

                notification.ActionImage = "notif_group.png";
                if ((NotificationMessageTypeEnum)dto.ID == NotificationMessageTypeEnum.SyndicationPendingPostApproval)
                    notification.ActionImage = "approve_post.png";

                notifications.Add(notification);
            }

            return notifications;
        }

        public static List<NotificationsAlert> GetNotificationList(UserInfoDTO userInfo)
        {
            List<NotificationsAlert> notifications = new List<NotificationsAlert>();

            List<NotificationsAlertDTO> notificationsAlertDtos = ProviderFactory.Notifications.GetNotificationList(userInfo);
            foreach (NotificationsAlertDTO dto in notificationsAlertDtos)
            {
                NotificationsAlert notification = new NotificationsAlert();
                notification.ID = dto.ID;
                notification.Status = dto.AlertType.ToString();

                switch (dto.AlertType)
                {
                    case NotificationMessageTypeEnum.NewReviewAlert:
                        notification.Status = "New Review Alert";
                        break;
                    case NotificationMessageTypeEnum.SyndicationPendingPostApproval:
                        notification.Status = "Pending Post Approval";
                        break;
                    case NotificationMessageTypeEnum.NewNegativeReviewAlert:
                        notification.Status = "New Negative Review Alert";
                        break;
                    case NotificationMessageTypeEnum.NegReviewNoResponse6h:
                        notification.Status = "Negative Review No Response 6h";
                        break;
                    case NotificationMessageTypeEnum.NegReviewNoResponse12h:
                        notification.Status = "Negative Review No Response 12h";
                        break;
                    case NotificationMessageTypeEnum.NegReviewNoResponse24h:
                        notification.Status = "Negative Review No Response 24h";
                        break;
                }

                notification.LocationName = dto.LocationName;
                notification.Description = dto.Description;

                notifications.Add(notification);
            }

            return notifications;
        }

        public static List<NotificationsApprovals> GetPendingApprovals(UserInfoDTO userInfo)
        {
            List<NotificationsApprovals> approvals = new List<NotificationsApprovals>();

            List<PendingApprovalInfoDTO> dto = ProviderFactory.Social.GetPendingApprovals(userInfo);

            foreach (PendingApprovalInfoDTO approval in dto)
            {
                NotificationsApprovals napp = new NotificationsApprovals();
                napp.ID = approval.PostApprovalID;
                napp.Description = approval.Message;

                if (approval.ScheduleDate != null)
                {
                    napp.AlertDateLocal = approval.ScheduleDate.LocalDate.Value;
                    napp.AlertDateLocalText = napp.AlertDateLocal.ToString("M/d/yyyy h:mm tt");
                    napp.TimeZone = approval.ScheduleDate.TimeZone.StandardName;
                }

                else if (approval.PostCreationDate != null)
                {

                    napp.AlertDateLocal = approval.PostCreationDate.LocalDate.Value;
                    napp.AlertDateLocalText = napp.AlertDateLocal.ToString("M/d/yyyy h:mm tt"); ;
                    napp.TimeZone = approval.PostCreationDate.TimeZone.StandardName;
                }

                napp.ImageUrl = "";
                if (!string.IsNullOrEmpty(approval.PostImageURL))
                    napp.ImageUrl = string.Format("http://platform.socialintegration.com/ImageSizer?image=/UploadedPostImages/{0}&w=370", approval.PostImageURL);
                //string.Format("{0}{1}", Settings.GetSetting("site.PublishImageUploadURL"), approval.PostImageURL);

                napp.Link = approval.Link;
                napp.LocationName = approval.TargetAccountName;
                napp.ScreenName = approval.SocialNetworkScreenName;
                napp.SyndicatorName = approval.SyndicatorName;
                napp.Status = "Pending Approval"; //TODO Always the same status ?????
                napp.PostID = approval.PostID;
                napp.TargetAccountID = approval.TargetAccountID;

                napp.PostTargetID = approval.PostTargetID;
                napp.LinkTitle = approval.LinkTitle;
                napp.LinkDescription = approval.LinkDescription;
                napp.LinkImageUrl = approval.LinkImageURL;

                napp.NetworkName = approval.SocialNetwork.ToString().ToLower();
                napp.SocialNetworkURL = approval.SocialNetworkURL.IsNullOptional("");

                napp.AllowChanges = approval.AllowChanges;
                napp.AllowMessageChange = approval.AllowChangeMessage.GetValueOrDefault();
                napp.AllowScheduleDateChange = approval.AllowChangePublishDate.GetValueOrDefault();


                approvals.Add(napp);
            }

            return approvals.OrderByDescending(a => a.AlertDateLocal).ToList();
        }

        public static bool PostApprovalAction(UserInfoDTO userInfo, long postApprovalID, string action)
        {
            ApprovalResponseResultDTO dto = ProviderFactory.Social.SubmitApprovalResponse(userInfo, postApprovalID, action == "approve" ? ApprovalResponseEnum.Approve : ApprovalResponseEnum.Reject);
            return !dto.Problems.Any();
        }

        public static MyPrefAlertEdit GetUserPerferences(UserInfoDTO userInfo)
        {
            MyPrefAlertEdit pref = new MyPrefAlertEdit();

            UserPreferencesDTO dto = ProviderFactory.Security.GetUserPreferences(userInfo);
            pref.DisableAll = !dto.NotificationsEnabled;

            return pref;
        }

        public static void SaveUserPerferences(UserInfoDTO userInfo, bool disabledAll)
        {
            UserPreferencesDTO dto = new UserPreferencesDTO();
            dto.NotificationsEnabled = !disabledAll;

            ProviderFactory.Security.SaveUserPreferences(userInfo, dto);
        }

        public static bool PostApprovalActionAll(UserInfoDTO userInfo, string postApprovalIds, string action)
        {
            ApprovalResponseEnum actionEnum = ApprovalResponseEnum.Approve;
            if (action != "approve")
                actionEnum = ApprovalResponseEnum.AutoApprove;

            string[] IDs = postApprovalIds.Split(',');
            foreach (string id in IDs)
            {
                ProviderFactory.Social.SubmitApprovalResponse(userInfo, id.ToLong(), actionEnum);
            }

            return true;
        }

        public static FBSocialDashboard SocialDashboard(UserInfoDTO userInfo, string timeFrame)
        {
            FBSocialDashboard fbSocialDashboard = new FBSocialDashboard();

            DateTime userNow = userInfo.EffectiveUser.CurrentDateTime.LocalDate.GetValueOrDefault();
            DateTime startDate = userNow;
            DateTime endDate = userNow;
            SocialDashboardTimeFrame(ref startDate, ref endDate, userNow, timeFrame);


            fbSocialDashboard.ChartData = new List<SocialDashboard>();

            List<SocialDashboardDataFacebook> facebooks = ProviderFactory.Social.GetSocialDashboardData_facebook(userInfo, startDate, endDate);
            foreach (SocialDashboardDataFacebook dto in facebooks)
            {
                SocialDashboard socialDashboard = new SocialDashboard();

                socialDashboard.PeriodEnd = dto.PeriodEnd;
                socialDashboard.Clicks = dto.Clicks.GetValueOrDefault();
                socialDashboard.FansNew = dto.FansNew.GetValueOrDefault();
                socialDashboard.FansLost = dto.FansLost.GetValueOrDefault();
                socialDashboard.AvgFans = dto.AvgFans.GetValueOrDefault();
                socialDashboard.PTAT = dto.PTAT.GetValueOrDefault();
                socialDashboard.PageImpressions = dto.PageImpressions.GetValueOrDefault();
                socialDashboard.PageImpressionsPaid = dto.PageImpressionsPaid.GetValueOrDefault();
                socialDashboard.PageViews = dto.PageViews.GetValueOrDefault();
                socialDashboard.PostImpressions = dto.PostImpressions.GetValueOrDefault();

                fbSocialDashboard.ChartData.Add(socialDashboard);
            }

            fbSocialDashboard.StartDate = startDate;
            fbSocialDashboard.EndDate = endDate;
            fbSocialDashboard.FaceBookTimeFrame = timeFrame;

            if (facebooks.Any())
            {
                fbSocialDashboard.FansTotal = facebooks.OrderByDescending(f => f.PeriodEnd).Where(f => f.AvgFans > 0).Select(f => f.AvgFans).FirstOrDefault().GetValueOrDefault();
                fbSocialDashboard.NewFansTotal = facebooks.Sum(f => f.FansNew.GetValueOrDefault());
                fbSocialDashboard.LostFansTotal = facebooks.Sum(f => f.FansLost.GetValueOrDefault());
                fbSocialDashboard.PTATTotal = facebooks.Sum(f => f.PTAT.GetValueOrDefault());
                fbSocialDashboard.PageViewTotal = facebooks.Sum(f => f.PageViews.GetValueOrDefault());
                fbSocialDashboard.ClickTotal = facebooks.Sum(f => f.Clicks.GetValueOrDefault());
                fbSocialDashboard.PageImpressionsTotal = facebooks.Sum(f => f.PageImpressions.GetValueOrDefault());
                fbSocialDashboard.PageImpressionsPaidTotal = facebooks.Sum(f => f.PageImpressionsPaid.GetValueOrDefault());
                fbSocialDashboard.PostImpressionsTotal = facebooks.Sum(f => f.PostImpressions.GetValueOrDefault());
            }

            return fbSocialDashboard;
        }

        private static void SocialDashboardTimeFrame(ref DateTime startDate, ref DateTime endDate, DateTime userNow, string timeFrame)
        {
            switch (timeFrame)
            {
                case "Last 7 Days":
                    endDate = userNow.AddDays(-1);
                    startDate = userNow.AddDays(-7);
                    break;

                case "Last 30 Days":
                    endDate = userNow.AddDays(-1);
                    startDate = userNow.AddDays(-30);
                    break;

                case "Current Month":
                    endDate = userNow.AddDays(-1);
                    startDate = new DateTime(userNow.Year, userNow.Month, 1);
                    break;

                case "Previous Month":
                    DateTime previousMonth = userNow.AddMonths(-1);
                    startDate = new DateTime(previousMonth.Year, previousMonth.Month, 1);
                    endDate = new DateTime(userNow.Year, userNow.Month, 1).AddDays(-1);
                    break;

                case "Current Quarter":
                    int currQuarter = (userNow.Month - 1) / 3 + 1;
                    startDate = new DateTime(userNow.Year, 3 * currQuarter - 2, 1);
                    endDate = new DateTime(userNow.Year, 3 * currQuarter + 1, 1).AddDays(-1);
                    break;

                case "Previous Quarter":
                    DateTime lastDayOfLastQuarter = userNow.AddMonths(-((userNow.Month - 1) % 3)).AddDays(-userNow.Day);
                    int prevQuarter = (lastDayOfLastQuarter.Month - 1) / 3 + 1;
                    startDate = new DateTime(lastDayOfLastQuarter.Year, 3 * prevQuarter - 2, 1);
                    endDate = new DateTime(lastDayOfLastQuarter.Year, 3 * prevQuarter + 1, 1).AddDays(-1);
                    break;

                case "Current Year":
                    endDate = userNow.AddDays(-1);
                    startDate = new DateTime(userNow.Year, 1, 1);
                    break;

                case "Previous Year":
                    startDate = new DateTime(userNow.Year - 1, 1, 1);
                    endDate = new DateTime(userNow.Year - 1, 12, 31);
                    break;
            }
        }

        public static TWSocialDashboard SocialDashboardTwitter(UserInfoDTO userInfo, string timeFrame)
        {
            TWSocialDashboard twSocialDashboard = new TWSocialDashboard();

            DateTime userNow = userInfo.EffectiveUser.CurrentDateTime.LocalDate.GetValueOrDefault();
            DateTime startDate = userNow;
            DateTime endDate = userNow;
            SocialDashboardTimeFrame(ref startDate, ref endDate, userNow, timeFrame);

            twSocialDashboard.ChartData = new List<TwitterChartData>();

            List<SocialDashboardDataTwitter> twitter = ProviderFactory.Social.GetSocialDashboardData_Twitter(userInfo, startDate, endDate);
            foreach (SocialDashboardDataTwitter dto in twitter)
            {
                TwitterChartData chart = new TwitterChartData();

                chart.PeriodEnd = dto.PeriodEnd;
                chart.FollowersCount = dto.FollowersCount.GetValueOrDefault();
                chart.FollowingCount = dto.FollowingCount.GetValueOrDefault();
                chart.FriendsCount = dto.FriendsCount.GetValueOrDefault();
                chart.IsWeekly = dto.IsWeekly;
                chart.ListedCount = dto.ListedCount.GetValueOrDefault();
                chart.NewFollowers = dto.NewFollowers.GetValueOrDefault();
                chart.RetweetsCount = dto.RetweetsCount.GetValueOrDefault();
                chart.TweetsCount = dto.TweetsCount.GetValueOrDefault();
                chart.NewTweetsCount = dto.NewTweets.GetValueOrDefault();

                twSocialDashboard.ChartData.Add(chart);
            }

            if (twitter.Any())
            {
                SocialDashboardDataTwitter last = twitter.OrderByDescending(t => t.PeriodEnd).FirstOrDefault();
                twSocialDashboard.Followers = last.FollowersCount.GetValueOrDefault();
                twSocialDashboard.Following = last.FollowingCount.GetValueOrDefault();
                twSocialDashboard.Lists = last.ListedCount.GetValueOrDefault();

                twSocialDashboard.NewFollowers = twitter.Sum(t => t.NewFollowers.GetValueOrDefault());
                twSocialDashboard.NewTweets = twitter.Sum(t => t.NewTweets.GetValueOrDefault());
            }

            twSocialDashboard.StartDate = startDate;
            twSocialDashboard.EndDate = endDate;
            twSocialDashboard.TwitterTimeFrame = timeFrame;


            return twSocialDashboard;
        }

        public static List<NotificationsApprovalHistory> GetPendingApprovalHistory(UserInfoDTO userInfo, PagedGridRequest pagedGridRequest, string timeFrame, string statusFilter, string networkFilter, ref int totalCount)
        {
            List<NotificationsApprovalHistory> history = new List<NotificationsApprovalHistory>();

            PostApprovalHistoryRequestDTO req = new PostApprovalHistoryRequestDTO();
            req.EndDate = DateTime.UtcNow;
            req.StartDate = req.EndDate.AddDays(timeFrame.ToInteger() * -1);
            req.SocialNetworkID = networkFilter.ToLong();
            //TODO: convert filter text to approval status enum
            //req.Status = statusFilter; 
            req.VirtualGroupID = 0;
            req.SortBy = 3;
            req.SortAscending = false;

            if (pagedGridRequest.Sorting.Any())
            {
                PagedGridSortDescriptor sort = pagedGridRequest.Sorting.First();
                switch (sort.Field)
                {
                    case "Status": req.SortBy = 1; break;
                    case "AlertDateLocal": req.SortBy = 2; break;
                    case "ActedOnDateLocal": req.SortBy = 3; break;
                    case "NetworkName": req.SortBy = 4; break;
                    case "LocationName": req.SortBy = 5; break;
                    case "SyndicatorName": req.SortBy = 6; break;
                    case "Description": req.SortBy = 7; break;
                }

                req.SortAscending = sort.Ascending;
            }

            req.StartIndex = pagedGridRequest.Skip;
            req.EndIndex = req.StartIndex + pagedGridRequest.Take;

            PostApprovalResultsDTO dto = ProviderFactory.Social.GetPostApprovals(userInfo, req);
            totalCount = dto.TotalRecords;

            foreach (PostApprovalResult approval in dto.Items)
            {
                NotificationsApprovalHistory napp = new NotificationsApprovalHistory();
                napp.ID = approval.PostApprovalID;
                napp.Description = approval.Message;

                if (approval.ScheduleDate != null)
                {
                    napp.AlertDateLocal = approval.ScheduleDate.LocalDate.Value;
                    napp.AlertDateLocalText = napp.AlertDateLocal.ToString("M/d/yyyy h:mm tt");
                    napp.TimeZone = approval.ScheduleDate.TimeZone.StandardName;
                }

                else if (approval.PostCreationDate != null)
                {

                    napp.AlertDateLocal = approval.PostCreationDate.LocalDate.Value;
                    napp.AlertDateLocalText = napp.AlertDateLocal.ToString("M/d/yyyy h:mm tt"); ;
                    napp.TimeZone = approval.PostCreationDate.TimeZone.StandardName;
                }

                napp.ImageUrl = "";
                //if (!string.IsNullOrEmpty(approval.PostImageURL))
                //    napp.ImageUrl = string.Format("http://platform.socialintegration.com/ImageSizer?image=/UploadedPostImages/{0}&w=370", approval.PostImageURL);
                ////string.Format("{0}{1}", Settings.GetSetting("site.PublishImageUploadURL"), approval.PostImageURL);

                napp.Link = approval.Link;
                napp.LocationName = approval.AccountName;
                napp.ScreenName = approval.ScreenName;
                napp.SyndicatorName = approval.SyndicatorName;
                napp.Status = approval.Status;
                napp.PostID = approval.PostID;
                napp.TargetAccountID = approval.AccountID;

                napp.PostTargetID = approval.PostTargetID;
                napp.LinkTitle = approval.LinkTitle;
                napp.LinkDescription = approval.LinkDescription;
                napp.LinkImageUrl = approval.LinkImageURL;

                napp.NetworkName = approval.SocialNetwork.ToString().ToLower();
                napp.SocialNetworkURL = approval.SocialNetworkURL.IsNullOptional("");

                napp.AllowApprove = approval.AllowApprove;
                napp.AllowDeny = approval.AllowDeny;
                napp.AllowEdit = approval.AllowEdit;
                napp.AllowUnApprove = approval.AllowUnApprove;
                napp.AllowUnDeny = approval.AllowUnDeny;

                napp.ActedOnDateLocalText = "";

                if (approval.DateApproved != null)
                    napp.ActedOnDateLocal = approval.DateApproved.LocalDate.Value;
                else if (approval.DateRejected != null)
                    napp.ActedOnDateLocal = approval.DateRejected.LocalDate.Value;
                else if (approval.PublishedDate != null)
                    napp.ActedOnDateLocal = approval.PublishedDate.LocalDate.Value;

                if (napp.ActedOnDateLocal != null)
                    napp.ActedOnDateLocalText = napp.ActedOnDateLocal.ToString("M/d/yyyy h:mm tt");


                history.Add(napp);
            }

            return history;

        }
    }
}
