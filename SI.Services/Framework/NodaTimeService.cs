﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SI.BLL;

namespace SI.Services.Framework
{
	public static class NodaTimeService
	{
		// This will return the Windows zone that matches the IANA zone, if one exists.
		public static string IanaToWindows(string ianaZoneId)
		{
			var tzdbSource = NodaTime.TimeZones.TzdbDateTimeZoneSource.Default;
			var mappings = tzdbSource.WindowsMapping.MapZones;
			var item = mappings.FirstOrDefault(x => x.TzdbIds.Contains(ianaZoneId));
			if (item == null) return null;
			return item.WindowsId;
		}

		// This will return the "primary" IANA zone that matches the given windows zone.
		public static string WindowsToIana(string windowsZoneId)
		{
			var tzdbSource = NodaTime.TimeZones.TzdbDateTimeZoneSource.Default;
			var tzi = TimeZoneInfo.FindSystemTimeZoneById(windowsZoneId);
			return tzdbSource.MapTimeZoneId(tzi);
		}

		public static string WindowsToIana(long userId)
		{
			var tzdbSource = NodaTime.TimeZones.TzdbDateTimeZoneSource.Default;

			TimeZoneInfo timezone = ProviderFactory.TimeZones.GetTimezoneForUserID(userId);
			
			var tzi = TimeZoneInfo.FindSystemTimeZoneById(timezone.Id);
			return tzdbSource.MapTimeZoneId(tzi);
		}
	}
}
