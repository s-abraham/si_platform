﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SI.Services.Framework.Models
{
    public class SaveEntityResult
    {
        public SaveEntityResult()
        {
            Problems = new List<string>();
        }
        public List<string> Problems { get; set; }
        public SIDateTime DateModified { get; set; }
        public bool Success
        {
            get
            {
                return !Problems.Any();
            }
        }
    }
}
