﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Kendo.Mvc;

namespace SI.Services.Framework.Models
{
	public class PagedGridRequest
	{
		public int Skip { get; set; }
		public int Take { get; set; }
		public List<PagedGridSortDescriptor> Sorting { get; set; }
		public List<PagedGridFilterDescriptor> Filtering { get; set; }
		public bool CompositeAndFilter { get; set; }
		public bool CompositeOrFilter { get; set; }
		public Dictionary<string, string> FieldMapping { get; set; }

		public PagedGridRequest()
		{
			Sorting = new List<PagedGridSortDescriptor>();
			Filtering = new List<PagedGridFilterDescriptor>();
		}
	}

	public class PagedGridSortDescriptor
	{
		public string Field { get; set; }
		public bool Ascending { get; set; }
	}

	public class PagedGridFilterDescriptor
	{
		public string Field { get; set; }
		public string MappedField { get; set; }
		public FilterOperator Operator { get; set; }
		public string Value { get; set; }
		public string Logical { get; set; }
		public Type MemberType { get; set; }
	}
}