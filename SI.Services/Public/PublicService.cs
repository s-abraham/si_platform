﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using System.Web.Security;
using SI.BLL;
using SI.DTO;

namespace SI.Services.Public
{
	public class PublicService
	{
		public static Guid CreateAccessToken(Guid resellerKey, Guid userKey)
		{
			//todo: change minutes to config setting
			return ProviderFactory.Security.CreateSSOAccessToken(resellerKey, userKey, 5);
		}

		public static bool ValidateSSI(Guid accesstoken, string domain, string userAgent)
		{
			ValidateUserResultDTO vuResult = ProviderFactory.Security.ValidateUser(accesstoken);
			if (vuResult.Outcome == ValidateUserOutcome.Success)
			{
				var auditObj = AuditUserActivity
					.New(vuResult.UserInfo, AuditUserActivityTypeEnum.SignedIn)
					.SetDescription("[{0}] Signed In to Domain [{1}]", vuResult.UserInfo.EffectiveUser.Username, domain)
					.SetUserAgent(userAgent).SetSSOToken(accesstoken);

				ProviderFactory.Logging.Audit(AuditLevelEnum.Information, AuditTypeEnum.SignedIn, string.Format("user signed in with single signon token {0}", accesstoken.ToString()), auditObj, vuResult.UserInfo);
				FormsAuthentication.SetAuthCookie(vuResult.UserInfo.ToAuthCookie(), false);
				return true;
			}

			return false;
		}

		public static Task<bool> SendRestPasswordEmail(string userName, string accessUrl)
		{
			UserDTO userDto = ProviderFactory.Security.GetUserForEmailByUsername(userName);

			AccessTokenBaseDTO token = null;

			if (userDto != null)
			{
				AccessTokenBaseDTO accessToken = new PasswordResetAccessTokenDTO("");
				accessToken.TimeTillExpiration = new TimeSpan(3, 0, 0, 0); //reset good for 3 days??
				accessToken.UserID = userDto.ID.Value;

				token = ProviderFactory.Security.CreateAccessToken(accessToken);
			}

			return Task.Factory.StartNew(() =>
				{
					if (userDto != null)
					{
						bool success = ProviderFactory.Notifications.SendResetPasswordEmail(string.Format("{0} {1}", userDto.FirstName, userDto.LastName), userDto.EmailAddress, string.Format("{0}/{1}", accessUrl, token.Token.ToString()));
						return success;
					}
					return false;
				});

		}

		//public static bool ValidatePasswordResetToken(Guid accessToken)
		//{
		//	AccessTokenBaseDTO accessTokenBaseDto = ProviderFactory.Security.GetAccessToken(accessToken, true);

		//	if (accessTokenBaseDto != null && accessTokenBaseDto.Type() == AccessTokenTypeEnum.PasswordReset)
		//	{
		//		ValidateUserResultDTO validateUserResultDto = ProviderFactory.Security.ValidateUser(new UserInfoDTO(accessTokenBaseDto.UserID, null));

		//		if (validateUserResultDto.Outcome == ValidateUserOutcome.Success)
		//		{
		//			ProviderFactory.Logging.Audit(AuditLevelEnum.Information, AuditTypeEnum.SignedIn, string.Format("user signed in with password reset token {0}", accessToken.ToString()), null, validateUserResultDto.UserInfo);
		//			FormsAuthentication.SetAuthCookie(validateUserResultDto.UserInfo.ToAuthCookie(), false);
		//			return true;
		//		}
		//	}

		//	return false;

		//}
	}
}
