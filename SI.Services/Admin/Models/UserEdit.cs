﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SI.Services.Admin.Models
{
	public class UserEdit
	{
		public long? UserID { get; set; }
		[Required(ErrorMessage = "Last Name is required.")]
		[Display(Name = "Last Name")]
		public string LastName { get; set; }

		[Required(ErrorMessage = "First Name is required.")]
		[Display(Name = "First Name")]
		public string FirstName { get; set; }

		[Required(ErrorMessage = "Email Address is required.")]
		[Display(Name = "Email Address")]
		[DataType(DataType.EmailAddress)]
		[RegularExpression(@"[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.[A-Za-z]{2,4}", ErrorMessage = "Email is not valid.")]
		public string Email { get; set; }

		[DataType(DataType.Date)]
		public DateTime CreateDate { get; set; }

		[Required(ErrorMessage = "Role is required.")]
		public long? Role { get; set; }

		public string RoleName { get; set; }
		public long? RoleID { get; set; }
        public string AccountName { get; set; }        
		public string Status { get; set; }
		public long? MemberOfLocationID { get; set; }
		public bool CascadeRole { get; set; }
		public bool LocalUser { get; set; } //For Filtering
		public long AccountID { get; set; } //For Filtering

		public string SSOKey { get; set; }

		public bool CanImpersonate { get; set; }
	}

	public class AlertEdit
	{
		public long? AccountID { get; set; }
		public long? UserID { get; set; }
		public bool DisableAll { get; set; }
	}

	public class UserAlert
	{
		public int? ID { get; set; }
		public string AccountName { get; set; }
		public string UserName { get; set; }
		public string Email { get; set; }
		public string AlertName { get; set; }
		public string Status { get; set; }
	}

	public class RoleTypes
	{
		public long? ID { get; set; }
		public string Name { get; set; }
	}
	
}
