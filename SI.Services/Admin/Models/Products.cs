﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SI.DTO;

namespace SI.Services.Admin.Models
{
	public class Products
	{
		public long ID { get; set; }
		public string ResellerID_PackageID { get; set; }
		public long ResellerID { get; set; }
		public long PackageID { get; set; }
		public string Name { get; set; }
		public DateTime AddedDate { get; set; }
		public string Category { get; set; }
		public string PackageName { get; set; }

	}

	public class ReputationConfigLink
	{
		public long ID { get; set; }
		public string Name { get; set; }
		public string Url { get; set; }
		public bool SuccessProcessed { get; set; }

        public ReviewUrlDTOEnum Status { get; set; }
        
	}

	public class SocialConfig
	{
		public long? ID { get; set; }
		public string NetworkName { get; set; }
		public long? NetworkID { get; set; }
		public string StatusMessage { get; set; }
		public string ConnectionStatus { get; set; }
		public bool IsRefreshable { get; set; }
		public string NetworkImageUrl { get; set; }
		public string NetworkDescription { get; set; }
		public bool IsConnected { get; set; }
		public string NetworkConnectionUrl { get; set; }
		public string StateContext { get; set; }
		public long SocialAppID { get; set; }
	}

	public class SyndicationConfig
	{
		public int ID { get; set; }
		public string Source { get; set; }
		public string Description { get; set; }
		public bool AutoApproved { get; set; }
		public bool ManualApproval { get; set; }
	}

	public class SocialLoginUrl
	{
		public long SocialAppID { get; set; }
		public string LoginUrl { get; set; }
	}
}
