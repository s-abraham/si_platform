﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SI.Services.Admin.Models
{
	public class UserDetails
	{
		public long UserID { get; set; }

	}


	public class UserDetailsNotifications
	{
		public long UserID { get; set; }
		public List<UserDetailsNotificationConfig> NotificationConfigs { get; set; }
		public List<UserDetailsRecentNotifications> RecentNotifications { get; set; }

		public UserDetailsNotifications()
		{
			NotificationConfigs = new List<UserDetailsNotificationConfig>();
			RecentNotifications = new List<UserDetailsRecentNotifications>();
		}
	}

	public class UserDetailsNotificationConfig
	{
		public long UserID { get; set; }
		public string Name { get; set; }
		public bool Enabled { get; set; }
		public bool CanSubscribe { get; set; }
		public string Type { get; set; }
		public string Method { get; set; }
	}

	public class UserDetailsRecentNotifications
	{
		public long UserID { get; set; }
		public long NotificationTargetID { get; set; }
		public string Name { get; set; }
		public DateTime DateNotified { get; set; }
		
	}

	public class UserDetailEmailedReports
	{
		public long UserID { get; set; }
		public List<UserDetailsReportConfig> ReportConfigs { get; set; }
		public List<UserDetailsRecentReports> RecentReports { get; set; }

		public UserDetailEmailedReports()
		{
			ReportConfigs = new List<UserDetailsReportConfig>();
			RecentReports = new List<UserDetailsRecentReports>();
		}
	}

	public class UserDetailsReportConfig
	{
		public long UserID { get; set; }
		public long ReportSubscriptionID { get; set; }
		public long ReportTemplateID { get; set; }
		public string ScheduleSpec { get; set; }
		public string Name { get; set; }
		public bool Enabled { get; set; }
	}

	public class UserDetailsRecentReports
	{
		public long UserID { get; set; }
		public long ReportLogID { get; set; }
		public string Name { get; set; }
		public DateTime DateNotified { get; set; }
	}

	public class UserDetailActivityList
	{
		public DateTime ActivityDate { get; set; }
		public string Activity { get; set; }
		public string AccountName { get; set; }
	}
}
