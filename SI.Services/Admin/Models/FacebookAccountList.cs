﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SI.Services.Admin.Models
{
	public class FacebookAccountList
	{
		public string FacebookID { get; set; }
		public string Name { get; set; }
		public string ImageUrl { get; set; }
		public string Address { get; set; }
		public string Website { get; set; }
	}

	public class GooglePageList
	{
		public string PageID { get; set; }
		public string Name { get; set; }
		public string ImageUrl { get; set; }
		public string Website { get; set; }
	}

	public class YouTubePageList
	{
		public string PageID { get; set; }
		public string Name { get; set; }
		public string ImageUrl { get; set; }
		public string Website { get; set; }
	}

	public class GoogleTokens
	{
		public string Token { get; set; }
		public string TokenType { get; set; }
		public string RefreshToken { get; set; }
	}
}