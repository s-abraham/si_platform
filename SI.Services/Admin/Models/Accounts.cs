﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SI.Services.Admin.Models
{
	public class AccountList
	{
		public long ID { get; set; }
		public string Name { get; set; }
		public string ParentAccountName { get; set; }
		public string Address { get; set; }
		public string City { get; set; }
		public string State { get; set; }
		public string ZipCode { get; set; }
		public string Type { get; set; }
        public DateTime DateCreated { get; set; }
		public string ExternalID { get; set; }
		public string Partner { get; set; }

		public int OemID { get; set; }
		public int PackageID { get; set; } //not sure what to do with this yet, if many etc.
	}

	public class AccountEdit
	{
		public long ID { get; set; }
		public bool Success { get; set; }
		public string Message { get; set; }
		public bool CanViewUsers { get; set; }
        public bool CanAddUsers { get; set; }
        public bool CanEditUsers { get; set; }
        public bool CanRemoveUsers { get; set; }
        public bool CanViewProducts { get; set; }
        public bool CanConfigProducts { get; set; }
        public bool CanAddProducts { get; set; }
        public bool CanRemoveProducts { get; set; }
        public bool CanViewChildren { get; set; }
        public bool CanAddChildren { get; set; }
        public bool CanRemoveFromParent { get; set; }
		public bool CanViewAccountSettings { get; set; }
		public bool CanViewAlerts { get; set; }
		public bool CanViewSchedule { get; set; }
		public bool CanViewInventory { get; set; }
		public bool CanViewCompetitors { get; set; }
		public bool CanViewSocialConfig { get; set; }
		public bool CanViewUserSSIKey { get; set; }
	}

	public class AccountLocationEdit
	{
		public bool Success { get; set; }
		public string Message { get; set; }

		public long? ID { get; set; }

		[Required(ErrorMessage = "Account Type is Required")]
		public long AccountTypeID { get; set; }
		public long? CRMSystemTypeID { get; set; }

		[DataType(DataType.EmailAddress)]
		[RegularExpression(@"[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.[A-Za-z]{2,4}", ErrorMessage = "Email is not valid.")]
		public string CRMSystemEmail { get; set; }

		[Required(ErrorMessage = "Account Name is Required")]
		public string Name { get; set; }
		
		[Display(Name = "Display Name")]
		public string DisplayName { get; set; }
		
		[Display(Name = "Email Address")]
		[DataType(DataType.EmailAddress)]
		[RegularExpression(@"[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.[A-Za-z]{2,4}", ErrorMessage = "Email is not valid.")]
		public string EmailAddress { get; set; }

		[DataType(DataType.EmailAddress)]
		[RegularExpression(@"[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.[A-Za-z]{2,4}", ErrorMessage = "Email is not valid.")]
		public string LeadsEmailAddress { get; set; }

		[Display(Name = "Website URL")]
		[DataType(DataType.Url)]
		[RegularExpression(@"((https?)://|(www)\.)[a-z0-9-]+(\.[a-z0-9-]+)+([/?].*)?$", ErrorMessage = "Not a valid URL")]
		public string WebsiteURL { get; set; }

		public string StreetAddress1 { get; set; }
		public string StreetAddress2 { get; set; }

		[Required(ErrorMessage = "Country Required")]
		public long? CountryID { get; set; }
		public string CountryName { get; set; }
		[Required(ErrorMessage = "State Required")]
		public long? StateID { get; set; }
		public string StateName { get; set; }
		public string StateAbbreviation { get; set; }
		
		public long? CityID { get; set; }
		[Required(ErrorMessage = "City Required")]
		public string CityName { get; set; }

		[Required(ErrorMessage = "Postal Code Required")]
		public string PostalCode { get; set; }

		public long? TimeZoneID { get; set; }
		
		[Display(Name = "Phone Number")]
		[DataType(DataType.PhoneNumber)]
		[RegularExpression(@"([0-9\(\)\/\+ \-]*)$", ErrorMessage = "Not a valid Phone Number")]
		public string PhoneNumber { get; set; }

		public long? VerticalMarketID { get; set; }
		public long[] FranchiseTypeIDs { get; set; }
		public long? ParentAccountID { get; set; }
		public bool? NewSubAccountAdd { get; set; }
		public string FacebookToken { get; set; }
		public string FacebookAccountID { get; set; }

		public string ExternalAccountID { get; set; }
		public bool CanViewExternalID { get; set; }
		
		public bool CanViewDemoFlag { get; set; }
		public bool IsDemoAccount { get; set; }

		public bool PostRequireApproval { get; set; }


	}

	public class AccountFilterList
	{
		public long ID { get; set; }
		public string Name { get; set; }
	}

	public class AccountSettings
	{
		public long AccountID { get; set; }
		public bool PostRequireApproval { get; set; }
	}

	public class SubAccount
	{
		public long ID { get; set; }
		public string Name { get; set; }
		public string City { get; set; }
		public string State { get; set; }
		public string ZipCode { get; set; }
		public string Partner { get; set; }
	}

	public class Competitor
	{
		public long ID { get; set; }
		public string Name { get; set; }
		public string City { get; set; }
		public string State { get; set; }
		public string ZipCode { get; set; }
		public long AccountID { get; set; }
	}

	public class Inventory
	{
		public long AccountID { get; set; }

		[Required(ErrorMessage = "Inventory ID is required.")]
		public string InventoryID { get; set; }
		public DateTime? LastImported { get; set; }
		public int NumberOfVehicles { get; set; }
		public string InventoryAppUrl { get; set; }
	}
	
	public class Packages
	{
		public long PackageID { get; set; }
		public string PackageName { get; set; }
	}

	public class SocialNetworks
	{
		public long ID { get; set; }
		public string Name { get; set; }
	}

	public class VirtualGroups
	{
		public int ID { get; set; }
		[Required]
		public string Name { get; set; }
		public int NumberOfAccounts { get; set; }

		[DataType(DataType.Date)]
		public DateTime CreateDate { get; set; }
		[DataType(DataType.Date)]
		public DateTime? ModifiedDate { get; set; }
		public List<long> SelectedLocationIDs { get; set; }

		public VirtualGroups()
		{
			SelectedLocationIDs = new List<long>();
		}
	}

	public class VirtualGroupSave
	{
		public int ID { get; set; }
		[Required]
		public string Name { get; set; }
		public string SelectedLocationIDs { get; set; }

	}
}
