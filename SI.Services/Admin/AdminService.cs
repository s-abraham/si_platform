﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Policy;
using System.Text;
using System.Web.Security;
using Facebook;
using Facebook.Entities;
using Google;
using Google.Model;
using ReviewUrlSearch;
using ReviewUrlSearch.Models;
using SI.BLL;
using SI.DTO;
using SI.Extensions;
using SI.Services.Admin.Models;
using SI.Services.Framework;
using SI.Services.Framework.Models;
using Twitter;
using Twitter.Entities;

namespace SI.Services.Admin
{
    /// <summary>
    /// Service layer support for the AdminController
    /// </summary>
    public static class AdminService
    {
		
		/// <summary>
		/// 
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="pagedGridRequest"></param>
        /// <param name="totalCount"></param>
        /// <returns></returns>
        public static List<AccountList> AccountList(UserInfoDTO userInfo, PagedGridRequest pagedGridRequest, ref int totalCount, long? parentAccountID = null, long? notInParentAccountID = null)
        {
            AccountSearchRequestDTO req = new AccountSearchRequestDTO();

            req.PackageIDs = new List<long>();
            req.FranchiseTypeIDs = new List<long>();
            req.AccountTypeIDs = new List<long>();
            req.UserInfo = userInfo;
            req.ParentAccountID = parentAccountID;
            req.NotParentAccountID = notInParentAccountID;

            req.SearchString = "";

            foreach (PagedGridFilterDescriptor filter in pagedGridRequest.Filtering)
            {
                if (filter.Field == "Name")
                    req.SearchString = filter.Value;

                if (filter.Field == "OemID")
                    req.FranchiseTypeIDs.Add(filter.Value.ToLong());

                if (filter.Field == "PackageID")
                    req.PackageIDs.Add(filter.Value.ToLong());
            }

            req.SortBy = 2;
            req.SortAscending = true;

            if (pagedGridRequest.Sorting.Any())
            {
                PagedGridSortDescriptor sort = pagedGridRequest.Sorting.First(); //what abt multiple sorts?
                switch (sort.Field)
                {
                    case "ID": req.SortBy = 1; break;
                    case "Name": req.SortBy = 2; break;
                    case "ParentAccountName": req.SortBy = 3; break;
                    case "Address": req.SortBy = 4; break;
                    case "City": req.SortBy = 5; break;
                    case "State": req.SortBy = 6; break;
                    case "ZipCode": req.SortBy = 7; break;
                }

                req.SortAscending = sort.Ascending;
            }

            req.StartIndex = pagedGridRequest.Skip;
            req.EndIndex = req.StartIndex + pagedGridRequest.Take;
            req.OnlyIfHasChildAccounts = false;

            AccountSearchResultDTO accountSearchResult = ProviderFactory.Security.SearchAccounts(req);

            List<AccountList> accountLists = new List<AccountList>();

            foreach (AccountListItemDTO result in accountSearchResult.Items)
            {
                AccountList accountList = new AccountList();
                accountList.ID = result.ID;
                accountList.Name = result.Name;
                accountList.Address = result.Address;
                accountList.City = result.City;
                accountList.DateCreated = result.DateCreated.LocalDate.GetValueOrDefault();
                accountList.ParentAccountName = result.ParentAccountName;
                accountList.State = result.State;
                accountList.Type = result.Type;
                accountList.ZipCode = result.ZipCode;
                accountList.Partner = result.ThisAccountsResellerName;

                accountList.ExternalID = string.Empty;
                if (!string.IsNullOrEmpty(result.OriginSystemIdentifier))
                    accountList.ExternalID = string.Format("({0})", result.OriginSystemIdentifier.IsNullOptional(""));

                accountLists.Add(accountList);
            }

            totalCount = accountSearchResult.TotalRecords;
            return accountLists;

        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="userInfo"></param>
        /// <param name="accountID"></param>
        /// <returns></returns>
        public static AccountEdit AccountEdit(UserInfoDTO userInfo, long accountID)
        {
            AccountEdit model = new AccountEdit();
            model.ID = accountID;
            model.Success = true;

            if (accountID > 0)
            {
                GetEntityDTO<AccountDTO, AccountOptionsDTO> getResult = ProviderFactory.Security.GetAccountByID(userInfo, accountID);
                if (getResult != null)
                {
                    if (getResult.HasProblems)
                    {
                        model.Success = false;
                        model.Message = getResult.Problems[0];
                    }
                    else
                    {
                        model.ID = getResult.Entity.ID.GetValueOrDefault();
                        model.CanAddChildren = getResult.Options.AddChildren;
                        model.CanAddProducts = getResult.Options.AddProducts;
                        model.CanAddUsers = getResult.Options.AddUsers;
                        model.CanEditUsers = getResult.Options.EditUsers;
                        model.CanRemoveFromParent = getResult.Options.RemoveFromParent;
                        model.CanRemoveProducts = getResult.Options.RemoveProducts;
                        model.CanRemoveUsers = getResult.Options.RemoveUsers;
                        model.CanViewAccountSettings = getResult.Options.ViewAccountSettings;
                        model.CanViewAlerts = getResult.Options.ViewAlerts;
                        model.CanViewChildren = getResult.Options.ViewChildren;
                        model.CanViewCompetitors = getResult.Options.ViewCompetitors;
                        model.CanViewInventory = getResult.Options.ViewInventory;
                        model.CanViewProducts = getResult.Options.ViewProducts;
                        model.CanConfigProducts = getResult.Options.ConfigProducts;
                        model.CanViewSchedule = getResult.Options.ViewSchedule;
                        model.CanViewUsers = getResult.Options.ViewUsers;
                        model.CanViewSocialConfig = getResult.Options.ViewSocialConfigTab;
                        model.CanViewUserSSIKey = getResult.Options.ViewSSOSecretKey;
                    }

                }
            }


            return model;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="userInfo"></param>
        /// <param name="accountID"></param>
        /// <returns></returns>
        public static AccountLocationEdit AccountLocationEdit(UserInfoDTO userInfo, long accountID)
        {
            AccountLocationEdit model = new AccountLocationEdit();
            model.ID = accountID;
            model.Success = true;

            if (accountID > 0)
            {
                GetEntityDTO<AccountDTO, AccountOptionsDTO> getResult = ProviderFactory.Security.GetAccountByID(userInfo, accountID);

                if (getResult != null)
                {
                    if (getResult.HasProblems)
                    {
                        model.Success = false;
                        model.Message = getResult.Problems[0];
                    }
                    else
                    {
                        model.ID = getResult.Entity.ID;
                        model.ParentAccountID = getResult.Entity.ParentAccountID.GetValueOrDefault();
                        model.AccountTypeID = getResult.Entity.AccountTypeID;
                        model.CRMSystemEmail = getResult.Entity.CRMSystemEmail;
                        model.CRMSystemTypeID = getResult.Entity.CRMSystemTypeID;
                        model.CityID = getResult.Entity.CityID;
                        model.CityName = getResult.Entity.CityName;
                        model.CountryID = getResult.Entity.CountryID;
                        model.CountryName = getResult.Entity.CountryName;
                        model.DisplayName = getResult.Entity.DisplayName;
                        model.EmailAddress = getResult.Entity.EmailAddress;
                        model.FranchiseTypeIDs = getResult.Entity.FranchiseTypes;
                        model.LeadsEmailAddress = getResult.Entity.LeadsEmailAddress;
						model.Name = getResult.Entity.Name;
                        model.PhoneNumber = getResult.Entity.PhoneNumber;
                        model.StateAbbreviation = getResult.Entity.StateAbbreviation;
                        model.StateID = getResult.Entity.StateID;
                        model.StateName = getResult.Entity.StateName;
                        model.StreetAddress1 = getResult.Entity.StreetAddress1;
                        model.StreetAddress2 = getResult.Entity.StreetAddress2;
                        model.WebsiteURL = getResult.Entity.WebsiteURL;
                        model.PostalCode = getResult.Entity.PostalCode;
                        model.TimeZoneID = getResult.Entity.TimezoneID;
                        model.PostRequireApproval = getResult.Entity.PostsRequireApproval;
	                    model.VerticalMarketID = getResult.Entity.VerticalID;

                        model.CanViewExternalID = getResult.Options.ViewExternalID;
                        if (model.CanViewExternalID)
                            model.ExternalAccountID = getResult.Entity.OriginSystemIdentifier;

                        model.CanViewDemoFlag = getResult.Options.ViewDemoCheckbox;
                        model.IsDemoAccount = getResult.Entity.IsDemoAccount.GetValueOrDefault();
                    }
                }
            }

            return model;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="userInfoDto"></param>
        /// <param name="accountLocationEdit"></param>
        /// <param name="problems"></param>
        /// <returns></returns>
        public static SaveEntityResult LocationEditSave(UserInfoDTO userInfoDto, AccountLocationEdit accountLocationEdit)
        {
            SaveEntityResult result = new SaveEntityResult();

            GetEntityDTO<AccountDTO, AccountOptionsDTO> getAccount;
            if (accountLocationEdit.ID > 0)
                getAccount = ProviderFactory.Security.GetAccountByID(userInfoDto, accountLocationEdit.ID.GetValueOrDefault());
            else
            {
                getAccount = new GetEntityDTO<AccountDTO, AccountOptionsDTO>();
                getAccount.Entity = new AccountDTO();

                //TODO TEMP SETTING UNTIL REAL RESELLER ID IS SET
                if (!userInfoDto.MemberOfPrimaryResellerID.HasValue)
                    userInfoDto.MemberOfPrimaryResellerID = 197922;

                if (accountLocationEdit.ParentAccountID.GetValueOrDefault() > 0)
                    getAccount.Entity.ParentAccountID = accountLocationEdit.ParentAccountID;
                else
                    getAccount.Entity.ParentAccountID = userInfoDto.MemberOfPrimaryResellerID;
            }

            if (getAccount != null)
            {
                AccountDTO accountDto = getAccount.Entity;

                accountDto.AccountTypeID = accountLocationEdit.AccountTypeID;
                accountDto.CRMSystemEmail = accountLocationEdit.CRMSystemEmail;
                accountDto.CRMSystemTypeID = accountLocationEdit.CRMSystemTypeID;
                accountDto.CityID = accountLocationEdit.CityID;
                accountDto.CityName = accountLocationEdit.CityName;
                accountDto.CountryID = accountLocationEdit.CountryID;
                accountDto.CountryName = accountLocationEdit.CountryName;
                accountDto.DisplayName = accountLocationEdit.DisplayName;
                accountDto.EmailAddress = accountLocationEdit.EmailAddress;
                accountDto.FranchiseTypes = accountLocationEdit.FranchiseTypeIDs;
                accountDto.LeadsEmailAddress = accountLocationEdit.LeadsEmailAddress;
                accountDto.Name = accountLocationEdit.Name;
                accountDto.PhoneNumber = accountLocationEdit.PhoneNumber;
                accountDto.StateAbbreviation = accountLocationEdit.StateAbbreviation;
                accountDto.StateID = accountLocationEdit.StateID;
                accountDto.StateName = accountLocationEdit.StateName;
                accountDto.StreetAddress1 = accountLocationEdit.StreetAddress1;
                accountDto.StreetAddress2 = accountLocationEdit.StreetAddress2;
                accountDto.WebsiteURL = accountLocationEdit.WebsiteURL;
                accountDto.TimezoneID = accountLocationEdit.TimeZoneID.GetValueOrDefault();
                accountDto.PostalCode = accountLocationEdit.PostalCode;
                accountDto.OriginSystemIdentifier = accountLocationEdit.ExternalAccountID;
                accountDto.IsDemoAccount = accountLocationEdit.IsDemoAccount;
	            accountDto.VerticalID = accountLocationEdit.VerticalMarketID;
                
                AuditAccountChanged auditAccount = new AuditAccountChanged();
                if (accountLocationEdit.ID.GetValueOrDefault() > 0)
                {
                    GetEntityDTO<AccountDTO, AccountOptionsDTO> getResult = ProviderFactory.Security.GetAccountByID(userInfoDto, accountLocationEdit.ID.GetValueOrDefault());
                    auditAccount.AccountOld = getResult.Entity;
                }

                SaveEntityDTO<AccountDTO> saveEntity = ProviderFactory.Security.Save(new SaveEntityDTO<AccountDTO>(userInfoDto) { Entity = accountDto });

                if (saveEntity.Problems.Any())
                {
                    result.Problems = saveEntity.Problems;
					
                }
                else
                {
                    accountDto.ID = saveEntity.Entity.ID;
                    auditAccount.AccountNew = accountDto;
                    result.DateModified = saveEntity.Entity.DateModified;

                    Auditor
                        .New(AuditLevelEnum.Information, AuditTypeEnum.AccountConfiguration, userInfoDto, "General-Config")
                        .SetAccountID(accountDto.ID)
                        .SetAuditDataObject(auditAccount)
                        .Save(ProviderFactory.Logging)
                    ;


                    //todo commented out was throwing exception when no address infor and grouping
                    //ProviderFactory.SendNotification.NewAccountNotification(saveEntity);
                    accountLocationEdit.ID = saveEntity.Entity.ID;
                }
            }
            else
            {
                result.Problems.Add("Can't find existing account.");
                //problems.Add("Can not find existing Account");
            }

            return result;

        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="accountID"></param>
        /// <returns></returns>
        public static List<ReputationConfigLink> ReputationConfigLinks(UserInfoDTO userInfo, long accountID)
        {
            List<ReputationConfigLink> links = new List<ReputationConfigLink>();

            List<ReputationConfigLinkDTO> clDto = new List<ReputationConfigLinkDTO>();

            List<AccountReviewUrlDTO> urls = ProviderFactory.Reviews.GetReviewUrls(accountID);
            if (urls != null)
            {
                foreach (var url in urls)
                {
                    ReputationConfigLinkDTO l1 = new ReputationConfigLinkDTO
                        {
                            ID = url.ID.GetValueOrDefault(),
                            Name = url.ReviewSourceName,
                            Url = url.HtmlURL,
                            ReviewSource = url.ReviewSource,
                            IsValid = url.isValid,
                            Status = url.Status
                        };
                    clDto.Add(l1);
                }

                List<DTO.ReviewSourceDTO> reviewSources = ProviderFactory.Reviews.GetReviewSources(accountID);

                IEnumerable<ReviewSourceDTO> results = null;

                results = from c in reviewSources
                          where !(from o in clDto select o.ReviewSource).Contains((ReviewSourceEnum)c.ID)
                          orderby c.DisplayOrder
                          select c;

                foreach (var url in results)
                {
                    ReputationConfigLinkDTO l1 = new ReputationConfigLinkDTO { ID = 0, Name = url.Name, Url = string.Empty, ReviewSource = (ReviewSourceEnum)url.ID };
                    clDto.Add(l1);
                }

                foreach (ReputationConfigLinkDTO linkDTO in clDto)
                {
                    ReputationConfigLink link = new ReputationConfigLink();
                    link.ID = linkDTO.ID;
                    link.Name = linkDTO.Name;
                    link.Url = linkDTO.Url;
                    link.SuccessProcessed = linkDTO.IsValid;
                    link.Status = linkDTO.Status;

                    links.Add(link);
                }

            }

            return links;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="linksDict"></param>
        /// <param name="accountID"></param>
        /// <returns></returns>
        public static SaveEntityResult ReputationConfigSave(UserInfoDTO userId, Dictionary<string, string> linksDict, long accountID, bool saveAll = true)
        {
            SaveEntityResult result = new SaveEntityResult();

            ReviewUrlDTO urldtoget = new ReviewUrlDTO();

            List<AccountReviewUrlDTO> urls = ProviderFactory.Reviews.GetReviewUrls(accountID);
            if (urls != null)
            {
                List<AuditReviewURLOld> auditReviewURLOld = new List<AuditReviewURLOld>();
                List<AuditReviewURLNew> auditReviewURLNew = new List<AuditReviewURLNew>();
                AuditReviewURL auditReviewURL = new AuditReviewURL();

                foreach (string key in linksDict.Keys)
                {
                    string reviewSourceName = string.Empty;

                    if (key.IndexOf("_") > 0)
                    {
                        reviewSourceName = key.Substring(0, key.IndexOf("_"));

                        if (reviewSourceName.ToLower() == "carscom")
                        {
                            reviewSourceName = "CarsDotCom";
                        }
                        if (reviewSourceName.ToLower() == "judysbook")
                        {
                            reviewSourceName = "JudysBook";
                        }

                    }

                    string linkId = key.Substring(key.IndexOf("_") + 1);
                    string url = linksDict[key];

                    if (!linkId.Contains("Requested"))
                    {
                        var oldurl = urls.Where(x => x.ID == linkId.ToNullableLong()).SingleOrDefault();

                        if (oldurl != null && oldurl.HtmlURL.ToLower().Trim() != url.ToLower().Trim() && saveAll)
                        {
                            auditReviewURLOld.Add(new AuditReviewURLOld
                                {
                                    HtmlURL = oldurl.HtmlURL,
                                    ApiURL = oldurl.ApiURL,
                                    ExternalID = oldurl.ExternalID,
                                    ExternalID2 = oldurl.ExternalID2,
                                    ReviewSource = (ReviewSourceEnum)oldurl.ReviewSource

                                });

                            auditReviewURLNew.Add(new AuditReviewURLNew
                                {
                                    HtmlURL = url,
                                    ReviewSource = (ReviewSourceEnum)Enum.Parse(typeof(ReviewSourceEnum), reviewSourceName)
                                });

                            //Update Row
                            ReviewUrlDTO urldto = new ReviewUrlDTO();
                            urldto.HtmlURL = url;
                            urldto.AccountID = accountID;
                            urldto.ReviewSource = (ReviewSourceEnum)Enum.Parse(typeof(ReviewSourceEnum), reviewSourceName);
                            urldto.isValid = true;

                            urldtoget = ProviderFactory.Reviews.SaveReviewUrl(userId, urldto);

                            result.DateModified = urldtoget.DateCreated;
                        }
                        else if (oldurl == null && !string.IsNullOrWhiteSpace(url))
                        {
                            auditReviewURLNew.Add(new AuditReviewURLNew
                                {
                                    HtmlURL = url,
                                    ReviewSource = (ReviewSourceEnum)Enum.Parse(typeof(ReviewSourceEnum), reviewSourceName)
                                });

                            //Insert New Row                        
                            ReviewUrlDTO urldto = new ReviewUrlDTO();
                            urldto.HtmlURL = url;
                            urldto.AccountID = accountID;
                            urldto.ReviewSource = (ReviewSourceEnum)Enum.Parse(typeof(ReviewSourceEnum), reviewSourceName);
                            urldto.isValid = true;

                            urldtoget = ProviderFactory.Reviews.SaveReviewUrl(userId, urldto);

                            result.DateModified = urldtoget.DateCreated;
                        }
                        //TODO Save link
                    }
                }

                #region Audit Log

                auditReviewURL.AccountID = accountID;
                auditReviewURL.auditReviewURLNew = auditReviewURLNew;
                auditReviewURL.auditReviewURLOld = auditReviewURLOld;
                const string AuditMessage = "Product-Config: Reputation Config";

                Auditor
                    .New(AuditLevelEnum.Information, AuditTypeEnum.AccountConfiguration, userId, AuditMessage)
                    .SetAccountID(accountID)
                    .SetAuditDataObject(auditReviewURL)
                    .Save(ProviderFactory.Logging)
                ;

                #endregion

                //Success = true;

            }



            return result;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="userInfo"></param>
        /// <param name="userEdit"></param>
        /// <param name="problems"></param>
        /// <returns></returns>
        public static SaveEntityResult UsersEditSave(UserInfoDTO userInfo, UserEdit userEdit)
        {
            SaveEntityResult result = new SaveEntityResult();

            bool Success = false;

            AuditUserChanged auditUser = new AuditUserChanged();
            UserDTO user = new UserDTO();

            user.EmailAddress = userEdit.Email;
            user.FirstName = userEdit.FirstName;
            user.LastName = userEdit.LastName;
            user.RootAccountID = userEdit.AccountID;
            user.MemberOfAccountID = userEdit.MemberOfLocationID.GetValueOrDefault();
            user.RoleTypeID = userEdit.Role;
            user.SuperAdmin = false;
            user.Username = userEdit.Email;
            user.ID = userEdit.UserID;
            user.CascadeRole = userEdit.CascadeRole;

            CreateUserResultDTO createUserResultDTO = new CreateUserResultDTO();
            const string AuditMessage = "Users";

            if (userEdit.UserID.GetValueOrDefault(0) == 0)
            {
                //Insert
                #region Audit Log

                auditUser.userDTONew = user;
                auditUser.userDTOOld = null;

                Auditor
                    .New(AuditLevelEnum.Information, AuditTypeEnum.AccountConfiguration, userInfo, AuditMessage)
                    .SetAccountID(user.RootAccountID)
                    .SetAuditDataObject(auditUser)
                    .Save(ProviderFactory.Logging)
                ;

                #endregion

                user.Password = "Password$";

                createUserResultDTO = ProviderFactory.Security.SaveUser(user, userInfo, false, false);

                if (createUserResultDTO.Outcome == CreateUserOutcome.Success)
                {
                    ProviderFactory.Security.AddRole(userInfo, createUserResultDTO.User.ID.Value, userEdit.AccountID, user.RoleTypeID.Value, userEdit.CascadeRole, false, false);
                    //Save New User Notification to DB 
                    //ProviderFactory.SendNotification.NewUserNotification(userInfo, createUserResultDTO.User.ID.Value);
                    Success = true;
                }
            }
            else
            {
                //Update
                #region Audit Log

                UserDTO userDTO = ProviderFactory.Security.GetUserByID(Convert.ToInt64(userEdit.UserID), userInfo);
                auditUser.userDTOOld = userDTO;
                auditUser.userDTONew = user;

                Auditor
                    .New(AuditLevelEnum.Information, AuditTypeEnum.AccountConfiguration, userInfo, AuditMessage)
                    .SetAccountID(user.RootAccountID)
                    .SetAuditDataObject(auditUser)
                    .Save(ProviderFactory.Logging)
                ;

                #endregion

                createUserResultDTO = ProviderFactory.Security.SaveUser(user, userInfo, false, false);
                if (createUserResultDTO.Outcome == CreateUserOutcome.Success)
                {
                    ProviderFactory.Security.AddRole(userInfo, createUserResultDTO.User.ID.Value, userEdit.AccountID, user.RoleTypeID.Value, userEdit.CascadeRole, false, false);
                    Success = true;
                }
            }

            if (createUserResultDTO.Outcome != CreateUserOutcome.Success)
            {
                string errorMessage = "";
                switch (createUserResultDTO.Outcome)
                {
                    case CreateUserOutcome.EmailAddressAlreadyExists:
                        errorMessage = "The entered Email address already exist.";
                        break;
                    case CreateUserOutcome.EmailAddressRequired:
                        errorMessage = "The Email address is required.";
                        break;
                    case CreateUserOutcome.InvalidEmailAddress:
                        errorMessage = "Invalid Email address entered.";
                        break;
                    case CreateUserOutcome.PasswordRequired:
                        errorMessage = "The password is required.";
                        break;
                    case CreateUserOutcome.UsernameAlreadyExists:
                        errorMessage = "The entered Email address (Username) already exist.";
                        break;
                    case CreateUserOutcome.UsernameRequired:
                        errorMessage = "The Username is required.";
                        break;
                }
                result.Problems.Add(errorMessage);
            }
            else
            {
                if (createUserResultDTO.User.DateModified != null)
                    result.DateModified = createUserResultDTO.User.DateModified;
                else
                    result.DateModified = createUserResultDTO.User.CreateDate;
            }

            return result;

        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="userInfo"></param>
        /// <param name="accountID"></param>
        /// <returns></returns>
        public static List<UserEdit> UsersEditGrid(UserInfoDTO userInfo, long accountID)
        {
            List<UserEdit> userEdits = new List<UserEdit>();

            UserSearchRequestDTO req = new UserSearchRequestDTO()
            {
                UserInfo = userInfo,
                SearchString = "",
                StartIndex = 1,
                EndIndex = 100,
                MemberOfAccountID = accountID,
                SortBy = 1,
                SortAscending = true
            };

            List<UserSearchResultItemDTO> usersDTO = ProviderFactory.Security.SearchUsers(req).Items;
            if (usersDTO != null)
            {
                foreach (var userDTO in usersDTO)
                {
                    if (userDTO.ID != userInfo.EffectiveUser.ID)
                    {
                        UserEdit userEdit = new UserEdit();
                        userEdit.CreateDate = userDTO.DateCreated.LocalDate.GetValueOrDefault();
                        userEdit.Email = userDTO.EmailAddress;
                        userEdit.FirstName = userDTO.FirstName;
                        userEdit.LastName = userDTO.LastName;
                        userEdit.UserID = userDTO.ID;
                        userEdit.Role = userDTO.RoleTypeID;
                        userEdit.RoleName = userDTO.RoleTypeName;
                        userEdit.Status = "Active";
                        userEdit.CascadeRole = userDTO.CascadeRole.GetValueOrDefault();
                        userEdit.MemberOfLocationID = userDTO.MemberOfAccountID;
                        userEdit.AccountName = userDTO.MemberOfAccountName;
                        userEdit.SSOKey = userDTO.SSOSecretKey.ToString();

                        userEdits.Add(userEdit);
                    }
                }
            }

            return userEdits;
        }

        public static List<UserEdit> UsersExistAccessEditGrid(UserInfoDTO userInfo, long accountID, string search, int startIndex, int endIndex, PagedGridRequest pagedGridRequest, string mode)
        {
            List<UserEdit> userEdits = new List<UserEdit>();

            UserSearchRequestDTO req = new UserSearchRequestDTO()
            {
                UserInfo = userInfo,
                SearchString = search,
                StartIndex = startIndex,
                EndIndex = endIndex,
                SortBy = 1,
                SortAscending = true
            };

            if (mode == "UsersAccessGrid")
                req.UsersWithoutRoleInAccountID = accountID;
            else
                req.NotMemberOfAccountID = accountID;

            foreach (PagedGridFilterDescriptor filter in pagedGridRequest.Filtering)
            {
                if (filter.Field == "LastName")
                    req.SearchString = filter.Value;

                if (filter.Field == "Email")
                    req.SearchString = filter.Value;
            }


            List<UserSearchResultItemDTO> usersDTO = ProviderFactory.Security.SearchUsers(req).Items;
            if (usersDTO != null)
            {
                foreach (var userDTO in usersDTO)
                {
                    if (userDTO.ID != userInfo.EffectiveUser.ID)
                    {
                        UserEdit userEdit = new UserEdit();
                        //userEdit.CreateDate = userDTO.CreateDate.LocalDate.GetValueOrDefault();
                        userEdit.Email = userDTO.EmailAddress;
                        userEdit.FirstName = userDTO.FirstName;
                        userEdit.LastName = userDTO.LastName;
                        userEdit.UserID = userDTO.ID;
                        //userEdit.Role = userDTO.RoleTypeID;
                        //userEdit.RoleName = userDTO.RoleTypeName;
                        userEdit.Status = "Active";
                        //userEdit.CascadeRole = userDTO.CascadeRole;
                        userEdit.MemberOfLocationID = userDTO.MemberOfAccountID;
                        userEdit.AccountName = userDTO.MemberOfAccountName;

                        userEdits.Add(userEdit);
                    }
                }
            }

            return userEdits;
        }

        public static List<UserEdit> SearchUsers(UserInfoDTO userInfo, PagedGridRequest pagedGridRequest, ref int totalCount)
        {
            UserSearchRequestDTO req = new UserSearchRequestDTO()
                {
                    UserInfo = userInfo,
                    StartIndex = pagedGridRequest.Skip,
                    EndIndex = pagedGridRequest.Skip + pagedGridRequest.Take,
                    SortAscending = true,
                    SortBy = 4
                };

            foreach (PagedGridFilterDescriptor filter in pagedGridRequest.Filtering)
            {
                if (filter.Field == "AccountID")
                    req.MemberOfAccountID = filter.Value.ToLong();

                if (filter.Field == "LastName")
                    req.SearchString = filter.Value;
            }

            if (pagedGridRequest.Sorting.Any())
            {
                PagedGridSortDescriptor sort = pagedGridRequest.Sorting.First();
                switch (sort.Field)
                {
                    case "LastName": req.SortBy = 3; break;
                    case "Email": req.SortBy = 4; break;
                    case "CreateDate": req.SortBy = 1; break;
                    case "AccountName": req.SortBy = 5; break;
                }

                req.SortAscending = sort.Ascending;
            }

            List<UserEdit> users = new List<UserEdit>();
            UserSearchResultDTO result = ProviderFactory.Security.SearchUsers(req);
            totalCount = result.TotalRecords;

            foreach (UserSearchResultItemDTO item in result.Items)
            {
                users.Add(new UserEdit()
                {
                    FirstName = item.FirstName,
                    LastName = item.LastName,
                    Email = item.EmailAddress,
                    MemberOfLocationID = item.MemberOfAccountID,
                    AccountName = item.MemberOfAccountName,
                    AccountID = item.MemberOfAccountID,
                    CreateDate = item.DateCreated.LocalDate.GetValueOrDefault(),
                    UserID = item.ID,
					CanImpersonate = item.CanImpersonate
                });
            }

            return users;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="accountID"></param>
        /// <returns></returns>
        public static List<Competitor> CompetitorEditGrid(long accountID)
        {
            List<Competitor> competitors = new List<Competitor>();

            List<CompetitorDTO> competitorDtos = ProviderFactory.Lists.GetCompetitorByAccountID(accountID);
            if (competitorDtos != null)
            {
                foreach (CompetitorDTO dto in competitorDtos)
                {
                    Competitor competitor = new Competitor();
                    competitor.ID = dto.ID;
                    competitor.AccountID = dto.AccountID;
                    competitor.City = dto.City;
                    competitor.Name = dto.Name;
                    competitor.State = dto.State;
                    competitor.ZipCode = dto.ZipCode;

                    competitors.Add(competitor);
                }
            }

            return competitors;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="userInfo"></param>
        /// <param name="ID"></param>
        /// <param name="problems"></param>
        /// <returns></returns>
        public static bool CompetitorRemove(UserInfoDTO userInfo, long ID, ref List<string> problems)
        {
            bool Success = false;

            SaveEntityDTO<bool> deleteEntity = ProviderFactory.Security.RemoveCompetitor(ID, userInfo);
            if (deleteEntity != null)
            {
                Success = deleteEntity.Entity;

                if (deleteEntity.Problems.Any())
                    problems.AddRange(deleteEntity.Problems);
            }

            return Success;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="userInfoDto"></param>
        /// <param name="accountID"></param>
        /// <param name="competitorID"></param>
        /// <param name="problems"></param>
        /// <returns></returns>
        public static bool CompetitorAdd(UserInfoDTO userInfoDto, long accountID, long competitorID, ref List<string> problems)
        {
            bool Success = false;
            const int maxCompetitors = 5;

            SaveEntityDTO<bool> saveEntity = ProviderFactory.Security.AddCompetitor(accountID, competitorID, maxCompetitors, userInfoDto);

            if (saveEntity != null)
            {
                Success = saveEntity.Entity;

                if (saveEntity.Problems.Any())
                    problems.AddRange(saveEntity.Problems);
            }

            return Success;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="accountID"></param>
        /// <returns></returns>
        public static List<Competitor> CompetitorAddEdit(UserInfoDTO userInfoDto, long accountID, int RangeInMiles = 50)
        {
            List<Competitor> competitors = new List<Competitor>();

            List<CompetitorDTO> competitorDtos = ProviderFactory.Lists.GetAccountsByDistance(userInfoDto, accountID, RangeInMiles);
            if (competitorDtos != null)
            {
                foreach (CompetitorDTO dto in competitorDtos)
                {
                    Competitor competitor = new Competitor();
                    competitor.ID = dto.ID;
                    competitor.AccountID = dto.AccountID;
                    competitor.City = dto.City;
                    competitor.Name = dto.Name;
                    competitor.State = dto.State;
                    competitor.ZipCode = dto.ZipCode;

                    competitors.Add(competitor);
                }
            }

            return competitors;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="userInfo"></param>
        /// <param name="accountID"></param>
        /// <returns></returns>
        public static List<Products> ProductsEditGrid(UserInfoDTO userInfo, long accountID)
        {
            List<Products> products = new List<Products>();

            List<AccountPackageListItemDTO> packages = ProviderFactory.Products.GetSubscribedPackagesForAccount(accountID, userInfo);
            if (packages != null)
            {
                foreach (var package in packages)
                {
                    Products product = new Products();
                    product.ID = package.SubscriptionID;
                    product.PackageName = package.PackageName;
                    product.AddedDate = package.DateAdded.LocalDate.GetValueOrDefault();
                    product.Name = package.PackageName;
                    product.PackageID = package.PackageID;
                    product.ResellerID = package.ResellerID;

                    products.Add(product);
                }
            }

            return products;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="userInfo"></param>
        /// <param name="accountID"></param>
        /// <returns></returns>
        public static List<Products> ProductListAvailable(UserInfoDTO userInfo, long accountID)
        {
            List<Products> products = new List<Products>();

            List<DTO.PackageDTO> packages = ProviderFactory.Products.GetAvailablePackagesForAccount(accountID, userInfo);
            if (packages != null)
            {
                foreach (var package in packages)
                {
                    Products product = new Products();
                    product.ID = package.ID;
                    product.ResellerID_PackageID = package.ResellerID + "|" + package.PackageID;
                    product.PackageName = package.PackageName;
                    product.AddedDate = package.DateCreated.LocalDate.GetValueOrDefault();
                    product.Name = package.PackageName;
                    product.PackageID = package.PackageID;
                    product.ResellerID = package.ResellerID;

                    products.Add(product);
                }
            }

            return products;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public static List<Packages> PackagesList()
        {
            List<Packages> packages = new List<Packages>();

            List<PackageDTO> packageDtos = ProviderFactory.Products.GetPackages().OrderBy(f => f.PackageName).ToList();
            if (packageDtos != null)
            {
                foreach (PackageDTO dto in packageDtos)
                {
                    Packages pack = new Packages();
                    pack.PackageID = dto.PackageID;
                    pack.PackageName = dto.PackageName;
                    packages.Add(pack);
                }
            }

            return packages;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="userInfo"></param>
        /// <param name="resellerIdPackageId"></param>
        /// <param name="accountID"></param>
        /// <param name="problems"></param>
        /// <returns></returns>
        public static SaveEntityResult ProductAdd(UserInfoDTO userInfo, string resellerIdPackageId, long accountID)
        {
            SaveEntityResult result = new SaveEntityResult();

            if (resellerIdPackageId.IndexOf("|") > 0)
            {
                string[] Ids = resellerIdPackageId.Split('|');
                long ResellerID = Ids[0].ToLong();
                long PackageID = Ids[1].ToLong();

                UpdatePackageResultDTO updatePackageResultDTO = ProviderFactory.Products.AddPackage(accountID, ResellerID, PackageID, userInfo, null, 0);
                if (updatePackageResultDTO != null)
                {
                    if (updatePackageResultDTO.Outcome == UpdatePackageOutcome.Success)
                    {
                        #region Audit Log
                        const string AuditMessage = "Product Selection";
                        AuditProductSelection auditProductSelection = new AuditProductSelection();

                        auditProductSelection.accountID = accountID;
                        auditProductSelection.ResellerID = ResellerID;
                        auditProductSelection.PackageID = PackageID;
                        auditProductSelection.SubscriptionID = 0;

                        Auditor
                            .New(AuditLevelEnum.Information, AuditTypeEnum.AccountConfiguration, userInfo, AuditMessage)
                            .SetAccountID(accountID)
                            .SetAuditDataObject(auditProductSelection)
                            .Save(ProviderFactory.Logging)
                        ;

                        #endregion

                        result.DateModified = new SIDateTime(DateTime.UtcNow, ProviderFactory.TimeZones.GetTimezoneForUserID(userInfo.EffectiveUser.ID.GetValueOrDefault()));

                    }
                    else
                    {
                        if (updatePackageResultDTO.Problems.Any())
                            result.Problems.AddRange(updatePackageResultDTO.Problems);
                    }
                }
            }
            else
                result.Problems.Add("Unable to update at this time.(Not Valid Reseller Or Package).");

            return result;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="userInfo"></param>
        /// <param name="SubscriptionID"></param>
        /// <param name="accountID"></param>
        /// <param name="problems"></param>
        /// <returns></returns>
        public static bool ProductRemove(UserInfoDTO userInfo, long SubscriptionID, long accountID, ref List<string> problems)
        {
            bool Success = false;

            UpdatePackageResultDTO updatePackageResultDTO = ProviderFactory.Products.RemovePackage(SubscriptionID, userInfo);
            if (updatePackageResultDTO != null)
            {
                if (updatePackageResultDTO.Outcome == UpdatePackageOutcome.Success)
                {
                    #region Audit Log
                    const string AuditMessage = "Product Selection";
                    AuditProductSelection auditProductSelection = new AuditProductSelection();

                    auditProductSelection.accountID = accountID;
                    auditProductSelection.SubscriptionID = SubscriptionID;

                    Auditor
                        .New(AuditLevelEnum.Information, AuditTypeEnum.AccountConfiguration, userInfo, AuditMessage)
                        .SetAccountID(accountID)
                        .SetAuditDataObject(auditProductSelection)
                        .Save(ProviderFactory.Logging)
                    ;

                    #endregion

                    Success = true;
                }
                else
                {
                    if (updatePackageResultDTO.Problems.Any())
                        problems.AddRange(updatePackageResultDTO.Problems);
                }
            }

            return Success;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="userInfoDto"></param>
        /// <param name="userID"></param>
        /// <param name="accountID"></param>
        /// <returns></returns>
        public static bool UserAddExistingUser(UserInfoDTO userInfo, long userID, long accountID, long roleID, bool cascade, bool accessEdit, ref string problems)
        {
            bool Success = true;

            if (accessEdit)
            {
                RoleChangeResultDTO dto = ProviderFactory.Security.AddRole(userInfo, userID, accountID, roleID, cascade, false, false);
                Success = dto.Success;
                if (!Success)
                    problems = string.Join(", ", dto.Problems);
            }
            else
            {
                ChangeUsersMemberAccountResultDTO changeUsersMember = ProviderFactory.Security.ChangeUserMemberOfAccount(userInfo, userID, accountID, roleID, cascade);
                Success = changeUsersMember.Success;
                if (!Success)
                    problems = string.Join(", ", changeUsersMember.Problems);
            }

            return Success;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        public static List<RoleTypes> RoleTypes(UserInfoDTO userInfo)
        {
            List<RoleTypes> roleTypes = new List<RoleTypes>();

            List<RoleTypeDTO> roleTypeDtos = ProviderFactory.Security.GetRoleTypes(userInfo).OrderBy(f => f.Name).ToList();
            if (roleTypeDtos != null)
            {
                foreach (RoleTypeDTO dto in roleTypeDtos)
                {
                    if (dto.Name != "Internal Admin" || userInfo.EffectiveUser.RoleTypeName == "Internal Admin")
                    {
                        RoleTypes roleType = new RoleTypes();
                        roleType.ID = dto.ID;
                        roleType.Name = dto.Name;
                        roleTypes.Add(roleType);
                    }
                }
            }

            return roleTypes;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="userInfo"></param>
        /// <param name="accountID"></param>
        /// <returns></returns>
        public static List<UserEdit> UsersAccessEditGrid(UserInfoDTO userInfo, long accountID)
        {
            List<UserEdit> userEdits = new List<UserEdit>();

            List<UserAccountAccessListItemDTO> usersDTO = ProviderFactory.Security.GetUserAccessListByAccountID(accountID, userInfo);
            if (usersDTO != null)
            {
                foreach (var userDTO in usersDTO)
                {
                    UserEdit userEdit = new UserEdit();
                    //userEdit.CreateDate = userDTO.CreateDate.LocalDate.GetValueOrDefault();
                    userEdit.Email = userDTO.UserEmailAddress;
                    userEdit.FirstName = userDTO.UserFirstName;
                    userEdit.LastName = userDTO.UserLastName;
                    userEdit.UserID = userDTO.UserID;
                    userEdit.Role = (long)userDTO.RoleType;
                    userEdit.RoleName = userDTO.RoleName;
                    userEdit.RoleID = userDTO.RoleID;
                    userEdit.Status = "Active";
                    userEdit.CascadeRole = userDTO.Cascading;
                    userEdit.MemberOfLocationID = userDTO.MemberOfAccountID;
                    userEdit.AccountName = userDTO.InheritedFromAccountName;
                    userEdit.LocalUser = !userDTO.InheritedFromAccountID.HasValue || userDTO.InheritedFromAccountID.GetValueOrDefault() == accountID;

                    userEdits.Add(userEdit);
                }
            }
            return userEdits;

        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="pagedGridRequest"></param>
        /// <returns></returns>
        public static List<SyndicationConfig> SyndicationEditGrid(UserInfoDTO userInfo, PagedGridRequest pagedGridRequest)
        {
            List<SyndicationConfig> syndicationConfigs = new List<SyndicationConfig>();

            SyndicationConfig config = new SyndicationConfig();
            config.ID = 1;
            config.Source = "Ally";
            config.Description = "description of Ally description of Ally description of Ally description of Ally description of Ally description of Ally";
            config.AutoApproved = false;
            config.ManualApproval = true;
            syndicationConfigs.Add(config);

            SyndicationConfig config2 = new SyndicationConfig();
            config2.ID = 2;
            config2.Source = "Carfax";
            config2.Description = "description of CarFax description of CarFax description of CarFax description of CarFax description of CarFax description of CarFax ";
            config2.AutoApproved = false;
            config2.ManualApproval = false;
            syndicationConfigs.Add(config2);

            SyndicationConfig config3 = new SyndicationConfig();
            config3.ID = 3;
            config3.Source = "Edmunds";
            config3.Description = "description of Edmunds description of Edmunds description of Edmunds description of Edmunds description of Edmunds description of Edmunds ";
            config3.AutoApproved = true;
            config3.ManualApproval = false;
            syndicationConfigs.Add(config3);

            return syndicationConfigs;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="pagedGridRequest"></param>
        /// <returns></returns>
        public static List<SocialConfig> SocialConfigListView(UserInfoDTO userInfo, PagedGridRequest pagedGridRequest, long? accountID)
        {
            List<SocialConfig> socialConfigs = new List<SocialConfig>();

            GetEntityDTO<AccountDTO, AccountOptionsDTO> accountDto = ProviderFactory.Security.GetAccountByID(userInfo, accountID.GetValueOrDefault());

            //TODO ONLY NETWORKS THAT HAVE SOCIALAPPS For
            List<SocialNetworkDTO> socialNetworkDtos = ProviderFactory.Social.GetSocialNetworks();
            foreach (SocialNetworkDTO network in socialNetworkDtos)
            {
                SocialConfig config = new SocialConfig();
                config.ID = 0;
                config.NetworkName = network.Name.ToTitleCase();
                config.NetworkID = network.ID;
                config.StatusMessage = " Never connected.";
                config.NetworkImageUrl = "/assets/style/aristo/images/link_unchain.png";
                config.ConnectionStatus = SocialConnectionStatusEnum.NotConnected.ToString().ToLower();
                config.IsRefreshable = false;
                config.NetworkDescription = "Never Connected";
                config.IsConnected = false;
                config.StateContext = Guid.NewGuid().ToString();

                SocialLoginUrl socialLoginUrl = GetSocialLoginUrl(userInfo, accountDto.Entity.ResellerForThisAccount.ID.GetValueOrDefault(), (SocialNetworkEnum)network.ID, config.StateContext);//.Replace("https://", "");;
                if (!string.IsNullOrEmpty(socialLoginUrl.LoginUrl))
                {
                    config.NetworkConnectionUrl = socialLoginUrl.LoginUrl;
                    config.SocialAppID = socialLoginUrl.SocialAppID;

                    socialConfigs.Add(config);
                }
            }

            List<SocialCredentialDTO> socialCredentialsDtos = ProviderFactory.Social.GetAccountSocialCredentials(userInfo, accountID.GetValueOrDefault());


			foreach (SocialConfig config in socialConfigs)
	        {
				SocialCredentialDTO dto = socialCredentialsDtos.Where(c => c.SocialNetworkID == config.NetworkID && c.IsActive).FirstOrDefault();

				if (dto != null)
                {
                    if (!string.IsNullOrEmpty(dto.Token))
                    {
                        config.ID = dto.ID;
                        //config.SocialAppID = dto.SocialAppID; // SPN-674

                        string status = "";
                        if (dto.IsValid)
                        {
                            status = "Connected";
                            config.NetworkImageUrl = "/assets/style/aristo/images/link.png";
                            config.ConnectionStatus = SocialConnectionStatusEnum.Connected.ToString().ToLower();
                            config.IsConnected = true;
                            config.IsRefreshable = true;
                        }
                        else if (!dto.IsValid)
                        {
                            status = "Broken Connection";
                            config.NetworkImageUrl = "/assets/style/aristo/images/link_break.png";
                            config.ConnectionStatus = SocialConnectionStatusEnum.PreviousConnectBroke.ToString().ToLower();
                        }

                        config.StatusMessage = status;
                        config.NetworkDescription = string.Format("{0}, last validated {1}", status, dto.DateLastVerified.GetValueOrDefault().ToShortDateString());
                    }
                }
                else
                {
					dto = socialCredentialsDtos.Where(c => c.SocialNetworkID == config.NetworkID && !c.IsActive).FirstOrDefault();
                    if (dto != null)
                    {
                        if (!string.IsNullOrEmpty(dto.Token))
                        {
                            config.ID = dto.ID;
                            //config.SocialAppID = dto.SocialAppID; // SPN-674

                            config.NetworkImageUrl = "/assets/style/aristo/images/link_unchain.png";
                            config.ConnectionStatus = SocialConnectionStatusEnum.NotConnected.ToString().ToLower();

                            config.StatusMessage = "Disconnected";
                            config.NetworkDescription = string.Format("{0}, last validated {1}", "Disconnected", dto.DateLastVerified.GetValueOrDefault().ToShortDateString());
                        }
                    }
                }



            }

            return socialConfigs;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="pagedGridRequest"></param>
        /// <returns></returns>
        public static List<UserAlert> AlertsGridList(UserInfoDTO userInfo, PagedGridRequest pagedGridRequest)
        {
            List<UserAlert> userAlerts = new List<UserAlert>();
            UserAlert alert = new UserAlert();
            alert.ID = 1;
            alert.AccountName = "Autostartups";
            alert.UserName = "Giacomo Guilizzoni";
            alert.Email = "john@me.com";
            alert.AlertName = "New Review Alert";
            alert.Status = "Active";
            userAlerts.Add(alert);

            UserAlert alert2 = new UserAlert();
            alert.ID = 2;
            alert2.AccountName = "Autostartups";
            alert2.UserName = "Giacomo Guilizzoni";
            alert2.Email = "john@me.com";
            alert2.AlertName = "Negative Review Alert";
            alert2.Status = "Active";
            userAlerts.Add(alert2);

            UserAlert alert3 = new UserAlert();
            alert3.ID = 3;
            alert3.AccountName = "Autostartups";
            alert3.UserName = "Giacomo Guilizzoni";
            alert3.Email = "john@me.com";
            alert3.AlertName = "Overdue Tasks Alert";
            alert3.Status = "Active";
            userAlerts.Add(alert3);

            UserAlert alert4 = new UserAlert();
            alert4.ID = 4;
            alert4.AccountName = "Autostartups";
            alert4.UserName = "Giacomo Guilizzoni";
            alert4.Email = "john@me.com";
            alert4.AlertName = "Publish Approvals Alert";
            alert4.Status = "Active";
            userAlerts.Add(alert4);

            return userAlerts;

        }

        public static Inventory InventoryEdit(long accountID)
        {
            Inventory inventory = new Inventory();
            inventory.AccountID = accountID;
            inventory.InventoryID = "IhaveNoClue";
            inventory.LastImported = DateTime.Now;
            inventory.NumberOfVehicles = 123;
            inventory.InventoryAppUrl = "http://www.thejohncarterfiles.com";

            return inventory;
        }

        public static bool UserActionMenu(UserInfoDTO userInfo, long userID, string menuAction, ref string responseMessage)
        {
            bool success = false;

            switch (menuAction)
            {
                case "resetpassword":
                    responseMessage = "Could not send Reset Password Email.";
                    UserDTO userDto = ProviderFactory.Security.GetUserByID(userID, userInfo);
                    if (userDto != null)
                    {
                        AccessTokenBaseDTO accessToken = new PasswordResetAccessTokenDTO("");
                        accessToken.TimeTillExpiration = new TimeSpan(3, 0, 0, 0); //reset good for 3 days??
                        accessToken.UserID = userDto.ID.Value;

                        AccessTokenBaseDTO token = ProviderFactory.Security.CreateAccessToken(accessToken);
                        string callback = string.Format("{0}/access", Settings.GetSetting("site.baseurl", "socialintegration.com"));

                        success = ProviderFactory.Notifications.SendResetPasswordEmail(string.Format("{0} {1}", userDto.FirstName, userDto.LastName), userDto.EmailAddress, string.Format("{0}/{1}", callback, token.Token.ToString()));

                        if (success)
                            responseMessage = string.Format("Reset Password email has been sent to the User {0} {1}, {2}", userDto.FirstName, userDto.LastName, userDto.EmailAddress);

                    }

                    break;
                case "markasinactive":
                    ProviderFactory.Security.SetUserActiveState(userInfo, userID, false);
                    success = true;
                    responseMessage = "User has been set as InActive.";
                    break;
            }

            return success;

        }

        public static bool UserAccessActionMenu(UserInfoDTO userInfo, long userID, long roleID, string menuAction, ref string problems)
        {
            bool Success = false;

            switch (menuAction)
            {
                case "remove":
                    if (roleID > 0)
                    {
                        RoleChangeResultDTO dto = ProviderFactory.Security.RemoveRole(userInfo, roleID);
                        Success = dto.Success;

                        if (!Success)
                            problems = string.Join(", ", dto.Problems);
                    }
                    break;

            }

            return Success;
        }

        public static bool SubAccountDetach(UserInfoDTO userInfo, long subAccountID)
        {
            ChangeParentAccountResultDTO changeParentAccountResultDto = ProviderFactory.Security.DetachAccount(userInfo, subAccountID);

            return changeParentAccountResultDto.Success;
        }

        public static bool SubAccountAddAccount(UserInfoDTO UserInfo, int subAccountID, long accountID, ref List<string> problems)
        {
            ChangeParentAccountResultDTO changeParentAccountResultDto = ProviderFactory.Security.ChangeParentAccount(UserInfo, subAccountID, accountID, false);
            problems = changeParentAccountResultDto.Problems;

            return changeParentAccountResultDto.Success;
        }

        public static SaveEntityResult SaveSyndicationConfig(IEnumerable<SyndicationConfig> syndicationConfigs, UserInfoDTO userInfo)
        {
            SaveEntityResult result = new SaveEntityResult();
            result.DateModified = new SIDateTime(DateTime.Now, userInfo.EffectiveUser.ID.GetValueOrDefault());
            return result;
        }

        public static bool AlertsEdit(AlertEdit alertEdit, ref List<string> problems)
        {
            return true;
        }

        public static bool InventoryEditSave(Inventory inventory, ref List<string> problems)
        {
            return true;
        }

        public static List<FacebookAccountList> FaceBookAccountList(string token)
        {
            List<FacebookAccountList> facebookAccountLists = new List<FacebookAccountList>();

            if (!string.IsNullOrEmpty(token))
            {
                FacebookAccount accounts = FacebookAPI.GetFacebookAccounts(token);
                if (accounts != null)
                {
                    foreach (Account act in accounts.data)
                    {
                        FacebookAccountList facebookAccount = new FacebookAccountList();

                        FacebookPage facebookPage = FacebookAPI.GetFacebookPageInformation(act.id, act.access_token);
                        FaceBookUserPicture picture = FacebookAPI.FaceBookUserPicture(act.id);

                        facebookAccount.Name = facebookPage.page.name.IsNullOptional("");
                        facebookAccount.FacebookID = act.id;
                        facebookAccount.Address = string.Format("{0} {1}, {2} {3}", facebookPage.page.location.street, facebookPage.page.location.city, facebookPage.page.location.state,
                                                                facebookPage.page.location.zip);
                        facebookAccount.Website = facebookPage.page.website;
                        facebookAccount.ImageUrl = picture.picture.data.url;

                        facebookAccountLists.Add(facebookAccount);
                    }
                }
            }

            return facebookAccountLists;
        }

        public static AccountLocationEdit GetFacebookPageInformation(string facebookAccountId, string token)
        {
            AccountLocationEdit accountLocation = new AccountLocationEdit();

            FacebookPage facebookPage = FacebookAPI.GetFacebookPageInformation(facebookAccountId, token);
            if (facebookPage != null && String.IsNullOrEmpty(facebookPage.facebookResponse.message))
            {
                accountLocation.CityName = facebookPage.page.location.city;
                accountLocation.CountryName = facebookPage.page.location.country;
                accountLocation.DisplayName = facebookPage.page.name;
                accountLocation.Name = facebookPage.page.name;
                accountLocation.PhoneNumber = facebookPage.page.phone;
                accountLocation.StateAbbreviation = facebookPage.page.location.state;
                accountLocation.StreetAddress1 = facebookPage.page.location.street;
                accountLocation.WebsiteURL = facebookPage.page.website;
                accountLocation.PostalCode = facebookPage.page.location.zip;
                accountLocation.Success = true;
            }
            else
            {
	            if (facebookPage != null)
		            accountLocation.Message = facebookPage.facebookResponse.message;
            }

            return accountLocation;
        }

        public static string GetFacebookAccessToken(string code, long accountID, UserInfoDTO userInfo)
        {
            SocialConfigDTO socialConfigDto = ProviderFactory.Social.GetSocialConfig(userInfo, GetAccountsReseller(userInfo, accountID), SocialNetworkEnum.Facebook);

			//socialConfigDto.AppCallback = "http://72.148.59.48:82/oAuth/Callback/";
			//socialConfigDto.AppID = "306195176076789";
			//socialConfigDto.AppSecret = "564678eedf1f5f9b1f96d453e4555f4a";

            try
            {
                if (Util.GetLocalIPAddress() == "192.168.8.111")
                {
                    //this is likely Reynolds local dev machine
                    socialConfigDto.AppCallback = "http://70.88.98.242:22222/oAuth/Callback/";
                    //socialConfigDto.AppID = "1514743978747674";
                    //socialConfigDto.AppSecret = "547cf71bcfb0efee201ce825544dd087";
                    socialConfigDto.AppID = "436310969778694";
                    socialConfigDto.AppSecret = "cc7abf7140d0136792c3592f9272b1b4";
                }
            }
            catch (Exception)
            {
            }


            return FacebookAPI.GetFacebookAccessToken(socialConfigDto.AppID, socialConfigDto.AppCallback, socialConfigDto.AppSecret, code);
        }

        public static SocialLoginUrl GetSocialLoginUrl(UserInfoDTO userInfo, long resellerID, SocialNetworkEnum network, string StateContext)
        {
            SocialLoginUrl socialLoginUrl = new SocialLoginUrl();

            SocialConfigDTO socialConfigDto = ProviderFactory.Social.GetSocialConfig(userInfo, resellerID, network);

            string authUrl = string.Empty;

            if (socialConfigDto != null)
            {
                switch (network)
                {
                    case SocialNetworkEnum.Facebook:
						//socialConfigDto.AppCallback = "http://72.148.59.48:82/oAuth/Callback/";
						//socialConfigDto.AppID = "306195176076789";

                        try
                        {
                            if (Util.GetLocalIPAddress() == "192.168.8.111")
                            {
                                //this is likely Reynolds local dev machine
                                socialConfigDto.AppCallback = "http://70.88.98.242:22222/oAuth/Callback/";
                                //socialConfigDto.AppID = "1514743978747674";
                                //socialConfigDto.AppSecret = "547cf71bcfb0efee201ce825544dd087";
                                socialConfigDto.AppID = "436310969778694";
                                socialConfigDto.AppSecret = "cc7abf7140d0136792c3592f9272b1b4";
                            }
                        }
                        catch (Exception)
                        {                
                        }

                        authUrl = string.Format(FacebookAPI.FacebookGraphAPIURLLogin, socialConfigDto.AppID, socialConfigDto.AppCallback, socialConfigDto.AppPermissions, StateContext);
                        break;
                    case SocialNetworkEnum.Google:
                    case SocialNetworkEnum.YouTube:
                        //socialConfigDto.AppPermissions = "https://www.googleapis.com/auth/userinfo.profile https://www.googleapis.com/auth/userinfo.email https://www.googleapis.com/auth/plus.pages.manage https://www.googleapis.com/auth/plus.circles.read https://www.googleapis.com/auth/plus.circles.write https://www.googleapis.com/auth/plus.stream.read https://www.googleapis.com/auth/plus.stream.write https://www.googleapis.com/auth/plus.media.readwrite https://www.googleapis.com/auth/plus.media.writeonly https://www.googleapis.com/auth/plus.media.upload";

                        //socialConfigDto.AppCallback = "http://localhost:99/oAuth/Callback";
                        //socialConfigDto.AppID = "257710766138-8nihqoelpdpl47l7324aorc3hcn3epr0.apps.googleusercontent.com";
                        //socialConfigDto.AppSecret = "YJ6qlgkKiUlszIKQ2Q41hSUo";
						

                        authUrl = string.Format(GoogleAPI.GoogleAPIAuthURL, socialConfigDto.AppCallback, socialConfigDto.AppID, socialConfigDto.AppPermissions, StateContext);
                        break;
                    case SocialNetworkEnum.Twitter:
                        authUrl = TwitterAPI.GetAuthUrl(socialConfigDto.AppID, socialConfigDto.AppSecret, string.Format("{0}?context={1}", socialConfigDto.AppCallback, StateContext));
                        break;
                }

                socialLoginUrl.SocialAppID = socialConfigDto.SocialAppID;
                socialLoginUrl.LoginUrl = authUrl;
            }

            return socialLoginUrl;
        }


        public static GoogleTokens GetGoogleAccessToken(string code, long accountId, UserInfoDTO userInfo)
        {
            GoogleTokens tokens = new GoogleTokens();

            GoogleToken googleToken = AdminService.GetGoogleTokenFromCode(userInfo, code, GetAccountsReseller(userInfo, accountId));
            if (googleToken != null)
            {
				
				tokens.Token = googleToken.access_token;
                tokens.RefreshToken = googleToken.refresh_token;
                tokens.TokenType = googleToken.token_type;

                return tokens;
            }

            return null;
        }

        public static GoogleToken GetGoogleTokenFromCode(UserInfoDTO userInfo, string code, long resellerID)
        {
            SocialConfigDTO socialConfigDto = ProviderFactory.Social.GetSocialConfig(userInfo, resellerID, SocialNetworkEnum.Google);

			//TODO REMOVE REMOVE REMOVE
            //socialConfigDto.AppCallback = "http://localhost:99/oAuth/Callback";
            //socialConfigDto.AppID = "257710766138-8nihqoelpdpl47l7324aorc3hcn3epr0.apps.googleusercontent.com";
            //socialConfigDto.AppSecret = "YJ6qlgkKiUlszIKQ2Q41hSUo";

            GoogleToken googleToken = GoogleAPI.GetGoogleTokenFromCode(socialConfigDto.AppCallback, socialConfigDto.AppID, socialConfigDto.AppSecret, code);

			if (!string.IsNullOrEmpty(googleToken.refresh_token))
			{
				GooglePlusAccessTokenRequestResponse requestResponse = GoogleAPI.GetGooglePlusAccessToken(socialConfigDto.AppID, socialConfigDto.AppSecret, googleToken.refresh_token, "refresh_token");

				if (!string.IsNullOrEmpty(requestResponse.access_token))
					googleToken.access_token = requestResponse.access_token;
			}

            return googleToken;

        }

        public static string GetGoogleUserID(string accessToken, string tokenType = null)
        {
            return GoogleAPI.GetGoogleUserInfoID(accessToken, tokenType);
        }

        public static List<GooglePageList> GooglePagesList(string token, string tokenType)
        {
            List<GooglePageList> googlePageList = new List<GooglePageList>();

            string userID = AdminService.GetGoogleUserID(token, tokenType);
            if (!string.IsNullOrEmpty(userID))
            {
                GooglePagesForUser googlePages = GoogleAPI.GetGooglePagesForUser(userID, token, tokenType);
                if (googlePages != null && googlePages.totalItems > 0)
                {
                    foreach (GooglePagesForUser.Item item in googlePages.items)
                    {
                        GooglePageList page = new GooglePageList();
                        page.PageID = item.id;
                        page.Name = item.displayName;
                        page.Website = item.url;
                        page.ImageUrl = item.image.url;

                        googlePageList.Add(page);
                    }
                }
            }

            return googlePageList;
        }

        public static List<YouTubePageList> YouTubeChannelsList(string token, string tokenType)
        {
            List<YouTubePageList> youTubePageList = new List<YouTubePageList>();

            string json = GoogleAPI.GetYouTubeChannels(token, tokenType);

            return youTubePageList;
        }

        public static long GetAccountsReseller(UserInfoDTO userInfo, long accountId)
        {
            GetEntityDTO<AccountDTO, AccountOptionsDTO> accountDto = ProviderFactory.Security.GetAccountByID(userInfo, accountId);
            return accountDto.Entity.ResellerForThisAccount.ID.GetValueOrDefault();
        }

        public static bool DisconnectSocialConnection(UserInfoDTO userInfo, long credentialID, long accountId)
        {
            return ProviderFactory.Social.DisconnectionSocialConnection(userInfo, credentialID, accountId);
        }

        public static bool RefreshSocialConnection(UserInfoDTO userInfo, long credentialID, long accountID)
        {
            SocialRefreshDTO socialRefreshDto = ProviderFactory.Social.GetSocialRefresh(userInfo, credentialID, accountID);
            if (socialRefreshDto != null)
            {
                if (socialRefreshDto.NetworkName == "google" || socialRefreshDto.NetworkName == "youtube")
                {
                    GooglePlusAccessTokenRequestResponse response = GoogleAPI.GetGooglePlusAccessToken(socialRefreshDto.AppID, socialRefreshDto.AppSecret, socialRefreshDto.RefreshToken, "refresh_token");
                    if (string.IsNullOrEmpty(response.errorResponse.message))
                    {
                        return ProviderFactory.Social.UpdateValidCredential(userInfo, credentialID, accountID);
                    }
                }
                else if (socialRefreshDto.NetworkName == "twitter")
                {
                    if (TwitterAPI.ValidateToken(socialRefreshDto.RefreshToken, socialRefreshDto.TokenSecret, socialRefreshDto.AppID, socialRefreshDto.AppSecret))
                        return ProviderFactory.Social.UpdateValidCredential(userInfo, credentialID, accountID);
                }
                else if (socialRefreshDto.NetworkName == "facebook")
                {
                    FacebookAccount accounts = FacebookAPI.GetFacebookAccounts(socialRefreshDto.RefreshToken);
                    if (accounts != null && accounts.data.Any())
                        return ProviderFactory.Social.UpdateValidCredential(userInfo, credentialID, accountID);
                }
            }

            return false;
        }


        public static bool SaveTwitterSocialConnection(string authCode, long configId, long accountId, long socialappid, UserInfoDTO userInfo)
        {
            if (!string.IsNullOrEmpty(authCode))
            {
                string[] tokens = authCode.Split(',');

                SocialConfigDTO socialConfigDto = ProviderFactory.Social.GetSocialConfig(userInfo, GetAccountsReseller(userInfo, accountId), SocialNetworkEnum.Twitter);
                if (socialConfigDto != null)
                {
                    TwitterOAuthResponse response = TwitterAPI.GetTwitterOAuthTokens(socialConfigDto.AppID, socialConfigDto.AppSecret, tokens[0], tokens[1]);
                    if (response.success)
                    {
                        string website = string.Format("http://Twitter.com/{0}", response.screenName);

                        AuditSocialConfig auditSocialConfig = new AuditSocialConfig();
                        auditSocialConfig.SocialAppId = socialappid;
                        auditSocialConfig.Token = response.token;
                        auditSocialConfig.Website = website;
                        ProviderFactory.Logging.Audit(AuditLevelEnum.Information, AuditTypeEnum.SocialConfiguration, "Social Config Save Twitter", auditSocialConfig, userInfo, accountId, configId, null, null);

                        //return ProviderFactory.Social.SaveSocialConnection(userInfo, configId, accountId, socialappid, response.token, response.tokenSecret, response.userID, website, response.pictureURL, response.screenName);

                        SocialConnectionInfoDTO dto = new SocialConnectionInfoDTO()
                        {
                            AccountID = accountId,
                            SocialNetwork = SocialNetworkEnum.Twitter,
                            PictureURL = response.pictureURL,
                            ScreenName = response.screenName,
                            Token = response.token,
                            TokenSecret = response.tokenSecret,
                            URI = website,
                            UniqueID = response.userID,
							SocialAppID = socialappid
                        };
                        SaveEntityDTO<SocialConnectionInfoDTO> result = ProviderFactory.Social.Save(new SaveEntityDTO<SocialConnectionInfoDTO>(dto, userInfo));
                        return (!result.Problems.Any());
                    }
                }
            }

            return false;
        }


        public static bool SaveSocialConnectionConfig(long configId, long accountID, long socialAppId, string pageID, string token, string website, UserInfoDTO userInfo, string network)
        {
            //get token back
            string pageToken = "";

            string websiteUrl = website;
            string pictureURL = "";
            string screenName = "";

            if (network == "facebook")
            {
                FacebookAccount accounts = FacebookAPI.GetFacebookAccounts(token);
                if (accounts != null)
                {
                    pageToken = accounts.data.Where(t => t.id == pageID).Select(t => t.access_token).FirstOrDefault();
                }
                websiteUrl = string.Format("http://www.facebook.com/home.php?#!/profile.php?id={0}", pageID);

                FacebookPage facebookPage = FacebookAPI.GetFacebookPageInformation(pageID, token);
                FaceBookUserPicture picture = FacebookAPI.FaceBookUserPicture(pageID);
                if (facebookPage != null)
                {
                    pictureURL = picture.picture.data.url;
                    screenName = facebookPage.page.name;
                }
            }

            if (string.IsNullOrEmpty(pageToken))
                pageToken = token;

            AuditSocialConfig auditSocialConfig = new AuditSocialConfig();
            auditSocialConfig.SocialAppId = socialAppId;
            auditSocialConfig.PageID = pageID;
            auditSocialConfig.Token = pageToken;
            auditSocialConfig.Website = websiteUrl;

            ProviderFactory.Logging.Audit(AuditLevelEnum.Information, AuditTypeEnum.SocialConfiguration, "Social Config Save", auditSocialConfig, userInfo, accountID, configId, null, null);

            SocialConnectionInfoDTO dto = new SocialConnectionInfoDTO()
            {
                AccountID = accountID,
                SocialNetwork = SocialNetworkEnum.Facebook,
                PictureURL = pictureURL,
                ScreenName = screenName,
                Token = pageToken,
                TokenSecret = string.Empty,
				URI = websiteUrl,
                UniqueID = pageID,
				SocialAppID = socialAppId
            };
            SaveEntityDTO<SocialConnectionInfoDTO> result = ProviderFactory.Social.Save(new SaveEntityDTO<SocialConnectionInfoDTO>(dto, userInfo));
            return (!result.Problems.Any());

            //return ProviderFactory.Social.SaveSocialConnection(userInfo, configId, accountID, socialAppId, pageToken, "", pageID, websiteUrl, pictureURL, screenName);
        }

        public static bool SaveGoogleConnectionConfig(long credentialID, long accountID, long socialAppId, string pageID, GoogleTokens googleTempTokens, string website, UserInfoDTO userInfo)
        {
            string websiteUrl = string.Format("https://plus.google.com/{0}/posts", pageID);
            string pictureURL = "";
            string screenName = "";

            string userID = AdminService.GetGoogleUserID(googleTempTokens.Token, googleTempTokens.TokenType);
            if (!string.IsNullOrEmpty(userID))
            {
                GooglePagesForUser googlePages = GoogleAPI.GetGooglePagesForUser(userID, googleTempTokens.Token, googleTempTokens.TokenType);
                if (googlePages != null && googlePages.totalItems > 0)
                {
                    GooglePagesForUser.Item item = googlePages.items.Where(i => i.id == pageID).FirstOrDefault();
                    if (item != null)
                    {
                        pictureURL = item.image.url;
                        screenName = item.displayName;
                    }
                }
            }

            AuditSocialConfig auditSocialConfig = new AuditSocialConfig();
            auditSocialConfig.SocialAppId = socialAppId;
            auditSocialConfig.PageID = pageID;
            auditSocialConfig.Token = googleTempTokens.RefreshToken;
            auditSocialConfig.Website = websiteUrl;

            ProviderFactory.Logging.Audit(AuditLevelEnum.Information, AuditTypeEnum.SocialConfiguration, "Social Config Save Google", auditSocialConfig, userInfo, accountID, credentialID, null, null);

            SocialConnectionInfoDTO dto = new SocialConnectionInfoDTO()
            {
                AccountID = accountID,
                SocialNetwork = SocialNetworkEnum.Google,
                PictureURL = pictureURL,
                ScreenName = screenName,
                Token = googleTempTokens.RefreshToken,
                TokenSecret = string.Empty,
				URI = websiteUrl,
                UniqueID = pageID,
				SocialAppID = socialAppId
            };
            SaveEntityDTO<SocialConnectionInfoDTO> result = ProviderFactory.Social.Save(new SaveEntityDTO<SocialConnectionInfoDTO>(dto, userInfo));
            return (!result.Problems.Any());

            //return ProviderFactory.Social.SaveSocialConnection(userInfo, credentialID, accountID, socialAppId, googleTempTokens.RefreshToken, "", pageID, websiteUrl, pictureURL, screenName);
        }

        public static List<SocialNetworks> NetworkList()
        {
            List<SocialNetworkDTO> socialNetworkDtos = ProviderFactory.Social.GetSocialNetworks();
            List<SocialNetworks> socialNetworks = socialNetworkDtos.Select(n => new SocialNetworks { ID = n.ID, Name = n.Name.ToTitleCase() }).ToList();

            return socialNetworks;
        }

        public static SaveEntityResult ReputationConfigAutoPopulate(UserInfoDTO userInfo, long accountID)
        {
            var reviewHarvester = new ReviewHarvester();
            SaveEntityResult saveEntityResult = new SaveEntityResult();

            AccountLocationEdit result = AccountLocationEdit(userInfo, accountID);
            List<ReviewURL> URLs = new List<ReviewURL>();

            if (result != null)
            {

                URLs = reviewHarvester.IdentifyLocationUrls((result.DisplayName == null) ? result.Name : result.DisplayName, result.CityName, result.StateAbbreviation, result.PostalCode, true);

                Dictionary<string, string> linksDict = new Dictionary<string, string>();
                foreach (var url in URLs)
                {
                    if (!linksDict.Keys.Contains(url.ReviewSourceName))
                    {
                        if (!string.IsNullOrWhiteSpace(url.URL))
                            linksDict.Add(url.ReviewSourceName + "_0", url.URL);
                    }
                }

                saveEntityResult = ReputationConfigSave(userInfo, linksDict, accountID, false);

            }

            return saveEntityResult;
        }

        public static string ReputationConfigAutoPopulate(UserInfoDTO userInfo, long accountID, string source)
        {
            var reviewHarvester = new ReviewHarvester();

            source = source.StripWhiteSpace();
            if (source == "Cars.com")
                source = "CarsDotCom";
            if (source == "Judy'sBook")
                source = "JudysBook";
            if (source == "CarDealerReviews")
                source = "CarDealer";

            ReviewSourceEnum reviewSource = (ReviewSourceEnum)Enum.Parse(typeof(ReviewSourceEnum), source);

            AccountLocationEdit result = AccountLocationEdit(userInfo, accountID);
            string URL = string.Empty;

            if (result != null)
            {
                URL = reviewHarvester.IdentifyLocationUrl(reviewSource, (result.DisplayName == null) ? result.Name : result.DisplayName, result.CityName, result.StateAbbreviation, result.PostalCode, true);
            }

            return URL;
        }

        public static bool ImpersonateUser(UserInfoDTO userInfo, long userIDToImpersonate)
        {
            if (ProviderFactory.Security.CanImpersonateUser(userInfo, userIDToImpersonate))
            {
                UserInfoDTO iUser = new UserInfoDTO(userInfo.LoggedInUser.ID.Value, userIDToImpersonate);
                FormsAuthentication.SetAuthCookie(iUser.ToAuthCookie(), false);
                UserInfoDTO iUser2 = ProviderFactory.Security.ValidateUser(iUser).UserInfo;
				Auditor
					.New(AuditLevelEnum.Information, AuditTypeEnum.UserActivity, iUser2, "[{0}] has begun impersonating [{1}]", iUser2.LoggedInUser.Username, iUser2.ImpersonatingUser.Username)
					.SetAuditDataObject(
						AuditUserActivity
							.New(iUser2, AuditUserActivityTypeEnum.SignedIn)
							.SetDescription("[{0}] has begun impersonating [{1}]", iUser2.LoggedInUser.Username, iUser2.ImpersonatingUser.Username)
					)
					.Save(ProviderFactory.Logging)
				;
                return true;
            }

            return false;
        }

        public static void StopImpersonation(UserInfoDTO userInfo)
        {
            if (userInfo.ImpersonatingUser != null)
            {
                UserInfoDTO userInfo2 = new UserInfoDTO(userInfo.LoggedInUser.ID.Value, null);
                FormsAuthentication.SetAuthCookie(userInfo2.ToAuthCookie(), false);

                Auditor
                    .New(AuditLevelEnum.Information, AuditTypeEnum.UserActivity, userInfo, "[{0}] stopped impersonating [{1}]", userInfo.LoggedInUser.Username, userInfo.ImpersonatingUser.Username)
                    .SetAuditDataObject(
                        AuditUserActivity
                            .New(userInfo, AuditUserActivityTypeEnum.SignedOut)
                            .SetDescription("[{0}] stopped impersonating [{1}]", userInfo.LoggedInUser.Username, userInfo.ImpersonatingUser.Username)
                    )
                    .Save(ProviderFactory.Logging)
                ;
            }
        }

        public static bool ValidateUser(string userName, string password, bool rememberMe, string domain, string userAgent)
        {
            ValidateUserResultDTO vuResult = ProviderFactory.Security.ValidateUser(userName, password);

            if (vuResult.Outcome == ValidateUserOutcome.Success)
            {
                FormsAuthentication.SetAuthCookie(vuResult.UserInfo.ToAuthCookie(), rememberMe);
                if (rememberMe)
                    FormsAuthentication.GetAuthCookie(vuResult.UserInfo.ToAuthCookie(), rememberMe).Expires.AddDays(7);

                Auditor
                    .New(AuditLevelEnum.Information, AuditTypeEnum.UserActivity, vuResult.UserInfo, "[{0}] Signed In to Domain [{1}]", vuResult.UserInfo.EffectiveUser.Username, domain)
                    .SetAuditDataObject(
                        AuditUserActivity
                            .New(vuResult.UserInfo, AuditUserActivityTypeEnum.SignedIn)
                            .SetDescription("[{0}] Signed In to Domain [{1}]", vuResult.UserInfo.EffectiveUser.Username, domain)
							.SetUserAgent(userAgent)
                    )
                    .Save(ProviderFactory.Logging)
                ;

                return true;
            }

            return false;
        }

        public static void UserSignedOut(UserInfoDTO user, string domain)
        {
            Auditor
                .New(AuditLevelEnum.Information, AuditTypeEnum.UserActivity, user,
                    "[{0}] Signed Out from Domain [{1}]", user.EffectiveUser.Username, domain
                )
                .SetAuditDataObject(
                    AuditUserActivity
                        .New(user, AuditUserActivityTypeEnum.SignedOut)
                        .SetDescription("[{0}] Signed Out from Domain [{1}]", user.EffectiveUser.Username, domain)
                )
                .Save(ProviderFactory.Logging)
            ;
        }

        public static List<ExceptionLog> ExceptionLogs(PagedGridRequest pagedGridRequest, ref int totalCount)
        {
            List<ExceptionLog> exceptionLogs = new List<ExceptionLog>();

            int TotalCount = 0;
            List<ExceptionLogDTO> exceptionLogDtos = ProviderFactory.Logging.GetExceptions(pagedGridRequest.Skip, pagedGridRequest.Take, ref TotalCount);
            if (exceptionLogDtos != null)
            {
                foreach (ExceptionLogDTO dto in exceptionLogDtos)
                {
                    ExceptionLog log = new ExceptionLog();
                    log.ID = dto.ID;
                    log.AssemblyName = dto.AssemblyName;
                    log.Class = dto.Class;
                    log.DateCreated = dto.DateCreated.ToLocalTime();
                    log.FullException = dto.FullException;
                    log.LineNumber = dto.LineNumber;
                    log.Message = dto.Message;
                    log.Method = dto.Method;
                    log.URL = dto.URL;

                    exceptionLogs.Add(log);
                }

                totalCount = TotalCount;
            }

            return exceptionLogs;

        }


        public static void FlushAccountAndPermissionCache()
        {
            ProviderFactory.Security.FlushAccountAndPermissionCache();
        }

	    public static bool AccountSettingsEdit(UserInfoDTO userInfo, AccountSettings accountSettings, ref List<string> problems)
	    {
		    bool Success = false;

			GetEntityDTO<AccountDTO, AccountOptionsDTO> getAccount;
			getAccount = ProviderFactory.Security.GetAccountByID(userInfo, accountSettings.AccountID);

			if (getAccount != null && !getAccount.HasProblems)
			{
				AccountDTO accountDto = getAccount.Entity;
				accountDto.PostsRequireApproval = accountSettings.PostRequireApproval;

				SaveEntityDTO<AccountDTO> saveEntity = ProviderFactory.Security.Save(new SaveEntityDTO<AccountDTO>(userInfo) { Entity = accountDto });

				if (saveEntity.Problems.Any())
				{
					problems.AddRange(saveEntity.Problems);
				}
				else
					Success = true;
			}
			else
			{
				problems.Add("Can't find existing account.");
			}

			return Success;

	    }


	    public static UserDetailEmailedReports GetReportTemplateAndEmailReport(long userId)
	    {
		    UserDetailEmailedReports emailedReports = new UserDetailEmailedReports();
		    emailedReports.UserID = userId;

		    ReportTemplateandEmailReportDTO dto = ProviderFactory.Report.GetReportTemplateAndEmailReport(userId);

			foreach (EmailReportTemplateDTO temp in dto.emailReportTemplates)
			{
				UserDetailsReportConfig config = new UserDetailsReportConfig();
				config.Name = temp.ReportName;
				config.ReportSubscriptionID = temp.ReportSubscriptionID.GetValueOrDefault();
				config.ReportTemplateID = temp.ReportTemplateID;
				config.ScheduleSpec = temp.ScheduleSpec;
				config.Enabled = temp.Status.GetValueOrDefault();
				config.UserID = userId;
				
				emailedReports.ReportConfigs.Add(config);
			}

			foreach (EmailedReportLogDTO log in dto.emailedReportLogs)
			{
				UserDetailsRecentReports report = new UserDetailsRecentReports();
				report.Name = log.Subject;
				report.DateNotified = log.DateSent.GetValueOrDefault();
				report.ReportLogID = log.ReportLogID;
				report.UserID = userId;

				emailedReports.RecentReports.Add(report);
			}

		    return emailedReports;
	    }

	    public static string GetReportLogByID(long reportLogId)
	    {
		    return ProviderFactory.Report.GetReportLogByID(reportLogId);
	    }

	    public static bool SetReportSubscriptions(UserInfoDTO userInfo, long? templateId, string spec, long? subscriptionId, bool enabled)
	    {
		    //return ProviderFactory.Report.SetReportSubscriptions(userInfo, templateId.GetValueOrDefault(), spec, subscriptionId.GetValueOrDefault(), !enabled);
            return true;
	    }

	    public static UserDetailsNotifications GetUserDetailsNotifications(long? userId)
	    {
		    UserDetailsNotifications detailsNotifications = new UserDetailsNotifications();
		    detailsNotifications.UserID = userId.GetValueOrDefault();

		    List<UserNotificationTypeInfoDTO> notifications = ProviderFactory.Notifications.GetAvailableNotificationsForUser(userId.GetValueOrDefault());
			foreach (UserNotificationTypeInfoDTO notification in notifications.OrderBy(o => o.TypeName))
			{
				UserDetailsNotificationConfig config = new UserDetailsNotificationConfig();
				config.UserID = notification.UserID;
				config.Name = notification.TypeName;
				config.CanSubscribe = notification.CanSubscribe;
				config.Enabled = notification.AccountsSubscribedTo > 0;
				config.Type = notification.Type.ToString();
				config.Method = notification.Method.ToString();
				
				detailsNotifications.NotificationConfigs.Add(config);
			}

		    List<RecentNotificationItemDTO> recentNotifications = ProviderFactory.Notifications.GetMostRecentEmailNotificationsForUser(userId.GetValueOrDefault(), 250);
			foreach (RecentNotificationItemDTO recentNoti in recentNotifications)
			{
				UserDetailsRecentNotifications recent = new UserDetailsRecentNotifications();
				recent.UserID = userId.GetValueOrDefault();
				recent.Name = recentNoti.Subject;
				recent.DateNotified = recentNoti.DateSent.LocalDate.GetValueOrDefault();
				recent.NotificationTargetID = recentNoti.NotificationTargetID;

				detailsNotifications.RecentNotifications.Add(recent);
			}

		    return detailsNotifications;
	    }

	    public static string GetBodyTextForNotificationTargetID(long notificationId)
	    {
		    return ProviderFactory.Notifications.GetBodyTextForNotificationTargetID(notificationId);
	    }

	    public static bool SetUserNotificationStates(UserInfoDTO userInfo, long? userId, string type, string method, bool? enabled)
	    {
		    SetNotificationStateDTO setNotification = new SetNotificationStateDTO();
		    setNotification.Enabled = enabled.GetValueOrDefault();
		    setNotification.Method = (NotificationMethodEnum) Enum.Parse(typeof(NotificationMethodEnum), method);
		    setNotification.Type = (NotificationMessageTypeEnum) Enum.Parse(typeof(NotificationMessageTypeEnum), type);

		    List<SetNotificationStateDTO> stateDto = new List<SetNotificationStateDTO>();
		    stateDto.Add(setNotification);
            
			ProviderFactory.Notifications.SetUserNotificationStates(userInfo, userId.GetValueOrDefault(), stateDto);
		    return true; 

	    }

	    public static bool ChangeUserMemberOfAccount(UserInfoDTO userInfo, long userID, long accountID)
	    {
		    ChangeUsersMemberAccountResultDTO dto = ProviderFactory.Security.ChangeUserMemberOfAccount(userInfo, userID, accountID);

		    return dto.Success;
	    }

		public static string GetUserPasswordResetLink(long userID)
	    {
			AccessTokenBaseDTO accessToken = new PasswordResetAccessTokenDTO("");
			accessToken.TimeTillExpiration = new TimeSpan(1, 0, 0, 0);
			accessToken.UserID = userID;
			accessToken.RemainingUses = 1;

			AccessTokenBaseDTO token = ProviderFactory.Security.CreateAccessToken(accessToken);
			return string.Format("{0}/access/{1}", Settings.GetSetting("site.baseurl", "socialintegration.com"), token.Token.ToString());

	    }

		public static List<UserDetailActivityList> GetUserActivity(long? userID, UserInfoDTO userInfo)
		{
			List<UserDetailActivityList> activity = new List<UserDetailActivityList>();

			List<UserActivityDTO> activityDTO = ProviderFactory.Security.GetUserActivityByUserID(userID.GetValueOrDefault(), 60, userInfo);
			foreach(UserActivityDTO dto in activityDTO)
			{
				UserDetailActivityList act = new UserDetailActivityList();
				act.AccountName = dto.AccountName;
				act.ActivityDate = dto.ActivityDate.LocalDate.Value; 
				act.Activity = dto.Activity;

				activity.Add(act);
			}

			return activity;
		}
    }


}
