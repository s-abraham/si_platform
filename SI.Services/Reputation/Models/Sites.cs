﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SI.Services.Reputation.Models
{
	public class Sites
	{
		public long SiteID { get; set; }
		public string SiteName { get; set; }
		public string SiteImageUrl { get; set; }
		public string SiteNameFilter { get; set; }
		public decimal AverageReviewScore { get; set; }
		public int NumberOfReviews { get; set; }
		public string NumberofReviewsFilter { get; set; }
		public int PositiveReviews { get; set; }
		public string PositiveReviewsFilter { get; set; }
		public int NegativeReviews { get; set; }
		public string NegativeReviewsFilter { get; set; }
		public int FilteredTotal { get; set; } //used for sorting
		public string FilteredTotalFilter { get; set; }
		public int FilteredPositive { get; set; }
		public string FilteredPositiveFilter { get; set; }
		public int FilteredNegative { get; set; }
		public string FilteredNegativeFilter { get; set; }
		public DateTime? LastReview { get; set; }
		public int UnReadTotal { get; set; } //used for sorting
		public string UnReadTotalFilter { get; set; }
		public int UnReadPositive { get; set; }
		public string UnReadPositiveFilter { get; set; }
		public int UnReadNegative { get; set; }
		public string UnReadNegativeFilter { get; set; }
		public int UnRespondTotal { get; set; } //used for sorting
		public string UnRespondTotalFilter { get; set; }
		public int UnRespondPositive { get; set; }
		public string UnRespondPositiveFilter { get; set; }
		public int UnRespondNegative { get; set; }
		public string UnRespondNegativeFilter { get; set; }

		public Location DealerLocation { get; set; } //for filtering
		public class Location
		{
			public int LocationID { get; set; }
		}
		
		public Sites()
		{
			DealerLocation = new Location();
		}

	}
}
