﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SI.Services.Reputation.Models
{
	public class CompetitiveAnalysis
	{
		public string MyScore { get; set; }
		public string CompetitorsScore { get; set; }
		public CAGroupAverages MyLocationGroupAvg { get; set; }
		public CAGroupAverages MyGroupsAvg { get; set; }
		public CAGroupAverages CompetitorsGroupAvg { get; set; }

		public List<CALocationAverages> LocationAverages { get; set; }

		public CompetitiveAnalysis()
		{
			LocationAverages = new List<CALocationAverages>();
			MyLocationGroupAvg = new CAGroupAverages();
			MyGroupsAvg = new CAGroupAverages();
			CompetitorsGroupAvg = new CAGroupAverages();
		}
	}

	public class CAGroupAverages
	{
		public string Name { get; set; }
		public CAGroupReviewCount CurrentCounts { get; set; }
		public CAGroupReviewCount Days30Counts { get; set; }

		public CAGroupAverages()
		{
			CurrentCounts = new CAGroupReviewCount();
			Days30Counts = new CAGroupReviewCount();
		}
	}

	public class CALocationAverages
	{
		public string Name { get; set; }
		public CAGroupReviewCount CurrentCounts { get; set; }
		public CAGroupReviewCount Last24Counts { get; set; }
		public CAGroupReviewCount Last7DaysCounts { get; set; }
		public CAGroupReviewCount Last30DaysCounts { get; set; }

		public CALocationAverages()
		{
			CurrentCounts = new CAGroupReviewCount();
			Last24Counts = new CAGroupReviewCount();
			Last7DaysCounts = new CAGroupReviewCount();
			Last30DaysCounts = new CAGroupReviewCount();
		}
	}

	public class CAGroupReviewCount
	{
		public string AvgScore { get; set; }
		public string Count { get; set; }
		public string PositiveCount { get; set; }
		public string NegativeCount { get; set; }
	}
}
