﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace SI.Services.Reputation.Models
{
	public class Stream
	{
		public long ReviewID { get; set; }
		
		[DataType(DataType.Date)]
		public DateTime ReviewDate { get; set; }        
		[DataType(DataType.Date)]
		public DateTime CollectedDate { get; set; }
		public Location DealerLocation { get; set; }
        public long SiteID { get; set; }
		public string SiteName { get; set; }
		public string SiteImageUrl { get; set; }
		public double Rating { get; set; }
		public string ReviewerName { get; set; }
		public string ReviewText { get; set; }
		public string ReviewUrl { get; set; }

		public bool HasBeenRead { get; set; }
		public bool HasBeenResponded { get; set; }
		public bool IsIgnored { get; set; }

		public bool IsFiltered { get; set; }
		public string Category { get; set; }

        public DateTime? ReadDate { get; set; }
        public DateTime? RespondedDate { get; set; }
        public DateTime? IgnoredDate { get; set; }

		public Stream()
		{
			DealerLocation = new Location();
		}

		public class Location
		{
			public long LocationID { get; set; }
			public string Name { get; set; }
			public string City { get; set; }
			public string StateAbrev { get; set; }
            public long StateID { get; set; }
			public string Zipcode { get; set; }
            public long OemID { get; set; }
			public string SiteUrl { get; set; }
		}
		
	}


   
}
