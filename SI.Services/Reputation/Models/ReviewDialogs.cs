﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SI.Services.Reputation.Models
{
	public class ReviewEmail
	{
		public long ReviewID { get; set; }
		
		[Required]
		[DataType(DataType.EmailAddress)]
		[Display(Name = "To Email")]
		public string ToEmail { get; set; }
		
		[DataType(DataType.EmailAddress)]
		[Display(Name = "From Email")]
		public string FromEmail { get; set; }

		[Required]
		public string Subject { get; set; }
		public string Comments { get; set; }
		public string ReviewDetails { get; set; }

		public List<AccountUser> AccountUsers { get; set; }

		public ReviewEmail()
		{
			AccountUsers = new List<AccountUser>();
		}

		public class AccountUser
		{
			public long UserID { get; set; }
			public string UserName { get; set; }
			public string Email { get; set; }
		}
	}

	
}
