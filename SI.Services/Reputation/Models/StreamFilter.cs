﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SI.Services.Reputation.Models
{
	public class StreamFilter
	{
		public string ID { get; set; }
		public string Name { get; set; }
		public List<FilterLocation> Locations { get; set; }
		public FilterRating Rating { get; set; }
		public FilterContent Content { get; set; }
		public FilterDates Dates { get; set; }
		public List<string> Sites { get; set; }
		public string Category { get; set; }
		public List<string> States { get; set; }
		public List<string> Oem { get; set; }
		public List<string> Status { get; set; }

		public StreamFilter()
		{
			Locations = new List<FilterLocation>();
			Sites = new List<string>();
			States = new List<string>();
			Oem = new List<string>();
			Status = new List<string>();
		}
	}

	public class FilterLocation
	{
		public int VirtualGroupdID { get; set; } 
		public string VirtualGroup { get; set; }
		public int DealerID { get; set; }
		public string Dealer { get; set; }
		
	}

	public class FilterRating
	{
		public RatingCompare Compare { get; set; }
		public int Number { get; set; }

		public enum RatingCompare
		{
			LessThanOrEqualTo,
			GreatThanOrEqualTo,
			EqualTo,
			LessThan,
			GreatThan
		}
	}

	public class FilterContent
	{
		public ContentCompare Compare { get; set; }
		public string Phrase { get; set; }

		public enum ContentCompare
		{
			Contains,
			DoesNotContain
		}
	}
	
	public class FilterDates
	{
		public DatesCompare Compare { get; set; }
		public SIDateTime FromDate { get; set; }
		public SIDateTime ToDate { get; set; }

		public enum DatesCompare
		{
			After,
			Before,
			Between
		}
	}

	
	
}
