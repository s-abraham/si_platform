﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using Kendo.Mvc;
using SI.BLL;
using SI.DTO;
using SI.Extensions;
using SI.Services.Admin;
using SI.Services.Admin.Models;
using SI.Services.Dashboard.Models;
using SI.Services.Framework.Models;
using SI.Services.Reputation.Models;

namespace SI.Services.Reputation
{
    public static class ReputationService
    {
        public static ReputationOverview ReviewStreamHeader(UserInfoDTO userInfo)
        {
            ReputationOverview overview = new ReputationOverview();

            DashboardOverviewDTO dto = ProviderFactory.Reviews.GetDashboardOverviewData(userInfo);
            List<ReviewSourceDTO> reviewSources = ProviderFactory.Reviews.GetReviewSources(userInfo);


            overview.NumberOfDealers = dto.ReputationOverview.NumberOfDealers;
            overview.ReviewsAllTime.NumberOfReviews = dto.ReputationOverview.ReviewsAllTime.Total;
            overview.ReviewsAllTime.NumberOfReviewsFilter = "";
            overview.ReviewsAllTime.TotalNegative = dto.ReputationOverview.ReviewsAllTime.TotalNegative;
            overview.ReviewsAllTime.TotalNegativeFilter = "rating,lte,2.499";
            overview.ReviewsAllTime.TotalPositive = dto.ReputationOverview.ReviewsAllTime.TotalPositive;
            overview.ReviewsAllTime.TotalPositiveFilter = "rating,gte,2.5";

            overview.UnReadPostive = dto.ReputationOverview.ReviewsAllTime.TotalPositive;
            overview.UnReadPostiveFilter = "status,HasBeenRead,false-status,HasBeenResponded,false-rating,gte,2.5";

            overview.UnReadNegative = dto.ReputationOverview.ReviewsAllTime.TotalNegative;
            overview.UnReadNegativeFilter = "status,HasBeenRead,false-status,HasBeenResponded,false-rating,lte,2.499";

            overview.UnReadFromYesterday = 0;
            overview.UnReadFromYesterdayFilter = string.Format("status,HasBeenRead,false-status,HasBeenResponded,false-dates,after,{0}", DateTime.Now.AddDays(-1).ToShortDateString());

            overview.UnReadThisMonth = 0;
            overview.UnReadThisMonthFilter = string.Format("status,HasBeenRead,false-status,HasBeenResponded,false-dates,after,{0}", DateTime.Now.AddMonths(-1).ToShortDateString());

            overview.UnReadFilter = "status,HasBeenRead,false-status,HasBeenResponded,false";

            overview.ToDoesRead = 0;
            overview.ToDoesUnRead = 0;
            overview.ToDoesResponsed = 0;
            overview.ToDoesNoResponse = 0;

            int gridMaxCount = 0;
            List<string> siteNames = new List<string>();
            List<int> siteCounts = new List<int>();
            List<double> siteAverages = new List<double>();
            foreach (ReviewSourceEnum reviewSource in dto.ReputationOverview.ReviewCountsBySource.Keys)
            {
                ReviewCountsDTO counts = dto.ReputationOverview.ReviewCountsBySource[reviewSource];
                ReviewSourceDTO source = (from rs in reviewSources where rs.ID == (long)reviewSource select rs).FirstOrDefault();
                if (source != null)
                {
                    if (counts.Total > 0)
                    {
                        siteNames.Add(source.Name);
                        siteCounts.Add(counts.Total);
                        siteAverages.Add(counts.AverageRating);
                        if (counts.Total > gridMaxCount)
                            gridMaxCount = counts.Total;
                    }
                }
            }

            gridMaxCount = (int)(Convert.ToDouble(gridMaxCount) + (Convert.ToDouble(gridMaxCount) * .1));

            //TODO if rating is 0 Omit in arrays
            overview.GridMaxReviewCountPerSite = gridMaxCount;
            overview.ReviewCountPerSite = siteCounts.ToArray();
            overview.AverageRatingsPerSite = siteAverages.ToArray();
            overview.RatingSites = siteNames.ToArray();

            return overview;
        }


        public static List<Stream> ReviewStreamListGrid(UserInfoDTO userInfo, PagedGridRequest pagedGridRequest, ref int totalCount)
        {
            List<Stream> streams = new List<Stream>();

            ReviewStreamSearchRequestDTO req = new ReviewStreamSearchRequestDTO();

            foreach (PagedGridFilterDescriptor filter in pagedGridRequest.Filtering)
            {
                if (filter.Field == "ReviewText")
                {
                    if (filter.Operator == FilterOperator.Contains)
                        req.ReviewTextContains = filter.Value.ToLower(); //.Replace(" ", " AND ");
                    else if (filter.Operator == FilterOperator.DoesNotContain)
                        req.ReviewTextDoesNotContain = filter.Value.ToLower(); //.Replace(" ", " AND ");
                }

                if (filter.Field == "DealerLocation.StateID")
                    req.StateIDs.Add(filter.Value.ToLong());

                if (filter.Field == "SiteID")
                    req.ReviewSourcesIDs.Add(filter.Value.ToLong());

                if (filter.Field == "Rating")
                {
                    if (filter.Operator == FilterOperator.IsGreaterThanOrEqualTo)
                        req.RatingIsGreaterThanOrEqualTo = filter.Value.ToDouble();
                    else if (filter.Operator == FilterOperator.IsLessThanOrEqualTo)
                        req.RatingIsLessThanOrEqualTo = filter.Value.ToDouble();
                }

                if (filter.Field == "ReviewDate")
                {
                    if (filter.Operator == FilterOperator.IsGreaterThanOrEqualTo)
                    {
                        req.MinDate = filter.Value.ToDateTimeNullable();
                    }
                    else if (filter.Operator == FilterOperator.IsLessThanOrEqualTo)
                    {
                        req.MaxDate = filter.Value.ToDateTimeNullable();
                    }
                    else if (filter.Operator == FilterOperator.IsEqualTo)
                    {
                        req.MinDate = filter.Value.ToDateTimeNullable();
                        req.MaxDate = filter.Value.ToDateTimeNullable();
                    }
                    else if (filter.Operator == FilterOperator.IsGreaterThan)
                    {
                        req.MinDate = filter.Value.ToDateTimeNullable().Value.AddDays(1);
                    }
                    else if (filter.Operator == FilterOperator.IsLessThan)
                    {
                        req.MaxDate = filter.Value.ToDateTimeNullable().Value.AddDays(-1);
                    }
                }

                if (filter.Field == "DealerLocation.OemID")
                    req.FranchiseTypeIDs.Add(filter.Value.ToLong());

                if (filter.Field == "Category")
                    req.CategoryID = filter.Value.ToLong();

                if (filter.Field == "DealerLocation.LocationID")
                    req.AccountIDs.Add(filter.Value.ToLong());


                if (filter.Field == "IsFiltered")
                {
                    req.IncludeFiltered = true;
                    req.IncludeUnFiltered = false;
                }
                else
                {
                    req.IncludeFiltered = false;
                    req.IncludeUnFiltered = true;
                }

	            if (filter.Field == "HasBeenRead")
	            {
		            if (filter.Value == "true")
			            req.FilterRead = true;
		            else
		                req.FilterUnRead = true;
	            }

				if (filter.Field == "HasBeenResponded")
				{
					if (filter.Value == "true")
						req.FilterResponded = true;
					else
						req.FilterUnResponded = true;
				}

				if (filter.Field == "IsIgnored")
				{
					req.FilterIgnored = true;
				}
	           

            }

            if (pagedGridRequest.Sorting.Any())
            {
                PagedGridSortDescriptor sort = pagedGridRequest.Sorting.First(); //what abt multiple sorts?
                switch (sort.Field)
                {
                    case "ReviewDate": req.SortBy = ReviewStreamSearchSortEnum.ReviewDate; break;
                    case "CollectedDate": req.SortBy = ReviewStreamSearchSortEnum.CollectedDate; break;
                    case "Name": req.SortBy = ReviewStreamSearchSortEnum.AccountName; break;
                    case "SiteID": req.SortBy = ReviewStreamSearchSortEnum.ReviewSourceName; break;
                    case "Rating": req.SortBy = ReviewStreamSearchSortEnum.Rating; break;                    
                }

                req.SortAscending = sort.Ascending;
            }

            req.StartIndex = pagedGridRequest.Skip;
            req.EndIndex = req.StartIndex + pagedGridRequest.Take;

            ReviewStreamSearchResultDTO result = ProviderFactory.Reviews.GetReviewStreamData(userInfo, req);
            List<StreamDTO> streamDtos = result.Items;
            int TotalCount = result.TotalRecords;

            if (TotalCount > 0)
            {
                foreach (StreamDTO review in streamDtos)
                {
                    Stream stream = new Stream();
                    Stream.Location location = new Stream.Location();

                    stream.ReviewID = review.ReviewID;
                    stream.ReviewDate = review.ReviewDate.LocalDate.GetValueOrDefault();
                    stream.CollectedDate = review.CollectedDate.LocalDate.GetValueOrDefault();
                    stream.SiteID = review.SiteID;
                    stream.SiteName = review.SiteName;
                    stream.SiteImageUrl = review.SiteImageUrl;
                    stream.Rating = review.Rating;
                    stream.ReviewerName = review.ReviewerName;
                    stream.ReviewText = review.ReviewText;
                    stream.ReviewUrl = review.ReviewUrl;
                    stream.IsFiltered = review.IsFiltered;
                    stream.Category = review.Category;

                    if (review.ReadDate.LocalDate.GetValueOrDefault() != DateTime.MinValue)
                    {
                        stream.ReadDate = review.ReadDate.LocalDate.GetValueOrDefault();
                    }
                    if (review.RespondedDate.LocalDate.GetValueOrDefault() != DateTime.MinValue)
                    {
                        stream.RespondedDate = review.RespondedDate.LocalDate.GetValueOrDefault();
                    }
                    if (review.IgnoredDate.LocalDate.GetValueOrDefault() != DateTime.MinValue)
                    {
                        stream.IgnoredDate = review.IgnoredDate.LocalDate.GetValueOrDefault();
                    }
	                stream.HasBeenRead = stream.ReadDate != null ? stream.ReadDate.HasValue : false;
					stream.HasBeenResponded = stream.RespondedDate != null ? stream.RespondedDate.HasValue : false;
					stream.IsIgnored = stream.IgnoredDate != null ? stream.IgnoredDate.HasValue : false;

                    location.LocationID = review.DealerLocation.LocationID;
                    location.Name = review.DealerLocation.Name;
                    location.City = review.DealerLocation.City;
                    location.StateAbrev = review.DealerLocation.StateAbrev;
                    location.StateID = review.DealerLocation.StateID;
                    location.Zipcode = review.DealerLocation.Zipcode;
                    location.OemID = 0; //review.DealerLocation.OemID;
                    location.SiteUrl = review.DealerLocation.SiteUrl;

                    stream.DealerLocation = location;

                    streams.Add(stream);
                }

                totalCount = TotalCount;
            }

            return streams;
        }

        public static List<Sites> ReviewSitesGrid(UserInfoDTO userInfo, PagedGridRequest pagedGridRequest, string timeFrame)
        {
            List<Sites> sites = new List<Sites>();

            DashboardOverviewDTO dto = ProviderFactory.Reviews.GetDashboardOverviewData(userInfo);
            List<ReviewSourceDTO> reviewSources = ProviderFactory.Reviews.GetReviewSources(userInfo);

            foreach (ReviewSourceEnum reviewSource in dto.ReputationOverview.ReviewCountsBySource.Keys)
            {
                ReviewCountsDTO counts = dto.ReputationOverview.ReviewCountsBySource[reviewSource];
                ReviewSourceDTO source = (from rs in reviewSources where rs.ID == (long)reviewSource select rs).FirstOrDefault();

                if (source != null && !string.IsNullOrEmpty(source.ImageUrl))
                {
                    Sites site = new Sites();

                    site.SiteID = source.ID;
                    site.SiteName = source.Name;
                    site.SiteImageUrl = source.ImageUrl;
					site.SiteNameFilter = string.Format("sites,eq,{0}-locations,eq,{1}", source.ID, userInfo.EffectiveUser.CurrentContextAccountID);
                    site.AverageReviewScore = Convert.ToDecimal(counts.AverageRating);
                    site.FilteredNegative = counts.TotalNegativeFiltered;
					site.FilteredNegativeFilter = string.Format("sites,eq,{0}-status,IsFiltered,true-rating,lte,2.499-locations,eq,{1}", source.ID, userInfo.EffectiveUser.CurrentContextAccountID);
                    site.FilteredPositive = counts.TotalPositiveFiltered;
					site.FilteredPositiveFilter = string.Format("sites,eq,{0}-status,IsFiltered,true-rating,gte,2.5-locations,eq,{1}", source.ID, userInfo.EffectiveUser.CurrentContextAccountID);
                    site.FilteredTotal = counts.TotalFiltered;
					site.FilteredTotalFilter = string.Format("sites,eq,{0}-status,IsFiltered,true-locations,eq,{1}", source.ID, userInfo.EffectiveUser.CurrentContextAccountID);
                    site.LastReview = counts.LastReviewDate != null ? counts.LastReviewDate.LocalDate : null;
                    site.NegativeReviews = counts.TotalNegative;
					site.NegativeReviewsFilter = string.Format("sites,eq,{0}-rating,lte,2.499-locations,eq,{1}", source.ID, userInfo.EffectiveUser.CurrentContextAccountID);
                    site.NumberOfReviews = counts.Total;
					site.NumberofReviewsFilter = string.Format("sites,eq,{0}-locations,eq,{1}", source.ID, userInfo.EffectiveUser.CurrentContextAccountID);
                    site.PositiveReviews = counts.TotalPositive;
					site.PositiveReviewsFilter = string.Format("sites,eq,{0}-rating,gte,2.5-locations,eq,{1}", source.ID, userInfo.EffectiveUser.CurrentContextAccountID);
                    site.UnReadNegative = 0;
					site.UnReadNegativeFilter = string.Format("sites,eq,{0}-status,HasBeenRead,false-rating,lte,2.499-locations,eq,{1}", source.ID, userInfo.EffectiveUser.CurrentContextAccountID);
                    site.UnReadPositive = 0;
					site.UnReadPositiveFilter = string.Format("sites,eq,{0}-status,HasBeenRead,false-rating,gte,2.5-locations,eq,{1}", source.ID, userInfo.EffectiveUser.CurrentContextAccountID);
                    site.UnReadTotal = 0;
					site.UnReadTotalFilter = string.Format("sites,eq,{0}-status,HasBeenRead,false-locations,eq,{1}", source.ID, userInfo.EffectiveUser.CurrentContextAccountID);
                    site.UnRespondNegative = 0;
					site.UnRespondNegativeFilter = string.Format("sites,eq,{0}-status,HasBeenResponded,false-rating,lte,2.499-locations,eq,{1}", source.ID, userInfo.EffectiveUser.CurrentContextAccountID);
                    site.UnRespondPositive = 0;
					site.UnRespondPositiveFilter = string.Format("sites,eq,{0}-status,HasBeenResponded,false-rating,gte,2.5-locations,eq,{1}", source.ID, userInfo.EffectiveUser.CurrentContextAccountID);
                    site.UnRespondTotal = 0;
					site.UnRespondTotalFilter = string.Format("sites,eq,{0}-status,HasBeenResponded,false-locations,eq,{1}", source.ID, userInfo.EffectiveUser.CurrentContextAccountID);

                    sites.Add(site);
                }
            }

            return sites.OrderBy(o => o.SiteName).ToList();
        }

        public static List<SelectListItem> VirtualGroupList(UserInfoDTO userInfo, string filter)
        {
	        List<SelectListItem> vGroups = new List<SelectListItem>();
	        List<VirtualGroupDTO> virtualGroupDtos = ProviderFactory.Security.GetVirtualGroups(userInfo);

			foreach (VirtualGroupDTO dto in virtualGroupDtos.Distinct())
			{
				vGroups.Add(new SelectListItem() {Text = dto.Name, Value = dto.ID.ToString()});
			}

	        if (string.IsNullOrEmpty(filter))
				return vGroups.OrderBy(o => o.Text).ToList();
            return vGroups.Where(g => g.Text.ToLower().StartsWith(filter)).OrderBy(o => o.Text).ToList();
        }

        public static ReviewEmail ReviewEmail(UserInfoDTO userInfo, long reviewID)
        {
            ReviewEmail reviewEmail = new ReviewEmail();
	        reviewEmail.ReviewID = reviewID;

	        ReviewDTO reviewDto = ProviderFactory.Reviews.GetReviewByID(reviewID);
	        reviewEmail.ReviewDetails = string.Format("{0} Review By {1} on {2} - Rating of {3}", reviewDto.ReviewSource, reviewDto.ReviewerName, reviewDto.ReviewDate.LocalDate.Value.ToShortDateString(), reviewDto.Rating);
	        reviewEmail.FromEmail = userInfo.EffectiveUser.EmailAddress;
	        reviewEmail.Subject = "Take a look at this review";

			List<UserEdit> userEdits = AdminService.UsersAccessEditGrid(userInfo, reviewDto.AccountID);
			foreach (UserEdit user in userEdits)
			{
				ReviewEmail.AccountUser accountUser = new ReviewEmail.AccountUser();
				accountUser.UserID = user.UserID.GetValueOrDefault();
				accountUser.UserName = string.Format("{0} {1} ({2})", user.FirstName, user.LastName, user.Email);
				accountUser.Email = user.Email;

				reviewEmail.AccountUsers.Add(accountUser);
			}

            return reviewEmail;
        }

		public static bool ReviewAction(UserInfoDTO userInfo, long reviewID, string menuaction)
		{
			return ProviderFactory.Reviews.UpdateReviewByID(userInfo, reviewID, menuaction);
		}

        public static bool ReviewActionEmail(UserInfoDTO userInfo, ReviewEmail reviewEmail, ref List<string> problems)
        {
	        ProviderFactory.Logging.Audit(AuditLevelEnum.Information, AuditTypeEnum.ReviewStreamAction,
	                                      string.Format("Review Action Email Review - To: {0} Comment: {1}", reviewEmail.ToEmail, reviewEmail.Comments)
	                                      , null, userInfo, null, null, null, reviewEmail.ReviewID);

	        ReviewShareEmailDTO reviewShareEmailDto = new ReviewShareEmailDTO();
	        reviewShareEmailDto.ReviewID = reviewEmail.ReviewID;
	        //reviewShareEmailDto.UserToEmailID = reviewEmail.ToEmail.ToLong();
	        reviewShareEmailDto.Subject = reviewEmail.Subject;
	        reviewShareEmailDto.Comments = reviewEmail.Comments;
            reviewShareEmailDto.ToEmailID = reviewEmail.ToEmail;

			bool success = ProviderFactory.Platform.SendReviewShareEmail(reviewShareEmailDto, userInfo.EffectiveUser.ID.GetValueOrDefault());

	        if (!success)
		        problems.Add("Can't send Review Email at this time.");

	        return success;
        }


		public static List<SelectListItem> DealerList(string virtualGroup, string dealerFilter, UserInfoDTO userInfo, long dealerIDFilter)
        {
			if (dealerIDFilter == 0)
			{
				AccountSearchRequestDTO req = new AccountSearchRequestDTO();

				req.PackageIDs = new List<long>();
				req.FranchiseTypeIDs = new List<long>();
				req.AccountTypeIDs = new List<long>();
				req.UserInfo = userInfo;
				req.SearchString = dealerFilter;
				req.SortBy = 2;
				req.SortAscending = true;
				req.StartIndex = 1;
				req.EndIndex = 100;
				req.OnlyIfHasChildAccounts = false;

				AccountSearchResultDTO accountSearchResult = ProviderFactory.Security.SearchAccounts(req);

				return accountSearchResult.Items.Select(a => new SelectListItem {Selected = false, Text = a.Name, Value = a.ID.ToString(CultureInfo.InvariantCulture)}).ToList();
			}
			else
			{
				List<SelectListItem> accounts = new List<SelectListItem>();
				SelectListItem selectListItem = new SelectListItem();
				selectListItem.Text = ProviderFactory.Security.GetAccountNameByID(dealerIDFilter);
				selectListItem.Value = dealerIDFilter.ToString(CultureInfo.InvariantCulture);
				selectListItem.Selected = true;

				accounts.Add(selectListItem);
				return accounts;
			}
        }

        public static List<FilterList> GetReviewSources(UserInfoDTO userInfo)
        {
            List<ReviewSourceDTO> reviewSource = ProviderFactory.Reviews.GetReviewSources(userInfo).OrderBy(s => s.Name).ToList();
            return reviewSource.Select(s => new FilterList() { ID = s.ID, Name = s.Name }).ToList();
        }

        public static List<FilterList> CategoryList()
        {
            List<CategoryDTO> category = ProviderFactory.Lists.GetCategory();
            return category.Select(s => new FilterList() { ID = s.ID, Name = s.Name }).ToList();
        }

        public static List<FilterList> StatesList()
        {
            List<StateDTO> state = ProviderFactory.Lists.GetStatesForCountry(1);
            return state.Select(s => new FilterList() { ID = s.ID, Name = s.Name }).ToList();
        }

        public static List<FilterList> OEMList()
        {
            List<FranchiseTypeDTO> franchise = ProviderFactory.Lists.GetFranchiseTypes();
            return franchise.Select(s => new FilterList() { ID = s.ID, Name = s.Name }).ToList();
        }

	    public static List<ReputationSitesReview> ReviewSitesReviews(UserInfoDTO userInfo)
	    {
			List<ReputationSitesReview> sitesReview = new List<ReputationSitesReview>();

			DashboardOverviewDTO dto = ProviderFactory.Reviews.GetDashboardOverviewData(userInfo);

		    List<ReputationMonthMetrics> metrics = dto.ReputationOverview.MonthlyMetrics6Months;
			List<ReviewSourceDTO> reviewSourceDtos = ProviderFactory.Reviews.GetReviewSources(userInfo);

			foreach (ReputationMonthMetrics metric in metrics)
			{
				ReputationSitesReview review = new ReputationSitesReview();
				review.AvgRating = Math.Round(metric.AvgRating, 2);
				review.MonthDate = metric.MonthDate;
				review.ReviewSource = reviewSourceDtos.Where(r => r.ID == (long)metric.ReviewSource).Select(r => r.Name).FirstOrDefault();

                if (review.ReviewSource != null)
                {
                    sitesReview.Add(review);
                }
			}

		    return sitesReview;
	    }

	    public static CompetitiveAnalysis CompetitiveAnalysisReport(UserInfoDTO userInfo)
	    {
		    CompetitiveAnalysis competitiveAnalysis = new CompetitiveAnalysis();

			CompAnalysisDataDTO compAnalysisDataDto = ProviderFactory.Reviews.GetCompetitiveAnalysisData(userInfo);

		    competitiveAnalysis.MyScore = string.Format("{0:n1}", compAnalysisDataDto.MyScore);
			competitiveAnalysis.CompetitorsScore = string.Format("{0:n1}", compAnalysisDataDto.CompetitorScore);

		    competitiveAnalysis.MyLocationGroupAvg.Name = "My Location(s)";
		    competitiveAnalysis.MyLocationGroupAvg.CurrentCounts.AvgScore = compAnalysisDataDto.Main.Current.Score.ToString("n1");
		    competitiveAnalysis.MyLocationGroupAvg.CurrentCounts.Count = compAnalysisDataDto.Main.Current.ReviewCount.ToString("n0");
		    competitiveAnalysis.MyLocationGroupAvg.CurrentCounts.PositiveCount = string.Format("+{0:n0}%", compAnalysisDataDto.Main.Current.PercentPositive * 100);
			competitiveAnalysis.MyLocationGroupAvg.CurrentCounts.NegativeCount = string.Format("-{0:n0}%", compAnalysisDataDto.Main.Current.PercentNegative * 100);
			competitiveAnalysis.MyLocationGroupAvg.Days30Counts.AvgScore = compAnalysisDataDto.Main.Last30Days.Score.ToString("n1");
			competitiveAnalysis.MyLocationGroupAvg.Days30Counts.Count = compAnalysisDataDto.Main.Last30Days.ReviewCount.ToString("n0");
			competitiveAnalysis.MyLocationGroupAvg.Days30Counts.PositiveCount = string.Format("+{0:n0}%", compAnalysisDataDto.Main.Last30Days.PercentPositive * 100);
			competitiveAnalysis.MyLocationGroupAvg.Days30Counts.NegativeCount = string.Format("-{0:n0}%", compAnalysisDataDto.Main.Last30Days.PercentNegative * 100);
			
			competitiveAnalysis.MyGroupsAvg.Name = "My Group Avg";
			competitiveAnalysis.MyGroupsAvg.CurrentCounts.AvgScore = compAnalysisDataDto.Group.Current.Score.ToString("n1");
			competitiveAnalysis.MyGroupsAvg.CurrentCounts.Count = compAnalysisDataDto.Group.Current.ReviewCount.ToString("n0");
			competitiveAnalysis.MyGroupsAvg.CurrentCounts.PositiveCount = string.Format("+{0:n0}%", compAnalysisDataDto.Group.Current.PercentPositive * 100);
			competitiveAnalysis.MyGroupsAvg.CurrentCounts.NegativeCount = string.Format("-{0:n0}%", compAnalysisDataDto.Group.Current.PercentNegative * 100);
			competitiveAnalysis.MyGroupsAvg.Days30Counts.AvgScore = compAnalysisDataDto.Group.Last30Days.Score.ToString("n1");
			competitiveAnalysis.MyGroupsAvg.Days30Counts.Count = compAnalysisDataDto.Group.Last30Days.ReviewCount.ToString("n0");
			competitiveAnalysis.MyGroupsAvg.Days30Counts.PositiveCount = string.Format("+{0:n0}%", compAnalysisDataDto.Group.Last30Days.PercentPositive * 100);
			competitiveAnalysis.MyGroupsAvg.Days30Counts.NegativeCount = string.Format("-{0:n0}%", compAnalysisDataDto.Group.Last30Days.PercentNegative * 100);

			competitiveAnalysis.CompetitorsGroupAvg.Name = "Competitors Avg";
			competitiveAnalysis.CompetitorsGroupAvg.CurrentCounts.AvgScore = compAnalysisDataDto.Comp.Current.Score.ToString("n1");
			competitiveAnalysis.CompetitorsGroupAvg.CurrentCounts.Count = compAnalysisDataDto.Comp.Current.ReviewCount.ToString("n0");
			competitiveAnalysis.CompetitorsGroupAvg.CurrentCounts.PositiveCount = string.Format("+{0:n0}%", compAnalysisDataDto.Comp.Current.PercentPositive * 100);
			competitiveAnalysis.CompetitorsGroupAvg.CurrentCounts.NegativeCount = string.Format("-{0:n0}%", compAnalysisDataDto.Comp.Current.PercentNegative * 100);
			competitiveAnalysis.CompetitorsGroupAvg.Days30Counts.AvgScore = compAnalysisDataDto.Comp.Last30Days.Score.ToString("n1");
			competitiveAnalysis.CompetitorsGroupAvg.Days30Counts.Count = compAnalysisDataDto.Comp.Last30Days.ReviewCount.ToString("n0");
			competitiveAnalysis.CompetitorsGroupAvg.Days30Counts.PositiveCount = string.Format("+{0:n0}%", compAnalysisDataDto.Comp.Last30Days.PercentPositive * 100);
			competitiveAnalysis.CompetitorsGroupAvg.Days30Counts.NegativeCount = string.Format("-{0:n0}%", compAnalysisDataDto.Comp.Last30Days.PercentNegative * 100);

            foreach (var item in compAnalysisDataDto.CompetitorScores)
            {
                CALocationAverages locs = new CALocationAverages();

                CompAnalysisSummaryGroupDTO summary = item.Value;
                locs.Name = item.Key;

                if (summary != null)
                {
                    locs.CurrentCounts.AvgScore = summary.Current.Score.ToString("n1");
                    locs.CurrentCounts.Count = summary.Current.ReviewCount.ToString("n0");
                    locs.CurrentCounts.PositiveCount = string.Format("+{0:n0}%", summary.Current.PercentPositive * 100);
                    locs.CurrentCounts.NegativeCount = string.Format("-{0:n0}%", summary.Current.PercentNegative * 100);

                    locs.Last24Counts.AvgScore = summary.Last24h.Score.ToString("n1");
                    locs.Last24Counts.Count = summary.Last24h.ReviewCount.ToString("n0");
                    locs.Last24Counts.PositiveCount = string.Format("+{0:n0}%", summary.Last24h.PercentPositive * 100);
                    locs.Last24Counts.NegativeCount = string.Format("-{0:n0}%", summary.Last24h.PercentNegative * 100);

                    locs.Last7DaysCounts.AvgScore = summary.Last7Days.Score.ToString("n1");
                    locs.Last7DaysCounts.Count = summary.Last7Days.ReviewCount.ToString("n0");
                    locs.Last7DaysCounts.PositiveCount = string.Format("+{0:n0}%", summary.Last7Days.PercentPositive * 100);
                    locs.Last7DaysCounts.NegativeCount = string.Format("-{0:n0}%", summary.Last7Days.PercentNegative * 100);

                    locs.Last30DaysCounts.AvgScore = summary.Last30Days.Score.ToString("n1");
                    locs.Last30DaysCounts.Count = summary.Last30Days.ReviewCount.ToString("n0");
                    locs.Last30DaysCounts.PositiveCount = string.Format("+{0:n0}%", summary.Last30Days.PercentPositive * 100);
                    locs.Last30DaysCounts.NegativeCount = string.Format("-{0:n0}%", summary.Last30Days.PercentNegative * 100);

                    competitiveAnalysis.LocationAverages.Add(locs);
                }
            }
            //foreach (SimpleAccountInfoDTO account in compAnalysisDataDto.Accounts)
            //{
            //    CALocationAverages locs = new CALocationAverages();
            //    locs.Name = account.Name;

            //    CompAnalysisSummaryGroupDTO summary = compAnalysisDataDto.CompetitorScores.Where(c => c.Key == account.ID).Select(c => c.Value).FirstOrDefault();
            //    if (summary != null)
            //    {
            //        locs.CurrentCounts.AvgScore = summary.Current.Score.ToString("n1");
            //        locs.CurrentCounts.Count = summary.Current.ReviewCount.ToString("n0");
            //        locs.CurrentCounts.PositiveCount = string.Format("+{0:n0}%", summary.Current.PercentPositive*100);
            //        locs.CurrentCounts.NegativeCount = string.Format("-{0:n0}%", summary.Current.PercentNegative*100);

            //        locs.Last24Counts.AvgScore = summary.Last24h.Score.ToString("n1");
            //        locs.Last24Counts.Count = summary.Last24h.ReviewCount.ToString("n0");
            //        locs.Last24Counts.PositiveCount = string.Format("+{0:n0}%", summary.Last24h.PercentPositive*100);
            //        locs.Last24Counts.NegativeCount = string.Format("-{0:n0}%", summary.Last24h.PercentNegative*100);

            //        locs.Last7DaysCounts.AvgScore = summary.Last7Days.Score.ToString("n1");
            //        locs.Last7DaysCounts.Count = summary.Last7Days.ReviewCount.ToString("n0");
            //        locs.Last7DaysCounts.PositiveCount = string.Format("+{0:n0}%", summary.Last7Days.PercentPositive*100);
            //        locs.Last7DaysCounts.NegativeCount = string.Format("-{0:n0}%", summary.Last7Days.PercentNegative*100);

            //        locs.Last30DaysCounts.AvgScore = summary.Last30Days.Score.ToString("n1");
            //        locs.Last30DaysCounts.Count = summary.Last30Days.ReviewCount.ToString("n0");
            //        locs.Last30DaysCounts.PositiveCount = string.Format("+{0:n0}%", summary.Last30Days.PercentPositive*100);
            //        locs.Last30DaysCounts.NegativeCount = string.Format("-{0:n0}%", summary.Last30Days.PercentNegative*100);

            //        competitiveAnalysis.LocationAverages.Add(locs);
            //    }
            //}

		    return competitiveAnalysis;
	    }

		public static List<ReviewCountPerSite> ReviewCountPerSite(UserInfoDTO userInfo, PagedGridRequest pagedGridRequest)
	    {
		    List<ReviewCountPerSite> reviewCountPerSites = new List<ReviewCountPerSite>();

			ReviewStreamCountPerSiteRequestDTO req = new ReviewStreamCountPerSiteRequestDTO();
			foreach (PagedGridFilterDescriptor filter in pagedGridRequest.Filtering)
			{
				if (filter.Field == "ReviewText")
				{
					if (filter.Operator == FilterOperator.Contains)
						req.ReviewTextContains = filter.Value.ToLower(); //.Replace(" ", " AND ");
					else if (filter.Operator == FilterOperator.DoesNotContain)
						req.ReviewTextDoesNotContain = filter.Value.ToLower(); //.Replace(" ", " AND ");
				}

				if (filter.Field == "DealerLocation.StateID")
					req.StateIDs.Add(filter.Value.ToLong());

				if (filter.Field == "SiteID")
					req.ReviewSourcesIDs.Add(filter.Value.ToLong());

				if (filter.Field == "Rating")
				{
					if (filter.Operator == FilterOperator.IsGreaterThanOrEqualTo)
						req.RatingIsGreaterThanOrEqualTo = filter.Value.ToDouble();
					else if (filter.Operator == FilterOperator.IsLessThanOrEqualTo)
						req.RatingIsLessThanOrEqualTo = filter.Value.ToDouble();
				}

				if (filter.Field == "ReviewDate")
				{
					if (filter.Operator == FilterOperator.IsGreaterThanOrEqualTo)
					{
						req.MinDate = filter.Value.ToDateTimeNullable();
					}
					else if (filter.Operator == FilterOperator.IsLessThanOrEqualTo)
					{
						req.MaxDate = filter.Value.ToDateTimeNullable();
					}
					else if (filter.Operator == FilterOperator.IsEqualTo)
					{
						req.MinDate = filter.Value.ToDateTimeNullable();
						req.MaxDate = filter.Value.ToDateTimeNullable();
					}
					else if (filter.Operator == FilterOperator.IsGreaterThan)
					{
						req.MinDate = filter.Value.ToDateTimeNullable().Value.AddDays(1);
					}
					else if (filter.Operator == FilterOperator.IsLessThan)
					{
						req.MaxDate = filter.Value.ToDateTimeNullable().Value.AddDays(-1);
					}
				}

				if (filter.Field == "DealerLocation.OemID")
					req.FranchiseTypeIDs.Add(filter.Value.ToLong());

				if (filter.Field == "Category")
					req.CategoryID = filter.Value.ToLong();

				if (filter.Field == "DealerLocation.LocationID")
					req.AccountIDs.Add(filter.Value.ToLong());


				if (filter.Field == "IsFiltered")
				{
					req.IncludeFiltered = true;
					req.IncludeUnFiltered = false;
				}
				else
				{
					req.IncludeFiltered = false;
					req.IncludeUnFiltered = true;
				}

				if (filter.Field == "HasBeenRead")
				{
					if (filter.Value == "true")
						req.FilterRead = true;
					else
						req.FilterUnRead = true;
				}

				if (filter.Field == "HasBeenResponded")
				{
					if (filter.Value == "true")
						req.FilterResponded = true;
					else
						req.FilterUnResponded = true;
				}

				if (filter.Field == "IsIgnored")
				{
					req.FilterIgnored = true;
				}


			}

			ReviewStreamCountPerSiteResultDTO result = ProviderFactory.Reviews.GetReviewCountPerSite(userInfo, req);

			foreach (ReviewCountPerSiteResultsDTO dto in result.results)
			{
				ReviewCountPerSite reviewCountPerSite = new ReviewCountPerSite();
				reviewCountPerSite.Count = dto.TotalReviews.GetValueOrDefault();
				reviewCountPerSite.Site = dto.Name;
				reviewCountPerSite.PosCount = dto.PositiveReviews.GetValueOrDefault();
				reviewCountPerSite.NegCount = dto.NegativeReviews.GetValueOrDefault();
				
				reviewCountPerSites.Add(reviewCountPerSite);
			}

		    return reviewCountPerSites;
	    }

	    public static List<ReviewPosNegChart> ReviewPosNegChart(UserInfoDTO userInfo)
	    {
		    List<ReviewPosNegChart> reviewPosNegCharts = new List<ReviewPosNegChart>();

			//DashboardOverviewDTO dto = ProviderFactory.Reviews.GetDashboardOverviewData(userInfo);

			//if (dto.ReputationOverview.ReviewsAllTime.TotalPositive > 0 || dto.ReputationOverview.ReviewsAllTime.TotalNegative > 0)
			//{
			//	reviewPosNegCharts.Add(new ReviewPosNegChart() {category = "Pos", value = dto.ReputationOverview.ReviewsAllTime.TotalPositive.ToString(), color = "#76b800"});
			//	reviewPosNegCharts.Add(new ReviewPosNegChart() {category = "Neg", value = dto.ReputationOverview.ReviewsAllTime.TotalNegative.ToString(), color = "#ef4c00"});
			//}
		    return reviewPosNegCharts;
	    }
    }
}
