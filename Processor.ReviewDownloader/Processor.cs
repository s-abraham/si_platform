﻿using Connectors;
using Connectors.Parameters;
using JobLib;
using SI;
using SI.BLL;
using SI.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Processor.ReviewDownloader
{
    public class Processor : ProcessorBase<ReviewDownloaderData>
    {
        // Need to pass the args through for the default constructor
        public Processor(string[] args) : base(args) { }

        protected override void start()
        {

            List<ReviewUrlDTO> urls = null;
            List<ReviewDownloaderData> datas = null;
            List<ReviewSourceAndAccountPairDTO> itemsToClear = null;
            List<RawReviewDTO> rawReviewsToSave = null;
            List<RawReviewSummaryDTO> rawReviewSummariesToSave = null;
            List<JobDTO> newJobsToQueue = null;

            try
            {
                datas = getAllJobDataItems();
                urls = ProviderFactory.Reviews.GetReviewUrls(UserInfoDTO.System, (from d in datas select d.Account_ReviewSourceID).ToList());
                itemsToClear = new List<ReviewSourceAndAccountPairDTO>();

                foreach (ReviewUrlDTO item in urls)
                {
                    itemsToClear.Add(new ReviewSourceAndAccountPairDTO() { AccountID = item.AccountID, ReviewSourceID = (long)item.ReviewSource });
                }

                if (itemsToClear.Any())
                {
                    // batch clear the raw data prior to the job cycle
                    ProviderFactory.Reviews.ClearRawReviewsForAccountAndSource(itemsToClear);
                }

                // save up raw reviews and summaries for a batch save
                rawReviewsToSave = new List<RawReviewDTO>();
                rawReviewSummariesToSave = new List<RawReviewSummaryDTO>();

                newJobsToQueue = new List<JobDTO>();

            }
            catch (Exception ex2)
            {
                ProviderFactory.Logging.LogException(ex2);
                ProviderFactory.Logging.Audit(AuditLevelEnum.Exception, AuditTypeEnum.ProcessorException, "Batch operation failed, parking jobs: " + ex2.Message, null, null, null, null, null, null);

                try
                {
                    // we will park the jobs because they technically didn't fail, the batch failed as a whole.
                    foreach (JobDTO item in getAllJobs())
                    {
                        setJobStatus(item.ID.Value, JobStatusEnum.Parked);
                    }
                }
                catch (Exception)
                {
                }
            }

            JobDTO job = getNextJob();
            long jobID = 0;
            while (job != null)
            {
                jobID = job.ID.Value;
                long? arsid = null;
                try
                {

                    setJobStatus(job.ID.Value, JobStatusEnum.Started);
                    ReviewDownloaderData data = getJobData(job.ID.Value);
                    arsid = data.Account_ReviewSourceID;

                    ReviewUrlDTO rUrl = (from u in urls where u.ID.Value == data.Account_ReviewSourceID select u).FirstOrDefault(); //ProviderFactory.Reviews.GetReviewUrl(data.Account_ReviewSourceID, 0);
                    if (rUrl == null)
                    {
                        setJobStatus(job.ID.Value, JobStatusEnum.Failed);
                        Auditor
                            .New(AuditLevelEnum.Error, AuditTypeEnum.ReviewDownloaderFailure, UserInfoDTO.System, "URL Not Found Failure")
                            .SetAccount_ReviewSourceID(data.Account_ReviewSourceID)
                            .SetAuditDataObject(new AuditReviewDownloaderFailure()
                            {
                                JobID = job.ID.Value,
                                WebException = string.Format("No Account_ReviewSource record could be found for Account_ReviewSourceID {0}. JobID {1}", data.Account_ReviewSourceID, job.ID),
                                ReviewSourceID = (long)rUrl.ReviewSource,
                                URL = ""
                            }
                            )
                            .Save(ProviderFactory.Logging)
                        ;
                    }
                    else if (String.IsNullOrEmpty(rUrl.HtmlURL) && rUrl.ReviewSource != ReviewSourceEnum.Facebook)
                    {
                        setJobStatus(job.ID.Value, JobStatusEnum.Failed);
                        Auditor
                            .New(AuditLevelEnum.Error, AuditTypeEnum.ReviewDownloaderFailure, UserInfoDTO.System, "Empty URL Failure")
                            .SetAccount_ReviewSourceID(data.Account_ReviewSourceID)
                            .SetAuditDataObject(new AuditReviewDownloaderFailure()
                            {
                                JobID = job.ID.Value,
                                WebException = string.Format("Could not process because URL was empty for Account_ReviewSourceID {0}. JobID {1}", data.Account_ReviewSourceID, job.ID),
                                ReviewSourceID = (long)rUrl.ReviewSource,
                                URL = ""
                            }
                            )
                            .Save(ProviderFactory.Logging)
                        ;
                    }
                    else
                    {
                        if (rUrl.ReviewSource == ReviewSourceEnum.Facebook)
                        {
                            SocialCredentialDTO socialCredentialsDTO = null;
                            if (!string.IsNullOrEmpty(rUrl.ExternalID))
                                socialCredentialsDTO = ProviderFactory.Social.GetSocialCredentialInfo(Convert.ToInt64(rUrl.ExternalID));

                            if (socialCredentialsDTO != null)
                            {
                                string FacebookGraphAPIRatingURL = "https://graph.facebook.com/{0}/ratings?limit=1000&fields=open_graph_story,created_time,reviewer,has_rating,has_review,rating,review_text&access_token={1}";
                                rUrl.ApiURL = string.Format(FacebookGraphAPIRatingURL, socialCredentialsDTO.UniqueID, socialCredentialsDTO.Token);                                
                            }
                        }

                        SIReviewConnection connection = ProviderFactory.Reviews.GetReviewConnection(rUrl.ReviewSource);
                        ReviewDownloadResult result = connection.Scrape(rUrl, data.DownloadDetails);

                        switch (result.Status)
                        {
                            case ReviewDownloadResult.StatusEnum.Success:

                                if (rUrl.AccountID > 0 && (long)rUrl.ReviewSource > 0)
                                {
                                    rawReviewSummariesToSave.Add(new RawReviewSummaryDTO()
                                    {
                                        AccountID = rUrl.AccountID,
                                        ReviewSource = rUrl.ReviewSource,
                                        SalesAverageRating = result.SalesAverageRating,
                                        SalesReviewCount = result.SalesReviewCount,
                                        ServiceAverageRating = result.ServiceAverageRating,
                                        ServiceReviewCount = result.ServiceReviewCount,
                                        ExtraInfo = result.ExtraInfo
                                    });
                                }

                                if (result.ReviewDetails != null)
                                {
                                    foreach (Connectors.ReviewDownloadResult.ReviewDetail detail in result.ReviewDetails)
                                    {
                                        rawReviewsToSave.Add(new RawReviewDTO()
                                        {
                                            AccountID = rUrl.AccountID,
                                            ReviewSource = rUrl.ReviewSource,
                                            ExtraInfo = detail.ExtraInfo,
                                            Rating = Decimal.Round(detail.Rating, 2),
                                            ReviewDate = detail.ReviewDate,
                                            ReviewerName = detail.ReviewerName,
                                            ReviewText = detail.ReviewTextFull,
                                            ReviewURL = null
                                        });

                                    }
                                }

                                // enqueue the integrator
                                ReviewIntegratorData riData = new ReviewIntegratorData()
                                {
                                    AccountID = rUrl.AccountID,
                                    ReviewSourceID = (long)rUrl.ReviewSource,
                                    RemainingRetries = 0
                                };
                                JobDTO riJob = new JobDTO()
                                {
                                    JobDataObject = riData,
                                    DateScheduled = SIDateTime.Now,
                                    JobStatus = JobStatusEnum.Queued,
                                    OriginJobID = job.ID.Value,
                                    Priority = 150,
                                    JobType = JobTypeEnum.ReviewIntegrator
                                };

                                newJobsToQueue.Add(riJob);

                                Auditor
                                    .New(AuditLevelEnum.Information, AuditTypeEnum.ReviewDownloaderSuccess, UserInfoDTO.System, "Downloader succeeded")
                                    .SetAccountID(rUrl.AccountID)
                                    .SetAccount_ReviewSourceID(data.Account_ReviewSourceID)
                                    .Save(ProviderFactory.Logging)
                                ;



                                setJobStatus(job.ID.Value, JobStatusEnum.Completed);

                                rUrl.LastSummaryRun = SIDateTime.Now;
                                rUrl.LastDetailRun = SIDateTime.Now;
                                rUrl.LastSummaryAttempted = SIDateTime.Now;
                                rUrl.LastDetailAttempted = SIDateTime.Now;
                                ProviderFactory.Reviews.SaveReviewUrl(UserInfoDTO.System, rUrl);

                                break;

                            case ReviewDownloadResult.StatusEnum.Exception:
                            case ReviewDownloadResult.StatusEnum.InvalidFormat:
                            case ReviewDownloadResult.StatusEnum.Timeout:
                                {
                                    rUrl.LastSummaryAttempted = SIDateTime.Now;
                                    rUrl.LastDetailAttempted = SIDateTime.Now;
                                    ProviderFactory.Reviews.SaveReviewUrl(UserInfoDTO.System, rUrl);

                                    AuditReviewDownloaderFailure aud = new AuditReviewDownloaderFailure()
                                    {
                                        FailedXPaths = result.FailureReasons,
                                        URL = rUrl.HtmlURL,
                                        JobID = job.ID.GetValueOrDefault(0),
                                        ReviewSourceID = (long)rUrl.ReviewSource
                                    };
                                    if (result.Exception != null)
                                        aud.WebException = result.Exception.ToString();

                                    Auditor
                                        .New(AuditLevelEnum.Error, AuditTypeEnum.ReviewDownloaderFailure, UserInfoDTO.System, result.Status.ToString())
                                        .SetAccountID(rUrl.AccountID)
                                        .SetAccount_ReviewSourceID(data.Account_ReviewSourceID)
                                        .SetAuditDataObject(aud)
                                        .Save(ProviderFactory.Logging)
                                    ;

                                }
                                setJobStatus(job.ID.Value, JobStatusEnum.Failed);
                                break;

                            default:
                                break;
                        }

                    }

                }
                catch (Exception ex)
                {
                    setJobStatus(job.ID.Value, JobStatusEnum.Failed);
                    ProviderFactory.Logging.LogException(ex, job.ID.Value);
                    Auditor
                        .New(AuditLevelEnum.Exception, AuditTypeEnum.ProcessorException, UserInfoDTO.System, ex.ToString())
                        .SetAccount_ReviewSourceID(arsid)
                        .Save(ProviderFactory.Logging)
                    ;
                }


                job = getNextJob();
            }

            if (rawReviewsToSave.Any())
            {
                List<long> idlist = ProviderFactory.Reviews.SaveNewRawReviews(rawReviewsToSave, jobID);
                if (idlist.Count() != rawReviewsToSave.Count())
                {
                    List<long> jobIDList = (from j in getAllJobs() select j.ID.Value).ToList();
                    Auditor
                        .New(AuditLevelEnum.Error, AuditTypeEnum.General, UserInfoDTO.System, "")
                        .SetMessage("RAW REVIEW SAVE MISMATCH: Downloaded {0} reviews, but only saved {1}... JobIDs:", rawReviewsToSave.Count(), idlist.Count(), string.Join(",", jobIDList))                       
                        .Save(ProviderFactory.Logging)
                    ;
                }
            }
            if (rawReviewSummariesToSave.Any())
            {
                ProviderFactory.Reviews.SaveNewRawReviewSummaries(rawReviewSummariesToSave, jobID);
            }
            if (newJobsToQueue.Any())
            {
                ProviderFactory.Jobs.Save(new SaveEntityDTO<List<JobDTO>>(newJobsToQueue, new UserInfoDTO(1, null)));
            }
        }
    }
}
