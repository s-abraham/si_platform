﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Processor.GoogleReviewDownloader
{
    class Program
    {
        static void Main(string[] args)
        {
            Processor p = new Processor(args);
            p.Start();
        }
    }
}
