﻿using Connectors;
using Connectors.Parameters;
using JobLib;
using SI.BLL;
using SI.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace Processor.GoogleReviewDownloader
{
    public class Processor : ProcessorBase<GoogleReviewDownloaderData>
    {
        // Need to pass the args through for the default constructor
        public Processor(string[] args) : base(args) { }

        public override void Start()
        {
            List<long> sumIDs = new List<long>();

            JobDTO job = getNextJob();

            while (job != null)
            {
                try
                {
                    setJobStatus(job.ID.Value, JobStatusEnum.Started);
                    GoogleReviewDownloaderData data = getJobData(job.ID.Value);

                    //get the review url
                    ReviewUrlDTO ruDTO = ProviderFactory.Reviews.GetReviewUrl(data.Account_ReviewSourceID);


                    SIGoogleReviewXPathParameters siGoogleReviewXPathParameters = new SIGoogleReviewXPathParameters();

                    siGoogleReviewXPathParameters.ParentnodeReviewSummaryXPath = ProviderFactory.Reviews.GetProcessorSetting("Google.Review.ParentnodeReviewSummaryXPath");
                    siGoogleReviewXPathParameters.RatingValueXPath = ProviderFactory.Reviews.GetProcessorSetting("Google.Review.RatingValueXPath");
                    siGoogleReviewXPathParameters.ReviewCountCollectedXPath = ProviderFactory.Reviews.GetProcessorSetting("Google.Review.CountCollectedXPath");
                    siGoogleReviewXPathParameters.ListNodeReviewDetailsXPath = ProviderFactory.Reviews.GetProcessorSetting("Google.Review.ListNodeReviewDetailsXPath");
                    siGoogleReviewXPathParameters.ReviewDetailRatingValueXPath = ProviderFactory.Reviews.GetProcessorSetting("Google.Review.ReviewDetailRatingValueXPath");
                    siGoogleReviewXPathParameters.ReviewDetailRatingHalfValueXPath = ProviderFactory.Reviews.GetProcessorSetting("Google.Review.ReviewDetailRatingHalfValueXPath");
                    siGoogleReviewXPathParameters.ReviewerNameXPath = ProviderFactory.Reviews.GetProcessorSetting("Google.Review.ReviewerNameXPath");
                    siGoogleReviewXPathParameters.ReviewDateXPath = ProviderFactory.Reviews.GetProcessorSetting("Google.Review.ReviewDateXPath");
                    siGoogleReviewXPathParameters.ReviewTextFullXPath = ProviderFactory.Reviews.GetProcessorSetting("Google.Review.ReviewTextFullXPath");
                    siGoogleReviewXPathParameters.ReviewCommentParentXPath = ProviderFactory.Reviews.GetProcessorSetting("Google.Review.ReviewCommentParentXPath");
                    siGoogleReviewXPathParameters.ReviewCommentDateXPath = ProviderFactory.Reviews.GetProcessorSetting("Google.Review.ReviewCommentDateXPath");
                    siGoogleReviewXPathParameters.ReviewCommentTextXPath = ProviderFactory.Reviews.GetProcessorSetting("Google.Review.ReviewCommentTextXPath");
                    siGoogleReviewXPathParameters.ReviewAPIPlaceIDXPath = ProviderFactory.Reviews.GetProcessorSetting("Google.Review.ReviewAPIPlaceIDXPath");



                    //SIGoogleReviewConnectionParameters siGoogleReviewConnectionParameters = new SIGoogleReviewConnectionParameters(
                    //    apiRequestUrl: @"https://plus.google.com/_/pages/local/loadreviews?_reqid=1043554&rt=j",
                    //    siGoogleReviewXPathParameters: siGoogleReviewXPathParameters,
                    //    htmlRequestUrl: @"https://plus.google.com/114284807299404883225/about?gl=US&hl=en-US"
                    //);

                    SIGoogleReviewConnectionParameters siGoogleReviewConnectionParameters = new SIGoogleReviewConnectionParameters(
                        apiRequestUrl: ruDTO.ApiURL,
                        siGoogleReviewXPathParameters: siGoogleReviewXPathParameters,
                        htmlRequestUrl: ruDTO.HtmlURL
                    );
                    GoogleReviewsConnection connection = new GoogleReviewsConnection(siGoogleReviewConnectionParameters);

                    SIConnection.ConnectionStatus status = connection.GetReviewsHtml();

                    connection.PostData.Clear();

                    connection.PostData.Add("f.req", "[\"7991780716084416063\",null,[null,null,[[28,0,1000,{}],[30,null,null,{}]]],[null,null,true,true,6,true]]");
                    connection.PostData.Add("at", "AObGSAjXxm1VBhhQWAfeQkIeTA9IoGethA:1372251820988");
                    //googleConnection.PostData.Add("f.req", "[\"278050991257464660\",null,[null,null,[[28,0,1000,{}],[30,null,null,{}]]],[null,null,true,true,6,true]]");
                    //googleConnection.PostData.Add("at", "AObGSAjXxm1VBhhQWAfeQkIeTA9IoGethA:1372251820988");

                    status = connection.GetReviewsAPI();

                    //7991780716084416063  castle lookupid  114284807299404883225
                    //278050991257464660 - url 112351237584855391100

                    switch (status)
                    {
                        case SIConnection.ConnectionStatus.Success:
                            ProviderFactory.Logging.Audit(
                                AuditLevelEnum.Exception, AuditTypeEnum.ReviewDownloaderFailure, "Success",
                                new AuditReviewDownloaderSuccess() { },
                                null, ruDTO.AccountID, null, ruDTO.ID.Value, null
                            );

                            sumIDs.Add(ProviderFactory.Reviews.SaveNewRawReviewSummary(new RawReviewSummaryDTO()
                            {
                                AccountID = ruDTO.AccountID,
                                ReviewSource = ruDTO.ReviewSource,
                                SalesAverageRating = connection.SalesAverageRating <= 0 ? null : (double?)connection.SalesAverageRating,
                                ServiceAverageRating = connection.ServiceAverageRating <= 0 ? null : (double?)connection.ServiceAverageRating,
                                SalesReviewCount = connection.SalesReviewCount,
                                ServiceReviewCount = connection.ServiceReviewCount,
                                ExtraInfo = connection.ExtraInfo
                            }));

                            if (data.DownloadDetails)
                            {
                                List<RawReviewDTO> rlist = new List<RawReviewDTO>();

                                foreach (SIReviewConnection.Review review in connection.ReviewDetails)
                                {
                                    RawReviewDTO raw = new RawReviewDTO()
                                    {
                                        AccountID = ruDTO.AccountID,
                                        ReviewSource = ruDTO.ReviewSource,
                                        ExtraInfo = review.ExtraInfo,
                                        Rating = review.Rating <= 0 ? null : (double?)review.Rating,
                                        ReviewDate = review.ReviewDate,
                                        ReviewerName = review.ReviewerName,
                                        ReviewText = review.ReviewTextFull,
                                        ReviewURL = review.FullReviewURL
                                    };

                                    if (review.Comments.Count() > 0)
                                    {
                                        foreach (SIReviewConnection.ReviewComment comm in review.Comments)
                                        {
                                            raw.Comments.Add(new RawReviewCommentDTO()
                                            {
                                                Comment = comm.Comment,
                                                CommentDate = comm.CommentDate
                                            });
                                        }
                                    }

                                    rlist.Add(raw);
                                }

                                if (rlist.Count() > 0)
                                {
                                    ProviderFactory.Reviews.SaveNewRawReviews(rlist);
                                }

                            }

                            setJobStatus(job.ID.Value, JobStatusEnum.Completed);
                            break;

                        case SIConnection.ConnectionStatus.WebException:
                            ProviderFactory.Logging.Audit(
                                AuditLevelEnum.Exception, AuditTypeEnum.ReviewDownloaderFailure, "Web Exception",
                                new AuditReviewDownloaderFailure() { FailedXPaths = connection.FailedXPaths, URL = ruDTO.HtmlURL, WebException = connection.ExceptionMessage },
                                null, ruDTO.AccountID, null, ruDTO.ID.Value, null
                            );
                            setJobStatus(job.ID.Value, JobStatusEnum.Failed);
                            break;

                        case SIConnection.ConnectionStatus.InvalidData:
                            ProviderFactory.Logging.Audit(
                                AuditLevelEnum.Exception, AuditTypeEnum.ReviewDownloaderFailure, "Invalid Data",
                                new AuditReviewDownloaderFailure() { FailedXPaths = connection.FailedXPaths, URL = ruDTO.HtmlURL, WebException = connection.ExceptionMessage },
                                null, ruDTO.AccountID, null, ruDTO.ID.Value, null
                            );
                            setJobStatus(job.ID.Value, JobStatusEnum.Failed);
                            break;

                        case SIConnection.ConnectionStatus.Timeout:
                            ProviderFactory.Logging.Audit(
                                AuditLevelEnum.Exception, AuditTypeEnum.ReviewDownloaderFailure, "Timeout",
                                new AuditReviewDownloaderFailure() { FailedXPaths = connection.FailedXPaths, URL = ruDTO.HtmlURL, WebException = connection.ExceptionMessage },
                                null, ruDTO.AccountID, null, ruDTO.ID.Value, null
                            );
                            setJobStatus(job.ID.Value, JobStatusEnum.Failed);
                            break;

                        default:
                            break;
                    }

                    job = getNextJob();
                }
                catch (Exception ex)
                {
                    try
                    {
                        ProviderFactory.Logging.LogException(ex);
                        setJobStatus(job.ID.Value, JobStatusEnum.Failed);
                    }
                    catch (Exception)
                    {
                    }
                }


            }
        }
    }
}
