﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Principal;
using System.Text;
using System.Threading.Tasks;

namespace Instagram
{
    public class InstagramAPI
    {
        private string _client_id;
        private string _client_secret;

        private string _authBaseURL = "https://instagram.com/oauth/authorize/?client_id={0}&redirect_uri={1}&response_type=token";

        private string _redirectURL = "http://socialintegration.com/oAuth.html";

        public InstagramAPI()
        {
            _client_id = "c71d582fd5f6403ba6c088fb63dfb3fe";
            _client_secret = "0038dc9f0fe44cb9bd1f4332222e0f6e";
            
        }

        public InstagramAPI(string userID, string token)
        {


        }

        public string GetAuthCode()
        {
            var result = string.Empty;
            result = String.Format(_authBaseURL, _client_id, _redirectURL);
            //Generated Code = dfc20bcf5cbb4402baa7373c5aee732d
            return result;
        }

        public string GetAuthCode(string clientID, string clientSecret)
        {
            var result = string.Empty;
            result = String.Format(_authBaseURL, _client_id, _redirectURL);

            return result;
        }

        public void GetUserInfo(string userId, string token)
        {
            //https://api.instagram.com/v1/users/1574083/?access_token=1034371454.68e4164.b2492a46a3b8457fa93109dbfceb25bd
        }

        public void GetUserFeed()
        {
            //https://api.instagram.com/v1/users/66/feed?access_token=1034371454.68e4164.b2492a46a3b8457fa93109dbfceb25bd
        }

        public void GetUserFollowsList(string token)
        {
            //https://api.instagram.com/v1/users/3/follows?access_token=1034371454.f59def8.82b6856d20a84cd3b6e5ab15461d9faa
        }

        public void GetUserFollowedByList(string token)
        {
            //https://api.instagram.com/v1/users/3/followed-by?access_token=1034371454.f59def8.82b6856d20a84cd3b6e5ab15461d9faa
        }

        public void GetUserRequestedBy(string token)
        {
            //https://api.instagram.com/v1/users/self/requested-by?access_token=1034371454.f59def8.82b6856d20a84cd3b6e5ab15461d9faa
        }

        public void GetRelationshipInfo(string token, string otherUserId)
        {
            //https://api.instagram.com/v1/users/1574083/relationship?access_token=1034371454.f59def8.82b6856d20a84cd3b6e5ab15461d9faa
            //https://api.instagram.com/v1/users/1574083/relationship?access_token=1034371454.f59def8.82b6856d20a84cd3b6e5ab15461d9faa
        }

        public void GetMediaInfo(string token)
        {
            //https://api.instagram.com/v1/media/3?access_token=1034371454.f59def8.82b6856d20a84cd3b6e5ab15461d9faa
        }

        public void GetMediaComments(string token, string mediaId)
        {
            //https://api.instagram.com/v1/media/555/comments?access_token=1034371454.f59def8.82b6856d20a84cd3b6e5ab15461d9faa
        }

        public void AddMediaComment(string token, string mediaId, string comment)
        {

        }

        public void DeleteMediaComment(string token, string mediaId)
        {

        }

        public void GetMediaLikes(string token, string mediaId)
        {
            //https://api.instagram.com/v1/media/555/likes?access_token=1034371454.f59def8.82b6856d20a84cd3b6e5ab15461d9faa
        }

        public void AddMediaLike(string token, string mediaId)
        {

        }

        public void DeleteMediaLike(string token, string mediaId)
        {

        }

        public void GetTagInfo(string token, string tag)
        {
            //https://api.instagram.com/v1/tags/nofilter?access_token=1034371454.f59def8.82b6856d20a84cd3b6e5ab15461d9faa
        }

        public void GetTagInfoRecent(string token, string tag, string minId, string maxId)
        {
            //https://api.instagram.com/v1/tags/snow/media/recent?access_token=1034371454.f59def8.82b6856d20a84cd3b6e5ab15461d9faa
        }

        public void SearchTagsByName(string token, string query)
        {
            //https://api.instagram.com/v1/tags/search?q=snowy&access_token=1034371454.f59def8.82b6856d20a84cd3b6e5ab15461d9faa
        }

    }
}
