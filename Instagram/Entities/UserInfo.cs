﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Instagram.Entities
{
    public class UserInfo
    {
        public string Id { get; set; }
        public string Username { get; set; }
        public string FullName { get; set; }
        public string ProfileName { get; set; }
        public string Bio { get; set; }
        public string Website { get; set; }
        public string MediaCount { get; set; }
        public string FollowsCount { get; set; }
        public string FollowedByCount { get; set; }
    }

    public class FeedItem
    {
        public string LocationId { get; set; }
        public string LocationLatitude { get; set; }
        public string LocationLongitude { get; set; }
        public string LocationName { get; set; }
        public string CommentsCount { get; set; }
        //public string X { get; set; }
        //public string X { get; set; }
        //public string X { get; set; }
        //public string X { get; set; }
        //public string X { get; set; }
        //public string X { get; set; }
        //public string X { get; set; }
        //public string X { get; set; }
        //public string X { get; set; }
        //public string X { get; set; }
        //public string X { get; set; }
        //public string X { get; set; }
        //public string X { get; set; }
        //public string X { get; set; }

    }
}
