﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SendGrid
{
    public class SendGridEntities
    {
    }

    public class BouncedEmail
    {
        public BouncedEmail()
        {
            email = string.Empty;
            reason = string.Empty;
            status = string.Empty;
            created = null;
        }
        public string status { get; set; }
        public string email { get; set; }
        public DateTime? created { get; set; }
        public string reason { get; set; }        
    }

    public class BlockedEmail
    {
        public BlockedEmail()
        {
            email = string.Empty;
            reason = string.Empty;
            status = string.Empty;
            created = null;
        }
        public string status { get; set; }
        public string email { get; set; }
        public DateTime? created { get; set; }
        public string reason { get; set; }
    }

    public class InvalidEmail
    {
        public InvalidEmail()
        {
            email = string.Empty;
            reason = string.Empty;
            created = null;
        }
        public string reason { get; set; }
        public string email { get; set; }
        public DateTime? created { get; set; }
    }
}
