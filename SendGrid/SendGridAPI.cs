﻿using SI;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Web.Script.Serialization;

namespace SendGrid
{
    public static class SendGridAPI
    {
        #region variable
        private static int MaxRedirects = 2;
        private static string UserAgent = "Mozilla/4.0 (compatible; Windows NT)";
        private static SInet _WebNet = null;
        private static int Timeout = 60 * 1000;
        private static string sendGrid_Username = Settings.GetSetting("sendgrid.login", "SDPlatform");
        private static string sendGrid_Password = Settings.GetSetting("sendgrid.password", "sHireUnder^$7*546");

        private static string BouncesURL = "https://sendgrid.com/api/bounces.get.json?api_user={0}&api_key={1}&date=1&days={2}&start_date={3}&end_date={4}&type={5}";
        private static string DeleteBouncesURL = "https://sendgrid.com/api/bounces.delete.json";

        private static string BlockedURL = "https://sendgrid.com/api/blocks.get.json?api_user={0}&api_key={1}&date=1&start_date={2}&end_date={3}";
        private static string DeleteBlockedURL = "https://sendgrid.com/api/blocks.delete.json";

        private static string InvalidURL = "https://sendgrid.com/api/invalidemails.get.json?api_user={0}&api_key={1}&date=1&start_date={2}&end_date={3}";
        private static string DeleteInvalidURL = " 	https://sendgrid.com/api/invalidemails.delete.json";

        #endregion

        #region Functions
        
        #region Public

        public static List<BouncedEmail> GetBouncedEmails(DateTime StartDate, DateTime EndDate, SendGridBounceTypeEnum BounceType, string EmailAddress = "")
        {
            List<BouncedEmail> BouncedEmails = new List<BouncedEmail>();
            System.TimeSpan diffResult = EndDate.Subtract(StartDate);

            string URL = string.Format(BouncesURL, sendGrid_Username, sendGrid_Password, diffResult.Days,StartDate.ToString("yyyy-MM-dd"),EndDate.ToString("yyyy-MM-dd"),BounceType.ToString().ToLower());

            if (!string.IsNullOrWhiteSpace(EmailAddress))
            {
                URL = URL + "&email=" + EmailAddress;
            }
            
            string rawResponse = string.Empty;
            rawResponse = HttpRequest(URL, null, UserAgent);
            if (!string.IsNullOrEmpty(rawResponse))
            {
                var jss = new JavaScriptSerializer();
                if (!rawResponse.StartsWith(@"{""error"":"))
                {
                    BouncedEmails = jss.Deserialize<List<BouncedEmail>>(rawResponse);
                }                
            }
            
            return BouncedEmails;
        }

        public static List<BlockedEmail> GetBlockedEmails(DateTime StartDate, DateTime EndDate)
        {
            List<BlockedEmail> BlockedEmails = new List<BlockedEmail>();
            
            string URL = string.Format(BlockedURL, sendGrid_Username, sendGrid_Password, StartDate.ToString("yyyy-MM-dd"), EndDate.ToString("yyyy-MM-dd"));
                        
            string rawResponse = string.Empty;
            rawResponse = HttpRequest(URL, null, UserAgent);
            if (!string.IsNullOrEmpty(rawResponse))
            {
                var jss = new JavaScriptSerializer();
                if (!rawResponse.StartsWith(@"{""error"":"))
                {
                    BlockedEmails = jss.Deserialize<List<BlockedEmail>>(rawResponse);
                }
            }

            return BlockedEmails;
        }

        public static List<InvalidEmail> GetInvalidEmails(DateTime StartDate, DateTime EndDate, string EmailAddress = "")
        {
            List<InvalidEmail> InvalidEmails = new List<InvalidEmail>();
            
            string URL = string.Format(InvalidURL, sendGrid_Username, sendGrid_Password,  StartDate.ToString("yyyy-MM-dd"), EndDate.ToString("yyyy-MM-dd"));

            if (!string.IsNullOrWhiteSpace(EmailAddress))
            {
                URL = URL + "&email=" + EmailAddress;
            }

            string rawResponse = string.Empty;
            rawResponse = HttpRequest(URL, null, UserAgent);
            if (!string.IsNullOrEmpty(rawResponse))
            {
                var jss = new JavaScriptSerializer();
                if (!rawResponse.StartsWith(@"{""error"":"))
                {
                    InvalidEmails = jss.Deserialize<List<InvalidEmail>>(rawResponse);
                }
            }

            return InvalidEmails;
        }

        public static bool DeleteBouncedEmail(string EmailAddress, SendGridBounceTypeEnum BounceType)
        {         
            bool result = false;

            if (!string.IsNullOrEmpty(EmailAddress))
            {
                string URL = DeleteBouncesURL;

                Dictionary<string, string> PostData = new Dictionary<string, string>();

                
                PostData.Add("api_user", sendGrid_Username);
                PostData.Add("api_key", sendGrid_Password);
                PostData.Add("type", BounceType.ToString().ToLower());
                PostData.Add("email", EmailAddress);
                PostData.Add("delete_all", "0");
                

                string rawResponse = string.Empty;
                rawResponse = HttpRequest(URL, PostData, UserAgent);

                if (!string.IsNullOrEmpty(rawResponse))
                {
                    var jss = new JavaScriptSerializer();
                    if (!rawResponse.StartsWith(@"{""error"":"))
                    {
                        result = true;
                    }
                    else
                    {
                        result = false;
                    }
                }
            }

            return result;                    
        }

        public static bool DeleteBlockedEmail(string EmailAddress)
        {
            bool result = false;

            if (!string.IsNullOrEmpty(EmailAddress))
            {
                string URL = DeleteBlockedURL;

                Dictionary<string, string> PostData = new Dictionary<string, string>();
                                
                PostData.Add("api_user", sendGrid_Username);
                PostData.Add("api_key", sendGrid_Password);
                PostData.Add("email", EmailAddress);
                
                string rawResponse = string.Empty;
                rawResponse = HttpRequest(URL, PostData, UserAgent);

                if (!string.IsNullOrEmpty(rawResponse))
                {
                    var jss = new JavaScriptSerializer();
                    if (!rawResponse.StartsWith(@"{""error"":"))
                    {
                        result = true;
                    }
                    else
                    {
                        result = false;
                    }
                }
            }

            return result;
        }

        public static bool DeleteInvalidEmail(string EmailAddress)
        {
            bool result = false;

            if (!string.IsNullOrEmpty(EmailAddress))
            {
                string URL = DeleteInvalidURL;

                Dictionary<string, string> PostData = new Dictionary<string, string>();

                PostData.Add("api_user", sendGrid_Username);
                PostData.Add("api_key", sendGrid_Password);
                PostData.Add("email", EmailAddress);

                string rawResponse = string.Empty;
                rawResponse = HttpRequest(URL, PostData, UserAgent);

                if (!string.IsNullOrEmpty(rawResponse))
                {
                    var jss = new JavaScriptSerializer();
                    if (!rawResponse.StartsWith(@"{""error"":"))
                    {
                        result = true;
                    }
                    else
                    {
                        result = false;
                    }
                }
            }

            return result;
        }

        #endregion

        #region Private

        private static string HttpRequest(string requestUrl, Dictionary<string, string> PostData, string userAgent = null)
        {
            string HttpRequestResults = string.Empty;
            try
            {
                if (_WebNet == null)
                    _WebNet = new SInet(false, MaxRedirects, null, Timeout);

                if (!string.IsNullOrWhiteSpace(userAgent))
                {
                    _WebNet.UserAgent = userAgent;
                }

                if (PostData != null)
                {
                    if (PostData.Count == 0)
                        HttpRequestResults = _WebNet.DownloadString(requestUrl, null);
                    else
                        HttpRequestResults = _WebNet.DownloadString(requestUrl, PostData);
                }
                else
                    HttpRequestResults = _WebNet.DownloadString(requestUrl, null);

            }
            catch (WebException webEx)
            {
                try
                {
                    using (var reader = new StreamReader(webEx.Response.GetResponseStream()))
                    {
                        var responseText = reader.ReadToEnd();
                        HttpRequestResults = responseText;
                    }
                }
                catch (Exception ex)
                {
                    HttpRequestResults = ex.Message;

                    return HttpRequestResults;
                }
            }
            catch (Exception ex)
            {
                HttpRequestResults = ex.Message;

                return HttpRequestResults;

            }

            return HttpRequestResults;
        }

        #endregion

        #endregion

    }
}
