﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Json;
using System.Text;
using System.Threading.Tasks;
using AWS;
using Connectors;
using Connectors.Parameters;
using Facebook;
using Facebook.Entities;
using JobLib;
using Newtonsoft.Json;
using SI;
using SI.BLL;
using SI.DTO;

namespace Processor.Social.Facebook
{
    public class Processor : ProcessorBase<FacebookDownloaderData>
    {
        private const string prefix = "AWS.";

        // Need to pass the args through for the default constructor
        public Processor(string[] args) : base(args) { }

        //static void Main(string[] args)
        protected override void start()
        {

            string accessKey = ProviderFactory.Reviews.GetProcessorSetting(prefix + "AWSAccessKey");
            string secretKey = ProviderFactory.Reviews.GetProcessorSetting(prefix + "AWSSecretKey");
            string serviceURL = ProviderFactory.Reviews.GetProcessorSetting(prefix + "DynamoDBServiceURL");
            string table = ProviderFactory.Reviews.GetProcessorSetting(prefix + "DynamoDBSocialRawTable");

            DynamoDB dynamoDB = new DynamoDB(accessKey, secretKey, serviceURL, table);

            JobDTO job = getNextJob();

            while (job != null)
            {
                try
                {
                    setJobStatus(job.ID.Value, JobStatusEnum.Started);
                    FacebookDownloaderData data = getJobData(job.ID.Value);

                    SocialCredentialDTO socialCredentialsDTO = null;
                    if (data.CredentialID.HasValue)
                        socialCredentialsDTO = ProviderFactory.Social.GetSocialCredentialInfo(data.CredentialID.Value);

                    switch (data.FacebookEnum)
                    {
                        case FacebookProcessorActionEnum.FacebookPageInsightsDownloader:
                            {
                                #region PAGE INSIGHTS

                                if (socialCredentialsDTO != null)
                                {
                                    string startDate = "";
                                    string endDate = "";
                                    SetDateForFacebookInsights(ref startDate, ref endDate, socialCredentialsDTO.UniqueID, socialCredentialsDTO.Token);

                                    SIFacebookPageParameters pageParameters = new SIFacebookPageParameters();
                                    pageParameters.UniqueID = socialCredentialsDTO.UniqueID;
                                    pageParameters.Token = socialCredentialsDTO.Token;
                                    pageParameters.StartDate = startDate;
                                    pageParameters.EndDate = endDate;

                                    SIFacebookPageConnectionParameters ConnectionParameters = new SIFacebookPageConnectionParameters(pageParameters);
                                    FacebookPageConnection pageconnection = new FacebookPageConnection(ConnectionParameters);

                                    FacebookInsights objFacebookInsights = pageconnection.GetFacebookInsights();

                                    //FaceBookJsonResponse faceBookInsights = FacebookAPI.GetJsonFaceBookPageInsights(socialCredentialsDTO.UniqueID, socialCredentialsDTO.Token, startDate, endDate);

                                    if (string.IsNullOrWhiteSpace(objFacebookInsights.facebookResponse.message))
                                    {
                                        string serializeValue = JsonConvert.SerializeObject(objFacebookInsights);

                                        var dynoDbResult = dynamoDB.PutValue(serializeValue);

                                        if (dynoDbResult.IsSuccess)
                                        {
                                            FacebookIntegratorData integratorData = new FacebookIntegratorData()
                                                {
                                                    FacebookEnum = FacebookProcessorActionEnum.FacebookPageInsightsIntegrator,
                                                    Key = dynoDbResult.Key,
                                                    CredentialID = data.CredentialID.Value,
                                                    Since = startDate,
                                                    Until = endDate
                                                };

                                            JobDTO jobIntegrator = new JobDTO()
                                                {
                                                    JobDataObject = integratorData,
                                                    JobType = integratorData.JobType,
                                                    DateScheduled = new SIDateTime(DateTime.UtcNow, TimeZoneInfo.Utc),
                                                    JobStatus = JobStatusEnum.Queued,
                                                    Priority = 300,
                                                    OriginJobID = job.ID
                                                };

                                            SaveEntityDTO<JobDTO> saveInfo = ProviderFactory.Jobs.Save(new SaveEntityDTO<JobDTO>(jobIntegrator, UserInfoDTO.System));
                                            Console.WriteLine(saveInfo.Entity.ID);

                                            setJobStatus(job.ID.Value, JobStatusEnum.Completed);

                                        }
                                        else
                                        {
                                            setJobStatus(job.ID.Value, JobStatusEnum.Failed);
                                            ProviderFactory.Logging.Audit(AuditLevelEnum.Warning, AuditTypeEnum.FacebookDownloaderFailure,
                                                                          string.Format("Facebook Post could not be saved to DynamoDB, CredentialID: {0}. JobID {1}",
                                                                                         data.CredentialID, job.ID), null, null, null, data.CredentialID);
                                        }


                                    }
                                    else
                                    {
                                        //Log Facebook Error
                                    }
                                }
                                else
                                {
                                    setJobStatus(job.ID.Value, JobStatusEnum.Failed);
                                    ProviderFactory.Logging.Audit(AuditLevelEnum.Error, AuditTypeEnum.FacebookDownloaderFailure,
                                                                  string.Format("Bad Credential ID: {0}. JobID {1}",
                                                                                data.CredentialID.GetValueOrDefault(), job.ID));
                                }
                                #endregion
                            }
                            break;
                        case FacebookProcessorActionEnum.FacebookPageInsightsDownloaderHistoric:
                            {
                                #region PAGE INSIGHTS

                                if (socialCredentialsDTO != null)
                                {
                                    string startDate = data.StartDate.GetValueOrDefault().ToShortDateString();
                                    string endDate = data.EndDate.GetValueOrDefault().ToShortDateString();

                                    //SetDateForFacebookInsights(ref startDate, ref endDate, socialCredentialsDTO.UniqueID, socialCredentialsDTO.Token);

                                    SIFacebookPageParameters pageParameters = new SIFacebookPageParameters();
                                    pageParameters.UniqueID = socialCredentialsDTO.UniqueID;
                                    pageParameters.Token = socialCredentialsDTO.Token;
                                    pageParameters.StartDate = startDate;
                                    pageParameters.EndDate = endDate;

                                    SIFacebookPageConnectionParameters ConnectionParameters = new SIFacebookPageConnectionParameters(pageParameters);
                                    FacebookPageConnection pageconnection = new FacebookPageConnection(ConnectionParameters);

                                    FacebookInsights objFacebookInsights = pageconnection.GetFacebookInsights();

                                    //FaceBookJsonResponse faceBookInsights = FacebookAPI.GetJsonFaceBookPageInsights(socialCredentialsDTO.UniqueID, socialCredentialsDTO.Token, startDate, endDate);

                                    if (string.IsNullOrWhiteSpace(objFacebookInsights.facebookResponse.message))
                                    {
                                        string serializeValue = JsonConvert.SerializeObject(objFacebookInsights);

                                        var dynoDbResult = dynamoDB.PutValue(serializeValue);

                                        if (dynoDbResult.IsSuccess)
                                        {
                                            FacebookIntegratorData integratorData = new FacebookIntegratorData()
                                            {
                                                FacebookEnum = FacebookProcessorActionEnum.FacebookPageInsightsIntegrator,
                                                Key = dynoDbResult.Key,
                                                CredentialID = data.CredentialID.Value,
                                                Since = startDate,
                                                Until = endDate
                                            };

                                            JobDTO jobIntegrator = new JobDTO()
                                            {
                                                JobDataObject = integratorData,
                                                JobType = integratorData.JobType,
                                                DateScheduled = new SIDateTime(DateTime.UtcNow, TimeZoneInfo.Utc),
                                                JobStatus = JobStatusEnum.Queued,
                                                Priority = 300,
                                                OriginJobID = job.ID
                                            };

                                            SaveEntityDTO<JobDTO> saveInfo = ProviderFactory.Jobs.Save(new SaveEntityDTO<JobDTO>(jobIntegrator, UserInfoDTO.System));
                                            Console.WriteLine(saveInfo.Entity.ID);

                                            setJobStatus(job.ID.Value, JobStatusEnum.Completed);

                                        }
                                        else
                                        {
                                            setJobStatus(job.ID.Value, JobStatusEnum.Failed);
                                            ProviderFactory.Logging.Audit(AuditLevelEnum.Warning, AuditTypeEnum.FacebookDownloaderFailure,
                                                                          string.Format("FacebookPageInsightsDownloaderHistoric could not be saved to DynamoDB, CredentialID: {0}. JobID {1}",
                                                                                         data.CredentialID, job.ID), null, null, null, data.CredentialID);
                                        }


                                    }
                                    else
                                    {
                                        //Log Facebook Error
                                        setJobStatus(job.ID.Value, JobStatusEnum.Failed);
                                    }
                                }
                                else
                                {
                                    setJobStatus(job.ID.Value, JobStatusEnum.Failed);
                                    ProviderFactory.Logging.Audit(AuditLevelEnum.Error, AuditTypeEnum.FacebookDownloaderFailure,
                                                                  string.Format("Bad Credential ID: {0}. JobID {1}",
                                                                                data.CredentialID.GetValueOrDefault(), job.ID));
                                }
                                #endregion
                            }
                            break;
                        case FacebookProcessorActionEnum.FacebookPostDownLoader:
                            {
                                #region post downloader

                                if (socialCredentialsDTO != null)
                                {
                                    SIFacebookPageParameters pageParameters = new SIFacebookPageParameters();
                                    pageParameters.UniqueID = socialCredentialsDTO.UniqueID;
                                    pageParameters.Token = socialCredentialsDTO.Token;

                                    int NumberOfDaysBack = data.NumberOfDaysBack.GetValueOrDefault(-1) * -1;
                                    List<InsightsDates> listInsightsDates = new List<InsightsDates>();
                                    
                                    if (NumberOfDaysBack > 60)
                                    {                                        
                                        while (NumberOfDaysBack > 0)
                                        {
                                            InsightsDates obj = new InsightsDates
                                            {
                                                start = NumberOfDaysBack,
                                                end = NumberOfDaysBack - 60
                                            };
                                            listInsightsDates.Add(obj);

                                            NumberOfDaysBack = NumberOfDaysBack - 61;

                                            if (NumberOfDaysBack - 60 < 0)
                                            {
                                                obj = new InsightsDates
                                                {
                                                    start = NumberOfDaysBack,
                                                    end = 0
                                                };
                                                //Debug.WriteLine(obj.start + "-" + obj.end);
                                                listInsightsDates.Add(obj);
                                                break;
                                            }
                                        }
                                    }
                                    else
                                    {
                                        InsightsDates obj = new InsightsDates
                                        {
                                            start = NumberOfDaysBack,
                                            end = 0
                                        };
                                        listInsightsDates.Add(obj);

                                        //Debug.WriteLine(obj.start + "-" + obj.end);
                                        //FacebookFeed facebookFeed = pageconnection.GetFacebookFeed();
                                    }

                                    SIFacebookPageConnectionParameters ConnectionParameters = new SIFacebookPageConnectionParameters(pageParameters);
                                    FacebookPageConnection pageconnection = new FacebookPageConnection(ConnectionParameters);
                                    FacebookFeed facebookFeed = new FacebookFeed();

                                    foreach (var item in listInsightsDates)
                                    {
                                        //pageParameters.StartDate = DateTime.Now.AddDays(data.NumberOfDaysBack.GetValueOrDefault(-1)).ToShortDateString(); //normal 2 days
                                        //pageParameters.EndDate = DateTime.Now.ToShortDateString();

                                        pageParameters.StartDate = DateTime.Now.AddDays(item.start * -1).ToShortDateString(); //normal 2 days
                                        pageParameters.EndDate = DateTime.Now.AddDays(item.end * -1).ToShortDateString();

                                        FacebookFeed FBFeed = pageconnection.GetFacebookFeed();

                                        if (string.IsNullOrEmpty(FBFeed.facebookResponse.message))
                                        {
                                            facebookFeed.facebookResponse = FBFeed.facebookResponse;

                                            foreach (var feed in FBFeed.feed.data)
                                            {
                                                facebookFeed.feed.data.Add(feed);
                                            }
                                        }
                                        Debug.WriteLine(pageParameters.StartDate + "-" + pageParameters.EndDate);
                                    }

                                    if (string.IsNullOrEmpty(facebookFeed.facebookResponse.message))
                                    {
                                        int iCounter = 1;
                                        foreach (DataFeed feed in facebookFeed.feed.data)
                                        {
                                            //ToDo Add dynmoDB 

                                            //Insert/Update Post Detail to Facebook Post Table
                                            string feedid = string.Empty;
                                            string resultid = string.Empty;

                                            if (string.IsNullOrWhiteSpace(feed.object_id))
                                            {
                                                resultid = feed.id;
                                                feedid = string.Empty;
                                            }
                                            else
                                            {
                                                resultid = feed.object_id;
                                                feedid = feed.id;
                                            }

                                            if (string.IsNullOrEmpty(feedid))
                                            {
                                                feedid = resultid;
                                            }
                                            
                                            string linkURL = string.Empty;
                                            string pictureURL = string.Empty;
                                            PostTypeEnum PostType = FacebookAPI.GetPostType(feed.type.ToLower());

                                            if (PostType == PostTypeEnum.Link)
                                            {
                                                linkURL = feed.link;
                                            }

                                            if (PostType == PostTypeEnum.Photo)
                                            {
                                                pictureURL = feed.picture;
                                            }

                                            if (PostType == PostTypeEnum.Video)
                                            {
                                                linkURL = feed.link;
                                            }

                                            if (PostType == PostTypeEnum.Offer)
                                            {
                                                linkURL = feed.link;
                                            }

                                            if (string.IsNullOrWhiteSpace(feed.message))
                                            {
                                                feed.message = feed.caption;
                                            }

                                            if (string.IsNullOrWhiteSpace(feed.message))
                                            {
                                                feed.message = feed.name;
                                            }

                                            Debug.WriteLine(iCounter + " ResultID :" + resultid + " ,Date: " + feed.created_time);

                                            bool Success = ProviderFactory.Social.SaveFaceBookPost(data.CredentialID.Value, feedid, resultid, feed.name, feed.message, PostType, linkURL, pictureURL, feed.created_time.ToUniversalTime(), feed.updated_time.ToUniversalTime(), feed.status_type);
                                            iCounter++;
                                            if (!Success)
                                            {
                                                ProviderFactory.Logging.Audit(AuditLevelEnum.Warning, AuditTypeEnum.FacebookIntegratorFailure,
                                                                              string.Format("Could not save FaceBookPost To Facebook Post table ResultID: {0}, FeedID: {2}. JobID {1}", resultid, job.ID, feedid), null, null, null, data.CredentialID);
                                            }
                                            else
                                            {
                                                setJobStatus(job.ID.Value, JobStatusEnum.Completed);
                                            }

                                        }


                                    }
                                    else
                                    {
                                        setJobStatus(job.ID.Value, JobStatusEnum.Failed);
                                        ProviderFactory.Logging.Audit(AuditLevelEnum.Warning, AuditTypeEnum.FacebookDownloaderFailure,
                                                                      string.Format("Facebook Feed could not be retrieved from Facebook, CredentialID: {0}. JobID {1}. Response: {2}",
                                                                                    data.CredentialID, job.ID, facebookFeed.facebookResponse.message), null, null, null, data.CredentialID);
                                    }
                                }
                                else
                                {
                                    setJobStatus(job.ID.Value, JobStatusEnum.Failed);
                                    ProviderFactory.Logging.Audit(AuditLevelEnum.Error, AuditTypeEnum.FacebookDownloaderFailure,
                                                                  string.Format("Bad Credential ID: {0}. JobID {1}",
                                                                                data.CredentialID.GetValueOrDefault(), job.ID));
                                }
                                break;

                                #endregion
                            }

                        case FacebookProcessorActionEnum.FacebookPageInformationDownloader:
                            {
                                #region PAGE BASIC INFORMATION

                                if (socialCredentialsDTO != null)
                                {
                                    FaceBookJsonResponse faceBookJsonResponse = FacebookAPI.GetJsonFaceBookPage(socialCredentialsDTO.UniqueID, socialCredentialsDTO.Token);

                                    if (faceBookJsonResponse.Success)
                                    {
                                        var dynoDbResult = dynamoDB.PutValue(faceBookJsonResponse.ResponseJson);
                                        if (dynoDbResult.IsSuccess)
                                        {
                                            FacebookIntegratorData integratorData = new FacebookIntegratorData()
                                                {
                                                    FacebookEnum = FacebookProcessorActionEnum.FacebookPageIntegrator,
                                                    Key = dynoDbResult.Key,
                                                    CredentialID = data.CredentialID.Value
                                                };

                                            JobDTO jobIntegrator = new JobDTO()
                                                {
                                                    JobDataObject = integratorData,
                                                    JobType = integratorData.JobType,
                                                    DateScheduled = new SIDateTime(DateTime.UtcNow, TimeZoneInfo.Utc),
                                                    JobStatus = JobStatusEnum.Queued,
                                                    Priority = 300,
                                                    OriginJobID = job.ID
                                                };

                                            SaveEntityDTO<JobDTO> saveInfo = ProviderFactory.Jobs.Save(new SaveEntityDTO<JobDTO>(jobIntegrator, UserInfoDTO.System));
                                            Console.WriteLine(saveInfo.Entity.ID);

                                            setJobStatus(job.ID.Value, JobStatusEnum.Completed);

                                        }
                                        else
                                        {
                                            setJobStatus(job.ID.Value, JobStatusEnum.Failed);
                                            ProviderFactory.Logging.Audit(AuditLevelEnum.Warning, AuditTypeEnum.FacebookDownloaderFailure,
                                                                          string.Format("Facebook Page could not be saved to DynamoDB, CredentialID: {0}. JobID {1}",
                                                                                         data.CredentialID, job.ID), null, null, null, data.CredentialID);
                                        }
                                    }
                                    else
                                    {
                                        setJobStatus(job.ID.Value, JobStatusEnum.Failed);
                                        ProviderFactory.Logging.Audit(AuditLevelEnum.Warning, AuditTypeEnum.FacebookDownloaderFailure,
                                                                      string.Format("Facebook Page could not be retrieved from Facebook, CredentialID: {0}. JobID {1}. Response: {2}",
                                                                                    data.CredentialID, job.ID, faceBookJsonResponse.ResponseJson), null, null, null, data.CredentialID);
                                    }
                                }
                                else
                                {
                                    setJobStatus(job.ID.Value, JobStatusEnum.Failed);
                                    ProviderFactory.Logging.Audit(AuditLevelEnum.Error, AuditTypeEnum.FacebookDownloaderFailure,
                                                                  string.Format("Bad Credential ID: {0}. JobID {1}",
                                                                                data.CredentialID.GetValueOrDefault(), job.ID));
                                }

                                #endregion
                            }
                            break;
                        case FacebookProcessorActionEnum.FacebookPostStatisticsDownloader:
                            {
                                // we should mark the post's DateLastRefreshed
                                ProviderFactory.Social.MarkFacebookPostRefreshed(data.PostID);

                                #region POST STATISTICS (Basic/Insights)

                                //PostTargetPublishInfoDTO info = ProviderFactory.Social.GetPostTargetPublishInfo(data.PostTargetID, data.PostImageID);
                                FacebookPostInfoDTO info = ProviderFactory.Social.GetFacebookPostInfo(data.PostID);

                                socialCredentialsDTO = info.Credential;

                                SIFacebookPostStatisticsParameters postParameters = new SIFacebookPostStatisticsParameters();

                                postParameters.Token = socialCredentialsDTO.Token;
                                postParameters.UniqueID = socialCredentialsDTO.UniqueID;
                                postParameters.Posttype = info.PostType;

                                SIFacebookPostStatisticsConnectionParameters postConnectionParameters = new SIFacebookPostStatisticsConnectionParameters(postParameters);
                                FacebookPostStatisticConnection postconnection = new FacebookPostStatisticConnection(postConnectionParameters);

                                #region Post Basic
                                postParameters.isSharedStory = false; 
                                if (!string.IsNullOrEmpty(info.ResultID))
                                {
                                    if (!string.IsNullOrEmpty(info.StatusType))
                                    {
                                        if (info.StatusType.ToLower() == "shared_story".ToLower())
                                        {
                                            postParameters.ResultID = info.FeedID;
                                            postParameters.isSharedStory = true; 
                                        }
                                        else
                                        {
                                            postParameters.ResultID = info.ResultID;
                                        }
                                    }
                                    else
                                    {
                                        postParameters.ResultID = info.ResultID;
                                    }

                                    //postParameters.ResultID = info.ResultID;

                                    FacebookPostStatistics objFacebookPostStatistics = postconnection.GetFacebookPostStatistics();

                                    //Save to dynoDb if objFacebookPostStatistics.facebookResponse.message = string.empty
                                    if (string.IsNullOrWhiteSpace(objFacebookPostStatistics.facebookResponse.message))
                                    {
                                        //save Facebook Page Insights to DynamoDB from SocialDealer AWS SDK
                                        string serializeValue = JsonConvert.SerializeObject(objFacebookPostStatistics);

                                        var dynoDbResult = dynamoDB.PutValue(serializeValue);

                                        #region Save to DynamoDB & Queue Next job

                                        if (dynoDbResult.IsSuccess)
                                        {
                                            FacebookIntegratorData integratorData = new FacebookIntegratorData()
                                            {
                                                FacebookEnum = FacebookProcessorActionEnum.FacebookPostIntegrator,
                                                Key = dynoDbResult.Key,
                                                CredentialID = info.CredentialID,
                                                FeedID = info.FeedID,
                                                ResultID = info.ResultID,
                                                FacebookPostID = data.PostID
                                            };

                                            JobDTO jobIntegrator = new JobDTO()
                                            {
                                                JobDataObject = integratorData,
                                                JobType = integratorData.JobType,
                                                DateScheduled = new SIDateTime(DateTime.UtcNow, TimeZoneInfo.Utc),
                                                JobStatus = JobStatusEnum.Queued,
                                                Priority = 300,
                                                OriginJobID = job.ID
                                            };

                                            SaveEntityDTO<JobDTO> saveInfo = ProviderFactory.Jobs.Save(new SaveEntityDTO<JobDTO>(jobIntegrator, UserInfoDTO.System));
                                        }
                                        else
                                        {
                                            setJobStatus(job.ID.Value, JobStatusEnum.Failed);
                                            ProviderFactory.Logging.Audit(AuditLevelEnum.Warning, AuditTypeEnum.FacebookDownloaderFailure,
                                                                          string.Format("Facebook Post could not be saved to DynamoDB, ResultID: {0}, CredentialID: {1}. JobID {2}",
                                                                                        info.ResultID, info.CredentialID, job.ID), null, null, null, info.CredentialID);
                                        }

                                        #endregion

                                    }
                                    else
                                    {
                                        ProviderFactory.Logging.Audit(AuditLevelEnum.Error, AuditTypeEnum.FacebookPostStatisticsDownloaderFailure,
                                                                        string.Format("Not able to get Facebook PostStatistics(Post Might Deleted Facebook) Message : {0}. JobID {1}", objFacebookPostStatistics.facebookResponse.ToString(), job.ID));
                                    }
                                    //var obj = dynamoDB.GetValue(result.Key);
                                    //FacebookPostStatistics objFacebookPostStatisticsNew = JsonConvert.DeserializeObject<FacebookPostStatistics>(obj.Value);
                                }
                                #endregion

                                #region Post Insights
                                if (!string.IsNullOrEmpty(info.FeedID))
                                {
                                    postParameters.FeedID = info.FeedID;

                                    FacebookInsights objFacebookInsights = postconnection.GetFacebookPostInsights();
                                    
                                    //Save to dynoDb if objFacebookPostStatistics.facebookResponse.message = string.empty
                                    if (string.IsNullOrWhiteSpace(objFacebookInsights.facebookResponse.message))
                                    {
                                        //save Facebook Page Insights to DynamoDB from SocialDealer AWS SDK
                                        string serializeValue = JsonConvert.SerializeObject(objFacebookInsights);

                                        var dynoDbResult = dynamoDB.PutValue(serializeValue);

                                        #region Save to DynamoDB & Queue Next job
                                        if (dynoDbResult.IsSuccess)
                                        {
                                            FacebookIntegratorData integratorData = new FacebookIntegratorData()
                                            {
                                                FacebookEnum = FacebookProcessorActionEnum.FacebookPostInsightsIntegrator,
                                                Key = dynoDbResult.Key,
                                                CredentialID = info.CredentialID,
                                                FeedID = info.FeedID,
                                                ResultID = info.ResultID,
                                                FacebookPostID = data.PostID
                                            };

                                            JobDTO jobIntegrator = new JobDTO()
                                            {
                                                JobDataObject = integratorData,
                                                JobType = integratorData.JobType,
                                                DateScheduled = new SIDateTime(DateTime.UtcNow, TimeZoneInfo.Utc),
                                                JobStatus = JobStatusEnum.Queued,
                                                Priority = 300,
                                                OriginJobID = job.ID
                                            };

                                            SaveEntityDTO<JobDTO> saveInfo = ProviderFactory.Jobs.Save(new SaveEntityDTO<JobDTO>(jobIntegrator, UserInfoDTO.System));

                                        }
                                        else
                                        {
                                            setJobStatus(job.ID.Value, JobStatusEnum.Failed);
                                            ProviderFactory.Logging.Audit(AuditLevelEnum.Warning, AuditTypeEnum.FacebookDownloaderFailure,
                                                                          string.Format("Facebook Post Insights could not be saved to DynamoDB, FeedID: {0}, CredentialID: {1}. JobID {2}",
                                                                                        info.FeedID, info.CredentialID, job.ID), null, null, null, info.CredentialID);
                                        }

                                        #endregion

                                    }
                                    else
                                    {
                                        ProviderFactory.Logging.Audit(AuditLevelEnum.Error, AuditTypeEnum.FacebookPostStatisticsDownloaderFailure,
                                                                        string.Format("Not able to get Facebook Post Insights(Post Might Deleted Facebook) Message : {0}. JobID {1}", objFacebookInsights.facebookResponse.ToString(), job.ID));
                                    }
                                    //var obj = dynamoDB.GetValue(result.Key);
                                    //FacebookInsights objFacebookInsightsNew = JsonConvert.DeserializeObject<FacebookInsights>(obj.Value);
                                }
                                #endregion

                                #endregion

                                setJobStatus(job.ID.Value, JobStatusEnum.Completed);
                            }
                            break;
                        case FacebookProcessorActionEnum.FacebookPublish:
                            {
                                #region Publish

                                Auditor
                                    .New(AuditLevelEnum.Information, AuditTypeEnum.PublishActivity, UserInfoDTO.System, "")
                                    .SetAuditDataObject(
                                        AuditPublishActivity
                                        .New(data.PostID, AuditPublishActivityTypeEnum.PublishProcessorEntry)
                                        .SetPostID(data.PostID)
                                        .SetPostTargetID(data.PostTargetID)
                                        .SetPostImageID(data.PostImageID)
                                        .SetJobID(job.ID)
                                    )
                                    .Save(ProviderFactory.Logging)
                                ;

                                string PublishImageUploadBaseURL = Settings.GetSetting("site.PublishImageUploadURL");
                                PostTargetPublishInfoDTO info = ProviderFactory.Social.GetPostTargetPublishInfo(data.PostTargetID, data.PostImageID);
                                socialCredentialsDTO = ProviderFactory.Social.GetSocialCredentialInfo(info.CredentialID);

                                SIFacebookPublishParameters publishParameters = new SIFacebookPublishParameters();
                                publishParameters.UniqueID = info.UniqueID;
                                publishParameters.Token = info.Token;
                                publishParameters.message = info.Message;
                                publishParameters.caption = string.Empty;
                                publishParameters.description = info.LinkDescription;
                                publishParameters.name = info.Title;
                                publishParameters.source = string.Empty;
                                publishParameters.targeting = "{'countries':''}";
                                if (info.PostType == PostTypeEnum.Photo)
                                {
                                    publishParameters.picture = PublishImageUploadBaseURL + info.ImageURL;
                                    //publishParameters.picture = string.IsNullOrWhiteSpace(info.RevisionImageURL) ? PublishImageUploadBaseURL + info.ImageURL : PublishImageUploadBaseURL + info.RevisionImageURL;
                                }
                                else if (info.PostType == PostTypeEnum.Link)
                                {
                                    if (!string.IsNullOrWhiteSpace(info.RevisionImageURL))
                                    {
                                        publishParameters.picture = info.RevisionImageURL;
                                    }
                                    else
                                    {
                                        publishParameters.picture = string.Empty;
                                    }
                                    //publishParameters.picture = string.IsNullOrWhiteSpace(info.RevisionImageURL) ? PublishImageUploadBaseURL + info.ImageURL : PublishImageUploadBaseURL + info.RevisionImageURL;
                                }
                                else
                                {
                                    publishParameters.picture = string.Empty;
                                }

                                //if (data.PostImageID.HasValue)
                                //{
                                //    publishParameters.picture = string.IsNullOrWhiteSpace(info.RevisionImageURL) ? PublishImageUploadBaseURL + info.ImageURL : PublishImageUploadBaseURL + info.RevisionImageURL;
                                //}
                                //else
                                //{
                                //    publishParameters.picture = string.Empty;
                                //}

                                publishParameters.link = info.Link;

                                SIFacebookPublishConnectionParameters publishConnectionParameters = new SIFacebookPublishConnectionParameters(publishParameters);
                                FacebookPublishConnection publishconnection = new FacebookPublishConnection(publishConnectionParameters);

                                switch (info.PostType)
                                {
                                    case PostTypeEnum.Status:
                                        {
                                            #region Status

                                            ProviderFactory.Social.SetPostTargetJobID(data.PostTargetID, job.ID.Value);
                                            FacebookResponse objFacebookResponse = publishconnection.CreateWallPost();

                                            if (!string.IsNullOrWhiteSpace(objFacebookResponse.ID))
                                            {
                                                ProviderFactory.Social.SetPostTargetResults(data.PostTargetID, objFacebookResponse.ID, null);
                                                setJobStatus(job.ID.Value, JobStatusEnum.Completed);

                                                QueueFacebookNewPostJob(info.CredentialID, job.ID.Value);
                                            }
                                            else
                                            {
                                                Auditor
                                                    .New(AuditLevelEnum.Error, AuditTypeEnum.PublishFailure, new UserInfoDTO(info.UserID, null), objFacebookResponse.ToString())
                                                    .SetAccountID(socialCredentialsDTO.AccountID)
                                                    .SetCredentialID(info.CredentialID)
                                                    .SetAuditDataObject(new AuditPublishFailure()
                                                    {
                                                        JobID = job.ID.Value,
                                                        PostImageID = data.PostImageID.GetValueOrDefault(0),
                                                        PostTargetID = data.PostTargetID,
                                                        SocialNetworkResponse = objFacebookResponse.ToString(),
                                                        CredentialID = socialCredentialsDTO != null ? socialCredentialsDTO.ID : 0,
                                                        AccountID = socialCredentialsDTO != null ? socialCredentialsDTO.AccountID : 0,
                                                        SocialAppID = socialCredentialsDTO != null ? socialCredentialsDTO.SocialAppID : 0,
                                                        SocialNetworkID = socialCredentialsDTO != null ? socialCredentialsDTO.SocialNetworkID : 0
                                                    })
                                                    .Save(ProviderFactory.Logging)
                                                ;

                                                //if (data.RemainingRetries == 0)
                                                {
                                                    // set failed in post target
                                                    ProviderFactory.Social.SetPostTargetFailed(data.PostTargetID);
                                                }
                                                setJobStatus(job.ID.Value, JobStatusEnum.Failed);
                                            }

                                            #endregion
                                        }
                                        break;
                                    case PostTypeEnum.Link:
                                        {
                                            #region Link

                                            publishParameters.name = info.LinkTitle;

                                            ProviderFactory.Social.SetPostTargetJobID(data.PostTargetID, job.ID.Value);
                                            FacebookResponse objFacebookResponse = publishconnection.CreateWallPost();

                                            if (!string.IsNullOrWhiteSpace(objFacebookResponse.ID))
                                            {
                                                ProviderFactory.Social.SetPostTargetResults(data.PostTargetID, objFacebookResponse.ID, null);
                                                setJobStatus(job.ID.Value, JobStatusEnum.Completed);

                                                QueueFacebookNewPostJob(info.CredentialID, job.ID.Value);
                                            }
                                            else
                                            {
                                                //UserDTO userDTO = ProviderFactory.Security.GetUserByID(info.UserID, null);


                                                Auditor
                                                    .New(AuditLevelEnum.Error, AuditTypeEnum.PublishFailure, new UserInfoDTO(info.UserID, null), objFacebookResponse.ToString())
                                                    .SetAccountID(socialCredentialsDTO.AccountID)
                                                    .SetCredentialID(info.CredentialID)
                                                    .SetAuditDataObject(new AuditPublishFailure()
                                                    {
                                                        JobID = job.ID.Value,
                                                        PostImageID = data.PostImageID.GetValueOrDefault(0),
                                                        PostTargetID = data.PostTargetID,
                                                        SocialNetworkResponse = objFacebookResponse.ToString(),
                                                        CredentialID = socialCredentialsDTO != null ? socialCredentialsDTO.ID : 0,
                                                        AccountID = socialCredentialsDTO != null ? socialCredentialsDTO.AccountID : 0,
                                                        SocialAppID = socialCredentialsDTO != null ? socialCredentialsDTO.SocialAppID : 0,
                                                        SocialNetworkID = socialCredentialsDTO != null ? socialCredentialsDTO.SocialNetworkID : 0
                                                    })
                                                    .Save(ProviderFactory.Logging);

                                                //if (data.RemainingRetries == 0)
                                                {
                                                    // set failed in post target
                                                    ProviderFactory.Social.SetPostTargetFailed(data.PostTargetID);
                                                }

                                                setJobStatus(job.ID.Value, JobStatusEnum.Failed);
                                            }


                                            #endregion
                                        }
                                        break;
                                    case PostTypeEnum.Photo:
                                        {

                                            #region Photo

                                            ProviderFactory.Social.SetPostImageJobID(data.PostImageID.Value, data.PostTargetID, job.ID.Value);
                                            FacebookResponse objFacebookResponse = publishconnection.UploadPhoto();

                                            if (!string.IsNullOrWhiteSpace(objFacebookResponse.ID))
                                            {
                                                ProviderFactory.Social.SetPostImageResults(data.PostImageID.Value, data.PostTargetID, objFacebookResponse.ID, null);
                                                ProviderFactory.Social.SetPostTargetResults(data.PostTargetID);

                                                setJobStatus(job.ID.Value, JobStatusEnum.Completed);

                                                QueueFacebookNewPostJob(info.CredentialID, job.ID.Value);
                                            }
                                            else
                                            {
                                                Auditor
                                                    .New(AuditLevelEnum.Error, AuditTypeEnum.PublishFailure, new UserInfoDTO(info.UserID, null), objFacebookResponse.ToString())
                                                    .SetAccountID(socialCredentialsDTO.AccountID)
                                                    .SetCredentialID(info.CredentialID)
                                                    .SetAuditDataObject(new AuditPublishFailure()
                                                    {
                                                        JobID = job.ID.Value,
                                                        PostImageID = data.PostImageID.GetValueOrDefault(0),
                                                        PostTargetID = data.PostTargetID,
                                                        SocialNetworkResponse = objFacebookResponse.ToString(),
                                                        CredentialID = socialCredentialsDTO != null ? socialCredentialsDTO.ID : 0,
                                                        AccountID = socialCredentialsDTO != null ? socialCredentialsDTO.AccountID : 0,
                                                        SocialAppID = socialCredentialsDTO != null ? socialCredentialsDTO.SocialAppID : 0,
                                                        SocialNetworkID = socialCredentialsDTO != null ? socialCredentialsDTO.SocialNetworkID : 0
                                                    })
                                                    .Save(ProviderFactory.Logging);

                                                //if (data.RemainingRetries == 0)
                                                {
                                                    // set failed in post target
                                                    ProviderFactory.Social.SetPostImageResultFailed(data.PostImageID.Value, data.PostTargetID);
                                                    ProviderFactory.Social.SetPostTargetFailed(data.PostTargetID);
                                                }

                                                setJobStatus(job.ID.Value, JobStatusEnum.Failed);
                                            }


                                            #endregion
                                        }
                                        break;
                                    case PostTypeEnum.Event:
                                        {
                                            #region Event
                                            #endregion
                                        }
                                        break;

                                }

                                #endregion
                            }
                            break;
                        case FacebookProcessorActionEnum.DeletePostsAndPageInfo:
                            {
                                #region Delete

                                ProviderFactory.Social.DeleteSocialData(socialCredentialsDTO.ID);
                                
                                #endregion
                            }
                            break;
                        //case FacebookProcessorActionEnum.FacebookPageReviewDownloader:
                        //    {
                        //        #region Review
                        //        if (socialCredentialsDTO != null)
                        //        {
                        //            SIReviewConnection connection = ProviderFactory.Reviews.GetReviewConnection(ReviewSourceEnum.Facebook);
                        //            //ReviewDownloadResult result = connection.Scrape(new ReviewUrlDTO(), true);

                        //            //SIFacebookReviewXPathParemeters reviewXPathParameters = new SIFacebookReviewXPathParemeters();
                        //            //reviewXPathParameters.UniqueID = socialCredentialsDTO.UniqueID;
                        //            //reviewXPathParameters.Token = socialCredentialsDTO.Token;

                        //            //SIFacebookReviewConnectionParameters ConnectionParameters = new SIFacebookReviewConnectionParameters(reviewXPathParameters);
                        //            //FacebookReviewsConnection reviewconnection = new FacebookReviewsConnection(ConnectionParameters);

                        //            //FacebookReviewResponse facebookReviewResponse = reviewconnection.GetFacebookReviewsFromAPI();
                        //        }

                        //        #endregion  
                        //    }
                        //    break;
                    }
                }
                catch (Exception ex)
                {
                    setJobStatus(job.ID.Value, JobStatusEnum.Failed);
                    ProviderFactory.Logging.LogException(ex);
                    ProviderFactory.Logging.Audit(AuditLevelEnum.Exception, AuditTypeEnum.ProcessorException, ex.Message, null, null, null, null, null, null);
                }

                job = getNextJob();
            }

        }

        private static void SetDateForFacebookInsights(ref string startDate, ref string endDate, string uniqueID, string token)
        {
            DateTime _date = DateTime.Now.AddDays(-1);

            List<InsightsDateWindow> insightsDateWindows = new List<InsightsDateWindow>();

            int lastCount = -1;
            for (int iCounter = 0; iCounter > -15; iCounter--)
            {
                DateTime TempDate = _date.AddDays(iCounter);

                var fb_startDate = TempDate.ToShortDateString() + " 00:00:00";
                var fb_endDate = TempDate.ToShortDateString() + " 23:59:59";

                SIFacebookPageParameters pageParameters = new SIFacebookPageParameters();

                pageParameters.UniqueID = uniqueID;
                pageParameters.Token = token;
                pageParameters.StartDate = fb_startDate;
                pageParameters.EndDate = fb_endDate;
                pageParameters.Periods = "day,lifetime,week,days_28";

                SIFacebookPageConnectionParameters ConnectionParameters = new SIFacebookPageConnectionParameters(pageParameters);
                FacebookPageConnection pageconnection = new FacebookPageConnection(ConnectionParameters);

                FacebookInsights facebookInsights = pageconnection.GetFacebookInsights();

                if (string.IsNullOrEmpty(facebookInsights.facebookResponse.code))
                {
                    InsightsDateWindow window = new InsightsDateWindow();
                    window.startDate = fb_startDate;
                    window.endDate = fb_endDate;
                    window.count = facebookInsights.insights.data.Count;
                    insightsDateWindows.Add(window);

                    if (facebookInsights.insights.data.Count >= 180 || (facebookInsights.insights.data.Count > 0 && facebookInsights.insights.data.Count == lastCount))
                        break;

                    lastCount = facebookInsights.insights.data.Count;
                }
            }

            InsightsDateWindow bestWindow = insightsDateWindows.OrderByDescending(w => w.count).FirstOrDefault();
            if (bestWindow != null)
            {
                startDate = bestWindow.startDate;
                endDate = bestWindow.endDate;
            }
            else
            {
                startDate = string.Format("{0} 00:00:00", _date.ToShortDateString());
                endDate = string.Format("{0} {1}:00:00", DateTime.Now.ToShortDateString(), DateTime.Now.Hour);
            }

        }

        private void QueueFacebookNewPostJob(long credentialID, long OriginJobID)
        {
            try
            {
                #region FacebookPost
                SocialCredentialDTO cred = ProviderFactory.Social.GetSocialCredentialInfo(credentialID);
                int NumberOfDaysBack = 1;

                if (cred != null)
                {
                    FacebookDownloaderData facebookDownloaderData = new FacebookDownloaderData();

                    facebookDownloaderData.CredentialID = cred.ID;
                    facebookDownloaderData.FacebookEnum = FacebookProcessorActionEnum.FacebookPostDownLoader;
                    facebookDownloaderData.NumberOfDaysBack = NumberOfDaysBack * -1;

                    JobDTO job1 = new JobDTO()
                    {
                        JobDataObject = facebookDownloaderData,
                        JobType = facebookDownloaderData.JobType,
                        DateScheduled = new SIDateTime(DateTime.UtcNow, TimeZoneInfo.Utc),
                        JobStatus = JobStatusEnum.Queued,
                        Priority = 200,
                        OriginJobID = OriginJobID
                    };
                                        
                    SaveEntityDTO<JobDTO> saveInfo = ProviderFactory.Jobs.Save(new SaveEntityDTO<JobDTO>(job1, UserInfoDTO.System));
                }

                #endregion
            }
            catch(Exception ex)
            {
                ProviderFactory.Logging.LogException(ex);
                ProviderFactory.Logging.Audit(AuditLevelEnum.Exception, AuditTypeEnum.ProcessorException, ex.Message, null, null, null, null, null, null);
            }
        }

        private class InsightsDates
        {
            public int start { get; set; }
            public int end { get; set; }
        }

        private class InsightsDateWindow
        {
            public string startDate { get; set; }
            public string endDate { get; set; }
            public int count { get; set; }
        }

    }


}
