﻿
using System;
using System.Collections.Generic;
using System.Data.EntityClient;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace SI.DAL
{
    public static class ContextFactory
    {

        private static int commandTimeoutSeconds = 2500;

        public static SystemDBEntities System
        {
            get
            {
                SystemDBEntities entities = new SystemDBEntities(Settings.GetSetting("connectionstring.system"));
                entities.CommandTimeout = commandTimeoutSeconds;
                return entities;
            }
        }

        public static MainEntities Main
        {
            get
            {
                MainEntities entities = new MainEntities(Settings.GetSetting("connectionstring.main"));
                entities.CommandTimeout = commandTimeoutSeconds;
                return entities;
            }
        }

        public static PMDWEntities Reporting
        {
            get
            {
                PMDWEntities entities = new PMDWEntities(Settings.GetSetting("connectionstring.pmdw"));
                entities.CommandTimeout = commandTimeoutSeconds;
                return entities;
            }
        }

        public static ImportEntities Import
        {
            get
            {
                ImportEntities entities = new ImportEntities(Settings.GetSetting("connectionstring.import"));
                entities.CommandTimeout = commandTimeoutSeconds;
                return entities;
            }
        }

        public static SDReviewEntities SDReviews
        {
            get
            {
                SDReviewEntities entities = new SDReviewEntities(Settings.GetSetting("connectionstring.sdreviews"));
                return entities;
            }
        }

        public static SocialDealerEntities SocialDealer
        {
            get
            {
                SocialDealerEntities entities = new SocialDealerEntities(Settings.GetSetting("connectionstring.socialdealer"));
                return entities;
            }
        }

        //public static NotificationEntities Notification
        //{
        //    get
        //    {
        //        NotificationEntities entities = new NotificationEntities(Settings.GetSetting("connectionstring.notification"));
        //        return entities;
        //    }
        //}
    }
}
