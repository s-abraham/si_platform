﻿using JobLib;
using SI.BLL;
using SI.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Processor.Audit
{
    public class Processor : ProcessorBase<AuditProcessorData>
    {
        // Need to pass the args through for the default constructor
        public Processor(string[] args) : base(args) { }

        protected override void start()
        {
            JobDTO job = getNextJob();
            while (job != null)
            {
                try
                {
                    setJobStatus(job.ID.Value, JobStatusEnum.Started);

                    // get audits to process
                    List<AuditDTO> toProcess = null;
                    do
                    {
                        toProcess = ProviderFactory.Logging.GetBatchOfAuditsToProcess();

                        List<long> auditsProcessed = new List<long>();

                        foreach (AuditTypeEnum type in (from t in toProcess select t.Type).Distinct())
                        {
                            try
                            {
                                AuditHandlerBase auditor = ProviderFactory.Logging.GetAuditHandler(type);
                                if (auditor != null)
                                {
                                    List<AuditDTO> items = (from tp in toProcess where tp.Type == type select tp).ToList();
                                    auditor.ProcessAudits(items);
                                    auditsProcessed.AddRange((from i in items select i.ID).ToList());
                                }
                                else
                                {
                                    // audit the fact that we cannot find an auditor for this batch of audits?
                                }
                            }
                            catch (Exception ex)
                            {
                                ProviderFactory.Logging.LogException(ex);
                            }

                        }

                        ProviderFactory.Logging.SetAuditsAsProcessed(auditsProcessed);
                        auditsProcessed.Clear();

                    } while (toProcess.Count() >= 50); // if we got a full batch of audits, grab another batch

                    setJobStatus(job.ID.Value, JobStatusEnum.Completed);
                }
                catch (Exception ex)
                {
                    setJobStatus(job.ID.Value, JobStatusEnum.Failed);
                    ProviderFactory.Logging.LogException(ex);
                    Auditor
                        .New(AuditLevelEnum.Exception, AuditTypeEnum.ProcessorException, UserInfoDTO.System, ex.ToString())
                        .Save(ProviderFactory.Logging)
                    ;
                }


                job = getNextJob();
            }
        }

    }
}
