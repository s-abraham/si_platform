﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Collections.Specialized;
using System.Text;
using System.Threading.Tasks;
using System.Web.Script.Serialization;
using Facebook.Entities;
using HtmlAgilityPack;
using Newtonsoft.Json;
using SI;
using SI.DTO;
using SI.Extensions;

namespace Facebook
{
    public static class FacebookAPI
    {
        #region variable

        private static int MaxRedirects = 2;
		private static string UserAgent = "Mozilla/4.0 (compatible; Windows NT)";

		private static SInet _WebNet = null;
        private static int Timeout = 60 * 1000;
        private static string FacebookGraphAPIURLPostStatistics { get; set; }
        private static string FacebookGraphAPIURLUserPicture { get; set; }
        private static string FacebookGraphAPIURLPostStatisticsByFeedID { get; set; }
        private static string FacebookGraphAPIURLWallPostCreate { get; set; }
        private static string FacebookGraphAPIURLUploadPhoto { get; set; }
        private static string FacebookGraphAPIURLAlbumGet { get; set; }
        private static string FacebookGraphAPIURLAlbumCreate { get; set; }
        private static string FacebookGraphAPIURLEventGet { get; set; }
        private static string FacebookGraphAPIURLEventCreate { get; set; }
        private static string FacebookGraphAPIURLEventStatistics { get; set; }
        private static string FacebookGraphAPIURLPageInsights { get; set; }
        private static string FacebookGraphAPIURLPostInsights { get; set; }
        private static string FacebookGraphAPIURLPage { get; set; }
        private static string FacebookGraphAPIURLObjectIDGetResultID { get; set; }
        private static string FacebookGraphAPIURLAccounts { get; set; }
        private static string FacebookGraphAPIURLoAuthToken { get; set; }
        private static string FacebookGraphAPIURLSearch { get; set; }
        private static string FacebookGraphAPIURLFeed { get; set; }
        public const string FacebookGraphAPIURLLogin = "https://graph.facebook.com/oauth/authorize?client_id={0}&redirect_uri={1}&scope={2}&display=popup&enable_profile_selector=1&state={3}";
        private static string FacebookAdsAccountInfoURL { get; set; }
        private static string FacebookAdsCampaignListURL { get; set; }
        private static string FacebookAdsCampaignInfoURL { get; set; }
        private static string FacebookAdsCampaignStatsURL { get; set; }
        private static string FacebookTokenValidateURL = "https://graph.facebook.com/debug_token?input_token={0}&access_token={1}";
        private static string FacebookGraphAPIPostCreateComment = "https://graph.facebook.com/{0}/comments?access_token={1}";
        private static string FacebookGraphAPIPostDeleteComment = "https://graph.facebook.com/{0}?method=delete&access_token={1}";
        private static string FacebookGraphAPICommentCreateComment = "https://graph.facebook.com/{0}/comments?access_token={1}";
        //private static string FacebookGraphAPICommentDeleteComment = "https://graph.facebook.com/{0}?method=delete&access_token={1}";
        private static string FacebookGraphAPIPostCreateLike = "https://graph.facebook.com/{0}/likes?access_token={1}&notify=false";
        private static string FacebookGraphAPIPostDeleteLike = "https://graph.facebook.com/{0}/likes?access_token={1}&notify=false";
        private static string FacebookGraphAPICommentCreateLike = "https://graph.facebook.com/{0}/likes?access_token={1}&notify=false";
        private static string FacebookGraphAPICommentDeleteLike = "https://graph.facebook.com/{0}/likes?method=delete&access_token={1}";
        private static string FacebookGraphAPIRatingURL = "https://graph.facebook.com/v2.0/{0}/ratings?fields={1}&limit={2}&access_token={3}";
        private static string FacebookGraphAPIMessagesURL = "https://graph.facebook.com/{0}/conversations?format=json&limit=500&fields=id,snippet,updated_time,message_count,unread_count,participants,senders&access_token={1}";
        private static string FacebookGraphAPIUpdateCoverPhotoURL = "https://graph.facebook.com/{0}/?access_token={1}";
        private static string FacebookGraphAPIURLWallPostDelete = "https://graph.facebook.com/{0}?access_token={1}";
        #endregion

        #region Functions

        #region Public

        #region Wall Post

        public static FacebookPostStatistics GetFacebookPostStatistics(string ResultID, string Token, PostTypeEnum type, bool isSharedStory, bool GetUserPicture = false)
        {            
            FacebookPostStatistics objFacebookPostStatistics = new FacebookPostStatistics();            
            string fields = string.Empty;

            if (type == PostTypeEnum.Status || type == PostTypeEnum.Link)
            {
                fields = "id,from.fields(id,name,picture),name,link,created_time,updated_time,likes.limit(50000).fields(id,name,picture),comments.limit(50000).fields(id,from.fields(id,name,picture),like_count,message,created_time,likes.limit(50000).fields(id,name,picture),user_likes,comments.limit(100).fields(id,from.fields(id,name,picture),like_count,message,created_time,user_likes)),message,privacy,type,application,is_published,object_id,shares";
                if (GetUserPicture)
                {
                    fields = "id,from.fields(id,name,picture),name,link,created_time,likes.limit(50000).fields(id,name,picture),comments.limit(50000).fields(id,from.fields(id,name,picture),like_count,message,created_time,likes.limit(50000).fields(id,name,picture),user_likes,comments.limit(100).fields(id,from.fields(id,name,picture),like_count,message,created_time,user_likes)),message,privacy,application,shares";
                }
                if (isSharedStory)
                {
                    fields = "id,from.fields(id,name,picture),name,link,created_time,updated_time,likes.limit(50000).fields(id,name,picture),comments.limit(50000).fields(id,from.fields(id,name,picture),like_count,message,created_time,likes.limit(50000).fields(id,name,picture),user_likes,comments.limit(100).fields(id,from.fields(id,name,picture),like_count,message,created_time,user_likes)),message,privacy,type,application,is_published,object_id,shares";
                }
            }
            else if (type == PostTypeEnum.Photo)
            {
                fields = "id,from.fields(id,name,picture),name,link,created_time,updated_time,likes.limit(50000).fields(id,name,picture),comments.limit(50000).fields(id,from.fields(id,name,picture),like_count,message,created_time,likes.limit(50000).fields(id,name,picture),user_likes,comments.limit(100).fields(id,from.fields(id,name,picture),like_count,message,created_time,user_likes)),sharedposts.limit(5000).fields(created_time,from.fields(id,name,picture),id,name,message)";
                if (isSharedStory)
                {
                    fields = "id,from.fields(id,name,picture),name,link,created_time,updated_time,likes.limit(50000).fields(id,name,picture),comments.limit(50000).fields(id,from.fields(id,name,picture),like_count,message,created_time,likes.limit(50000).fields(id,name,picture),user_likes,comments.limit(100).fields(id,from.fields(id,name,picture),like_count,message,created_time,user_likes)),shares";
                }
            }
			
            FacebookResponse ObjFacebookResponse = new FacebookResponse();
            PostStatistics objPostStatistics = new PostStatistics();
            try
            {
                if (!string.IsNullOrEmpty(ResultID) && !string.IsNullOrEmpty(Token))
                {
                    if (string.IsNullOrEmpty(FacebookGraphAPIURLPostStatistics))
                    {
                        FacebookGraphAPIURLPostStatistics = "https://graph.facebook.com/{0}?fields={1}&access_token={2}";
                    }
                                       
                    string URL = string.Format(FacebookGraphAPIURLPostStatistics, ResultID, fields, Token);

                    string rawResponse = string.Empty;

                    try
                    {
                        rawResponse = HttpRequest(URL, null, UserAgent);

                        if (!string.IsNullOrEmpty(rawResponse))
                        {
                            var jss = new JavaScriptSerializer();
                            if (rawResponse.StartsWith(@"{""error"":"))
                            {
                                rawResponse = rawResponse.Replace(@"{""error"":", "");
                                rawResponse = rawResponse.Substring(0, rawResponse.Length - 1);

                                ObjFacebookResponse = jss.Deserialize<FacebookResponse>(rawResponse);
                            }
                            else
                            {
                                objPostStatistics = jss.Deserialize<PostStatistics>(rawResponse);

                                if (type == PostTypeEnum.Photo)
                                {
                                    objPostStatistics.message = objPostStatistics.name;
                                }

                                #region GetUserPicture
                                if (GetUserPicture)
                                {
                                    if (objPostStatistics.comments != null)
                                    {
                                        foreach (var item in objPostStatistics.comments.data)
                                        {
                                            //Get User Picture
                                            FaceBookUserPicture objFaceBookUserPicture = FaceBookUserPicture(item.from.id);
                                            if (objFaceBookUserPicture != null)
                                            {
                                                if (objFaceBookUserPicture.picture.data != null)
                                                {
                                                    item.from.pictureurl = objFaceBookUserPicture.picture.data.url;
                                                }
                                                else
                                                {
                                                    item.from.pictureurl = string.Empty;
                                                }
                                            }
                                            else
                                            {
                                                item.from.pictureurl = string.Empty;
                                            }

                                        }
                                    }

                                    if (objPostStatistics != null)
                                    {
                                        //Get User Picture
                                        FaceBookUserPicture objFaceBookUserPicture = FaceBookUserPicture(objPostStatistics.from.id);
                                        if (objFaceBookUserPicture != null)
                                        {
                                            if (objFaceBookUserPicture.picture.data != null)
                                            {
                                                objPostStatistics.from.pictureurl = objFaceBookUserPicture.picture.data.url;
                                            }
                                            else
                                            {
                                                objPostStatistics.from.pictureurl = string.Empty;
                                            }
                                        }
                                        else
                                        {
                                            objPostStatistics.from.pictureurl = string.Empty;
                                        }
                                    }
                                }
                                #endregion

                                if (rawResponse == @"{""id"":""" + "" + ResultID + "" + @"""}")
                                {
                                    objPostStatistics.is_Deleted = true;
                                }

                                if (objPostStatistics.comments != null && objPostStatistics.comments.data != null)
                                {
                                    objPostStatistics.commentscount = objPostStatistics.comments.data.Count;
                                }

                                if (objPostStatistics.likes != null && objPostStatistics.likes.data != null)
                                {
                                    objPostStatistics.likescount = objPostStatistics.likes.data.Count;
                                }

                                if (objPostStatistics.sharedposts != null && objPostStatistics.sharedposts.data != null)
                                {
                                    objPostStatistics.sharescount = objPostStatistics.sharedposts.data.Count;
                                }
                                else if (objPostStatistics.shares != null)
                                {
                                    objPostStatistics.sharescount = objPostStatistics.shares.count;
                                }

                            }

                            objFacebookPostStatistics.facebookResponse = ObjFacebookResponse;
                            objFacebookPostStatistics.postStatistics = objPostStatistics;
                        }
                        else
                        {
                            ObjFacebookResponse.message = "Problem to Retrying PostStatistics from FaceBook.";

                            objFacebookPostStatistics.facebookResponse = ObjFacebookResponse;
                            objFacebookPostStatistics.postStatistics = objPostStatistics;
                        }
                    }

                    catch (Exception ex)
                    {
                        #region ERROR HANDLING
                        ObjFacebookResponse.message = ex.Message;                        
                        #endregion
                    }
                }
                else
                {
                    ObjFacebookResponse.message = "Invalid Parameters";

                    objFacebookPostStatistics.facebookResponse = ObjFacebookResponse;
                    objFacebookPostStatistics.postStatistics = objPostStatistics;
                }


            }
            catch (Exception ex)
            {
                #region ERROR HANDLING
                ObjFacebookResponse.message = ex.Message;
                #endregion
            }


            return objFacebookPostStatistics;
        }

		public static FaceBookJsonResponse GetJsonFacebookPost(string PostID, string postType, string Token)
		{
			FaceBookJsonResponse faceBookJsonResponse = new FaceBookJsonResponse();

			try
			{
				if (!string.IsNullOrEmpty(PostID) && !string.IsNullOrEmpty(Token))
				{
					string fields = string.Empty;
					postType = postType.ToLower();

					if (postType == "status" || postType == "link")
						fields = "id,from,name,link,created_time,updated_time,likes.limit(50000).fields(id,name),comments.limit(50000).fields(id,from,like_count,message,created_time,likes.limit(50000).fields(id,name),user_likes),message,privacy,type,application,is_published,object_id,shares";
					else if (postType == "photo")
						fields = "id,from,name,link,created_time,updated_time,likes.limit(50000).fields(id,name),comments.limit(50000).fields(id,from,like_count,message,created_time,likes.limit(50000).fields(id,name),user_likes),sharedposts.limit(5000).fields(created_time,from,id,name,message)";

					if (string.IsNullOrEmpty(FacebookGraphAPIURLPostStatistics))
					{
						FacebookGraphAPIURLPostStatistics = "https://graph.facebook.com/{0}?fields={1}&access_token={2}";
					}

					string URL = string.Format(FacebookGraphAPIURLPostStatistics, PostID, fields, Token);
					string rawResponse = HttpRequest(URL, null, UserAgent);

					if (!string.IsNullOrEmpty(rawResponse))
					{
						faceBookJsonResponse.ResponseJson = rawResponse;

						if (rawResponse.StartsWith(@"{""error"":") == false)
							faceBookJsonResponse.Success = true;
					}
				}
			}
			catch (Exception ex)
			{
				faceBookJsonResponse.ResponseJson = ex.Message;
			}

			return faceBookJsonResponse;
		}
        
        public static FacebookPostStatisticsFeedID GetFacebookPostStatisticsByFeedId(string FeedID, string Token, bool GetUserPicture = false)
        {            
            FacebookPostStatisticsFeedID objFacebookPostStatisticsFeedID = new FacebookPostStatisticsFeedID();
            FacebookResponse ObjFacebookResponse = new FacebookResponse();
            PostStatisticsByFeedID objPostStatisticsByFeedID = new PostStatisticsByFeedID();
            try
            {
                if (!string.IsNullOrEmpty(FeedID) && !string.IsNullOrEmpty(Token))
                {
                    if (string.IsNullOrEmpty(FacebookGraphAPIURLPostStatisticsByFeedID))
                    {
                        FacebookGraphAPIURLPostStatisticsByFeedID = "https://graph.facebook.com/{0}?access_token={1}";
                    }

                    string URL = string.Format(FacebookGraphAPIURLPostStatisticsByFeedID, FeedID, Token);
                    string rawResponse = string.Empty;
                    try
                    {

                        rawResponse = HttpRequest(URL, null, UserAgent);

                        if (!string.IsNullOrEmpty(rawResponse))
                        {
                            var jss = new JavaScriptSerializer();
                            if (rawResponse.StartsWith(@"{""error"":"))
                            {
                                rawResponse = rawResponse.Replace(@"{""error"":", "");
                                rawResponse = rawResponse.Substring(0, rawResponse.Length - 1);

                                ObjFacebookResponse = jss.Deserialize<FacebookResponse>(rawResponse);
                            }
                            else
                            {
                                objPostStatisticsByFeedID = jss.Deserialize<PostStatisticsByFeedID>(rawResponse);

                                #region GetUserPicture
                                if (GetUserPicture)
                                {
                                    if (objPostStatisticsByFeedID.comments != null)
                                    {
                                        foreach (var item in objPostStatisticsByFeedID.comments.data)
                                        {
                                            //Get User Picture
                                            FaceBookUserPicture objFaceBookUserPicture = FaceBookUserPicture(item.from.id);
                                            if (objFaceBookUserPicture != null)
                                            {
                                                if (objFaceBookUserPicture.picture.data != null)
                                                {
                                                    item.from.pictureurl = objFaceBookUserPicture.picture.data.url;
                                                }
                                                else
                                                {
                                                    item.from.pictureurl = string.Empty;
                                                }
                                            }
                                            else
                                            {
                                                item.from.pictureurl = string.Empty;
                                            }

                                        }
                                    }

                                    if (objPostStatisticsByFeedID != null)
                                    {
                                        //Get User Picture
                                        FaceBookUserPicture objFaceBookUserPicture = FaceBookUserPicture(objPostStatisticsByFeedID.from.id);
                                        if (objFaceBookUserPicture != null)
                                        {
                                            if (objFaceBookUserPicture.picture.data != null)
                                            {
                                                objPostStatisticsByFeedID.from.pictureurl = objFaceBookUserPicture.picture.data.url;
                                            }
                                            else
                                            {
                                                objPostStatisticsByFeedID.from.pictureurl = string.Empty;
                                            }
                                        }
                                        else
                                        {
                                            objPostStatisticsByFeedID.from.pictureurl = string.Empty;
                                        }
                                    }
                                }
                                #endregion

                                if (rawResponse == @"{""id"":""" + "" + FeedID + "" + @"""}")
                                {
                                    objPostStatisticsByFeedID.is_Deleted = true;
                                }
                            }

                            objFacebookPostStatisticsFeedID.facebookResponse = ObjFacebookResponse;
                            objFacebookPostStatisticsFeedID.postStatisticsbyfeedid = objPostStatisticsByFeedID;
                        }
                        else
                        {
                            ObjFacebookResponse.message = "Problem to Retrying PostStatistics By FeedID from FaceBook.";

                            objFacebookPostStatisticsFeedID.facebookResponse = ObjFacebookResponse;
                            objFacebookPostStatisticsFeedID.postStatisticsbyfeedid = objPostStatisticsByFeedID;
                        }
                    }

                    catch (Exception ex)
                    {
                        #region ERROR HANDLING
                        ObjFacebookResponse.message = ex.Message;
                        #endregion
                    }
                }
                else
                {
                    ObjFacebookResponse.message = "Invalid Parameters";

                    objFacebookPostStatisticsFeedID.facebookResponse = ObjFacebookResponse;
                    objFacebookPostStatisticsFeedID.postStatisticsbyfeedid = objPostStatisticsByFeedID;
                }


            }
            catch (Exception ex)
            {
                #region ERROR HANDLING
                ObjFacebookResponse.message = ex.Message;
                #endregion
            }


            return objFacebookPostStatisticsFeedID;
        }
        
		public static FaceBookJsonResponse GetJsonFacebookPostInsights(string PostID, string Token)
		{
			FaceBookJsonResponse faceBookJsonResponse = new FaceBookJsonResponse();

			try
			{
				if (!string.IsNullOrEmpty(PostID) && !string.IsNullOrEmpty(Token))
				{
					if (string.IsNullOrEmpty(FacebookGraphAPIURLPostInsights))
					{
						FacebookGraphAPIURLPostInsights = "https://graph.facebook.com/{0}/insights?format=json&access_token={1}";
					}

					string URL = string.Format(FacebookGraphAPIURLPostInsights, PostID, Token);
					string rawResponse = HttpRequest(URL, null, UserAgent);

					if (!string.IsNullOrEmpty(rawResponse))
					{
						faceBookJsonResponse.ResponseJson = rawResponse;

						if (rawResponse.StartsWith(@"{""error"":") == false)
							faceBookJsonResponse.Success = true;
					}
				}
			}
			catch (Exception ex)
			{
				faceBookJsonResponse.ResponseJson = ex.Message;
			}

			return faceBookJsonResponse;
		}

        public static FacebookInsights GetFacebookPostInsights(string FeedID, string token)
        {
            var objFacebookResponse = new FacebookResponse();
            var objFacebookInsights = new FacebookInsights();
            var objInsights = new Insights();
            try
            {                
                var errorMessage = string.Empty;
                if (!string.IsNullOrEmpty(FeedID))
                {   
                    if (string.IsNullOrEmpty(FacebookGraphAPIURLPostInsights))
                    {
                        FacebookGraphAPIURLPostInsights = "https://graph.facebook.com/{0}/insights?format=json&access_token={1}";
                    }

                    string URL = string.Format(FacebookGraphAPIURLPostInsights, FeedID, token);                    

                    string rawResponse = string.Empty;
                    try
                    {
                        rawResponse = HttpRequest(URL, null, UserAgent);

                        if (!string.IsNullOrEmpty(rawResponse))
                        {
                            var jss = new JavaScriptSerializer();
                            if (rawResponse.StartsWith(@"{""error"":"))
                            {
                                rawResponse = rawResponse.Replace(@"{""error"":", "");
                                rawResponse = rawResponse.Substring(0, rawResponse.Length - 1);

                                objFacebookResponse = jss.Deserialize<FacebookResponse>(rawResponse);

                                objFacebookInsights.facebookResponse = objFacebookResponse;
                                //objFacebookInsights.responseJson = rawResponse;
                                objFacebookInsights.insights = objInsights;
                            }
                            else
                            {
                                objInsights = JsonConvert.DeserializeObject<Insights>(rawResponse);
                                objFacebookInsights.insights = objInsights;
                            }

                        }
                        else
                        {
                            objFacebookResponse.message = "Problem in Retrying FeceBook Post Insights.";

                            objFacebookInsights.facebookResponse = objFacebookResponse;
                            //objFacebookInsights.responseJson = rawResponse;
                            objFacebookInsights.insights = objInsights;
                        }
                    }
                    catch (Exception ex)
                    {
                        #region ERROR HANDLING

                        objFacebookResponse.message = ex.Message;
                        objFacebookResponse.code = string.Empty;
                        objFacebookResponse.type = "Exception\\GetFacebookPostInsights";
                        objFacebookResponse.ID = string.Empty;
                                                 
                        #endregion
                    }
                }
                else
                {
                    objFacebookResponse.message = "FeedID is is Zero.";

                    objFacebookInsights.facebookResponse = objFacebookResponse;
                    //objFacebookInsights.responseJson = string.Empty;
                    objFacebookInsights.insights = objInsights;
                }
            }
            catch (Exception ex)
            {
                #region ERROR HANDLING

                objFacebookResponse.ID = string.Empty;
                objFacebookResponse.message = ex.Message;
                objFacebookResponse.type = "Exception\\GetFacebookPostInsights";
                objFacebookResponse.code = string.Empty;

                #endregion
            }

            return objFacebookInsights;
        }

        public static FacebookResponse CreateFacebookWallPost(string UniqueID,string Token, string message, string caption, string description, string name, string picture, string link, string source, string targeting)
        {
            FacebookResponse ObjFacebookResponse = new FacebookResponse();            
            try
            {
                if (!string.IsNullOrEmpty(UniqueID) && !string.IsNullOrEmpty(Token))
                {
                    if (string.IsNullOrEmpty(FacebookGraphAPIURLWallPostCreate))
                    {
                        FacebookGraphAPIURLWallPostCreate = "https://graph.facebook.com/{0}/feed?access_token={1}";
                    }

                    string URL = string.Format(FacebookGraphAPIURLWallPostCreate, UniqueID, Token);
                                        
                    Dictionary<string, string> PostData = new Dictionary<string, string>();

                    string parameters = string.Empty;

                    if (!string.IsNullOrEmpty(message))
                        PostData.Add("message", System.Web.HttpUtility.UrlEncode(message));
                    if (!string.IsNullOrEmpty(caption))
                        PostData.Add("caption", caption);
                    if (!string.IsNullOrEmpty(description))
                        PostData.Add("description", System.Web.HttpUtility.UrlEncode(description));
                    if (!string.IsNullOrEmpty(name))
                        PostData.Add("name", System.Web.HttpUtility.UrlEncode(name));
                    if (!string.IsNullOrEmpty(picture))
                        PostData.Add("picture", picture);
                    else
                        PostData.Add("picture", string.Empty);
                    if (!string.IsNullOrEmpty(source))
                        PostData.Add("source", System.Web.HttpUtility.UrlEncode(source));
                    if (!string.IsNullOrEmpty(targeting))
                        PostData.Add("targeting", System.Web.HttpUtility.UrlEncode(targeting));

                    if (!string.IsNullOrEmpty(link))
                    {
                        //PostData.Add("link", link);                        
                        PostData.Add("link", System.Web.HttpUtility.UrlEncode(link));                        
                    }

                    string rawResponse = string.Empty;
                    try
                    {
                        rawResponse = HttpRequest(URL, PostData, UserAgent);

                        if (!string.IsNullOrEmpty(rawResponse))
                        {
                            var jss = new JavaScriptSerializer();
                            if (rawResponse.Contains("error"))
                            {
                                rawResponse = rawResponse.Replace(@"{""error"":", "");
                                rawResponse = rawResponse.Substring(0, rawResponse.Length - 1);
                            }
                            ObjFacebookResponse = jss.Deserialize<FacebookResponse>(rawResponse);
                        }
                        else
                        {
                            ObjFacebookResponse.message = "Invalid Http Request.";
                        }

                        //result = app.Post(dlSocialNetwork.UniqueID + "/feed", parameters);
                    }
                    catch (Exception ex)
                    {
                        #region ERROR HANDLING

                        if (!string.IsNullOrEmpty(rawResponse))
                        {
                            ObjFacebookResponse.message = ex.Message;
                            ObjFacebookResponse.code = string.Empty;
                            ObjFacebookResponse.type = "Exception\\CreateFacebookWallPost";
                            ObjFacebookResponse.ID = string.Empty;
                        }

                        #endregion
                    }

                }
                else
                {
                    ObjFacebookResponse.ID = string.Empty;
                    ObjFacebookResponse.message = "InValid Input.";
                    ObjFacebookResponse.type = "Validation";
                    ObjFacebookResponse.code = string.Empty;
                }
            }
            catch (Exception ex)
            {
                #region ERROR HANDLING
                ObjFacebookResponse.ID = string.Empty;
                ObjFacebookResponse.message = ex.Message;
                ObjFacebookResponse.type = "Exception\\CreateFacebookWallPost";
                ObjFacebookResponse.code = string.Empty;
                #endregion
            }
            return ObjFacebookResponse;
        }

        public static FacebookStreamAction CreateCommentOnPost(string postID, string Token, string comment)
        {
            var facebookStreamAction = new FacebookStreamAction();

            if (!string.IsNullOrEmpty(postID) && !string.IsNullOrEmpty(Token) && !string.IsNullOrEmpty(comment))
            {
                var rawResponse = string.Empty;
                try
                {
                    Dictionary<string, string> PostData = new Dictionary<string, string>();

                    var URL = string.Format(FacebookGraphAPIPostCreateComment, postID, Token);

                    PostData.Add("message", System.Web.HttpUtility.UrlEncode(comment));

                    rawResponse = HttpRequest(URL, PostData, UserAgent);

                    if (!string.IsNullOrEmpty(rawResponse))
                    {
                        var jss = new JavaScriptSerializer();
                        if (rawResponse.Contains("error"))
                        {
                            rawResponse = rawResponse.Replace(@"{""error"":", "");
                            rawResponse = rawResponse.Substring(0, rawResponse.Length - 1);
                            facebookStreamAction.IsSuccess = false;
                            facebookStreamAction.FacebookResponse.message = rawResponse;
                        }
                        else
                        {
                            facebookStreamAction.ActionResultID = Convert.ToString(rawResponse);
                            facebookStreamAction.IsSuccess = true;
                        }
                    }
                }
                catch (WebException wex)
                {
                    if (wex.Response != null)
                    {
                        using (var errorResponse = (HttpWebResponse)wex.Response)
                        {
                            using (var reader = new StreamReader(errorResponse.GetResponseStream()))
                            {
                                facebookStreamAction.IsSuccess = false;
                                facebookStreamAction.FacebookResponse.message = reader.ReadToEnd();
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    facebookStreamAction.IsSuccess = false;
                    facebookStreamAction.FacebookResponse.message = "General Exception";
                }
            }
            else
            {
                facebookStreamAction.IsSuccess = false;
                facebookStreamAction.FacebookResponse.message = "Missing or Invalid Parameters";
            }
           
            return facebookStreamAction;
        }

        public static FacebookStreamAction DeleteCommentOnPost(string commentID, string Token)
        {
            var facebookStreamAction = new FacebookStreamAction();

            if (!string.IsNullOrEmpty(Token) && !string.IsNullOrEmpty(commentID))
            {
                var rawResponse = string.Empty;
                try
                {
                    FacebookRequest objFacebookRequest = new FacebookRequest();

                    var URL = string.Format(FacebookGraphAPIPostDeleteComment, commentID, Token);
                            
                    rawResponse = HttpRequest(URL, null, UserAgent);

                    if (!string.IsNullOrEmpty(rawResponse))
                    {
                        var jss = new JavaScriptSerializer();
                        if (rawResponse.Contains("error"))
                        {
                            rawResponse = rawResponse.Replace(@"{""error"":", "");
                            rawResponse = rawResponse.Substring(0, rawResponse.Length - 1);
                            facebookStreamAction.IsSuccess = false;
                            facebookStreamAction.FacebookResponse.message = rawResponse;
                        }
                        else
                        {
                            facebookStreamAction.IsSuccess = Convert.ToBoolean(rawResponse);
                        }
                    }
                }
                catch (WebException wex)
                {
                    if (wex.Response != null)
                    {
                        using (var errorResponse = (HttpWebResponse)wex.Response)
                        {
                            using (var reader = new StreamReader(errorResponse.GetResponseStream()))
                            {
                                facebookStreamAction.IsSuccess = false;
                                facebookStreamAction.FacebookResponse.message = reader.ReadToEnd();
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    facebookStreamAction.IsSuccess = false;
                    facebookStreamAction.FacebookResponse.message = "General Exception";
                }
            }
            else
            {
                facebookStreamAction.IsSuccess = false;
                facebookStreamAction.FacebookResponse.message = "Missing or Invalid Parameters";
            }

            return facebookStreamAction;
        }

        public static FacebookStreamAction CreateCommentOnComment(string commentID, string Token, string comment)
        {
            var facebookStreamAction = new FacebookStreamAction();
                
            if (!string.IsNullOrEmpty(commentID) && !string.IsNullOrEmpty(Token) && !string.IsNullOrEmpty(comment))
            {
                var rawResponse = string.Empty;
                try
                {
                    FacebookRequest objFacebookRequest = new FacebookRequest();
                    Dictionary<string, string> PostData = new Dictionary<string, string>();
                    var URL = string.Format(FacebookGraphAPICommentCreateComment, commentID, Token);
                    PostData.Add("message", System.Web.HttpUtility.UrlEncode(comment));
                    rawResponse = HttpRequest(URL, PostData, UserAgent);

                    if (!string.IsNullOrEmpty(rawResponse))
                    {
                        var jss = new JavaScriptSerializer();
                        if (rawResponse.Contains("error"))
                        {
                            rawResponse = rawResponse.Replace(@"{""error"":", "");
                            rawResponse = rawResponse.Substring(0, rawResponse.Length - 1); 
                            facebookStreamAction.IsSuccess = false;
                            facebookStreamAction.FacebookResponse.message = rawResponse;
                        }
                        else
                        {
                            facebookStreamAction.ActionResultID = Convert.ToString(rawResponse);
                            facebookStreamAction.IsSuccess = true;
                        }
                    }
                }
                catch (WebException wex)
                {
                    if (wex.Response != null)
                    {
                        using (var errorResponse = (HttpWebResponse)wex.Response)
                        {
                            using (var reader = new StreamReader(errorResponse.GetResponseStream()))
                            {
                                facebookStreamAction.IsSuccess = false;
                                facebookStreamAction.FacebookResponse.message = reader.ReadToEnd();
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    facebookStreamAction.IsSuccess = false;
                    facebookStreamAction.FacebookResponse.message = "General Exception";
                }
            }
            else
            {
                facebookStreamAction.IsSuccess = false;
                facebookStreamAction.FacebookResponse.message = "Missing or Invalid Parameters";
            }

            return facebookStreamAction;
        }

        //public static void DeleteCommentForComment(string UniqueID, string Token, string comment, string commentID)
        //{


        //}

        public static FacebookStreamAction LikePost(string postID, string Token)
        {
            var facebookStreamAction = new FacebookStreamAction();

            if (!string.IsNullOrEmpty(Token) && !string.IsNullOrEmpty(postID))
            {
                try
                {
                    var URL = string.Format(FacebookGraphAPIPostCreateLike, postID, Token);

                    using (var client = new WebClient())
                    {
                        var values = new NameValueCollection();

                        values[""] = "";
                        values[""] = "";

                        var rawResponse = client.UploadString(URL, "POST", "");

                        if (!string.IsNullOrEmpty(rawResponse))
                        {
                            if (rawResponse.Contains("error"))
                            {
                                rawResponse = rawResponse.Replace(@"{""error"":", "");
                                rawResponse = rawResponse.Substring(0, rawResponse.Length - 1);
                                facebookStreamAction.IsSuccess = false;
                                facebookStreamAction.FacebookResponse.message = rawResponse;
                            }
                            else
                            {
                                facebookStreamAction.IsSuccess = Convert.ToBoolean(rawResponse);
                            }
                        }
                    }
                }
                catch (WebException wex)
                {
                    if (wex.Response != null)
                    {
                        using (var errorResponse = (HttpWebResponse)wex.Response)
                        {
                            using (var reader = new StreamReader(errorResponse.GetResponseStream()))
                            {
                                facebookStreamAction.IsSuccess = false;
                                facebookStreamAction.FacebookResponse.message = reader.ReadToEnd();
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    facebookStreamAction.IsSuccess = false;
                    facebookStreamAction.FacebookResponse.message = "General Exception";
                }
            }
            else
            {
                facebookStreamAction.IsSuccess = false;
                facebookStreamAction.FacebookResponse.message = "Missing or Invalid Parameters";
            }
            
            return facebookStreamAction;
        }

        public static FacebookStreamAction UnLikePost(string postID, string Token)
        {
            var facebookStreamAction = new FacebookStreamAction();
            
            if (!string.IsNullOrEmpty(Token))
            {
                try
                {
                    var URL = string.Format(FacebookGraphAPIPostDeleteLike, postID, Token);

                    using (var client = new WebClient())
                    {
                        var rawResponse = client.UploadString(URL, "DELETE", "");

                        if (!string.IsNullOrEmpty(rawResponse))
                        {
                            if (rawResponse.Contains("error"))
                            {
                                rawResponse = rawResponse.Replace(@"{""error"":", "");
                                rawResponse = rawResponse.Substring(0, rawResponse.Length - 1);
                                facebookStreamAction.IsSuccess = false;
                                facebookStreamAction.FacebookResponse.message = rawResponse;
                            }
                            else
                            {
                                facebookStreamAction.IsSuccess = Convert.ToBoolean(rawResponse);
                            }
                        }
                    }
                }
                catch (WebException wex)
                {
                    if (wex.Response != null)
                    {
                        using (var errorResponse = (HttpWebResponse)wex.Response)
                        {
                            using (var reader = new StreamReader(errorResponse.GetResponseStream()))
                            {
                                facebookStreamAction.IsSuccess = false;
                                facebookStreamAction.FacebookResponse.message = reader.ReadToEnd();
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    facebookStreamAction.IsSuccess = false;
                    facebookStreamAction.FacebookResponse.message = "General Exception";
                }
            }
            else
            {
                facebookStreamAction.IsSuccess = false;
                facebookStreamAction.FacebookResponse.message = "Missing or Invalid Parameters";
            }

            return facebookStreamAction;
        }

        public static FacebookStreamAction LikeComment(string commentID, string Token)
        {
            var facebookStreamAction = new FacebookStreamAction();
            
            if (!string.IsNullOrEmpty(Token))
            {
                try
                {
                    var URL = string.Format(FacebookGraphAPICommentCreateLike, commentID, Token);

                    using (var client = new WebClient())
                    {
                        var rawResponse = client.UploadString(URL, "POST", "");

                        if (!string.IsNullOrEmpty(rawResponse))
                        {
                            if (rawResponse.Contains("error"))
                            {
                                rawResponse = rawResponse.Replace(@"{""error"":", "");
                                rawResponse = rawResponse.Substring(0, rawResponse.Length - 1);
                                facebookStreamAction.IsSuccess = false;
                                facebookStreamAction.FacebookResponse.message = rawResponse;
                            }
                            else
                            {
                                facebookStreamAction.IsSuccess = Convert.ToBoolean(rawResponse);
                            }
                        }
                    }
                }
                catch (WebException wex)
                {
                    if (wex.Response != null)
                    {
                        using (var errorResponse = (HttpWebResponse)wex.Response)
                        {
                            using (var reader = new StreamReader(errorResponse.GetResponseStream()))
                            {
                                facebookStreamAction.IsSuccess = false;
                                facebookStreamAction.FacebookResponse.message = reader.ReadToEnd();
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    facebookStreamAction.IsSuccess = false;
                    facebookStreamAction.FacebookResponse.message = "General Exception";
                }
            }
            else
            {
                facebookStreamAction.IsSuccess = false;
                facebookStreamAction.FacebookResponse.message = "Missing or Invalid Parameters";
            }

            return facebookStreamAction;
        }

        public static FacebookStreamAction UnLikeComment(string commentID, string Token)
        {
            var facebookStreamAction = new FacebookStreamAction();

                if (!string.IsNullOrEmpty(Token))
                {
                    try
                    {
                        var URL = string.Format(FacebookGraphAPICommentDeleteLike, commentID, Token);

                        using (var client = new WebClient())
                        {
                            var rawResponse = client.UploadString(URL, "DELETE", "");

                            if (!string.IsNullOrEmpty(rawResponse))
                            {
                                if (rawResponse.Contains("error"))
                                {
                                    rawResponse = rawResponse.Replace(@"{""error"":", "");
                                    rawResponse = rawResponse.Substring(0, rawResponse.Length - 1);
                                    facebookStreamAction.IsSuccess = false;
                                    facebookStreamAction.FacebookResponse.message = rawResponse;
                                }
                                else
                                {
                                    facebookStreamAction.IsSuccess = Convert.ToBoolean(rawResponse);
                                }
                            }
                        }
                    }
                    catch (WebException wex)
                    {
                        if (wex.Response != null)
                        {
                            using (var errorResponse = (HttpWebResponse)wex.Response)
                            {
                                using (var reader = new StreamReader(errorResponse.GetResponseStream()))
                                {
                                    facebookStreamAction.IsSuccess = false;
                                    facebookStreamAction.FacebookResponse.message = reader.ReadToEnd();
                                }
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        facebookStreamAction.IsSuccess = false;
                        facebookStreamAction.FacebookResponse.message = "General Exception";
                    }
                }
                else
                {
                    facebookStreamAction.IsSuccess = false;
                    facebookStreamAction.FacebookResponse.message = "Missing or Invalid Parameters";
                }

                return facebookStreamAction;
        }

        public static FacebookStreamAction DeletePost(string postID, string Token)
        {
            FacebookStreamAction facebookStreamAction = new FacebookStreamAction();
            
            try
            {
                if (!string.IsNullOrEmpty(postID) && !string.IsNullOrEmpty(Token))
                {
                    if (string.IsNullOrEmpty(FacebookGraphAPIURLWallPostDelete))
                    {
                        FacebookGraphAPIURLWallPostDelete = "https://graph.facebook.com/{0}?access_token={1}";
                    }

                    string URL = string.Format(FacebookGraphAPIURLWallPostDelete, postID, Token);

                    string rawResponse = string.Empty;
                    try
                    {
                        using (var client = new WebClient())
                        {
                            rawResponse = client.UploadString(URL, "DELETE", "");

                            if (!string.IsNullOrEmpty(rawResponse))
                            {
                                var jss = new JavaScriptSerializer();
                                if (rawResponse.Contains("error"))
                                {
                                    rawResponse = rawResponse.Replace(@"{""error"":", "");
                                    rawResponse = rawResponse.Substring(0, rawResponse.Length - 1);

                                    facebookStreamAction.IsSuccess = false;
                                    facebookStreamAction.FacebookResponse = jss.Deserialize<FacebookResponse>(rawResponse);
                                    //facebookStreamAction.FacebookResponse.message = rawResponse;
                                }
                                else if (rawResponse == "true")
                                {
                                    facebookStreamAction.IsSuccess = Convert.ToBoolean(rawResponse);
                                }
                                else if (rawResponse == "false")
                                {
                                    facebookStreamAction.IsSuccess = Convert.ToBoolean(rawResponse);
                                }
                                else
                                {
                                    facebookStreamAction.FacebookResponse.message = rawResponse;
                                }
                            }
                        }                       
                    }
                    catch (WebException wex)
                    {
                        if (wex.Response != null)
                        {
                            var jss = new JavaScriptSerializer();
                            using (var errorResponse = (HttpWebResponse)wex.Response)
                            {
                                using (var reader = new StreamReader(errorResponse.GetResponseStream()))
                                {
                                    facebookStreamAction.IsSuccess = false;
                                    string ErrorResponse = reader.ReadToEnd().Replace(@"{""error"":", "");
                                    ErrorResponse = ErrorResponse.Substring(0, ErrorResponse.Length - 1);
                                    facebookStreamAction.FacebookResponse = jss.Deserialize<FacebookResponse>(ErrorResponse);
                                    //facebookStreamAction.FacebookResponse.message = reader.ReadToEnd();
                                }
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        facebookStreamAction.IsSuccess = false;
                        facebookStreamAction.FacebookResponse.message = "General Exception";
                    }

                }
                else
                {
                    facebookStreamAction.IsSuccess = false;
                    facebookStreamAction.FacebookResponse.message = "Invalid Parameters";
                }
            }
            catch (Exception ex)
            {
                #region ERROR HANDLING

                facebookStreamAction.IsSuccess = false;
                facebookStreamAction.FacebookResponse.message = ex.Message;
                

                #endregion
            }

            return facebookStreamAction;
            //return new FacebookStreamAction();
        }

        #endregion

        #region Photo

        public static FaceBookUserPicture FaceBookUserPicture(string FacebookID)
        {
            FaceBookUserPicture objFaceBookUserPicture = new FaceBookUserPicture();
           
            if (!string.IsNullOrEmpty(FacebookID))
            {
                if (string.IsNullOrEmpty(FacebookGraphAPIURLUserPicture))
                {
                    FacebookGraphAPIURLUserPicture = "https://graph.facebook.com/{0}?fields=picture.type%28small%29";
                }

                string URL = string.Format(FacebookGraphAPIURLUserPicture, FacebookID);

                string rawResponse = string.Empty;
                   
                rawResponse = HttpRequest(URL, null, UserAgent);

                if (!string.IsNullOrEmpty(rawResponse))
                {
                    var jss = new JavaScriptSerializer();
                    if (rawResponse.StartsWith(@"{""error"":"))
                    {                                
                    }
                    else
                    {
                        objFaceBookUserPicture = jss.Deserialize<FaceBookUserPicture>(rawResponse);
                    }
                }                                     
            }
           
            return objFaceBookUserPicture;
        }

        public static FacebookResponse UploadFacebookPhoto(string Token, string message, string imagePath, string album_id, byte[] Btimage = null)
        {
            FacebookResponse ObjFacebookResponse = new FacebookResponse();            
            try
            {
                if (!string.IsNullOrEmpty(Token))
                {
                    if (string.IsNullOrEmpty(FacebookGraphAPIURLUploadPhoto))
                    {
                        FacebookGraphAPIURLUploadPhoto = "https://graph.facebook.com/";
                    }
                   
                    string rawResponse = string.Empty;
                    try
                    {                       
                        // Create Boundary
                        string boundary = "---------------------------" + DateTime.Now.Ticks.ToString("x");


                        // Create Path                
                        if (!string.IsNullOrEmpty(album_id))
                        {
                            FacebookGraphAPIURLUploadPhoto += album_id + "/";
                        }
                        if(!FacebookGraphAPIURLUploadPhoto.Contains("photos"))
                            FacebookGraphAPIURLUploadPhoto += "photos";

                        // Get HttpWebRequest from SInet
                        //string UserAgent = "Mozilla/4.0 (compatible; Windows NT)";
                        string ContentType = "multipart/form-data; boundary=" + boundary;


                        HttpWebRequest httpRequest = GetRequest(FacebookGraphAPIURLUploadPhoto, UserAgent);
                        httpRequest.Method = "POST";
                        httpRequest.ServicePoint.Expect100Continue = false;
                        httpRequest.ContentType = ContentType;
                        httpRequest.KeepAlive = false;

                        // New String Builder
                        StringBuilder sb = new StringBuilder();

                        // Add Form Data
                        string formdataTemplate = "--{0}\r\nContent-Disposition: form-data; name=\"{1}\"\r\n\r\n{2}\r\n";

                        UTF8Encoding encoding = new UTF8Encoding();

                        // Access Token
                        sb.AppendFormat(formdataTemplate, boundary, "access_token", Token);

                        // Message
                        sb.AppendFormat(formdataTemplate, boundary, "message", message);

                        // Header
                        string headerTemplate = "--{0}\r\nContent-Disposition: form-data; name=\"{1}\"; filename=\"{2}\"\r\nContent-Type: {3}\r\n\r\n";

                        sb.AppendFormat(headerTemplate, boundary, "source", imagePath, @"application/octet-stream");

                        // File
                        string formString = sb.ToString();
                        byte[] formBytes = encoding.GetBytes(formString);
                        
                        
                        //byte[] formBytes = encoding.GetBytes(formString);

                        byte[] trailingBytes = encoding.GetBytes("\r\n--" + boundary + "--\r\n");
                        byte[] image;
                        if (Btimage == null)
                            image = DownloadData(imagePath, null, null);
                        else
                            image = Btimage;

                        if (image.Count() == 0)
                        {
                            ObjFacebookResponse.code = "000";
                            ObjFacebookResponse.message = "Image Should now be NULL";
                            ObjFacebookResponse.PhotoURL = imagePath;
                            ObjFacebookResponse.status = "fail";

                            return ObjFacebookResponse;
                        }

                        // Memory Stream
                        MemoryStream imageMemoryStream = new MemoryStream();
                        imageMemoryStream.Write(image, 0, image.Length);

                        // Set Content Length
                        long imageLength = imageMemoryStream.Length;
                        long contentLength = formBytes.Length + imageLength + trailingBytes.Length;
                        httpRequest.ContentLength = contentLength;

                        // Get Request Stream
                        httpRequest.AllowWriteStreamBuffering = false;
                        Stream strm_out = httpRequest.GetRequestStream();

                        // Write to Stream
                        strm_out.Write(formBytes, 0, formBytes.Length);
                        byte[] buffer = new Byte[checked((uint)Math.Min(4096, (int)imageLength))];

                        int bytesRead = 0;
                        int bytesTotal = 0;
                        imageMemoryStream.Seek(0, SeekOrigin.Begin);
                        while ((bytesRead = imageMemoryStream.Read(buffer, 0, buffer.Length)) != 0)
                        {
                            strm_out.Write(buffer, 0, bytesRead); bytesTotal += bytesRead;
                        }
                        strm_out.Write(trailingBytes, 0, trailingBytes.Length);

                        // Close Stream
                        strm_out.Close();

                        rawResponse = httpDownloadString(httpRequest);

                        if (!string.IsNullOrEmpty(rawResponse))
                        {
                            var jss = new JavaScriptSerializer();
                            if (rawResponse.Contains("error"))
                            {
                                rawResponse = rawResponse.Replace(@"{""error"":", "");
                                rawResponse = rawResponse.Substring(0, rawResponse.Length - 1);
                            }
                            ObjFacebookResponse = jss.Deserialize<FacebookResponse>(rawResponse);
                        }
                        else
                        {
                            ObjFacebookResponse.message = "Invalid Http Request.";
                        }

                        ObjFacebookResponse.PhotoURL = imagePath;
                        //result = app.Post(dlSocialNetwork.UniqueID + "/feed", parameters);
                    }

                    catch (Exception ex)
                    {
                        #region ERROR HANDLING                    
                        
                            ObjFacebookResponse.message = ex.Message;
                            ObjFacebookResponse.code = string.Empty;
                            ObjFacebookResponse.type = "Exception\\UploadFacebookPhoto";
                            ObjFacebookResponse.ID = string.Empty;
                        
                        #endregion
                    }


                }
            }
            catch (Exception ex)
            {
                #region ERROR HANDLING

                ObjFacebookResponse.ID = string.Empty;
                ObjFacebookResponse.message = ex.Message;
                ObjFacebookResponse.type = "Exception\\UploadFacebookPhoto";
                ObjFacebookResponse.code = string.Empty;

                #endregion
            }

            return ObjFacebookResponse;
        }

        #endregion

		#region Account

		public static FacebookAccount GetFacebookAccounts(string accessToken)
		{
			FacebookAccount facebookAccount = new FacebookAccount();

			if (!string.IsNullOrEmpty(accessToken))
			{
				if (string.IsNullOrEmpty(FacebookGraphAPIURLAccounts))
					FacebookGraphAPIURLAccounts = "https://graph.facebook.com/me/accounts?type=page&access_token={0}";

				string URL = string.Format(FacebookGraphAPIURLAccounts, accessToken);

				string rawResponse = string.Empty;
				
				rawResponse = HttpRequest(URL, null, UserAgent);

				if (!string.IsNullOrEmpty(rawResponse))
				{
					var jss = new JavaScriptSerializer();
					if (rawResponse.StartsWith(@"{""error"":"))
					{
						return null;
					}
					else
					{
						facebookAccount = jss.Deserialize<FacebookAccount>(rawResponse);
					}
				}				
				
			}

			return facebookAccount;
		}
		
		#endregion
        
		#region Album

		public static FacebookAlbum GetFacebookAlbums(string UniqueID, string Token)
        {            
            FacebookAlbum FacebookAlbums = new FacebookAlbum();
            string fields = "id,name,can_upload,count";
            string limit = "1000";
            FacebookResponse ObjFacebookResponse = new FacebookResponse();
            Album albums = new Album();
            try
            {
                if (string.IsNullOrEmpty(FacebookGraphAPIURLAlbumGet))
                {
                    FacebookGraphAPIURLAlbumGet = "https://graph.facebook.com/{0}/albums?fields={1}&limit={2}&access_token={3}";
                }
                
                if (!string.IsNullOrEmpty(UniqueID) && !string.IsNullOrEmpty(Token))
                {
                    string URL = string.Format(FacebookGraphAPIURLAlbumGet, UniqueID, fields, limit, Token);

                    string rawResponse = string.Empty;

                    try
                    {
                        rawResponse = HttpRequest(URL, null, UserAgent);

                        if (!string.IsNullOrEmpty(rawResponse))
                        {
                            var jss = new JavaScriptSerializer();
                            if (rawResponse.StartsWith(@"{""error"":"))
                            {
                                rawResponse = rawResponse.Replace(@"{""error"":", "");
                                rawResponse = rawResponse.Substring(0, rawResponse.Length - 1);

                                ObjFacebookResponse = jss.Deserialize<FacebookResponse>(rawResponse);
                            }
                            else
                            {
                                albums = jss.Deserialize<Album>(rawResponse);
                            }

                            FacebookAlbums.facebookResponse = ObjFacebookResponse;
                            FacebookAlbums.albums = albums;
                        }
                        else
                        {
                            ObjFacebookResponse.message = "Problem to Retrying Album List from FaceBook.";

                            FacebookAlbums.facebookResponse = ObjFacebookResponse;
                            FacebookAlbums.albums = albums;
                        }
                    }

                    catch (Exception ex)
                    {
                        #region ERROR HANDLING
                        ObjFacebookResponse.message = ex.Message;
                        #endregion
                    }
                }
                else
                {
                    ObjFacebookResponse.message = "Invalid Parameters";

                    FacebookAlbums.facebookResponse = ObjFacebookResponse;
                    FacebookAlbums.albums = albums;
                }
            }
            catch (Exception ex)
            {
                #region ERROR HANDLING
                ObjFacebookResponse.message = ex.Message;
                #endregion
            }

            return FacebookAlbums;
        }

        public static FacebookResponse CreateFacebookAlbum(string UniqueID, string Token, string albumName, string albumMessage)
        {
            FacebookResponse ObjFacebookResponse = new FacebookResponse();            
            try
            {                
                if (string.IsNullOrEmpty(FacebookGraphAPIURLAlbumCreate))
                {
                    FacebookGraphAPIURLAlbumCreate = "https://graph.facebook.com/{0}/albums?access_token={1}";
                }               
                if (!string.IsNullOrEmpty(albumName) && !string.IsNullOrEmpty(albumMessage) && !string.IsNullOrEmpty(UniqueID) && !string.IsNullOrEmpty(Token))
                {
                    string URL = string.Format(FacebookGraphAPIURLAlbumCreate, UniqueID, Token);

                    string rawResponse = string.Empty;
                    try
                    {
                        FacebookRequest objFacebookRequest = new FacebookRequest();

                        Dictionary<string, string> PostData = new Dictionary<string, string>();
                        
                        if (!string.IsNullOrEmpty(albumName))
                            PostData.Add("name", System.Web.HttpUtility.UrlEncode(albumName));
                        if (!string.IsNullOrEmpty(albumMessage))
                            PostData.Add("message", System.Web.HttpUtility.UrlEncode(albumMessage));

                        rawResponse = HttpRequest(URL, PostData, UserAgent);

                        if (!string.IsNullOrEmpty(rawResponse))
                        {
                            var jss = new JavaScriptSerializer();
                            if (rawResponse.Contains("error"))
                            {
                                rawResponse = rawResponse.Replace(@"{""error"":", "");
                                rawResponse = rawResponse.Substring(0, rawResponse.Length - 1);
                            }
                            ObjFacebookResponse = jss.Deserialize<FacebookResponse>(rawResponse);
                        }
                    }
                    catch (Exception ex)
                    {
                        #region ERROR HANDLING

                        ObjFacebookResponse.message = ex.Message;
                        ObjFacebookResponse.code = string.Empty;
                        ObjFacebookResponse.type = "Exception";
                        ObjFacebookResponse.ID = string.Empty;
                        
                        #endregion
                    }
                }
                else
                {
                    ObjFacebookResponse.message = "Invalid Parameters";
                }
            }
            catch (Exception ex)
            {
                #region ERROR HANDLING
                               
                ObjFacebookResponse.ID = string.Empty;
                ObjFacebookResponse.message = ex.Message;
                ObjFacebookResponse.type = "Exception\\CreateFacebookAlbum";
                ObjFacebookResponse.code = string.Empty;

                #endregion
            }

            return ObjFacebookResponse;
        }

        #endregion

        #region Event

        public static FacebookEvent GetFacebookEvent(string UniqueID, string Token)
        {            
            FacebookEvent objFacebookEvent = new FacebookEvent();
            string fields = "id,name,start_time,end_time,is_date_only,description,location";
            string limit = "1000";

            FacebookResponse ObjFacebookResponse = new FacebookResponse();
            List<Event> events = new List<Event>();
            try
            {
                if (!string.IsNullOrEmpty(UniqueID) && !string.IsNullOrEmpty(Token))
                {
                    if (string.IsNullOrEmpty(FacebookGraphAPIURLEventGet))
                    {
                        FacebookGraphAPIURLEventGet = "https://graph.facebook.com/{0}/events?fields={1}&limit={2}&access_token={3}";
                    }

                    string URL = string.Format(FacebookGraphAPIURLEventGet, UniqueID, fields, limit, Token);

                    string rawResponse = string.Empty;

                    try
                    {
                        rawResponse = HttpRequest(URL, null, UserAgent);

                        if (!string.IsNullOrEmpty(rawResponse))
                        {
                            var jss = new JavaScriptSerializer();
                            if (rawResponse.StartsWith(@"{""error"":"))
                            {
                                rawResponse = rawResponse.Replace(@"{""error"":", "");
                                rawResponse = rawResponse.Substring(0, rawResponse.Length - 1);

                                ObjFacebookResponse = jss.Deserialize<FacebookResponse>(rawResponse);
                            }
                            else if (rawResponse.StartsWith(@"{""data"":"))
                            {
                                rawResponse = rawResponse.Substring(8, rawResponse.Length - 8);
                                rawResponse = rawResponse.Substring(0, rawResponse.Length - 1);

                                int lastindex = rawResponse.LastIndexOf(']');
                                rawResponse = rawResponse.Substring(0, lastindex + 1);

                                events = jss.Deserialize<List<Event>>(rawResponse);
                            }

                            objFacebookEvent.facebookResponse = ObjFacebookResponse;
                            objFacebookEvent.events = events;
                        }
                        else
                        {
                            ObjFacebookResponse.message = "Problem to Retrying Question List from FaceBook.";

                            objFacebookEvent.facebookResponse = ObjFacebookResponse;
                            objFacebookEvent.events = events;
                        }
                    }

                    catch (Exception ex)
                    {
                        #region ERROR HANDLING
                        ObjFacebookResponse.message = ex.Message;
                        ObjFacebookResponse.code = string.Empty;
                        ObjFacebookResponse.type = "Exception";
                        ObjFacebookResponse.ID = string.Empty;
                        #endregion
                    }
                }
                else
                {
                    ObjFacebookResponse.message = "Invalid Parameters";

                    objFacebookEvent.facebookResponse = ObjFacebookResponse;
                    objFacebookEvent.events = events;
                }


            }
            catch (Exception ex)
            {
                #region ERROR HANDLING

                ObjFacebookResponse.ID = string.Empty;
                ObjFacebookResponse.message = ex.Message;
                ObjFacebookResponse.type = "Exception\\CreateFacebookAlbum";
                ObjFacebookResponse.code = string.Empty;

                #endregion
            }


            return objFacebookEvent;
        }

        public static FacebookResponse CreateFacebookEvent(string UniqueID, string Token, string eventname, string startTime, string endTime, string location, string description)
        {
            FacebookResponse ObjFacebookResponse = new FacebookResponse();            
            try
            {
                if (string.IsNullOrEmpty(FacebookGraphAPIURLEventCreate))
                {
                    FacebookGraphAPIURLEventCreate = "https://graph.facebook.com/{0}/events?access_token={1}";
                }

                string URL = string.Format(FacebookGraphAPIURLEventCreate, UniqueID, Token);

                Dictionary<string, string> PostData = new Dictionary<string, string>();

                if (!string.IsNullOrEmpty(eventname))
                    PostData.Add("name", System.Web.HttpUtility.UrlEncode(eventname));
                if (!string.IsNullOrEmpty(startTime))                    
                    PostData.Add("start_time", Convert.ToDateTime(startTime).ToUniversalTime().ToString("yyyy-MM-ddTHH:mm:ss.fffZ"));                    
                if (!string.IsNullOrEmpty(endTime))
                    PostData.Add("end_time", Convert.ToDateTime(endTime).ToUniversalTime().ToString("yyyy-MM-ddTHH:mm:ss.fffZ"));                    
                if (!string.IsNullOrEmpty(location))
                    PostData.Add("location", System.Web.HttpUtility.UrlEncode(location));
                if (!string.IsNullOrEmpty(description))
                    PostData.Add("description", System.Web.HttpUtility.UrlEncode(description));                
                
                string rawResponse = string.Empty;
                try
                {
                    rawResponse = HttpRequest(URL, PostData, UserAgent);

                    if (!string.IsNullOrEmpty(rawResponse))
                    {
                        var jss = new JavaScriptSerializer();
                        if (rawResponse.Contains("error"))
                        {
                            rawResponse = rawResponse.Replace(@"{""error"":", "");
                            rawResponse = rawResponse.Substring(0, rawResponse.Length - 1);
                        }

                        ObjFacebookResponse = jss.Deserialize<FacebookResponse>(rawResponse);
                    }
                    else
                    {
                        ObjFacebookResponse.message = "Invalid Http Request.";
                    }

                    //result = app.Post(dlSocialNetwork.UniqueID + "/feed", parameters);
                }
                catch (Exception ex)
                {
                    #region ERROR HANDLING

                    ObjFacebookResponse.message = ex.Message;
                    ObjFacebookResponse.code = string.Empty;
                    ObjFacebookResponse.type = "Exception";
                    ObjFacebookResponse.ID = string.Empty;
                    
                    #endregion
                }

            }
            catch (Exception ex)
            {
                #region ERROR HANDLING
             
                ObjFacebookResponse.ID = string.Empty;
                ObjFacebookResponse.message = ex.Message;
                ObjFacebookResponse.type = "Exception\\CreateFacebookEvent";
                ObjFacebookResponse.code = string.Empty;

                #endregion
            }

            return ObjFacebookResponse;
        }

        public static FacebookEventStatistics GetFacebookEventStatistics(string ResultID, string Token)
        {
            EventStatistics objEventStatistics = new EventStatistics();
            FacebookResponse ObjFacebookResponse = new FacebookResponse();
            FacebookEventStatistics objFacebookEventStatistics = new FacebookEventStatistics();

            string fields = "name,id,rsvp_status";
            string limit = "50000";
            string summary = "1";

            try
            {
                if (!string.IsNullOrEmpty(ResultID) && !string.IsNullOrEmpty(Token))
                {
                    if (string.IsNullOrEmpty(FacebookGraphAPIURLEventStatistics))
                    {
                        FacebookGraphAPIURLEventStatistics = "https://graph.facebook.com/{0}/invited?fields={1}&limit={2}&summary={3}&access_token={4}";
                    }

                    string URL = string.Format(FacebookGraphAPIURLEventStatistics, ResultID, fields, limit, summary, Token);

                    string rawResponse = string.Empty;

                    try
                    {
                        rawResponse = HttpRequest(URL, null, UserAgent);

                        if (!string.IsNullOrEmpty(rawResponse))
                        {
                            var jss = new JavaScriptSerializer();
                            if (rawResponse.StartsWith(@"{""error"":"))
                            {
                                rawResponse = rawResponse.Replace(@"{""error"":", "");
                                rawResponse = rawResponse.Substring(0, rawResponse.Length - 1);

                                ObjFacebookResponse = jss.Deserialize<FacebookResponse>(rawResponse);
                            }
                            else
                            {
                                objEventStatistics = jss.Deserialize<EventStatistics>(rawResponse);
                            }

                            objFacebookEventStatistics.facebookResponse = ObjFacebookResponse;
                            objFacebookEventStatistics.eventStatistics = objEventStatistics;
                        }
                        else
                        {
                            ObjFacebookResponse.message = "Problem to Retrying Event Statistics from FaceBook.";

                            objFacebookEventStatistics.facebookResponse = ObjFacebookResponse;
                            objFacebookEventStatistics.eventStatistics = objEventStatistics;
                        }
                    }

                    catch (Exception ex)
                    {
                        #region ERROR HANDLING
                        ObjFacebookResponse.message = ex.Message;
                        #endregion
                    }

                }
                else
                {
                    ObjFacebookResponse.message = "Invalid Parameters";

                    objFacebookEventStatistics.facebookResponse = ObjFacebookResponse;
                    objFacebookEventStatistics.eventStatistics = objEventStatistics;
                }
            }
            catch (Exception ex)
            {
                #region ERROR HANDLING
                ObjFacebookResponse.message = ex.Message;
                #endregion
            }

            return objFacebookEventStatistics;
        }

        #endregion

        #region INSIGHTS

		public static FaceBookJsonResponse GetJsonFaceBookPageInsights(string uniqueID, string token, string startDate, string endDate)
		{
			FaceBookJsonResponse faceBookJsonResponse = new FaceBookJsonResponse();

			try
			{
				string since = startDate.Replace(" ", "T");
				string until = endDate.Replace(" ", "T");

				if (string.IsNullOrEmpty(FacebookGraphAPIURLPageInsights))
				{
					FacebookGraphAPIURLPageInsights = "https://graph.facebook.com/{0}/insights?format=json&since={1}&until={2}&access_token={3}";
				}

				string URL = string.Format(FacebookGraphAPIURLPageInsights, uniqueID, since, until, token);
				string rawResponse = HttpRequest(URL, null, UserAgent);

				if (!string.IsNullOrEmpty(rawResponse))
				{
					faceBookJsonResponse.ResponseJson = rawResponse;
					if (rawResponse.StartsWith(@"{""error"":") == false)
						faceBookJsonResponse.Success = true;
				}
			}
			catch (Exception ex)
			{
				faceBookJsonResponse.ResponseJson = ex.Message;
			}
			return faceBookJsonResponse;
		}

        public static FacebookInsights GetFacebookPageInsights(string UniqueID, string token, string startDate, string endDate, string periods)
        {
            var objFacebookResponse = new FacebookResponse();
            var objFacebookInsights = new FacebookInsights();
            var objInsights = new Insights();
            try
            {
                var isValid = true;
                var errorMessage = string.Empty;

                var fromDate = Convert.ToDateTime(startDate);
                var toDate = Convert.ToDateTime(endDate);
                var toTime = string.Empty;
                if (fromDate > toDate)
                {
                    isValid = false;
                    errorMessage = "To Date Cannot be Greater than From Date.";
                }

                if (isValid)
                {
                    if (toDate.ToShortDateString() == DateTime.Now.ToShortDateString())
                    {
                        toTime = "T" + DateTime.Now.Hour.ToString() + ":00:00";
                    }
                    else
                    {
                        toTime = "T" + "23:59:00";
                    }
                    var since = fromDate.ToString("yyyy-MM-dd") + "T00:00:00";
                    var until = toDate.ToString("yyyy-MM-dd") + toTime;

                    if (string.IsNullOrEmpty(FacebookGraphAPIURLPageInsights))
                    {
                        FacebookGraphAPIURLPageInsights = "https://graph.facebook.com/{0}/insights?format=json&since={1}&until={2}&access_token={3}";                        
                    }

                    string URL = string.Format(FacebookGraphAPIURLPageInsights, UniqueID, since, until, token);
                    

                    string rawResponse = string.Empty;
                    try
                    {
                        rawResponse = HttpRequest(URL, null, UserAgent);

                        if (!string.IsNullOrEmpty(rawResponse))
                        {
                            var jss = new JavaScriptSerializer();
                            if (rawResponse.StartsWith(@"{""error"":"))
                            {
                                rawResponse = rawResponse.Replace(@"{""error"":", "");
                                rawResponse = rawResponse.Substring(0, rawResponse.Length - 1);

                                objFacebookResponse = jss.Deserialize<FacebookResponse>(rawResponse);

                                objFacebookInsights.facebookResponse = objFacebookResponse;
                                objFacebookInsights.insights = objInsights;
                            }
                            else
                            {
                                objInsights = JsonConvert.DeserializeObject<Insights>(rawResponse);

                                if (objInsights.data.Count > 0)
                                {
                                    if (string.IsNullOrEmpty(periods))
                                        periods = "day,lifetime,week";

                                    var words = periods.Split(',');
	                                var data = objInsights.data.Where(d => words.Contains(d.period)).ToList();

                                    if (data.Count == 0)
                                    {
                                        errorMessage = "Some of the period is invalid.";
                                        objFacebookResponse.message = objFacebookResponse.message + errorMessage;
                                    }

                                    objFacebookInsights.facebookResponse = objFacebookResponse;
                                    objFacebookInsights.insights.data = new List<DataInsights>();
                                    objFacebookInsights.insights.data = data;
                                }
                                else
                                {
                                    errorMessage = "No Insights Record Found.";
                                    objFacebookResponse.message = objFacebookResponse.message + errorMessage;

                                    objFacebookInsights.facebookResponse = objFacebookResponse;
                                    objFacebookInsights.insights.data = new List<DataInsights>();
                                }
                            }

                        }
                        else
                        {
                            objFacebookResponse.message = "Problem in Retrying FeceBook Insights.";

                            objFacebookInsights.facebookResponse = objFacebookResponse;
                            objFacebookInsights.insights = objInsights;
                        }
                    }
                    catch (Exception ex)
                    {
                        #region ERROR HANDLING

                        objFacebookResponse.message = ex.Message;
                        objFacebookResponse.code = string.Empty;
                        objFacebookResponse.type = "Exception\\GetFacebookPageInsights";
                        objFacebookResponse.ID = string.Empty;

                        #endregion
                    }
                }
                else
                {
                    objFacebookResponse.message = errorMessage;

                    objFacebookInsights.facebookResponse = objFacebookResponse;
                    objFacebookInsights.insights = objInsights;
                }
            }
            catch (Exception ex)
            {
                #region ERROR HANDLING
               
                objFacebookResponse.ID = string.Empty;
                objFacebookResponse.message = ex.Message;
                objFacebookResponse.type = "Exception\\GetFacebookPageInsights";
                objFacebookResponse.code = string.Empty;

                #endregion
            }

            return objFacebookInsights;
        }

        #endregion

        #region PAGE INFORMATION 


		public static FaceBookJsonResponse GetJsonFaceBookPage(string UniqueID, string Token)
		{
			FaceBookJsonResponse faceBookJsonResponse = new FaceBookJsonResponse();

			try
			{
				if (!string.IsNullOrEmpty(UniqueID) && !string.IsNullOrEmpty(Token))
				{
					string fields = "name,is_published,website,username,about,location,phone,can_post,checkins,were_here_count,talking_about_count,unread_notif_count,new_like_count,offer_eligible,category,id,link,likes,influences,keywords,members,parent_page,unread_message_count,unseen_message_count,network";

					if (string.IsNullOrEmpty(FacebookGraphAPIURLPage))
					{
						FacebookGraphAPIURLPage = "https://graph.facebook.com/{0}?fields={1}&access_token={2}";
					}

					string URL = string.Format(FacebookGraphAPIURLPage, UniqueID, fields, Token);
					string rawResponse = HttpRequest(URL, null, UserAgent);

					if (!string.IsNullOrEmpty(rawResponse))
					{
						faceBookJsonResponse.ResponseJson = rawResponse;
						
						if (rawResponse.StartsWith(@"{""error"":") == false)
							faceBookJsonResponse.Success = true;
					}
				}
			}
			catch (Exception ex)
			{
				faceBookJsonResponse.ResponseJson = ex.Message;
			}

			return faceBookJsonResponse;
		}

        public static FacebookPage GetFacebookPageInformation(string UniqueID, string Token)
        {            
            FacebookPage objFacebookPage = new FacebookPage();

            string fields = "name,is_published,website,username,about,location,phone,can_post,checkins,were_here_count,talking_about_count,unread_notif_count,new_like_count,offer_eligible,category,id,link,likes,influences,keywords,members,unread_message_count,unseen_message_count,network";
            FacebookResponse ObjFacebookResponse = new FacebookResponse();
            Page page = new Page();
            try
            {
                if (string.IsNullOrEmpty(FacebookGraphAPIURLPage))
                {
                    FacebookGraphAPIURLPage = "https://graph.facebook.com/{0}?fields={1}&access_token={2}";
                }
                
                if (!string.IsNullOrEmpty(UniqueID) && !string.IsNullOrEmpty(Token))
                {
                    string URL = string.Format(FacebookGraphAPIURLPage, UniqueID, fields, Token);

                    string rawResponse = string.Empty;

                    try
                    {
                        rawResponse = HttpRequest(URL, null, UserAgent);

                        if (!string.IsNullOrEmpty(rawResponse))
                        {
                            var jss = new JavaScriptSerializer();
                            if (rawResponse.StartsWith(@"{""error"":"))
                            {
                                rawResponse = rawResponse.Replace(@"{""error"":", "");
                                rawResponse = rawResponse.Substring(0, rawResponse.Length - 1);

                                ObjFacebookResponse = jss.Deserialize<FacebookResponse>(rawResponse);

                            }
                            else
                            {
                                page = jss.Deserialize<Page>(rawResponse);
                            }

                            objFacebookPage.facebookResponse = ObjFacebookResponse;
                            objFacebookPage.page = page;
                            objFacebookPage.responseJson = rawResponse;
                        }
                        else
                        {
                            ObjFacebookResponse.message = "Problem to Retrying Page Information from FaceBook.";

                            objFacebookPage.facebookResponse = ObjFacebookResponse;
                            objFacebookPage.page = page;
                            objFacebookPage.responseJson = rawResponse;
                        }
                    }
                    catch (Exception ex)
                    {
                        #region ERROR HANDLING
                        ObjFacebookResponse.message = ex.Message;
                        #endregion
                    }
                }
                else
                {
                    ObjFacebookResponse.message = "Invalid Parameters";

                    objFacebookPage.facebookResponse = ObjFacebookResponse;
                    objFacebookPage.page = page;
                }
            }
            catch (Exception ex)
            {
                #region ERROR HANDLING
                ObjFacebookResponse.message = ex.Message;
                #endregion
            }

            return objFacebookPage;
        }

        public static FacebookObjectIDByResultID GetFacebookObjectIDbyFeedID(string FeedID, string Token)
        {   
            string fields = "object_id,actions";

            FacebookResponse ObjFacebookResponse = new FacebookResponse();
            FacebookObjectIDByResultID objFacebookObjectIDByResultID = new FacebookObjectIDByResultID();
            try
            {
                if (!string.IsNullOrEmpty(FeedID) && !string.IsNullOrEmpty(Token))
                {
                    if (string.IsNullOrEmpty(FacebookGraphAPIURLObjectIDGetResultID))
                    {
                        FacebookGraphAPIURLObjectIDGetResultID = "https://graph.facebook.com/{0}?fields={1}&access_token={2}";
                    }

                    string URL = string.Format(FacebookGraphAPIURLObjectIDGetResultID, FeedID, fields, Token);

                    string rawResponse = string.Empty;
                    try
                    {
                        rawResponse = HttpRequest(URL, null, UserAgent);

                        if (!string.IsNullOrEmpty(rawResponse))
                        {
                            var jss = new JavaScriptSerializer();
                            if (rawResponse.StartsWith(@"{""error"":"))
                            {
                                rawResponse = rawResponse.Replace(@"{""error"":", "");
                                rawResponse = rawResponse.Substring(0, rawResponse.Length - 1);

                                ObjFacebookResponse = jss.Deserialize<FacebookResponse>(rawResponse);
                            }
                            else
                            {
                                objFacebookObjectIDByResultID = jss.Deserialize<FacebookObjectIDByResultID>(rawResponse);
                            }

                            objFacebookObjectIDByResultID.facebookResponse = ObjFacebookResponse;
                        }
                        else
                        {
                            ObjFacebookResponse.message = "Problem to Retrying Event By Result ID from FaceBook.";

                            objFacebookObjectIDByResultID.facebookResponse = ObjFacebookResponse;
                        }
                    }
                    catch (Exception ex)
                    {
                        #region ERROR HANDLING
                        ObjFacebookResponse.message = ex.Message;
                        #endregion
                    }
                }
                else
                {
                    ObjFacebookResponse.message = "Invalid Parameters";

                    objFacebookObjectIDByResultID.facebookResponse = ObjFacebookResponse;
                    //objFacebookEvent.events = events;
                }
                //473216489376933_368874216533892?fields=
            }
            catch (Exception ex)
            {
                #region ERROR HANDLING
                ObjFacebookResponse.message = ex.Message;
                #endregion
            }

            return objFacebookObjectIDByResultID;
        }

        public static FacebookSearch GetFacebookSearch(string PageName)
        {
            FacebookResponse ObjFacebookResponse = new FacebookResponse();
            FacebookSearch objFacebookSearch = new FacebookSearch();
            Search objSearch = new Search();

            try
            {
                if (!string.IsNullOrEmpty(PageName))
                {
                    if (string.IsNullOrEmpty(FacebookGraphAPIURLSearch))
                    {
                        FacebookGraphAPIURLSearch = "https://graph.facebook.com/search?q={0}&type=page&access_token=AAAAANqD36fQBAAFAqE1dFdLQG7NSBhtfvQkU5ZBwqabJDbfTGTYdKjw0WwT4h6fODoWoEtbbajypmqO21Akeund5PWzxh0Qvxwi6z6UHJ4ZA53bYxe";
                    }

                    string URL = string.Format(FacebookGraphAPIURLSearch, PageName);

                    string rawResponse = string.Empty;

                    try
                    {
                        rawResponse = HttpRequest(URL, null, UserAgent);

                        if (!string.IsNullOrEmpty(rawResponse))
                        {
                            var jss = new JavaScriptSerializer();
                            FacebookSearchPage objfacebookSearchPage = new FacebookSearchPage();

                            if (rawResponse.StartsWith(@"{""error"":"))
                            {
                                rawResponse = rawResponse.Replace(@"{""error"":", "");
                                rawResponse = rawResponse.Substring(0, rawResponse.Length - 1);

                                ObjFacebookResponse = jss.Deserialize<FacebookResponse>(rawResponse);
                            }
                            else
                            {
                                SearchData _objSearchData = jss.Deserialize<SearchData>(rawResponse);

                                if (_objSearchData.data != null)
                                {
                                    if (_objSearchData.data.Count > 1)
                                    {
                                        foreach (var item in _objSearchData.data)
                                        {
                                            if (item.name.Trim().ToLower() == PageName.Trim().ToLower())
                                            {
                                                objSearch.id = item.id;
                                                objSearch.name = item.name;

                                                objfacebookSearchPage = GetFacebookPageDetail(_objSearchData.data.FirstOrDefault().id);
                                                break;
                                            }
                                        }
                                    }
                                    else
                                    {
                                        objSearch.id = _objSearchData.data.FirstOrDefault().id;
                                        objSearch.name = _objSearchData.data.FirstOrDefault().name;

                                        objfacebookSearchPage = GetFacebookPageDetail(_objSearchData.data.FirstOrDefault().id);
                                    }

                                }
                            }
                            objFacebookSearch.data = objSearch;
                            objFacebookSearch.facebookSearchPage = objfacebookSearchPage;
                        }
                        else
                        {
                            ObjFacebookResponse.message = "Problem to Retrying PostStatistics from FaceBook.";
                            objFacebookSearch.data = objSearch;
                        }
                    }

                    catch (Exception ex)
                    {
                        #region ERROR HANDLING

                        ObjFacebookResponse.message = ex.Message;
                        
                        #endregion
                    }
                }
                else
                {
                    ObjFacebookResponse.message = "Invalid Parameters";
                    objFacebookSearch.data = objSearch;
                }
            }
            catch (Exception ex)
            {
                #region ERROR HANDLING
                ObjFacebookResponse.message = ex.Message;
                #endregion
            }

            objFacebookSearch.AccountName = PageName;

            return objFacebookSearch;
        }

        public static FacebookSearchPage GetFacebookPageDetail(string FacebookPageID)
        {
            FacebookResponse ObjFacebookResponse = new FacebookResponse();
            FacebookSearchPage objFacebookSearchPage = new FacebookSearchPage();

            string fields = "id,checkins,likes,link,location,name,phone,talking_about_count,username,website,were_here_count";
            try
            {
                if (!string.IsNullOrEmpty(FacebookPageID))
                {
                    if (string.IsNullOrEmpty(FacebookGraphAPIURLPage))
                    {
                        FacebookGraphAPIURLPage = "https://graph.facebook.com/{0}?fields={1}";
                    }

                    string URL = string.Format(FacebookGraphAPIURLPage, FacebookPageID, fields);

                    string rawResponse = string.Empty;

                    try
                    {

                        rawResponse = HttpRequest(URL, null, UserAgent);

                        if (!string.IsNullOrEmpty(rawResponse))
                        {
                            var jss = new JavaScriptSerializer();
                            if (rawResponse.StartsWith(@"{""error"":"))
                            {
                                rawResponse = rawResponse.Replace(@"{""error"":", "");
                                rawResponse = rawResponse.Substring(0, rawResponse.Length - 1);

                                ObjFacebookResponse = jss.Deserialize<FacebookResponse>(rawResponse);
                            }
                            else
                            {
                                objFacebookSearchPage = jss.Deserialize<FacebookSearchPage>(rawResponse);

                            }
                        }
                        else
                        {
                            ObjFacebookResponse.message = "Problem to Retrying PostStatistics from FaceBook.";
                        }
                    }
                    catch (Exception ex)
                    {
                        #region ERROR HANDLING
                        ObjFacebookResponse.message = ex.Message;
                        #endregion
                    }
                }
                else
                {
                    ObjFacebookResponse.message = "Invalid Parameters";
                }
            }
            catch (Exception ex)
            {
                #region ERROR HANDLING
                ObjFacebookResponse.message = ex.Message;
                #endregion
            }

            //objFacebookSearch.AccountName = PageName;

            return objFacebookSearchPage;
        }

        public static FacebookFeed GetFacebookFeed(string UniqueID, string Token, string FromDate, string ToDate, string limit = "250")
        {            
            FacebookFeed objFacebookFeed = new FacebookFeed();
            string fields = "created_time,id,object_id,type,application,from,message,picture,link,name,updated_time,description,caption,status_type";
            string format = "json";

            bool isValid = true;
            string ErrorMessage = string.Empty;

            //created_time,id,object_id,type,application&until=2012-10-22T00:00:00.0000000-06:00&since=2012-01-01T00:00:00.0000000-06:00&format=json&limit=1000
            FacebookResponse ObjFacebookResponse = new FacebookResponse();
            Feed feed = new Feed();
            try
            {
                if (string.IsNullOrEmpty(FacebookGraphAPIURLFeed))
                {
                    FacebookGraphAPIURLFeed = "https://graph.facebook.com/{0}/feed?fields={1}&since={2}&until={3}&format={4}&limit={5}&access_token={6}";                    
                }

                DateTime _FromDate = Convert.ToDateTime(FromDate);
                DateTime _ToDate = Convert.ToDateTime(ToDate);
                string toTime = string.Empty;
                if (_FromDate > _ToDate)
                {
                    isValid = false;
                    ErrorMessage = "To Date Cannot be Greater than From Date.";
                }

                if (isValid)
                {
                    if (!string.IsNullOrEmpty(UniqueID) && !string.IsNullOrEmpty(Token))
                    {
                        toTime = "T" + "23:59:00";

                        //if (_ToDate.ToShortDateString() == DateTime.Now.ToShortDateString())
                        //{
                        //    toTime = "T" + DateTime.Now.Hour.ToString() + ":00:00";
                        //}
                        //else
                        //{
                        //    toTime = "T" + "23:59:00";
                        //}
                        var since = _FromDate.ToString("yyyy-MM-dd") + "T00:00:00";
                        var until = _ToDate.ToString("yyyy-MM-dd") + toTime;

                        if (string.IsNullOrEmpty(limit))
                        {
                            limit = "250";
                        }
                        string URL = string.Format(FacebookGraphAPIURLFeed, UniqueID, fields, since, until, format, limit, Token);

                        string rawResponse = string.Empty;

                        try
                        {
                            rawResponse = HttpRequest(URL, null, UserAgent);

                            if (!string.IsNullOrEmpty(rawResponse))
                            {
                                var jss = new JavaScriptSerializer();
                                if (rawResponse.StartsWith(@"{""error"":"))
                                {
                                    rawResponse = rawResponse.Replace(@"{""error"":", "");
                                    rawResponse = rawResponse.Substring(0, rawResponse.Length - 1);

                                    ObjFacebookResponse = jss.Deserialize<FacebookResponse>(rawResponse);

                                }
                                else
                                {
                                    feed = jss.Deserialize<Feed>(rawResponse);
                                }

                                objFacebookFeed.facebookResponse = ObjFacebookResponse;
                                objFacebookFeed.feed = feed;
                            }
                            else
                            {
                                ObjFacebookResponse.message = "Problem to Retrying feed Information from FaceBook.";

                                objFacebookFeed.facebookResponse = ObjFacebookResponse;
                                objFacebookFeed.feed = feed;

                            }

                            //result = app.Post(dlSocialNetwork.UniqueID + "/feed", parameters);
                        }
                        catch (Exception ex)
                        {
                            #region ERROR HANDLING
                            ObjFacebookResponse.message = ex.Message;
                            #endregion
                        }
                    }
                    else
                    {

                        ObjFacebookResponse.message = "Invalid Parameters UniqueID or Token.";

                        objFacebookFeed.facebookResponse = ObjFacebookResponse;
                        objFacebookFeed.feed = feed;
                    }
                }
                else
                {
                    ObjFacebookResponse.message = ErrorMessage;

                    objFacebookFeed.facebookResponse = ObjFacebookResponse;
                    objFacebookFeed.feed = feed;
                }

            }
            catch (Exception ex)
            {
                #region ERROR HANDLING
                ObjFacebookResponse.message = ex.Message;
                #endregion
            }

            return objFacebookFeed;
        }

        public static FacebookFeed GetFacebookFeed(string UniqueID, string Token, string PageURL = "", string limit = "10")
        {
            FacebookFeed objFacebookFeed = new FacebookFeed();
            string fields = "created_time,id,object_id,type,application,from,message,picture,link,name,updated_time,description,caption";
            string format = "json";

            bool isValid = true;
            string ErrorMessage = string.Empty;

            FacebookResponse ObjFacebookResponse = new FacebookResponse();
            Feed feed = new Feed();
            try
            {
                if (string.IsNullOrEmpty(FacebookGraphAPIURLFeed))
                {
                    FacebookGraphAPIURLFeed = "https://graph.facebook.com/{0}/feed?fields={1}&format={2}&limit={3}&access_token={4}";
                }
                                
                if (isValid)
                {
                    if (!string.IsNullOrEmpty(UniqueID) && !string.IsNullOrEmpty(Token))
                    {
                        if (string.IsNullOrEmpty(limit))
                        {
                            limit = "10";
                        }

                        string URL = string.Empty;
                        if (string.IsNullOrEmpty(PageURL))
                        {
                            FacebookGraphAPIURLFeed = "https://graph.facebook.com/{0}/feed?fields={1}&format={2}&limit={3}&access_token={4}";
                            URL = string.Format(FacebookGraphAPIURLFeed, UniqueID, fields, format, limit, Token);
                        }
                        else
                        {
                            FacebookGraphAPIURLFeed = "https://graph.facebook.com/{0}/feed?fields={1}&format={2}&limit={3}&access_token={4}&until={5}";
                            URL = string.Format(FacebookGraphAPIURLFeed, UniqueID, fields, format, limit, Token, PageURL);
                        }

                        string rawResponse = string.Empty;

                        try
                        {
                            rawResponse = HttpRequest(URL, null, UserAgent);

                            if (!string.IsNullOrEmpty(rawResponse))
                            {
                                var jss = new JavaScriptSerializer();
                                if (rawResponse.StartsWith(@"{""error"":"))
                                {
                                    rawResponse = rawResponse.Replace(@"{""error"":", "");
                                    rawResponse = rawResponse.Substring(0, rawResponse.Length - 1);

                                    ObjFacebookResponse = jss.Deserialize<FacebookResponse>(rawResponse);

                                }
                                else
                                {
                                    feed = jss.Deserialize<Feed>(rawResponse);
                                }

                                objFacebookFeed.facebookResponse = ObjFacebookResponse;
                                objFacebookFeed.feed = feed;
                            }
                            else
                            {
                                ObjFacebookResponse.message = "Problem to Retrying feed Information from FaceBook.";

                                objFacebookFeed.facebookResponse = ObjFacebookResponse;
                                objFacebookFeed.feed = feed;

                            }

                            //result = app.Post(dlSocialNetwork.UniqueID + "/feed", parameters);
                        }
                        catch (Exception ex)
                        {
                            #region ERROR HANDLING
                            ObjFacebookResponse.message = ex.Message;
                            #endregion
                        }
                    }
                    else
                    {

                        ObjFacebookResponse.message = "Invalid Parameters UniqueID or Token.";

                        objFacebookFeed.facebookResponse = ObjFacebookResponse;
                        objFacebookFeed.feed = feed;
                    }
                }
                else
                {
                    ObjFacebookResponse.message = ErrorMessage;

                    objFacebookFeed.facebookResponse = ObjFacebookResponse;
                    objFacebookFeed.feed = feed;
                }

            }
            catch (Exception ex)
            {
                #region ERROR HANDLING
                ObjFacebookResponse.message = ex.Message;
                #endregion
            }

            return objFacebookFeed;
        }

        public static FacebookFeed GetFacebookFeed(string pagingUrl)
        {
            FacebookFeed objFacebookFeed = new FacebookFeed();


            return objFacebookFeed;
        }

        #endregion

        #region TOKEN INFORMATION

        public static List<string> GetFacebookPermissions(string Token)
        {            
            string message = string.Empty;
            string Xpath = string.Empty;
            string ParentnodePermissionsDetailsXPath = "//table[contains(@class,'uiInfoTable')]";
            string listnodePermissionsDetailsXPath = ".//tr";
            string td_text = string.Empty;
            List<string> listPermissions = new List<string>();
            
            if (!string.IsNullOrEmpty(Token))
            {
                string URL = (string.Format("https://developers.facebook.com/tools/debug/access_token?q={0}", Token));

                string responseHtml = HttpRequest(URL, null, UserAgent); //GetResponse(URL);

                var doc = new HtmlDocument();
                doc.OptionFixNestedTags = true;
                doc.LoadHtml(responseHtml);

                string Error = string.Empty;
                try
                {
                    Error = doc.DocumentNode.SelectSingleNode("//div[contains(@class,'pam uiBoxRed')]").InnerText;
                }
                catch
                {
                    Error = string.Empty;
                }

                if (string.IsNullOrEmpty(Error))
                {
                    Xpath = ParentnodePermissionsDetailsXPath;
                    HtmlNode reviewNode = doc.DocumentNode.SelectSingleNode(ParentnodePermissionsDetailsXPath);

                    Xpath = listnodePermissionsDetailsXPath;
                    var links = reviewNode.SelectNodes(listnodePermissionsDetailsXPath);

                    if (links != null && links.Count > 0)
                    {
                        string th_text = string.Empty;
                        bool isvalid = false;
                        foreach (var html in links)
                        {
                            Xpath = ".//th";
                            th_text = html.SelectSingleNode(".//th").InnerText;

                            if (th_text.ToLower().Contains("scopes"))
                            {
                                td_text = html.SelectSingleNode(".//td/a").InnerText;
                                isvalid = true;
                                break;
                            }
                            else if (th_text.ToLower().Contains("error parsing url"))
                            {
                                //Xpath = ".//td";
                                td_text = html.SelectSingleNode(".//td").InnerText;
                                isvalid = false;
                                break;
                            }
                        }

                        if (!string.IsNullOrEmpty(td_text) && isvalid)
                        {
                            string[] Permissions = td_text.Split(' ');
                            foreach (string Permission in Permissions)
                            {
                                listPermissions.Add(Permission);
                            }
                        }
                        else
                        {
                            listPermissions.Add(td_text);
                        }
                    }
                }
                else
                {
                    message = "The access token could not be decrypted";
                    listPermissions.Add(message);
                }
            }
            else
            {
                message = "Invalid Parameters UniqueID or Token.";
                listPermissions.Add(message);
            }
                       
            return listPermissions;
        }

		public static string GetFacebookAccessToken(string appID, string appCallback, string appSecret, string oAuthCode)
		{
			string AccessToken = "";

			if (!string.IsNullOrEmpty(appID))
			{
				if (string.IsNullOrEmpty(FacebookGraphAPIURLoAuthToken))
					FacebookGraphAPIURLoAuthToken = "https://graph.facebook.com/oauth/access_token?client_id={0}&redirect_uri={1}&client_secret={2}&code={3}";

				string URL = string.Format(FacebookGraphAPIURLoAuthToken, appID, appCallback, appSecret, oAuthCode);

				string rawResponse = string.Empty;
				try
				{
					rawResponse = HttpRequest(URL, null, UserAgent);

					if (!string.IsNullOrEmpty(rawResponse) && rawResponse.StartsWith("access_token"))
					{
						AccessToken = rawResponse.Substring(rawResponse.IndexOf("=") + 1);
						if (AccessToken.IndexOf("&expires") > 0)
						{
							AccessToken = AccessToken.Substring(0, AccessToken.IndexOf("&expires"));
						}
					}
				}
				catch (Exception ex)
				{

				}
			}

			return AccessToken;
		}

        public static bool ValidateToken(string Token, string Apptoken)
        {
            bool isValid = false;

            FacebookTokenInfo objFacebookTokenInfo = new FacebookTokenInfo();
            DebugToken objDebugToken = new DebugToken();
            FacebookResponse objFacebookResponse = new FacebookResponse();

            try
            {
                if (!string.IsNullOrEmpty(Token) && !string.IsNullOrEmpty(Apptoken))
                {
                    string URL = string.Format(FacebookTokenValidateURL, Token, Apptoken);

                    string rawResponse = HttpRequest(URL, null, UserAgent);

                    if (!string.IsNullOrEmpty(rawResponse))
                    {
                        var jss = new JavaScriptSerializer();

                        objDebugToken = jss.Deserialize<DebugToken>(rawResponse);

                        //if (rawResponse.StartsWith(@"{""error"":"))
                        //{
                        //    rawResponse = rawResponse.Replace(@"{""error"":", "");
                        //    rawResponse = rawResponse.Substring(0, rawResponse.Length - 1);

                        //    objFacebookResponse = jss.Deserialize<FacebookResponse>(rawResponse);
                        //    isValid = false;
                        //}
                        //else
                        //{
                        //    objDebugToken = jss.Deserialize<DebugToken>(rawResponse);
                        //}

                        objFacebookTokenInfo.facebookResponse = objFacebookResponse;
                        objFacebookTokenInfo.debugToken = objDebugToken;

                        if (objDebugToken != null)
                        {
                            if (!string.IsNullOrEmpty(objDebugToken.data.error.message))
                            {
                                isValid = false;
                            }
                            else
                            {
                                isValid = objDebugToken.data.is_valid;
                            }
                        }
                        
                    }
                    else
                    {
                        isValid = false;
                    }
                }
                else
                {
                    return isValid;
                }
            }
            catch (Exception ex)
            {
                return isValid;
            }

            return isValid;
        }

        public static FacebookTokenInfo ValidateToken2(string Token, string Apptoken)
        {            
            FacebookTokenInfo objFacebookTokenInfo = new FacebookTokenInfo();
            DebugToken objDebugToken = new DebugToken();
            FacebookResponse objFacebookResponse = new FacebookResponse();

            try
            {
                if (!string.IsNullOrEmpty(Token) && !string.IsNullOrEmpty(Apptoken))
                {
                    string URL = string.Format(FacebookTokenValidateURL, Token, Apptoken);

                    string rawResponse = HttpRequest(URL, null, UserAgent);

                    if (!string.IsNullOrEmpty(rawResponse))
                    {
                        var jss = new JavaScriptSerializer();

                        objDebugToken = jss.Deserialize<DebugToken>(rawResponse);
                                              
                        objFacebookTokenInfo.facebookResponse = objFacebookResponse;
                        objFacebookTokenInfo.debugToken = objDebugToken;
                    }                    
                }                
            }
            catch (Exception ex)
            {                
            }

            return objFacebookTokenInfo;
        }

        public static bool ValidateTokenForOldCARFAXApp(string Token,string UniqueID)
        {
            bool isValid = false;

            FacebookTokenInfo objFacebookTokenInfo = new FacebookTokenInfo();
            DebugToken objDebugToken = new DebugToken();
            FacebookResponse objFacebookResponse = new FacebookResponse();

            try
            {
                if (!string.IsNullOrEmpty(Token))
                {
                    string URL = string.Format("https://graph.facebook.com/v2.0/{0}?access_token={1}",UniqueID, Token);

                    string rawResponse = HttpRequest(URL, null, UserAgent);

                    if (!string.IsNullOrEmpty(rawResponse))
                    {
                        var jss = new JavaScriptSerializer();
                        if (rawResponse.StartsWith(@"{""error"":"))
                        {
                            rawResponse = rawResponse.Replace(@"{""error"":", "");
                            rawResponse = rawResponse.Substring(0, rawResponse.Length - 1);

                            objFacebookResponse = jss.Deserialize<FacebookResponse>(rawResponse);
                            isValid = false;
                        }
                        else
                        {
                            objDebugToken = jss.Deserialize<DebugToken>(rawResponse);
                            isValid = true;
                        }
                    }
                    else
                    {
                        isValid = false;
                    }
                }
                else
                {
                    return isValid;
                }
            }
            catch (Exception ex)
            {
                return isValid;
            }

            return isValid;
        }
        
        #endregion

        #region Ads

        public static FacebookAdsAccountInfo GetAdsAccountInfo(string accountNumber, string token)
        {
            //https://graph.facebook.com/act_124232571038675/?fields=account_id,account_status,balance,business_name,capabilities,daily_spend_limit,id,name,spend_cap,timezone_id,timezone_name&access_token=CAAB945ZCZALqgBAN7Ja0lSIDa8Uoe2XaDa5lllZBIKurGs637Sr4lxQ9c4dcLfZCdLCV2u7tZCokPHPysrcx9CJ9p8QKx6izsZBzrik8qN5BgVeEcjmtwQRfZCZCFiG6ShEURcts3nI0YeOFbgnR9NTxWUhf6F9ZC1QQVq0SX95JG2NsiqJHX4gzAX9NWj7ZATLh491rmtPQYchgZDZD
            var result = new FacebookAdsAccountInfo();
            var htmlResult = string.Empty;
            _WebNet = new SInet(true, 1, "", 30);

            if (!String.IsNullOrEmpty(token) && !String.IsNullOrEmpty(accountNumber))
            {
                var fields = "account_id,account_status,balance,business_name,capabilities,daily_spend_limit,id,name,spend_cap,timezone_id,timezone_name";

                if (string.IsNullOrEmpty(FacebookAdsAccountInfoURL))
                {
                    FacebookAdsAccountInfoURL = "https://graph.facebook.com/{0}/?fields={1}&access_token={2}";
                }

                var URL = string.Format(FacebookAdsAccountInfoURL, accountNumber, fields, token);
                dynamic dynamicResult;

                try
                {
                    dynamicResult = _WebNet.GetRequestDynamic(URL, out htmlResult);

                    if (!string.IsNullOrEmpty(htmlResult))
                    {
                        var jss = new JavaScriptSerializer();
                        if (htmlResult.StartsWith(@"{""error"":"))
                        {
                            #region Error
                            htmlResult = htmlResult.Replace(@"{""error"":", "");
                            htmlResult = htmlResult.Substring(0, htmlResult.Length - 1);
                            result.FacebookResponse = jss.Deserialize<FacebookResponse>(htmlResult);
                            result.Success = false;
                            result.FailureCodes.Add("Facebook Error");
                            #endregion
                        }
                        else
                        {
                            if (dynamicResult != null)
                            {
                                #region Extract Fields
                                result.AccountStatus = (FacebookAdsAccountStatusEnum)Convert.ToInt32(dynamicResult.account_status ?? 0);
                                result.AccountId = dynamicResult.account_id ?? "";
                                result.DailySpendLimit = Convert.ToInt32(dynamicResult.daily_spend_limit ?? 0);
                                result.BusinessName = dynamicResult.business_name ?? "";
                                result.AdsAccountName = dynamicResult.name ?? "";
                                result.Balance = Convert.ToInt32(dynamicResult.balance ?? 0);
                                result.TimezoneName = dynamicResult.timezone_name ?? "";
                                result.Id = dynamicResult.id ?? "";
                                result.TimezoneId = Convert.ToInt32(dynamicResult.timezone_id ?? 0);

                                if (dynamicResult.capabilities != null && dynamicResult.capabilities.Count > 0)
                                {
                                    foreach (dynamic item in dynamicResult.capabilities)
                                    {
                                        result.Capabilities.Add((FacebookAdsAccountCapabilitiesEnum)Convert.ToInt32(item.Value ?? 0));
                                    }
                                }

                                result.Success = true;
                                #endregion
                            }
                        }
                    }
                    else
                    {
                        result.Success = false;
                        result.FailureCodes.Add("Error while executing GetAdsAccountInfo() FaceBook");
                    }
                }
                catch (Exception ex)
                {
                    #region ERROR HANDLING
                    #endregion
                }

            }
            else
            {
                result.Success = false;
                result.FailureCodes.Add("Invalid input parameters");
            }

            return result;
        }

        public static FacebookAdsCampaignsLists GetAdsCampaignListByAdsAccountID(string accountNumber, string token)
        {
            //https://graph.facebook.com/act_124232571038675/adcampaigns?fields=account_id,name,campaign_status,start_time,end_time,updated_time,created_time,daily_budget,lifetime_budget,budget_remaining&access_token=CAAB945ZCZALqgBAOZCpdWl0OOTZAQWWbeAHFccNNLmgNjGqxpq4BNw4GrynOQxsZA2MFNmNxMUC1jFHpKqr27FO2VCFdFnbv6BrFWXK5BmSujN0sHFITrZC9VvIzElZB91wko5KZBECInZBARAZCPZCMZBm9aAkISF6lLSaaKD2PyK8TPy7SRLR1P1SVMKruTrZAc05H4j7bjPytHWQZDZD

            var result = new FacebookAdsCampaignsLists();
            var htmlResult = string.Empty;
            FacebookAdsCampaignDetails facebookAdsCampaignDetails;
            _WebNet = new SInet(true, 1, "", 30);

            if (!String.IsNullOrEmpty(token) && !String.IsNullOrEmpty(accountNumber))
            {
                var fields = "account_id,name,campaign_status,start_time,end_time,updated_time,created_time,daily_budget,lifetime_budget,budget_remaining";

                if (string.IsNullOrEmpty(FacebookAdsCampaignListURL))
                {
                    FacebookAdsCampaignListURL = "https://graph.facebook.com/{0}/adcampaigns?fields={1}&access_token={2}";
                }

                var URL = string.Format(FacebookAdsCampaignListURL, accountNumber, fields, token);
                dynamic dynamicResult;

                try
                {
                    dynamicResult = _WebNet.GetRequestDynamic(URL, out htmlResult);

                    if (!string.IsNullOrEmpty(htmlResult))
                    {
                        var jss = new JavaScriptSerializer();
                        if (htmlResult.StartsWith(@"{""error"":"))
                        {
                            #region Error
                            htmlResult = htmlResult.Replace(@"{""error"":", "");
                            htmlResult = htmlResult.Substring(0, htmlResult.Length - 1);
                            result.FacebookResponse = jss.Deserialize<FacebookResponse>(htmlResult);
                            result.Success = false;
                            result.FailureCodes.Add("Facebook Error");
                            #endregion
                        }
                        else
                        {
                            if (dynamicResult != null)
                            {
                                if (dynamicResult.data != null && dynamicResult.data.Count > 0)
                                {
                                    foreach (dynamic item in dynamicResult.data)
                                    {
                                        #region Extract Fields
                                        facebookAdsCampaignDetails = new FacebookAdsCampaignDetails();
                                        facebookAdsCampaignDetails.AccountId = item.account_id ?? "";
                                        facebookAdsCampaignDetails.CampaignStatus = ((FacebookAdsCampaignStatusEnum)Convert.ToInt32(item.campaign_status ?? 0));
                                        facebookAdsCampaignDetails.LifetimeBudget = Convert.ToInt32(item.lifetime_budget ?? 0);
                                        facebookAdsCampaignDetails.CampaignName = item.name ?? "";
                                        facebookAdsCampaignDetails.StartTime = DateTime.Parse(item.start_time ?? DateTime.Now);
                                        facebookAdsCampaignDetails.CreatedTime = DateTime.Parse(item.created_time ?? DateTime.Now);
                                        facebookAdsCampaignDetails.EndTime = DateTime.Parse(item.end_time ?? DateTime.Now);
                                        facebookAdsCampaignDetails.UpdatedTime = DateTime.Parse(item.updated_time ?? DateTime.Now);
                                        facebookAdsCampaignDetails.CampaignId = item.id ?? "";
                                        facebookAdsCampaignDetails.BudgetRemaining = Convert.ToInt32(item.budget_remaining ?? 0);
                                        facebookAdsCampaignDetails.Success = true;
                                        result.Campaigns.Add(facebookAdsCampaignDetails);
                                        #endregion
                                    }
                                }

                                result.Success = true;
                            }
                        }
                    }
                    else
                    {
                        result.Success = false;
                        result.FailureCodes.Add("Error while executing GetAdsCampaignListByAdsAccountID() FaceBook");
                    }
                }
                catch (Exception ex)
                {
                    #region ERROR HANDLING
                    #endregion
                }

            }
            else
            {
                result.Success = false;
                result.FailureCodes.Add("Invalid input parameters");
            }

            return result;

        }

        public static FacebookAdsCampaignDetails GetAdsCampaignInfoByCampaignID(string campaignID, string token)
        {
            //https://graph.facebook.com/6013059452397?fields=account_id,name,campaign_status,start_time,end_time,updated_time,created_time,daily_budget,lifetime_budget,budget_remaining&access_token=CAAB945ZCZALqgBAOZCpdWl0OOTZAQWWbeAHFccNNLmgNjGqxpq4BNw4GrynOQxsZA2MFNmNxMUC1jFHpKqr27FO2VCFdFnbv6BrFWXK5BmSujN0sHFITrZC9VvIzElZB91wko5KZBECInZBARAZCPZCMZBm9aAkISF6lLSaaKD2PyK8TPy7SRLR1P1SVMKruTrZAc05H4j7bjPytHWQZDZD

            var result = new FacebookAdsCampaignDetails();
            var htmlResult = string.Empty;

            _WebNet = new SInet(true, 1, "", 30);

            if (!String.IsNullOrEmpty(token) && !String.IsNullOrEmpty(campaignID))
            {
                var fields = "account_id,name,campaign_status,start_time,end_time,updated_time,created_time,daily_budget,lifetime_budget,budget_remaining";

                if (string.IsNullOrEmpty(FacebookAdsCampaignInfoURL))
                {
                    FacebookAdsCampaignInfoURL = "https://graph.facebook.com/{0}/?fields={1}&access_token={2}";
                }

                var URL = string.Format(FacebookAdsCampaignInfoURL, campaignID, fields, token);
                dynamic dynamicResult;

                try
                {
                    dynamicResult = _WebNet.GetRequestDynamic(URL, out htmlResult);

                    if (!string.IsNullOrEmpty(htmlResult))
                    {
                        var jss = new JavaScriptSerializer();
                        if (htmlResult.StartsWith(@"{""error"":"))
                        {
                            #region Error
                            htmlResult = htmlResult.Replace(@"{""error"":", "");
                            htmlResult = htmlResult.Substring(0, htmlResult.Length - 1);
                            result.FacebookResponse = jss.Deserialize<FacebookResponse>(htmlResult);
                            result.Success = false;
                            result.FailureCodes.Add("Facebook Error");
                            #endregion
                        }
                        else
                        {
                            if (dynamicResult != null)
                            {
                                #region Extract Fields
                                result.AccountId = dynamicResult.account_id ?? "";

                                result.CampaignStatus = ((FacebookAdsCampaignStatusEnum)Convert.ToInt32(dynamicResult.campaign_status ?? 0));
                                result.LifetimeBudget = Convert.ToInt32(dynamicResult.lifetime_budget ?? 0);
                                result.CampaignName = dynamicResult.name ?? "";
                                result.StartTime = DateTime.Parse(dynamicResult.start_time ?? DateTime.Now);
                                result.CreatedTime = DateTime.Parse(dynamicResult.created_time ?? DateTime.Now);
                                result.EndTime = DateTime.Parse(dynamicResult.end_time ?? DateTime.Now);
                                result.UpdatedTime = DateTime.Parse(dynamicResult.updated_time ?? DateTime.Now);
                                result.CampaignId = dynamicResult.id ?? "";
                                result.BudgetRemaining = Convert.ToInt32(dynamicResult.budget_remaining ?? 0);
                                #endregion

                                result.Success = true;
                            }
                        }
                    }
                    else
                    {
                        result.Success = false;
                        result.FailureCodes.Add("Error while executing GetAdsCampaignInfoByCampaignID() FaceBook");
                    }
                }
                catch (Exception ex)
                {
                    #region ERROR HANDLING
                    #endregion
                }

            }
            else
            {
                result.Success = false;
                result.FailureCodes.Add("Invalid input parameters");
            }

            return result;
        }

        public static FacebookAdsCampaignStats GetAdsCampaignInfoStats(string campaignID, string token)
        {
            //https://graph.facebook.com/6013059452397/stats?access_token=CAAB945ZCZALqgBAOZCpdWl0OOTZAQWWbeAHFccNNLmgNjGqxpq4BNw4GrynOQxsZA2MFNmNxMUC1jFHpKqr27FO2VCFdFnbv6BrFWXK5BmSujN0sHFITrZC9VvIzElZB91wko5KZBECInZBARAZCPZCMZBm9aAkISF6lLSaaKD2PyK8TPy7SRLR1P1SVMKruTrZAc05H4j7bjPytHWQZDZD

            var result = new FacebookAdsCampaignStats();
            var htmlResult = string.Empty;

            _WebNet = new SInet(true, 1, "", 30);

            if (!String.IsNullOrEmpty(token) && !String.IsNullOrEmpty(campaignID))
            {
                if (string.IsNullOrEmpty(FacebookAdsCampaignStatsURL))
                {
                    FacebookAdsCampaignStatsURL = "https://graph.facebook.com/{0}/stats?&access_token={1}";
                }

                var URL = string.Format(FacebookAdsCampaignStatsURL, campaignID, token);
                dynamic dynamicResult;

                try
                {
                    dynamicResult = _WebNet.GetRequestDynamic(URL, out htmlResult);

                    if (!string.IsNullOrEmpty(htmlResult))
                    {
                        var jss = new JavaScriptSerializer();
                        if (htmlResult.StartsWith(@"{""error"":"))
                        {
                            #region Error
                            htmlResult = htmlResult.Replace(@"{""error"":", "");
                            htmlResult = htmlResult.Substring(0, htmlResult.Length - 1);
                            result.FacebookResponse = jss.Deserialize<FacebookResponse>(htmlResult);
                            result.Success = false;
                            result.FailureCodes.Add("Facebook Error");
                            #endregion
                        }
                        else
                        {
                            if (dynamicResult != null)
                            {
                                #region Extract Fields
                                result.Id = dynamicResult.id ?? "";
                                result.CampaignId = dynamicResult.campaign_id ?? "";
                                result.ToplineId = dynamicResult.topline_id ?? "";
                                result.StartTime = DateTime.Parse(dynamicResult.start_time ?? DateTime.Now);
                                result.EndTime = DateTime.Parse(dynamicResult.end_time ?? DateTime.Now);
                                result.ImpressionsCount = Convert.ToInt32(dynamicResult.impresions ?? 0);
                                result.ClicksCount = Convert.ToInt32(dynamicResult.clicks ?? 0);
                                result.Spent = Convert.ToInt32(dynamicResult.spent ?? 0);
                                result.SocialImpressionsCount = Convert.ToInt32(dynamicResult.social_impressions ?? 0);
                                result.SocialClicksCount = Convert.ToInt32(dynamicResult.social_clicks ?? 0);
                                result.SocialSpent = Convert.ToInt32(dynamicResult.social_spent ?? 0);
                                result.UniqueImpressionsCount = Convert.ToInt32(dynamicResult.unique_impressions ?? 0);
                                result.SocialUniqueImpressions = Convert.ToInt32(dynamicResult.social_unique_impressions ?? 0);
                                result.UniqueClicks = Convert.ToInt32(dynamicResult.unique_clicks ?? 0);
                                result.SocialUniqueClicks = Convert.ToInt32(dynamicResult.social_unique_clicks ?? 0);
                                result.IsCompleted = Convert.ToBoolean(dynamicResult.is_completed ?? false);

                                if (dynamicResult.actions != null)
                                {
                                    result.ActionsPhotoViewCount = Convert.ToInt32(dynamicResult.actions.photo_view ?? 0);
                                    result.ActionsPostLikeCount = Convert.ToInt32(dynamicResult.actions.post_like ?? 0);
                                    result.ActionsLinkClickCount = Convert.ToInt32(dynamicResult.actions.link_click ?? 0);
                                    result.ActionsCommentCount = Convert.ToInt32(dynamicResult.actions.comment ?? 0);
                                    result.ActionsLikeCount = Convert.ToInt32(dynamicResult.actions.like ?? 0);
                                    result.ActionsPostCount = Convert.ToInt32(dynamicResult.actions.post ?? 0);
                                }

                                if (dynamicResult.inline_actions != null)
                                {
                                    result.InlineActionsTitleClicksCount = Convert.ToInt32(dynamicResult.inline_actions.title_clicks ?? 0);
                                    result.InlineActionsLikeCount = Convert.ToInt32(dynamicResult.inline_actions.like ?? 0);
                                    result.InlineActionsRsvpYesCount = Convert.ToInt32(dynamicResult.inline_actions.rsvp_yes ?? 0);
                                    result.InlineActionsRsvpMaybeCount = Convert.ToInt32(dynamicResult.inline_actions.rsvp_maybe ?? 0);
                                    result.InlineActionsPostLikeCount = Convert.ToInt32(dynamicResult.inline_actions.post_like ?? 0);
                                    result.InlineActionsCommentCount = Convert.ToInt32(dynamicResult.inline_actions.comment ?? 0);
                                    result.InlineActionsPhotoViewCount = Convert.ToInt32(dynamicResult.inline_actions.photo_view ?? 0);
                                    result.InlineActionsLinkClickCount = Convert.ToInt32(dynamicResult.inline_actions.link_click ?? 0);
                                    result.InlineActionsVideoPlayCount = Convert.ToInt32(dynamicResult.inline_actions.video_play ?? 0);
                                    result.InlineActionsQuestionVoteCount = Convert.ToInt32(dynamicResult.inline_actions.question_vote ?? 0);
                                }

                                #endregion

                                result.Success = true;
                            }
                        }
                    }
                    else
                    {
                        result.Success = false;
                        result.FailureCodes.Add("Error while executing GetAdsCampaignInfoByCampaignID() FaceBook");
                    }
                }
                catch (Exception ex)
                {
                    #region ERROR HANDLING
                    #endregion
                }

            }
            else
            {
                result.Success = false;
                result.FailureCodes.Add("Invalid input parameters");
            }

            return result;
        }

        public static void GetAdsStatsAllCampaignsinAccount(string accountNumber)
        {
            //https://graph.facebook.com/act_124232571038675/adcampaignstats?access_token=CAAB945ZCZALqgBAI2S108Mcc8PHHCcZAMT2hA7xOiTv0BAqSasIttABPHVKZCswXTgOAZBgX2MQppn6qg3SEpoFYSZCvzNwGSOvSZClJJAZCneXbWg8JbV7NgCok5JOH5FfXNA8yqvxrSJTL4UUm5EtOLnqUNkL7IjLxq8uK9ZA6l3OPYy4EME9A01l9AI3NqbEZAUHf8hQo2tXgZDZD








        }

        public static void GetAdsCampaignAdGroups(string accountNumber)
        {
            //https://graph.facebook.com/6013201367997/adgroups?access_token=CAAB945ZCZALqgBAI2S108Mcc8PHHCcZAMT2hA7xOiTv0BAqSasIttABPHVKZCswXTgOAZBgX2MQppn6qg3SEpoFYSZCvzNwGSOvSZClJJAZCneXbWg8JbV7NgCok5JOH5FfXNA8yqvxrSJTL4UUm5EtOLnqUNkL7IjLxq8uK9ZA6l3OPYy4EME9A01l9AI3NqbEZAUHf8hQo2tXgZDZD
        }

        public static void GetAdsCampaignAdCreatives(string accountNumber)
        {
            //https://graph.facebook.com/6013059452397/adcreatives/?fields=name,type,object_id,body,image_url,id,title,link_url,preview_url,related_fan_page,story_id&access_token=CAAAANqD36fQBAM7LKTCNspN6UsUCIWRhNQOUyJQOZCmXc6FI6jZAGMZBu5ZCJtHY2ZBfWeXQAIZBlZC7IweAeUfyaMHAxgjBNcfT6OwOUYdjLzewxKd6kPaZBXfCO4lK766W6tIwq0VAe19v70iI3IV44zPAt6H3d5vTvHWjKoGPTJv8uQCzYZArcW5kDFw53Fwmhi1pSK0WunAZDZD
        }


        #endregion

        #region Reviews

        public static FacebookReviewResponse GetReviews(string uniqueID, string Token, bool getAllReviews = false, string limit = "100")
        {
            //Sample Query
            //https://graph.facebook.com/v2.0/175419496588/ratings?fields=created_time,has_rating,has_review,open_graph_story,rating,review_text,reviewer&limit=75&access_token=CAAGfD25ViIcBABVMJTaAtMXcNjwxtzU6aEDQzC3kGTAUZAdu6jfvK2ZAqHy6ZCKcNQusMlUQa2vXLU6CD7h6WPE7FbfZC5wv03qXlF6DQOQ87UdUKaGPHHRWRx13JHo2noCY3xwK31EZCzlZC1akGCugZBhHYYyXPbe6NciVFGLXhNfd3VakGZCj5Jms4IjNxnEZD

            var facebookReviewResponseReturn = new FacebookReviewResponse();
            //var facebookReviewList = new FacebookReviewList();
            string fields = "created_time,has_rating,has_review,open_graph_story,rating,review_text,reviewer";

            if (!string.IsNullOrEmpty(uniqueID) && !string.IsNullOrEmpty(Token))
            {

                //https://graph.facebook.com/v2.0/{0}/ratings?fields={1}&limit={2}&access_token={3}
                FacebookGraphAPIRatingURL = "https://graph.facebook.com/v2.0/{0}/ratings?fields={1}&access_token={2}";
                var URL = string.Format(FacebookGraphAPIRatingURL, uniqueID, fields, Token);

                using (var client = new WebClient())
                {
                    //var rawResponse = HttpRequest(URL, null, UserAgent);
                    var rawResponse = GetReviews(URL);
                    
                    var facebookReviewResponse = DeserializeReviews(rawResponse);

                    if (!facebookReviewResponse.IsSuccess)
                    {
                        if (rawResponse.Contains(@"""code"":100"))
                        {
                            FacebookGraphAPIRatingURL = "https://graph.facebook.com/v2.0/{0}/ratings?access_token={1}";
                            URL = string.Format(FacebookGraphAPIRatingURL, uniqueID, Token);
                            rawResponse = GetReviews(URL);
                            facebookReviewResponse = DeserializeReviews(rawResponse);
                            FacebookGraphAPIRatingURL = "https://graph.facebook.com/v2.0/{0}/ratings?fields={1}&access_token={2}";
                        }
                    }

                    if (facebookReviewResponse.IsSuccess)
                    {
                        facebookReviewResponseReturn.IsSuccess = true;
                        foreach (var review in facebookReviewResponse.ReviewList.data)
                        {
                            facebookReviewResponseReturn.ReviewList.data.Add(review);
                        }

                        if (getAllReviews)
                        {
                            while (!string.IsNullOrEmpty(facebookReviewResponse.ReviewList.paging.cursors.after))
                            {
                                rawResponse = GetReviews(URL + "&after=" + facebookReviewResponse.ReviewList.paging.cursors.after);
                                facebookReviewResponse = DeserializeReviews(rawResponse);

                                if (!facebookReviewResponse.IsSuccess)
                                {
                                    if (rawResponse.Contains(@"""code"":100"))
                                    {
                                        FacebookGraphAPIRatingURL = "https://graph.facebook.com/v2.0/{0}/ratings?access_token={1}";
                                        URL = string.Format(FacebookGraphAPIRatingURL, uniqueID, Token);
                                        rawResponse = GetReviews(URL);
                                        facebookReviewResponse = DeserializeReviews(rawResponse);
                                        FacebookGraphAPIRatingURL = "https://graph.facebook.com/v2.0/{0}/ratings?fields={1}&access_token={2}";
                                    }
                                }


                                if (facebookReviewResponse.IsSuccess)
                                {
                                    facebookReviewResponseReturn.IsSuccess = true;
                                    foreach (var review in facebookReviewResponse.ReviewList.data)
                                    {
                                        facebookReviewResponseReturn.ReviewList.data.Add(review);
                                    }
                                }
                            }
                        }
                    }
                    else
                    {
                        facebookReviewResponseReturn = facebookReviewResponse;
                    }
                }
            }
            else
            {
                facebookReviewResponseReturn.IsSuccess = false;
                facebookReviewResponseReturn.FacebookResponse.message = "Missing or Invalid Parameters";
            }

            return facebookReviewResponseReturn;
        }

        private static FacebookReviewResponse DeserializeReviews(string rawResponse)
        {
            var facebookReviewResponse = new FacebookReviewResponse();
            var facebookReviewList = new FacebookReviewList();

            if (!string.IsNullOrEmpty(rawResponse))
            {
                if (rawResponse.Contains("error") || rawResponse.Contains("exception"))
                {
                    rawResponse = rawResponse.Replace(@"{""error"":", "");
                    rawResponse = rawResponse.Substring(0, rawResponse.Length - 1);
                    facebookReviewResponse.IsSuccess = false;
                    facebookReviewResponse.FacebookResponse.message = rawResponse;
                }
                else
                {
                    var jss = new JavaScriptSerializer();
                    facebookReviewList = jss.Deserialize<FacebookReviewList>(rawResponse);
                    //FixReviewText(ref facebookReviewList);
                    facebookReviewResponse.ReviewList = facebookReviewList;                   
                    facebookReviewResponse.IsSuccess = true;
                }
            }

            return facebookReviewResponse;
        }
        private static string GetReviews(string URL)
        {
            string rawResponse = string.Empty;

            if (!string.IsNullOrEmpty(URL))
            {
                try
                {
                    using (var client = new WebClient())
                    {
                        rawResponse = HttpRequest(URL, null, UserAgent);
                    }
                }
                catch (WebException wex)
                {
                    if (wex.Response != null)
                    {
                        using (var errorResponse = (HttpWebResponse)wex.Response)
                        {
                            using (var reader = new StreamReader(errorResponse.GetResponseStream()))
                            {                                
                                rawResponse = reader.ReadToEnd();
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    rawResponse = "exception";
                }
            }
            else
            {
                rawResponse = "error";
            }

            return rawResponse;
        }

        #endregion

        public static FacebookMessageResponse GetMessages(string uniqueID, string Token)
        {
            //Sample Query
            //https://graph.facebook.com/255547016773/conversations?format=json&limit=500&fields=id,snippet,updated_time,message_count,unread_count,participants,senders&access_token=CAAGfD25ViIcBAC1sjcdMextc9jh1uQ85ZAc2bQyvqaxaKWgORhZCxH8GI62unPC9MYeZA9cqZBmyHJJUMGzVaxfo3W8bGrbg5JNHTaIPDmR21G899vMT6dNuW9JekZB1bkIBdVZBAvKRwcthsdxcZC9ZAtMXHOHlI6qGKo5y6j1M87EDXVkbRrFp
            
            var facebookMessageResponse = new FacebookMessageResponse();
            var facebookMessageList = new FacebookMessageList();

            if (!string.IsNullOrEmpty(Token) && !string.IsNullOrEmpty(Token))
            {
                try
                {
                    var URL = string.Format(FacebookGraphAPIMessagesURL, uniqueID, Token);

                    using (var client = new WebClient())
                    {
                        var rawResponse = HttpRequest(URL, null, UserAgent);

                        if (!string.IsNullOrEmpty(rawResponse))
                        {
                            if (rawResponse.Contains("error"))
                            {
                                rawResponse = rawResponse.Replace(@"{""error"":", "");
                                rawResponse = rawResponse.Substring(0, rawResponse.Length - 1);
                                facebookMessageResponse.IsSuccess = false;
                                facebookMessageResponse.FacebookResponse.message = rawResponse;
                            }
                            else
                            {
                                var jss = new JavaScriptSerializer();
                                facebookMessageList = jss.Deserialize<FacebookMessageList>(rawResponse);
                                //FixReviewText(ref facebookReviewList);
                                facebookMessageResponse.MessageList = facebookMessageList;
                                facebookMessageResponse.IsSuccess = true;
                            }
                        }
                    }
                }
                catch (WebException wex)
                {
                    if (wex.Response != null)
                    {
                        using (var errorResponse = (HttpWebResponse)wex.Response)
                        {
                            using (var reader = new StreamReader(errorResponse.GetResponseStream()))
                            {
                                facebookMessageResponse.IsSuccess = false;
                                facebookMessageResponse.FacebookResponse.message = reader.ReadToEnd();
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    facebookMessageResponse.IsSuccess = false;
                    facebookMessageResponse.FacebookResponse.message = "General Exception";
                }
            }
            else
            {
                facebookMessageResponse.IsSuccess = false;
                facebookMessageResponse.FacebookResponse.message = "Missing or Invalid Parameters";
            }

            return facebookMessageResponse;
        }

        #region CoverPhoto

        public static FacebookCoverPhotoChangeResponse FacebookChangeCoverPhoto(string uniqueID, string photoID, string token, string yOffset = "0", string suppressCoverStory = "true")
        {
            //Sample Query
            //https://graph.facebook.com/{0}/?cover={1}&offset_y={2}&no_feed_story={3}&access_token={4}

            var facebookCoverPhotoChangeResponse = new FacebookCoverPhotoChangeResponse();

            if (!string.IsNullOrEmpty(photoID) && !string.IsNullOrEmpty(token) && !string.IsNullOrEmpty(uniqueID) && !string.IsNullOrEmpty(token))
            {
                var rawResponse = string.Empty;
                try
                {
                    Dictionary<string, string> PostData = new Dictionary<string, string>();

                    var URL = string.Format(FacebookGraphAPIUpdateCoverPhotoURL, uniqueID, token);

                    PostData.Add("cover", System.Web.HttpUtility.UrlEncode(photoID));
                    PostData.Add("offset_y", System.Web.HttpUtility.UrlEncode(yOffset));
                    PostData.Add("no_feed_story", System.Web.HttpUtility.UrlEncode(suppressCoverStory));

                    rawResponse = HttpRequest(URL, PostData, UserAgent);

                    if (!string.IsNullOrEmpty(rawResponse))
                    {
                        var jss = new JavaScriptSerializer();
                        if (rawResponse.Contains("error"))
                        {
                            rawResponse = rawResponse.Replace(@"{""error"":", "");
                            rawResponse = rawResponse.Substring(0, rawResponse.Length - 1);
                            facebookCoverPhotoChangeResponse.IsSuccess = false;
                            facebookCoverPhotoChangeResponse.FacebookResponse.message = rawResponse;
                        }
                        else
                        {
                            facebookCoverPhotoChangeResponse.IsSuccess = Convert.ToBoolean(rawResponse);
                        }
                    }
                }
                catch (WebException wex)
                {
                    if (wex.Response != null)
                    {
                        using (var errorResponse = (HttpWebResponse)wex.Response)
                        {
                            using (var reader = new StreamReader(errorResponse.GetResponseStream()))
                            {
                                facebookCoverPhotoChangeResponse.IsSuccess = false;
                                facebookCoverPhotoChangeResponse.FacebookResponse.message = reader.ReadToEnd();
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    facebookCoverPhotoChangeResponse.IsSuccess = false;
                    facebookCoverPhotoChangeResponse.FacebookResponse.message = "General Exception";
                }
            }
            else
            {
                facebookCoverPhotoChangeResponse.IsSuccess = false;
                facebookCoverPhotoChangeResponse.FacebookResponse.message = "Missing or Invalid Parameters";
            }

            return facebookCoverPhotoChangeResponse;
        }

        
        #endregion
        
        #endregion


        #region Private

        public static PostTypeEnum GetPostType(string type)
        {
            PostTypeEnum postType = new PostTypeEnum();

            if (type.ToLower() == "link")
                postType = PostTypeEnum.Link;
            if (type.ToLower() == "photo")
                postType = PostTypeEnum.Photo;
            if (type.ToLower() == "status")
                postType = PostTypeEnum.Status;
            if (type.ToLower() == "event")
                postType = PostTypeEnum.Event;
            if (type.ToLower() == "video")
                postType = PostTypeEnum.Video;
            if (type.ToLower() == "offer")
                postType = PostTypeEnum.Offer;
            if (type.ToLower() == "question")
                postType = PostTypeEnum.Question;

            return postType;
        }

        private static string HttpRequest(string requestUrl, Dictionary<string, string> PostData, string userAgent = null)
        {
            string HttpRequestResults = string.Empty;
            try
            {
                if (_WebNet == null)
                    _WebNet = new SInet(false, MaxRedirects, null, Timeout);

                if (!string.IsNullOrWhiteSpace(userAgent))
                {
                    _WebNet.UserAgent = userAgent;
                }

                if (PostData != null)
                {
                    if (PostData.Count == 0)
                        HttpRequestResults = _WebNet.DownloadString(requestUrl, null);
                    else
                        HttpRequestResults = _WebNet.DownloadString(requestUrl, PostData);
                }
                else
                    HttpRequestResults = _WebNet.DownloadString(requestUrl, null);

            }
            catch (WebException webEx)
            {
                try
                {
                    using (var reader = new StreamReader(webEx.Response.GetResponseStream()))
                    {
                        var responseText = reader.ReadToEnd();
                        HttpRequestResults = responseText;
                    }
                }
                catch (Exception ex)
                {
                    HttpRequestResults = ex.Message;

                    return HttpRequestResults;
                }
            }
            catch (Exception ex)
            {
                HttpRequestResults = ex.Message;
                
                return HttpRequestResults;

            }

            return HttpRequestResults;
        }

        private static byte[] DownloadData(string requestUrl, Dictionary<string, string> PostData, string userAgent = null)
        {
            try
            {
                if (_WebNet == null)
                    _WebNet = new SInet(false, MaxRedirects, null, Timeout);

                if (!string.IsNullOrWhiteSpace(userAgent))
                {
                    _WebNet.UserAgent = userAgent;
                }

                if (PostData == null)
                    PostData = new Dictionary<string, string>();

                if (PostData.Count == 0)
                    return _WebNet.DownloadData(requestUrl, null);
                return _WebNet.DownloadData(requestUrl, PostData);
            }
            catch (WebException webEx)
            {          
                
            }
            return null;
        }

        private static HttpWebRequest GetRequest(string requestUrl, string userAgent = null)
        {
            if (_WebNet == null)
                _WebNet = new SInet(false, MaxRedirects, null, Timeout);
            if (!string.IsNullOrWhiteSpace(userAgent))
            {
                _WebNet.UserAgent = userAgent;
            }

            return _WebNet.GetWebRequest(requestUrl);
        }

        private static string httpDownloadString(HttpWebRequest httpRequest)
        {
            string rawResponse = string.Empty;
            try
            {
                //Get Response

                using (HttpWebResponse svcResponse = (HttpWebResponse)httpRequest.GetResponse())
                {
                    using (Stream responseStream = svcResponse.GetResponseStream())
                    {
                        try
                        {
                            using (StreamReader readr = new StreamReader(responseStream, Encoding.UTF8))
                            {
                                rawResponse = readr.ReadToEnd();
                                rawResponse = rawResponse.Trim();
                            }
                        }
                        catch (Exception ex)
                        {
                            #region ERROR HANDLING
                            rawResponse = @"{""error"": {    ""message"": """ + "" + ex.Message + "" + @""", ""type"": ""Exception"",    ""code"": 000  }}";
                            #endregion
                        }
                    }

                }
            }
            catch (WebException webex)
            {
                using (var reader = new StreamReader(webex.Response.GetResponseStream()))
                {
                    var responseText = reader.ReadToEnd();
                    rawResponse = responseText;
                }
            }
            catch (Exception ex)
            {
                #region ERROR HANDLING
                rawResponse = @"{""error"": {    ""message"": """ + "" + ex.Message + "" + @""", ""type"": ""Exception"",    ""code"": 000  }}";
                #endregion
            }

            return rawResponse;
        }

        private static void FixReviewText(ref FacebookReviewList facebookReviewList)
        {
            var counter = 0;
            if (facebookReviewList != null && facebookReviewList.data != null)
            {
                foreach (var review in facebookReviewList.data)
                {
                    counter++;
                    Debug.WriteLine("Item {0}", counter);
                    if (review.has_review != null && review.has_review)
                    {
                        if (review.open_graph_story != null && review.open_graph_story.data != null && review.open_graph_story.data.review_text != null)
                        {
                            if (string.IsNullOrEmpty(review.review_text))
                            {
                                review.review_text = review.open_graph_story.data.review_text;
                            }
                        }
                    }
                }
                
            }
        }

        
        #endregion

        #endregion
    }
}
