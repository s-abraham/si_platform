﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Facebook.Entities
{
    public class FacebookAdsResponse
    {
        public FacebookAdsResponse()
        {
            Success = false;
            FailureCodes = new List<string>();
            FacebookResponse = new FacebookResponse();
        }

        public bool Success { get; set; }
        public List<string> FailureCodes { get; set; }
        public FacebookResponse FacebookResponse { get; set; }
    }


    public enum FacebookAdsAccountStatusEnum
    {
        Active = 1,
        Disabled = 2,
        Unsettled = 3,
        PendingReview = 4,
        TemporarilyUnavailable = 101,
        PendingClosure = 100
    }

    public enum FacebookAdsAccountCapabilitiesEnum
    {
        DirectSales = 1,
        Premium = 2,
        ViewTags = 4,
        ShareCustomClusters = 5,
        UseLookalikeAudiences = 6,
        CustomAudiencesOptOutLink = 7,
        CustomAudiencesFolders = 12,
        AcceptedMobileAppTagTOS = 13,
        MobileAppTag = 14
    }

    public enum FacebookAdsTimeZones
    {
        TZ_UNKNOWN = 0,
        TZ_AMERICA_LOS_ANGELES = 1,
        TZ_AMERICA_DENVER = 2,
        TZ_PACIFIC_HONOLULU = 3,
        TZ_AMERICA_ANCHORAGE = 4,
        TZ_AMERICA_PHOENIX = 5,
        TZ_AMERICA_CHICAGO = 6,
        TZ_AMERICA_NEW_YORK = 7,
        TZ_ASIA_DUBAI = 8,
        TZ_AMERICA_ARGENTINA_SAN_LUIS = 9,
        TZ_AMERICA_ARGENTINA_BUENOS_AIRES = 10,
        TZ_AMERICA_ARGENTINA_SALTA = 11,
        TZ_EUROPE_VIENNA = 12,
        TZ_AUSTRALIA_PERTH = 13,
        TZ_AUSTRALIA_BROKEN_HILL = 14,
        TZ_AUSTRALIA_SYDNEY = 15,
        TZ_EUROPE_SARAJEVO = 16,
        TZ_ASIA_DHAKA = 17,
        TZ_EUROPE_BRUSSELS = 18,
        TZ_EUROPE_SOFIA = 19,
        TZ_ASIA_BAHRAIN = 20,
        TZ_AMERICA_LA_PAZ = 21,
        TZ_AMERICA_NORONHA = 22,
        TZ_AMERICA_CAMPO_GRANDE = 23,
        TZ_AMERICA_BELEM = 24,
        TZ_AMERICA_SAO_PAULO = 25,
        TZ_AMERICA_NASSAU = 26,
        TZ_AMERICA_DAWSON = 27,
        TZ_AMERICA_VANCOUVER = 28,
        TZ_AMERICA_DAWSON_CREEK = 29,
        TZ_AMERICA_EDMONTON = 30,
        TZ_AMERICA_RAINY_RIVER = 31,
        TZ_AMERICA_REGINA = 32,
        TZ_AMERICA_ATIKOKAN = 33,
        TZ_AMERICA_IQALUIT = 34,
        TZ_AMERICA_TORONTO = 35,
        TZ_AMERICA_BLANC_SABLON = 36,
        TZ_AMERICA_HALIFAX = 37,
        TZ_AMERICA_ST_JOHNS = 38,
        TZ_EUROPE_ZURICH = 39,
        TZ_PACIFIC_EASTER = 40,
        TZ_AMERICA_SANTIAGO = 41,
        TZ_ASIA_SHANGHAI = 42,
        TZ_AMERICA_BOGOTA = 43,
        TZ_AMERICA_COSTA_RICA = 44,
        TZ_ASIA_NICOSIA = 45,
        TZ_EUROPE_PRAGUE = 46,
        TZ_EUROPE_BERLIN = 47,
        TZ_EUROPE_COPENHAGEN = 48,
        TZ_AMERICA_SANTO_DOMINGO = 49,
        TZ_PACIFIC_GALAPAGOS = 50,
        TZ_AMERICA_GUAYAQUIL = 51,
        TZ_EUROPE_TALLINN = 52,
        TZ_AFRICA_CAIRO = 53,
        TZ_ATLANTIC_CANARY = 54,
        TZ_EUROPE_MADRID = 55,
        TZ_EUROPE_HELSINKI = 56,
        TZ_EUROPE_PARIS = 57,
        TZ_EUROPE_LONDON = 58,
        TZ_AFRICA_ACCRA = 59,
        TZ_EUROPE_ATHENS = 60,
        TZ_AMERICA_GUATEMALA = 61,
        TZ_ASIA_HONG_KONG = 62,
        TZ_AMERICA_TEGUCIGALPA = 63,
        TZ_EUROPE_ZAGREB = 64,
        TZ_EUROPE_BUDAPEST = 65,
        TZ_ASIA_JAKARTA = 66,
        TZ_ASIA_MAKASSAR = 67,
        TZ_ASIA_JAYAPURA = 68,
        TZ_EUROPE_DUBLIN = 69,
        TZ_ASIA_JERUSALEM = 70,
        TZ_ASIA_KOLKATA = 71,
        TZ_ASIA_BAGHDAD = 72,
        TZ_ATLANTIC_REYKJAVIK = 73,
        TZ_EUROPE_ROME = 74,
        TZ_AMERICA_JAMAICA = 75,
        TZ_ASIA_AMMAN = 76,
        TZ_ASIA_TOKYO = 77,
        TZ_AFRICA_NAIROBI = 78,
        TZ_ASIA_SEOUL = 79,
        TZ_ASIA_KUWAIT = 80,
        TZ_ASIA_BEIRUT = 81,
        TZ_ASIA_COLOMBO = 82,
        TZ_EUROPE_VILNIUS = 83,
        TZ_EUROPE_LUXEMBOURG = 84,
        TZ_EUROPE_RIGA = 85,
        TZ_AFRICA_CASABLANCA = 86,
        TZ_EUROPE_SKOPJE = 87,
        TZ_EUROPE_MALTA = 88,
        TZ_INDIAN_MAURITIUS = 89,
        TZ_INDIAN_MALDIVES = 90,
        TZ_AMERICA_TIJUANA = 91,
        TZ_AMERICA_HERMOSILLO = 92,
        TZ_AMERICA_MAZATLAN = 93,
        TZ_AMERICA_MEXICO_CITY = 94,
        TZ_ASIA_KUALA_LUMPUR = 95,
        TZ_AFRICA_LAGOS = 96,
        TZ_AMERICA_MANAGUA = 97,
        TZ_EUROPE_AMSTERDAM = 98,
        TZ_EUROPE_OSLO = 99,
        TZ_PACIFIC_AUCKLAND = 100,
        TZ_ASIA_MUSCAT = 101,
        TZ_AMERICA_PANAMA = 102,
        TZ_AMERICA_LIMA = 103,
        TZ_ASIA_MANILA = 104,
        TZ_ASIA_KARACHI = 105,
        TZ_EUROPE_WARSAW = 106,
        TZ_AMERICA_PUERTO_RICO = 107,
        TZ_ASIA_GAZA = 108,
        TZ_ATLANTIC_AZORES = 109,
        TZ_EUROPE_LISBON = 110,
        TZ_AMERICA_ASUNCION = 111,
        TZ_ASIA_QATAR = 112,
        TZ_EUROPE_BUCHAREST = 113,
        TZ_EUROPE_BELGRADE = 114,
        TZ_EUROPE_KALININGRAD = 115,
        TZ_EUROPE_MOSCOW = 116,
        TZ_EUROPE_SAMARA = 117,
        TZ_ASIA_YEKATERINBURG = 118,
        TZ_ASIA_OMSK = 119,
        TZ_ASIA_KRASNOYARSK = 120,
        TZ_ASIA_IRKUTSK = 121,
        TZ_ASIA_YAKUTSK = 122,
        TZ_ASIA_VLADIVOSTOK = 123,
        TZ_ASIA_MAGADAN = 124,
        TZ_ASIA_KAMCHATKA = 125,
        TZ_ASIA_RIYADH = 126,
        TZ_EUROPE_STOCKHOLM = 127,
        TZ_ASIA_SINGAPORE = 128,
        TZ_EUROPE_LJUBLJANA = 129,
        TZ_EUROPE_BRATISLAVA = 130,
        TZ_AMERICA_EL_SALVADOR = 131,
        TZ_ASIA_BANGKOK = 132,
        TZ_AFRICA_TUNIS = 133,
        TZ_EUROPE_ISTANBUL = 134,
        TZ_AMERICA_PORT_OF_SPAIN = 135,
        TZ_ASIA_TAIPEI = 136,
        TZ_EUROPE_KIEV = 137,
        TZ_AMERICA_MONTEVIDEO = 138,
        TZ_AMERICA_CARACAS = 139,
        TZ_ASIA_HO_CHI_MINH = 140,
        TZ_AFRICA_JOHANNESBURG = 141,
        TZ_NUM_TIMEZONES = 142,
    }

    public class FacebookAdsAccountInfo : FacebookAdsResponse
    {
        public FacebookAdsAccountInfo()
        {
            Capabilities = new List<FacebookAdsAccountCapabilitiesEnum>();       
        }

        public FacebookAdsAccountStatusEnum AccountStatus { get; set; }
        public string AccountId { get; set; }
        public int DailySpendLimit { get; set; }
        public string BusinessName { get; set; }
        public string AdsAccountName { get; set; }
        public int Balance { get; set; }
        public List<FacebookAdsAccountCapabilitiesEnum> Capabilities { get; set; }
        public string TimezoneName { get; set; }
        public string Id { get; set; }
        public int TimezoneId { get; set; }
    }

    public class FacebookAdsCampaignsLists : FacebookAdsResponse
    {
        public FacebookAdsCampaignsLists()
        {
            Campaigns = new List<FacebookAdsCampaignDetails>();
        }

        public List<FacebookAdsCampaignDetails> Campaigns { get; set; }
    }

    public enum FacebookAdsCampaignStatusEnum
    {
        Active = 1,
        Paused = 2,
        Deleted = 3
    }

    public class FacebookAdsCampaignDetails : FacebookAdsResponse
    {
        public string AccountId { get; set; }
        public int LifetimeBudget { get; set; }
        public string CampaignName { get; set; }
        public DateTime StartTime { get; set; }
        public DateTime CreatedTime { get; set; }
        public FacebookAdsCampaignStatusEnum CampaignStatus { get; set; }
        public int BudgetRemaining { get; set; }
        public DateTime EndTime { get; set; }
        public DateTime UpdatedTime { get; set; }
        public string CampaignId { get; set; }
    }

    public class FacebookAdsCampaignStats : FacebookAdsResponse
    {
        public string Id { get; set; }
        public string CampaignId { get; set; }

        public string ToplineId { get; set; }
        public string StartTime { get; set; }
        public string EndTime { get; set; }
        public string ImpressionsCount { get; set; }
        public string ClicksCount { get; set; }
        public string Spent { get; set; }
        public string SocialImpressionsCount { get; set; }
        public string SocialClicksCount { get; set; }
        public string SocialSpent { get; set; }
        public string UniqueImpressionsCount { get; set; }

        public string SocialUniqueImpressions { get; set; }
        public string UniqueClicks { get; set; }
        public string SocialUniqueClicks { get; set; }
        public string IsCompleted { get; set; }
        public int ActionsPhotoViewCount { get; set; }
        public int ActionsPostLikeCount { get; set; }
        public int ActionsLinkClickCount { get; set; }
        public int ActionsCommentCount { get; set; }
        public int ActionsLikeCount { get; set; }
        public int ActionsPostCount { get; set; }
        public int InlineActionsTitleClicksCount { get; set; }
        public int InlineActionsLikeCount { get; set; }
        public int InlineActionsRsvpYesCount { get; set; }
        public int InlineActionsRsvpMaybeCount { get; set; }
        public int InlineActionsPostLikeCount { get; set; }
        public int InlineActionsCommentCount { get; set; }
        public int InlineActionsPhotoViewCount { get; set; }
        public int InlineActionsLinkClickCount { get; set; }
        public int InlineActionsVideoPlayCount { get; set; }
        public int InlineActionsQuestionVoteCount { get; set; }

    }


    public enum FacebookAdsCreativeTypes
    {
        BasicAd = 1,
        SocialAdForPage = 2,
        SocialAdForEvent = 3,
        SocialAdForFBApp = 4,
        HomePageStandard = 12,
        SponsoredStories = 25,
        PageAdsForPagePost = 27,
        MobileAppInstallAds = 32    
    }

    public class FacebookAdGroupInfo : FacebookAdsResponse
    {
        public string Id { get; set; }
        public string xxxxxx { get; set; }
        public string AccountId { get; set; }
        public List<KeyValuePair<string,int>> BidInfo { get; set; }
        public string ObjectiveSource { get; set; }
        public string LastUpdatedByAppId { get; set; }
        public DateTime UpdatedTime { get; set; }
        public string Name { get; set; }
        public DateTime CreatedTime { get; set; }
        public string CampaignId { get; set; }
        
        //public string xxxxxx { get; set; }
        //public string xxxxxx { get; set; }
        //public string xxxxxx { get; set; }
        //public string xxxxxx { get; set; }
        //public string xxxxxx { get; set; }
        //public string xxxxxx { get; set; }
        //public string xxxxxx { get; set; }
        //public string xxxxxx { get; set; }


    }

    public class FacebookAdsTargeting
    {
        public List<int> Genders { get; set; }
        public int AgeMax { get; set; }
        public int AgeMin { get; set; }
        public List<FacebookAdsCities> GeoLocations { get; set; }
        public string xxxxxx { get; set; }
        
    }

    public class FacebookAdsCities
    {
        public string Key { get; set; }
        public string CityName { get; set; }
        public string Region { get; set; }
        public string RegionId { get; set; }
        public string Country { get; set; }
        public int Radius { get; set; }
        public string DistanceUnit { get; set; }
    }




    public class TrackingSpec
    {
        public List<string> ActionType { get; set; }
        public List<string> Page { get; set; }
    }

    //public class Cursors
    //{
    //    public string after { get; set; }
    //    public string before { get; set; }
    //}

    //public class CursorPaging
    //{
    //    public Cursors cursors { get; set; }
    //    public string next { get; set; }
    //}

    //public class DataCampaign
    //{
    //    public int campaign_status { get; set; }
    //    public string id { get; set; }
    //}

    //public class Campaign
    //{
    //    public List<DataCampaign> data { get; set; }
    //    public CursorPaging paging { get; set; }
    //    public int count { get; set; }
    //    public int limit { get; set; }
    //    public int offset { get; set; }
    //}

}
