﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using HtmlAgilityPack;

namespace SI
{
    public static class StringUtils
    {
        private static List<HtmlNode> GetNode(HtmlNode parent, string tagName, KeyValuePair<string, string> property)
        {
            var nodes = new List<HtmlNode>();
            try
            {
                foreach (var child in parent.ChildNodes)
                {
                    if (child.Name == tagName)
                    {
                        foreach (HtmlAttribute attribute in child.Attributes)
                        {
                            if (attribute.Name == property.Key
                                && attribute.Value == property.Value)
                            {
                                nodes.Add(child);
                            }
                        }
                    }
                    if (child.HasChildNodes)
                    {
                        nodes.AddRange(GetNode(child, tagName, property));
                    }
                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine("[StringUtils.ExtractSubstring Error: ", ex.Message);
            }

            return nodes;
        }

        public static string ExtractSubstring(string searchString, string startSequence, string endSequence)
        {
            var result = string.Empty;
            var start = 0;
            var end = 0;

            try
            {
                if (!string.IsNullOrEmpty(searchString))
                {
                    start = searchString.IndexOf(startSequence) + startSequence.Length;

                    if (searchString.IndexOf(endSequence, start) < 0)
                    {
                        end = searchString.Length;
                    }
                    else
                    {
                        end = endSequence == null ? searchString.Length : searchString.IndexOf(endSequence, start);
                    }


                    if (start >= 0 && end > 0)
                    {
                        var length = end - start;
                        result = searchString.Substring(start, length);
                    }
                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine("[StringUtils.ExtractSubstring Error: ", ex.Message);
            }

            return result;


        }

        public static string ExtractSubstring(string searchString, string startSequence, string endSequence, ref int lastPosition)
        {
            var result = string.Empty;

            try
            {
                if (!string.IsNullOrEmpty(searchString))
                {
                    var start = searchString.IndexOf(startSequence, lastPosition) + startSequence.Length;
                    var end = searchString.IndexOf(endSequence, start);

                    if (start >= 0 && end > 0)
                    {
                        var length = end - start;
                        result = searchString.Substring(start, length);
                        lastPosition = end;
                    }
                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine("[StringUtils.ExtractSubstring Error: ", ex.Message);
            }

            return result;
        }

        public static string ExtractSubstringRegex(string searchString, string startSequence, string endSequence)
        {
            var result = string.Empty;

            var pattern = @"(?s)(?<=" + startSequence + ").+?(?=" + endSequence + ")";

            var matches = Regex.Matches(searchString, pattern);

            if (matches.Count > 0)
            {
                result = matches[0].ToString();
            }

            return result;
        }

        public static List<int> AllIndexesOf(this string searchString, string value)
        {
            if (String.IsNullOrEmpty(value))
            {
                throw new ArgumentException("the string to find may not be empty", "value");
            }
                
            var indexes = new List<int>();
            for (int index = 0; ; index += value.Length)
            {
                index = searchString.IndexOf(value, index);
                if (index == -1)
                {
                    return indexes;
                }
                    
                indexes.Add(index);
            }
        }

        public static string HtmlStrip(this string input)
        {
            var result = string.Empty;
            if (!string.IsNullOrWhiteSpace(input))
            {
                input = input.Replace("\\u003C", "<");
                input = input.Replace("\u003C", "<");

                input = Regex.Replace(input, "<style>(.|\n)*?</style>", string.Empty);
                input = Regex.Replace(input, @"<xml>(.|\n)*?</xml>", string.Empty); // remove all <xml></xml> tags and anything inbetween.  
                input = Regex.Replace(input, @"\t|\n|\r", "");
                result = Regex.Replace(input, @"<(.|\n)*?>", string.Empty).Trim(); // remove any tags but not there content "<p>bob<span> johnson</span></p>" becomes "bob johnson"    
            }
            return result;

        }

        public static string HTMLEncode(this string input)
        {
            var result = string.Empty;

            result = WebUtility.HtmlEncode(input);

            return result;
        }

        public static string HTMLDecode(this string input)
        {
            var result = string.Empty;

            result = WebUtility.HtmlDecode(input);

            return result;
        }

        public static string URLEncode(this string input, string spaceReplacement = "%20")
        {
            var result = string.Empty;

            result = input.Replace(" ", spaceReplacement);

            return result;
        }

        public static string PrepStringForSQL(string input)
        {
            var result = "'" + input.Replace("'", "''").Replace("’", "''") + "'";
            return result;
        }

        public static string PrepDateForSQL(DateTime input)
        {
            var result = "'" + input.ToString() + "'";
            return result;
        }


        public static string ExtractNumeric(this string input)
        {
            var result = string.Empty;

            result = Regex.Replace(input, "[^0-9.]", "");

            return result;
        }

        public static T ParseEnum<T>(string value)
        {
            return (T)Enum.Parse(typeof(T), value, true);
        }
    }
}
