﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace SI
{
    public static class HashGenerator
    {
        private static HashAlgorithm _sha1 = SHA1Managed.Create("sha1");
        public static string GetURLFriendlyHash(string sourceData, int size)
        {
            return GetURLFriendlyHash(Encoding.UTF7.GetBytes(sourceData.Trim().ToLower()), size);
        }
        public static string GetURLFriendlyHash(byte[] sourceData, int size)
        {
            lock (_sha1)
            {
                byte[] hashValue = _sha1.ComputeHash(sourceData);
                string theHash = Convert.ToBase64String(hashValue).Trim();
                theHash = theHash.ToLower().Replace("/", "x").Replace("+", "y").Replace("=", "z");
                if (theHash.Length > size)
                    theHash = theHash.Substring(0, size);
                return theHash;
            }
        }
    }
}
