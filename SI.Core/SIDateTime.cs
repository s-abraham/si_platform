﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SI
{
    public class SIDateTime : IComparable<SIDateTime>
    {
        public long? UserID { get; set; }

        private TimeZoneInfo _timeZone = null;
        public TimeZoneInfo TimeZone
        {
            get
            {
                return _timeZone;
            }
            set
            {
                _timeZone = value;
                if (_utcDate.HasValue && !_localDate.HasValue)
                {
                    _localDate = TimeZoneInfo.ConvertTimeFromUtc(_utcDate.Value, _timeZone);
                }
                else if (!_utcDate.HasValue && _localDate.HasValue)
                {
                    _utcDate = TimeZoneInfo.ConvertTimeToUtc(_localDate.Value, _timeZone);
                }
                else if (_utcDate.HasValue && _localDate.HasValue)
                {
                    // when timezone is explicitly set and we already have converted dates, trust the utc date and re-correct
                    _localDate = TimeZoneInfo.ConvertTimeFromUtc(_utcDate.Value, _timeZone);
                }
            }
        }

        private DateTime? _utcDate = null;
        public void SetUTCDate(DateTime value)
        {
            _utcDate = value;
            if (_timeZone != null && _utcDate.HasValue)
            {
                _localDate = TimeZoneInfo.ConvertTimeFromUtc(_utcDate.Value, _timeZone);
            }
            else if (!_utcDate.HasValue)
            {
                _utcDate = null;
            }
        }

        public DateTime GetUTCDate(TimeZoneInfo timeZone)
        {
            if (TimeZone == null) TimeZone = timeZone;

            if (_utcDate.HasValue)
                return _utcDate.Value;

            throw new InvalidOperationException("UTC Date is not known.");
        }

        private DateTime? _localDate = null;
        public DateTime? LocalDate
        {
            get
            {
                return _localDate;
            }
            set
            {
                _localDate = value;

                if (_timeZone != null && _localDate.HasValue)
                {
                    _utcDate = TimeZoneInfo.ConvertTimeToUtc(_localDate.Value, _timeZone);
                }
                else if (!_localDate.HasValue)
                {
                    _utcDate = null;
                }
            }
        }

        public SIDateTime(DateTime utcDate, TimeZoneInfo timeZone)
        {
            _utcDate = new DateTime(utcDate.Ticks, DateTimeKind.Utc);
            UserID = null;

            _timeZone = timeZone;
            _localDate = TimeZoneInfo.ConvertTimeFromUtc(_utcDate.Value, timeZone);

        }

        public SIDateTime(DateTime localDate, long userID)
        {
            _timeZone = null;
            _utcDate = null;

            LocalDate = DateTime.SpecifyKind(localDate, DateTimeKind.Unspecified);
            UserID = userID;
        }

        public SIDateTime(SIDateTime createFrom)
        {
            if (createFrom._utcDate.HasValue && createFrom._timeZone != null)
            {
                _utcDate = new DateTime(createFrom._utcDate.Value.Ticks, DateTimeKind.Utc);
                UserID = null;

                _timeZone = createFrom._timeZone;
                _localDate = TimeZoneInfo.ConvertTimeFromUtc(_utcDate.Value, _timeZone);

            }
            else
            {
                _timeZone = null;
                _utcDate = null;

                LocalDate = DateTime.SpecifyKind(createFrom._localDate.Value, DateTimeKind.Unspecified);
                UserID = createFrom.UserID;
            }

        }

        public static TimeSpan operator -(SIDateTime sdt1, SIDateTime sdt2)
        {
            if (sdt1._utcDate.HasValue && sdt2._utcDate.HasValue)
            {
                return sdt1._utcDate.Value - sdt2._utcDate.Value;
            }
            if (sdt1.LocalDate.HasValue && sdt2.LocalDate.HasValue && sdt1.TimeZone == sdt2.TimeZone)
            {
                return sdt1.LocalDate.Value - sdt2.LocalDate.Value;
            }

            throw new InvalidOperationException("Subtract operator is only valid on two SIDateTime objects if both have UTDDate or both have LocalDate and the Time Zones match");
        }

        public static SIDateTime CreateSIDateTime(DateTime? utcDate, TimeZoneInfo timeZone)
        {
            if (!utcDate.HasValue) return null;
            return new SIDateTime(utcDate.Value, timeZone);
        }

        public DateTime GetUTCForGivenTimezone(TimeZoneInfo timeZone)
        {
            return TimeZoneInfo.ConvertTimeToUtc(LocalDate.Value, timeZone);
        }

        public void StripTime()
        {
            if (_utcDate.HasValue)
            {
                _utcDate = _utcDate.Value.Date;
            }
            if (_localDate.HasValue)
            {
                _localDate = _localDate.Value.Date;
            }
        }

        public static SIDateTime Now
        {
            get
            {
                return new SIDateTime(DateTime.UtcNow, TimeZoneInfo.Utc);
            }
        }
        public static SIDateTime MinValue
        {
            get
            {
                TimeZoneInfo inf = TimeZoneInfo.FindSystemTimeZoneById("UTC");
                return new SIDateTime(DateTime.MinValue.ToUniversalTime(), inf);
            }
        }
        public static SIDateTime MaxValue
        {
            get
            {
                TimeZoneInfo inf = TimeZoneInfo.FindSystemTimeZoneById("UTC");
                return new SIDateTime(DateTime.MaxValue.ToUniversalTime(), inf);
            }
        }

        public SIDateTime Copy()
        {
            return new SIDateTime(this);
        }

        public SIDateTime Add(TimeSpan timeToAdd)
        {
            SIDateTime copy = this.Copy();
            if (copy._utcDate.HasValue)
            {
                copy.SetUTCDate(copy._utcDate.Value.Add(timeToAdd));
            }
            else
            {
                copy.LocalDate = copy.LocalDate.Value.Add(timeToAdd);
            }
            return copy;
        }
        public SIDateTime Subtract(TimeSpan timeToSubtract)
        {
            SIDateTime copy = this.Copy();
            if (copy._utcDate.HasValue)
            {
                copy.SetUTCDate(copy._utcDate.Value.Subtract(timeToSubtract));
            }
            else
            {
                copy.LocalDate = copy.LocalDate.Value.Subtract(timeToSubtract);
            }
            return copy;
        }

        public override string ToString()
        {
            if (_utcDate.HasValue)
                return _utcDate.ToString();
            if (_localDate.HasValue)
                return _localDate.ToString();
            return "";
        }


        public int CompareTo(SIDateTime other)
        {
            if (_utcDate.HasValue && other._utcDate.HasValue)
            {
                return this._utcDate.Value.CompareTo(other._utcDate.Value);
            }
            if (_localDate.HasValue && other._localDate.HasValue && _timeZone != null && other._timeZone != null && _timeZone == other._timeZone)
            {
                return this._localDate.Value.CompareTo(other._localDate.Value);
            }
            return -1;
        }
    }
}
