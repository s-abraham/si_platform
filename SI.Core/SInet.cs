﻿using System.Diagnostics;
using System.Dynamic;
using HtmlAgilityPack;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace SI
{

    public class SInet : IDisposable
    {

        private Random _r = new Random(DateTime.Now.Millisecond);
        private CookieContainer _cookies = new CookieContainer();
        private string _lastReferrer = null;
        private string _userAgent = null;
        private bool _useProxy = false;
        private int _maxRedirects = 1;

        private int _timeout = 15000;

        private WebProxy _proxy = null;

        #region user agent list

        private static List<string> _userAgents = new List<string>() {
		    "Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 5.1; .NET CLR 2.0.50727)",
		    "Mozilla/4.0 (compatible; MSIE 8.0; AOL 9.5; AOLBuild 4337.43; Windows NT 6.0; Trident/4.0; SLCC1; .NET CLR 2.0.50727; Media Center PC 5.0; .NET CLR 3.5.21022; .NET CLR 3.5.30729; .NET CLR 3.0.30618)",
		    "Mozilla/4.0 (compatible; MSIE 7.0; AOL 9.5; AOLBuild 4337.34; Windows NT 6.0; WOW64; SLCC1; .NET CLR 2.0.50727; Media Center PC 5.0; .NET CLR 3.5.30729; .NET CLR 3.0.30618)",
		    "Mozilla/5.0 (X11; U; Linux i686; pl-PL; rv:1.9.0.2) Gecko/20121223 Ubuntu/9.25 (jaunty) Firefox/3.8",
		    "Mozilla/5.0 (Windows; U; Windows NT 5.1; ja; rv:1.9.2a1pre) Gecko/20090402 Firefox/3.6a1pre (.NET CLR 3.5.30729)",
		    "Mozilla/5.0 (Windows; U; Windows NT 6.0; en-US; rv:1.9.1b4) Gecko/20090423 Firefox/3.5b4 GTB5 (.NET CLR 3.5.30729)",
		    "Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 5.1; Avant Browser; .NET CLR 2.0.50727; MAXTHON 2.0)",
		    "Mozilla/4.0 (compatible; MSIE 8.0; Windows NT 6.1; WOW64; Trident/4.0; SLCC2; Media Center PC 6.0; InfoPath.2; MS-RTC LM 8)",
		    "Mozilla/4.0 (compatible; MSIE 8.0; Windows NT 6.0; WOW64; Trident/4.0; SLCC1; .NET CLR 2.0.50727; Media Center PC 5.0; InfoPath.2; .NET CLR 3.5.21022; .NET CLR 3.5.30729; .NET CLR 3.0.30618)",
		    "Mozilla/4.0 (compatible; MSIE 7.0b; Windows NT 6.0)",
		    "Mozilla/4.0 (compatible; MSIE 7.0b; Windows NT 5.1; Media Center PC 3.0; .NET CLR 1.0.3705; .NET CLR 1.1.4322; .NET CLR 2.0.50727; InfoPath.1)",
		    "Opera/9.70 (Linux i686 ; U; zh-cn) Presto/2.2.0",
		    "Opera 9.7 (Windows NT 5.2; U; en)",
		    "Mozilla/5.0 (Windows; U; Windows NT 6.0; en-US; rv:1.8.1.8pre) Gecko/20070928 Firefox/2.0.0.7 Navigator/9.0RC1",
		    "Mozilla/5.0 (X11; U; Linux i686; en-US; rv:1.8.1.7pre) Gecko/20070815 Firefox/2.0.0.6 Navigator/9.0b3",
		    "Mozilla/5.0 (Windows; U; Windows NT 5.1; en) AppleWebKit/526.9 (KHTML, like Gecko) Version/4.0dp1 Safari/526.8",
		    "Mozilla/5.0 (Windows; U; Windows NT 6.0; ru-RU) AppleWebKit/528.16 (KHTML, like Gecko) Version/4.0 Safari/528.16",
		    "Opera/9.64 (X11; Linux x86_64; U; en) Presto/2.1.1"
        };

        #endregion

        public SInet(bool useProxy, int maxRedirects, string initialReferrer, int timeoutMilliseconds)
        {
            _userAgent = _userAgents[_r.Next(0, _userAgents.Count() - 1)];
            _maxRedirects = maxRedirects;
            _lastReferrer = initialReferrer;
            _useProxy = useProxy;
            _timeout = timeoutMilliseconds;

            if (useProxy)
            {
                
                switch (Settings.GetSetting("proxy.useproxyserver"))
                {
                    case "primary" :
                        _proxy = new WebProxy(Settings.GetSetting("proxy.primary"), true);
                        Debug.WriteLine("Using Primary Proxy" + Settings.GetSetting("proxy.primary"));
                        break;

                    case "secondary":
                        _proxy = new WebProxy(Settings.GetSetting("proxy.secondary"), true);
                        Debug.WriteLine("Using Secondard Proxy" + Settings.GetSetting("proxy.secondary"));
                        break;
                }
            }
        }

        public CookieContainer Cookie
        {
            get
            {
                return _cookies;
            }
            set
            {
                _cookies = value;
            }
        }

        public string UserAgent
        {
            get
            {
                return _userAgent;
            }
            set
            {
                _userAgent = value;
            }
        }

        //public dynamic DownloadDocument(string url)
        //{
        //    var postVars = new Dictionary<String, String>();
        //    var htmlRawResult = DownloadString(url, postVars);
        //    dynamic emptyResult = "";

        //    if (!string.IsNullOrEmpty(htmlRawResult))
        //    {
        //        dynamic dynamicResult = Newtonsoft.Json.JsonConvert.DeserializeObject<dynamic>(htmlRawResult);

        //        //if (editorialDetails != null && editorialDetails.Count > 0)
        //        //{
        //        //    foreach (var editorial in editorialDetails)
        //        //    {
        //        //        try
        //        //        {
        //        //            editorialItems.Add(PopulateEditorials(editorial));
        //        //        }
        //        //        catch (Exception ex)
        //        //        {

        //        //            editorialItemResult.ErrorMessage = (ex.InnerException != null) ? ex.InnerException.Message : ex.Message;
        //        //            editorialItemResult.Status = false;
        //        //        }
        //        //    }

        //        //    editorialItemResult.Value = editorialItems;
        //        //    editorialItemResult.Status = true;
        //        //}
        //        //else
        //        //{
        //        //    editorialItemResult.Status = true;
        //        //    Debug.WriteLine("No Editorials Found");
        //        //}
        //        return dynamicResult;
        //    }
        //    else
        //    {
        //        return emptyResult;
        //    }
        //}

        public HtmlDocument DownloadDocument(string url, out string rawHtml)
        {
            var html = DownloadString(url);

            var doc = new HtmlDocument();
            doc.OptionFixNestedTags = true;
            doc.LoadHtml(html);
            rawHtml = html;

            return doc;
        }

        public HtmlDocument DownloadDocument(string url, Dictionary<string, string> postVars)
        {
            var doc = new HtmlDocument();

            //if (VerifyValidURL(url))
            //{
            var html = DownloadString(url, postVars);

            doc.OptionFixNestedTags = true;
            doc.LoadHtml(html);
            return doc;
            //}

            //return null;
        }

        public dynamic GetRequestDynamic(string requestUrl, out string htmlResult)
        {
            htmlResult = string.Empty;
            dynamic dynamicResult;

            htmlResult = DownloadString(requestUrl);

            if (htmlResult != null)
            {
                dynamicResult = Newtonsoft.Json.JsonConvert.DeserializeObject<dynamic>(htmlResult);
            }
            else
            {
                htmlResult = "";
                dynamicResult = Newtonsoft.Json.JsonConvert.DeserializeObject<dynamic>(htmlResult);
            }

            return dynamicResult;
        }

        public string DownloadString(string url, Dictionary<String, String> postVars)
        {

            HttpWebRequest wr = (HttpWebRequest)WebRequest.Create(url);

            if (_lastReferrer != null)
            {
                wr.Referer = _lastReferrer;
            }
            _lastReferrer = url;

            //wr.CookieContainer = _cookies;

            wr.UserAgent = _userAgent;
            wr.AllowAutoRedirect = (_maxRedirects > 0);
            if (_maxRedirects > 0)
                wr.MaximumAutomaticRedirections = _maxRedirects;
            wr.Timeout = _timeout;

            if (_proxy != null)
            {
                wr.Proxy = _proxy;
                bool usePrimary = (Settings.GetSetting<string>("proxy.useproxyserver").Trim().ToLower() == "primary");
                string authHeader = Settings.GetSetting<string>(usePrimary ? "proxy.primary.header" : "proxy.secondary.header").Trim();
                //var authInfo = "SOCIALDEALER" + ":" + "devteam@3100";
                if (!string.IsNullOrWhiteSpace(authHeader))
                {
                    authHeader = Convert.ToBase64String(Encoding.Default.GetBytes(authHeader));
                    wr.Headers.Add(HttpRequestHeader.ProxyAuthorization, "Basic " + authHeader);
                }
            }

            if (postVars != null)
            {
                String postString = "";
                foreach (String key in postVars.Keys)
                {
                    if (postString != "")
                        postString = postString + "&";
                    String theValue = postVars[key].Trim();
                    postString = postString + key.Trim() + "=" + theValue;
                }

                wr.Method = "POST";
                wr.ContentType = "application/x-www-form-urlencoded";
                
                //byte[] bytedata = Encoding.UTF8.GetBytes(postString);

                UTF8Encoding encoding = new UTF8Encoding();
                byte[] bytedata = encoding.GetBytes(postString);

                wr.ContentLength = bytedata.Length;

                using (Stream requestStream = wr.GetRequestStream())
                {
                    requestStream.Write(bytedata, 0, bytedata.Length);
                    requestStream.Close();
                }

                
            }


            using (HttpWebResponse resp = (HttpWebResponse)wr.GetResponse())
            {
                try
                {                   
                    foreach (Cookie cook in resp.Cookies)
                    {
                        _cookies.Add(cook);
                    }
                }
                catch (Exception) { } //we do not care about exeptions on setting the cookies

                string html = "";

                using (Stream rStream = resp.GetResponseStream())
                {                    
                    using (StreamReader sr = new StreamReader(rStream))
                    {
                        
                        html = sr.ReadToEnd();
                        return html;
                    }
                }
            }

        }

        //        public bool VerifyValidURL(string url)
        //        {
        //            var result = false;

        //            try
        //            {
        //                HttpWebRequest wr = WebRequest.Create(url) as HttpWebRequest;
        //                if (_lastReferrer != null)
        //                {
        //                    wr.Referer = _lastReferrer;
        //                }
        //                _lastReferrer = url;

        //                //wr.CookieContainer = _cookies;

        //                wr.UserAgent = _userAgent;
        //                wr.AllowAutoRedirect = (_maxRedirects > 0);
        //                if (_maxRedirects > 0)
        //                    wr.MaximumAutomaticRedirections = _maxRedirects;
        //                wr.Timeout = _timeout;

        //                if (_proxy != null)
        //                {
        //                    wr.Proxy = _proxy;
        //                }
        ////                wr.Method = "HEAD";
        //                HttpWebResponse response = wr.GetResponse() as HttpWebResponse;

        //                //if (response.StatusCode == HttpStatusCode.OK || response.StatusCode == HttpStatusCode.Redirect)
        //                //{
        //                //    result = true;
        //                //}

        //                int statusCode = (int)response.StatusCode;

        //                if (statusCode >= 100 && statusCode < 400)
        //                {
        //                    return true;
        //                }
        //            }
        //            catch
        //            {
        //                result = false;
        //                Debug.WriteLine("URL: " + url + "Failed");
        //            }

        //            return result;
        //        }

        public string DownloadString(string url, string httpMethod, string contentType, byte[] requestStreamData)
        {

            HttpWebRequest wr = (HttpWebRequest)WebRequest.Create(url);

            if (_lastReferrer != null)
            {
                wr.Referer = _lastReferrer;
            }
            _lastReferrer = url;

            //wr.CookieContainer = _cookies;

            wr.UserAgent = _userAgent;
            wr.AllowAutoRedirect = (_maxRedirects > 0);
            if (_maxRedirects > 0)
                wr.MaximumAutomaticRedirections = _maxRedirects;
            wr.Timeout = _timeout;

            if (_proxy != null)
            {
                bool usePrimary = (Settings.GetSetting<string>("proxy.useproxyserver").Trim().ToLower() == "primary");
                string authHeader = Settings.GetSetting<string>(usePrimary ? "proxy.primary.header" : "proxy.secondary.header").Trim();                
                //var authInfo = "SOCIALDEALER" + ":" + "devteam@3100";
                if (!string.IsNullOrWhiteSpace(authHeader))
                {
                    authHeader = Convert.ToBase64String(Encoding.Default.GetBytes(authHeader));
                    wr.Headers.Add(HttpRequestHeader.ProxyAuthorization, "Basic " + authHeader);
                }
            }

            wr.Method = "POST";
            if (!string.IsNullOrWhiteSpace(httpMethod))
            {
                wr.Method = httpMethod.ToUpper().Trim();
            }

            wr.ContentType = "application/x-www-form-urlencoded";
            if (!string.IsNullOrWhiteSpace(contentType))
            {
                wr.ContentType = contentType.Trim();
            }

            byte[] bytedata = requestStreamData;
            wr.ContentLength = bytedata.Length;

            wr.ServicePoint.Expect100Continue = false;
            wr.KeepAlive = false;
            wr.AllowWriteStreamBuffering = false;

            using (Stream requestStream = wr.GetRequestStream())
            {
                requestStream.Write(bytedata, 0, bytedata.Length);
                requestStream.Close();
            }

            using (HttpWebResponse resp = (HttpWebResponse)wr.GetResponse())
            {
                try
                {
                    foreach (Cookie cook in resp.Cookies)
                    {
                        _cookies.Add(cook);
                    }
                }
                catch (Exception) { } //we do not care about exeptions on setting the cookies

                string html = "";

                using (Stream rStream = resp.GetResponseStream())
                {
                    using (StreamReader sr = new StreamReader(rStream))
                    {
                        html = sr.ReadToEnd();
                        return html;
                    }
                }
            }

        }

        public HttpWebRequest GetWebRequest(string url)
        {
            HttpWebRequest wr = (HttpWebRequest)WebRequest.Create(url);

            if (_lastReferrer != null)
            {
                wr.Referer = _lastReferrer;
            }
            _lastReferrer = url;

            //wr.CookieContainer = _cookies;

            wr.UserAgent = _userAgent;
            wr.AllowAutoRedirect = (_maxRedirects > 0);
            if (_maxRedirects > 0)
                wr.MaximumAutomaticRedirections = _maxRedirects;
            wr.Timeout = _timeout;

            if (_proxy != null)
            {
                //var authInfo = "SOCIALDEALER" + ":" + "devteam@3100";
                //authInfo = Convert.ToBase64String(Encoding.Default.GetBytes(authInfo));
                //wr.Headers.Add(HttpRequestHeader.ProxyAuthorization, "Basic " + authInfo);

                bool usePrimary = (Settings.GetSetting<string>("proxy.useproxyserver").Trim().ToLower() == "primary");
                string authHeader = Settings.GetSetting<string>(usePrimary ? "proxy.primary.header" : "proxy.secondary.header").Trim();                
                if (!string.IsNullOrWhiteSpace(authHeader))
                {
                    authHeader = Convert.ToBase64String(Encoding.Default.GetBytes(authHeader));
                    wr.Headers.Add(HttpRequestHeader.ProxyAuthorization, "Basic " + authHeader);
                }
            }

            return wr;
        }

        public string DownloadString(string url)
        {
            var result = string.Empty;
            if (!string.IsNullOrEmpty(url))
            {
                
                //result = new WebClient().DownloadString(url);

                using (var wc = new WebClient())
                {
                    if (_useProxy)
                    {
                        wc.Proxy = _proxy;    
                    }
                        
                    result = wc.DownloadString(url);
                }
            }

            return result;
        }

        public string DownloadStringCustom(string url, string useragent = null)
        {
            var result = string.Empty;
            try
            {
                var client = new WebClient();
                if (!string.IsNullOrEmpty(url))
                {
                    if (!string.IsNullOrWhiteSpace(useragent))
                    {
                        client.Headers.Add("user-agent", useragent);
                    }
                    result = client.DownloadString(url);
                }
            }
            catch (Exception ex)
            {

            }

            return result;
        }

        public byte[] DownloadData(string url, Dictionary<String, String> postVars)
        {

            HttpWebRequest wr = (HttpWebRequest)WebRequest.Create(url);

            if (_lastReferrer != null)
            {
                wr.Referer = _lastReferrer;
            }
            _lastReferrer = url;

            //wr.CookieContainer = _cookies;

            wr.UserAgent = _userAgent;
            wr.AllowAutoRedirect = (_maxRedirects > 0);
            if (_maxRedirects > 0)
                wr.MaximumAutomaticRedirections = _maxRedirects;
            wr.Timeout = _timeout;

            if (_proxy != null)
            {
                //var authInfo = "SOCIALDEALER" + ":" + "devteam@3100";
                //authInfo = Convert.ToBase64String(Encoding.Default.GetBytes(authInfo));
                //wr.Headers.Add(HttpRequestHeader.ProxyAuthorization, "Basic " + authInfo);
                bool usePrimary = (Settings.GetSetting<string>("proxy.useproxyserver").Trim().ToLower() == "primary");
                string authHeader = Settings.GetSetting<string>(usePrimary ? "proxy.primary.header" : "proxy.secondary.header").Trim();
                if (!string.IsNullOrWhiteSpace(authHeader))
                {
                    authHeader = Convert.ToBase64String(Encoding.Default.GetBytes(authHeader));
                    wr.Headers.Add(HttpRequestHeader.ProxyAuthorization, "Basic " + authHeader);
                }
            }

            if (postVars != null)
            {
                String postString = "";
                foreach (String key in postVars.Keys)
                {
                    if (postString != "")
                        postString = postString + "&";
                    String theValue = postVars[key].Trim();
                    postString = postString + key.Trim() + "=" + theValue;
                }

                wr.Method = "POST";
                wr.ContentType = "application/x-www-form-urlencoded";
                byte[] bytedata = Encoding.UTF8.GetBytes(postString);
                wr.ContentLength = bytedata.Length;

                using (Stream requestStream = wr.GetRequestStream())
                {
                    requestStream.Write(bytedata, 0, bytedata.Length);
                    requestStream.Close();
                }
            }

            using (HttpWebResponse resp = (HttpWebResponse)wr.GetResponse())
            {
                try
                {
                    foreach (Cookie cook in resp.Cookies)
                    {
                        _cookies.Add(cook);
                    }
                }
                catch (Exception) { } //we do not care about exeptions on setting the cookies


                using (Stream rStream = resp.GetResponseStream())
                {
                    return readStreamToEnd(rStream);
                }
            }

        }

        private static byte[] readStreamToEnd(System.IO.Stream stream)
        {
            long originalPosition = 0;

            if (stream.CanSeek)
            {
                originalPosition = stream.Position;
                stream.Position = 0;
            }

            try
            {
                byte[] readBuffer = new byte[4096];

                int totalBytesRead = 0;
                int bytesRead;

                while ((bytesRead = stream.Read(readBuffer, totalBytesRead, readBuffer.Length - totalBytesRead)) > 0)
                {
                    totalBytesRead += bytesRead;

                    if (totalBytesRead == readBuffer.Length)
                    {
                        int nextByte = stream.ReadByte();
                        if (nextByte != -1)
                        {
                            byte[] temp = new byte[readBuffer.Length * 2];
                            Buffer.BlockCopy(readBuffer, 0, temp, 0, readBuffer.Length);
                            Buffer.SetByte(temp, totalBytesRead, (byte)nextByte);
                            readBuffer = temp;
                            totalBytesRead++;
                        }
                    }
                }

                byte[] buffer = readBuffer;
                if (readBuffer.Length != totalBytesRead)
                {
                    buffer = new byte[totalBytesRead];
                    Buffer.BlockCopy(readBuffer, 0, buffer, 0, totalBytesRead);
                }
                return buffer;
            }
            finally
            {
                if (stream.CanSeek)
                {
                    stream.Position = originalPosition;
                }
            }
        }

        public static string GetFinalRedirectedUrl(string url)
        {
            var result = string.Empty;
            /*           

            var Uris = new Uri(url);

            HttpWebRequest req = (HttpWebRequest)WebRequest.Create(Uris);
            //req3.Proxy = proxy;
            req.Method = "HEAD";
            req.AllowAutoRedirect = false;

            HttpWebResponse myResp = (HttpWebResponse)req.GetResponse();
            if (myResp.StatusCode == HttpStatusCode.Redirect)
            {
                var temp = myResp.GetResponseHeader("Location");
                //Recursive call
                result = GetFinalRedirectedUrl(temp);
            }
            else
            {
                result = url;
            }
            */

            //Build the request object.
            var request = (HttpWebRequest)WebRequest.Create(new Uri(url));
            //Get the response object from request.
            var response = request.GetResponse();
            //Get the final URL, when the host redirects the request to a new destination, this would be the last URL from the last redirect operation.
            result = response.ResponseUri.ToString();

            return result;
        }


        public void Dispose()
        {

        }
    }




}
