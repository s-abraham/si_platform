﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace SI
{
    public static class Util
    {
        /// <summary>
        /// Retrieves the contents of an embedded resource file as a string.
        /// </summary>
        /// <param name="filename">The name of the file to find and retrieve</param>
        /// <returns>String contents of the found embedded resource file.</returns>
        public static string GetResourceTextFile(string filename)
        {
            return GetResourceTextFile(filename, null);
        }

        /// <summary>
        /// Retrieves the contents of an embedded resource file as a string.
        /// </summary>
        /// <param name="filename">The name of the file to find and retrieve</param>
        /// <returns>String contents of the found embedded resource file.</returns>
        public static string GetResourceTextFile(string filename, Assembly optionalAssemblyToSearch)
        {
            //string result = string.Empty;

            //using (Stream stream = Assembly.GetExecutingAssembly().GetManifestResourceStream(GetResourceNamesContaining(filename).First()))
            //{
            //    using (StreamReader sr = new StreamReader(stream))
            //    {
            //        result = sr.ReadToEnd();
            //    }
            //}
            //return result;

            List<Assembly> asses = new List<Assembly>() { Assembly.GetExecutingAssembly(), Assembly.GetCallingAssembly(), Assembly.GetEntryAssembly() };
            if (optionalAssemblyToSearch != null && !asses.Contains(optionalAssemblyToSearch))
            {
                asses.Add(optionalAssemblyToSearch);
            }
            return getFirstResourceTextFileFound(asses, filename);
        }

        private static string GetResourceName(string fileName)
        {

            foreach (string name in Assembly.GetExecutingAssembly().GetManifestResourceNames())
            {
                if (name.Trim().ToLower().Contains(fileName.Trim().ToLower()))
                    return name;
            }
            foreach (string name in Assembly.GetCallingAssembly().GetManifestResourceNames())
            {
                if (name.Trim().ToLower().Contains(fileName.Trim().ToLower()))
                    return name;
            }
            if (Assembly.GetEntryAssembly() != null)
            {
                foreach (string name in Assembly.GetEntryAssembly().GetManifestResourceNames())
                {
                    if (name.Trim().ToLower().Contains(fileName.Trim().ToLower()))
                        return name;
                }
            }

            return null;
        }

        private static string getFirstResourceTextFileFound(List<Assembly> assembliesToSearch, string fileName)
        {
            List<string> foundItems = new List<string>();
            foreach (Assembly ass in assembliesToSearch)
            {
                if (ass != null)
                {
                    foreach (string name in ass.GetManifestResourceNames())
                    {
                        if (name.Trim().ToLower().Contains(fileName.Trim().ToLower()))
                        {
                            string result = string.Empty;
                            using (Stream stream = ass.GetManifestResourceStream(name))
                            {
                                using (StreamReader sr = new StreamReader(stream))
                                {
                                    result = sr.ReadToEnd();
                                }
                            }
                            return result;
                        }
                    }
                }
            }

            return null;

        }

        public static List<string> GetResourceNamesContaining(List<Assembly> assembliesToSearch, string containsText)
        {
            List<string> foundItems = new List<string>();
            foreach (Assembly ass in assembliesToSearch)
            {
                if (ass != null)
                {
                    foreach (string name in ass.GetManifestResourceNames())
                    {
                        if (name.Trim().ToLower().Contains(containsText.Trim().ToLower()))
                            foundItems.Add(name);
                    }
                }
            }

            return foundItems;
        }

        public static List<string> GetFilteredWords()
        {
            var results = new List<string>();

            try
            {

            }
            catch (Exception ex)
            {

                throw;
            }


            return results;
        }

        public static string SafeSQL(string s)
        {
            s = s.Replace("'", "''");
            return s;
        }

        public static string GetProcessInstanceName(int pid)
        {
            PerformanceCounterCategory cat = new PerformanceCounterCategory("Process");
            string[] instances = cat.GetInstanceNames();

            foreach (string instance in instances)
            {

                using (PerformanceCounter cnt = new PerformanceCounter("Process", "ID Process", instance, true))
                {
                    int val = (int)cnt.RawValue;
                    if (val == pid)
                    {
                        return instance;
                    }
                }
            }

            return "";
        }

        public static string GetLocalIPAddress()
        {
            IPHostEntry host;
            string localIP = "";
            host = Dns.GetHostEntry(Dns.GetHostName());
            foreach (IPAddress ip in host.AddressList)
            {
                if (ip.AddressFamily == AddressFamily.InterNetwork)
                {
                    localIP = ip.ToString();
                    break;
                }
            }
            return localIP;
        }


        #region data utils

        public static T GetReaderValue<T>(DbDataReader reader, string fieldName, T defaultValue)
        {
            try
            {
                int index = reader.GetOrdinal(fieldName);

                if (reader.IsDBNull(index))
                    return defaultValue;

                Type t = typeof(T);
                Type u = Nullable.GetUnderlyingType(t);


                object value = reader.GetValue(index);

                if (u == null)
                {
                    return (T)Convert.ChangeType(value, t);
                }
                else
                {
                    return (T)Convert.ChangeType(value, u);
                }

            }
            catch (Exception ex)
            {
                throw new Exception(ex.ToString(), ex);
            }

        }

        #endregion
    }
}
