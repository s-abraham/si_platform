﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace SI
{
    public static class Settings
    {
        /// <summary>
        /// Retreives a setting value of a given type.
        /// </summary>
        /// <param name="settingKey">Case insensitive key for the desired setting.</param>
        /// <param name="defaultValue">Value to return if the settng was not found.</param>
        /// <returns>Returns the setting value if present, or the defaultValue parameter's value if not.</returns>private const string _settingsFileLocation = "C:/_CONFIG/SocialPathic.xml";
        private static System.IO.FileSystemWatcher _watcher = null;
        private static Dictionary<string, string> _settings = new Dictionary<string, string>();
        private static bool _wasInitted = false;

        #region init and file watching

        private static void init()
        {
            loadSettings();

            _watcher = new System.IO.FileSystemWatcher();
            _watcher.Path = getSettingsFile().Directory.FullName;
            _watcher.Filter = getSettingsFile().Name;
            _watcher.Changed += new System.IO.FileSystemEventHandler(_watcher_Changed);
            _watcher.EnableRaisingEvents = true;
            _wasInitted = true;
        }
        private static void _watcher_Changed(object sender, System.IO.FileSystemEventArgs e)
        {
            loadSettings();
        }

        #endregion

        #region saving and loading settings from file

        private static void loadSettings()
        {
            lock (_settings)
            {
                string baseXML = Util.GetResourceTextFile("DefaultSettings.xml", null);

                //if the file does not exist, write it out
                if (!File.Exists(getSettingsFile().FullName))
                {
                    File.WriteAllText(getSettingsFile().FullName, baseXML);
                }
                else //check for settings that have been added to the default, and add them to the file
                {
                    XDocument xmlBase = null;
                    using (StringReader reader = new StringReader(baseXML))
                    {
                        xmlBase = XDocument.Load(reader);
                    }
                    XDocument xmlCurrent = XDocument.Load(getSettingsFile().FullName);
                    XElement xSettings = xmlCurrent.Descendants("settings").First();

                    Dictionary<string, string> currentSettings = new Dictionary<string, string>();
                    foreach (XElement ele in xmlCurrent.Descendants("settings").Descendants())
                    {
                        currentSettings.Add(ele.Name.ToString().Trim().ToLower(), ele.Value);
                    }

                    bool newSettingsAdded = false;
                    foreach (XElement ele in xmlBase.Descendants("settings").Descendants())
                    {
                        string key = ele.Name.ToString().Trim().ToLower();
                        if (!currentSettings.ContainsKey(key))
                        {
                            //add new setting
                            XElement newItem = new XElement(key);
                            newItem.Value = ele.Value.Trim();
                            xSettings.Add(newItem);
                            newSettingsAdded = true;
                        }
                    }

                    if (newSettingsAdded)
                    {
                        File.Delete(getSettingsFile().FullName);
                        xmlCurrent.Save(getSettingsFile().FullName);
                    }
                }

                //load the settings
                {
                    _settings.Clear();
                    XDocument xml = XDocument.Load(getSettingsFile().FullName);
                    foreach (XElement ele in xml.Descendants("settings").Descendants())
                    {
                        _settings.Add(ele.Name.ToString().Trim().ToLower(), ele.Value);
                    }
                }
            }
        }

        private static void saveSettings()
        {
            try
            {
                _watcher.EnableRaisingEvents = false;

                lock (_settings)
                {
                    FileInfo fileInfo = getSettingsFile();
                    string oldPathname = fileInfo.FullName;
                    if (File.Exists(oldPathname))
                    {
                        string backupPathname = string.Format("{0}\\{1}.{2}.xml", fileInfo.DirectoryName, Path.GetFileNameWithoutExtension(fileInfo.FullName), DateTime.Now.ToString("yyyy.MM.dd.hh.mm.ss"));
                        if (!File.Exists(backupPathname))
                        {
                            File.Copy(oldPathname, backupPathname);
                        }
                    }

                    XDocument doc = new XDocument();
                    XElement settings = new XElement("settings");
                    doc.Add(settings);

                    foreach (string key in (from k in _settings.Keys orderby k select k))
                    {
                        XElement item = new XElement(key);
                        item.Value = _settings[key];
                        settings.Add(item);
                    }
                    if (File.Exists(oldPathname))
                        File.Delete(oldPathname);

                    doc.Save(oldPathname);

                }
            }
            catch (Exception ex)
            {
                ex = ex;
                throw new Exception(ex.ToString(), ex);
            }
            finally
            {
                _watcher.EnableRaisingEvents = true;
            }
        }

        private static FileInfo getSettingsFile()
        {
            return new FileInfo(ConfigurationManager.AppSettings["SettingsFile"]);
        }

        #endregion

        #region public methods

        /// <summary>
        /// Retreives a setting value.
        /// </summary>
        /// <param name="settingKey">Case insensitive key for the desired setting.</param>
        /// <returns>Returns the setting value if present, or null if not.</returns>
        public static string GetSetting(string settingKey)
        {
            if (!_wasInitted) init();
            settingKey = settingKey.Trim().ToLower();
            if (_settings.ContainsKey(settingKey))
                return _settings[settingKey];

            return null;
        }

        /// <summary>
        /// Retreives a setting value.
        /// </summary>
        /// <param name="settingKey">Case insensitive key for the desired setting.</param>
        /// <param name="defaultValue">Value to return if the settng was not found.</param>
        /// <returns>Returns the setting value if present, or the defaultValue parameter's value if not.</returns>
        public static string GetSetting(string settingKey, string defaultValue)
        {
            if (!_wasInitted) init();
            string val = GetSetting(settingKey);
            if (val == null)
                return defaultValue;
            else
                return val;
        }

        /// <summary>
        /// Retreives a setting value of a given type.
        /// </summary>
        /// <param name="settingKey">Case insensitive key for the desired setting.</param>
        /// <param name="defaultValue">Value to return if the settng was not found.</param>
        /// <returns>Returns the setting value if present, or the defaultValue parameter's value if not.</returns>
        public static T GetSetting<T>(string settingKey, T defaultValue)
        {
            if (!_wasInitted) init();
            var setVal = GetSetting(settingKey);
            if (string.IsNullOrEmpty(setVal))
                return defaultValue;
            return (T)Convert.ChangeType((object)setVal, typeof(T));
        }

        /// <summary>
        /// Retreives a setting value of a given type.
        /// </summary>
        /// <param name="settingKey">Case insensitive key for the desired setting.</param>
        /// <returns>Returns the setting value if present</returns>
        public static T GetSetting<T>(string settingKey)
        {
            if (!_wasInitted) init();
            return (T)Convert.ChangeType((object)GetSetting(settingKey), typeof(T));
        }

        /// <summary>
        /// Saves a setting value and causes a backup of the config file to be made, and a new 
        /// config file to be written with the new value.
        /// </summary>
        /// <param name="key">Case insensitive setting key</param>
        /// <param name="value">New value to be assigned to the key provided</param>
        public static void SaveSetting(string key, string value)
        {
            if (!_wasInitted) init();
            lock (_settings)
            {
                string settingKey = key.Trim().ToLower();
                if (!_settings.ContainsKey(settingKey))
                    _settings.Add(settingKey, value);
                else
                    _settings[settingKey] = value;
                saveSettings();
            }
        }

        /// <summary>
        /// Removes a setting identified by the key.  If it is found and removed, 
        /// the settings file will be saved to disk with a backup being made beforehand.
        /// </summary>
        /// <param name="key">Case insensitive key identifying the setting to be removed.</param>
        public static void DeleteSetting(string key)
        {
            if (!_wasInitted) init();
            string theKey = key.Trim().ToLower();
            lock (_settings)
            {
                if (!_settings.ContainsKey(theKey))
                    return;
                _settings.Remove(theKey);
                saveSettings();
            }
        }

        #endregion

    }
}
