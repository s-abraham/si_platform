﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace SI.Extensions
{
    public static class StringExtensions
    {
		public static bool ContainsCaseInsensitive(this string source, string value)
		{
			int results = source.IndexOf(value, StringComparison.CurrentCultureIgnoreCase);
			return results == -1 ? false : true;
		}

		public static Regex GuidRegEx = new Regex(@"^(\{){0,1}[0-9a-fA-F]{8}\-[0-9a-fA-F]{4}\-[0-9a-fA-F]{4}\-[0-9a-fA-F]{4}\-[0-9a-fA-F]{12}(\}){0,1}$");
		public static bool IsGuid(this string s)
		{
			return GuidRegEx.IsMatch(s);
		}

		public static Regex EmailRegEx = new Regex(@"^[^@]+@[^@]+\.[^@]+$");
		public static bool IsEmail(this string s)
		{
			return EmailRegEx.IsMatch(s);
		}

		public static long ToLong(this string value)
		{
			if (string.IsNullOrEmpty(value))
				return default(long);

			long retVal;

			if (Int64.TryParse(value, out retVal))
				return retVal;

			return default(long);
		}

		public static int ToInteger(this string value)
		{
			if (string.IsNullOrEmpty(value))
				return default(int);

			int retVal;

			if (Int32.TryParse(value, out retVal))
				return retVal;

			return default(int);
		}

		public static decimal ToDecimal(this string value)
		{
			if (string.IsNullOrEmpty(value))
				return default(decimal);

			decimal retVal;

			if (decimal.TryParse(value, out retVal))
				return retVal;

			return default(decimal);
		}

		public static double ToDouble(this string value)
		{
			if (string.IsNullOrEmpty(value))
				return default(double);

			double retVal;

			if (double.TryParse(value, out retVal))
				return retVal;

			return default(double);
		}

		public static string ToCurrency(this string value)
		{
			if (!string.IsNullOrEmpty(value))
			{
				string filtered = value.Replace("$", "").Replace(",", "");
				decimal decimalCurr;

				if (decimal.TryParse(filtered, out decimalCurr))
					return string.Format("{0:c0}", decimalCurr);
			}

			return string.Empty;
		}

		public static int? ToNullableInteger(this string value)
		{
			if (string.IsNullOrEmpty(value))
				return null;

			int retVal;
			return Int32.TryParse(value.Replace("%", ""), out retVal) ? retVal : default(int);
		}

		public static long? ToNullableLong(this string value)
		{
			if (string.IsNullOrEmpty(value))
				return null;

			long retVal;
			return Int64.TryParse(value, out retVal) ? retVal : default(long);
		}

		public static double? ToNullableDouble(this string value)
		{
			if (string.IsNullOrEmpty(value))
				return null;

			double retVal;
			return double.TryParse(value.Replace("$","").Replace(",", ""), out retVal) ? retVal : default(double);
		}

		public static bool ToBool(this string value)
		{
			if (string.IsNullOrEmpty(value))
				return false;

			bool retVal;
			return bool.TryParse(value, out retVal) ? retVal : default(bool);
		}

		public static DateTime ToDateTime(this string value)
		{
			if (string.IsNullOrEmpty(value))
				return default(DateTime);

			DateTime retVal;

			if (DateTime.TryParse(value, out retVal))
				return retVal;

			return default(DateTime);
		}

		public static DateTime? ToDateTimeNullable(this string value)
		{
			if (string.IsNullOrEmpty(value))
				return null;

			DateTime retVal;

			if (DateTime.TryParse(value, out retVal))
				return retVal;

			return default(DateTime);
		}


		/// <summary>
		/// Tests whether the current string is numeric in nature or not.
		/// </summary>
		/// <param name="line"></param>
		/// <returns></returns>
		public static bool IsNumeric(this string line)
		{
			int param = 0;
			return int.TryParse(line, out param);
		}

		/// <summary>
		/// Returns if the string is alphanumeric
		/// </summary>
		/// <param name="str"></param>
		/// <returns></returns>
		public static bool IsAlphaNumeric(this string str)
		{
			Regex pattern = new Regex("[^a-zA-Z0-9]");

			return !pattern.IsMatch(str);
		}

		public static string IsNullOptional(this string source, string optional)
		{
			if (string.IsNullOrEmpty(source))
				return optional;
			return source;
		}

		public static string ToTitleCase(this string mText)
		{
			string rText = "";
			System.Globalization.CultureInfo cultureInfo = System.Threading.Thread.CurrentThread.CurrentCulture;
			System.Globalization.TextInfo TextInfo = cultureInfo.TextInfo;
			rText = TextInfo.ToTitleCase(mText.ToLower());

			return rText;
		}

		public static string StripHtml(this string input)
		{
			var tagsExpression = new Regex(@"</?.+?>");
			return tagsExpression.Replace(input, " ");
		}

		public static string ToBlankableString(this double? input)
		{
			if (input.HasValue)
				return input.Value.ToString();
			return "";
		}

		public static string ToBlankableString(this int? input)
		{
			if (input.HasValue)
				return input.Value.ToString();
			return "";
		}

		public static string StripWhiteSpace(this string input)
		{
			if (string.IsNullOrEmpty(input))
				return string.Empty;

			return Regex.Replace(input, @"\s+", "");
		}
    }
}
