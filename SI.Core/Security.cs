﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace SI
{
    public class Security
    {
        private static SHA512Managed _sha = null;      
        private static Random r = new Random(DateTime.Now.Millisecond + DateTime.Now.Minute + DateTime.Now.Second);

        #region Reversible Encryption for Stored Network Tokens

        /// <summary>
        /// Encrypts a string value using AES, and using password and salt from settings
        /// </summary>
        /// <param name="value">The value to encrypt</param>
        /// <returns>Base64 encoded result of the encryption</returns>
        public static string Encrypt(string value)
        {
            byte[] data = getEncryptionEncoding().GetBytes(value);
            using (MemoryStream ms = new MemoryStream())
            {
                using (CryptoStream stream = new CryptoStream(ms, getEncrypter(), CryptoStreamMode.Write))
                {
                    stream.Write(data, 0, data.Length);
                    stream.FlushFinalBlock();
                }
                byte[] encryptedData = ms.ToArray();
                string encoded = Convert.ToBase64String(encryptedData);
                return encoded;
            }
        }

        /// <summary>
        /// Decrypts a string value using AES, and using password and salt from settings
        /// </summary>
        /// <param name="value">The Base64 encoded encrypted value</param>
        /// <returns>The decrypted value</returns>
        public static string Decrypt(string value)
        {
            byte[] data = Convert.FromBase64String(value);
            using (MemoryStream ms = new MemoryStream())
            {
                ms.Write(data, 0, data.Length);
                ms.Position = 0;
                using (CryptoStream stream = new CryptoStream(ms, getDecrypter(), CryptoStreamMode.Read))
                {
                    ms.Flush();
                    using (StreamReader reader = new StreamReader(stream, getEncryptionEncoding()))
                    {
                        return reader.ReadToEnd();
                    }
                }
                
            }
        }
                
        private static ICryptoTransform getEncrypter()
        {

            byte[] key;
            byte[] iv;
            getEncryptionKeys(out key, out iv);

            AesCryptoServiceProvider provider = new AesCryptoServiceProvider();
            provider.Padding = getPaddingMode();
            ICryptoTransform transform = provider.CreateEncryptor(key, iv);

            return transform;

        }

        private static ICryptoTransform getDecrypter()
        {

            byte[] key;
            byte[] iv;
            getEncryptionKeys(out key, out iv);

            AesCryptoServiceProvider provider = new AesCryptoServiceProvider();
            provider.Padding = getPaddingMode();
            ICryptoTransform transform = provider.CreateDecryptor(key, iv);

            return transform;

        }

        private static void getEncryptionKeys(out byte[] key, out byte[] iv)
        {
            byte[] salt = Encoding.UTF8.GetBytes(Settings.GetSetting("security.hashsalt").Trim());
            Rfc2898DeriveBytes keyGenerator = new Rfc2898DeriveBytes(Settings.GetSetting("security.aespassword").Trim(), salt);

            key = keyGenerator.GetBytes(16);
            iv = keyGenerator.GetBytes(16);
        }
        private static PaddingMode getPaddingMode()
        {
            return PaddingMode.PKCS7;
        }
        private static Encoding getEncryptionEncoding()
        {
            return Encoding.Unicode;
        }

        #endregion

        #region password hashing

        /// <summary>
        /// Creates a salted password hash.
        /// </summary>
        /// <param name="password">The cleartext password</param>
        /// <returns>A base64 encoded salted hash.</returns>
        public static string GetPasswordHash(string password)
        {
            if (_sha == null) _sha = new SHA512Managed();

            int maxLength = Settings.GetSetting<int>("security.hashlength");
            string salt = Settings.GetSetting("security.hashsalt");

            password = string.Format("{0}{1}", password.Trim().ToLower(), salt);
            byte[] hash = _sha.ComputeHash(System.Text.Encoding.UTF8.GetBytes(password));
            string hashString = Convert.ToBase64String(hash);
            if (hashString.Length > maxLength)
                hashString = hashString.Substring(0, maxLength);
            return hashString;
        }

        public static string GetHash(string content, int maxLength)
        {
            if (_sha == null) _sha = new SHA512Managed();
            
            content = string.Format("{0}", content.Trim().ToLower());
            byte[] hash = _sha.ComputeHash(System.Text.Encoding.UTF8.GetBytes(content));
            string hashString = Convert.ToBase64String(hash);
            if (hashString.Length > maxLength)
                hashString = hashString.Substring(0, maxLength);
            return hashString;
        }

        public static string GetFilenameSafeHash(byte[] data, int maxLength)
        {

            if (_sha == null) _sha = new SHA512Managed();

            byte[] hash = _sha.ComputeHash(data);
            string hashString = Convert.ToBase64String(hash);
            hashString = SafeName(hashString);
            if (hashString.Length > maxLength)
            {
                hashString = hashString.Substring(0, maxLength);
            }
            return hashString;
        }

        #endregion

        #region safe strings

        private const string _urlChars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890-~.";
        /// <summary>
        /// Removes characters from a string that cannot be used in a URL
        /// </summary>
        /// <param name="name">The unprocessed text</param>
        /// <returns>URL safe string</returns>
        public static string SafeURL(string name)
        {
            string outString = "";
            for (int i = 0; i < name.Length; i++)
            {
                if (_urlChars.Contains(name.Substring(i, 1)))
                    outString = outString + name.Substring(i, 1);
            }
            return outString;
        }

        private const string _nameChars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ.'";
        /// <summary>
        /// Removes any non alpha characters, or the period, from the string provided.
        /// </summary>
        /// <param name="name">The unprocessed text.</param>
        /// <returns>The alpha + period text.</returns>
        public static string SafeName(string name)
        {
            string outString = "";
            for (int i = 0; i < name.Length; i++)
            {
                if (_nameChars.Contains(name.Substring(i, 1)))
                    outString = outString + name.Substring(i, 1);
            }
            return outString;
        }

        #endregion

        #region email validation

        private static Regex ValidEmailRegex = CreateValidEmailRegex();
        private static Regex CreateValidEmailRegex()
        {
            string validEmailPattern = @"^(?!\.)(""([^""\r\\]|\\[""\r\\])*""|"
                + @"([-a-z0-9!#$%&'*+/=?^_`{|}~]|(?<!\.)\.)*)(?<!\.)"
                + @"@[a-z0-9][\w\.-]*[a-z0-9]\.[a-z][a-z\.]*[a-z]$";

            return new Regex(validEmailPattern, RegexOptions.IgnoreCase);
        }

        /// <summary>
        /// Determines if an email address is valid.
        /// </summary>
        /// <param name="emailAddress">The email address to test.</param>
        /// <returns>True if the email address supplied is valid</returns>
        public static bool EmailIsValid(string emailAddress)
        {
            bool isValid = ValidEmailRegex.IsMatch(emailAddress);

            return isValid;
        }

        #endregion

        #region random codes

        private const string _codeChars = "abcdefghijklmnopqrstuvwxyz1234567890";

        /// <summary>
        /// Creates a string filled with random letters or numbers.
        /// </summary>
        /// <param name="length">The length of the string to create.</param>
        /// <returns>The randomly created alphanumeric string.</returns>
        public static string GetRandomCode(int length)
        {
            string theCode = "";
            for (int i = 1; i <= length; i++)
            {
                theCode = theCode + _codeChars.Substring(r.Next(0, _codeChars.Length - 1), 1);
            }

            return theCode;

        }

        #endregion
    }
}
