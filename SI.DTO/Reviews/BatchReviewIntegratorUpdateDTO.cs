﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SI.DTO
{
    public class BatchReviewIntegratorUpdateDTO
    {
        public BatchReviewIntegratorUpdateDTO()
        {
            RawReviewIDsToDelete = new List<long>();
            ReviewIDsSetFiltered = new List<long>();
            ReviewIDsSetUnfiltered = new List<long>();
            ReviewsToSave = new List<ReviewDTO>();
            ReviewSummaries = new List<ReviewSummaryDTO>();
        }

        public List<long> RawReviewIDsToDelete { get; set; }
        public List<long> ReviewIDsSetFiltered { get; set; }
        public List<long> ReviewIDsSetUnfiltered { get; set; }
        public List<ReviewDTO> ReviewsToSave { get; set; }

        public List<ReviewSummaryDTO> ReviewSummaries { get; set; }
    }
}
