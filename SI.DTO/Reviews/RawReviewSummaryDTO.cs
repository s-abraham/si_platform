﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SI.DTO
{
    public class RawReviewSummaryDTO
    {
        public long AccountID { get; set; }
        public ReviewSourceEnum ReviewSource { get; set; }

        public decimal SalesAverageRating { get; set; }
        public int? SalesReviewCount { get; set; }

        public decimal ServiceAverageRating { get; set; }
        public int? ServiceReviewCount { get; set; }

        public List<KeyValuePair<string, string>> ExtraInfo { get; set; }
    }
}
