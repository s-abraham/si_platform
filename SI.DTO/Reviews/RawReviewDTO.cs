﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SI.DTO
{
    public class RawReviewDTO
    {
        public RawReviewDTO()
        {
            Comments = new List<RawReviewCommentDTO>();
            ExtraInfo = new List<KeyValuePair<string, string>>();
        }

        public long AccountID { get; set; }
        public ReviewSourceEnum ReviewSource { get; set; }

        public decimal? Rating { get; set; }

        public string ReviewerName { get; set; }
        public SIDateTime ReviewDate { get; set; }

        public string ReviewText { get; set; }
        public string ReviewURL { get; set; }

        public List<RawReviewCommentDTO> Comments { get; set; }
        public List<KeyValuePair<string, string>> ExtraInfo { get; set; }
    }
}
