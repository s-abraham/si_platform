﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SI.DTO
{
    public class BatchReviewIntegratorPullDTO
    {
        public BatchReviewIntegratorPullDTO()
        {
            Reviews = new List<ReviewDTO>();
            Summaries = new List<ReviewSummaryDTO>();
        }
        public List<ReviewDTO> Reviews { get; set; }
        public List<ReviewSummaryDTO> Summaries { get; set; }
    }
}
