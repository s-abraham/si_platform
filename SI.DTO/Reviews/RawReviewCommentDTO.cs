﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SI.DTO
{
    public class RawReviewCommentDTO
    {
        public string Comment { get; set; }
        public SIDateTime CommentDate { get; set; }
    }
}
