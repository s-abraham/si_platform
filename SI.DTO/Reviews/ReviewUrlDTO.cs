﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SI.DTO
{
    public class ReviewUrlDTO
    {
        public long? ID { get; set; }

        public long AccountID { get; set; }
        public ReviewSourceEnum ReviewSource { get; set; }

        public string HtmlURL { get; set; }
        public string ApiURL { get; set; }

        public string ExternalID { get; set; }
        public string ExternalID2 { get; set; }

        public SIDateTime LastSummaryRun { get; set; }
        public SIDateTime LastSummaryAttempted { get; set; }
        public SIDateTime LastDetailRun { get; set; }
        public SIDateTime LastDetailAttempted { get; set; }
        public SIDateTime LastValidation { get; set; }
        public SIDateTime DateCreated { get; set; }    
        public bool isValid { get; set; }
    }

    public class AccountReviewUrlDTO
    {
        public long? ID { get; set; }

        public long AccountID { get; set; }
        public ReviewSourceEnum ReviewSource { get; set; }
        public string ReviewSourceName { get; set; }

        public string HtmlURL { get; set; }
        public string ApiURL { get; set; }

        public string ExternalID { get; set; }
        public string ExternalID2 { get; set; }
      
        public DateTime LastSummaryRun { get; set; }
        public DateTime LastSummaryAttempted { get; set; }
        public DateTime LastDetailRun { get; set; }
        public DateTime LastDetailAttempted { get; set; }
        public DateTime LastValidation { get; set; }

        public DateTime DateCreated { get; set; }
        public int? SummaryDelta { get; set; }
        public int? DetailDelta { get; set; }

        public bool isValid { get; set; }

        public ReviewUrlDTOEnum Status{ get; set; }

        public int URLCreatedHours { get; set; }
    }

    public enum ReviewUrlDTOEnum
    {
        Valid =1,
        InValid = 2,
        NotYetRan =3
    }
}
