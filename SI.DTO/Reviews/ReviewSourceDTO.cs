﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SI.DTO
{
    public class ReviewSourceDTO
    {
        public long ID { get; set; }
        public string Name { get; set; }
        public string ImageUrl { get; set; }
        public int DisplayOrder { get; set; }
        public ReviewSourceStatusEnum Status { get; set; }
    }

    public enum ReviewSourceStatusEnum : int
    {
        Inactive = 0,
        Active = 1,
        Paused = 2
    }
}
