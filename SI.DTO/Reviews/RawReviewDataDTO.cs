﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SI.DTO
{
    public class RawReviewDataDTO
    {
        public List<ReviewDTO> RawReviews { get; set; }
        public RawReviewSummaryDTO Summary { get; set; }
    }
}
