﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SI.DTO
{
    public class SignupSetupInfoDTO
    {
        public SignupSetupInfoDTO()
        {
            VerticalMarkets = new List<VerticalMarketDTO>();
            PackageInfos = new List<SignupPackageInfoDTO>();
            Countries = new List<CountryDTO>();
        }
                
        public List<VerticalMarketDTO> VerticalMarkets { get; set; }
        public List<CountryDTO> Countries { get; set; }
        public List<SignupPackageInfoDTO> PackageInfos { get; set; }
        
    }

    public class SignupPackageInfoDTO
    {
        public long ResellerPackageID { get; set; }
        public long PackageID { get; set; }
        public long ResellerID { get; set; }

        public string PackageName { get; set; }
        public string ResellerName { get; set; }

        public decimal? Price { get; set; }
        public bool Default { get; set; }
    }
}
