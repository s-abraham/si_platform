﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SI.DTO
{
    public class SignupSubmitInfoDTO
    {
        public SignupSubmitInfoDTO()
        {
            FranchiseTypeIDs = new List<long>();
            SelectedResellerPackages = new List<SignupSubmitSelectedPackageInfoDTO>();
        }
        
        #region company info

		public long? SyndicatorID { get; set; }
        public long ResellerID { get; set; }
        public long? PreferredParentAccountID { get; set; }
        public long TimezoneID { get; set; }
        public string AccessTokenUsed { get; set; }

        public string AccountName { get; set; }
        public string StreetAddress { get; set; }
        public string CityName { get; set; }

        public long VerticalID { get; set; }
        public List<long> FranchiseTypeIDs { get; set; }

        public long CountryID { get; set; }
        public long StateID { get; set; }

        public string PostalCode { get; set; }
        public string BillingPhone { get; set; }

        public string CompanyEmailAddress { get; set; }
        public string CompanyWebsiteURL { get; set; }

        #endregion

        #region user info

        public string UserFirstName { get; set; }
        public string UserLastName { get; set; }
        public string UserEmailAddress { get; set; }

        #endregion

        #region credential info

        public SocialNetworkEnum SocialNetwork { get; set; }
        public long SocialAppID { get; set; }
        public string WebsiteURL { get; set; }
        public string UniqueID { get; set; }
        public string URI { get; set; }
        public string PictureURL { get; set; }
        public string ScreenName { get; set; }
        public string Token { get; set; }
        public string TokenSecret { get; set; }

        #endregion

        #region Package Info

        public List<SignupSubmitSelectedPackageInfoDTO> SelectedResellerPackages { get; set; }

        #endregion

    }

    public class SignupSubmitSelectedPackageInfoDTO
    {
        public long ResellerPackageID { get; set; }
        public decimal? Price { get; set; }
    }
}
