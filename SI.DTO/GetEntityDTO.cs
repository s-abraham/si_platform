﻿using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SI.DTO
{
    /// <summary>
    /// This is the class used to retrieve single objects from the business logic layer.
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class GetEntityDTO<T>
    {
        public GetEntityDTO()
        {
            Entity = default(T);
            Problems = new List<string>();            
        }
        public GetEntityDTO(T obj) : this()
        {
            Entity = obj;            
        }
        
        public T Entity { get; set; }

        public List<string> Problems { get; set; }


        public bool HasProblems
        {
            get
            {
                return Problems.Any();
            }
        }
    }

    public class GetEntityDTO<T,O> : GetEntityDTO<T>
    {
        public GetEntityDTO() : base()
        {
            Options = default(O);
        }
        public GetEntityDTO(T obj, O opts) : base(obj)
        {
            Options = opts;
        }

        public O Options { get; set; }
    }
}
