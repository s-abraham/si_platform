﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SI.DTO
{
    public class HarvesterSourceSettingsDTO
    {
        public string ResultNode { get; set; }
        public string LinkNode { get; set; }
    }


}
