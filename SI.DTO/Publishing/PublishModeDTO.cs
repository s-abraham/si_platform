﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SI.DTO
{
    public class PublishModeDTO
    {
        /// <summary>
        /// This will either be "Publish" for standard publish, or the name of a syndicator for others
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// If this is not null, then it indicates that the mode is for syndication.
        /// </summary>
        public long? SyndicatorID { get; set; }

        /// <summary>
        /// If this is a syndicator, the url will be a logo for that syndicator
        /// </summary>
        public string LogoURL { get; set; }

        public bool AllowPostNow { get; set; }

        public bool ForceApprovals { get; set; }
    }
}
