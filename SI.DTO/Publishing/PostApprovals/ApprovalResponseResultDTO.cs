﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SI.DTO
{
    public class ApprovalResponseResultDTO
    {
        public ApprovalResponseResultDTO()
        {
            Problems = new List<string>();
        }
        public List<string> Problems { get; set; }
    }    
}
