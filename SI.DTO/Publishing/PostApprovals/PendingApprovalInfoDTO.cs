﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SI.DTO
{
    public class PendingApprovalInfoDTO
    {
        public long PostApprovalID { get; set; }

        public long PostID { get; set; }
        public long PostTargetID { get; set; }
        public string Title { get; set; }
        public string Message { get; set; }
        public string Link { get; set; }
        public string LinkDescription { get; set; }
        public string LinkTitle { get; set; }
        public string LinkImageURL { get; set; }
        public string PostImageURL { get; set; }

        public long TargetAccountID { get; set; }
        public string TargetAccountName { get; set; }

        public int ApprovalOrder { get; set; }

        public long? SyndicatorID { get; set; }
        public string SyndicatorName { get; set; }

        public SIDateTime ScheduleDate { get; set; }
        public SIDateTime PublishedDate { get; set; }
        public SIDateTime PostCreationDate { get; set; }

        public SocialNetworkEnum SocialNetwork { get; set; }

        public long PostedByUserID { get; set; }
        public string PostedByUserName { get; set; }

        public string SocialNetworkName { get; set; }
        public string SocialNetworkScreenName { get; set; }
        public string SocialNetworkPictureURL { get; set; }

        public string SocialNetworkURL { get; set; }
        public string SocialNetworkUniqueID { get; set; }

        public bool CredentialIsActive { get; set; }
        public bool CredentialIsValid { get; set; }

        public bool AllowChanges
        {
            get
            {
                return
                    AllowChangePublishDate.GetValueOrDefault(false)
                    || AllowChangeMessage.GetValueOrDefault(false)
                    || AllowChangeImage.GetValueOrDefault(false)
                    || AllowChangeTargets.GetValueOrDefault(false)
                    || AllowChangeLinkURL.GetValueOrDefault(false)
                    || AllowChangeLinkThumb.GetValueOrDefault(false)
                    || AllowChangeLinkText.GetValueOrDefault(false)
                ;
            }
        }

        public bool? AllowChangePublishDate { get; set; }
        public bool? AllowChangeMessage { get; set; }
        public bool? AllowChangeImage { get; set; }
        public bool? AllowChangeTargets { get; set; }
        public bool? AllowChangeLinkURL { get; set; }
        public bool? AllowChangeLinkThumb { get; set; }
        public bool? AllowChangeLinkText { get; set; }

    }
}
