﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SI.DTO
{
    public class RevisePostDTO
    {
        public RevisePostDTO()
        {            
            SocialNetworks = new List<RevisePostSocialNetworkInfoDTO>();
            PostImages = new List<PostImageDTO>();
        }

        public long PostID { get; set; }

        public PostTypeEnum PostType { get; set; }

        public string PostTitle { get; set; }
        public long AccountID { get; set; }
        public string AccountName { get; set; }

        public string SyndicatorName { get; set; }
        public SyndicationPublishOptionsDTO SyndicationOptions { get; set; }

        public string Message { get; set; }

        public string Link { get; set; }
        public string LinkTitle { get; set; }
        public string LinkDescription { get; set; }
        public string LinkThumbURL { get; set; }

        public SIDateTime ScheduleDate { get; set; }

        public List<PostImageDTO> PostImages { get; set; }

        public List<RevisePostSocialNetworkInfoDTO> SocialNetworks { get; set; }


    }

    public class RevisePostSocialNetworkInfoDTO
    {
        public long CredentialID { get; set; }
        public SocialNetworkEnum SocialNetwork { get; set; }
        public string ScreenName { get; set; }
        public string URL { get; set; }
        public bool Selected { get; set; }
        public SIDateTime ScheduleDate { get; set; }
    }
}
