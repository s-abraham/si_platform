﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SI.DTO
{
    public class PostImageResultDTO
    {
        public long PostImageResultID {get; set;}
        public long PostImageID {get; set;}
        public long PostTargetID {get; set;}
        public long? JobID {get; set;}
        public string ResultID {get; set;}
        public string FeedID {get; set;}
        public DateTime? ProcessedDate {get; set;}
        public DateTime? FailedDate { get; set; }
        
    }
}
