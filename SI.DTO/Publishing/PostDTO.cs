﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SI.DTO
{
    public class PostDTO
    {
		public PostDTO()
		{
			Images = new List<PostImageDTO>();
			Targets = new List<PostTargetDTO>();
		}

        public long? ID { get; set; }
        
        public string Title { get; set; }
        
        public PostSourceEnum Source { get; set; }
        public PostTypeEnum Type { get; set; }

        public long PostCategoryID { get; set; }

        public PostRevisionDTO MasterRevision { get; set; }

        public List<PostImageDTO> Images { get; set; }
        public List<PostTargetDTO> Targets { get; set; }

        public bool Draft { get; set; }

        public long? SyndicatorID { get; set; }

        public SyndicationPublishOptionsDTO SyndicationOptions { get; set; }

        public SIDateTime DateCreated { get; set; }
        public SIDateTime DateModified { get; set; }

        public SIDateTime ScheduleDate { get; set; }
        public SIDateTime PostExpirationDate { get; set; }
		public SIDateTime ApprovalExpirationDate { get; set; }

        public long PostedByUserID { get; set; }

        public int TotalTargetCount { get; set; }
        public int TargetsUserCanSeeCount { get; set; }
        public bool RevisionMode { get; set; }
    }
}
