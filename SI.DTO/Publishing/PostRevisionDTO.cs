﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SI.DTO
{
    public class PostRevisionDTO
    {
        public long? ID { get; set; }

        public string Message { get; set; }
        public string Link { get; set; }
        public string LinkTitle { get; set; }
        public string LinkDescription { get; set; }

        public string ImageURL { get; set; }

        public SIDateTime DateCreated { get; set; }
    }
}
