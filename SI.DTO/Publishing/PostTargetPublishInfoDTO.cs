﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SI.DTO
{
    public class PostTargetPublishInfoDTO
    {
        public long CredentialID { get; set; }

        public long PostID { get; set; }
        public PostTypeEnum PostType { get; set; }
        public long PostTargetID { get; set; }
        public long? PostImageID { get; set; }

        public string AppID { get; set; }
        public string AppSecret { get; set; }

        public string UniqueID { get; set; }
        public string Token { get; set; }
        public string TokenSecret { get; set; }

        public string Title { get; set; }
        public string Message { get; set; }
        public string Link { get; set; }
        public string LinkTitle { get; set; }
        public string LinkDescription { get; set; }
        public string ImageURL { get; set; }
        public string RevisionImageURL { get; set; }

        public SIDateTime ScheduleDate { get; set; }
        public SIDateTime PublishedDate { get; set; }

        public string ImageResultID { get; set; }
        public string ImageFeedID { get; set; }

        public string ResultID { get; set; }
        public string FeedID { get; set; }

        public long UserID { get; set; }
    }
}
