﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SI.DTO
{
    public class PostImageDTO
    {
        public long? ID { get; set; }        
        public string URL { get; set; }
    }
}
