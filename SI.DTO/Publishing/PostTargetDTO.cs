﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SI.DTO
{
    public class PostTargetDTO
    {
        public long? ID { get; set; }
        public long CredentialID { get; set; }

        public PostTargetStatusEnum Status { get; set; }

        /// <summary>
        /// This should be read-only from the UI.  If DateSceduled is null, this value should be ignored and is undefined.
        /// </summary>
        public TimeSpan TimeUntilScheduled { get; set; }

        /// <summary>
        /// This should be read-only from the UI.  If null, it means immediate delivery
        /// </summary>
        public SIDateTime DateScheduled { get; set; }
    }
}
