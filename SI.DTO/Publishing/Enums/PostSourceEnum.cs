﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SI.DTO
{
    public enum PostSourceEnum : long
    {
        Review = 1,
        Publish = 2
    }
}
