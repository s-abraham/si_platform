﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SI.DTO
{
    public enum PostTargetStatusEnum
    {
        Draft,
        Scheduled,
        Processing,
        Published,
        Expired,
        Failed
    }
}
