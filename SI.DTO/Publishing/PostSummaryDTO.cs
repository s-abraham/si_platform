﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SI.DTO
{
    public class PostSummaryDTO
    {
        // basic information
        public long PostID { get; set; }
        public string Title { get; set; }
        public PostTypeEnum PostType { get; set; }

        // content stuff
        public string LastMasterRevisionMessage { get; set; }
        public string LastMasterRevisionLink { get; set; }
        public string LastMasterRevisionLinkImageURL { get; set; }
        public string LastMasterRevisionLinkDescription { get; set; }
        public string LastMasterRevisionLinkTitle { get; set; }

        public long? FirstPostImageID { get; set; }
        public string FirstPostImageURL { get; set; }

        // category stuff
        public long PostCategoryID { get; set; }
        public long PostCategoryVerticalID { get; set; }
        public string PostCategoryName { get; set; }
        public string PostCategoryVertcialName { get; set; }

        // dates
        public SIDateTime ScheduledDate { get; set; }
        public SIDateTime CreatedDate { get; set; }
        public SIDateTime ApprovalExpirationDate { get; set; }
        public SIDateTime PostExpirationDate { get; set; }

        // creator
        public long CreatedByUserID { get; set; }
        public string CreatedByUser { get; set; }

        // counts
        public int NumberOfTargets { get; set; }
        public int NumberOfAccountsTargetted { get; set; }

        public int NumberOfRevisedTargets { get; set; }
        public int NumberOfAccountsWithRevisedTargets { get; set; }

        // options
        public SyndicationPublishOptionsDTO SyndicationPublishOptions { get; set; }
    }
}
