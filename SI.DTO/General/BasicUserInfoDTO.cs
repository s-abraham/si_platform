﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SI.DTO
{
    public class BasicUserInfoDTO
    {
        public long ID { get; set; }
        public string Username { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string EmailAddress { get; set; }

        public bool IsActive { get; set; }

        public long MemberOfAccountID { get; set; }
        public string MemberOfAccountName { get; set; }
    }
}
