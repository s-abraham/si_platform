﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SI.DTO
{
    public class EdmundsReviewCommentDTO
    {        
        public SIDateTime CommentDate { get; set; }
        public string Text { get; set; }
    }
}
