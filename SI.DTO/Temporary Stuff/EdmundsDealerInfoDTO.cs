﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SI.DTO
{
    public class EdmundsDealerInfoDTO
    {
        public long EdmundsDealerID { get; set; }
        public string DealerName { get; set; }
        public string CityName { get; set; }
        public string StateName { get; set; }
        public string HtmlURL { get; set; }
    }
}
