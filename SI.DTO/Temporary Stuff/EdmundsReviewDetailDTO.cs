﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SI.DTO
{
    public class EdmundsReviewDetailDTO
    {
        public EdmundsReviewDetailDTO()
        {
            Comments = new List<EdmundsReviewCommentDTO>();
        }

        public long EdmundsDealerID { get; set; }

        public string ReviewType { get; set; }

        public decimal? Rating { get; set; }
        public string ReviewerName { get; set; }
        public string ReviewText { get; set; }

        public List<EdmundsReviewCommentDTO> Comments { get; set; }
        
        public SIDateTime ReviewDate { get; set; }
    }
}
