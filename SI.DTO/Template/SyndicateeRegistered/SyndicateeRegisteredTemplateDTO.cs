﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SI.DTO.Models.Template.SyndicatorAdded
{
    public class SyndicatorAddedTemplateDTO
    {
        public string body { get; set; }
    }
}
