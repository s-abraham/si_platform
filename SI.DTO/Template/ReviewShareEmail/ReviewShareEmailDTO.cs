﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SI.DTO
{
	public class ReviewShareEmailTemplateDTO
	{
		public string Body { get; set; }
	}

	public class ReviewShareEmailDTO
	{
		public long ReviewID { get; set; }
		public long UserToEmailID { get; set; }
		public string Subject { get; set; }
		public string Comments { get; set; }
        public string ToEmailID { get; set; }
		
	}
}
