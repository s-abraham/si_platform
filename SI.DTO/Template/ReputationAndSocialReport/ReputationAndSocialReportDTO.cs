﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SI.DTO
{
    public class ReputationAndSocialReportDTO
    {
        public string Body { get; set; }
        public string TemplateRepeatingBlock { get; set; }
        public string TemplateRepeatingBlockSummary { get; set; }
    }
}
