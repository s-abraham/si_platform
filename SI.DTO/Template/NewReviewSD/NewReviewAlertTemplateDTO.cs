﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SI.DTO
{
    public class NewReviewAlertTemplateDTO
    {
        public string body { get; set; }
        public string TemplateRepeatingBlock { get; set; }
    }
}
