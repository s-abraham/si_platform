﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SI.DTO
{
    public class NewSocialAlertTemplateDTO
    {
        public string body { get; set; }
        public string CommentRepeatingBlock { get; set; }
        public string PostRepeatingBlock { get; set; }       
    }
}
