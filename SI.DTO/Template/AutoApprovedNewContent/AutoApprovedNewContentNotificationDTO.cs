﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SI.DTO
{
    public class AutoApprovedNewContentNotificationDTO
    {
        public string Body { get; set; }
        public string Accounts { get; set; }
        public string LinkSection { get; set; }
    }
}
