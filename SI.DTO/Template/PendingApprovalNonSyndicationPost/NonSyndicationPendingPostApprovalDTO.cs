﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SI.DTO
{
    public class NonSyndicationPendingPostApprovalDTO
    {
        public string Body { get; set; }
        public string Image { get; set; }
        public string Link { get; set; }
        public string TemplateRepeatingBlock { get; set; }
    }
}
