﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SI.DTO
{
    public class ClientFaceingTokenValidationDTO
    {
        public string Body { get; set; }
        public string Subject { get; set; }

        public string Body48Hours { get; set; }
        public string Subject48Hours { get; set; }

        public string Body96Hours { get; set; }
        public string Subject96Hours { get; set; }
    }
}

