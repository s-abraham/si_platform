﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SI.DTO
{    
    public class MenuItemDTO
    {
        public MenuItemDTO()
        {
            Items = new List<MenuItemDTO>();
        }

        public string Name { get; set; }
        public string URL { get; set; }
        public List<MenuItemDTO> Items { get; set; }
        public bool Selected { get; set; }
    }
}