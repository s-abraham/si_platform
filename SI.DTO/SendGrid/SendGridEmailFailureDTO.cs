﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SI.DTO
{
    public class SendGridEmailFailureDTO
    {
        public string EmailAddress { get; set; }
        public long? MatchedUserID { get; set; }
        
        public SendGridStatusEnum SendGridStatus { get; set; }
        public SendGridProcessingStatusEnum ProcessingStatus { get; set; }

        public DateTime DateCreated { get; set; }
        public bool PreviouslyFound { get; set; }

    }
}
