﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SI.DTO
{
    public class AuditProductSelection
    {
        public long accountID { get; set; }
        public long ResellerID { get; set; }
        public long PackageID { get; set; }
        public long SubscriptionID { get; set; }
    }
}
