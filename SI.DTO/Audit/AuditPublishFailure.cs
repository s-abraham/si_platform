﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SI.DTO
{
    public class AuditPublishFailure
    {
        public long JobID { get; set; }

        public long PostTargetID { get; set; }
        public long? PostImageID { get; set; }

        public long AccountID { get; set; }
        public long CredentialID { get; set; }
        public long SocialNetworkID { get; set; }
        public long SocialAppID { get; set; }

        public string SocialNetworkResponse { get; set; }
    }
}
