﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SI.DTO
{
    public class AuditAssertionFailed
    {
        public string Description { get; set; }
    }
}
