﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SI.DTO
{
    public class AuditReviewURL
    {
        public long AccountID { get; set; }
        public List<AuditReviewURLOld> auditReviewURLOld { get; set; }
        public List<AuditReviewURLNew> auditReviewURLNew { get; set; }        
    }

    public class AuditReviewURLOld
    {
        public ReviewSourceEnum ReviewSource { get; set; }
        public string HtmlURL { get; set; }
        public string ApiURL { get; set; }
        public string ExternalID { get; set; }
        public string ExternalID2 { get; set; }
    }

    public class AuditReviewURLNew
    {
        public ReviewSourceEnum ReviewSource { get; set; }
        public string HtmlURL { get; set; }        
    }

}

