﻿using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SI.DTO
{
    public class AuditNewReviewDetected
    {
        public long ReviewID { get; set; }
        public long AccountID { get; set; }
    }
}
