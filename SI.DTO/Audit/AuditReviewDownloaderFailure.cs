﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SI.DTO
{
    public class AuditReviewDownloaderFailure
    {
        public long JobID { get; set; }
        public long ReviewSourceID { get; set; }

        public string URL { get; set; }
        public string WebException { get; set; }
        public List<string> FailedXPaths { get; set; }        
    }
}
