﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SI.DTO
{
    public class AuditProcessorException
    {
        public List<long> JobIDs { get; set; }
        public string ExceptionMessage { get; set; }
    }
}
