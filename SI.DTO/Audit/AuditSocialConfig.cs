﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SI.DTO
{
	public class AuditSocialConfig
	{
		public long SocialAppId { get; set; }
		public string PageID { get; set; }
		public string Token { get; set; }
		public string Website { get; set; }
		
	}
}
