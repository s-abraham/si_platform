﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SI.DTO
{
    public enum AuditTypeEnum : long
    {
        General = 0, //used for debugging

        // these are user action related audits
        SignedIn = 10,
        SignedOut = 20,
        UserActivity = 50,

        AccountConfiguration = 2040,
        AccountActivity = 2041,

        SQLException = 100,
        AssertFailedCondition = 110,

        ReviewDownloaderSuccess = 500,
        ReviewDownloaderFailure = 510,
        NewReviewDetected = 520,

        PublishFailure = 600,
        PublishSuccess = 610,
        QueuePublishJobFailure = 601,

        FacebookDownloaderFailure = 700,
        FacebookIntegratorFailure = 701,
        FacebookPostStatisticsDownloaderFailure = 706,

        GooglePlusDownloaderFailure = 702,
        GooglePlusIntegratorFailure = 703,

        TwitterDownloaderFailure = 704,
        TwitterIntegratorFailure = 705,

        SocialConfiguration = 800,

        ControllerAction = 1000,
        ControllerException = 1001,
        JobFailure = 1100,
        ProcessorException = 1200,

        ReviewURLHarvestSucceeded = 2000,
        ReviewURLHarvestFailed = 2010,
        ReviewURLValidationSucceeded = 2020,
        ReviewURLValidationFailed = 2030,

        NotificationService = 2050,

        PublishActivity = 3000,

        ReviewStreamAction = 3100,

        TokenValidatorFailure = 3200

    }
}
