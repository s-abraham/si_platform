﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SI.DTO
{
    public class AuditReviewURLValidationInfo
    {
        public long AccountID { get; set; }
        public ReviewSourceEnum ReviewSource { get; set; }
        public string URL { get; set; }
        public List<string> Reasons { get; set; }
    }
}
