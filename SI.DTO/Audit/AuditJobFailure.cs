﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SI.DTO
{
    public class AuditJobFailure
    {
        public JobTypeEnum JobType { get; set; }
        public long JobID { get; set; }
        public string JobData { get; set; }
        public int RemainingChainedJobs { get; set; }
    }
}
