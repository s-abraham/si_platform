﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SI.DTO
{
    public class AuditUserActivity
    {
        public AuditUserActivity()
        {
        }

        public AuditUserActivity(UserInfoDTO userInfo, AuditUserActivityTypeEnum type)
        {
            UserID = userInfo.LoggedInUser.ID.Value;
            if (userInfo.ImpersonatingUser != null)
            {
                ImpersonatedUserID = userInfo.ImpersonatingUser.ID;
            }
            Type = type;
        }

        public long UserID { get; set; }
        public long? ImpersonatedUserID { get; set; }

        public AuditUserActivityTypeEnum Type { get; set; }

        public long? AccountID { get; set; }
        public long? UserIDActedUpon { get; set; }
        public long? RoleTypeID { get; set; }
        public long? ReviewSourceID { get; set; }
        public long? CredentialID { get; set; }
        public long? ObjectID { get; set; }

        public long? PostID { get; set; }
        public long? PostTargetID { get; set; }
        public long? PostImageResultID { get; set; }

        public long? ResellerID { get; set; }
        public long? SubscriptionID { get; set; }
        public long? PackageID { get; set; }

        public Guid? SSOToken { get; set; }
        public Guid? AccessToken { get; set; }

        public long? SyndicatorID { get; set; }
        public string Description { get; set; }
        public string UserAgent { get; set; }

        #region builder helpers

        public static AuditUserActivity New(UserInfoDTO userInfo, AuditUserActivityTypeEnum type)
        {
            return new AuditUserActivity(userInfo, type);
        }
        public AuditUserActivity SetType(AuditUserActivityTypeEnum type)
        {
            Type = type;
            return this;
        }
        public AuditUserActivity SetAccountID(long? accountID)
        {
            AccountID = accountID;
            return this;
        }
        public AuditUserActivity SetObjectID(long? objectID)
        {
            ObjectID = objectID;
            return this;
        }
        public AuditUserActivity SetPostID(long? objectID)
        {
            PostID = objectID;
            return this;
        }
        public AuditUserActivity SetPostTargetID(long? objectID)
        {
            PostTargetID = objectID;
            return this;
        }
        public AuditUserActivity SetPostImageResultID(long? objectID)
        {
            PostImageResultID = objectID;
            return this;
        }
        public AuditUserActivity SetPackageID(long? objectID)
        {
            PackageID = objectID;
            return this;
        }
        public AuditUserActivity SetResellerID(long? objectID)
        {
            ResellerID = objectID;
            return this;
        }
        public AuditUserActivity SetSubscriptionID(long? objectID)
        {
            SubscriptionID = objectID;
            return this;
        }
        public AuditUserActivity SetSyndicatorID(long? objectID)
        {
            SyndicatorID = objectID;
            return this;
        }
        public AuditUserActivity SetUserIDActedUpon(long? userID)
        {
            UserIDActedUpon = userID;
            return this;
        }
        public AuditUserActivity SetRoleTypeID(long? roleTypeID)
        {
            RoleTypeID = roleTypeID;
            return this;
        }
        public AuditUserActivity SetCredentialID(long? credentialID)
        {
            CredentialID = credentialID;
            return this;
        }
        public AuditUserActivity SetReviewSourceID(long? reviewSourceID)
        {
            ReviewSourceID = reviewSourceID;
            return this;
        }
        public AuditUserActivity SetSSOToken(Guid? ssoToken)
        {
            SSOToken = ssoToken;
            return this;
        }
        public AuditUserActivity SetDescription(string description)
        {
            Description = description;
            return this;
        }
        public AuditUserActivity SetDescription(string format, params object[] args)
        {
            Description = string.Format(format, args);
            return this;
        }
        public AuditUserActivity SetUserAgent(string userAgent)
        {
            UserAgent = userAgent;
            return this;
        }

        #endregion

    }

    /// <summary>
    /// Type of user activity to log.  Do NOT change enum text once used.
    /// </summary>
    public enum AuditUserActivityTypeEnum : long
    {
        SignedIn = 1,
        SignedOut = 2,

        ChangedPassword = 10,
        RequestedPasswordResetFromLogin = 11,
        UserInvalidPassword = 12,
        UserLocked = 13,

        CreatedNewUser = 20,
        ChangedExistingUser = 21,

        CreatedNewAccount = 30,
        ChangedExistingAccount = 31,

        SubAccountAdded = 32,
        SubAccountRemoved = 33,

        CompetitorAdded = 35,
        CompetitorRemoved = 36,

        ChangedContextAccount = 40,

        AddedSocialConnection = 50,
        UpdatedSocialConnection = 51,
        RemovedSocialConnection = 52,

        RoleRemoved = 60,
        RoleAdded = 61,

        AccountMembershipRemoved = 70,
        AccountMembershipAdded = 71,

        ReviewURLChanged = 80,

        MarkedItemAsRead = 90,
        MarkedItemAsResponded = 91,
        MarkedItemAsIgnored = 92,

        SavedPost = 100,
        SavedPostForSyndication = 101,
        SavedPostRevision = 102,
        SavedTargetRevision = 103,

        PostOpenedForRevision = 160,
        AttemptingToSavePostForRevision = 161,
        SavePostCalled = 162,

        PostTargetApproved = 104,
        PostTargetRejected = 105,
        PostTargetAutoApprovalAdded = 106,
        PostTargetAutoApprovalRemoved = 107,
        PostTargetDeleted = 108,

        PostTargetUndeleted = 150,
        PostTargetAdded = 151,

        PostImageResultDeleted = 109,

        SubscriptionAdded = 110,
        SubscriptionRemoved = 120,
        SubscriptionChanged = 130,

        AddAccountSocialNetworkAutoApproval = 131,
        RemoveAccountSocialNetworkAutoApproval = 132,

        SyndicationUnsubscribe = 133,

        AddedReportSubscription = 134,
        DisableReportSubscription = 135,

        SendGridBlockedEmailAddress = 170,
        SendGridBouncedEmailAddress = 171,
        SengGridMarkedEmailAddressInvalid = 172,

        DeletedSendGridBlockOnEmailAddress = 175,
        DeletedSendGridBounceOnEmailAddress = 176,
        DeletedSendGridInvalidMarkOnEmailAddress = 177
    }
}
