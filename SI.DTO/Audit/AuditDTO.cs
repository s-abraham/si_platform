﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SI.DTO
{
    public class AuditDTO
    {
        public long ID { get; set; }
        public AuditLevelEnum Level { get; set; }
        public AuditTypeEnum Type { get; set; }
        public string Message { get; set; }
        public string Info { get; set; }

        public Guid? ProcessBatch { get; set; }

        public SIDateTime DateCreated { get; set; }
        public SIDateTime DateProcessed { get; set; }

        public long UserID { get; set; }

        public long? ImpersonatedUserID { get; set; }
        public long? AccountID { get; set; }
        public long? CredentialID { get; set; }
        public long? Account_ReviewSourceID { get; set; }
        public long? ReviewID { get; set; }
    }
}
