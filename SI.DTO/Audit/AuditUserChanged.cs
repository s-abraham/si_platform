﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SI.DTO
{
    public class AuditUserChanged
    {
        public UserDTO userDTONew { get; set; }
        public UserDTO userDTOOld { get; set; }
    }
}
