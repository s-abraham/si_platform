﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SI.DTO
{
    public class AuditAccountChanged
    {
        public AccountDTO AccountOld { get; set; }
        public AccountDTO AccountNew { get; set; }
    }
}
