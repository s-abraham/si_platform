﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SI.DTO
{
    public class AuditAccountActivity
    {
        public AuditAccountActivity(long accountID, AccountActivityTypeEnum type)
        {
            AccountID = accountID;
            Type = type;
        }

        public AccountActivityTypeEnum Type { get; set; }
        public long AccountID { get; set; }
        public long? ResellerPackageID { get; set; }
        public long? PackageID { get; set; }
        public string Description { get; set; }
        public FeatureTypeEnum? FeatureType { get; set; }

        #region builder helpers

        public static AuditAccountActivity New(long accountID, AccountActivityTypeEnum type)
        {
            return new AuditAccountActivity(accountID, type);
        }
        public AuditAccountActivity SetResellerPackageID(long? value)
        {
            ResellerPackageID = value;
            return this;
        }
        public AuditAccountActivity SetPackageID(long? value)
        {
            PackageID = value;
            return this;
        }
        public AuditAccountActivity SetFeatureType(FeatureTypeEnum? value)
        {
            FeatureType = value;
            return this;
        }

        public AuditAccountActivity SetDescription(string description)
        {
            Description = description;
            return this;
        }
        public AuditAccountActivity SetDescription(string format, params object[] args)
        {
            Description = string.Format(format, args);
            return this;
        }

        #endregion
    }
}
