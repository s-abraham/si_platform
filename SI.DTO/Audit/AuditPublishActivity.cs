﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SI.DTO
{
    public class AuditPublishActivity
    {
        public long PostID { get; set; }
        public long? PostTargetID { get; set; }
        public long? PostImageID { get; set; }
        public long? AccountID { get; set; }
        public long? JobID { get; set; }
        public string Description { get; set; }
        public AuditPublishActivityTypeEnum Type { get; set; }

        public AuditPublishActivity(long postID, AuditPublishActivityTypeEnum type)
        {
            PostID = postID;
            Type = type;
        }

        public static AuditPublishActivity New(long postID, AuditPublishActivityTypeEnum type)
        {
            AuditPublishActivity obj = new AuditPublishActivity(postID, type);
            return obj;
        }

        public AuditPublishActivity SetPostID(long objectID)
        {
            PostID = objectID;
            return this;
        }
        public AuditPublishActivity SetPostTargetID(long? objectID)
        {
            PostTargetID = objectID;
            return this;
        }
        public AuditPublishActivity SetPostImageID(long? objectID)
        {
            PostImageID = objectID;
            return this;
        }
        public AuditPublishActivity SetJobID(long? objectID)
        {
            JobID = objectID;
            return this;
        }
        public AuditPublishActivity SetAccountID(long? accountID)
        {
            AccountID = accountID;
            return this;
        }
        public AuditPublishActivity SetDescription(string description)
        {
            Description = description;
            return this;
        }
        public AuditPublishActivity SetDescription(string format, params object[] args)
        {
            Description = string.Format(format, args);
            return this;
        }
    }

    public enum AuditPublishActivityTypeEnum : long
    {
        SavedNew = 1,
        SavedExisting = 20,
        PickedUpForQueuing = 30,

        PassedToPublishJobListQueuer = 40,
        PublishJobListQueuerLoadedTarget = 50,
        PublishJobListQueuerLoadedImage = 60,

        PublishJobListQueuerStartingJobQueueing = 70,
        PublishJobListQueuerSocialNetworkNotRepresented = 80,
        PublishJobListQueuerSocialNetworkRepresented = 90,

        PublishJobListQueuerAddingPublishJob = 100,
        PublishJobListQueuerAddingPublishImageJob = 110,
        PublishJobListQueuerAddingChainedJob = 120,

        PublishJobListQueuerSaveJobsFailure = 130,
        PublishJobListQueuerSavedJobs = 140,

        PublishProcessorEntry = 150,

        ApproversInitWarning = 160,

        // deletion related

        PostDeletionInitiated = 200,
        PostDeletionQueued = 201,
        PostDeletionFailedToQueue = 202,
        PostDeletionProcessing = 203,

        PostTargetDeletionQueued = 205,
        PostTargetDeletionProcessing = 206,

        CouldNotMatchPostTargetDeletionRecord = 210,
        MatchedPostTargetDeletionRecord = 211,

        DeletionSucceeded = 220,
        DeletionFailed = 221


    }
}
