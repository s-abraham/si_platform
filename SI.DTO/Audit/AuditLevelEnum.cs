﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SI.DTO
{
    public enum AuditLevelEnum : long
    {
        Information = 0,    // Basic informational message
        Warning = 25,       // Something we might be concerned about
        Error = 50,         // Something we found that should be of concern
        Exception = 75      // Unexpected system exception
    }
}
