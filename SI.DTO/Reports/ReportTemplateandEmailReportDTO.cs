﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SI.DTO
{
    public class ReportTemplateandEmailReportDTO
    {
        public ReportTemplateandEmailReportDTO()
        {
            emailReportTemplates = new List<EmailReportTemplateDTO>();
            emailedReportLogs = new List<EmailedReportLogDTO>();
        }

        public List<EmailReportTemplateDTO> emailReportTemplates { get; set; }
        public List<EmailedReportLogDTO> emailedReportLogs { get; set; }
    }

    public class EmailReportTemplateDTO
    {
        public long ResellerID { get; set; }
        public long ReportTemplateID { get; set; }
        public string ReportName { get; set; }
        public long? ReportSubscriptionID { get; set; }
        public string ScheduleSpec { get; set; }
        public bool? Status { get; set; }
    }

    public class EmailedReportLogDTO
    {
        public long ReportLogID { get; set; }        
        public string Subject { get; set; }
        public DateTime? DateSent { get; set; }
        public DateTime? DateFailed { get; set; }
    }

	public class EmailedReportLogDetailsDTO
	{
		public long ID { get; set; }
		public long SubscriptionID { get; set; }
		public string EmailAddress { get; set; }
		public string Subject { get; set; }
		public string Body { get; set; }
		public DateTime? DateSent { get; set; }
		public DateTime? DateFailed { get; set; }
	}
}
