﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SI.DTO
{
    public class CompAnalysisDataDTO
    {
        public CompAnalysisDataDTO()
        {
            CompetitorScores = new Dictionary<string, CompAnalysisSummaryGroupDTO>();
            Accounts = new List<SimpleAccountInfoDTO>();
        }

        public decimal MyScore { get; set; }
        public decimal CompetitorScore { get; set; }

        public CompAnalysisSummaryGroupDTO Main { get; set; }
        public CompAnalysisSummaryGroupDTO Group { get; set; }
        public CompAnalysisSummaryGroupDTO Comp { get; set; }

        public Dictionary<string, CompAnalysisSummaryGroupDTO> CompetitorScores { get; set; }

        public List<SimpleAccountInfoDTO> Accounts { get; set; }
    }

    public class CompAnalysisSummaryGroupDTO
    {
        public CompAnalysisSummaryDTO Current { get; set; }
        public CompAnalysisSummaryDTO Last24h { get; set; }
        public CompAnalysisSummaryDTO Last7Days { get; set; }
        public CompAnalysisSummaryDTO Last30Days { get; set; }
    }

    public class CompAnalysisSummaryDTO
    {
        public decimal Score { get; set; }
        public int ReviewCount { get; set; }
        public int Count { get; set; }
        public int CountPositive { get; set; }
        public int CountNegative { get; set; }
        public decimal PercentPositive { get; set; }
        public decimal PercentNegative { get; set; }
    }

}
