﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SI.DTO
{
	public class EnterpriseReputationSocialReportDTO
	{
		public long AccountID { get; set; }
		public string AccountName { get; set; }
		public int TotalReviews { get; set; }
		public int YTDGrowth { get; set; }
		public decimal YTDPercentGrowth { get; set; }
		public decimal PercentPositive { get; set; }
		public string PercentPostitiveColor { get; set; }
		public decimal Rating { get; set; }
		public decimal RatingPercentChange { get; set; }

		public int TotalFans { get; set; }
		public int YTDFanGrowth { get; set; }
		public decimal YTDPercentFanGrowth { get; set; }
		public int YTDEngagedUsers { get; set; }
		public int YTDFanImpressions { get; set; }
		public int YTDTotalImpressions { get; set; }
		public int YTDPaidPostImpressions { get; set; }

		public int TotalFollowers { get; set; }
		public int YTDFollowersGrowth { get; set; }
		public decimal YTDTwitterPercentGrowth { get; set; }

		public int TotalSubscribers { get; set; }
		public int YTDSubscribersGrowth { get; set; }

		public int TotalPlusOnes { get; set; }
		public int YTDPlusOnesGrowth { get; set; }


	}

	public class EnterpriseReputationSocialDetailReportDTO
	{
		public long AccountID { get; set; }
		public string AccountName { get; set; }
		public int TotalReviews { get; set; }
		public int PositiveReviews { get; set; }
		public int NegativeReviews { get; set; }
		public decimal Rating { get; set; }
		public int NewReview { get; set; }
		public int Fans { get; set; }
		public int NewFans { get; set; }
		public int EngagedUsers { get; set; }
		public int FanImpressions { get; set; }
		public int TotalImpressions { get; set; }
		public int PaidPostImpressions { get; set; }
		public int Followers { get; set; }
		public int SubScribers { get; set; }
		public int PlusOnes { get; set; }



	}
}
