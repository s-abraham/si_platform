﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SI.DTO
{
    public enum SendGridStatusEnum : long
    {        
	    HardBounced = 1,
	    Softbounced = 2,
	    Blocked = 3,
	    MarkedInvalid = 4
    }

    public enum SendGridProcessingStatusEnum : long
    {
        Queued = 1,
        Released = 2,
        Ignored = 3,
        DeleteError = 4
    }
}
