﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SI.DTO
{
    public enum PermissionTypeEnum : long
    {
        accounts_view = 17,
        accounts_admin = 13,

        products_view = 18,
        products_admin = 19,

        users_view = 20,
        users_impersonate = 25,
        users_admin = 14,

        reviews_view = 50,

        virtualGroups_create = 16,
        virtualGroups_admin = 15,

        social_admin = 60,

        // allows users to "create" content for publishing
        social_publish = 65,

        // allows users to "approve" content to be published
        social_approveposts = 66,

        social_view = 70,

        reseller_admin = 200,
        internal_admin = 300,
        system_admin = 10017
        

    }
}
