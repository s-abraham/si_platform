﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SI.DTO
{
    public enum GooglePlusProcessorActionEnum
    {
        GooglePlusPageInformationDownloder = 301,
        GooglePlusPublish = 302,
        GooglePlusPostStatisticsDownloder = 303,
        GooglePlusPageInformationIntegrator = 304,
        GooglePlusPostStatisticsIntegrator = 305,
        GooglePlusPostDownLoader = 306,
    }
}
