﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SI.DTO
{
    public enum PostStatusEnum
    {
        Scheduled = 0,
        Processing = 1,
        Published = 2,
        Failed = 3,
        WaitingOnApproval = 4,
        Draft = 5,
        Rejected = 6,
		SyndicationwithNoApproverHold = 7
    }
}
