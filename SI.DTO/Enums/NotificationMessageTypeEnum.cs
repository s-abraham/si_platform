﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SI.DTO
{
    public enum NotificationMessageTypeEnum : long
    {
        NewReviewAlert = 2,
        NewNegativeReviewAlert = 3,
        NegReviewNoResponse6h = 4,
        NegReviewNoResponse12h = 5,
        NegReviewNoResponse24h = 6,
        SyndicationSignupCC = 11,
        SocialAlert = 12,
        EnrollmentWelcomeEmail = 13,

        SyndicationPendingPostApproval = 7,
        NonSyndicationPendingPostApproval = 14,
        AutoApprovedNewContent=20,

        ReputationReportForPlatform = 15,
        SocialReportForPlatform = 16,
        EnterpriseReputationAndSocialReportForPlatform = 17,
        ReviewEmail = 18,
        TokenValidationReport = 19        
    }
}
