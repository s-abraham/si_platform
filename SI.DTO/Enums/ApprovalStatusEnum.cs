﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SI.DTO
{
    public enum ApprovalStatusEnum : int
    {
        All = 0,

        Pending = 10,

        Approved = 20,
        AutoApproved = 21,
        Rejectecd = 22

    }
}
