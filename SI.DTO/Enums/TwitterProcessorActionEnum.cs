﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SI.DTO
{
    public enum TwitterProcessorActionEnum
    {
        TwitterPageInformationDownloder = 201,
        TwitterPostStatisticsDownloder = 202,
        TwitterPublish = 203,
        TwitterPageInformationIntegrator = 204,
        TwitterPostStatisticsIntegrator = 205,

    }
}
