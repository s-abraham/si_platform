﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SI.DTO
{
    public enum ReviewSummaryDataSourceEnum : long
    {
        Scrape = 100,
        Backfill = 200,
        Gapfill = 300
    }
}
