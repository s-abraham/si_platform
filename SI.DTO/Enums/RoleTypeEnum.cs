﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SI.DTO
{
    public enum RoleTypeEnum : long
    {
        basic_admin = 1,

        basic_admincreator = 10,
        basic_adminapprover = 11,

        basic_readonly = 2,
        basic_engager = 3,

        basic_internal_admin = 4


    }
}
