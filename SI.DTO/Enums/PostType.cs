﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SI.DTO
{
    public enum PostType : int
    {
        Status = 1,
        Photo = 2,
        Link = 3,
        Event = 4,
        Video =5,
        Offer = 6,
        Question = 7
    }
}
