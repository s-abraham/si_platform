﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SI.DTO
{
    public enum FeatureTypeEnum : long
    {
        PaidProduct = 10,

        ReputationCore = 100,
        SocialCore = 200,
        
        // Syndication
        SyndicatorCore = 5000,
        SyndicateeCore = 5001,

        LexusSyndicator = 5100,
        LexusSyndicatee = 5101,

        CarfaxSyndicator = 5150,
        CarfaxSyndicatee = 5151,

        AutoConverseSyndicator = 6160,
        AutoConverseSyndicatee = 6161,

        AllySyndicator = 7160,
        AllySyndicatee = 7161	
    }
}
