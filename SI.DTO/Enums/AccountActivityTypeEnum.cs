﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SI.DTO
{
    public enum AccountActivityTypeEnum
    {
        FeatureAdded = 1,
        FeatureRemoved = 2,

        SubscribedToPackage = 10,
        PackageSubscriptionRemoved = 20,

        SignupMatched = 100,
        SignupCreated = 101,

        FacebookTokenCreated = 102,
        TwitterTokenCreated = 106,
        GooglePlusTokenCreated = 107,
        
        FacebookTokenInValid = 103,
        TwitterTokenInValid = 104,
        GooglePlusTokenInValid = 105,

        InvalidTokenEmailSent = 108,

        SocialDataBackFillDataQueued  = 109,
        SocialDataDeleteJobQueued = 110,

        UpdatedPostTargetForNewCredential = 111
    }
}
