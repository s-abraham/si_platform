﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SI.DTO
{
    public enum NotificationMethodEnum : long 
    {
        HtmlEmail = 10,
	    TextEmail = 20,
	    SMS = 30,
	    Platform = 40
    }
}
