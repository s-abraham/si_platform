﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SI.DTO
{
    public enum SocialNetworkEnum
    {
        Facebook = 1,
        Google = 2,
        Twitter = 3,
        YouTube = 4,
        Blogger = 7,
        WordPress = 8
    }
}
