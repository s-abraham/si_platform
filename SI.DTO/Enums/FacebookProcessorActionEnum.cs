﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SI.DTO
{
    public enum FacebookProcessorActionEnum
    {
        FacebookPageInsightsDownloader = 101,       
        FacebookPageInformationDownloader = 102,
        FacebookPublish = 103,
        FacebookPostStatisticsDownloader = 104,
		FacebookPageIntegrator = 105,
		FacebookPostDownLoader = 106,
		FacebookPostIntegrator = 107,
		FacebookPageInsightsIntegrator = 108,
		FacebookPostInsightsIntegrator = 109,
        FacebookPageInsightsDownloaderHistoric = 110,

        DeletePostsAndPageInfo = 111
    }
}
