﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SI.DTO
{
    public enum AccountTypeEnum : long
    {
        Grouping = 3,
        Company = 1,
        Unknown = 4,

        Person = 2
    }

    public class AccountTypesDTO
    {
        public long ID { get; set; }
        public string Name { get; set; }
    }
}
