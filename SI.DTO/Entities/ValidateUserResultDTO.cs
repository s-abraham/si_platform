﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SI.DTO
{
    public class ValidateUserResultDTO
    {
        public ValidateUserOutcome Outcome { get; set; }     
        public UserInfoDTO UserInfo { get; set; }
        public UserUIOptionsDTO UIOptions { get; set; }
    }

    public enum ValidateUserOutcome
    {
        UserNotFound,
        InvalidPassword,
        Success,
        AccountLocked
    }
}
