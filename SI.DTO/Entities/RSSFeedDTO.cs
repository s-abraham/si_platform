﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SI.DTO
{
    public class RSSFeedDTO
    {

        public long? ID { get; set; }
        public long VerticalID { get; set; }

        public bool IsValid { get; set; }
        public bool IsActive { get; set; }

        public string URL { get; set; }

        public SIDateTime LastAttempted { get; set; }
        public SIDateTime LastSuccessful { get; set; }

    }
}
