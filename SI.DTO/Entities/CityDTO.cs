﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SI.DTO
{
    public class CityDTO
    {
        public long ID { get; set; }
        public string Name { get; set; }

        public long StateID { get; set; }
        public long? ZipCodeID { get; set; }
        public long? LanguageID { get; set; }        
    }
}
