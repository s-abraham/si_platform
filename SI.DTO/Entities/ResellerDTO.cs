﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SI.DTO
{
    public class ResellerDTO
    {
        public ResellerDTO()
        {
            ID = null;
        }
        
        public long? ID { get; set; }
        public long AccountID { get; set; }
        public string ThemeName { get; set; }
        
        public SIDateTime DateCreated { get; set; }
    }
}
