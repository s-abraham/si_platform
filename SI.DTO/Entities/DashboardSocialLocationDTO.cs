﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SI.DTO
{
    public class DashboardSocialLocationDTO
    {
        public DashboardSocialLocationDTO()
        {
            DashboardSocialLocationSocialNetWork = new List<DashboardSocialLocationSocialNetWorkDTO>();
        }
        public long AccountID { get; set; }
        public string AccountName { get; set; }
        public string StreetName { get; set; }
        public string CityName { get; set; }
        public string StateAbbrev { get; set; }
        public string StateLong { get; set; }
        public string ZipCode { get; set; }

        public List<DashboardSocialLocationSocialNetWorkDTO> DashboardSocialLocationSocialNetWork { get; set; }

        public int FacebookPosts { get; set; }
        public int FacebookPostComments { get; set; }
        public int FacebookPostLikes { get; set; }
        public int FacebookShares { get; set; }
        public int GooglePlusPosts { get; set; }
        public int GooglePlusComments { get; set; }
        public int GooglePlusPlusoners { get; set; }
        public int GooglePlusResharers { get; set; }
        public int TwitterPosts { get; set; }
        public int TwitterPostRetweetCount { get; set; }

        public SIDateTime LastPostDateTime { get; set; }
        
    }

    public class DashboardSocialLocationSocialNetWorkDTO
    {
        public long CredentialID { get; set; }
        public string UniqueID { get; set; }        
        public string PictureURL { get; set; }
        public string URI { get; set; }
        public SocialNetworkEnum SocialNetwork { get; set; }
        public bool IsValid { get; set; }
		public bool IsActive { get; set; }

    }
}
