﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SI.DTO
{
	public class SocialConnectionStatusDTO
	{
		public SocialNetworkEnum Source { get; set; }
		public SocialConnectionStatusEnum Status { get; set; }

	}
}
