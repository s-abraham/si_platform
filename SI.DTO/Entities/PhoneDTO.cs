﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SI.DTO
{
    public class PhoneDTO
    {

        public PhoneDTO()
        {
            ID = null;
        }

        public long? ID { get; set; }
        public string Number { get; set; }

    }
}
