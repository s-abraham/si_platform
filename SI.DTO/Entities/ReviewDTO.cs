﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SI.DTO
{
    public class ReviewDTO
    {
        public long? ID { get; set; }
        public long AccountID { get; set; }
        public ReviewSourceEnum ReviewSource { get; set; }

        public decimal Rating { get; set; }
        public string ReviewerName { get; set; }
        public string ReviewText { get; set; }
        public string ReviewURL { get; set; }

        public string ExtraInfo { get; set; }

        public SIDateTime ReviewDate { get; set; }

        public bool Filtered { get; set; }

        public SIDateTime DateEntered { get; set; }
        public SIDateTime DateUpdated { get; set; }
    }
}
