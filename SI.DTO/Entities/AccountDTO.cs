﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace SI.DTO
{
    public class AccountDTO
    {

        public AccountDTO()
        {
            ID = null;
        }

        public long? ID { get; set; }

        public long AccountTypeID { get; set; }
        public AccountTypeEnum Type { get; set; }

        public bool? IsDemoAccount { get; set; }

        public int AccountDepth { get; set; }

        public long? CRMSystemTypeID { get; set; }
		
		[DataType(DataType.EmailAddress)]
		[RegularExpression(@"[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.[A-Za-z]{2,4}", ErrorMessage = "Email is not valid.")]
		public string CRMSystemEmail { get; set; }

        public long? ParentAccountID { get; set; }
        public string ParentAccountName { get; set; }

		[Required]
        public string Name { get; set; }
		
		[Display(Name = "Display Name")]
        public string DisplayName { get; set; }

		[Required]
		[Display(Name = "Email Address")]
		[DataType(DataType.EmailAddress)]
		[RegularExpression(@"[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.[A-Za-z]{2,4}", ErrorMessage = "Email is not valid.")]
        public string EmailAddress { get; set; }

		[DataType(DataType.EmailAddress)]
		[RegularExpression(@"[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.[A-Za-z]{2,4}", ErrorMessage = "Email is not valid.")]
        public string LeadsEmailAddress { get; set; }

		[Required]
		[Display(Name = "Website URL")]
		[DataType(DataType.Url)]
		[RegularExpression(@"((https?)://|(www)\.)[a-z0-9-]+(\.[a-z0-9-]+)+([/?].*)?$", ErrorMessage = "Not a valid URL")]
        public string WebsiteURL { get; set; }

		[Required]
        public string StreetAddress1 { get; set; }
        public string StreetAddress2 { get; set; }

        public long? CountryID { get; set; }
        public string CountryName { get; set; }

        public long? StateID { get; set; }
        public string StateName { get; set; }
        public string StateAbbreviation { get; set; }

        public long? CityID { get; set; }
		[Required]
        public string CityName { get; set; }
		
        public string PostalCode { get; set; }

		[Required]
		[Display(Name = "Phone Number")]
		[DataType(DataType.PhoneNumber)]
		[RegularExpression(@"([0-9\(\)\/\+ \-]*)$", ErrorMessage = "Not a valid Phone Number")]
        public string PhoneNumber { get; set; }

		public long[] FranchiseTypes { get; set; }
		public long? VerticalID { get; set; }

        public SIDateTime DateCreated { get; set; }
        public SIDateTime DateModified { get; set; }

        public long TimezoneID { get; set; }

        /// <summary>
        /// Any post made to targets belinging to this account will require approval
        /// </summary>
        public bool PostsRequireApproval { get; set; }

        // source specific fields
        public long? EdmundsDealershipID { get; set; }

        public string OriginSystemIdentifier { get; set; }

        public ResellerDTO ResellerForThisAccount { get; set; }

    }

	
}