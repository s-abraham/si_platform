﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SI.DTO
{
    public class PermissionDTO
    {
        public long ID { get; set; }
        public long AccountID { get; set; }
        public long UserID { get; set; }
        public long PermissionTypeID { get; set; }
        public long? InheritiedFromAccountID { get; set; }
    }
}
