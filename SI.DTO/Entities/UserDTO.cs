﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SI.DTO
{
	public class UserDTO
	{
		public UserDTO()
		{
			ID = null;
            Syndicators = new List<string>();
		}

		public long? ID { get; set; }
		public long RootAccountID { get; set; }
		public long MemberOfAccountID { get; set; }

		public bool SuperAdmin { get; set; }
		public string Username { get; set; }
		public string Password { get; set; }
		public string EmailAddress { get; set; }

		public string FirstName { get; set; }
		public string LastName { get; set; }

		public SIDateTime CreateDate { get; set; }
		public bool CascadeRole { get; set; }
		public long? RoleTypeID { get; set; }
		public string RoleTypeName { get; set; }

		public string AccountName { get; set; }
		public int NotificationCount { get; set; }

		public long? TimezoneID { get; set; }

		public ResellerDTO Reseller { get; set; }

        public int InvalidLoginAttempts { get; set; }
		public bool AccountLocked { get; set; }

        public Guid SSOSecretKey { get; set; }

        // the current selected context account
        public long CurrentContextAccountID { get; set; }

        public bool HasMultipleAccountAccess { get; set; }
        public bool HasMultipleAccountsWithPaidProducts { get; set; }
        public bool CurrentAccountHasChildren { get; set; }

		//features on/off
		
		public bool CanReportsMenu { get; set; }
		
		//public bool CanSystemOpsMenu { get; set; }

		public bool CanDealerPostActivity { get; set; }
		public bool CanPublish { get; set; }
		public bool CanSocialLocation { get; set; }
        public bool CanSocialDashboard { get; set; }
		public bool CanApprovalHistory { get; set; }
		
		public bool CanDashboardOverviewBasic { get; set; }
        public bool ViewDashboardOverviewReputation { get; set; }
        public bool ViewDashboardOverviewSocial { get; set; }
        public bool CanViewNotificationGlobe { get; set; }
		
		public bool CanDashboardReputationLocation { get; set; }

		
		public bool CanReputationStream { get; set; }
		public bool CanReputationSites { get; set; }
		public bool CanCompetitiveAnalysis { get; set; }

		public bool CanReportSAM { get; set; }
		//public bool CanReportUsers { get; set; }
        public bool CanMenuReportsPostPerformance { get; set; }
        public bool CanMenuReportsPostStream { get; set; }
        public bool CanMenuReportPostSummary { get; set; }

        public bool CanViewReputationReports { get; set; }
        public bool CanViewSocialReports { get; set; }
        public bool CanViewSyndicatorReports { get; set; }
        public bool CanViewSyndicateeReports { get; set; }


		public bool CanAdminAccount { get; set; }
		public bool CanAdminUsers { get; set; }
		public bool CanAdminVirtualGroups { get; set; }

		public bool CanAdminSystem { get; set; }
		public bool CanAdminExceptions { get; set; }
		public bool CanAdminFlushCache { get; set; }

		public bool CanSupportCenter { get; set; }
		public bool CanSupportCenterThemes { get; set; }
		public bool CanSupportCenterPackages { get; set; }
		public bool CanSupportCenterEmployees { get; set; }
		public bool CanSupportCenterPlatformMessages { get; set; }
		public bool CanSupportCenterSingleSignOn { get; set; }
		public bool CanSupportCenterAccountManagers { get; set; }

		

		public bool ViewDashboardOverviewReputationGrid { get; set; }
		public bool ViewDashboardOverviewSocialGrid { get; set; }
		public bool ViewReviewStreamCountPerSite { get; set; }
		public bool ViewReviewStreamPositiveNegative { get; set; }
		public bool ViewReviewStreamGrid { get; set; }
		public bool ViewReviewStreamFilter { get; set; }
		public bool ViewReviewStreamGridAction { get; set; }

		public bool CanReviewStreamRespondedAction { get; set; }
		public bool CanReviewStreamMarkReadAction { get; set; }
		public bool CanReviewStreamEmailAction { get; set; }
		public bool CanReviewStreamShareAction { get; set; }
		public bool CanReviewStreamIgnoreAction { get; set; }

		public bool ViewReviewSiteGrid { get; set; }
		public bool CanDealerPostActivityCompose { get; set; }
		public bool CanPostManagementCompose { get; set; }
		public bool ViewDealerPostActivityGridFilter { get; set; }
		public bool ViewDealerPostActivityGrid { get; set; }
		public bool ViewPostManagementGrid { get; set; }

		public bool CanPublishDataInserts { get; set; }
		public bool ViewPublishAccountsFilter { get; set; }
		public bool CanPublishSendNow { get; set; }
		public bool CanPublishSchedulePost { get; set; }
		public bool CanPublishSyndicationOptions { get; set; }
		public bool CanPublishSyndicate { get; set; }
		public bool AllowSyndicationWithoutApproval { get; set; }
		public bool CanSocialStream { get; set; }
        public bool AllowSyndicationEdit { get; set; }
        public bool CanSocialPostManagement { get; set; }


		public bool ViewAdminAccountsFilter { get; set; }
        public bool ViewAdminAccountPackagesFilter { get; set; }
		public bool ViewAdminAccountsGrid { get; set; }
		public bool CanAdminAccountEdit { get; set; }

		public bool ViewAccountPicker { get; set; }
		public bool ViewAccountPickerNotifications { get; set; }
		public bool CanPreferencesMenu { get; set; }
		public bool CanChangePasswordMenu { get; set; }
        
        public bool CanProductConfigurationMenu { get; set; }
		public bool CanMessageSupportMenu { get; set; }
		public bool CanNotificationsMenu { get; set; }
        public bool CanSyndicationHelpPage { get; set; }

        public List<string> Syndicators { get; set; }


		public bool ViewUserPreferencesNotifications { get; set; }
        public bool EnableUserPreferencesNotificationsReputation { get; set; }
        public bool EnableUserPreferencesNotificationsSocial { get; set; }

        public bool CanViewAdminReports { get; set; }
        public bool CanMenuSocialSnapshot { get; set; }
        public bool CanMenuReputationSnapshot { get; set; }

    

        public SIDateTime AccountDeactivatedDate { get; set; }
        public SIDateTime DateModified { get; set; }
		public SIDateTime CurrentDateTime { get; set; }
		
	}
}