﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SI.DTO
{
    public class CountryDTO
    {
        public long ID { get; set; }
        public string Name { get; set; }
        public string Prefix { get; set; }

        public long? LanguageID { get; set; }
    }
}
