﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SI.DTO
{
    public enum PhoneTypeDTO : int
    {
        Home = 1,
        Work = 2,
        Mobile = 3
    }
}
