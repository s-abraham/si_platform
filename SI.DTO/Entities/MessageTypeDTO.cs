﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SI.DTO
{
    public class MessageTypeDTO
    {
        public int MessageTypeId { get; set; }
        public int MessageCategoryId { get; set; }
        public string MessageTypeName { get; set; }
        public int DisplayOrder { get; set; }
        public string MessageTemplate { get; set; }
        public string Description { get; set; }
    }
}
