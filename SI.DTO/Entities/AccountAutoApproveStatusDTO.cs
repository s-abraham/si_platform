﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SI.DTO
{
    public class AccountAutoApproveStatusDTO
    {
        public long SyndicatorID { get; set; }        
        public string AccountName { get; set; }
        public long AccountID { get; set; }        
        public string SyndicatorName { get; set; }
        public SocialNetworkEnum SocialNetwork { get; set; }
        public string SocialNetworkName { get; set; }
        public long? PostAutoApprovalID { get; set; }
        public bool AutoApproveStatus { get; set; }
        public SIDateTime DateCreated { get; set; }        
    }
}
