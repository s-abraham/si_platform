﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SI.DTO
{
    public class PackageDTO
    {
        public long ID { get; set; }
        public long PackageID { get; set; }
        public long ResellerID { get; set; }

        public string ResellerName { get; set; }
        public string PackageName { get; set; }        
        public SIDateTime DateCreated { get; set; }
    }
}
