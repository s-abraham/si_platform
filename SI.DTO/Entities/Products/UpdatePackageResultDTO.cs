﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SI.DTO
{
    public class UpdatePackageResultDTO
    {
        public UpdatePackageResultDTO()
        {
            Problems = new List<string>();
        }
        public UpdatePackageOutcome Outcome { get; set; }
        public List<string> Problems { get; set; }
    }

    public enum UpdatePackageOutcome
    {
        Success = 0,
        Failure = 1
    }
}
