﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SI.DTO
{
    public class AccountPackageListItemDTO
    {
        public long SubscriptionID { get; set; }
        public long AccountID { get; set; }

        public long ResellerID { get; set; }
        public string ResellerName { get; set; }

        public long PackageID { get; set; }
        public string PackageName { get; set; }

        public SIDateTime DateAdded { get; set; }
    }
}
