﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SI.DTO
{
    public class UserInfoDTO
    {
        public UserInfoDTO(long loggedInUserID, long? impersonatingUser)
        {
            LoggedInUser = new UserDTO() { ID = loggedInUserID };
            if (impersonatingUser.HasValue)
                ImpersonatingUser = new UserDTO() { ID = impersonatingUser };
            else
                ImpersonatingUser = null;
        }
        public UserInfoDTO(UserDTO loggedInUser, UserDTO impersonatingUser)
        {
            LoggedInUser = loggedInUser;
            ImpersonatingUser = impersonatingUser;
        }

        public UserDTO LoggedInUser { get; set; }
        public UserDTO ImpersonatingUser { get; set; }
        public long? MemberOfPrimaryResellerID { get; set; }

        public string ToAuthCookie()
        {
            if (ImpersonatingUser == null)
                return LoggedInUser.ID.Value.ToString(CultureInfo.InvariantCulture);
            else
                return string.Format("{0}|{1}", LoggedInUser.ID.Value.ToString(CultureInfo.InvariantCulture), ImpersonatingUser.ID.Value.ToString(CultureInfo.InvariantCulture));
        }

        public UserDTO EffectiveUser
        {
            get
            {
                if (ImpersonatingUser != null)
                    return ImpersonatingUser;
                return LoggedInUser;
            }
        }

        public static UserInfoDTO System
        {
            get
            {
                UserInfoDTO dto = new UserInfoDTO(1, null);
                dto.EffectiveUser.CurrentContextAccountID = 197922; //social dealer is default context
                return dto;
            }
        }
    }
}
