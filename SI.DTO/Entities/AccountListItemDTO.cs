﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SI.DTO
{
    public class AccountListItemDTO
    {
        public AccountListItemDTO()
        {
            ConnectedSocialNetworks = new List<SocialNetworkEnum>();
        }

        public int RowNum { get; set; }

        public long ID { get; set; }

        public string Name { get; set; }
        
        public long? ParentAccountID { get; set; }
        public string ParentAccountName { get; set; }

        public long? EdmundsDealershipID { get; set; }

        public string Address { get; set; }
        public string City { get; set; }

        public string State { get; set; }
        public string ZipCode { get; set; }

        
        public string Type { get; set; }

        public int UserCount { get; set; }
        public int VirtualGroupCount { get; set; }

        public string FranchiseTypes { get; set; }

        public DateTime DateCreatedUTC { get; set; }
        public SIDateTime DateCreated { get; set; }

        public string OriginSystemIdentifier { get; set; }
        
        public int AlertCount { get; set; }

        public long? ResellerID { get; set; }

        public long? ThisAccountsResellerID { get; set; }
        public string ThisAccountsResellerName { get; set; }

        public int NotificationCount { get; set; }
        public int RolledNotificationCount { get; set; }

        public List<SocialNetworkEnum> ConnectedSocialNetworks { get; set; }
        
    }

    
}
