﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SI.DTO
{
    public class ReviewSummaryDTO
    {
        public ReviewSummaryDataSourceEnum ReviewSummaryDataSource { get; set; }

        public long AccountID { get; set; }
        public long ReviewSourceID { get; set; }

        public SIDateTime SummaryDate { get; set; }

        public double SalesAverageRating { get; set; }
        public double ServiceAverageRating { get; set; }

        public int SalesReviewCount { get; set; }
        public int ServiceReviewCount { get; set; }

        public int NewReviewsCount { get; set; }
        public int NewReviewsPositiveCount { get; set; }
        public int NewReviewsNegativeCount { get; set; }
        public double NewReviewsAverageRating { get; set; }
        public double NewReviewsRatingSum { get; set; }

        public int TotalPositiveReviews { get; set; }
        public int TotalNegativeReviews { get; set; }

        public bool HasBefore { get; set; }
        public bool HasAfter { get; set; }
    }
}
