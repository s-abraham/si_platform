﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SI.DTO
{
    public class ZipCodeDTO
    {
        public long ID { get; set; }
        public string ZipCode { get; set; }
        public long StateID { get; set; }

        public double? Latitude { get; set; }
        public double? Longitude { get; set; }
    }
}
