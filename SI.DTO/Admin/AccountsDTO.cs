﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SI.DTO
{
	public class AccountListDTO
	{
		public long ID { get; set; }
		public string Name { get; set; }
		public string ParentAccountName { get; set; }
		public string Address { get; set; }
		public string City { get; set; }
		public string State { get; set; }
		public string ZipCode { get; set; }
		public string Type { get; set; }
		public SIDateTime DateCreated { get; set; }

		public int OemID { get; set; }
		public int PackageID { get; set; } //not sure what to do with this yet, if many etc.
	}
	
	public class CompetitorDTO
	{
		public long ID { get; set; }
		public string Name { get; set; }
		public string City { get; set; }
		public string State { get; set; }
		public string ZipCode { get; set; }
		public long AccountID { get; set; }
	}

	
	
}
