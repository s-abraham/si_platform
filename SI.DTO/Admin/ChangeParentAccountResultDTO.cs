﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SI.DTO
{
    public class ChangeParentAccountResultDTO
    {
        public ChangeParentAccountResultDTO()
        {
            Problems = new List<string>();
        }
        public bool Success { get; set; }
        public List<string> Problems { get; set; }
    }
}
