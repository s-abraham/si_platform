﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SI.DTO
{
	public class ExceptionLogDTO
	{
		public long ID { get; set; }
		public string AssemblyName { get; set; }
		public string Class { get; set; }
		public string Method { get; set; }
		public int LineNumber { get; set; }
		public string Message { get; set; }
		public string URL { get; set; }
		public string FullException { get; set; }
		public DateTime DateCreated { get; set; }

	}
}
