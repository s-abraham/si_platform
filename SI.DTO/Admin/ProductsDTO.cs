﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SI.DTO
{
	public class ReputationConfigLinkDTO
	{
		public long ID { get; set; }
		public string Name { get; set; }
		public string Url { get; set; }
		public ReviewSourceEnum ReviewSource { get; set; }
		public bool IsValid { get; set; }
        public ReviewUrlDTOEnum Status { get; set; }
	}
	
}
