﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SI.DTO
{
    public class UserNotificationTypeInfoDTO
    {
        /// <summary>
        /// The ID of the user
        /// </summary>
        public long UserID { get; set; }

        /// <summary>
        /// The notification message type.
        /// </summary>
        public NotificationMessageTypeEnum Type { get; set; }
        
        /// <summary>
        /// The notification method, HTMLEmail or Platform
        /// </summary>
        public NotificationMethodEnum Method { get; set; }

        /// <summary>
        /// The text name of the notification message type
        /// </summary>
        public string TypeName { get; set; }

        /// <summary>
        /// True if the user can subscribe to this notification type for at least one account
        /// </summary>
        public bool CanSubscribe { get; set; }

        /// <summary>
        /// The number of accounts that the user is subscribed to receive this notification for
        /// </summary>
        public int AccountsSubscribedTo { get; set; }

    }
}
