﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SI.DTO
{
    public class EmailNotificationDTO
    {
        public int NotificationID { get; set; }
        public string SendFrom { get; set; }
        public SendToDTO SendTo { get; set; }
        public SendCCDTO SendCC { get; set; }
        public SendBCCDTO SendBCC { get; set; }
        public int MessageTypeID { get; set; }
        public int MessageCategoryID { get; set; }
        public int NotificationTransmissionID { get; set; }
        public string Subject { get; set; }
        public string Body { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime? ProcessedDate { get; set; }
        public long? AccountID { get; set; }
        public DateTime ScheduledDate { get; set; }
        //public NotificationStatus StatusID { get; set; }
    }
}
