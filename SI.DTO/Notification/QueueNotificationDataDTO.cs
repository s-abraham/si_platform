﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SI.DTO
{
    /// <summary>
    /// This class holds information for a single notifiable event. 
    /// </summary>
    public class QueueNotificationDataDTO
    {
        /// <summary>
        /// The type of notification to queue
        /// </summary>
        public NotificationMessageTypeEnum Type { get; set; }

        /// <summary>
        /// The ID of the entity which the event is related
        /// </summary>
        public long EntityID { get; set; }

        /// <summary>
        /// The account id related to the event.  It is from this account id that 
        /// subscriptions are determined
        /// </summary>
        public long AccountID { get; set; }
                
    }

}
