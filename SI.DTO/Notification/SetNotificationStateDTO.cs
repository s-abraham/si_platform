﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SI.DTO
{   
    public class SetNotificationStateDTO
    {
        /// <summary>
        /// The notification message type.
        /// </summary>
        public NotificationMessageTypeEnum Type { get; set; }

        /// <summary>
        /// The notification method, HTMLEmail or Platform
        /// </summary>
        public NotificationMethodEnum Method { get; set; }

        /// <summary>
        /// If true, will set the given notification message type to be enabled, which in practice means that 
        /// any exclusions for the given type and method will be deleted.
        /// </summary>
        public bool Enabled { get; set; }
    }
}
