﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SI.DTO
{
    public class SendCCDTO
    {
        public SendCCDTO()
        {
            sendCC = new List<string>();
        }
        public List<string> sendCC { get; set; }
    }
}
