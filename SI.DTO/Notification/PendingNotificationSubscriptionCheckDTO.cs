﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SI.DTO
{
    public class PendingNotificationSubscriptionCheckDTO
    {
        public long? AccountID { get; set; }
        public long? UserID { get; set; }
    }
}
