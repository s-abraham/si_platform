﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SI.DTO
{
    public class SocialCommnetsNotificationDTO
    {
        public long PostID { get; set; }
		public long AccountID { get; set; }
		public string PostMessage { get; set; }
        public long PostTypeID { get; set; }
		public string LinkURL { get; set; }
		public string PictureURL { get; set; }
		public string ResultID { get; set; }
		public DateTime PostCreateTime { get; set; }
        public long FaceBookCommentID { get; set; }
		public string CommentID { get; set; }
		public string CommnetMessage { get; set; }
		public DateTime CommentCreateTime { get; set; }
        public int CommentLikesCount { get; set; }
		public string CommentorName { get; set; }
		public string CommentorFacebookID { get; set; }
		public string CommentorPictureURL { get; set; }
        public long CommentsCount { get; set; }
        public long LikesCount { get; set; }
        public long SharesCount { get; set; }
    }
}
