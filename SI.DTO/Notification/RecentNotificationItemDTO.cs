﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SI.DTO
{
    public class RecentNotificationItemDTO
    {
        /// <summary>
        /// The NotificationTarget ID that identified the email notification sent out
        /// </summary>
        public long NotificationTargetID { get; set; }

        /// <summary>
        /// The subject of the email that was sent
        /// </summary>
        public string Subject { get; set; }

        /// <summary>
        /// The notification message type.
        /// </summary>
        public NotificationMessageTypeEnum Type { get; set; }

        /// <summary>
        /// The text name of the notification message type
        /// </summary>
        public string TypeName { get; set; }

        /// <summary>
        /// The date that the notification was sent
        /// </summary>
        public SIDateTime DateSent { get; set; }
    }
}
