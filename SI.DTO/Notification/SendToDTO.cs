﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SI.DTO
{
    public class SendToDTO
    {
        public SendToDTO()
        {
            sendTo = new List<string>();
        }
        public List<string> sendTo { get; set; }
    }
}
