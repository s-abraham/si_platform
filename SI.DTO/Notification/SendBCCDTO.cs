﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SI.DTO
{
    public class SendBCCDTO
    {
        public SendBCCDTO()
        {
            sendBCC = new List<string>();
        }
        public List<string> sendBCC { get; set; }
    }
}
