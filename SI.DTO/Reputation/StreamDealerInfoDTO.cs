﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SI.DTO
{
    public class StreamDealerInfoDTO
    {
        public int TotalDealer { get; set; }
        public int ReviewCount { get; set; }
        public int TotalPositiveReviews { get; set; }
        public int TotalNegativeReviews { get; set; }

    }
}
