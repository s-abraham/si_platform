﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace SI.DTO
{
	public class StreamDTO
	{
        public long ReviewID { get; set; }
		public SIDateTime ReviewDate { get; set; }
		public SIDateTime CollectedDate { get; set; }
		public Location DealerLocation { get; set; }
		public long SiteID { get; set; }
		public string SiteName { get; set; }
		public string SiteImageUrl { get; set; }
		public double Rating { get; set; }
		public string ReviewerName { get; set; }
		public string ReviewText { get; set; }
		public string ReviewUrl { get; set; }
		
        //public bool HasBeenRead { get; set; }
        //public bool HasBeenResponded { get; set; }
        //public bool IsIgnored { get; set; }
		
        public bool IsFiltered { get; set; }
		
        public string Category { get; set; }

        public SIDateTime ReadDate { get; set; }
        public SIDateTime RespondedDate { get; set; }
        public SIDateTime IgnoredDate { get; set; }


		public StreamDTO()
		{
			DealerLocation = new Location();
		}

		public class Location
		{
			public long LocationID { get; set; }
			public string Name { get; set; }
			public string City { get; set; }
			public string StateAbrev { get; set; }
			public long StateID { get; set; }
			public string Zipcode { get; set; }
			//public int OemID { get; set; }
			public string SiteUrl { get; set; }
		}

	}


   
}
