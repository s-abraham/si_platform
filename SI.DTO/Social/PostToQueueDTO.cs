﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SI.DTO
{
    public class PostToQueueDTO
    {
        public long PostID { get; set; }
        public long AccountID { get; set; }
    }
}
