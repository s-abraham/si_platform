﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SI.DTO
{
	public class SocialCredentialDTO
	{
		public long ID { get; set; }
        public long AccountID { get; set; }
        public long SocialAppID { get; set; }
        public string UniqueID { get; set; }
        public string Token { get; set; }
        public string TokenSecret { get; set; }
        public bool IsValid { get; set; }
        public bool IsActive { get; set; }

        public int AppVersion { get; set; }
        public string AppID { get; set; }
        public string CallbackURL { get; set; }
        public string AppSecret { get; set; }
        public bool IsSocialAppActive { get; set; }

		public long SocialNetworkID { get; set; }

        public string ScreenName { get; set; }
        public string PictureURL { get; set; }
        
		public string SocialNetworkName { get; set; }
		public string PageUrl { get; set; }
        
		public DateTime? DateLastVerified { get; set; }

        public string AppToken { get; set; }

        public long? CreatedByUserID { get; set; }
	}

	public class SocialNetworkDTO
	{
		public long ID { get; set; }
		public string Name { get; set; }
	}

    public class SyndicatorsDTO
    {
        public long ID { get; set; }
        public string Name { get; set; }
    }

	public class SocialRefreshDTO
	{
		public long CredentialID { get; set; }
		public string NetworkName { get; set; }
		public string AppID { get; set; }
		public string AppSecret { get; set; }
		public string CallbackURL { get; set; }
		public string RefreshToken { get; set; }
		public string TokenSecret { get; set; }
		public string UniqueID { get; set; }
	}
}
