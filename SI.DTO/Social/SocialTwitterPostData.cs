﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SI.DTO
{
    public class SocialTwitterPostData
    {
        public long AccountID { get; set; }
        public string AccountName { get; set; }     
        public long PostTargetID { get; set; }
        public DateTime StatsDate { get; set; }
        public DateTime PublishedDate { get; set; }
        public bool IsFavorited { get; set; }
        public bool IsRetweeted { get; set; }
        public int RetweetCount { get; set; }
        public string ImageURL { get; set; }
        public string Link { get; set; }
        public string Message { get; set; }
        public PostTypeEnum PostTypeID { get; set; }                
    }
}
