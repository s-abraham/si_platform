﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SI.DTO
{
	public class LinkPreviewDTO
	{
		public string PageTitle { get; set; }
		public string PageDescription { get; set; }
		public List<string> PageImages { get; set; }
		public string PageUrl { get; set; }
		public bool IsValidUrl { get; set; }

		public LinkPreviewDTO()
		{
			PageImages = new List<string>();
		}
	}
}
