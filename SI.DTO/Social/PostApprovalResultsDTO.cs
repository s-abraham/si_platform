﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SI.DTO
{
   public class   PostApprovalResultsDTO : SearchResultBaseDTO
   {
       public PostApprovalResultsDTO()
        {
            Items = new List<PostApprovalResult>();
        }

       public List<PostApprovalResult> Items { get; set; }
   }

    public class PostApprovalResult 
    {
        public PostApprovalResult()
        {
            AllowEdit = false;
            AllowDeny = false;
            AllowApprove = false;
            AllowUnApprove= false;
            AllowUnDeny = false;
        }

        public long PostTargetID { get; set; }

        public ApprovalStatusEnum ApprovalStatus { get; set; }
        public string Status
        {
            get
            {
                switch (ApprovalStatus)
                {
                    case ApprovalStatusEnum.All:
                        return "";
                    case ApprovalStatusEnum.Pending:
                        return "Pending";
                    case ApprovalStatusEnum.Approved:
                        return "Approved";
                    case ApprovalStatusEnum.AutoApproved:
                        return "Auto-Approved";
                    case ApprovalStatusEnum.Rejectecd:
                        return "Rejected";
                    default:
                        return "";
                }
            }
        }
                
        public SIDateTime ScheduleDate { get; set; }
        public SocialNetworkEnum SocialNetwork { get; set; }
        public string SocialNetworkName { get; set; }
        public string SocialNetworkPictureURL { get; set; }
        public string SocialNetworkUniqueID { get; set; }

        public long AccountID { get; set; }
        public string AccountName { get; set; }        
        public string ScreenName { get; set; }
                
        public bool CredentialIsActive { get; set; }
        public bool CredentialIsValid { get; set; }
                
        public string SocialNetworkURL { get; set; }

        public long? SyndicatorID { get; set; }
        public string SyndicatorName { get; set; }

        public long PostID { get; set; }
        public long PostTypeID { get; set; }

        public string Message { get; set; }
        public string LinkTitle { get; set; }
        public string LinkDescription { get; set; }
        public string Link { get; set; }
        public string LinkImageURL { get; set; }

        public long? ApprovedByUserID { get; set; }
        public string ApprovedByUserName { get; set; }
        public string ApprovedByUserEmail { get; set; }

        public int ImagesAttempted { get; set; }
        public int ImagesFailed { get; set; }
        public int ImagesSucceeeded { get; set; }
        public int ImagesDeleted { get; set; }

        public string TargetResultID { get; set; }
        public bool? PostTargetDeleted { get; set; }

        public SIDateTime PickedUpDate { get; set; }
                
        public SIDateTime PublishedDate { get; set; }
        public SIDateTime FailedDate { get; set; }
        public SIDateTime NoApproverHoldDate { get; set; }
        public long PostApprovalID { get; set; }

        public SIDateTime DateApproved { get; set; }
        public SIDateTime DateRejected { get; set; }
        public bool AutoApproved { get; set; }
        
        public string Title { get; set; }
        
        public long PostedByUserID { get; set; }
        public string PostedByUserName { get; set; }
        public SIDateTime PostCreationDate { get; set; }

        public bool AllowEdit { get; set; }
        public bool AllowDeny { get; set; }
        public bool AllowApprove { get; set; }
        public bool AllowUnApprove { get; set; }
        public bool AllowUnDeny { get; set; }

		
    }

	public class PostApprovalHistoryRequestDTO : SearchRequestBaseDTO
	{
		public List<long> AccountIDs { get; set; }
		public long VirtualGroupID { get; set; }
		public long SocialNetworkID { get; set; }
		public DateTime StartDate { get; set; }
		public DateTime EndDate { get; set; }
		public ApprovalStatusEnum Status { get; set; }

		
		public PostApprovalHistoryRequestDTO()
		{
            Status = ApprovalStatusEnum.All;
		}
	}
}
