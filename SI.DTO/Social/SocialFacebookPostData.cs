﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SI.DTO
{
    public class SocialFacebookPostData
    {
        public long AccountID { get; set; }
        public string AccountName { get; set; }
        public long? FacebookPostID { get; set; }
        public long? PostTargetID { get; set; }
        public int? EngagedUsers { get; set; }
        public int? Impressions { get; set; }
        public int? ImpressionsUnique { get; set; }
        public int? Likes { get; set; }
        public int? Shares { get; set; }
        public int? Comments { get; set; }
        public DateTime StatsDate { get; set; }
        public DateTime PublishedDate { get; set; }
        public string PictureURL { get; set; }
        public string LinkURL { get; set; }
        public string Message { get; set; }
        public PostTypeEnum PostTypeID { get; set; }
    }
}
