﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SI.DTO
{
    public class SocialDashboardDataFacebook
    {
        public DateTime PeriodEnd { get; set; }
        public bool IsWeekly { get; set; }
        public int? FansNew { get; set; }
        public int? FansLost { get; set; }
        public int? AvgFans { get; set; }
        public int? Clicks { get; set; }
        public int? PageImpressions { get; set; }
        public int? PageImpressionsPaid { get; set; }
        public int? PostImpressions { get; set; }
        public int? PageViews { get; set; }
        public int? Stories { get; set; }
        public int? StoryAdds { get; set; }
        public int? PTAT { get; set; }
        public int? LastFans { get; set; }
    }
}
