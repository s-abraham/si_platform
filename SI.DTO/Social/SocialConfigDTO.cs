﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SI.DTO
{
	public class SocialConfigDTO
	{
		public string AppID { get; set; }
		public string AppSecret { get; set; }
		public string AppCallback { get; set; }
		public string AppPermissions { get; set; }
		public long SocialAppID { get; set; }
	}
}
