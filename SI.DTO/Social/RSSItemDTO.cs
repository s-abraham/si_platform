﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SI.DTO
{
	public class RSSItemDTO
	{
		public long ID { get; set; }
		public long RSSFeedID { get; set; }
		public string Title { get; set; }
		public string Link { get; set; }
		public string Summary { get; set; }
		public string SummaryAsText { get; set; }
        public SIDateTime PubDate { get; set; }
		public string MainImage { get; set; }
		public SIDateTime DateCreated { get; set; }

		public RSSItemDTO()
		{
		}
	}
}
