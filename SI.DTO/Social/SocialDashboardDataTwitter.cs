﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SI.DTO
{
    public class SocialDashboardDataTwitter
    {
        public bool IsWeekly { get; set; }
        public DateTime PeriodEnd { get; set; }

        public int? RetweetsCount { get; set; }
        public int? NewFollowers { get; set; }
        public int? TweetsCount { get; set; }
        public int? FollowingCount { get; set; }
        public int? FollowersCount { get; set; }
        public int? ListedCount { get; set; }
        public int? FriendsCount { get; set; }
        public int? NewTweets { get; set; }
    }
}
