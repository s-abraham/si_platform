﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SI.DTO
{
    public class SocialConnectionInfoDTO
    {
        public long? ID { get; set; }

        public long AccountID { get; set; }
        public long SocialAppID { get; set; }

        public SocialNetworkEnum SocialNetwork { get; set; }

        public string UniqueID { get; set; }       

        public string Token { get; set; }
        public string TokenSecret { get; set; }
        
        public string ScreenName { get; set; }
        public string PictureURL { get; set; }
        public string URI { get; set; }
    }
}
