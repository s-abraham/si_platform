﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SI.DTO
{
    public class ControllerStatusDTO
    {
        public ControllerStatusDTO()
        {
            Processes = new List<ProcessStatusDTO>();
        }

        public long ID { get; set; }

        public string MachineName { get; set; }
        public string IPAddress { get; set; }

        public int LoadRating { get; set; }
        public int CurrentLoad { get; set; }

        public List<ProcessStatusDTO> Processes { get; set; }

        public String DateCreated { get; set; }
        public String LastHeartbeat { get; set; }

        public int JobCount { get; set; }

        public bool IsAlive { get; set; }
    }
}
