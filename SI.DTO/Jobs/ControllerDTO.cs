﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SI.DTO
{
    public class ControllerDTO
    {
        public ControllerDTO()
        {
            ID = null;
        }

        public long? ID { get; set; }

        public string MachineName { get; set; }
        public string IPAddress { get; set; }
        public int LoadRating { get; set; }        
    }
}
