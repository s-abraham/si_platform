﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SI.DTO
{
    public class JobListDTO
    {
        public JobListDTO()
        {
            JobIDs = new List<long>();
        }
        public long? JobTypeID { get; set; }
        public List<long> JobIDs { get; set; }
    }
}
