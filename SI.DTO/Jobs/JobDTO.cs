﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SI.DTO
{
    public class JobDTO
    {
        public JobDTO()
        {
            ChainedJobs = new Queue<ChainedJobSpec>();
        }
        public long? ID { get; set; }

        public JobTypeEnum JobType { get; set; }
        public JobStatusEnum JobStatus { get; set; }
        public int Priority { get; set; }

        public long? ControllerID { get; set; }
        public long? ProcessID { get; set; }

        public long? OriginJobID { get; set; }

        public string JobData { get; set; }
        public JobDataBase JobDataObject { get; set; }

        public bool ContinueChainIfFailed { get; set; }
        public Queue<ChainedJobSpec> ChainedJobs { get; set; }

        public SIDateTime DateScheduled { get; set; }
        public SIDateTime DateStarted { get; set; }
        public SIDateTime DateCompleted { get; set; }

    }

    public class ChainedJobSpec
    {
        public JobTypeEnum JobType { get; set; }
        public int Priority { get; set; }
        public string JobData { get; set; }
        public int MaxAttempts { get; set; }
        public bool ContinueChainIfFailed { get; set; }
    }
}
