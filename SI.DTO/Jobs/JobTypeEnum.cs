﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SI.DTO
{
    public enum JobTypeEnum : long
    {
        AuditProcessor = 5,
        DiagnosticsProcessor = 6,

        ReviewDownloader = 10,
        ReviewIntegrator = 20,
        ReviewRunCleanup = 30,

        ReviewURLValidator = 200,

        SocialDownloadQueuer = 210,

        FacebookDownloader = 300,
        FacebookPublish = 700,
        FacebookIntegrator = 301,
        FacebookReviewDownloader = 702,

        TwitterPublish = 400,
        TwitterDownloader = 401,
        TwitterIntegrator = 302,

        GooglePlusPublish = 500,
        GooglePlusDownloader = 501,
        GooglePlusIntegrator = 303,

        YouTubePublish = 600,
        YouTubeDownloader = 601,
        YouTubeIntegrator = 602,

        ReviewDownloadQueuer = 1000,
        PublishJobQueuer = 1010,

        DeletePost = 1050,
        DeletePostTarget = 1051,

        Notification = 1100,

        TokenValidator = 1200,
        TokenValidatorQueuer = 1210,

        RSSDownloader = 1300,
        RSSDownloadQueuer = 1301,

        ReportEmail = 1400,
        LegacyMigration = 1500,

        SendGridProcessor = 1600


    }
}
