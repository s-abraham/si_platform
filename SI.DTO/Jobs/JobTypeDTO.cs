﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SI.DTO
{
    public class JobTypeDTO
    {
        public long ID { get; set; }
        public string Name { get; set; }
        public int BatchSize { get; set; }
        public int LoadRating { get; set; }
        public int? MaxSecondsPerJob { get; set; }
        public string BundleHash { get; set; }
    }
}
