﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SI.DTO
{
    public enum JobStatusEnum : long
    {
        Queued = 0,
        PickedUp = 10,
        Assigned = 20,
        Started = 30,
        Completed = 40,
        Failed = 50,

        Parked = 100 //Job is in limbo, probably due to testing of some sort
    }
}
