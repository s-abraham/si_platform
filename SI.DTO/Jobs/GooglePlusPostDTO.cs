﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SI.DTO
{
    public class GooglePlusPostDTO
    {
        public long CredentialID { get; set; }	    
	    public string ResultID { get; set; }
	    public string GooglePlusPostID { get; set; }
	    public string Name { get; set; }
	    public string Message { get; set; }
	    public long PostTypeID { get; set; }
	    public string LinkURL { get; set; }
	    public string PictureURL { get; set; }
	    public DateTime GooglePlusCreatedTime { get; set; }
        public DateTime GooglePlusUpdatedTime { get; set; }
    }
}
