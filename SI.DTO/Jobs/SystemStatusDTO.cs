﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SI.DTO
{
    public class SystemStatusDTO
    {
        public SystemStatusDTO()
        {
            Controllers = new List<ControllerStatusDTO>();
        }

        public List<ControllerStatusDTO> Controllers { get; set; }
        public List<JobStatusSummaryItemDTO> JobStatusSummaryItems { get; set; }
    }
}
