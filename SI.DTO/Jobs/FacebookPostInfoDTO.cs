﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SI.DTO
{
    public class FacebookPostInfoDTO
    {
        public SocialCredentialDTO Credential { get; set; }
        public long CredentialID { get; set; }
        public string ResultID { get; set; }
        public string FeedID { get; set; }
        public string StatusType { get; set; }
        public PostTypeEnum PostType { get; set; }
    }
}
