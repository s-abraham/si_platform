﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SI.DTO
{
    public class PerfSnapshotDTO
    {
        public PerfSnapshotDTO()
        {
            Jobs = new List<PerfSnapshotJobDTO>();
        }
        public long ControllerID { get; set; }
        public int CPULoad { get; set; }
        public int AvailableRAM { get; set; }     
        public List<PerfSnapshotJobDTO> Jobs {get;set;}
    }

    public class PerfSnapshotJobDTO
    {
        public long JobTypeID { get; set; }
        public int CPULoad { get; set; }
    }
}
