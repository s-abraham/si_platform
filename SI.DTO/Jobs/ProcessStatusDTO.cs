﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SI.DTO
{
    public class ProcessStatusDTO
    {
        public ProcessStatusDTO()
        {
            Jobs = new List<JobDTO>();
        }
        public List<JobDTO> Jobs { get; set; }
    }
}
