﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SI.DTO
{
    public class DeletePostTargetData : JobDataBase
    {
        public override JobTypeEnum JobType
        {
            get { return JobTypeEnum.DeletePostTarget; }
        }

        /// <summary>
        /// The PostTargetID of the target to delete
        /// </summary>
        public long PostTargetID { get; set; }

        /// <summary>
        /// The PostImageResult pertaining to the image to delete
        /// </summary>
        public long? PostImageResultID { get; set; }
    }
}
