﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SI.DTO
{
    public class QueueReviewDownloadsData : JobDataBase
    {
        public QueueReviewDownloadsData()
        {
            ReviewSources = new List<ReviewSourceEnum>();
        }
        
        public List<ReviewSourceEnum> ReviewSources { get; set; }
        public int DownloaderJobPriority { get; set; }
        public bool DownloadDetails { get; set; }
        public int JobSpreadMinutes { get; set; }

        public override JobTypeEnum JobType
        {
            get { return JobTypeEnum.ReviewDownloadQueuer; }
        }
    }
}
