﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SI.DTO
{
    public class ReviewIntegratorData : JobDataBase
    {
        public override JobTypeEnum JobType
        {
            get { return JobTypeEnum.ReviewIntegrator; }
        }

        public long AccountID { get; set; }
        public long ReviewSourceID { get; set; }

    }
}
