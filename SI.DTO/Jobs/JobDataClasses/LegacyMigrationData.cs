﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SI.DTO
{
    public class LegacyMigrationData : JobDataBase
    {
        public override JobTypeEnum JobType
        {
            get { return JobTypeEnum.LegacyMigration; }
        }

        public LegacyMigrationType Type { get; set; }

        /// <summary>
        /// The account id in the NEW systemn
        /// </summary>
        public long AccountID { get; set; }

        /// <summary>
        /// The dealer location id from the OLD system
        /// </summary>
        public int DealerLocationID { get; set; }

        public DateTime? MinQueryDate { get; set; }
        public DateTime? MaxQueryDate { get; set; }

        public enum LegacyMigrationType : int
        {
            SocialFacebookPost = 1
        }
    }
}
