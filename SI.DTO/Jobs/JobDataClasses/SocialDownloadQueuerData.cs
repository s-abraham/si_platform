﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SI.DTO
{
    public class SocialDownloadQueuerData : JobDataBase
    {
        public override JobTypeEnum JobType
        {
            get { return JobTypeEnum.SocialDownloadQueuer; }
        }

        public SocialDownloadQueuerAction Action { get; set; }
        public int? NumberOfDaysBack { get; set; }
        public long? AccountID { get; set; }

        public int? SpreadSeconds { get; set; }
    }

    public enum SocialDownloadQueuerAction : int
    {
        //facebook types
        FacebookPageInsightsAndInfo = 1,        
        FacebookPostStats = 2,
        FacebookPost = 3,

        //google types
        GooglePlusPageInfo = 10,
        GooglePlusPostStats = 11,
        GooglePlusPost = 12,

        //twitter
        TwitterPageInfo = 20,
        TwitterPostStats = 21
    }
}
