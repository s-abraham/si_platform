﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SI.DTO
{
    public class RSSDownloadQueuerData : JobDataBase
    {
        public override JobTypeEnum JobType
        {
            get { return JobTypeEnum.RSSDownloadQueuer; }
        }

        public int MaxItems { get; set; }
        public int DaysBack { get; set; }
        public int SpreadMinutes { get; set; }
        public bool DownloadImages { get; set; }
    }
}
