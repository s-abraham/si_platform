﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SI.DTO
{
    public class TwitterIntegratorData : JobDataBase
    {
        public override JobTypeEnum JobType
        {
            get { return JobTypeEnum.TwitterIntegrator; }
        }

        public TwitterProcessorActionEnum TwitterActionEnum { get; set; }

        public string Key { get; set; }
        public long? CredentialID { get; set; }
        public string ResultID { get; set; }
        public long PostTargetID { get; set; }

    }
}
