﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SI.DTO
{
    public class ReviewRunCleanupData : JobDataBase
    {
        public bool QueueNewReviewNotifications { get; set; }
        public bool ClearAllPendingReviewDownloaderJobs { get; set; }
        public bool ClearAllPendingReviewIntegratorJobs { get; set; }

        public override JobTypeEnum JobType
        {
            get { return JobTypeEnum.ReviewRunCleanup; }
        }
    }
}
