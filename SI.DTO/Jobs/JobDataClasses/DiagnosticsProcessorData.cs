﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SI.DTO
{
    public class DiagnosticsProcessorData : JobDataBase
    {
        public override JobTypeEnum JobType
        {
            get { return JobTypeEnum.DiagnosticsProcessor; }
        }

        public DiagnosticProcessorActionEnum Action { get; set; }

    }

    public enum DiagnosticProcessorActionEnum : int
    {
        //routine database cleanup
        PerformDatabaseCleanup = 1,

        //the 12 hour diagnostic report email
        Send12HourReputationDiagReport = 2,

        //applies default notification subscriptions based on a user or account
        ApplyDefaultNotificationSubscriptions = 3,

        //initializes post approvers and subscriptions for posts
        ProcessPostApproverInits = 4
    }
}
