﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SI.DTO
{
    public class ReportProcessorData : JobDataBase
    {
        public override JobTypeEnum JobType
        {
            get { return JobTypeEnum.ReportEmail; }
        }
    }
}
