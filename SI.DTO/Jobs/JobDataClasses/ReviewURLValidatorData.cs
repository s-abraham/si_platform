﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SI.DTO
{
    public class ReviewURLValidatorData : JobDataBase
    {
        public ReviewURLValidatorData(string companyName, string address, string city, string stateFull, string stateAbbrev, string zip, string phone)
        {
            CompanyName = companyName;
            StreetAddress = address;
            City = city;
            StateFull = stateFull;
            StateAbbrev = stateAbbrev;
            Zip = zip;
            Phone = phone;
        }

        /// <summary>
        /// The review source to validate
        /// </summary>
        public ReviewSourceEnum ReviewSource { get; set; }

        /// <summary>
        /// The account ID to validate
        /// </summary>
        public long AccountID { get; set; }

        public string CompanyName { get; set; }
        public string StreetAddress { get; set; }
        public string City { get; set; }
        public string StateFull { get; set; }
        public string StateAbbrev { get; set; }
        public string Zip { get; set; }
        public string Phone { get; set; }

        public override JobTypeEnum JobType
        {
            get { return JobTypeEnum.ReviewURLValidator; }
        }

    }
}
