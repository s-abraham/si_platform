﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SI.DTO
{
    public class FacebookIntegratorData : JobDataBase
    {
        public override JobTypeEnum JobType
        {
            get { return JobTypeEnum.FacebookIntegrator; }
        }

        public FacebookProcessorActionEnum FacebookEnum { get; set; }    

        public string Key { get; set; }
		public long CredentialID { get; set; }
		public string FeedID { get; set; }
		public string ResultID { get; set; }
		public string Since { get; set; }
		public string Until { get; set; }

        public long FacebookPostID { get; set; }
    }
}
