﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SI.DTO
{
    public class SendGridProcessorData : JobDataBase
    {
        public override JobTypeEnum JobType
        {
            get { return JobTypeEnum.SendGridProcessor; }
        }

        public SendGridProcessorAction Action { get; set; }        
    }

    public enum SendGridProcessorAction : int
    {
        CollectFailures = 1,
        DeleteSendGridObjects = 2
    }
}
