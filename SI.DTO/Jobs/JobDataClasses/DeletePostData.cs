﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SI.DTO
{
    public class DeletePostData : JobDataBase
    {        
        public override JobTypeEnum JobType
        {
            get { return JobTypeEnum.DeletePost; }
        }

        /// <summary>
        /// The PostID to delete
        /// </summary>
        public long PostID { get; set; }

    }
}
