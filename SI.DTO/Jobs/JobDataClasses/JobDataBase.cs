﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SI.DTO
{
    
    public abstract class JobDataBase
    {


        /// <summary>
        /// The Job Type that refers to this job data class
        /// </summary>
        public abstract JobTypeEnum JobType { get; }
        
        /// <summary>
        /// The remaining number of retries allowed, if any for this job.  Set to zero for no allowed retries.
        /// </summary>
        public int RemainingRetries { get; set; }

    }
}
