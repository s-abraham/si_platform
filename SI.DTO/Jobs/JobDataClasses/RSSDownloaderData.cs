﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SI.DTO
{
    public class RSSDownloaderData : JobDataBase
    {        
        public int MaxItems { get; set; }
        public int DaysBack { get; set; }
        public long RSSFeedID { get; set; }
        public bool DownloadImages { get; set; }

        public override JobTypeEnum JobType
        {
            get { return JobTypeEnum.RSSDownloader; }
        }
    }
}
