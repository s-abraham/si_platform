﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SI.DTO
{
    public class ReviewDownloaderData : JobDataBase
    {
        /// <summary>
        /// The review URL pertaining to this download
        /// </summary>
        public long Account_ReviewSourceID { get; set; }

        /// <summary>
        /// Download the details in addition to the summary
        /// </summary>
        public bool DownloadDetails { get; set; }

        //public ReviewSourceEnum ReviewSource { get; set; }

        public override JobTypeEnum JobType
        {
            get { return JobTypeEnum.ReviewDownloader; }
        }
    }
}
