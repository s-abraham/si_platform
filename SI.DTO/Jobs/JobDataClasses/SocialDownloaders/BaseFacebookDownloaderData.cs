﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SI.DTO
{
    public abstract class BaseFacebookDownloaderData : JobDataBase
    {
        public long AccountID { get; set; }

        public override JobTypeEnum JobType
        {
            get { return JobTypeEnum.FacebookPublish; }
        }
    }
}
