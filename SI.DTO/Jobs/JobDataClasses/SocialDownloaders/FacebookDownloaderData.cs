﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SI.DTO
{
    public class FacebookDownloaderData : JobDataBase
    {        
        public long? CredentialID { get; set; }

        public long? ReferencePostTargetID { get; set; }

        public long PostID     { get; set; }
        public long PostTargetID { get; set; }
        public long? PostImageID { get; set; }        

        public FacebookProcessorActionEnum FacebookEnum  { get; set; }

        public int? NumberOfDaysBack { get; set; }

        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }

        public override JobTypeEnum JobType
        {
            get { return JobTypeEnum.FacebookDownloader; }
        }
    }

    public class FacebookPublisherData : JobDataBase
    {
        public long? CredentialID { get; set; }

        public long? ReferencePostTargetID { get; set; }

        public long PostID { get; set; }
        public long PostTargetID { get; set; }
        public long? PostImageID { get; set; }

        public FacebookProcessorActionEnum FacebookEnum { get; set; }

        public int? NumberOfDaysBack { get; set; }

        public override JobTypeEnum JobType
        {
            get { return JobTypeEnum.FacebookPublish; }
        }
    }

    public class FacebookReviewData : JobDataBase
    {
        public long? CredentialID { get; set; }

        public long? ReferencePostTargetID { get; set; }

        public long PostID { get; set; }
        public long PostTargetID { get; set; }
        public long? PostImageID { get; set; }

        public FacebookProcessorActionEnum FacebookEnum { get; set; }

        public int? NumberOfDaysBack { get; set; }

        public override JobTypeEnum JobType
        {
            get { return JobTypeEnum.FacebookReviewDownloader; }
        }
    }
}
