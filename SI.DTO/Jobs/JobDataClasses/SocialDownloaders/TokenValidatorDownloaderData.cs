﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SI.DTO
{
    public class TokenValidatorDownloaderData : JobDataBase
    {
        public long? CredentialID { get; set; }

        public SocialNetworkEnum SocialNetwork { get; set; }

        public override JobTypeEnum JobType
        {
            get { return JobTypeEnum.TokenValidator; }
        }    
    }
}
