﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SI.DTO
{
    public class GooglePlusDownloaderData : JobDataBase
    {
        public long? CredentialID { get; set; }

        public long? ReferencePostTargetID { get; set; }

        public long PostID { get; set; }
        public long PostTargetID { get; set; }
        public long? PostImageID { get; set; }
        public string FirstImageURL { get; set; }

        public GooglePlusProcessorActionEnum GooglePlusEnum { get; set; }

        public override JobTypeEnum JobType
        {
            get { return JobTypeEnum.GooglePlusDownloader; }
        }       
    }

    public class GooglePlusPublisherData : JobDataBase
    {
        public long? CredentialID { get; set; }

        public long? ReferencePostTargetID { get; set; }

        public long PostID { get; set; }
        public long PostTargetID { get; set; }
        public long? PostImageID { get; set; }
        public string FirstImageURL { get; set; }

        public GooglePlusProcessorActionEnum GooglePlusEnum { get; set; }

        public override JobTypeEnum JobType
        {
            get { return JobTypeEnum.GooglePlusPublish; }
        }
    }
}
