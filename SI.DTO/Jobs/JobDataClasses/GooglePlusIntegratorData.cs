﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SI.DTO
{
    public class GooglePlusIntegratorData : JobDataBase
    {
        public override JobTypeEnum JobType
        {
            get { return JobTypeEnum.GooglePlusIntegrator; }
        }

        public GooglePlusProcessorActionEnum GooglePlusEnum { get; set; }

        public string Key { get; set; }
        public long? CredentialID { get; set; }
        public string ResultID { get; set; }

        public long GooglePlusPostID { get; set; }
    }
}
