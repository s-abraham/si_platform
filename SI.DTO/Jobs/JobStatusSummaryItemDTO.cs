﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SI.DTO
{
    public class JobStatusSummaryItemDTO
    {
        public long JobStatusID { get; set; }
        public string Status { get; set; }
        public int Total { get; set; }
    }
}
