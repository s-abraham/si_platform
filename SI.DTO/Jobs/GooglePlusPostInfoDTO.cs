﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SI.DTO
{
    public class GooglePlusPostInfoDTO
    {
        public SocialCredentialDTO Credential { get; set; }
        public long CredentialID { get; set; }
        public string ResultID { get; set; }
        public string GooglePlusPostID { get; set; }        
        public PostTypeEnum PostType { get; set; }
        public long ID { get; set; }
    }
}
