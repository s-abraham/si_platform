﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SI.DTO
{
    public enum AccessTokenTypeEnum : long
    {
        // user security related access
        PasswordReset = 100,
        // notification related access
        PostApproval = 200,
        // syndication invitation
        SyndicationInvitation = 300,
        // syndication Unsubscribe
        SyndicationUnsubscribe = 400,
        //
        SyndicationSelfRegistration = 500,
        // Product config to fix social connections
        SocialProductConfig = 600,
        // goes directly to the post approval history
        PostApprovalHistory = 700

    }
}
