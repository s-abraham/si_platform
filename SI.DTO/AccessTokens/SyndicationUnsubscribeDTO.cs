﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SI.Extensions;

namespace SI.DTO
{
    public class SyndicationUnsubscribeDTO : AccessTokenBaseDTO
    {
        public SyndicationUnsubscribeDTO(string dataPacket) : base(dataPacket) { }
        public SyndicationUnsubscribeDTO() : base() { }

        public override AccessTokenTypeEnum Type()
        {
            return AccessTokenTypeEnum.SyndicationUnsubscribe;
        }

        public long NotificationMessageTypeID { get; set; }
        public long NotificationTemplateID { get; set; }
		
		public long ResellerID { get; set; }
        
        #region packet encoding and decoding

        protected override void decodeDataPacket(string dataPacket)
        {
            string[] parts = dataPacket.Split('|');
            if (parts.Length == 3)
            {
                NotificationMessageTypeID = parts[0].ToLong();
                NotificationTemplateID = parts[1].ToLong();
				ResellerID = parts[2].ToLong();                
            }
        }

        public override string GetDataPacket()
        {
            return string.Format("{0}|{1}|{2}", NotificationMessageTypeID, NotificationTemplateID, ResellerID);
        }

        #endregion
    }
}
