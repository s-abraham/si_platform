﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SI.DTO
{
    public class PasswordResetAccessTokenDTO : AccessTokenBaseDTO
    {
        #region boilerplate 

        public PasswordResetAccessTokenDTO(string dataPacket) : base(dataPacket) { }

        public override AccessTokenTypeEnum Type()
        {
            return AccessTokenTypeEnum.PasswordReset;
        }

        #endregion

        #region packet encoding and decoding

        protected override void decodeDataPacket(string dataPacket)
        {
            // nothing needed
        }

        public override string GetDataPacket()
        {
            return null;
        }

        #endregion

    }
}
