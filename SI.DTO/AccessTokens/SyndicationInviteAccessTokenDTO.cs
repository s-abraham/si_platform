﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SI.DTO
{
    public class SyndicationInviteAccessTokenDTO : AccessTokenBaseDTO
    {
        #region boilerplate 

        public SyndicationInviteAccessTokenDTO(string dataPacket) : base(dataPacket) { }
        public SyndicationInviteAccessTokenDTO() : base() { }

        public override AccessTokenTypeEnum Type()
        {
            return AccessTokenTypeEnum.SyndicationInvitation;
        }

        #endregion

        /// <summary>
        /// The syndicator that sent a syndication invite to the customer
        /// </summary>
        public long SyndicatorID { get; set; }
        public long AccountID { get; set; }
        public long PackageID { get; set; }
        public long ResellerID { get; set; }

        #region packet encoding and decoding

        protected override void decodeDataPacket(string dataPacket)
        {
            string[] parts = dataPacket.Split(new char[] { '|' }, StringSplitOptions.RemoveEmptyEntries);
            if (parts.Length == 4)
            {
                SyndicatorID = Convert.ToInt64(parts[0]);
                AccountID = Convert.ToInt64(parts[1]);
                PackageID = Convert.ToInt64(parts[2]);
                ResellerID = Convert.ToInt64(parts[3]);
            }
        }

        public override string GetDataPacket()
        {
            return SyndicatorID.ToString(CultureInfo.InvariantCulture)
                + '|' + AccountID.ToString(CultureInfo.InvariantCulture)
                + '|' + PackageID.ToString(CultureInfo.InvariantCulture)
                + '|' + ResellerID.ToString(CultureInfo.InvariantCulture)
            ;
        }

        #endregion

    }
}
