﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SI.DTO
{
    public abstract class AccessTokenBaseDTO
    {
        public AccessTokenBaseDTO(string dataPacket)
        {
            decodeDataPacket(dataPacket);
        }
        public AccessTokenBaseDTO()
        {            
        }

        /// <summary>
        /// The Token ID
        /// </summary>
        public long? ID { get; set; }

        /// <summary>
        /// The user id that is mapped to this token
        /// </summary>
        public long UserID { get; set; }

        /// <summary>
        /// Gets The Access Token Type
        /// </summary>
        public abstract AccessTokenTypeEnum Type();

        /// <summary>
        /// The actual GUID
        /// </summary>
        public Guid Token { get; set; }

        /// <summary>
        /// The state of the token
        /// </summary>
        public AccessTokenStateEnum State
        {
            get
            {
                if (TimeTillExpiration.HasValue && TimeTillExpiration.Value.TotalSeconds <= 1)
                {
                    return AccessTokenStateEnum.PastExpirationDate;
                }
                if (RemainingUses.HasValue && RemainingUses.Value == 0)
                {
                    return AccessTokenStateEnum.NoMoreRemainingUses;
                }
                return AccessTokenStateEnum.Valid;
            }
        }
        
        /// <summary>
        /// If not null, this token will expire in this amount of time
        /// </summary>
        public TimeSpan? TimeTillExpiration { get; set; }

        /// <summary>
        /// If not null, this token will only be valid for this many more uses
        /// </summary>
        public int? RemainingUses { get; set; }

        /// <summary>
        /// If applicable, the implementation class should decode the packet data here
        /// </summary>
        protected abstract void decodeDataPacket(string dataPacket);

        /// <summary>
        /// If applicable, the implementing class should encode the data packet here
        /// </summary>
        /// <returns></returns>
        public abstract string GetDataPacket();

    }
}
