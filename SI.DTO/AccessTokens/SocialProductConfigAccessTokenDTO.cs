﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SI.DTO
{
	public class SocialProductConfigAccessTokenDTO : AccessTokenBaseDTO
	{
		 #region boilerplate

        public SocialProductConfigAccessTokenDTO(string dataPacket) : base(dataPacket) { }
		public SocialProductConfigAccessTokenDTO() : base() { }

        public override AccessTokenTypeEnum Type()
        {
            return AccessTokenTypeEnum.SocialProductConfig;
        }

        #endregion

		public long? AccountID { get; set; }

		#region packet encoding and decoding

		protected override void decodeDataPacket(string dataPacket)
		{
			AccountID = null;

			if (!string.IsNullOrWhiteSpace(dataPacket))
			{
				long lval;
				if (long.TryParse(dataPacket, out lval))
				{
					AccountID = lval;
				}
			}
		}

		public override string GetDataPacket()
		{
			return AccountID.ToString();
		}

		#endregion

	}
}
