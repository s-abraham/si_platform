﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SI.DTO
{
    public class PostApprovalHistoryAccessTokenDTO : AccessTokenBaseDTO
    {
        public PostApprovalHistoryAccessTokenDTO(string dataPacket) : base(dataPacket) { }
        public PostApprovalHistoryAccessTokenDTO() : base() { }

        public override AccessTokenTypeEnum Type()
        {
            return AccessTokenTypeEnum.PostApprovalHistory;
        }

        protected override void decodeDataPacket(string dataPacket)
        {
            // nothing needed
        }

        public override string GetDataPacket()
        {
            return null;
        }
    }
}
