﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SI.Extensions;

namespace SI.DTO
{
	public class SyndicationSelfRegistrationDTO : AccessTokenBaseDTO
	{
		 #region boilerplate 

        public SyndicationSelfRegistrationDTO(string dataPacket) : base(dataPacket){}
		public SyndicationSelfRegistrationDTO() : base(){}

        public override AccessTokenTypeEnum Type()
        {
            return AccessTokenTypeEnum.SyndicationSelfRegistration;
        }

        #endregion

        /// <summary>
        /// The syndicator that sent a syndication invite to the customer
        /// </summary> 
		public long ResellerID { get; set; }
		public long? SyndicatorID { get; set; }
		public long? VerticalMarketID { get; set; }
		public long? PreferedParentAccountID { get; set; }
        public long[] PackageIDs { get; set; }
		public long[] DefaultPackageIDs { get; set; }
		public decimal[] Prices { get; set; }
        

        #region packet encoding and decoding

        protected override void decodeDataPacket(string dataPacket)
        {
            string[] parts = dataPacket.Split('|');
            if (parts.Length == 7)
            {
				ResellerID = parts[0].ToLong();

				if (!string.IsNullOrEmpty(parts[1]) && parts[1] != "0")
					SyndicatorID = parts[1].ToLong();

				if (!string.IsNullOrEmpty(parts[2]) && parts[2] != "0")
					VerticalMarketID = parts[2].ToLong();

				if (!string.IsNullOrEmpty(parts[3]) && parts[3] != "0")
					PreferedParentAccountID = parts[3].ToLong();

				if (!string.IsNullOrEmpty(parts[4]))
				{
					string[] ids = parts[4].Split(',');
					if (ids.Length > 0)
					{
						PackageIDs = new long[ids.Length];
						int idx = 0;
						foreach (string id in ids)
						{
							PackageIDs[idx] = id.ToLong();
							idx++;
						}
					}
				}

				if (!string.IsNullOrEmpty(parts[5]))
				{
					string[] ids = parts[5].Split(',');
					if (ids.Length > 0)
					{
						DefaultPackageIDs = new long[ids.Length];
						int idx = 0;
						foreach (string id in ids)
						{
							DefaultPackageIDs[idx] = id.ToLong();
							idx++;
						}
					}
				}

				if (!string.IsNullOrEmpty(parts[6]))
				{
					string[] ids = parts[6].Split(',');
					if (ids.Length > 0)
					{
						Prices = new decimal[ids.Length];
						int idx = 0;
						foreach (string id in ids)
						{
							Prices[idx] = id.ToDecimal();
							idx++;
						}
					}
				}
               
            }
        }

        public override string GetDataPacket()
        {
	        StringBuilder packet = new StringBuilder("");

	        packet.Append(ResellerID.ToString());
	        packet.Append("|");
	        
			if (SyndicatorID.HasValue)
		        packet.Append(SyndicatorID.Value.ToString());
			packet.Append("|");
			
			if (VerticalMarketID.HasValue)
				packet.Append(VerticalMarketID.Value.ToString());
			packet.Append("|");

			if (PreferedParentAccountID.HasValue)
				packet.Append(PreferedParentAccountID.Value.ToString());
			packet.Append("|");

			if (PackageIDs != null && PackageIDs.Length > 0)
				packet.Append(string.Join(",", PackageIDs));
			packet.Append("|");

			if (DefaultPackageIDs != null && DefaultPackageIDs.Length > 0)
				packet.Append(string.Join(",", DefaultPackageIDs));
			packet.Append("|");

			if (Prices != null && Prices.Length > 0)
				packet.Append(string.Join(",", Prices));

	        return packet.ToString();
        }

        #endregion
	}
}
