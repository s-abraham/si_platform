﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SI.DTO
{
    public class PostApprovalAccessTokenDTO : AccessTokenBaseDTO
    {
        #region boilerplate

        public PostApprovalAccessTokenDTO(string dataPacket) : base(dataPacket) { }
        public PostApprovalAccessTokenDTO() : base() { }

        public override AccessTokenTypeEnum Type()
        {
            return AccessTokenTypeEnum.PostApproval;
        }

        #endregion

        public long? PostID { get; set; }
        public long? PostTargetID { get; set; }

        #region packet encoding and decoding

        protected override void decodeDataPacket(string dataPacket)
        {
            PostID = null;
            PostTargetID = null;

            if (!string.IsNullOrWhiteSpace(dataPacket))
            {
                string[] parts = dataPacket.Split(new char[] { '|' }, StringSplitOptions.RemoveEmptyEntries);
                if (parts.Length == 2)
                {
                    long lval;
                    if (long.TryParse(parts[0], out lval))
                    {
                        PostID = lval;
                    }
                    if (long.TryParse(parts[1], out lval))
                    {
                        PostTargetID = lval;
                    }
                }
            }
        }

        public override string GetDataPacket()
        {
            return string.Format("{0}|{1}", PostID, PostTargetID);
        }

        #endregion

    }
}
