﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SI.DTO
{
    public class CreateUserResultDTO
    {
        public CreateUserOutcome Outcome { get; set; }
        public UserDTO User { get; set; }
    }

    public enum CreateUserOutcome
    {
        PasswordRequired,
        UsernameRequired,
        EmailAddressRequired,
        UsernameAlreadyExists,
        EmailAddressAlreadyExists,
        InvalidEmailAddress,
        Success        
    }
}
