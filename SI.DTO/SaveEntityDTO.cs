﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SI.DTO
{
    public class SaveEntityDTO<T>
    {
        public SaveEntityDTO(UserInfoDTO userInfo)
        {
            UserInfo = userInfo;
            Problems = new List<string>();
            Entity = default(T);
        }
        public SaveEntityDTO(T obj, UserInfoDTO userInfo)
        {
            UserInfo = userInfo;
            Problems = new List<string>();
            Entity = obj;
        }
        public T Entity { get; set; }

        public List<string> Problems { get; set; }
        public UserInfoDTO UserInfo { get; set; }
    }
}
