﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SI.DTO
{
    public class DashboardOverviewDTO
    {
        public ReputationOverviewDTO ReputationOverview { get; set; }
        public SocialOverviewDTO SocialOverview { get; set; }

        public DashboardOverviewDTO()
        {
            ReputationOverview = new ReputationOverviewDTO();
            SocialOverview = new SocialOverviewDTO();
        }
    }

    public class SocialOverviewDTO
    {
        public SocialOverviewDTO()
        {
            FBLikes = new List<SocialMonthlyLikesDTO>();
            SocialInfoByAccount = new Dictionary<long, CombinedSocialInfoDTO>();
        }
        
        public List<SocialMonthlyLikesDTO> FBLikes { get; set; }
        public List<SocialMonthlyLikesDTO> FBLikes3Months
        {
            get
            {
                List<SocialMonthlyLikesDTO> list = new List<SocialMonthlyLikesDTO>();
             

                //2 months ago
				DateTime monthdDate = new DateTime(DateTime.Now.AddMonths(-2).Year, DateTime.Now.AddMonths(-2).Month, 1);
                list.Add(
                    new SocialMonthlyLikesDTO()
                    {
                        MonthDate = monthdDate,
                        Likes = (from fbl in FBLikes where fbl.MonthDate.Year == monthdDate.Year && fbl.MonthDate.Month == monthdDate.Month select fbl.Likes).FirstOrDefault()
                    }
                );

				//last month
				monthdDate = new DateTime(DateTime.Now.AddMonths(-1).Year, DateTime.Now.AddMonths(-1).Month, 1);
				list.Add(
					new SocialMonthlyLikesDTO()
					{
						MonthDate = monthdDate,
						Likes = (from fbl in FBLikes where fbl.MonthDate.Year == monthdDate.Year && fbl.MonthDate.Month == monthdDate.Month select fbl.Likes).FirstOrDefault()
					}
				);
				
				//this month
				 monthdDate = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1);
				list.Add(
					new SocialMonthlyLikesDTO()
					{
						MonthDate = monthdDate,
						Likes = (from fbl in FBLikes where fbl.MonthDate.Year == monthdDate.Year && fbl.MonthDate.Month == monthdDate.Month select fbl.Likes).FirstOrDefault()
					}
				);
                return list;
            }
        }

        //by location social data
        public Dictionary<long, CombinedSocialInfoDTO> SocialInfoByAccount { get; set; }

        //New
        public int GooglePlusPosts { get; set; }
        public int GooglePlusPostComments { get; set; }
        public int GooglePlusPostPlusoners { get; set; }
        public int GooglePlusPostResharers { get; set; }

        public int FBPosts { get; set; }
        public int FBPostComments { get; set; } //FBComments
        public int FBPostLikes { get; set; }
        public int FBPostShares { get; set; }

        public int TwitterPosts { get; set; }
        public int TwitterRetweetCount { get; set; }

        public int FacebookPageLikes_AllTime { get; set; }
        public int GooglePlusPlusOneCount_AllTime { get; set; }
        public int TwitterFollowersCount_AllTime { get; set; }

        public int FacebookPageLikes_30days { get; set; }
        public int GooglePlusPlusOneCount_30days { get; set; }
        public int TwitterFollowersCount_30days { get; set; }

        public int FacebookPageLikes_7days { get; set; }
        public int GooglePlusPlusOneCount_7days { get; set; }
        public int TwitterFollowersCount_7days { get; set; }
        
    }

    public class SocialMonthlyLikesDTO
    {        
        public DateTime MonthDate { get; set; }
        public int Likes { get; set; }
    }

    public class ReputationOverviewDTO
    {
        public int NumberOfDealers { get; set; }

        public double AllNonZeroRatings { get; set; }
        public int ReviewDetailCount { get; set; }

        public ReviewCountsDTO ReviewsAllTime { get; set; }
        public ReviewCountsDTO ReviewsLast30 { get; set; }
        public ReviewCountsDTO ReviewsLast7 { get; set; }

        public Dictionary<ReviewSourceEnum, ReviewCountsDTO> ReviewCountsBySource { get; set; }
        public Dictionary<long, ReviewCountsDTO> ReviewCountsByAccountID { get; set; }
        public Dictionary<long, SimpleAccountInfoDTO> AccountList { get; set; }

        public List<ReputationMonthMetrics> MonthlyMetrics { get; set; }
        public List<ReputationMonthMetrics> MonthlyMetrics6Months
        {
            get
            {
                return (from m in MonthlyMetrics where m.MonthDate >= DateTime.UtcNow.AddMonths(-6) select m).ToList();
            }
        }

        public ReputationOverviewDTO()
        {
            AccountList = new Dictionary<long, SimpleAccountInfoDTO>();

            ReviewsAllTime = new ReviewCountsDTO();
            ReviewsLast30 = new ReviewCountsDTO();
            ReviewsLast7 = new ReviewCountsDTO();
            ReviewCountsBySource = new Dictionary<ReviewSourceEnum, ReviewCountsDTO>();
            ReviewCountsByAccountID = new Dictionary<long, ReviewCountsDTO>();
            MonthlyMetrics = new List<ReputationMonthMetrics>();                       
        }
    }

    public class ReputationMonthMetrics
    {
        public DateTime MonthDate { get; set; }
        public ReviewSourceEnum ReviewSource { get; set; }        
        public double AvgRating { get; set; }
    }

    public class ReviewCountsDTO
    {
        public int Total { get; set; }
        public int TotalFiltered { get; set; }
        public int TotalPositive { get; set; }
        public int TotalPositiveFiltered { get; set; }
        public int TotalNegative { get; set; }
        public int TotalNegativeFiltered { get; set; }
        public double AverageRating { get; set; }
        public SIDateTime LastReviewDate { get; set; }
    }

    public class CombinedSocialInfoDTO
    {
        public CombinedSocialInfoDTO()
        {
            ConnectedNetworks = new List<SocialNetworkEnum>();
        }
        public List<SocialNetworkEnum> ConnectedNetworks { get; set; }
        

        public int PageLikes { get; set; }
        public int PageTalkingAbout { get; set; }
        public int PageWereHere { get; set; }
        
        /// <summary>
        /// All metrics
        /// </summary>
        public SocialMetricsDTO PostMetrics { get; set; }

        /// <summary>
        /// Metrics for posts made through our system
        /// </summary>
        public SocialMetricsDTO InternalPostMetrics { get; set; }

        /// <summary>
        /// Metrics for posts made outside our system
        /// </summary>
        public SocialMetricsDTO ExternalPostMetrics { get; set; }
    }

    public class SocialMetricsDTO
    {
        public int Posts { get; set; }
        public int Comments { get; set; }
        public int PostLikes { get; set; }        
        public int Shares { get; set; }
        public SIDateTime LastPostDate { get; set; }
    }

}
