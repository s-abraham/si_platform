﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SI.DTO
{
	public class NotificationSummaryDTO
	{
		public int ID { get; set; }
		public string ActionImage { get; set; }
		public string Text { get; set; }
        public int Count { get; set; }
        public NotificationAlertLevelIconEnum AlertLevel { get; set; }
	}

    public enum NotificationAlertLevelIconEnum
    {
        alert,
        information,
        warning
    }

	public class NotificationsToDoDTO
	{
		public int ID { get; set; }
		public string Type { get; set; }
		public string ActionImage { get; set; }
		public string LocationName { get; set; }
		public string Description { get; set; }
		public DateTime ToDoDateLocal { get; set; }
		public bool Completed { get; set; }
		public DateTime? CompletedDateLocal { get; set; }
	}

	public class NotificationsAlertDTO
	{
		public long ID { get; set; }
		public NotificationMessageTypeEnum AlertType { get; set; }		
		public string LocationName { get; set; }
		public string Description { get; set; }

	}

}
