﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SI.DTO
{
    public class SearchResultBaseDTO
    {
        public int StartIndex { get; set; }
        public int EndIndex { get; set; }
        public int TotalRecords { get; set; }

        public int SortBy { get; set; }
        public bool SortAscending { get; set; }
                
    }
}
