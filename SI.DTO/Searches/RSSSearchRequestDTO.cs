﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SI.DTO
{
    public class RSSSearchRequestDTO : SearchRequestBaseDTO
    {
        public string SearchText { get; set; }
		public string Franchise { get; set; }

        public long? RSSItemID { get; set; }

        public SIDateTime MinPubDate { get; set; }
        public SIDateTime MaxPubDate { get; set; }
        public SIDateTime MinCreateDate { get; set; }
        public SIDateTime MaxCreateDate { get; set; }
    }
}
