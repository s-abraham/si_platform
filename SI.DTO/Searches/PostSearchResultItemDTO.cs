﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SI.DTO
{
    public class PostSearchResultItemDTO
    {
        public PostSearchResultItemDTO()
        {

        }

        public long PostID { get; set; }

        public long? SyndicatorID { get; set; }
        public long? SyndicatorAccountID { get; set; }

        public string Title { get; set; }
        public SIDateTime Date { get; set; }
        public PostTypeEnum Type { get; set; }

        public SIDateTime MinScheduleDate { get; set; }
        public SIDateTime MaxScheduleDate { get; set; }
        public SIDateTime MinPublishedDate { get; set; }
        public SIDateTime MaxPublishedDate { get; set; }
        public SIDateTime MinFailedDate { get; set; }
        public SIDateTime MaxFailedDate { get; set; }
        public SIDateTime MinPostExpirationDate { get; set; }
        public SIDateTime MaxPostExpirationDate { get; set; }
        public SIDateTime ApprovalExpirationDate { get; set; }

        public long UserID { get; set; }
        public string Username { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }

        public int FacebookCount { get; set; }
        public int TwitterCount { get; set; }
        public int GooglePlusCount { get; set; }

        public int DealerCount { get; set; }
        public int DealerApproved { get; set; }
        public int DealerDenied { get; set; }

        public int DealersFailed { get; set; }
        public int DealersSucceeded { get; set; }

        public int FacebookApproved { get; set; }
        public int TwitterApproved { get; set; }
        public int GooglePlusApproved { get; set; }

        public int FacebookDenied { get; set; }
        public int TwitterDenied { get; set; }
        public int GooglePlusDenied { get; set; }

        public int FacebookSucceeded { get; set; }
        public int TwitterSucceeded { get; set; }
        public int GooglePlusSucceeded { get; set; }

        public int FacebookFailed { get; set; }
        public int TwitterFailed { get; set; }
        public int GooglePlusFailed { get; set; }

        public int Total { get; set; }
        public int Completed { get; set; }
        public int Failed { get; set; }

        public bool AllowEdit { get; set; }
        public bool AllowDelete { get; set; }
    }

}
