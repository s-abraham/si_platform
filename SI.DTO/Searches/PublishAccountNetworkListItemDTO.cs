﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SI.DTO
{
    public class PublishAccountNetworkListItemDTO
    {
        public PublishAccountNetworkListItemDTO()
        {
			Franchises = new List<FranchisesTypes>();
        }
        public long ResellerID { get; set; }
        public long CredentialID { get; set; }
        public long AccountID { get; set; }
		public string Name { get; set; }
        public long SocialAppID { get; set; }
        public long SocialNetworkID { get; set; }
		public List<FranchisesTypes> Franchises { get; set; }
        public long StateID { get; set; }
		public string StateAbbr { get; set; }
		public string Address { get; set; }
		public string City { get; set; }
		public string Zipcode { get; set; }
		public string Network { get; set; }
        public bool IsCredentialActive { get; set; }
        public bool IsSocialAppActive { get; set; }
        public SIDateTime LastPostDate { get; set; }
        public SIDateTime NextScheduledPostDate { get; set; }

        public string SocialNetworkURL { get; set; }
        public string SocialNetworkUniqueID { get; set; }
        public string SocialNetworkScreenName { get; set; }
		public string SocialNetworkPictureURL { get; set; }

        public bool IsAutoApproved { get; set; }

        public long TimezoneID { get; set; }
        public string TimezoneName { get; set; }
        public int TimezoneOffsetMinutesFromLoggedInUsersZone { get; set; }

		public class FranchisesTypes
		{
			public long ID { get; set; }
			public string Name { get; set; }
		}
	}

	
}
