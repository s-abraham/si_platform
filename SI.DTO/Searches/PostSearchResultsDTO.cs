﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SI.DTO
{
    public class PostSearchResultsDTO : SearchResultBaseDTO
    {
        public PostSearchResultsDTO()
        {
            Items = new List<PostSearchResultItemDTO>();
        }

        public List<PostSearchResultItemDTO> Items { get; set; }
    }
}
