﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SI.DTO
{
    public class PublishAccountNetworkListSearchResultDTO : SearchResultBaseDTO
    {
        public PublishAccountNetworkListSearchResultDTO()
        {
            Items = new List<PublishAccountNetworkListItemDTO>();
        }

        public List<PublishAccountNetworkListItemDTO> Items { get; set; }
    }
}
