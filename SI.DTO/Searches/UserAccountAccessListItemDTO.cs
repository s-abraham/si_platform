﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SI.DTO
{
    public class UserAccountAccessListItemDTO
    {
        public long AccountID { get; set; }

        public long UserID { get; set; }
        public long RoleID { get; set; }

        public string Username { get; set; }
        public string UserFirstName { get; set; }
        public string UserLastName { get; set; }
        public string UserEmailAddress { get; set; }

        public long MemberOfAccountID { get; set; }
        
        public RoleTypeEnum RoleType { get; set; }
        public string RoleName { get; set; }
        public bool Cascading { get; set; }

        public long? InheritedFromAccountID { get; set; }
        public string InheritedFromAccountName { get; set; }

    }
}
