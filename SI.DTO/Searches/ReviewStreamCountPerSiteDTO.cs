﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SI.DTO
{
	public class ReviewStreamCountPerSiteRequestDTO
	{
		public ReviewStreamCountPerSiteRequestDTO()
		{
			SearchString = null;
			AccountIDs = new List<long>();
			StateIDs = new List<long>();
			CategoryID = null;
			ReviewSourcesIDs = new List<long>();
			FranchiseTypeIDs = new List<long>();
			RatingIsGreaterThanOrEqualTo = null;
			RatingIsLessThanOrEqualTo = null;
			ReviewTextContains = null;
			ReviewTextDoesNotContain = null;
			MinDate = null;
			MaxDate = null;
			IncludeFiltered = false;
			IncludeUnFiltered = true;
		}

		public string SearchString { get; set; }
		public List<long> AccountIDs { get; set; }
		public bool IncludeFiltered { get; set; }
		public bool IncludeUnFiltered { get; set; }
		public bool FilterRead { get; set; }
		public bool FilterUnRead { get; set; }
		public bool FilterResponded { get; set; }
		public bool FilterUnResponded { get; set; }
		public bool FilterIgnored { get; set; }
		public double? RatingIsGreaterThanOrEqualTo { get; set; }
		public double? RatingIsLessThanOrEqualTo { get; set; }
		public string ReviewTextContains { get; set; }
		public string ReviewTextDoesNotContain { get; set; }
		public List<long> StateIDs { get; set; }
		public DateTime? MinDate { get; set; }
		public DateTime? MaxDate { get; set; }
		public List<long> ReviewSourcesIDs { get; set; }
		public long? CategoryID { get; set; }
		public List<long> FranchiseTypeIDs { get; set; }
	}

	public class ReviewStreamCountPerSiteResultDTO
	{
		public List<ReviewCountPerSiteResultsDTO> results { get; set; }

		public ReviewStreamCountPerSiteResultDTO()
		{
			results = new List<ReviewCountPerSiteResultsDTO>();
		}
	}

	public class ReviewCountPerSiteResultsDTO
	{
		//public long ReviewSourceID { get; set; }
		public int? TotalReviews { get; set; }
		public string Name { get; set; }
		public int? PositiveReviews { get; set; }
		public int? NegativeReviews { get; set; }
	}
}
