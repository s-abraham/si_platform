﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SI.DTO
{
    public class AccountSearchRequestDTO : SearchRequestBaseDTO
    {
        public AccountSearchRequestDTO()
        {
            FranchiseTypeIDs = new List<long>();
            PackageIDs = new List<long>();
            AccountTypeIDs = new List<long>();
            ExcludeAccountIDs = new List<long>();
            ConnectedSocialNetworks = new List<SocialNetworkEnum>();
        }

        public UserInfoDTO UserInfo { get; set; }

        /// <summary>
        /// Only accounts with matching text will be returned (meta search)
        /// </summary>
        public string SearchString { get; set; }

        /// <summary>
        /// If not null, only accounts who are within the specified virtual group will be returned
        /// </summary>
        public long? InVirtualGroupID { get; set; }
        /// <summary>
        /// If not null, only accounts who are not within the specified virtual group will be returned
        /// </summary>
        public long? NotInVirtualGroupID { get; set; }

        /// <summary>
        /// If not null, only accounts who are children of the specified parent account id will be returned
        /// </summary>
        public long? ParentAccountID { get; set; }
        /// <summary>
        /// If not null, only accounts who are not children of the specified parent account id will be returned
        /// </summary>
        public long? NotParentAccountID { get; set; }

        public List<long> FranchiseTypeIDs { get; set; }
        /// <summary>
        /// If not null or empty, only accounts with the currently subscribed package IDs will be returned
        /// </summary>
        public List<long> PackageIDs { get; set; }
        /// <summary>
        /// If not null or empty, only accounts with the following accountIDs will be returned
        /// </summary>
        public List<long> AccountTypeIDs { get; set; }

        /// <summary>
        /// Accounts listed here will not be included in the results
        /// </summary>
        public List<long> ExcludeAccountIDs { get; set; }

        /// <summary>
        /// Only accounts that have at least one child account will be returned
        /// </summary>
        public bool OnlyIfHasChildAccounts { get; set; }

        /// <summary>
        /// Only accounts with the specified OriginSystemIdentifier will be returned
        /// </summary>
        public string OriginSystemIdentifier { get; set; }

        /// <summary>
        /// If not null or empty, only accounts with active connected networks listed here will be returned
        /// </summary>
        public List<SocialNetworkEnum> ConnectedSocialNetworks { get; set; }

        /// <summary>
        /// If not null, only this account id and its descendants will be returned.
        /// </summary>
        public long? AccountIDAndDescendants { get; set; }
                
    }
}
