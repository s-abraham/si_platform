﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SI.DTO
{
    public class UserSearchResultItemDTO
    {
        public long ID { get; set; }

        public long MemberOfAccountID { get; set; }
        public string MemberOfAccountName { get; set; }

        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string EmailAddress { get; set; }

        public long? RoleTypeID { get; set; }
        public string RoleTypeName { get; set; }

        public bool? CascadeRole { get; set; }

        public SIDateTime DateCreated { get; set; }
        public DateTime DateCreatedUTC { get; set; }

		public Guid SSOSecretKey { get; set; }

        public bool CanImpersonate { get; set; }
		public bool CanEditUserOnMemberOfAccount { get; set; }
		public bool CanEditMemberOfAccount { get; set; }
    }
}
