﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SI.DTO
{
    public class PostTargetSearchRequestDTO: SearchRequestBaseDTO
    {
        public PostTargetSearchRequestDTO(UserInfoDTO userInfo)
        {
            PostIDs = new List<long>();
            AccountIDs = new List<long>();
            FranchiseTypeIDs = new List<long>();
            SocialNetworks = new List<SocialNetworkEnum>();
            MinDateScheduled = new SIDateTime(DateTime.Now.AddDays(-60), userInfo.EffectiveUser.ID.Value);

            PostType = null;
            PostStatus = null;
            PostedByUserID = null;
        }

        public List<long> PostIDs { get; set; }
        public List<long> AccountIDs { get; set; }
        public List<long> FranchiseTypeIDs { get; set; }
        public List<SocialNetworkEnum> SocialNetworks { get; set; }

        public PostTypeEnum? PostType { get; set; }
        public PostStatusEnum? PostStatus { get; set; }
        public long? PostedByUserID { get; set; }

        public SIDateTime MinDateScheduled { get; set; }
        public SIDateTime MaxDateScheduled { get; set; }

    }
}
