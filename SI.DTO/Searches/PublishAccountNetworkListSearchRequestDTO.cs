﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SI.DTO
{
    public class PublishAccountNetworkListSearchRequestDTO : SearchRequestBaseDTO
    {
        /// <summary>
        /// If not null, returns a target list with syndicatees for the given syndicator
        /// </summary>
        public long? SyndicatorID { get; set; }

        /// <summary>
        /// Restricts to given social networks
        /// </summary>
        public List<long> SocialNetworkIDs = new List<long>();

        /// <summary>
        /// Restricts to given Franchise Types (OEMs)
        /// </summary>
        public List<long> FranchiseTypeIDs = new List<long>();

        /// <summary>
        /// If not null and hs at least one item in list, restricts to only the given account IDs
        /// </summary>
        public List<long> AccountIDs = new List<long>();

        /// <summary>
        /// Restricts to accounts with given the search string text is found
        /// </summary>
        public string AccountSearchString = null;

        /// <summary>
        /// Only include targets contained in the specified virtual group
        /// </summary>
        public long? VirtualGroupID { get; set; }

		/// <summary>
		/// Only States
		/// </summary>
		public long? StateID { get; set; }
    }
}
