﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SI.DTO
{
    public class ReviewStreamSearchResultDTO : SearchResultBaseDTO
    {
        public ReviewStreamSearchResultDTO()
        {
            Items = new List<StreamDTO>();
        }
        public List<StreamDTO> Items { get; set; }
    }
}
