﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SI.DTO
{
    public class PostTargetSearchResultItemDTO
    {
        public PostTargetSearchResultItemDTO()
        {

        }
        public long PostID { get; set; }

        public long CredentialID { get; set; }
        public long PostTargetID { get; set; }
        public long? SyndicatorID { get; set; }
        public SocialNetworkEnum SocialNetwork { get; set; }

        public string AccountName { get; set; }
        public long AccountID { get; set; }

        public string SocialNetworkPageURL { get; set; }
        public string SocialNetworkScreenName { get; set; }

        public PostStatusEnum Status { get; set; }
        public string StatusName { get; set; }
        public SIDateTime StatusDate { get; set; }
		public SIDateTime CreateDate { get; set; }
        public TimeSpan? StatusTimspan { get; set; }

        public SIDateTime PostExpirationDate { get; set; }
        public SIDateTime ApprovalExpirationDate { get; set; }

        public long UserID { get; set; }
        public string Username { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }

        public string Title { get; set; }
        public string PostImageURL { get; set; }

        public int? Actions { get; set; }
        public int? Impressions { get; set; }

        public PostTypeEnum Type { get; set; }

        public PostTargetTypeEnum PostTargetType { get; set; }

        public string FailureResponse { get; set; }

        public bool CanEdit { get; set; }
        public bool CanRepost { get; set; }
        public bool CanDelete { get; set; }

    }

    public enum PostTargetTypeEnum
    {
        Scheduled,
        Immediate,
        Syndicated
    }
}
