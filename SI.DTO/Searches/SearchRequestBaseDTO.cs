﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SI.DTO
{
    public abstract class SearchRequestBaseDTO
    {
        //public SecurityContextDTO SecurityContext { get; set; }
        
        public int StartIndex { get; set; }
        public int EndIndex { get; set; }

        public int SortBy { get; set; }
        public bool SortAscending { get; set; }
                
    }
}
