﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SI.DTO
{
    public class UserSearchRequestDTO : SearchRequestBaseDTO
    {
        public UserSearchRequestDTO()
        {
            IncludeDeactivatedUsers = false;
        }

        public UserInfoDTO UserInfo { get; set; }

        public string SearchString { get; set; }
        public long? MemberOfAccountID { get; set; }
        public long? NotMemberOfAccountID { get; set; }
        public long? UsersWithoutRoleInAccountID { get; set; }
        public bool IncludeDeactivatedUsers { get; set; }
    }
}
