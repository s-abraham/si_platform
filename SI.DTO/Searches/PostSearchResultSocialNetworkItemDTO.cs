﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SI.DTO
{
    public class PostSearchResultSocialNetworkItemDTO
    {
        public long PostID { get; set; }
        public long SocialNetworkID { get; set; }
        public string SocialNetworkName { get; set; }
    }
}
