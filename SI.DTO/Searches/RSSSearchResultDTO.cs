﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SI.DTO
{
    public class RSSSearchResultDTO : SearchResultBaseDTO
    {
        public RSSSearchResultDTO()
        {
            Items = new List<RSSItemDTO>();
        }
        public List<RSSItemDTO> Items { get; set; }
    }
}
