﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SI.DTO
{
    public class PostTargetSearchResultsDTO : SearchResultBaseDTO
    {
        public PostTargetSearchResultsDTO()
        {
            Items = new List<PostTargetSearchResultItemDTO>();
        }

        public List<PostTargetSearchResultItemDTO> Items { get; set; }
    }
}
