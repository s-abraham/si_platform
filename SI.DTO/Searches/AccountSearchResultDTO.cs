﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SI.DTO
{
    public class AccountSearchResultDTO : SearchResultBaseDTO
    {
        public List<AccountListItemDTO> Items { get; set; }
        public string SearchString { get; set; }
    }
}
