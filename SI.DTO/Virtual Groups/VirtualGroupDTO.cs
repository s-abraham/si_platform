﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SI.DTO
{
    public class VirtualGroupDTO
    {
        public long? ID { get; set; }
        public long VirtualGroupTypeID { get; set; }

        public string Name { get; set; }
        public bool? IsSystem { get; set; }

        public bool EditableByUser { get; set; }

        public SIDateTime DateCreated { get; set; }        
    }
}
