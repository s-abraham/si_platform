﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SI.DTO
{
    public class ScheduleDTO
    {
        public long? ID { get; set; }
        public string Name { get; set; }

        public ScheduleSpecDTO ScheduleSpec { get; set; }

        public JobTypeEnum JobType { get; set; }
        public string JobData { get; set; }
        public int JobPriority { get; set; }

        public DateTime? DateLastExecuted { get; set; }
    }
}
