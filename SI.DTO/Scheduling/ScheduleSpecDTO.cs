﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SI.DTO
{
    public class ScheduleSpecDTO
    {
        public ScheduleSpecDTO()
        {
            DaysOfWeek = new List<DayOfWeek>();
            EarliestHour = null;
            LatestHour = null;
            DaysOfMonth = new List<int>();
        }

        /// <summary>
        /// The days of the week that a schedule is active.  If no items are added, all days are assumed.
        /// </summary>
        public List<DayOfWeek> DaysOfWeek { get; set; }

        /// <summary>
        /// The days of the month when the schedule is active.  If no items are added, all days are assumed.
        /// </summary>
        public List<int> DaysOfMonth { get; set; }

        /// <summary>
        /// The earliest hour of the day when the schedule is active (between 0 and 23).  If null, 0 is assumed.
        /// </summary>
        public int? EarliestHour { get; set; }

        /// <summary>
        /// The latest hour of the day when the schedule is active (between 0 and 23).  If null, 23 is assumed.
        /// </summary>
        public int? LatestHour { get; set; }

        /// <summary>
        /// The minimum amount of time that must elapse between executions of the schedule.
        /// </summary>
        public TimeSpan MinimumTimeBetweenSchedules { get; set; }

        /// <summary>
        /// Gets the next date and time when a schedule should run.  If null, the previous run date will be assumed to be now
        /// minus the MinimumTimeBetweenSchedules
        /// </summary>
        public DateTime GetNextRunDate(DateTime? previousRunDate)
        {
            DateTime prev = DateTime.UtcNow.Subtract(MinimumTimeBetweenSchedules);
            if (previousRunDate.HasValue)
                prev = previousRunDate.Value;

            //start at the minimum time between schedules
            DateTime test = prev.Add(MinimumTimeBetweenSchedules);

            while (!validToRun(prev, test))
            {
                //if days of month is set, advance to next legit day
                if (DaysOfMonth.Any())
                {
                    while (!DaysOfMonth.Contains(test.Day))
                    {
                        test = test.Date.AddDays(1);
                    }
                }

                //if days of week is set, advance to next legit day
                if (DaysOfWeek.Any())
                {
                    while (!DaysOfWeek.Contains(test.DayOfWeek))
                    {
                        test = test = test.Date.AddDays(1);
                    }
                }

                //if earliest hour is set, advance to at least that hour...
                if (EarliestHour.HasValue)
                {
                    while (test.Hour < EarliestHour.Value)
                    {
                        test = new DateTime(test.Year, test.Month, test.Day, test.Hour, 0, 0);
                        test = test.AddHours(1);
                    }
                }
                if (LatestHour.HasValue)
                {
                    while (test.Hour > LatestHour.Value)
                    {
                        test = new DateTime(test.Year, test.Month, test.Day, test.Hour, 0, 0);
                        test = test.AddHours(1);
                    }
                }
            }

            return test;
        }
        private bool validToRun(DateTime prev, DateTime test)
        {
            if (test < prev.Add(MinimumTimeBetweenSchedules)) return false;
            if (DaysOfMonth.Any() && !DaysOfMonth.Contains(test.Day)) return false;
            if (DaysOfWeek.Any() && !DaysOfWeek.Contains(test.DayOfWeek)) return false;
            if (EarliestHour.HasValue && test.Hour < EarliestHour.Value) return false;
            if (LatestHour.HasValue && test.Hour > LatestHour.Value) return false;

            return true;
        }

        public override string ToString()
        {
            StringBuilder sb = new StringBuilder();
            sb.AppendFormat("{0}", string.Join(",", (from d in DaysOfWeek select (int)d).ToList()));
            sb.AppendFormat("|{0}", string.Join(",", DaysOfMonth));
            sb.AppendFormat("|{0}", EarliestHour.GetValueOrDefault(-1));
            sb.AppendFormat("|{0}", LatestHour.GetValueOrDefault(-1));
            sb.AppendFormat("|{0}", MinimumTimeBetweenSchedules.TotalSeconds);
            return sb.ToString();
        }
        public static ScheduleSpecDTO FromString(string s)
        {
            ScheduleSpecDTO dto = new ScheduleSpecDTO();
            string[] parts = s.Split(new char[] { '|' }, StringSplitOptions.None);
            if (parts.Count() != 5)
                return null;

            {
                string[] xp = parts[0].Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
                foreach (string item in xp)
                {
                    int i;
                    if (int.TryParse(item, out i)) dto.DaysOfWeek.Add((DayOfWeek)i);
                }
            }
            {
                string[] xp = parts[1].Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
                foreach (string item in xp)
                {
                    int i;
                    if (int.TryParse(item, out i)) dto.DaysOfMonth.Add(i);
                }
            }
            {
                int i;
                if (int.TryParse(parts[2], out i))
                {
                    if (i > -1) dto.EarliestHour = i;
                }
            }
            {
                int i;
                if (int.TryParse(parts[3], out i))
                {
                    if (i > -1) dto.LatestHour = i;
                }
            }
            {
                int i;
                if (int.TryParse(parts[4], out i))
                {
                    dto.MinimumTimeBetweenSchedules = new TimeSpan(0, 0, 0, i);
                }
            }

            return dto;
        }
    }

}
