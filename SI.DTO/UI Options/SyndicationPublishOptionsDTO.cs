﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SI.DTO
{
    public class SyndicationPublishOptionsDTO
    {
        public SyndicationPublishOptionsDTO()
        {
        }

        public SyndicationPublishOptionsDTO(SIDateTime originalPublishDate)
        {
            _originalPublishDate = originalPublishDate;
        }

        public bool AllowChangePublishDate { get; set; }
        public int? MaxDaysBeforeOriginalPublishDate { get; set; }
        public int? MaxDaysAfterOriginalPublishDate { get; set; }

        public bool AllowChangeMessage { get; set; }
        public bool RequireMessage { get; set; }
                
        public bool AllowChangeImage { get; set; }
        public bool RequireImage { get; set; }

        public bool PickSingleImage { get; set; }

        public bool AllowChangeTargets { get; set; }
        public bool ForbidFacebookTargetChange { get; set; }
        public bool ForbidTwitterTargetChange { get; set; }
        public bool ForbidGoogleTargetChange { get; set; }
        
        public bool AllowChangeLinkURL { get; set; }
        public bool RequireLinkURL { get; set; }

        public bool AllowChangeLinkText { get; set; }
        public bool AllowChangeLinkThumb { get; set; }

        private SIDateTime _originalPublishDate = null;
        public SIDateTime OriginalPublishDate
        {
            get
            {
                return _originalPublishDate;
            }
        }

        
        public SIDateTime MinNewPublishDate
        {
            get
            {
                if (_originalPublishDate == null) return null;

                if (MaxDaysBeforeOriginalPublishDate==null)
                    return null;

                return _originalPublishDate.Subtract(new TimeSpan(MaxDaysBeforeOriginalPublishDate.Value, 0, 0, 0));
            }
        }        
        public SIDateTime MaxNewPublishDate
        {
            get
            {
                if (_originalPublishDate == null) return null;
                if (MaxDaysAfterOriginalPublishDate == null)
                    return null;

                return _originalPublishDate.Add(new TimeSpan(MaxDaysAfterOriginalPublishDate.Value, 0, 0, 0));
            }
        }
        
    }
}
