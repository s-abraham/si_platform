﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SI.DTO
{
    public class AccountOptionsDTO
    {

        public bool ViewUsers { get; set; }
        public bool AddUsers { get; set; }
        public bool EditUsers { get; set; }
        public bool RemoveUsers { get; set; }
        public bool ConfigProducts { get; set; }
        public bool ViewProducts { get; set; }
        public bool AddProducts { get; set; }
        public bool RemoveProducts { get; set; }

        public bool ViewChildren { get; set; }
        public bool AddChildren { get; set; }

        public bool RemoveFromParent { get; set; }

		public bool ViewAccountSettings { get; set; }
		public bool ViewAlerts { get; set; }
		public bool ViewSchedule { get; set; }
		public bool ViewInventory { get; set; }
		public bool ViewCompetitors { get; set; }

        public bool ViewSocialConfigTab { get; set; }
		public bool ViewSSOSecretKey { get; set; }

        public bool ViewDemoCheckbox { get; set; }

        public bool ViewExternalID { get; set; }

                
    }
}
