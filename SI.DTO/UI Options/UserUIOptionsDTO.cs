﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SI.DTO
{
    public class UserUIOptionsDTO
    {
        public bool CanManageSocialConnections { get; set; }
        public bool SupportCenterEnabled { get; set; }

        public bool AdminMenuEnabled { get; set; }
        public bool CanAddAccounts { get; set; }        
        
    }
}
