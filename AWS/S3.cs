﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Runtime.InteropServices;
using System.Runtime.Remoting.Channels;
using System.Text;
using System.Threading.Tasks;
using Amazon.Runtime;
using Amazon.S3;
using Amazon.S3.IO;
using Amazon.S3.Model;
using SI.DAL;

namespace AWS
{
    public class S3
    {
        private AmazonS3Client _client;
        private Stopwatch _sw;
        private string _socialRawTable = string.Empty;
        private AmazonS3Config _config;

        //http://stackoverflow.com/questions/1161022/storing-my-amazon-credentials-in-c-sharp-desktop-app?rq=1
        //http://dotnetwithmvc.wordpress.com/2013/10/04/c-s3-list-objects/

        /// <summary>
        /// Expl
        /// </summary>
        public S3()
        {
            CreateClient();

            _sw = new Stopwatch();
            _sw.Start();
        }

        /// <summary>
        /// Expl
        /// </summary>
        /// <param name="XXXX"></param>
        /// <param name="XXXX"></param>
        /// <param name="XXXX"></param>
        /// <param name="XXXX"></param>
        public S3(string accessKey, string secretKey, string ServiceURL, string Table)
        {
            CreateClient(accessKey, secretKey, ServiceURL, Table);

            _sw = new Stopwatch();
            _sw.Start();
        }
        
        /// <summary>
        /// Expl
        /// </summary>
        /// <param name="bucket">Name of bucket</param>
        /// <param name="subDirectory">Subdirectory Name</param>
        /// <param name="localFilePath">Path to local file</param>
        /// <returns>bool</returns>
        /// <documentation></documentation>
        private bool UploadFile(string bucket, string subDirectory, string localFilePath, string key)
        {
            var result = false;

            try
            {
                if ((!string.IsNullOrEmpty(bucket)) && (!string.IsNullOrEmpty(subDirectory)) && (!string.IsNullOrEmpty(localFilePath)) && (!string.IsNullOrEmpty(key)))
                {
                    // Create a PutObject request
                    PutObjectRequest request = new PutObjectRequest
                    {
                        BucketName = bucket,
                        Key = key,
                        FilePath = localFilePath
                    };

                    // Put object
                    PutObjectResponse response = _client.PutObject(request);

                    //var s3DirectoryInfo = new S3DirectoryInfo(_client, bucket);
                    //var directoryInfo = s3DirectoryInfo.CopyFromLocal(subDirectory);
                    //result = true;



                    result = true;
                }
                else
                {
                    throw new Exception("Invalid bucket, subDirectory, localFilePath, or key value passed");
                }
            }
            catch (AmazonS3Exception s3ex)
            {
                result = false;
                throw new Exception("AmazonS3Exception: " + s3ex.Message);
            }
            catch (Exception ex)
            {
                result = false;
                throw new Exception("S3 Class Exception: " + ex.Message);
            }

            return result;


        }

        /// <summary>
        /// Returns a list of all buckets contained in this account.
        /// </summary>
        /// <returns>List of existing bucket names</returns>
        /// <documentation>http://docs.aws.amazon.com/sdkfornet1/latest/apidocs/html/M_Amazon_S3_AmazonS3Client_ListBuckets.htm</documentation>
        public List<string> ListBuckets()
        {
            var result = new List<string>();

            try
            {
                var response = _client.ListBuckets();
                result.AddRange(response.Buckets.Select(item => item.BucketName));
            }
            catch (AmazonS3Exception s3ex)
            {
                throw new Exception("AmazonS3Exception: " + s3ex.Message);
            }
            catch (Exception ex)
            {
                throw new Exception("S3 Class Exception: " + ex.Message);
            }

            return result;
        }

        /// <summary>
        /// Creates specified sub directory the specified bucket
        /// </summary>
        /// <param name="bucket">Name of bucket</param>
        /// <param name="subDirectory">Subdirectory Name</param>
        /// <returns>bool</returns>
        /// <remarks>Returns false if error occurs but true does not mean that subdirectory has been successfully created. A separate call to VerifySubDirectoryExists() should be performed before using this subdirectory </remarks>
        /// <documentation>http://docs.aws.amazon.com/sdkfornet1/latest/apidocs/html/M_Amazon_S3_IO_S3DirectoryInfo_CreateSubdirectory.htm http://blogs.aws.amazon.com/net?ref_=pe_395770_30744850_21</documentation>
        public bool CreateBucketSubDirectory(string bucket, string subDirectory)
        {
            var result = false;
        
            try
            {
                if ((!string.IsNullOrEmpty(bucket)) && (!string.IsNullOrEmpty(subDirectory)))
                {
                    var s3DirectoryInfo = new S3DirectoryInfo(_client, bucket);
                    var directoryInfo = s3DirectoryInfo.CreateSubdirectory(subDirectory);
                    result = true;
                }
                else
                {
                    throw new Exception("Invalid bucket or subDirectory value passed");
                }
            }
            catch (AmazonS3Exception s3ex)
            {
                result = false;
                throw new Exception("AmazonS3Exception: " + s3ex.Message);
            }
            catch (Exception ex)
            {
                result = false;
                throw new Exception("S3 Class Exception: " + ex.Message);
            }
            
            return result;
        }

        /// <summary>
        /// Verifies if the specified subdirectory exists in the specified bucket
        /// </summary>
        /// <param name="bucket">Name of bucket</param>
        /// <param name="subDirectory">Subdirectory Name</param>
        /// <returns>bool</returns>
        /// <remarks>Returns false if error occurs  but true does not mean that subdirectory has been successfully created. A separate call to VerifySubDirectoryExists() should be performed before using this subdirectory </remarks>
        /// <documentation>http://docs.aws.amazon.com/sdkfornet1/latest/apidocs/html/M_Amazon_S3_IO_S3DirectoryInfo_CreateSubdirectory.htm http://blogs.aws.amazon.com/net?ref_=pe_395770_30744850_21</documentation>
        public bool VerifySubDirectoryExists(string bucket, string subDirectory)
        {
            var result = false;

            try
            {
                if ((!string.IsNullOrEmpty(bucket)) && (!string.IsNullOrEmpty(subDirectory)))
                {
                    var subDirectoryList = GetSubDirectoryList(bucket);
                    if (subDirectoryList.Contains(subDirectory))
                    {
                        result = true;
                    }
                }
                else
                {
                    throw new Exception("Invalid bucket or subDirectory value passed");
                }
            }
            catch (AmazonS3Exception s3ex)
            {
                result = false;
                throw new Exception("AmazonS3Exception: " + s3ex.Message);
            }
            catch (Exception ex)
            {
                result = false;
                throw new Exception("S3 Class Exception: " + ex.Message);
            }
            
            return result;
        }

        /// <summary>
        /// Gets a list of subdirectories contained in the specified bucket
        /// </summary>
        /// <param name="bucket">Name of bucket</param>
        /// <returns>Subdirectory names</returns>
        /// <remarks></remarks>
        /// <documentation>http://docs.aws.amazon.com/sdkfornet1/latest/apidocs/html/M_Amazon_S3_IO_S3DirectoryInfo_EnumerateDirectories_1.htm</documentation>
        public List<string> GetSubDirectoryList(string bucket)
        {
            var result = new List<string>();

            try
            {
                if (!string.IsNullOrEmpty(bucket))
                {
                    var s3DirectoryInfo = new S3DirectoryInfo(_client, bucket);
                    var subDirectories = s3DirectoryInfo.EnumerateDirectories();
                    result = subDirectories.Select(s => s.Name).ToList();
                }
                else
                {
                    throw new Exception("Invalid bucket value passed");
                }
                
            }
            catch (AmazonS3Exception s3ex)
            {
                throw new Exception("AmazonS3Exception: " + s3ex.Message);
            }
            catch (Exception ex)
            {
                throw new Exception("S3 Class Exception: " + ex.Message);
            }

            return result;
        }

        /// <summary>
        /// Gets a list of subdirectories contained in the specified bucket
        /// </summary>
        /// <param name="bucket">Name of bucket</param>
        /// <returns>List of S3DirectoryInfo objects</string></returns>
        /// <remarks></remarks>
        /// <documentation>http://docs.aws.amazon.com/sdkfornet1/latest/apidocs/html/M_Amazon_S3_IO_S3DirectoryInfo_EnumerateDirectories_1.htm</documentation>
        public List<S3DirectoryInfo> GetSubDirectoryInfoList(string bucket)
        {
            var result = new List<S3DirectoryInfo>();

            try
            {
                if (!string.IsNullOrEmpty(bucket))
                {
                    var s3DirectoryInfo = new S3DirectoryInfo(_client, bucket);
                    var subDirectories = s3DirectoryInfo.EnumerateDirectories();
                    result = subDirectories.ToList();
                }
                else
                {
                    throw new Exception("Invalid bucket value passed");
                }
            }
            catch (AmazonS3Exception s3ex)
            {
                throw new Exception("AmazonS3Exception: " + s3ex.Message);
            }
            catch (Exception ex)
            {
                throw new Exception("S3 Class Exception: " + ex.Message);
            }

            return result;
        }

        /// <summary>
        /// Gets a list of subdirectories contained in the specified bucket
        /// </summary>
        /// <param name="bucket">Name of bucket</param>
        /// <returns>List of bucket contents</string></returns>
        /// <remarks></remarks>
        /// <documentation>http://docs.aws.amazon.com/sdkfornet1/latest/apidocs/html/M_Amazon_S3_AmazonS3Client_ListObjects.htm http://ceph.com/docs/master/radosgw/s3/csharp/ http://www.example-code.com/csharp/s3.asp</documentation>
        public List<string> GetBucketContentsList(string bucket)
        {
            var result = new List<string>();

            try
            {
                if (!string.IsNullOrEmpty(bucket))
                {
                    var request = new ListObjectsRequest();
                    request.BucketName = bucket;
                    ListObjectsResponse response = _client.ListObjects(request);
                    result.AddRange(response.S3Objects.Select(o => o.Key));
                }
                else
                {
                    throw new Exception("Invalid bucket value passed");
                }
            }
            catch (AmazonS3Exception s3ex)
            {
                throw new Exception("AmazonS3Exception: " + s3ex.Message);
            }
            catch (Exception ex)
            {
                throw new Exception("S3 Class Exception: " + ex.Message);
            }

            return result;
        }

        /// <summary>
        /// Returns list of files contained in the specified bucket and specified subDirectory
        /// </summary>
        /// <param name="bucket">Name of bucket</param>
        /// <param name="subDirectory">Subdirectory Name</param>
        /// <returns>List of files</returns>
        /// <documentation>http://docs.aws.amazon.com/sdkfornet1/latest/apidocs/html/M_Amazon_S3_IO_S3DirectoryInfo_EnumerateFiles.htm</documentation>
        public List<string> GetDirectoryContentsList(string bucket, string subDirectory)
        {
            var result = new List<string>();

            try
            {
                if ((!string.IsNullOrEmpty(bucket)) && (!string.IsNullOrEmpty(subDirectory)))
                {
                    var s3DirectoryInfo = new S3DirectoryInfo(_client, bucket);
                    var directoryContents = s3DirectoryInfo.EnumerateFiles();
                    result = directoryContents.Select(s => s.Name).ToList();
                }
                else
                {
                    throw new Exception("Invalid bucket or subDirectory value passed");
                }
            }
            catch (AmazonS3Exception s3ex)
            {
                throw new Exception("AmazonS3Exception: " + s3ex.Message);
            }
            catch (Exception ex)
            {
                throw new Exception("S3 Class Exception: " + ex.Message);
            }

            return result;
        }
        
        /// <summary>
        /// Deletes specified bucket
        /// </summary>
        /// <param name="bucket">Name of bucket</param>
        /// <returns>bool</returns>
        /// <remarks>Returns false if error occurs  but true does not mean that subdirectory has been successfully deleted. A separate call to VerifySubDirectoryExists() should be performed before using this subdirectory </remarks>
        /// <documentation>http://docs.aws.amazon.com/sdkfornet1/latest/apidocs/html/M_Amazon_S3_AmazonS3Client_DeleteBucket.htm</documentation>
        public bool DeleteBucket(string bucket)
        {
            var result = false;

            try
            {
                if (!string.IsNullOrEmpty(bucket))
                {
                    var request = new DeleteBucketRequest();
                    request.BucketName = bucket;
                    _client.DeleteBucket(request);
                }
                else
                {
                    throw new Exception("Invalid bucket value passed");
                }
            }
            catch (AmazonS3Exception s3ex)
            {
                result = false;
                throw new Exception("AmazonS3Exception: " + s3ex.Message);
            }
            catch (Exception ex)
            {
                result = false;
                throw new Exception("S3 Class Exception: " + ex.Message);
            }

            return result;
        }

        /// <summary>
        /// Deletes specified object in specific bucket
        /// </summary>
        /// <param name="bucket">Name of bucket</param>
        /// <param name="subDirectory">Key</param>
        /// <returns>List of files</returns>
        /// <documentation>http://docs.aws.amazon.com/sdkfornet1/latest/apidocs/html/M_Amazon_S3_AmazonS3Client_DeleteObject.htm</documentation>
        public bool DeleteObjectByKey(string bucket, string key)
        {
            var result = false;

            try
            {
                if ((!string.IsNullOrEmpty(bucket)) && (!string.IsNullOrEmpty(key)))
                {
                    var request = new DeleteObjectRequest();
                    request.BucketName = bucket;
                    request.Key = key;
                    _client.DeleteObject(request);
                }
                else
                {
                    throw new Exception("Invalid bucket or key value passed");
                }
            }
            catch (AmazonS3Exception s3ex)
            {
                result = false;
                throw new Exception("AmazonS3Exception: " + s3ex.Message);
            }
            catch (Exception ex)
            {
                result = false;
                throw new Exception("S3 Class Exception: " + ex.Message);
            }

            return result;

            
        }
        
        private void CreateClient()
        {

            //http://docs.aws.amazon.com/AmazonS3/latest/dev/HLuploadFileDotNet.html

            const string prefix = "AWS.";
            var accessKey = GetProcessorSetting(prefix + "AWSAccessKey");
            var secretKey = GetProcessorSetting(prefix + "AWSSecretKey");
            
            AWSCredentials credentials = new BasicAWSCredentials(accessKey, secretKey);

            _config = new AmazonS3Config();
            //_config.ServiceURL = GetProcessorSetting(prefix + "S3ServiceURL");
            //_config.ServiceURL = Amazon.RegionEndpoint.USWest2;
            _config.RegionEndpoint = Amazon.RegionEndpoint.USEast1;
            //_config.RegionEndpoint = Amazon.RegionEndpoint.USWest2;
            //_client = new AmazonS3Client(credentials);
            
            //_client = new AmazonS3Client(credentials, Amazon.RegionEndpoint.USWest2);
            //_client = new AmazonS3Client(credentials, "s3-us-west-2");
            //_client = new AmazonS3Client(credentials);
            _client = new AmazonS3Client(credentials, _config);


        }

        private void CreateClient(string _accessKey, string _secretKey, string _serviceURL, string _table)
        {
            var config = new AmazonS3Config();
            const string prefix = "AWS.";

            var accessKey = _accessKey;
            var secretKey = _secretKey;

            AWSCredentials credentials = new BasicAWSCredentials(accessKey, secretKey);

            config.ServiceURL = _serviceURL;
            _socialRawTable = _table;

            _client = new AmazonS3Client(credentials, config);
        }

        private string GetProcessorSetting(string key)
        {
            Dictionary<string, string> _processorSettingsCache = null;

            if (_processorSettingsCache == null)
            {
                _processorSettingsCache = new Dictionary<string, string>();
                lock (_processorSettingsCache)
                {
                    using (SystemDBEntities db = ContextFactory.System)
                    {
                        List<ProcessorSetting> list = (from p in db.ProcessorSettings select p).ToList();
                        foreach (ProcessorSetting set in list)
                        {
                            _processorSettingsCache.Add(set.Name.Trim().ToLower(), set.Value);
                        }
                    }
                }
            }

            lock (_processorSettingsCache)
            {
                string value = null;
                _processorSettingsCache.TryGetValue(key.Trim().ToLower(), out value);
                return value;
            }
        }
    }

    class UploadObject
    {
        static string bucketName = "*** bucket name ***";
        static string keyName    = "*** key name when object is created ***";
        static string filePath   = "*** absolute path to a sample file to upload ***";

        static IAmazonS3 client;

        public static void Main(string[] args)
        {
            using (client = new AmazonS3Client(Amazon.RegionEndpoint.USEast1))
            {
                Console.WriteLine("Uploading an object");
                WritingAnObject();
            }

            Console.WriteLine("Press any key to continue...");
            Console.ReadKey();
        }

        static void WritingAnObject()
        {
            try
            {
                PutObjectRequest putRequest1 = new PutObjectRequest
                {
                    BucketName = bucketName,
                    Key = keyName,
                    ContentBody = "sample text" 
                };

                PutObjectResponse response1 = client.PutObject(putRequest1);

                // 2. Put object-set ContentType and add metadata.
                PutObjectRequest putRequest2 = new PutObjectRequest
                {
                    BucketName = bucketName,
                    Key = keyName,
                    FilePath = filePath,
                    ContentType = "text/plain"
                };
                putRequest2.Metadata.Add("x-amz-meta-title", "someTitle");
                
                PutObjectResponse response2 = client.PutObject(putRequest2);

            }
            catch (AmazonS3Exception amazonS3Exception)
            {
                if (amazonS3Exception.ErrorCode != null &&
                    (amazonS3Exception.ErrorCode.Equals("InvalidAccessKeyId")
                    ||
                    amazonS3Exception.ErrorCode.Equals("InvalidSecurity")))
                {
                    Console.WriteLine("Check the provided AWS Credentials.");
                    Console.WriteLine(
                        "For service sign up go to http://aws.amazon.com/s3");
                }
                else
                {
                    Console.WriteLine(
                        "Error occurred. Message:'{0}' when writing an object"
                        , amazonS3Exception.Message);
                }
            }
        }
    }
}
