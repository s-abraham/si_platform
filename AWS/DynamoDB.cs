﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Amazon.DynamoDBv2.Model;
using Amazon.OpsWorks.Model;
using Amazon.Runtime;
using AWS.Parameters;
using SI.DAL;

using Amazon;
using Amazon.DynamoDBv2;
using Amazon.DynamoDBv2.DocumentModel;
using Amazon.ElastiCache.Model;
using Amazon.S3;
using Amazon.S3.Model;

namespace AWS
{
    public class DynamoDB
    {
        private AmazonDynamoDBClient _client;
        private Stopwatch _sw;
        private string _socialRawTable = string.Empty;
        

        public DynamoDB()
        {
            CreateClient();

            _sw = new Stopwatch();
            _sw.Start();
        }

        public DynamoDB(string accessKey, string secretKey, string ServiceURL, string Table)
        {
            CreateClient(accessKey, secretKey, ServiceURL, Table);

            _sw = new Stopwatch();
            _sw.Start();
        }

        public DynamoDBPutResult PutValue(string value)
        {
            var result = new DynamoDBPutResult();

            result = AddSocialRawRecord(value);

            return result;
        }

        public DynamoDBGetResult GetValue(string key)
        {
            var result = new DynamoDBGetResult();

            result = GetSocialRawRecord(key);

            return result;
        }

        public DynamoDBDeleteResult DeleteKey(string key)
        {
            var result = new DynamoDBDeleteResult();

            result = DeleteSocialRawRecord(key);

            return result;
        }

        private void CreateClient()
        {
            var config = new AmazonDynamoDBConfig();
            const string prefix = "AWS.";

            var accessKey = GetProcessorSetting(prefix + "AWSAccessKey");
            var secretKey = GetProcessorSetting(prefix + "AWSSecretKey");

            AWSCredentials credentials = new BasicAWSCredentials(accessKey, secretKey);

            config.ServiceURL = GetProcessorSetting(prefix + "DynamoDBServiceURL");
            _socialRawTable = GetProcessorSetting(prefix + "DynamoDBSocialRawTable");

            //_socialRawTable = "SocialTest";
            _client = new AmazonDynamoDBClient(credentials, config);
        }

        private void CreateClient(string _accessKey, string _secretKey, string _serviceURL, string _table)
        {
            var config = new AmazonDynamoDBConfig();
            const string prefix = "AWS.";

            var accessKey = _accessKey;
            var secretKey = _secretKey;

            AWSCredentials credentials = new BasicAWSCredentials(accessKey, secretKey);

            config.ServiceURL = _serviceURL;
            _socialRawTable = _table;


            //_socialRawTable = "SocialTest";
            _client = new AmazonDynamoDBClient(credentials, config);
        }

        private void LoadSettings()
        {

        }

        private DynamoDBPutResult AddSocialRawRecord(string value)
        {
            var result = new DynamoDBPutResult();
            result.IsSuccess = false;

            try
            {
                var table = Table.LoadTable(_client, _socialRawTable);

                var socialRawDoc = new Document();
                var key = GenerateId();
                socialRawDoc["JobKey"] = key;

                if (value.Length > 63800)
                {
                    socialRawDoc["JobValue"] = Compress(value);
                    socialRawDoc["IsCompressed"] = "true";
                }
                else
                {
                    socialRawDoc["JobValue"] = value;
                    socialRawDoc["IsCompressed"] = "false";
                }
                
                socialRawDoc["InsertDate"] = DateTime.Now;
                Debug.WriteLine("Generated Id: " + key);
                table.PutItem(socialRawDoc);
                result.Key = key;
                result.IsSuccess = true;
            }
            catch (AmazonClientException ex)
            {
                result.IsSuccess = false;
                result.AWSErrorCode = ex.Message;
                result.ErrorMessage = "AWS Error: " + ex.Message;
            }
            catch (Exception ex)
            {
                result.IsSuccess = false;
                result.AWSErrorCode = "";
                result.ErrorMessage = ex.Message;
            }
            
            return result;
        }

        public static string Base64Compress(string data)
        {
            var result = string.Empty;
            var enc = Encoding.ASCII;

            using (var ms = new MemoryStream())
            {
                using (var ds = new DeflateStream(ms, CompressionMode.Compress))
                {
                    byte[] b = enc.GetBytes(data);
                    ds.Write(b, 0, b.Length);
                }

                result = Convert.ToBase64String(ms.ToArray());
            }

            return result;
        }

        public static string Compress(string text)
        {
            var result = string.Empty;
            byte[] buffer = Encoding.UTF8.GetBytes(text);
            MemoryStream ms = new MemoryStream();
            using (GZipStream zip = new GZipStream(ms, CompressionMode.Compress, true))
            {
                zip.Write(buffer, 0, buffer.Length);
            }

            ms.Position = 0;
            var outStream = new MemoryStream();

            byte[] compressed = new byte[ms.Length];
            ms.Read(compressed, 0, compressed.Length);

            byte[] gzBuffer = new byte[compressed.Length + 4];
            System.Buffer.BlockCopy(compressed, 0, gzBuffer, 4, compressed.Length);
            System.Buffer.BlockCopy(BitConverter.GetBytes(buffer.Length), 0, gzBuffer, 0, 4);
            result = Convert.ToBase64String(gzBuffer);
            return result;
        }

        public static string Decompress(string compressedText)
        {
            var result = string.Empty;

            byte[] gzBuffer = Convert.FromBase64String(compressedText);
            int buffLength = gzBuffer.Length - 4;
            using (MemoryStream ms = new MemoryStream())
            {
                int msgLength = BitConverter.ToInt32(gzBuffer, 0);
                ms.Write(gzBuffer, 4, buffLength);

                byte[] buffer = new byte[msgLength];

                ms.Position = 0;
                using (GZipStream zip = new GZipStream(ms, CompressionMode.Decompress))
                {
                    zip.Read(buffer, 0, buffer.Length);
                }

                result = Encoding.UTF8.GetString(buffer);
            }
            return result;
        }

        private DynamoDBDeleteResult DeleteSocialRawRecord(string key)
        {
            var result = new DynamoDBDeleteResult();
            result.IsSuccess = false;

            try
            {
                var request = new DeleteItemRequest
                {
                    TableName = _socialRawTable,
                    Key = new Dictionary<string, AttributeValue>()
                    {
                        { "JobKey", new AttributeValue { S = key } }
                    }
                };
                var response = _client.DeleteItem(request);

                // Check the response.
                var deleteResponse = response.DeleteItemResult;
                var attributeList = deleteResponse.Attributes; // Attribute list in the response.
                result.IsSuccess = true;
                // Print item.
                Debug.WriteLine("\nPrinting item that is just deleted ............");
                Debug.WriteLine(attributeList);
            }
            catch (AmazonClientException ex)
            {
                result.IsSuccess = false;
                result.AWSErrorCode = ex.Message;
                result.ErrorMessage = "AWS Error: " + ex.Message;
            }
            catch (Exception ex)
            {
                result.IsSuccess = false;
                result.AWSErrorCode = "";
                result.ErrorMessage = ex.Message;
            }
            
            return result;
        }

        private DynamoDBGetResult GetSocialRawRecord(string key)
        {
            var result = new DynamoDBGetResult();
            result.IsSuccess = false;
            var decompressed = string.Empty;
            try
            {
                var request = new GetItemRequest
                {
                    TableName = _socialRawTable,
                    Key = new Dictionary<string, AttributeValue>()
                    {
                        { "JobKey", new AttributeValue { S = key} }
                    },
                    AttributesToGet = new List<string>() { "JobKey", "JobValue", "InsertDate", "IsCompressed" },
                    ConsistentRead = true
                };
                var response = _client.GetItem(request);

                // Check the response.
                var getResult = response.GetItemResult;
                var attributeList = getResult.Item; // attribute list in the response.
                result.IsSuccess = true;
                result.IsCompressed = bool.Parse(getResult.Item["IsCompressed"].S);
                result.Key = key;

                if (result.IsCompressed)
                {
                    decompressed = Decompress(getResult.Item["JobValue"].S);
                    result.Value = decompressed;
                }
                else
                {
                    result.Value = getResult.Item["JobValue"].S;
                }
                
                result.InsertDate = DateTime.Parse(getResult.Item["InsertDate"].S);
                Debug.WriteLine("\nPrinting item after retrieving it ............");
                Debug.WriteLine(attributeList);
            }
            catch (AmazonClientException ex)
            {
                result.IsSuccess = false;
                result.AWSErrorCode = ex.Message;
                result.ErrorMessage = "AWS Error: " + ex.Message;
            }
            catch (Exception ex)
            {
                result.IsSuccess = false;
                result.AWSErrorCode = "";
                result.ErrorMessage = ex.Message;
            }

            return result;
        }

        private string GenerateId()

        {
            var result = string.Empty;
            result = Guid.NewGuid().ToString();
            return result;
        }

        private string GetProcessorSetting(string key)
        {
            Dictionary<string, string> _processorSettingsCache = null;

            if (_processorSettingsCache == null)
            {
                _processorSettingsCache = new Dictionary<string, string>();
                lock (_processorSettingsCache)
                {
                    using (SystemDBEntities db = ContextFactory.System)
                    {
                        List<ProcessorSetting> list = (from p in db.ProcessorSettings select p).ToList();
                        foreach (ProcessorSetting set in list)
                        {
                            _processorSettingsCache.Add(set.Name.Trim().ToLower(), set.Value);
                        }
                    }
                }
            }

            lock (_processorSettingsCache)
            {
                string value = null;
                _processorSettingsCache.TryGetValue(key.Trim().ToLower(), out value);
                return value;
            }
        }


    }
}
