﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AWS.Parameters
{
    public class DynamoResult
    {
        public bool IsSuccess { get; set; }
        public string ErrorMessage { get; set; }
        public string AWSErrorCode { get; set; }
    }
}
