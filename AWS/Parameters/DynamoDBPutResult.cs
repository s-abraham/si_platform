﻿namespace AWS.Parameters
{
    public class DynamoDBPutResult : DynamoResult
    {
        public string Key { get; set; }
    }
}