﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AWS.Parameters
{
    public class DynamoDBGetResult : DynamoResult
    {
        public string Key { get; set; }
        public string Value { get; set; }
        public bool IsCompressed { get; set; }
        public DateTime InsertDate { get; set; }
    }
}
