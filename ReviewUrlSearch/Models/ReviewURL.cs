﻿using System;
using System.Runtime.Serialization;

namespace ReviewUrlSearch.Models
{
    [Serializable, DataContract]
    public class ReviewURL
    {
        [DataMember(Order = 1)]
        public int ReviewSourceID { get; set; }
        [DataMember(Order = 2)]
        public string ReviewSourceName { get; set; }
        [DataMember(Order = 3)]
        public string URL { get; set; }

        [DataMember(Order = 4)]
        public int AccountId { get; set; }

        //[DataMember(Order = 1)]
        //public string Google {get; set;}
        //[DataMember(Order = 2)]
        //public string DealerRater  {get; set;}
        //[DataMember(Order = 3)]
        //public string YahooLocal  {get; set;}
        //[DataMember(Order = 4)]
        //public string Yelp   {get; set;}
        //[DataMember(Order = 5)]
        //public string CarDealerReviews  {get; set;}
        //[DataMember(Order = 6)]
        //public string YellowPages  {get; set;}
        //[DataMember(Order = 7)]
        //public string EdmundsReviews  {get; set;}
        //[DataMember(Order = 8)]
        //public string CarsDotCom  {get; set;}
        //[DataMember(Order = 9)]
        //public string CitySearch  {get; set;}
        //[DataMember(Order = 10)]
        //public string JudysBook { get; set; }
        //[DataMember(Order = 11)]
        //public string InsiderPages { get; set; }

    }
}