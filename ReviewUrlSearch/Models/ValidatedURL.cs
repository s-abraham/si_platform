﻿namespace ReviewUrlSearch.Models
{
    [System.Serializable, System.Runtime.Serialization.DataContract]
    public class ValidatedURL
    {
        [System.Runtime.Serialization.DataMember(Order = 1)]
        public int ReviewSourceID { get; set; }
        [System.Runtime.Serialization.DataMember(Order = 2)]
        public string ReviewSourceName { get; set; }
        [System.Runtime.Serialization.DataMember(Order = 3)]
        public string URL { get; set; }
        [System.Runtime.Serialization.DataMember(Order = 4)]
        public bool isValid { get; set; }
        public int MismatchCount { get; set; }
     }
}
