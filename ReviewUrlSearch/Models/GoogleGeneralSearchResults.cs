﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ReviewUrlSearch.Models
{
    public class GoogleGeneralSearchResults
    {
        public GoogleGeneralSearchResults()
        {
            GeneratedURL = new List<string>();
        }
        public List<string> GeneratedURL { get; set; }
    }
}
