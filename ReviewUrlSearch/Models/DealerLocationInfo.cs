﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace ReviewUrlSearch.Models
{
    [Serializable, DataContract]
    public class DealerLocationInfo
    {
        public DealerLocationInfo()
        {
            ReviewURLs = new List<ReviewURL>();
            AccountDetails = new AccountInfo();
        }

        [DataMember(Order = 1)]
        public string DealerLocationName { get; set; }
        [DataMember(Order = 2)]
        public AccountInfo AccountDetails { get; set; }
        [DataMember(Order = 3)]
        public string Rating { get; set; }
        [DataMember(Order = 4)]
        public string ReviewCount { get; set; }
        [DataMember(Order = 5)]
        public List<ReviewURL> ReviewURLs { get; set; }
    }
}
