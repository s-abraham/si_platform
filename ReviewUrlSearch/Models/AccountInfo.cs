﻿namespace ReviewUrlSearch.Models
{
    public class AccountInfo
    {
        private string _city;
        private string _state;
        public string Address { get; set; }

        public string City
        {
            get { return _city; }
            set { _city = value; }
        }

        public string State
        {
            get { return _state; }
            set { _state = value; }
        }

        public string Zip { get; set; }
        public string GeoCode { get; set; }
        public string Phone { get; set; }
        public string WebsitetURL { get; set; }
        public string FacebookURL { get; set; }
        public string TwitterURL { get; set; }
        public string GooglePlusURL { get; set; }


    }
}