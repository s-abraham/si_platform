﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ReviewUrlSearch
{
    public class Enums
    {
        public enum ReviewSource : long
        {
            All = 0,

            Google = 100,
            DealerRater = 200,
            Yahoo= 300,
            Yelp = 400,
            CarDealer = 500,
            YellowPages = 600,
            Edmunds = 700,
            CarsDotCom = 800,
            CitySearch = 900,
            JudysBook = 1000,

            InsiderPages = 1100,

            GoogleCA = 1200,
            DealerRaterCA = 1300,
            YelpCA = 1400,
            YellowPagesCA = 1500,
            JudysBookCA = 1600,

            SureCritic = 1700,

            BetterBusinessBureau = 1800,

            BetterBusinessBureauCA = 1900
        }
    }
}
