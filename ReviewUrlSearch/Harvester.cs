﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Security.Policy;
using System.Text;
using System.Threading.Tasks;
using ReviewUrlSearch.Models;
using ReviewUrlSearch.Parameters;
using ReviewUrlSearch.ReviewSources;
using SI;
using SI.DTO;
using HtmlAgilityPack;
using System.Net;
using Connectors.Parameters;


namespace ReviewUrlSearch
{
    public class Harvester : ReviewSourceProcessor
    {
        private static SearchParameters searchParameters = new SearchParameters();

        private SInet _WebNet = null;
        protected string HTMLRaw { get; set; }

        public string ExceptionMessage { get; set; }
        public List<string> FailedXPaths { get; set; }

        public static SI.BLL.Implementation.HarvestorxPaths xPaths = new SI.BLL.Implementation.HarvestorxPaths();

        public Harvester()
        {

        }
        
        public string GetReviewURL(SearchParameters searchParamters)
        {
            var result = string.Empty;
            string requestURL = string.Empty;

            xPaths = SI.BLL.ProviderFactory.Harvester.GetHarvesterSettings(searchParamters.ReviewSource);

            //RequestURL = ConstructQueryUrl(searchParamters.ReviewSource, searchParamters);

            requestURL = SetParameters(xPaths.QueryBasePath, searchParamters);

            var backupRequestURL = new GoogleGeneralSearchResults(); // ConstructGoogleSearchQueryURL("www.dealerrater.com", "dealerrater.com", searchParamters);
            
            Debug.WriteLine(requestURL);

            result = ProcessURL(searchParamters.ReviewSource, requestURL, backupRequestURL);

            return result;
        }

        //Obscelete code - Delete in future
        //private static string ConstructQueryUrl(SI.DTO.ReviewSourceEnum reviewSource, SearchParameters searchParameters)
        //{
        //    var result = string.Empty;
        //    var queryUrlPath = string.Empty;
        //    try
        //    {
        //        switch (reviewSource)
        //        {
        //            case SI.DTO.ReviewSourceEnum.CarsDotCom:
        //                result = SetParameters(_basePathParameters.Carsdotcom, reviewSource, searchParameters);
        //                break;
        //            case SI.DTO.ReviewSourceEnum.DealerRater:
        //                result = SetParameters(_basePathParameters.DealerRater, reviewSource, searchParameters);
        //                break;
        //            case SI.DTO.ReviewSourceEnum.DealerRaterCA:
        //                result = SetParameters(_basePathParameters.DealerRaterCA, reviewSource, searchParameters);
        //                break;
        //            case SI.DTO.ReviewSourceEnum.Yahoo:
        //                result = SetParameters(_basePathParameters.Yahoo, reviewSource, searchParameters);
        //                break;
        //            case SI.DTO.ReviewSourceEnum.Yelp:
        //                result = SetParameters(_basePathParameters.Yelp, reviewSource, searchParameters);
        //                break;
        //            case SI.DTO.ReviewSourceEnum.YelpCA:
        //                result = SetParameters(_basePathParameters.YelpCA, reviewSource, searchParameters);
        //                break;
        //            case SI.DTO.ReviewSourceEnum.YellowPages:
        //                result = SetParameters(_basePathParameters.YellowPages, reviewSource, searchParameters);
        //                break;
        //            case SI.DTO.ReviewSourceEnum.YellowPagesCA:
        //                result = SetParameters(_basePathParameters.YellowPagesCA, reviewSource, searchParameters);
        //                break;
        //            case SI.DTO.ReviewSourceEnum.Edmunds:
        //                result = SetParameters(_basePathParameters.Edmunds, reviewSource, searchParameters);
        //                break;
        //            case SI.DTO.ReviewSourceEnum.CitySearch:
        //                result = SetParameters(_basePathParameters.CitySearch, reviewSource, searchParameters);
        //                break;
        //            case SI.DTO.ReviewSourceEnum.JudysBook:
        //                result = SetParameters(_basePathParameters.JudysBook, reviewSource, searchParameters);
        //                break;
        //            case SI.DTO.ReviewSourceEnum.JudysBookCA:
        //                result = SetParameters(_basePathParameters.JudysBookCA, reviewSource, searchParameters);
        //                break;
        //            case SI.DTO.ReviewSourceEnum.InsiderPages:
        //                result = SetParameters(_basePathParameters.InsiderPages, reviewSource, searchParameters);
        //                break;
        //            case SI.DTO.ReviewSourceEnum.SureCritic:
        //                result = SetParameters(_basePathParameters.SureCritic, reviewSource, searchParameters);
        //                break;
        //            case SI.DTO.ReviewSourceEnum.BetterBusinessBureau:
        //                result = SetParameters(_basePathParameters.BetterBusinessBureau, reviewSource, searchParameters);
        //                break;
        //            case SI.DTO.ReviewSourceEnum.BetterBusinessBureauCA:
        //                result = SetParameters(_basePathParameters.BetterBusinessBureauCA, reviewSource, searchParameters);
        //                break;
        //            case SI.DTO.ReviewSourceEnum.CarDealer:
        //                result = SetParameters(_basePathParameters.CarDealerReviews, reviewSource, searchParameters);
        //                break;
        //            case SI.DTO.ReviewSourceEnum.Google:
        //                result = SetParameters(_basePathParameters.Google, reviewSource, searchParameters);
                        
        //                break;
                    
        //            default:
        //                throw new ArgumentOutOfRangeException("reviewSource");
        //        }
        //    }
        //    catch (Exception)
        //    {

        //        throw;
        //    }



        //    return result;
        //}

        private static GoogleGeneralSearchResults ConstructGoogleSearchQueryURL(string reviewSourceDomain, string reviewSource, SearchParameters searchParameters)
        {
            var result = new GoogleGeneralSearchResults();
            
            // Search using CompanyName and Inner Site Specific with Quotes around CompanyName
            var searchPhrase1 = "%22" + searchParameters.CompanyName.URLEncode("+") + "%22%20site:" + reviewSourceDomain.URLEncode("+");
            result.GeneratedURL.Add(xPaths.GoogleSearch.Replace("{SearchPhrase}", searchPhrase1));

            // Search using CompanyName, City, State and reviewsource with No Quotes
            var searchPhrase2 = searchParameters.CompanyName.URLEncode("+") + "+" + searchParameters.City.URLEncode("+") + "+" + searchParameters.State.URLEncode("+")
                 + "+" + reviewSource.URLEncode("+");
            result.GeneratedURL.Add(xPaths.GoogleSearch.Replace("{SearchPhrase}", searchPhrase2));

            // Search using CompanyName, City, State and Zip  and reviewsource with No Quotes
            var searchPhrase3 = searchParameters.CompanyName.URLEncode("+") + "+" + searchParameters.City.URLEncode("+") + "+" + searchParameters.State.URLEncode("+")
                 + "+" + searchParameters.Zip.URLEncode("+") + "+" + reviewSource.URLEncode("+");
            result.GeneratedURL.Add(xPaths.GoogleSearch.Replace("{SearchPhrase}", searchPhrase3));

            return result;
        }

        private static string SetParameters(string queryUrlPath, SearchParameters searchParameters)
        {
            var result = string.Empty;

            if (searchParameters.ReviewSource == SI.DTO.ReviewSourceEnum.Edmunds || searchParameters.ReviewSource == SI.DTO.ReviewSourceEnum.CitySearch || searchParameters.ReviewSource == SI.DTO.ReviewSourceEnum.JudysBook || searchParameters.ReviewSource == SI.DTO.ReviewSourceEnum.InsiderPages)
            {
                result = queryUrlPath.Replace("{LocationName}", searchParameters.CompanyName.URLEncode("+"))
                                     .Replace("{City}", searchParameters.City.URLEncode("+"))
                                     .Replace("{State}", searchParameters.State.URLEncode("+"))
                                     .Replace("{Zip}", searchParameters.Zip.URLEncode("+"));
            }
            else
            {
                result = queryUrlPath.Replace("{LocationName}", searchParameters.CompanyName.URLEncode())
                                 .Replace("{City}", searchParameters.City.URLEncode())
                                 .Replace("{State}", searchParameters.State.URLEncode())
                                 .Replace("{Zip}", searchParameters.Zip.URLEncode())
                                 .Replace("{Radius}", searchParameters.Radius);
            }
            

            return result;
        }

        private HtmlDocument RetrieveHTML(string requestUrl, string userAgent = null)
        {
            try
            {
                var htmlDoc = new HtmlDocument();
                if (_WebNet == null)
                    _WebNet = new SInet(false, 5, null, 60 * 1000);
                //_WebNet = new SInet(ConnectionParameters.UseProxy, ConnectionParameters.MaxRedirects, null, ConnectionParameters.TimeoutSeconds * 1000);

                if (!string.IsNullOrWhiteSpace(userAgent))
                {
                    _WebNet.UserAgent = userAgent;
                }

                htmlDoc = _WebNet.DownloadDocument(requestUrl, null);
                
                return htmlDoc;
            }
            catch (WebException webEx)
            {
                ExceptionMessage = webEx.Message;
            }
            return null;
        }

 
        private string ProcessURL(SI.DTO.ReviewSourceEnum reviewSource, string queryUrl, GoogleGeneralSearchResults backupQueryUrlList)
        
        {
            var result = string.Empty;
            //var xpathParameters = new SDXPathParameters();

            var htmlDoc = RetrieveHTML(queryUrl, "Mozilla/5.0 (Windows NT 6.2; WOW64; rv:25.0) Gecko/20100101 Firefox/25.0");

            result = RetrieveURLResult(htmlDoc, xPaths);


            //switch (reviewSource)
            //{
            //    case SI.DTO.ReviewSourceEnum.Google:
            //        break;
            //    case SI.DTO.ReviewSourceEnum.DealerRater:
            //        var dealerRaterProcessing = new DealerRaterProcessing();
            //        result = dealerRaterProcessing.RetrieveURLResult(htmlDoc, xPaths);
            //        break;
            //    case SI.DTO.ReviewSourceEnum.Yahoo:
            //        //result = DealerRaterProcessing.RetrieveURLResult(htmlDoc, xPaths);
            //        break;
            //    case SI.DTO.ReviewSourceEnum.Yelp:
            //        var YelpProcessing = new YelpProcessing();
            //        result = YelpProcessing.RetrieveURLResult(htmlDoc, xPaths);
            //        break;
            //    case SI.DTO.ReviewSourceEnum.CarDealer:
            //        //result = DealerRaterProcessing.RetrieveURLResult(htmlDoc, xPaths);
            //        break;
            //    case SI.DTO.ReviewSourceEnum.YellowPages:
            //        //result = DealerRaterProcessing.RetrieveURLResult(htmlDoc, xPaths);
            //        break;
            //    case SI.DTO.ReviewSourceEnum.Edmunds:
            //        //result = DealerRaterProcessing.RetrieveURLResult(htmlDoc, xPaths);
            //        break;
            //    case SI.DTO.ReviewSourceEnum.CarsDotCom:
            //        //result = DealerRaterProcessing.RetrieveURLResult(htmlDoc, xPaths);
            //        break;
            //    case SI.DTO.ReviewSourceEnum.CitySearch:
            //        //result = DealerRaterProcessing.RetrieveURLResult(htmlDoc, xPaths);
            //        break;
            //    case SI.DTO.ReviewSourceEnum.JudysBook:
            //        //result = DealerRaterProcessing.RetrieveURLResult(htmlDoc, xPaths);
            //        break;
            //    case SI.DTO.ReviewSourceEnum.InsiderPages:
            //        //result = DealerRaterProcessing.RetrieveURLResult(htmlDoc, xPaths);
            //        break;
            //    case SI.DTO.ReviewSourceEnum.GoogleCA:
            //        break;
            //    case SI.DTO.ReviewSourceEnum.DealerRaterCA:
            //        break;
            //    case SI.DTO.ReviewSourceEnum.YelpCA:
            //        break;
            //    case SI.DTO.ReviewSourceEnum.YellowPagesCA:
            //        break;
            //    case SI.DTO.ReviewSourceEnum.JudysBookCA:
            //        break;
            //    case SI.DTO.ReviewSourceEnum.SureCritic:
            //        break;
            //    case SI.DTO.ReviewSourceEnum.BetterBusinessBureau:
            //        break;
            //    case SI.DTO.ReviewSourceEnum.BetterBusinessBureauCA:
            //        break;
            //}

            return result;

        }

        private string RetrieveURLResult(HtmlDocument htmlDoc, SI.BLL.Implementation.HarvestorxPaths xPaths)
        {
            var result = string.Empty;
            string refAttribute = xPaths.QueryURLAttribute;
            string parentURL = xPaths.QueryParentURL;
            try
            {
                if (htmlDoc != null)
                {
                    HtmlNodeCollection searchNodes = SelectNodes(htmlDoc.DocumentNode, xPaths.QueryIterator, false);

                    if (searchNodes != null && searchNodes.Count() >= 1)
                    {
                        // Extract link from first result
                        result = searchNodes.FirstOrDefault().SelectSingleNode(xPaths.QueryURL).GetAttributeValue(refAttribute, "");
                    }
                    if (!string.IsNullOrEmpty(result))
                    {
                        result = parentURL + result;
                    }

                }
            }
            catch (Exception ex)
            {

            }

            return result;
        }
    }
}
