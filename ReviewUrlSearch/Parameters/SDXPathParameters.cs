﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ReviewUrlSearch.Parameters
{
    public class SDXPathParameters
    {
        public string ResultNode { get; set; }
        public string LinkNode { get; set; }
    }
}
