﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ReviewUrlSearch.Parameters
{
    public class BasePathParameters
    {
        public string GoogleSearch { get; set; }

        public string Google { get; set; }
        public string DealerRater { get; set; }
        public string Yahoo { get; set; }
        public string Yelp { get; set; }
        public string CarDealerReviews { get; set; }
        public string YellowPages { get; set; }
        public string Edmunds { get; set; }
        public string Carsdotcom { get; set; }
        public string CitySearch { get; set; }
        public string JudysBook { get; set; }

        public string InsiderPages { get; set; }

        public string GoogleCA { get; set; }
        public string DealerRaterCA { get; set; }
        public string YelpCA { get; set; }
        public string YellowPagesCA { get; set; }
        public string JudysBookCA { get; set; }

        public string SureCritic { get; set; }

        public string BetterBusinessBureau { get; set; }

        public string BetterBusinessBureauCA { get; set; }
    }
}
