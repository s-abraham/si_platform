﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SI;

namespace ReviewUrlSearch.Parameters
{
    public class SearchParameters
    {
        private string _state;
        private string _stateFull;
        private string _zip;

        public SearchParameters()
        {
            Radius = "10";
        }

        public SearchParameters(int accountId, SI.DTO.ReviewSourceEnum reviewSource, string companyName, string streetAddress, string city, string state, string zip)
        {
            AccountId = accountId;
            ReviewSource = reviewSource;
            CompanyName = companyName;
            StreetAddress = streetAddress;
            City = city;
            State = state;
            Zip = zip;
        }

        public SearchParameters( int accountId, SI.DTO.ReviewSourceEnum reviewSource, string companyName, string streetAddress, string city, string state, string zip, string phone, string radius = "10")
        {
            AccountId = accountId;
            ReviewSource = reviewSource;
            CompanyName = companyName;
            StreetAddress = streetAddress;
            City = city;
            State = state;
            Zip = zip;
            Phone = phone;
            Radius = radius;
        }

        public SI.DTO.ReviewSourceEnum ReviewSource { get; set; }

        public int AccountId { get; set; }

        public string CompanyName { get; set; }
        public string StreetAddress { get; set; }
        public string City { get; set; }

        public string State
        {
            get { return _state; }
            set
            {
                _state = value.HtmlStrip();
                if (string.IsNullOrWhiteSpace(_stateFull))
                {
                    _stateFull = SI.Lookups.GetStateFullName(_state);
                }
            }
        }

        public string StateFull
        {
            get { return _stateFull; }
            set
            {
                _stateFull = value.HtmlStrip();
                if (string.IsNullOrWhiteSpace(_state))
                {
                    _state = SI.Lookups.GetStateAbbreviation(_stateFull).ToString();
                }
            }
        }

        public string Zip
        {
            get { return _zip; }
            set { _zip = value.Length < 5 ? "0" + value : value; }
        }

        public string Phone { get; set; }
        public string Radius { get; set; }

        public Lookups.Country Country { get; set; }        
    }
}
