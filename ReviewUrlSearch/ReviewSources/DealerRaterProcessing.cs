﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SI.DTO;
using HtmlAgilityPack;

namespace ReviewUrlSearch.ReviewSources
{
    public class DealerRaterProcessing : ReviewSourceProcessor
    {
        public string RetrieveURLResult(HtmlDocument htmlDoc, SI.BLL.Implementation.HarvestorxPaths xPaths)
        {
            var result = string.Empty;

            if (htmlDoc != null)
            {
                HtmlNodeCollection searchNodes = SelectNodes(htmlDoc.DocumentNode, xPaths.QueryIterator, false);
                
                if (searchNodes != null && searchNodes.Count() >= 1)
                {
                    // Extract link from first result
                    result = searchNodes.FirstOrDefault().SelectSingleNode(xPaths.QueryURL).GetAttributeValue("data-ctorig", "");
                }
            }

            return result;
        }

    }

}