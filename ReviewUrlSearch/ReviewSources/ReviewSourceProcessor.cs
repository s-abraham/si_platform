﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using HtmlAgilityPack;
using SI;
using SI.BLL;
using SI.DTO;

namespace ReviewUrlSearch.ReviewSources
{
    public abstract class ReviewSourceProcessor
    {
        private SInet _WebNet = null;

        protected string ResultNode { get; set; }
        protected string LinkNode { get; set; }
        protected ReviewSourceEnum ReviewSource { get; set; }
        protected string HTMLRaw { get; set; }

        public string _url { get; set; }

        public ReviewSourceProcessor()
        {
            GetHarvesterSettings();
        }

        public ReviewSourceProcessor(string url, ReviewSourceEnum reviewSource)
        {
            _url = url;
            ReviewSource = reviewSource;
            GetHarvesterSettings();
        }

        //public abstract string ProcessRawQueryResults();

        protected virtual HtmlDocument GetHtmlDoc(string requestUrl)
        {
            var htmlDoc = new HtmlDocument();
            var rawHtml = string.Empty;
            if (_WebNet == null)
            {
                _WebNet = new SInet(false, 5, null, 60 * 1000);
            }

            htmlDoc = _WebNet.DownloadDocument(requestUrl, out rawHtml);

            HTMLRaw = rawHtml;

            return htmlDoc;
        }

        private void GetHarvesterSettings()
        {
            //var harvesterSettings = ProviderFactory.Harvester.GetHarvesterSettings(ReviewSource);

            //ResultNode = harvesterSettings.ResultNode;
            //LinkNode = harvesterSettings.LinkNode;
        }

        protected HtmlNode SelectSingleNode(HtmlNode node, string xPath, bool allowNull = false, List<string> failedXPaths = null)
        {
            if (node != null && !string.IsNullOrEmpty(xPath))
            {
                HtmlNode selectedNode = node.SelectSingleNode(xPath);
                if (!allowNull && selectedNode == null && failedXPaths != null)
                    failedXPaths.Add(xPath);
                return selectedNode;
            }
            return null;
        }

        protected HtmlNodeCollection SelectNodes(HtmlNode node, string xPath, bool allowNull = false, List<string> failedXPaths = null)
        {
            if (node != null && !string.IsNullOrEmpty(xPath))
            {
                HtmlNodeCollection nodeCollection = node.SelectNodes(xPath);
                if (!allowNull && nodeCollection == null && failedXPaths != null)
                    failedXPaths.Add(xPath);
                return nodeCollection;
            }
            return null;
        }

    }
}
