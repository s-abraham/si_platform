﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using HtmlAgilityPack;
using ReviewUrlSearch.Models;
using SI;
using ReviewUrlSearch.Parameters;
using SI.BLL.Implementation;
using System.Text.RegularExpressions;
using SI.DTO;
using Connectors;

namespace ReviewUrlSearch
{
    public class ReviewHarvester
    {
        #region variable

        //private static ProxyDetails _proxyDetails = new ProxyDetails();
        private SInet _WebNet = null;
        private int _requestTimeout = 200;
        private Boolean _useProxy = true;
        private string[] _retryErrorMsg = new string[1];
        private static SearchParameters searchParameters = new SearchParameters();
        private string locationNameFormated = "";

        #endregion

        #region Function

        public List<ReviewURL> IdentifyLocationUrls(string DealershipName, string PhysicalCity, string State,  string ZipCode, bool ScrapCarsDotComURL = true, int accountId = 0)
        {
            var result = string.Empty;
            List<ReviewURL> listReviewURL = new List<ReviewURL>();
            try
            {
                if ((!string.IsNullOrEmpty(DealershipName)) && (!string.IsNullOrEmpty(PhysicalCity)) && (!string.IsNullOrEmpty(State)) && (!string.IsNullOrEmpty(ZipCode)))
                {
                    string statefull = SI.Lookups.GetStateFullName(State);

                    List<ReviewURL> listReviewURL_Temp = new List<ReviewURL>();
                    ReviewURL objReviewURL;
                    string requestURL = string.Empty;

                    string queryBasePath = string.Empty;
                    HarvestorxPaths xPaths = new HarvestorxPaths();

                    #region GoogleSection
                    searchParameters.ReviewSource = SI.DTO.ReviewSourceEnum.Google;
                    xPaths = SI.BLL.ProviderFactory.Harvester.GetHarvesterSettings(searchParameters.ReviewSource);
                    queryBasePath = xPaths.QueryBasePath;

                    requestURL = ConstructQueryUrl(ReviewUrlSearch.Enums.ReviewSource.Google, DealershipName, PhysicalCity, State, Convert.ToString(ZipCode), false, queryBasePath);

                    //GetReviewURL(ReviewUrlSearch.Enums.ReviewSource.Google, requestURL, xPaths, out listReviewURL, ScrapCarsDotComURL);
                    if (listReviewURL.Count == 0)
                    {
                        requestURL = ConstructQueryUrl(ReviewUrlSearch.Enums.ReviewSource.Google, DealershipName, PhysicalCity, State, Convert.ToString(ZipCode), true, queryBasePath);
                        GetReviewURL(ReviewUrlSearch.Enums.ReviewSource.Google, requestURL, xPaths, out listReviewURL, ScrapCarsDotComURL);
                    }
                    #endregion


                    objReviewURL = new ReviewURL();
                    searchParameters.ReviewSource = SI.DTO.ReviewSourceEnum.Yelp;
                    xPaths = SI.BLL.ProviderFactory.Harvester.GetHarvesterSettings(searchParameters.ReviewSource);
                    queryBasePath = xPaths.QueryBasePath;
                    requestURL = ConstructQueryUrl(ReviewUrlSearch.Enums.ReviewSource.Yelp, DealershipName, PhysicalCity, State, Convert.ToString(ZipCode), false, queryBasePath);
                    objReviewURL.URL = GetReviewURL(ReviewUrlSearch.Enums.ReviewSource.Yelp, requestURL, xPaths, out listReviewURL_Temp);
                    objReviewURL.ReviewSourceID = Convert.ToInt32(ReviewUrlSearch.Enums.ReviewSource.Yelp);
                    objReviewURL.ReviewSourceName = Convert.ToString(ReviewUrlSearch.Enums.ReviewSource.Yelp);
                    objReviewURL.AccountId = accountId;
                    listReviewURL.Add(objReviewURL);

                    objReviewURL = new ReviewURL();
                    searchParameters.ReviewSource = SI.DTO.ReviewSourceEnum.CitySearch;
                    xPaths = SI.BLL.ProviderFactory.Harvester.GetHarvesterSettings(searchParameters.ReviewSource);
                    queryBasePath = xPaths.QueryBasePath;
                    requestURL = ConstructQueryUrl(ReviewUrlSearch.Enums.ReviewSource.CitySearch, DealershipName, PhysicalCity, State, Convert.ToString(ZipCode), false, queryBasePath);
                    objReviewURL.URL = GetReviewURL(ReviewUrlSearch.Enums.ReviewSource.CitySearch, requestURL, xPaths, out listReviewURL_Temp);
                    objReviewURL.ReviewSourceID = Convert.ToInt32(ReviewUrlSearch.Enums.ReviewSource.CitySearch);
                    objReviewURL.ReviewSourceName = Convert.ToString(ReviewUrlSearch.Enums.ReviewSource.CitySearch);
                    objReviewURL.AccountId = accountId;
                    listReviewURL.Add(objReviewURL);

                    objReviewURL = new ReviewURL();
                    searchParameters.ReviewSource = SI.DTO.ReviewSourceEnum.YellowPages;
                    xPaths = SI.BLL.ProviderFactory.Harvester.GetHarvesterSettings(searchParameters.ReviewSource);
                    queryBasePath = xPaths.QueryBasePath;
                    requestURL = ConstructQueryUrl(ReviewUrlSearch.Enums.ReviewSource.YellowPages, DealershipName, PhysicalCity, State, Convert.ToString(ZipCode), false, queryBasePath);
                    objReviewURL.URL = GetReviewURL(ReviewUrlSearch.Enums.ReviewSource.YellowPages, requestURL, xPaths, out listReviewURL_Temp);
                    objReviewURL.ReviewSourceID = Convert.ToInt32(ReviewUrlSearch.Enums.ReviewSource.YellowPages);
                    objReviewURL.ReviewSourceName = Convert.ToString(ReviewUrlSearch.Enums.ReviewSource.YellowPages);
                    objReviewURL.AccountId = accountId;
                    listReviewURL.Add(objReviewURL);

                    objReviewURL = new ReviewURL();
                    searchParameters.ReviewSource = SI.DTO.ReviewSourceEnum.JudysBook;
                    xPaths = SI.BLL.ProviderFactory.Harvester.GetHarvesterSettings(searchParameters.ReviewSource);
                    queryBasePath = xPaths.QueryBasePath;
                    requestURL = ConstructQueryUrl(ReviewUrlSearch.Enums.ReviewSource.JudysBook, DealershipName, PhysicalCity, State, Convert.ToString(ZipCode), false, queryBasePath);
                    objReviewURL.URL = GetReviewURL(ReviewUrlSearch.Enums.ReviewSource.JudysBook, requestURL, xPaths, out listReviewURL_Temp);
                    objReviewURL.ReviewSourceID = Convert.ToInt32(ReviewUrlSearch.Enums.ReviewSource.JudysBook);
                    objReviewURL.ReviewSourceName = Convert.ToString(ReviewUrlSearch.Enums.ReviewSource.JudysBook);
                    objReviewURL.AccountId = accountId;
                    listReviewURL.Add(objReviewURL);

                    ReviewURL data = new ReviewURL();
                    data = listReviewURL.Where(x => x.ReviewSourceID == Convert.ToInt32(ReviewUrlSearch.Enums.ReviewSource.CarsDotCom)).SingleOrDefault();

                    if (data == null && ScrapCarsDotComURL)
                    {
                        objReviewURL = new ReviewURL();
                        searchParameters.ReviewSource = SI.DTO.ReviewSourceEnum.CarsDotCom;
                        xPaths = SI.BLL.ProviderFactory.Harvester.GetHarvesterSettings(searchParameters.ReviewSource);
                        queryBasePath = xPaths.QueryBasePath;
                        requestURL = ConstructQueryUrl(ReviewUrlSearch.Enums.ReviewSource.CarsDotCom, DealershipName, PhysicalCity, State, Convert.ToString(ZipCode), false, queryBasePath);

                        objReviewURL.URL = GetReviewURL(ReviewUrlSearch.Enums.ReviewSource.CarsDotCom, requestURL, xPaths, out listReviewURL_Temp, false, locationNameFormated);
                        objReviewURL.ReviewSourceID = Convert.ToInt32(ReviewUrlSearch.Enums.ReviewSource.CarsDotCom);
                        objReviewURL.ReviewSourceName = Convert.ToString(ReviewUrlSearch.Enums.ReviewSource.CarsDotCom);
                        objReviewURL.AccountId = accountId;
                        listReviewURL.Add(objReviewURL);
                    }

                    data = listReviewURL.Where(x => x.ReviewSourceID == Convert.ToInt32(ReviewUrlSearch.Enums.ReviewSource.Yahoo)).SingleOrDefault();
                    if (data == null)
                    {
                        objReviewURL = new ReviewURL();
                        searchParameters.ReviewSource = SI.DTO.ReviewSourceEnum.Yahoo;
                        xPaths = SI.BLL.ProviderFactory.Harvester.GetHarvesterSettings(searchParameters.ReviewSource);
                        queryBasePath = xPaths.QueryBasePath;
                        requestURL = ConstructQueryUrl(ReviewUrlSearch.Enums.ReviewSource.Yahoo, DealershipName, PhysicalCity, State, Convert.ToString(ZipCode), false, queryBasePath);
                        objReviewURL.URL = GetReviewURL(ReviewUrlSearch.Enums.ReviewSource.Yahoo, requestURL, xPaths, out listReviewURL_Temp);
                        objReviewURL.ReviewSourceID = Convert.ToInt32(ReviewUrlSearch.Enums.ReviewSource.Yahoo);
                        objReviewURL.ReviewSourceName = Convert.ToString(ReviewUrlSearch.Enums.ReviewSource.Yahoo);
                        objReviewURL.AccountId = accountId;
                        listReviewURL.Add(objReviewURL);
                    }
                    data = listReviewURL.Where(x => x.ReviewSourceID == Convert.ToInt32(ReviewUrlSearch.Enums.ReviewSource.InsiderPages)).SingleOrDefault();
                    if (data == null)
                    {
                        objReviewURL = new ReviewURL();
                        searchParameters.ReviewSource = SI.DTO.ReviewSourceEnum.InsiderPages;
                        xPaths = SI.BLL.ProviderFactory.Harvester.GetHarvesterSettings(searchParameters.ReviewSource);
                        queryBasePath = xPaths.QueryBasePath;
                        requestURL = ConstructQueryUrl(ReviewUrlSearch.Enums.ReviewSource.InsiderPages, DealershipName, PhysicalCity, State, Convert.ToString(ZipCode), false, queryBasePath);
                        objReviewURL.URL = GetReviewURL(ReviewUrlSearch.Enums.ReviewSource.InsiderPages, requestURL, xPaths, out listReviewURL_Temp);
                        objReviewURL.ReviewSourceID = Convert.ToInt32(ReviewUrlSearch.Enums.ReviewSource.InsiderPages);
                        objReviewURL.ReviewSourceName = Convert.ToString(ReviewUrlSearch.Enums.ReviewSource.InsiderPages);
                        objReviewURL.AccountId = accountId;
                        listReviewURL.Add(objReviewURL);
                    }
                    data = listReviewURL.Where(x => x.ReviewSourceID == Convert.ToInt32(ReviewUrlSearch.Enums.ReviewSource.DealerRater)).SingleOrDefault();
                    if (data == null)
                    {
                        objReviewURL = new ReviewURL();
                        searchParameters.ReviewSource = SI.DTO.ReviewSourceEnum.DealerRater;
                        xPaths = SI.BLL.ProviderFactory.Harvester.GetHarvesterSettings(searchParameters.ReviewSource);
                        queryBasePath = xPaths.QueryBasePath;
                        requestURL = ConstructQueryUrl(ReviewUrlSearch.Enums.ReviewSource.DealerRater, DealershipName, PhysicalCity, State, Convert.ToString(ZipCode), false, queryBasePath);
                        objReviewURL.URL = GetReviewURL(ReviewUrlSearch.Enums.ReviewSource.DealerRater, requestURL, xPaths, out listReviewURL_Temp);
                        objReviewURL.ReviewSourceID = Convert.ToInt32(ReviewUrlSearch.Enums.ReviewSource.DealerRater);
                        objReviewURL.ReviewSourceName = Convert.ToString(ReviewUrlSearch.Enums.ReviewSource.DealerRater);
                        objReviewURL.AccountId = accountId;
                        listReviewURL.Add(objReviewURL);
                    }

                    data = listReviewURL.Where(x => x.ReviewSourceID == Convert.ToInt32(ReviewUrlSearch.Enums.ReviewSource.Edmunds)).SingleOrDefault();
                    if (data == null)
                    {
                        objReviewURL = new ReviewURL();
                        searchParameters.ReviewSource = SI.DTO.ReviewSourceEnum.Edmunds;
                        xPaths = SI.BLL.ProviderFactory.Harvester.GetHarvesterSettings(searchParameters.ReviewSource);
                        queryBasePath = xPaths.QueryBasePath;
                        requestURL = ConstructQueryUrl(ReviewUrlSearch.Enums.ReviewSource.Edmunds, DealershipName, PhysicalCity, State, Convert.ToString(ZipCode), false, queryBasePath);
                        objReviewURL.URL = GetReviewURL(ReviewUrlSearch.Enums.ReviewSource.Edmunds, requestURL, xPaths, out listReviewURL_Temp);
                        objReviewURL.ReviewSourceID = Convert.ToInt32(ReviewUrlSearch.Enums.ReviewSource.Edmunds);
                        objReviewURL.ReviewSourceName = Convert.ToString(ReviewUrlSearch.Enums.ReviewSource.Edmunds);
                        objReviewURL.AccountId = accountId;
                        listReviewURL.Add(objReviewURL);
                    }


                    ////objReviewURL.EdmundsReviews = "http://www.edmunds.com/dealerships/" + statefull.Replace(" ", "") + "/" + PhysicalCity.Replace(" ", "") + "/" + DealershipName.Replace(" ", "");
                    //objReviewURL = new ReviewURL();
                    //searchParameters.ReviewSource = SI.DTO.ReviewSourceEnum.Edmunds;
                    //xPaths = SI.BLL.ProviderFactory.Harvester.GetHarvesterSettings(searchParameters.ReviewSource);
                    //queryBasePath = xPaths.QueryBasePath;
                    //objReviewURL.URL = "http://www.edmunds.com/dealerships/" + statefull.Replace(" ", "") + "/" + PhysicalCity.Replace(" ", "") + "/" + DealershipName.Replace(" ", "") + "/sales.1.html";
                    //objReviewURL.ReviewSourceID = Convert.ToInt32(ReviewUrlSearch.Enums.ReviewSource.Edmunds);
                    //objReviewURL.ReviewSourceName = Convert.ToString(ReviewUrlSearch.Enums.ReviewSource.Edmunds);
                    //listReviewURL.Add(objReviewURL);

                    //objReviewURL = new ReviewURL();
                    //objReviewURL.URL = string.Empty;
                    //objReviewURL.ReviewSourceID = Convert.ToInt32(ReviewUrlSearch.Enums.ReviewSource.CarDealerReviews);
                    //objReviewURL.ReviewSourceName = Convert.ToString(ReviewUrlSearch.Enums.ReviewSource.CarDealerReviewsdmundsReviews);
                    //listReviewURL.Add(objReviewURL);
                }

            }
            catch (Exception ex)
            {

                throw;
            }

            return listReviewURL.OrderBy(o => o.ReviewSourceID).ToList();

        }

        public string IdentifyLocationUrl(SI.DTO.ReviewSourceEnum reviewSource, string DealershipName, string PhysicalCity, string State, string ZipCode, bool ScrapCarsDotComURL = true)
        {
            string statefull = SI.Lookups.GetStateFullName(State);

            var URL = string.Empty;

            ReviewURL objReviewURL;
            string requestURL = string.Empty;
            List<ReviewURL> listReviewURL_Temp = new List<ReviewURL>();

            string queryBasePath = string.Empty;
            HarvestorxPaths xPaths = new HarvestorxPaths();

            switch (reviewSource)
            {
                case ReviewSourceEnum.All:
                    {
                        #region All
                        #endregion
                    }
                    break;
                case ReviewSourceEnum.Google:
                    {
                        #region Google
                        List<ReviewURL> listReviewURL = new List<ReviewURL>();

                        searchParameters.ReviewSource = SI.DTO.ReviewSourceEnum.Google;
                        xPaths = SI.BLL.ProviderFactory.Harvester.GetHarvesterSettings(searchParameters.ReviewSource);
                        queryBasePath = xPaths.QueryBasePath;

                        requestURL = ConstructQueryUrl(ReviewUrlSearch.Enums.ReviewSource.Google, DealershipName, PhysicalCity, State, Convert.ToString(ZipCode), false, queryBasePath);

                        //GetReviewURL(ReviewUrlSearch.Enums.ReviewSource.Google, requestURL, xPaths, out listReviewURL, ScrapCarsDotComURL);
                        if (listReviewURL.Count == 0)
                        {
                            requestURL = ConstructQueryUrl(ReviewUrlSearch.Enums.ReviewSource.Google, DealershipName, PhysicalCity, State, Convert.ToString(ZipCode), true, queryBasePath);
                            GetReviewURL(ReviewUrlSearch.Enums.ReviewSource.Google, requestURL, xPaths, out listReviewURL, ScrapCarsDotComURL);
                        }

                        if (listReviewURL.Count > 0)
                        {
                            ReviewURL data = new ReviewURL();
                            data = listReviewURL.Where(x => x.ReviewSourceID == Convert.ToInt32(ReviewUrlSearch.Enums.ReviewSource.Google)).SingleOrDefault();

                            if (data != null)
                            {
                                URL = data.URL;
                            }
                        }
                        #endregion
                    }
                    break;
                case ReviewSourceEnum.DealerRater:
                    {
                        #region DealerRater
                        
                        objReviewURL = new ReviewURL();
                        searchParameters.ReviewSource = SI.DTO.ReviewSourceEnum.DealerRater;
                        xPaths = SI.BLL.ProviderFactory.Harvester.GetHarvesterSettings(searchParameters.ReviewSource);
                        queryBasePath = xPaths.QueryBasePath;
                        requestURL = ConstructQueryUrl(ReviewUrlSearch.Enums.ReviewSource.DealerRater, DealershipName, PhysicalCity, State, Convert.ToString(ZipCode), false, queryBasePath);
                        objReviewURL.URL = GetReviewURL(ReviewUrlSearch.Enums.ReviewSource.DealerRater, requestURL, xPaths, out listReviewURL_Temp);
                        objReviewURL.ReviewSourceID = Convert.ToInt32(ReviewUrlSearch.Enums.ReviewSource.DealerRater);
                        objReviewURL.ReviewSourceName = Convert.ToString(ReviewUrlSearch.Enums.ReviewSource.DealerRater);
                        //objReviewURL.AccountId = accountId;
                        //listReviewURL.Add(objReviewURL);                        
                        URL = objReviewURL.URL;

                        #endregion
                    }
                    break;
                case ReviewSourceEnum.Yahoo:
                    {
                        #region Yahoo
                        
                            objReviewURL = new ReviewURL();
                            searchParameters.ReviewSource = SI.DTO.ReviewSourceEnum.Yahoo;
                            xPaths = SI.BLL.ProviderFactory.Harvester.GetHarvesterSettings(searchParameters.ReviewSource);
                            queryBasePath = xPaths.QueryBasePath;
                            requestURL = ConstructQueryUrl(ReviewUrlSearch.Enums.ReviewSource.Yahoo, DealershipName, PhysicalCity, State, Convert.ToString(ZipCode), false, queryBasePath);
                            objReviewURL.URL = GetReviewURL(ReviewUrlSearch.Enums.ReviewSource.Yahoo, requestURL, xPaths, out listReviewURL_Temp);
                            objReviewURL.ReviewSourceID = Convert.ToInt32(ReviewUrlSearch.Enums.ReviewSource.Yahoo);
                            objReviewURL.ReviewSourceName = Convert.ToString(ReviewUrlSearch.Enums.ReviewSource.Yahoo);
                            //objReviewURL.AccountId = accountId;
                            URL = objReviewURL.URL;
                            
                        #endregion
                    }
                    break;
                case ReviewSourceEnum.Yelp:
                    {
                        #region Yelp

                        objReviewURL = new ReviewURL();
                        searchParameters.ReviewSource = SI.DTO.ReviewSourceEnum.Yelp;
                        xPaths = SI.BLL.ProviderFactory.Harvester.GetHarvesterSettings(searchParameters.ReviewSource);
                        queryBasePath = xPaths.QueryBasePath;
                        requestURL = ConstructQueryUrl(ReviewUrlSearch.Enums.ReviewSource.Yelp, DealershipName, PhysicalCity, State, Convert.ToString(ZipCode), false, queryBasePath);
                        objReviewURL.URL = GetReviewURL(ReviewUrlSearch.Enums.ReviewSource.Yelp, requestURL, xPaths, out listReviewURL_Temp);
                        objReviewURL.ReviewSourceID = Convert.ToInt32(ReviewUrlSearch.Enums.ReviewSource.Yelp);
                        objReviewURL.ReviewSourceName = Convert.ToString(ReviewUrlSearch.Enums.ReviewSource.Yelp);
                        
                        URL = objReviewURL.URL;

                        #endregion
                    }
                    break;
                case ReviewSourceEnum.CarDealer:
                    {
                        #region CarDealer

                        #endregion
                    }
                    break;
                case ReviewSourceEnum.YellowPages:
                    {
                        #region YellowPages

                        objReviewURL = new ReviewURL();
                        searchParameters.ReviewSource = SI.DTO.ReviewSourceEnum.YellowPages;
                        xPaths = SI.BLL.ProviderFactory.Harvester.GetHarvesterSettings(searchParameters.ReviewSource);
                        queryBasePath = xPaths.QueryBasePath;
                        requestURL = ConstructQueryUrl(ReviewUrlSearch.Enums.ReviewSource.YellowPages, DealershipName, PhysicalCity, State, Convert.ToString(ZipCode), false, queryBasePath);
                        objReviewURL.URL = GetReviewURL(ReviewUrlSearch.Enums.ReviewSource.YellowPages, requestURL, xPaths, out listReviewURL_Temp);
                        objReviewURL.ReviewSourceID = Convert.ToInt32(ReviewUrlSearch.Enums.ReviewSource.YellowPages);
                        objReviewURL.ReviewSourceName = Convert.ToString(ReviewUrlSearch.Enums.ReviewSource.YellowPages);
                        
                        URL = objReviewURL.URL;

                        #endregion
                    }
                    break;
                case ReviewSourceEnum.Edmunds:
                    {
                        #region Edmunds

                        objReviewURL = new ReviewURL();
                        searchParameters.ReviewSource = SI.DTO.ReviewSourceEnum.Edmunds;
                        xPaths = SI.BLL.ProviderFactory.Harvester.GetHarvesterSettings(searchParameters.ReviewSource);
                        queryBasePath = xPaths.QueryBasePath;
                        requestURL = ConstructQueryUrl(ReviewUrlSearch.Enums.ReviewSource.Edmunds, DealershipName, PhysicalCity, State, Convert.ToString(ZipCode), false, queryBasePath);
                        objReviewURL.URL = GetReviewURL(ReviewUrlSearch.Enums.ReviewSource.Edmunds, requestURL, xPaths, out listReviewURL_Temp);
                        objReviewURL.ReviewSourceID = Convert.ToInt32(ReviewUrlSearch.Enums.ReviewSource.Edmunds);
                        objReviewURL.ReviewSourceName = Convert.ToString(ReviewUrlSearch.Enums.ReviewSource.Edmunds);
                        //objReviewURL.AccountId = accountId;

                        URL = objReviewURL.URL;

                        #endregion
                    }
                    break;
                case ReviewSourceEnum.CarsDotCom:
                    {
                        #region CarsDotCom
                        
                        objReviewURL = new ReviewURL();
                        searchParameters.ReviewSource = SI.DTO.ReviewSourceEnum.CarsDotCom;
                        xPaths = SI.BLL.ProviderFactory.Harvester.GetHarvesterSettings(searchParameters.ReviewSource);
                        queryBasePath = xPaths.QueryBasePath;
                        requestURL = ConstructQueryUrl(ReviewUrlSearch.Enums.ReviewSource.CarsDotCom, DealershipName, PhysicalCity, State, Convert.ToString(ZipCode), false, queryBasePath);

                        objReviewURL.URL = GetReviewURL(ReviewUrlSearch.Enums.ReviewSource.CarsDotCom, requestURL, xPaths, out listReviewURL_Temp, false, locationNameFormated);
                        objReviewURL.ReviewSourceID = Convert.ToInt32(ReviewUrlSearch.Enums.ReviewSource.CarsDotCom);
                        objReviewURL.ReviewSourceName = Convert.ToString(ReviewUrlSearch.Enums.ReviewSource.CarsDotCom);
                        //objReviewURL.AccountId = accountId;

                        URL = objReviewURL.URL;

                        #endregion
                    }
                    break;
                case ReviewSourceEnum.CitySearch:
                    {
                        #region CitySearch

                        objReviewURL = new ReviewURL();
                        searchParameters.ReviewSource = SI.DTO.ReviewSourceEnum.CitySearch;
                        xPaths = SI.BLL.ProviderFactory.Harvester.GetHarvesterSettings(searchParameters.ReviewSource);
                        queryBasePath = xPaths.QueryBasePath;
                        requestURL = ConstructQueryUrl(ReviewUrlSearch.Enums.ReviewSource.CitySearch, DealershipName, PhysicalCity, State, Convert.ToString(ZipCode), false, queryBasePath);
                        objReviewURL.URL = GetReviewURL(ReviewUrlSearch.Enums.ReviewSource.CitySearch, requestURL, xPaths, out listReviewURL_Temp);
                        objReviewURL.ReviewSourceID = Convert.ToInt32(ReviewUrlSearch.Enums.ReviewSource.CitySearch);
                        objReviewURL.ReviewSourceName = Convert.ToString(ReviewUrlSearch.Enums.ReviewSource.CitySearch);
                        //objReviewURL.AccountId = accountId;

                        URL = objReviewURL.URL;

                        #endregion
                    }
                    break;
                case ReviewSourceEnum.JudysBook:
                    {
                        #region JudysBook

                        objReviewURL = new ReviewURL();
                        searchParameters.ReviewSource = SI.DTO.ReviewSourceEnum.JudysBook;
                        xPaths = SI.BLL.ProviderFactory.Harvester.GetHarvesterSettings(searchParameters.ReviewSource);
                        queryBasePath = xPaths.QueryBasePath;
                        requestURL = ConstructQueryUrl(ReviewUrlSearch.Enums.ReviewSource.JudysBook, DealershipName, PhysicalCity, State, Convert.ToString(ZipCode), false, queryBasePath);
                        objReviewURL.URL = GetReviewURL(ReviewUrlSearch.Enums.ReviewSource.JudysBook, requestURL, xPaths, out listReviewURL_Temp);
                        objReviewURL.ReviewSourceID = Convert.ToInt32(ReviewUrlSearch.Enums.ReviewSource.JudysBook);
                        objReviewURL.ReviewSourceName = Convert.ToString(ReviewUrlSearch.Enums.ReviewSource.JudysBook);
                        
                        URL = objReviewURL.URL;

                        #endregion
                    }
                    break;
                case ReviewSourceEnum.InsiderPages:
                    {
                        #region InsiderPages
                        
                        objReviewURL = new ReviewURL();
                        searchParameters.ReviewSource = SI.DTO.ReviewSourceEnum.InsiderPages;
                        xPaths = SI.BLL.ProviderFactory.Harvester.GetHarvesterSettings(searchParameters.ReviewSource);
                        queryBasePath = xPaths.QueryBasePath;
                        requestURL = ConstructQueryUrl(ReviewUrlSearch.Enums.ReviewSource.InsiderPages, DealershipName, PhysicalCity, State, Convert.ToString(ZipCode), false, queryBasePath);
                        objReviewURL.URL = GetReviewURL(ReviewUrlSearch.Enums.ReviewSource.InsiderPages, requestURL, xPaths, out listReviewURL_Temp);
                        objReviewURL.ReviewSourceID = Convert.ToInt32(ReviewUrlSearch.Enums.ReviewSource.InsiderPages);
                        objReviewURL.ReviewSourceName = Convert.ToString(ReviewUrlSearch.Enums.ReviewSource.InsiderPages);
                        //objReviewURL.AccountId = accountId;
                        //listReviewURL.Add(objReviewURL);

                        URL = objReviewURL.URL;

                        #endregion
                    }
                    break;
                case ReviewSourceEnum.GoogleCA:
                    {
                        #region GoogleCA

                        #endregion
                    }
                    break;
                case ReviewSourceEnum.DealerRaterCA:
                    {
                        #region DealerRaterCA

                        #endregion
                    }
                    break;
                case ReviewSourceEnum.YelpCA:
                    {
                        #region YelpCA

                        #endregion
                    }
                    break;
                case ReviewSourceEnum.YellowPagesCA:
                    {
                        #region YellowPagesCA

                        #endregion
                    }
                    break;
                case ReviewSourceEnum.JudysBookCA:
                    {
                        #region JudysBookCA

                        #endregion
                    }
                    break;
                case ReviewSourceEnum.SureCritic:
                    {
                        #region SureCritic

                        #endregion
                    }
                    break;
                case ReviewSourceEnum.BetterBusinessBureau:
                    {
                        #region BetterBusinessBureau

                        #endregion
                    }
                    break;
                case ReviewSourceEnum.BetterBusinessBureauCA:
                    {
                        #region BetterBusinessBureauCA

                        #endregion
                    }
                    break;
                default:
                    throw new ArgumentOutOfRangeException("reviewSource");
            }

            return URL;
        }

        private static string SetParameters(string queryUrlPath, SearchParameters searchParameters)
        {
            var result = string.Empty;

            if (searchParameters.ReviewSource == SI.DTO.ReviewSourceEnum.Edmunds || searchParameters.ReviewSource == SI.DTO.ReviewSourceEnum.CitySearch || searchParameters.ReviewSource == SI.DTO.ReviewSourceEnum.JudysBook || searchParameters.ReviewSource == SI.DTO.ReviewSourceEnum.InsiderPages)
            {
                result = queryUrlPath.Replace("{LocationName}", searchParameters.CompanyName.URLEncode("+"))
                                     .Replace("{City}", searchParameters.City.URLEncode("+"))
                                     .Replace("{State}", searchParameters.State.URLEncode("+"))
                                     .Replace("{Zip}", searchParameters.Zip.URLEncode("+"));
            }
            else
            {
                result = queryUrlPath.Replace("{LocationName}", searchParameters.CompanyName.URLEncode())
                                 .Replace("{City}", searchParameters.City.URLEncode())
                                 .Replace("{State}", searchParameters.State.URLEncode())
                                 .Replace("{Zip}", searchParameters.Zip.URLEncode())
                                 .Replace("{Radius}", searchParameters.Radius);
            }


            return result;
        }

        private string GetReviewURL(ReviewUrlSearch.Enums.ReviewSource reviewSource, string queryUrl, HarvestorxPaths xPaths, out List<ReviewURL> listReviewURL, bool ScrapCarsDotComURL = true, string locationName = "")
        {
            var result = string.Empty;

            //var proxyResponse = new ProxyResponse();
            listReviewURL = new List<ReviewURL>();
            ReviewURL objReviewURL = new ReviewURL();

            try
            {
                if (!string.IsNullOrEmpty(queryUrl))
                {
                    //SetProxyDetails(queryUrl);

                    //var responseHtml = Common.ExtractHtml(_proxyDetails);
                    var responseHTML = RetrieveHTML(queryUrl, "Mozilla/5.0 (Windows NT 5.1) AppleWebKit/535.2 (KHTML, like Gecko) Chrome/15.0.872.0 Safari/535.2");

                    if (!string.IsNullOrEmpty(responseHTML))
                    {
                        switch (reviewSource)
                        {
                            case ReviewUrlSearch.Enums.ReviewSource.Google:
                                result = ExtractGoogleURL(responseHTML, xPaths, out listReviewURL, ScrapCarsDotComURL);
                                break;
                            case ReviewUrlSearch.Enums.ReviewSource.DealerRater:
                                result = ExtractDealerRaterUrl(responseHTML, xPaths);
                                break;
                            case ReviewUrlSearch.Enums.ReviewSource.Yahoo:
                                result = ExtractYahooLocalURL(responseHTML, xPaths);
                                break;
                            case ReviewUrlSearch.Enums.ReviewSource.Yelp:
                                result = ExtractYelpURL(responseHTML, xPaths);
                                break;
                            case ReviewUrlSearch.Enums.ReviewSource.CarDealer:
                                //result = ExtractCarDealerURL(responseHTML, xPaths);
                                break;
                            case ReviewUrlSearch.Enums.ReviewSource.YellowPages:
                                result = ExtractYellowpagesURL(responseHTML, xPaths);
                                break;
                            case ReviewUrlSearch.Enums.ReviewSource.Edmunds:
                                result = ExtractEdmundsURL(responseHTML, xPaths);
                                break;
                            case ReviewUrlSearch.Enums.ReviewSource.CarsDotCom:
                                result = ExtractCarsDotComURL(responseHTML, xPaths, locationName);
                                break;
                            case ReviewUrlSearch.Enums.ReviewSource.CitySearch:
                                result = ExtractCitySearchURL(responseHTML, xPaths);
                                break;
                            case ReviewUrlSearch.Enums.ReviewSource.JudysBook:
                                result = ExtractJudysbookURL(responseHTML, xPaths);
                                break;
                            case ReviewUrlSearch.Enums.ReviewSource.InsiderPages:
                                //result = ExtractInsiderPagesUrl(responseHTML, xPaths);
                                break;
                            default:
                                throw new ArgumentOutOfRangeException("reviewSource");
                        }
                    }
                    else
                    {

                    }

                }
                else
                {

                }
            }
            catch (Exception)
            {

                throw;
            }
            return result;
        }

        private static string ExtractEdmundsURL(string htmlText, HarvestorxPaths xPaths)
        {
            var result = string.Empty;

            try
            {
                var doc = new HtmlDocument();
                doc.OptionFixNestedTags = true;
                doc.LoadHtml(htmlText);


                HtmlNode reviewNode = doc.DocumentNode.SelectSingleNode(xPaths.QueryIterator);

                if (reviewNode != null)
                {
                    if (reviewNode.SelectSingleNode(xPaths.QueryURL) != null)
                    {
                        result = reviewNode.SelectSingleNode(xPaths.QueryURL).GetAttributeValue(xPaths.QueryURLAttribute, "");
                        result = xPaths.QueryParentURL + result;
                    }
                    else
                    {
                        result = string.Empty;
                    }
                }
            }
            catch (Exception ex)
            {
                //throw;
            }

            return result;
        }

        private static string ExtractCarDealerURL(string htmlText, HarvestorxPaths xPaths)
        {
            var result = string.Empty;

            try
            {
                var doc = new HtmlDocument();
                doc.OptionFixNestedTags = true;
                doc.LoadHtml(htmlText);


                HtmlNode reviewNode = doc.DocumentNode.SelectSingleNode(xPaths.QueryIterator);

                if (reviewNode != null)
                {
                    result = reviewNode.SelectSingleNode(xPaths.QueryURL).GetAttributeValue(xPaths.QueryURLAttribute, "");

                    //result = xPaths.QueryParentURL + result;
                }
                else
                {
                    result = string.Empty;
                }


            }
            catch (Exception ex)
            {

                //throw;
            }

            return result;
        }

        private static string ExtractGoogleURL(string htmlText, HarvestorxPaths xPaths, out List<ReviewURL> listReviewURL, bool ScrapCarsDotComURL)
        {
            var result = string.Empty;
            var dealerRaterUrl = string.Empty;
            var yahooUrl = string.Empty;
            var insiderpagesUrl = string.Empty;
            var EdmundsUrl = string.Empty;

            //var SectionPath = Convert.ToString(ConfigurationManager.AppSettings["Google.SectionPath"]);
            //var URLPath = Convert.ToString(ConfigurationManager.AppSettings["Google.URLPath"]);
            var moreReviewsPath = Convert.ToString(ConfigurationManager.AppSettings["Google.MoreReviews"]);
            listReviewURL = new List<ReviewURL>();
            ReviewURL objReviewURL;

            try
            {
                var doc = new HtmlDocument();
                doc.OptionFixNestedTags = true;
                doc.LoadHtml(htmlText);

                //Extract Google Review URL

                var reviewNode = doc.DocumentNode.SelectSingleNode(xPaths.QueryIterator);

                var urlNode = reviewNode.SelectSingleNode(xPaths.QueryURL);
                if (urlNode != null)
                {
                    objReviewURL = new ReviewURL();
                    string googleurl = urlNode.GetAttributeValue(xPaths.QueryURLAttribute, ""); ;

                    if (!string.IsNullOrEmpty(googleurl))
                    {
                        int startindex = googleurl.IndexOf("&amp;q=") + 7;
                        int endindex = googleurl.IndexOf("about") + 5;

                        googleurl = googleurl.Substring(startindex, endindex - startindex);
                    }

                    objReviewURL.URL = googleurl;
                    objReviewURL.ReviewSourceID = Convert.ToInt32(ReviewUrlSearch.Enums.ReviewSource.Google);
                    objReviewURL.ReviewSourceName = Convert.ToString(ReviewUrlSearch.Enums.ReviewSource.Google);
                    listReviewURL.Add(objReviewURL);

                }
                //Commented out since new Google Page is Google Maps and doesn't have other review sites there.
                #region OtherReviewSourceFromGoogle
                //Determine if page contains review urls for DealerRater, Yahoo.com, InsiderPages.com, and Edmunds.com
                //var moreReviews = reviewNode.SelectSingleNode(moreReviewsPath);
                //if (!string.IsNullOrEmpty(moreReviews.InnerHtml))
                //{
                //    var nodes = moreReviews.SelectNodes(".//div[4]/span/a");
                //    foreach (var node in nodes)
                //    {
                //        try
                //        {
                //            var url = node.GetAttributeValue("href", "");

                //            if (!string.IsNullOrEmpty(url))
                //            {
                //                if (url.ToLower().Contains("www.dealerrater.com"))
                //                {
                //                    objReviewURL = new ReviewURL();
                //                    objReviewURL.URL = url.ToLower().Substring(9, url.IndexOf("&") - 9);
                //                    objReviewURL.ReviewSourceID = Convert.ToInt32(ReviewUrlSearch.Enums.ReviewSource.DealerRater);
                //                    objReviewURL.ReviewSourceName = Convert.ToString(ReviewUrlSearch.Enums.ReviewSource.DealerRater);
                //                    listReviewURL.Add(objReviewURL);
                //                }

                //                if (url.ToLower().Contains("local.yahoo.com"))
                //                {
                //                    objReviewURL = new ReviewURL();
                //                    objReviewURL.URL = url.ToLower().Substring(9, url.IndexOf("&") - 9);
                //                    objReviewURL.ReviewSourceID = Convert.ToInt32(ReviewUrlSearch.Enums.ReviewSource.Yahoo);
                //                    objReviewURL.ReviewSourceName = Convert.ToString(ReviewUrlSearch.Enums.ReviewSource.Yahoo);
                //                    listReviewURL.Add(objReviewURL);
                //                }

                //                if (url.ToLower().Contains("www.insiderpages.com"))
                //                {
                //                    objReviewURL = new ReviewURL();
                //                    objReviewURL.URL = url.ToLower().Substring(9, url.IndexOf("&") - 9);
                //                    objReviewURL.ReviewSourceID = Convert.ToInt32(ReviewUrlSearch.Enums.ReviewSource.InsiderPages);
                //                    objReviewURL.ReviewSourceName = Convert.ToString(ReviewUrlSearch.Enums.ReviewSource.InsiderPages);
                //                    listReviewURL.Add(objReviewURL);
                //                }

                //                if (url.ToLower().Contains("www.cars.com") && ScrapCarsDotComURL)
                //                {
                //                    objReviewURL = new ReviewURL();
                //                    objReviewURL.URL = url.ToLower().Substring(9, url.IndexOf("&") - 9) + "/";
                //                    objReviewURL.ReviewSourceID = Convert.ToInt32(ReviewUrlSearch.Enums.ReviewSource.CarsDotCom);
                //                    objReviewURL.ReviewSourceName = Convert.ToString(ReviewUrlSearch.Enums.ReviewSource.CarsDotCom);
                //                    listReviewURL.Add(objReviewURL);
                //                }

                //                //if (url.ToLower().Contains("www.edmunds.com"))
                //                //{
                //                //    objReviewURL.EdmundsReviews = url.ToLower().Substring(9, url.IndexOf("&") - 9);
                //                //}
                //            }

                //        }
                //        catch
                //        {

                //        }
                //    }

                //}
                #endregion

            }
            catch (Exception ex)
            {

                // Retry With New Google Base URL
                return string.Empty; //objReviewURL.Google;

            }

            return result;
        }

        private static string ExtractYelpURL(string htmlText, HarvestorxPaths xPaths)
        {
            var result = string.Empty;

            //var urlPath = Convert.ToString(ConfigurationManager.AppSettings["Yelp.URLPath"]);

            try
            {
                var doc = new HtmlDocument();
                doc.OptionFixNestedTags = true;
                doc.LoadHtml(htmlText);


                HtmlNode reviewNode = doc.DocumentNode.SelectSingleNode(xPaths.QueryIterator);

                if (reviewNode != null)
                {
                    result = reviewNode.SelectSingleNode(xPaths.QueryURL).GetAttributeValue(xPaths.QueryURLAttribute, "");

                    result = xPaths.QueryParentURL + result;
                }
                else
                {
                    result = string.Empty;
                }


            }
            catch (Exception ex)
            {

                //throw;
            }

            return result;
        }

        private static string ExtractCarsDotComURL(string htmlText, HarvestorxPaths xPaths, string locationNameFormated)
        {
            var result = string.Empty;
            string dealerName = string.Empty;
            try
            {
                var doc = new HtmlDocument();
                doc.OptionFixNestedTags = true;
                doc.LoadHtml(htmlText);

                HtmlNodeCollection details = doc.DocumentNode.SelectNodes(xPaths.QueryIterator);
                string[] locationNameComponents = locationNameFormated.Split();
                if (details != null && details.Count() > 0)
                {
                    foreach (HtmlNode node in details)
                    {
                        dealerName = node.SelectSingleNode(xPaths.QueryURL).InnerText.ToString().Trim().ToLower();
                        if (!string.IsNullOrEmpty(dealerName))
                        {
                            // check if dealerName contains all elements from input locationName input parameter
                            bool b = locationNameComponents.All(s => dealerName.Contains(s));
                            if (b == true)
                            {
                                result = node.SelectSingleNode(xPaths.QueryURL).GetAttributeValue(xPaths.QueryURLAttribute, "");
                                break;
                            }
                        }
                    }
                    if (!string.IsNullOrEmpty(result))
                    {
                        result = xPaths.QueryParentURL + result + "reviews/";
                    }
                }
            }
            catch (Exception ex)
            {
                //throw;
            }

            return result;
        }

        private static string ExtractCitySearchURL(string htmlText, HarvestorxPaths xPaths)
        {
            var result = string.Empty;

            try
            {
                var doc = new HtmlDocument();
                doc.OptionFixNestedTags = true;
                doc.LoadHtml(htmlText);

                var htmlnode = doc.DocumentNode.SelectSingleNode(xPaths.QueryURL);
                if (htmlnode != null)
                {
                    result = htmlnode.GetAttributeValue(xPaths.QueryURLAttribute, "");
                }

            }
            catch (Exception ex)
            {
                //throw;
            }

            return result;
        }

        private static string ExtractYellowpagesURL(string htmlText, HarvestorxPaths xPaths)
        {
            var result = string.Empty;

            try
            {
                var doc = new HtmlDocument();
                doc.OptionFixNestedTags = true;
                doc.LoadHtml(htmlText);

                //var htmlnode = doc.DocumentNode.SelectSingleNode("//div[@class='clearfix paid-listing result track-listing vcard'][1]");
                var htmlnode = doc.DocumentNode.SelectSingleNode(xPaths.QueryIterator);

                if (htmlnode != null)
                {
                    result = htmlnode.SelectSingleNode(xPaths.QueryURL).GetAttributeValue(xPaths.QueryURLAttribute, "");
                }


            }
            catch (Exception ex)
            {

                //throw;
            }

            return result;
        }

        private static string ExtractJudysbookURL(string htmlText, HarvestorxPaths xPaths)
        {
            var result = string.Empty;

            try
            {
                var doc = new HtmlDocument();
                doc.OptionFixNestedTags = true;
                doc.LoadHtml(htmlText);

                result = doc.DocumentNode.SelectSingleNode(xPaths.QueryURL).GetAttributeValue(xPaths.QueryURLAttribute, "");

                if (!string.IsNullOrEmpty(result))
                {
                    result = xPaths.QueryParentURL + result.Substring(6);
                    result = result.Replace("'", "").ToLower();
                }

            }
            catch (Exception ex)
            {
                //throw;
            }

            return result;
        }

        private static string ExtractYahooLocalURL(string htmlText, HarvestorxPaths xPaths)
        {
            var result = string.Empty;

            try
            {
                var doc = new HtmlDocument();
                doc.OptionFixNestedTags = true;
                doc.LoadHtml(htmlText);

                var nodes = doc.DocumentNode.SelectNodes(xPaths.QueryIterator);

                if (nodes != null && nodes.Count > 0)
                {
                    var htmlnode = nodes[0];
                    result = htmlnode.SelectSingleNode(xPaths.QueryURL).GetAttributeValue(xPaths.QueryURLAttribute, "");

                    if (result.Contains("**"))
                    {
                        result = result.Substring(result.IndexOf("**") + 2).Replace("%3a", ":");

                        if (result.Contains("dest"))
                        {
                            result = result.Substring(result.IndexOf("dest") + 5).Replace("%3a", ":");
                        }
                    }
                    else if (result.Contains("dest="))
                    {
                        result = result.Substring(result.IndexOf("dest=") + 5).Replace("%3a", ":");
                    }
                    else
                    {
                        result = result.Replace("%3a", ":");
                    }


                    result = Uri.UnescapeDataString(Uri.UnescapeDataString(result));
                    //Console.WriteLine("Unescaped string: {0}", DataString);
                }
            }
            catch (Exception ex)
            {

                //throw;
            }

            return result;
        }

        private static string ExtractInsiderPagesUrl(string htmlText, HarvestorxPaths xPaths)
        {
            var result = string.Empty;

            try
            {
                var doc = new HtmlDocument();
                doc.OptionFixNestedTags = true;
                doc.LoadHtml(htmlText);


                result = doc.DocumentNode.SelectSingleNode(xPaths.QueryURL).GetAttributeValue(xPaths.QueryURLAttribute, "");

                if (!string.IsNullOrEmpty(result))
                {
                    result = xPaths.QueryParentURL + result;
                }

            }
            catch (Exception ex)
            {
                //throw;
            }

            return result;
        }

        private static string ExtractDealerRaterUrl(string htmlText, HarvestorxPaths xPaths)
        {
            var result = string.Empty;

            try
            {
                var doc = new HtmlDocument();
                doc.OptionFixNestedTags = true;
                doc.LoadHtml(htmlText);

                var htmlnode = doc.DocumentNode.SelectSingleNode(xPaths.QueryIterator);
                if (htmlnode != null)
                {
                    result = htmlnode.SelectSingleNode(xPaths.QueryURL).GetAttributeValue(xPaths.QueryURLAttribute, "");
                }

            }
            catch (Exception ex)
            {

                //throw;
            }

            return result;
        }

        private string RetrieveHTML(string requestUrl, string userAgent = null)
        {
            string result = string.Empty;
            try
            {
                //var htmlDoc = new HtmlDocument();                

                if (_WebNet == null)
                    _WebNet = new SInet(false, 5, null, 60 * 1000);
                //_WebNet = new SInet(ConnectionParameters.UseProxy, ConnectionParameters.MaxRedirects, null, ConnectionParameters.TimeoutSeconds * 1000);

                if (!string.IsNullOrWhiteSpace(userAgent))
                {
                    _WebNet.UserAgent = userAgent;
                }

                result = _WebNet.DownloadStringCustom(requestUrl, userAgent);
            }
            catch (WebException webEx)
            {
            }

            return result;
        }

        private string ConstructQueryUrl(ReviewUrlSearch.Enums.ReviewSource reviewSource, string locationName, string city, string state, string zip, bool isReTry, string queryUrlPath)
        {
            var result = string.Empty;
            //var queryUrlPath = string.Empty;
            try
            {
                //Load Base Urls from App.Config
                if (zip.Length < 5)
                {
                    zip = "0" + zip;
                }
                switch (reviewSource)
                {
                    case ReviewUrlSearch.Enums.ReviewSource.Google:
                        result = queryUrlPath.Replace("{LocationName}", locationName)
                                             .Replace("{City}", city)
                                             .Replace("{State}", state)
                                             .Replace("{Zip}", zip);
                        break;
                    case ReviewUrlSearch.Enums.ReviewSource.DealerRater:
                        result = queryUrlPath.Replace("{LocationName}", locationName)
                                             .Replace("{City}", city)
                                             .Replace("{State}", state)
                                             .Replace("{Zip}", zip);
                        break;
                    case ReviewUrlSearch.Enums.ReviewSource.Yahoo:
                        result = queryUrlPath.Replace("{LocationName}", locationName)
                                             .Replace("{City}", city)
                                             .Replace("{State}", state)
                                             .Replace("{Zip}", zip);
                        break;
                    case ReviewUrlSearch.Enums.ReviewSource.Yelp:
                        result = queryUrlPath.Replace("{LocationName}", locationName)
                                             .Replace("{City}", city)
                                             .Replace("{State}", state)
                                             .Replace("{Zip}", zip);
                        break;
                    case ReviewUrlSearch.Enums.ReviewSource.CarDealer:

                        break;
                    case ReviewUrlSearch.Enums.ReviewSource.YellowPages:
                        result = queryUrlPath.Replace("{LocationName}", locationName)
                                             .Replace("{City}", city)
                                             .Replace("{State}", state)
                                             .Replace("{Zip}", zip);
                        break;
                    case ReviewUrlSearch.Enums.ReviewSource.Edmunds:
                        result = queryUrlPath.Replace("{LocationName}", locationName)
                                             .Replace("{City}", city)
                                             .Replace("{State}", state)
                                             .Replace("{Zip}", zip);
                        break;
                    case ReviewUrlSearch.Enums.ReviewSource.CarsDotCom:
                        var franchiseList = FranchiseList();
                        var franchise = string.Empty;

                        foreach (var item in franchiseList)
                        {
                            if (locationName.Trim().ToLower().Contains(item.ToLower()))
                            {
                                franchise = item;
                                break;
                            }
                        }

                        if (!string.IsNullOrWhiteSpace(franchise))
                        {
                            result = queryUrlPath.Replace("{Make}", franchise)
                                                 .Replace("{Zip}", zip);
                            //locationNameFormated = locationNameFormated.Replace(city, " ");
                            locationNameFormated = locationName.Replace(franchise, "").Replace(city, " ").Replace(" of ", " ").Trim().ToLower();
                            while (locationNameFormated.Contains("  ")) locationNameFormated = locationNameFormated.Replace("  ", " ");

                        }
                        break;
                    case ReviewUrlSearch.Enums.ReviewSource.CitySearch:
                        result = queryUrlPath.Replace("{LocationName}", locationName)
                                             .Replace("{City}", city)
                                             .Replace("{State}", state)
                                             .Replace("{Zip}", zip);
                        break;
                    case ReviewUrlSearch.Enums.ReviewSource.JudysBook:
                        result = queryUrlPath.Replace("{LocationName}", locationName)
                                             .Replace("{City}", city)
                                             .Replace("{State}", state)
                                             .Replace("{Zip}", zip).Replace(" ", "%20");
                        break;
                    case ReviewUrlSearch.Enums.ReviewSource.InsiderPages:
                        result = queryUrlPath.Replace("{LocationName}", locationName)
                                             .Replace("{City}", city)
                                             .Replace("{State}", state)
                                             .Replace("{Zip}", zip);

                        break;
                    default:
                        throw new ArgumentOutOfRangeException("reviewSource");
                }
            }
            catch (Exception)
            {

                throw;
            }

            return result;
        }

        private List<string> FranchiseList()
        {
            var franchiseList = new List<string>();
            franchiseList.Add("Chevrolet");
            franchiseList.Add("GMC");
            franchiseList.Add("Ford");
            franchiseList.Add("Toyota");
            franchiseList.Add("Honda");
            franchiseList.Add("Buick");
            franchiseList.Add("Jeep");
            franchiseList.Add("Dodge");
            franchiseList.Add("Chrysler");
            franchiseList.Add("Cadillac");
            franchiseList.Add("Nissan");
            franchiseList.Add("Hyundai");
            franchiseList.Add("Mazda");
            franchiseList.Add("Kia");
            franchiseList.Add("Infiniti");
            franchiseList.Add("Volvo");
            franchiseList.Add("Scion");
            franchiseList.Add("BMW");
            franchiseList.Add("Jaguar");
            franchiseList.Add("Subaru");
            franchiseList.Add("Lincoln");
            franchiseList.Add("Acura");
            franchiseList.Add("Volkswagen");
            franchiseList.Add("Mercedes-Benz");
            franchiseList.Add("Lexus");
            franchiseList.Add("Audi");
            franchiseList.Add("Porsche");
            franchiseList.Add("Ram");
            franchiseList.Add("Ferrari");
            franchiseList.Add("Fiat");
            franchiseList.Add("Suzuki");
            franchiseList.Add("Aston Martin");
            franchiseList.Add("Bentley");
            franchiseList.Add("Bugatti");
            franchiseList.Add("Lotus");
            franchiseList.Add("Maserati");
            franchiseList.Add("Maybach");
            franchiseList.Add("MINI");
            franchiseList.Add("Spyker");
            franchiseList.Add("Rolls-Royce");
            franchiseList.Add("Lamborghini");
            franchiseList.Add("Land Rover");
            franchiseList.Add("Pontiac");
            franchiseList.Add("Mitsubishi");
            franchiseList.Add("Isuzu");
            franchiseList.Add("Fisker");
            franchiseList.Add("McLaren");
            franchiseList.Add("smart");
            franchiseList.Add("Tesla");

            return franchiseList;
        }

        #endregion


    }
}
