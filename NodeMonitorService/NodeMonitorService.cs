﻿using SI;
using SI.BLL;
using SI.Core;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace NodeMonitorService
{
    public partial class NodeMonitorService : ServiceBase
    {
        AbortableBackgroundWorker _worker = null;
        PerformanceCounter _cpuCounter = new PerformanceCounter("Processor", "% Processor Time", "_Total", true);

        private int _secondsBetweenChecks = 5;
        private int _lowChecksBeforeRestart = 3;
        private int _lowCPUThreshold = 5;
        private int _minMinutesBetweenRestarts = 60;


        public NodeMonitorService()
        {
            InitializeComponent();
        }

        protected override void OnStart(string[] args)
        {
            stopMe();

            _secondsBetweenChecks = Settings.GetSetting<int>("nodemonitor.secondsbetweenchecks", 5);
            _lowChecksBeforeRestart = Settings.GetSetting<int>("nodemonitor.lowchecksbeforerestart", 5);
            _lowCPUThreshold = Settings.GetSetting<int>("nodemonitor.lowcputhreshold", 5);
            _minMinutesBetweenRestarts = Settings.GetSetting<int>("nodemonitor.minminutesbetweenrestarts", 60);

            try
            {
                _cpuCounter.NextValue();
            }
            catch (Exception)
            {
            }

            _worker = new AbortableBackgroundWorker();
            _worker.DoWork += _worker_DoWork;
            _worker.RunWorkerAsync();
        }

        void _worker_DoWork(object sender, DoWorkEventArgs e)
        {
            DateTime lastRestarted = DateTime.Now;
            int lowCPUCount = 0;
            while (true)
            {
                try
                {
                    int cpuLevel = Convert.ToInt32(_cpuCounter.NextValue());
                    if (cpuLevel <= _lowCPUThreshold)
                    {
                        lowCPUCount++;
                        if (lowCPUCount > _lowChecksBeforeRestart)
                        {
                            stopControllerService();
                            startControllerService();
                            lowCPUCount = 0;
                            lastRestarted = DateTime.Now;
                        }
                    }
                    else
                    {
                        lowCPUCount = 0;
                    }

                    int mintuesSinceRestart = Math.Abs(Convert.ToInt32((DateTime.Now - lastRestarted).TotalMinutes));
                    if (mintuesSinceRestart > _minMinutesBetweenRestarts)
                    {
                        stopControllerService();
                        startControllerService();
                        lowCPUCount = 0;
                        lastRestarted = DateTime.Now;
                    }
                }
                catch (Exception ex)
                {
                    ProviderFactory.Logging.LogException(ex);
                }
                Thread.Sleep(_secondsBetweenChecks * 1000);
            }
        }

        protected override void OnStop()
        {
            stopMe();
        }

        private static string _serviceName = "SI Controller Service";

        private void stopControllerService()
        {
            try
            {
                using (ServiceController myService = new ServiceController(_serviceName))
                {
                    string status = myService.Status.ToString().ToUpper().Trim();
                    if (status != "STOPPED")
                    {
                        myService.Stop();
                    }
                }
            }
            catch (Exception ex)
            {
                ProviderFactory.Logging.LogException(ex);
            }
            Thread.Sleep(5000);
        }
        private void startControllerService()
        {
            try
            {
                using (ServiceController myService = new ServiceController(_serviceName))
                {
                    string status = myService.Status.ToString().ToUpper().Trim();
                    if (status != "RUNNING")
                    {
                        myService.Start();
                    }
                }
            }
            catch (Exception ex)
            {
                ProviderFactory.Logging.LogException(ex);
            }
            Thread.Sleep(5000);
        }

        private void stopMe()
        {
            if (_worker != null)
            {
                try
                {
                    _worker.Abort();
                }
                catch (Exception) { }
                try
                {
                    _worker.DoWork -= _worker_DoWork;
                }
                catch (Exception) { }
                try
                {
                    _worker.Dispose();
                }
                catch (Exception) { }
                _worker = null;
            }
        }
    }
}
