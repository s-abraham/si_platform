﻿using System.Web.Security;
using SI.BLL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Routing;
using SI.DTO;
using SI.Extensions;
using SI.WEB.Portal.Framework;
using SI.WEB.Portal.Models;

namespace SI.WEB.Portal
{
    public class MvcApplication : System.Web.HttpApplication
    {
        private static ILoggingProvider _log = ProviderFactory.Logging;

        private static AssetManager _assetManager = null;
        public static AssetManager AssetManager
        {
            get
            {
                lock (_log)
                {
                    if (_assetManager == null)
                    {
                        _assetManager = new AssetManager();
                    }
                    return _assetManager;
                }
            }
        }

        protected void Application_Start()
        {
            Locale.RegisterCustomCultures();

            AreaRegistration.RegisterAllAreas();

            WebApiConfig.Register(GlobalConfiguration.Configuration);
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);

			ModelBinders.Binders.Add(typeof(SIDateTime), new SIDateTimeBinder());

			if (!Settings.GetSetting<bool>("assets.cache"))
			{
				lock (_log)
				{
					AssetManager.Rebuild();
				}
			}
			
        }
        protected void Application_BeginRequest()
        {
			//if (!Settings.GetSetting<bool>("assets.cache"))
			//{
			//	lock (_log)
			//	{
			//		AssetManager.Rebuild();
			//	}
			//}
        }

		protected void Session_Start()
		{
			if (HttpContext.Current.User.Identity.IsAuthenticated)
			{
                BaseMembersModel membersModel = new BaseMembersModel();
				if (membersModel.UserInfo != null)
				{
					ValidateUserResultDTO vUser = ProviderFactory.Security.ValidateUser(membersModel.UserInfo);
					if (vUser.Outcome != ValidateUserOutcome.Success)
						HttpContext.Current.Response.Redirect("~/Account/Login");
					else
					{
						Auditor
							.New(AuditLevelEnum.Information, AuditTypeEnum.UserActivity, vUser.UserInfo, "[{0}] Persisted Sign In to Domain [{1}]", vUser.UserInfo.EffectiveUser.Username, Request.Url.Authority)
							.SetAuditDataObject(
								AuditUserActivity
									.New(vUser.UserInfo, AuditUserActivityTypeEnum.SignedIn)
									.SetDescription("[{0}] Persisted Sign In to Domain [{1}]", vUser.UserInfo.EffectiveUser.Username, Request.Url.Authority)
									.SetUserAgent(Request.UserAgent)
							)
							.Save(ProviderFactory.Logging);
					}
				}
				else
				{
					try
					{
						FormsAuthentication.SignOut();
					}
					catch(Exception)
					{}
				}
			}
		}

        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            //filters.Add(new HandleErrorAttribute()); // default MVC setting
            filters.Add(new CustomHandleErrorAttribute());
        }

		public override string GetVaryByCustomString(HttpContext context, string custom)
		{
			if (custom == "UserIDCache")
				return context.User.Identity.Name;
			
			return base.GetVaryByCustomString(context, custom);
		}

	    protected void Application_Error()
	    {
		    Exception exception = Server.GetLastError();
            
			_log.LogException(exception);

			if (System.Diagnostics.Debugger.IsAttached || (HttpContext.Current != null && HttpContext.Current.Request.Url.Authority.Contains("localhost")))
				return;

		    Server.ClearError();
			if (HttpContext.Current != null)
				HttpContext.Current.ClearError();

			// Handle HTTP errors
		    if (exception.GetType() == typeof (HttpException))
		    {
			    Response.Redirect("/404Page.html", true);
				return;
		    }
			
		    Response.StatusDescription = "Internal Error 500";
		    Response.StatusCode = 500;
		    if (Request.Headers["X-Requested-With"] != null && Request.Headers["X-Requested-With"] == "XMLHttpRequest")
		    {
			    //Response.RedirectPermanent("/500Page.html", false);
				return;
		    }

			//kill login cookie
		    foreach (string cookieName in Request.Cookies.AllKeys)
		    {
			    Response.Cookies[cookieName].Expires = DateTime.Now.AddDays(-1);
		    }
		    
			Response.Redirect("/500Page.html", true);
	    }

    }
}