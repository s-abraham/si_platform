﻿using System;
using System.Drawing;
using System.IO;
using System.Web;
using System.Net;
using System.Web.Caching;

namespace SI.WEB.Portal.Framework
{
	public class ImageHandler : IHttpHandler
	{
		public bool IsReusable
		{
			get { return true; }
		}

		public int _width;
		public int _height;
		public static string noImageUrl = @"\assets\style\aristo\images\dots1.png";
		private HttpContext currentContext;

		public void ProcessRequest(HttpContext context)
		{
			string imageURL = context.Request.QueryString["image"];

			if (string.IsNullOrEmpty(imageURL))
				return;
			if (imageURL == "null")
				return;

			currentContext = context;

			string cacheImageName = string.Format("{0}_{1}", imageURL, context.Request.QueryString["w"]);

			//check cache for thumbnail
			if (context.Cache[cacheImageName] != null)
			{
				if (imageURL.EndsWith(".gif"))
					context.Response.ContentType = "image/gif";
				else if (imageURL.EndsWith(".png"))
					context.Response.ContentType = "image/png";
				else
					context.Response.ContentType = "image/jpeg";

				context.Response.BinaryWrite((byte[])context.Cache[cacheImageName]);
			}
			else
			{
				//get from external and cache
				Bitmap bitInput = GetImage(imageURL);
				Bitmap bitOutput;

				if (bitInput != null)
				{
					bitInput = RotateFlipImage(context, bitInput);

					if (SetHeightWidth(context, bitInput))
						bitOutput = ResizeImage(bitInput, _width, _height);
					else
						bitOutput = bitInput;

					MemoryStream mem = new MemoryStream();
					using (bitOutput)
					{
						if (imageURL.EndsWith(".gif"))
						{
							context.Response.ContentType = "image/gif";
							bitOutput.Save(mem, System.Drawing.Imaging.ImageFormat.Gif);
						}
						else if (imageURL.EndsWith(".png"))
						{
							context.Response.ContentType = "image/png";
							bitOutput.Save(mem, System.Drawing.Imaging.ImageFormat.Png);
						}
						else
						{
							context.Response.ContentType = "image/jpeg";
							bitOutput.Save(mem, System.Drawing.Imaging.ImageFormat.Jpeg);
						}
					}

					byte[] bitmapBytes = mem.ToArray();
					//place in cache for 2 hours
					context.Cache.Insert(cacheImageName, bitmapBytes, null, Cache.NoAbsoluteExpiration, TimeSpan.FromHours(2), CacheItemPriority.BelowNormal, null);
					//send to page
					context.Response.BinaryWrite(bitmapBytes);

					bitInput.Dispose();
				}
				else
				{
					//create a default image so something is displayed
					using (Stream imgStream = File.Open(context.Request.MapPath(noImageUrl), FileMode.Open, FileAccess.Read, FileShare.ReadWrite))
					{
						Bitmap defaultBitmap = new Bitmap(imgStream);
						using (defaultBitmap)
						{
							context.Response.ContentType = "image/png";
							defaultBitmap.Save(context.Response.OutputStream, System.Drawing.Imaging.ImageFormat.Png);
						}
					}
				}
			}
		}

		/// <summary>
		/// Get the image requested via the query string. 
		/// </summary>
		/// <returns>Return the requested image or the "no image" default if it does not exist.</returns>
		public Bitmap GetImage(string imageURL)
		{
			Bitmap bitOutput = null;

			if (imageURL.Contains("http://") || imageURL.Contains("https://"))
			{
				try
				{
					WebRequest req = WebRequest.Create(imageURL);
					req.Timeout = 8000;
					WebResponse response = req.GetResponse();
					Stream stream = response.GetResponseStream();
					bitOutput = new Bitmap(stream);
					stream.Close();
				}
				catch
				{
					bitOutput = null;
				}
			}
			else  //local
			{
				try
				{
					using (Stream imgStream = File.Open(currentContext.Request.MapPath(imageURL), FileMode.Open, FileAccess.Read, FileShare.ReadWrite))
					{
						bitOutput = new Bitmap(imgStream);
					}
				}
				catch
				{
					bitOutput = null;
				}
			}
			return bitOutput;
		}

		/// <summary>
		/// Set the height and width of the handler class.
		/// </summary>
		/// The context to get the query string parameters, typically current context.    
		/// The bitmap that determines the     
		/// <returns>True if image needs to be resized, false if original dimensions can be kept.</returns>
		public bool SetHeightWidth(HttpContext context, Bitmap bitInput)
		{
			double inputRatio = Convert.ToDouble(bitInput.Width) / Convert.ToDouble(bitInput.Height);

			if (!(String.IsNullOrEmpty(context.Request["w"])) && !(String.IsNullOrEmpty(context.Request["h"])))
			{
				_width = Int32.Parse(context.Request["w"]);
				_height = Int32.Parse(context.Request["h"]);
				return true;
			}
			else if (!(String.IsNullOrEmpty(context.Request["w"])))
			{
				_width = Int32.Parse(context.Request["w"]);
				_height = Convert.ToInt32((_width / inputRatio));
				return true;
			}
			else if (!(String.IsNullOrEmpty(context.Request["h"])))
			{
				_height = Int32.Parse(context.Request["h"]);
				_width = Convert.ToInt32((_height * inputRatio));
				return true;
			}
			else
			{
				_height = bitInput.Height;
				_width = bitInput.Width;
				return false;
			}
		}

		/// <summary>
		/// Flip or rotate the bitmap according to the query string parameters.
		/// </summary>
		/// The context of the query string parameters.    
		/// The bitmap to be flipped or rotated.    
		/// <returns>The bitmap after it has been flipped or rotated.</returns>
		public Bitmap RotateFlipImage(HttpContext context, Bitmap bitInput)
		{
			Bitmap bitOut = bitInput;

			if (string.IsNullOrEmpty(context.Request["RotateFlip"]))
			{
				return bitInput;
			}
			else if (context.Request["RotateFlip"] == "Rotate180flipnone")
			{
				bitOut.RotateFlip(RotateFlipType.Rotate180FlipNone);
			}
			else if (context.Request["RotateFlip"] == "Rotate180flipx")
			{
				bitOut.RotateFlip(RotateFlipType.Rotate180FlipX);
			}
			else if (context.Request["RotateFlip"] == "Rotate180flipxy")
			{
				bitOut.RotateFlip(RotateFlipType.Rotate180FlipXY);
			}
			else if (context.Request["RotateFlip"] == "Rotate180flipy")
			{
				bitOut.RotateFlip(RotateFlipType.Rotate180FlipY);
			}
			else if (context.Request["RotateFlip"] == "Rotate270flipnone")
			{
				bitOut.RotateFlip(RotateFlipType.Rotate270FlipNone);
			}
			else if (context.Request["RotateFlip"] == "Rotate270flipx")
			{
				bitOut.RotateFlip(RotateFlipType.Rotate270FlipX);
			}
			else if (context.Request["RotateFlip"] == "Rotate270FlipXY")
			{
				bitOut.RotateFlip(RotateFlipType.Rotate270FlipXY);
			}
			else if (context.Request["RotateFlip"] == "Rotate270FlipY")
			{
				bitOut.RotateFlip(RotateFlipType.Rotate270FlipY);
			}
			else if (context.Request["RotateFlip"] == "Rotate90FlipNone")
			{
				bitOut.RotateFlip(RotateFlipType.Rotate90FlipNone);
			}
			else if (context.Request["RotateFlip"] == "Rotate90FlipX")
			{
				bitOut.RotateFlip(RotateFlipType.Rotate90FlipX);
			}
			else if (context.Request["RotateFlip"] == "Rotate90FlipXY")
			{
				bitOut.RotateFlip(RotateFlipType.Rotate90FlipXY);
			}
			else if (context.Request["RotateFlip"] == "Rotate90FlipY")
			{
				bitOut.RotateFlip(RotateFlipType.Rotate90FlipY);
			}
			else if (context.Request["RotateFlip"] == "RotateNoneFlipX")
			{
				bitOut.RotateFlip(RotateFlipType.RotateNoneFlipX);
			}
			else if (context.Request["RotateFlip"] == "RotateNoneFlipXY")
			{
				bitOut.RotateFlip(RotateFlipType.RotateNoneFlipXY);
			}
			else if (context.Request["RotateFlip"] == "RotateNoneFlipY")
			{
				bitOut.RotateFlip(RotateFlipType.RotateNoneFlipY);
			}
			else
			{
				return bitInput;
			}

			return bitOut;
		}

		/// <summary>
		/// Resizes bitmap using high quality algorithms.
		/// </summary>
		///  
		private Bitmap ResizeImage(Bitmap originalImage, int newWidth, int newHeight)
		{

			Bitmap newImage = new Bitmap(originalImage, newWidth, newHeight);
			Graphics g = Graphics.FromImage(newImage);
			g.CompositingQuality = System.Drawing.Drawing2D.CompositingQuality.HighQuality;
			g.CompositingMode = System.Drawing.Drawing2D.CompositingMode.SourceCopy;
			g.InterpolationMode = System.Drawing.Drawing2D.InterpolationMode.HighQualityBicubic;
			g.DrawImage(originalImage, 0, 0, newImage.Width, newImage.Height);
			originalImage.Dispose();

			return newImage;
		}
	}

}