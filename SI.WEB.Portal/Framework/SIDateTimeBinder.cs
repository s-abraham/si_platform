﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SI.Extensions;
using SI.WEB.Portal.Models;

namespace SI.WEB.Portal.Framework
{
	public class SIDateTimeBinder : IModelBinder
	{
		public object BindModel(ControllerContext controllerContext, ModelBindingContext bindingContext)
		{
			var value = bindingContext.ValueProvider.GetValue(bindingContext.ModelName);
			var date = value.ConvertTo(typeof (DateTime), CultureInfo.CurrentCulture);
			
            BaseMembersModel model = new BaseMembersModel();
			
			return new SIDateTime((DateTime)date, model.UserInfo.EffectiveUser.ID.GetValueOrDefault());
		}
	}

}