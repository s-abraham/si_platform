﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SI.Services.Admin.Models;
using SI.Services.Social.Models;
using SI.WEB.Portal.Models;

namespace SI.WEB.Portal.Framework
{
	public static class SessionKeys
	{
		private const string _key_facebookTempToken = "_facebookTempToken";
		private const string _key_googleTempTokens = "_googleTempTokens";
		private const string _key_facebookComments = "_facebookComments";

		public static List<FacebookComment> facebookComments
		{
			get
			{
				if (HttpContext.Current.Session[_key_facebookComments] != null)
					return HttpContext.Current.Session[_key_facebookComments] as List<FacebookComment>;
				return new List<FacebookComment>();
			}
			set { HttpContext.Current.Session[_key_facebookComments] = value; }
		}
		
		public static string facebookTempToken
		{
			get
			{
				if (HttpContext.Current.Session[_key_facebookTempToken] != null)
					return HttpContext.Current.Session[_key_facebookTempToken].ToString();
				return string.Empty;
			}
			set { HttpContext.Current.Session[_key_facebookTempToken] = value; }
		}

		public static GoogleTokens googleTempTokens
		{
			get
			{
				if (HttpContext.Current.Session[_key_googleTempTokens] != null)
					return HttpContext.Current.Session[_key_googleTempTokens] as GoogleTokens;
				return null;
			}
			set
			{
				HttpContext.Current.Session[_key_googleTempTokens] = value;
			}
		}
		
	}
}