﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;


namespace SI.WEB.Portal.Framework
{
	public static class SIHelpers
	{
		public static string ModelErrorsToHtml(ModelStateDictionary ModelState)
		{
			StringBuilder modelErrors = new StringBuilder();
			modelErrors.Append("<div class='validation-summary-errors'><ul>");

			foreach (ModelError error in ModelState.Values.SelectMany(v => v.Errors))
			{
				modelErrors.AppendFormat("<li>{0}</li>", error.ErrorMessage);
			}
			modelErrors.Append("</ul></div>");

			return modelErrors.ToString();
		}

		public static string EntityErrorsToHtml(List<string> errors)
		{
			StringBuilder entityErrors = new StringBuilder();
			entityErrors.Append("<div class='validation-summary-errors'><ul>");

			foreach (string error in errors)
			{
				entityErrors.AppendFormat("<li>{0}</li>", error);
			}
			entityErrors.Append("</ul></div>");

			return entityErrors.ToString();
		}
	}
}