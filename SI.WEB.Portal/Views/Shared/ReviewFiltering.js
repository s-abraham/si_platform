﻿
var ReviewFiltering = {	
	locationName: '',
	gridSelector: '',
	datesFieldName: '',

	removeLocation: function(id) {
		$("#locationListItem_" + id).fadeOut(400, function() {
			$(this).remove();
			if ($(".location-filter-list").html().trim() == "")
				$(".filter-locations").fadeOut(100);
		});
	},

	filterDealers: function(e) {
		//console.log(e.filter);


		//var groupValue = "All";
		//if (filterViewModel.groupFilterValue != null)
		//	groupValue = filterViewModel.groupFilterValue.Text;
		//if (data.filter)
		//	return { virtualGroup: groupValue, dealerFilter: data.filter.filters[0].value };
		//return { virtualGroup: groupValue, dealerFilter: "" };

		//TODO fix this for changes to allow any dealer with no group
		//the issue here is getting the group when there are many locations

		var filter = "";
		if (e.filter)
			filter = e.filter.filters[0].value;

		return {
			virtualGroup: "All",
			dealerFilter: filter
		};

		//e.filter.filters[0].value
	},

	sitesFilterDataBound: function(e) {
		var scope = angular.element("#filterController").scope();
		scope.$apply(function() {
			if (scope.sitesFilterItems.length > 0) {
				$("#sitesFilter").data("kendoMultiSelect").value(scope.sitesFilterItems);
				scope.applyFilter();
				scope.sitesFilterItems = [];
			}
		});
	},

	locationDealersChange: function(e) {
		var scope = angular.element("#filterController").scope();
		scope.$apply(function() {
			scope.locationsFilterItems = []; //clear out any from the filter parameters
		});
	}
};


function ReviewFilterController($scope) {

	$scope.menuChange = function (e) {
		var item = $(e.item);
		var action = item.data("action");
		if (action == null) return;

		if (action != "locations")
			item.hide();
		$scope.filterAction(action);
	};

	$scope.filterAction = function (action, onInit) {
		switch (action) {
			case "locations":
				$scope.AddLocation(onInit);
				break;
			case "sites":
			case "category":
			case "state":
			case "oem":
				$scope.showTemplatedFilter(action);
				break;
			default:
				$(".filter-" + action).fadeIn();
		}
	};

	$scope.numberOfLocations = 0;
	$scope.AddLocation = function (onInit) {
		if ($(".location-filter-list").html().trim() == "")
			$(".filter-locations").fadeIn();

		$scope.numberOfLocations++;
		var locationID = "";
		var locationName = "";
		if (onInit) {
			if ($scope.locationsFilterItems.length > 0) {
				locationID = $scope.locationsFilterItems[0];
				locationName = ReviewFiltering.locationName;
			}
		}

		var templt = kendo.template($("#filter-location-template").html());
		var html = templt({ id: $scope.numberOfLocations, value: locationID, text: locationName });
		$(".location-filter-list").append(html);
	};

	$scope.showTemplatedFilter = function (filter) {
		$(".filter-" + filter).fadeIn();

		if ($("." + filter + "-filter-list").html().trim() == "") {
			var templt = kendo.template($("#filter-" + filter + "-template").html());
			var html = templt({});

			$("." + filter + "-filter-list").append(html);
		}
	};

	$scope.cancelFilter = function (filter) {
		$(".filter-" + filter).fadeOut();
		$("#actionMenu").find('[data-action="' + filter + '"]').show();

		var activefilters = 0;
		$("div[class^='filter-']").each(function () {
			if ($(this).css("display") == "block")
				activefilters++;
		})
		if (activefilters == 1)
			$scope.clearAllFilter();
	};

	$scope.resetFilters = function () {
		$("div[class^='filter-']").hide();
		$("#actionMenu").find('li').show();
		$(".location-filter-list").html("");
		$("#filterItems").html("");
		$("#filterLabel").hide();
		$scope.numberOfLocations = 0;

	};

	$scope.clearAllFilter = function () {
		$scope.resetFilters();
		$(ReviewFiltering.gridSelector).data("kendoGrid").dataSource.filter({});
	};

	$scope.applyFilter = function () {

		var filterObj = [];
		var filterLabel = [];
		$("#filterItems").html("");

		$("div[class^='filter-']").each(function () {
			var me = $(this);

			if (me.css("display") != "none") {
				var filterType = me.attr("class").replace("filter-", "");
				switch (filterType) {
					case "content":
						filterObj.push({ field: "ReviewText", operator: $scope.contentFilterTypeValue.toLowerCase(), value: $scope.contentFilterTextValue });
						filterLabel.push("<span class='ftitle'>Content</span> " + $scope.contentFilterTypeValue + " '" + $scope.contentFilterTextValue + "'");
						break;
					case "rating":
						filterObj.push({
							logic: "and",
							filters: [
								{ field: "Rating", operator: "gte", value: $scope.ratingFilterValue[0] },
								{ field: "Rating", operator: "lte", value: $scope.ratingFilterValue[1] }
							]
						});
						filterLabel.push("<span class='ftitle'>Rating</span> >= " + $scope.ratingFilterValue[0] + " and <= " + $scope.ratingFilterValue[1]);
						break;

					case "dates":
						var fieldName = ReviewFiltering.datesFieldName;

						var toDatePlus = new Date($scope.dateFilterToValue);
						toDatePlus = new Date(toDatePlus.addDays(1).addSeconds(-1));

						if ($scope.dateFilterTypeValue == "between") {
							filterObj.push({
								logic: "and",
								filters: [
									{ field: fieldName, operator: "gte", value: $scope.dateFilterFromValue },
									{ field: fieldName, operator: "lte", value: toDatePlus }
								]
							});
							filterLabel.push("<span class='ftitle'>Date</span> " + $scope.dateFilterTypeValue + " " + kendo.toString($scope.dateFilterFromValue, "M/dd/yyyy") + " and " + kendo.toString($scope.dateFilterToValue, "M/dd/yyyy"));
						} else {
							//have to handle the time frame ie. midnight forward
							if ($scope.dateFilterTypeValue == "on") {
								filterObj.push({ field: fieldName, operator: "eq", value: $scope.dateFilterFromValue });
							} else {
								if ($scope.dateFilterTypeValue == "before")
									filterObj.push({ field: fieldName, operator: "lt", value: $scope.dateFilterFromValue });
								else {
									var fromDatePlus = new Date($scope.dateFilterFromValue);
									fromDatePlus = new Date(fromDatePlus.addDays(1).addSeconds(-1));
									filterObj.push({ field: fieldName, operator: "gt", value: fromDatePlus });
								}
							}
							filterLabel.push("<span class='ftitle'>Date</span> " + $scope.dateFilterTypeValue + " " + kendo.toString($scope.dateFilterFromValue, "M/dd/yyyy"));
						}
						break;
					case "sites":
						$scope.AddMultiFilter("#sitesFilter", "SiteID", filterObj, "Site", filterLabel);
						break;
					case "oem":
						$scope.AddMultiFilter("#oemFilter", "DealerLocation.OemID", filterObj, "OEM", filterLabel);
						break;
					case "state":
						$scope.AddMultiFilter("#stateFilter", "DealerLocation.StateID", filterObj, "State", filterLabel);
						break;
					case "category":
						var selectedCategory = $("#categoryFilter").data("kendoDropDownList").value();
						filterObj.push({ field: "Category", operator: "eq", value: selectedCategory });
						filterLabel.push("<span class='ftitle'>Category</span> = " + selectedCategory);
						break;
					case "status":
						var flabel = "";
						if ($scope.statusFiltervalue != null) {
							for (var idx = 0; idx < $scope.statusFiltervalue.length; idx++) {
								var filter = $scope.statusFiltervalue[idx].split(':');
								filterObj.push({ field: filter[0], operator: "eq", value: filter[1] });

								if (flabel != "") flabel += ", ";
								if (filter[0] == "HasBeenRead" && filter[1] == "true") flabel += "Read";
								else if (filter[0] == "HasBeenRead" && filter[1] == "false") flabel += "UnRead";
								else if (filter[0] == "HasBeenResponded" && filter[1] == "true") flabel += "Responded";
								else if (filter[0] == "HasBeenResponded" && filter[1] == "false") flabel += "No Response";
								else if (filter[0] == "IsIgnored" && filter[1] == "true") flabel += "Ignored";
								else if (filter[0] == "IsFiltered" && filter[1] == "true") flabel += "Filtered";
							}
						}
						filterLabel.push("<span class='ftitle'>Status</span> = " + flabel);
						break;
					case "locations":
						if ($scope.locationsFilterItems.length > 0) {
							$scope.numberOfLocations = 1;
							filterObj.push({ field: "DealerLocation.LocationID", operator: "eq", value: $scope.locationsFilterItems[0] });
							filterLabel.push("<span class='ftitle'>Location</span> = " + ReviewFiltering.locationName);
							break;
						}

						if ($scope.numberOfLocations == 1) {
							var dropdown = $("#locationDealers_1").data("kendoComboBox");
							filterObj.push({ field: "DealerLocation.LocationID", operator: "eq", value: dropdown.value() });
							filterLabel.push("<span class='ftitle'>Location</span> = " + dropdown.text());
						} else {

							var multiFilterObj = { logic: "or", filters: [] };
							var flabel = "";
							for (var idx = 1; idx <= $scope.numberOfLocations; idx++) {
								var dropdown = $("#locationDealers_" + idx).data("kendoComboBox");
								multiFilterObj.filters.push({ field: "DealerLocation.LocationID", operator: "eq", value: dropdown.value() });

								if (flabel != "") flabel += ", ";
								flabel += dropdown.text();
							}
							filterObj.push(multiFilterObj);
							filterLabel.push("<span class='ftitle'>Location</span> = " + flabel);
						}
						break;
				}
			}
		});

		if (filterObj.length > 0) {
			for (var idx = 0; idx < filterLabel.length; idx++) {
				$("#filterItems").append("<div class='filterLabel'>" + filterLabel[idx] + "</div>");
			}
			$("#filterLabel").css("display", "inline-block");

			$(ReviewFiltering.gridSelector).data("kendoGrid").dataSource.filter(filterObj);
		}
		return filterObj;
	};

	$scope.AddMultiFilter = function (filterSel, filterField, currentFilters, name, filterLabel) {
		var filterValue = $(filterSel).data("kendoMultiSelect");
		var fValue = filterValue.value();

		if (fValue && fValue.length > 0) {
			if (fValue.length == 1) {
				currentFilters.push({ field: filterField, operator: "eq", value: fValue });
				filterLabel.push("<span class='ftitle'>" + name + "</span> = " + filterValue.dataItems()[0].Name);
			} else {
				var flabel = "";
				var multiFilterObj = { logic: "or", filters: [] };
				for (var idx = 0; idx < fValue.length; idx++) {
					multiFilterObj.filters.push({ field: filterField, operator: "eq", value: fValue[idx] });

					if (flabel != "") flabel += ", ";
					flabel += filterValue.dataItems()[idx].Name;
				}
				currentFilters.push(multiFilterObj);
				filterLabel.push("<span class='ftitle'>" + name + "</span> = " + flabel);
			}
		}
	};

	$scope.filterParameters = "";
	$scope.sitesFilterItems = [];
	$scope.locationsFilterItems = [];

	$scope.init = function (filterParameters) {
		if (filterParameters && filterParameters != "") {
			$scope.filterParameters = filterParameters + "";
			var filters = filterParameters.split('-');
			var statusFilterItems = [];

			for (idx = 0; idx < filters.length; idx++) {
				var params = filters[idx].split(',');
				if (params.length > 0) {
					var action = params[0], operator = params[1], value = params[2];

					switch (action) {
						case "dates":
							$scope.dateFilterTypeValue = operator;
							$scope.dateFilterFromValue = value;
							$("#DateToFilter").hide();
							if (operator == "between" && params.length > 3) {
								$scope.dateFilterToValue = params[3];
								$("#DateToFilter").show();
							}
							$scope.filterAction(action);
							break;
						case "rating":
							$scope.ratingFilterValue[0] = 0;
							$scope.ratingFilterValue[1] = 5;
							if (operator == "gte")
								$scope.ratingFilterValue[0] = value;
							if (operator == "lte")
								$scope.ratingFilterValue[1] = value;
							setTimeout(function () {
								$("#ratingSlider").data("kendoRangeSlider").value($scope.ratingFilterValue);
							}, 200);

							$scope.filterAction(action);
							break;
						case "status":
							statusFilterItems.push(operator + ":" + value);
							break;
						case "sites":
							$scope.sitesFilterItems.push(value);
							break;
						case "locations":
							$scope.locationsFilterItems.push(value);
							break;
					}
				}
			}

			if (statusFilterItems.length > 0) {
				$scope.statusFiltervalue = statusFilterItems;
				$scope.filterAction("status");
				setTimeout(function () {
					$("#statusFilter").data("kendoMultiSelect").value(statusFilterItems);
				}, 200)
			}

			if ($scope.sitesFilterItems.length > 0) {
				$scope.filterAction("sites");
			}

			if ($scope.locationsFilterItems.length > 0) {
				$scope.filterAction("locations", true);
			}
		}
	};

	$scope.dateFilterFromChange = function () {
		var endPicker = $("#dateFilterTo").data("kendoDatePicker"),
		startDate = this.value();

		if (startDate) {
			startDate = new Date(startDate);
			startDate.setDate(startDate.getDate() + 1);
			endPicker.min(startDate);
		}
	}

	$scope.dateFilterToChange = function () {
		var startPicker = $("#dateFilterFrom").data("kendoDatePicker"),
		endDate = this.value();

		if (endDate) {
			endDate = new Date(endDate);
			endDate.setDate(endDate.getDate() - 1);
			startPicker.max(endDate);
		}
	}

	$scope.dateFilterTypeChange = function () {
		if (this.value() == "between")
			$("#DateToFilter").show();
		else
			$("#DateToFilter").hide();
	}

	//model
	$scope.ratingFilterValue = [0, 5];
	$scope.contentFilterTypeValue = "Contains";
	$scope.contentFilterTextValue;
	$scope.dateFilterTypeValue = "between";

	var now = new Date();
	$scope.dateFilterToValue = kendo.toString(now, "MM/dd/yyyy");

	var sevenDays = now;
	sevenDays.setDate(now.getDate() - 7);
	$scope.dateFilterFromValue = kendo.toString(sevenDays, "MM/dd/yyyy");

	$scope.statusFiltervalue = [""];

};
