﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AttributeRouting.Web.Mvc;
using SI.WEB.Portal.Framework;
using SI.WEB.Portal.Models;
using SI.DTO;
using SI.BLL;
using SI.WEB.Portal.Json;

namespace SI.WEB.Portal.Controllers
{
    [Authorize]
    public class MembersController : SuperController
    {

		[Route("members/system", RouteName = "MembersSystem")]
		[CompressResponse]
		[BaseMembersModelAction("CanAdminSystem", "system")]
		public ActionResult ManageSystem()
		{
			return View();
		}


        #region data api

        #region Controllers and System Stats

		[Route("members/systemstatus")]
        [CompressResponse]
        public JsonResult SystemStatus()
        {
            try
            {
                BaseMembersModel model = new BaseMembersModel();
                SystemStatusDTO dto = ProviderFactory.Jobs.GetSystemStatus(model.UserInfo.EffectiveUser.ID.Value);

                return Json(new SystemStatusResponse() { success = true, Status = dto }, JsonRequestBehavior.AllowGet);

            }
            catch (Exception ex)
            {
                ProviderFactory.Logging.LogException(ex, Request.RawUrl);
                return Json(new BaseResponse() { success = false, title = Locale.Localize("failure"), message = Locale.Localize("system.problem") }, JsonRequestBehavior.AllowGet);
            }
        }


		[Route("members/controller")]
        [CompressResponse]
        public JsonResult Controller(ObjectRequest req)
        {
            try
            {
                // loads a base user model up
                BaseMembersModel baseModel = new BaseMembersModel();

                GetEntityDTO<ControllerDTO> getResult = null;

                if (req.id > 0)
                {
                    getResult = ProviderFactory.Jobs.GetController(baseModel.UserInfo.EffectiveUser.ID.Value, req.id);
                }

                BaseResponse model = new BaseResponse();


                if (getResult.HasProblems)
                {
                    model.success = false;
                    model.message = getResult.Problems[0];
                }
                else
                {
                    model.success = true;
                    model.entity = getResult.Entity;
                }

                return Json(model, JsonRequestBehavior.AllowGet);

            }
            catch (Exception ex)
            {
                ProviderFactory.Logging.LogException(ex, Request.RawUrl);
                return Json(new BaseResponse() { success = false, title = Locale.Localize("failure"), message = Locale.Localize("system.problem") }, JsonRequestBehavior.AllowGet);
            }

        }

		[HttpPut]
		[Route("members/controller")]
        [CompressResponse]
        public JsonResult Controller(ControllerDTO req)
        {
            try
            {
                BaseMembersModel model = new BaseMembersModel();
                SaveEntityDTO<ControllerDTO> result = ProviderFactory.Jobs.Save(new SaveEntityDTO<ControllerDTO>(model.UserInfo) { Entity = req });

            }
            catch (Exception ex)
            {
                ProviderFactory.Logging.LogException(ex, Request.RawUrl);
                return Json(new BaseResponse() { success = false, title = Locale.Localize("failure"), message = Locale.Localize("system.problem") });
            }

            return Json(new BaseResponse() { success = false, title = Locale.Localize("failure"), message = Locale.Localize("system.problem") });
        }

		[Route("members/jobtypes")]
        [CompressResponse]
        public JsonResult JobTypes(ObjectRequest req)
        {
            try
            {
                List<JobTypeDTO> theList = ProviderFactory.Jobs.GetJobTypes();
                BaseResponse model = new BaseResponse() { entity = theList, success = true, title = null, message = null };
                return Json(model, JsonRequestBehavior.AllowGet);

            }
            catch (Exception ex)
            {
                ProviderFactory.Logging.LogException(ex, Request.RawUrl);
                return Json(new BaseResponse() { success = false, title = Locale.Localize("failure"), message = Locale.Localize("system.problem") }, JsonRequestBehavior.AllowGet);
            }

        }

		[Route("members/jobtype")]
        [CompressResponse]
        public JsonResult JobType(ObjectRequest req)
        {
            try
            {
                JobTypeDTO dto = ProviderFactory.Jobs.GetJobTypes().Where(j => j.ID == req.id).FirstOrDefault();
                BaseResponse model = new BaseResponse() { entity = dto, success = true, title = null, message = null };
                return Json(model, JsonRequestBehavior.AllowGet);

            }
            catch (Exception ex)
            {
                ProviderFactory.Logging.LogException(ex, Request.RawUrl);
                return Json(new BaseResponse() { success = false, title = Locale.Localize("failure"), message = Locale.Localize("system.problem") }, JsonRequestBehavior.AllowGet);
            }

        }

		[HttpPut]
		[Route("members/jobtype")]
        [CompressResponse]
        public JsonResult JobType(JobTypeDTO req)
        {
            try
            {
                //JobTypeDTO dto = ProviderFactory.Jobs.GetJobTypes().Where(j => j.ID == req.id).FirstOrDefault();
                //BaseResponse model = new BaseResponse() { entity = dto, success = true, title = null, message = null };

                BaseMembersModel model = new BaseMembersModel();
                SaveEntityDTO<JobTypeDTO> res = ProviderFactory.Jobs.Save(new SaveEntityDTO<JobTypeDTO>(req, model.UserInfo));
                return Json(model, JsonRequestBehavior.AllowGet);

            }
            catch (Exception ex)
            {
                ProviderFactory.Logging.LogException(ex, Request.RawUrl);
                return Json(new BaseResponse() { success = false, title = Locale.Localize("failure"), message = Locale.Localize("system.problem") }, JsonRequestBehavior.AllowGet);
            }

        }

        #endregion

        #region Accounts

		[Route("members/account")]
        [CompressResponse]
        public JsonResult Account(ObjectRequest req)
        {
            try
            {
                // loads a base user model up
                BaseMembersModel baseModel = new BaseMembersModel();

                GetEntityDTO<AccountDTO, AccountOptionsDTO> getResult = null;

                if (req.id == 0)
                {
                    getResult = ProviderFactory.Security.GetNewAccount(baseModel.UserInfo, req.pid);
                }
                else
                {
                    getResult = ProviderFactory.Security.GetAccountByID(baseModel.UserInfo, req.id);
                }

                AccountModel model = new AccountModel();
                model.Opts = getResult.Options;

                if (getResult.HasProblems)
                {
                    model.success = false;
                    model.message = getResult.Problems[0];
                }
                else
                {
                    model.success = true;
                    model.Account = getResult.Entity;
                }


                return Json(model, JsonRequestBehavior.AllowGet);

            }
            catch (Exception ex)
            {
                ProviderFactory.Logging.LogException(ex, Request.RawUrl);
                return Json(new BaseResponse() { success = false, title = Locale.Localize("failure"), message = Locale.Localize("system.problem") }, JsonRequestBehavior.AllowGet);
            }

        }

		//[HttpPut]
		//[Route("members/account")]
		//[CompressResponse]
		//public JsonResult Account(AccountDTO dto)
		//{
		//	try
		//	{

		//	}
		//	catch (Exception ex)
		//	{
		//		ProviderFactory.Logging.LogException(ex, Request.RawUrl);
		//		return Json(new BaseResponse() { success = false, title = Locale.Localize("failure"), message = Locale.Localize("system.problem") });
		//	}

		//	return Json(new BaseResponse() { success = false, title = Locale.Localize("failure"), message = Locale.Localize("system.problem") });
		//}

		[HttpPost]
		[Route("members/getaccounts")]
        [CompressResponse]
        public JsonResult AccountSearch(AccountSearchRequestDTO req)
        {
            try
            {
                // need the security context to pass to the search
                BaseMembersModel model = new BaseMembersModel();

                if (req.PackageIDs == null) req.PackageIDs = new List<long>();
                if (req.FranchiseTypeIDs == null) req.FranchiseTypeIDs = new List<long>();
                if (req.AccountTypeIDs == null) req.AccountTypeIDs = new List<long>();
                req.UserInfo = model.UserInfo;

                AccountSearchResultDTO res = ProviderFactory.Security.SearchAccounts(req);

                return Json(res, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                ProviderFactory.Logging.LogException(ex, Request.RawUrl);
                return Json(new BaseResponse() { success = false, title = Locale.Localize("failure"), message = Locale.Localize("system.problem") }, JsonRequestBehavior.AllowGet);
            }
        }

		[HttpPost]
		[Route("members/setaccount")]
        [CompressResponse]
        public JsonResult AccountContextSwitch(int? id)
		{
			BaseMembersModel baseMembersModel = new BaseMembersModel();
			ProviderFactory.Security.SetUserCurrentContextAccountID(baseMembersModel.UserInfo, id.GetValueOrDefault());

            return Json(new BaseResponse() { success = true, title = "", message = "" });
        }


        #endregion



        #endregion

    }




}
