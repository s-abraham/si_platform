﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SI.WEB.Portal.Json
{
    public class ListRequest
    {
        /// <summary>
        /// The type of list to retrieve
        /// </summary>
        public string type { get; set; }
    }
}