﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SI.WEB.Portal.Json
{
    public class ListResponse<T> : BaseResponse
    {
        public string type { get; set; }
        public List<T> list { get; set; }
    }
}