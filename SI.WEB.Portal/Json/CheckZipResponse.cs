﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SI.WEB.Portal.Json
{
    public class CheckZipResponse : BaseResponse
    {
        public string stateid { get; set; }
        public string city { get; set; }
    }
}