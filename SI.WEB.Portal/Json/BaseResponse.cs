﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SI.WEB.Portal.Json
{
    public class BaseResponse
    {
        public bool success { get; set; }

        public string title { get; set; }
        public string message { get; set; }
        public object entity { get; set; }
    }
}