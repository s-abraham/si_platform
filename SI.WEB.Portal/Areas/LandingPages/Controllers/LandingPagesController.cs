﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AttributeRouting;
using AttributeRouting.Web.Mvc;
using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using SI.Services.Admin;
using SI.Services.Admin.Models;
using SI.Services.LandingPages;
using SI.Services.LandingPages.Models;
using SI.Services.Public;
using SI.WEB.Portal.Framework;
using SI.WEB.Portal.Models;
using SI.DTO;
using SI.BLL;

namespace SI.WEB.Portal.Areas.LandingPages.Controllers
{
    [Authorize]
    [RouteArea("LandingPages")]
    public class LandingPagesController : Controller
    {
        [AllowAnonymous]
        [Route("Syndication/Signup/{accesstoken}", RouteName = "SyndicationSignup")]
        public ActionResult SyndicationSignup(Guid? accesstoken)
        {
            if (accesstoken != null && accesstoken.HasValue)
            {
                if (PublicService.ValidateSSI(accesstoken.GetValueOrDefault(), Request.Url.Authority, Request.UserAgent))
                {
					AccessTokenBaseDTO accessTokenBaseDto = ProviderFactory.Security.GetAccessToken(accesstoken.Value, false);
	                if (accessTokenBaseDto != null && accessTokenBaseDto.Type() == AccessTokenTypeEnum.SyndicationInvitation)
	                {
		                BaseModel baseModel = new BaseModel();
		                ViewBag.BaseModel = baseModel;

		                string FacebookStateContext = Guid.NewGuid().ToString();
		                ViewBag.FacebookStateContext = FacebookStateContext;

		                string TwitterStateContext = Guid.NewGuid().ToString();
		                ViewBag.TwitterStateContext = TwitterStateContext;

						SyndicationInviteAccessTokenDTO syndicationInviteAccessToken = accessTokenBaseDto as SyndicationInviteAccessTokenDTO;

                        ViewBag.FacebookLoginURL = AdminService.GetSocialLoginUrl(null, syndicationInviteAccessToken.ResellerID, SocialNetworkEnum.Facebook, FacebookStateContext);
                        ViewBag.TwitterLoginURL = AdminService.GetSocialLoginUrl(null, syndicationInviteAccessToken.ResellerID, SocialNetworkEnum.Twitter, TwitterStateContext);

		                SyndicationSignup signup = LandingPagesService.GetSyndicationSignUpCustom(syndicationInviteAccessToken.SyndicatorID);

		                ViewBag.AccessToken = accesstoken.GetValueOrDefault();

						//ViewBag.AccountID = syndicationInviteAccessToken.AccountID;
						//ViewBag.PackageID = syndicationInviteAccessToken.PackageID;
						//ViewBag.ResellerID = syndicationInviteAccessToken.ResellerID;

		                return View("SyndicationSignup", signup);
	                }
                }
            }

            Request.Abort();
            return Content("");
        }

		[AllowAnonymous]
		[Route("Syndication/Registration/{accesstoken}", RouteName = "SyndicationSelfRegistration")]
		public ActionResult SyndicationSelfRegistration(Guid? accesstoken)
		{
			if (accesstoken != null && accesstoken.HasValue)
			{
				AccessTokenBaseDTO accessTokenBaseDto = ProviderFactory.Security.GetAccessToken(accesstoken.Value, false);
				if (accessTokenBaseDto != null && accessTokenBaseDto.Type() == AccessTokenTypeEnum.SyndicationSelfRegistration)
				{
					BaseModel baseModel = new BaseModel();
					ViewBag.BaseModel = baseModel;

					string FacebookStateContext = Guid.NewGuid().ToString();
					ViewBag.FacebookStateContext = FacebookStateContext;
					
					SyndicationSelfRegistrationDTO selfRegistrationDto = accessTokenBaseDto as SyndicationSelfRegistrationDTO;
					SyndicationRegistration syndicationRegistration = LandingPagesService.PopulateSyndicationRegistration(selfRegistrationDto);

					return View("SyndicationSelfRegistration", syndicationRegistration);
				}
			}

			Request.Abort();
			return Content("");
		}

		[AllowAnonymous]
		[HttpPost]
		public ActionResult SaveSyndicationRegistration(SyndicationRegistration syndicationRegistration)
		{
			string modelErrors = "";
			bool success = false;

			if (ModelState.IsValid)
			{
				List<string> Problems = new List<string>();
				success =  LandingPagesService.SaveSyndicationRegistration(syndicationRegistration, SessionKeys.facebookTempToken, ref Problems);

				if (!success)
					modelErrors = SIHelpers.EntityErrorsToHtml(Problems);
			}
			else
				modelErrors = SIHelpers.ModelErrorsToHtml(ModelState);

			string returnStatus = string.Empty;
			if (!success)
				returnStatus = string.Format("Unable to Register at this time. {0}", modelErrors);

			return Json(new { success, message = returnStatus });
		}

		[AllowAnonymous]
		public ActionResult FacebookAccounts(string code, Guid accessToken)
        {
            ViewBag.FacebookCode = code;

			AccessTokenBaseDTO accessTokenBaseDto = ProviderFactory.Security.GetAccessToken(accessToken, false);

			long ResellerID = 0;
			if (accessTokenBaseDto.Type() == AccessTokenTypeEnum.SyndicationInvitation)
			{
				SyndicationInviteAccessTokenDTO accessTokenDto = accessTokenBaseDto as SyndicationInviteAccessTokenDTO;
				ResellerID = accessTokenDto.ResellerID;
			}
			else if (accessTokenBaseDto.Type() == AccessTokenTypeEnum.SyndicationSelfRegistration)
			{
				SyndicationSelfRegistrationDTO accessTokenDto = accessTokenBaseDto as SyndicationSelfRegistrationDTO;
				ResellerID = accessTokenDto.ResellerID;
			}

			string token = LandingPagesService.GetFaceBookToken(code, ResellerID);

            //Token Saved when new account added
            SessionKeys.facebookTempToken = token;

            return PartialView("_FacebookAccounts");
        }

		[AllowAnonymous]
        public JsonResult LandingPagesFacebookAccounts([DataSourceRequest] DataSourceRequest request)
        {
            string token = SessionKeys.facebookTempToken;

            List<FacebookAccountList> facebookAccountLists = AdminService.FaceBookAccountList(token);

            return Json(facebookAccountLists.ToDataSourceResult(request), JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult SaveTwitterConnection(string authCode, long? socialappid, Guid accessToken)
        {
            BaseMembersModel baseModel = new BaseMembersModel();

            bool Success = LandingPagesService.SaveTwitterConnection(baseModel.UserInfo, authCode, socialappid, accessToken);

            return Json(new { Success });
        }

        [HttpPost]
		public JsonResult SaveFacebookConnection(string pageID, string website, long? socialappid, Guid accesstoken)
        {
            BaseMembersModel baseModel = new BaseMembersModel();

            bool Success = LandingPagesService.SaveFacebookConnection(baseModel.UserInfo, pageID, website, socialappid, SessionKeys.facebookTempToken, accesstoken);

            return Json(new { Success });
        }
		
		[Route("Syndication/Unsubscribe", RouteName = "SyndicationUnsubscribe")]
		[BaseMembersModelAction]
		public ActionResult SyndicationUnsubscribe()
		{
			bool Success = false;
			if (Session["Syndication_UnSubscribeStatus"] != null)
				Success = (bool) Session["Syndication_UnSubscribeStatus"];

			if (Success)
				ViewBag.Message = "You have successfully Unsubscribed.";
			else
				ViewBag.Message = "There was an issue Unsubscribing. Please contact Support at 877.326.7624 ext#1.";

			return View();
		}


    }
}
