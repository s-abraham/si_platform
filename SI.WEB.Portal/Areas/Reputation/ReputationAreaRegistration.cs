﻿using System.Web.Mvc;

namespace SI.WEB.Portal.Areas.Reputation
{
	public class ReputationAreaRegistration : AreaRegistration
	{
		public override string AreaName
		{
			get
			{
				return "Reputation";
			}
		}

		public override void RegisterArea(AreaRegistrationContext context)
		{
			context.MapRoute(
				"Reputation_default",
				"Reputation/{controller}/{action}/{id}",
				new { action = "Index", id = UrlParameter.Optional }
			);
		}
	}
}
