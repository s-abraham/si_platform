﻿

function filterSites(e) {
    var filterSel = $("#TimeFrameFilter").val();
    if (filterSel != "Custom") {
    	var ds = $("#ReviewSitesList").data().kendoGrid.dataSource;
    	ds.transport.options.read.url = '@(Url.Action("ReviewSitesGrid", "Reputation"))' + "?timeFrame=" + filterSel;
    	ds.read();
    } else {
    	$(".stream-filter").slideDown("fast");
    	$(".filter-dates").fadeIn();
    }
}

function sitesDataBound(e) {
	$("#ReviewSitesList tbody tr .reputation-filter-links").each(function () {
		var me = this;
		$(me).click(function () {
			var filter = $(this).data("filter");
			$("#filterParameters").val(filter);
			$("#ReviewStreamForm").submit();
		});
	});

	kendo.ui.progress($(".reputation-sites"), false);
	$(".sites-grid").hide().css({ visibility: "visible" }).fadeIn("fast");
}
	
function Init() {
	kendo.ui.progress($(".reputation-sites"), true);
	
	$("#activeFilter").click(function () {
		if ($(".stream-filter").css("display") == "block")
			$(".stream-filter").slideUp("fast");
		else
			$(".stream-filter").slideDown("fast");
	});

	if (ApplyFilterFromParameters() == false)
		$('#ReviewSitesList').data("kendoGrid").dataSource.read();

}
