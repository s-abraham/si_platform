﻿
var ReviewStream = {
	getStars: function(rating) {
		var starClasses = ["nostar", "halfstar", "onestar", "onehalfstar", "twostar", "twohalfstar", "threestar", "threehalfstar", "fourstar", "fourhalfstar", "fivestar"]

		rating = Math.round(rating * 10) / 10
		var ratingIdx = (Math.round((rating * 2)) / 2) * 2;
		if (ratingIdx > 10)
			ratingIdx = 10;

		var templt = kendo.template($("#rating-template").html());
		return templt({ starClass: starClasses[ratingIdx], rating: rating });
	},

	FilteringStart: function () {
		var dsChart = $("#ReviewCountChart").data("kendoChart").dataSource;
		dsChart.filter(this.filter().filters);
	},

	streamDataBound: function(e) {
		$("#ReviewStreamList tbody tr .action-dropdown").each(function() {
			$(".menu", this).kendoMenu({
				direction: "left",
				openOnClick: true,
				//open: function (e) {
				//	var ul = $(e.item).closest(".menu");
				//	var kwindow = ul.closest(".k-grid");
				//	var gridContent = ul.closest(".k-grid-content")
				//	var scrollbar = gridContent.find(".k-scrollbar-vertical");

				//	console.log(kwindow);
				//	console.log(kwindow.offset());
				//	console.log(ul);
				//	console.log(ul.offset());

				//	var offsetmenu = ul.offset().top - kwindow.offset().top;
				//	var currentSTop = scrollbar.scrollTop();
				//	setTimeout(function () {
				//		console.log(gridContent);
				//		console.log(kendo.format("offsetmenu = {0}, currentSTop = {1}", offsetmenu, currentSTop));

				//		scrollbar.scrollTop(offsetmenu + 176 + currentSTop);
				//	}, 100);
				//},
				select: function(e) {
					var action = $(e.item).data("action");
					if (!action || action == "gotoreview") return;

					var reviewId = $(e.item).closest(".action-dropdown").data("review-id");
					ReviewStream.reviewMenuAction(action, reviewId, e.item);
				}
			});
		});

		$("#ReviewStreamList tbody tr .checkbox").each(function() {
			$(this).on("click", function(e) {
				e.stopPropagation();
			});
		});

		$("#ReviewStreamList tbody tr").click(function() {
			var chkbox = $(this).find(".checkbox");
			if (chkbox.prop("checked") == "")
				chkbox.prop("checked", "checked");
			else
				chkbox.removeAttr("checked");
		});
	},
	
	reviewMenuAction: function(action, id, menuItem) {
		var url = '/Reputation/Reputation/ReviewAction?reviewID=' + id + '&menuaction=' + action;
		if (action == "email" || action == "share") {
			var kendoWindow = $("#reviewMenuActionWindow").data("kendoWindow");
			if (!kendoWindow) {
				kendoWindow = $("#reviewMenuActionWindow").kendoWindow({
					title: "Review Action",
					content: url,
					draggable: true,
					visible: true,
					modal: true,
					minHeight: 300,
					minWidth: 500,
					pinned: true,
					actions: ["Maximize", "Close"],
					refresh: function () {
						kendo.ui.progress($(".k-window"), false);
						$(".review-email-form").fadeIn("fast");
						this.center();
					}
				}).data("kendoWindow");
			} else {
				$(".review-email-form").hide();
				kendoWindow.refresh({ url: url });
			}
			kendo.ui.progress($(".k-window"), true);
			kendoWindow.center();
			kendoWindow.open();


		} else {
			var ckBox = $(menuItem).find("input[type='checkbox']");
			var orgChecked = ckBox.prop("checked");
			var ckSet = orgChecked ? "" : "checked";
			ckBox.prop("checked", ckSet);

			if (action == "responded" && !orgChecked)
				$(menuItem).parent().find(".ignore-check").prop("checked", "");
			if (action == "ignore" && !orgChecked) {
				$(menuItem).parent().find(".responded-check").prop("checked", "");
			}

			$.get(url, function(data) {
				if (data.success) { 
					var dataItem = ReviewStream.getGridDataItemByReviewID(id);
					if (action == "markasread") {
						$("#ReadIcon" + id).toggleClass("read-icon-read", orgChecked);
						$("#ReadIcon" + id).toggleClass("read-icon-not", !orgChecked);
						dataItem.HasBeenRead = ckSet;
					} else if (action == "responded") {
						$("#RespondIcon" + id).toggleClass("responded-icon-nr", orgChecked);
						$("#RespondIcon" + id).toggleClass("responded-icon-r", !orgChecked);
						$("#ReadIcon" + id).removeClass("read-icon-read").addClass("read-icon-not");
						dataItem.HasBeenRead = true;
						dataItem.HasBeenResponded = ckSet;
					} else {
						$("#ReadIcon" + id).removeClass("read-icon-read").addClass("read-icon-not");
						dataItem.HasBeenRead = true;
					}
				}
			});
		}
	},
	
	getActions: function(reviewId, read, responded, reviewUrl, ignored) {
		var templt = kendo.template($("#action-template").html());
		return templt({
			ReadChecked: (read ? "checked" : ""),
			RespondedChecked: (responded ? "checked" : ""),
			IgnoreChecked: (ignored ? "checked" : ""),
			ReadIcon: (read ? "not" : "read"),
			RespondIcon: (responded ? "r" : "nr"),
			ReviewUrl: (reviewUrl !== null ? reviewUrl : ""),
			ReviewID: reviewId
		});
	},
	
	getReviewDate: function(revDate) {
		var templt = kendo.template($("#date-template").html());
		return templt({ ReviewDate: revDate });
	},
	
	getLocation: function(dealerLoc) {
		var templt = kendo.template($("#location-template").html());
		return templt({ DealerLocation: dealerLoc });
	},
	
	getReviewDetails: function(site, name, reviewText, reviewUrl, reviewID) {
		var templt = kendo.template($("#detail-template").html());
		return templt({ SiteName: site, ReviewerName: name, ReviewText: reviewText, ReviewUrl: reviewUrl, ReviewID: reviewID });
	},
	
	getGridDataItemByReviewID: function(reviewID) {
		var data = $("#ReviewStreamList").data("kendoGrid").dataSource.data();
		for (var idx = 0; idx < data.length; idx++) {
			var dataItem = data[idx];
			if (dataItem.ReviewID == reviewID) {
				return dataItem;
			}
		}
	},
	
	IsEmail: function(email) {
		var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
		return regex.test(email);
	},
	
	Init: function() {
		//$("#ReviewStreamList .checkbox-all").click(function () {
		//	var allchecking = $(this).is(':checked');
		//	$("#ReviewStreamList tbody tr .checkbox").each(function () {
		//		if (allchecking)
		//			$(this).prop("checked", "checked");
		//		else
		//			$(this).removeAttr("checked");
		//	});
		//});

		$("#activeFilter").click(function () {
			if ($(".stream-filter").css("display") == "block")
				$(".stream-filter").slideUp("fast");
			else
				$(".stream-filter").slideDown("fast");
		});

		var grid = $('#ReviewStreamList').data("kendoGrid");
		grid.dataSource.originalFilter = grid.dataSource.filter;grid.dataSource.filter = function() {
			if (arguments.length > 0) {
				var me = this;
				setTimeout(function() {
					me.trigger("gridFiltering", arguments);
				}, 200);
			}
			return grid.dataSource.originalFilter.apply(this, arguments);
		}
		grid.dataSource.bind("gridFiltering", ReviewStream.FilteringStart);

		if (ApplyFilterFromParameters() == false)
			grid.dataSource.read();

		$(".stream-grid").kendoTooltip({
			filter: ".view-full-text",
			width: 500,
			position: "left",
			autoHide: false,
			content: function(e) {
				var id = $(e.target).data("review-id");
				return ReviewStream.getGridDataItemByReviewID(id).ReviewText;
			}
		});
	}
}
