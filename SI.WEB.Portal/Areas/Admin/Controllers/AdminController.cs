﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Globalization;
using System.Linq;
using System.Web.Mvc;
using AttributeRouting;
using AttributeRouting.Web.Mvc;
using Kendo.Mvc;
using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using SI.Extensions;
using SI.Services.Admin;
using SI.Services.Admin.Models;
using SI.WEB.Portal.Framework;
using SI.WEB.Portal.Models;
using SI.Services.Framework.Models;
using SI.DTO;

namespace SI.WEB.Portal.Areas.Admin.Controllers
{
    [Authorize]
    [RouteArea("Admin")]
    public class AdminController : Controller
    {

        [Route("Admin/Accounts", RouteName = "AdminAccounts")]
        [BaseMembersModelAction("CanAdminAccount")]
        public ActionResult Accounts()
        {
            return View();
        }

        #region Account List
        public JsonResult AccountListGrid([DataSourceRequest] DataSourceRequest request)
        {
            BaseMembersModel model = new BaseMembersModel();
            int TotalCount = 0;

            List<AccountList> accounts = AdminService.AccountList(model.UserInfo, KendoSupport.PagedGridRequest(request), ref TotalCount);

            DataSourceResult result = new DataSourceResult();
            result.Data = accounts;
            result.Total = TotalCount;

            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [BaseMembersModelAction]
        public ActionResult AccountEdit(long? accountID, long? parentAccountID)
        {
            ViewBag.AccountID = accountID.GetValueOrDefault();
            ViewBag.ParentAccountID = parentAccountID.GetValueOrDefault();

            BaseMembersModel membersModel = ViewBag.BaseModel as BaseMembersModel;

            AccountEdit accountEdit = AdminService.AccountEdit(membersModel.UserInfo, accountID.GetValueOrDefault());

            return PartialView("_AccountEdit", accountEdit);
        }
        #endregion

        #region Location Edit
        public ActionResult LocationEdit(long? accountID, long? parentAccountID)
        {
            ViewBag.AccountID = accountID.GetValueOrDefault();
            BaseMembersModel membersModel = new BaseMembersModel();

            AccountLocationEdit accountLocationEdit = AdminService.AccountLocationEdit(membersModel.UserInfo, accountID.GetValueOrDefault());

            ViewBag.FacebookLoginURL = "";
            SessionKeys.facebookTempToken = "";
            string FacebookStateContext = Guid.NewGuid().ToString();
            ViewBag.FacebookStateContext = FacebookStateContext;
            if (accountID.GetValueOrDefault() == 0)
            {
                accountLocationEdit.ParentAccountID = parentAccountID.GetValueOrDefault();
                SocialLoginUrl socialLogin = AdminService.GetSocialLoginUrl(membersModel.UserInfo, AdminService.GetAccountsReseller(membersModel.UserInfo, parentAccountID.GetValueOrDefault()), SocialNetworkEnum.Facebook, FacebookStateContext);
                ViewBag.FacebookLoginURL = socialLogin.LoginUrl;
            }

            accountLocationEdit.NewSubAccountAdd = false;
            ViewData.TemplateInfo.HtmlFieldPrefix = "";
            if (parentAccountID.HasValue)
            {
                ViewData.TemplateInfo.HtmlFieldPrefix = "SUBACT";
                accountLocationEdit.NewSubAccountAdd = true;
            }

            return PartialView("_LocationEdit", accountLocationEdit);
        }

        [HttpPost]
        public ActionResult LocationEdit(AccountLocationEdit accountLocationEdit)
        {
            BaseMembersModel model = new BaseMembersModel();

            string modelErrors = "";

            SaveEntityResult result = null;

            if (ModelState.IsValid)
            {
                if (accountLocationEdit.ID.GetValueOrDefault() == 0 && !string.IsNullOrEmpty(SessionKeys.facebookTempToken))
                {
                    accountLocationEdit.FacebookToken = SessionKeys.facebookTempToken;
                }

                result = AdminService.LocationEditSave(model.UserInfo, accountLocationEdit);

                if (!result.Success)
                    modelErrors = SIHelpers.EntityErrorsToHtml(result.Problems);
            }
            else
                modelErrors = SIHelpers.ModelErrorsToHtml(ModelState);

            string returnStatus;

            if (result.Success)
                returnStatus = string.Format("Saved on {0} {1}", result.DateModified.LocalDate.Value.ToString(CultureInfo.InvariantCulture), result.DateModified.TimeZone.DisplayName);
            else
                returnStatus = string.Format("Unable to Save at this time. {0}", modelErrors);

            return Json(new { success = result.Success, message = returnStatus, accountID = accountLocationEdit.ID });
        }

        [HttpPost]
        public ActionResult LocationEditSUBACT([Bind(Prefix = "SUBACT")] AccountLocationEdit accountLocationEdit)
        {
            BaseMembersModel model = new BaseMembersModel();

            string modelErrors = "";
            SaveEntityResult result = null;

            if (ModelState.IsValid)
            {
                if (accountLocationEdit.ID.GetValueOrDefault() == 0 && !string.IsNullOrEmpty(SessionKeys.facebookTempToken))
                {
                    accountLocationEdit.FacebookToken = SessionKeys.facebookTempToken;
                }

                result = AdminService.LocationEditSave(model.UserInfo, accountLocationEdit);

                if (!result.Success)
                    modelErrors = SIHelpers.EntityErrorsToHtml(result.Problems);
            }
            else
                modelErrors = SIHelpers.ModelErrorsToHtml(ModelState);

            string returnStatus;

            if (result.Success)
                returnStatus = string.Format("Saved on {0} {1}", result.DateModified.LocalDate.Value.ToString(CultureInfo.InvariantCulture), result.DateModified.TimeZone.DisplayName);
            else
                returnStatus = string.Format("Unable to Save at this time. {0}", modelErrors);

            return Json(new { success = result.Success, message = returnStatus, accountID = accountLocationEdit.ID });
        }


        #endregion

        #region Users Edit
        public ActionResult UsersEdit(long? accountID, long? parentAccountID, string perms)
        {
            ViewBag.AccountID = accountID.GetValueOrDefault();
            ViewBag.ShowSSIKeyColumn = perms.IsNullOptional("0") == "1";

            UserEdit userEdit = new UserEdit();

            userEdit.AccountID = accountID.GetValueOrDefault();
            userEdit.MemberOfLocationID = accountID.GetValueOrDefault();
            //userEdit.ID = 
            return PartialView("_UsersEdit", userEdit);
        }

        [HttpPost]
        public ActionResult UsersEdit(UserEdit userEdit)
        {
            BaseMembersModel bmm = new BaseMembersModel();
            SaveEntityResult result = new SaveEntityResult();

            string modelErrors = "";

            if (ModelState.IsValid)
            {
                result = AdminService.UsersEditSave(bmm.UserInfo, userEdit);

                if (!result.Success)
                    modelErrors = SIHelpers.EntityErrorsToHtml(result.Problems);
            }
            else
                modelErrors = SIHelpers.ModelErrorsToHtml(ModelState);

            string returnStatus;

            if (result.Success)
                returnStatus = string.Format("Saved on {0} {1}", result.DateModified.LocalDate.Value.ToString(CultureInfo.InvariantCulture), result.DateModified.TimeZone.DisplayName);
            else
                returnStatus = string.Format("Unable to Save at this time. {0}", modelErrors);

            return Json(new { result.Success, message = returnStatus });
        }


        public JsonResult UsersEditGrid([DataSourceRequest] DataSourceRequest request, long? accountID)
        {
            BaseMembersModel bmm = new BaseMembersModel();

            List<UserEdit> userEdits = AdminService.UsersEditGrid(bmm.UserInfo, accountID.GetValueOrDefault());

            return Json(userEdits.ToDataSourceResult(request), JsonRequestBehavior.AllowGet);
        }

        public JsonResult UsersExistAccessEditGrid([DataSourceRequest] DataSourceRequest request, long? accountID, string mode)
        {
            BaseMembersModel bmm = new BaseMembersModel();

            List<UserEdit> userEdits = AdminService.UsersExistAccessEditGrid(bmm.UserInfo, accountID.Value, "", 1, 100, KendoSupport.PagedGridRequest(request), mode);

            return Json(userEdits.ToDataSourceResult(request), JsonRequestBehavior.AllowGet);
        }


        public JsonResult UsersAccessEditGrid([DataSourceRequest] DataSourceRequest request, long? accountID)
        {
            BaseMembersModel bmm = new BaseMembersModel();

            List<UserEdit> userEdits = AdminService.UsersAccessEditGrid(bmm.UserInfo, accountID.GetValueOrDefault());

            return Json(userEdits.ToDataSourceResult(request), JsonRequestBehavior.AllowGet);
        }

        public ActionResult UserActionMenu(int? userID, string menuAction)
        {
            BaseMembersModel model = new BaseMembersModel();
            string responseMessage = "";

            bool Success = AdminService.UserActionMenu(model.UserInfo, userID.GetValueOrDefault(), menuAction, ref responseMessage);

            return Json(new { Success, message = responseMessage }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult UserAccessActionMenu(int? userID, int? roleID, string menuAction)
        {
            BaseMembersModel model = new BaseMembersModel();
            string problems = "";
            bool Success = AdminService.UserAccessActionMenu(model.UserInfo, userID.GetValueOrDefault(), roleID.GetValueOrDefault(), menuAction, ref problems);

            string returnStatus = "There was a problem updating the user. " + problems;
            if (Success)
                returnStatus = "User Updated";

            return Json(new { success = Success, message = returnStatus });
        }
        #endregion

        #region Sub-Accounts Edit

        public ActionResult SubAccountsEdit(long? accountID, string perms)
        {
            ViewBag.AccountID = accountID.GetValueOrDefault();
            ViewBag.CanAddChildren = perms.IsNullOptional("") == "1";

            return PartialView("_SubAccountsEdit");
        }

        [HttpPost]
        public ActionResult SubAccountDetach(long? subAccountID, long? accountID)
        {
            BaseMembersModel model = new BaseMembersModel();

            bool Success = AdminService.SubAccountDetach(model.UserInfo, subAccountID.GetValueOrDefault());

            return Json(new { Success });
        }

        public JsonResult SubAccountEditGrid([DataSourceRequest] DataSourceRequest request, long? accountID)
        {
            BaseMembersModel model = new BaseMembersModel();

            int TotalCount = 0;
            List<AccountList> accounts = AdminService.AccountList(model.UserInfo, KendoSupport.PagedGridRequest(request), ref TotalCount, accountID);

            List<SubAccount> subAccounts = new List<SubAccount>();
            foreach (AccountList accountList in accounts)
            {
                SubAccount subAccount = new SubAccount();
                subAccount.ID = accountList.ID;
                subAccount.Name = accountList.Name;
                subAccount.City = accountList.City;
                subAccount.State = accountList.State;
                subAccount.ZipCode = accountList.ZipCode;
                subAccount.Partner = accountList.Partner;

                subAccounts.Add(subAccount);
            }

            DataSourceResult result = new DataSourceResult();
            result.Data = subAccounts;
            result.Total = TotalCount;

            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult SubAccountAddAccount(int? subAccountID, long? accountID)
        {
            BaseMembersModel baseMembersModel = new BaseMembersModel();

            List<string> Problems = new List<string>();

            bool Success = AdminService.SubAccountAddAccount(baseMembersModel.UserInfo, subAccountID.GetValueOrDefault(), accountID.GetValueOrDefault(), ref Problems);

            string message = "";
            if (Success == false)
                message = string.Join(", ", Problems);

            return Json(new { Success, message });
        }

        public JsonResult SubAccountExistingEditGrid([DataSourceRequest] DataSourceRequest request, long? accountID)
        {
            BaseMembersModel model = new BaseMembersModel();

            if (request.Filters == null || !request.Filters.Any())
                request.Filters.Add(new FilterDescriptor() { Member = "Name", MemberType = Type.GetType("string"), Operator = FilterOperator.StartsWith, Value = "xxxxxxx" });

            int TotalCount = 0;
            List<AccountList> accounts = AdminService.AccountList(model.UserInfo, KendoSupport.PagedGridRequest(request), ref TotalCount, null, accountID);

            List<SubAccount> subAccounts = new List<SubAccount>();
            foreach (AccountList accountList in accounts)
            {
                SubAccount subAccount = new SubAccount();
                subAccount.ID = accountList.ID;
                subAccount.Name = accountList.Name;
                subAccount.City = accountList.City;
                subAccount.State = accountList.State;
                subAccount.ZipCode = accountList.ZipCode;
                subAccount.Partner = accountList.Partner;

                subAccounts.Add(subAccount);
            }

            DataSourceResult result = new DataSourceResult();
            result.Data = subAccounts;
            result.Total = TotalCount;

            return Json(result, JsonRequestBehavior.AllowGet);
        }




        #endregion

        #region Product Config Edit
        public ActionResult ProductConfigEdit(long? accountID, long? parentAccountID, string perms)
        {
            ViewBag.AccountID = accountID.GetValueOrDefault();
            ViewBag.SocialPermissions = perms;

            return PartialView("_ProductConfigEdit");
        }

        public ActionResult ReputationConfig(long? accountID)
        {
            ViewBag.AccountID = accountID.GetValueOrDefault();
            BaseMembersModel model = new BaseMembersModel();

            List<ReputationConfigLink> links = AdminService.ReputationConfigLinks(model.UserInfo, accountID.GetValueOrDefault());

            return PartialView("_ReputationConfig", links);
        }

        [HttpPost]
        public ActionResult ReputationConfig(FormCollection form, long? accountID)
        {
            BaseMembersModel model = new BaseMembersModel();
            Dictionary<string, string> linksDict = form.AllKeys.ToDictionary(k => k, v => form[v]);

            SaveEntityResult result = AdminService.ReputationConfigSave(model.UserInfo, linksDict, accountID.GetValueOrDefault());

            bool success = false;
            string _message = string.Empty;
            if (result.DateModified == null)
            {
                _message = string.Format("No change in URLs.");
            }
            else
            {
                _message = string.Format("Saved on {0} {1}", result.DateModified.LocalDate.Value.ToString(CultureInfo.InvariantCulture), result.DateModified.TimeZone.DisplayName);
                success = result.Success;
            }
            return Json(new { success, message = _message });
        }

        [HttpPost]
        public ActionResult ReputationConfigAutoPopulate(long? accountID, string source)
        {
            BaseMembersModel model = new BaseMembersModel();

            string url = AdminService.ReputationConfigAutoPopulate(model.UserInfo, accountID.GetValueOrDefault(), source);

            return Json(new { Success = !string.IsNullOrEmpty(url), url });
        }

        public ActionResult SyndicationConfig(long? accountID)
        {
            ViewBag.AccountID = accountID.GetValueOrDefault();

            return PartialView("_SyndicationConfig");
        }


        public JsonResult SyndicationEditGrid([DataSourceRequest] DataSourceRequest request)
        {
            //TODO DUMMY DATA
            BaseMembersModel model = new BaseMembersModel();
            List<SyndicationConfig> syndicationConfigs = AdminService.SyndicationEditGrid(model.UserInfo, KendoSupport.PagedGridRequest(request));

            return Json(syndicationConfigs.ToDataSourceResult(request), JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult SaveSyndicationConfig(IEnumerable<SyndicationConfig> syndicationConfigs)
        {
            string modelErrors = "";

            //TODO DUMMY DATA return message with users Timezone now
            BaseMembersModel model = new BaseMembersModel();
            SaveEntityResult result = AdminService.SaveSyndicationConfig(syndicationConfigs, model.UserInfo);

            if (!result.Success)
                modelErrors = SIHelpers.EntityErrorsToHtml(result.Problems);

            string returnStatus;
            if (result.Success)
                returnStatus = string.Format("Saved on {0} {1}", result.DateModified.LocalDate.Value.ToString(CultureInfo.InvariantCulture), result.DateModified.TimeZone.DisplayName);
            else
                returnStatus = string.Format("Unable to Save at this time. {0}", modelErrors);

            return Json(new { success = result.Success, message = returnStatus });
        }

        public ActionResult SocialConfig(long? accountID)
        {
            ViewBag.AccountID = accountID.GetValueOrDefault();

            string FacebookStateContext = Guid.NewGuid().ToString();
            ViewBag.FacebookStateContext = FacebookStateContext;

            return PartialView("_SocialConfig");
        }

        public JsonResult SocialConfigListView([DataSourceRequest] DataSourceRequest request, long? accountID)
        {
            BaseMembersModel model = new BaseMembersModel();
            List<SocialConfig> socialConfigs = AdminService.SocialConfigListView(model.UserInfo, KendoSupport.PagedGridRequest(request), accountID);

            return Json(socialConfigs.ToDataSourceResult(request), JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult RefreshSocialConnection(long? configID, long? accountID)
        {
            BaseMembersModel model = new BaseMembersModel();

            bool Success = AdminService.RefreshSocialConnection(model.UserInfo, configID.GetValueOrDefault(), accountID.GetValueOrDefault());

            return Json(new { Success });
        }


        #endregion

        #region Product Edit

        public ActionResult ProductsEdit(long? accountID)
        {
            ViewBag.AccountID = accountID.GetValueOrDefault();

            return PartialView("_ProductsEdit");
        }

        public JsonResult ProductsEditGrid([DataSourceRequest] DataSourceRequest request, long? accountID)
        {
            BaseMembersModel model = new BaseMembersModel();

            List<Products> products = AdminService.ProductsEditGrid(model.UserInfo, accountID.GetValueOrDefault());

            return Json(products.ToDataSourceResult(request), JsonRequestBehavior.AllowGet);
        }

        public JsonResult ProductList(long? accountID)
        {
            BaseMembersModel model = new BaseMembersModel();

            List<Products> products = AdminService.ProductListAvailable(model.UserInfo, accountID.GetValueOrDefault());

            return Json(products, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetPackagesList()
        {
            List<Packages> packages = AdminService.PackagesList();

            return Json(packages, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetSocialNetworks()
        {
            List<SocialNetworks> networks = AdminService.NetworkList();
            return Json(networks, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult ProductAdd(string ResellerID_PackageID, long? accountID)
        {
            BaseMembersModel model = new BaseMembersModel();

            string modelErrors = "";
            SaveEntityResult result = new SaveEntityResult();
            if (!string.IsNullOrEmpty(ResellerID_PackageID))
            {
                result = AdminService.ProductAdd(model.UserInfo, ResellerID_PackageID, accountID.GetValueOrDefault());

                if (!result.Success)
                    modelErrors = SIHelpers.EntityErrorsToHtml(result.Problems);
            }
            else
                modelErrors = "(Not Valid Reseller Or Package).";

            string returnStatus;

            if (result.Success)
                returnStatus = string.Format("Saved on {0} {1}", result.DateModified.LocalDate.Value.ToString(CultureInfo.InvariantCulture), result.DateModified.TimeZone.DisplayName);
            else
                returnStatus = string.Format("Unable to update at this time. {0}", modelErrors);

            return Json(new { success = result.Success, message = returnStatus });
        }

        [HttpPost]
        public ActionResult ProductRemove(long? SubscriptionID, long? accountID)
        {
            BaseMembersModel model = new BaseMembersModel();

            string modelErrors = "";
            bool success = false;

            if (SubscriptionID != null && SubscriptionID.HasValue)
            {
                List<string> Problems = new List<string>();
                success = AdminService.ProductRemove(model.UserInfo, SubscriptionID.GetValueOrDefault(), accountID.GetValueOrDefault(), ref Problems);

                if (!success)
                    modelErrors = SIHelpers.EntityErrorsToHtml(Problems);
            }
            else
                modelErrors = "(Not Valid Subscription).";

            string returnStatus;

            if (success)
                returnStatus = string.Format("Saved on {0}", model.UserInfo.EffectiveUser.CurrentDateTime.LocalDate.GetValueOrDefault().ToString(CultureInfo.InvariantCulture));
            else
                returnStatus = string.Format("Unable to update at this time. {0}", modelErrors);

            return Json(new { success, message = returnStatus });
        }
        #endregion

        #region Users Access Edit

        public ActionResult UsersAccessEdit(long? accountID)
        {
            ViewBag.AccountID = accountID.GetValueOrDefault();

            return PartialView("_UsersAccessEdit");
        }

        public ActionResult UsersExistingEdit(long? accountID)
        {
            ViewBag.AccountID = accountID.GetValueOrDefault();

            return PartialView("_UsersExistingEdit");
        }

        [HttpPost]
        public JsonResult UserAddExistingUser(long? userID, long? accountID, long? roleID, bool? cascade, bool? access)
        {
            BaseMembersModel model = new BaseMembersModel();

            string problems = "";
            bool success = AdminService.UserAddExistingUser(model.UserInfo, userID.GetValueOrDefault(), accountID.GetValueOrDefault(), roleID.GetValueOrDefault(), cascade.GetValueOrDefault(), access.GetValueOrDefault(), ref problems);

            string returnStatus = "There was a problem updating the user. " + problems;
            if (success)
                returnStatus = "User Updated";

            return Json(new { success, message = returnStatus });
        }
        #endregion

        #region Existing User Edit
        public JsonResult UserExistAccountList()
        {
            int TotalCount = 0;

            BaseMembersModel model = new BaseMembersModel();

            DataSourceRequest request = new DataSourceRequest();
            request.Page = 1;
            request.PageSize = 100;
            request.Sorts = new List<SortDescriptor>();
            request.Sorts.Add(new SortDescriptor() { Member = "Name", SortDirection = ListSortDirection.Ascending });

            List<AccountList> accounts = AdminService.AccountList(model.UserInfo, KendoSupport.PagedGridRequest(request), ref TotalCount);

            List<AccountFilterList> accountFilter = new List<AccountFilterList>();
            foreach (AccountList accountList in accounts)
            {
                AccountFilterList filterList = new AccountFilterList();
                filterList.ID = accountList.ID;
                filterList.Name = accountList.Name;

                accountFilter.Add(filterList);
            }

            return Json(accountFilter, JsonRequestBehavior.AllowGet);
        }


        #endregion

        #region Virtual Groups

        [HttpGet]
        [Route("Admin/VirtualGroups", RouteName = "AdminVirtualGroups")]
        [BaseMembersModelAction("CanAdminVirtualGroups")]
        public ActionResult VirtualGroups()
        {
            List<SelectListItem> accessibleLoctions = new List<SelectListItem>
				{
					new SelectListItem(){ Text = "Henry Chevrolet", Value = "1" },
					new SelectListItem(){ Text = "Henry Buick", Value = "2" },
					new SelectListItem(){ Text = "Henry BMW", Value = "3"},
					new SelectListItem(){ Text = "Henry Dodge", Value = "4"}
				};

            ViewBag.AccessibleLocations = accessibleLoctions;
            VirtualGroupSave virtualGroup = new VirtualGroupSave();
            virtualGroup.ID = 0;

            return View("VirtualGroups", virtualGroup);
        }


        [HttpPost]
        public ActionResult VirtualGroups(VirtualGroupSave virtualGroupSave)
        {
            BaseMembersModel model = new BaseMembersModel();

            string modelErrors = "";
            bool success = false;

            if (ModelState.IsValid)
            {
                //TODO Save VG
                //virtualGroupSave.SelectedLocationIDs  //comma delimited ids
                success = true;
            }
            else
                modelErrors = SIHelpers.ModelErrorsToHtml(ModelState);

            string returnStatus;

            if (success)
                returnStatus = string.Format("Saved on {0}", model.UserInfo.EffectiveUser.CurrentDateTime.LocalDate.GetValueOrDefault().ToString(CultureInfo.InvariantCulture));
            else
                returnStatus = "Unable to save at this time." + modelErrors;

            return Json(new { success, message = returnStatus });
        }

        public JsonResult VirtualGroupEditGrid([DataSourceRequest] DataSourceRequest request)
        {
            List<VirtualGroups> virtualGroups = new List<VirtualGroups>();

            VirtualGroups vg1 = new VirtualGroups();
            vg1.ID = 1;
            vg1.Name = "MileOne";
            vg1.NumberOfAccounts = 85;
            vg1.CreateDate = DateTime.Now;
            vg1.ModifiedDate = DateTime.Now;
            vg1.SelectedLocationIDs.Add(1);
            vg1.SelectedLocationIDs.Add(3);
            virtualGroups.Add(vg1);

            VirtualGroups vg2 = new VirtualGroups();
            vg2.ID = 2;
            vg2.Name = "Motoworld";
            vg2.NumberOfAccounts = 6;
            vg2.CreateDate = DateTime.Now;
            vg2.ModifiedDate = DateTime.Now;
            vg2.SelectedLocationIDs.Add(2);
            virtualGroups.Add(vg2);

            VirtualGroups vg3 = new VirtualGroups();
            vg3.ID = 3;
            vg3.Name = "Heritage";
            vg3.NumberOfAccounts = 6;
            vg3.CreateDate = DateTime.Now;
            vg3.ModifiedDate = DateTime.Now;
            vg3.SelectedLocationIDs.Add(4);
            virtualGroups.Add(vg3);

            return Json(virtualGroups.ToDataSourceResult(request));
        }

        #endregion



        public JsonResult RoleTypeList()
        {
            BaseMembersModel model = new BaseMembersModel();
            List<RoleTypes> roleTypes = AdminService.RoleTypes(model.UserInfo);

            return Json(roleTypes, JsonRequestBehavior.AllowGet);
        }

        public ActionResult AlertsEdit(long? accountID)
        {
            ViewBag.AccountID = accountID.GetValueOrDefault();

            AlertEdit alertEdit = new AlertEdit();

            return PartialView("_AlertsEdit", alertEdit);
        }

        [HttpPost]
        public ActionResult AlertsEdit(AlertEdit alertEdit)
        {
            //TODO DUMMY DATA
            BaseMembersModel model = new BaseMembersModel();
            string modelErrors = "";

            List<string> Problems = new List<string>();
            bool success = AdminService.AlertsEdit(alertEdit, ref Problems);

            if (!success)
                modelErrors = SIHelpers.EntityErrorsToHtml(Problems);

            string returnStatus;
            if (success)
                returnStatus = string.Format("Saved on {0}", model.UserInfo.EffectiveUser.CurrentDateTime.LocalDate.GetValueOrDefault().ToString(CultureInfo.InvariantCulture));
            else
                returnStatus = string.Format("Unable to Save at this time. {0}", modelErrors);

            return Json(new { success, message = returnStatus });
        }

        public JsonResult AlertsGridList([DataSourceRequest] DataSourceRequest request)
        {
            //TODO DUMMY DATA			
            BaseMembersModel model = new BaseMembersModel();
            List<UserAlert> userAlerts = AdminService.AlertsGridList(model.UserInfo, KendoSupport.PagedGridRequest(request));

            return Json(userAlerts.ToDataSourceResult(request), JsonRequestBehavior.AllowGet);
        }

        public ActionResult ScheduledEdit(long? accountID)
        {
            ViewBag.AccountID = accountID.GetValueOrDefault();

            return PartialView("_ScheduledEdit");
        }

        #region Competitors
        public ActionResult CompetitorsEdit(long? accountID)
        {
            ViewBag.AccountID = accountID.GetValueOrDefault();
            ViewBag.maxCompetitor = 5;

            return PartialView("_CompetitorsEdit");
        }

        public JsonResult CompetitorEditGrid([DataSourceRequest] DataSourceRequest request, long? accountID)
        {
            List<Competitor> competitors = AdminService.CompetitorEditGrid(accountID.GetValueOrDefault());

            return Json(competitors.ToDataSourceResult(request), JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult CompetitorRemove(long? ID)
        {
            BaseMembersModel model = new BaseMembersModel();

            string modelErrors = "";
            bool success;

            List<string> Problems = new List<string>();
            success = AdminService.CompetitorRemove(model.UserInfo, ID.GetValueOrDefault(), ref Problems);

            if (Problems.Any())
                modelErrors = SIHelpers.EntityErrorsToHtml(Problems);

            string returnStatus = string.Empty;

            if (success)
                returnStatus = string.Format("Deleted on {0}", model.UserInfo.EffectiveUser.CurrentDateTime.LocalDate.GetValueOrDefault().ToString(CultureInfo.InvariantCulture));
            else
                returnStatus = string.Format("Unable to remove at this time. {0}", modelErrors);

            return Json(new { success, message = returnStatus });
        }

        [HttpPost]
        public ActionResult CompetitorAdd(long? competitorID, long? accountID)
        {
            BaseMembersModel model = new BaseMembersModel();

            string modelErrors = "";
            bool success;

            List<string> Problems = new List<string>();
            success = AdminService.CompetitorAdd(model.UserInfo, accountID.GetValueOrDefault(), competitorID.GetValueOrDefault(), ref Problems);

            if (Problems.Any())
            {
                modelErrors = SIHelpers.EntityErrorsToHtml(Problems);
            }

            string returnStatus = string.Empty;

            if (success)
                returnStatus = string.Format("Saved on {0}", model.UserInfo.EffectiveUser.CurrentDateTime.LocalDate.GetValueOrDefault().ToString(CultureInfo.InvariantCulture));
            else
                returnStatus = string.Format("Unable to update at this time. {0}", modelErrors);

            return Json(new { success, message = returnStatus });

        }

        public JsonResult CompetitorAddEditGrid([DataSourceRequest] DataSourceRequest request, long? accountID)
        {
            BaseMembersModel model = new BaseMembersModel();

            List<Competitor> competitors = AdminService.CompetitorAddEdit(model.UserInfo, accountID.GetValueOrDefault());

            return Json(competitors.ToDataSourceResult(request), JsonRequestBehavior.AllowGet);
        }

        #endregion

        public ActionResult InventoryEdit(long? accountID)
        {
            ViewBag.AccountID = accountID.GetValueOrDefault();

            //TODO DUMMY DATA
            Inventory inventory = AdminService.InventoryEdit(accountID.GetValueOrDefault());

            return PartialView("_InventoryEdit", inventory);
        }

        [HttpPost]
        public ActionResult InventoryEdit(Inventory inventory)
        {
            //TODO DUMMY DATA SAVE
            BaseMembersModel model = new BaseMembersModel();
            string modelErrors = "";
            bool success;

            List<string> Problems = new List<string>();
            success = AdminService.InventoryEditSave(inventory, ref Problems);

            if (Problems.Any())
                modelErrors = SIHelpers.EntityErrorsToHtml(Problems);

            string returnStatus = string.Empty;

            if (success)
                returnStatus = string.Format("Saved on {0}", model.UserInfo.EffectiveUser.CurrentDateTime.LocalDate.GetValueOrDefault().ToString(CultureInfo.InvariantCulture));
            else
                returnStatus = string.Format("Unable to Save at this time. {0}", modelErrors);

            return Json(new { success, message = returnStatus });
        }

        public ActionResult AccountSettingsEdit(long? accountID)
        {
            AccountSettings accountSettings = new AccountSettings();
            accountSettings.AccountID = accountID.GetValueOrDefault();

            BaseMembersModel membersModel = new BaseMembersModel();

            AccountLocationEdit accountLocationEdit = AdminService.AccountLocationEdit(membersModel.UserInfo, accountID.GetValueOrDefault());

            accountSettings.PostRequireApproval = accountLocationEdit.PostRequireApproval;

            return PartialView("_AccountSettingsEdit", accountSettings);
        }

        [HttpPost]
        public ActionResult AccountSettingsEdit(AccountSettings accountSettings)
        {
            string modelErrors = "";
            bool success;

            List<string> Problems = new List<string>();
            BaseMembersModel membersModel = new BaseMembersModel();
            success = AdminService.AccountSettingsEdit(membersModel.UserInfo, accountSettings, ref Problems);

            if (Problems.Any())
                modelErrors = SIHelpers.EntityErrorsToHtml(Problems);

            string returnStatus = string.Empty;

            if (success)
                returnStatus = string.Format("Saved on {0}", membersModel.UserInfo.EffectiveUser.CurrentDateTime.LocalDate.GetValueOrDefault().ToString(CultureInfo.InvariantCulture));
            else
                returnStatus = string.Format("Unable to Save at this time. {0}", modelErrors);

            return Json(new { success, message = returnStatus });


        }

        [Route("Admin/Users", RouteName = "AdminUsers")]
        [BaseMembersModelAction("CanAdminUsers")]
        public ActionResult Users()
        {
            return View();
        }


        [Route("Admin/System", RouteName = "AdminSystem")]
        [BaseMembersModelAction("CanAdminSystem")]
        public ActionResult System()
        {
            return View();
        }

        public JsonResult UsersMemberOfAccountGrid([DataSourceRequest] DataSourceRequest request)
        {
            BaseMembersModel bmm = new BaseMembersModel();
			
            int TotalCount = 0;

            List<UserEdit> userEdits = AdminService.SearchUsers(bmm.UserInfo, KendoSupport.PagedGridRequest(request), ref TotalCount);

            DataSourceResult result = new DataSourceResult();
            result.Data = userEdits;
            result.Total = TotalCount;

            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public JsonResult AccountListOfUsersMembers(string text)
        {
            int TotalCount = 0;

            BaseMembersModel model = new BaseMembersModel();

            DataSourceRequest request = new DataSourceRequest();
            request.Page = 1;
            request.PageSize = 100;
            request.Sorts = new List<SortDescriptor>();
            request.Sorts.Add(new SortDescriptor() { Member = "Name", SortDirection = ListSortDirection.Ascending });
            request.Filters = new List<IFilterDescriptor>();
            request.Filters.Add(new FilterDescriptor("Name", FilterOperator.IsEqualTo, text));

            List<AccountList> accounts = AdminService.AccountList(model.UserInfo, KendoSupport.PagedGridRequest(request), ref TotalCount);

            List<AccountFilterList> accountFilter = new List<AccountFilterList>();
            foreach (AccountList accountList in accounts)
            {
                AccountFilterList filterList = new AccountFilterList();
                filterList.ID = accountList.ID;
                filterList.Name = accountList.Name;

                accountFilter.Add(filterList);
            }

            return Json(accountFilter, JsonRequestBehavior.AllowGet);
        }

        public ActionResult FacebookAccounts(string code, long? accountID)
        {
            ViewBag.FacebookCode = code;

            BaseMembersModel model = new BaseMembersModel();
            string token = AdminService.GetFacebookAccessToken(code, accountID.GetValueOrDefault(), model.UserInfo);

            //Token Saved when new account added
            SessionKeys.facebookTempToken = token;

            return PartialView("_FacebookAccounts");
        }


        public JsonResult AdminFacebookAccounts([DataSourceRequest] DataSourceRequest request)
        {
            string token = SessionKeys.facebookTempToken;

            BaseModel model = new BaseModel();
            ViewBag.BaseModel = model;

            List<FacebookAccountList> facebookAccountLists = AdminService.FaceBookAccountList(token);

            return Json(facebookAccountLists.ToDataSourceResult(request), JsonRequestBehavior.AllowGet);
        }

        [AllowAnonymous]
        public JsonResult GetFacebookAccountDetails(string facebookAccountID)
        {
            AccountLocationEdit accountLocation;

            string token = SessionKeys.facebookTempToken;
            if (!string.IsNullOrEmpty(token))
                accountLocation = AdminService.GetFacebookPageInformation(facebookAccountID, token);
            else
                accountLocation = new AccountLocationEdit();

            return Json(accountLocation, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GooglePages(string code, long? accountID)
        {
            ViewBag.GoogleCode = code;

            BaseMembersModel model = new BaseMembersModel();
            GoogleTokens tokens = AdminService.GetGoogleAccessToken(code, accountID.GetValueOrDefault(), model.UserInfo);

            SessionKeys.googleTempTokens = tokens;

            return PartialView("_GooglePages");
        }

        public ActionResult YouTubeChannels(string code, long? accountID)
        {
            ViewBag.YouTubeCode = code;

            BaseMembersModel model = new BaseMembersModel();
            GoogleTokens tokens = AdminService.GetGoogleAccessToken(code, accountID.GetValueOrDefault(), model.UserInfo);

            SessionKeys.googleTempTokens = tokens;

            return PartialView("_YouTubeChannels");
        }

        public JsonResult AdminYouTubeChannels([DataSourceRequest] DataSourceRequest request)
        {
            BaseModel model = new BaseModel();
            ViewBag.BaseModel = model;

            List<YouTubePageList> youTubePageList = AdminService.YouTubeChannelsList(SessionKeys.googleTempTokens.Token, SessionKeys.googleTempTokens.TokenType);

            return Json(youTubePageList.ToDataSourceResult(request), JsonRequestBehavior.AllowGet);
        }

        public JsonResult AdminGooglePages([DataSourceRequest] DataSourceRequest request)
        {
            BaseModel model = new BaseModel();
            ViewBag.BaseModel = model;

            List<GooglePageList> googlePageList = AdminService.GooglePagesList(SessionKeys.googleTempTokens.Token, SessionKeys.googleTempTokens.TokenType);

            return Json(googlePageList.ToDataSourceResult(request), JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult SaveSocialPageConnectionConfig(string network, string pageID, string website, long? configId, long? accountID, long? socialAppId)
        {
            BaseMembersModel model = new BaseMembersModel();
            bool Success = false;


            if (network == "facebook")
                Success = AdminService.SaveSocialConnectionConfig(configId.GetValueOrDefault(), accountID.GetValueOrDefault(), socialAppId.GetValueOrDefault(), pageID, SessionKeys.facebookTempToken, website, model.UserInfo, network);
            else if (network == "google")
                Success = AdminService.SaveGoogleConnectionConfig(configId.GetValueOrDefault(), accountID.GetValueOrDefault(), socialAppId.GetValueOrDefault(), pageID, SessionKeys.googleTempTokens, website, model.UserInfo);

            return Json(new { Success });
        }


        [HttpPost]
        public JsonResult SaveSocialConnection(long? configID, long? accountID, string authCode, string network, long? socialappid)
        {
            BaseMembersModel model = new BaseMembersModel();

            bool Success = false;

            if (network == "youtube")
            {
                //Success = AdminService.SaveGoogleSocialConnection(authCode, configID.GetValueOrDefault(), accountID.GetValueOrDefault(), socialappid.GetValueOrDefault(), model.UserInfo);
            }
            else
                if (network == "twitter")
                {
                    Success = AdminService.SaveTwitterSocialConnection(authCode, configID.GetValueOrDefault(), accountID.GetValueOrDefault(), socialappid.GetValueOrDefault(), model.UserInfo);
                }

            return Json(new { Success });
        }

        [HttpPost]
        public JsonResult DisconnectSocialConnection(long? configID, long? accountID)
        {
            BaseMembersModel model = new BaseMembersModel();

            bool Success = AdminService.DisconnectSocialConnection(model.UserInfo, configID.GetValueOrDefault(), accountID.GetValueOrDefault());

            return Json(new { Success });
        }


        [Route("Admin/ExceptionLog", RouteName = "ExceptionLog")]
        [BaseMembersModelAction("CanAdminExceptions")]
        public ActionResult ExceptionLog()
        {
            BaseMembersModel membersModel = ViewBag.BaseModel as BaseMembersModel;

            if (!membersModel.UserInfo.EffectiveUser.SuperAdmin)
                return Redirect("/");

            return View();
        }

        [Route("Admin/FlushServerCaches", RouteName = "FlushServerCaches")]
        [BaseMembersModelAction("CanAdminFlushCache")]
        public ActionResult FlushServerCaches()
        {
            BaseMembersModel membersModel = ViewBag.BaseModel as BaseMembersModel;

            if (membersModel.UserInfo.EffectiveUser.SuperAdmin)
            {
                AdminService.FlushAccountAndPermissionCache();
            }

            return RedirectToAction("DealerDashboard", "Dashboard", new { area = "Dashboard" });
        }

        public JsonResult ExceptionLogRead([DataSourceRequest] DataSourceRequest request)
        {
            int TotalCount = 0;
            List<ExceptionLog> exceptionLogs = AdminService.ExceptionLogs(KendoSupport.PagedGridRequest(request), ref TotalCount);

            DataSourceResult result = new DataSourceResult();
            result.Data = exceptionLogs;
            result.Total = TotalCount;

            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [BaseMembersModelAction]
        public ActionResult ProductConfigWindow()
        {
            BaseMembersModel membersModel = ViewBag.BaseModel as BaseMembersModel;
            ViewBag.AccountID = membersModel.UserInfo.EffectiveUser.CurrentContextAccountID;


            return PartialView("ProductConfigWindow");
        }

        public ActionResult UserDetailEdit(long? userID, string accountName, long accountID)
        {
            ViewBag.UserID = userID.GetValueOrDefault();
            ViewBag.AccountName = accountName;
            ViewBag.AccountID = accountID;

            return PartialView("_UserDetailEdit");
        }

        public ActionResult UserDetailNotifications(long? userID)
        {
            ViewBag.UserID = userID.GetValueOrDefault();

            UserDetailsNotifications userDetailsNotifications = AdminService.GetUserDetailsNotifications(userID);

            return PartialView("_UserDetailNotifications", userDetailsNotifications);
        }

        public ActionResult UserDetailEmailedReports(long? userID)
        {
            ViewBag.UserID = userID.GetValueOrDefault();

            UserDetailEmailedReports userDetailEmailed = AdminService.GetReportTemplateAndEmailReport(userID.GetValueOrDefault());

            return PartialView("_UserDetailEmailedReports", userDetailEmailed);
        }

		public ActionResult UserDetailActivity(long? userID)
		{
			ViewBag.UserID = userID.GetValueOrDefault();

			BaseMembersModel model = new BaseMembersModel();

			List<UserDetailActivityList> activity = AdminService.GetUserActivity(userID, model.UserInfo);

			return PartialView("_UserDetailActivity", activity);
		}


        public ActionResult UserReportNotificationDetails(long? reportLogId, long? notificationId)
        {
            if (reportLogId.GetValueOrDefault() > 0)
                ViewBag.Detail = AdminService.GetReportLogByID(reportLogId.GetValueOrDefault());
            else if (notificationId.GetValueOrDefault() > 0)
                ViewBag.Detail = AdminService.GetBodyTextForNotificationTargetID(notificationId.GetValueOrDefault());

            return PartialView("_UserReportNotificationDetails");
        }

        [HttpPost]
        public JsonResult EmailedReportsSaveConfig(long? templateID, long? subscriptionID, string spec, bool? enabled)
        {
            BaseMembersModel model = new BaseMembersModel();

            bool Success = AdminService.SetReportSubscriptions(model.UserInfo, templateID, spec, subscriptionID, enabled.GetValueOrDefault());

            return Json(new { Success });
        }

        [HttpPost]
        public JsonResult NotificationSaveConfig(long? userID, string type, string method, bool? enabled)
        {
			BaseMembersModel model = new BaseMembersModel();

            bool Success = AdminService.SetUserNotificationStates(model.UserInfo,  userID, type, method, enabled);
            return Json(new { Success });            
        }

		[HttpPost]
		public JsonResult UserChangeMemberOfAccount(long? userID, long? accountID)
		{
			BaseMembersModel model = new BaseMembersModel();

			bool Success = AdminService.ChangeUserMemberOfAccount(model.UserInfo, userID.GetValueOrDefault(), accountID.GetValueOrDefault());
			return Json(new { Success });            
		}

		public JsonResult GetUserPasswordLink(long? userID)
		{
			string link = AdminService.GetUserPasswordResetLink(userID.GetValueOrDefault());

			return Json(new {link}, JsonRequestBehavior.AllowGet);
		}

    }
}
