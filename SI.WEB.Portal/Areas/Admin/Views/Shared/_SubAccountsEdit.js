﻿


var SubAccounts = {
	AccountID: '',
	
	ApplyFilters: function() {
		var filterObj = [];
		var search = $("#SearchTextSubA").val();
		if (search != null && search != "") {
			var multiFilterObj = { logic: "or", filters: [] };
			multiFilterObj.filters.push({ field: "Name", operator: "startswith", value: search });
			multiFilterObj.filters.push({ field: "City", operator: "startswith", value: search });
			filterObj.push(multiFilterObj);
		}

		$('#SubAccountGrid').data("kendoGrid").dataSource.filter(filterObj);
		$('#SubAccountGrid').data("kendoGrid").refresh();
	},

	accountDetach: function(e) {
		var data = this.dataItem(this.select());
		SIGlobal.confirmDialog("Detach", "Are you sure you want to Detach the Account " + data.Name + "?", function(button) {
			if (button == "Yes") {
				var url = '/Admin/Admin/SubAccountDetach';
				var addData = { subAccountID: data.ID, accountID: SubAccounts.AccountID };
				$.post(url, addData, function(data) {
					//alert(data); //TODO any message here??
					$('#SubAccountGrid').data("kendoGrid").dataSource.read();
					$("#AccountExistGrid").data("kendoGrid").dataSource.read();
				});
			}
		});
	},

	addAccount: function() {
		$("#SubAccountEdit").fadeOut("fast");
		$("#AddAccountPartial").load(kendo.format('/Admin/Admin/LocationEdit?accountID=0&parentAccountID={0}', SubAccounts.AccountID), function () {
			$("#AddAccountPartial").fadeIn("fast");
			$(".button-bar h3").html("Sub-Accounts * Add Account *");

		});
	},

	NewAccountCreated: function(newDisplayName) {
		$("#AddAccountPartial").fadeOut("fast", function() {
			$("#AddAccountPartial").html("");
		});
		$("#SubAccountEdit").fadeIn("fast");
		$(".button-bar h3").html("Sub-Accounts - New Account " + newDisplayName + " Created");
		$('#SubAccountGrid').data("kendoGrid").dataSource.read();
	},
		
	NewAccountCancel: function () {
		$("#AddAccountPartial").fadeOut("fast", function () {
			$("#AddAccountPartial").html("");
		});
		$("#SubAccountEdit").fadeIn("fast");
		$(".button-bar h3").html("Sub-Accounts");
	},
	
	Init: function() {
		SIGlobal.placeHolderSim(this);

		$("#SearchTextSubA").keyup(function (e) {
			SubAccounts.ApplyFilters();
		});
		$("#AddAccountSub").click(SubAccounts.addAccount);
	}
};
