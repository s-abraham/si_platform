﻿

var AccountEdit = {
	accountID: '',
	parentAccountID: '',
	currentView: "LocationEdit",	
		
	menuSelect: function (item) {
		SIGlobal.dirtyFlagConfirm(function (response) {
			if (response == "Yes") AccountEdit.MenuChange(item);
		});			
	},
		
	MenuChange: function (e) {
		$("#AccountMenu").find(".k-state-selected").removeClass("k-state-selected");
		$(e.item).addClass("k-state-selected");
		$("#form" + AccountEdit.currentView).dirtyFlagReset();

		AccountEdit.SwitchViews($(e.item).attr("view"), null, $(e.item).attr("perms"));
	},
		
	SwitchViews: function (viewName, fnCallBack, perms) {

		if ($("#" + viewName).html() == "") {
			$("#" + viewName).load(kendo.format('/Admin/Admin/{0}?accountID={1}&parentAccountID={2}&perms={3}', viewName, AccountEdit.accountID, AccountEdit.parentAccountID, perms), function () {
				$("#" + AccountEdit.currentView).fadeOut("fast", function() {
					$("#" + viewName).fadeIn("fast");
					AccountEdit.currentView = viewName;

					if (fnCallBack)
						fnCallBack();
				});
			});
		} else {
			$("#" + AccountEdit.currentView).fadeOut("fast", function () {
				$("#" + viewName).fadeIn("fast");
				AccountEdit.currentView = viewName;
				
				if (fnCallBack)
					fnCallBack();
			});
		}
	},
	
	Init: function() {
		$("#LoadingLoacations").hide();
		$("#LocationEdit").fadeIn("fast");

		$(".close-window-button").click(function () {
			var kendoWindow = $("#AccountEdit").data("kendoWindow");
			kendoWindow.close();
		});
	}

};



