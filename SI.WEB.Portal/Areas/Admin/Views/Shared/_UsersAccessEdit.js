﻿

var UserAccess = {
	ApplyFilters: function() {
		var filterObj = [];
		var search = $("#SearchTextUA").val();
		if (search != null && search != "") {
			var multiFilterObj = { logic: "or", filters: [] };
			multiFilterObj.filters.push({ field: "LastName", operator: "startswith", value: search });
			multiFilterObj.filters.push({ field: "Email", operator: "contains", value: search });
			filterObj.push(multiFilterObj);
		}

		var status = $("#StatusSearch").data("kendoDropDownList").dataItem();
		if (status.Value != "") {
			filterObj.push({ field: "Status", operator: "eq", value: status.Value });
		}

		if ($("#LocalUsersOnly").prop("checked") == true) {
			filterObj.push({ field: "LocalUser", operator: "eq", value: true });
		}

		$('#UsersAccessGrid').data("kendoGrid").dataSource.filter(filterObj);
		$('#UsersAccessGrid').data("kendoGrid").refresh();
	},

	userAccessDataBound: function (e) {
		$("#UsersAccessGrid tbody tr .action-dropdown").each(function () {
			$(".menu", this).kendoMenu({
				direction: "left",
				openOnClick: true,
				open: function(e) {
					var ul = $(e.item).closest(".menu");
					var kwindow = ul.closest(".k-window");
					var gridContent = ul.closest(".k-grid-content");
					var offsetmenu = ul.offset().top - kwindow.offset().top;
					var currentSTop = gridContent.scrollTop();
					setTimeout(function() {
						gridContent.scrollTop(offsetmenu - 181 + currentSTop);
					}, 100);
				},
				select: function(e) {
					var action = $(e.item).data("action");
					if (!action) return;

					var dataItem = $(e.item).closest(".action-dropdown");
					UserAccess.UserAccessMenuAction(action, dataItem.data("user-id"), dataItem.data("role-id"));
				}
			});
		});

		setTimeout(function() {
			var grid = $("#UsersAccessGrid").data("kendoGrid");
			var row = e.sender.tbody.find('tr:first');
			if (row) {
				grid.select(row);
				row.trigger('click');
			}
		}, 100);
	},

	UserAccessMenuAction: function (action, userID, roleID) {
		kendo.ui.progress($("#UsersAccessGrid"), true);
		var url = '/Admin/Admin/UserAccessActionMenu';
		var addData = { userID: userID, roleID: roleID, menuaction: action };
		$.post(url, addData, function (data) {
			kendo.ui.progress($("#UsersAccessGrid"), false);
			if (data.success) {
				$("#UsersAccessGrid").data("kendoGrid").dataSource.read();
			}
			SIGlobal.alertDialog("User " + action, data.message);
				
		});
	},

	getUAActions: function(userID, roleID, localUser) {
		var templt = kendo.template($("#action-template-ua").html());
		return templt({ UserID: userID, RoleID: roleID, LocalUser: localUser });
	},
	
	Init: function() {
		SIGlobal.placeHolderSim(this);

		$("#SearchTextUA").keyup(function (e) {
			UserAccess.ApplyFilters();
		});
		$("#LocalUsersOnly").change(function (e) {
			UserAccess.ApplyFilters();
		});

		$("#AddAccessExistingUserButton").on("click", function () {
			$("#AddAccessExistingUser").show();
			$("#UserEditMode").val("access");
		});
	}
};
	
