﻿
var ProductEdit = {
	AccountID: '',
	
	productConfig: function(e) {
		var data = this.dataItem(this.select());

		$("#AccountMenu li").removeClass("k-state-selected");
		$("#AccountMenu li").eq(2).addClass("k-state-selected");
		
		AccountEdit.SwitchViews("ProductConfigEdit", function () {
			var tabs = $("#ConfigTabs").data("kendoTabStrip");

			switch (data.Category) {
				case "social":
					tabs.select(0); break;
				case "reputation":
					tabs.select(1); break;
				case "syndication":
					tabs.select(2); break;
			}
		});

	},
	
	productRemove: function (e) {
		var data = this.dataItem(this.select());
		SIGlobal.confirmDialog("Remove Product", "Are you sure you want to Remove the Product " + data.Name + "?", function(button) {
			if (button == "Yes") {
				var url = '/Admin/Admin/ProductRemove';
				var addData = { SubscriptionID: data.ID, accountID: ProductEdit.AccountID };
				$.post(url, addData, function(data) {						
					$('#ProductsGrid').data("kendoGrid").dataSource.read();
					$('#ProductSelect').data("kendoDropDownList").dataSource.read();
				});
			}
		});
	},
	
	Init: function() {
		$("#AddProductButton").on("click", function () {
			$("#resultsAddPE").html("");
			var pID = $("#ProductSelect").data("kendoDropDownList").value();
			if (pID != "") {
				$("#LoadingIconSavePE").show();
				var url = '/Admin/Admin/ProductAdd';
				var addData = { ResellerID_PackageID: pID, accountID: ProductEdit.AccountID };
				$.post(url, addData, function (data) {
					$("#resultsAddPE").html(data.message);
					$("#LoadingIconSavePE").hide();

					if (data.success) {
						$('#ProductsGrid').data("kendoGrid").dataSource.read();
						$('#ProductSelect').data("kendoDropDownList").dataSource.read();
					}

				});
			} else {
				SIGlobal.alertDialog("Select Product", "Need to select a Product.", "i");
			}
		});
	}
};


