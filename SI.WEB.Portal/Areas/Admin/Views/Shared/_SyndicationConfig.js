﻿

var SynConfig = {
	postGridData: [],
	syngridDataBound: function () {
		$('#SyndicationGrid').on('click', '.chkbxAutoApproved', function() {
			var row = $(this).closest('tr');
			var grid = $('#SyndicationGrid').data().kendoGrid;
			var changedData = grid.dataSource.getByUid(row.data("uid")).toJSON();
			var checked = $(this).is(':checked');

			for (var idx = 0; idx < SynConfig.postGridData.length; idx++) {
				if (SynConfig.postGridData[idx].ID == changedData.ID) {
					SynConfig.postGridData[idx].AutoApproved = checked;
					grid.expandRow(row);
					return;
				}
			}

			changedData.AutoApproved = checked;
			SynConfig.postGridData.push(changedData);
			grid.expandRow(row);
		});
		
		$('#SyndicationGrid').on('click', '.chkbxManualApproval', function () {
			var row = $(this).closest('tr');
			var grid = $('#SyndicationGrid').data().kendoGrid;
			var changedData = grid.dataSource.getByUid(row.data("uid")).toJSON();
			var checked = $(this).is(':checked');

			for (var idx = 0; idx < SynConfig.postGridData.length; idx++) {
				if (SynConfig.postGridData[idx].ID == changedData.ID) {
					SynConfig.postGridData[idx].ManualApproval = checked;
					grid.expandRow(row);
					return;
				}
			}

			changedData.ManualApproval = checked;
			SynConfig.postGridData.push(changedData);
			grid.expandRow(row);
		});

		$(".syndication-grid").dirtyFlagSet();
	},
			
	SaveSyndGridItems: function () {
		EditSaved = true;
		if (SynConfig.postGridData.length > 0) {
			var url = '/Admin/Admin/SaveSyndicationConfig';
			$.postJSON(url, { 'syndicationConfigs': SynConfig.postGridData }, function (data) {
				$("#resultsSyndication").html(data.message);
				$(".syndication-grid").dirtyFlagReset();
				//reload on success only?
				$('#SyndicationGrid').data().kendoGrid.dataSource.read();
			});
		} else
			$("#resultsSyndication").html("Nothing to save.");
	},
	
	Init: function() {
		$("#saveButtonSyndication").click(function () {
			SynConfig.SaveSyndGridItems();
		});
	}
};

