﻿

var UserEdit = {
	kendoValidator: null,
	submitCount: 0,
	justSaved: false,
	keyupTimer: null,
		
	BeginSubmitUser: function () {
		if (UserEdit.submitCount > 0) return false;
		UserEdit.submitCount++;
		EditSaved = true;
	},
	
	SaveFailure: function(){
		SIGlobal.formSaveFailure();
		UserEdit.submitCount = 0;
	},
	    
	UserInfoSave: function (response) {
		$("#formUserEdit").dirtyFlagReset();
		$("#resultsUTS").html(response.message);
		UserEdit.submitCount = 0;
		UserEdit.justSaved = true;
			
		if (response.Success)
			$("#UsersGrid").data("kendoGrid").dataSource.read();
	},
		
	userDataBound: function (e) {
		$("#UsersGrid tbody tr .action-dropdown").each(function () {
			$(".menu", this).kendoMenu({
				direction: "left",
				openOnClick: true,
				open: function(e) {
					var ul = $(e.item).closest(".menu");
					var kwindow = ul.closest(".k-window");
					var gridContent = ul.closest(".k-grid-content");
					var offsetmenu = ul.offset().top - kwindow.offset().top;
					var currentSTop = gridContent.scrollTop();
					setTimeout(function () {
						gridContent.scrollTop(offsetmenu - 181 + currentSTop);
					}, 100);
				},
				select: function (e) {
					var action = $(e.item).data("action");
					if (!action) return;

					var userId = $(e.item).closest(".action-dropdown").data("user-id");
					UserEdit.UserMenuAction(action, userId, e.item);
				}
			});
		});
	},
		
	RolesDataBound: function(e) {
		var grid = $("#UsersGrid").data("kendoGrid");
		var row = grid.tbody.find('tr:first');
		if (row) {
			grid.select(row);
			row.trigger('click');
		}
	},
		
	UserMenuAction: function (action, id, menuItem) {
		kendo.ui.progress($("#UsersGrid"), true);
		var url = '/Admin/Admin/UserActionMenu?userID=' + id + '&menuaction=' + action;
		$.get(url, function (data) {
			kendo.ui.progress($("#UsersGrid"), false);
			if (data.Success) {
				$("#UsersGrid").data("kendoGrid").dataSource.read();
			}
			SIGlobal.alertDialog("User Action", data.message);
		});
	},

	getActions: function (userID) {
		var templt = kendo.template($("#action-template").html());
		return templt({ UserID: userID });
	},

	userEdit: function (e) {
		$("#EditAddUser").show();
		$("#AddExistingUser").hide();

		if (UserEdit.justSaved == false)
			$("#resultsUTS").html("");
		UserEdit.justSaved = false;
		
		var data = this.dataItem(this.select());
		$("#FirstName").val(data.FirstName).css('color', '');
		$("#LastName").val(data.LastName).css('color', '');
		$("#Email").val(data.Email).css('color', '');
		$("#UserID").val(data.UserID);
		$("#SSOKey").val(data.SSOKey);
			
		var dropdownlist = $("#Role").data("kendoDropDownList");
		dropdownlist.select(function (dataItem) {
			return dataItem.ID == data.Role;
		});
			
		$("#CascadeRole").prop("checked", data.CascadeRole);
		$("#EditTitle").html(kendo.format("Edit User - {0} - User ID: {1}", data.LastName, data.Email));
	},
	
	Init: function() {
		UserEdit.kendoValidator = $("#formUserEdit").kendoValidator().data("kendoValidator");
		$("#formUserEdit").dirtyFlagSet();
		$(".ico-required").attr("title", "Required");

		SIGlobal.placeHolderSim(this);

		$("#SearchText").keyup(function (e) {
			if (UserEdit.keyupTimer != null)
				clearTimeout(UserEdit.keyupTimer);

			UserEdit.keyupTimer = setTimeout(function () {
				var search = $("#SearchText").val();
				var filterObj = [];
				if (search != null && search != "") {
					var multiFilterObj = { logic: "or", filters: [] };
					multiFilterObj.filters.push({ field: "LastName", operator: "startswith", value: search });
					multiFilterObj.filters.push({ field: "Email", operator: "contains", value: search });
					filterObj.push(multiFilterObj);
				}

				var grid = $('#UsersGrid').data("kendoGrid");
				grid.dataSource.filter(filterObj);
				grid.refresh();

			}, 500);
		});

		$("#NewUserButton").on("click", function () {
			$("#EditAddUser").show();
			$("#AddExistingUser").hide();
			$("#resultsUTS").html("");
			$("#EditTitle").html("New User");
			$("#FirstName").val("");
			$("#LastName").val("");
			$("#Email").val("");
			$("#Role").data("kendoDropDownList").select(1);
			$("#UserID").val("0");
			$("#CascadeRole").prop("checked", false);
			$("#SSOKey").val("");
		});

		$("#AddExistingUserButton").on("click", function () {
			$("#EditAddUser").hide();
			$("#AddExistingUser").show();
		});

		$("#saveButtonUserEdit").click(function () {
			if (UserEdit.kendoValidator.validate()) {
				SIGlobal.ValidationSummaryClear();
				$("#resultsUTS").html("");

				$("#formUserEdit").submit();
			} else {
				SIGlobal.ValidationSummaryPopulate(UserEdit.kendoValidator.errors());
			}
		});
	}
};
