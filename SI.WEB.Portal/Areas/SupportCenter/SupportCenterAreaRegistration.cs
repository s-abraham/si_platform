﻿using System.Web.Mvc;

namespace SI.WEB.Portal.Areas.SupportCenter
{
	public class SupportCenterAreaRegistration : AreaRegistration
	{
		public override string AreaName
		{
			get
			{
				return "SupportCenter";
			}
		}

		public override void RegisterArea(AreaRegistrationContext context)
		{
			context.MapRoute(
				"SupportCenter_default",
				"SupportCenter/{controller}/{action}/{id}",
				new { action = "Index", id = UrlParameter.Optional }
			);
		}
	}
}
