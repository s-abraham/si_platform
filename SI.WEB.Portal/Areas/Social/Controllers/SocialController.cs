﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AttributeRouting;
using AttributeRouting.Web.Mvc;
using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using SI.BLL;
using SI.Extensions;
using SI.Services.Framework;
using SI.Services.Social;
using SI.Services.Social.Models;
using SI.WEB.Portal.Framework;
using SI.WEB.Portal.Models;

namespace SI.WEB.Portal.Areas.Social.Controllers
{
	[Authorize]
	[RouteArea("Social")]
    public class SocialController : Controller
    {
		[Route("PostActivity/{*filter}", RouteName = "SocialDealerPostActivity")]
		[BaseMembersModelAction("CanDealerPostActivity")]
		public ActionResult DealerPostActivity(string filter = "")
		{
			BaseMembersModel model = ViewBag.baseModel as BaseMembersModel;

			ViewBag.filterParameters = filter;
			ViewBag.currentAccountID = 0;
			ViewBag.currentAccountName = "";
			ViewBag.ImageBaseUrl = Settings.GetSetting("site.PublishImageUploadURL");
			ViewBag.FullPage = false;
			ViewBag.ContentLibraryPost = false;

			if (string.IsNullOrEmpty(filter))
			{
				ViewBag.currentAccountID = model.UserInfo.EffectiveUser.CurrentContextAccountID;
				ViewBag.currentAccountName = SocialService.GetAccountName(model.UserInfo.EffectiveUser.CurrentContextAccountID);
			}

			return View();
		}

		[Route("Publish", RouteName = "SocialPublish")]
		[BaseMembersModelAction("CanPublish")]
		public ActionResult Publish()
		{
			ViewBag.FullPage = true;
			return View();
		}

		[Route("ContentLibrary", RouteName = "SocialContentLibrary")]
		[BaseMembersModelAction("CanPublish")]
		public ActionResult ContentLibrary()
		{
			ViewBag.FullPage = false;
			
			return View();
		}


		[Route("PostCalendar", RouteName = "SocialPostCalendar")]
		[BaseMembersModelAction("CanDealerPostActivity")]
		public ActionResult PostCalendar()
		{
			BaseMembersModel model = ViewBag.baseModel as BaseMembersModel;

			ViewBag.IANATimeZone = NodaTimeService.WindowsToIana(model.UserInfo.EffectiveUser.ID.GetValueOrDefault());
			ViewBag.ImageBaseUrl = Settings.GetSetting("site.PublishImageUploadURL");

			return View();
		}

		[Route("ProfileManager", RouteName = "SocialStream")]
		[BaseMembersModelAction("CanSocialStream")]
		public ActionResult SocialStream()
		{
			return View();
		}

        [Route("SyndicationHelp", RouteName = "SocialSyndicationHelp")]
        [BaseMembersModelAction("CanSyndicationHelpPage")]
        public ActionResult SyndicationHelp()
        {
            BaseMembersModel model = ViewBag.baseModel as BaseMembersModel;

            return View();
        }


		public JsonResult DealerPostActivityGrid([DataSourceRequest] DataSourceRequest request)
		{
            BaseMembersModel model = new BaseMembersModel();

			int TotalCount = 0;

			List<PostActivity> post = SocialService.DealerPostActivityGrid(model.UserInfo, KendoSupport.PagedGridRequest(request), ref TotalCount);

			DataSourceResult result = new DataSourceResult();
			result.Data = post;
			result.Total = TotalCount;

			return Json(result, JsonRequestBehavior.AllowGet);
		}

		public JsonResult DealerPostActivityAccounts(string dealerFilter, string VirtualGroup, long? dealerIDFilter)
		{
			BaseMembersModel model = new BaseMembersModel();

			List<SelectListItem> accounts = SocialService.GetDealerPostActivityAccounts(model.UserInfo, dealerFilter, dealerIDFilter.GetValueOrDefault(0));

			return Json(accounts, JsonRequestBehavior.AllowGet);
		}

		public JsonResult PostActivityPostedBy(long? locationID, string timeFrame)
		{
            BaseMembersModel model = new BaseMembersModel();

			List<PostActivityPostedBy> post = SocialService.DealerPostActivityPostedBy(model.UserInfo, locationID.GetValueOrDefault(), timeFrame);

			return Json(post, JsonRequestBehavior.AllowGet);
		}

		[HttpGet]
		public ActionResult ComposeMessage(bool? mode, long? postID, long? rssItemID, bool? imagePost, bool? repost, bool? libraryPost)
		{
			BaseMembersModel model = new BaseMembersModel();
			ViewBag.BaseModel = model;
			ViewBag.ContentLibraryPost = libraryPost.GetValueOrDefault();

			ComposeMessage composeMessage;

			if (rssItemID.GetValueOrDefault() > 0 || postID.GetValueOrDefault() == 0)
			{
				composeMessage = new ComposeMessage();
				composeMessage.PostID = 0;

				composeMessage.PublishOptions.MaxDaysBeforeOriginalPublishDate = 1;
				composeMessage.PublishOptions.MaxDaysAfterOriginalPublishDate = 30;

				SocialService.SetComposeMessageDefaults(composeMessage, model.UserInfo.EffectiveUser);

				if (rssItemID.GetValueOrDefault() > 0)
				{
					ViewBag.ContentLibraryPost = true;

					if (!imagePost.GetValueOrDefault())
						SocialService.GetPostForRssItem(model.UserInfo, rssItemID.GetValueOrDefault(), composeMessage);
					else
						SocialService.GetPostForRssItemImagePost(model.UserInfo, rssItemID.GetValueOrDefault(), Server.MapPath("~/UploadedPostImages"), composeMessage);
				}
			}
			else
			{
				composeMessage = SocialService.GetPost(model.UserInfo, postID);
				composeMessage.UploadImagePath = Settings.GetSetting("site.PublishImageUploadURL");

				if (!composeMessage.PublishOptions.MaxDaysBeforeOriginalPublishDate.HasValue)
				{
					composeMessage.PublishOptions.MaxDaysBeforeOriginalPublishDate = 1;
					composeMessage.PublishOptions.MaxDaysAfterOriginalPublishDate = 30;
				}

				if (repost.GetValueOrDefault())
				{
					//composeMessage.Targets = "";
					SocialService.SetComposeMessageDefaults(composeMessage, model.UserInfo.EffectiveUser);
				}
			}

			ViewBag.FullPage = mode.GetValueOrDefault();

			ViewBag.currentAccountID = 0;
			ViewBag.currentAccountName = "";
			if (postID.GetValueOrDefault(0) == 0)
			{
				ViewBag.currentAccountID = model.UserInfo.EffectiveUser.CurrentContextAccountID;
				ViewBag.currentAccountName = SocialService.GetAccountName(model.UserInfo.EffectiveUser.CurrentContextAccountID);
			}

			return View("_ComposeMessage", composeMessage);
		}
		
		[HttpPost]
		public ActionResult ComposeMessage(ComposeMessage composeMessage, bool? mode)
		{
			BaseMembersModel model = new BaseMembersModel();

			string modelErrors = "";
			bool success = false;

			if (ModelState.IsValid)
			{
				List<string> Problems = new List<string>();
				success = SocialService.SavePublish(composeMessage, model.UserInfo, ref Problems);

				if (!success)
					modelErrors = SIHelpers.EntityErrorsToHtml(Problems);
			}
			else
				modelErrors = SIHelpers.ModelErrorsToHtml(ModelState);

			string returnStatus;

			if (success)
				returnStatus = string.Format("Posted on {0}",  DateTime.Now.ToString());
			else
				returnStatus = string.Format("Unable to Post at this time. {0}", modelErrors);

			return Json(new { success, message = returnStatus, postID = composeMessage.PostID });
		}

		public JsonResult MessageAccountsGrid([DataSourceRequest] DataSourceRequest request, long? syndicatorID)
		{
            BaseMembersModel model = new BaseMembersModel();

			int TotalCount = 0;

			List<MessageAccounts> accounts = SocialService.MessageAccountsGrid(model.UserInfo, KendoSupport.PagedGridRequest(request), syndicatorID, ref TotalCount);

			DataSourceResult result = new DataSourceResult();
			result.Data = accounts;
			result.Total = TotalCount;

			return Json(result, JsonRequestBehavior.AllowGet);
		}
		
		public JsonResult AutoLinkScrap(string url)
		{
			LinkScrape linkScrape = SocialService.AutoLinkScrap(url);

			return Json(linkScrape, JsonRequestBehavior.AllowGet);
		}


		public ActionResult PostImageUpload(IEnumerable<HttpPostedFileBase> ImageUpload)
		{
			if (ImageUpload != null)
			{
				foreach (var file in ImageUpload)
				{
					if (file.ContentType.StartsWith("image"))
					{
						string guidName = string.Format("{0}{1}", Guid.NewGuid().ToString(), Path.GetExtension(file.FileName));
						
						string physicalPath = Path.Combine(Server.MapPath("~/UploadedPostImages"), guidName); //TODO get path from config
						file.SaveAs(physicalPath);

						return Json(guidName, "text/plain");
					}
				}
			}

			return Content("ERROR");
		}

		public ActionResult PostImageUploadRemove(string[] fileNames)
		{
			if (fileNames != null)
			{
				foreach (string fileName in fileNames)
				{
					string physicalPath = Path.Combine(Server.MapPath("~/UploadedPostImages"), fileName);
					if (System.IO.File.Exists(physicalPath))
					{
						System.IO.File.Delete(physicalPath);
						return Content("");
					}
				}
			}

			return Content("ERROR");
		}

		public JsonResult MessageTypes()
		{
			BaseMembersModel model = new BaseMembersModel();

			List<MessageTypes> messageTypes = SocialService.GetPublishMessageTypes(model.UserInfo);
			
			return Json(messageTypes, JsonRequestBehavior.AllowGet);
		}

		[HttpGet]
		public ActionResult GetPostMessageToolTip(long? id)
		{
			string message = SocialService.GetPostTargetMessage(id);
			return Content(message);
		}

		[HttpGet]
		public ActionResult GetPostLinkToolTip(long? id)
		{
			string link = SocialService.GetPostTargetLink(id);
			return Content(string.Format("<a href='{0}' target='_blank'>{0}</a>", link));
		}

        [BaseMembersModelAction]
		public ActionResult ReviseSyndicationPost(long? postID, long? accountID)
		{
			BaseMembersModel model = ViewBag.baseModel as BaseMembersModel;

			RevisePost revisePost = SocialService.GetPostForRevision(model.UserInfo, postID.GetValueOrDefault(), accountID.GetValueOrDefault());
			
			return PartialView("_ReviseSyndicationPost", revisePost);
		}

		[HttpPost]
		public ActionResult ReviseSyndicationPost(RevisePost revisePost)
		{
			BaseMembersModel model = new BaseMembersModel();

			string modelErrors = "";
			bool success = false;

			if (ModelState.IsValid)
			{
				List<string> Problems = new List<string>();
				success = SocialService.SavePostRevisionForAccount(revisePost, model.UserInfo, ref Problems);

				if (!success)
					modelErrors = SIHelpers.EntityErrorsToHtml(Problems);
			}
			else
				modelErrors = SIHelpers.ModelErrorsToHtml(ModelState);

			string returnStatus;

			if (success)
				returnStatus = string.Format("Saved on {0}", DateTime.Now.ToString());
			else
				returnStatus = string.Format("Unable to Update Post at this time. {0}", modelErrors);

			return Json(new { success, message = returnStatus });
		}

		public JsonResult ContentLibraryRSSListView([DataSourceRequest] DataSourceRequest request, string search, int? timeframe, string franchise)
		{
			BaseMembersModel model = new BaseMembersModel();

			int totalCount = 0;

			List<ContentLibraryRSSItems> rssItems = SocialService.GetRSSItems(model.UserInfo, KendoSupport.PagedGridRequest(request), search, timeframe, franchise, ref totalCount);

			DataSourceResult result = new DataSourceResult();
			result.Data = rssItems;
			result.Total = totalCount;

			return Json(result, JsonRequestBehavior.AllowGet);
		}

		public JsonResult DealerPostCalendar(string Start, string End)
		{
			BaseMembersModel model = new BaseMembersModel();
			
			int TotalCount = 0;

			List<PostActivityCalendar> post = SocialService.DealerPostCalendar(model.UserInfo, model.UserInfo.EffectiveUser.CurrentContextAccountID, Start, End, ref TotalCount);

			DataSourceResult result = new DataSourceResult();
			result.Data = post;
			result.Total = TotalCount;

			return Json(result, JsonRequestBehavior.AllowGet);
		}

		public JsonResult PostAction(long? postTargetID, string menuaction)
		{
			BaseMembersModel model = new BaseMembersModel();
			string Message = "";
			bool Success = SocialService.PostMenuAction(model.UserInfo, postTargetID.GetValueOrDefault(), menuaction, ref Message);

			return Json(new { success = Success, message = Message }, JsonRequestBehavior.AllowGet);
		}

		public ActionResult StreamAccountPicker()
		{
			return PartialView("_StreamAccountPicker");
		}

		public ActionResult FacebookStream()
		{
			return PartialView("_FacebookStream");
		}

		public JsonResult StreamAccountList()
		{
			BaseMembersModel model = new BaseMembersModel();

			List<StreamAccount> streamAccounts = SocialService.SocialAccounts(model.UserInfo);

			return Json(streamAccounts, JsonRequestBehavior.AllowGet);
		}

		public JsonResult FacebookStreamListView(string until, long? accountID)
		{
			BaseMembersModel model = new BaseMembersModel();

			List<FacebookComment> facebookComments = new List<FacebookComment>();
			List<FacebookStream> facebookStreams = SocialService.GetFacebookStreams(model.UserInfo, facebookComments, accountID.GetValueOrDefault(), until);
			List<FacebookComment> sess_facebcomments = SessionKeys.facebookComments;
			
			sess_facebcomments.Clear();
			sess_facebcomments.AddRange(facebookComments);
			SessionKeys.facebookComments = sess_facebcomments;
			
			return Json(facebookStreams, JsonRequestBehavior.AllowGet);
		}

		public JsonResult FacebookStreamCommentsListView(string fbPostID, long? credentialID, int? pageSize, string type)
		{
			if (pageSize.GetValueOrDefault() == 3)
				return Json(SessionKeys.facebookComments.Where(c => c.PostUniqueID == fbPostID).ToList(), JsonRequestBehavior.AllowGet);

			return Json(SocialService.GetFacebookComments(fbPostID, credentialID.GetValueOrDefault(), pageSize.GetValueOrDefault(), type), JsonRequestBehavior.AllowGet);
		}

		[HttpPost]
		public ActionResult FacebookCommentDelete(string commentID, long? credentialID)
		{
			string message = "";
			bool success = SocialService.DeleteCommentonPost(credentialID.GetValueOrDefault(), commentID, ref message);

			return Json(new { success, message });
		}

		[HttpPost]
		public ActionResult FacebookCommentSave(string postID, long? credentialID, string comment)
		{
			string message = "";
			bool success = SocialService.CreateCommentonPost(credentialID.GetValueOrDefault(), postID, comment, ref message);

			return Json(new { success, message });
		}

		[HttpPost]
		public ActionResult FacebookCommentLike(string commentID, long? credentialID)
		{
			string message = "";
			bool success = SocialService.LikeComment(credentialID.GetValueOrDefault(), commentID, ref message);

			return Json(new { success, message });
		}

		[HttpPost]
		public ActionResult FacebookCommentUnLike(string commentID, long? credentialID)
		{
			string message = "";
			bool success = SocialService.UnLikeComment(credentialID.GetValueOrDefault(), commentID, ref message);

			return Json(new { success, message });
		}

		[HttpPost]
		public ActionResult FacebookPostDelete(string postID, long? credentialID)
		{
			string message = "";
			bool success = SocialService.DeletePost(credentialID.GetValueOrDefault(), postID, ref message);
			
			return Json(new { success, message });
		}

		[HttpPost]
		public JsonResult FacebookLike(string postID, long? credentialID)
		{
			string message = "";
			bool success = SocialService.LikePost(credentialID.GetValueOrDefault(), postID, ref message);

			return Json(new { success, message });
		}

		public ActionResult TwitterStream()
		{
			return PartialView("_TwitterStream");
		}

		public JsonResult TweetStreamListView(string since, long? accountID, string section, int count)
		{
			BaseMembersModel model = new BaseMembersModel();
			List<TwitterStream> twitterStreams = null;

			if (section == "home")
				twitterStreams = SocialService.GetHomeTweets(model.UserInfo, accountID.GetValueOrDefault(), count, since);
			if (section == "user")
				twitterStreams = SocialService.GetUserTweets(model.UserInfo, accountID.GetValueOrDefault(), count, since);
			if (section == "mentions")
				twitterStreams = SocialService.GetMentions(model.UserInfo, accountID.GetValueOrDefault(), count, since);

			return Json(twitterStreams, JsonRequestBehavior.AllowGet);
		}

		[HttpPost]
		public JsonResult TweetFavorite(string tweetID, long? credentialID, bool favorite)
		{
			BaseMembersModel model = new BaseMembersModel();

			string message = "";
			bool success;
			if (!favorite)
				success = SocialService.SetFavorite(model.UserInfo, credentialID.GetValueOrDefault(), tweetID, ref message);
			else
				success = SocialService.DeleteFavorite(model.UserInfo, credentialID.GetValueOrDefault(), tweetID, ref message);

			return Json(new {success, message});
		}

		[HttpPost]
		public JsonResult TweetDelete(string tweetID, long? credentialID)
		{
			BaseMembersModel model = new BaseMembersModel();

			string message = "";
			bool success = SocialService.DeleteTweet(model.UserInfo, credentialID.GetValueOrDefault(), tweetID, ref message);

			return Json(new { success, message });
		}

		[HttpPost]
		public JsonResult TweetReply(string tweetID, long? credentialID, string reply)
		{
			BaseMembersModel model = new BaseMembersModel();

			string message = "";
			bool success = SocialService.ReplyToTweet(model.UserInfo, credentialID.GetValueOrDefault(), reply, tweetID, ref message);

			return Json(new { success, message });
		}


		public ActionResult GoogleStream()
		{
			return PartialView("_GoogleStream");
		}
		
		public JsonResult GoogleStreamListView(long? accountID, int page)
		{
			BaseMembersModel model = new BaseMembersModel();
			
			List<GooglePlusStream> googleStreams = SocialService.GetGooglePlusStreams(model.UserInfo, accountID.GetValueOrDefault(), page);

			return Json(googleStreams, JsonRequestBehavior.AllowGet);
		}

		public JsonResult GoogleStreamCommentsListView(string gPostID, long? credentialID, int? pageSize)
		{
			BaseMembersModel model = new BaseMembersModel();

			return Json(SocialService.GetGoogleComments(model.UserInfo, gPostID, credentialID.GetValueOrDefault(), pageSize.GetValueOrDefault()), JsonRequestBehavior.AllowGet);
		}
	
		[HttpPost]
		public ActionResult GooglePostDelete(string postID, long? credentialID)
		{
			string message = "";
			bool success = SocialService.DeleteGooglePlusActivity(credentialID.GetValueOrDefault(), postID, ref message);
			
			return Json(new { success, message });
		}

		[HttpPost]
		public ActionResult GoogleCommentDelete(string commentID, long? credentialID)
		{
			string message = "";
			bool success = SocialService.DeleteGooglePlusComment(credentialID.GetValueOrDefault(), commentID, ref message);

			return Json(new { success, message });
		}

		[HttpPost]
		public ActionResult GoogleCommentSave(string postID, long? credentialID, string comment)
		{
			string message = "";
			bool success = SocialService.AddGooglePlusComment(credentialID.GetValueOrDefault(), postID, comment, ref message);

			return Json(new { success, message });
		}


		[Route("PostStatistics", RouteName = "SocialPostStatistics")]
		[BaseMembersModelAction("CanPublish")]
		public ActionResult SocialPostStatistics()
		{
			return View();
		}

		public ActionResult FacebookStatistics()
		{
			return PartialView("_FacebookStatistics");
		}

		public ActionResult TwitterStatistics()
		{
			return PartialView("_TwitterStatistics");
		}

		public ActionResult GoogleStatistics()
		{
			return PartialView("_GoogleStatistics");
		}

		public JsonResult FaceBookPostStatisticsGrid([DataSourceRequest] DataSourceRequest request)
		{
			BaseMembersModel model = new BaseMembersModel();

			int TotalCount = 0;

			List<FaceBookPostStatistics> post = SocialService.FaceBookPostStatisticsGrid(model.UserInfo, KendoSupport.PagedGridRequest(request), ref TotalCount);

			DataSourceResult result = new DataSourceResult();
			result.Data = post;
			result.Total = TotalCount;

			return Json(result, JsonRequestBehavior.AllowGet);
		}

		public JsonResult TwitterPostStatisticsGrid([DataSourceRequest] DataSourceRequest request)
		{
			BaseMembersModel model = new BaseMembersModel();

			int TotalCount = 0;

			List<TwitterPostStatistics> post = SocialService.TwitterPostStatisticsGrid(model.UserInfo, KendoSupport.PagedGridRequest(request), ref TotalCount);

			DataSourceResult result = new DataSourceResult();
			result.Data = post;
			result.Total = TotalCount;

			return Json(result, JsonRequestBehavior.AllowGet);
		}


		[Route("PostManagement", RouteName = "SocialPostManagement")]
		[BaseMembersModelAction("CanDealerPostActivity")]
		public ActionResult PostManagement()
		{
			return View();
		}

		public JsonResult PostMangementGrid([DataSourceRequest] DataSourceRequest request, string timeFrame)
		{
			BaseMembersModel model = new BaseMembersModel();

			List<PostManagement> post = SocialService.PostManagementGrid(model.UserInfo, KendoSupport.PagedGridRequest(request), timeFrame.ToInteger());

			return Json(post.ToDataSourceResult(request), JsonRequestBehavior.AllowGet);
		}

		public JsonResult PostManagementAction(long? postID, string menuaction)
		{
			BaseMembersModel model = new BaseMembersModel();
			string Message = "";
			bool Success = SocialService.PostManagementAction(model.UserInfo, postID.GetValueOrDefault(), menuaction, ref Message);

			return Json(new { success = Success, message = Message }, JsonRequestBehavior.AllowGet);
		}

		[Route("PostManagementV2", RouteName = "SocialPostManagementV2")]
		[BaseMembersModelAction("CanDealerPostActivity")]
		public ActionResult PostManagementV2()
		{
			return View();
		}

		public JsonResult PostMangementV2Grid([DataSourceRequest] DataSourceRequest request, string timeFrame)
		{
			BaseMembersModel model = new BaseMembersModel();

			List<PostManagementV2> post = SocialService.PostManagementV2Grid(model.UserInfo, KendoSupport.PagedGridRequest(request), timeFrame.ToInteger());

			return Json(post.ToDataSourceResult(request), JsonRequestBehavior.AllowGet);
		}

		public JsonResult PostManagementV2Action(long? postID, string menuaction)
		{
			BaseMembersModel model = new BaseMembersModel();
			string Message = "";
			bool Success = SocialService.PostManagementV2Action(model.UserInfo, postID.GetValueOrDefault(), menuaction, ref Message);

			return Json(new { success = Success, message = Message }, JsonRequestBehavior.AllowGet);
		}

		public ActionResult PostManagementDetails(long postID)
		{
			BaseMembersModel model = new BaseMembersModel();
			PostManagementDetails postManagementDetails = SocialService.PostManagementDetails(model.UserInfo, postID);
			
			return PartialView("_PostManagementDetails", postManagementDetails);
		}

		public JsonResult AccountFranchises(long? accountID)
		{
			List<SelectListItem> franchises = SocialService.AccountFranchises(accountID);
			return Json(franchises, JsonRequestBehavior.AllowGet);
		}



		[Route("ProductConfig", RouteName = "SocialProductConfigByToken")]
		[BaseMembersModelAction]
		public ActionResult SocialProductConfigByToken()
		{
			long? AccountID = 0;

			if (Session["SocialProductConfig_AccountID"] != null)
				AccountID = (long?)Session["SocialProductConfig_AccountID"];

			if (AccountID.GetValueOrDefault() > 0)
			{
				ViewBag.AccountName = SocialService.GetAccountName(AccountID.Value);
				ViewBag.AccountID = AccountID;
				return View();
			}

			return RedirectToRoute("Home");
		}


    }
}
