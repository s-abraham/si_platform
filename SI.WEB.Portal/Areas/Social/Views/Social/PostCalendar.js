﻿

	
function CalendarDataBound(e) {
	$(".k-event").children().removeAttr("title");
}

function EventDetails(target) {
	var data = $("#PostCalendar").data().kendoScheduler.dataSource.getByUid(target.data("uid"));
		
	var url = '/Social/Social/GetPostMessageToolTip?id=' + data.PostTargetID;
	var message = $.ajax({ type: "GET", url: url, async: false }).responseText;
		
	var image = "";
	if (data.Type == "Photo")
		image = "<img src='/ImageSizer?image=" + imageFolderPath + data.PostImageURL + "&w=160' class='loading-image-img'></img>";
	else if (data.Type == "Link") {
		url = '/Social/Social/GetPostLinkToolTip?id=' + data.PostTargetID;
		image = $.ajax({ type: "GET", url: url, async: false }).responseText;
	}

	var templt = kendo.template($("#eventDetailsTemplate").html());
	return templt(
		{
			Title: data.title,
			Type: data.Type,
			LocationName: data.LocationName,
			StatusDateText: data.StatusDateText,
			StatusName: data.StatusName,
			PostedBy: data.PostedBy,
			NetworkName: data.NetworkName,
			ImageHtml: image,
			SocialUrl: data.SocialUrl,
			Message: message
		});
		
}

function Init() {
	
	var scheduler = $('#PostCalendar').data("kendoScheduler");
	scheduler.dataSource.transport.parameterMap = function (options, operation) {
		var result = {
			Start: kendo.format('{0:M-dd-yyyy}', scheduler.view().startDate()),
			End: kendo.format('{0:M-dd-yyyy}', scheduler.view().endDate())
		}
		return result;
	}
	$('#PostCalendar').data().kendoScheduler.dataSource.read();

}

$(function () {

	$(".calendar").kendoTooltip({
		filter: ".k-event,.k-task",
		width: 400,
		//height: 200,
		position: "bottom",
		//showAfter: 100,
		showOn: "click",
		autoHide: false,
		animation: {
			open: {
				effects: "fade:in",
				duration: 150
			}
		},
		content: function (e) {
			var waitImg = $("<span class='loading-icon'></span");
			$(e.target).find(".k-event-template").prepend(waitImg);

			var html = EventDetails(e.target);
			waitImg.remove();
			return html;
		}

	});


})
