﻿
function rssListItemBound() {
	$(".rssitem-publish").click(function () {
		ShowComposeWindow($(this).data("rssitemid"), false);
	});

	$(".rssitem-postimage").click(function () {
		ShowComposeWindow($(this).data("rssitemid"), true);
	});
		
	$("#TopPage").kendoPager({
		autoBind: true,
		numeric: true,
		pageSizes: [8, 12, 24, 48, 96],
		refresh: true,
		messages: {
			display: "Showing {0}-{1} from {2} Content",
			empty: "No Content found.",
			itemsPerPage: "Content per page"
		},
		dataSource: $('#RSSItemsListView').data("kendoListView").dataSource
	});
}


function PostSaved(message) {
	$("#PublishWindow").data("kendoWindow").close();
	$("#saveResults").html(message);
}

function ShowComposeWindow(rssItemID, imagePost) {
	var kendoWindow = $("#PublishWindow").data("kendoWindow");
	if (!kendoWindow) {
		kendoWindow = $("#PublishWindow").kendoWindow({
			title: "Compose a Message",
			content: '/Social/Social/ComposeMessage?mode=False&rssItemID=' + rssItemID + '&imagePost=' + imagePost,
			modal: true,
			iframe: false,
			draggable: true,
			visible: false,
			width: 1000,
			//height: 1100,
			actions: ["Refresh", "Maximize", "Close"],
			refresh: function () {
				$(".k-window-content").fadeIn("fast");
				this.center();
			}
		}).data("kendoWindow");
	} else {
		//$(".social-compose-message").html("<div class='loading-image' style='height: 48px;'></div>");
		$(".k-window-content").hide();
		kendoWindow.refresh({ url: '/Social/Social/ComposeMessage?mode=False&rssItemID=' + rssItemID + '&imagePost=' + imagePost });
	}

	kendoWindow.center();
	kendoWindow.open();
}

function searchCriteria() {
	return {
		search: $("#SearchText").val(),
		timeframe: $("#TimeFrame").data("kendoDropDownList").value(),
		franchise: $("#Franchise").data("kendoDropDownList").text()
	};
}

$(function () {
	
	$("#SearchButton").click(function () {
		var ds = $('#RSSItemsListView').data("kendoListView").dataSource;
		//ds.transport.options.read.url = "/Social/Social/ContentLibraryRSSListView?search=" + $("#SearchText").val();
		ds.page(1);
		//ds.read();
	});

	$(".rss-items").kendoTooltip({
		filter: ".image",
		width: 800,
		height: 420,
		position: "center",
		showOn: "click",
		autoHide: false,
		animation: {
			open: {
				effects: "fade:in",
				duration: 150
			}
		},
		content: kendo.template($("#tooltip-template").html())
	});
});

