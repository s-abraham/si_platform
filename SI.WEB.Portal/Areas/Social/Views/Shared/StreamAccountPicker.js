﻿

var treeSelectedAccount = 0;

function treeSelect(e) {
	var checkbox = $(e.node).find(":checkbox");
	var checked = checkbox.prop("checked");
	var item = e.sender.dataSource.getByUid($(e.node).data("uid"));
	item.checked = !checked;
	checkbox.prop("checked", !checked);
		
	treeSelectedAccount = checkbox.val();
		
	var tabStrip = $("#StreamTabs").data("kendoTabStrip");
	var tabs = tabStrip.items();
	for (idx = 0; idx < tabs.length; idx++) {
		tabStrip.disable(tabs[idx]);
	}
		
	if (typeof FacebookStream == "object")
		FacebookStream.ClearPost();
	if (typeof TwitterStream == "object")
		TwitterStream.ClearTweets();
	if (typeof GoogleStream == "object")
		GoogleStream.ClearPost();

	var selectedTab = tabStrip.select().index();

	var hasFB = item.Networks.indexOf("Facebook") >= 0;
	var hasTW = item.Networks.indexOf("Twitter") >= 0;
	var hasGG = item.Networks.indexOf("Google") >= 0;

	if (hasFB) {
		tabStrip.enable(tabs[0], true);

		if (typeof FacebookStream == "object") {
			FacebookStream.LoadAccount(treeSelectedAccount);
		}
			
		if ((selectedTab == 1 && !hasTW) || (selectedTab == 2 && !hasGG)) {
			tabStrip.activateTab(tabs[0]);
		}
	}

	if (hasTW) {
		tabStrip.enable(tabs[1], true);

		if (typeof TwitterStream == "object")
			TwitterStream.LoadAccount(treeSelectedAccount);
			
		if ((selectedTab == 0 && !hasFB) || (selectedTab == 2 && !hasGG)) {
			tabStrip.activateTab(tabs[1]);
		}
	}
		
	if (hasGG) {
		tabStrip.enable(tabs[2], true);

		if (typeof GoogleStream == "object")
			GoogleStream.LoadAccount(treeSelectedAccount);

		if ((selectedTab == 0 && !hasFB) || (selectedTab == 1 && !hasTW)) {
			tabStrip.activateTab(tabs[2]);
		}
	}
}

function treeDataBound() {
	$("#AccountsTreeView").css("height", "" + $(window).height() - 158 + "px");
}

$(function() {
	//$("#LoadStream").click(function() {
	//	var items = $("#AccountsTreeView").data("kendoTreeView").getCheckedItems();
	//	console.log(items);
	//})
})

kendo.ui.TreeView.prototype.getCheckedItems = (function () {
	function getCheckedItems() {
		var nodes = this.dataSource.view();
		return getCheckedNodes(nodes);
	}

	function getCheckedNodes(nodes) {
		var node, childCheckedNodes;
		var checkedNodes = [];

		for (var i = 0; i < nodes.length; i++) {
			node = nodes[i];
			if (node.checked) {
				checkedNodes.push(node);
			}
			if (node.hasChildren) {
				childCheckedNodes = getCheckedNodes(node.children.view());
				if (childCheckedNodes.length > 0) {
					checkedNodes = checkedNodes.concat(childCheckedNodes);
				}
			}
		}
		return checkedNodes;
	}

	return getCheckedItems;
})();

