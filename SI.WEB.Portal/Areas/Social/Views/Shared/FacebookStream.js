﻿
var FacebookStream = {
	selectedAccountID: 0,
	lastUntil: "",
	postDataSource: null,
		
	ClearPost: function() {
		$(".facebook-post").remove();
	},
		
	LoadAccount: function (accountID) {
		$("#More").hide();
		FacebookStream.selectedAccountID = accountID;
		FacebookStream.lastUntil = "";
		FacebookStream.postDataSource.transport.options.read.data.accountID = accountID;
		FacebookStream.postDataSource.transport.options.read.data.until = FacebookStream.lastUntil;
		FacebookStream.postDataSource.read();
	},

	dataBound: function (e) {
		kendo.ui.progress($("#posts"), false);
		kendo.ui.progress($("#MoreDiv"), false);

		if (e.response != null && e.response.length > 0) {
			FacebookStream.lastUntil = e.response[0].Until;
			$("#More").show();
				
			setTimeout(function() {
				$(".autogrow").autogrow();

				$(".comment-link").click(function(e) {
					$("#commentArea" + $(this).data("id")).show();
				});

				$(".like-link").click(function(e) {
					var me = $(this);
					var postId = me.data("id");
					var credId = me.data("credentialid");
					var post = me.closest(".facebook-post");
					kendo.ui.progress(post, true);
					var url = '/Social/Social/FacebookLike';
					$.post(url, { postID: postId, credentialID: credId }, function (data) {
						kendo.ui.progress(post, false);
						if (!data.success)
							SIGlobal.alertDialog("Post Like", data.message, "e");
						else {
							$("#LikeCount" + postId).show();
							var count = $("#LikeCount" + postId + " .count").html();
							if (count == "")
								count = 1;
							else {
								count = parseInt(count) + 1;
							}
							$("#LikeCount" + postId + " .count").html(count);
						}
					});
				});

				$(".comment-save").click(function(e) {
					var id = $(this).data("id");
					kendo.ui.progress($("#commentArea" + id), true);

					var url = '/Social/Social/FacebookCommentSave';
					$.post(url, { postID: id, credentialID: $(this).data("credentialid"), comment: $("#comment" + id).val() }, function(data) {
						kendo.ui.progress($("#commentArea" + id), false);
						$("#comment" + id).val("");
						if (!data.success)
							SIGlobal.alertDialog("Save Comment", data.message, "e");
						else {
							$("#moreComments" + id).show();
							$("#viewComments" + id).trigger("click");
						}
					});
				});

				$(".post-delete").click(function() {
					var me = $(this);
					var postId = me.data("id");
					var credId = me.data("credentialid");
					var post = me.closest(".facebook-post");

					SIGlobal.confirmDialog("Delete Post", "Are you sure you want to Delete the Post?", function(button) {
						if (button == "Yes") {
							kendo.ui.progress(post, true);
							var url = '/Social/Social/FacebookPostDelete';
							$.post(url, { postID: postId, credentialID: credId }, function(data) {
								kendo.ui.progress(post, false);
								if (!data.success)
									SIGlobal.alertDialog("Delete Post", data.message, "e");
								else {
									post.remove();
								}
							});
						}
					});
				});

				$(".facebook-post").each(function() {

					$(this).hover(function() { $(this).find(".post-delete").css("visibility", "visible"); }, function() { $(this).find(".post-delete").css("visibility", "hidden"); });

					var commentBlock = $(".post-comments", this);
					if (commentBlock.data("loaded") == false) {
						commentBlock.data("loaded", true);
						var id = commentBlock.data("id");
						var credId = commentBlock.data("credentialid");

						$("#viewComments" + id).click(function() {
							var me = $(this);
							var ds = $("#postComments" + me.data("id")).data("kendoListView").dataSource;
							ds.transport.options.read.data.pageSize = ds.transport.options.read.data.pageSize + 50;
							ds.transport.options.read.data.type = me.data("type");
							ds.read();
						});

						commentBlock.kendoListView({
							dataSource: new kendo.data.DataSource({
								transport: {
									read: {
										url: '/Social/Social/FacebookStreamCommentsListView',
										dataType: "json",
										data: { fbPostID: id, credentialID: credId, pageSize: 3, type: "" }
									}
								}
							}),
							dataBound: function() {
								$("#commentCount" + id).html(this.dataSource._data.length);

								if (this.dataSource._data.length > 0) {
									$("#commentArea" + id).show();
									$("#moreComments" + id).show();

									var comment = $(".comment", this.wrapper);
									comment.hover(function() { $(this).find(".delete").css("visibility", "visible"); }, function() { $(this).find(".delete").css("visibility", "hidden"); });
									comment.find(".delete").click(function() {
										var commentId = $(this).data("id");
										var postID = $(this).data("postid");
										var postComments = $("#postComments" + postID);
										var credId = postComments.data("credentialid");
										SIGlobal.confirmDialog("Delete Comment", "Are you sure you want to Delete the Comment?", function(button) {
											if (button == "Yes") {
												kendo.ui.progress(postComments, true);
												var url = '/Social/Social/FacebookCommentDelete';
												$.post(url, { commentID: commentId, credentialID: credId }, function(data) {
													kendo.ui.progress(postComments, false);
													if (!data.success)
														SIGlobal.alertDialog("Delete Comment", data.message, "e");
													else {
														$("#viewComments" + postID).trigger("click");
													}
												});
											}
										});
									});
									comment.find(".like-link").click(function() {
										var me = $(this);
										var commentId = me.data("id");
										var credId = me.data("credentialid");
											
										if (me.html() == "Unlike") {
											FacebookStream.UnlikeComment(comment, commentId, credId, me);
											return;
										}
										FacebookStream.LikeComment(comment, commentId, credId, me);
											
									});
										
									comment.find(".unlike-link").click(function () {
										var me = $(this);
										var commentId = me.data("id");
										var credId = me.data("credentialid");
										if (me.html() == "Like") {
											FacebookStream.LikeComment(comment, commentId, credId, me);
											return;
										}
										FacebookStream.UnlikeComment(comment, commentId, credId, me);
										
									});
								}
							},
							template: kendo.template($("#commentTemplate").html())
						});
					}
				});

			}, 100);
		}
	},

	UnlikeComment: function(comment, commentId, credId, link){
		kendo.ui.progress(comment, true);
		var url = '/Social/Social/FacebookCommentUnLike';
		$.post(url, { commentID: commentId, credentialID: credId }, function (data) {
			kendo.ui.progress(comment, false);
			if (!data.success)
				SIGlobal.alertDialog("Comment Like", data.message, "e");
			else {
				var likeCount = $("#likecount" + commentId);
				var count = $(".count", likeCount).html();
				count = parseInt(count) - 1;
				if (count <= 0) {
					likeCount.hide();
					$(".count", likeCount).html("");
				}
				else {
					$(".count", likeCount).html(count);
				}
				link.html("Like");
			}
		});
	},
		
	LikeComment: function (comment, commentId, credId, link) {
		kendo.ui.progress(comment, true);
		var url = '/Social/Social/FacebookCommentLike';
		$.post(url, { commentID: commentId, credentialID: credId }, function (data) {
			kendo.ui.progress(comment, false);
			if (!data.success)
				SIGlobal.alertDialog("Comment Like", data.message, "e");
			else {
				var likeCount = $("#likecount" + commentId).show();
				var count = $(".count", likeCount).html();
				if (count == "")
					count = 1;
				else {
					count = parseInt(count) + 1;
				}
				$(".count", likeCount).html(count);

				link.html("Unlike");
			}
		});
	}
};

$(function() {
	var template = kendo.template($("#streamTemplate").html());
	FacebookStream.postDataSource = new kendo.data.DataSource({
		transport: {
			read: {
				url: '/Social/Social/FacebookStreamListView',
				dataType: "json",
				data: { until: "", accountID: FacebookStream.selectedAccountID }
			}
		},
		requestStart: function() {
			kendo.ui.progress($("#posts"), true);
		},
		requestEnd: FacebookStream.dataBound,
		change: function() {
			$("#posts").append(kendo.render(template, this.view()));
		}
	});

	if (treeSelectedAccount > 0) {
		FacebookStream.selectedAccountID = treeSelectedAccount;
		FacebookStream.postDataSource.transport.options.read.data.accountID = treeSelectedAccount;
		FacebookStream.postDataSource.read();
	}

	$("#More").click(function() {
		kendo.ui.progress($("#MoreDiv"), true);
		FacebookStream.postDataSource.transport.options.read.data.until = FacebookStream.lastUntil;
		FacebookStream.postDataSource.read();

	});
	
});
	
