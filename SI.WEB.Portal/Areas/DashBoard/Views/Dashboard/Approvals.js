﻿
var gridSelector = "#ApprovalNotificationGrid";

$(function() {
    $("#window").kendoWindow({
    	width: "400px",
    	minHeight: "392px",
    	title: "Post Image",
    	actions: ["Close"],
    	visible: false
    });
	    
    $("#ApproveAllPending").click(function() {
    	ApproveAll("approve");
    })
    $("#AutoApproveAll").click(function () {
    	ApproveAll("auto-approve");
    })

});

function ApproveAll(action) {
	var data = $(gridSelector).data().kendoGrid.dataSource.data();
	var ids = [];
	for (var idx = 0; idx < data.length; idx++) {
		if (data[idx].Status == "Pending Approval")
			ids.push(data[idx].ID);
	}

	var url = '/Dashboard/Dashboard/PostApprovalActionAll';
	var addData = { postApprovalIDS: ids.join(), action: action };
	$.post(url, addData, function (data) {
		if (data.success) {
			$(gridSelector).data("kendoGrid").dataSource.read();
		}
		SIGlobal.alertDialog("Post Approval All", data.message);
	});
}
	
function approvalsBound() {
	$(".approve-post").click(function (e) {
		e.preventDefault();
		e.stopPropagation();

		var data = GetRowData(this);
		SendAction(data.ID, "approve");
	});
		
	$(".reject-post").click(function (e) {
		e.preventDefault();
		e.stopPropagation();

		var data = GetRowData(this);
		SendAction(data.ID, "reject");
	});
	    
	$(".edit-post").click(function (e) {
		e.preventDefault();
		e.stopPropagation();

		var data = GetRowData(this);
		ShowReviseWindow(data.PostID, data.TargetAccountID);
	});

	$(".unapprove-post").click(function (e) {
		e.preventDefault();
		e.stopPropagation();

		var data = GetRowData(this);
		SendAction(data.ID, "unapprove");
	});

	$(".unreject-post").click(function (e) {
		e.preventDefault();
		e.stopPropagation();

		var data = GetRowData(this);
		SendAction(data.ID, "unreject");
	});
}
	
function SendAction(id, type) {
	var url = '/Dashboard/Dashboard/PostApprovalAction';
	var addData = { postApprovalID: id, action: type };
	$.post(url, addData, function (data) {
		if (data.success) {
			$(gridSelector).data("kendoGrid").dataSource.read();
		}
		SIGlobal.alertDialog("Post Approval", data.message);
	});
}
	
function getDescription(imageUrl, link, description) {
	var templt = kendo.template($("#description-template").html());
	return templt({ imageUrl: imageUrl, link: link, description: description });
}
	
function getActions(status, id, allowChanges) {
	var templt = kendo.template($("#action-template").html());
	return templt({ status: status, ID: id, allowChanges: allowChanges });
}

function getActionsHistory(status, id, AllowApprove, AllowDeny, AllowEdit, AllowUnApprove, AllowUnDeny) {

	//AllowApprove = true; AllowDeny = true; AllowEdit = true; AllowUnApprove = true; AllowUnDeny = true;

	var templt = kendo.template($("#action-history-template").html());
	return templt({ status: status, ID: id, allowApprove: AllowApprove, allowDeny: AllowDeny, allowEdit: AllowEdit, allowUnApprove: AllowUnApprove, allowUnDeny: AllowUnDeny });
}
	
function ShowImage(url) {
	$("#window").data("kendoWindow").content("<div style='text-align: center; width: 100%; height: 100%;white-space: nowrap;min-height: 392px;'><span style='display: inline-block;height: 100%;min-height: 392px;vertical-align: middle;'></span><img style='vertical-align: middle;border: 1px solid darkgray;' src='" + url + "'/></div>").center().open().title("Post Image");
		
}

function GetRowData(row) {
	return $(gridSelector).data().kendoGrid.dataSource.getByUid($(row).closest('tr').data("uid"));
}

function PostSaved(message) {
	$("#ReviseWindow").data("kendoWindow").close();
	$("#saveResults").html(message);
	$(gridSelector).data("kendoGrid").dataSource.read();
}

function ShowReviseWindow(postID, accountID) {
	var kendoWindow = $("#ReviseWindow").data("kendoWindow");
	if (!kendoWindow) {
		kendoWindow = $("#ReviseWindow").kendoWindow({
			title: "Edit a Message",
			content: '/Social/Social/ReviseSyndicationPost?postID=' + postID + '&accountID=' + accountID,
			modal: true,
			iframe: false,
			draggable: true,
			visible: false,
			width: 1000,
			minHeight: 300,
			actions: ["Refresh", "Maximize", "Close"],
			refresh: function () {
				kendo.ui.progress($(".k-window"), false);
				$(".k-window-content").fadeIn("fast");
				this.center();
			}
		}).data("kendoWindow");
	} else {
		$(".k-window-content").hide();
		kendoWindow.refresh({ url: '/Social/Social/ReviseSyndicationPost?postID=' + postID + '&accountID=' + accountID });
	}
		
	kendo.ui.progress($(".k-window"), true);
	kendoWindow.center();
	kendoWindow.open();
}

function ApplyFilter() {
	$(gridSelector).data("kendoGrid").dataSource.read();
}

function getFilters() {
	var timeFrame = $("#TimeFrame").data("kendoDropDownList").value();
	var statusFilter = $("#StatusFilter").data("kendoDropDownList").value();
	var networkFilter = $("#NetworkFilter").data("kendoDropDownList").value();

	return {
		timeFrame: timeFrame,
		statusFilter: statusFilter,
		networkFilter: networkFilter

	}
}

