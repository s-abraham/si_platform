﻿
	
$(function () {

	$("#activeFilter").click(function () {
		if ($(".stream-filter").css("display") == "block")
			$(".stream-filter").slideUp("fast");
		else
			$(".stream-filter").slideDown("fast");
	});


});

function filterLocations(e) {
	var filterSel = $("#TimeFrameFilter").val();
	var filterNet = $("#NetworkStatus").val();
	if (filterSel != "Custom") {
		var ds = $("#LocationList").data().kendoGrid.dataSource;
		ds.transport.options.read.url = '/Dashboard/Dashboard/SocialLocationsGrid' + "?timeFrame=" + filterSel + "&network=" + filterNet;
		ds.read();
	} else {
		$(".stream-filter").slideDown("fast");
		$(".filter-dates").fadeIn();
	}
}
	
function filterNetworks(e) {
	var filterSel = $("#TimeFrameFilter").val();
	var filterNet = $("#NetworkStatus").val();
	var ds = $("#LocationList").data().kendoGrid.dataSource;
	ds.transport.options.read.url = '/Dashboard/Dashboard/SocialLocationsGrid' + "?timeFrame=" + filterSel + "&network=" + filterNet;
	ds.read();
}

function getLocation(dealerLoc) {
	var templt = kendo.template($("#location-template").html());
	return templt({ DealerLocation: dealerLoc });
}
	
function getNetworks(networks, uri) {
	if (networks != "") {
		var nets = networks.split(',');
		var netUri = uri.split('|');
		if (nets.length > 0) {
			var templt = kendo.template($("#network-template").html());
			return templt({ facebook: nets[0], twitter: nets[1], google: nets[2], fbUri: netUri[0], twUri: netUri[1], ggUri: netUri[2] });
		}
	}
}

function locationsDataBound(e) {
	$("#LocationList tbody tr .reputation-filter-links").each(function () {
		var me = this;
		$(me).click(function () {
			var filter = $(this).data("filter");
			$("#filterParameters").val(filter);
			$("#ReviewStreamForm").submit();
		});
	});


	$("#LocationList tbody tr .sites-filter-links").each(function() {
		var me = this;
		$(me).click(function () {
			var filter = $(this).data("filter");
			$("#filterParameters").val(filter);
			$("#ReviewStreamForm").attr("action", '/Reputation/ReviewSites').submit();
		});
	});
}

function GoToRepLocation() {
	SIGlobal.showGridErrors = false;
	document.location.href = '/Dashboard/ReputationLocations';
	return false;
}

function ShowConfigWindow(accountID) {

	var accountName = "";
	var view = $("#LocationList").data().kendoGrid.dataSource.view();
	for (var i = 0; i < view.length; i++) {
		node = view[i];
		if (node.DealerLocation.LocationID == accountID) {
			accountName = node.DealerLocation.Name;
			break;
		}
	}

	var kendoWindow = $("#AccountEdit").data("kendoWindow");
	if (!kendoWindow) {
		kendoWindow = $("#AccountEdit").kendoWindow({
			content: '/Admin/Admin/SocialConfig?accountID=' + accountID,
			modal: true,
			iframe: false,
			draggable: true,
			visible: false,
			width: 700,
			minHeight: 450,
			actions: ["Maximize", "Close"],
			refresh: function () {
				this.center();
			}
		}).data("kendoWindow");
	} else {
		kendoWindow.refresh({ url: '/Admin/Admin/SocialConfig?accountID=' + accountID });
	}

	kendoWindow.title("Product Configuration - " + accountName);
	kendoWindow.center();
	kendoWindow.open();
}

function NetworkData() {
	var status = $("#NetworkStatus").data("kendoDropDownList").value();
	
	return {
		network : status
	}
}