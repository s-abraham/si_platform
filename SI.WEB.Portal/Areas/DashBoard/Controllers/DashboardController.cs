﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AttributeRouting;
using AttributeRouting.Web.Mvc;
using Kendo.Mvc;
using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using SI.BLL;
using SI.DTO;
using SI.Extensions;
using SI.Services.Admin;
using SI.Services.Admin.Models;
using SI.Services.Dashboard;
using SI.Services.Dashboard.Models;
using SI.WEB.Portal.Framework;
using SI.WEB.Portal.Models;

namespace SI.WEB.Portal.Areas.DashBoard.Controllers
{
	[Authorize]
	[RouteArea("Dashboard")]
    public class DashboardController : Controller
    {
		[Route("Overview", RouteName = "DashboardOverview")]
		[BaseMembersModelAction]
		public ActionResult DealerDashboard()
		{
			BaseMembersModel baseModel = ViewBag.BaseModel as BaseMembersModel;
            Overview overview = DashboardService.Overview(baseModel.UserInfo, baseModel.UserInfo.EffectiveUser.CurrentContextAccountID);

			return View(overview);
		}
		
		[Route("ReputationLocations", RouteName = "DashboardReputationLocations")]
		[BaseMembersModelAction("CanDashboardReputationLocation")]
		public ActionResult ReputationLocations()
		{
			return View();
		}

		[Route("SocialLocations", RouteName = "DashboardSocialLocations")]
		[BaseMembersModelAction("CanSocialLocation")]
		public ActionResult SocialLocations()
		{
			return View();
		}

		[Route("Social", RouteName = "DashboardSocial")]
		[BaseMembersModelAction]
		public ActionResult SocialDashboard()
		{
			return View();
		}
	
		public JsonResult LocationsGrid([DataSourceRequest] DataSourceRequest request, string timeFrame)
		{
			//TODO Filter on Timeframe
            BaseMembersModel model = new BaseMembersModel();
            List<Dealerships> dealerships = DashboardService.LocationsGrid(model.UserInfo, KendoSupport.PagedGridRequest(request), timeFrame);

			return Json(dealerships.ToDataSourceResult(request), JsonRequestBehavior.AllowGet);
		}


		public JsonResult SocialLocationsGrid([DataSourceRequest] DataSourceRequest request, string timeFrame, string network = "connected")
		{
			//TODO DUMMY DATA Filter on Timeframe
            BaseMembersModel model = new BaseMembersModel();
			List<SocialDealerships> dealerships = DashboardService.SocialLocationsGrid(model.UserInfo, KendoSupport.PagedGridRequest(request), timeFrame, network);

			return Json(dealerships.ToDataSourceResult(request), JsonRequestBehavior.AllowGet);
		}

		public ActionResult TotalToDos()
		{
			//TODO Add Todos and Notifications
			return Content("12");
		}

		public ActionResult Notifications()
		{
			BaseMembersModel model = new BaseMembersModel();
			ViewBag.baseModel = model;

			return PartialView("_Notifications");
		}

		public JsonResult ToDoListView([DataSourceRequest] DataSourceRequest request)
		{
			BaseMembersModel model = new BaseMembersModel();
			
			//TODO DUMMY DATA
			List<ToDoSummary> toDos = DashboardService.ToDoListView(model.UserInfo, KendoSupport.PagedGridRequest(request), model.AssetImagePath);

			return Json(toDos.ToDataSourceResult(request), JsonRequestBehavior.AllowGet);
		}

		public JsonResult NotificationListView([DataSourceRequest] DataSourceRequest request)
		{
			BaseMembersModel model = new BaseMembersModel();
			List<NotificationSummary> notification = DashboardService.GetGlobeNotifications(model.UserInfo);

			return Json(notification.ToDataSourceResult(request), JsonRequestBehavior.AllowGet);
		}

		[BaseMembersModelAction]
		public ActionResult MyPreferences()
		{
			return PartialView("_UserPreferences");
		}
	
		public ActionResult UserPreferencesNotifications()
		{
			BaseMembersModel model = new BaseMembersModel();
			MyPrefAlertEdit myPrefAlertEdit = DashboardService.GetUserPerferences(model.UserInfo);

			return PartialView("_UserPreferencesNotifications", myPrefAlertEdit);
		}

		public JsonResult MyPrefGridList([DataSourceRequest] DataSourceRequest request)
		{
			//TODO DUMMY DATA
            BaseMembersModel model = new BaseMembersModel();
			List<MyPrefNotifications> myPref = DashboardService.MyPrefGridList(model.UserInfo, KendoSupport.PagedGridRequest(request));

			return Json(myPref.ToDataSourceResult(request), JsonRequestBehavior.AllowGet);
		}

		[HttpPost]
		//public ActionResult SaveMyPrefNotifications(IEnumerable<MyPrefNotifications> myPrefNotifications)
		public ActionResult SaveMyPrefNotifications(bool disabledAll)
		{
			BaseMembersModel model = new BaseMembersModel();
			DashboardService.SaveUserPerferences(model.UserInfo, disabledAll);

			return Json(new { success = true, message = string.Format("Saved on {0}", DateTime.Now.ToString(CultureInfo.InvariantCulture)) });
		}

		[HttpPost]
		public ActionResult SaveMyPrefDetails(MyPrefDetails myPrefDetails)
		{
			//TODO return message with users Timezone now
			return Json(new { success = true, message = string.Format("Saved on {0}", DateTime.Now.ToString(CultureInfo.InvariantCulture)) });
		}

		public ActionResult UserPreferencesPassword()
		{
			MyPrefPassword myPrefPassword = new MyPrefPassword();

			return PartialView("_UserPreferencesPassword", myPrefPassword);
		}

		[HttpPost]
		[ValidateAntiForgeryToken]
		public ActionResult UserPreferencesPassword(MyPrefPassword myPrefPassword)
		{
            BaseMembersModel bmm = new BaseMembersModel();

			string modelErrors = "";
			bool success = false;

			if (ModelState.IsValid)
			{
				string Problem = "";
				success = DashboardService.UserPreferencesPassword(bmm.UserInfo, myPrefPassword, ref Problem);

				if (!success)
					modelErrors = Problem;
			}
			else
				modelErrors = SIHelpers.ModelErrorsToHtml(ModelState);

			string returnStatus;

			if (success)
				returnStatus = string.Format("Saved on {0}", System.DateTime.Now.ToString(CultureInfo.InvariantCulture));
			else
				returnStatus = string.Format("Unable to save at this time. {0}", modelErrors);

			return Json(new { success, message = returnStatus });
		}

		public ActionResult NotificationConfig(int? configID)
		{
			//TODO DUMMY DATA
			NotificationConfigItem notificationConfigItem = DashboardService.NotificationConfig(configID.GetValueOrDefault());

			return PartialView("_NotificationConfig", notificationConfigItem);
		}
		
		public JsonResult NotificationConfigAccounts(int? configID)
		{
			//TODO ALL THIS JUST FOR DUMMY DATA, return the accounts associated with the USer

			BaseMembersModel model = new BaseMembersModel();

			//TODO REPLACE with a new a

			int TotalCount = 0;


			DataSourceRequest request = new DataSourceRequest();
			request.Page = 1;
			request.PageSize = 100;
			request.Sorts = new List<SortDescriptor>();
			request.Sorts.Add(new SortDescriptor() { Member = "Name", SortDirection = ListSortDirection.Ascending });

			List<AccountList> accounts = AdminService.AccountList(model.UserInfo, KendoSupport.PagedGridRequest(request), ref TotalCount);

			//TODO THIS TO REDUCE THE PAYLOAD
			List<AccountFilterList> accountFilter = new List<AccountFilterList>();
			foreach (AccountList accountList in accounts)
			{
				AccountFilterList filterList = new AccountFilterList();
				filterList.ID = accountList.ID;
				filterList.Name = accountList.Name;

				accountFilter.Add(filterList);
			}

			return Json(accountFilter, JsonRequestBehavior.AllowGet);
		}

		[Route("NotificationManager", RouteName = "NotificationManage")]
		[BaseMembersModelAction]
		public ActionResult NotificationManage()
		{
			return View("NotificationManage");
		}

		public JsonResult NotificationManageToDoGrid([DataSourceRequest] DataSourceRequest request, bool? showCompleted)
		{
			//TODO the default query is NON completed,  change for all

            BaseMembersModel model = new BaseMembersModel();
			List<NotificationsToDo> notificationstodo = DashboardService.NotificationManageToDoGrid(model.UserInfo, KendoSupport.PagedGridRequest(request), showCompleted);

            return Json(notificationstodo.ToDataSourceResult(request), JsonRequestBehavior.AllowGet);
		}

        public JsonResult NotificationManageAlertGrid([DataSourceRequest] DataSourceRequest request)
		{
			//TODO the default query is NO Cleared

            BaseMembersModel model = new BaseMembersModel();

	        List<NotificationsAlert> toDos = DashboardService.GetNotificationList(model.UserInfo);

			return Json(toDos.ToDataSourceResult(request), JsonRequestBehavior.AllowGet);
		}

		[Route("Approvals", RouteName = "NotificationApprovals")]
		[BaseMembersModelAction]
		public ActionResult Approvals()
		{
			return View("Approvals");
		}

		public JsonResult NotificationApprovalsGrid([DataSourceRequest] DataSourceRequest request)
		{
			BaseMembersModel model = new BaseMembersModel();

			List<NotificationsApprovals> approvals = DashboardService.GetPendingApprovals(model.UserInfo);
                        
			return Json(approvals.ToDataSourceResult(request), JsonRequestBehavior.AllowGet);
		}

		[Route("ApprovalHistory", RouteName = "NotificationApprovalHistory")]
		[BaseMembersModelAction]
		public ActionResult ApprovalHistory()
		{
			return View();
		}

		public JsonResult NotificationApprovalHistoryGrid([DataSourceRequest] DataSourceRequest request, string timeFrame, string statusFilter, string networkFilter)
		{
			BaseMembersModel model = new BaseMembersModel();
			int TotalCount = 0;

			List<NotificationsApprovalHistory> approvals = DashboardService.GetPendingApprovalHistory(model.UserInfo, KendoSupport.PagedGridRequest(request), timeFrame, statusFilter, networkFilter, ref TotalCount);

			DataSourceResult result = new DataSourceResult();
			result.Data = approvals;
			result.Total = TotalCount;

			return Json(result, JsonRequestBehavior.AllowGet);
		}

		[HttpPost]
		public ActionResult CompleteToDo(int? notificationID)
		{
			//TODO Complete to todos
			bool success = DashboardService.CompleteToDo(notificationID.GetValueOrDefault());

			return Json(new { success, message = "ToDo Completed notificationID: " + notificationID });
		}

		[HttpPost]
		public ActionResult PostApprovalAction(long? postApprovalID, string action)
		{
			BaseMembersModel model = new BaseMembersModel();

			bool success = DashboardService.PostApprovalAction(model.UserInfo, postApprovalID.GetValueOrDefault(), action);
			string message = string.Format("Could not {0} Post at this time.", action);

            if (success)
            {
                switch (action)
                {
                    case "approve":
                        message = "Post has been approved and will be published per schedule.";
                        break;
                    case "auto-approve":
                        message = "All Posts have been approved and Auto-Approved is set.";
                        break;
                    default:
                        message = "Post has been denied and will not be published";
                        break;
                }                
            }

			return Json(new { success, message });
		}

		[HttpPost]
		public ActionResult PostApprovalActionAll(string postApprovalIDS, string action)
		{
			BaseMembersModel model = new BaseMembersModel();

			bool success = DashboardService.PostApprovalActionAll(model.UserInfo, postApprovalIDS, action);
			string message = string.Format("Could not {0} Post at this time.", action);
            if (success)
            {
                switch (action)
                {
                    case "approve":
                        message = "Post has been approved and will be published per schedule.";
                        break;
                    case "auto-approve":
                        message = "All Posts have been approved and Auto-Approved is set.";
                        break;
                    default:
                        message = "Post has been denied and will not be published";
                        break;
                }
            }

			return Json(new { success, message });
		}
		
		[HttpPost]
		public ActionResult ClearAlert(int? notificationID)
		{
			//TODO Add to Clear alert
			bool success = DashboardService.ClearAlert(notificationID.GetValueOrDefault());

			return Json(new { success, message = "Clear Alert notificationID: " + notificationID });
		}

		[HttpPost]
		public ActionResult ClearAllAlerts()
		{
			//TODO Add to Clear ALL alert
            BaseMembersModel model = new BaseMembersModel();
			bool success = DashboardService.ClearAllAlerts(model.UserInfo);

			return Json(new { success, message = "Clear ALL Alerts"} );
		}
		
		[OutputCache(Duration = 36000)]
		public ActionResult FacebookSocialDashboard(string timeFrame = "Last 30 Days")
		{
			BaseMembersModel model = new BaseMembersModel();

			FBSocialDashboard fbSocial = DashboardService.SocialDashboard(model.UserInfo, timeFrame);

			return PartialView("_FacebookSocialDashboard", fbSocial);
		}

		[OutputCache(Duration = 36000)]
		public ActionResult TwitterSocialDashboard(string timeFrame = "Last 30 Days")
		{
			BaseMembersModel model = new BaseMembersModel();

			TWSocialDashboard twSocial = DashboardService.SocialDashboardTwitter(model.UserInfo, timeFrame);

			return PartialView("_TwitterSocialDashboard", twSocial);
		}

		public ActionResult GoogleSocialDashboard()
		{
			return PartialView("_GoogleSocialDashboard");
		}

    }
}
