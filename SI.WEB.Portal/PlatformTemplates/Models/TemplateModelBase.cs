﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SI.BLL;

namespace SI.WEB.Portal.PlatformTemplates.Models
{
	public class TemplateModelBase
	{
		public TemplateModelBase(string currentUrl = "")
        {
			string themeName = "aristo";
			if (!string.IsNullOrEmpty(currentUrl))
			{
				IThemeProvider tp = ProviderFactory.Themes;
				themeName = tp.GetThemeName(currentUrl);
			}
				
            BaseURL = Settings.GetSetting("site.baseurl");
            CSSUrl = string.Format("{0}/style/{1}/{2}", BaseURL, themeName, MvcApplication.AssetManager.GetThemeHash(themeName));
            JSUrl = string.Format("{0}/script/{1}", BaseURL, MvcApplication.AssetManager.JSHash);
			AssetImagePath = string.Format("{0}/assets/style/{1}/images/", BaseURL, themeName);
        }

        public string BaseURL { get; set; }
        public string CSSUrl { get; set; }
        public string JSUrl { get; set; }
		public string AssetImagePath { get; set; }
	}
}