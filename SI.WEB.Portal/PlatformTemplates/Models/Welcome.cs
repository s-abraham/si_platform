﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SI.WEB.Portal.Models;

namespace SI.WEB.Portal.PlatformTemplates.Models
{
	public class Welcome : TemplateModelBase
	{
		public string Title { get; set; }
		public string Body { get; set; }

		public Welcome(string currentUrl = "") : base(currentUrl)
		{
			
		}
	}
}