﻿app.controller('SIJobTypesController', function ($scope) {

    // load a model into the scope

    $scope.load = function (model) {
        if (model.success) {
            $scope.entity = model.entity;
            setTimeout("$('#" + $scope.dlgkey + "').dialog('option', 'title', 'Job Types')", 250);

        } else {
            $scope.error = model.message;
        }
    }

    // load the raw model
    $scope.load($scope.rawModel);

});