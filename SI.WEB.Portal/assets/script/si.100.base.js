﻿
$(document).ready(function () {
	$.ajaxSetup({ cache: false });
    siInit();
})

function siInit() {
    bindHovers();
    bindTabs();
}

function bindHovers() {
    $.each($('.dsel'), function () { bindHover(this); });
    $.each($('.sel'), function () { bindHover(this); });
}

function bindHover(elm) {
    if (!$(elm).hasClass('hbound')) {
        $(elm).mouseenter(function () {
            if ($(elm).hasClass('dsel')) {
                $(elm)
                    .removeClass('dsel')
                    .addClass('hov')
                ;
            }
        });
        $(elm).mouseleave(function () {
            if ($(elm).hasClass('hov')) {
                $(elm)
                    .removeClass('hov')
                    .addClass('dsel')
                ;
            }
        });
        $(elm).addClass('hbound');
    }
}

function bindTabs() {
    $.each($('div[si-tabgroup]'), function () {
        var tElement = $(this);

        if (!tElement.hasClass('tbound')) {

            var tabs = [];

            $.each($(tElement).children("div[si-tab]"), function () {

                $(this).addClass('si-dlg-content');
                var tabid = $(this).attr('si-tab');

                var selector = "div[si-tab-sel='" + tabid + "']:first";                
                var tiDiv = $(tElement).find(selector).first();
                var sel = tiDiv.hasClass("sel");

                tiDiv.data('tg', $(tElement));
                tiDiv.data('sel', sel);
                tiDiv.data('tab', $(this));

                tiDiv.click(function () {
                    var self = $(this);
                    if (!self.data('sel')) {
                        $.each($(self.data('tg')).data('tabs'), function () {
                            this.mnu.data('sel', false).addClass('dsel').removeClass('sel').removeClass('hov');
                            this.tab.hide();
                        });
                    }

                    self.data('sel', true).addClass('sel').removeClass('dsel').removeClass('hov');
                    $(self.data('tab')).show();
                });
                
                if (sel) $(this).show(); else $(this).hide();

                tabs.push({ tab: $(this), mnu: tiDiv });

            });

            $(tElement).data('tabs', tabs);

            tElement.addClass('tbound');
        }
    });
}