﻿app.controller('SIJobTypeController', function ($scope) {

    // load a model into the scope

    $scope.load = function (model) {
        if (model.success) {
            $scope.entity = model.entity;
            setTimeout("$('#" + $scope.dlgkey + "').dialog('option', 'title', '" + model.entity.Name + "')", 250);
            
        } else {
            $scope.error = model.message;
        }
    }

    //save the data
    $scope.save = function () {
        var model = ajxs('/members/' + $scope.type, $scope.entity, 'PUT');
        $scope.load(model);
    }

    // load the raw model
    $scope.load($scope.rawModel);

});