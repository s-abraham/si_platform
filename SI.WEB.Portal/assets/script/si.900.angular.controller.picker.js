﻿
app.controller('SIAccountPicker', function ($scope, $http, $timeout) {

    $scope.picker = {
        left: 0,
        top: 21,
        width: 450,
        height: 500,
        dropped: false,
        iclass: 'ico-circle-r',
        alist: [],
        tree: [],
        loading: false,
        promchange: null,
        ftext: 'Loading...',
        treemode: false,
        filters: {
            FranchiseTypeIDs: [],
            PackageIDs: [],
            SearchString: '',
            HasChildAccounts: false,
            SortBy: 1,
            SortAscending: true,
            StartIndex: 1,
            EndIndex: 100,
            PerPage: 100            
        },
        canceller: null
    };


    $scope.picker.gotoAllAccounts = function() {
        document.location.href = "/Admin/Accounts";
    };

    $scope.picker.apick = function() {
        $scope.picker.dropped = !$scope.picker.dropped;
        $scope.picker.statechange();
    };

    $scope.picker.statechange = function() {
        if ($scope.picker.dropped) {
            if ($scope.picker.firstLoad) {
                $scope.picker.firstLoad = false;
                $scope.picker.reload();
            }
            $scope.picker.position();
            $scope.picker.iclass = 'ico-circle-d';
            setTimeout("$('#apsearch').focus();", 150);
        } else {
            $scope.picker.iclass = 'ico-circle-r';
        }
    };

    $scope.picker.position = function() {
        var pos = $('#pickparent').position();
        var wid = $('#pickparent').width();

        $scope.picker.left = (pos.left - $scope.picker.width) + (wid - 10);

        var vHeight = $(window).height();
        $scope.picker.height = vHeight - 50;

    };

    $scope.picker.reload = function () {
        //if ($scope.picker.canceller != null) {
        //    $scope.picker.canceller.resolve();
        //}

        //$scope.picker.canceller = $q.defer();

        $scope.picker.loading = true;
        $http({
            url: '/members/getaccounts',
            data: $.j2MVC($scope.picker.filters),
            method: 'POST'
            //,timeout: $scope.picker.canceller.promise
        }).success(function(data, status, headers, config) {

            if ((data.SearchString == $scope.picker.filters.SearchString) || ($scope.picker.filters.SearchString==null) || ($scope.picker.filters.SearchString=='')) {
                $scope.picker.loading = false;
                $scope.picker.alist = data.Items;
                $scope.picker.treemode = false;
                $scope.picker.ftext = 'Showing ' + data.StartIndex + ' - ' + ((data.TotalRecords < data.EndIndex) ? data.TotalRecords : data.EndIndex) + ' of ' + data.TotalRecords;
            }

            $scope.picker.canceller = null;

        }).error(function(data, status, headers, config) {
            $scope.picker.loading = false;
            $scope.picker.canceller = null;
        });
    };

    $scope.toggle = function(id) {
        $scope.findAccountID($scope.picker.alist, id).expand = !$scope.findAccountID($scope.picker.alist, id).expand;
    };

    $scope.findAccountID = function(accounts, id) {
        for (var i = 0; i < accounts.length; i++) {
            if (accounts[i].ID == id) return accounts[i];
        }
        return null;
    };

    $scope.$watch('picker.filters.SearchString', function (val) {
        if ($scope.picker.dropped)
            $scope.picker.change();
    });

    $scope.picker.change = function() {
        if ($scope.loading) return;
        if ($scope.picker.promchange != null) {
            $timeout.cancel($scope.picker.promchange);
        }
        $scope.picker.promchange = $timeout($scope.picker.reload, 500, true);
    };

    $(window).resize(function ($scope) {

        // do a reposition here

    });


    $('#apseeall').keydown(function (e) {
        if (e.keyCode == 27) {
            appScope().$apply(function ($scope) {
                $scope.picker.apick();
            });
        }
    });

    $('#apsearch').keydown(function (e) {
        if (e.keyCode == 27) {
            appScope().$apply(function ($scope) {
                $scope.picker.apick();
            });
        }
    });
});