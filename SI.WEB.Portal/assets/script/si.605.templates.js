﻿/**********************************************************

SI Template Manager

This object lazy loads the various templates used
by dialogs and screens.

**********************************************************/

var SITemplates = new SITemplateManager();
function SITemplateManager() {

    this.get = function (type) {
        if (this.cache.hasOwnProperty(type) && this.cache[type] != null) {
            return this.cache[type];
        }

        var ret = ajxs('/template', { type: type });
        if (ret.success) {
            this.cache[ret.type] = ret;
            return this.cache[type];
        }
        return null;
    }

    this.reload = function (type) {
        this.cache[type] = null;
        this.get(type);
    }

    this.clear = function () {
        this.cache = {};
    }

    this.clear();
}