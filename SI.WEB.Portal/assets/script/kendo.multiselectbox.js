﻿// MultiSelectBox, Kendo Plugin
// -----------------------------------------------------------
(function ($) {
	var MultiSelectBox = window.kendo.ui.DropDownList.extend({

		init: function (element, options) {
			var me = this;
			
			options.name = element.id;
			// setup template to include a checkbox
			options.template = kendo.template(
                kendo.format('<label><input type="checkbox" onclick="$({3}).data().handler.checkboxClick();" name="{0}" value="#= {1} #" style="margin-top: 0;"/>&nbsp;#= {2} #</label>',
                    element.id + "_option_" + options.dataValueField,
                    options.dataValueField,
                    options.dataTextField,
	                ("'\\#" + element.id + "'")
                )
            );
			// remove option label from options, b/c DDL will create an item for it
			if (options.optionLabel !== undefined && options.optionLabel !== null && options.optionLabel !== "") {
				me.optionLabel = options.optionLabel;
				options.optionLabel = undefined;
			}
			
			// create drop down UI
			window.kendo.ui.DropDownList.fn.init.call(me, element, options);
			// setup change trigger when popup closes
			//me.popup.bind('close', function () {
			//	var values = me.ul.find(":checked")
            //        .map(function () { return this.value; }).toArray();
			//	// check for array inequality
			//	if (values < me.selectedIndexes || values > me.selectedIndexes) {
			//		me._setText();
			//		me._setValues();
			//		me.trigger('change', {});
			//	}
			//});
			
			me._setText();
			me.bind('dataBound', function () {
				me.element.val(me.value());
			});
		},
		
		checkboxClick: function () {
			var me = this;
			var values = me.ul.find(":checked").map(function () { return this.value; }).toArray();
			// check for array inequality
			if (values < me.selectedIndexes || values > me.selectedIndexes) {
				me._setText();
				me._setValues();
				me.trigger('change', {});
			}
		},

		options: {
			name: "MultiSelectBox"
		},

		optionLabel: "",

		selectedIndexes: [],

		_accessor: function (vals, idx) { // for view model changes
			var me = this;
			if (vals === undefined) {
				return me.selectedIndexes;
			}
		},

		value: function (vals) {
			var me = this;
			if (vals === undefined) { // for view model changes
				return me._accessor();
			} else { // for loading from view model
				var checkboxes = me.ul.find("input[type='checkbox']");
				if (vals.length > 0) {
					// convert to array of strings
					var valArray = vals
                        .map(function (item) { return item + ''; });
					checkboxes.each(function () {
						this.checked = $.inArray(this.value, valArray) !== -1;
					});
					me._setText();
					me._setValues();
				}
			}
		},

		_select: function (li) { }, // kills highlighting behavior
		_blur: function () { }, // kills popup-close-on-click behavior

		_setText: function () { // set text based on selections
			var me = this;
			var text = me.ul.find(":checked")
                .map(function () { return $(this).parent().text(); })
                .toArray();
			if (text.length === 0)
				me.text(me.optionLabel);
			else {
				text = text.join(', ');
				me.text(text);
				$(me.span).attr("title", text);
			}
		},
		_setValues: function () { // set selectedIndexes based on selection
			var me = this;
			var values = me.ul.find(":checked")
                .map(function () { return this.value; })
                .toArray();
			me.selectedIndexes = values;
			me.element.val(values);
		}

	});

	window.kendo.ui.plugin(MultiSelectBox);

})(jQuery);
// ===========================================================

// view model
// -----------------------------------------------------------
//var testVM = kendo.observable({
//	testItems: [],
//	testItemSource: new kendo.data.DataSource({
//		data: [
//            { Id: 1, Name: "Test 1" },
//            { Id: 2, Name: "Test 2" },
//            { Id: 3, Name: "Test 3" },
//            { Id: 4, Name: "Test 4" }
//		]
//	}),
//});
// ===========================================================

//$(document).ready(function () {
//	kendo.bind($("#testView"), testVM);
//});

//<div id="testView">
//    <select id="testItems"  multiple="multiple" data-role="multiselectbox" data-option-label="-" data-text-field="Name" data-value-field="Id" data-bind="source: testItemSource, value: testItems" />
//</div>