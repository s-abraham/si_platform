﻿//sets up the auto-fill for city/state when zip is entered
function bindZipFill(zipid, cityid, stateid, focusid) {    
    var zObj = $('#' + zipid);
    zObj.attr({ 'oldval': zObj.val(), 'cityid': cityid, 'stateid': stateid, 'focusid': focusid });
    zObj.blur(function () {
        zObj = $(this);
        var oldVal = zObj.attr('oldval');
        if (oldVal != zObj.val()) {
            zObj.attr('oldval', zObj.val());
            ajx('/zipcheck', { 'zip': zObj.val() }, function (rval) {
                if (rval.success == true) {
                    $('#' + zObj.attr('stateid')).val(rval.stateid);
                    if (rval.city != null) {
                        $('#' + zObj.attr('cityid')).val(rval.city);
                    }

                    var fObj = $('#' + zObj.attr('focusid'));
                    if (fObj != null && rval.city != null)
                        $(fObj).focus();
                }
            });
        }
    });
}

//function ajx(url, data, done)