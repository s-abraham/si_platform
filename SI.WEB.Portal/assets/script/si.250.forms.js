﻿
//plugin to set dirty flag usage $('form').dirtyFlagSet
(function ($) {
	$.fn.dirtyFlagSet = function () {

		var _self = this;
		$(':input:not([type=hidden],[type=file])', _self).each(function (i) {
			$(this).data('initial_value', $(this).val());
		});

		$(':input:not([type=hidden],[type=file])', _self).keyup(function () {
			if ($(this).val() != $(this).data('initial_value')) {
				if ($(this).hasClass("noDirtyFlag") === false)
					$(this).addClass("dirtyFlag");
			} else
				$(this).removeClass("dirtyFlag");
		});

		$(':input:not([type=hidden],[type=file])', _self).bind('change paste', function () {
			if ($(this).hasClass("noDirtyFlag") === false)
				$(this).addClass("dirtyFlag");
		});

		//try to use Icon for now, if dont like can go to this
		//setTimeout(function () {
		//	$('input[data-val-required]:not([type=hidden],[type=file],[type=checkbox])').each(function () {
		//		var par = $(this).parent();
		//		$('.k-dropdown-wrap', par).addClass('required-item');
		//		//if (par.hasClass('k-numeric-wrap') === false)
		//		par.addClass('required-item');
		//	});
		//}, 1000);


	};

	$.fn.dirtyFlagReset = function () {
		var _self = this;
		$('.dirtyFlag', _self).each(function () { $(this).removeClass("dirtyFlag"); });
	};

})(jQuery);

jQuery.extend({
	postJSON: function (url, data, callback) {
		return jQuery.ajax({
			type: "POST",
			url: url,
			data: JSON.stringify(data),
			success: callback,
			dataType: "json",
			contentType: "application/json; charset=utf-8",
			async: false,
			processData: false
			//error
		});
	}
});

(function ($) {
	$.fn.hasScrollBar = function () {
		//console.log(this.get(0).scrollHeight);
		//console.log(this.get(0).innerHeight);
		//console.log($(document).innerHeight()); //Does not work for Chrome, height and innerHeight are the same ??
		var headerOffset = 81;
		if ($("html").hasClass("k-webkit"))
			return false;
		
		return this.get(0) ? this.get(0).scrollHeight > ($(document).innerHeight() - headerOffset) : false;
		
	}
})(jQuery);

/*
 * Usage:
 *
 *  $("#mySelector").printThis({
 *      debug: false,               * show the iframe for debugging
 *      importCSS: true,            * import page CSS
 *      printContainer: true,       * grab outer container as well as the contents of the selector
 *      loadCSS: "path/to/my.css",  * path to additional css file
 *      pageTitle: "",              * add title to print page
 *      removeInline: false,        * remove all inline styles from print elements
 *      printDelay: 333,            * variable print delay
 *      header: null,               * prefix to html
 *      formValues: true            * preserve input/form values
 *  });
 *
 * Notes:
 *  - the loadCSS will load additional css (with or without @media print) into the iframe, adjusting layout
 */
;
(function ($) {
	var opt;
	$.fn.printThis = function (options) {
		opt = $.extend({}, $.fn.printThis.defaults, options);
		var $element = this instanceof jQuery ? this : $(this);

		var strFrameName = "printThis-" + (new Date()).getTime();

		if (window.location.hostname !== document.domain && navigator.userAgent.match(/msie/i)) {
			// Ugly IE hacks due to IE not inheriting document.domain from parent
			// checks if document.domain is set by comparing the host name against document.domain
			var iframeSrc = "javascript:document.write(\"<head><script>document.domain=\\\"" + document.domain + "\\\";</script></head><body></body>\")";
			var printI = document.createElement('iframe');
			printI.name = "printIframe";
			printI.id = strFrameName;
			printI.className = "MSIE";
			document.body.appendChild(printI);
			printI.src = iframeSrc;

		} else {
			// other browsers inherit document.domain, and IE works if document.domain is not explicitly set
			var $frame = $("<iframe id='" + strFrameName + "' name='printIframe' />");
			$frame.appendTo("body");
		}


		var $iframe = $("#" + strFrameName);

		// show frame if in debug mode
		if (!opt.debug) $iframe.css({
			position: "absolute",
			width: "0px",
			height: "0px",
			left: "-600px",
			top: "-600px"
		});


		// $iframe.ready() and $iframe.load were inconsistent between browsers    
		setTimeout(function () {
			
			var $doc = $iframe.contents();

			// import page stylesheets
			if (opt.importCSS) $("link[rel=stylesheet]").each(function () {
				var href = $(this).attr("href");
				if (href) {
					var media = $(this).attr("media") || "all";
					$doc.find("head").append("<link type='text/css' rel='stylesheet' href='" + href + "' media='" + media + "'>")
				}
			});

			//add title of the page
			if (opt.pageTitle) $doc.find("head").append("<title>" + opt.pageTitle + "</title>");

			// import additional stylesheet
			if (opt.loadCSS) $doc.find("head").append("<link type='text/css' rel='stylesheet' href='" + opt.loadCSS + "'>");

			// print header
			if (opt.header) $doc.find("body").append(opt.header);

			var $clone = $element.clone();

			var $links = $clone.find('a');
			if ($links.length) {
				$links.each(function () {
					$(this).removeAttr("href");
				});
			}

			$clone.find('script').remove();

			// grab $.selector as container
			if (opt.printContainer) $doc.find("body").append($clone.outer());
				// otherwise just print interior elements of container
			else $clone.each(function () {
				$doc.find("body").append($(this).html());
			});

			// capture form/field values
			if (opt.formValues) {
				// loop through inputs
				var $input = $clone.find('input');
				if ($input.length) {
					$input.each(function () {
						var $this = $(this),
                            $name = $(this).attr('name'),
                            $checker = $this.is(':checkbox') || $this.is(':radio'),
                            $iframeInput = $doc.find('input[name=' + $name + ']'),
                            $value = $this.val();

						//order matters here
						if (!$checker) {
							$iframeInput.val($value);
						} else if ($this.is(':checked')) {
							if ($this.is(':checkbox')) {
								$iframeInput.attr('checked', 'checked');
							} else if ($this.is(':radio')) {
								$doc.find('input[name=' + $name + '][value=' + $value + ']').attr('checked', 'checked');
							}
						}

					});
				}

				//loop through selects
				var $select = $clone.find('select');
				if ($select.length) {
					$select.each(function () {
						var $this = $(this),
                            $name = $(this).attr('name'),
                            $value = $this.val();
						$doc.find('select[name=' + $name + ']').val($value);
					});
				}

				//loop through textareas
				var $textarea = $clone.find('textarea');
				if ($textarea.length) {
					$textarea.each(function () {
						var $this = $(this),
                            $name = $(this).attr('name'),
                            $value = $this.val();
						$doc.find('textarea[name=' + $name + ']').val($value);
					});
				}
			} // end capture form/field values

			// remove inline styles
			if (opt.removeInline) {
				// $.removeAttr available jQuery 1.7+
				if ($.isFunction($.removeAttr)) {
					$doc.find("body *").removeAttr("style");
				} else {
					$doc.find("body *").attr("style", "");
				}
			}

			setTimeout(function () {
				if ($iframe.hasClass("MSIE")) {
					// check if the iframe was created with the ugly hack
					// and perform another ugly hack out of neccessity
					window.frames["printIframe"].focus();
					$doc.find("head").append("<script>  window.print(); </script>");
				} else {
					// proper method
					$iframe[0].contentWindow.focus();
					$iframe[0].contentWindow.print();
				}

				$clone.trigger("done");
				//remove iframe after print
				if (!opt.debug) {
					setTimeout(function () {
						$iframe.remove();
					}, 1000);
				}

			}, opt.printDelay);

		}, 333);

	};

	// defaults
	$.fn.printThis.defaults = {
		debug: false, // show the iframe for debugging
		importCSS: true, // import parent page css
		printContainer: true, // print outer container/$.selector
		loadCSS: "", // load an additional css file
		pageTitle: "", // add title to print page
		removeInline: false, // remove all inline styles
		printDelay: 333, // variable print delay
		header: null, // prefix to html
		formValues: true // preserve input/form values
	};

	// $.selector container
	jQuery.fn.outer = function () {
		return $($("<div></div>").html(this.clone())).html()
	}
})(jQuery);

var SIGlobal = {
	
	DateTimeNow: function () {
		var d = new Date();

		var month = d.getMonth() + 1;
		var day = d.getDate();
		var hour = d.getHours();
		var ampm = "AM";
		if (hour > 12) {
			hour -= 12;
			ampm = "PM";
		}
		var minute = d.getMinutes();
		var second = d.getSeconds();

		var output =
		(('' + month).length < 2 ? '0' : '') + month + '-' +
			(('' + day).length < 2 ? '0' : '') + day + '-' +
			d.getFullYear() + '  ' +
			hour + ':' +
			(('' + minute).length < 2 ? '0' : '') + minute + ':' +
			(('' + second).length < 2 ? '0' : '') + second + ' ' + ampm;

		return output;
	},
	
	showGridErrors: true,
	gridErrorHandler: function (e) {
		if (!SIGlobal.showGridErrors) return;
		if (e.errors) {
			var message = "Errors:\n";
			$.each(e.errors, function(key, value) {
				if ('errors' in value) {
					$.each(value.errors, function() {
						message += this + "\n";
					});
				}
			});
			SIGlobal.alertDialog("Data Error", message, "e");
		} else {
			SIGlobal.alertDialog("An Error has occured", e.errorThrown, "e");
		}
	},

	formSaveFailure: function () {
		SIGlobal.alertDialog("Save Failure", 'An error has occurred during the save.', "e");
	},

	dirtyFlagConfirm: function (fnCallback) {
		if ($('.dirtyFlag,.k-dirty').length) {
			SIGlobal.confirmDialog("Confirm Save", 'There are changes that need to be saved. Do you want to continue?', fnCallback);
		} else
			fnCallback("Yes");

	},
	
	alertDialog: function (title, message, level) {
		var iconName = "k-ext-information";
		switch (level) {
			case "q":
				iconName = "k-ext-question";
				break;
			case "w":
				iconName = "k-ext-warning";
				break;
			case "e":
				iconName = "k-ext-error";
				break;
		}

		$.when(kendo.ui.ExtAlertDialog.show({
			title: title,
			message: message,
			icon: iconName,
			height: "150px"
		}));
	},

	confirmDialog: function (title, message, fnCallback) {
		$.when(kendo.ui.ExtYesNoDialog.show({
			title: title,
			message: message,
			icon: "k-ext-question"
		})).done(function (response) {
			fnCallback(response.button);
		});
	},
	
	placeHolderSim: function(owner) {
		if (!("placeholder" in document.createElement("input"))) {
			$("input[placeholder], textarea[placeholder]").each(function () {
				var $me = $(this);
				var val = $me.attr("placeholder");
				if (this.value == "") {
					this.value = val;
					$me.css('color', '#a8a8a8');
				}

				$me.focus(function () {
					if (this.value == val) {
						this.value = "";
					}
					$(this).css('color', '');
				}).blur(function () {
					if ($.trim(this.value) == "") {
						this.value = val;
						$me.css('color', '#a8a8a8');
					}
				})
			});
			
			// Clear default placeholder values on form submit
			$('form').on("submit", function () {
				$(owner).find("input[placeholder], textarea[placeholder]").each(function () {
					if (this.value == $(this).attr("placeholder")) {
						this.value = "";
					}
				});
			});
		}
	},
	
	selectedPreferencesSection: "",
	showPreferencesWindow: function() {
		var window = $("#UserPreferencesWindow");
		
		if (!window.data("kendoWindow")) {
			window.kendoWindow({
				title: "My Preferences",
				content: "/dashboard/dashboard/MyPreferences",
				modal: true,
				iframe: false,
				draggable: true,
				resizable: false,
				visible: false,
				width: 780,
				minWidth: 780,
				height: 480,
				minHeight: 480,
				actions: ["Maximize", "Close"]//,
				//close: onEditClose
			})
		} else {
			UserPrefs.InitView();
		}
		
		var kendoWindow = window.data("kendoWindow");
		kendoWindow.center();
		kendoWindow.open();
	},
	
	closePreferencesWindow: function() {
		var kendoWindow = $("#UserPreferencesWindow").data("kendoWindow");
		kendoWindow.close();
	},
	
	selectedSupportCenterSection: "",
	showSupportCenterWindow: function (section) {
		var window = $("#SupportCenterWindow");
		this.selectedSupportCenterSection = section;
		
		if (!window.data("kendoWindow")) {
			window.kendoWindow({
				title: "Support Center",
				content: "/SupportCenter/SupportCenter/SupportCenter",
				modal: true,
				iframe: false,
				draggable: true,
				resizable: false,
				visible: false,
				width: 780,
				minWidth: 780,
				height: 480,
				minHeight: 480,
				actions: ["Maximize", "Close"]//,
				//close: onEditClose
			})
		} else {
			SupportCenter.InitView();
		}

		var kendoWindow = window.data("kendoWindow");
		kendoWindow.center();
		kendoWindow.open();

	},
	
	closeSupportCenterWindow: function () {
		var kendoWindow = $("#SupportCenterWindow").data("kendoWindow");
		kendoWindow.close();
	},
	
	facebookAccountSelectCallback: null,
	
	ValidationSummaryClear: function() {
		var list = $(".validation-summary-errors ul");
		list.empty();
		$(".validation-summary-errors").removeClass("validation-summary-errors").addClass("validation-summary-valid").hide();
	},
	
	ValidationSummaryPopulate: function(errors) {
		$(".validation-summary-valid").addClass("validation-summary-errors");
		var list = $(".validation-summary-errors ul");
		list.empty();

		$(errors).each(function () {
			list.append("<li style='float:left;width: 50%'>" + this + "</li>");
		});
		$(".validation-summary-errors").show();
	},

	userTZoffsetMinutes: null,
	
	DisplayTimeDelta: function(postDate) {

		var diff = postDate - Date.now().addMinutes(SIGlobal.userTZoffsetMinutes);
		var ago = diff < 0;
		diff = Math.abs(diff);

		var units = [
			1000 * 60 * 60 * 24 * 7,
			1000 * 60 * 60 * 24,
			1000 * 60 * 60,
			1000 * 60,
			1000
		];

		var rv = [];
		for (var i = 0; i < units.length; ++i) {
			rv.push(Math.floor(diff / units[i]));
			diff = diff % units[i];
		}

		var ts = "";
		if (rv[0] > 0) {
			ts = ts + " " + rv[0] + " Week";
			if (rv[0] > 1) ts = ts + "s";
		}
		if (rv[1] > 0) {
			if (ts != "") ts = ts + ",";
			ts = ts + " " + rv[1] + " Day";
			if (rv[1] > 1) ts = ts + "s";
		}
		if (rv[2] > 0) {
			if (ts != "") ts = ts + ",";
			ts = ts + " " + rv[2] + " Hour";
			if (rv[2] > 1) ts = ts + "s";
		}
		if (rv[3] > 0) {
			if (ts != "") ts = ts + ",";
			ts = ts + " " + rv[3] + " Min";
			if (rv[3] > 1) ts = ts + "s";
		}

		if (!ago) {
			ts = "(in " + ts + ")";
		} else {
			ts = "(" + ts + " ago)";
		}

		return ts;

	}
};
	