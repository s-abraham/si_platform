﻿
app.controller('SISystemStatsController', function ($scope, $http, $timeout) {

    $scope.refreshIntervalSeconds = 10;
    $scope.stats = {};
    $scope.refreshId = null;

    $scope.refresh = function () {
        $http({
            url: '/members/systemstatus',
            data: null,
            method: 'GET'
        }).success(function (data, status, headers, config) {
            $scope.stats = data.Status;
            $scope.refreshId = $timeout(function () {
                $scope.refresh();
            }, $scope.refreshIntervalSeconds * 1000);
        }).error(function (data, status, headers, config) {
            $scope.refreshId = $timeout(function () {
                $scope.refresh();
            }, $scope.refreshIntervalSeconds * 1000);
        });
    }


    $scope.refresh();

});