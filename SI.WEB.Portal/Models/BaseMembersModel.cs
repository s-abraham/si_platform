﻿using SI.BLL;
using SI.DTO;
using System;
using System.Collections.Generic;
using System.Web;
using System.Web.Security;
using SI.Extensions;


namespace SI.WEB.Portal.Models
{
    public class BaseMembersModel : BaseModel
    {
        public BaseMembersModel()
        {
            
            UserInfoDTO userInfo = userInfoFromAuthCookie(HttpContext.Current.User.Identity.Name);
			//UserInfoDTO userInfo = userInfoFromAuthCookie("42044");

            ValidateUserResultDTO vUser = ProviderFactory.Security.ValidateUser(userInfo);
            if (vUser.Outcome == ValidateUserOutcome.Success)
            {                    
                _userInfo = vUser.UserInfo;

	            if (vUser.UIOptions != null)
		            UIOptions = vUser.UIOptions;
	            else
		            UIOptions = new UserUIOptionsDTO();

				CurrentAccountCountryID = ProviderFactory.Security.GetAccountCountryByID(vUser.UserInfo.EffectiveUser.CurrentContextAccountID);
	            CurrentContextAccountName = ProviderFactory.Security.GetAccountNameByID(vUser.UserInfo.EffectiveUser.CurrentContextAccountID);

				CSSUrl = string.Format("{0}/style/{1}/{2}", BaseURL, _userInfo.EffectiveUser.Reseller.ThemeName, MvcApplication.AssetManager.GetThemeHash(_userInfo.EffectiveUser.Reseller.ThemeName));

                //CSSUrl = string.Format("{0}/style/{1}/{2}", BaseURL, "hermanadvertising", MvcApplication.AssetManager.GetThemeHash("hermanadvertising"));
            }
            else
            {
                FormsAuthentication.SignOut();
                HttpContext.Current.Response.Redirect(this.BaseURL, true);
                return;
            }

			//TODO Social Connection rights, note dont use UserInfo it is read only have to use _userInfo
            //UIOptions.CanManageSocialConnections = true;
	        
			//TODO Social Connections and status
	        SocialConnections = new List<SocialConnectionStatusDTO>();
	        SocialConnections.Add(new SocialConnectionStatusDTO { Source = SocialNetworkEnum.Facebook, Status = SocialConnectionStatusEnum.Connected });
			SocialConnections.Add(new SocialConnectionStatusDTO { Source = SocialNetworkEnum.Google, Status = SocialConnectionStatusEnum.PreviousConnectBroke });
			SocialConnections.Add(new SocialConnectionStatusDTO { Source = SocialNetworkEnum.Twitter, Status = SocialConnectionStatusEnum.NotConnected });
			SocialConnections.Add(new SocialConnectionStatusDTO { Source = SocialNetworkEnum.YouTube, Status = SocialConnectionStatusEnum.NotConnected });

            // find the members path for menu purposes

            string path = HttpContext.Current.Request.Url.OriginalString.ToLower();
            int mIndex = path.IndexOf("/members");
            if (mIndex >= 0)
            {
                path = path.Substring(mIndex + 8);
                if (path.Length > 1 && path.Substring(0, 1) == "/")
                {
                    path = path.Substring(1);
                }
                else if (path.Length == 1 && path.Substring(0, 1) == "/")
                {
                    path = "";
                }
                MembersPath = path;
            }
            else
            {
                MembersPath = "";
            }
        }

        //public AccountDTO SelectedAccount { get; set; }
		public long? CurrentAccountCountryID { get; set; }

        //public UserDTO User { get; set; }
        private UserInfoDTO _userInfo = null;
        public UserInfoDTO UserInfo
        {
            get
            {
                return _userInfo;
            }
        }

        public UserUIOptionsDTO UIOptions { get; set; }

        public string MembersPath { get; set; }

		public List<SocialConnectionStatusDTO> SocialConnections { get; set; }

		public string CurrentContextAccountName { get; set; }

        private static UserInfoDTO userInfoFromAuthCookie(string authCookieValue)
        {
            string[] parts = authCookieValue.Split(new char[] { '|' }, StringSplitOptions.RemoveEmptyEntries);
            long loggedInUserID = 0;


            long.TryParse(parts[0], out loggedInUserID);
            if (parts.Length > 1)
            {
                long impersonatingUserID = 0;
                long.TryParse(parts[1], out impersonatingUserID);

                return new UserInfoDTO(loggedInUserID, impersonatingUserID);
            }
            else
            {
                return new UserInfoDTO(loggedInUserID, null);
            }
        }
		
    }
}