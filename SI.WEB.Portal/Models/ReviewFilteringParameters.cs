﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SI.WEB.Portal.Models
{
	public class ReviewFilteringParameters
	{
		public string menuItems { get; set; }
		public string gridSelector { get; set; }
		public string datesFieldName { get; set; }
	}
}