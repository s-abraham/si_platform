﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SI.WEB.Portal.Models
{
    public class FormModel : BaseModel
    {
        /// <summary>
        /// The title of the form
        /// </summary>
        public string Title { get; set; }

        /// <summary>
        /// If true, a styled box will surround the form and also include a title area.
        /// This should be false for dialog based forms.
        /// </summary>
        public bool RenderContainer { get; set; }

        /// <summary>
        /// Determines whether to render an area along the bottom for buttons.
        /// </summary>
        public bool RenderButtonArea { get; set; }

        /// <summary>
        /// The client side DOM ID of the form
        /// </summary>
        public string DomID { get; set; }

        /// <summary>
        /// The fixed width of the form
        /// </summary>
        public int Width { get; set; }

        /// <summary>
        /// The form should be bound to a SlimValidator
        /// </summary>
        public bool HasValidation { get; set; }
               
        
    }
}