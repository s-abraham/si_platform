﻿using SI.BLL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SI.Extensions;

namespace SI.WEB.Portal.Models
{
    public class BaseModel
    {
        protected ILoggingProvider _log = ProviderFactory.Logging;

        private List<Crumb> _breadCrumbs = new List<Crumb>();

        public BaseModel()
        {
            IThemeProvider tp = ProviderFactory.Themes;
            string calledURL = HttpContext.Current.Request.Url.OriginalString;
            string themeName = tp.GetThemeName(calledURL);

            //BaseURL = Settings.GetSetting("site.baseurl");
            string[] parts = (from p in calledURL.Split(new char[] { '/' }, StringSplitOptions.RemoveEmptyEntries) select p).ToArray();
            BaseURL = parts[0] + "//" + parts[1];
            

            SiteName = Settings.GetSetting("site.name");
	        CDNUrl = Settings.GetSetting("site.cdnbaseurl", "http://cdn.socialintegration.com");
            CSSUrl = string.Format("{0}/style/{1}/{2}", BaseURL, themeName, MvcApplication.AssetManager.GetThemeHash(themeName));
            JSUrl = string.Format("{0}/script/{1}", BaseURL, MvcApplication.AssetManager.JSHash);
            VersionNumber = string.Format("Version #{0}", System.Reflection.Assembly.GetExecutingAssembly().GetName().Version);
            AssetImagePath = string.Format("{0}/assets/style/{1}/images/", BaseURL, themeName);
	        EnabledMixPanel = Settings.GetSetting("site.enableMixPanel").IsNullOptional("false").ToBool();
        }

        public string BaseURL { get; set; }
        public string SiteName { get; set; }
        public string CSSUrl { get; set; }
        public string JSUrl { get; set; }
		public string CDNUrl { get; set; }
        public string VersionNumber { get; set; }
        public string AssetImagePath { get; set; }
		public bool EnabledMixPanel { get; set; }
    }
}