﻿using JobLib;
using SI.BLL;
using SI.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Processor.PublishJobQueuer
{
    public class Processor : ProcessorBase<PublishJobQueuerData>
    {
        // Need to pass the args through for the default constructor
        public Processor(string[] args) : base(args) { }

        protected override void start()
        {
            JobDTO job = getNextJob();
            while (job != null)
            {
                try
                {                    
                    setJobStatus(job.ID.Value, JobStatusEnum.Started);
                    PublishJobQueuerData data = getJobData(job.ID.Value);

                    List<PostToQueueDTO> items = ProviderFactory.Social.GetPostsToQueue();
                    foreach (PostToQueueDTO item in items)
                    {
                        ProviderFactory.Social.QueuePost(item.PostID, item.AccountID);
                    }
                    
                    setJobStatus(job.ID.Value, JobStatusEnum.Completed);

                }
                catch (Exception ex)
                {
                    setJobStatus(job.ID.Value, JobStatusEnum.Failed);
                    ProviderFactory.Logging.LogException(ex);
                    ProviderFactory.Logging.Audit(AuditLevelEnum.Exception, AuditTypeEnum.ProcessorException, ex.Message, null, null, null, null, null, null);
                }
                job = getNextJob();
            }
        }
    }
}
