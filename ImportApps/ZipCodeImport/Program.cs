﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;

using System.Data.SqlClient;

using LumenWorks.Framework.IO;
using LumenWorks.Framework.IO.Csv;
using SI;


namespace ZipUpdater
{
    class Program
    {
        static void Main(string[] args)
        {
            if (args.Contains("download"))
            {
                DownloadZipDatabase();
            }
            if (args.Contains("import"))
            {
                ImportZipDatabase();
            }

        }

        static private void DownloadZipDatabase()
        {
            Console.WriteLine("Downloading zip database...");
            string zipPathname = string.Format("{0}zipcodes.csv", Path.GetTempPath());
            string zipDBUrl = Settings.GetSetting("zipcodedb.url");

            if (File.Exists(zipPathname))
                File.Delete(zipPathname);

            using (WebClient wc = new WebClient())
            {
                wc.DownloadFile(zipDBUrl, zipPathname);
            }
        }

        static private void ImportZipDatabase()
        {

            Console.WriteLine("Importing zip database...");                        
            string zipPathname = string.Format("{0}zipcodes.csv", Path.GetTempPath());

            System.Globalization.CultureInfo cultureInfo = System.Threading.Thread.CurrentThread.CurrentCulture;
            System.Globalization.TextInfo TextInfo = cultureInfo.TextInfo;

            using (CsvReader csv = new CsvReader(new StreamReader(zipPathname), true))
            {
                int fieldCount = csv.FieldCount;
                string[] headers = csv.GetFieldHeaders();
                List<string> lcHeaders = new List<string>();
                foreach (string header in headers)
                {
                    lcHeaders.Add(header.Trim().ToLower());
                }

                //exec spUpdZip 48381,'Milford', 'MI', 'Oakland', 3.3, 4.6

                int idxZipcode = lcHeaders.IndexOf("zipcode");
                int idxCity = lcHeaders.IndexOf("city");
                int idxState = lcHeaders.IndexOf("state");
                int idxCounty = lcHeaders.IndexOf("county");
                int idxLat = lcHeaders.IndexOf("lat");
                int idxLon = lcHeaders.IndexOf("long");
                int idxPreferred = lcHeaders.IndexOf("preferred");

                int idxPop = lcHeaders.IndexOf("estimatedpopulation");
                int idxTaxFiled = lcHeaders.IndexOf("taxreturnsfiled");
                int idxWages = lcHeaders.IndexOf("totalwages");

                StringBuilder sb = new StringBuilder();
                int addCount = 0;
                int totalCount = 0;
                while (csv.ReadNextRecord())
                {
                    //string preferred = csv[idxPreferred].Trim().ToLower();

                    if (1 == 1)
                    {
                        try
                        {
                            int zip = Convert.ToInt32(csv[idxZipcode]);
                            string city = TextInfo.ToTitleCase(csv[idxCity].Trim().ToLower());
                            string state = csv[idxState].Trim().ToUpper();
                            double lat = Convert.ToDouble(csv[idxLat]);
                            double lon = Convert.ToDouble(csv[idxLon]);

                            int? population = Convert.ToInt32(csv[idxPop]);
                            int? taxFiled = Convert.ToInt32(csv[idxTaxFiled]);
                            int? wages = Convert.ToInt32(csv[idxWages]);


                            sb.AppendFormat(
                                "exec spUpdateZip {0},'{1}','{2}','{3}',{4}, {5}, {6}, {7} \r\n",
                                zip, SafeSQL(city), SafeSQL(state), lat, lon,
                                population.HasValue?population.Value.ToString(CultureInfo.InvariantCulture):"null",
                                taxFiled.HasValue ? taxFiled.Value.ToString(CultureInfo.InvariantCulture) : "null",
                                wages.HasValue ? wages.Value.ToString(CultureInfo.InvariantCulture) : "null"
                            );

                            addCount++;
                            totalCount++;
                        }
                        catch (Exception)
                        {
                            //no operation
                        }

                        if (addCount == 1000)
                        {
                            exec(sb.ToString());
                            sb.Clear();
                            addCount = 0;

                            Console.WriteLine(string.Format("Imported {0} Zipcodes", totalCount));
                        }
                    }
                }

                if (sb.Length > 0)
                {
                    exec(sb.ToString());
                    Console.WriteLine(string.Format("Imported {0} Zipcodes", totalCount));
                }
            }
        }


        static private void exec(string sql)
        {
            string conString = Settings.GetSetting("connectionstring.main.direct");
            using (SqlConnection con = new SqlConnection(conString))
            {
                con.Open();

                using (SqlCommand cmd = new SqlCommand(sql, con))
                {
                    cmd.ExecuteNonQuery();
                }

                con.Close();
            }
        }

        static private string SafeSQL(string s)
        {
            s = s.Replace("'", "''");
            return s;
        }
    }
}
