﻿using LumenWorks.Framework.IO.Csv;
using SI;
using SI.BLL;
using SI.DTO;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace EdmundsImport
{
    /// <summary>
    /// Imports the edmunds dealer file...
    /// </summary>
    class Program
    {

        private static Random r = new Random();

        private class EdmundsItem
        {

            public string DealershipName { get; set; }
            public long CDDFranchiseID { get; set; }
            public long CDDDealershipID { get; set; }

            public string StreetAddress { get; set; }
            public string City { get; set; }
            public string StateAbbreviation { get; set; }

            public long ZipCode { get; set; }

            public string Make { get; set; }
        }

        static void Main(string[] args)
        {
            //Console.WriteLine("Starting Database Wipe and Edmunds Import in 5 seconds...");
            //Thread.Sleep(5000);

            //long sysUserID = ProviderFactory.Security.GetSystemUserID();

            //RoleTypeDTO rtAdmin = ProviderFactory.Security.GetRoleTypes(sysUserID).Find(r => r.Name.Equals("Admin"));
            //RoleTypeDTO rtReadOnly = ProviderFactory.Security.GetRoleTypes(sysUserID).Find(r => r.Name.Equals("ReadOnly"));
            //RoleTypeDTO rtEngage = ProviderFactory.Security.GetRoleTypes(sysUserID).Find(r => r.Name.Equals("Engager"));


            //string importFile = args[0].Trim();
            //Console.WriteLine("Starting with import file: {0}", importFile);

            //try
            //{

            //    ProviderFactory.Security.ResetDatabase();

            //    //create the master account
            //    AccountDTO aMaster = new AccountDTO()
            //    {
            //        ParentAccountID = null,
            //        AccountTypeID = 3,
            //        CityID = 33472,
            //        StateID = 15,
            //        CountryID = 1,
            //        ZipCode = 60181,
            //        CRMSystemTypeID = null,
            //        DateCreated = SIDateTime.Now,
            //        DateModified = SIDateTime.Now,
            //        Name = "AutoStartUps",
            //        DisplayName = null,
            //        EmailAddress = null,
            //        LeadsEmailAddress = null,
            //        StreetAddress1 = "One Tower Lane",
            //        StreetAddress2 = "Suite 3100",
            //        WebsiteURL = "http://www.autostartups.com",
            //        PhoneNumber = "6305065546"
            //    };
            //    aMaster = ProviderFactory.Security.Save(new SaveEntityDTO<AccountDTO>(new UserInfoDTO(0, null)) { Entity = aMaster }).Entity;

            //    // create a master user
            //    UserDTO mUser = new UserDTO()
            //    {
            //        SuperAdmin = true,
            //        Username = "muser",
            //        Password = "Admin!@#$",
            //        RootAccountID = aMaster.ID.Value,
            //        FirstName = "Master",
            //        LastName = "User",
            //        EmailAddress = "reynolds@autostartups.com"
            //    };
            //    mUser = ProviderFactory.Security.SaveUser(mUser).User;

            //    //create a view only user
            //    UserDTO vUser = new UserDTO()
            //    {
            //        SuperAdmin = false,
            //        Username = "vuser",
            //        Password = "Admin!@#$",
            //        RootAccountID = aMaster.ID.Value,
            //        FirstName = "View",
            //        LastName = "User",
            //        EmailAddress = "reynolds@autostartups.com"
            //    };
            //    vUser = ProviderFactory.Security.SaveUser(mUser).User;



            //    // create SI Account
            //    AccountDTO aSI = new AccountDTO()
            //    {
            //        ParentAccountID = aMaster.ID,
            //        AccountTypeID = 3,
            //        CityID = 33472,
            //        StateID = 15,
            //        CountryID = 1,
            //        ZipCode = 60181,
            //        CRMSystemTypeID = null,
            //        DateCreated = SIDateTime.Now,
            //        DateModified = SIDateTime.Now,
            //        Name = "Social Integration",
            //        DisplayName = null,
            //        EmailAddress = null,
            //        LeadsEmailAddress = null,
            //        StreetAddress1 = "One Tower Lane",
            //        StreetAddress2 = "Suite 3100",
            //        WebsiteURL = "http://www.socialintegration.com",
            //        PhoneNumber = "6305065546"
            //    };
            //    aSI = ProviderFactory.Security.Save(new SaveEntityDTO<AccountDTO>(new UserInfoDTO(0, null)) { Entity = aSI }).Entity;

            //    // create SD Account
            //    AccountDTO aSD = new AccountDTO()
            //    {
            //        ParentAccountID = aMaster.ID,
            //        AccountTypeID = 3,
            //        CityID = 33472,
            //        StateID = 15,
            //        CountryID = 1,
            //        ZipCode = 60181,
            //        CRMSystemTypeID = null,
            //        DateCreated = SIDateTime.Now,
            //        DateModified = SIDateTime.Now,
            //        Name = "Social Dealer",
            //        DisplayName = null,
            //        EmailAddress = null,
            //        LeadsEmailAddress = null,
            //        StreetAddress1 = "One Tower Lane",
            //        StreetAddress2 = "Suite 3100",
            //        WebsiteURL = "http://www.socialdealer.com",
            //        PhoneNumber = "6305065546"
            //    };

            //    aSD = ProviderFactory.Security.Save(new SaveEntityDTO<AccountDTO>(new UserInfoDTO(0, null)) { Entity = aSD }).Entity;

            //    System.Globalization.CultureInfo cultureInfo = System.Threading.Thread.CurrentThread.CurrentCulture;
            //    System.Globalization.TextInfo TextInfo = cultureInfo.TextInfo;

            //    List<EdmundsItem> elist = new List<EdmundsItem>();

            //    using (CsvReader csv = new CsvReader(new StreamReader(importFile), true))
            //    {
            //        int fieldCount = csv.FieldCount;
            //        string[] headers = csv.GetFieldHeaders();
            //        List<string> lcHeaders = new List<string>();
            //        foreach (string header in headers)
            //        {
            //            lcHeaders.Add(header.Trim().ToLower());
            //        }

            //        //exec spUpdZip 48381,'Milford', 'MI', 'Oakland', 3.3, 4.6

            //        int idxDealerName = lcHeaders.IndexOf("dealership name");
            //        int idxFranchiseID = lcHeaders.IndexOf("cdd franchise id");
            //        int idxDealerID = lcHeaders.IndexOf("cdd dealership id");
            //        int idxStreet1 = lcHeaders.IndexOf("physical street");
            //        int idxCity = lcHeaders.IndexOf("physical city");
            //        int idxState = lcHeaders.IndexOf("physical state/province");
            //        int idxZipcode = lcHeaders.IndexOf("physical zip/postal code");
            //        int idxMake = lcHeaders.IndexOf("make");

            //        while (csv.ReadNextRecord())
            //        {
            //            try
            //            {
            //                EdmundsItem item = new EdmundsItem()
            //                {
            //                    ZipCode = Convert.ToInt64(csv[idxZipcode]),
            //                    City = TextInfo.ToTitleCase(csv[idxCity].Trim().ToLower()),
            //                    StateAbbreviation = csv[idxState].Trim().ToUpper(),
            //                    CDDDealershipID = Convert.ToInt64(csv[idxDealerID]),
            //                    CDDFranchiseID = Convert.ToInt64(csv[idxFranchiseID]),
            //                    StreetAddress = TextInfo.ToTitleCase(csv[idxStreet1].Trim().ToLower()),
            //                    Make = csv[idxMake].Trim(),
            //                    DealershipName = TextInfo.ToTitleCase(csv[idxDealerName].Trim())
            //                };
            //                elist.Add(item);
            //            }
            //            catch (Exception ex)
            //            {
            //                ex = ex;
            //            }


            //        }


            //        int processed = 0;
            //        int counter = 0;

            //        List<StateDTO> states = ProviderFactory.Lists.GetStatesForCountry(1);
            //        List<FranchiseTypeDTO> franchisetypes = ProviderFactory.Lists.GetFranchiseTypes();

            //        List<string> makes = (from e in elist select e.Make).Distinct().ToList();
            //        foreach (string mmake in makes)
            //        {
            //            if ((from ft in franchisetypes where ft.Name.Trim().ToLower() == mmake.Trim().ToLower() select ft).Count() == 0)
            //            {
            //                if (mmake.Trim().ToLower() != "used")
            //                {
            //                    ProviderFactory.Lists.AddFranchiseType(mmake);
            //                    franchisetypes = ProviderFactory.Lists.GetFranchiseTypes();
            //                }
            //            }
            //        }

            //        List<long> itemList = (from e in elist select e.CDDDealershipID).Distinct().ToList();

            //        Console.WriteLine("{0} Dealers to Process", itemList.Count());

            //        //foreach (EdmundsItem item in elist)
            //        foreach (long dealerID in itemList)
            //        {
            //            List<EdmundsItem> dealerList = (from e in elist where e.CDDDealershipID.Equals(dealerID) select e).ToList();

            //            AccountDTO parentAccount = null;

            //            List<long> franchiseTypes = new List<long>();

            //            EdmundsItem eiParent = null;
            //            eiParent = (from d in dealerList select d).FirstOrDefault();
            //            if (eiParent != null)
            //            {
            //                foreach (EdmundsItem mitem in dealerList)
            //                {
            //                    if (!string.IsNullOrWhiteSpace(mitem.Make) && mitem.Make.Trim().ToLower() != "used")
            //                    {
            //                        long? ft = (from f in franchisetypes where f.Name.Trim().ToLower() == mitem.Make.Trim().ToLower() select f.ID).FirstOrDefault();
            //                        if (ft.HasValue)
            //                        {
            //                            franchiseTypes.Add(ft.Value);
            //                        }
            //                    }
            //                }

            //                StateDTO state2 = (from s in states where s.Abbreviation.Trim().ToLower() == eiParent.StateAbbreviation.Trim().ToLower() select s).FirstOrDefault();

            //                string theName = eiParent.DealershipName.Trim();

            //                //foreach (EdmundsItem mitem in dealerList)
            //                //{
            //                //    theName = theName.Replace(mitem.Make, "");
            //                //}
            //                //theName = theName.Replace("  ", " ");
            //                //theName = theName.Replace("  ", " ");
            //                //theName = theName.Replace("  ", " ");
            //                //theName = theName.Replace("  ", " ").Trim();                            

            //                if (state2 != null)
            //                {
            //                    parentAccount = new AccountDTO()
            //                    {
            //                        ParentAccountID = aSD.ID,
            //                        AccountTypeID = 1, //location
            //                        CityName = eiParent.City,
            //                        StateID = state2.ID,
            //                        CountryID = 1,
            //                        ZipCode = eiParent.ZipCode,
            //                        CRMSystemTypeID = null,
            //                        DateCreated = SIDateTime.Now,
            //                        DateModified = SIDateTime.Now,
            //                        Name = string.Format("{0} {1}", theName, dealerID),
            //                        DisplayName = theName,
            //                        EmailAddress = null,
            //                        LeadsEmailAddress = null,
            //                        StreetAddress1 = eiParent.StreetAddress,
            //                        StreetAddress2 = null,
            //                        WebsiteURL = null,
            //                        PhoneNumber = null,
            //                        FranchiseTypes = franchiseTypes.ToArray(),
            //                        EdmundsDealershipID = dealerID
            //                    };

            //                    SaveEntityDTO<AccountDTO> saveEnt = ProviderFactory.Security.Save(new SaveEntityDTO<AccountDTO>(new UserInfoDTO(0, null)) { Entity = parentAccount });
            //                    if (saveEnt.Problems.Count() > 0)
            //                    {
            //                        saveEnt = saveEnt;
            //                    }
            //                    parentAccount = saveEnt.Entity;


            //                    processed++;
            //                    counter++;
            //                    if (counter == 100)
            //                    {
            //                        counter = 0;
            //                        Console.WriteLine("Processed {0} dealers...", processed);
            //                    }
            //                }
            //            }


            //            // create an admin user                       
            //            if (parentAccount != null)
            //            {
            //                //UserDTO adminUser1 = new UserDTO()
            //                //{
            //                //    SuperAdmin = false,
            //                //    Username = string.Format("{0}admin",parentAccount.ID),
            //                //    Password = "Admin!@#$",
            //                //    RootAccountID = parentAccount.ID.Value,
            //                //    FirstName = "Group",
            //                //    LastName = "Admin",
            //                //    EmailAddress = string.Format("{0}admin@autostartups.com", parentAccount.ID)
            //                //};
            //                //adminUser1 = ProviderFactory.Security.SaveUser(adminUser1).User;

            //                //ProviderFactory.Security.AddRole(adminUser1.ID.Value, parentAccount.ID.Value, rtAdmin.ID.Value, true);
            //            }

            //            //foreach (EdmundsItem item2 in dealerList)
            //            //{
            //            //    List<long> franchiseTypes = new List<long>();

            //            //    string make = item2.Make;
            //            //    if (make.Trim().ToLower() != "used")
            //            //    {
            //            //        long? ft = (from f in franchisetypes where f.Name.Trim().ToLower() == make.Trim().ToLower() select f.ID).FirstOrDefault();
            //            //        if (ft.HasValue)
            //            //        {
            //            //            franchiseTypes.Add(ft.Value);
            //            //        }
            //            //    }

            //            //    StateDTO state = (from s in states where s.Abbreviation.Trim().ToLower() == item2.StateAbbreviation.Trim().ToLower() select s).FirstOrDefault();

            //            //    if (state != null)
            //            //    {
            //            //        AccountDTO account = new AccountDTO()
            //            //        {
            //            //            ParentAccountID = (parentAccount == null) ? aSD.ID.Value : parentAccount.ID.Value,
            //            //            AccountTypeID = 1,
            //            //            CityName = item2.City,
            //            //            StateID = state.ID,
            //            //            CountryID = 1,
            //            //            ZipCode = item2.ZipCode,
            //            //            CRMSystemTypeID = null,
            //            //            DateCreated = DateTime.Now,
            //            //            DateModified = DateTime.Now,
            //            //            Name = item2.DealershipName + ((parentAccount == null) ?  r.Next(100000,999999).ToString()  : " (" + item2.Make + ")"),
            //            //            DisplayName = null,
            //            //            EmailAddress = null,
            //            //            LeadsEmailAddress = null,
            //            //            StreetAddress1 = item2.StreetAddress,
            //            //            StreetAddress2 = null,
            //            //            WebsiteURL = null,
            //            //            PhoneNumber = null,
            //            //            FranchiseTypeIDs = franchiseTypes
            //            //        };

            //            //        account = ProviderFactory.Security.SaveAccount(mUser.ID.Value, account).Entity;

            //            //        UserDTO adminUser2 = new UserDTO()
            //            //        {
            //            //            SuperAdmin = false,
            //            //            Username = string.Format("{0}admin", account.ID),
            //            //            Password = "Admin!@#$",
            //            //            RootAccountID = account.ID.Value,
            //            //            FirstName = "Account",
            //            //            LastName = "Admin",
            //            //            EmailAddress = string.Format("{0}admin@autostartups.com", account.ID)
            //            //        };
            //            //        adminUser2 = ProviderFactory.Security.SaveUser(adminUser2).User;
            //            //        ProviderFactory.Security.AddRole(adminUser2.ID.Value, account.ID.Value, rtAdmin.ID.Value, false);

            //            //        UserDTO readonlyUser1 = new UserDTO()
            //            //        {
            //            //            SuperAdmin = false,
            //            //            Username = string.Format("{0}readonly", account.ID),
            //            //            Password = "Admin!@#$",
            //            //            RootAccountID = account.ID.Value,
            //            //            FirstName = "Account",
            //            //            LastName = "Admin",
            //            //            EmailAddress = string.Format("{0}readonly@autostartups.com", account.ID)
            //            //        };
            //            //        readonlyUser1 = ProviderFactory.Security.SaveUser(readonlyUser1).User;
            //            //        ProviderFactory.Security.AddRole(readonlyUser1.ID.Value, account.ID.Value, rtReadOnly.ID.Value, false);
            //            //    }

            //            //    counter++;
            //            //    processed++;

            //            //    if (counter == 100)
            //            //    {
            //            //        counter = 0;
            //            //        Console.WriteLine("Processed {0} dealers...", processed);
            //            //    }
            //            //}

            //        }

            //    }

            //}
            //catch (Exception ex)
            //{
            //    throw new Exception(ex.ToString(), ex);
            //}
        }
    }
}
