﻿using ICSharpCode.SharpZipLib.Zip;
using SI.BLL;
using SI.DTO;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ProcessorBundleManager
{
    public partial class frmMain : Form
    {
        public frmMain()
        {
            InitializeComponent();
            init();
        }

        private void init()
        {
            loadJobTypes();
            lvFiles.DragEnter += lvFiles_DragEnter;
            lvFiles.DragDrop += lvFiles_DragDrop;
        }

        void lvFiles_DragDrop(object sender, DragEventArgs e)
        {
            Array a = (Array)e.Data.GetData(DataFormats.FileDrop);

            if (a != null)
            {
                lvFiles.Items.Clear();

                for (int i = 0; i < a.Length; i++)
                {
                    string s = a.GetValue(i).ToString();

                    string filename = Path.GetFileName(s);
                    FileInfo finfo = new FileInfo(s);

                    ListViewItem lvi = new ListViewItem();
                    lvi.Text = filename;
                    lvi.Tag = s;
                    lvi.SubItems.Add(finfo.Length.ToString());
                    lvFiles.Items.Add(lvi);
                }


                // Call OpenFile asynchronously.
                // Explorer instance from which file is dropped is not responding
                // all the time when DragDrop handler is active, so we need to return
                // immidiately (especially if OpenFile shows MessageBox).

                //this.BeginInvoke(m_DelegateOpenFile, new Object[] { s });

                this.Activate();
            }
        }

        void lvFiles_DragEnter(object sender, DragEventArgs e)
        {
            if (e.Data.GetDataPresent(DataFormats.FileDrop))
                e.Effect = DragDropEffects.Copy;
            else
                e.Effect = DragDropEffects.None;
        }

        private void loadJobTypes()
        {
            List<JobTypeDTO> jtypes = ProviderFactory.Jobs.GetJobTypes();
            lv.Items.Clear();

            foreach (JobTypeDTO item in jtypes)
            {
                ListViewItem lvi = new ListViewItem();
                lvi.Text = item.ID.ToString();
                lvi.Tag = item.ID;
                lvi.SubItems.Add(item.Name);
                lvi.SubItems.Add(item.BundleHash);
                lv.Items.Add(lvi);
            }

            updateView();
        }

        private void lv_SelectedIndexChanged(object sender, EventArgs e)
        {
            updateView();
        }

        private void updateView()
        {
            if (lv.SelectedItems.Count > 0)
            {
                ListViewItem itm = lv.SelectedItems[0];
                lblName.Text = itm.SubItems[1].Text;
                lblName.Visible = true;
                lblFileList.Visible = true;
                lvFiles.Visible = true;
                cmdSave.Visible = true;
                lvFiles.Items.Clear();
            }
            else
            {
                lblFileList.Visible = false;
                lblName.Visible = false;
                lvFiles.Visible = false;
                cmdSave.Visible = false;
            }
        }

        private void cmdSave_Click(object sender, EventArgs e)
        {
            if (lv.SelectedItems.Count == 0)
            {
                MessageBox.Show("You must first select a job type.", "No Job Type Selected", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return;
            }
            if (lvFiles.Items.Count == 0)
            {
                MessageBox.Show("There are no files selected.  Drag files into the list to proceed.", "No Files Selected", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return;
            }

            long jobTypeID = (long)lv.SelectedItems[0].Tag;

            using (MemoryStream fStream = new MemoryStream())
            {
                using (ZipOutputStream zoStream = new ZipOutputStream(fStream))
                {
                    foreach (ListViewItem item in lvFiles.Items)
                    {
                        string pathName = (string)item.Tag;
                        string filename = Path.GetFileName(pathName);

                        ZipEntry entry = new ZipEntry(filename);
                        zoStream.PutNextEntry(entry);

                        using (FileStream fs = File.OpenRead(pathName))
                        {
                            byte[] buffer = new byte[1024];
                            int bytesRead;
                            do
                            {
                                bytesRead = fs.Read(buffer, 0, buffer.Length);
                                zoStream.Write(buffer, 0, bytesRead);
                            } while (bytesRead > 0);
                        }

                    }

                    zoStream.Finish();
                    zoStream.Close();
                }

                byte[] mybundle = fStream.ToArray();

                //long userID = ProviderFactory.Security.GetUserByUsername("muser").ID.Value;
                ProviderFactory.Jobs.StoreJobBundle(0, jobTypeID, mybundle);

                MessageBox.Show("The bundle has been saved!");


            }

        }

    }
}

















