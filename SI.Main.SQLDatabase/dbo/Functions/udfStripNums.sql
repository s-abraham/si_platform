﻿CREATE FUNCTION [dbo].[udfStripNums] (@mixedString AS varchar(max))
RETURNS varchar(max)
as
BEGIN 
DECLARE @resultString varchar(max), @tmpString char(1), @i int, @strLen int 
SELECT @resultString='', @i=0, @strLen=len(@mixedString)

WHILE @strLen>=@i
BEGIN 
SELECT @tmpString=substring (@mixedString,@i/*character index*/,1/*one character*/)
if isnumeric(@tmpString)=0	
SELECT @resultString=@resultString+@tmpString	
SELECT @i=@i+1
END 
RETURN @resultString
END