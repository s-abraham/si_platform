﻿CREATE FUNCTION [dbo].[fnList2StringTable]
(
	@List NVARCHAR(MAX),
	@Delim CHAR
)
RETURNS
@ParsedList TABLE
(
	value nvarchar(100) not null
)
AS
BEGIN

	DECLARE @item VARCHAR(MAX), @Pos INT
	SET @List = LTRIM(RTRIM(@List))+ @Delim
	SET @Pos = CHARINDEX(@Delim, @List, 1)
	WHILE @Pos > 0
	BEGIN
		SET @item = LTRIM(RTRIM(LEFT(@List, @Pos - 1)))
		IF @item <> ''
		BEGIN
			INSERT INTO @ParsedList (value)
			VALUES (@item)
		END
		SET @List = RIGHT(@List, LEN(@List) - @Pos)
		SET @Pos = CHARINDEX(@Delim, @List, 1)
	END
	RETURN

END