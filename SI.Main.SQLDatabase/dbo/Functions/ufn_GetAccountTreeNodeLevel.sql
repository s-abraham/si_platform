﻿CREATE FUNCTION [dbo].[ufn_GetAccountTreeNodeLevel] ( @pCurrentNode    INT )
RETURNS INT
AS
BEGIN

    DECLARE @vParentID            INT

    IF @pCurrentNode = 0 OR @pCurrentNode IS NULL
        RETURN 0

    SELECT @vParentID = [ParentAccountID]
    FROM [dbo].[Accounts]
    WHERE [ID] = @pCurrentNode

    RETURN [dbo].[ufn_GetAccountTreeNodeLevel] ( @vParentID ) + 1

END