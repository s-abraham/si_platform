﻿CREATE procedure [dbo].[spUpsertReview]
	@ID				bigint		=null,
	@AccountID		bigint,
	@ReviewSourceID	bigint,
	@Rating			float,
	@ReviewerName	varchar(50),
	@ReviewDate		datetime,
	@ReviewText		varchar(max),
	@ReviewURL		varchar(200),
	@ExtraInfo		varchar(max),
	@Filtered		bit
as

set nocount on

if @ID=0 set @ID=null

if (@ID is null) begin

	insert Reviews (	AccountID, ReviewSourceID, Rating, ReviewerName, ReviewDate, ReviewText,
						ReviewURL, ExtraInfo, Filtered, DateUpdated, DateEntered )
	values ( @AccountID, @ReviewSourceID, @Rating, @ReviewerName, @ReviewDate, @ReviewText,
						@ReviewURL, @ExtraInfo, @Filtered, getdate(), getdate() )

	select scope_identity() ID

end else begin

	update	Reviews
	set		Rating=@Rating, ReviewText=@ReviewText, ReviewURL=@ReviewURL, ExtraInfo=@ExtraInfo,
			Filtered=@Filtered, DateUpdated=getdate()
	where	ID=@ID

	select @ID ID, @AccountID AccountID
end