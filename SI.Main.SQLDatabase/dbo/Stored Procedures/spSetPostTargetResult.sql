﻿CREATE procedure spSetPostTargetResult
	@postTargetID bigint,
	@resultID varchar(250),
	@feedID varchar(250)
as

set nocount on

update  PostTargets
set ResultID=@resultID, FeedID=@feedID, PublishedDate=getdate()
where	ID=@postTargetID