﻿
/*

Returns a list of Sub_Accounts by Geographical Location Distance. (to be modifyed later)

exec spGetAccountsByDistance2 @AccountID=197930                            --@ZipCode='83221'
*/

CREATE procedure [dbo].[spGetAccountsByDistance]
@AccountID bigint = 0 
as

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED

DECLARE @Zip varchar (50)
SET @Zip =
(
    SELECT  zip.ZipCode FROM Accounts AS a LEFT OUTER JOIN
                         Addresses AS ba ON a.BillingAddressID = ba.ID LEFT OUTER JOIN
                         Cities AS cit ON ba.CityID = cit.ID LEFT OUTER JOIN
                         ZipCodes AS zip ON cit.ZipCodeID = zip.ID
			WHERE (a.ID = @AccountID)
)

SELECT DISTINCT 
                         a.ID AS AccountID, a.DisplayName, cit.Name AS CityName, zip.ZipCode AS ZipCode, sta.Name AS StateName, 
                         sta.Abbreviation AS StateAbbreviation, dbo.ufn_GetAccountTreeNodeLevel(a.ID) AS [Level]
FROM            Accounts AS a LEFT OUTER JOIN
                         Addresses AS ba ON a.BillingAddressID = ba.ID LEFT OUTER JOIN
                         Cities AS cit ON ba.CityID = cit.ID LEFT OUTER JOIN
                         States AS sta ON cit.StateID = sta.ID LEFT OUTER JOIN
                         ZipCodes AS zip ON cit.ZipCodeID = zip.ID
WHERE        
(a.AccountTypeID = 1) 
AND (dbo.ufn_GetAccountTreeNodeLevel(a.ID) > 1) 
AND (LEFT(zip.ZipCode, 3) = LEFT(@Zip, 3)) -- TO CHANGE
AND a.ID NOT IN (SELECT        ac.CompetitorAccountID 
					FROM            Accounts_Competitors AS ac 
					WHERE       ac.AccountID=@AccountID
					 )