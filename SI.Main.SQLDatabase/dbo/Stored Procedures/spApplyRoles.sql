﻿
/*

applies permissions based upon roles, optionally for only a given user

*/

CREATE procedure [dbo].[spApplyRoles]
	@UserID bigint = null
as

select	u.id
into	#users
from	Users u
where	u.ID = coalesce(@UserID,u.ID)

delete	p
from	Permissions p
where exists (
	select		*
	from		Accounts_Users au
	inner join	#users u on au.UserID=u.ID
	where au.ID = p.Account_UserID
)

insert	Permissions(Account_UserID, PermissionTypeID, IncludeChildren)
select		distinct r.Account_UserID, rtpt.PermissionTypeID, r.IncludeChildren
from		Roles r
inner join	RoleTypes_PermissionTypes rtpt on r.RoleTypeID = rtpt.RoleTypeID
where exists (
	select	*
	from		Accounts_Users au
	inner join	#users u on au.UserID = u.ID
	where		au.ID = r.Account_UserID	
)

drop table #users