﻿CREATE procedure [dbo].[spUpsertFacebookCommentLike]

	--identity
	@FromID	varchar(100),
	@FromName varchar(250),
	@PictureURL varchar(250),
	
	-- to link up with the existing comment
	@FBCommentID varchar(250)

as

set nocount on

declare @FacebookIdentityID bigint
exec spUpsertFacebookIdentity @FromID=@FromID, @FromName=@FromName, @PictureURL=@PictureURL, @FacebookIdentityID=@FacebookIdentityID OUTPUT

declare @FacebookCommentID bigint
select	@FacebookCommentID = ID
from	FacebookComments with(nolock)
where	FBCommentID=@FBCommentID

if (@FacebookCommentID>0) begin
	declare @FacebookLikeID bigint
	select	@FacebookLikeID = ID
	from	FacebookLikes  with(nolock)
	where	FacebookIdentitiesID = @FacebookIdentityID
	and		FacebookCommentsID = @FacebookCommentID

	if (coalesce(@FacebookLikeID,0)=0) begin
		insert FacebookLikes (FacebookPostsID, FacebookCommentsID, FacebookIdentitiesID, DateCreated)
		values (null, @FacebookCommentID, @FacebookIdentityID, getdate())
	end
end