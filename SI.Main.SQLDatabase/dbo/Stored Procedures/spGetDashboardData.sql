﻿--select * from accounts where name like ('%hendrick%')
--spGetDashboardData @AccountIDs='198126|206924|206925|220169'
--spGetDashboardData @AccountIDs='220633|220635|220636|220637|220638|220639|220640|220641|220642|220643|220644|220645|220646|220647|220648|220649|220650|220651|220652|220653|220654|220655|220656|220657|220658|220659|220660|220661|220662|220663|220664|220665|220666|220667|220668|220669|220670|220671|220672|220673|220674|220675|220676|220677|220678|220679|220680|220681|220682|220683|220684|220685|220686|220687|220688|220689|220690|220691|220692|220693|220694|220695|220696|220697|220698|220699|220700|220701|220702|220703|220704|220705|220706|220707|220708|220709|220710|220711|220712|220713|220714|220715|220716|220717|220718|220719|220720|220721|220722|220723|220724|220725|220726|220727|220728|220729|220730|220731|220732|220739|220740|220741|220742|220743|220744'

--select top 100 * from reviewsummaries
--select * from users
CREATE procedure [dbo].[spGetDashboardData]
	@AccountIDs varchar(MAX) = ''
as

set nocount on

declare @dt datetime
set @dt=getdate()

select * 
into #ids
from dbo.fnList2IDTable(@AccountIDs, '|')

print '1: ' + convert(varchar, datediff(ms,@dt,getdate()))
set @dt=getdate()

-- get the detail temp data
select		AccountID, ReviewSourceID, Rating, ReviewDate, cast(Filtered as int) Filtered, cast(RatingPolarity as int) RatingPolarity, DateEntered
into		#reviews
from		Reviews r with(nolock)
inner join	#ids ids on ids.id=r.AccountID

print '2: ' + convert(varchar, datediff(ms,@dt,getdate()))
set @dt=getdate()

create index temp_reviewdetail_accountid on #reviews(AccountID)
create index temp_reviewdetail_reviewsourceid on #reviews(ReviewSourceID)
create index temp_reviewdetail_reviewdate on #reviews(ReviewDate)

print '3: ' + convert(varchar, datediff(ms,@dt,getdate()))
set @dt=getdate()

-- get the newest summary temp data
select		rs.AccountID, rs.ReviewSourceID, rs.SummaryDate, rs.SalesAverageRating, rs.SalesReviewCount, rs.ServiceAverageRating, rs.ServiceReviewCount,
			rs.NewReviewsCount, rs.NewReviewsAverageRating, rs.NewReviewsRatingSum, rs.NewReviewsPositiveCount, rs.NewReviewsNegativeCount,
			rs.TotalPositiveReviews, rs.TotalNegativeReviews
into		#summaries
from		ReviewSummaries rs with(nolock)
inner join	#ids ids on ids.id=rs.AccountID
inner join (
	select		rs2.AccountID, max(rs2.SummaryDate) SummaryDate
	from		ReviewSummaries rs2 with(nolock)
	inner join	#ids ids on ids.id=rs2.AccountID
	group by	rs2.AccountID
) rs1 on rs1.AccountID=rs.AccountID and rs1.SummaryDate=rs.SummaryDate

print '4: ' + convert(varchar, datediff(ms,@dt,getdate()))
set @dt=getdate()

-- get last 30 data (also works with last 7)
select		rs.AccountID, rs.ReviewSourceID, rs.SummaryDate, rs.SalesAverageRating, rs.SalesReviewCount, rs.ServiceAverageRating, rs.ServiceReviewCount,
			rs.NewReviewsCount, rs.NewReviewsAverageRating, rs.NewReviewsRatingSum, rs.NewReviewsPositiveCount, rs.NewReviewsNegativeCount,
			rs.TotalPositiveReviews, rs.TotalNegativeReviews
into		#summaries30
from		ReviewSummaries rs with(nolock)
inner join	dbo.fnList2IDTable(@AccountIDs, '|') ids on ids.id=rs.AccountID
where rs.SummaryDate>=dateadd(day,-30, getdate())

print '5: ' + convert(varchar, datediff(ms,@dt,getdate()))
set @dt=getdate()

create table #counts (
	ReviewSourceID			bigint null,
	AccountID				bigint null,
	LastXDays				int null,
	Total					int null,
	TotalFiltered			int null,
	TotalPositive			int null,
	TotalPositiveFiltered	int null,
	TotalNegative			int null,
	TotalNegativeFiltered	int null,
	AverageRating			decimal(2,1),
	LastReviewDate			datetime
)

--insert the rows with empty values, this serves as a template for the actual value fills
insert		#counts (	ReviewSourceID, AccountID, LastXDays, 
						Total, TotalFiltered, TotalPositive, TotalPositiveFiltered,
						TotalNegative, TotalNegativeFiltered, AverageRating)
select		rs.ID, ids.ID, 7, 0, 0, 0, 0, 0, 0, 0
from		ReviewSources rs with(nolock)
cross join	#ids ids

insert		#counts (	ReviewSourceID, AccountID, LastXDays, 
						Total, TotalFiltered, TotalPositive, TotalPositiveFiltered,
						TotalNegative, TotalNegativeFiltered, AverageRating)
select		rs.ID, ids.ID, 30, 0, 0, 0, 0, 0, 0, 0
from		ReviewSources rs with(nolock)
cross join	#ids ids

insert		#counts (	ReviewSourceID, AccountID, LastXDays, 
						Total, TotalFiltered, TotalPositive, TotalPositiveFiltered,
						TotalNegative, TotalNegativeFiltered, AverageRating)
select		rs.ID, ids.ID, null, 0, 0, 0, 0, 0, 0, 0
from		ReviewSources rs with(nolock)
cross join	#ids ids

insert		#counts (	ReviewSourceID, AccountID, LastXDays, 
						Total, TotalFiltered, TotalPositive, TotalPositiveFiltered,
						TotalNegative, TotalNegativeFiltered, AverageRating)
select		rs.ID, null, 7, 0, 0, 0, 0, 0, 0, 0
from		ReviewSources rs with(nolock)

insert		#counts (	ReviewSourceID, AccountID, LastXDays, 
						Total, TotalFiltered, TotalPositive, TotalPositiveFiltered,
						TotalNegative, TotalNegativeFiltered, AverageRating)
select		rs.ID, null, 30, 0, 0, 0, 0, 0, 0, 0
from		ReviewSources rs with(nolock)

insert		#counts (	ReviewSourceID, AccountID, LastXDays, 
						Total, TotalFiltered, TotalPositive, TotalPositiveFiltered,
						TotalNegative, TotalNegativeFiltered, AverageRating)
select		rs.ID, null, null, 0, 0, 0, 0, 0, 0, 0
from		ReviewSources rs with(nolock)

insert		#counts (	ReviewSourceID, AccountID, LastXDays, 
						Total, TotalFiltered, TotalPositive, TotalPositiveFiltered,
						TotalNegative, TotalNegativeFiltered, AverageRating)
select		null, ids.ID, 7, 0, 0, 0, 0, 0, 0, 0
from		#ids ids

insert		#counts (	ReviewSourceID, AccountID, LastXDays, 
						Total, TotalFiltered, TotalPositive, TotalPositiveFiltered,
						TotalNegative, TotalNegativeFiltered, AverageRating)
select		null, ids.ID, 30, 0, 0, 0, 0, 0, 0, 0
from		#ids ids

insert		#counts (	ReviewSourceID, AccountID, LastXDays, 
						Total, TotalFiltered, TotalPositive, TotalPositiveFiltered,
						TotalNegative, TotalNegativeFiltered, AverageRating)
select		null, ids.ID, null, 0, 0, 0, 0, 0, 0, 0
from		#ids ids

insert		#counts (	ReviewSourceID, AccountID, LastXDays, 
						Total, TotalFiltered, TotalPositive, TotalPositiveFiltered,
						TotalNegative, TotalNegativeFiltered, AverageRating)
values		(0,0,null, 0, 0, 0, 0, 0, 0, 0)

insert		#counts (	ReviewSourceID, AccountID, LastXDays, 
						Total, TotalFiltered, TotalPositive, TotalPositiveFiltered,
						TotalNegative, TotalNegativeFiltered, AverageRating)
values		(0,0,7, 0, 0, 0, 0, 0, 0, 0)

insert		#counts (	ReviewSourceID, AccountID, LastXDays, 
						Total, TotalFiltered, TotalPositive, TotalPositiveFiltered,
						TotalNegative, TotalNegativeFiltered, AverageRating)
values		(0,0,30, 0, 0, 0, 0, 0, 0, 0)

print '6: ' + convert(varchar, datediff(ms,@dt,getdate()))
set @dt=getdate()

-- fill the summary sourced data

update	c
set c.Total=coalesce(summaries.Total,0), c.AverageRating=coalesce(summaries.AvgRating,0)
from		#counts c
inner join (
	select	sum(s.SalesReviewCount) Total, avg(s.SalesAverageRating) AvgRating, 0 AccountID, 0 ReviewSourceID, 0 LastXDays
	from	#summaries s

	union all

	select	sum(s.NewReviewsCount) Total, avg(s.SalesAverageRating) AvgRating, 0 AccountID, 0 ReviewSourceID, 7 LastXDays
	from	#summaries30 s
	where	s.SummaryDate>=dateadd(day, -7, cast(floor(cast( getdate() as float )) as datetime))

	union all 

	select	sum(s.NewReviewsCount) Total, avg(s.SalesAverageRating) AvgRating, 0 AccountID, 0 ReviewSourceID, 30 LastXDays
	from	#summaries30 s	
	where	s.SummaryDate>=dateadd(day, -30, cast(floor(cast( getdate() as float )) as datetime))	

	union all

	select	s.SalesReviewCount Total, s.SalesAverageRating AvgRating, s.AccountID AccountID, s.ReviewSourceID ReviewSourceID, 0 LastXDays
	from	#summaries s

	union all 

	select	sum(s.NewReviewsCount) Total, avg(s.SalesAverageRating) AvgRating, s.AccountID AccountID, s.ReviewSourceID ReviewSourceID, 7 LastXDays
	from	#summaries30 s
	where	s.SummaryDate>=dateadd(day, -7, cast(floor(cast( getdate() as float )) as datetime))
	group by s.AccountID, s.ReviewSourceID

	union all 

	select	sum(s.NewReviewsCount) Total, avg(s.SalesAverageRating) AvgRating, s.AccountID AccountID, s.ReviewSourceID ReviewSourceID, 30 LastXDays
	from	#summaries30 s	
	where	s.SummaryDate>=dateadd(day, -30, cast(floor(cast( getdate() as float )) as datetime))	
	group by s.AccountID, s.ReviewSourceID

	union all 

	select	sum(s.SalesReviewCount) Total, avg(s.SalesAverageRating) AvgRating, s.AccountID AccountID, 0 ReviewSourceID, 0 LastXDays
	from	#summaries s	
	group by s.AccountID

	union all 

	select	sum(s.NewReviewsCount) Total, avg(s.SalesAverageRating) AvgRating, s.AccountID AccountID, 0 ReviewSourceID, 7 LastXDays
	from	#summaries30 s
	where	s.SummaryDate>=dateadd(day, -7, cast(floor(cast( getdate() as float )) as datetime))	
	group by s.AccountID

	union all 

	select	sum(s.NewReviewsCount) Total, avg(s.SalesAverageRating) AvgRating, s.AccountID AccountID, 0 ReviewSourceID, 30 LastXDays
	from	#summaries30 s
	where	s.SummaryDate>=dateadd(day, -30, cast(floor(cast( getdate() as float )) as datetime))	
	group by s.AccountID

	union all 

	select	sum(s.SalesReviewCount) Total, avg(s.SalesAverageRating) AvgRating, 0 AccountID, s.ReviewSourceID ReviewSourceID, 0 LastXDays
	from	#summaries s	
	group by s.ReviewSourceID

	union all 

	select	sum(s.NewReviewsCount) Total, avg(s.SalesAverageRating) AvgRating, 0 AccountID, s.ReviewSourceID ReviewSourceID, 7 LastXDays
	from	#summaries30 s
	where	s.SummaryDate>=dateadd(day, -7, cast(floor(cast( getdate() as float )) as datetime))	
	group by s.ReviewSourceID

	union all 

	select	sum(s.NewReviewsCount) Total, avg(s.SalesAverageRating) AvgRating, 0 AccountID, s.ReviewSourceID ReviewSourceID, 30 LastXDays
	from	#summaries30 s
	where	s.SummaryDate>=dateadd(day, -30, cast(floor(cast( getdate() as float )) as datetime))	
	group by s.ReviewSourceID

) summaries on	summaries.LastXDays=coalesce(c.LastXDays,0)
			and	summaries.AccountID=coalesce(c.AccountID,0)
			and	summaries.ReviewSourceID=coalesce(c.ReviewSourceID,0)

-- fill the detail sourced data

print '7: ' + convert(varchar, datediff(ms,@dt,getdate()))
set @dt=getdate()

update	c
set c.TotalFiltered=coalesce(dets.TotalFiltered,0), 
	c.TotalPositive=coalesce(dets.TotalPositive,0), c.TotalPositiveFiltered=coalesce(dets.TotalPositiveFiltered,0),
	c.TotalNegative=coalesce(dets.TotalNegative,0), c.TotalNegativeFiltered=coalesce(dets.TotalNegativeFiltered,0),
	c.LastReviewDate=coalesce(dets.LastReviewDate,0)
from		#counts c
inner join (

	select	0 LastXDays, 0 AccountID, 0 ReviewSourceID, max(r.ReviewDate) LastReviewDate,
			sum(r.Filtered) TotalFiltered, 
			sum((1-r.Filtered)*r.RatingPolarity) TotalPositive, sum((r.Filtered)*r.RatingPolarity) TotalPositiveFiltered,
			sum((1-r.Filtered)*(1-r.RatingPolarity)) TotalNegative, sum((r.Filtered)*(1-r.RatingPolarity)) TotalNegativeFiltered
	from	#reviews r

	union all

	select	7 LastXDays, 0 AccountID, 0 ReviewSourceID, max(r.ReviewDate) LastReviewDate,
			sum(r.Filtered) TotalFiltered, 
			sum((1-r.Filtered)*r.RatingPolarity) TotalPositive, sum((r.Filtered)*r.RatingPolarity) TotalPositiveFiltered,
			sum((1-r.Filtered)*(1-r.RatingPolarity)) TotalNegative, sum((r.Filtered)*(1-r.RatingPolarity)) TotalNegativeFiltered
	from	#reviews r
	where	r.ReviewDate >= dateadd(day, -7, cast(floor(cast( getdate() as float )) as datetime)) 

	union all

	select	30 LastXDays, 0 AccountID, 0 ReviewSourceID, max(r.ReviewDate) LastReviewDate,
			sum(r.Filtered) TotalFiltered, 
			sum((1-r.Filtered)*r.RatingPolarity) TotalPositive, sum((r.Filtered)*r.RatingPolarity) TotalPositiveFiltered,
			sum((1-r.Filtered)*(1-r.RatingPolarity)) TotalNegative, sum((r.Filtered)*(1-r.RatingPolarity)) TotalNegativeFiltered
	from	#reviews r
	where	r.ReviewDate >= dateadd(day, -30, cast(floor(cast( getdate() as float )) as datetime)) 

	union all

	select	0 LastXDays, 0 AccountID, r.ReviewSourceID ReviewSourceID, max(r.ReviewDate) LastReviewDate,
			sum(r.Filtered) TotalFiltered, 
			sum((1-r.Filtered)*r.RatingPolarity) TotalPositive, sum((r.Filtered)*r.RatingPolarity) TotalPositiveFiltered,
			sum((1-r.Filtered)*(1-r.RatingPolarity)) TotalNegative, sum((r.Filtered)*(1-r.RatingPolarity)) TotalNegativeFiltered
	from	#reviews r
	group by	r.ReviewSourceID

	union all

	select	7 LastXDays, 0 AccountID, r.ReviewSourceID ReviewSourceID, max(r.ReviewDate) LastReviewDate,
			sum(r.Filtered) TotalFiltered, 
			sum((1-r.Filtered)*r.RatingPolarity) TotalPositive, sum((r.Filtered)*r.RatingPolarity) TotalPositiveFiltered,
			sum((1-r.Filtered)*(1-r.RatingPolarity)) TotalNegative, sum((r.Filtered)*(1-r.RatingPolarity)) TotalNegativeFiltered
	from	#reviews r
	where	r.ReviewDate >= dateadd(day, -7, cast(floor(cast( getdate() as float )) as datetime)) 
	group by r.ReviewSourceID

	union all

	select	30 LastXDays, 0 AccountID, r.ReviewSourceID ReviewSourceID, max(r.ReviewDate) LastReviewDate,
			sum(r.Filtered) TotalFiltered, 
			sum((1-r.Filtered)*r.RatingPolarity) TotalPositive, sum((r.Filtered)*r.RatingPolarity) TotalPositiveFiltered,
			sum((1-r.Filtered)*(1-r.RatingPolarity)) TotalNegative, sum((r.Filtered)*(1-r.RatingPolarity)) TotalNegativeFiltered
	from	#reviews r
	where	r.ReviewDate >= dateadd(day, -30, cast(floor(cast( getdate() as float )) as datetime)) 
	group by r.ReviewSourceID

	union all

	select	0 LastXDays, r.AccountID AccountID, 0 ReviewSourceID, max(r.ReviewDate) LastReviewDate,
			sum(r.Filtered) TotalFiltered, 
			sum((1-r.Filtered)*r.RatingPolarity) TotalPositive, sum((r.Filtered)*r.RatingPolarity) TotalPositiveFiltered,
			sum((1-r.Filtered)*(1-r.RatingPolarity)) TotalNegative, sum((r.Filtered)*(1-r.RatingPolarity)) TotalNegativeFiltered
	from	#reviews r	
	group by	r.AccountID

	union all

	select	7 LastXDays, r.AccountID AccountID, 0 ReviewSourceID, max(r.ReviewDate) LastReviewDate,
			sum(r.Filtered) TotalFiltered, 
			sum((1-r.Filtered)*r.RatingPolarity) TotalPositive, sum((r.Filtered)*r.RatingPolarity) TotalPositiveFiltered,
			sum((1-r.Filtered)*(1-r.RatingPolarity)) TotalNegative, sum((r.Filtered)*(1-r.RatingPolarity)) TotalNegativeFiltered
	from	#reviews r
	where	r.ReviewDate >= dateadd(day, -7, cast(floor(cast( getdate() as float )) as datetime)) 
	group by	r.AccountID

	union all

	select	30 LastXDays, r.AccountID AccountID, 0 ReviewSourceID, max(r.ReviewDate) LastReviewDate,
			sum(r.Filtered) TotalFiltered, 
			sum((1-r.Filtered)*r.RatingPolarity) TotalPositive, sum((r.Filtered)*r.RatingPolarity) TotalPositiveFiltered,
			sum((1-r.Filtered)*(1-r.RatingPolarity)) TotalNegative, sum((r.Filtered)*(1-r.RatingPolarity)) TotalNegativeFiltered
	from	#reviews r
	where	r.ReviewDate >= dateadd(day, -30, cast(floor(cast( getdate() as float )) as datetime)) 
	group by	r.AccountID

	union all

	select	0 LastXDays, r.AccountID AccountID, r.ReviewSourceID ReviewSourceID, max(r.ReviewDate) LastReviewDate,
			sum(r.Filtered) TotalFiltered, 
			sum((1-r.Filtered)*r.RatingPolarity) TotalPositive, sum((r.Filtered)*r.RatingPolarity) TotalPositiveFiltered,
			sum((1-r.Filtered)*(1-r.RatingPolarity)) TotalNegative, sum((r.Filtered)*(1-r.RatingPolarity)) TotalNegativeFiltered
	from	#reviews r	
	group by	r.ReviewSourceID, r.AccountID

	union all

	select	7 LastXDays, r.AccountID AccountID, r.ReviewSourceID ReviewSourceID, max(r.ReviewDate) LastReviewDate,
			sum(r.Filtered) TotalFiltered, 
			sum((1-r.Filtered)*r.RatingPolarity) TotalPositive, sum((r.Filtered)*r.RatingPolarity) TotalPositiveFiltered,
			sum((1-r.Filtered)*(1-r.RatingPolarity)) TotalNegative, sum((r.Filtered)*(1-r.RatingPolarity)) TotalNegativeFiltered
	from	#reviews r
	where	r.ReviewDate >= dateadd(day, -7, cast(floor(cast( getdate() as float )) as datetime)) 
	group by	r.ReviewSourceID, r.AccountID

	union all

	select	30 LastXDays, r.AccountID AccountID, r.ReviewSourceID ReviewSourceID, max(r.ReviewDate) LastReviewDate,
			sum(r.Filtered) TotalFiltered, 
			sum((1-r.Filtered)*r.RatingPolarity) TotalPositive, sum((r.Filtered)*r.RatingPolarity) TotalPositiveFiltered,
			sum((1-r.Filtered)*(1-r.RatingPolarity)) TotalNegative, sum((r.Filtered)*(1-r.RatingPolarity)) TotalNegativeFiltered
	from	#reviews r
	where	r.ReviewDate >= dateadd(day, -30, cast(floor(cast( getdate() as float )) as datetime)) 
	group by	r.ReviewSourceID, r.AccountID

) dets on	dets.LastXDays=coalesce(c.LastXDays,0)
			and	dets.AccountID=coalesce(c.AccountID,0)
			and	dets.ReviewSourceID=coalesce(c.ReviewSourceID,0)

print '8: ' + convert(varchar, datediff(ms,@dt,getdate()))
set @dt=getdate()

--spGetDashboardData @AccountIDs='200049|206585|218308|218727|208072'

--the actual data
select *  from #counts

--the simple account list
--select count(*) from accounts where physicaladdressid is null
select		a.ID, coalesce(a.DisplayName,a.Name) Name,
			adr.Street1 AddressLine1,
			coalesce(c.Name,'') + ', ' + coalesce(s.Abbreviation,s.Name,'') + ' ' + coalesce(z.ZipCode,'') AddressLine2,
			a.AccountTypeID
from		Accounts a with(nolock)
left join	Addresses adr with(nolock) on a.BillingAddressID=adr.ID
left join	Cities c with(nolock) on adr.CityID=c.ID
left join	States s with(nolock) on c.StateID=s.ID
left join	ZipCodes z with(nolock) on c.ZipCodeID=z.ID
where exists (
	select	*
	from	#ids ids
	where ids.ID=a.ID
)

print '9: ' + convert(varchar, datediff(ms,@dt,getdate()))
set @dt=getdate()


drop table #ids
drop table #reviews
drop table #summaries
drop table #summaries30