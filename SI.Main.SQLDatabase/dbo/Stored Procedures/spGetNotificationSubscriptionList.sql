﻿/*

select * from users
insert NotificationSubscriptions(RecipientUserID, AccountID, NotificationTemplateID, ScheduleSpec)
values (31973, 197920, 2, '||-1|-1|3600' )

*/

CREATE procedure spGetNotificationSubscriptionList

as

select		ns.ID NotificationSubscriptionID, ns.ScheduleSpec, max(ntar.DateSent) LastNotifiedDate
from		NotificationSubscriptions ns
inner join	NotificationTemplates nt on ns.NotificationTemplateID=nt.ID
left join	NotificationRecipients nr on nr.NotificationTemplateID=nt.ID
left join	NotificationTargets ntar on ntar.ID=nr.NotificationTargetID
group by	ns.ID, ns.ScheduleSpec