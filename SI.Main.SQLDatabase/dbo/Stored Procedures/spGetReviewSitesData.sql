﻿/*
	EXEC spGetReviewSitesData '200049|206585|218308|218727|208072'
*/
CREATE PROCEDURE [dbo].[spGetReviewSitesData]
	@AccountIDs NVARCHAR(MAX) = ''	
AS
BEGIN
	CREATE TABLE #tblAccount (AccountID BIGINT NULL)

	INSERT INTO #tblAccount(AccountID) 
	SELECT * FROM dbo.fnList2IDTable(@AccountIDs, '|')				

	DECLARE @tblReviewDetail1 TABLE (ReviewSourceID INT NOT NULL,
									ReviewSourceName VARCHAR(50) NOT NULL,
									ImagePath VARCHAR(500) NULL,
									ReviewCount INT NOT NULL,
									AverageRating FLOAt NOT NULL,
									PositiveReviews INT NOT NULL,
									NegativeReviews INT NOT NULL,
									PositiveReviewsFiltered INT NOT NULL,
									NegativeReviewsFiltered INT NOT NULL,
									DateUpdated DATETIME NULL,
									UnReadPositive INT NOT NULL,
									UnReadNegative INT NOT NULL,
									UnRespondPositive INT NOT NULL,
									UnRespondNegative INT NOT NULL,
									DisplayOrder INT NOT NULL
									)
	
	--select * from @tblReviewDetail1 

	INSERT INTO @tblReviewDetail1
	SELECT	rss.ID AS 'ReviewSourceID',
			rss.Name AS 'ReviewSourceName',
			rss.ImagePath AS 'ImagePath',
			--rs.AccountID AS 'AccountID',			
			ISNULL(SUM(rs.SalesReviewCount),0)  AS 'ReviewCount',
			ISNULL(ROUND(AVG(rs.SalesAverageRating),1),0)  AS 'AverageRating',
			ISNULL(SUM(rs.TotalPositiveReviews),0)  AS 'PositiveReviews',
			ISNULL(SUM(rs.TotalNegativeReviews),0)  AS 'NegativeReviews',
			(	SELECT ISNULL(COUNT(r1.ID),0)
				from Reviews r1 
						INNER JOIN #tblAccount tbl1
							ON tbl1.AccountID = r1.AccountID
				where	r1.ReviewSourceID = rss.ID 
						AND r1.Filtered = 1 
						AND r1.RatingPolarity = 1) AS 'PositiveReviewsFiltered',
			(	SELECT ISNULL(COUNT(r1.ID),0)
				from Reviews r1 
						INNER JOIN #tblAccount tbl1
							ON tbl1.AccountID = r1.AccountID
				where	r1.ReviewSourceID = rss.ID
						AND r1.Filtered = 1 
						AND r1.RatingPolarity = 0) AS 'NegativeReviewsFiltered',			
			(	SELECT MAX(r1.DateEntered)
				from Reviews r1 
						INNER JOIN #tblAccount tbl1
							ON tbl1.AccountID = r1.AccountID
				where	r1.ReviewSourceID = rss.ID) AS 'DateUpdated',
			 --MAX(rs.DateEntered) AS 'DateUpdated',
			 0 AS 'UnReadPositive',
			 0 AS 'UnReadNegative',
			 0 AS 'UnRespondPositive',
			 0 AS 'UnRespondNegative',
			 rss.DisplayOrder		 
	FROM #tblAccount tbl
		INNER JOIN dbo.ReviewSummaries rs
			ON tbl.AccountID = rs.AccountID
				and rs.SummaryDate = (SELECT MAX(rs1.SummaryDate) from ReviewSummaries rs1 where rs1.AccountID = rs.AccountID and rs1.ReviewSourceID = rs.ReviewSourceID)
					and rs.SalesAverageRating > 0
		RIGHT OUTER JOIN dbo.ReviewSources rss
			ON rs.ReviewSourceID = rss.ID
	where  rss.ImagePath IS NOT NULL
	GROUP BY rss.ID,rss.Name,rss.ImagePath,rss.DisplayOrder --,rs.AccountID
	ORDER BY rss.DisplayOrder ASC
	
	--select @TotalCount = count(*) from	@tblReviewDetail1

	select * from @tblReviewDetail1

	DROP TABLE #tblAccount	
END