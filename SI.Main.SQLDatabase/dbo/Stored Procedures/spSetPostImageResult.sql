﻿CREATE procedure spSetPostImageResult
	@postImageID bigint,
	@postTargetID bigint,
	@resultID varchar(250),
	@feedID varchar(250)
as

set nocount on

update  PostImageResults
set ResultID=@resultID, FeedID=@feedID, ProcessedDate=getdate()
where	PostTargetID=@postTargetID
and		PostImageID=@postImageID