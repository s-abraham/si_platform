﻿--spSearchPosts @SortBy=1, @SortAscending=0, @StartIndex=1, @EndIndex=10
CREATE procedure [dbo].[spSearchPosts]
	@postIDs			varchar(max) = '',
	@accountIDs			varchar(max) = '',				-- restrict to these account ids	
	@FranchiseTypeIDs	nvarchar(100) = null,			-- a pipe delimited list of franchise type ids, any of which can match for a result to be valid
	@SocialNetworkIDs	nvarchar(100) = null,			-- a pipe delimited list of network ids, any of which can match for a result to be valid
	@MinScheduleDate	datetime = null,
	@MaxScheduleDate	datetime = null,
	@SortBy int,
	@SortAscending bit,
	@StartIndex int,	
	@EndIndex int
as

set nocount on

if (@postIDs is null) set @postIDs = ''
set @postIDs = ltrim(rtrim(@postIDs))
if (@accountIDs is null) set @accountIDs = ''
set @accountIDs = ltrim(rtrim(@accountIDs))
if (@FranchiseTypeIDs is null) set @FranchiseTypeIDs = ''
set @FranchiseTypeIDs = ltrim(rtrim(@FranchiseTypeIDs))

select	a.ID
into	#ta
from	Accounts a with(nolock)

if (@accountIDs <> '') begin
	delete	t
	from	#ta t
	where not exists (
		select	*
		from	dbo.fnList2IDTable(@accountIDs, '|') aids
		where	aids.id = t.id
	)
end

if (@FranchiseTypeIDs <> '') begin
	delete	t
	from	#ta t
	where not exists (
		select	*
		from		dbo.fnList2IDTable(@FranchiseTypeIDs, '|') fids
		inner join	Accounts_FranchiseTypes aft with(nolock) on aft.FranchiseTypeID=fids.id
		where		aft.AccountID=t.ID
	)
end

select	p.ID, cast(0 as int) pstat
into	#p
from	Posts p with(nolock)
where exists (
	select		*
	from		PostTargets pt with(nolock)
	inner join	Credentials c with(nolock) on c.ID=pt.CredentialID
	inner join	#ta t on t.ID = c.AccountID
	where		pt.PostID=p.ID
)

if (@PostIDs <> '') begin
	delete	tp
	from	#p tp
	where not exists (
		select		*
		from		dbo.fnList2IDTable(@PostIDs, '|') pids
		where		pids.id=tp.id		
	)
end

if (@SocialNetworkIDs <> '') begin
	delete	tp
	from	#p tp
	where not exists (
		select		*
		from		dbo.fnList2IDTable(@SocialNetworkIDs, '|') snids
		inner join	SocialApps sa with(nolock) on sa.SocialNetworkID=snids.ID
		inner join	Credentials c with(nolock) on c.SocialAppID=sa.ID
		inner join	PostTargets pt with(nolock) on pt.CredentialID=c.ID
		where		pt.PostID=tp.ID
	)
end

if (@MinScheduleDate is not null) begin
	delete	 tp
	from	#p tp
	where not exists (
		select		*
		from		Posts p with(nolock)
		where		p.ID=tp.ID
		and			p.ScheduleDate>=@MinScheduleDate
	)
end

if (@MaxScheduleDate is not null) begin
	delete	 tp
	from	#p tp
	where not exists (
		select		*
		from		Posts p with(nolock)
		where		p.ID=tp.ID
		and			p.ScheduleDate<=@MaxScheduleDate
	)
end


-- 0 = queued, 1= processing, 2 = published, 3=Failed

update	tp
set		tp.pstat = 1
from	#p tp
where exists (
	select		*
	from		PostTargets pt with(nolock)
	where		pt.PostID=tp.ID
	and			pt.PickedUpDate is not null
	and			pt.PublishedDate is null
)

update	tp
set		tp.pstat = 2
from	#p tp
where	tp.pstat = 0
and not exists (
	select		*
	from		PostTargets pt with(nolock)
	where		pt.PostID=tp.ID
	and			pt.PublishedDate is null
)

update	tp
set		tp.pstat = 3
from	#p tp
where	tp.pstat = 0
and exists (
	select		*
	from		PostTargets pt with(nolock)
	where		pt.PostID=tp.ID
	and			pt.PublishedDate is null
	and			pt.PickedUpDate is not null
	and			abs(datediff(minute, pt.PickedUpDate, getdate())) > 40
)

create table #tr (
	id		bigint not null,
	pstat	int not null,
	rownum	int not null
)

if @SortBy = 1 begin
	insert	#tr (id,pstat,rownum)
	select	tp.ID, tp.pstat, ROW_NUMBER() OVER( order by tp.pstat asc )
	from		#p tp
	inner join	Posts p with(nolock) on tp.ID=p.ID
end
if @SortBy = 2 begin
	insert	#tr (id,pstat,rownum)
	select	tp.ID, tp.pstat, ROW_NUMBER() OVER( order by p.ScheduleDate asc )
	from		#p tp
	inner join	Posts p with(nolock) on tp.ID=p.ID
end
if @SortBy = 3 begin
	insert	#tr (id,pstat,rownum)
	select	tp.ID, tp.pstat, ROW_NUMBER() OVER( order by p.title asc )
	from		#p tp
	inner join	Posts p with(nolock) on tp.ID=p.ID
end

if @SortAscending = 0 begin
	declare @maxnum int
	select @maxnum = max(rownum) + 1 from #tr
	update	#tr
	set rownum = @maxNum - rownum
end

select count(*) Total from #tr

select	p.ID PostID, tp.pstat PostStatusEnum, p.Title,
		0 Actions, 0 Impressions, p.PostTypeID,
		p.UserID, u.Username, p.ScheduleDate, count(distinct c.AccountID) NumberOfAccounts,
		max(pt.PublishedDate) PublishedDate, max(pt.PickedUpDate) PickedUpDate,
		u.Firstname, u.LastName, tp.RowNum
into	#t3
from		Posts p with(nolock)
inner join	#tr tp on p.ID=tp.ID
inner join	PostTargets pt with(nolock) on pt.PostID=p.ID
inner join	[Credentials] c with(nolock) on c.ID=pt.CredentialID
inner join	Users u with(nolock) on u.ID=p.UserID
where		tp.rownum between @StartIndex and @EndIndex
group by	p.ID, tp.pstat, p.PostTypeID, p.title,
			p.UserID, u.Username, p.ScheduleDate, tp.RowNum,
			u.Firstname, u.LastName
order by	tp.RowNum

--main results
select * from #t3 order by RowNum

--social network lists
select		p.PostID, sn.ID SocialNetworkID, sn.Name SocialNetworkName
from		#t3 p
inner join	PostTargets pt with(nolock) on p.PostID=pt.PostID
inner join	Credentials c with(nolock) on pt.CredentialID=c.ID
inner join	SocialApps sa with(nolock) on sa.ID=c.SocialAppID
inner join	SocialNetworks sn with(nolock) on sa.SocialNetworkID=sn.ID

drop table #t3
drop table #p
drop table #ta

--spSearchPosts @SortBy=3, @SortAscending=0, @StartIndex=1, @EndIndex=5