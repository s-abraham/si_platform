﻿--spGetAccountAcccessList 201526
CREATE procedure spGetAccountAcccessList
	@AccountID bigint
as

set nocount on

create table #t (
	RoleID bigint not null,
	AccountID bigint not null,	
	RoleTypeID bigint not null,
	InheritedFromAccountID bigint null,
	UserID bigint not null
)

insert #t (RoleID,AccountID,RoleTypeID,InheritedFromAccountID,UserID) exec [spGetAccountRoles] @AccountID=@AccountID

select		t.*, rt.Name RoleName, r.IncludeChildren, 
			u.FirstName, u.LastName, u.EmailAddress, u.DateCreated, u.MemberOfAccountID, u.RootAccountID, u.SuperAdmin, u.Username,
			coalesce(a.DisplayName, a.Name) InheritedFromAccountName
from		#t t
inner join	RoleTypes rt on t.RoleTypeID=rt.ID
inner join	Roles r on t.RoleID=r.ID
inner join	Users u on t.UserID=u.ID
left join	Accounts a on t.InheritedFromAccountID = a.ID

drop table #t