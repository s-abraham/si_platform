﻿CREATE procedure [dbo].[spGetAccountTimezone]
   @AccountID bigint
as

set nocount on

select	a.TimezoneID TimezoneID
from		Accounts a
where a.ID	= @AccountID