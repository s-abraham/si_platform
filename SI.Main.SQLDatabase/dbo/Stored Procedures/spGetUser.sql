﻿--select * from users
-- exec spGetUser @userid=31973
CREATE PROCEDURE [dbo].[spGetUser]
	@username nvarchar(50) = null,
	@userID bigint = null,
	@returnPermissions bit = 1,
	@returnRoles bit = 1,
	@emailAddress nvarchar(100) = null
AS
BEGIN

if (@userID is null) begin	
	if (@username is not null) begin
		select		@userID = u.ID
		from		Users u with(nolock)
		where		u.Username = @username
	end else begin
		select		@userID = u.ID
		from		Users u with(nolock)
		where		u.EmailAddress = @emailAddress
	end
end

/*------------------- Basic Info ------------------*/
select		u.*, r.RoleTypeID, rt.Name RoleTypeName, r.IncludeChildren CascadeRole, coalesce(a.DisplayName, a.Name) AccountName
from		Users u with(nolock)
inner join	Accounts a with(nolock) on u.MemberOfAccountID=a.ID
left join	Accounts_Users au with(nolock) on au.AccountID=a.ID and au.UserID=u.ID
left join	Roles r with(nolock) on r.Account_UserID=au.ID
left join	RoleTypes rt with(nolock) on rt.ID=r.RoleTypeID
where		u.ID = @userID


/*------------------- Permissions ------------------*/

if @returnPermissions = 1 begin
	--exec spGetAccountPermissions @UserID = @userID
	select		au.AccountID, p.PermissionTypeID, p.IncludeChildren
	from		[Permissions] p
	inner join	Accounts_Users au on p.Account_UserID = au.ID
	where		au.UserID = @userID
end

/*------------------- Roles ------------------*/

if @returnRoles = 1 begin
	exec spGetAccountRoles @UserID=@userID
end



/*------------------- Settings ------------------*/

select	*
from	UserSettings with(nolock)
where	UserID=@userID



/*------------------- Notifications ------------------*/

SELECT        COUNT(noti.NotificationId) AS NotiCount
FROM            [SI.Notification].dbo.Notification AS noti 
				LEFT OUTER JOIN
                         [SI.Notification].dbo.NotificationRead AS nr ON noti.NotificationId = nr.NotificationId 
				INNER JOIN
                         [SI.Notification].dbo.NotificationTransmission AS ntr ON noti.NotificationId = ntr.NotificationId 
				INNER JOIN
                         [SI.Notification].dbo.NotificationMethod AS nm ON ntr.NotificationMethodId = nm.NotificationMethodId
WHERE        (nm.NotificationMethodId = 1) AND (nr.NotificationReadId IS NULL)


END