﻿
--spValidateAccessToken '062D9A72-E21A-406B-A893-0AD0449ADEE5'
CREATE procedure [dbo].[spValidateAccessToken]
	@AccessToken uniqueidentifier
as

set nocount on

select		u.ID UserID, r.ID ResellerID, r.AccountID AccountID
from		SSOAccessTokens t with(nolock)
inner join	Resellers r with(nolock) on t.ResellerSecretKey=r.SSOSecretKey
inner join	Users u with(nolock) on t.UserSecretKey=u.SSOSecretKey
where		t.AccessToken=@AccessToken
and			t.DateExpires>=getdate()