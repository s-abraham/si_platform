﻿
/*

Returns a list of Competitors for given account id.

exec spGetAccountCompetitors @AccountID=197923
*/

CREATE procedure [dbo].[spGetAccountCompetitors]
	@AccountID bigint
as

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED

SELECT        a.ID AS AccountID, a.DisplayName, cit.Name AS CityName, CONVERT(varchar, zip.ZipCode) AS ZipCode, sta.Name AS StateName, 
                         sta.Abbreviation AS StateAbbreviation, ac.ID AS 'Accounts_CompetitorsID'
FROM            Accounts AS a LEFT OUTER JOIN
                         Accounts_Competitors AS ac ON a.ID = ac.CompetitorAccountID LEFT OUTER JOIN
                         Addresses AS ba ON a.BillingAddressID = ba.ID LEFT OUTER JOIN
                         Cities AS cit ON ba.CityID = cit.ID LEFT OUTER JOIN
                         States AS sta ON cit.StateID = sta.ID LEFT OUTER JOIN
                         ZipCodes AS zip ON cit.ZipCodeID = zip.ID
WHERE        (ac.Active = 1)
		AND ac.AccountID=@AccountID