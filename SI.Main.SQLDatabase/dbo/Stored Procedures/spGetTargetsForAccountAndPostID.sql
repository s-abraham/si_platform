﻿/*
select * from posttargets
select * from credentials

spGetTargetsForAccountAndPostID @accountID=197924, @postID=1

*/
CREATE procedure spGetTargetsForAccountAndPostID

@postID bigint,
@accountID bigint

as

set nocount on

select		pt.ID PostTargetID, sa.ID SocialAppID, c.ID CredentialID, p.PostTypeID, sa.SocialNetworkID
from		PostTargets pt with(nolock)
inner join	Posts p with(nolock) on p.ID=pt.PostID
inner join	[Credentials] c with(nolock) on pt.CredentialID=c.ID
inner join	SocialApps sa with(nolock) on c.SocialAppID=sa.ID
where		pt.PostID=@postID
and			c.AccountID=@accountID

select		pimg.PostID, pimg.ID PostImageID
from		PostImages pimg with(nolock)
where		pimg.PostID=@postID