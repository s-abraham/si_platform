﻿--exec spGetAccountTree
CREATE procedure [dbo].[spGetAccountTree]

as

with cte as (
    select		a.ID, a.ParentAccountID, convert(int,0) Level
	from		Accounts a with(nolock)
	where		a.ParentAccountID is null

    union all

    select		a.Id, a.ParentAccountID, P.Level+1 
    from		Accounts as a with(nolock)
    inner join	cte as P on a.ParentAccountID = P.Id
)

select cte.*, r.ID ResellerID, coalesce(a.DisplayName,a.Name) Name
from cte
inner join Accounts a with(nolock) on cte.ID = a.ID
left join Resellers r with(nolock) on cte.ID = r.AccountID
order by level