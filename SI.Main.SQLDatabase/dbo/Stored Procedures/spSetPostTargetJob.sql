﻿CREATE procedure [dbo].[spSetPostTargetJob]
	@postTargetID bigint,
	@jobID bigint
as

set nocount on


update  PostTargets
set		JobID=@jobID, PublishedDate=null
where	ID=@postTargetID