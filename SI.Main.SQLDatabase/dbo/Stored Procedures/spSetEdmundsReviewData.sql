﻿CREATE procedure spSetEdmundsReviewData

@accountID bigint,
@reviewCountSales int,
@reviewCountService int,
@avgReviewSales decimal(18,2),
@avgReviewService decimal(18,2)

as

set nocount on

declare @id bigint
set @id = null
select @id = id from EdmundsReviewData where AccountID=@accountID

if (@id>0) begin
	update	EdmundsReviewData
	set		AvgRatingSales = @avgReviewSales,
			AvgRatingService = @avgReviewService,
			RatingCountSales = @reviewCountSales,
			RatingCountService = @reviewCountService,
			DateModified = getdate()
	where	id=@id
end else begin
	insert EdmundsReviewData (AccountID, AvgRatingSales, AvgRatingService, RatingCountSales, RatingCountService, DateCreated, DateModified)
	values (@accountID, @avgReviewSales, @avgReviewService, @reviewCountSales, @reviewCountService, getdate(), getdate())
end