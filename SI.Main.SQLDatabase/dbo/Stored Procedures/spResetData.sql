﻿/*
select * from accounts
select * from phonetypes
select * from phones
select * from addresses
select * from accounttypes
*/

CREATE procedure [dbo].[spResetData]

as

	delete permissions
	delete roles

	delete accounts_users
	delete accounts_features
	delete accounts_franchisetypes
	delete accounts_packages
	
	delete virtualgroups

	delete usersettings
	delete users

	delete accounts

	delete addresses

	delete franchisetypes