﻿-- select * from users
-- spSearchUsers @CallingUserID=31973, @SortBy=5, @SortAscending=0, @MemberOfAccountID=197924
CREATE procedure [dbo].[spSearchUsers]	
	@CallingUserID bigint,	-- the user that is asking for the list
	
	@SearchStringParts nvarchar(1000) = null,	-- pipe delimited list of search strings.  all must match for a result to be valid
	@UsersWithoutRoleInAccountID bigint = null,

	@MemberOfAccountID bigint = null,
	@NotMemberOfAccountID bigint = null,

	@SortBy int = 1,
	@SortAscending bit = 1,
	@StartIndex int = 1,	
	@EndIndex int = 20
as 

set nocount on

if @UsersWithoutRoleInAccountID<1 begin
	set @UsersWithoutRoleInAccountID=null
end
if @MemberOfAccountID<1 begin
	set @MemberOfAccountID=null
end
if @NotMemberOfAccountID<1 begin
	set @NotMemberOfAccountID=null
end

--select * from permissiontypes

declare @callerIsSuperAdmin bit
select @callerIsSuperAdmin = SuperAdmin
from Users with(nolock)
where ID=@CallingUserID

create table #plist (
	PermissionID bigint not null,
	AccountID bigint not null,
	PermissionTypeID bigint not null,
	InheritedFromAccountID bigint null,
	UserID bigint not null
)

if @callerIsSuperAdmin = 0 begin
	insert #plist(PermissionID,AccountID,PermissionTypeID,InheritedFromAccountID,UserID) exec spGetAccountPermissions @UserID=@CallingUserID, @PermissionTypeID=20
end else begin
	insert #plist (PermissionID,AccountID,PermissionTypeID,InheritedFromAccountID,UserID)
	select	distinct 0, a.ID, 0, 0, 0
	from	Accounts a
end


select	u.ID
into #t
from	Users u
where	exists (
	select	*
	from	#plist pl
	where	pl.AccountID=u.MemberOfAccountID
)

if @UsersWithoutRoleInAccountID is not null begin
	delete	t
	from	#t t
	where not exists (
		select	*
		from		Accounts_Users au with(nolock)
		inner join	Roles r with(nolock) on r.Account_UserID = au.ID
		where	au.UserID=t.ID		
	)
end

if @MemberOfAccountID is not null begin
	delete	t
	from	#t t
	where not exists (
		select	*
		from		Users u with(nolock)		
		where		u.MemberOfAccountID=@MemberOfAccountID	
		and			u.id=t.id	
	)
end
if @NotMemberOfAccountID is not null begin
	delete	t
	from	#t t
	where exists (
		select	*
		from		Users u with(nolock)		
		where		u.MemberOfAccountID=@NotMemberOfAccountID		
		and			u.id=t.id
	)
end


if @SearchStringParts is not null and @SearchStringParts <> '' begin
	
	create table #stable (value nvarchar(100))

	insert	#stable(value)
	select	value 
	from	dbo.fnList2StringTable(@SearchStringParts, '|')

	declare @searchString varchar(100)

	declare st cursor fast_forward for select value from #stable
	open st
	fetch next from st into @searchString
	while @@FETCH_STATUS = 0 begin	

		delete	uid
		from	#t uid
		where not exists (
			select	*
			from		Users u with(nolock)			
			where		u.ID=uid.ID
			and (u.FirstName + '|' + u.LastName + '|' + u.EmailAddress) like ('%' + @searchString + '%')
		)
		fetch next from st into @searchString
	end
	close st
	deallocate st


	drop table #stable

end

select	count(*) Total
from	#t


create table #uid2 (ID bigint not null, RowNum int not null)

-- apply the sorts

if @SortBy = 1
	insert	#uid2 (ID, RowNum)
	select	a.ID, ROW_NUMBER() OVER (order by a.ID asc)
	from	#t a
if @SortBy = 2
	insert	#uid2 (ID, RowNum)
	select	a.ID, ROW_NUMBER() OVER (order by u.FirstName asc)
	from	#t a
	inner join	Users u with(nolock) on u.ID = a.ID
if @SortBy = 3
	insert	#uid2 (ID, RowNum)
	select	a.ID, ROW_NUMBER() OVER (order by u.LastName asc)
	from	#t a
	inner join	Users u with(nolock) on u.ID = a.ID
if @SortBy = 4
	insert	#uid2 (ID, RowNum)
	select	a.ID, ROW_NUMBER() OVER (order by u.EmailAddress asc)
	from	#t a
	inner join	Users u with(nolock) on u.ID = a.ID
if @SortBy = 5
	insert	#uid2 (ID, RowNum)
	select	t.ID, ROW_NUMBER() OVER (order by coalesce(a.DisplayName,a.Name) asc)
	from	#t t
	inner join	Users u with(nolock) on u.ID = t.ID
	inner join	Accounts a with(nolock) on u.MemberOfAccountID=a.ID

if @SortAscending = 0 begin
	declare @maxnum int
	select @maxnum = max(RowNum) + 1 from #uid2
	update	#uid2
	set RowNum = @maxNum - RowNum
end

select		u.ID, u.FirstName, u.LastName, u.EmailAddress, u.DateCreated DateCreatedUTC, 
			a.ID MemberOfAccountID, coalesce(a.DisplayName, a.Name) MemberOfAccountName,
			rt.ID RoleTypeID, rt.Name RoleTypeName, r.IncludeChildren CascadeRole, u.SSOSecretKey
from		Users u
inner join	#uid2 t on t.ID=u.ID
inner join	Accounts a on u.MemberOfAccountID=a.ID
left join Accounts_Users au on a.ID = au.AccountID and u.ID=au.UserID
left join Roles r on r.Account_userID = au.ID
left join RoleTypes rt on rt.ID = r.RoleTypeID
where		t.RowNum between @StartIndex and @EndIndex



drop table #t
drop table #plist