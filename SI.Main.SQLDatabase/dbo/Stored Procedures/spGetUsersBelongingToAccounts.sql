﻿--select * from users
-- exec spGetUsers @userid=31973
CREATE PROCEDURE [dbo].[spGetUsersBelongingToAccounts]
	@memberOfAccountIDs	varchar(max) = null
AS

BEGIN

set nocount on

create table #temp (
	ID bigint not null
)

insert #temp (id)
select	id
from	dbo.fnList2IDTable(@memberOfAccountIDs, ',')

/*------------------- Basic Info ------------------*/
select		u.*, r.RoleTypeID, rt.Name RoleTypeName, r.IncludeChildren CascadeRole, coalesce(a.DisplayName, a.Name) AccountName
from		Users u with(nolock)
inner join	Accounts a with(nolock) on u.MemberOfAccountID=a.ID
left join	Accounts_Users au with(nolock) on au.AccountID=a.ID and au.UserID=u.ID
left join	Roles r with(nolock) on r.Account_UserID=au.ID
left join	RoleTypes rt with(nolock) on rt.ID=r.RoleTypeID
where exists (
	select		*
	from		#temp t
	where		t.ID = u.MemberOfAccountID
)


/*------------------- Notifications ------------------*/

SELECT        COUNT(noti.NotificationId) AS NotiCount
FROM            [SI.Notification].dbo.Notification AS noti 
				LEFT OUTER JOIN
                         [SI.Notification].dbo.NotificationRead AS nr ON noti.NotificationId = nr.NotificationId 
				INNER JOIN
                         [SI.Notification].dbo.NotificationTransmission AS ntr ON noti.NotificationId = ntr.NotificationId 
				INNER JOIN
                         [SI.Notification].dbo.NotificationMethod AS nm ON ntr.NotificationMethodId = nm.NotificationMethodId
WHERE        (nm.NotificationMethodId = 1) AND (nr.NotificationReadId IS NULL)

drop table #temp

END