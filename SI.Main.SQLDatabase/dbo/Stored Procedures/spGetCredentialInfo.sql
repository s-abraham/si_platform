﻿--spGetCredentialInfo @CredentialID=197924
--select * from Credentials
CREATE procedure [dbo].[spGetCredentialInfo]
	@CredentialID bigint
as

set nocount on

select		c.ID CredentialID, c.AccountID, c.SocialAppID, c.UniqueID, c.Token, c.TokenSecret, c.IsValid, c.IsActive,
			sa.AppVersion, sa.AppID, sa.CallbackURL, sa.AppSecret, sa.Active IsSocialAppActive, sa.SocialNetworkID,
			c.ScreenName, c.PictureURL, sn.Name SocialNetworkName, c.URI PageURL, sa.AppToken, sa.AppTokenPageID
from		[Credentials] c with(nolock)
inner join	SocialApps sa with(nolock) on c.SocialAppID=sa.ID
inner join	SocialNetworks sn with(nolock) on sn.ID = sa.SocialNetworkID
where		c.ID = @CredentialID