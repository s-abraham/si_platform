﻿
CREATE procedure [dbo].[spGetPostsToQueue]

as

select		distinct p.ID PostID, c.AccountID
from		Posts p with(nolock)
inner join	PostTargets pt with(nolock) on p.ID=pt.PostID
inner join	[Credentials] c with(nolock) on c.ID = pt.CredentialID
where		(pt.ScheduleDate <= getdate() or pt.ScheduleDate is null)
and			pt.PickedUpDate is null