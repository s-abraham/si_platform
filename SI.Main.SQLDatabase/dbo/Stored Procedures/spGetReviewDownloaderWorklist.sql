﻿/*
exec spGetReviewDownloaderWorklist @AccountID=0, @ReviewSourceIDs='100,200'
*/

CREATE procedure [dbo].[spGetReviewDownloaderWorklist]
	
	@ReviewSourceIDs	varchar(max) = null,
	@AccountID			bigint = null

as

set nocount on

if @ReviewSourceIDs = '' set @ReviewSourceIDs = null
if @AccountID=0 set @AccountID=null

create table #rsids (id bigint not null)

if @ReviewSourceIDs is null begin
	insert #rsids (id)
	select id 
	from ReviewSources
	where Status=1
end else begin
	insert #rsids (id)
	select lst.id 
	from dbo.fnList2IDTable(@ReviewSourceIDs,',') lst
	inner join ReviewSources rs on lst.id = rs.id
	where rs.Status=1
end

select		ar.ID Account_ReviewSourceID, ar.ReviewSourceID, ar.AccountID
from		Accounts_ReviewSources ar with(nolock)
where		ar.IsValid=1
and			ar.AccountID=coalesce(@AccountID,ar.AccountID)
and	exists (
	select	*
	from	#rsids rsids
	where	rsids.id = ar.ReviewSourceID
)


drop table #rsids