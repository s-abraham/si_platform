﻿/****************************************************************
** Name: [spSaveTwitterPage]
**
*****************************************************************
** Change History
*****************************************************************
** #    Date       Author     Description 
**		
*****************************************************************/
/*
	EXEC [spSaveTwitterPage] 
*/
CREATE PROCEDURE [dbo].[spSaveTwitterPage] 
	@ID	bigint	= 0,	
	@CredentialID bigint = 0,			
	@TweetsCount	int = 0,
	@FollowingCount	int = 0,
	@FollowersCount	int = 0,	
	@ListedCount	int = 0,
	@FavoritesCount	int = 0,
	@FriendsCount	int = 0,
	@RetweetsCount	int = 0,
	@Error nvarchar(max) = ''	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	SET XACT_ABORT ON;
	
	DECLARE @DateCreated DATETIME = GETUTCDATE()
	DECLARE @QueryDate DATE = GETUTCDATE()
	DECLARE @PriviousFollowersCount INT = 0
	DECLARE @NewFollowersCount INT = 0
	
	IF NOT EXISTS(	SELECT * 
					FROM	[dbo].[TwitterPages] 
					WHERE	CredentialID = @CredentialID AND QueryDate = @QueryDate)
	BEGIN
		
		IF EXISTS(	SELECT * 
					FROM	[dbo].[TwitterPages] 
					WHERE	CredentialID = @CredentialID)
		BEGIN
			-- Check for Any Error for FollowersCount Count
			IF(@Error = '')
			BEGIN
				-- Calculate @NewFollowersCount from PriviousDay FollowersCount
				SET @QueryDate = GETDATE() - 1

				SELECT @PriviousFollowersCount =  FollowersCount
				FROM	[dbo].[TwitterPages] 
				WHERE	CredentialID = @CredentialID 					
						AND QueryDate = @QueryDate

				SET @NewFollowersCount = @FollowersCount - @PriviousFollowersCount 	

				SET @QueryDate = GETDATE()
			END
			ELSE
			BEGIN
				--Take Avg of Last 10 Days				
				print ''
			END			
			
		END
		ELSE -- Set NewPlusOneCount = 0 As UniqueID Does not have PriviousDay History
		BEGIN
			SET @NewFollowersCount = 0			
		END

		INSERT INTO [dbo].[TwitterPages]
           (CredentialID, 
			TweetsCount, 
			FollowingCount, 
			FollowersCount, 
			NewFollowersCount, 
			ListedCount, 
			FavoritesCount, 
			FriendsCount, 
			RetweetsCount, 			
			QueryDate, 
			DateCreated
           )
     VALUES
           (@CredentialID, 
			@TweetsCount, 
			@FollowingCount, 
			@FollowersCount, 
			@NewFollowersCount, 
			@ListedCount, 
			@FavoritesCount, 
			@FriendsCount, 
			@RetweetsCount, 			
			@QueryDate, 
			@DateCreated)
	END
	ELSE
	BEGIN
		
		-- Check for Any Error 
		IF(@Error = '')
		BEGIN
			-- Calculate NewPlusOneCount from PriviousDay PlusOneCount
			SET @QueryDate = GETUTCDATE() - 1
			SELECT @PriviousFollowersCount =  ISNULL(FollowersCount,0)
			FROM	[dbo].[TwitterPages] 
			WHERE	CredentialID = @CredentialID
					AND QueryDate = @QueryDate 					

			SET @NewFollowersCount = @FollowersCount - @PriviousFollowersCount 		

			SET @QueryDate = GETUTCDATE()
		END
		ELSE
		BEGIN
			-- Set NewPlusOneCount and @FollowerCount from Same Day NewPlusOneCount
			SELECT @NewFollowersCount =  NewFollowersCount, @FollowersCount = FollowersCount
			FROM	[dbo].[TwitterPages] 
			WHERE	CredentialID = @CredentialID
					AND QueryDate = @QueryDate
			 
		END
		IF(@Error != '')
		BEGIN
			-- Set @FollowersCount from Same Day NewPlusOneCount
			SELECT @FollowersCount =  FollowersCount
			FROM	[dbo].[TwitterPages] 
			WHERE	CredentialID = @CredentialID
					AND QueryDate = @QueryDate
		END
		
		UPDATE [dbo].[TwitterPages]
		SET TweetsCount = @TweetsCount, 
			FollowingCount = @FollowingCount, 
			FollowersCount = @FollowersCount, 
			NewFollowersCount = @NewFollowersCount, 
			ListedCount = @ListedCount, 
			FavoritesCount = @FavoritesCount, 
			FriendsCount = @FriendsCount, 
			RetweetsCount = @RetweetsCount			
		WHERE	CredentialID = @CredentialID 				
				AND QueryDate = @QueryDate
	END
	
END	-- BEGIN