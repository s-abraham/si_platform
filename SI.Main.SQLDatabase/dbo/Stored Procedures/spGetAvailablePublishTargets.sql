﻿/*
select * from credentials
exec spGetAvailablePublishTargets @AccountIDs='197924', @SortBy=1, @SortAscending=1, @StartIndex=1, @EndIndex=1000
*/
CREATE procedure [dbo].[spGetAvailablePublishTargets]
	@accountIDs varchar(max) = '',				-- restrict to these account ids
	@SearchStringParts nvarchar(1000) = null,		-- pipe delimited list of search strings.  all must match for a result to be valid
	@FranchiseTypeIDs nvarchar(100) = null,			-- a pipe delimited list of franchise type ids, any of which can match for a result to be valid
	@SocialNetworkIDs nvarchar(100) = null,			-- a pipe delimited list of network ids, any of which can match for a result to be valid
	@SortBy int,
	@SortAscending bit,
	@StartIndex int,	
	@EndIndex int
as

set nocount on

create table #aids(
	id bigint not null
)

if (@accountids is not null and @accountIDs<>'')
	insert #aids (id)
	select id
	from dbo.fnList2IDTable(@accountIDs, '|')
else 
	insert #aids (id)
	select id
	from accounts

if @FranchiseTypeIDs is not null and @FranchiseTypeIDs <> '' begin
	
	delete	a
	from	#aids a
	where not exists (
		select	*
		from		Accounts_FranchiseTypes aft with(nolock)
		inner join	dbo.fnList2IDTable(@FranchiseTypeIDs, '|') l on l.ID = aft.FranchiseTypeID
		where	a.ID = aft.AccountID		
	)

end

if @SearchStringParts is not null and @SearchStringParts <> '' begin
	
	--select * from 
	create table #stable (value nvarchar(100))

	insert	#stable(value)
	select	value 
	from	dbo.fnList2StringTable(@SearchStringParts, '|')

	declare @searchString varchar(100)

	declare st cursor fast_forward for select value from #stable
	open st
	fetch next from st into @searchString
	while @@FETCH_STATUS = 0 begin	

		delete	aid
		from	#aids aid
		where not exists (
			select	*
			from		Accounts a with(nolock)
			left join	Addresses ad1 with(nolock) on a.BillingAddressID = ad1.ID
			left join	Cities c1 with(nolock) on ad1.CityID = c1.ID
			left join	ZipCodes zip1 with(nolock) on c1.ZipCodeID = zip1.ID
			left join	States s1 with(nolock) on c1.StateID = s1.ID
			left join	Countries co1 with(nolock) on s1.CountryID = co1.ID
			left join	Addresses ad2 with(nolock) on a.PhysicalAddressID = ad2.ID
			left join	Cities c2 with(nolock) on ad2.CityID = c2.ID		
			left join	ZipCodes zip2 with(nolock) on c2.ZipCodeID = zip2.ID
			left join	States s2 with(nolock) on c2.StateID = s2.ID
			left join	Countries co2 with(nolock) on s2.CountryID = co2.ID
			left join	Phones ph with(nolock) on a.BillingPhoneID = ph.ID

			where	aid.id = a.id

			and ( 
						a.Name
				+ '|' + isnull(a.DisplayName,'')
				+ '|' + isnull(a.EmailAddress,'')
				+ '|' + isnull(a.LeadsEmailAddress,'')
				+ '|' + isnull(ad1.Street1,'')
				+ '|' + isnull(ad1.Street2,'')
				+ '|' + isnull(ad1.Street3,'')
				+ '|' + isnull(c1.Name,'')
				+ '|' + isnull(s1.name,'')
				+ '|' + isnull(s1.abbreviation,'')
				+ '|' + isnull(co1.Name,'')
				+ '|' + isnull(co1.Prefix,'')
				+ '|' + isnull(convert(varchar(5), zip1.ZipCode),'')
				+ '|' + isnull(ad2.Street1,'')
				+ '|' + isnull(ad2.Street2,'')
				+ '|' + isnull(ad2.Street3,'')
				+ '|' + isnull(c2.Name,'')
				+ '|' + isnull(s2.name,'')
				+ '|' + isnull(s2.abbreviation,'')
				+ '|' + isnull(co2.Name,'')
				+ '|' + isnull(co2.Prefix,'')
				+ '|' + isnull(zip2.ZipCode,'')
				+ '|' + isnull(ph.Number,'')
			) like ('%' + @searchString + '%')
		)
		fetch next from st into @searchString
	end
	close st
	deallocate st


	drop table #stable

end

select	c.ID CredentialID, sa.ResellerID, c.AccountID, c.SocialAppID, sa.SocialNetworkID,
		city.StateID, states.Abbreviation as StateAbbr, addr.Street1 as Address, city.Name as City, zipcodes.ZipCode, sn.Name as Network, a.Name,
		c.IsActive IsCredentialActive, sa.Active IsSocialAppActive, cast (null as datetime) LastPostDate
into	#t
from		Accounts a with(nolock)
inner join	#aids aids with(nolock) on a.ID=aids.ID
inner join	Credentials c with(nolock) on a.ID=c.AccountID
inner join	SocialApps sa with(nolock) on c.SocialAppID=sa.ID
inner join SocialNetworks sn with(nolock) on sa.SocialNetworkID = sn.ID
left join	Addresses addr with(nolock) on a.BillingAddressID=addr.ID
left join	Cities city with(nolock) on addr.CityID=city.ID
left join States states with(nolock) on city.StateID = states.ID
left join ZipCodes zipcodes with(nolock) on city.ZipCodeID = zipcodes.ID
where	c.IsActive=1 and c.IsValid=1

if (@SocialNetworkIDs is not null and @SocialNetworkIDs <> '') 
begin
	delete #t
	where SocialNetworkID not in (select ID from dbo.fnList2IDTable(@SocialNetworkIDs, '|'))
end


update	t
set t.LastpostDate=x.LastPostDate
from	#t t
inner join (
	select		c.ID CredentialID, max(pt.PublishedDate) LastPostDate
	from		PostTargets pt with(nolock)
	inner join	Credentials c with(nolock) on pt.CredentialID=c.ID
	where exists (
		select		*
		from		#aids aids
		inner join	Credentials c2 with(nolock) on aids.id=c2.AccountID
		where		c2.AccountID=c.AccountID
	)
	group by c.ID
) x  on t.CredentialID=x.CredentialID

--return the total count
select count(*) as TotalCount from #t

create table #t2 (
	RowNum int not null,
	CredentialID bigint not null,
	ResellerID bigint not null,
	AccountID bigint not null,
	Name nvarchar(100) null,
	SocialAppId bigint not null,
	SocialNetworkID bigint not null,
	StateID bigint null,
	StateAbbr nvarchar(150) null,
	Address nvarchar(100) null,
	City nvarchar(100) null,
	ZipCode nvarchar(50) null,
	Network nvarchar(100) null,
	IsCredentialActive bit not null,
	IsSocialAppActive bit not null,
	LastPostDate datetime null
)

if @SortBy = 1 --account name
	insert	#t2 (RowNum, CredentialID, ResellerID, AccountID, SocialAppID, SocialNetworkID, StateID, StateAbbr, Address, City, ZipCode, Network, Name, IsCredentialActive, IsSocialAppActive)
	select	ROW_NUMBER() OVER (order by coalesce(a.DisplayName,a.Name) asc),
			ids.CredentialID, ids.ResellerID, ids.AccountID, ids.SocialAppID, ids.SocialNetworkID, ids.StateID, ids.StateAbbr, ids.Address, ids.City, ids.ZipCode, ids.Network, ids.Name, ids.IsCredentialActive, ids.IsSocialAppActive
	from	#t ids
	inner join Accounts a with(nolock) on ids.AccountID=a.ID
else if @SortBy = 2 --social network name
	insert	#t2 (RowNum, CredentialID, ResellerID, AccountID, SocialAppID, SocialNetworkID, StateID, StateAbbr, Address, City, ZipCode, Network, Name, IsCredentialActive, IsSocialAppActive)
	select	ROW_NUMBER() OVER (order by sn.Name asc),
			ids.CredentialID, ids.ResellerID, ids.AccountID, ids.SocialAppID, ids.SocialNetworkID, ids.StateID, ids.StateAbbr, ids.Address, ids.City, ids.ZipCode, ids.Network, ids.Name, ids.IsCredentialActive, ids.IsSocialAppActive
	from	#t ids
	inner join SocialNetworks sn with(nolock) on ids.SocialNetworkID=sn.ID
else if @SortBy = 3 --state name
	insert	#t2 (RowNum, CredentialID, ResellerID, AccountID, SocialAppID, SocialNetworkID, StateID, StateAbbr, Address, City, ZipCode, Network, Name, IsCredentialActive, IsSocialAppActive)
	select	ROW_NUMBER() OVER (order by coalesce(s.Name,'') asc),
			ids.CredentialID, ids.ResellerID, ids.AccountID, ids.SocialAppID, ids.SocialNetworkID, ids.StateID, ids.StateAbbr, ids.Address, ids.City, ids.ZipCode, ids.Network, ids.Name, ids.IsCredentialActive, ids.IsSocialAppActive
	from	#t ids
	left join States s with(nolock) on ids.StateID=s.ID
else if @SortBy = 4 --last post
	insert	#t2 (RowNum, CredentialID, ResellerID, AccountID, SocialAppID, SocialNetworkID, StateID, StateAbbr, Address, City, ZipCode, Network, Name, IsCredentialActive, IsSocialAppActive)
	select	ROW_NUMBER() OVER (order by LastPostDate asc),
			ids.CredentialID, ids.ResellerID, ids.AccountID, ids.SocialAppID, ids.SocialNetworkID, ids.StateID, ids.StateAbbr, ids.Address, ids.City, ids.ZipCode, ids.Network, ids.Name, ids.IsCredentialActive, ids.IsSocialAppActive
	from	#t ids
else 
	insert	#t2 (RowNum, CredentialID, ResellerID, AccountID, SocialAppID, SocialNetworkID, StateID, StateAbbr, Address, City, ZipCode, Network, Name, IsCredentialActive, IsSocialAppActive)
	select	ROW_NUMBER() OVER (order by coalesce(a.DisplayName,a.Name) asc),
			ids.CredentialID, ids.ResellerID, ids.AccountID, ids.SocialAppID, ids.SocialNetworkID, ids.StateID, ids.StateAbbr, ids.Address, ids.City, ids.ZipCode, ids.Network, ids.Name, ids.IsCredentialActive, ids.IsSocialAppActive
	from	#t ids
	inner join Accounts a with(nolock) on ids.AccountID=a.ID

drop table #t

if @SortAscending = 0 begin
	declare @maxnum int
	select @maxnum = max(RowNum) + 1 from #t2
	update	#t2
	set RowNum = @maxNum - RowNum
end

select	t.*
into #t3
from	#t2 t
where	RowNum between @StartIndex and @EndIndex


--return the raw results
select		t.*, a.TimeZoneID, tz.Name as TimeZoneName
from #t3	t
inner join	Accounts a with(nolock) on a.ID=t.AccountID
inner join TimeZones tz with(nolock) on a.TimeZoneID = tz.ID
order by RowNum

--return the franchise type ids
select	distinct t.AccountID, aft.FranchiseTypeID, ft.Name as FranchiseType
from		#t3 t
inner join	Accounts_FranchiseTypes aft with(nolock) on t.AccountID = aft.AccountID
inner join FranchiseTypes ft with(nolock) on aft.FranchiseTypeID = ft.ID

drop table #t2
drop table #t3
drop table #aids

/*
exec spGetAvailablePublishTargets @AccountIDs='197924', @SortBy=3, @SortAscending=1, @StartIndex=1, @EndIndex=1000, @SearchStringParts='e'
*/