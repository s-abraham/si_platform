﻿--select * from reviewsummaries
CREATE procedure spUpsertReviewSummary

@reviewSummaryDataSourceID bigint,
@accountID bigint,
@reviewSourceID bigint,
@salesAverageRating float,
@serviceAverageRating float,
@salesReviewCount int,
@serviceReviewCount int,
@extraInfo varchar(max),
@newReviewsCount int,
@newReviewsAverageRating float,
@newReviewsRatingSum float,
@newReviewsPositiveCount int,
@newReviewsNegativeCount int,
@totalPositiveReviews int,
@totalNegativeReviews int,
@summaryDate datetime

as

set nocount on


set @summaryDate=cast(cast (@summaryDate as DATE) as datetime)

declare @id bigint

select	@id = id
from	ReviewSummaries
where	AccountID=@accountID
and		ReviewSourceID=@reviewSourceID
and		SummaryDate=@summaryDate

if (@id>0) begin

update ReviewSummaries
	set ReviewSummaryDataSourceID=@reviewSummaryDataSourceID,
	salesAverageRating=@salesAverageRating,
	serviceAverageRating=@serviceAverageRating,
	salesReviewCount=@salesReviewCount,
	serviceReviewCount=@serviceReviewCount,
	extraInfo=@extraInfo,
	newReviewsCount=@newReviewsCount,
	newReviewsAverageRating=@newReviewsAverageRating,
	newReviewsRatingSum=@newReviewsRatingSum,
	newReviewsPositiveCount=@newReviewsPositiveCount,
	newReviewsNegativeCount=@newReviewsNegativeCount,
	totalPositiveReviews=@totalPositiveReviews,
	totalNegativeReviews=@totalNegativeReviews,
	DateUpdated=getdate()
where id=@id

end else begin

INSERT INTO [dbo].[ReviewSummaries]
           ([ReviewSummaryDataSourceID]
           ,[AccountID]
           ,[ReviewSourceID]
           ,[SalesAverageRating]
           ,[ServiceAverageRating]
           ,[SalesReviewCount]
           ,[ServiceReviewCount]
           ,[ExtraInfo]
           ,[NewReviewsCount]
           ,[NewReviewsAverageRating]
           ,[NewReviewsRatingSum]
           ,[NewReviewsPositiveCount]
           ,[NewReviewsNegativeCount]
           ,[TotalPositiveReviews]
           ,[TotalNegativeReviews]
           ,[SummaryDate]
           ,[DateUpdated]
           ,[DateEntered])
     VALUES
           (@ReviewSummaryDataSourceID
           ,@AccountID
           ,@ReviewSourceID
           ,@SalesAverageRating
           ,@ServiceAverageRating
           ,@SalesReviewCount
           ,@ServiceReviewCount
           ,@ExtraInfo
           ,@NewReviewsCount
           ,@NewReviewsAverageRating
           ,@NewReviewsRatingSum
           ,@NewReviewsPositiveCount
           ,@NewReviewsNegativeCount
           ,@TotalPositiveReviews
           ,@TotalNegativeReviews
           ,@SummaryDate
           ,getdate()
           ,getdate()
		   )


end

set nocount on