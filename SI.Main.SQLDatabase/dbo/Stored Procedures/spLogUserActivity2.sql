﻿
CREATE procedure [dbo].[spLogUserActivity2]
	@UserActivityTypeID bigint,
	@UserActivityTypeName varchar(50),

	@UserID bigint,
	@ImpersonatedUserID bigint,
	@UserIDActedUponID bigint,
	@AccountID bigint,
	@RoleTypeID bigint,
	@SSOToken uniqueidentifier,
	@CredentialID bigint,
	@Description varchar(250),
	@ActivityDate datetime,

	@ReviewSourceID bigint
as

set nocount on

if not exists (select * from UserActivityTypes with(nolock) where ID=@UserActivityTypeID) begin
	insert UserActivityTypes(ID, Name) values (@UserActivityTypeID,@UserActivityTypeName)
end

insert _UserActivityLogs (UserID, ImpersonatedUserID, UserActivityTypeID, AccountID, RoleTypeID, SSOToken, CredentialID, [Description], ActivityDate, ReviewSourceID)
values (@UserID, @ImpersonatedUserID, @UserActivityTypeID, @AccountID, @RoleTypeID, @SSOToken, @CredentialID, @Description, @ActivityDate, @ReviewSourceID)