﻿/*

Returns a list of account IDs where the given user has the given role type id.
If the role type id is null, all roles for that user are given for all accounts.

--select * from users
exec [spGetAccountRoles] @UserID=41999
exec [spGetAccountRoles] @AccountID=201526
*/

CREATE procedure [dbo].[spGetAccountRoles]
	@UserID bigint = NULL,
	@AccountID bigint = NULL,
	@RoleTypeID bigint = null
as

set nocount on

;with cte as (
		select		a.ID, a.ParentAccountID, r.ID RoleID, r.RoleTypeID, a.ID InheritedFromAccountID, au.UserID
		from		Accounts a with(nolock)
		inner join	Accounts_Users au with(nolock) on a.ID=au.AccountID
		inner join	Roles r with(nolock) on r.Account_UserID=au.ID
		where r.IncludeChildren=1
		and au.UserID=coalesce(@userid,au.UserID)
		and r.RoleTypeID = coalesce(@RoleTypeID,r.RoleTypeID)

		union all

		select		a.Id, a.ParentAccountID, P.RoleID PermissionID, P.RoleTypeID, P.InheritedFromAccountID, P.UserID
		from		Accounts as a with(nolock)
		inner join	cte as P on a.ParentAccountID = P.Id
		and P.RoleTypeID = coalesce(@RoleTypeID,P.RoleTypeID)
	)

select *
into #t5
from cte

	
select		r.ID RoleID, au1.AccountID AccountID, r.RoleTypeID, convert(bigint,null) InheritedFromAccountID, au1.UserID
from		Accounts_Users au1 with(nolock)
inner join	Roles r with(nolock)	on	r.IncludeChildren = 0
										and	r.Account_UserID = au1.ID
where		au1.UserID=coalesce(@userid,au1.UserID)
and r.RoleTypeID = coalesce(@RoleTypeID,r.RoleTypeID)
and			au1.AccountID=coalesce(@AccountID,au1.AccountID)


union all

select		t.RoleID RoleID, t.ID AccountID, t.RoleTypeID, t.InheritedFromAccountID InheritedFromAccountID, t.UserID
from		#t5 t with(nolock)
where		t.ID=coalesce(@AccountID,t.ID)
	
drop table #t5