﻿/*
select top 100 * from reviews
select * from notifications
select * from notificationsubscriptions
insert notifications (NotificationMessageTypeID, EntityID, DateCreated)
values (2, 354415, getdate())
*/
CREATE procedure spAddNotificationRecipients

	@NotificationSubscriptionIDs varchar(max)

as


insert		NotificationRecipients (NotificationID, UserID, NotificationTemplateID, NotificationTargetID)

select		n.ID, ns.RecipientUserID, ns.NotificationTemplateID, null
from		NotificationSubscriptions ns
inner join	dbo.fnList2IDTable(@NotificationSubscriptionIDs, '|') ids on ids.id = ns.ID
inner join	NotificationTemplates nt on ns.NotificationTemplateID = nt.ID
inner join	Notifications n on	n.NotificationMessageTypeID = nt.NotificationMessageTypeID
								and n.AccountID = ns.AccountID
where not exists (
	select		*
	from		NotificationRecipients nr
	where		nr.UserID = ns.RecipientUserID
	and			nr.NotificationID = n.ID
	and			nr.NotificationTemplateID = ns.NotificationTemplateID
)