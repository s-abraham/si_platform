﻿
--select * from accounts where name like ('%hendrick%')
--spGetDashboardData2 @AccountIDs='198126|206924|206925|220169'
--spGetDashboardData2 @AccountIDs='220633|220635|220636|220637|220638|220639|220640|220641|220642|220643|220644|220645|220646|220647|220648|220649|220650|220651|220652|220653|220654|220655|220656|220657|220658|220659|220660|220661|220662|220663|220664|220665|220666|220667|220668|220669|220670|220671|220672|220673|220674|220675|220676|220677|220678|220679|220680|220681|220682|220683|220684|220685|220686|220687|220688|220689|220690|220691|220692|220693|220694|220695|220696|220697|220698|220699|220700|220701|220702|220703|220704|220705|220706|220707|220708|220709|220710|220711|220712|220713|220714|220715|220716|220717|220718|220719|220720|220721|220722|220723|220724|220725|220726|220727|220728|220729|220730|220731|220732|220739|220740|220741|220742|220743|220744'

--select top 100 * from reviewsummaries
--select * from users
CREATE procedure [dbo].[spGetDashboardData2]
	@AccountIDs varchar(MAX) = ''
as

set nocount on

declare @totalms int
set @totalms=0
declare @localms int
declare @dt datetime
set @dt=getdate()

select * 
into #ids
from dbo.fnList2IDTable(@AccountIDs, '|')

if @AccountIDs is null set @AccountIDs=''
set @AccountIDs = ltrim(rtrim(@AccountIDs))

if @AccountIDs='' begin
	insert	#ids(id)
	select	a.id from accounts a with(nolock)
	where exists (
		select	*
		from	ixvReviewDetail rd with(noexpand, nolock)
		where	rd.AccountID=a.id
	)
end

set @localms=datediff(ms,@dt,getdate()); set @totalms=@totalms+@localms
print '1: ' + convert(varchar, @localms)
set @dt=getdate()

-- get max review summary date for all accounts
select	sd.AccountID, sd.ReviewSourceID, max(sd.SummaryDate) MaxDate
into	#maxdates
from		ixvSummaryData sd with(noexpand, nolock)
inner join	#ids ids on sd.AccountID=ids.id
group by	sd.AccountID, sd.ReviewSourceID

set @localms=datediff(ms,@dt,getdate()); set @totalms=@totalms+@localms
print '2: ' + convert(varchar, @localms)
set @dt=getdate()

select		sd.*
into		#cursum
from		ixvSummaryData sd with(noexpand, nolock)
inner join	#maxdates md on sd.AccountID=md.AccountID and sd.ReviewSourceID=md.ReviewSourceID and sd.SummaryDate=md.MaxDate

set @localms=datediff(ms,@dt,getdate()); set @totalms=@totalms+@localms
print '2.5: ' + convert(varchar, @localms)
set @dt=getdate()

-- get all time reviews

select	sum(sd.SalesReviewCount) Total, sum(sd.TotalNegativeReviews) NegativeReviews, sum(sd.TotalPositiveReviews) PositiveReviews
from	#cursum sd

set @localms=datediff(ms,@dt,getdate()); set @totalms=@totalms+@localms
print '3: ' + convert(varchar, @localms)
set @dt=getdate()

-- get all from unfiltered review detail for these accounts

select	rd.*
into	#alldet
from	ixvReviewDetail rd with(noexpand, nolock)
inner join	#ids ids on rd.AccountID=ids.id

set @localms=datediff(ms,@dt,getdate()); set @totalms=@totalms+@localms
print '4: ' + convert(varchar, @localms)
set @dt=getdate()

-- get last 30 days counts

select	count(*) Total, sum(case when Rating>2.5 then 1 else 0 end) PositiveReviews, sum(case when Rating<=2.5 then 1 else 0 end) NegativeReviews
from	#alldet
where	ReviewDate >= dateadd(day, -30, getdate())

set @localms=datediff(ms,@dt,getdate()); set @totalms=@totalms+@localms
print '4: ' + convert(varchar, @localms)
set @dt=getdate()

-- get last 7 days counts

select	count(*) Total, sum(case when Rating>2.5 then 1 else 0 end) PositiveReviews, sum(case when Rating<=2.5 then 1 else 0 end) NegativeReviews
from	#alldet
where	ReviewDate >= dateadd(day, -7, getdate())

set @localms=datediff(ms,@dt,getdate()); set @totalms=@totalms+@localms
print '5: ' + convert(varchar, @localms)
set @dt=getdate()

-- locations-reputation

select		cs.AccountID AccountID, cast(null as decimal(6,2)) AvgRating, sum(cs.SalesReviewCount) TotalReviews, 
			sum(cs.TotalPositiveReviews) PositiveReviews, sum(cs.TotalNegativeReviews) NegativeReviews, 
			cast(null as datetime) LastReviewDate
into		#locrev
from		#cursum cs
group by	cs.AccountID

set @localms=datediff(ms,@dt,getdate()); set @totalms=@totalms+@localms
print '6: ' + convert(varchar, @localms)
set @dt=getdate()

update		l
set			l.AvgRating=t.AvgRating
from		#locrev l
inner join	(
	select		cs.AccountID, avg(cs.AvgRating) AvgRating
	from		#cursum cs
	where		cs.AvgRating>0
	group by	cs.AccountID
) t on t.AccountID=l.AccountID


set @localms=datediff(ms,@dt,getdate()); set @totalms=@totalms+@localms
print '7: ' + convert(varchar, @localms)
set @dt=getdate()

update		l
set			l.LastReviewDate=t.ReviewDate
from		#locrev l
inner join	(
	select		d.AccountID, max(d.ReviewDate) ReviewDate
	from		#alldet d
	group by	d.AccountID
) t on t.AccountID=l.AccountID


set @localms=datediff(ms,@dt,getdate()); set @totalms=@totalms+@localms
print '8: ' + convert(varchar, @localms)
set @dt=getdate()

select		cs.ReviewSourceID ReviewSourceID, cast(null as decimal(6,2)) AvgRating, sum(cs.SalesReviewCount) TotalReviews, 
			sum(cs.TotalPositiveReviews) PositiveReviews, sum(cs.TotalNegativeReviews) NegativeReviews, 
			cast(null as datetime) LastReviewDate
into		#revsit
from		#cursum cs
group by	cs.ReviewSourceID

set @localms=datediff(ms,@dt,getdate()); set @totalms=@totalms+@localms
print '9: ' + convert(varchar, @localms)
set @dt=getdate()

update		l
set			l.AvgRating=t.AvgRating
from		#revsit l
inner join	(
	select		cs.ReviewSourceID, avg(cs.AvgRating) AvgRating
	from		#cursum cs
	where		cs.AvgRating>0
	group by	cs.ReviewSourceID
) t on t.ReviewSourceID=l.ReviewSourceID


set @localms=datediff(ms,@dt,getdate()); set @totalms=@totalms+@localms
print '10: ' + convert(varchar, @localms)
set @dt=getdate()

update		l
set			l.LastReviewDate=t.ReviewDate
from		#revsit l
inner join	(
	select		d.ReviewSourceID, max(d.ReviewDate) ReviewDate
	from		#alldet d
	group by	d.ReviewSourceID
) t on t.ReviewSourceID=l.ReviewSourceID


set @localms=datediff(ms,@dt,getdate()); set @totalms=@totalms+@localms
print '11: ' + convert(varchar, @localms)
set @dt=getdate()

select * from #locrev
select * from #revsit





--spGetDashboardData2 @AccountIDs='220633|220635|220636|220637|220638|220639|220640|220641|220642|220643|220644|220645|220646|220647|220648|220649|220650|220651|220652|220653|220654|220655|220656|220657|220658|220659|220660|220661|220662|220663|220664|220665|220666|220667|220668|220669|220670|220671|220672|220673|220674|220675|220676|220677|220678|220679|220680|220681|220682|220683|220684|220685|220686|220687|220688|220689|220690|220691|220692|220693|220694|220695|220696|220697|220698|220699|220700|220701|220702|220703|220704|220705|220706|220707|220708|220709|220710|220711|220712|220713|220714|220715|220716|220717|220718|220719|220720|220721|220722|220723|220724|220725|220726|220727|220728|220729|220730|220731|220732|220739|220740|220741|220742|220743|220744'


--the simple account list
select		a.ID, coalesce(a.DisplayName,a.Name) Name,
			adr.Street1 AddressLine1,
			coalesce(c.Name,'') + ', ' + coalesce(s.Abbreviation,s.Name,'') + ' ' + coalesce(z.ZipCode,'') AddressLine2,
			a.AccountTypeID
from		Accounts a with(nolock)
left join	Addresses adr with(nolock) on a.BillingAddressID=adr.ID
left join	Cities c with(nolock) on adr.CityID=c.ID
left join	States s with(nolock) on c.StateID=s.ID
left join	ZipCodes z with(nolock) on c.ZipCodeID=z.ID
where exists (
	select	*
	from	#ids ids
	where ids.ID=a.ID
)

set @localms=datediff(ms,@dt,getdate()); set @totalms=@totalms+@localms
print '12: ' + convert(varchar, @localms)
set @dt=getdate()

print 'Total Execution Time: ' + convert(varchar, @totalms)

drop table #revsit
drop table #locrev
drop table #cursum
drop table #alldet
drop table #maxdates
drop table #ids