﻿CREATE PROCEDURE [dbo].[ReportSyndicationPostApproval]
		@PostID bigint
AS
BEGIN
	SET NOCOUNT ON 
	declare @dummy TABLE(
		PostID bigint,
		LocationName varchar(200),
		ContactName varchar(150),
		Email varchar(200),
		Action varchar(100)
	)

	insert @dummy values (1, 'Castle Chevrolet', 'Jim Smith', 'aaaa@email.com', 'Auto Approved')
	insert @dummy values (1, 'Castle Buick', 'Tarzan', 'aaaa@email.com', 'No Action')
	insert @dummy values (1, 'Castle Ford', 'Jane', 'aaaa@email.com', 'Auto Approved')
	insert @dummy values (1, 'Castle Honda', 'Thor', 'aaaa@email.com', 'Deny')
	insert @dummy values (1, 'Castle Chevrolet', 'Coulson', 'aaaa@email.com', 'Auto Approved')
	insert @dummy values (1, 'Castle Chevrolet', 'Barack Obama', 'aaaa@email.com', 'Auto Approved')
	insert @dummy values (1, 'Castle Kia', 'Bob Hope', 'aaaa@email.com', 'Auto Approved')
	insert @dummy values (1, 'Castle Chevrolet', 'Richard Nixon', 'aaaa@email.com',  'No Action')
	insert @dummy values (1, 'Castle BMW', 'Iron Man', 'aaaa@email.com', 'Auto Approved')
	insert @dummy values (1, 'Castle Chevrolet', 'Captain America', 'aaaa@email.com', 'Auto Approved')



	
	select * from @dummy order by LocationName





END