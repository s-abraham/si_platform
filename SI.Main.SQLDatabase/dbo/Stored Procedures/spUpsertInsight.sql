﻿CREATE procedure spUpsertInsight
	@CredentialID bigint,
	@QueryDate date,
	@FacebookInsightsNameID bigint,
	@FacebookPostID bigint = null,
	@Value int,
	@ValueJSON varchar(5000)	
as

set nocount on

declare @FacebookInsightID bigint

select	@FacebookInsightID = ID
from	FacebookInsights fi with(nolock)
where	fi.CredentialID = @CredentialID
and		fi.QueryDate = @QueryDate
and		fi.FacebookInsightsNamesID = @FacebookInsightsNameID
and		coalesce(fi.FacebookPostID,0)=coalesce(@FacebookPostID,0)

if (@FacebookInsightID>0) begin
	update	FacebookInsights
	set		Value=@Value,
			ValueJSON=@ValueJSON
	where	ID=	@FacebookInsightID	
end else begin
	insert	FacebookInsights(FacebookInsightsNamesID, CredentialID, FacebookPostID, ValueJSON, Value, QueryDate, DateCreated)
	values	(@FacebookInsightsNameID, @CredentialID, @FacebookPostID, @ValueJSON, @Value, @QueryDate, getdate())
end