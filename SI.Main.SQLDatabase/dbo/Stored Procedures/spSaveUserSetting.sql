﻿CREATE procedure [dbo].[spSaveUserSetting]

@userid bigint,
@name varchar(100),
@value varchar(100)

as

delete UserSettings where UserID=@userid and Name=@name

if (@value is not null) begin
	insert UserSettings(userID,Name,Value)
	values (@userid, @name, @value)
end