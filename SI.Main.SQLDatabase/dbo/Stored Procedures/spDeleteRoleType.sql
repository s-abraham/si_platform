﻿CREATE procedure spDeleteRoleType
	@roleTypeID bigint
as

set nocount on

delete	Roles
where	RoleTypeID = @roleTypeID

delete	RoleTypes_PermissionTypes
where	RoleTypeID = @roleTypeID

delete	RoleTypes 
where	ID = @roleTypeID

exec spApplyRoles