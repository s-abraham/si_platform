﻿/*		

Sorts: 
Review Date = 1
Colected Date = 2
Account Name = 3
Review Source Name = 4
Rating = 5

EXEC spGetReviewStreamData2 @AccountIDs='220633|220635|220636|220637|220638|220639|220640|220641|220642|220643|220644|220645|220646|220647|220648|220649|220650|220651|220652|220653|220654|220655|220656|220657|220658|220659|220660|220661|220662|220663|220664|220665|220666|220667|220668|220669|220670|220671|220672|220673|220674|220675|220676|220677|220678|220679|220680|220681|220682|220683|220684|220685|220686|220687|220688|220689|220690|220691|220692|220693|220694|220695|220696|220697|220698|220699|220700|220701|220702|220703|220704|220705|220706|220707|220708|220709|220710|220711|220712|220713|220714|220715|220716|220717|220718|220719|220720|220721|220722|220723|220724|220725|220726|220727|220728|220729|220730|220731|220732|220739|220740|220741|220742|220743|220744',
							@SortBy = 1,
							@SortAscending = 0,
							@StartIndex = 1,
							@EndIndex = 50,
							@ReviewSourceIDs = '100|200|300|400|500|600|700|800|900'
		
*/
CREATE PROCEDURE [dbo].[spGetReviewStreamData2]
	@AccountIDs varchar(MAX),
		
	@StateIDs varchar(1000) = '',	
	@FranchiseTypeIDs varchar(1000) = '',	

	@IncludeFiltered bit = 0,
	@IncludeUnFiltered bit = 1,
	@ReviewSourceIDs varchar(200) = '',	

	@RatingIsGreaterThanOrEqualTo float = null,
	@RatingIsLessThanOrEqualTo float = null,

	@ReviewTextContains nvarchar(500) = null,
	@ReviewTextDoesNotContain nvarchar(500) = null,

	@MinReviewDate datetime = NULL,
	@MaxReviewDate datetime = NULL,
		
	@CategoryID int = null,	
	@SortBy int = 1,
	@SortAscending bit = 1,

	@StartIndex int = 1,
	@EndIndex int = 25
AS
BEGIN

set nocount on

if @RatingIsGreaterThanOrEqualTo = 0 set @RatingIsGreaterThanOrEqualTo = null
if @RatingIsLessThanOrEqualTo = 5 set @RatingIsLessThanOrEqualTo = null

if @CategoryID = 0 set @CategoryID = null
if ltrim(rtrim(@StateIDs))='' set @StateIDs=null
if ltrim(rtrim(@FranchiseTypeIDs))='' set @FranchiseTypeIDs=null
if ltrim(rtrim(@ReviewSourceIDs))='' set @ReviewSourceIDs=null

if ltrim(rtrim(@ReviewTextContains))='' set @ReviewTextContains=null
if ltrim(rtrim(@ReviewTextDoesNotContain))='' set @ReviewTextDoesNotContain=null

--diagnostic timer stuff

declare @totalms int
set @totalms=0
declare @localms int
declare @dt datetime
set @dt=getdate()

-- get the account list

select id
into #ids
from dbo.fnList2IDTable(@AccountIDs, '|')

set @localms=datediff(ms,@dt,getdate()); set @totalms=@totalms+@localms
print '0: ' + convert(varchar, @localms)
set @dt=getdate()

if (@StateIDs is not null) begin
	delete	ids
	from	#ids ids
	where not exists (
		select	*
		from		Accounts a with(nolock)
		inner join	Addresses ad with(nolock) on a.BillingAddressID = ad.ID
		inner join	Cities c with(nolock) on ad.CityID = c.ID
		inner join	dbo.fnList2IDTable(@StateIDs, '|') sel on sel.id = c.StateID
		where		a.ID = ids.ID
	)
end

set @localms=datediff(ms,@dt,getdate()); set @totalms=@totalms+@localms
print '1: ' + convert(varchar, @localms)
set @dt=getdate()

if (@FranchiseTypeIDs is not null) begin
	delete	ids
	from	#ids ids
	where not exists (
		select	*
		from		Accounts_FranchiseTypes aft with(nolock)		
		inner join	dbo.fnList2IDTable(@FranchiseTypeIDs, '|') sel on sel.id = aft.FranchiseTypeID
		where		aft.AccountID = ids.ID
	)
end

set @localms=datediff(ms,@dt,getdate()); set @totalms=@totalms+@localms
print '2: ' + convert(varchar, @localms)
set @dt=getdate()

if (@CategoryID is not null) begin
	delete	ids
	from	#ids ids
	where not exists (
		select	*
		from		Accounts_FranchiseTypes aft with(nolock)		
		inner join	FranchiseCategories_FranchiseType fcft with(nolock) on fcft.FranchiseTypeID=aft.FranchiseTypeID
		where		aft.AccountID = ids.ID
		and			fcft.CategoryID=@CategoryID
	)
end

set @localms=datediff(ms,@dt,getdate()); set @totalms=@totalms+@localms
print '2.1: ' + convert(varchar, @localms)
set @dt=getdate()


-- pull the review ids into memory

create table #rids (
	ID bigint not null
)

if (@IncludeUnfiltered = 1) begin
	insert #rids (ID)
	select	ID
	from	ixvReviewDetail ird with(nolock, noexpand)
	where exists (
		select	*
		from	#ids ids
		where	ids.ID = ird.AccountID
	)
end

set @localms=datediff(ms,@dt,getdate()); set @totalms=@totalms+@localms
print '3: ' + convert(varchar, @localms)
set @dt=getdate()

if (@IncludeFiltered = 1) begin
	insert #rids (ID)
	select	ID
	from	ixvReviewDetailF ird with(nolock, noexpand)
	where exists (
		select	*
		from	#ids ids
		where	ids.ID = ird.AccountID
	)
end

set @localms=datediff(ms,@dt,getdate()); set @totalms=@totalms+@localms
print '4: ' + convert(varchar, @localms)
set @dt=getdate()

--filter review sources
if (@ReviewSourceIDs is not null) begin
	delete	rids
	from	#rids rids
	where not exists (
		select		*
		from		(
			select		ird.ID
			from		ixvReviewDetail ird with(nolock, noexpand)
			inner join	dbo.fnList2IDTable(@ReviewSourceIDs, '|') sel on sel.id = ird.ReviewSourceID
			inner join	#ids ids on ids.id = ird.AccountID
			where		@IncludeUnFiltered = 1
			union
			select		ird.ID
			from		ixvReviewDetailF ird with(nolock, noexpand)
			inner join	dbo.fnList2IDTable(@ReviewSourceIDs, '|') sel on sel.id = ird.ReviewSourceID
			inner join	#ids ids on ids.id = ird.AccountID
			where		@IncludeFiltered = 1
		) ird
		where		ird.ID=rids.ID		
	)
end

set @localms=datediff(ms,@dt,getdate()); set @totalms=@totalms+@localms
print '5: ' + convert(varchar, @localms)
set @dt=getdate()

-- fitler by ratings

if (@RatingIsGreaterThanOrEqualTo is not null) begin
	delete	rids
	from	#rids rids
	where not exists (
		select		*
		from		(
			select		ird.ID
			from		ixvReviewDetail ird with(nolock, noexpand)			
			inner join	#ids ids on ids.id = ird.AccountID
			where		@IncludeUnFiltered = 1			
			and			Rating>=@RatingIsGreaterThanOrEqualTo

			union

			select		ird.ID
			from		ixvReviewDetailF ird with(nolock, noexpand)
			inner join	#ids ids on ids.id = ird.AccountID
			where		@IncludeFiltered = 1
			and			Rating>=@RatingIsGreaterThanOrEqualTo
		) ird
		where		ird.ID=rids.ID		
	)
end

set @localms=datediff(ms,@dt,getdate()); set @totalms=@totalms+@localms
print '6: ' + convert(varchar, @localms)
set @dt=getdate()

if (@RatingIsLessThanOrEqualTo is not null) begin
	delete	rids
	from	#rids rids
	where not exists (
		select		*
		from		(
			select		ird.ID
			from		ixvReviewDetail ird with(nolock, noexpand)			
			inner join	#ids ids on ids.id = ird.AccountID
			where		@IncludeUnFiltered = 1			
			and			Rating<=@RatingIsLessThanOrEqualTo

			union

			select		ird.ID
			from		ixvReviewDetailF ird with(nolock, noexpand)
			inner join	#ids ids on ids.id = ird.AccountID
			where		@IncludeFiltered = 1
			and			Rating<=@RatingIsLessThanOrEqualTo
		) ird
		where		ird.ID=rids.ID		
	)
end

set @localms=datediff(ms,@dt,getdate()); set @totalms=@totalms+@localms
print '7: ' + convert(varchar, @localms)
set @dt=getdate()


-- date filters

if (@MinReviewDate is not null) begin
	delete	rids
	from	#rids rids
	where not exists (
		select		*
		from		(
			select		ird.ID
			from		ixvReviewDetail ird with(nolock, noexpand)			
			inner join	#ids ids on ids.id = ird.AccountID
			where		@IncludeUnFiltered = 1			
			and			ReviewDate>=@MinReviewDate

			union

			select		ird.ID
			from		ixvReviewDetailF ird with(nolock, noexpand)
			inner join	#ids ids on ids.id = ird.AccountID
			where		@IncludeFiltered = 1
			and			ReviewDate>=@MinReviewDate
		) ird
		where		ird.ID=rids.ID		
	)
end

set @localms=datediff(ms,@dt,getdate()); set @totalms=@totalms+@localms
print '8: ' + convert(varchar, @localms)
set @dt=getdate()

if (@MaxReviewDate is not null) begin
	delete	rids
	from	#rids rids
	where not exists (
		select		*
		from		(
			select		ird.ID
			from		ixvReviewDetail ird with(nolock, noexpand)			
			inner join	#ids ids on ids.id = ird.AccountID
			where		@IncludeUnFiltered = 1			
			and			ReviewDate<=@MaxReviewDate

			union

			select		ird.ID
			from		ixvReviewDetailF ird with(nolock, noexpand)
			inner join	#ids ids on ids.id = ird.AccountID
			where		@IncludeFiltered = 1
			and			ReviewDate<=@MaxReviewDate
		) ird
		where		ird.ID=rids.ID		
	)
end

set @localms=datediff(ms,@dt,getdate()); set @totalms=@totalms+@localms
print '9: ' + convert(varchar, @localms)
set @dt=getdate()

-- text searches

if (@ReviewTextContains is not null) begin
	delete	rids
	from	#rids rids
	where not exists (
		select		*
		from		#rids rids2
		inner join	Reviews r with(nolock) on r.ID=rids2.ID	
		where		r.ID=rids.ID
		and			r.ReviewText like ('%' + @ReviewTextContains + '%')
	)
end

set @localms=datediff(ms,@dt,getdate()); set @totalms=@totalms+@localms
print '10: ' + convert(varchar, @localms)
set @dt=getdate()

if (@ReviewTextDoesNotContain is not null) begin
	delete	rids
	from	#rids rids
	where exists (
		select		*
		from		#rids rids2
		inner join	Reviews r with(nolock) on r.ID=rids2.ID	
		where		r.ID=rids.ID
		and			r.ReviewText like ('%' + @ReviewTextDoesNotContain + '%')
	)
end

set @localms=datediff(ms,@dt,getdate()); set @totalms=@totalms+@localms
print '11: ' + convert(varchar, @localms)
set @dt=getdate()

/* 
process the sorts
*/

create table #t1 (
	RowNum int not null,
	ID	bigint not null
)

if @SortBy = 1 begin

	insert	#t1 (ID,RowNum)
		select			ird.ID, ROW_NUMBER() OVER (order by ird.ReviewDate asc)
		from
		(
			select		ird.ID, ird.ReviewDate
			from		ixvReviewDetail ird with(nolock, noexpand)			
			inner join	#rids ids on ids.id = ird.ID
			where		@IncludeUnFiltered = 1

			union

			select		ird.ID, ird.ReviewDate
			from		ixvReviewDetailF ird with(nolock, noexpand)
			inner join	#rids ids on ids.id = ird.ID
			where		@IncludeFiltered = 1
		) ird

end else if @SortBy = 2 begin

	insert	#t1 (ID,RowNum)
	select		rids.ID, ROW_NUMBER() OVER (order by r.DateEntered asc)
	from		#rids rids
	inner join	Reviews r with(nolock) on rids.ID=r.ID	

end else if @SortBy = 3 begin

	insert	#t1 (ID,RowNum)
	select		rids.ID, ROW_NUMBER() OVER (order by coalesce(a.DisplayName,a.[Name]) asc)
	from		#rids rids
	inner join	Reviews r with(nolock) on rids.ID=r.ID	
	inner join	Accounts a with(nolock) on r.AccountID=a.ID

end else if @SortBy = 4 begin

	insert	#t1 (ID,RowNum)
	select			ird.ID, ROW_NUMBER() OVER (order by ird.Name asc)
	from
	(
		select		ird.ID, rs.Name
		from		ixvReviewDetail ird with(nolock, noexpand)			
		inner join	#rids ids on ids.id = ird.ID
		inner join	ReviewSources rs with(nolock) on ird.ReviewSourceID=rs.ID
		where		@IncludeUnFiltered = 1

		union

		select		ird.ID, rs.Name
		from		ixvReviewDetailF ird with(nolock, noexpand)
		inner join	#rids ids on ids.id = ird.ID
		inner join	ReviewSources rs with(nolock) on ird.ReviewSourceID=rs.ID
		where		@IncludeFiltered = 1
	) ird

end else if @SortBy = 5 begin

	insert	#t1 (ID,RowNum)
	select			ird.ID, ROW_NUMBER() OVER (order by ird.Rating asc)
	from
	(
		select		ird.ID, ird.Rating
		from		ixvReviewDetail ird with(nolock, noexpand)			
		inner join	#rids ids on ids.id = ird.ID
		where		@IncludeUnFiltered = 1

		union

		select		ird.ID, ird.Rating
		from		ixvReviewDetailF ird with(nolock, noexpand)
		inner join	#rids ids on ids.id = ird.ID
		where		@IncludeFiltered = 1
	) ird

end


set @localms=datediff(ms,@dt,getdate()); set @totalms=@totalms+@localms
print '11: ' + convert(varchar, @localms)
set @dt=getdate()

create index t1_temp on #t1(rownum)

set @localms=datediff(ms,@dt,getdate()); set @totalms=@totalms+@localms
print '12: ' + convert(varchar, @localms)
set @dt=getdate()

if @SortAscending = 0 begin
	declare @maxrownum int
	select @maxrownum = max(rownum) from #t1
	update #t1 set rownum=@maxrownum-rownum
end

set @localms=datediff(ms,@dt,getdate()); set @totalms=@totalms+@localms
print '13: ' + convert(varchar, @localms)
set @dt=getdate()

-- total records
select count(*) Total from #t1


-- final projection
select		t.RowNum, t.ID ReviewID, r.AccountID, coalesce(a.DisplayName,a.[Name]) AccountName,
			c.[Name] City, s.Abbreviation StateAbbrev, s.ID StateID, z.ZipCode,
			r.ReviewSourceID, rs.[Name] ReviewSourceName, rs.ImagePath,
			r.Rating, r.ReviewerName, r.ReviewDate, r.ReviewText, r.ReviewURL, r.ExtraInfo, r.Filtered, r.DateEntered,
			ar.URL SiteURL
from		#t1 t
inner join	Reviews r with(nolock) on t.ID=r.ID
inner join	ReviewSources rs with(nolock) on r.ReviewSourceID=rs.ID
inner join	Accounts_ReviewSources ar with(nolock) on ar.AccountID=r.AccountID and ar.ReviewSourceID=r.ReviewSourceID
inner join	Accounts a with(nolock) on r.AccountID=a.ID
left join	Addresses ad with(nolock) on a.BillingAddressID = ad.ID
left join	Cities c with(nolock) on ad.CityID = c.ID
left join	States s with(nolock) on c.StateID = s.ID
left join	ZipCodes z with(nolock) on c.ZipCodeID = z.ID
where		t.RowNum between @StartIndex and @EndIndex
order by	t.RowNum


set @localms=datediff(ms,@dt,getdate()); set @totalms=@totalms+@localms
print '14: ' + convert(varchar, @localms)
set @dt=getdate()


/*		
EXEC spGetReviewStreamData2 @AccountIDs='220633|220635|220636|220637|220638|220639|220640|220641|220642|220643|220644|220645|220646|220647|220648|220649|220650|220651|220652|220653|220654|220655|220656|220657|220658|220659|220660|220661|220662|220663|220664|220665|220666|220667|220668|220669|220670|220671|220672|220673|220674|220675|220676|220677|220678|220679|220680|220681|220682|220683|220684|220685|220686|220687|220688|220689|220690|220691|220692|220693|220694|220695|220696|220697|220698|220699|220700|220701|220702|220703|220704|220705|220706|220707|220708|220709|220710|220711|220712|220713|220714|220715|220716|220717|220718|220719|220720|220721|220722|220723|220724|220725|220726|220727|220728|220729|220730|220731|220732|220739|220740|220741|220742|220743|220744',
							@IncludeFiltered=0,
							@IncludeUnFiltered=1,
							@SortBy = 5,
							@SortAscending = 0,
							@StartIndex = 1,
							@EndIndex = 50,
							@ReviewSourceIDs = '100|200|300|400|500|600|700|800|900',
							@RatingIsGreaterThanOrEqualTo = 2.5,
							@RatingIsLessThanOrEqualTo = 4.9,
							@MinReviewDate = '10/1/2013',
							@MaxReviewDate = '1/2/2014'      ,
							--@ReviewTextContains = 'brick',
							--@ReviewTextDoesNotContain = 'and',
		
*/




drop table #t1
drop table #rids
drop table #ids

print 'Total Execution Time: ' + convert(varchar, @totalms)


END