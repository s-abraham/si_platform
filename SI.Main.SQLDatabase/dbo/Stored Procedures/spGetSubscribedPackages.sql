﻿CREATE procedure spGetSubscribedPackages

	@accountID bigint

as

set nocount on


select		s.AccountID, s.ID SubscriptionID, rp2.ResellerID ResellerID, rp2.PackageID PackageID, s.DateCreated DateAdded,
			a.Name ResellerName, p.Name PackageName
from		Subscriptions s
inner join	Resellers_Packages rp2 on s.Reseller_PackageID=rp2.ID
inner join	Packages p on rp2.PackageID=p.ID
inner join	Resellers r on rp2.ResellerID=r.ID
inner join	Accounts a on r.AccountID=a.ID
where	coalesce(s.StartDate,'1/1/2012')<getdate()
and		coalesce(s.EndDate,'1/1/2050')>getdate()
and		s.AccountID=@accountID
and		s.Active=1