﻿CREATE procedure spUpsertFacebookComment

	--identity
	@FromID	varchar(100),
	@FromName varchar(250),
	@PictureURL varchar(250),

	--comment
	@FacebookPostID bigint,
	@FBCommentID varchar(250),
	@Message nvarchar(max),
	@FBCreateTime datetime,
	@UserLikes bit,
	@LikeCount int

as

set nocount on


declare @FacebookIdentityID bigint
exec spUpsertFacebookIdentity @FromID=@FromID, @FromName=@FromName, @PictureURL=@PictureURL, @FacebookIdentityID=@FacebookIdentityID

declare @FacebookCommentID bigint
select	@FacebookCommentID = fids.ID
from	FacebookComments fids with(nolock)
where	fids.FBCommentID=@FBCommentID

if (@FacebookCommentID>0) begin
	update	FacebookComments
	set		Message=@Message, LikeCount=@LikeCount, UserLikes=@UserLikes
	where	ID=@FacebookCommentID
end else begin
	insert	FacebookComments (FacebookPostsID, FacebookIdentitiesID, FBCommentID, Message, FBCreateTime, UserLikes, LikeCount, DateCreated)
	values	(@FacebookPostID, @FacebookIdentityID, @FBCommentID, @Message, @FBCreateTime, @UserLikes, @LikeCount, getdate())
	set @FacebookCommentID = SCOPE_IDENTITY()
end

select @FacebookCommentID