﻿/*
exec spGetReviewDownloaderWorklist @AccountID=201527, @ReviewSourceIDs='100,200'
*/

CREATE procedure [dbo].[spGetAccountReviewURL]	
	@ReviewSourceIDs	varchar(max) = null,
	@AccountID			bigint = null
as

set nocount on

if @ReviewSourceIDs = '' set @ReviewSourceIDs = null
if @AccountID=0 set @AccountID=null

create table #rsids (id bigint not null)

if @ReviewSourceIDs is null begin
	insert #rsids (id)
	select id from ReviewSources
end else begin
	insert #rsids (id)
	select id from dbo.fnList2IDTable(@ReviewSourceIDs,',')
end

select		ar.ID Account_ReviewSourceID, ar.ReviewSourceID, ar.AccountID, ar.URL
from		Accounts_ReviewSources ar with(nolock)
where		ar.IsValid=1
and			ar.AccountID=coalesce(@AccountID,ar.AccountID)
and	exists (
	select	*
	from	#rsids rsids
	where	rsids.id = ar.ReviewSourceID
)


drop table #rsids