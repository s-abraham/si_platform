﻿CREATE procedure [dbo].[spUpsertFacebookPostLike]

	--identity
	@FromID	varchar(100),
	@FromName varchar(250),
	@PictureURL varchar(250),
	
	--post to like	
	@FacebookPostID bigint

as

set nocount on

declare @FacebookIdentityID bigint
exec spUpsertFacebookIdentity @FromID=@FromID, @FromName=@FromName, @PictureURL=@PictureURL, @FacebookIdentityID=@FacebookIdentityID OUTPUT


declare @FacebookLikeID bigint
select	@FacebookLikeID = ID
from	FacebookLikes with(nolock)
where	FacebookIdentitiesID = @FacebookIdentityID
and		FacebookPostsID = @FacebookPostID

if (coalesce(@FacebookLikeID,0)=0) begin
	insert FacebookLikes (FacebookPostsID, FacebookCommentsID, FacebookIdentitiesID)
	values (@FacebookPostID, null, @FacebookIdentityID)
end