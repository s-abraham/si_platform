﻿CREATE procedure spGetReviewDataForAccountAndSource

@AccountID bigint,
@ReviewSourceID bigint

as

set nocount on

select	*
from	ReviewSummaries with(nolock)
where	AccountID=@AccountID
and		ReviewSourceID=@ReviewSourceID

select	*
from	Reviews with(nolock)
where	AccountID=@AccountID
and		ReviewSourceID=@ReviewSourceID