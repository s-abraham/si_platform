﻿CREATE PROCEDURE spUpdateNotificationTarget	
	@NotificationTargetID bigint =NULL,
	@DateSent DATETIME = NULL,
	@NotificationStatusID bigint = NULL

AS
	UPDATE	NotificationTargets
	SET		DateSent = @DateSent,
			NotificationStatusID = @NotificationStatusID
	WHERE	ID = @NotificationTargetID