﻿CREATE procedure spUpsertFacebookIdentity
	@FromID	varchar(100),
	@FromName varchar(250),
	@PictureURL varchar(250),

	@FacebookIdentityID bigint OUTPUT
as

set nocount on

select	@FacebookIdentityID = fids.ID
from	FacebookIdentities fids with(nolock)
where	fids.FacebookID=@FromID

if (@FacebookIdentityID>0) begin
	update	FacebookIdentities
	set		Name=@FromName, PictureURL=@PictureURL
	where	ID=@FacebookIdentityID
end else begin
	insert	FacebookIdentities (Name, FacebookID, PictureURL)
	values	(@FromID, @FromName, @PictureURL)
	set @FacebookIdentityID = SCOPE_IDENTITY()
end