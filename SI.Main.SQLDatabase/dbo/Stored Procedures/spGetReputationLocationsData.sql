﻿/*
	EXEC spGetReputationLocationsData '200049|206585|218308|218727|208072'
*/
CREATE PROCEDURE [dbo].[spGetReputationLocationsData]
	@AccountIDs NVARCHAR(MAX) = ''	
AS
BEGIN
	CREATE TABLE #tblAccount (AccountID BIGINT NULL)

	INSERT INTO #tblAccount(AccountID) 
	SELECT * FROM dbo.fnList2IDTable(@AccountIDs, '|')				

	--select AccountID from #tblAccount

	--DECLARE @tblReviewDetail1 TABLE (	AccountID INT NOT NULL,
	--									DisplayName VARCHAR(50) NOT NULL,
	--									[Address] VARCHAR(500) NOT NULL,
	--									CityName VARCHAR(50)  NOT NULL,
	--									StateAbbreviation VARCHAR(5)  NOT NULL,
	--									StateID INT  NOT NULL,
	--									Zipcode VARCHAR(10)  NOT NULL,
	--									ReviewCount INT NOT NULL,
	--									AverageRating FLOAt NOT NULL,
	--									PositiveReviews INT NOT NULL,
	--									NegativeReviews INT NOT NULL,									
	--									DateUpdated DATETIME NULL,
	--									ToDoPositive INT NOT NULL,
	--									ToDoNegative INT NOT NULL)
	
	

	
	SELECT	tbl.AccountID AS 'AccountID',
			a.DisplayName AS 'DisplayName',
			ba.Street1 AS 'Street1',
			cit.Name AS 'CityName',
			sta.Abbreviation AS 'StateAbbreviation',
			sta.ID AS 'StateID',
			zip.ZipCode AS 'ZipCode',
			ISNULL(SUM(rs.SalesReviewCount),0)  AS 'ReviewCount',
			ISNULL(ROUND(AVG(rs.SalesAverageRating),1),0)  AS 'AverageRating',
			ISNULL(SUM(rs.TotalPositiveReviews),0)  AS 'PositiveReviews',
			ISNULL(SUM(rs.TotalNegativeReviews),0)  AS 'NegativeReviews',					
			MAX(rs.DateEntered) AS 'DateUpdated',
			0 AS 'ToDoPositive',
			0 AS 'ToDoNegative'			 			 	 
	FROM #tblAccount tbl
		LEFT OUTER JOIN Accounts a
			ON tbl.AccountID = a.ID
		LEFT OUTER JOIN Addresses ba
			on a.BillingAddressID = ba.ID
		LEFT OUTER JOIN Cities cit
			on ba.CityID = cit.ID
		LEFT OUTER JOIN States sta
			on cit.StateID = sta.ID
		LEFT OUTER JOIN Countries cou
			on sta.CountryID = cou.ID
		LEFT OUTER JOIN ZipCodes zip
			on cit.ZipCodeID = zip.ID
		LEFT OUTER JOIN Phones p
			on a.BillingPhoneID = p.ID
		LEFT OUTER JOIN dbo.ReviewSummaries rs
			ON tbl.AccountID = rs.AccountID
				and rs.SummaryDate = (SELECT MAX(rs1.SummaryDate) from ReviewSummaries rs1 where rs1.AccountID = rs.AccountID and rs1.ReviewSourceID = rs.ReviewSourceID)
					and rs.SalesAverageRating > 0			
	GROUP BY tbl.AccountID,a.DisplayName,ba.Street1,cit.Name,sta.Abbreviation,sta.ID,zip.ZipCode
	order by ISNULL(ROUND(AVG(rs.SalesAverageRating),1),0) DESC
	
	
	--select @TotalCount = count(*) from	@tblReviewDetail1

	--select DisplayName from @tblReviewDetail1

	DROP TABLE #tblAccount	
END