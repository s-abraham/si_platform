﻿/*
	EXEC [spGetAvailablePackagesForAccount] 197922
*/
CREATE procedure [dbo].[spGetAvailablePackagesForAccount]

@accountID bigint

as

set nocount on

select	rp.PackageID, rp.ResellerID, p.Name PackageName, a.Name ResellerName,rp.ID AS Resellers_PackagesID,rp.DateCreated
from		Resellers_Packages rp
inner join	Packages p on rp.PackageID=p.ID
inner join	Resellers r on rp.ResellerID=r.ID
inner join	Accounts a on r.AccountID=a.ID
where not exists (
	select	*
	from		Subscriptions s
	inner join	Resellers_Packages rp2 on s.Reseller_PackageID=rp.ID
	where	coalesce(s.StartDate,'1/1/2012')<getdate()
	and		coalesce(s.EndDate,'1/1/2050')>getdate()
	and		rp2.PackageID=p.ID
	and		s.AccountID=@accountID
	and		s.Active=1
)