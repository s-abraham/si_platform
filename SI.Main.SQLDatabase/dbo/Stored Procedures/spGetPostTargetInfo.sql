﻿/*

select * from posttargets
select * from postimages

exec spGetPostTargetInfo @PostTargetID=1
exec spGetPostTargetInfo @PostTargetID=1, @PostImageID=1
*/
CREATE procedure [dbo].[spGetPostTargetInfo]

@PostTargetID bigint,
@PostImageID bigint = null

as

set nocount on

declare @revid bigint

select		top 1 @revid=pr.ID
from		PostRevisions pr with(nolock)
where		pr.PostTargetID=@PostTargetID
order by	pr.DateCreated desc

if @revid is null 
	select		top 1 @revid=pr.ID
	from		PostRevisions pr with(nolock)
	inner join	PostTargets pt with(nolock) on pt.ID=@PostTargetID
	where		pr.PostID=pt.PostID
	order by	pr.DateCreated desc
	
if (@PostImageID is not null) begin

select		pt.ID PostTargetID, pt.PostID, r.[Message], r.Link, r.ImageURL RevisionImageURL,
			i.URL ImageURL, i.ID PostImageID, p.PostTypeID,
			c.UniqueID, c.Token, c.TokenSecret, sa.AppID, sa.AppSecret, c.ID CredentialID,
			r.LinkDescription, r.LinkTitle, p.Title, 
			pir.ResultID ImageResultID, pir.FeedID ImageFeedID,
			pt.ResultID, pt.FeedID, ph.Number  PhoneNumber, a.WebsiteURL, a.TimeZoneID,
			coalesce(a.DisplayName, a.Name) AccountName, pt.ScheduleDate, pir.ProcessedDate PublishedDate,
			p.UserID

from		PostTargets pt with(nolock)
inner join	Posts p with(nolock) on p.ID=pt.PostID
inner join	PostRevisions r with(nolock) on pt.PostID = r.PostID
inner join	PostImages i with(nolock) on i.PostID = pt.PostID
inner join	[Credentials] c with(nolock) on pt.CredentialID = c.ID
inner join	Accounts a with(nolock) on a.ID = c.AccountID
left join	Addresses addr with(nolock) on a.BillingAddressID=addr.ID
left join	Phones ph with(nolock) on a.BillingPhoneID=ph.ID
inner join	SocialApps sa with(nolock) on c.SocialAppID = sa.ID
left join	PostImageResults pir with(nolock) on pir.PostImageID=i.ID and pir.PostTargetID=@PostTargetID
where		pt.ID = @PostTargetID
and			r.ID = @revid
and			i.ID=@PostImageID

end else begin

select		pt.ID PostTargetID, pt.PostID, r.[Message], r.Link, r.ImageURL RevisionImageURL,
			cast ('' as varchar(300)) ImageURL, cast (0 as bigint) PostImageID,
			c.UniqueID, c.Token, c.TokenSecret, sa.AppID, sa.AppSecret, c.ID CredentialID,
			r.LinkDescription, r.LinkTitle, p.Title, p.PostTypeID,
			cast(null as varchar(250)) ImageResultID, cast(null as varchar(250)) ImageFeedID,
			pt.ResultID, pt.FeedID, ph.Number PhoneNumber, a.WebsiteURL,
			coalesce(a.DisplayName, a.Name) AccountName, a.TimeZoneID, pt.ScheduleDate, pt.PublishedDate,
			p.UserID

from		PostTargets pt with(nolock)
inner join	Posts p with(nolock) on p.ID=pt.PostID
inner join	PostRevisions r with(nolock) on pt.PostID=r.PostID
inner join	[Credentials] c with(nolock) on pt.CredentialID = c.ID
inner join	Accounts a with(nolock) on a.ID = c.AccountID
left join	Addresses addr with(nolock) on a.BillingAddressID=addr.ID
left join	Phones ph with(nolock) on a.BillingPhoneID=ph.ID
inner join	SocialApps sa with(nolock) on c.SocialAppID = sa.ID
where		pt.ID = @PostTargetID
and			r.ID = @revid


end