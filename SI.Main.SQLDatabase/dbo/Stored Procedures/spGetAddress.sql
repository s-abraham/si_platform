﻿
--exec spGetAddress 3
CREATE procedure [dbo].[spGetAddress]

	@AddressID bigint

as 

set nocount on

select		a.ID, a.Street1, a.Street2, a.Street3, a.CityID, 
			ci.Name CityName, ci.StateID,
			s.Name StateName, s.Abbreviation StateAbbreviation,
			z.ZipCode, co.Name CountryName, co.Prefix CountryPrefix,
			coalesce(a.Latitude,z.Latitude) Latitude,
			coalesce(a.Longitude,z.Longitude) Longitude,
			coalesce(l3.ID,l2.ID,l1.ID) LanguageID,
			coalesce(l3.Name,l2.Name,l1.Name) LanguageName,
			coalesce(l3.CultureCode,l2.CultureCode,l1.CultureCode) LanguageCultureCode
			

from		Addresses a with(nolock)

inner join	Cities ci with(nolock) on a.CityID=ci.ID
left join	Languages l3 with(nolock) on ci.LanguageID=l3.ID
inner join	States s with(nolock) on ci.StateID=s.ID
left join	Languages l2 with(nolock) on s.LanguageID=l2.ID
inner join	Countries co with(nolock) on s.CountryID=co.ID
left join	Languages l1 with(nolock) on co.LanguageID=l1.ID

left join	ZipCodes z with(nolock) on ci.ZipCodeID=z.ID

where a.ID=@AddressID