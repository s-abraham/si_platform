﻿
/*
select * from resellers
select * from users
spCreateAccessToken '71C0C2FA-4ED6-4335-80C8-D8CD372F535E', '08F5EB7A-07B4-4DE6-9EC7-1F81F6EFE7D5', 1
*/
CREATE procedure [dbo].[spCreateAccessToken]
	@ResellerSecretKey uniqueidentifier,
	@UserSecretKey uniqueidentifier,
	@MinutesTillExpiration int
as

set nocount on

	declare @expiryDate datetime
	set @expiryDate=dateadd(minute, @MinutesTillExpiration, getdate())

	insert	SSOAccessTokens (ResellerSecretKey,UserSecretKey,DateCreated,DateExpires)
	select	r.SSOSecretKey, u.SSOSecretKey, getdate(), @expiryDate
	from		Resellers r with(nolock)
	cross join	Users u with(nolock)
	where		u.SSOSecretKey=@UserSecretKey
	and			r.SSOSecretKey=@ResellerSecretKey

	select	AccessToken
	from	SSOAccessTokens with(nolock)
	where	DateExpires=@expiryDate
	and		ResellerSecretKey=@ResellerSecretKey
	and		UserSecretKey=@UserSecretKey