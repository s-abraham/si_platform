﻿CREATE PROCEDURE spUpdateNotififcationRecipient
	@NotififcationRecipientIDs varchar(MAX) = '',
	@NotificationTargetID bigint = 0
AS

	UPDATE	NotificationRecipients
	SET		NotificationTargetID = @NotificationTargetID
	WHERE	ID IN (select id FROM dbo.fnList2IDTable(@NotififcationRecipientIDs, '|'))