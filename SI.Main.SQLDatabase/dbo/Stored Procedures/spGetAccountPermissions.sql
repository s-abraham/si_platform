﻿
/*

Returns a list of account IDs where the given user has the given permission type id.
If the permission type id is null, all permissions for that user are given for all accounts.

--select * from users

exec spGetAccountPermissions @UserID=41999
exec spGetAccountPermissions @AccountID=201526
*/

CREATE procedure [dbo].[spGetAccountPermissions]
	@UserID bigint = null,
	@AccountID bigint = null,
	@PermissionTypeID bigint = null
as

set nocount on

;with cte as (
	select		a.ID, a.ParentAccountID, p.ID PermissionID, p.PermissionTypeID, a.ID InheritedFromAccountID, au.UserID
	from		Accounts a with(nolock)
	inner join	Accounts_Users au with(nolock) on a.ID=au.AccountID
	inner join	Permissions p with(nolock) on p.Account_UserID=au.ID
	where p.IncludeChildren=1	
	and au.UserID=coalesce(@userid,au.UserID)	
	and p.PermissionTypeID = coalesce(@PermissionTypeID, p.PermissionTypeID)

	union all

	select		a.Id, a.ParentAccountID, P.PermissionID PermissionID, P.PermissionTypeID, P.InheritedFromAccountID, P.UserID
	from		Accounts as a with(nolock)
	inner join	cte as P on a.ParentAccountID = P.Id
	and P.PermissionTypeID = coalesce(@PermissionTypeID, P.PermissionTypeID)
)
select *
into #t2
from cte


select		p.ID PermissionID, au1.AccountID AccountID, p.PermissionTypeID, convert(bigint,null) InheritedFromAccountID, au1.UserID
from		Accounts_Users au1 with(nolock)
inner join	Permissions p with(nolock)	on	p.IncludeChildren = 0
										and	p.Account_UserID = au1.ID
where		au1.UserID=coalesce(@userid,au1.UserID)
and			p.PermissionTypeID = coalesce(@PermissionTypeID, p.PermissionTypeID)
and			au1.AccountID=coalesce(@accountID,au1.AccountID)

union all

select		t.PermissionID PermissionID, t.ID AccountID, t.PermissionTypeID, t.InheritedFromAccountID InheritedFromAccountID, t.UserID
from		#t2 t with(nolock)
where		t.ID=coalesce(@AccountID,t.ID)



drop table #t2