﻿
--select * from accounts
--exec [spGetAccount] 53806

CREATE procedure [dbo].[spGetAccount]
	@accountID bigint
as

set nocount on

declare @depth int;

with cte as (
    select		a.ID, a.ParentAccountID, convert(int,0) Level
	from		Accounts a with(nolock)
	where		a.ParentAccountID is null

    union all

    select		a.Id, a.ParentAccountID, P.Level+1 as Level
    from		Accounts as a with(nolock)
    inner join	cte as P on a.ParentAccountID = P.Id
)
select @depth = Level
from cte
where ID = @accountID


--get the account

select	a.*,
		ba.Street1, ba.Street2, ba.Street3, 
		ba.CityID, cit.Name CityName, zip.ZipCode, 
		cit.StateID, sta.Name StateName, 
		sta.Abbreviation StateAbbreviation, sta.CountryID, cou.Name CountryName,
		p.Number Phone, coalesce(parent.DisplayName,parent.Name) ParentAccountName,
		@depth AccountDepth, a.EdmundsDealershipID, a.OriginSystemIdentifier
from		Accounts a with(nolock)
left join	Accounts parent with(nolock) on a.ParentAccountID = parent.ID
left join	Addresses ba with(nolock) on a.BillingAddressID = ba.ID
left join	Cities cit with(nolock) on ba.CityID = cit.ID
left join	States sta with(nolock) on cit.StateID = sta.ID
left join	Countries cou with(nolock) on sta.CountryID = cou.ID
left join	ZipCodes zip with(nolock) on cit.ZipCodeID = zip.ID
left join	Phones p with(nolock) on a.BillingPhoneID = p.ID

where		a.ID = @accountID


--get the franchise types for this account

select  ft.*
from		FranchiseTypes ft with(nolock)
where exists (
	select		*
	from		Accounts_FranchiseTypes af with(nolock)
	where		af.AccountID = @accountID
	and			af.FranchiseTypeID = ft.ID
)