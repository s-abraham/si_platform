﻿/*
spGetNotificationsToSend 2
*/

CREATE procedure spGetNotificationsToSend

	@NotificationMessageTypeID bigint

as

set nocount on

select		nr.ID NotificationRecipientID, nr.UserID, nr.NotificationTemplateID, nt.NotificationMessageTypeID,
			u.EmailAddress, n.EntityID, n.AccountID, nt.NotificationMethodID
into		#t
from		NotificationRecipients nr with(nolock)
inner join	Users u with(nolock) on nr.UserID = u.ID
inner join	NotificationTemplates nt with(nolock) on nt.ID = nr.NotificationTemplateID
inner join	Notifications n with(nolock) on n.ID = nr.NotificationID
where		nr.NotificationTargetID is null
and			nt.NotificationMessageTypeID = @NotificationMessageTypeID

select		nt.*
from		NotificationTemplates nt with(nolock)
where exists (
	select	*
	from	#t t
	where	t.NotificationTemplateID=nt.ID
)

if (@NotificationMessageTypeID = 2) begin
	select	r.*
	from	Reviews r with(nolock)
	where exists (
		select	*
		from	#t t
		where	t.EntityID=r.ID
	)
end

select * from #t



drop table #t