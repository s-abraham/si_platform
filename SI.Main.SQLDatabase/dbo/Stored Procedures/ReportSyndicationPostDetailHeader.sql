﻿
CREATE PROCEDURE dbo.ReportSyndicationPostDetailHeader 
	@PostID bigint
AS
BEGIN
	SET NOCOUNT ON 

	declare @dummy TABLE(
		PostID bigint,
		PostDate datetime,
		[Type] varchar(50),
		ImageUrl varchar(250),
		[Message] varchar(500),
		Submitted int,
		AutoApproved int,
		Approved int,
		Failed int,
		Success int
		
	)

	insert @dummy values (1, getdate(), 'Image', '', 'test message 1', 11,12,13,14,15)
	insert @dummy values (2, getdate(), 'Image', '', 'test message 2', 11,12,13,14,15)		
	insert @dummy values (3, getdate(), 'Status', '', 'test message 3 test message 3 test message 3 test message 3 test message 3 test message 3 test message 3 test message 3 test message 3 test message 3 test message 3 test message 3 test message 3 test message 3 test message 3 test message 3 test message 3 test message 3 END', 11,12,13,14,15)
	insert @dummy values (4, getdate(), 'Image', '', 'test message 4', 11,12,13,14,15)
	insert @dummy values (5, getdate(), 'Image', '', 'test message 5', 11,12,13,14,15)
	insert @dummy values (6, getdate(), 'Image', '', 'test message 6', 11,12,13,14,15)		
	insert @dummy values (7, getdate(), 'Status', '', 'test message 7 test message 3 test message 3 test message 3 test message 3 test message 3 test message 3 test message 3 test message 3 test message 3 test message 3 test message 3 test message 3 test message 3 test message 3 test message 3 test message 3 test message 3 END', 11,12,13,14,15)
	insert @dummy values (8, getdate(), 'Image', '', 'test message 8', 11,12,13,14,15)

	select * from @dummy where PostID = @PostID
	
END