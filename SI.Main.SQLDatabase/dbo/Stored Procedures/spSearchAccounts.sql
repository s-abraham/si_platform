﻿/*

Performs an account search, returning only the accounts that match the criteria and
have the given permission type id for the given user

select * from users
update users set superadmin=1 where id=14

exec spSearchAccounts 
	@SearchStringParts = 'autost',
	@FranchiseTypeIDs = null,
	@PackageIDs = null,
	@AccountTypeIDs = null,
	@HasChildAccounts = 0,
	@CallingUserID = 1,
	@PermissionTypeID = 13,
	--@ParentAccountID = 197922,
	
	@SortBy = 7,
	@SortAscending = 0,
	@StartIndex = 1,
	@EndIndex = 25

*/

CREATE procedure [dbo].[spSearchAccounts]

	@SearchStringParts nvarchar(1000) = null,		-- pipe delimited list of search strings.  all must match for a result to be valid
	@FranchiseTypeIDs nvarchar(100) = null,			-- a pipe delimited list of franchise type ids, any of which can match for a result to be valid
	@PackageIDs nvarchar(100) = null,				-- a pipe delimited list of package ids, any of which can match for a result to be valid
	@AccountTypeIDs nvarchar(100) = null,			-- a pipe delimited list of account type ids, any of which can match for a result to be valid
	@HasChildAccounts bit,							-- if true, only those accounts that have a child account will be returned
	@CallingUserID bigint,							-- the user that initiated the search
	@PermissionTypeID bigint,						-- the permisison type the initiating user must have for an account to be returned
	@ParentAccountID bigint = null,					-- the accounts returned must have this parent account id
	@NotParentAccountID bigint = null,				-- the accounts returned must NOT have this parent account id	
	@OriginSystemIdentifier varchar(100) = null,	-- the id from the system that was the origin of this record, null if entered by SI personnel
	@SortBy int,
	@SortAscending bit,
	@StartIndex int,	
	@EndIndex int
as

set nocount on

if @ParentAccountID = 0 set @ParentAccountID = null
if @NotParentAccountID = 0 set @NotParentAccountID = null
if @OriginSystemIdentifier = '' set @OriginSystemIdentifier = null

declare @isSuper bit
select @isSuper = SuperAdmin from Users with(nolock) where ID = @CallingUserID

create table #accountIDs (ID bigint not null)

if (@isSuper = 1) begin --we start with all accounts if a user is a super admin

	insert #accountIDs (ID) select ID from Accounts with(nolock)

end else begin --restrict accounts by permission otherwise
	
	create table #tPerms (PermissionID bigint not null, AccountID bigint not null, PermissionTypeID bigint not null, InheritedFromAccountID bigint null, UserID bigint not null)
	
	insert #tPerms
	exec spGetAccountPermissions @UserID=@CallingUserID, @PermissionTypeID=@PermissionTypeID

	insert	#accountIDs(ID)
	select	distinct AccountID
	from	#tPerms

	drop table #tPerms

end

if (@OriginSystemIdentifier is not null) begin
	delete	a
	from	#accountIDs a
	where not exists (
		select	*
		from	Accounts a2 with(nolock)		
		where	a.ID = a2.id
		and		a2.OriginSystemIdentifier = @OriginSystemIdentifier
	)
end

if (@ParentAccountID is not null) begin
	delete	a
	from	#accountIDs a
	where not exists (
		select	*
		from	Accounts a2 with(nolock)		
		where	a.ID = a2.id
		and		a2.ParentAccountID = @ParentAccountID		
	)
end

if (@NotParentAccountID is not null) begin
	delete	a
	from	#accountIDs a
	where exists (
		select	*
		from	Accounts a2 with(nolock)		
		where	a.ID = a2.id
		and		a2.ParentAccountID = @NotParentAccountID		
	)
end

if @FranchiseTypeIDs is not null and @FranchiseTypeIDs <> '' begin
	
	delete	a
	from	#accountIDs a
	where not exists (
		select	*
		from		Accounts_FranchiseTypes aft with(nolock)
		inner join	dbo.fnList2IDTable(@FranchiseTypeIDs, '|') l on l.ID = aft.FranchiseTypeID
		where	a.ID = aft.AccountID		
	)

end

if @PackageIDs is not null and @PackageIDs <> '' begin
	
	delete	a
	from	#accountIDs a
	where not exists (
		select	*
		from		Subscriptions sub with(nolock)
		inner join	Resellers_Packages rp with(nolock) on sub.Reseller_PackageID=rp.ID
		inner join	dbo.fnList2IDTable(@PackageIDs, '|') l on l.ID = rp.PackageID
		where	a.ID = sub.AccountID
		and		sub.Active = 1
		and		coalesce(sub.StartDate,'1/1/2012')<getdate()
		and		coalesce(sub.EndDate,'1/1/2050')>getdate()
	)

end

if @AccountTypeIDs is not null and @AccountTypeIDs <> '' begin
	
	delete	a
	from	#accountIDs a
	where not exists (
		select	*
		from		Accounts ac with(nolock)
		inner join	dbo.fnList2IDTable(@AccountTypeIDs, '|') l on l.ID = ac.AccountTypeID
		where	a.ID = ac.ID		
	)

end

if @HasChildAccounts = 1 begin
	
	delete	a
	from	#accountIDs a
	where not exists (
		select	*
		from	Accounts a2 with(nolock)
		where	a2.ParentAccountID = a.ID
	)

end

if @SearchStringParts is not null and @SearchStringParts <> '' begin
	
	--select * from 
	create table #stable (value nvarchar(100))

	insert	#stable(value)
	select	value 
	from	dbo.fnList2StringTable(@SearchStringParts, '|')

	declare @searchString varchar(100)

	declare st cursor fast_forward for select value from #stable
	open st
	fetch next from st into @searchString
	while @@FETCH_STATUS = 0 begin	

		delete	aid
		from	#accountIDs aid
		where not exists (
			select	*
			from		Accounts a with(nolock)
			left join	Addresses ad1 with(nolock) on a.BillingAddressID = ad1.ID
			left join	Cities c1 with(nolock) on ad1.CityID = c1.ID
			left join	ZipCodes zip1 with(nolock) on c1.ZipCodeID = zip1.ID
			left join	States s1 with(nolock) on c1.StateID = s1.ID
			left join	Countries co1 with(nolock) on s1.CountryID = co1.ID
			left join	Addresses ad2 with(nolock) on a.PhysicalAddressID = ad2.ID
			left join	Cities c2 with(nolock) on ad2.CityID = c2.ID		
			left join	ZipCodes zip2 with(nolock) on c2.ZipCodeID = zip2.ID
			left join	States s2 with(nolock) on c2.StateID = s2.ID
			left join	Countries co2 with(nolock) on s2.CountryID = co2.ID
			left join	Phones ph with(nolock) on a.BillingPhoneID = ph.ID

			where	aid.id = a.id

			and ( 
						a.Name
				+ '|' + isnull(a.DisplayName,'')
				+ '|' + isnull(a.EmailAddress,'')
				+ '|' + isnull(a.LeadsEmailAddress,'')
				+ '|' + isnull(ad1.Street1,'')
				+ '|' + isnull(ad1.Street2,'')
				+ '|' + isnull(ad1.Street3,'')
				+ '|' + isnull(c1.Name,'')
				+ '|' + isnull(s1.name,'')
				+ '|' + isnull(s1.abbreviation,'')
				+ '|' + isnull(co1.Name,'')
				+ '|' + isnull(co1.Prefix,'')
				+ '|' + isnull(convert(varchar(5), zip1.ZipCode),'')
				+ '|' + isnull(ad2.Street1,'')
				+ '|' + isnull(ad2.Street2,'')
				+ '|' + isnull(ad2.Street3,'')
				+ '|' + isnull(c2.Name,'')
				+ '|' + isnull(s2.name,'')
				+ '|' + isnull(s2.abbreviation,'')
				+ '|' + isnull(co2.Name,'')
				+ '|' + isnull(co2.Prefix,'')
				+ '|' + isnull(zip2.ZipCode,'')
				+ '|' + isnull(ph.Number,'')
				+ '|' + isnull(a.OriginSystemIdentifier,'')
			) like ('%' + @searchString + '%')
		)
		fetch next from st into @searchString
	end
	close st
	deallocate st


	drop table #stable

end


-- return the total count

select	count(*) Total
from	#accountIDs

-- sort order: 1=ID, 2=Name, 3=ParentName, 4=Address, 5=City, 6=State, 7=Zip, ( 8=UserCount, 9=VG Count, 10=DateCreated )

create table #aid2 (ID bigint not null, RowNum int not null)

-- apply the sorts

if @SortBy = 1
	insert	#aid2 (ID, RowNum)
	select	a.ID, ROW_NUMBER() OVER (order by a.ID asc)
	from	#accountIDs a
if @SortBy = 2
	insert	#aid2 (ID, RowNum)
	select	a.ID, ROW_NUMBER() OVER (order by coalesce(a.DisplayName, a.Name) asc)
	from		#accountIDs t
	inner join	Accounts a with(nolock) on a.ID = t.ID
if @SortBy = 3
	insert	#aid2 (ID, RowNum)
	select	a.ID, ROW_NUMBER() OVER (order by coalesce(ap.DisplayName, ap.Name, '') asc)
	from		#accountIDs t
	inner join	Accounts a with(nolock) on a.ID = t.ID
	left join	Accounts ap with(nolock) on a.ParentAccountID = ap.ID
if @SortBy = 4
	insert	#aid2 (ID, RowNum)
	select	a.ID, ROW_NUMBER() OVER (order by coalesce(ad.Street1, '') asc)
	from		#accountIDs t
	inner join	Accounts a with(nolock) on a.ID = t.ID
	left join	Addresses ad with(nolock) on a.BillingAddressID = ad.ID
if @SortBy = 5
	insert	#aid2 (ID, RowNum)
	select	a.ID, ROW_NUMBER() OVER (order by coalesce(c.Name, '') asc)
	from		#accountIDs t
	inner join	Accounts a with(nolock) on a.ID = t.ID
	left join	Addresses ad with(nolock) on a.BillingAddressID = ad.ID
	left join	Cities c with(nolock) on ad.CityID = c.ID
if @SortBy = 6
	insert	#aid2 (ID, RowNum)
	select	a.ID, ROW_NUMBER() OVER (order by coalesce(s.Abbreviation, '') asc)
	from		#accountIDs t
	inner join	Accounts a with(nolock) on a.ID = t.ID
	left join	Addresses ad with(nolock) on a.BillingAddressID = ad.ID
	left join	Cities c with(nolock) on ad.CityID = c.ID
	left join	States s with(nolock) on c.StateID = s.ID
if @SortBy = 7
	insert	#aid2 (ID, RowNum)
	select	a.ID, ROW_NUMBER() OVER (order by coalesce(z.ZipCode, '') asc)
	from		#accountIDs t
	inner join	Accounts a with(nolock) on a.ID = t.ID
	left join	Addresses ad with(nolock) on a.BillingAddressID = ad.ID
	left join	Cities c with(nolock) on ad.CityID = c.ID
	left join	ZipCodes z with(nolock) on c.ZipCodeID = z.ID

if @SortAscending = 0 begin
	declare @maxnum int
	select @maxnum = max(RowNum) + 1 from #aid2
	update	#aid2
	set RowNum = @maxNum - RowNum
end


select		a2.RowNum, coalesce(a.DisplayName, a.Name) [Name], a.ID, 
			a.ParentAccountID, coalesce(pa.DisplayName,pa.name,null) ParentAccountName,
			ad.Street1 [Address], c.Name City, s.Abbreviation [State], zip.ZipCode ZipCode,
			a.DateCreated DateCreatedUTC, at.Name [Type], a.EdmundsDealershipID,
			r.ID ResellerID,
			(SELECT Count(NotificationId) FROM [SI.Notification].dbo.[Notification] WHERE AccountID = a.ID) as AlertCount,
			a.OriginSystemIdentifier

from		Accounts a
inner join	#aid2 a2 with(nolock) on a.ID=a2.ID
inner join	AccountTypes at with(nolock) on a.AccountTypeID = at.ID
left join	Accounts pa with(nolock) on a.ParentAccountID = pa.ID
left join	Addresses ad with(nolock) on a.BillingAddressID = ad.ID
left join	Cities c with(nolock) on ad.CityID = c.ID
left join	ZipCodes zip with(nolock) on c.ZipCodeID = zip.ID
left join	States s with(nolock) on c.StateID = s.ID
left join	Resellers r with(nolock) on a.ID = r.AccountID


where a2.RowNum between @StartIndex and @EndIndex
order by a2.RowNum asc

drop table #accountIDs
drop table #aid2