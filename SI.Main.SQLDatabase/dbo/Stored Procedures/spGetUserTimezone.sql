﻿CREATE procedure spGetUserTimezone
   @userID bigint
as

set nocount on

select	coalesce(u.TimezoneID, a.TimezoneID) TimezoneID
from		Users u
inner join	Accounts a on u.MemberOfAccountID=a.ID
where u.ID=@userID