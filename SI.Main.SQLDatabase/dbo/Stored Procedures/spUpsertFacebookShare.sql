﻿CREATE procedure [dbo].[spUpsertFacebookShare]

	--identity
	@FromID	varchar(100),
	@FromName varchar(250),
	@PictureURL varchar(250),

	--other stuff
	@FacebookPostID bigint,
	@FBSharedPostID varchar(100),
	@FBCreateTime datetime,
	@Message nvarchar(max)

as

set nocount on

declare @FacebookIdentityID bigint
exec spUpsertFacebookIdentity @FromID=@FromID, @FromName=@FromName, @PictureURL=@PictureURL, @FacebookIdentityID=@FacebookIdentityID OUTPUT

declare @FacebookSharedPostID bigint
select	@FacebookSharedPostID = fbsp.ID
from	FacebookSharedPosts fbsp with(nolock)
where	fbsp.FBSharedPostID=@FBSharedPostID

if (@FacebookSharedPostID>0) begin
	update FacebookSharedPosts
	set	Message=@Message
	where ID=@FacebookSharedPostID
end else begin
	insert FacebookSharedPosts(FacebookPostsID, FacebookIdentitiesID, FBSharedPostID, FBCreateTime, Message, DateCreated)
	values (@FacebookPostID, @FacebookIdentityID, @FBSharedPostID, @FBCreateTime, @Message, getdate())
	set @FacebookSharedPostID = SCOPE_IDENTITY()
end

select @FacebookSharedPostID