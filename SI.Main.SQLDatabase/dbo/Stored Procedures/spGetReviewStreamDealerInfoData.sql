﻿/*	
	exec spGetReviewStreamDealerInfoData 
		@AccountIDs = '200049|206585|218308|218727|208072'
*/
CREATE PROCEDURE [dbo].[spGetReviewStreamDealerInfoData]
	@AccountIDs NVARCHAR(MAX) = ''	
AS
BEGIN
	
	SELECT	COUNT(distinct a.ID) AS 'TotalDealer',
			ISNULL(SUM(rs.SalesReviewCount),0) AS 'ReviewCount',
			ISNULL(SUM(rs.TotalPositiveReviews),0) AS 'TotalPositiveReviews',
			ISNULL(SUM(rs.TotalNegativeReviews),0) AS 'TotalNegativeReviews'
	FROM	dbo.fnList2IDTable(@AccountIDs, '|') a
			Left OUTER join ReviewSummaries rs
				on a.id = rs.AccountID
					and rs.SummaryDate = (SELECT MAX(rs1.SummaryDate) from ReviewSummaries rs1 where rs1.AccountID = rs.AccountID and rs1.ReviewSourceID = rs.ReviewSourceID)

END