﻿/*
	DECLARE @TotalCount INT 
	exec spGetReviewStreamData 
		@AccountIDs = '220169|206932|206944|206956|215032|206931|206943|206955|215031|206930|206942|206954|215030|206929|206941|206953|206965|206925|206937|206949|206961|215037|206926|206938|206950|206962|215038|206927|206939|206951|206963|215039|206928|206940|206952|206964|215040|206935|206947|206959|215035|206924|206936|206948|206960|215036|206934|206946|206958|215034|206933|206945|206957|215033',
		@RatinIsGreaterThanOrEqualTo = 0,
		@RatinIsLessThanOrEqualTo = 5,
		@StateID = '',
		@ReviewTextContains = '',
		@ReviewTextDoesNotContain = '',
		@DateFrom = NULL,
		@DateTo = NULL,
		@ReviewSourcesIDs = '500',
		@CategoryID = 0,
		@StartIndex = 0,
		@EndIndex = 100,
		@TotalCount = @TotalCount OUTPUT
		
select @TotalCount

	EXEC spGetReviewStreamData '200049|206585|218308|218727|208072',0,5,28,1,100
*/
CREATE PROCEDURE [dbo].[spGetReviewStreamData]
	@AccountIDs NVARCHAR(MAX) = '',
	@RatinIsGreaterThanOrEqualTo float = 0,
	@RatinIsLessThanOrEqualTo float = 5,
	@StateID NVARCHAR(1000) = '',	
	@ReviewTextContains NVARCHAR(1000) = '',
	@ReviewTextDoesNotContain NVARCHAR(1000) = '',
	@DateFrom DATETIME = NULL,
	@DateTo DATETIME = NULL,
	@DateOn DATETIME = NULL,
	@DateAfter DATETIME = NULL,
	@DateBefore DATETIME = NULL,
	@ReviewSourcesIDs NVARCHAR(100) = '',	
	@CategoryID INT = 0,	
	@OEMIDs NVARCHAR(1000) = '',	
	@ReviewStatusIDs NVARCHAR(100) = '',	
	@LocationGroupIDs NVARCHAR(100) = '',	
	@LocationIDs NVARCHAR(100) = '',
	@SortBy VARCHAR(50) = '',
	@SortAscending bit = 1,
	@StartIndex int = 1,
	@EndIndex int = 25,
	@TotalCount int OUT
AS
BEGIN
	
	IF @SortBy = ''
	BEGIN
		SET @SortBy = 'ReviewDate'
	END

	print @SortBy

	DECLARE @isFilter BIT = 0

	CREATE TABLE #tblAccount (AccountID BIGINT NULL)

	--DECLARE @tblReviewDetail1 TABLE (	ROWNUM INT IDENTITY(1, 1),
	--									ReviewID INT NOT NULL,
	--									AccountID INT NULL,
	--									DisplayName VARCHAR(500) NULL,
	--									ReviewSourceID INT NOT NULL,
	--									ReviewSourceName VARCHAR(500) NOT NULL,
	--									ImagePath VARCHAR(500) NULL,
	--									Rating FLOAT NULL,
	--									ReviewerName VARCHAR(500) NULL,
	--									ReviewDate DATETIME NULL,
	--									ReviewText NVARCHAR(MAX) NULL,
	--									ReviewURL VARCHAR(500) NULL,
	--									ExtraInfo XML NULL,
	--									Filtered BIT NULL,
	--									DateEntered DATETIME NOT NULL,
	--									City VARCHAR(100) NOT NULL,
	--									StateAbrev VARCHAR(100) NOT NULL,
	--									StateID INT NOT NULL,
	--									Zipcode VARCHAR(100) NULL,
	--									OemID INT NULL,
	--									SiteUrl VARCHAR(100) NOT NULL)

	--SELECT * FROM @tblReviewDetail1

	CREATE TABLE #tblReviewDetail (	--ROWNUM INT IDENTITY(1, 1),
									ReviewID INT NOT NULL,
									AccountID INT NULL,
									DisplayName VARCHAR(500) NULL,
									ReviewSourceID INT NOT NULL,
									ReviewSourceName VARCHAR(500) NOT NULL,
									ImagePath VARCHAR(500) NULL,
									Rating FLOAT NULL,
									ReviewerName VARCHAR(500) NULL,
									ReviewDate DATETIME NULL,
									ReviewText NVARCHAR(MAX) NULL,
									ReviewURL VARCHAR(500) NULL,
									ExtraInfo NVARCHAR(MAX)  NULL,
									Filtered BIT NULL,
									DateEntered DATETIME NOT NULL,
									City VARCHAR(100) NOT NULL,
									StateAbrev VARCHAR(100) NOT NULL,
									StateID INT NOT NULL,
									Zipcode VARCHAR(100) NULL,
									OemID INT NULL,
									SiteUrl VARCHAR(500) NULL)
	
	CREATE TABLE #tblReviewDetail1 (	ROWNUM INT IDENTITY(1, 1),
										ReviewID INT NOT NULL,
										AccountID INT NULL,
										DisplayName VARCHAR(500) NULL,
										ReviewSourceID INT NOT NULL,
										ReviewSourceName VARCHAR(500) NOT NULL,
										ImagePath VARCHAR(500) NULL,
										Rating FLOAT NULL,
										ReviewerName VARCHAR(500) NULL,
										ReviewDate DATETIME NULL,
										ReviewText NVARCHAR(MAX) NULL,
										ReviewURL VARCHAR(500) NULL,
										ExtraInfo NVARCHAR(MAX)  NULL,
										Filtered BIT NULL,
										DateEntered DATETIME NOT NULL,
										City VARCHAR(100) NOT NULL,
										StateAbrev VARCHAR(100) NOT NULL,
										StateID INT NOT NULL,
										Zipcode VARCHAR(100) NULL,
										OemID INT NULL,
										SiteUrl VARCHAR(500) NULL)

	IF @CategoryID != 0 AND @CategoryID IS NOT NULL
	BEGIN

		INSERT INTO #tblAccount(AccountID) 
		SELECT DISTINCT a.id 
		FROM dbo.fnList2IDTable(@AccountIDs, '|')  as a
			INNER JOIN Accounts_FranchiseTypes aft
				ON a.id = aft.AccountID
			INNER JOIN FranchiseCategories_FranchiseType cft
				ON aft.FranchiseTypeID = cft.FranchiseTypeID	
		WHERE cft.CategoryID = @CategoryID
	END
	ELSE
	BEGIN
		INSERT INTO #tblAccount(AccountID) 
		SELECT * FROM dbo.fnList2IDTable(@AccountIDs, '|')				
	END	

	--select * from #tblAccount

	IF @ReviewTextDoesNotContain != '' --OR @ReviewTextDoesNotContain IS NOT NULL
	BEGIN
		
		INSERT INTO #tblReviewDetail
		SELECT   r.ID
				,a.[ID]
				,coalesce(a.DisplayName,a.Name) DisplayName
				,r.ReviewSourceID
				,rs.Name AS 'ReviewSourceName'
				,rs.ImagePath
				,r.Rating
				,r.ReviewerName
				,r.ReviewDate
				,r.ReviewText
				,r.ReviewURL
				,CAST(r.ExtraInfo AS NVARCHAR(MAX)) AS 'ExtraInfo'
				,r.Filtered
				,r.DateEntered
				,city.Name
				,s.Abbreviation
				,city.StateID	
				,city.ZipCodeID
				,0
				,ar.URL
			FROM	#tblAccount tbl
					INNER JOIN [dbo].[Reviews] r
						ON tbl.AccountID = r.AccountID
							and r.Filtered = 0
					INNER JOIN dbo.Accounts a
						ON r.AccountID = a.ID
					INNER JOIN dbo.ReviewSources rs
						ON r.ReviewSourceID = rs.ID
					INNER JOIN dbo.Addresses ads
						on a.BillingAddressID = ads.ID
					INNER JOIN dbo.Cities city
						ON ads.CityID = city.ID
					INNER JOIN dbo.States s
						ON city.StateID = s.ID
					INNER JOIN ZipCodes z
						ON city.ZipCodeID = z.ID
					INNER JOIN [dbo].[Accounts_ReviewSources] ar
						ON r.AccountID = ar.AccountID
							AND r.ReviewSourceID = ar.ReviewSourceID
			WHERE	ISNULL(r.Rating,0) BETWEEN @RatinIsGreaterThanOrEqualTo AND @RatinIsLessThanOrEqualTo AND
					NOT CONTAINS(ReviewText, @ReviewTextDoesNotContain)			

			--SET @isFilter = 1
	END
	ELSE IF @ReviewTextContains != '' --OR @ReviewTextContains IS NOT NULL
	BEGIN
		
		print '@ReviewTextContains : ' + @ReviewTextContains
		INSERT INTO #tblReviewDetail
		SELECT   r.ID
				,a.[ID]
				,coalesce(a.Displayname,a.Name) DisplayName
				,r.ReviewSourceID
				,rs.Name AS 'ReviewSourceName'
				,rs.ImagePath
				,r.Rating
				,r.ReviewerName
				,r.ReviewDate
				,r.ReviewText
				,r.ReviewURL
				,CAST(r.ExtraInfo AS NVARCHAR(MAX)) AS 'ExtraInfo'
				,r.Filtered
				,r.DateEntered
				,city.Name
				,s.Abbreviation
				,city.StateID	
				,city.ZipCodeID
				,0
				,ar.URL
			FROM	#tblAccount tbl
					INNER JOIN [dbo].[Reviews] r
						ON tbl.AccountID = r.AccountID
							and r.Filtered = 0
					INNER JOIN dbo.Accounts a
						ON r.AccountID = a.ID
					INNER JOIN dbo.ReviewSources rs
						ON r.ReviewSourceID = rs.ID
					INNER JOIN dbo.Addresses ads
						on a.BillingAddressID = ads.ID
					INNER JOIN dbo.Cities city
						ON ads.CityID = city.ID
					INNER JOIN dbo.States s
						ON city.StateID = s.ID
					INNER JOIN ZipCodes z
						ON city.ZipCodeID = z.ID
					INNER JOIN [dbo].[Accounts_ReviewSources] ar
						ON r.AccountID = ar.AccountID
							AND r.ReviewSourceID = ar.ReviewSourceID
			WHERE	ISNULL(r.Rating,0) BETWEEN @RatinIsGreaterThanOrEqualTo AND @RatinIsLessThanOrEqualTo AND
					CONTAINS(ReviewText, @ReviewTextContains)			
			--SET @isFilter = 1
	END
	ELSE 
	BEGIN
		INSERT INTO #tblReviewDetail
		SELECT   r.ID
				,a.[ID]
				,coalesce(a.Displayname,a.Name) DisplayName
				,r.ReviewSourceID
				,rs.Name AS 'ReviewSourceName'
				,rs.ImagePath
				,r.Rating
				,r.ReviewerName
				,r.ReviewDate
				,r.ReviewText
				,r.ReviewURL
				,CAST(r.ExtraInfo AS NVARCHAR(MAX)) AS 'ExtraInfo'
				,r.Filtered
				,r.DateEntered
				,city.Name
				,s.Abbreviation
				,city.StateID	
				,city.ZipCodeID
				,0
				,ar.URL
			FROM	#tblAccount tbl
					INNER JOIN [dbo].[Reviews] r
						ON tbl.AccountID = r.AccountID
							and r.Filtered = 0
					INNER JOIN dbo.Accounts a
						ON r.AccountID = a.ID
					INNER JOIN dbo.ReviewSources rs
						ON r.ReviewSourceID = rs.ID
					INNER JOIN dbo.Addresses ads
						on a.BillingAddressID = ads.ID
					INNER JOIN dbo.Cities city
						ON ads.CityID = city.ID
					INNER JOIN dbo.States s
						ON city.StateID = s.ID
					INNER JOIN ZipCodes z
						ON city.ZipCodeID = z.ID
					INNER JOIN [dbo].[Accounts_ReviewSources] ar
						ON r.AccountID = ar.AccountID
							AND r.ReviewSourceID = ar.ReviewSourceID					
			WHERE ISNULL(r.Rating,0) BETWEEN @RatinIsGreaterThanOrEqualTo AND @RatinIsLessThanOrEqualTo			
	END

	--select * from #tblReviewDetail

	IF @LocationGroupIDs != '' AND @LocationGroupIDs IS NOT NULL
	BEGIN
		print '@LocationGroupIDs'

		SET @isFilter = 1
	END
	
	IF @LocationIDs != '' AND @LocationIDs IS NOT NULL
	BEGIN
		print '@LocationIDs'

		SET @isFilter = 1
	END

	IF @ReviewStatusIDs != '' AND @ReviewStatusIDs IS NOT NULL
	BEGIN
		print '@ReviewStatusIDs'

		IF (SELECT COUNT(*) FROM #tblReviewDetail1) = 0
		BEGIN
			INSERT INTO #tblReviewDetail1
			SELECT * FROM #tblReviewDetail
			WHERE #tblReviewDetail.Filtered IN (SELECT * FROM dbo.fnList2IDTable(@ReviewStatusIDs, '|')) 					
			ORDER BY
				CASE WHEN @SortBy = 'ReviewDate' AND @SortAscending = 0 THEN ReviewDate END,
				CASE WHEN @SortBy = 'ReviewDate' AND @SortAscending != 0 THEN ReviewDate END DESC,
				CASE WHEN @SortBy = 'CollectedDate' AND @SortAscending = 0 THEN DateEntered END,
				CASE WHEN @SortBy = 'CollectedDate' AND @SortAscending != 0 THEN DateEntered END DESC,
				CASE WHEN @SortBy = 'Name' AND @SortAscending = 0 THEN DisplayName END,
				CASE WHEN @SortBy = 'Name' AND @SortAscending != 0 THEN DisplayName END DESC,
				CASE WHEN @SortBy = 'SiteID' AND @SortAscending = 0 THEN ReviewSourceID END,
				CASE WHEN @SortBy = 'SiteID' AND @SortAscending != 0 THEN ReviewSourceID END DESC,
				CASE WHEN @SortBy = 'Rating' AND @SortAscending = 0 THEN Rating END,
				CASE WHEN @SortBy = 'Rating' AND @SortAscending != 0 THEN Rating END DESC,
				CASE WHEN @SortBy = 'ReviewText' AND @SortAscending = 0 THEN ReviewText END,
				CASE WHEN @SortBy = 'ReviewText' AND @SortAscending != 0 THEN ReviewText END DESC
		END
		ELSE
		BEGIN			
			DELETE FROM #tblReviewDetail1
			WHERE ROWNUM NOT IN (	SELECT ROWNUM FROM #tblReviewDetail1
									WHERE #tblReviewDetail1.Filtered IN (SELECT * FROM dbo.fnList2IDTable(@ReviewStatusIDs, '|')))
		END

		SET @isFilter = 1
	END
	
	IF @StateID != '' AND @StateID IS NOT NULL
	BEGIN
		print '@StateID'
		

		IF (SELECT COUNT(*) FROM #tblReviewDetail1) = 0
		BEGIN
			INSERT INTO #tblReviewDetail1
			SELECT * FROM #tblReviewDetail
			WHERE #tblReviewDetail.StateID IN (SELECT * FROM dbo.fnList2IDTable(@StateID, '|'))
			ORDER BY
				CASE WHEN @SortBy = 'ReviewDate' AND @SortAscending = 0 THEN ReviewDate END,
				CASE WHEN @SortBy = 'ReviewDate' AND @SortAscending != 0 THEN ReviewDate END DESC,
				CASE WHEN @SortBy = 'CollectedDate' AND @SortAscending = 0 THEN DateEntered END,
				CASE WHEN @SortBy = 'CollectedDate' AND @SortAscending != 0 THEN DateEntered END DESC,
				CASE WHEN @SortBy = 'Name' AND @SortAscending = 0 THEN DisplayName END,
				CASE WHEN @SortBy = 'Name' AND @SortAscending != 0 THEN DisplayName END DESC,
				CASE WHEN @SortBy = 'SiteID' AND @SortAscending = 0 THEN ReviewSourceID END,
				CASE WHEN @SortBy = 'SiteID' AND @SortAscending != 0 THEN ReviewSourceID END DESC,
				CASE WHEN @SortBy = 'Rating' AND @SortAscending = 0 THEN Rating END,
				CASE WHEN @SortBy = 'Rating' AND @SortAscending != 0 THEN Rating END DESC,
				CASE WHEN @SortBy = 'ReviewText' AND @SortAscending = 0 THEN ReviewText END,
				CASE WHEN @SortBy = 'ReviewText' AND @SortAscending != 0 THEN ReviewText END DESC
		END
		ELSE
		BEGIN			
			DELETE FROM #tblReviewDetail1
			WHERE ROWNUM NOT IN (SELECT ROWNUM FROM #tblReviewDetail1
								WHERE #tblReviewDetail1.StateID IN (SELECT * FROM dbo.fnList2IDTable(@StateID, '|')))
		END

		SET @isFilter = 1
	END

	IF @DateFrom IS NOT NULL AND @DateTo IS NOT NULL
	BEGIN
		print '@DateFrom  & @DateTo'

		IF (SELECT COUNT(*) FROM #tblReviewDetail1) = 0
		BEGIN
			INSERT INTO #tblReviewDetail1
			SELECT * FROM #tblReviewDetail
			WHERE CAST(#tblReviewDetail.ReviewDate AS DATE) BETWEEN CAST(@DateFrom AS DATE) AND CAST(@DateTo AS DATE)
			ORDER BY
				CASE WHEN @SortBy = 'ReviewDate' AND @SortAscending = 0 THEN ReviewDate END,
				CASE WHEN @SortBy = 'ReviewDate' AND @SortAscending != 0 THEN ReviewDate END DESC,
				CASE WHEN @SortBy = 'CollectedDate' AND @SortAscending = 0 THEN DateEntered END,
				CASE WHEN @SortBy = 'CollectedDate' AND @SortAscending != 0 THEN DateEntered END DESC,
				CASE WHEN @SortBy = 'Name' AND @SortAscending = 0 THEN DisplayName END,
				CASE WHEN @SortBy = 'Name' AND @SortAscending != 0 THEN DisplayName END DESC,
				CASE WHEN @SortBy = 'SiteID' AND @SortAscending = 0 THEN ReviewSourceID END,
				CASE WHEN @SortBy = 'SiteID' AND @SortAscending != 0 THEN ReviewSourceID END DESC,
				CASE WHEN @SortBy = 'Rating' AND @SortAscending = 0 THEN Rating END,
				CASE WHEN @SortBy = 'Rating' AND @SortAscending != 0 THEN Rating END DESC,
				CASE WHEN @SortBy = 'ReviewText' AND @SortAscending = 0 THEN ReviewText END,
				CASE WHEN @SortBy = 'ReviewText' AND @SortAscending != 0 THEN ReviewText END DESC
		END
		ELSE
		BEGIN			
			DELETE FROM #tblReviewDetail1
			WHERE ROWNUM NOT IN (	SELECT ROWNUM FROM #tblReviewDetail1
									WHERE CAST(#tblReviewDetail1.ReviewDate AS DATE) BETWEEN CAST(@DateFrom AS DATE) AND CAST(@DateTo AS DATE)
								)
		END

		--DELETE FROM #tblReviewDetail
		--WHERE CAST(#tblReviewDetail.ReviewDate AS DATE) BETWEEN CAST(@DateFrom AS DATE) AND CAST(@DateTo AS DATE)

		SET @isFilter = 1
	END

	IF @DateOn IS NOT NULL
	BEGIN
		print '@DateOn'

		IF (SELECT COUNT(*) FROM #tblReviewDetail1) = 0
		BEGIN
			INSERT INTO #tblReviewDetail1
			SELECT * FROM #tblReviewDetail
			WHERE CAST(#tblReviewDetail.ReviewDate AS DATE) = CAST(@DateOn AS DATE)
			ORDER BY
				CASE WHEN @SortBy = 'ReviewDate' AND @SortAscending = 0 THEN ReviewDate END,
				CASE WHEN @SortBy = 'ReviewDate' AND @SortAscending != 0 THEN ReviewDate END DESC,
				CASE WHEN @SortBy = 'CollectedDate' AND @SortAscending = 0 THEN DateEntered END,
				CASE WHEN @SortBy = 'CollectedDate' AND @SortAscending != 0 THEN DateEntered END DESC,
				CASE WHEN @SortBy = 'Name' AND @SortAscending = 0 THEN DisplayName END,
				CASE WHEN @SortBy = 'Name' AND @SortAscending != 0 THEN DisplayName END DESC,
				CASE WHEN @SortBy = 'SiteID' AND @SortAscending = 0 THEN ReviewSourceID END,
				CASE WHEN @SortBy = 'SiteID' AND @SortAscending != 0 THEN ReviewSourceID END DESC,
				CASE WHEN @SortBy = 'Rating' AND @SortAscending = 0 THEN Rating END,
				CASE WHEN @SortBy = 'Rating' AND @SortAscending != 0 THEN Rating END DESC,
				CASE WHEN @SortBy = 'ReviewText' AND @SortAscending = 0 THEN ReviewText END,
				CASE WHEN @SortBy = 'ReviewText' AND @SortAscending != 0 THEN ReviewText END DESC
		END
		ELSE
		BEGIN
			DELETE FROM #tblReviewDetail1
			WHERE ROWNUM NOT IN (	SELECT ROWNUM FROM #tblReviewDetail1
									WHERE CAST(#tblReviewDetail1.ReviewDate AS DATE) = CAST(@DateOn AS DATE)
								)
		END

		SET @isFilter = 1
	END

	IF @DateAfter IS NOT NULL
	BEGIN
		print '@DateAfter'

		IF (SELECT COUNT(*) FROM #tblReviewDetail1) = 0
		BEGIN
			INSERT INTO #tblReviewDetail1
			SELECT * FROM #tblReviewDetail
			WHERE CAST(#tblReviewDetail.ReviewDate AS DATE) > CAST(@DateAfter AS DATE)
			ORDER BY
				CASE WHEN @SortBy = 'ReviewDate' AND @SortAscending = 0 THEN ReviewDate END,
				CASE WHEN @SortBy = 'ReviewDate' AND @SortAscending != 0 THEN ReviewDate END DESC,
				CASE WHEN @SortBy = 'CollectedDate' AND @SortAscending = 0 THEN DateEntered END,
				CASE WHEN @SortBy = 'CollectedDate' AND @SortAscending != 0 THEN DateEntered END DESC,
				CASE WHEN @SortBy = 'Name' AND @SortAscending = 0 THEN DisplayName END,
				CASE WHEN @SortBy = 'Name' AND @SortAscending != 0 THEN DisplayName END DESC,
				CASE WHEN @SortBy = 'SiteID' AND @SortAscending = 0 THEN ReviewSourceID END,
				CASE WHEN @SortBy = 'SiteID' AND @SortAscending != 0 THEN ReviewSourceID END DESC,
				CASE WHEN @SortBy = 'Rating' AND @SortAscending = 0 THEN Rating END,
				CASE WHEN @SortBy = 'Rating' AND @SortAscending != 0 THEN Rating END DESC,
				CASE WHEN @SortBy = 'ReviewText' AND @SortAscending = 0 THEN ReviewText END,
				CASE WHEN @SortBy = 'ReviewText' AND @SortAscending != 0 THEN ReviewText END DESC
		END
		ELSE
		BEGIN
			DELETE FROM #tblReviewDetail1
			WHERE ROWNUM NOT IN (	SELECT ROWNUM FROM #tblReviewDetail1
									WHERE CAST(#tblReviewDetail1.ReviewDate AS DATE) > CAST(@DateAfter AS DATE)
								)
		END

		SET @isFilter = 1
	END

	IF @DateBefore IS NOT NULL
	BEGIN
		print '@DateBefore'

		IF (SELECT COUNT(*) FROM #tblReviewDetail1) = 0
		BEGIN
			INSERT INTO #tblReviewDetail1
			SELECT * FROM #tblReviewDetail
			WHERE CAST(#tblReviewDetail.ReviewDate AS DATE) < CAST(@DateBefore AS DATE)
			ORDER BY
				CASE WHEN @SortBy = 'ReviewDate' AND @SortAscending = 0 THEN ReviewDate END,
				CASE WHEN @SortBy = 'ReviewDate' AND @SortAscending != 0 THEN ReviewDate END DESC,
				CASE WHEN @SortBy = 'CollectedDate' AND @SortAscending = 0 THEN DateEntered END,
				CASE WHEN @SortBy = 'CollectedDate' AND @SortAscending != 0 THEN DateEntered END DESC,
				CASE WHEN @SortBy = 'Name' AND @SortAscending = 0 THEN DisplayName END,
				CASE WHEN @SortBy = 'Name' AND @SortAscending != 0 THEN DisplayName END DESC,
				CASE WHEN @SortBy = 'SiteID' AND @SortAscending = 0 THEN ReviewSourceID END,
				CASE WHEN @SortBy = 'SiteID' AND @SortAscending != 0 THEN ReviewSourceID END DESC,
				CASE WHEN @SortBy = 'Rating' AND @SortAscending = 0 THEN Rating END,
				CASE WHEN @SortBy = 'Rating' AND @SortAscending != 0 THEN Rating END DESC,
				CASE WHEN @SortBy = 'ReviewText' AND @SortAscending = 0 THEN ReviewText END,
				CASE WHEN @SortBy = 'ReviewText' AND @SortAscending != 0 THEN ReviewText END DESC
		END
		ELSE
		BEGIN
			DELETE FROM #tblReviewDetail1
			WHERE ROWNUM NOT IN (	SELECT ROWNUM FROM #tblReviewDetail1
									WHERE CAST(#tblReviewDetail1.ReviewDate AS DATE) > CAST(@DateBefore AS DATE)
								)
		END

		SET @isFilter = 1
	END

	IF @ReviewSourcesIDs != '' AND @ReviewSourcesIDs IS NOT NULL
	BEGIN
		print '@ReviewSourcesIDs'

		IF (SELECT COUNT(*) FROM #tblReviewDetail1) = 0
		BEGIN
			PRINT 'IF'

			INSERT INTO #tblReviewDetail1
			SELECT * FROM #tblReviewDetail
			WHERE #tblReviewDetail.ReviewSourceID IN (SELECT * FROM dbo.fnList2IDTable(@ReviewSourcesIDs, '|'))
			ORDER BY
				CASE WHEN @SortBy = 'ReviewDate' AND @SortAscending = 0 THEN ReviewDate END,
				CASE WHEN @SortBy = 'ReviewDate' AND @SortAscending != 0 THEN ReviewDate END DESC,
				CASE WHEN @SortBy = 'CollectedDate' AND @SortAscending = 0 THEN DateEntered END,
				CASE WHEN @SortBy = 'CollectedDate' AND @SortAscending != 0 THEN DateEntered END DESC,
				CASE WHEN @SortBy = 'Name' AND @SortAscending = 0 THEN DisplayName END,
				CASE WHEN @SortBy = 'Name' AND @SortAscending != 0 THEN DisplayName END DESC,
				CASE WHEN @SortBy = 'SiteID' AND @SortAscending = 0 THEN ReviewSourceID END,
				CASE WHEN @SortBy = 'SiteID' AND @SortAscending != 0 THEN ReviewSourceID END DESC,
				CASE WHEN @SortBy = 'Rating' AND @SortAscending = 0 THEN Rating END,
				CASE WHEN @SortBy = 'Rating' AND @SortAscending != 0 THEN Rating END DESC,
				CASE WHEN @SortBy = 'ReviewText' AND @SortAscending = 0 THEN ReviewText END,
				CASE WHEN @SortBy = 'ReviewText' AND @SortAscending != 0 THEN ReviewText END DESC
		END
		ELSE
		BEGIN
			PRINT 'ELSE'

			DELETE FROM #tblReviewDetail1
			WHERE ROWNUM NOT IN (	SELECT ROWNUM FROM #tblReviewDetail1
									WHERE #tblReviewDetail1.ReviewSourceID IN (SELECT * FROM dbo.fnList2IDTable(@ReviewSourcesIDs, '|'))
								)
		END

		SET @isFilter = 1
	END

	
	IF @CategoryID != 0 AND @CategoryID IS NOT NULL
	BEGIN
		print '@CategoryID'

		IF (SELECT COUNT(*) FROM #tblReviewDetail1) = 0
		BEGIN
			INSERT INTO #tblReviewDetail1
			SELECT #tblReviewDetail.*
			FROM #tblReviewDetail			
			ORDER BY 
				CASE WHEN @SortBy = 'ReviewDate' AND @SortAscending = 0 THEN ReviewDate END,
				CASE WHEN @SortBy = 'ReviewDate' AND @SortAscending != 0 THEN ReviewDate END DESC,
				CASE WHEN @SortBy = 'CollectedDate' AND @SortAscending = 0 THEN DateEntered END,
				CASE WHEN @SortBy = 'CollectedDate' AND @SortAscending != 0 THEN DateEntered END DESC,
				CASE WHEN @SortBy = 'Name' AND @SortAscending = 0 THEN DisplayName END,
				CASE WHEN @SortBy = 'Name' AND @SortAscending != 0 THEN DisplayName END DESC,
				CASE WHEN @SortBy = 'SiteID' AND @SortAscending = 0 THEN ReviewSourceID END,
				CASE WHEN @SortBy = 'SiteID' AND @SortAscending != 0 THEN ReviewSourceID END DESC,
				CASE WHEN @SortBy = 'Rating' AND @SortAscending = 0 THEN Rating END,
				CASE WHEN @SortBy = 'Rating' AND @SortAscending != 0 THEN Rating END DESC,
				CASE WHEN @SortBy = 'ReviewText' AND @SortAscending = 0 THEN ReviewText END,
				CASE WHEN @SortBy = 'ReviewText' AND @SortAscending != 0 THEN ReviewText END DESC
		END
		ELSE
		BEGIN
			DELETE FROM #tblReviewDetail1
			WHERE ROWNUM NOT IN (	SELECT ROWNUM FROM #tblReviewDetail1
									INNER JOIN Accounts_FranchiseTypes aft ON #tblReviewDetail1.AccountID = aft.AccountID
									INNER JOIN FranchiseCategories_FranchiseType cft ON aft.FranchiseTypeID = cft.FranchiseTypeID
									WHERE cft.CategoryID = @CategoryID
								)
		END

		SET @isFilter = 1
	END

	IF @OEMIDs != '' AND @OEMIDs IS NOT NULL
	BEGIN
		print '@OEMIDs'

		IF (SELECT COUNT(*) FROM #tblReviewDetail1) = 0
		BEGIN
			INSERT INTO #tblReviewDetail1
			SELECT #tblReviewDetail.* 
			FROM #tblReviewDetail
					INNER JOIN Accounts_FranchiseTypes aft
						ON #tblReviewDetail.AccountID = aft.AccountID
			WHERE aft.FranchiseTypeID IN (SELECT * FROM dbo.fnList2IDTable(@OEMIDs, '|'))
			ORDER BY
				CASE WHEN @SortBy = 'ReviewDate' AND @SortAscending = 0 THEN ReviewDate END,
				CASE WHEN @SortBy = 'ReviewDate' AND @SortAscending != 0 THEN ReviewDate END DESC,
				CASE WHEN @SortBy = 'CollectedDate' AND @SortAscending = 0 THEN DateEntered END,
				CASE WHEN @SortBy = 'CollectedDate' AND @SortAscending != 0 THEN DateEntered END DESC,
				CASE WHEN @SortBy = 'Name' AND @SortAscending = 0 THEN DisplayName END,
				CASE WHEN @SortBy = 'Name' AND @SortAscending != 0 THEN DisplayName END DESC,
				CASE WHEN @SortBy = 'SiteID' AND @SortAscending = 0 THEN ReviewSourceID END,
				CASE WHEN @SortBy = 'SiteID' AND @SortAscending != 0 THEN ReviewSourceID END DESC,
				CASE WHEN @SortBy = 'Rating' AND @SortAscending = 0 THEN Rating END,
				CASE WHEN @SortBy = 'Rating' AND @SortAscending != 0 THEN Rating END DESC,
				CASE WHEN @SortBy = 'ReviewText' AND @SortAscending = 0 THEN ReviewText END,
				CASE WHEN @SortBy = 'ReviewText' AND @SortAscending != 0 THEN ReviewText END DESC
		END
		ELSE
		BEGIN
			DELETE FROM #tblReviewDetail1
			WHERE ROWNUM NOT IN (	SELECT ROWNUM FROM #tblReviewDetail1
									INNER JOIN Accounts_FranchiseTypes aft ON #tblReviewDetail1.AccountID = aft.AccountID
									WHERE aft.FranchiseTypeID IN (SELECT * FROM dbo.fnList2IDTable(@OEMIDs, '|'))
								)
		END

		SET @isFilter = 1

	END

	IF @ReviewStatusIDs != '' AND @ReviewStatusIDs IS NOT NULL
	BEGIN
		print '@ReviewStatusIDs'

		SET @isFilter = 1
	END

	print @isFilter

	IF @isFilter = 0
	BEGIN
		INSERT INTO #tblReviewDetail1
		SELECT * FROM #tblReviewDetail
		ORDER BY
				CASE WHEN @SortBy = 'ReviewDate' AND @SortAscending = 0 THEN ReviewDate END,
				CASE WHEN @SortBy = 'ReviewDate' AND @SortAscending != 0 THEN ReviewDate END DESC,
				CASE WHEN @SortBy = 'CollectedDate' AND @SortAscending = 0 THEN DateEntered END,
				CASE WHEN @SortBy = 'CollectedDate' AND @SortAscending != 0 THEN DateEntered END DESC,
				CASE WHEN @SortBy = 'Name' AND @SortAscending = 0 THEN DisplayName END,
				CASE WHEN @SortBy = 'Name' AND @SortAscending != 0 THEN DisplayName END DESC,
				CASE WHEN @SortBy = 'SiteID' AND @SortAscending = 0 THEN ReviewSourceID END,
				CASE WHEN @SortBy = 'SiteID' AND @SortAscending != 0 THEN ReviewSourceID END DESC,
				CASE WHEN @SortBy = 'Rating' AND @SortAscending = 0 THEN Rating END,
				CASE WHEN @SortBy = 'Rating' AND @SortAscending != 0 THEN Rating END DESC,
				CASE WHEN @SortBy = 'ReviewText' AND @SortAscending = 0 THEN ReviewText END,
				CASE WHEN @SortBy = 'ReviewText' AND @SortAscending != 0 THEN ReviewText END DESC
	END

	select @TotalCount = count(*) from	#tblReviewDetail1

	SELECT	 ROWNUM
			,ReviewID
			,AccountID
			,DisplayName
			,ReviewSourceID
			,ReviewSourceName
			,ImagePath
			,Rating
			,ReviewerName
			,ReviewDate
			,ISNULL(ReviewText,'') AS 'ReviewText'
			,ReviewURL
			,ExtraInfo
			,Filtered
			,DateEntered 
			,City
			,StateAbrev
			,StateID
			,Zipcode
			,OemID
			,SiteUrl 
	FROM #tblReviewDetail1	
	where  ROWNUM between @StartIndex and @EndIndex
	ORDER BY ROWNUM ASC


	DROP TABLE #tblReviewDetail
	DROP TABLE #tblReviewDetail1
	DROP TABLE #tblAccount

END