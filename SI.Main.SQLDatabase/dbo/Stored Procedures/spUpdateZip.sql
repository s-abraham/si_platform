﻿--exec spUpdateZip 7675,'Westwood','NJ','40.98',-74.03, 24083, 13245, 1089095041 
CREATE procedure [dbo].[spUpdateZip]
	@zip bigint,
	@city nvarchar(150),
	@state nvarchar(150),
	@lat float,
	@lon float,
	@population int = null,
	@taxfiled int = null,
	@wages int = null
as

declare @USA bigint
declare @StateID bigint
declare @CityID bigint
declare @AddedCity bit
declare @ZipcodeID bigint

set @AddedCity = 0
select @USA=ID from Countries with(nolock) where ltrim(rtrim(lower(Prefix)))='us'
select @StateID=ID from States with(nolock) where ltrim(rtrim(lower(Abbreviation)))=ltrim(rtrim(lower(@state))) and CountryID=@USA


if @StateID is not null begin
	
	select @CityID=ID from Cities with(nolock) where StateID=@StateID and ltrim(rtrim(lower(Name))) = ltrim(rtrim(lower(@city)))
	if @CityID is null begin
		set @AddedCity = 1
		insert Cities	(Name,StateID,ZipCodeID,LanguageID)
		values			(@city,@StateID,null,null)

		select @CityID=SCOPE_IDENTITY()
	end

	select @ZipcodeID = ID from zipcodes with(nolock) where ZipCode=@zip

	if @ZipcodeID is not null begin

		update	zipcodes
		set		StateID=@StateID, Latitude=@lat, Longitude=@lon, EstimatedPopulation=@population, TotalWages=@wages, TaxReturnsFiled=@taxfiled, DateModified=getdate()
		where	ID=@zip		

	end else begin

		insert	zipcodes(ZipCode,StateID,Latitude,Longitude,EstimatedPopulation,TotalWages,TaxReturnsFiled,DateCreated,DateModified)
		values	(@zip,@StateID,@lat,@lon,@population,@wages,@taxfiled,getdate(),getdate())

		select @ZipcodeID=SCOPE_IDENTITY()

	end

	update cities
	set ZipCodeID=@ZipcodeID
	where id=@CityID
	

end