﻿CREATE PROCEDURE [dbo].[ReportSyndicationPostStream]
	@PostID bigint
AS
BEGIN
	SET NOCOUNT ON 
	declare @dummy TABLE(
		PostID bigint,
		LocationName varchar(200),
		Action varchar(50),
		Message varchar(200),
		FromName varchar(100),
		CreatedDate datetime
	)

	insert @dummy values (1, 'Castle Chevrolet', 'Comment', 'Herbie FOREVER!', 'Sanju Abraham', getdate())
	insert @dummy values (1, 'Castle Buick', 'Comment', 'Go Braves!', 'Sanju Abraham', getdate())
	insert @dummy values (1, 'Castle Ford', 'Comment', 'SOCIALDEALER RULES', 'Reynolds Kosloskey', getdate())
	insert @dummy values (1, 'Castle Honda', 'Like', '', 'Sanju Abraham', getdate())
	insert @dummy values (1, 'Castle Chevrolet', 'Like', '', 'Sanju Abraham', getdate())
	insert @dummy values (1, 'Castle Chevrolet', 'Like', '', 'Sanju Abraham', getdate())
	insert @dummy values (1, 'Castle Kia', 'Share', '', 'Sanju Abraham', getdate())
	insert @dummy values (1, 'Castle Chevrolet', 'Share', '', 'Sanju Abraham', getdate())
	insert @dummy values (1, 'Castle BMW', 'Like', '', 'Sanju Abraham', getdate())
	insert @dummy values (1, 'Castle Chevrolet', 'Comment', 'Denver is going to win the Super Bowl ', 'Arpan Patel', getdate())



	
	select * from @dummy order by CreatedDate desc, LocationName





END