﻿CREATE procedure [dbo].[spSetPostImageJob]
	@postImageID bigint,
	@postTargetID bigint,
	@jobID bigint
as

set nocount on

declare @id bigint 
set @id=null
select @id=ID from PostImageResults
where	PostTargetID=@postTargetID
and		PostImageID=@postImageID

if (coalesce(@id,0)=0)
	insert PostImageResults(PostImageID, PostTargetID, JobID, ResultID, FeedID, ProcessedDate)
	values (@postImageID, @postTargetID, @jobID, null, null, null)
else 
	update  PostImageResults
	set		JobID=@jobID, ProcessedDate=null
	where	PostTargetID=@postTargetID
	and		PostImageID=@postImageID