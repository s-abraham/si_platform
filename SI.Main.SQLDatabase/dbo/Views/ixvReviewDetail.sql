﻿

create view [dbo].[ixvReviewDetail] with SCHEMABINDING

as

select		r.ID, r.AccountID, r.ReviewSourceID, r.ReviewDate, r.Rating, count_big(*) ReviewCount
from		dbo.Reviews r
where		r.Filtered = 0 
group by	r.ID, r.AccountID, r.ReviewSourceID, r.ReviewDate, r.Rating
  

/*
select count(*) from ixvReviewDetail
select top 100 * from ixvReviewDetail
*/
GO
CREATE UNIQUE CLUSTERED INDEX [ixvReviewDetail_IX1]
    ON [dbo].[ixvReviewDetail]([AccountID] ASC, [ReviewSourceID] ASC, [ID] ASC);

