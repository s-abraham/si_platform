﻿CREATE VIEW vReviewStreamData
AS
    SELECT   r.ID AS 'ReviewID'
			,a.[ID] AS 'AccountID'
			,a.DisplayName
			,r.ReviewSourceID
			,rs.Name AS 'ReviewSourceName'
			,rs.ImagePath
			,r.Rating
			,r.ReviewerName
			,r.ReviewDate
			,r.ReviewText
			,r.ReviewURL
			,r.ExtraInfo
			,r.Filtered
			,r.DateEntered
			,city.Name
			,s.Abbreviation
			,city.StateID	
			,z.ZipCode AS 'ZipCode'
			,0 AS 'OEMID'
			,ar.URL
		FROM	[dbo].[Reviews] r				
				INNER JOIN dbo.Accounts a
					ON r.AccountID = a.ID
				INNER JOIN dbo.ReviewSources rs
					ON r.ReviewSourceID = rs.ID
				INNER JOIN dbo.Addresses ads
					on a.BillingAddressID = ads.ID
				INNER JOIN dbo.Cities city
					ON ads.CityID = city.ID
				INNER JOIN dbo.States s
					ON city.StateID = s.ID
				INNER JOIN dbo.ZipCodes z
					ON city.ZipCodeID = z.ID
				INNER JOIN [dbo].[Accounts_ReviewSources] ar
					ON r.AccountID = ar.AccountID
						AND r.ReviewSourceID = ar.ReviewSourceID