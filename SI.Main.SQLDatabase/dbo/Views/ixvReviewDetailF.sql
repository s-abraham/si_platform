﻿
/*
select count(*) from ixvReviewDetail
select top 100 * from ixvReviewDetail
*/
CREATE VIEW [dbo].[ixvReviewDetailF]
WITH SCHEMABINDING 
AS
SELECT        r.ID, AccountID, ReviewDate, Rating, ReviewSourceID, COUNT_BIG(*) AS ReviewCount
FROM            dbo.Reviews AS r
WHERE        (Filtered = 1)
GROUP BY r.ID, AccountID, ReviewDate, Rating, ReviewSourceID
GO
CREATE UNIQUE CLUSTERED INDEX [ixvReviewDetailF_IX1]
    ON [dbo].[ixvReviewDetailF]([ID] ASC, [AccountID] ASC, [ReviewDate] ASC, [Rating] ASC, [ReviewSourceID] ASC);

