﻿
/****** Script for SelectTopNRows command from SSMS  ******/
CREATE VIEW [dbo].[vPostsCreatedToday]
AS
SELECT        p.ID AS PostId, p.ScheduleDate, p.DateCreated, p.DateModified, p.UserID, u.Username, c.AccountID, a.Name AS AccountName, p.Title, 
                         ps.Name AS PostSource, pt1.Name AS PostType, p.PostExpirationDate, p.Draft, pt.ID AS PostTargetId, pt.JobID AS TargetJobId, pt.ResultID AS TargetResultId, 
                         pt.FeedID AS TargetFeedId, pt.Deleted AS TargetDeleted, pt.ScheduleDate AS TargetScheduleDate, pt.PostExpirationDate AS TargetPostExpirationDate, 
                         pt.PickedUpDate AS TargetPickedUpDate, pt.PublishedDate AS TargetPublishedDate, pr.Message, pr.Link, pr.DateCreated AS Expr1
FROM            dbo.Posts AS p INNER JOIN
                         dbo.PostTargets AS pt ON p.ID = pt.PostID INNER JOIN
                         dbo.Credentials AS c ON pt.CredentialID = c.ID INNER JOIN
                         dbo.Accounts AS a ON c.AccountID = a.ID INNER JOIN
                         dbo.Users AS u ON p.UserID = u.ID INNER JOIN
                         dbo.PostTypes AS pt1 ON p.PostTypeID = pt1.ID INNER JOIN
                         dbo.PostSources AS ps ON p.PostSourceID = ps.ID INNER JOIN
                         dbo.PostRevisions AS pr ON p.ID = pr.PostID
WHERE        (CONVERT(varchar(10), p.DateCreated, 101) = CONVERT(varchar(10), GETDATE(), 101))
GO
EXECUTE sp_addextendedproperty @name = N'MS_DiagramPaneCount', @value = 2, @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'vPostsCreatedToday';


GO
EXECUTE sp_addextendedproperty @name = N'MS_DiagramPane2', @value = N'd
         Begin Table = "pr"
            Begin Extent = 
               Top = 138
               Left = 38
               Bottom = 268
               Right = 209
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'vPostsCreatedToday';


GO
EXECUTE sp_addextendedproperty @name = N'MS_DiagramPane1', @value = N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[20] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "p"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 136
               Right = 226
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "pt"
            Begin Extent = 
               Top = 6
               Left = 264
               Bottom = 136
               Right = 452
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "c"
            Begin Extent = 
               Top = 6
               Left = 490
               Bottom = 136
               Right = 673
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "a"
            Begin Extent = 
               Top = 6
               Left = 711
               Bottom = 136
               Right = 918
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "u"
            Begin Extent = 
               Top = 6
               Left = 956
               Bottom = 136
               Right = 1159
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "pt1"
            Begin Extent = 
               Top = 6
               Left = 1197
               Bottom = 102
               Right = 1367
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "ps"
            Begin Extent = 
               Top = 102
               Left = 1197
               Bottom = 198
               Right = 1367
            End
            DisplayFlags = 280
            TopColumn = 0
         En', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'vPostsCreatedToday';

