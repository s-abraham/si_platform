﻿

create view [dbo].[ixvSummaryData] with SCHEMABINDING

as

select		rs.AccountID, rs.ReviewSourceID, rs.SummaryDate, 
			rs.NewReviewsCount, rs.SalesReviewCount, rs.SalesAverageRating AvgRating, 
			rs.NewReviewsPositiveCount, rs.NewReviewsNegativeCount,
			rs.TotalPositiveReviews, rs.TotalNegativeReviews,
			count_big(*) DummyCount

from		dbo.ReviewSummaries rs

group by	rs.AccountID, rs.ReviewSourceID, rs.SummaryDate, 
			rs.NewReviewsCount, rs.SalesReviewCount, rs.SalesAverageRating,
			rs.NewReviewsPositiveCount, rs.NewReviewsNegativeCount,
			rs.TotalPositiveReviews, rs.TotalNegativeReviews


  

/*
select top 1000 * from reviewsummaries
select count(*) from ixvSummaryData
select top 100 * from ixvSummaryData
*/
GO
CREATE UNIQUE CLUSTERED INDEX [ixvSummaryData_IX1]
    ON [dbo].[ixvSummaryData]([AccountID] ASC, [ReviewSourceID] ASC, [SummaryDate] ASC);

