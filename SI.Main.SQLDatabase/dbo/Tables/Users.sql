﻿CREATE TABLE [dbo].[Users] (
    [ID]                   BIGINT           IDENTITY (1, 1) NOT NULL,
    [RootAccountID]        BIGINT           NOT NULL,
    [Username]             NVARCHAR (50)    NOT NULL,
    [FirstName]            NVARCHAR (100)   NOT NULL,
    [LastName]             NVARCHAR (100)   NOT NULL,
    [SuperAdmin]           BIT              NOT NULL,
    [Password]             NVARCHAR (100)   NULL,
    [TimezoneID]           BIGINT           NULL,
    [MemberOfAccountID]    BIGINT           NOT NULL,
    [EmailAddress]         NVARCHAR (100)   NULL,
    [InvalidLoginAttempts] INT              CONSTRAINT [DF_Users_InvalidLoginAttempts] DEFAULT ((0)) NOT NULL,
    [AccountLocked]        BIT              CONSTRAINT [DF_Users_AccountLocked] DEFAULT ((0)) NOT NULL,
    [SSOSecretKey]         UNIQUEIDENTIFIER CONSTRAINT [DF_Users_SSOSecretKey] DEFAULT (newid()) NOT NULL,
    [DateCreated]          DATETIME         NOT NULL,
    [DateModified]         DATETIME         NULL,
    CONSTRAINT [PK_Users] PRIMARY KEY CLUSTERED ([ID] ASC),
    CONSTRAINT [FK_Users_Accounts] FOREIGN KEY ([RootAccountID]) REFERENCES [dbo].[Accounts] ([ID]),
    CONSTRAINT [FK_Users_Accounts1] FOREIGN KEY ([MemberOfAccountID]) REFERENCES [dbo].[Accounts] ([ID])
);








GO
CREATE NONCLUSTERED INDEX [IX_Users_EmailAddress]
    ON [dbo].[Users]([EmailAddress] ASC);


GO
CREATE UNIQUE NONCLUSTERED INDEX [IX_Users_Username]
    ON [dbo].[Users]([Username] ASC);

