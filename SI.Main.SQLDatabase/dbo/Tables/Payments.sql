﻿CREATE TABLE [dbo].[Payments] (
    [ID]            BIGINT          IDENTITY (1, 1) NOT NULL,
    [AccountID]     BIGINT          NOT NULL,
    [PaymentDate]   DATE            NOT NULL,
    [Amount]        DECIMAL (19, 4) NOT NULL,
    [InvoiceNumber] VARCHAR (50)    NULL,
    [DateInvoiced]  DATETIME        NULL,
    [DateDue]       DATETIME        NOT NULL,
    [DatePaid]      DATETIME        NULL,
    [DateCreated]   DATETIME        NOT NULL,
    CONSTRAINT [PK_Payments] PRIMARY KEY CLUSTERED ([ID] ASC)
);


GO
CREATE NONCLUSTERED INDEX [IX_Payments_AccountID_DateDue_DateInvoiced_DatePaid]
    ON [dbo].[Payments]([AccountID] ASC, [DateDue] ASC, [DateInvoiced] ASC, [DatePaid] ASC);

