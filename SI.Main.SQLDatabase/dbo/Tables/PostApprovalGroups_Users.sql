﻿CREATE TABLE [dbo].[PostApprovalGroups_Users] (
    [ID]                  BIGINT IDENTITY (1, 1) NOT NULL,
    [PostApprovalGroupID] BIGINT NOT NULL,
    [UserID]              BIGINT NOT NULL,
    CONSTRAINT [PK_PostApprovalGroups_Users] PRIMARY KEY CLUSTERED ([ID] ASC),
    CONSTRAINT [FK_PostApprovalGroups_Users_PostApprovalGroups] FOREIGN KEY ([PostApprovalGroupID]) REFERENCES [dbo].[PostApprovalGroups] ([ID]),
    CONSTRAINT [FK_PostApprovalGroups_Users_Users] FOREIGN KEY ([UserID]) REFERENCES [dbo].[Users] ([ID])
);


GO
CREATE UNIQUE NONCLUSTERED INDEX [IX_PostApprovalGroups_Users_PostApprovalGroupID_UserID]
    ON [dbo].[PostApprovalGroups_Users]([PostApprovalGroupID] ASC, [UserID] ASC);

