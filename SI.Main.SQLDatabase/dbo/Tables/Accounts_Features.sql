﻿CREATE TABLE [dbo].[Accounts_Features] (
    [ID]            BIGINT IDENTITY (1, 1) NOT NULL,
    [AccountID]     BIGINT NOT NULL,
    [FeatureTypeID] BIGINT NOT NULL,
    CONSTRAINT [PK_Accounts_Features] PRIMARY KEY CLUSTERED ([ID] ASC),
    CONSTRAINT [FK_Accounts_Features_Accounts] FOREIGN KEY ([AccountID]) REFERENCES [dbo].[Accounts] ([ID])
);




GO
CREATE UNIQUE NONCLUSTERED INDEX [IX_Accounts_Features_AccountID_FeatureTypeID]
    ON [dbo].[Accounts_Features]([AccountID] ASC, [FeatureTypeID] ASC);

