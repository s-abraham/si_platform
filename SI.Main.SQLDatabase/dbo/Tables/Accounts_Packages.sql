﻿CREATE TABLE [dbo].[Accounts_Packages] (
    [ID]           BIGINT   IDENTITY (1, 1) NOT NULL,
    [AccountID]    BIGINT   NOT NULL,
    [PackageID]    BIGINT   NOT NULL,
    [DateCreated]  DATETIME NOT NULL,
    [DateModified] DATETIME NOT NULL,
    CONSTRAINT [PK_Accounts_Packages] PRIMARY KEY CLUSTERED ([ID] ASC),
    CONSTRAINT [FK_Accounts_Packages_Accounts] FOREIGN KEY ([AccountID]) REFERENCES [dbo].[Accounts] ([ID])
);




GO
CREATE UNIQUE NONCLUSTERED INDEX [IX_Accounts_Packages_AccountID_PackageID]
    ON [dbo].[Accounts_Packages]([AccountID] ASC, [PackageID] ASC);

