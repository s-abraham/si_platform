﻿CREATE TABLE [dbo].[PostTargets] (
    [ID]                 BIGINT        IDENTITY (1, 1) NOT NULL,
    [PostID]             BIGINT        NOT NULL,
    [CredentialID]       BIGINT        NOT NULL,
    [JobID]              BIGINT        NULL,
    [ResultID]           VARCHAR (250) NULL,
    [FeedID]             VARCHAR (250) NULL,
    [Deleted]            BIT           NOT NULL,
    [ScheduleDate]       DATETIME      NULL,
    [PostExpirationDate] DATETIME      NULL,
    [PickedUpDate]       DATETIME      NULL,
    [PublishedDate]      DATETIME      NULL,
    [FailedDate]         DATETIME      NULL,
    CONSTRAINT [PK_PostTargets] PRIMARY KEY CLUSTERED ([ID] ASC),
    CONSTRAINT [FK_PostTargets_Credentials] FOREIGN KEY ([CredentialID]) REFERENCES [dbo].[Credentials] ([ID]),
    CONSTRAINT [FK_PostTargets_Posts] FOREIGN KEY ([PostID]) REFERENCES [dbo].[Posts] ([ID])
);




GO
CREATE NONCLUSTERED INDEX [IX_PostTargets_ProcessedDate]
    ON [dbo].[PostTargets]([PublishedDate] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_PostTargets_JobID]
    ON [dbo].[PostTargets]([JobID] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_PostTargets_PostID]
    ON [dbo].[PostTargets]([PostID] ASC);

