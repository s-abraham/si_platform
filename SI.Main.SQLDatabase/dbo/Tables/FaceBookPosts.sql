﻿CREATE TABLE [dbo].[FaceBookPosts] (
    [ID]                BIGINT         IDENTITY (1, 1) NOT NULL,
    [CredentialID]      BIGINT         NOT NULL,
    [SIPostTargetID]    BIGINT         NULL,
    [FeedID]            VARCHAR (250)  NOT NULL,
    [ResultID]          VARCHAR (250)  NOT NULL,
    [Name]              NVARCHAR (MAX) NULL,
    [Message]           NVARCHAR (MAX) NULL,
    [PostTypeID]        BIGINT         NULL,
    [LinkURL]           VARCHAR (1000) NULL,
    [PictureURL]        VARCHAR (1000) NULL,
    [FBCreatedTime]     DATETIME       NULL,
    [FBUpdatedTime]     DATETIME       NULL,
    [DateCreated]       DATETIME       CONSTRAINT [DF__FaceBookP__DateC__0A6AEDB6] DEFAULT (getdate()) NOT NULL,
    [DateLastRefreshed] DATETIME       NULL,
    CONSTRAINT [PK_FACEBOOKPOST] PRIMARY KEY CLUSTERED ([ID] ASC),
    CONSTRAINT [FK_FaceBookPosts_Credentials] FOREIGN KEY ([CredentialID]) REFERENCES [dbo].[Credentials] ([ID])
);


GO
CREATE NONCLUSTERED INDEX [IX_FaceBookPosts_DateLastRefreshed]
    ON [dbo].[FaceBookPosts]([DateLastRefreshed] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_FaceBookPosts_CredentialID]
    ON [dbo].[FaceBookPosts]([CredentialID] ASC);

