﻿CREATE TABLE [dbo].[UserActivityTypes] (
    [ID]   BIGINT       NOT NULL,
    [Name] VARCHAR (50) NOT NULL,
    CONSTRAINT [PK_UserActivityTypes] PRIMARY KEY CLUSTERED ([ID] ASC)
);

