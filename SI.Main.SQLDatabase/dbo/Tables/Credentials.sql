﻿CREATE TABLE [dbo].[Credentials] (
    [ID]                BIGINT        IDENTITY (1, 1) NOT NULL,
    [AccountID]         BIGINT        NOT NULL,
    [SocialAppID]       BIGINT        NOT NULL,
    [UniqueID]          VARCHAR (50)  NOT NULL,
    [URI]               VARCHAR (500) NOT NULL,
    [Token]             VARCHAR (600) NOT NULL,
    [TokenSecret]       VARCHAR (600) NULL,
    [ScreenName]        VARCHAR (100) NULL,
    [PictureURL]        VARCHAR (800) NULL,
    [IsValid]           BIT           NOT NULL,
    [IsActive]          BIT           NOT NULL,
    [DateLastValidated] DATETIME      NOT NULL,
    [DateCreated]       DATETIME      NOT NULL,
    [DateModified]      DATETIME      NOT NULL,
    CONSTRAINT [PK_SocialNetworkCredentials] PRIMARY KEY CLUSTERED ([ID] ASC),
    CONSTRAINT [FK_Credentials_Accounts] FOREIGN KEY ([AccountID]) REFERENCES [dbo].[Accounts] ([ID]),
    CONSTRAINT [FK_Credentials_SocialApps] FOREIGN KEY ([SocialAppID]) REFERENCES [dbo].[SocialApps] ([ID])
);




GO
CREATE NONCLUSTERED INDEX [IX_Credentials_SocialAppID]
    ON [dbo].[Credentials]([SocialAppID] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_Credentials_AccountID]
    ON [dbo].[Credentials]([AccountID] ASC);

