﻿CREATE TABLE [dbo].[ZipCodes] (
    [ID]                  BIGINT        IDENTITY (1, 1) NOT NULL,
    [StateID]             BIGINT        NOT NULL,
    [ZipCode]             NVARCHAR (50) NOT NULL,
    [Latitude]            FLOAT (53)    NULL,
    [Longitude]           FLOAT (53)    NULL,
    [TaxReturnsFiled]     INT           NULL,
    [TotalWages]          INT           NULL,
    [EstimatedPopulation] INT           NULL,
    [DateCreated]         DATETIME      NOT NULL,
    [DateModified]        DATETIME      NOT NULL,
    CONSTRAINT [PK_ZipCodes] PRIMARY KEY CLUSTERED ([ID] ASC),
    CONSTRAINT [FK_ZipCodes_States] FOREIGN KEY ([StateID]) REFERENCES [dbo].[States] ([ID])
);






GO



GO
CREATE NONCLUSTERED INDEX [IX_ZipCodes_StateID]
    ON [dbo].[ZipCodes]([StateID] ASC);


GO
CREATE UNIQUE NONCLUSTERED INDEX [IX_ZipCodes_ZipCode_StateID]
    ON [dbo].[ZipCodes]([ZipCode] ASC, [StateID] ASC);

