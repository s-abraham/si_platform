﻿CREATE TABLE [dbo].[NotificationMessageTypes] (
    [ID]                            BIGINT        NOT NULL,
    [Name]                          VARCHAR (50)  NOT NULL,
    [NotificationMessageCategoryID] BIGINT        NOT NULL,
    [SortOrder]                     INT           NOT NULL,
    [Description]                   VARCHAR (200) NULL,
    [RequireIndividualReads]        BIT           NOT NULL,
    CONSTRAINT [PK_NotificationMessageTypes] PRIMARY KEY CLUSTERED ([ID] ASC),
    CONSTRAINT [FK_NotificationMessageTypes_NotificationMessageCategories] FOREIGN KEY ([NotificationMessageCategoryID]) REFERENCES [dbo].[NotificationMessageCategories] ([ID])
);

