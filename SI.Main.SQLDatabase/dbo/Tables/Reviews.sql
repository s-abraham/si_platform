﻿CREATE TABLE [dbo].[Reviews] (
    [ID]             BIGINT         IDENTITY (1, 1) NOT NULL,
    [AccountID]      BIGINT         NOT NULL,
    [ReviewSourceID] BIGINT         NOT NULL,
    [Rating]         DECIMAL (5, 2) NULL,
    [ReviewerName]   VARCHAR (50)   NULL,
    [ReviewDate]     DATETIME       NULL,
    [ReviewText]     VARCHAR (MAX)  NULL,
    [ReviewURL]      VARCHAR (200)  NULL,
    [ExtraInfo]      VARCHAR (MAX)  NULL,
    [Filtered]       BIT            NOT NULL,
    [TestChunk1]     VARCHAR (MAX)  NULL,
    [TestChunk2]     VARCHAR (MAX)  NULL,
    [DateUpdated]    DATETIME       NOT NULL,
    [DateEntered]    DATETIME       NOT NULL,
    [RatingPolarity] AS             (CONVERT([bit],case when [Rating]=(0) then NULL when [Rating]<=(2.5) AND [Rating]>(0) then (0) when [Rating]>(2.5) then (1)  end,(0))) PERSISTED,
    CONSTRAINT [PK_Reviews] PRIMARY KEY CLUSTERED ([ID] ASC),
    CONSTRAINT [FK_Reviews_Accounts] FOREIGN KEY ([AccountID]) REFERENCES [dbo].[Accounts] ([ID]),
    CONSTRAINT [FK_Reviews_ReviewSources] FOREIGN KEY ([ReviewSourceID]) REFERENCES [dbo].[ReviewSources] ([ID])
);






GO
CREATE NONCLUSTERED INDEX [IX_Reviews_AccountID]
    ON [dbo].[Reviews]([AccountID] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_Reviews_AccountID_ReviewSourceID]
    ON [dbo].[Reviews]([AccountID] ASC, [ReviewSourceID] ASC);

