﻿CREATE TABLE [dbo].[NotificationMessageCategories] (
    [ID]        BIGINT       NOT NULL,
    [Name]      VARCHAR (50) NOT NULL,
    [SortOrder] INT          NOT NULL,
    CONSTRAINT [PK_NotificationMessageCategories] PRIMARY KEY CLUSTERED ([ID] ASC)
);

