﻿CREATE TABLE [dbo].[Products_FeatureTypes] (
    [ID]            BIGINT IDENTITY (1, 1) NOT NULL,
    [ProductID]     BIGINT NOT NULL,
    [FeatureTypeID] BIGINT NOT NULL,
    CONSTRAINT [PK_Products_FeatureTypes] PRIMARY KEY CLUSTERED ([ID] ASC),
    CONSTRAINT [FK_Products_FeatureTypes_FeatureTypes] FOREIGN KEY ([FeatureTypeID]) REFERENCES [dbo].[FeatureTypes] ([ID]),
    CONSTRAINT [FK_Products_FeatureTypes_Products] FOREIGN KEY ([ProductID]) REFERENCES [dbo].[Products] ([ID])
);


GO
CREATE UNIQUE NONCLUSTERED INDEX [IX_Products_FeatureTypes_ProductID_FeatureTypeID]
    ON [dbo].[Products_FeatureTypes]([ProductID] ASC, [FeatureTypeID] ASC);

