﻿CREATE TABLE [dbo].[Cities] (
    [ID]         BIGINT         IDENTITY (1, 1) NOT NULL,
    [Name]       NVARCHAR (100) NOT NULL,
    [StateID]    BIGINT         NOT NULL,
    [ZipCodeID]  BIGINT         NULL,
    [LanguageID] BIGINT         NULL,
    CONSTRAINT [PK_Cities] PRIMARY KEY CLUSTERED ([ID] ASC),
    CONSTRAINT [FK_Cities_Languages] FOREIGN KEY ([LanguageID]) REFERENCES [dbo].[Languages] ([ID]),
    CONSTRAINT [FK_Cities_States] FOREIGN KEY ([StateID]) REFERENCES [dbo].[States] ([ID]),
    CONSTRAINT [FK_Cities_ZipCodes] FOREIGN KEY ([ZipCodeID]) REFERENCES [dbo].[ZipCodes] ([ID])
);


GO
CREATE NONCLUSTERED INDEX [IX_Cities_StateID]
    ON [dbo].[Cities]([StateID] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_Cities_ZipCodeID]
    ON [dbo].[Cities]([ZipCodeID] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_Cities_Name]
    ON [dbo].[Cities]([Name] ASC);

