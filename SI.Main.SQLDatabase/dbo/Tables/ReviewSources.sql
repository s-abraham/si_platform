﻿CREATE TABLE [dbo].[ReviewSources] (
    [ID]           BIGINT        NOT NULL,
    [Name]         VARCHAR (100) NOT NULL,
    [DisplayOrder] INT           NOT NULL,
    [ImagePath]    VARCHAR (500) NULL,
    [Status]       INT           NOT NULL,
    CONSTRAINT [PK_ReviewSources] PRIMARY KEY CLUSTERED ([ID] ASC)
);



