﻿CREATE TABLE [dbo].[PermissionTypes] (
    [ID]   BIGINT       NOT NULL,
    [Name] VARCHAR (50) NOT NULL,
    [Code] VARCHAR (20) NOT NULL,
    CONSTRAINT [PK_PermissionTypes] PRIMARY KEY CLUSTERED ([ID] ASC)
);




GO
CREATE UNIQUE NONCLUSTERED INDEX [IX_PermissionTypes_Code]
    ON [dbo].[PermissionTypes]([Code] ASC);

