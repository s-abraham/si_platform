﻿CREATE TABLE [dbo].[CRMSystemTypes] (
    [ID]         BIGINT        IDENTITY (1, 1) NOT NULL,
    [Name]       NVARCHAR (50) NOT NULL,
    [VerticalID] BIGINT        NULL,
    CONSTRAINT [PK_CRMSystemTypes] PRIMARY KEY CLUSTERED ([ID] ASC),
    CONSTRAINT [FK_CRMSystemTypes_Verticals] FOREIGN KEY ([VerticalID]) REFERENCES [dbo].[Verticals] ([ID])
);

