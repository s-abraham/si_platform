﻿CREATE TABLE [dbo].[NotificationSubscriptions] (
    [ID]                     BIGINT        IDENTITY (1, 1) NOT NULL,
    [RecipientUserID]        BIGINT        NOT NULL,
    [AccountID]              BIGINT        NOT NULL,
    [NotificationTemplateID] BIGINT        NOT NULL,
    [ScheduleSpec]           VARCHAR (300) NOT NULL,
    CONSTRAINT [PK_NotificationSubscriptions] PRIMARY KEY CLUSTERED ([ID] ASC),
    CONSTRAINT [FK_NotificationSubscriptions_Accounts] FOREIGN KEY ([AccountID]) REFERENCES [dbo].[Accounts] ([ID]),
    CONSTRAINT [FK_NotificationSubscriptions_NotificationTemplates] FOREIGN KEY ([NotificationTemplateID]) REFERENCES [dbo].[NotificationTemplates] ([ID]),
    CONSTRAINT [FK_NotificationSubscriptions_Users] FOREIGN KEY ([RecipientUserID]) REFERENCES [dbo].[Users] ([ID])
);


GO
CREATE UNIQUE NONCLUSTERED INDEX [IX_NotificationSubscriptions_AccountID_NotificationTemplateID_RecipientUserID]
    ON [dbo].[NotificationSubscriptions]([AccountID] ASC, [NotificationTemplateID] ASC, [RecipientUserID] ASC);

