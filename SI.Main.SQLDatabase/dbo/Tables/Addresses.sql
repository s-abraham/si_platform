﻿CREATE TABLE [dbo].[Addresses] (
    [ID]        BIGINT         IDENTITY (1, 1) NOT NULL,
    [Street1]   NVARCHAR (100) NOT NULL,
    [Street2]   NVARCHAR (100) NULL,
    [Street3]   NVARCHAR (100) NULL,
    [CityID]    BIGINT         NOT NULL,
    [Latitude]  FLOAT (53)     NULL,
    [Longitude] FLOAT (53)     NULL,
    CONSTRAINT [PK_Addresses] PRIMARY KEY CLUSTERED ([ID] ASC),
    CONSTRAINT [FK_Addresses_Cities] FOREIGN KEY ([CityID]) REFERENCES [dbo].[Cities] ([ID])
);


GO
CREATE NONCLUSTERED INDEX [IX_Addresses_Street1]
    ON [dbo].[Addresses]([Street1] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_Addresses_CityID]
    ON [dbo].[Addresses]([CityID] ASC);

