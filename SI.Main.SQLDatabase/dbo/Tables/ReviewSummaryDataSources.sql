﻿CREATE TABLE [dbo].[ReviewSummaryDataSources] (
    [ID]   BIGINT       NOT NULL,
    [Name] VARCHAR (50) NOT NULL,
    CONSTRAINT [PK_ReviewSummaryDataSources] PRIMARY KEY CLUSTERED ([ID] ASC)
);


GO
CREATE UNIQUE NONCLUSTERED INDEX [IX_ReviewSummaryDataSources_Name]
    ON [dbo].[ReviewSummaryDataSources]([Name] ASC);

