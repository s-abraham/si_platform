﻿CREATE TABLE [dbo].[PostImageResults] (
    [ID]            BIGINT        IDENTITY (1, 1) NOT NULL,
    [PostImageID]   BIGINT        NOT NULL,
    [PostTargetID]  BIGINT        NOT NULL,
    [JobID]         BIGINT        NULL,
    [ResultID]      VARCHAR (250) NULL,
    [FeedID]        VARCHAR (250) NULL,
    [ProcessedDate] DATETIME      NULL,
    CONSTRAINT [PK_PostImageResults] PRIMARY KEY CLUSTERED ([ID] ASC),
    CONSTRAINT [FK_PostImageResults_PostImages] FOREIGN KEY ([PostImageID]) REFERENCES [dbo].[PostImages] ([ID]),
    CONSTRAINT [FK_PostImageResults_PostTargets] FOREIGN KEY ([PostTargetID]) REFERENCES [dbo].[PostTargets] ([ID])
);


GO
CREATE NONCLUSTERED INDEX [IX_PostImageResults_ProcessedDate]
    ON [dbo].[PostImageResults]([ProcessedDate] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_PostImageResults_PostImageID]
    ON [dbo].[PostImageResults]([PostImageID] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_PostImageResults_PostTargetID]
    ON [dbo].[PostImageResults]([PostTargetID] ASC);

