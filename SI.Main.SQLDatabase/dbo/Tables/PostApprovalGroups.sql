﻿CREATE TABLE [dbo].[PostApprovalGroups] (
    [ID]   BIGINT        IDENTITY (1, 1) NOT NULL,
    [Name] VARCHAR (100) NOT NULL,
    CONSTRAINT [PK_PostApprovalGroup] PRIMARY KEY CLUSTERED ([ID] ASC)
);

