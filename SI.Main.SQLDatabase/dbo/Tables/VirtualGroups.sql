﻿CREATE TABLE [dbo].[VirtualGroups] (
    [ID]                 BIGINT         IDENTITY (1, 1) NOT NULL,
    [VirtualGroupTypeID] BIGINT         NOT NULL,
    [Name]               NVARCHAR (250) NOT NULL,
    [DateCreated]        DATETIME       NOT NULL,
    [IsSystem]           BIT            NULL,
    CONSTRAINT [PK_VirtualGroups] PRIMARY KEY CLUSTERED ([ID] ASC),
    CONSTRAINT [FK_VirtualGroups_VirtualGroupTypes] FOREIGN KEY ([VirtualGroupTypeID]) REFERENCES [dbo].[VirtualGroupTypes] ([ID])
);



