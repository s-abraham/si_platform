﻿CREATE TABLE [dbo].[Accounts_Users] (
    [ID]        BIGINT IDENTITY (1, 1) NOT NULL,
    [AccountID] BIGINT NOT NULL,
    [UserID]    BIGINT NOT NULL,
    CONSTRAINT [PK_Accounts_Users] PRIMARY KEY CLUSTERED ([ID] ASC),
    CONSTRAINT [FK_Accounts_Users_Accounts] FOREIGN KEY ([AccountID]) REFERENCES [dbo].[Accounts] ([ID]),
    CONSTRAINT [FK_Accounts_Users_Users] FOREIGN KEY ([UserID]) REFERENCES [dbo].[Users] ([ID])
);


GO
CREATE UNIQUE NONCLUSTERED INDEX [IX_Accounts_Users_AccountID_UserID]
    ON [dbo].[Accounts_Users]([AccountID] ASC, [UserID] ASC);

