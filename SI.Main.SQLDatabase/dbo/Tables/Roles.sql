﻿CREATE TABLE [dbo].[Roles] (
    [ID]              BIGINT IDENTITY (1, 1) NOT NULL,
    [Account_UserID]  BIGINT NOT NULL,
    [RoleTypeID]      BIGINT NOT NULL,
    [IncludeChildren] BIT    NOT NULL,
    CONSTRAINT [PK_Roles_1] PRIMARY KEY CLUSTERED ([ID] ASC),
    CONSTRAINT [FK_Roles_Accounts_Users] FOREIGN KEY ([Account_UserID]) REFERENCES [dbo].[Accounts_Users] ([ID]),
    CONSTRAINT [FK_Roles_RoleTypes] FOREIGN KEY ([RoleTypeID]) REFERENCES [dbo].[RoleTypes] ([ID])
);


GO
CREATE UNIQUE NONCLUSTERED INDEX [IX_Roles_Account-UserID_RoleTypeID]
    ON [dbo].[Roles]([Account_UserID] ASC, [RoleTypeID] ASC);

