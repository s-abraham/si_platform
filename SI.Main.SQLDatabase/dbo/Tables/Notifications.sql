﻿CREATE TABLE [dbo].[Notifications] (
    [ID]                        BIGINT   IDENTITY (1, 1) NOT NULL,
    [NotificationMessageTypeID] BIGINT   NOT NULL,
    [AccountID]                 BIGINT   NOT NULL,
    [EntityID]                  BIGINT   NOT NULL,
    [DateCreated]               DATETIME NOT NULL,
    CONSTRAINT [PK_Notifications] PRIMARY KEY CLUSTERED ([ID] ASC),
    CONSTRAINT [FK_Notifications_Accounts] FOREIGN KEY ([AccountID]) REFERENCES [dbo].[Accounts] ([ID]),
    CONSTRAINT [FK_Notifications_NotificationMessageTypes] FOREIGN KEY ([NotificationMessageTypeID]) REFERENCES [dbo].[NotificationMessageTypes] ([ID])
);

