﻿CREATE TABLE [dbo].[RoleTypes] (
    [ID]          BIGINT        NOT NULL,
    [Name]        NVARCHAR (50) NOT NULL,
    [AccountID]   BIGINT        NULL,
    [Hidden]      BIT           NOT NULL,
    [DateCreated] DATETIME      NOT NULL,
    CONSTRAINT [PK_Roles] PRIMARY KEY CLUSTERED ([ID] ASC),
    CONSTRAINT [FK_RoleTypes_Accounts] FOREIGN KEY ([AccountID]) REFERENCES [dbo].[Accounts] ([ID])
);



