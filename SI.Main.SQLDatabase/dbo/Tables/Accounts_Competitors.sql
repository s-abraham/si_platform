﻿CREATE TABLE [dbo].[Accounts_Competitors] (
    [ID]                  BIGINT   IDENTITY (1, 1) NOT NULL,
    [AccountID]           BIGINT   NOT NULL,
    [CompetitorAccountID] BIGINT   NOT NULL,
    [Active]              BIT      CONSTRAINT [DF_Accounts_Competitors_Active] DEFAULT ((1)) NOT NULL,
    [DateEntered]         DATETIME CONSTRAINT [DF_Accounts_Competitors] DEFAULT (getdate()) NOT NULL,
    CONSTRAINT [PK_Accounts_Competitors] PRIMARY KEY CLUSTERED ([ID] ASC),
    CONSTRAINT [FK_Accounts_Competitors_Accounts] FOREIGN KEY ([AccountID]) REFERENCES [dbo].[Accounts] ([ID]),
    CONSTRAINT [FK_Accounts_Competitors_Competitors] FOREIGN KEY ([CompetitorAccountID]) REFERENCES [dbo].[Accounts] ([ID])
);

