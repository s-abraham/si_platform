﻿CREATE TABLE [dbo].[Packages] (
    [ID]          BIGINT         IDENTITY (1, 1) NOT NULL,
    [Name]        NVARCHAR (100) NOT NULL,
    [DateCreated] DATETIME       NOT NULL,
    CONSTRAINT [PK_Packages] PRIMARY KEY CLUSTERED ([ID] ASC)
);



