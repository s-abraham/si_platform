﻿CREATE TABLE [dbo].[AccountTypes] (
    [ID]   BIGINT       IDENTITY (1, 1) NOT NULL,
    [Name] VARCHAR (50) NOT NULL,
    CONSTRAINT [PK_AccountTypes] PRIMARY KEY CLUSTERED ([ID] ASC)
);

