﻿CREATE TABLE [dbo].[_UserActivityLogs] (
    [ID]                 BIGINT           IDENTITY (1, 1) NOT NULL,
    [UserID]             BIGINT           NOT NULL,
    [ImpersonatedUserID] BIGINT           NULL,
    [UserActivityTypeID] BIGINT           NOT NULL,
    [UserIDActedUpon]    BIGINT           NULL,
    [AccountID]          BIGINT           NULL,
    [RoleTypeID]         BIGINT           NULL,
    [ReviewSourceID]     BIGINT           NULL,
    [SSOToken]           UNIQUEIDENTIFIER NULL,
    [CredentialID]       BIGINT           NULL,
    [Description]        VARCHAR (250)    NULL,
    [ActivityDate]       DATETIME         NOT NULL,
    CONSTRAINT [PK__UserActivityLogs] PRIMARY KEY CLUSTERED ([ID] ASC)
);

