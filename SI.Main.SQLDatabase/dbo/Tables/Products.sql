﻿CREATE TABLE [dbo].[Products] (
    [ID]          BIGINT        IDENTITY (1, 1) NOT NULL,
    [Name]        VARCHAR (100) NOT NULL,
    [Active]      BIT           NOT NULL,
    [DateCreated] DATETIME      NOT NULL,
    CONSTRAINT [PK_Products] PRIMARY KEY CLUSTERED ([ID] ASC)
);

