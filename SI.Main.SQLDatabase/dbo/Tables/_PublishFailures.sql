﻿CREATE TABLE [dbo].[_PublishFailures] (
    [ID]                    BIGINT        IDENTITY (1, 1) NOT NULL,
    [JobID]                 BIGINT        NOT NULL,
    [PostTargetID]          BIGINT        NOT NULL,
    [CredentialID]          BIGINT        NOT NULL,
    [SocialAppID]           BIGINT        NOT NULL,
    [SocialNetworkID]       BIGINT        NOT NULL,
    [AccountID]             BIGINT        NOT NULL,
    [PostImageID]           BIGINT        NULL,
    [SocialNetworkResponse] VARCHAR (MAX) NULL,
    [DateCreated]           DATETIME      NOT NULL,
    CONSTRAINT [PK__PublishFailures] PRIMARY KEY CLUSTERED ([ID] ASC)
);


GO
CREATE NONCLUSTERED INDEX [IX__PublishFailures_SocialNetworkID]
    ON [dbo].[_PublishFailures]([SocialNetworkID] ASC);


GO
CREATE NONCLUSTERED INDEX [IX__PublishFailures_SocialAppID]
    ON [dbo].[_PublishFailures]([SocialAppID] ASC);


GO
CREATE NONCLUSTERED INDEX [IX__PublishFailures_PostImageID]
    ON [dbo].[_PublishFailures]([PostImageID] ASC);


GO
CREATE NONCLUSTERED INDEX [IX__PublishFailures_PostTargetID]
    ON [dbo].[_PublishFailures]([PostTargetID] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_PublishFailures_JobID]
    ON [dbo].[_PublishFailures]([JobID] ASC);

