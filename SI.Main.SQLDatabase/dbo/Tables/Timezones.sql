﻿CREATE TABLE [dbo].[Timezones] (
    [ID]                 BIGINT         IDENTITY (1, 1) NOT NULL,
    [MicrosoftID]        VARCHAR (100)  NOT NULL,
    [Name]               VARCHAR (200)  NOT NULL,
    [Active]             BIT            NOT NULL,
    [BaseUTCOffsetHours] DECIMAL (5, 2) NOT NULL,
    CONSTRAINT [PK_Timezones] PRIMARY KEY CLUSTERED ([ID] ASC)
);

