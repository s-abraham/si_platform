﻿CREATE TABLE [dbo].[Phones] (
    [ID]          BIGINT        IDENTITY (1, 1) NOT NULL,
    [PhoneTypeID] BIGINT        NOT NULL,
    [Number]      NVARCHAR (50) NOT NULL,
    [DateCreated] DATETIME      NULL,
    CONSTRAINT [PK_Phones] PRIMARY KEY CLUSTERED ([ID] ASC),
    CONSTRAINT [FK_Phones_PhoneTypes] FOREIGN KEY ([PhoneTypeID]) REFERENCES [dbo].[PhoneTypes] ([ID])
);


GO
CREATE NONCLUSTERED INDEX [IX_Phones_Number]
    ON [dbo].[Phones]([Number] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_Phones_PhoneTypeID]
    ON [dbo].[Phones]([PhoneTypeID] ASC);

