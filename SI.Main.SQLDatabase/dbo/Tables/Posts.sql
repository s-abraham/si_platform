﻿CREATE TABLE [dbo].[Posts] (
    [ID]                     BIGINT         IDENTITY (1, 1) NOT NULL,
    [Title]                  NVARCHAR (150) NOT NULL,
    [PostSourceID]           BIGINT         NOT NULL,
    [UserID]                 BIGINT         NOT NULL,
    [AccountID]              BIGINT         NOT NULL,
    [PostCategoryID]         BIGINT         NOT NULL,
    [PostTypeID]             BIGINT         NOT NULL,
    [DateCreated]            DATETIME       NOT NULL,
    [DateModified]           DATETIME       NOT NULL,
    [ScheduleDate]           DATETIME       NULL,
    [PostExpirationDate]     DATETIME       NULL,
    [Draft]                  BIT            NOT NULL,
    [ApprovalExpirationDate] DATETIME       NULL,
    CONSTRAINT [PK_Posts] PRIMARY KEY CLUSTERED ([ID] ASC),
    CONSTRAINT [FK_Posts_Accounts] FOREIGN KEY ([AccountID]) REFERENCES [dbo].[Accounts] ([ID]),
    CONSTRAINT [FK_Posts_PostCategories] FOREIGN KEY ([PostCategoryID]) REFERENCES [dbo].[PostCategories] ([ID]),
    CONSTRAINT [FK_Posts_PostSources] FOREIGN KEY ([PostSourceID]) REFERENCES [dbo].[PostSources] ([ID]),
    CONSTRAINT [FK_Posts_PostTypes] FOREIGN KEY ([PostTypeID]) REFERENCES [dbo].[PostTypes] ([ID]),
    CONSTRAINT [FK_Posts_Users] FOREIGN KEY ([UserID]) REFERENCES [dbo].[Users] ([ID])
);




GO
CREATE NONCLUSTERED INDEX [IX_Posts_AccountID]
    ON [dbo].[Posts]([AccountID] ASC);

