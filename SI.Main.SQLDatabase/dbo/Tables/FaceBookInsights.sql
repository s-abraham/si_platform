﻿CREATE TABLE [dbo].[FaceBookInsights] (
    [ID]                      BIGINT         IDENTITY (1, 1) NOT NULL,
    [FaceBookInsightsNamesID] BIGINT         NOT NULL,
    [CredentialID]            BIGINT         NOT NULL,
    [FacebookPostID]          BIGINT         NULL,
    [ValueJSON]               VARCHAR (5000) NULL,
    [Value]                   INT            NULL,
    [QueryDate]               DATE           NOT NULL,
    [DateCreated]             DATETIME       CONSTRAINT [DF__FaceBookI__DateC__7FED5F43] DEFAULT (getdate()) NOT NULL,
    CONSTRAINT [PK_FACEBOOKINSIGHTS] PRIMARY KEY CLUSTERED ([ID] ASC),
    CONSTRAINT [FK_FACEBOOKINSIGHTSNAME_FACEBOOKINSIGHTS] FOREIGN KEY ([FaceBookInsightsNamesID]) REFERENCES [dbo].[FaceBookInsightsNames] ([ID])
);


GO
CREATE UNIQUE NONCLUSTERED INDEX [IX_FaceBookInsights_CredentialID_FacebookPostID_QueryDate_ FacebookPostID (ASC), FaceBookInsightsNamesID]
    ON [dbo].[FaceBookInsights]([CredentialID] ASC, [QueryDate] ASC, [FacebookPostID] ASC, [FaceBookInsightsNamesID] ASC);


GO
CREATE NONCLUSTERED INDEX [FK_FACEBOOKINSIGHTSNAME_FACEBOOKINSIGHTS]
    ON [dbo].[FaceBookInsights]([FaceBookInsightsNamesID] ASC);

