﻿CREATE TABLE [dbo].[PostImages] (
    [ID]     BIGINT        IDENTITY (1, 1) NOT NULL,
    [PostID] BIGINT        NOT NULL,
    [URL]    VARCHAR (300) NOT NULL,
    CONSTRAINT [PK_PostImages] PRIMARY KEY CLUSTERED ([ID] ASC),
    CONSTRAINT [FK_PostImages_Posts] FOREIGN KEY ([PostID]) REFERENCES [dbo].[Posts] ([ID])
);


GO
CREATE NONCLUSTERED INDEX [IX_PostImages_PostID]
    ON [dbo].[PostImages]([PostID] ASC);

