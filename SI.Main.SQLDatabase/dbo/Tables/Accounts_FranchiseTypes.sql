﻿CREATE TABLE [dbo].[Accounts_FranchiseTypes] (
    [ID]              BIGINT IDENTITY (1, 1) NOT NULL,
    [AccountID]       BIGINT NOT NULL,
    [FranchiseTypeID] BIGINT NOT NULL,
    CONSTRAINT [PK_Accounts_FranchiseTypes] PRIMARY KEY CLUSTERED ([ID] ASC),
    CONSTRAINT [FK_Accounts_FranchiseTypes_Accounts] FOREIGN KEY ([AccountID]) REFERENCES [dbo].[Accounts] ([ID]),
    CONSTRAINT [FK_Accounts_FranchiseTypes_FranchiseTypes] FOREIGN KEY ([FranchiseTypeID]) REFERENCES [dbo].[FranchiseTypes] ([ID])
);


GO
CREATE UNIQUE NONCLUSTERED INDEX [IX_Accounts_FranchiseTypes_AccountID_FranchiseTypeID]
    ON [dbo].[Accounts_FranchiseTypes]([AccountID] ASC, [FranchiseTypeID] ASC);

