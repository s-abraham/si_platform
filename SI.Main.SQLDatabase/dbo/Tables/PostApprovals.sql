﻿CREATE TABLE [dbo].[PostApprovals] (
    [ID]                  BIGINT   IDENTITY (1, 1) NOT NULL,
    [PostTargetID]        BIGINT   NOT NULL,
    [ApprovalOrder]       INT      NOT NULL,
    [UserID]              BIGINT   NULL,
    [PostApprovalGroupID] BIGINT   NULL,
    [DateApproved]        DATETIME NULL,
    CONSTRAINT [PK_PostApprovals] PRIMARY KEY CLUSTERED ([ID] ASC),
    CONSTRAINT [FK_PostApprovals_PostApprovalGroups] FOREIGN KEY ([PostApprovalGroupID]) REFERENCES [dbo].[PostApprovalGroups] ([ID]),
    CONSTRAINT [FK_PostApprovals_PostTargets] FOREIGN KEY ([PostTargetID]) REFERENCES [dbo].[PostTargets] ([ID]),
    CONSTRAINT [FK_PostApprovals_Users] FOREIGN KEY ([UserID]) REFERENCES [dbo].[Users] ([ID])
);


GO
CREATE NONCLUSTERED INDEX [IX_PostApprovals_PostApprovalGroupID]
    ON [dbo].[PostApprovals]([PostApprovalGroupID] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_PostApprovals_UserID]
    ON [dbo].[PostApprovals]([UserID] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_PostApprovals_PostTargetID_DateApproved]
    ON [dbo].[PostApprovals]([PostTargetID] ASC, [DateApproved] ASC);

