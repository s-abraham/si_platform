﻿CREATE TABLE [dbo].[Verticals] (
    [ID]   BIGINT        IDENTITY (1, 1) NOT NULL,
    [Name] VARCHAR (100) NOT NULL,
    CONSTRAINT [PK_Verticles] PRIMARY KEY CLUSTERED ([ID] ASC)
);

