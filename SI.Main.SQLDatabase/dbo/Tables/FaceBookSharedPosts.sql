﻿CREATE TABLE [dbo].[FaceBookSharedPosts] (
    [ID]                   BIGINT         IDENTITY (1, 1) NOT NULL,
    [FaceBookPostsID]      BIGINT         NOT NULL,
    [FaceBookIdentitiesID] BIGINT         NOT NULL,
    [FBSharedPostID]       VARCHAR (100)  NULL,
    [FBCreateTime]         DATETIME       NULL,
    [Message]              NVARCHAR (MAX) NULL,
    [DateCreated]          DATETIME       CONSTRAINT [DF__FaceBookS__DateC__0D475A61] DEFAULT (getdate()) NOT NULL,
    CONSTRAINT [PK_FACEBOOKSHAREDPOST] PRIMARY KEY CLUSTERED ([ID] ASC),
    CONSTRAINT [FK_FACEBOOKIDENTITIES_FACEBOOKSHAREDPOSTS] FOREIGN KEY ([FaceBookIdentitiesID]) REFERENCES [dbo].[FaceBookIdentities] ([ID]),
    CONSTRAINT [RefFaceBookPosts41] FOREIGN KEY ([FaceBookPostsID]) REFERENCES [dbo].[FaceBookPosts] ([ID])
);


GO
CREATE UNIQUE NONCLUSTERED INDEX [IX_FaceBookSharedPosts_FBSharedPostID]
    ON [dbo].[FaceBookSharedPosts]([FBSharedPostID] ASC);


GO
CREATE NONCLUSTERED INDEX [FK_FACEBOOKPOST_FACEBOOKSHAREDPOSTS]
    ON [dbo].[FaceBookSharedPosts]([FaceBookPostsID] ASC);


GO
CREATE NONCLUSTERED INDEX [FK_FACEBOOKIDENTITIES_FACEBOOKSHAREDPOSTS]
    ON [dbo].[FaceBookSharedPosts]([FaceBookIdentitiesID] ASC);

