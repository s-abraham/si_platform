﻿CREATE TABLE [dbo].[FaceBookInsightsNames] (
    [ID]          BIGINT         IDENTITY (1, 1) NOT NULL,
    [Name]        VARCHAR (100)  NULL,
    [Period]      VARCHAR (50)   NULL,
    [PageOrPost]  VARCHAR (25)   NULL,
    [Title]       VARCHAR (100)  NULL,
    [Description] VARCHAR (1000) NULL,
    [DateCreated] DATETIME       CONSTRAINT [DF__FaceBookI__DateC__02C9CBEE] DEFAULT (getdate()) NOT NULL,
    CONSTRAINT [PK_FACEBOOKINSIGHTSNAME] PRIMARY KEY CLUSTERED ([ID] ASC)
);


GO
CREATE NONCLUSTERED INDEX [IX_FaceBookInsightsNames_Name_Period_PageOrPost]
    ON [dbo].[FaceBookInsightsNames]([Name] ASC, [Period] ASC, [PageOrPost] ASC);

