﻿CREATE TABLE [dbo].[Countries] (
    [ID]         BIGINT         IDENTITY (1, 1) NOT NULL,
    [Name]       NVARCHAR (100) NOT NULL,
    [Prefix]     NVARCHAR (3)   NOT NULL,
    [LanguageID] BIGINT         NULL,
    CONSTRAINT [PK_Countries] PRIMARY KEY CLUSTERED ([ID] ASC),
    CONSTRAINT [FK_Countries_Languages] FOREIGN KEY ([LanguageID]) REFERENCES [dbo].[Languages] ([ID])
);


GO
CREATE NONCLUSTERED INDEX [IX_Countries_Prefix]
    ON [dbo].[Countries]([Prefix] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_Countries_Name]
    ON [dbo].[Countries]([Name] ASC);

