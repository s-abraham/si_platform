﻿CREATE TABLE [dbo].[_NewReviewsToNotify] (
    [ID]          BIGINT   IDENTITY (1, 1) NOT NULL,
    [ReviewID]    BIGINT   NOT NULL,
    [DateCreated] DATETIME NOT NULL,
    CONSTRAINT [PK__NewReviewsToNotify] PRIMARY KEY CLUSTERED ([ID] ASC)
);

