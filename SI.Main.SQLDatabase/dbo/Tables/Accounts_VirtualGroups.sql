﻿CREATE TABLE [dbo].[Accounts_VirtualGroups] (
    [ID]             BIGINT IDENTITY (1, 1) NOT NULL,
    [AccountID]      BIGINT NOT NULL,
    [VirtualGroupID] BIGINT NOT NULL,
    CONSTRAINT [PK_Accounts_VirtualGroups] PRIMARY KEY CLUSTERED ([ID] ASC),
    CONSTRAINT [FK_Accounts_VirtualGroups_Accounts] FOREIGN KEY ([AccountID]) REFERENCES [dbo].[Accounts] ([ID]),
    CONSTRAINT [FK_Accounts_VirtualGroups_VirtualGroups] FOREIGN KEY ([VirtualGroupID]) REFERENCES [dbo].[VirtualGroups] ([ID])
);


GO
CREATE UNIQUE NONCLUSTERED INDEX [IX_Accounts_VirtualGroups_AccountID_VirtualGroupID]
    ON [dbo].[Accounts_VirtualGroups]([AccountID] ASC, [VirtualGroupID] ASC);

