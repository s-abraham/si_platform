﻿CREATE TABLE [dbo].[PostSources] (
    [ID]   BIGINT       IDENTITY (1, 1) NOT NULL,
    [Name] VARCHAR (50) NOT NULL,
    CONSTRAINT [PK_PostSources] PRIMARY KEY CLUSTERED ([ID] ASC)
);

