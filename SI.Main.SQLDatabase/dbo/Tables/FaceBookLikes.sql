﻿CREATE TABLE [dbo].[FaceBookLikes] (
    [ID]                   BIGINT   IDENTITY (1, 1) NOT NULL,
    [FaceBookPostsID]      BIGINT   NULL,
    [FaceBookCommentsID]   BIGINT   NULL,
    [FaceBookIdentitiesID] BIGINT   NOT NULL,
    [Unlike]               BIT      NULL,
    [DateCreated]          DATETIME NULL,
    CONSTRAINT [PK_FACEBOOKLIKE] PRIMARY KEY CLUSTERED ([ID] ASC),
    CONSTRAINT [FK_FACEBOOKIDENTITIES_FACEBOOKLIKES] FOREIGN KEY ([FaceBookIdentitiesID]) REFERENCES [dbo].[FaceBookIdentities] ([ID])
);


GO
CREATE NONCLUSTERED INDEX [FK_FACEBOOKPOST_FACEBOOKLIKES]
    ON [dbo].[FaceBookLikes]([FaceBookPostsID] ASC);


GO
CREATE NONCLUSTERED INDEX [FK_FACEBOOKCOMMENT_FACEBOOKLIKES]
    ON [dbo].[FaceBookLikes]([FaceBookCommentsID] ASC);


GO
CREATE NONCLUSTERED INDEX [FK_FACEBOOKIDENTITIES_FACEBOOKLIKES]
    ON [dbo].[FaceBookLikes]([FaceBookIdentitiesID] ASC);

