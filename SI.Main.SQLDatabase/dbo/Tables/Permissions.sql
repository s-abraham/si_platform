﻿CREATE TABLE [dbo].[Permissions] (
    [ID]               BIGINT IDENTITY (1, 1) NOT NULL,
    [Account_UserID]   BIGINT NOT NULL,
    [PermissionTypeID] BIGINT NOT NULL,
    [IncludeChildren]  BIT    NOT NULL,
    CONSTRAINT [PK_Permissions] PRIMARY KEY CLUSTERED ([ID] ASC),
    CONSTRAINT [FK_Permissions_Accounts_Users] FOREIGN KEY ([Account_UserID]) REFERENCES [dbo].[Accounts_Users] ([ID]),
    CONSTRAINT [FK_Permissions_PermissionTypes] FOREIGN KEY ([PermissionTypeID]) REFERENCES [dbo].[PermissionTypes] ([ID])
);

