﻿CREATE TABLE [dbo].[NotificationTargets] (
    [ID]                   BIGINT        IDENTITY (1, 1) NOT NULL,
    [NotificationMethodID] BIGINT        NOT NULL,
    [Subject]              VARCHAR (250) NULL,
    [Body]                 VARCHAR (MAX) NULL,
    [Address]              VARCHAR (250) NULL,
    [NotificationStatusID] BIGINT        NULL,
    [DateSent]             DATETIME      NULL,
    [DateRead]             DATETIME      NULL,
    CONSTRAINT [PK_NotificationTargets] PRIMARY KEY CLUSTERED ([ID] ASC),
    CONSTRAINT [FK_NotificationTargets_NotificationMethods] FOREIGN KEY ([NotificationMethodID]) REFERENCES [dbo].[NotificationMethods] ([ID]),
    CONSTRAINT [FK_NotificationTargets_NotificationTargets] FOREIGN KEY ([NotificationStatusID]) REFERENCES [dbo].[NotificationStatuses] ([ID])
);

