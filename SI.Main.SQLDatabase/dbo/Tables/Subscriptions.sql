﻿CREATE TABLE [dbo].[Subscriptions] (
    [ID]                 BIGINT          IDENTITY (1, 1) NOT NULL,
    [AccountID]          BIGINT          NOT NULL,
    [Reseller_PackageID] BIGINT          NOT NULL,
    [Price]              DECIMAL (19, 4) NOT NULL,
    [StartDate]          DATE            NULL,
    [EndDate]            DATE            NULL,
    [Active]             BIT             NOT NULL,
    [DateCreated]        DATETIME        NOT NULL,
    CONSTRAINT [PK_Subscriptions] PRIMARY KEY CLUSTERED ([ID] ASC),
    CONSTRAINT [FK_Subscriptions_Accounts] FOREIGN KEY ([AccountID]) REFERENCES [dbo].[Accounts] ([ID]),
    CONSTRAINT [FK_Subscriptions_Resellers_Packages] FOREIGN KEY ([Reseller_PackageID]) REFERENCES [dbo].[Resellers_Packages] ([ID])
);


GO
CREATE NONCLUSTERED INDEX [IX_Subscriptions_ResellerPackageID_StartDate_EndDate]
    ON [dbo].[Subscriptions]([Reseller_PackageID] ASC, [StartDate] ASC, [EndDate] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_Subscriptions_AccountID_StartDate_EndDate]
    ON [dbo].[Subscriptions]([AccountID] ASC, [StartDate] ASC, [EndDate] ASC);

