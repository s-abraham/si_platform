﻿CREATE TABLE [dbo].[ReviewSummaries] (
    [ID]                        BIGINT         IDENTITY (1, 1) NOT NULL,
    [ReviewSummaryDataSourceID] BIGINT         NOT NULL,
    [AccountID]                 BIGINT         NOT NULL,
    [ReviewSourceID]            BIGINT         NOT NULL,
    [SalesAverageRating]        DECIMAL (6, 2) NULL,
    [ServiceAverageRating]      DECIMAL (6, 2) NULL,
    [SalesReviewCount]          INT            NULL,
    [ServiceReviewCount]        INT            NULL,
    [ExtraInfo]                 VARCHAR (MAX)  NULL,
    [NewReviewsCount]           INT            NULL,
    [NewReviewsAverageRating]   DECIMAL (6, 2) NULL,
    [NewReviewsRatingSum]       DECIMAL (6, 2) NULL,
    [NewReviewsPositiveCount]   INT            NULL,
    [NewReviewsNegativeCount]   INT            NULL,
    [TotalPositiveReviews]      INT            NULL,
    [TotalNegativeReviews]      INT            NULL,
    [SummaryDate]               DATETIME       NOT NULL,
    [DateUpdated]               DATETIME       NOT NULL,
    [DateEntered]               DATETIME       NOT NULL,
    CONSTRAINT [PK_ReviewSummaries] PRIMARY KEY CLUSTERED ([ID] ASC)
);






GO
CREATE NONCLUSTERED INDEX [IX_ReviewSummaries_AccountID]
    ON [dbo].[ReviewSummaries]([AccountID] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_ReviewSummaries_AccountID_ReviewSourceID]
    ON [dbo].[ReviewSummaries]([AccountID] ASC, [ReviewSourceID] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_ReviewSummaries_SummaryDate]
    ON [dbo].[ReviewSummaries]([SummaryDate] ASC);


GO
CREATE UNIQUE NONCLUSTERED INDEX [IX_ReviewSummaries_AccountID_ReviewSourceID_SummaryDate]
    ON [dbo].[ReviewSummaries]([AccountID] ASC, [ReviewSourceID] ASC, [SummaryDate] ASC);

