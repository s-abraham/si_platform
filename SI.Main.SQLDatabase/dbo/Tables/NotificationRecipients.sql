﻿CREATE TABLE [dbo].[NotificationRecipients] (
    [ID]                     BIGINT IDENTITY (1, 1) NOT NULL,
    [NotificationID]         BIGINT NOT NULL,
    [UserID]                 BIGINT NOT NULL,
    [NotificationTemplateID] BIGINT NOT NULL,
    [NotificationTargetID]   BIGINT NULL,
    CONSTRAINT [PK_NotificationRecipients] PRIMARY KEY CLUSTERED ([ID] ASC),
    CONSTRAINT [FK_NotificationRecipients_Notifications] FOREIGN KEY ([NotificationID]) REFERENCES [dbo].[Notifications] ([ID]),
    CONSTRAINT [FK_NotificationRecipients_NotificationTargets] FOREIGN KEY ([NotificationTargetID]) REFERENCES [dbo].[NotificationTargets] ([ID]),
    CONSTRAINT [FK_NotificationRecipients_NotificationTemplates] FOREIGN KEY ([NotificationTemplateID]) REFERENCES [dbo].[NotificationTemplates] ([ID]),
    CONSTRAINT [FK_NotificationRecipients_Users] FOREIGN KEY ([UserID]) REFERENCES [dbo].[Users] ([ID])
);

