﻿CREATE TABLE [dbo].[ReviewComments] (
    [ID]            BIGINT        IDENTITY (1, 1) NOT NULL,
    [ReviewID]      BIGINT        NOT NULL,
    [CommenterName] VARCHAR (50)  NULL,
    [Comment]       VARCHAR (MAX) NOT NULL,
    [CommentDate]   DATETIME      NULL,
    CONSTRAINT [PK_ReviewComments] PRIMARY KEY CLUSTERED ([ID] ASC)
);



