﻿CREATE TABLE [dbo].[SocialApps] (
    [ID]              BIGINT        IDENTITY (1, 1) NOT NULL,
    [ResellerID]      BIGINT        NOT NULL,
    [SocialNetworkID] BIGINT        NOT NULL,
    [AppVersion]      INT           NOT NULL,
    [CallbackURL]     VARCHAR (300) NULL,
    [AppID]           VARCHAR (100) NOT NULL,
    [AppSecret]       VARCHAR (250) NULL,
    [Scope]           VARCHAR (MAX) NULL,
    [Active]          BIT           NOT NULL,
    [DateCreated]     DATETIME      NOT NULL,
    [DateModified]    DATETIME      NOT NULL,
    [AppToken]        VARCHAR (500) NULL,
    [AppTokenPageID]  VARCHAR (50)  NULL,
    CONSTRAINT [PK_SocialApps] PRIMARY KEY CLUSTERED ([ID] ASC),
    CONSTRAINT [FK_SocialApps_Resellers] FOREIGN KEY ([ResellerID]) REFERENCES [dbo].[Resellers] ([ID]),
    CONSTRAINT [FK_SocialApps_SocialNetworks] FOREIGN KEY ([SocialNetworkID]) REFERENCES [dbo].[SocialNetworks] ([ID])
);




GO
CREATE UNIQUE NONCLUSTERED INDEX [IX_SocialApps_ResellerID_SocialNetworkID_AppVersion]
    ON [dbo].[SocialApps]([ResellerID] ASC, [SocialNetworkID] ASC, [AppVersion] ASC);

