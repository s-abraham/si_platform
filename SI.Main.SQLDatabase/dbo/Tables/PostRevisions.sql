﻿CREATE TABLE [dbo].[PostRevisions] (
    [ID]              BIGINT        IDENTITY (1, 1) NOT NULL,
    [PostID]          BIGINT        NOT NULL,
    [PostTargetID]    BIGINT        NULL,
    [Message]         VARCHAR (MAX) NULL,
    [Link]            VARCHAR (500) NULL,
    [ImageURL]        VARCHAR (300) NULL,
    [LinkDescription] VARCHAR (MAX) NULL,
    [LinkTitle]       VARCHAR (250) NULL,
    [DateCreated]     DATETIME      NOT NULL,
    CONSTRAINT [PK_PostRevisions] PRIMARY KEY CLUSTERED ([ID] ASC),
    CONSTRAINT [FK_PostRevisions_Posts] FOREIGN KEY ([PostID]) REFERENCES [dbo].[Posts] ([ID])
);


GO
CREATE NONCLUSTERED INDEX [IX_PostRevisions_PostTargetID]
    ON [dbo].[PostRevisions]([PostTargetID] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_PostRevisions_PostID]
    ON [dbo].[PostRevisions]([PostID] ASC);

