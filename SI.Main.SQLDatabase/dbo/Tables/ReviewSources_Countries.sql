﻿CREATE TABLE [dbo].[ReviewSources_Countries] (
    [ID]             BIGINT IDENTITY (1, 1) NOT NULL,
    [ReviewSourceID] BIGINT NOT NULL,
    [CountryID]      BIGINT NOT NULL,
    CONSTRAINT [PK_ReviewSources_Countries] PRIMARY KEY CLUSTERED ([ID] ASC),
    CONSTRAINT [FK_ReviewSources_Countries_Countries] FOREIGN KEY ([CountryID]) REFERENCES [dbo].[Countries] ([ID]),
    CONSTRAINT [FK_ReviewSources_Countries_ReviewSources] FOREIGN KEY ([ReviewSourceID]) REFERENCES [dbo].[ReviewSources] ([ID])
);


GO
CREATE UNIQUE NONCLUSTERED INDEX [IX_ReviewSources_Countries_CountryID_ReviewSourceID]
    ON [dbo].[ReviewSources_Countries]([CountryID] ASC, [ReviewSourceID] ASC);

