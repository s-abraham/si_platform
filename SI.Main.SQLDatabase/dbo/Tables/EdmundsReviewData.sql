﻿CREATE TABLE [dbo].[EdmundsReviewData] (
    [ID]                 BIGINT          IDENTITY (1, 1) NOT NULL,
    [AccountID]          BIGINT          NOT NULL,
    [AvgRatingSales]     DECIMAL (18, 2) NOT NULL,
    [AvgRatingService]   DECIMAL (18, 2) NOT NULL,
    [RatingCountSales]   INT             NOT NULL,
    [RatingCountService] INT             NOT NULL,
    [DateCreated]        DATETIME        NOT NULL,
    [DateModified]       DATETIME        NOT NULL,
    CONSTRAINT [PK_EdmundsReviewData] PRIMARY KEY CLUSTERED ([ID] ASC)
);


GO
CREATE UNIQUE NONCLUSTERED INDEX [IX_EdmundsReviewData_AccountID]
    ON [dbo].[EdmundsReviewData]([AccountID] ASC);

