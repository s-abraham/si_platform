﻿CREATE TABLE [dbo].[TwitterPages] (
    [ID]                BIGINT   IDENTITY (1, 1) NOT NULL,
    [CredentialID]      BIGINT   NOT NULL,
    [TweetsCount]       INT      NULL,
    [FollowingCount]    INT      NULL,
    [FollowersCount]    INT      NULL,
    [NewFollowersCount] INT      NULL,
    [ListedCount]       INT      NULL,
    [FavoritesCount]    INT      NULL,
    [FriendsCount]      INT      NULL,
    [RetweetsCount]     INT      NULL,
    [QueryDate]         DATE     NOT NULL,
    [DateCreated]       DATETIME NOT NULL,
    CONSTRAINT [PK_TwitterPages] PRIMARY KEY CLUSTERED ([ID] ASC),
    CONSTRAINT [FK_TwitterPages_Credentials] FOREIGN KEY ([CredentialID]) REFERENCES [dbo].[Credentials] ([ID])
);

