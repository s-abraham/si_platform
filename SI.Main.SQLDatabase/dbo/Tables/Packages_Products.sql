﻿CREATE TABLE [dbo].[Packages_Products] (
    [ID]        BIGINT IDENTITY (1, 1) NOT NULL,
    [PackageID] BIGINT NOT NULL,
    [ProductID] BIGINT NOT NULL,
    CONSTRAINT [PK_Packages_Products] PRIMARY KEY CLUSTERED ([ID] ASC),
    CONSTRAINT [FK_Packages_Products_Packages] FOREIGN KEY ([PackageID]) REFERENCES [dbo].[Packages] ([ID]),
    CONSTRAINT [FK_Packages_Products_Products] FOREIGN KEY ([ProductID]) REFERENCES [dbo].[Products] ([ID])
);


GO
CREATE UNIQUE NONCLUSTERED INDEX [IX_Packages_Products_PackageID_ProductID]
    ON [dbo].[Packages_Products]([PackageID] ASC, [ProductID] ASC);

