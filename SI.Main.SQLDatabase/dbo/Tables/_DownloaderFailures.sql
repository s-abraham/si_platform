﻿CREATE TABLE [dbo].[_DownloaderFailures] (
    [ID]                     BIGINT        IDENTITY (1, 1) NOT NULL,
    [Account_ReviewSourceID] BIGINT        NOT NULL,
    [AccountID]              BIGINT        NOT NULL,
    [ReviewSourceID]         BIGINT        NOT NULL,
    [JobID]                  BIGINT        NOT NULL,
    [FailureReason]          VARCHAR (MAX) NOT NULL,
    [FailureAuditDate]       DATETIME      NOT NULL,
    CONSTRAINT [PK__DownloaderFailures] PRIMARY KEY CLUSTERED ([ID] ASC)
);


GO
CREATE NONCLUSTERED INDEX [IX__DownloaderFailures_JobID]
    ON [dbo].[_DownloaderFailures]([JobID] ASC);


GO
CREATE NONCLUSTERED INDEX [IX__DownloaderFailures_AccountID]
    ON [dbo].[_DownloaderFailures]([AccountID] ASC);


GO
CREATE NONCLUSTERED INDEX [IX__DownloaderFailures_FailureAuditDate_ReviewSourceID]
    ON [dbo].[_DownloaderFailures]([FailureAuditDate] ASC, [ReviewSourceID] ASC);


GO
CREATE NONCLUSTERED INDEX [IX__DownloaderFailures_FailureAuditDate]
    ON [dbo].[_DownloaderFailures]([FailureAuditDate] ASC);

