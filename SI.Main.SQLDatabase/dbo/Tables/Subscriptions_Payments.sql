﻿CREATE TABLE [dbo].[Subscriptions_Payments] (
    [ID]             BIGINT IDENTITY (1, 1) NOT NULL,
    [PaymentID]      BIGINT NOT NULL,
    [SubscriptionID] BIGINT NOT NULL,
    CONSTRAINT [PK_Subscriptions_Payments] PRIMARY KEY CLUSTERED ([ID] ASC),
    CONSTRAINT [FK_Subscriptions_Payments_Payments] FOREIGN KEY ([PaymentID]) REFERENCES [dbo].[Payments] ([ID]),
    CONSTRAINT [FK_Subscriptions_Payments_Subscriptions] FOREIGN KEY ([SubscriptionID]) REFERENCES [dbo].[Subscriptions] ([ID])
);


GO
CREATE UNIQUE NONCLUSTERED INDEX [IX_Subscriptions_Payments_PaymentID_SubscriptionID]
    ON [dbo].[Subscriptions_Payments]([PaymentID] ASC, [SubscriptionID] ASC);

