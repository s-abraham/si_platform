﻿CREATE TABLE [dbo].[Resellers_Packages] (
    [ID]          BIGINT   IDENTITY (1, 1) NOT NULL,
    [ResellerID]  BIGINT   NOT NULL,
    [PackageID]   BIGINT   NOT NULL,
    [Active]      BIT      NOT NULL,
    [DateCreated] DATETIME NOT NULL,
    CONSTRAINT [PK_Resellers_Packages] PRIMARY KEY CLUSTERED ([ID] ASC),
    CONSTRAINT [FK_Resellers_Packages_Packages] FOREIGN KEY ([PackageID]) REFERENCES [dbo].[Packages] ([ID]),
    CONSTRAINT [FK_Resellers_Packages_Resellers] FOREIGN KEY ([ResellerID]) REFERENCES [dbo].[Resellers] ([ID])
);


GO
CREATE UNIQUE NONCLUSTERED INDEX [IX_Resellers_Packages_ResellerID_PackageID]
    ON [dbo].[Resellers_Packages]([ResellerID] ASC, [PackageID] ASC);

