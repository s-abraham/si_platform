﻿CREATE TABLE [dbo].[VirtualGroupTypes] (
    [ID]       BIGINT        IDENTITY (1, 1) NOT NULL,
    [Name]     NVARCHAR (50) NOT NULL,
    [IsSystem] BIT           NULL,
    CONSTRAINT [PK_VirtualGroupTypes] PRIMARY KEY CLUSTERED ([ID] ASC)
);



