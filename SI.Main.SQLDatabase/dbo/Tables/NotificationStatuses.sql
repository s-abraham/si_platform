﻿CREATE TABLE [dbo].[NotificationStatuses] (
    [ID]         BIGINT       NOT NULL,
    [StatusName] VARCHAR (20) NOT NULL,
    [SortOrder]  INT          NOT NULL,
    CONSTRAINT [PK_NotificationStatuses] PRIMARY KEY CLUSTERED ([ID] ASC)
);

