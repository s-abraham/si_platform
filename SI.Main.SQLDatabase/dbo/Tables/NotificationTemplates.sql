﻿CREATE TABLE [dbo].[NotificationTemplates] (
    [ID]                        BIGINT        IDENTITY (1, 1) NOT NULL,
    [NotificationMessageTypeID] BIGINT        NOT NULL,
    [NotificationMethodID]      BIGINT        NOT NULL,
    [ResellerID]                BIGINT        NULL,
    [Subject]                   VARCHAR (300) NULL,
    [Body]                      VARCHAR (MAX) NOT NULL,
    CONSTRAINT [PK_NotificationTemplates] PRIMARY KEY CLUSTERED ([ID] ASC),
    CONSTRAINT [FK_NotificationTemplates_NotificationMessageTypes] FOREIGN KEY ([NotificationMessageTypeID]) REFERENCES [dbo].[NotificationMessageTypes] ([ID]),
    CONSTRAINT [FK_NotificationTemplates_NotificationMethods] FOREIGN KEY ([NotificationMethodID]) REFERENCES [dbo].[NotificationMethods] ([ID]),
    CONSTRAINT [FK_NotificationTemplates_Resellers] FOREIGN KEY ([ResellerID]) REFERENCES [dbo].[Resellers] ([ID])
);

