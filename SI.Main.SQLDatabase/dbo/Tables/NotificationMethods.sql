﻿CREATE TABLE [dbo].[NotificationMethods] (
    [ID]        BIGINT       NOT NULL,
    [Name]      VARCHAR (50) NOT NULL,
    [SortOrder] INT          NOT NULL,
    CONSTRAINT [PK_NotificationMethods] PRIMARY KEY CLUSTERED ([ID] ASC)
);

