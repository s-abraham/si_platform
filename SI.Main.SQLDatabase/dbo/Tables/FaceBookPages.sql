﻿CREATE TABLE [dbo].[FaceBookPages] (
    [ID]            BIGINT   IDENTITY (1, 1) NOT NULL,
    [CredentialID]  BIGINT   NULL,
    [UnreadNotify]  INT      NULL,
    [CheckIns]      INT      NULL,
    [NewLike]       INT      NULL,
    [Likes]         INT      NULL,
    [UnreadMessage] INT      NULL,
    [UnseenMessage] INT      NULL,
    [WereHere]      INT      NULL,
    [TalkingAbout]  INT      NULL,
    [QueryDate]     DATE     NOT NULL,
    [DateCreated]   DATETIME CONSTRAINT [DF__FaceBookP__DateC__078E810B] DEFAULT (getdate()) NOT NULL,
    CONSTRAINT [PK_FACEBOOKPAGE] PRIMARY KEY CLUSTERED ([ID] ASC),
    CONSTRAINT [FK_FaceBookPages_Credentials] FOREIGN KEY ([CredentialID]) REFERENCES [dbo].[Credentials] ([ID])
);

