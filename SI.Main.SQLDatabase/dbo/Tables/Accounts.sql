﻿CREATE TABLE [dbo].[Accounts] (
    [ID]                     BIGINT         IDENTITY (1, 1) NOT NULL,
    [AccountTypeID]          BIGINT         NOT NULL,
    [ParentAccountID]        BIGINT         NULL,
    [CRMSystemTypeID]        BIGINT         NULL,
    [VerticalID]             BIGINT         NULL,
    [Name]                   NVARCHAR (100) NOT NULL,
    [DisplayName]            NVARCHAR (100) NULL,
    [PhysicalAddressID]      BIGINT         NULL,
    [BillingPhoneID]         BIGINT         NULL,
    [BillingAddressID]       BIGINT         NULL,
    [WebsiteURL]             NVARCHAR (100) NULL,
    [EmailAddress]           NVARCHAR (100) NULL,
    [LeadsEmailAddress]      NVARCHAR (100) NULL,
    [EdmundsDealershipID]    BIGINT         NULL,
    [TimezoneID]             BIGINT         NOT NULL,
    [OriginSystemIdentifier] VARCHAR (100)  NULL,
    [IsDemoAccount]          BIT            NULL,
    [DateCreated]            DATETIME       NOT NULL,
    [DateModified]           DATETIME       NOT NULL,
    CONSTRAINT [PK_Accounts] PRIMARY KEY CLUSTERED ([ID] ASC),
    CONSTRAINT [FK_Accounts_Accounts] FOREIGN KEY ([ParentAccountID]) REFERENCES [dbo].[Accounts] ([ID])
);








GO
CREATE UNIQUE NONCLUSTERED INDEX [IX_Accounts_Name]
    ON [dbo].[Accounts]([Name] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_Accounts]
    ON [dbo].[Accounts]([LeadsEmailAddress] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_Accounts_EmailAddress]
    ON [dbo].[Accounts]([EmailAddress] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_Accounts_AccountTypeID]
    ON [dbo].[Accounts]([AccountTypeID] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_Accounts_ParentAccountID]
    ON [dbo].[Accounts]([ParentAccountID] ASC);


GO
CREATE UNIQUE NONCLUSTERED INDEX [IX_Accounts_Name_DisplayName]
    ON [dbo].[Accounts]([Name] ASC, [DisplayName] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_Accounts_OriginSystemIdentifier]
    ON [dbo].[Accounts]([OriginSystemIdentifier] ASC);

