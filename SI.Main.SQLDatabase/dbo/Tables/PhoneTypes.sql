﻿CREATE TABLE [dbo].[PhoneTypes] (
    [ID]   BIGINT        IDENTITY (1, 1) NOT NULL,
    [Name] NVARCHAR (20) NOT NULL,
    CONSTRAINT [PK_PhoneTypes] PRIMARY KEY CLUSTERED ([ID] ASC)
);

