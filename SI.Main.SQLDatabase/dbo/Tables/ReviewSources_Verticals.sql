﻿CREATE TABLE [dbo].[ReviewSources_Verticals] (
    [ID]             BIGINT IDENTITY (1, 1) NOT NULL,
    [ReviewSourceID] BIGINT NOT NULL,
    [VerticalID]     BIGINT NOT NULL,
    CONSTRAINT [PK_ReviewSources_Verticals] PRIMARY KEY CLUSTERED ([ID] ASC),
    CONSTRAINT [FK_ReviewSources_Verticals_ReviewSources] FOREIGN KEY ([ReviewSourceID]) REFERENCES [dbo].[ReviewSources] ([ID]),
    CONSTRAINT [FK_ReviewSources_Verticals_Verticals] FOREIGN KEY ([VerticalID]) REFERENCES [dbo].[Verticals] ([ID])
);


GO
CREATE UNIQUE NONCLUSTERED INDEX [IX_ReviewSources_Verticals_VerticalID_ReviewSourceID]
    ON [dbo].[ReviewSources_Verticals]([VerticalID] ASC, [ReviewSourceID] ASC);

