﻿CREATE TABLE [dbo].[FaceBookComments] (
    [ID]                   BIGINT        IDENTITY (1, 1) NOT NULL,
    [FaceBookPostsID]      BIGINT        NOT NULL,
    [FaceBookIdentitiesID] BIGINT        NOT NULL,
    [FBCommentID]          VARCHAR (250) NOT NULL,
    [Message]              VARCHAR (MAX) NULL,
    [FBCreateTime]         DATETIME      NULL,
    [UserLikes]            BIT           NULL,
    [LikeCount]            INT           NULL,
    [DateCreated]          DATETIME      DEFAULT (getdate()) NOT NULL,
    CONSTRAINT [PK_FACEBOOKCOMMENT] PRIMARY KEY CLUSTERED ([ID] ASC),
    CONSTRAINT [RefFaceBookIdentities24] FOREIGN KEY ([FaceBookIdentitiesID]) REFERENCES [dbo].[FaceBookIdentities] ([ID]),
    CONSTRAINT [RefFaceBookPosts38] FOREIGN KEY ([FaceBookPostsID]) REFERENCES [dbo].[FaceBookPosts] ([ID])
);


GO
CREATE UNIQUE NONCLUSTERED INDEX [IX_FaceBookComments_FBCommentID]
    ON [dbo].[FaceBookComments]([FBCommentID] ASC);


GO
CREATE NONCLUSTERED INDEX [FK_FACEBOOKPOST_FACEBOOKCOMMENTS]
    ON [dbo].[FaceBookComments]([FaceBookPostsID] ASC);


GO
CREATE NONCLUSTERED INDEX [FK_FACEBOOKIDENTITIES_FACEBOOKCOMMENTS]
    ON [dbo].[FaceBookComments]([FaceBookIdentitiesID] ASC);

