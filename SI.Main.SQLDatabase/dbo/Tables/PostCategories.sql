﻿CREATE TABLE [dbo].[PostCategories] (
    [ID]         BIGINT        IDENTITY (1, 1) NOT NULL,
    [VerticalID] BIGINT        NOT NULL,
    [Name]       NVARCHAR (50) NOT NULL,
    CONSTRAINT [PK_PostCategories] PRIMARY KEY CLUSTERED ([ID] ASC)
);

