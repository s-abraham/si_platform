﻿CREATE TABLE [dbo].[UserSettings] (
    [ID]     BIGINT        IDENTITY (1, 1) NOT NULL,
    [UserID] BIGINT        NOT NULL,
    [Name]   VARCHAR (100) NOT NULL,
    [Value]  VARCHAR (100) NOT NULL,
    CONSTRAINT [PK_UserSettings] PRIMARY KEY CLUSTERED ([ID] ASC),
    CONSTRAINT [FK_UserSettings_Users] FOREIGN KEY ([UserID]) REFERENCES [dbo].[Users] ([ID])
);




GO
CREATE UNIQUE NONCLUSTERED INDEX [IX_UserSettings_UserID_Name]
    ON [dbo].[UserSettings]([UserID] ASC, [Name] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_UserSettings_UserID]
    ON [dbo].[UserSettings]([UserID] ASC);

