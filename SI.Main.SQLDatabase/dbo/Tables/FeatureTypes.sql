﻿CREATE TABLE [dbo].[FeatureTypes] (
    [ID]          BIGINT         IDENTITY (1, 1) NOT NULL,
    [Name]        NVARCHAR (50)  NOT NULL,
    [Description] NVARCHAR (250) NULL,
    CONSTRAINT [PK_FeatureTypes] PRIMARY KEY CLUSTERED ([ID] ASC)
);

