﻿CREATE TABLE [dbo].[States] (
    [ID]           BIGINT         IDENTITY (1, 1) NOT NULL,
    [Name]         NVARCHAR (150) NOT NULL,
    [Abbreviation] NVARCHAR (10)  NOT NULL,
    [CountryID]    BIGINT         NOT NULL,
    [LanguageID]   BIGINT         NULL,
    CONSTRAINT [PK_States] PRIMARY KEY CLUSTERED ([ID] ASC),
    CONSTRAINT [FK_States_Countries] FOREIGN KEY ([CountryID]) REFERENCES [dbo].[Countries] ([ID])
);


GO
CREATE UNIQUE NONCLUSTERED INDEX [IX_States_CountryID_Name]
    ON [dbo].[States]([CountryID] ASC, [Name] ASC);


GO
CREATE UNIQUE NONCLUSTERED INDEX [IX_States_CountryID_Abbreviation]
    ON [dbo].[States]([CountryID] ASC, [Abbreviation] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_States_CountryID]
    ON [dbo].[States]([CountryID] ASC);

