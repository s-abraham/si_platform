﻿CREATE TABLE [dbo].[SocialPostActivity] (
    [ID]                BIGINT        IDENTITY (1, 1) NOT NULL,
    [PostDate]          DATETIME      NOT NULL,
    [SourceTypeEnum]    BIGINT        NOT NULL,
    [SourceIdentifier]  VARCHAR (250) NULL,
    [SourceName]        VARCHAR (100) NULL,
    [LastHarvestedDate] DATETIME      NULL,
    [FeedID]            VARCHAR (250) NULL,
    [PostTargetID]      BIGINT        NULL,
    [PostImageResultID] BIGINT        NULL,
    [Message]           VARCHAR (MAX) NULL,
    [Link]              VARCHAR (500) NULL,
    [ImageURL]          VARCHAR (300) NULL,
    CONSTRAINT [PK_SocialPostActivity] PRIMARY KEY CLUSTERED ([ID] ASC)
);


GO
CREATE NONCLUSTERED INDEX [IX_SocialPostActivity_PostDate_LastHarvestedDate]
    ON [dbo].[SocialPostActivity]([PostDate] ASC, [LastHarvestedDate] ASC);


GO
CREATE UNIQUE NONCLUSTERED INDEX [IX_SocialPostActivity_FeedID]
    ON [dbo].[SocialPostActivity]([FeedID] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_SocialPostActivity_PostImageResultID]
    ON [dbo].[SocialPostActivity]([PostImageResultID] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_SocialPostActivity_PostTargetID]
    ON [dbo].[SocialPostActivity]([PostTargetID] ASC);

