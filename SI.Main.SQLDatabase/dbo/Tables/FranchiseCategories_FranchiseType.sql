﻿CREATE TABLE [dbo].[FranchiseCategories_FranchiseType] (
    [ID]              INT    IDENTITY (1, 1) NOT NULL,
    [FranchiseTypeID] BIGINT NULL,
    [CategoryID]      INT    NULL,
    CONSTRAINT [PK_Category_FranchiseType] PRIMARY KEY CLUSTERED ([ID] ASC),
    CONSTRAINT [FK_Category_FranchiseType_Category] FOREIGN KEY ([CategoryID]) REFERENCES [dbo].[FranchiseCategories] ([ID]),
    CONSTRAINT [FK_Category_FranchiseType_FranchiseTypes] FOREIGN KEY ([FranchiseTypeID]) REFERENCES [dbo].[FranchiseTypes] ([ID])
);

