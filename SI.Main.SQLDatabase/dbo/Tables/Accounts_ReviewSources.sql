﻿CREATE TABLE [dbo].[Accounts_ReviewSources] (
    [ID]                   BIGINT         IDENTITY (1, 1) NOT NULL,
    [AccountID]            BIGINT         NOT NULL,
    [ReviewSourceID]       BIGINT         NOT NULL,
    [URL]                  VARCHAR (1200) NULL,
    [URLAPI]               VARCHAR (1200) NULL,
    [ExternalID]           VARCHAR (150)  NULL,
    [ExternalID2]          VARCHAR (150)  NULL,
    [LastSummeryAttempted] DATETIME       NULL,
    [LastSummaryRun]       DATETIME       NULL,
    [LastDetailAttempted]  DATETIME       NULL,
    [LastDetailRun]        DATETIME       NULL,
    [LastValidation]       DATETIME       NULL,
    [IsValid]              BIT            NOT NULL,
    [DateCreated]          DATETIME       NOT NULL,
    CONSTRAINT [PK_Accounts_ReviewSources] PRIMARY KEY CLUSTERED ([ID] ASC)
);








GO



GO
CREATE UNIQUE NONCLUSTERED INDEX [IX_Accounts_ReviewSources_AccountID_ReviewSourceID]
    ON [dbo].[Accounts_ReviewSources]([AccountID] ASC, [ReviewSourceID] ASC);

