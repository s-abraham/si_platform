﻿CREATE TABLE [dbo].[RoleTypes_PermissionTypes] (
    [ID]               BIGINT IDENTITY (1, 1) NOT NULL,
    [RoleTypeID]       BIGINT NOT NULL,
    [PermissionTypeID] BIGINT NOT NULL,
    CONSTRAINT [PK_RoleTypes_PermissionTypes] PRIMARY KEY CLUSTERED ([ID] ASC),
    CONSTRAINT [FK_RoleTypes_PermissionTypes_PermissionTypes] FOREIGN KEY ([PermissionTypeID]) REFERENCES [dbo].[PermissionTypes] ([ID]),
    CONSTRAINT [FK_RoleTypes_PermissionTypes_RoleTypes] FOREIGN KEY ([RoleTypeID]) REFERENCES [dbo].[RoleTypes] ([ID])
);


GO
CREATE UNIQUE NONCLUSTERED INDEX [IX_RoleTypes_PermissionTypes_RoleTypeID_PermissionTypeID]
    ON [dbo].[RoleTypes_PermissionTypes]([RoleTypeID] ASC, [PermissionTypeID] ASC);

