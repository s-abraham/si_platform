﻿CREATE TABLE [dbo].[Resellers] (
    [ID]           BIGINT           IDENTITY (1, 1) NOT NULL,
    [AccountID]    BIGINT           NOT NULL,
    [ThemeName]    VARCHAR (50)     NULL,
    [SSOSecretKey] UNIQUEIDENTIFIER CONSTRAINT [DF_Resellers_SSISecretKey] DEFAULT (newid()) NOT NULL,
    [DateCreated]  DATETIME         NOT NULL,
    CONSTRAINT [PK_Resellers] PRIMARY KEY CLUSTERED ([ID] ASC),
    CONSTRAINT [FK_Resellers_Accounts] FOREIGN KEY ([AccountID]) REFERENCES [dbo].[Accounts] ([ID])
);




GO
CREATE UNIQUE NONCLUSTERED INDEX [IX_Resellers_AccountID]
    ON [dbo].[Resellers]([AccountID] ASC);




GO
CREATE UNIQUE NONCLUSTERED INDEX [IX_Resellers_SSOSecretKey]
    ON [dbo].[Resellers]([SSOSecretKey] ASC);

