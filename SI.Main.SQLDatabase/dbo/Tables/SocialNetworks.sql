﻿CREATE TABLE [dbo].[SocialNetworks] (
    [ID]   BIGINT         IDENTITY (1, 1) NOT NULL,
    [Name] NVARCHAR (100) NOT NULL,
    CONSTRAINT [PK_SocialNetwork] PRIMARY KEY CLUSTERED ([ID] ASC)
);

