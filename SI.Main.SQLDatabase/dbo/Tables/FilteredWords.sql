﻿CREATE TABLE [dbo].[FilteredWords] (
    [ID]    BIGINT         IDENTITY (1, 1) NOT NULL,
    [Value] NVARCHAR (100) NOT NULL,
    CONSTRAINT [PK_FilteredWords] PRIMARY KEY CLUSTERED ([ID] ASC)
);

