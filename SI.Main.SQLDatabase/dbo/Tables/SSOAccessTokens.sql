﻿CREATE TABLE [dbo].[SSOAccessTokens] (
    [ID]                BIGINT           IDENTITY (1, 1) NOT NULL,
    [AccessToken]       UNIQUEIDENTIFIER CONSTRAINT [DF_SSOAccessTokens_AccessToken] DEFAULT (newid()) NOT NULL,
    [ResellerSecretKey] UNIQUEIDENTIFIER NOT NULL,
    [UserSecretKey]     UNIQUEIDENTIFIER NOT NULL,
    [DateCreated]       DATETIME         NOT NULL,
    [DateExpires]       DATETIME         NOT NULL,
    CONSTRAINT [PK_SSOAccessTokens] PRIMARY KEY CLUSTERED ([ID] ASC)
);


GO
CREATE UNIQUE NONCLUSTERED INDEX [IX_SSOAccessTokens_AccessToken]
    ON [dbo].[SSOAccessTokens]([AccessToken] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_SSOAccessTokens_AccessToken_DateExpires]
    ON [dbo].[SSOAccessTokens]([AccessToken] ASC, [DateExpires] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_SSOAccessTokens_ResellerSecretKey_UserSecretKey_DateExpires]
    ON [dbo].[SSOAccessTokens]([ResellerSecretKey] ASC, [UserSecretKey] ASC, [DateExpires] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_SSOAccessTokens_ResellerSecretKey_UserSecretKey]
    ON [dbo].[SSOAccessTokens]([ResellerSecretKey] ASC, [UserSecretKey] ASC);

