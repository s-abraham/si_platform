﻿CREATE TABLE [dbo].[FranchiseTypes] (
    [ID]   BIGINT        IDENTITY (1, 1) NOT NULL,
    [Name] NVARCHAR (50) NOT NULL,
    CONSTRAINT [PK_FranchiseTypes] PRIMARY KEY CLUSTERED ([ID] ASC)
);

