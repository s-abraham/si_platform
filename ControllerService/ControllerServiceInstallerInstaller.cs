﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration.Install;
using System.Linq;
using System.Threading.Tasks;

namespace ControllerService
{
    [RunInstaller(true)]
    public partial class ControllerServiceInstallerInstaller : System.Configuration.Install.Installer
    {
        public ControllerServiceInstallerInstaller()
        {
            InitializeComponent();
        }
    }
}
