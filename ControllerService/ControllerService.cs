﻿using JobLib;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.Security;
using System.ServiceProcess;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace ControllerService
{
    public partial class ControllerService : ServiceBase
    {
        private static Controller c = null;

        public ControllerService()
        {
            InitializeComponent();
            bool sourceFound = false;
            try
            {
                sourceFound = EventLog.SourceExists("SISource");
            }
            catch (SecurityException)
            {
                sourceFound = false;
            }

            if (!sourceFound)
            {
                System.Diagnostics.EventLog.CreateEventSource(
                    "SISource", "SIControllerService");
            }
            
            eventLog.Source = "SISource";
            eventLog.Log = "SIControllerService";
        }

        protected override void OnStart(string[] args)
        {
            try
            {

            eventLog.WriteEntry("SI Controller Service Starting...", EventLogEntryType.Information);
            c = new Controller();
            c.Start();
            eventLog.WriteEntry("SI Controller Service Started...", EventLogEntryType.Information);

            }
            catch (Exception ex)
            {
                eventLog.WriteEntry("Start exception: " + ex.ToString(), EventLogEntryType.Error);
            }
        }

        protected override void OnStop()
        {
            try
            {

                eventLog.WriteEntry("SI Controller Service Stopping...", EventLogEntryType.Information);
                c.Stop();
                eventLog.WriteEntry("SI Controller Service Stopped...", EventLogEntryType.Information);

            }
            catch (Exception ex)
            {
                eventLog.WriteEntry("Stop exception: " + ex.ToString(), EventLogEntryType.Error);
            }
        }
    }
}
