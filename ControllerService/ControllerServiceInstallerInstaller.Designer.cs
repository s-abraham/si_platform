﻿namespace ControllerService
{
    partial class ControllerServiceInstallerInstaller
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.controllerProcessInstaller = new System.ServiceProcess.ServiceProcessInstaller();
            this.controllerServiceInstaller = new System.ServiceProcess.ServiceInstaller();
            // 
            // controllerProcessInstaller
            // 
            this.controllerProcessInstaller.Account = System.ServiceProcess.ServiceAccount.LocalSystem;
            this.controllerProcessInstaller.Password = null;
            this.controllerProcessInstaller.Username = null;
            // 
            // controllerServiceInstaller
            // 
            this.controllerServiceInstaller.ServiceName = "SI Controller Service";
            this.controllerServiceInstaller.StartType = System.ServiceProcess.ServiceStartMode.Automatic;
            // 
            // ControllerServiceInstallerInstaller
            // 
            this.Installers.AddRange(new System.Configuration.Install.Installer[] {
            this.controllerProcessInstaller,
            this.controllerServiceInstaller});

        }

        #endregion

        private System.ServiceProcess.ServiceProcessInstaller controllerProcessInstaller;
        private System.ServiceProcess.ServiceInstaller controllerServiceInstaller;
    }
}