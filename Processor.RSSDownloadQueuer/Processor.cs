﻿using JobLib;
using SI.BLL;
using SI.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Processor.RSSDownloadQueuer
{
    public class Processor : ProcessorBase<RSSDownloadQueuerData>
    {
        // Need to pass the args through for the default constructor
        public Processor(string[] args) : base(args) { }

        protected override void start()
        {
            JobDTO job = getNextJob();

            while (job != null)
            {
                try
                {
                    setJobStatus(job.ID.Value, JobStatusEnum.Started);
                    RSSDownloadQueuerData data = getJobData(job.ID.Value);

                    ProviderFactory.RSS.QueueRSSDownloads(data.SpreadMinutes, data.DaysBack, data.MaxItems, data.DownloadImages);

                    setJobStatus(job.ID.Value, JobStatusEnum.Completed);
                }

                catch (Exception ex)
                {
                    setJobStatus(job.ID.Value, JobStatusEnum.Failed);
                    ProviderFactory.Logging.LogException(ex);
                }

                job = getNextJob();
            }
        }
    }
}
