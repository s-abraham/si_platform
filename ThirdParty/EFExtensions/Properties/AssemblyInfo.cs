﻿//----------------------------------------------------------------
// Copyright (c) Microsoft Corporation.  All rights reserved.
//----------------------------------------------------------------

using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using System;
using System.Resources;
using System.Globalization;

[assembly: AssemblyTitle("EFExtensions")]
[assembly: AssemblyCompany("Microsoft")]
[assembly: AssemblyProduct("EFExtensions")]
[assembly: AssemblyCopyright("Copyright © Microsoft 2009")]

[assembly: ComVisible(false)]
[assembly: CLSCompliant(true)]
[assembly: AssemblyVersion("1.0.0.0")]
[assembly: AssemblyFileVersion("1.0.0.0")]

[assembly: NeutralResourcesLanguage("en-US")]
