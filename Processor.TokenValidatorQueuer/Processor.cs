﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using JobLib;
using SI;
using SI.BLL;
using SI.DTO;

namespace Processor.TokenValidatorQueuer
{
    public class Processor : ProcessorBase<TokenValidatorQueuerData>
    {
        // Need to pass the args through for the default constructor
        public Processor(string[] args) : base(args) { }

        protected override void start()
        {
            Auditor
               .New(AuditLevelEnum.Information, AuditTypeEnum.General, new UserInfoDTO(1, null), "TokenValidatorQueuer starting")
               .Save(ProviderFactory.Logging)
            ;
            JobDTO job = getNextJob();

            if (job == null)
            {
                Auditor
                   .New(AuditLevelEnum.Information, AuditTypeEnum.JobFailure, new UserInfoDTO(1, null), "TokenValidatorQueuer found NULL for getNextJob() first call!")
                   .Save(ProviderFactory.Logging)
                ;
            }

            while (job != null)
            {
                try
                {
                    setJobStatus(job.ID.Value, JobStatusEnum.Started);
                    TokenValidatorQueuerData data = getJobData(job.ID.Value);
                    
                    List<SocialCredentialDTO> items = ProviderFactory.Social.GetSocialCredentials();

                    Auditor
                      .New(AuditLevelEnum.Information, AuditTypeEnum.General, new UserInfoDTO(1, null),
                              "TokenValidatorQueuer Queueing jobs for {0} Credential.",
                              items.Count()
                          )
                      .Save(ProviderFactory.Logging)
                  ;

                    List<JobDTO> joblist = new List<JobDTO>();
                    DateTime jobDate = DateTime.UtcNow;

                    foreach (SocialCredentialDTO item in items)
                    {
                        if (item.IsValid == true)
                        {
                            TokenValidatorDownloaderData TokenDownloaderData = new TokenValidatorDownloaderData()
                            {
                                CredentialID = item.ID,
                                SocialNetwork = (SocialNetworkEnum)item.SocialNetworkID
                            };

                            JobDTO TokenValidatorjob = new JobDTO()
                            {
                                DateScheduled = new SIDateTime(jobDate, TimeZoneInfo.Utc),
                                JobStatus = JobStatusEnum.Queued,
                                JobDataObject = TokenDownloaderData,
                                JobType = JobTypeEnum.TokenValidator,
                                Priority = 100,
                                ContinueChainIfFailed = false
                            };

                            joblist.Add(TokenValidatorjob);

                            if (joblist.Count() >= 1000)
                            {
                                ProviderFactory.Jobs.Save(new SaveEntityDTO<List<JobDTO>>(joblist, UserInfoDTO.System));
                                joblist.Clear();
                            }
                        }
                    }

                    if (joblist.Any())
                    {
                        ProviderFactory.Jobs.Save(new SaveEntityDTO<List<JobDTO>>(joblist, UserInfoDTO.System));
                        joblist.Clear();
                    }

                    setJobStatus(job.ID.Value, JobStatusEnum.Completed);

                }
                catch (Exception ex)
                {
                    setJobStatus(job.ID.Value, JobStatusEnum.Failed);
                    ProviderFactory.Logging.LogException(ex);
                    ProviderFactory.Logging.Audit(AuditLevelEnum.Exception, AuditTypeEnum.ProcessorException, ex.Message, null, null, null, null, null, null);
                }
                job = getNextJob();
            }
        }
    }
}
