﻿using AWS;
using Google.Model;
using JobLib;
using Newtonsoft.Json;
using SI.BLL;
using SI.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Processor.Social.GooglePlusIntegrator
{
    public class Processor : ProcessorBase<GooglePlusIntegratorData>
    {
        private const string prefix = "AWS.";

        // Need to pass the args through for the default constructor
        public Processor(string[] args) : base(args) { }

        //static void Main(string[] args)
        protected override void start()
        {
            string accessKey = ProviderFactory.Reviews.GetProcessorSetting(prefix + "AWSAccessKey");
            string secretKey = ProviderFactory.Reviews.GetProcessorSetting(prefix + "AWSSecretKey");
            string serviceURL = ProviderFactory.Reviews.GetProcessorSetting(prefix + "DynamoDBServiceURL");
            string table = ProviderFactory.Reviews.GetProcessorSetting(prefix + "DynamoDBSocialRawTable");

            DynamoDB dynamoDB = new DynamoDB(accessKey, secretKey, serviceURL, table);

            JobDTO job = getNextJob();

            while (job != null)
            {
                try
                {
                    setJobStatus(job.ID.Value, JobStatusEnum.Started);
                    GooglePlusIntegratorData data = getJobData(job.ID.Value);

                    //Get the data from dynamoDB based on Key 
                    var dynDBResult = dynamoDB.GetValue(data.Key);
                    if (dynDBResult.IsSuccess)
                    {
                        switch (data.GooglePlusEnum)
                        {
                            case GooglePlusProcessorActionEnum.GooglePlusPageInformationIntegrator:
                                {
                                    #region GooglePlusPageInformationIntegrator
                                    GooglePlusPageStatistics objGooglePlusPageStatistics = JsonConvert.DeserializeObject<GooglePlusPageStatistics>(dynDBResult.Value);

                                    if (objGooglePlusPageStatistics != null)
                                    {   
                                        bool Success = ProviderFactory.Social.SaveGooglePlusPage(data.CredentialID.Value, objGooglePlusPageStatistics.PlusOneCount
                                                                                                            , objGooglePlusPageStatistics.FollowerCount
                                                                                                            , objGooglePlusPageStatistics.PostCount
                                                                                                            , objGooglePlusPageStatistics.Error);

                                        if (!Success)
                                        {
                                            setJobStatus(job.ID.Value, JobStatusEnum.Failed);
                                            ProviderFactory.Logging.Audit(AuditLevelEnum.Warning, AuditTypeEnum.FacebookIntegratorFailure,
                                                                            string.Format("Could not save GooglePlus Page Information DynamoDB Key: {0}, JobID {1}", data.Key, job.ID), null, null, null, data.CredentialID);
                                        }
                                        else
                                        {
                                            setJobStatus(job.ID.Value, JobStatusEnum.Completed);
                                        }
                                    }
                                    else
                                    {
                                        setJobStatus(job.ID.Value, JobStatusEnum.Failed);
                                        ProviderFactory.Logging.Audit(AuditLevelEnum.Warning, AuditTypeEnum.FacebookIntegratorFailure,
                                                                        string.Format("Deserialized Page object failure DynamoDB Key: {0}. JobID {1}", data.Key, job.ID));
                                    }
                                    #endregion
                                }
                                break;
                            case GooglePlusProcessorActionEnum.GooglePlusPostStatisticsIntegrator:
                                {
                                    #region GooglePlusPostStatisticsIntegrator

                                    GooglePlusPostStatistics objGooglePlusPostStatistics = JsonConvert.DeserializeObject<GooglePlusPostStatistics>(dynDBResult.Value);

                                    if (objGooglePlusPostStatistics != null)
                                    {
                                        bool Success = ProviderFactory.Social.SaveGooglePlusPostStatistics(objGooglePlusPostStatistics, data.GooglePlusPostID);

                                        if (!Success)
                                        {
                                            setJobStatus(job.ID.Value, JobStatusEnum.Failed);
                                            ProviderFactory.Logging.Audit(AuditLevelEnum.Warning, AuditTypeEnum.FacebookIntegratorFailure,
                                                                            string.Format("Could not save GooglePlus Post Statistics DynamoDB Key: {0}, JobID {1}", data.Key, job.ID), null, null, null, data.CredentialID);
                                        }
                                        else
                                        {
                                            setJobStatus(job.ID.Value, JobStatusEnum.Completed);
                                        }
                                    }
                                    else
                                    {
                                        setJobStatus(job.ID.Value, JobStatusEnum.Failed);
                                        ProviderFactory.Logging.Audit(AuditLevelEnum.Warning, AuditTypeEnum.FacebookIntegratorFailure,
                                                                        string.Format("Deserialized Page object failure DynamoDB Key: {0}. JobID {1}", data.Key, job.ID));
                                    }
                                    #endregion
                                }
                                break;
                        }

                    }
                }
                catch (Exception ex)
                {
                    setJobStatus(job.ID.Value, JobStatusEnum.Failed);
                    ProviderFactory.Logging.LogException(ex, job.ID.Value);

                    Auditor
                        .New(AuditLevelEnum.Exception, AuditTypeEnum.ProcessorException, UserInfoDTO.System, ex.ToString())
                        .Save(ProviderFactory.Logging)
                    ;

                }
                job = getNextJob();
            }

        }

    }
}
