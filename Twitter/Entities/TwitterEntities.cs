﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Twitterizer;

namespace Twitter.Entities
{
    public class TwitterEntities
    {
       
    }
    #region TWITTER REQUEST

    public class TwitterRequest
    {
        public TwitterRequest()
        {
            RequestURL = string.Empty;
            Parameters = string.Empty;
            Method = string.Empty;
            Status = string.Empty;
            link = string.Empty;
            oauth_token = string.Empty;
            oauth_token_secret = string.Empty;
            DealerLocationSocialNetworkPostID = 0;
            DealerLocationSocialNetworkPostQueueID = 0;
        }
        public string RequestURL { get; set; }
        public string Parameters { get; set; }
        public string Method { get; set; }
        public int TimeOut { get; set; }
        public string link { get; set; }
        public string Status { get; set; }
        public string oauth_token { get; set; }
        public string oauth_token_secret { get; set; }
        public string oauth_consumer_key { get; set; }
        public string oauth_consumer_secret { get; set; }
        public int DealerLocationSocialNetworkPostID { get; set; }
        public int DealerLocationSocialNetworkPostQueueID { get; set; }
    }

    #endregion

    #region TWITTER PAGE INSIGHTS RESPONSE

    public class TwitterPageInsightsResponse
    {
        public TwitterPageInsightsResponse()
        {
            errors = new TwitterResponse();
            id = string.Empty;
            id_str = string.Empty;
            name = string.Empty;
            screen_name = string.Empty;
            location = string.Empty;
            url = string.Empty;
            description = string.Empty;
            @protected = false;
            followers_count = 0;
            friends_count = 0;
            listed_count = 0;
            created_at = string.Empty;
            favourites_count = 0;
            utc_offset = 0;
            time_zone = string.Empty;
            geo_enabled = false;
            verified = false;
            statuses_count = 0;
            lang = string.Empty;
            status = new TwitterPageInsightsResponseStatus();
            contributors_enabled = false;
            is_translator = false;
            following = false;
            follow_request_sent = false;
            notifications = false;
        }

        public TwitterResponse errors { get; set; }
        public string id { get; set; }
        public string id_str { get; set; }
        public string name { get; set; }
        public string screen_name { get; set; }
        public string location { get; set; }
        public string url { get; set; }
        public string description { get; set; }
        public bool @protected { get; set; }
        public int followers_count { get; set; }
        public int friends_count { get; set; }
        public int listed_count { get; set; }
        public string created_at { get; set; }
        public int favourites_count { get; set; }
        public int? utc_offset { get; set; }
        public string time_zone { get; set; }
        public bool geo_enabled { get; set; }
        public bool verified { get; set; }
        public int statuses_count { get; set; }
        public string lang { get; set; }
        public TwitterPageInsightsResponseStatus status { get; set; }
        public bool contributors_enabled { get; set; }
        public bool is_translator { get; set; }
        public bool? following { get; set; }
        public bool? follow_request_sent { get; set; }
        public bool? notifications { get; set; }
    }

    public class TwitterPageInsightsResponseStatus
    {
        public TwitterPageInsightsResponseStatus()
        {
            created_at = string.Empty;
            id = string.Empty;
            id_str = string.Empty;
            text = string.Empty;
            source = string.Empty;
            truncated = false;
            in_reply_to_status_id = 0;
            in_reply_to_status_id_str = 0;
            in_reply_to_user_id = 0;
            in_reply_to_user_id_str = 0;
            in_reply_to_screen_name = 0;
            geo = new Geo();
            coordinates = new Coordinates();
            place = new TwitterPageInsightsResponsePlace();
            retweet_count = 0;
            favorited = false;
            retweeted = false;
            possibly_sensitive = false;
        }

        public string created_at { get; set; }
        public string id { get; set; }
        public string id_str { get; set; }
        public string text { get; set; }
        public string source { get; set; }
        public bool truncated { get; set; }
        public int? in_reply_to_status_id { get; set; }
        public int? in_reply_to_status_id_str { get; set; }
        public int? in_reply_to_user_id { get; set; }
        public int? in_reply_to_user_id_str { get; set; }
        public int? in_reply_to_screen_name { get; set; }
        public Geo geo { get; set; }
        public Coordinates coordinates { get; set; }
        public TwitterPageInsightsResponsePlace place { get; set; }
        public object contributors { get; set; }
        public int retweet_count { get; set; }
        public bool favorited { get; set; }
        public bool retweeted { get; set; }
        public bool possibly_sensitive { get; set; }
    }


    public class Geo
    {
        public Geo()
        {
            type = string.Empty;
            coordinates = new List<double>();
        }

        public string type { get; set; }
        public List<double> coordinates { get; set; }
    }

    public class Coordinates
    {
        public Coordinates()
        {
            type = string.Empty;
            coordinates = new List<double>();

        }
        public string type { get; set; }
        public List<double> coordinates { get; set; }
    }

    public class TwitterPageInsightsResponsePlace
    {
        public TwitterPageInsightsResponsePlace()
        {
            id = string.Empty;
            url = string.Empty;
            place_type = string.Empty;
            name = string.Empty;
            full_name = string.Empty;
            country = string.Empty;
            country_code = string.Empty;
            bounding_box = new BoundingBox();
            attributes = new Attributes();

        }
        public string id { get; set; }
        public string url { get; set; }
        public string place_type { get; set; }
        public string name { get; set; }
        public string full_name { get; set; }
        public string country_code { get; set; }
        public string country { get; set; }
        public BoundingBox bounding_box { get; set; }
        public Attributes attributes { get; set; }
    }

    public class BoundingBox
    {
        public BoundingBox()
        {
            type = string.Empty;
            coordinates = new List<List<List<double>>>();
        }
        public string type { get; set; }
        public List<List<List<double>>> coordinates { get; set; }
    }

    public class Attributes
    {
        public Attributes()
        {

        }

    }


    #endregion

    #region TWITTER RESPONSE

    public class TwitterResponse
    {
        public TwitterResponse()
        {
            ID = string.Empty;
            message = string.Empty;
            type = string.Empty;
            code = string.Empty;
            status = string.Empty;
        }
        public string ID { get; set; }
        public string message { get; set; }
        public string type { get; set; }
        public string code { get; set; }
        public string status { get; set; }
        public string error { get; set; }
        public string request { get; set; }

        public override string ToString()
        {
            string str = "ID : " + ID + " , Message :" + message + " , Type : " + type + " , Code : " + code + ", error : " + error + ", request : " + request;

            return str;
        }
    }

	public class TwitterOAuthResponse
	{
		public string token { get; set; }
		public string tokenSecret { get; set; }
		public string screenName { get; set; }
		public string pictureURL { get; set; }
		public string userID { get; set; }
		public string webSite { get; set; }
		public bool success { get; set; }
	}

    #endregion

    public class TwitterStatues
    {
        public TwitterStatues()
        {
            twitterResponse = new TwitterResponse();
            tweets = new List<Tweet>();
        }
        public TwitterResponse twitterResponse { get; set; }
        public List<Tweet> tweets { get; set; }
    }

    public class TwitterResponseToken
    {
        public TwitterResponseToken()
        {
            isValid = false;
            ErrorMessage = string.Empty;
        }
        public bool isValid { get; set; }
        public string ErrorMessage { get; set; }
    }

    public class Tweet
    {
        public DateTime CreatedDate { get; set; }
        public string TweetID { get; set; }
        public string Message { get; set; }
        public string LinkURL { get; set; }
        public string PictureURL { get; set; }        
        public string Type { get; set; }
        public string Source { get; set; }
    }    

    public class TwitterPostStatistics
    {
        public TwitterPostStatistics()
        {
            TweetID = string.Empty;            
            RetweetCount = 0;
            IsRetweeted = false;
            Error = string.Empty;
        }
        public string TweetID { get; set; }
        public bool? IsFavorited { get; set; }
        public int? RetweetCount { get; set; }
        public bool IsRetweeted { get; set; }        
        public string Error { get; set; }        
    }

    [Serializable]
    public class TwitterTweet
    {
        public string ID { get; set; }
        public string Text { get; set; }
        public string CreatedAt { get; set; }
		public DateTime CreatedDate { get; set; }
        public string Since { get; set; }
        public bool Favorited { get; set; }
        public TwitterInReplyTo InReplyTo { get; set; }
        public TwitterUserInfo User { get; set; }
        public TwitterRetweet ReTweet { get; set; }
        public string PhotoURL { get; set; }

        public TwitterTweet() { }
       
        public TwitterTweet(TwitterStatus tweet, string screenName, OAuthTokens tokens)
        {
            ID = Convert.ToString(tweet.Id);
            Text = AddLinks(tweet.Text);
            CreatedAt = tweet.CreatedDate.ToString("ddd MM dd yyyy HH:mm:ss zzz");
	        CreatedDate = tweet.CreatedDate;
            Since = TwitterAPI.GetSinceDescription(tweet.CreatedDate);
            Favorited = Convert.ToBoolean(tweet.IsFavorited);
            InReplyTo = new TwitterInReplyTo(tweet);
            User = new TwitterUserInfo(tweet.User, screenName);
            if (tweet.Entities != null)
            {
                foreach (var item in tweet.Entities)
                {
                    if (item.GetType().Name.ToLower() == "TwitterMediaEntity".ToLower())
                    {
                        PhotoURL = ((Twitterizer.Entities.TwitterMediaEntity)(item)).MediaUrl;
                    }
                }
            }
        }

        public static string AddLinks(string content)
        {
            Regex urlRegex = new Regex(@"(^|[\n ])(?<url>(www|ftp)\.[^ ,""\s<]*)", RegexOptions.IgnoreCase | RegexOptions.Compiled);
            content = urlRegex.Replace(content, " <a href=\"http://${url}\" target=\"_blank\">${url}</a>");
            Regex httpUrlRegex = new Regex(@"(^|[\n ])(?<url>(http://www\.|http://|https://)[^ ,""\s<]*)", RegexOptions.IgnoreCase | RegexOptions.Compiled);
            content = httpUrlRegex.Replace(content, " <a href=\"${url}\" target=\"_blank\">${url}</a>");
            Regex emailRegex = new Regex(@"(?<url>[a-zA-Z_0-9.-]+\@[a-zA-Z_0-9.-]+\.\w+\s)", RegexOptions.IgnoreCase | RegexOptions.Compiled);
            content = emailRegex.Replace(content, " <a href=\"mailto:${url}\">${url}</a> ");
            Regex trendRegex = new Regex(@"(?<hash>#)(?<trend>[a-zA-Z_0-9.-]+)", RegexOptions.IgnoreCase | RegexOptions.Compiled);
            content = trendRegex.Replace(content, "<a href=\"http://twitter.com/#!/search?q=%23${trend}\">#${trend}</a> ");
            Regex atRegex = new Regex(@"(?<at>@)(?<user>[a-zA-Z_0-9.-]+)", RegexOptions.IgnoreCase | RegexOptions.Compiled);
            content = atRegex.Replace(content, "@<a href=\"http://twitter.com/${user}\">${user}</a> ");
            return content;
        }
    }

    [Serializable]
    public class TwitterInReplyTo
    {
        public string ID { get; set; }
        public string UserID { get; set; }
        public string ScreenName { get; set; }

        public TwitterInReplyTo()
        {
            ID = string.Empty;
            UserID = string.Empty;
            ScreenName = string.Empty;
        }       
        public TwitterInReplyTo(TwitterStatus tweet)
        {
            ID = Convert.ToString(tweet.InReplyToStatusId);
            UserID = Convert.ToString(tweet.InReplyToUserId);
            ScreenName = Convert.ToString(tweet.InReplyToScreenName);
        }
    }

    [Serializable]
    public class TwitterUserInfo
    {
        public bool IsOwner { get; set; }
        public string Type { get; set; }
        public string ID { get; set; }
        public string UserID { get; set; }
        public string Name { get; set; }
        public string ScreenName { get; set; }
        public string CreatedAt { get; set; }
        public string ProfileImageUrl { get; set; }
        public string ProfileBackgroundImageUrl { get; set; }

        public TwitterUserInfo()
        {

        }
        
        public TwitterUserInfo(Twitterizer.TwitterUser user, string screenName)
        {
            ID = Convert.ToString(user.Id);
            UserID = Convert.ToString(user.Id);
            Type = user.ToString();
            Name = user.Name;
            IsOwner = user.ScreenName == screenName;
            ScreenName = user.ScreenName;
            CreatedAt = user.CreatedDate.Value.ToString("ddd MM dd yyyy HH:mm:ss zzz");
            ProfileImageUrl = user.ProfileImageLocation;
            ProfileBackgroundImageUrl = user.ProfileBackgroundImageLocation;
        }
    }

    [Serializable]
    public class TwitterRetweet
    {
        public string ID { get; set; }
        public string Text { get; set; }
        public string CreatedAt { get; set; }
        public string Since { get; set; }
        public bool Favorited { get; set; }
        public TwitterInReplyTo InReplyTo { get; set; }
        public TwitterUserInfo User { get; set; }

        public TwitterRetweet()
        {

        }
        
        public TwitterRetweet(TwitterStatus tweet, string screenName)
        {
            ID = Convert.ToString(tweet.Id);
            Text = TwitterTweet.AddLinks(tweet.Text);
            CreatedAt = tweet.CreatedDate.ToString("ddd MM dd yyyy HH:mm:ss zzz");
            Since = TwitterAPI.GetSinceDescription(tweet.CreatedDate);
            Favorited = Convert.ToBoolean(tweet.IsFavorited);
            InReplyTo = new TwitterInReplyTo(tweet);
            User = new TwitterUserInfo(tweet.User, screenName);
        }
    }

    public enum TwitterRequestResult
    {
        Success = 0,
        FileNotFound = 1,
        BadRequest = 2,
        Unauthorized = 3,
        NotAcceptable = 4,
        RateLimited = 5,
        TwitterIsDown = 6,
        TwitterIsOverloaded = 7,
        ConnectionFailure = 8,
        Unknown = 9,
        ProxyAuthenticationRequired = 10,
        Exception = 11,
    }
}
