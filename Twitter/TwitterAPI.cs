﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Web.Script.Serialization;
using SI;
using Twitter.Entities;
using Twitterizer;

namespace Twitter
{
    public static class TwitterAPI
    {
        //private static int TwitterMaxLength = 140;
        private static SInet _WebNet = null;
        private static int MaxRedirects = 2;
        private static string UserAgent = "Mozilla/4.0 (compatible; Windows NT)";
        private static int Timeout = 60 * 1000;
        private static int TweetperPage = 25;
        
        public static TwitterResponse StatusUpdate(string status, string link, string oauth_token, string oauth_token_secret, string oauth_consumer_key, string oauth_consumer_secret)
        {
            TwitterResponse objTwitterResponse = new TwitterResponse();
            OAuthTokens tokens = new OAuthTokens();
            int maxLength = 0;

            try
            {
                if (!string.IsNullOrEmpty(oauth_token) && !string.IsNullOrEmpty(oauth_token_secret))
                {
                    
                    int TwitterMaxLength = 140;
                    if (!string.IsNullOrEmpty(status) || link.Length > 0)
                    {
                        if (string.IsNullOrEmpty(status))
                        {
                            status = link;
                        }

                        int linklength = 0;

                        if (!string.IsNullOrEmpty(link))
                        {
                            //linklength = link.Length;
                            linklength = 23;
                        }

                        if (linklength > 0)
                        {
                            maxLength = Convert.ToInt32(TwitterMaxLength) - linklength - 1;
                            if (status.Length > maxLength)
                            {
                                status = status.Substring(0, maxLength);
                            }
                            status = status + " " + link;
                        }
                        else if (status.Length > Convert.ToInt32(TwitterMaxLength))
                        {
                            status = status.Substring(0, Convert.ToInt32(TwitterMaxLength));
                        }

                        tokens.ConsumerKey = oauth_consumer_key;
                        tokens.ConsumerSecret = oauth_consumer_secret;
                        tokens.AccessToken = oauth_token;
                        tokens.AccessTokenSecret = oauth_token_secret;

                        var tweetResponse = TwitterStatus.Update(tokens, status);

                        if (!String.IsNullOrEmpty(tweetResponse.ErrorMessage))
                        {
                            objTwitterResponse.error = tweetResponse.ErrorMessage;
                            objTwitterResponse.code = "0000";
                        }
                        else if (!String.IsNullOrEmpty(tweetResponse.Content))
                        {
                            if (tweetResponse.Content.StartsWith(@"{""errors"))
                            {
                                objTwitterResponse.error = tweetResponse.Result + ", "+  tweetResponse.Content;                                
                                objTwitterResponse.code = "0000";
                            }
                            else
                            {
                                objTwitterResponse.ID = Convert.ToString(tweetResponse.ResponseObject.Id);//.StringId;                                

                                objTwitterResponse.error = string.Empty;
                                objTwitterResponse.code = string.Empty;
                            }
                        }
                        else
                        {                            
                            objTwitterResponse.error = "Not able to post to twitter.";
                            objTwitterResponse.code = "000";
                        }
                    }
                    else
                    {
                        objTwitterResponse.error = "Missing Required Parameter: Status";
                        objTwitterResponse.code = "0000";
                    }
                }
                else
                {
                    objTwitterResponse.error = "Missing Required Parameter: oauth_token or oauth_token_secret";
                    objTwitterResponse.code = "0000";
                }
            }
            catch (Exception ex)
            {
                #region ErrorHandler
                objTwitterResponse.error = ex.Message;
                objTwitterResponse.code = "0000";
                #endregion
            }

            return objTwitterResponse;
        }

        public static string StatusUpdateWithMediaLog(string status, string imagePath, string oauth_token, string oauth_token_secret, string oauth_consumer_key, string oauth_consumer_secret)
        {
            string result = string.Empty;
            OAuthTokens tokens = new OAuthTokens();

            try
            {
                if (!string.IsNullOrEmpty(oauth_token) && !string.IsNullOrEmpty(oauth_token_secret))
                {
                    
                    if (!string.IsNullOrEmpty(status))
                    {
                        int linklength = 24;
                        int TwitterMaxLength = 140;
                        TwitterMaxLength = TwitterMaxLength - linklength;

                        if (status.Length > Convert.ToInt32(TwitterMaxLength))
                        {
                            status = status.Substring(0, Convert.ToInt32(TwitterMaxLength));
                        }

                        tokens.ConsumerKey = oauth_consumer_key;
                        tokens.ConsumerSecret = oauth_consumer_secret;
                        tokens.AccessToken = oauth_token;
                        tokens.AccessTokenSecret = oauth_token_secret;

                        byte[] fileData = DownloadData(imagePath, null, null);
                        result = "Status:" + status + ", status.Length:" + status.Length + ", TwitterMaxLength: " + TwitterMaxLength;
                        return result;                       
                    }                   
                }               
            }
            catch (Exception ex)
            {               
            }

            return result;
        }

        public static TwitterResponse StatusUpdateWithMedia(string status, string imagePath, string oauth_token, string oauth_token_secret, string oauth_consumer_key, string oauth_consumer_secret)
        {
            TwitterResponse objTwitterResponse = new TwitterResponse();
            OAuthTokens tokens = new OAuthTokens();
            
            try
            {
                if (!string.IsNullOrEmpty(oauth_token) && !string.IsNullOrEmpty(oauth_token_secret))
                {

                    if (!string.IsNullOrEmpty(status))
                    {
                        //TODO : Write a code to get from Twitter : https://dev.twitter.com/docs/api/1.1/get/help/configuration (characters_reserved_per_media: 23)
                        int linklength = 24;
                        int TwitterMaxLength = 140;
                        TwitterMaxLength = TwitterMaxLength - linklength;

                        if (status.Length > Convert.ToInt32(TwitterMaxLength))
                        {
                            status = status.Substring(0, Convert.ToInt32(TwitterMaxLength));
                        }

                        tokens.ConsumerKey = oauth_consumer_key;
                        tokens.ConsumerSecret = oauth_consumer_secret;
                        tokens.AccessToken = oauth_token;
                        tokens.AccessTokenSecret = oauth_token_secret;

                        byte[] fileData = DownloadData(imagePath, null, null);
                        
                        var tweetResponse = TwitterStatus.UpdateWithMedia(tokens, status, fileData);

                        if (!String.IsNullOrEmpty(tweetResponse.ErrorMessage))
                        {
                            objTwitterResponse.error = tweetResponse.ErrorMessage;
                            objTwitterResponse.code = "0000";
                        }
                        else if (!String.IsNullOrEmpty(tweetResponse.Content))
                        {
                            if (tweetResponse.Content.StartsWith(@"{""errors"))
                            {
                                objTwitterResponse.error = tweetResponse.Result + ", " + tweetResponse.Content;
                                objTwitterResponse.code = "0000";
                            }
                            else
                            {
                                objTwitterResponse.ID = Convert.ToString(tweetResponse.ResponseObject.Id);//.StringId;                                

                                objTwitterResponse.error = string.Empty;
                                objTwitterResponse.code = string.Empty;
                            }

                        }
                        else
                        {
                            objTwitterResponse.error = "Not able to post to twitter.";
                            objTwitterResponse.code = "000";
                        }


                        //if (!String.IsNullOrEmpty(tweetResponse.ErrorMessage))
                        //{
                        //    objTwitterResponse.error = tweetResponse.ErrorMessage;
                        //    objTwitterResponse.code = "0000";
                        //}
                        //else
                        //{
                        //    objTwitterResponse.ID = Convert.ToString(tweetResponse.ResponseObject.Id);//.StringId;
                        //    objTwitterResponse.message = tweetResponse.ErrorMessage;

                        //    objTwitterResponse.error = string.Empty;
                        //    objTwitterResponse.code = string.Empty;
                        //}
                    }
                    else
                    {
                        objTwitterResponse.error = "Missing Required Parameter: Status";
                        objTwitterResponse.code = "0000";
                    }
                }
                else
                {
                    objTwitterResponse.error = "Missing Required Parameter: oauth_token or oauth_token_secret";
                    objTwitterResponse.code = "0000";
                }
            }
            catch (Exception ex)
            {
                #region ErrorHandler
                objTwitterResponse.error = ex.Message;
                objTwitterResponse.code = "0000";
                #endregion
            }

            return objTwitterResponse;
        }

        public static TwitterPageInsightsResponse GetTwitterPageInsights(string uniqueID, string oauth_token, string oauth_token_secret, string oauth_consumer_key, string oauth_consumer_secret)
        {
            TwitterPageInsightsResponse objTwitterPageInsightsResponse = new TwitterPageInsightsResponse();

            TwitterResponse objTwitterResponse = new TwitterResponse();
            OAuthTokens tokens = new OAuthTokens();

            try
            {
                if (!string.IsNullOrEmpty(oauth_token) && !string.IsNullOrEmpty(oauth_token_secret))
                {
                    tokens.ConsumerKey = oauth_consumer_key;
                    tokens.ConsumerSecret = oauth_consumer_secret;
                    tokens.AccessToken = oauth_token;
                    tokens.AccessTokenSecret = oauth_token_secret;

                    var tweetResponse = TwitterUser.Show(tokens, Convert.ToDecimal(uniqueID));
                    var jss = new JavaScriptSerializer();

                    if (!String.IsNullOrEmpty(tweetResponse.ErrorMessage))
                    {
                        objTwitterResponse.error = tweetResponse.ErrorMessage;
                        objTwitterResponse.code = "0000";

                        try
                        {
                            objTwitterPageInsightsResponse = jss.Deserialize<TwitterPageInsightsResponse>(tweetResponse.Content);
                        }
                        catch (Exception ex)
                        {
                            objTwitterResponse.error = objTwitterResponse.error + " ,Exception : " + ex.Message;
                        }

                    }
                    else
                    {
                        objTwitterPageInsightsResponse = jss.Deserialize<TwitterPageInsightsResponse>(tweetResponse.Content);
                    }
                }
                else
                {
                    objTwitterResponse.error = "Missing Required Parameter: oauth_token or oauth_token_secret";
                    objTwitterResponse.code = "0000";
                }
            }
            catch (Exception ex)
            {
                #region ErrorHandler

                objTwitterResponse.error = ex.Message;
                objTwitterResponse.code = "0000";

                #endregion
            }

            objTwitterPageInsightsResponse.errors = objTwitterResponse;

            return objTwitterPageInsightsResponse;
        }

        public static TwitterPostStatistics  GetTwitterStatusStatistics(decimal statusID, string oauth_token, string oauth_token_secret, string oauth_consumer_key, string oauth_consumer_secret)
        {
            string result = string.Empty;
            OAuthTokens tokens = new OAuthTokens();
            TwitterPostStatistics PostStatistics = new TwitterPostStatistics();
            try
            {
                if (!string.IsNullOrEmpty(oauth_token) && !string.IsNullOrEmpty(oauth_token_secret) && statusID > 0 )
                {
                    tokens.ConsumerKey = oauth_consumer_key;
                    tokens.ConsumerSecret = oauth_consumer_secret;
                    tokens.AccessToken = oauth_token;
                    tokens.AccessTokenSecret = oauth_token_secret;

                    var tweetResponse = TwitterStatus.Show(tokens, statusID);

                    if (tweetResponse != null)
                    {
                        if (tweetResponse.Result == RequestResult.Success)
                        {
                            PostStatistics.TweetID =  tweetResponse.ResponseObject.StringId;
                            PostStatistics.IsFavorited = tweetResponse.ResponseObject.IsFavorited;
                            PostStatistics.RetweetCount = tweetResponse.ResponseObject.RetweetCount;
                            PostStatistics.IsRetweeted = tweetResponse.ResponseObject.Retweeted;                                                        
                        }
                        else
                        {
                            PostStatistics.Error = tweetResponse.Content;
                        }
                    }
                    else
                    {
                        PostStatistics.Error = "Not able to get Twitter PostStatistics.";
                    }

                    //result = tweetResponse.Content;
                }
            }
            catch (Exception ex)
            {                
                PostStatistics.Error = ex.Message;
            }

            return PostStatistics;
        }

        public static bool ValidateToken(string oauth_token, string oauth_token_secret, string oauth_consumer_key, string oauth_consumer_secret)
        {
            bool result = false;            
            OAuthTokens tokens = new OAuthTokens();

            try
            {
                if (!string.IsNullOrEmpty(oauth_token) && !string.IsNullOrEmpty(oauth_token_secret))
                {
                    tokens.ConsumerKey = oauth_consumer_key;
                    tokens.ConsumerSecret = oauth_consumer_secret;
                    tokens.AccessToken = oauth_token;
                    tokens.AccessTokenSecret = oauth_token_secret;

                    var tokenStatusResponse = TwitterAccount.VerifyCredentials(tokens);

                    if (tokenStatusResponse.Result.ToString().ToLower() == "success")
                    {
                        result = true;
                    }
                }

            }
            catch (Exception ex)
            {
                throw ex;
            }

            return result;
        }

        public static TwitterResponseToken ValidateToken2(string oauth_token, string oauth_token_secret, string oauth_consumer_key, string oauth_consumer_secret)
        {
            //bool result = false;
            OAuthTokens tokens = new OAuthTokens();
            TwitterResponseToken result = new TwitterResponseToken();
            try
            {
                if (!string.IsNullOrEmpty(oauth_token) && !string.IsNullOrEmpty(oauth_token_secret))
                {
                    tokens.ConsumerKey = oauth_consumer_key;
                    tokens.ConsumerSecret = oauth_consumer_secret;
                    tokens.AccessToken = oauth_token;
                    tokens.AccessTokenSecret = oauth_token_secret;

                    TwitterResponse<TwitterUser> tokenStatusResponse = TwitterAccount.VerifyCredentials(tokens);

                    if (tokenStatusResponse.Result.ToString().ToLower() == "success")
                    {
                        result.isValid = true;
                        result.ErrorMessage = string.Empty;
                    }
                    else
                    {
                        result.isValid = false;
                        result.ErrorMessage = tokenStatusResponse.ErrorMessage;

                        if (string.IsNullOrEmpty(result.ErrorMessage))
                        {
                            result.ErrorMessage = tokenStatusResponse.Content;
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                throw ex;
            }

            return result;
        }
                
        public static TwitterStatues GetListofTwitterStatus(string oauth_token, string oauth_token_secret, string oauth_consumer_key, string oauth_consumer_secret)
        {
            TwitterStatues twitterStatus = new Entities.TwitterStatues();
            TwitterResponse objTwitterResponse = new TwitterResponse();
            List<Tweet> tweets = new List<Tweet>();

            try
            {
                OAuthTokens oAuthTokens = new OAuthTokens();
                oAuthTokens.AccessToken = oauth_token;
                oAuthTokens.AccessTokenSecret = oauth_token_secret;
                oAuthTokens.ConsumerKey = oauth_consumer_key;
                oAuthTokens.ConsumerSecret = oauth_consumer_secret;

                UserTimelineOptions usertimelineOptions = new UserTimelineOptions();
                usertimelineOptions.Page = 1;
                usertimelineOptions.Count = 200;
                usertimelineOptions.IncludeRetweets = false;

                TwitterResponse<TwitterStatusCollection> twitterStatusCollections = Twitterizer.TwitterTimeline.UserTimeline(oAuthTokens, usertimelineOptions);

                if (twitterStatusCollections.Result == Twitterizer.RequestResult.Success)
                {
                    if (twitterStatusCollections.ResponseObject != null)
                    {
                        foreach (var twitterStatusCollection in twitterStatusCollections.ResponseObject)
                        {
                            Tweet tweet = new Tweet();

                            tweet.CreatedDate = twitterStatusCollection.CreatedDate.ToUniversalTime();
                            tweet.Message = twitterStatusCollection.Text;
                            tweet.TweetID = twitterStatusCollection.StringId;
                            tweet.Source = twitterStatusCollection.Source;

                            foreach (var Entitie in twitterStatusCollection.Entities)
	                        {

                                if (Entitie.GetType().Name.ToLower() == "TwitterUrlEntity".ToLower())
                                {
                                    Twitterizer.Entities.TwitterUrlEntity twitterUrlEntity = ((Twitterizer.Entities.TwitterUrlEntity)(Entitie));

                                    tweet.LinkURL = twitterUrlEntity.ExpandedUrl;                                    
                                    tweet.Type = "Link";
                                }                                
                                else if (Entitie.GetType().Name.ToLower() == "TwitterMediaEntity".ToLower())
                                {
                                    Twitterizer.Entities.TwitterMediaEntity twitterMediaEntity = ((Twitterizer.Entities.TwitterMediaEntity)(Entitie));

                                    tweet.PictureURL = twitterMediaEntity.MediaUrl;                                    
                                    tweet.Type = "Photo";
                                }
	                        }

                            if (string.IsNullOrWhiteSpace(tweet.Type))
                            {
                                tweet.Type = "Status";
                            }

                            tweets.Add(tweet);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                #region ErrorHandler
                objTwitterResponse.error = ex.Message;
                objTwitterResponse.code = "0000";
                #endregion
            }

            twitterStatus.twitterResponse = objTwitterResponse;
            twitterStatus.tweets = tweets;

            return twitterStatus;
        }

        public static List<TwitterTweet> GetHomeTweets(string oauth_token, string oauth_token_secret, string oauth_consumer_key, string oauth_consumer_secret,string screenName, string count, string sinceID)
        {
            List<TwitterTweet> results = new List<TwitterTweet>();
            
            try
            {
                int PageNumber = 1;                

                if (!string.IsNullOrEmpty(count))
                {
                    PageNumber = (int)Math.Ceiling(double.Parse(count) / TweetperPage) + 1;
                }

                OAuthTokens oAuthTokens = new OAuthTokens();
                oAuthTokens.ConsumerKey = oauth_consumer_key;
                oAuthTokens.ConsumerSecret = oauth_consumer_secret;
                oAuthTokens.AccessToken = oauth_token;
                oAuthTokens.AccessTokenSecret = oauth_token_secret;

                TimelineOptions timelineOptions = new TimelineOptions();
                timelineOptions.Page = PageNumber;
                timelineOptions.Count = TweetperPage;

                if (!string.IsNullOrEmpty(sinceID) && sinceID != "0")
                {
                    timelineOptions.SinceStatusId = ulong.Parse(sinceID);
                }
                
                TwitterResponse<TwitterStatusCollection> Tweets = Twitterizer.TwitterTimeline.HomeTimeline(oAuthTokens, timelineOptions);

                foreach (var tweet in Tweets.ResponseObject)
                {
                    try
                    {
                        results.Add(new TwitterTweet(tweet, screenName, oAuthTokens));
                    }
                    catch
                    {
                    }
                }
            }
            catch (Exception ex)
            {
                results = new List<TwitterTweet>();
            }

            return results;

        }

        public static List<TwitterTweet> GetUserTweets(string oauth_token, string oauth_token_secret, string oauth_consumer_key, string oauth_consumer_secret, string screenName, string count, string sinceID)
        {
             List<TwitterTweet> results = new List<TwitterTweet>();
             try
             {
                 int PageNumber = 1;
                 if (!string.IsNullOrEmpty(count))
                 {
                     PageNumber = (int)Math.Ceiling(double.Parse(count) / TweetperPage) + 1;
                 }

                 OAuthTokens oAuthTokens = new OAuthTokens();
                 oAuthTokens.ConsumerKey = oauth_consumer_key;
                 oAuthTokens.ConsumerSecret = oauth_consumer_secret;
                 oAuthTokens.AccessToken = oauth_token;
                 oAuthTokens.AccessTokenSecret = oauth_token_secret;

                 UserTimelineOptions usertimelineOptions = new UserTimelineOptions();
                 usertimelineOptions.Page = PageNumber;
                 usertimelineOptions.Count = TweetperPage;
                 usertimelineOptions.IncludeRetweets = true;

                 if (!string.IsNullOrEmpty(sinceID) && sinceID != "0")
                 {
                     usertimelineOptions.SinceStatusId = ulong.Parse(sinceID);
                 }

                 TwitterResponse<TwitterStatusCollection> Tweets = Twitterizer.TwitterTimeline.UserTimeline(oAuthTokens, usertimelineOptions);
                 foreach (var tweet in Tweets.ResponseObject)
                 {
                     try
                     {
                         results.Add(new TwitterTweet(tweet, screenName, oAuthTokens));
                     }
                     catch
                     {
                     }
                 }
             }
             catch (Exception ex)
             {
                 results = new List<TwitterTweet>();
             }

             return results;
        }

        public static List<TwitterTweet> GetMentions(string oauth_token, string oauth_token_secret, string oauth_consumer_key, string oauth_consumer_secret, string screenName, string count, string sinceID)
        {
            List<TwitterTweet> results = new List<TwitterTweet>();
            try
            {
                int PageNumber = 1;
                if (!string.IsNullOrEmpty(count))
                {
                    PageNumber = (int)Math.Ceiling(double.Parse(count) / TweetperPage) + 1;
                }

                OAuthTokens oAuthTokens = new OAuthTokens();
                oAuthTokens.ConsumerKey = oauth_consumer_key;
                oAuthTokens.ConsumerSecret = oauth_consumer_secret;
                oAuthTokens.AccessToken = oauth_token;
                oAuthTokens.AccessTokenSecret = oauth_token_secret;

                TimelineOptions timelineOptions = new TimelineOptions();
                timelineOptions.Page = PageNumber;
                timelineOptions.Count = TweetperPage;


                if (!string.IsNullOrEmpty(sinceID) && sinceID != "0")
                {
                    timelineOptions.SinceStatusId = ulong.Parse(sinceID);
                }

                TwitterResponse<TwitterStatusCollection> Tweets = Twitterizer.TwitterTimeline.Mentions(oAuthTokens, timelineOptions);
                foreach (var tweet in Tweets.ResponseObject)
                {
                    try
                    {
                        results.Add(new TwitterTweet(tweet, screenName, oAuthTokens));
                    }
                    catch
                    {
                    }
                }
            }
            catch (Exception ex)
            {
                results = new List<TwitterTweet>();
            }

            return results;
        }

        public static TwitterRequestResult SetFavorite(string oauth_token, string oauth_token_secret, string oauth_consumer_key, string oauth_consumer_secret, string statusID)
        {
            TwitterRequestResult result = new TwitterRequestResult();
            result = TwitterRequestResult.Unknown;

            try
            {
                if (Convert.ToDecimal(statusID.Trim()) > 0)
                {
                    OAuthTokens oAuthTokens = new OAuthTokens();
                    oAuthTokens.ConsumerKey = oauth_consumer_key;
                    oAuthTokens.ConsumerSecret = oauth_consumer_secret;
                    oAuthTokens.AccessToken = oauth_token;
                    oAuthTokens.AccessTokenSecret = oauth_token_secret;

                    var response = Twitterizer.TwitterFavorite.Create(oAuthTokens, Convert.ToDecimal(statusID.Trim()));           
         
                    if(response != null)
                    {
                        if(response.Result != null)
                        {
                            result = (TwitterRequestResult)response.Result;
                        }
                    }                    
                }
            }
            catch (Exception ex)
            {
                //TO DO: Logging
                result = TwitterRequestResult.Exception;
            }

            return result;
        }

        public static TwitterRequestResult DeleteFavorite(string oauth_token, string oauth_token_secret, string oauth_consumer_key, string oauth_consumer_secret, string statusID)
        {
            TwitterRequestResult result = new TwitterRequestResult();
            result = TwitterRequestResult.Unknown;

            try
            {
                if (Convert.ToDecimal(statusID.Trim()) > 0)
                {
                    OAuthTokens oAuthTokens = new OAuthTokens();
                    oAuthTokens.ConsumerKey = oauth_consumer_key;
                    oAuthTokens.ConsumerSecret = oauth_consumer_secret;
                    oAuthTokens.AccessToken = oauth_token;
                    oAuthTokens.AccessTokenSecret = oauth_token_secret;

                    var response = Twitterizer.TwitterFavorite.Delete(oAuthTokens, Convert.ToDecimal(statusID.Trim()));
                    if (response != null)
                    {
                        if (response.Result != null)
                        {
                            result = (TwitterRequestResult)response.Result;
                        }
                    }   
                }
            }
            catch (Exception ex)
            {
                //TO DO: Logging                
                result = TwitterRequestResult.Exception;
            }

            return result;
        }

        public static TwitterTweet Retweet(string oauth_token, string oauth_token_secret, string oauth_consumer_key, string oauth_consumer_secret, string statusID, string screenName)
        {
            TwitterTweet tweet = new TwitterTweet();
            try
            {
                if (!string.IsNullOrEmpty(statusID))
                {
                    OAuthTokens oAuthTokens = new OAuthTokens();
                    oAuthTokens.ConsumerKey = oauth_consumer_key;
                    oAuthTokens.ConsumerSecret = oauth_consumer_secret;
                    oAuthTokens.AccessToken = oauth_token;
                    oAuthTokens.AccessTokenSecret = oauth_token_secret;

                    TwitterResponse<TwitterStatus> retweet = Twitterizer.TwitterStatus.Retweet(oAuthTokens, Convert.ToDecimal(statusID));

                    if (retweet != null)
                    {
                        tweet = new TwitterTweet(retweet.ResponseObject, screenName, null);
                    }
                }

                return tweet;
            }
            catch (Exception ex)
            {
                //TO DO: Logging
                throw;
            }
        }

        public static TwitterRequestResult ReplyToTweet(string oauth_token, string oauth_token_secret, string oauth_consumer_key, string oauth_consumer_secret, string status, string inReplyToStatusID)
        {
            TwitterRequestResult result = new TwitterRequestResult();
            result = TwitterRequestResult.Unknown;

            try
            {              
                if (Convert.ToDecimal(inReplyToStatusID.Trim()) > 0)
                {
                    OAuthTokens oAuthTokens = new OAuthTokens();
                    oAuthTokens.ConsumerKey = oauth_consumer_key;
                    oAuthTokens.ConsumerSecret = oauth_consumer_secret;
                    oAuthTokens.AccessToken = oauth_token;
                    oAuthTokens.AccessTokenSecret = oauth_token_secret;

                    StatusUpdateOptions options = new StatusUpdateOptions();
                    options.InReplyToStatusId = Convert.ToDecimal(inReplyToStatusID);

                    var response = Twitterizer.TwitterStatus.Update(oAuthTokens, status, options);

                    if (response != null)
                    {
                        if (response.Result != null)
                        {
                            result = (TwitterRequestResult)response.Result;
                        }
                    } 
                }

            }
            catch (Exception ex)
            {
                //TO DO: Logging
                result = TwitterRequestResult.Exception;
            }

            return result;
        }

        public static TwitterRequestResult DeleteTweet(string oauth_token, string oauth_token_secret, string oauth_consumer_key, string oauth_consumer_secret, string statusID)
        {
            TwitterRequestResult result = new TwitterRequestResult();
            result = TwitterRequestResult.Unknown;

            try
            {
                if (!string.IsNullOrEmpty(statusID))
                {

                    if (Convert.ToDecimal(statusID.Trim()) > 0)
                    {
                        OAuthTokens oAuthTokens = new OAuthTokens();
                        oAuthTokens.ConsumerKey = oauth_consumer_key;
                        oAuthTokens.ConsumerSecret = oauth_consumer_secret;
                        oAuthTokens.AccessToken = oauth_token;
                        oAuthTokens.AccessTokenSecret = oauth_token_secret;

                        var response = Twitterizer.TwitterStatus.Delete(oAuthTokens, Convert.ToDecimal(statusID.Trim()));

                        if (response != null)
                        {
                            if (response.Result != null)
                            {
                                result = (TwitterRequestResult)response.Result;
                            }
                        } 
                    }
                }
            }
            catch (Exception ex)
            {
                //TO DO: Logging
                result = TwitterRequestResult.Exception;
            }

            return result;
        }

        private static byte[] DownloadData(string requestUrl, Dictionary<string, string> PostData, string userAgent = null)
        {
            try
            {
                if (_WebNet == null)
                    _WebNet = new SInet(false, MaxRedirects, null, Timeout);

                if (!string.IsNullOrWhiteSpace(userAgent))
                {
                    _WebNet.UserAgent = userAgent;
                }

                if (PostData == null)
                    PostData = new Dictionary<string, string>();

                if (PostData.Count == 0)
                    return _WebNet.DownloadData(requestUrl, null);
                return _WebNet.DownloadData(requestUrl, PostData);
            }
            catch (WebException webEx)
            {
                throw webEx;
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return null;
		}

        public static string GetSinceDescription(DateTime postedOn)
        {
            postedOn = postedOn.ToUniversalTime();
            DateTime currentDate = DateTime.UtcNow;

            TimeSpan timegap = currentDate.Subtract(postedOn);

            if (timegap.Days >= 7)
                return string.Concat(postedOn.ToString("MMMM dd, yyyy"), " at ", postedOn.ToString("hh:mm tt"));
            else if (timegap.Days > 1)
                return string.Concat(timegap.Days, " days ago");
            else if (timegap.Days == 1)
                return " yesterday";
            else if (timegap.Hours >= 2)
                return string.Concat(timegap.Hours, " hours ago");
            else if (timegap.Hours >= 1)
                return " an hour ago";
            else if (timegap.Minutes >= 60)
                return " more than an hour ago";
            else if (timegap.Minutes >= 5)
                return string.Concat(" ", timegap.Minutes, " minutes ago");
            else if (timegap.Minutes >= 1)
                return " a few minutes ago";
            else
                return " less than a minute ago";
        }

		#region oAuth

	    public static string GetAuthUrl(string consumerKey, string consumerSecret, string callback)
	    {
			OAuthTokenResponse requestToken = OAuthUtility.GetRequestToken(consumerKey, consumerSecret, callback);

			Uri urlAuth = OAuthUtility.BuildAuthorizationUri(requestToken.Token);

		    return urlAuth.AbsoluteUri;
	    }

		public static TwitterOAuthResponse GetTwitterOAuthTokens(string appId, string appSecret, string oauth_token, string oauth_verifier)
		{
			TwitterOAuthResponse response = new TwitterOAuthResponse();

			OAuthTokenResponse accessToken = OAuthUtility.GetAccessToken(appId, appSecret, oauth_token, oauth_verifier);

			if (!string.IsNullOrEmpty(accessToken.Token))
			{

				OAuthTokens tokens = new OAuthTokens();
				tokens.AccessToken = accessToken.Token;
				tokens.AccessTokenSecret = accessToken.TokenSecret;
				tokens.ConsumerKey = appId;
				tokens.ConsumerSecret = appSecret;

				TwitterResponse<TwitterUser> authUser = TwitterUser.Show(tokens, accessToken.UserId);
				if (authUser.Result == Twitterizer.RequestResult.Success)
				{
					response.token = accessToken.Token;
					response.tokenSecret = accessToken.TokenSecret;
					response.screenName = accessToken.ScreenName;
					response.userID = accessToken.UserId.ToString();
					response.pictureURL = authUser.ResponseObject.ProfileImageLocation;
					response.webSite = authUser.ResponseObject.Website;
					response.success = true;
				}
			}

			return response;
		}

		#endregion
	}
}
