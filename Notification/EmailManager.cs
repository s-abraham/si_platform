﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;
using Notification.Entities;
using SendGridMail;
using SendGridMail.Transport;
using SI;

namespace Notification
{
    public class EmailManager
    {
        private string sendGrid_Username = string.Empty;
        private string sendGrid_Password = string.Empty;
        private bool sendGrid_UseClickTracking = false;
        private bool sendGrid_UseOpenTracking = false;
        private bool sendGrid_UseSpamCheck = false;

        public EmailManager()
        {


        }

        public bool sendNotification(string Category, string Subject, string Body, string SendFrom, string SendTo, string SendCC, string SendBCC)
        {
            bool sendResult = false;
            sendGrid_Username = Settings.GetSetting("sendgrid.login", "SDPlatform");
            sendGrid_Password = Settings.GetSetting("sendgrid.password", "sHireUnder^$7*546");
            sendGrid_UseClickTracking = true;
            sendGrid_UseOpenTracking = true;
            sendGrid_UseSpamCheck = true;

            try
            {
                EmailDetail emailDetail = new EmailDetail();

                if (!string.IsNullOrWhiteSpace(Category) && !string.IsNullOrWhiteSpace(Body) && !string.IsNullOrWhiteSpace(Subject) && !string.IsNullOrWhiteSpace(SendFrom))
                {
                    if (!string.IsNullOrEmpty(SendTo))
                    {

                        List<string> SendTolist = new List<string> { SendTo };
                        List<string> SendCClist = new List<string> { SendCC };
                        List<string> SendBCClist = new List<string> { SendBCC };

                        //List<string> SendTolist = new List<string> { "reynolds@autostartups.com", "s.abraham@socialdealer.com", "a.patel@socialdealer.com" };
                        //List<string> SendCClist = new List<string>();
                        //List<string> SendBCClist = new List<string>();


                        emailDetail.SendFrom = SendFrom;
                        emailDetail.Body = Body;
                        emailDetail.Category = Category;
                        emailDetail.DestinationToList = SendTolist;
                        if (!string.IsNullOrEmpty(SendCC)) emailDetail.DestinationCCList = SendCClist;
                        if (!string.IsNullOrEmpty(SendBCC)) emailDetail.DestinationBCCList = SendBCClist;
                        emailDetail.Subject = Subject;
                        emailDetail.UseClickTracking = sendGrid_UseClickTracking;
                        emailDetail.UseOpenTracking = sendGrid_UseOpenTracking;
                        emailDetail.UseSpamCheck = sendGrid_UseSpamCheck;

                        sendResult = SendMail(emailDetail);
                    }
                }
            }
            catch (Exception ex)
            {
                sendResult = false;
            }
            return sendResult;
        }

        private bool SendMail(EmailDetail emailDetail)
        {
            var result = false;

            var myMessage = SendGrid.GenerateInstance();

            foreach (var destinationTo in emailDetail.DestinationToList)
            {
                myMessage.AddTo(destinationTo);
            }

            foreach (var destinationCC in emailDetail.DestinationCCList)
            {
                myMessage.AddCc(destinationCC);
            }

            foreach (var destinationBCC in emailDetail.DestinationBCCList)
            {
                myMessage.AddBcc(destinationBCC);
            }

            myMessage.From = new MailAddress(emailDetail.SendFrom);
            myMessage.Subject = emailDetail.Subject;

            myMessage.Html = emailDetail.Body;
            myMessage.Headers.Add("X-SMTPAPI", "{\"category\":" + emailDetail.Category + "\"}");

            myMessage.InitializeFilters();

            if (emailDetail.UseClickTracking)
            {
                myMessage.EnableClickTracking(true);
            }

            if (emailDetail.UseOpenTracking)
            {
                myMessage.EnableOpenTracking();
            }

            if (emailDetail.UseSpamCheck)
            {
                myMessage.EnableSpamCheck();
            }

            // Create credentials, specifying your user name and password.
            var credentials = new NetworkCredential(sendGrid_Username, sendGrid_Password);

            // Create an SMTP transport for sending email.


            var transportSMTP = SMTP.GenerateInstance(credentials, port: 2525);

            // Send the email.
            transportSMTP.Deliver(myMessage);

            result = true;


            return result;
        }
    }
}
