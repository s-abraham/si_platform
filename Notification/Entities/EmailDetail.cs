﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Notification.Entities
{
    [Serializable()]
    public class EmailDetail
    {
        public EmailDetail()
        {
            DestinationToList = new List<string>();
            DestinationCCList = new List<string>();
            DestinationBCCList = new List<string>();
            Subject = string.Empty;
            Body = string.Empty;
            Category = string.Empty;
            UseClickTracking = true;
            UseOpenTracking = true;
            UseSpamCheck = true;
        }

        [System.Xml.Serialization.XmlElement("DestinationToList")]
        public List<string> DestinationToList { get; set; }

        [System.Xml.Serialization.XmlElement("DestinationCCList")]
        public List<string> DestinationCCList { get; set; }

        [System.Xml.Serialization.XmlElement("DestinationBCCList")]
        public List<string> DestinationBCCList { get; set; }

        [System.Xml.Serialization.XmlElement("Subject")]
        public string Subject { get; set; }

        [System.Xml.Serialization.XmlElement("Body")]
        public string Body { get; set; }

        [System.Xml.Serialization.XmlElement("Category")]
        public string Category { get; set; }

        [System.Xml.Serialization.XmlElement("UseClickTracking")]
        public bool UseClickTracking { get; set; }

        [System.Xml.Serialization.XmlElement("UseOpenTracking")]
        public bool UseOpenTracking { get; set; }

        [System.Xml.Serialization.XmlElement("UseSpamCheck")]
        public bool UseSpamCheck { get; set; }

        [System.Xml.Serialization.XmlElement("UseSpamCheck")]
        public string SendFrom { get; set; }

        public override string ToString()
        {
            var result = new StringBuilder();

            result.Append(" To[" + string.Join(",", DestinationToList.ToArray()) + "]");

            result.Append(" Subject: [" + Subject + "]");
            result.Append(" Body: [" + Body + "]");
            result.Append(" Category: [" + Category + "]");
            result.Append(" UseClickTracking: [" + UseClickTracking + "]");
            result.Append(" UseOpenTracking: [" + UseOpenTracking + "]");
            result.Append(" UseSpamCheck: [" + UseSpamCheck + "]");

            //result.Append(" CC[" + string.Join(",", DestinationCCList.ToArray()) + "]");
            //result.Append(" BCC[" + string.Join(",", DestinationBCCList.ToArray()) + "]");

            return result.ToString();
        }
    }
}
